--EquipmentInstanceInfoPanel.lua
local EquipmentInstanceInfoPanel = Class.EquipmentInstanceInfoPanel(ClassTypes.BasePanel)

local BloodDungeonDataHelper = GameDataHelper.BloodDungeonDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local InstanceConfig = GameConfig.InstanceConfig

function EquipmentInstanceInfoPanel:Awake()
    self.disableCameraCulling = true
	self:InitCallback()
	self:InitComps()
end

function EquipmentInstanceInfoPanel:InitCallback()
end

function EquipmentInstanceInfoPanel:InitComps()
    self._isOpen = true
	self._btnExpand = self:FindChildGO("Container_Content/Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
    
	self._containerInfo = self:FindChildGO("Container_Content/Container_Info")
    self._initPosition = self._containerInfo.transform.localPosition
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
	self._imgLeft = self:FindChildGO("Container_Content/Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Container_Content/Button_Expand/Image_Right")

    self._nameText = self:GetChildComponent("Container_Content/Button_Expand/Text_Name","TextMeshWrapper")
    self._titleText = self:GetChildComponent("Container_Content/Container_Info/Text_Title","TextMeshWrapper")
    self._descText = self:GetChildComponent("Container_Content/Container_Info/Text_Desc","TextMeshWrapper")
end

function EquipmentInstanceInfoPanel:OnShow(data)
    self:SetText(data)
end

function EquipmentInstanceInfoPanel:SetData(data)
    self:SetText(data)
end

function EquipmentInstanceInfoPanel:OnClose()
end

function EquipmentInstanceInfoPanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function EquipmentInstanceInfoPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    -- self._containerInfo.transform.localPosition = Vector3.New(-280, 0, 0)
    self._tweenPosition:SetFromToPos(self._containerInfo.transform.localPosition, self._initPosition, 0.4, 1, nil)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(0, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function EquipmentInstanceInfoPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetFromToPos(self._initPosition, Vector3.New(self._initPosition.x - 280, self._initPosition.y, self._initPosition.z), 0.4, 1, nil)
    -- self._containerInfo.transform.localPosition = Vector3.New(0, 0, 0)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(-280, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

--{}
--instanceId:副本id
--num：当前波数
--totalNum：前波怪物总数
--killNum：当前波已杀死怪物数量
function EquipmentInstanceInfoPanel:SetText(data)    
    if data == nil then return end
    local num = data.num
    local instanceId = data.instanceId
    local texts = BloodDungeonDataHelper:GetDungeonDesc(instanceId)
    if texts and texts[num] and texts[num][1] then
        self._titleText.text = LanguageDataHelper.CreateContent(texts[num][1], {["0"]= num})
                                .. data.killNum .. "/" .. data.totalNum
    else
        self._titleText.text = ""
    end

    if texts and texts[num] and texts[num][2] then
        self._descText.text = LanguageDataHelper.CreateContent(texts[num][2])
    else
        self._descText.text = ""
    end
    self._nameText.text = LanguageDataHelper.CreateContent(BloodDungeonDataHelper:GetDesc(instanceId))
end