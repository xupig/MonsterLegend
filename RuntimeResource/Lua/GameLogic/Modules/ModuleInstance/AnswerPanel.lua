--AnswerPanel.lua
local AnswerPanel = Class.AnswerPanel(ClassTypes.BasePanel)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PetInstanceDataHelper = GameDataHelper.PetInstanceDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local QuestionDataHelper = GameDataHelper.QuestionDataHelper
local QuestionRankRewardDataHelper = GameDataHelper.QuestionRankRewardDataHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local public_config = GameWorld.public_config
local AnswerConfig = GameConfig.AnswerConfig
local AnswerManager = PlayerManager.AnswerManager
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale
local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local TaskCommonManager = PlayerManager.TaskCommonManager
local PlayerActionManager = GameManager.PlayerActionManager
local StringStyleUtil = GameUtil.StringStyleUtil
local XImageFlowLight = GameMain.XImageFlowLight

local result_scale_time = 0.5
local result_time = 1

require"Modules.ModuleInstance.ChildComponent.AnswerRankListItem"
local AnswerRankListItem = ClassTypes.AnswerRankListItem

require"Modules.ModuleInstance.ChildComponent.AnswerRankRewardListItem"
local AnswerRankRewardListItem = ClassTypes.AnswerRankRewardListItem

function AnswerPanel:Awake()
    self.disableCameraCulling = true
	self:InitCallback()
	self:InitComps()
end

function AnswerPanel:InitCallback()
    self._answerChange = function(flag) self:AnswerChange(flag) end
end

function AnswerPanel:InitComps()
    self._deviation = (GUIManager.canvasWidth - GUIManager.PANEL_WIDTH)/2
	self._containerInfo = self:FindChildGO("Container_Content/Container_Info")
    self:FindChildGO("Container_Content").transform.localPosition = self:FindChildGO("Container_Content").transform.localPosition + Vector3.New(-self._deviation, 0, 0)
    self._initPosition = self._containerInfo.transform.localPosition


    self._questionStart = false
    self._isOpen = true
	self._btnExpand = self:FindChildGO("Container_Content/Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
    
    self._questionContainer = self:FindChildGO("Container_Answer")
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
	self._imgLeft = self:FindChildGO("Container_Content/Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Container_Content/Button_Expand/Image_Right")

    self._answer1Text = self:GetChildComponent("Container_Answer/Button_Answer_1/Text_Answer_1","TextMeshWrapper")
    self._answer2Text = self:GetChildComponent("Container_Answer/Button_Answer_2/Text_Answer_2","TextMeshWrapper")
    self._questionSecondsText = self:GetChildComponent("Container_Answer/Text_Seconds","TextMeshWrapper")
    self._expText = self:GetChildComponent("Container_Content/Container_Info/Container_Content/Text_Exp","TextMeshWrapper")
    self._scoreText = self:GetChildComponent("Container_Content/Container_Info/Container_Content/Text_Score","TextMeshWrapper")
    self._countDownText = self:GetChildComponent("Container_Content/Container_Info/Container_Content/Text_CountDown","TextMeshWrapper")
    self._questionText = self:GetChildComponent("Container_Answer/Text_Question","TextMeshWrapper")
    -- self._titleText = self:GetChildComponent("Container_Info/Text_Title","TextMeshWrapper")
    -- self._descText = self:GetChildComponent("Container_Info/Text_Desc","TextMeshWrapper")

    --答题答案显示
    self._correcrtImageGo = self:FindChildGO("Container_Answer_Result/Image_Correct")
    self._correcrtImageScale = self._correcrtImageGo.gameObject:AddComponent(typeof(XGameObjectTweenScale))
    self._errorImageGo = self:FindChildGO("Container_Answer_Result/Image_Error")
    self._errorImageScale = self._errorImageGo.gameObject:AddComponent(typeof(XGameObjectTweenScale))

    --排行榜奖励
    self._rankRewardContainer = self:FindChildGO("Container_Rank")

    --倒计时
    self._countDownContainer = self:FindChildGO("Container_Count_Down")
    self._countDownTitleText = self:GetChildComponent("Container_Count_Down/Text_Title","TextMeshWrapper")
    self._countDownTimeText = self:GetChildComponent("Container_Count_Down/Text_Time","TextMeshWrapper")
    self._countDownContainer:SetActive(false)

    self._commonViews = {}
    self._selectViews = {}
    self._common1ImageGo = self:FindChildGO("Container_Answer/Button_Answer_1/Image_Common")
    self._select1ImageGo = self:FindChildGO("Container_Answer/Button_Answer_1/Image_Select")
    self._common2ImageGo = self:FindChildGO("Container_Answer/Button_Answer_2/Image_Common")
    self._select2ImageGo = self:FindChildGO("Container_Answer/Button_Answer_2/Image_Select")
    table.insert(self._commonViews, self._common1ImageGo)
    table.insert(self._commonViews, self._common2ImageGo)
    table.insert(self._selectViews, self._select1ImageGo)
    table.insert(self._selectViews, self._select2ImageGo)
    self._common1ImageGo:SetActive(false)
    self._select2ImageGo:SetActive(false)

    self._answer1Button = self:FindChildGO("Container_Answer/Button_Answer_1")
    self._answer1ButtonFlowLight = self:AddChildComponent("Container_Answer/Button_Answer_1/Image_Select", XImageFlowLight)
    self._csBH:AddClick(self._answer1Button, function() self:OnAnswerClick(1) end)

    self._answer2Button = self:FindChildGO("Container_Answer/Button_Answer_2")
    self._answer2ButtonFlowLight = self:AddChildComponent("Container_Answer/Button_Answer_2/Image_Common", XImageFlowLight)
    self._csBH:AddClick(self._answer2Button, function() self:OnAnswerClick(2) end)

    self._rankRewardButton = self:FindChildGO("Container_Content/Container_Info/Container_Rank/Text")
    self._csBH:AddClick(self._rankRewardButton, function() self:OnRankRewardClick() end)

    self._rankRewardCloseButton = self:FindChildGO("Container_Rank/Container_PopupBig/Button_Close")
    self._csBH:AddClick(self._rankRewardCloseButton, function() self:OnRankRewardClose() end)

    self._rankRewardShadeButton = self:FindChildGO("Container_Rank/Button_Shade")
    self._csBH:AddClick(self._rankRewardShadeButton, function() self:OnRankRewardClose() end)

    self:InitRankList()
    self:InitRankRewardList()
end

function AnswerPanel:InitRankRewardList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Rank/ScrollViewList")
	self._rankRewardList = list
	self._rankRewardListlView = scrollView 
    self._rankRewardListlView:SetHorizontalMove(false)
    self._rankRewardListlView:SetVerticalMove(true)
    self._rankRewardList:SetItemType(AnswerRankRewardListItem)
    self._rankRewardList:SetPadding(0, 0, 0, 0)
    self._rankRewardList:SetGap(0, 0)
    self._rankRewardList:SetDirection(UIList.DirectionLeftToRight, 1, 100)

    self._rankRewardContainer:SetActive(false)
end

function AnswerPanel:InitRankList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/Container_Info/Container_Title/ScrollViewList")
	self._rankList = list
	self._rankListlView = scrollView 
    self._rankListlView:SetHorizontalMove(false)
    self._rankListlView:SetVerticalMove(true)
    self._rankList:SetItemType(AnswerRankListItem)
    self._rankList:SetPadding(0, 0, 0, 0)
    self._rankList:SetGap(4, 0)
    self._rankList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function AnswerPanel:OnShow(data)
    self:SetData(data)
    self:AddEventListeners()
    PlayerActionManager:StopAllAction()
end

function AnswerPanel:SetData(data)
    local type = data.type
    if type == AnswerConfig.SHOW_EXP_SCORE then
        --显示经验和积分
        self:ShowExpAndScore(data)
    elseif type == AnswerConfig.SHOW_BEGIN_COUNT_DOWN then
        --显示距离答题开始的倒计时
        self._questionStart = false
        self:ShowBeginCountDown(data)
    -- elseif type == AnswerConfig.SHOW_ANSWER_COUNT_DOWN then
    --     --显示答题倒计时
    --     self:ShowAnswerCountDownInfo(data)
    elseif type == AnswerConfig.SHOW_END_COUNT_DOWN then
        --显示答题结束倒计时
        self._questionStart = false
        self:ShowEndCountDown(data)
    elseif type == AnswerConfig.SHOW_ANSWER_RESULT then
        --显示答题是否正确
        self:ShowAnswerResult(data)
    elseif type == AnswerConfig.SHOW_ANSWER_INFO then
        --显示答题相关信息（题目）
        self._questionStart = true
        self:ShowAnswerInfo(data)
    elseif type == AnswerConfig.SHOW_ANSWER_RANK then
        --显示答题排行榜
        self:ShowRank(data)
    elseif type == AnswerConfig.INSTANCE_TICK_TIMER then
        --副本时间计时器
        self:UpdateTimeInfo()
    else

    end
    self._questionContainer:SetActive(self._questionStart)
end

function AnswerPanel:OnClose()
    self._beginEndTime = nil
    self._endEndTime = nil
    self._questionEndTime = nil
    self._correcrtImageGo:SetActive(false)
    self._errorImageGo:SetActive(false)
    self._rankList:SetDataList({})
    self._rankRewardContainer:SetActive(false)
    self._countDownContainer:SetActive(false)
    self._endTime = nil
    self:ClearResultTimer()
    self:RemoveEventListeners()
end

function AnswerPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ANSWER_CHANGE, self._answerChange)
end

function AnswerPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ANSWER_CHANGE, self._answerChange)
end

function AnswerPanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function AnswerPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    -- self._containerInfo.transform.localPosition = Vector3.New(-280, 0, 0)
    self._tweenPosition:SetFromToPos(self._containerInfo.transform.localPosition, self._initPosition, 0.4, 1, nil)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(0, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function AnswerPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetFromToPos(self._initPosition, Vector3.New(self._initPosition.x - 280, self._initPosition.y, self._initPosition.z), 0.4, 1, nil)
    -- self._containerInfo.transform.localPosition = Vector3.New(0, 0, 0)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(-280, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

function AnswerPanel:ShowExpAndScore(data)
    self._expText.text = LanguageDataHelper.CreateContent(75815,{
        ["0"] = StringStyleUtil.GetLongNumberString(data.exp)
    }) 
    
    self._scoreText.text = LanguageDataHelper.CreateContent(75814,{
        ["0"] = data.score
    })
end

function AnswerPanel:ShowAnswerInfo(data)
    self._beginEndTime = nil
    self._endEndTime = nil

    self._questionText.text = LanguageDataHelper.CreateContent(75817,{
        ["0"] = data.num,
        ["1"] = public_config.MAX_QUESTION_COUNT,
        ["2"] = QuestionDataHelper:GetQuestionDesc(data.titleId)
    }) 
    self._answer1Text.text = QuestionDataHelper:GetAnswer1Desc(data.titleId)
    self._answer2Text.text = QuestionDataHelper:GetAnswer2Desc(data.titleId)
    --每道题目的答题结束时间
    self._questionEndTime = data.questionBeginTime + public_config.QUESTION_CHOOSE_ANSWER_TIME
    --活动的结束时间
    self._endTime = data.startTime + public_config.QUESTION_CHOOSE_ANSWER_TIME * public_config.MAX_QUESTION_COUNT + public_config.QUESTION_PREPARE_TIME

    
    self:AnswerChange(AnswerManager:GetAnswer())
    -- for i=1,2 do
    --     self._commonViews[i]:SetActive(true)
    --     self._selectViews[i]:SetActive(false)
    -- end
end

 --显示答题倒计时
 function AnswerPanel:ShowAnswerCountDownInfo(data)
    if self._questionEndTime == nil then return end
 end

 function AnswerPanel:OnAnswerClick(num)
    --寻路跳转
    local data = GlobalParamsHelper.GetParamValue(876)
    local answer = AnswerManager:GetAnswer()
    PlayerActionManager:StopFindPosition()

    if answer ~= num then        
        local npcId = data[num][1]
        TaskCommonManager:GoToNpc(npcId, false, true)
    end
 end

 function AnswerPanel:ShowAnswerResult(data)
    local result = data.result
    if result then        
        self._correcrtImageGo.transform.localScale = Vector3.zero
        self._correcrtImageGo:SetActive(true)
        self._correcrtImageScale:SetFromToScale(0, 1, result_scale_time, 1, nil)
        self._resultTimer = TimerHeap:AddTimer((result_scale_time + result_time) * 1000, 0, 0, function()
            self._correcrtImageGo:SetActive(false)
            self:ClearResultTimer()
        end)
    else
        self._errorImageGo.transform.localScale = Vector3.zero
        self._errorImageGo:SetActive(true)
        self._errorImageScale:SetFromToScale(0, 1, result_scale_time, 1, nil)
        self._resultTimer = TimerHeap:AddTimer((result_scale_time + result_time) * 1000, 0, 0, function()
            self._errorImageGo:SetActive(false)
            self:ClearResultTimer()
        end)
    end
 end

 function AnswerPanel:ShowRank(data)
    self._rankList:SetDataList(data.rankData)
 end

 function AnswerPanel:ClearResultTimer()
    if self._resultTimer ~= nil then
        TimerHeap:DelTimer(self._resultTimer)
        self._resultTimer = nil
    end
 end

 function AnswerPanel:OnRankRewardClick()
    self._rankRewardContainer:SetActive(true)
    self._rankRewardList:SetDataList(QuestionRankRewardDataHelper:GetRewardListByMyLevel())
 end

 function AnswerPanel:OnRankRewardClose()
    self._rankRewardContainer:SetActive(false)
 end

 function AnswerPanel:ShowBeginCountDown(data)
    self._endTime = data.startTime + public_config.QUESTION_CHOOSE_ANSWER_TIME * public_config.MAX_QUESTION_COUNT + public_config.QUESTION_PREPARE_TIME * 2
    self._countDownContainer:SetActive(true)
    self._countDownTitleText.text = LanguageDataHelper.CreateContent(75822)
    self._countDownTimeText.text = ""
    self._beginEndTime = data.startTime + public_config.QUESTION_PREPARE_TIME
 end
 
 function AnswerPanel:ShowEndCountDown(data)
    self._endTime = data.startTime  + public_config.QUESTION_PREPARE_TIME
    self._countDownContainer:SetActive(true)
    self._countDownTitleText.text = LanguageDataHelper.CreateContent(75810)
    self._countDownTimeText.text = ""
    self._endEndTime = data.startTime + public_config.QUESTION_PREPARE_TIME
 end 

 function AnswerPanel:UpdateTimeInfo()
    --显示活动结束时间
    if self._endTime ~= nil then
        local countDownTime = self._endTime - DateTimeUtil.GetServerTime()
        if countDownTime < 0 then
            countDownTime = 0   
        end
        self._countDownText.text = LanguageDataHelper.CreateContent(75813, {
            ["0"] = DateTimeUtil:ParseTime(countDownTime)
        })
    end

    --显示答题倒计时
    if self._questionEndTime ~= nil then
        local time = self._questionEndTime - DateTimeUtil.GetServerTime()
        if time > public_config.QUESTION_CHOOSE_ANSWER_TIME then
            time = public_config.QUESTION_CHOOSE_ANSWER_TIME 
        elseif time < 0 then
            time = 0   
        end
        self._questionSecondsText.text = LanguageDataHelper.CreateContent(75820,{
            ["0"] = time
        }) 
    end

    --显示活动开启倒计时
    if self._beginEndTime ~= nil then
        local time = self._beginEndTime - DateTimeUtil.GetServerTime()
        if time > public_config.QUESTION_PREPARE_TIME then
            time = public_config.QUESTION_PREPARE_TIME 
        elseif time < 0 then
            time = 0   
        end
        self._countDownTimeText.text = time
        self._countDownContainer:SetActive(true)
    else
        self._countDownContainer:SetActive(false)
    end

    --显示活动结束倒计时
    if self._endEndTime ~= nil then
        local time = self._endEndTime - DateTimeUtil.GetServerTime()
        if time > public_config.QUESTION_PREPARE_TIME then
            time = public_config.QUESTION_PREPARE_TIME 
        elseif time < 0 then
            time = 0   
        end
        self._countDownTimeText.text = time
        self._countDownContainer:SetActive(true)
    else
        self._countDownContainer:SetActive(self._beginEndTime ~= nil)
    end
end

function AnswerPanel:AnswerChange(num)
    self:SetButtonAnswer(num)
    AnswerManager:Answer(num)
end

function AnswerPanel:SetButtonAnswer(num)
    -- for i=1,2 do
    --     self._commonViews[i]:SetActive(not (num == i))
    --     self._selectViews[i]:SetActive(num == i)
    -- end
    self._answer1ButtonFlowLight.enabled = num == 1
    self._answer2ButtonFlowLight.enabled = num == 2
end