-- TowerBuildListItem.lua
local TowerBuildListItem = Class.TowerBuildListItem(ClassTypes.UIListItem)

TowerBuildListItem.interface = GameConfig.ComponentsConfig.Component
TowerBuildListItem.classPath = "Modules.ModuleInstance.ChildComponent.TowerBuildListItem"

local TowerDefenseDataHelper = GameDataHelper.TowerDefenseDataHelper
local TowerInstanceManager = PlayerManager.TowerInstanceManager

function TowerBuildListItem:Awake()
    self._base.Awake(self)
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._numText = self:GetChildComponent("Text_Num","TextMeshWrapper")
end

function TowerBuildListItem:OnDestroy()
    self._itemContainer = nil
    self._base.OnDestroy(self)
end

--override
function TowerBuildListItem:OnRefreshData(id)
    GameWorld.AddIcon(self._itemContainer, TowerDefenseDataHelper:GetIcon(id), 0, true)
    self:UpdateText()
end

function TowerBuildListItem:UpdateText()
    self._numText.text = TowerInstanceManager:GetShowTextById(self:GetIndex() + 1)
end