-- BalanceMoneyRewardItem.lua
local BalanceMoneyRewardItem = Class.BalanceMoneyRewardItem(ClassTypes.UIListItem)

BalanceMoneyRewardItem.interface = GameConfig.ComponentsConfig.Component
BalanceMoneyRewardItem.classPath = "Modules.ModuleInstance.ChildComponent.BalanceMoneyRewardItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

function BalanceMoneyRewardItem:Awake()
    self._base.Awake(self)
    self._itemContainer = self:FindChildGO("Container_Value/Container_Icon")
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper')
    self._numText = self:GetChildComponent("Container_Value/Text_Num",'TextMeshWrapper')

    self._rect = self:FindChildGO("Container_Value"):GetComponent("RectTransform")
    self._initPosition = self._rect.transform.localPosition
end

function BalanceMoneyRewardItem:OnDestroy()
    self._itemContainer = nil
    self._nameText = nil
    self._numText = nil
    self._base.OnDestroy(self)
end

--override
function BalanceMoneyRewardItem:OnRefreshData(data)
    local itemId = data.itemId
    local num = data.num

    self._nameText.text = LanguageDataHelper.CreateContent(58646, {
        ["0"] = ItemDataHelper.GetItemName(itemId)
    })
    GameWorld.AddIcon(self._itemContainer, ItemDataHelper.GetIcon(itemId))
    self._numText.text = StringStyleUtil.GetLongNumberString(num)
    local length = self._nameText.preferredWidth
    self._rect.transform.localPosition = Vector3.New(length, self._rect.transform.localPosition.y, 0)
end