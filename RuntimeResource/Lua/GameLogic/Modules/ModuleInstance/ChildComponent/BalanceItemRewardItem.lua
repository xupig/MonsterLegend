-- BalanceItemRewardItem.lua
local BalanceItemRewardItem = Class.BalanceItemRewardItem(ClassTypes.UIListItem)

BalanceItemRewardItem.interface = GameConfig.ComponentsConfig.Component
BalanceItemRewardItem.classPath = "Modules.ModuleInstance.ChildComponent.BalanceItemRewardItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIParticle = ClassTypes.UIParticle

function BalanceItemRewardItem:Awake()
    self._base.Awake(self)
    self._itemManager = ItemManager()
    self._itemContainer = self:FindChildGO("Container_Content/Container_Icon")
    self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
    self._icon:ActivateTipsById()
    self._nameText = self:GetChildComponent("Container_Content/Text_Name", 'TextMeshWrapper')
    self._contentContainer = self:FindChildGO("Container_Content")

    self._fx_Container = self:FindChildGO("Container_Content/Container_Fx")
    self._fx_ui_30037 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30037")
    self._fx_ui_30037:Stop()
end

function BalanceItemRewardItem:OnDestroy()
    self._itemManage = nil 
    self._icon = nil
    self._itemContainer = nil
    self._base.OnDestroy(self)
end

--override
function BalanceItemRewardItem:OnRefreshData(data)
end

function BalanceItemRewardItem:DoShow(data)
    self._contentContainer:SetActive(true)
    local itemId = data.itemId
    local num = data.num
    self._icon:SetItem(itemId, num)
    self._nameText.text = ItemDataHelper.GetItemNameWithColor(itemId)
    self._fx_ui_30037:Play(true, true)
end

function BalanceItemRewardItem:Hide()
    self._contentContainer:SetActive(false)
end