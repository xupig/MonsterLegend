-- AnswerRankListItem.lua
local AnswerRankListItem = Class.AnswerRankListItem(ClassTypes.UIListItem)

AnswerRankListItem.interface = GameConfig.ComponentsConfig.Component
AnswerRankListItem.classPath = "Modules.ModuleInstance.ChildComponent.AnswerRankListItem"

function AnswerRankListItem:Awake()
    self._base.Awake(self)
    self._nameText = self:GetChildComponent("Text_Name","TextMeshWrapper")
    self._scoreText = self:GetChildComponent("Text_Score","TextMeshWrapper")
end

function AnswerRankListItem:OnDestroy()
    self._base.OnDestroy(self)
end

--override
function AnswerRankListItem:OnRefreshData(data)
    self._nameText.text = data[1]
    self._scoreText.text = data[2] or 0
end