-- TowerListItem.lua
local TowerListItem = Class.TowerListItem(ClassTypes.UIListItem)

TowerListItem.interface = GameConfig.ComponentsConfig.Component
TowerListItem.classPath = "Modules.ModuleInstance.ChildComponent.TowerListItem"

local TowerDefenseDataHelper = GameDataHelper.TowerDefenseDataHelper
local TowerInstanceManager = PlayerManager.TowerInstanceManager

function TowerListItem:Awake()
    self._base.Awake(self)
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._numText = self:GetChildComponent("Text_Num","TextMeshWrapper")
end

function TowerListItem:OnDestroy()
    self._itemContainer = nil
    self._base.OnDestroy(self)
end

--override
function TowerListItem:OnRefreshData(id)
    GameWorld.AddIcon(self._itemContainer, TowerDefenseDataHelper:GetIcon(id))
    self:UpdateText()
end

function TowerListItem:UpdateText()
    self._numText.text = TowerInstanceManager:GetShowTextById(self:GetIndex() + 1)
end