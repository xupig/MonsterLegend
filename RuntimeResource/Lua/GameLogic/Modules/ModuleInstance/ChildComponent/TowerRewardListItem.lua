-- TowerRewardListItem.lua
local TowerRewardListItem = Class.TowerRewardListItem(ClassTypes.UIListItem)

TowerRewardListItem.interface = GameConfig.ComponentsConfig.Component
TowerRewardListItem.classPath = "Modules.ModuleInstance.ChildComponent.TowerRewardListItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemConfig = GameConfig.ItemConfig
local StringStyleUtil = GameUtil.StringStyleUtil

function TowerRewardListItem:Awake()
    self._base.Awake(self)
    self._nameText = self:GetChildComponent("Text_Name","TextMeshWrapper")
end

function TowerRewardListItem:OnDestroy()
    self._base.OnDestroy(self)
end

--override
function TowerRewardListItem:OnRefreshData(data)
    local itemId = data.itemId
    local num = data.num
    local quality = ItemDataHelper.GetQuality(itemId)
    local colorId = ItemConfig.QualityTextMap[quality]
    local str = ItemDataHelper.GetItemName(itemId) .. ":" .. num
    
    self._nameText.text = StringStyleUtil.GetColorStringWithColorId(str,colorId)
    
end
