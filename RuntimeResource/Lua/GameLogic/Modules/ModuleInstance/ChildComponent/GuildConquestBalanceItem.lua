
-- GuildConquestBalanceItem.lua
local GuildConquestBalanceItem = Class.GuildConquestBalanceItem(ClassTypes.UIListItem)

GuildConquestBalanceItem.interface = GameConfig.ComponentsConfig.Component
GuildConquestBalanceItem.classPath = "Modules.ModuleInstance.ChildComponent.GuildConquestBalanceItem"


local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local NobilityDataHelper = GameDataHelper.NobilityDataHelper

function GuildConquestBalanceItem:Awake()
    self:InitView()
end

function GuildConquestBalanceItem:OnDestroy()

end

function GuildConquestBalanceItem:InitView()
    self._enemyImage = self:FindChildGO("Image_Enemy")
    self._friendImage = self:FindChildGO("Image_Friend")
    self._nobilityIcon = self:FindChildGO("Container_Icon")
    self._rankText = self:GetChildComponent("Text_Rank", 'TextMeshWrapper')
    self._nameText = self:GetChildComponent("Text_Name", 'TextMeshWrapper')
    self._killText = self:GetChildComponent("Text_Kill", 'TextMeshWrapper')
    self._deadText = self:GetChildComponent("Text_Dead", 'TextMeshWrapper')
    self._resourceText = self:GetChildComponent("Text_Resource", 'TextMeshWrapper')
    self._rewardText = self:GetChildComponent("Text_Reward", 'TextMeshWrapper')
end

--override {1-player_name, 2-玩家爵位，3-击杀，4-死亡，5-夺点, 6-战力，7-dbid, 8-是否获胜，9-是否友军，10-排名，11-奖励 12-额外奖励}
function GuildConquestBalanceItem:OnRefreshData(data)
    --爵位
    local nobility = data[2] or 0
    if nobility ~= 0 then
        local icon = NobilityDataHelper:GetNobilityShow(nobility)
        self._nobilityIcon:SetActive(true)
        GameWorld.AddIcon(self._nobilityIcon, icon)
    else
        self._nobilityIcon:SetActive(false)
    end
    if data[9] then
        self._enemyImage:SetActive(false)
        self._friendImage:SetActive(true)
    else
        self._enemyImage:SetActive(true)
        self._friendImage:SetActive(false)
    end
    self._rankText.text = data[10]
    self._nameText.text = data[1]
    self._killText.text = data[3]
    self._deadText.text = data[4]
    self._resourceText.text = data[5]
    local reward = data[11]
    local extraReward = data[12]
    self._rewardText.text = string.format("%s(%s)", reward, extraReward)
end