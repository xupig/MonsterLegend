-- AnswerRankRewardListItem.lua
local AnswerRankRewardListItem = Class.AnswerRankRewardListItem(ClassTypes.UIListItem)

AnswerRankRewardListItem.interface = GameConfig.ComponentsConfig.Component
AnswerRankRewardListItem.classPath = "Modules.ModuleInstance.ChildComponent.AnswerRankRewardListItem"

local QuestionRankRewardDataHelper = GameDataHelper.QuestionRankRewardDataHelper
local UIList = ClassTypes.UIList

require "Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem

function AnswerRankRewardListItem:Awake()
    self._base.Awake(self)
    self._descText = self:GetChildComponent("Text_Desc","TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Container_Icon")
    self:InitRewardList()
end

function AnswerRankRewardListItem:InitRewardList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listlView = scrollView 
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    self._list:SetItemType(RewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function AnswerRankRewardListItem:OnDestroy()
    self._base.OnDestroy(self)
    self._descText = nil
    self._iconContainer = nil
end

--override
function AnswerRankRewardListItem:OnRefreshData(id)
    self._descText.text = LanguageDataHelper.CreateContent(QuestionRankRewardDataHelper:GetDesc(id))
    GameWorld.AddIcon(self._iconContainer, QuestionRankRewardDataHelper:GetIcon(id))
    self._list:SetDataList(QuestionRankRewardDataHelper:GetRewardShow(id))
end