-- ExpEfficiencyItem.lua
--经验副本效率道具Item
local ExpEfficiencyItem = Class.ExpEfficiencyItem(ClassTypes.BaseComponent)

ExpEfficiencyItem.interface = GameConfig.ComponentsConfig.Component
ExpEfficiencyItem.classPath = "Modules.ModuleInstance.ChildComponent.ExpEfficiencyItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local BagManager = PlayerManager.BagManager
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ShopDataHelper = GameDataHelper.ShopDataHelper
local ShopManager = PlayerManager.ShopManager
local GUIManager = GameManager.GUIManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper

function ExpEfficiencyItem:Awake()
	self._base.Awake(self)

	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._icon:ActivateTipsById()

	self._btnUse = self:FindChildGO("Button_Use")
	self._csBH:AddClick(self._btnUse, function() self:OnUseClick() end )

	self._txtRate = self:GetChildComponent("Text_Rate","TextMeshWrapper")
end

function ExpEfficiencyItem:UpdateData(itemId,rate)
	self._itemId = itemId
	self._icon:SetItem(itemId)
	self._txtRate.text = LanguageDataHelper.CreateContent(36)..rate.."%"
end

function ExpEfficiencyItem:UpdateCount(count)
	if count > 0 and self._btnBuy then
		self._btnBuy:SetActive(false)
		self._txtPrice.gameObject:SetActive(false)
		self.moneyIcon:SetActive(false)
	end
	self._icon:SetCountString(tostring(count))
	if count > 0 then
		self._btnUse:SetActive(true)
	end
end

function ExpEfficiencyItem:OnUseClick()
	local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._itemId)
	local effectId = ItemDataHelper.GetItemEffect(self._itemId)
    if pos > 0 then
    	local addInfo = GameWorld.Player().add_rate_info
    	if addInfo then
    		for i, addItem in ipairs(addInfo) do
    			local addInfoData = GlobalParamsHelper.GetParamValue(833)
	            for k, addItemInfo in pairs(addInfoData) do
	                if k == addItem[1] then
	                	local args = LanguageDataHelper.GetArgsTable()
	                	args["0"] = LanguageDataHelper.CreateContent(addItemInfo[2])
	                	args["1"] = ItemDataHelper.GetItemNameWithColor(self._itemId)
	                	local cnId
                        if k ==  effectId[2] then
                            cnId = 58775
                        else
                            cnId = 58770
                        end
                        local str = LanguageDataHelper.CreateContentWithArgs(cnId,args)
	                	GUIManager.ShowMessageBox(2,str,
							function() BagManager:RequsetUse(pos,1,0) end
						)
						return
	                end
	            end
    		end
    	end
    	
        BagManager:RequsetUse(pos,1,0)
    end
end

function ExpEfficiencyItem:ShowBuy()
	if self._btnBuy == nil then
		self._txtPrice = self:GetChildComponent("Text_Price","TextMeshWrapper")
		self.moneyIcon = self:FindChildGO("Container_Money")
		self._btnBuy = self:FindChildGO("Button_Buy")
		self._csBH:AddClick(self._btnBuy, function() self:OnBuy() end )
	end
	self._txtPrice.gameObject:SetActive(true)
	self:ShowPrice()
	self.moneyIcon:SetActive(true)
	self._btnBuy:SetActive(true)
	self._btnUse:SetActive(false)
end

function ExpEfficiencyItem:ShowPrice()
	local shopItemList = ShopDataHelper:GetShopDataByItemId(self._itemId)
	local targetShopItem
	local data1
	local data2
	local price1
	local price2
	local money1
	local money2

	for i=1,#shopItemList do
		local data = shopItemList[i]
		local type, price = ShopDataHelper:GetItemTypeAndOriPrice(data.__id)
		if type == public_config.MONEY_TYPE_COUPONS_BIND then
			data1 = data
			price1 = price
			money1 = type
		end

		if type == public_config.MONEY_TYPE_COUPONS then
			data2 = data
			price2 = price
			money2 = type
		end
	end

	self.moneyType = money1
	self._txtPrice.text = price1
	if GameWorld.Player().money_coupons_bind < price1 and GameWorld.Player().money_coupons >= price2 then
		self._txtPrice.text = price2
		self.moneyType = money2
	end
    GameWorld.AddIcon(self.moneyIcon,ItemDataHelper.GetIcon(self.moneyType))
end

function ExpEfficiencyItem:OnBuy()
	local shopItemList = ShopDataHelper:GetShopDataByItemId(self._itemId)
	local targetShopItem
	local data1
	local data2
	local price1
	local price2
	for i=1,#shopItemList do
		local data = shopItemList[i]
		local type, price = ShopDataHelper:GetItemTypeAndOriPrice(data.__id)
		if type == public_config.MONEY_TYPE_COUPONS_BIND then
			data1 = data
			price1 = price
		end

		if type == public_config.MONEY_TYPE_COUPONS then
			data2 = data
			price2 = price
		end
	end

	targetShopItem = data1
	if GameWorld.Player().money_coupons_bind < price1 and GameWorld.Player().money_coupons >= price2 then
		targetShopItem = data2
	end

	ShopManager:BuyShopItem(targetShopItem.__id,1,0)
end