-- InstanceProgressBossItem.lua
local InstanceProgressBossItem = Class.InstanceProgressBossItem(ClassTypes.UIListItem)

InstanceProgressBossItem.interface = GameConfig.ComponentsConfig.PointableComponent
InstanceProgressBossItem.classPath = "Modules.ModuleInstance.ChildComponent.InstanceProgressBossItem"
local GameSceneManager = GameManager.GameSceneManager

function InstanceProgressBossItem:Awake()
	self._base.Awake(self)
	self._containerHead = self:FindChildGO("Container_Head")
	self._imgKilled = self:FindChildGO("Image_Killed")
end


function InstanceProgressBossItem:OnRefreshData(data)
	local bossId = data
end

function InstanceProgressBossItem:SetKill(isKilled)
	self._imgKilled:SetActive(isKilled)
end