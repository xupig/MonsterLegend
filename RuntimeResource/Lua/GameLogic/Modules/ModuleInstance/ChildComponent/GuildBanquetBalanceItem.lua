
-- GuildBanquetBalanceItem.lua
local GuildBanquetBalanceItem = Class.GuildBanquetBalanceItem(ClassTypes.UIListItem)

GuildBanquetBalanceItem.interface = GameConfig.ComponentsConfig.Component
GuildBanquetBalanceItem.classPath = "Modules.ModuleInstance.ChildComponent.GuildBanquetBalanceItem"


local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local NobilityDataHelper = GameDataHelper.NobilityDataHelper
local public_config = GameWorld.public_config
local GuildManager = PlayerManager.GuildManager

function GuildBanquetBalanceItem:Awake()
    self:InitView()
end

function GuildBanquetBalanceItem:OnDestroy()

end

function GuildBanquetBalanceItem:InitView()
    self._enemyImage = self:FindChildGO("Image_Enemy")
    self._friendImage = self:FindChildGO("Image_Friend")
    self._rankText = self:GetChildComponent("Text_Rank", 'TextMeshWrapper')
    self._nameText = self:GetChildComponent("Text_Name", 'TextMeshWrapper')
    self._resourceText = self:GetChildComponent("Text_Resource", 'TextMeshWrapper')
end

--override {RANK_KEY_GUILD_NAME, RANK_KEY_BONFIRE_SCORE, RANK_KEY_GUILD_DBID,RANK_KEY_RANK_VALUE}
function GuildBanquetBalanceItem:OnRefreshData(data)
    if data[public_config.RANK_KEY_GUILD_DBID] == GuildManager:GetGuildId() then
        self._enemyImage:SetActive(false)
        self._friendImage:SetActive(true)
    else
        self._enemyImage:SetActive(true)
        self._friendImage:SetActive(false)
    end
    self._rankText.text = data[public_config.RANK_KEY_RANK_VALUE]
    self._nameText.text =  data[public_config.RANK_KEY_GUILD_NAME]
    self._resourceText.text =data[public_config.RANK_KEY_BONFIRE_SCORE]
end