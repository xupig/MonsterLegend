--GuildMonsterInstanceInfoPanel.lua
local GuildMonsterInstanceInfoPanel = Class.GuildMonsterInstanceInfoPanel(ClassTypes.BasePanel)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local BaseUtil = GameUtil.BaseUtil

function GuildMonsterInstanceInfoPanel:Awake()
    self.disableCameraCulling = true
	self:InitCallback()
	self:InitComps()
end

function GuildMonsterInstanceInfoPanel:InitCallback()
end

function GuildMonsterInstanceInfoPanel:InitComps()
    self._isOpen = true
	self._btnExpand = self:FindChildGO("Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
    
	self._containerInfo = self:FindChildGO("Container_Info")
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
    self._initPosition = self._containerInfo.transform.localPosition
	self._imgLeft = self:FindChildGO("Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Button_Expand/Image_Right")

    self._titleText = self:GetChildComponent("Container_Info/Text_Title","TextMeshWrapper")
    -- self._descText = self:GetChildComponent("Container_Info/Text_Desc","TextMeshWrapper")
end

function GuildMonsterInstanceInfoPanel:OnShow(data)
    GameWorld.Player():StartAutoFight()
    self:SetText(data)
end

function GuildMonsterInstanceInfoPanel:SetData(data)
    self:SetText(data)
end

function GuildMonsterInstanceInfoPanel:OnClose()
end

function GuildMonsterInstanceInfoPanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function GuildMonsterInstanceInfoPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetFromToPos(self._containerInfo.transform.localPosition, self._initPosition, 0.4, 1, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function GuildMonsterInstanceInfoPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetFromToPos(self._initPosition, Vector3.New(self._initPosition.x - 280, self._initPosition.y, self._initPosition.z), 0.4, 1, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

function GuildMonsterInstanceInfoPanel:SetText(data)
    local totalNum = 1
    local nowNum = (data and data.num or 0)
    local showStr = BaseUtil.GetColorStringByCount(nowNum, totalNum)
    self._titleText.text = LanguageDataHelper.CreateContent(53196,{["0"] = showStr})
end