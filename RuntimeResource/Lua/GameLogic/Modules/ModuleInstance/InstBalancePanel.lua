-- InstBalancePanel.lua
local InstBalancePanel = Class.InstBalancePanel(ClassTypes.BasePanel)

require "Modules.ModuleInstance.ChildComponent.BalanceMoneyRewardItem"
local BalanceMoneyRewardItem = ClassTypes.BalanceMoneyRewardItem

require "Modules.ModuleInstance.ChildComponent.BalanceItemRewardItem"
local BalanceItemRewardItem = ClassTypes.BalanceItemRewardItem

require "Modules.ModuleInstance.ChildComponent.GuildConquestBalanceItem"
local GuildConquestBalanceItem = ClassTypes.GuildConquestBalanceItem

require "Modules.ModuleInstance.ChildComponent.GuildBanquetBalanceItem"
local GuildBanquetBalanceItem = ClassTypes.GuildBanquetBalanceItem

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

local public_config = GameWorld.public_config
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ButtonWrapper = UIExtension.ButtonWrapper
local TimerHeap = GameWorld.TimerHeap
local InstanceManager = PlayerManager.InstanceManager
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PlayerActionManager = GameManager.PlayerActionManager
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local SceneConfig = GameConfig.SceneConfig
local AionTowerManager = PlayerManager.AionTowerManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local NobilityDataHelper = GameDataHelper.NobilityDataHelper
local GuildManager = PlayerManager.GuildManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GuildConquestManager = PlayerManager.GuildConquestManager
local action_config = require("ServerConfig/action_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local BagData = PlayerManager.PlayerDataManager.bagData
local TaskCommonManager = PlayerManager.TaskCommonManager

local XGameObjectTweenScale = GameMain.XGameObjectTweenScale
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local XImageFlowLight = GameMain.XImageFlowLight

local perShowRewardNum = 1
local perTime = 0

local function SortByOrder(a, b)
    if a[3] == b[3] then
        if a[4] == b[4] then
            if a[5] == b[5] then
                return a[6] > b[6]
            else
                return a[5] > b[5]
            end
        else
            return a[4] < b[4]
        end
    else
        return a[3] > b[3]
    end
end

function InstBalancePanel:Awake()
    self:Init()
end

function InstBalancePanel:OnShow(data)
    self:SetData(data)
end

function InstBalancePanel:SetData(data)
    self._data = data
    self:StartTween(data)
    do return end

    self._commonContainer:SetActive(true)
    self._countdownTime = 10
    self._balanceData = data.instanceBalanceData
    self._callback = data.callBack
    self._sweep = data.sweep
    -- local balanceData = PlayerManager.PlayerDataManager.instanceBalanceData
    local result = balanceData:GetResult()
    self._result = result
    self._successContainer:SetActive(result)
    self._failContainer:SetActive(not result)
    self._aiontowerCountDownSuccessText.gameObject:SetActive(result)
    self._aiontowerCountDownFailedText.gameObject:SetActive(not result)    
    
    local sceneType = balanceData:GetSceneType()
    self._sceneType = sceneType
    local start = balanceData:GetStart()
    for i = 1, 3 do
        -- if sceneType ==
        if i <= start then
            self._starts[i]:SetGray(1)
        else
            self._starts[i]:SetGray(0)
        end
    end
    
    if sceneType == SceneConfig.SCENE_TYPE_PET_INSTANCE or sceneType == SceneConfig.SCENE_TYPE_GOLD or sceneType == SceneConfig.SCENE_TYPE_EQUIPMENT then
        self._tipsText.gameObject:SetActive(true)
    else
        self._tipsText.gameObject:SetActive(false)
    end
    
    if sceneType == SceneConfig.SCENE_TYPE_AION_TOWER then --永恒之塔
        GameWorld.Player():StopAutoFight()
        self._commonButtom:SetActive(false)
        self._aiontowerButtom:SetActive(true)
        self._type = 2
        self._btnAiontowerNext:SetActive(result)
        if AionTowerManager:GetNowPos() == 2 then
            self._btnAiontowerNext:SetActive(false)
            self._aiontowerCountDownSuccessText.gameObject:SetActive(false)
            self._aiontowerCountDownFailedText.gameObject:SetActive(true)
            self._isExit = true
            self._callback = function()
                EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_aion_tower_level1_quit_ui_button")
                self:ExitInstance()
            end
        end
    else
        if self._sweep then
            self._countdownTime = 0
            self._sweepContainer:SetActive(true)
            self._commonButtom:SetActive(false)
            self._aiontowerButtom:SetActive(false)
            self._btnAgainSweep:SetActive(InstanceManager:IsCanAgain())
            local sweep = InstanceManager:GetSweepData()
            GameWorld.AddIcon(self._icon, ItemDataHelper.GetIcon(sweep:GetItemId()))
            local bagCount = BagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(sweep:GetItemId())
            self._sweepCostNum.text = bagCount .. "/" .. sweep:GetItemNum()
        else
            self._commonButtom:SetActive(true)
            self._aiontowerButtom:SetActive(false)
            self._sweepContainer:SetActive(false)
            self._type = 1
        end
    
    end
    
    if sceneType == SceneConfig.SCENE_TYPE_GUARD_GODDNESS then
        self:ShowGuardGoddnessReward(true, balanceData:GetKillMonster(), balanceData:GetExp(), balanceData:GetStarExp())
    else
        self:ShowGuardGoddnessReward(false)
    end
    
    if sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP then
        self._pvpContainer:SetActive(true)
        self._pvpMoneyRewardContainer:SetActive(not table.isEmpty(balanceData:GetMoneyRewards()))
        self._pvpItemRewardContainer:SetActive(not table.isEmpty(balanceData:GetItemRewards()))
        self._pvpMoneyRewardGridLauoutGroup:SetDataList(balanceData:GetMoneyRewards())
        -- self._pvpItemRewardGridLauoutGroup:SetDataList(balanceData:GetItemRewards())
        self._pvpItemRewardGridListData = balanceData:GetItemRewards()
        self._pvpItemRewardGridList:SetDataList(self._pvpItemRewardGridListData)
        self._pvpItemRewardGridList:SetCenter(-80, false)
        
        local len = self._pvpItemRewardGridList:GetLength()
        for i = 1, len do
            self._pvpItemRewardGridList:GetItem(i - 1):Hide()
        end
        self:ShowTweenReward(self._pvpItemRewardGridList, self._pvpItemRewardGridListData, 0)
        
        self._moneyRewardContainer:SetActive(false)
        self._itemRewardContainer:SetActive(false)
        self._textDefeat.gameObject:SetActive(true)
        self._textStageScore.gameObject:SetActive(true)
        self._textStageInfo.gameObject:SetActive(true)
        self._textDefeat.text = balanceData:GetDefeat()
        self._textStageScore.text = balanceData:GetStageScore()
        self._textStageInfo.text = balanceData:GetStageInfo()
        local stageIcon = balanceData:GetNewStageIcon()
        if stageIcon ~= nil then
            self._containerStageChange:SetActive(true)
            GameWorld.AddIcon(self._containerStageChangeStage, stageIcon)
            self._textStageChangeInfo.text = balanceData:GetStageChangeText()
        else
            self._containerStageChange:SetActive(false)
        end
    else
        self._pvpContainer:SetActive(false)
        self._moneyRewardContainer:SetActive(not table.isEmpty(balanceData:GetMoneyRewards()))
        self._itemRewardContainer:SetActive(not table.isEmpty(balanceData:GetItemRewards()))
        self._moneyRewardGridLauoutGroup:SetDataList(balanceData:GetMoneyRewards())
        -- local itemRewardNum = #balanceData:GetItemRewards()
        -- if itemRewardNum > 10 then
        --     self._rewardGridLauoutGroup:SetChildAlignment(EnumType.TextAnchor.UpperCenter)
        -- else
        --     self._rewardGridLauoutGroup:SetChildAlignment(EnumType.TextAnchor.MiddleCenter)
        -- end
        -- self._rewardGridLauoutGroup:SetChildAlignment(EnumType.TextAnchor.MiddleCenter)
        -- self._itemRewardGridLauoutGroup:SetDataList(balanceData:GetItemRewards())
        self._itemRewardGridListData = balanceData:GetItemRewards()
        self._itemRewardGridList:SetDataList(self._itemRewardGridListData)
        self._itemRewardGridList:SetCenter(-40, false)
        
        local len = self._itemRewardGridList:GetLength()
        for i = 1, len do
            self._itemRewardGridList:GetItem(i - 1):Hide()
        end
        self:ShowTweenReward(self._itemRewardGridList, self._itemRewardGridListData, 0)
        
        self._pvpMoneyRewardContainer:SetActive(false)
        self._pvpItemRewardContainer:SetActive(false)
        self._textDefeat.gameObject:SetActive(false)
        self._textStageScore.gameObject:SetActive(false)
        self._textStageInfo.gameObject:SetActive(false)
    end
    
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then
        self._countdownTime = 30
        
        self._guildConquestContainer:SetActive(true)
        self:GuildConquestBalance(balanceData)
    else
        self._guildConquestContainer:SetActive(false)
    end

    if sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then
        self._countdownTime = 10
        self._commonContainer:SetActive(false)
        self._guildBanquetContainer:SetActive(true)
        self:GuildBanquetBalance(balanceData)
    else
        self._guildBanquetContainer:SetActive(false)
    end
    
    self._containerStart:SetActive(true)
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST or sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP 
    or sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then
        self._containerStart:SetActive(false)
    end
    
    self:SetCountDownText(self._countdownTime)
    
    if self._countdownTime ~= 0 then
        self._countdownTimer = TimerHeap:AddSecTimer(0, 1, self._countdownTime, function()self:OnCountdownTick() end)
    end
end

function InstBalancePanel:ShowTweenReward(list, listData, index)
    local len = list:GetLength()
    
    if index ~= len then
        local num = perShowRewardNum
        if index + perShowRewardNum >= len then
            --最终剩余数量少于设定值
            num = len - index
        end
        
        for i = 1, num do
            list:GetItem(index + i - 1):DoShow(listData[index + i])
            if i == num and index + i ~= len then
                if index ~= 0 and (index + 1) % 10 == 0 then
                    list:SetRow(index / 10 + 1)
                end
                -- if perTime == 0 then
                --     self:ShowTweenReward(list, listData, index + i)
                -- else
                self._rewardTimer = TimerHeap:AddSecTimer(perTime, 0, 0, function()
                    self:ShowTweenReward(list, listData, index + i)
                end)
            -- end
            end
        end
    end
end

function InstBalancePanel:SetCountDownText(num)
    local text = LanguageDataHelper.CreateContent(58647, {
        ["0"] = self._countdownTime
    })

    local successText = LanguageDataHelper.CreateContent(58791, {
        ["0"] = self._countdownTime
    })
    
    if self._type == 1 then
        self._commonCountDownText.text = text
    elseif self._type == 2 then
        self._aiontowerCountDownSuccessText.text = successText
        self._aiontowerCountDownFailedText.text = text
    end
end

function InstBalancePanel:OnClose()
    self._isExit = nil
    self:ClearTimer()
    self._sweepContainer:SetActive(false)

    self:ToInitState()
end

function InstBalancePanel:ClearTimer()
    if self._countdownTimer ~= nil then
        TimerHeap:DelTimer(self._countdownTimer)
        self._countdownTimer = nil
    end
end

function InstBalancePanel:OnDestroy()
end

function InstBalancePanel:Init()
    self:InitView()
end

function InstBalancePanel:ToInitState()
    self._successContainer:SetActive(false)
    self._successWordBg:SetActive(false)
    self._bgContainer:SetActive(false)
    self._successWord1:SetActive(false)
    self._successWord2:SetActive(false)
    self._jianContainer:SetActive(false)
    self._wingLeft:SetActive(false)
    self._wingRight:SetActive(false)

    for i,v in ipairs(self._starts) do
        v.gameObject:SetActive(false)
    end

    
    self._commonButtom:SetActive(false)
    self._aiontowerButtom:SetActive(false)
    self._sweepContainer:SetActive(false)
    self._containerStart:SetActive(true)
    self._rewardContainer:SetActive(false)

    -- self._pvpMoneyRewardContainer:SetActive(false)
    -- self._pvpItemRewardContainer:SetActive(false)
    -- self._textDefeat.gameObject:SetActive(false)
    -- self._textStageScore.gameObject:SetActive(false)
    -- self._textStageInfo.gameObject:SetActive(false)
    
    self._guildBanquetContainer:SetActive(false)
    self._guardGoddnessContainer:SetActive(false)
    self._guildConquestContainer:SetActive(false)
    self._commonContainer:SetActive(true)
    self._pvpContainer:SetActive(false)

    if self._startTimer then
        TimerHeap:DelTimer(self._startTimer)
        self._startTimer = nil
    end

    if self._rewardTimer then
        TimerHeap:DelTimer(self._rewardTimer)
        self._rewardTimer = nil
    end

    self._failContainer:SetActive(false)
    self._failWordBg:SetActive(false)
    self._failWingLeft:SetActive(false)
    self._failWingRight:SetActive(false)
    self._failWord1:SetActive(false)
    self._failWord2:SetActive(false)
end

function InstBalancePanel:StartTween(data)
    self:ToInitState()
    self._balanceData = self._data.instanceBalanceData
    self._result = self._balanceData:GetResult()
    self._sceneType = self._balanceData:GetSceneType()    
    self._start = self._balanceData:GetStart()
    self._countdownTime = 10
    self._callback = data.callBack
    self._sweep = data.sweep

    if self._sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then
        self._countdownTime = 10
        self._commonContainer:SetActive(false)
        self._guildBanquetContainer:SetActive(true)
        self:GuildBanquetBalance(self._balanceData)

        self._commonButtom:SetActive(true)
        self._aiontowerButtom:SetActive(false)
        self._sweepContainer:SetActive(false)
        self._type = 1

        self:SetCountDownText(self._countdownTime)
    
        if self._countdownTime ~= 0 then
            self._countdownTimer = TimerHeap:AddSecTimer(0, 1, self._countdownTime, function()self:OnCountdownTick() end)
        end
    else
        self._guildBanquetContainer:SetActive(false)
        if self._result then
            self:StartSuccessTween()
        else
            self:StartFailTween()
        end
    end
end

function InstBalancePanel:StartFailTween()
    --1.文字底由大到小出来
    --1.翅膀降落
    --3.文字出来
    self._failContainer:SetActive(true)
    self._failWordBg:SetActive(true)
    self._failWordBgScale:SetFromToScale(0, 1, 0.2, 1, self._failWordTweenSuccessCallBack)

    self._failWingLeft:SetActive(true)
    self._failWingLeftRotation:SetFromToRotation(self._failWingLeftInitRotation, self._failWingLeftInitRotation + Vector3(0, 0, 100), 0.2, 1,self._failWingLeftCallBack)
    self._failWingRight:SetActive(true)
    self._failWingRightRotation:SetFromToRotation(self._failWingRightInitRotation, self._failWingRightInitRotation + Vector3(0, 0, 100), 0.2, 1,self._failWingRightCallBack)
end

function InstBalancePanel:FailWingLeftCallBack()
    self._failWingLeftRotation:SetFromToRotation(self._failWingLeft.transform.eulerAngles, self._failWingLeftInitRotation + Vector3(0, 0, 80), 0.1, 1,nil)
end

function InstBalancePanel:FailWingRightCallBack()
    self._failWingRightRotation:SetFromToRotation(self._failWingRight.transform.eulerAngles, self._failWingRightInitRotation + Vector3(0, 0, 80), 0.1, 1,nil)
end

function InstBalancePanel:FailWordTweenSuccessCallBack()
    self._failWord1:SetActive(true)
    self._failWord1Scale:SetSacleDirection(0,1,0.1,1,3,2,nil)
    self._failWord2:SetActive(true)
    self._failWord2Scale:SetSacleDirection(0,1,0.1,1,3,2,nil)

    
    self._bgContainer:SetActive(true)
    self._bgContainerScale:SetSacleDirection(0,1,0.1,1,3,2,nil)
    TimerHeap:AddSecTimer(0.2, 0, 0, self._failWingCompleteCallBack)
end

function InstBalancePanel:FailWingCompleteCallBack()
    self._containerStart:SetActive(false)
    self._aiontowerCountDownSuccessText.gameObject:SetActive(self._result)
    self._aiontowerCountDownFailedText.gameObject:SetActive(not self._result)  

    if self._sceneType == SceneConfig.SCENE_TYPE_PET_INSTANCE or self._sceneType == SceneConfig.SCENE_TYPE_GOLD or self._sceneType == SceneConfig.SCENE_TYPE_EQUIPMENT then
        self._tipsText.gameObject:SetActive(true)
    else
        self._tipsText.gameObject:SetActive(false)
    end
    
    if self._sceneType == SceneConfig.SCENE_TYPE_GUARD_GODDNESS then--守卫公会
        self:ShowGuardGoddnessReward(true, self._balanceData:GetKillMonster(), self._balanceData:GetExp(), self._balanceData:GetStarExp())
    elseif self._sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then --公会争霸
        self._countdownTime = 30
        self._guildConquestContainer:SetActive(true)
        self:GuildConquestBalance(self._balanceData)
    elseif self._sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then--公会篝火
        self._countdownTime = 10
        self._commonContainer:SetActive(false)
        self._guildBanquetContainer:SetActive(true)
        self:GuildBanquetBalance(self._balanceData)
    end
    
    if self._sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP then--PVP
        self._pvpContainer:SetActive(true)
        self._pvpMoneyRewardContainer:SetActive(not table.isEmpty(self._balanceData:GetMoneyRewards()))
        self._pvpItemRewardContainer:SetActive(not table.isEmpty(self._balanceData:GetItemRewards()))
        self._pvpMoneyRewardGridLauoutGroup:SetDataList(self._balanceData:GetMoneyRewards())
        -- self._pvpItemRewardGridLauoutGroup:SetDataList(balanceData:GetItemRewards())
        self._pvpItemRewardGridListData = self._balanceData:GetItemRewards()
        self._pvpItemRewardGridListlView:SetHorizontalMove(false)
        self._pvpItemRewardGridListlView:SetVerticalMove(true)
        self._pvpItemRewardGridList:SetDataList(self._pvpItemRewardGridListData)
        self._pvpItemRewardGridList:SetCenter(-80, false)
        
        local len = self._pvpItemRewardGridList:GetLength()
        for i = 1, len do
            self._pvpItemRewardGridList:GetItem(i - 1):Hide()
        end
        self._rewardContainer:SetActive(true)
        self:ShowTweenReward(self._pvpItemRewardGridList, self._pvpItemRewardGridListData, 0)
        
        self._moneyRewardContainer:SetActive(false)
        self._itemRewardContainer:SetActive(false)
        self._textDefeat.gameObject:SetActive(true)
        self._textStageScore.gameObject:SetActive(true)
        self._textStageInfo.gameObject:SetActive(true)
        self._textDefeat.text = self._balanceData:GetDefeat()
        self._textStageScore.text = self._balanceData:GetStageScore()
        self._textStageInfo.text = self._balanceData:GetStageInfo()
        local stageIcon = self._balanceData:GetNewStageIcon()
        if stageIcon ~= nil then
            self._containerStageChange:SetActive(true)
            GameWorld.AddIcon(self._containerStageChangeStage, stageIcon)
            self._textStageChangeInfo.text = self._balanceData:GetStageChangeText()
        else
            self._containerStageChange:SetActive(false)
        end

        self._type = 1
        self._commonButtom:SetActive(true)
        self._aiontowerButtom:SetActive(false)
        self._sweepContainer:SetActive(false)
        self:SetCountDownText(self._countdownTime)
    
        if self._countdownTime ~= 0 then
            self._countdownTimer = TimerHeap:AddSecTimer(0, 1, self._countdownTime, function()self:OnCountdownTick() end)
        end
    else --一般副本结算
        self:ShowReward()

        self:SetCountDownText(self._countdownTime)
    
        if self._countdownTime ~= 0 then
            self._countdownTimer = TimerHeap:AddSecTimer(0, 1, self._countdownTime, function()self:OnCountdownTick() end)
        end
    end
end

function InstBalancePanel:StartSuccessTween()
    -- 1.文字底由大到小出来
    -- 2.内容底部出来
    -- 2.文字出来
    -- 2.翅膀展开
    -- 2.剑插下来
    -- 3.文字动一下
    -- 4.奖励出来
    -- 5.奖励出来
    -- 5.星星出来
    self._successContainer:SetActive(true)
    self._successWordBg:SetActive(true)
    self._successWordBgScale:SetFromToScale(0, 1, 0.2, 1, self._successWordTweenSuccessCallBack)
end

function InstBalancePanel:SuccessWordTweenSuccessCallBack()
    self._bgContainer:SetActive(true)
    self._bgContainerScale:SetSacleDirection(0,1,0.1,1,3,2,nil)

    self._successWord1:SetActive(true)
    self._successWord1Scale:SetFromToScale(0, 1, 0.1, 1, self._successWord1Scale1CallBack)

    self._jianContainer:SetActive(true)
    self._jianContainerPos:SetFromToPosTween(self._jianInitPosition, self._jianInitPosition + Vector3(0, -468, 0), 0.1, 1, 3,nil)
    
    self._wingLeft:SetActive(true)
    self._wingLeftRotation:SetFromToRotation(self._wingLeftInitRotation, self._wingLeftInitRotation + Vector3(0, 0, -30), 0.1, 1,self._successWingLeftCallBack)
    self._wingRight:SetActive(true)
    self._wingRightRotation:SetFromToRotation(self._wingRightInitRotation, self._wingRightInitRotation + Vector3(0, 0, -30), 0.1, 1,self._successWingRightCallBack)
    TimerHeap:AddSecTimer(0.2, 0, 0, self._successWingCompleteCallBack)
end

function InstBalancePanel:SuccessWingLeftCallBack()
    self._wingLeftRotation:SetFromToRotation(self._wingLeft.transform.eulerAngles, self._wingLeftInitRotation + Vector3(0, 0, -17), 0.1, 1,nil)
end

function InstBalancePanel:SuccessWingRightCallBack()
    self._wingRightRotation:SetFromToRotation(self._wingRight.transform.eulerAngles, self._wingRightInitRotation + Vector3(0, 0, -17), 0.1, 1,nil)
end

function InstBalancePanel:SuccessWingCompleteCallBack()
    self._aiontowerCountDownSuccessText.gameObject:SetActive(self._result)
    self._aiontowerCountDownFailedText.gameObject:SetActive(not self._result)  
    local startShow
    if self._sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST or self._sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP 
    or self._sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then
        startShow = false
        self._containerStart:SetActive(false)
    else
        startShow = true
        self._containerStart:SetActive(true)
    end

    if self._sceneType == SceneConfig.SCENE_TYPE_PET_INSTANCE or self._sceneType == SceneConfig.SCENE_TYPE_GOLD or self._sceneType == SceneConfig.SCENE_TYPE_EQUIPMENT then
        self._tipsText.gameObject:SetActive(true)
    else
        self._tipsText.gameObject:SetActive(false)
    end

    if self._sceneType == SceneConfig.SCENE_TYPE_GUARD_GODDNESS then--守卫公会
        self:ShowGuardGoddnessReward(true, self._balanceData:GetKillMonster(), self._balanceData:GetExp(), self._balanceData:GetStarExp())
    elseif self._sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then --公会争霸
        self._countdownTime = 30
        self._guildConquestContainer:SetActive(true)
        self:GuildConquestBalance(self._balanceData)
    elseif self._sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then--公会篝火
        self._countdownTime = 10
        self._commonContainer:SetActive(false)
        self._guildBanquetContainer:SetActive(true)
        self:GuildBanquetBalance(self._balanceData)
    end

    if self._sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP then--PVP
        self._pvpContainer:SetActive(true)
        self._pvpMoneyRewardContainer:SetActive(not table.isEmpty(self._balanceData:GetMoneyRewards()))
        self._pvpItemRewardContainer:SetActive(not table.isEmpty(self._balanceData:GetItemRewards()))
        self._pvpMoneyRewardGridLauoutGroup:SetDataList(self._balanceData:GetMoneyRewards())
        -- self._pvpItemRewardGridLauoutGroup:SetDataList(balanceData:GetItemRewards())
        self._pvpItemRewardGridListData = self._balanceData:GetItemRewards()
        self._pvpItemRewardGridListlView:SetHorizontalMove(false)
        self._pvpItemRewardGridListlView:SetVerticalMove(true)
        self._pvpItemRewardGridList:SetDataList(self._pvpItemRewardGridListData)
        self._pvpItemRewardGridList:SetCenter(-80, false)
        
        local len = self._pvpItemRewardGridList:GetLength()
        for i = 1, len do
            self._pvpItemRewardGridList:GetItem(i - 1):Hide()
        end
        self._rewardContainer:SetActive(true)
        self:ShowTweenReward(self._pvpItemRewardGridList, self._pvpItemRewardGridListData, 0)
        
        self._moneyRewardContainer:SetActive(false)
        self._itemRewardContainer:SetActive(false)
        self._textDefeat.gameObject:SetActive(true)
        self._textStageScore.gameObject:SetActive(true)
        self._textStageInfo.gameObject:SetActive(true)
        self._textDefeat.text = self._balanceData:GetDefeat()
        self._textStageScore.text = self._balanceData:GetStageScore()
        self._textStageInfo.text = self._balanceData:GetStageInfo()
        local stageIcon = self._balanceData:GetNewStageIcon()
        if stageIcon ~= nil then
            self._containerStageChange:SetActive(true)
            GameWorld.AddIcon(self._containerStageChangeStage, stageIcon)
            self._textStageChangeInfo.text = self._balanceData:GetStageChangeText()
        else
            self._containerStageChange:SetActive(false)
        end

        self._commonButtom:SetActive(true)
        self._aiontowerButtom:SetActive(false)
        self._sweepContainer:SetActive(false)
        self._type = 1
        self:SetCountDownText(self._countdownTime)
    
        if self._countdownTime ~= 0 then
            self._countdownTimer = TimerHeap:AddSecTimer(0, 1, self._countdownTime, function()self:OnCountdownTick() end)
        end
    else --一般副本结算，扫荡        
        local start = self._balanceData:GetStart()
        if startShow == true then
            for i = 1, 3 do
                -- if sceneType ==
                if i <= start then
                    self._startTimer = TimerHeap:AddSecTimer(0.1 * i, 0, 0, function() 
                        self:ShowStart(i)
                    end)
                    
                end
            end
        end

        self:ShowReward()

        self:SetCountDownText(self._countdownTime)
    
        if self._countdownTime ~= 0 then
            self._countdownTimer = TimerHeap:AddSecTimer(0, 1, self._countdownTime, function()self:OnCountdownTick() end)
        end
    end
end

function InstBalancePanel:ShowReward()
    self._rewardContainer:SetActive(true)
    self._moneyRewardContainer:SetActive(not table.isEmpty(self._balanceData:GetMoneyRewards()))
    self._itemRewardContainer:SetActive(not table.isEmpty(self._balanceData:GetItemRewards()))
    self._moneyRewardGridLauoutGroup:SetDataList(self._balanceData:GetMoneyRewards())
    -- local itemRewardNum = #balanceData:GetItemRewards()
    -- if itemRewardNum > 10 then
    --     self._rewardGridLauoutGroup:SetChildAlignment(EnumType.TextAnchor.UpperCenter)
    -- else
    --     self._rewardGridLauoutGroup:SetChildAlignment(EnumType.TextAnchor.MiddleCenter)
    -- end
    -- self._rewardGridLauoutGroup:SetChildAlignment(EnumType.TextAnchor.MiddleCenter)
    -- self._itemRewardGridLauoutGroup:SetDataList(balanceData:GetItemRewards())
    self._itemRewardGridListData = self._balanceData:GetItemRewards()
    self._itemRewardGridList:SetDataList(self._itemRewardGridListData)
    self._itemRewardGridList:SetCenter(-40, false)
    
    local len = self._itemRewardGridList:GetLength()
    for i = 1, len do
        self._itemRewardGridList:GetItem(i - 1):Hide()
    end
    self:ShowTweenReward(self._itemRewardGridList, self._itemRewardGridListData, 0)    

    if self._sceneType == SceneConfig.SCENE_TYPE_AION_TOWER then --永恒之塔
        GameWorld.Player():StopAutoFight()
        self._commonButtom:SetActive(false)
        self._aiontowerButtom:SetActive(true)
        self._type = 2
        self._btnAiontowerNext:SetActive(self._result)
        if AionTowerManager:GetNowPos() == 2 then
            self._btnAiontowerNext:SetActive(false)
            self._aiontowerCountDownSuccessText.gameObject:SetActive(false)
            self._aiontowerCountDownFailedText.gameObject:SetActive(true)
            self._isExit = true
            self._callback = function()
                EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_aion_tower_level1_quit_ui_button")
                self:ExitInstance()
            end
        end
    else
        if self._sweep then
            self._countdownTime = 0
            self._sweepContainer:SetActive(true)
            self._commonButtom:SetActive(false)
            self._aiontowerButtom:SetActive(false)
            self._btnAgainSweep:SetActive(InstanceManager:IsCanAgain())
            local sweep = InstanceManager:GetSweepData()
            GameWorld.AddIcon(self._icon, ItemDataHelper.GetIcon(sweep:GetItemId()))
            local bagCount = BagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(sweep:GetItemId())
            self._sweepCostNum.text = bagCount .. "/" .. sweep:GetItemNum()
        else
            self._commonButtom:SetActive(true)
            self._aiontowerButtom:SetActive(false)
            self._sweepContainer:SetActive(false)
            self._type = 1
        end    
    end

end

function InstBalancePanel:ShowStart(i)
    self._starts[i].gameObject:SetActive(true)
    self._startScales[i]:SetFromToScale(0, 1, 0.1, 1, nil)
end

function InstBalancePanel:SuccessWord1Scale1CallBack()
    self._successWord2:SetActive(true)
    self._successWord2Scale:SetFromToScale(0, 1, 0.1, 1, nil)
end

--初始化
function InstBalancePanel:InitView()
    self._successWordTweenSuccessCallBack = function() self:SuccessWordTweenSuccessCallBack() end
    self._successWord1Scale1CallBack = function() self:SuccessWord1Scale1CallBack() end
    self._successWingLeftCallBack = function() self:SuccessWingLeftCallBack() end
    self._successWingRightCallBack = function() self:SuccessWingRightCallBack() end
    self._successWingCompleteCallBack = function() self:SuccessWingCompleteCallBack()end
    -- self._showStart = function() self:ShowStart() end

    --胜利
    self._jianContainer = self:FindChildGO("Container_Common/Container_Jian")
    self._jianContainerPos = self._jianContainer:AddComponent(typeof(XGameObjectTweenPosition))
    self._jianInitPosition = self._jianContainer.transform.localPosition
    self._bgContainer = self:FindChildGO("Container_Common/Container_Bg")
    self._bgContainerScale = self._bgContainer:AddComponent(typeof(XGameObjectTweenScale))
    self._successContainer = self:FindChildGO("Container_Common/Container_Success")
    self._successWordBg = self:FindChildGO("Container_Common/Container_Success/Image_Pass_Word_Bg")
    self._successWordBgScale = self._successWordBg:AddComponent(typeof(XGameObjectTweenScale))
    self._successWord1 = self:FindChildGO("Container_Common/Container_Success/Image_Word_1")
    self._successWord1FlowLight = self:AddChildComponent("Container_Common/Container_Success/Image_Word_1", XImageFlowLight)
    self._successWord1FlowLight:SetSpeed(-25)
    self._successWord1Scale = self._successWord1:AddComponent(typeof(XGameObjectTweenScale))
    self._successWord2 = self:FindChildGO("Container_Common/Container_Success/Image_Word_2")
    self._successWord2FlowLight = self:AddChildComponent("Container_Common/Container_Success/Image_Word_2", XImageFlowLight)
    self._successWord2FlowLight:SetSpeed(-25)
    self._successWord2Scale = self._successWord2:AddComponent(typeof(XGameObjectTweenScale))
    self._rewardContainer = self:FindChildGO("Container_Common/Container_Reward")
    self._wingLeft = self:FindChildGO("Container_Common/Container_Success/Image_Wing_Left")
    self._wingLeftRotation = self._wingLeft:AddComponent(typeof(XGameObjectTweenRotation))
    self._wingLeftInitRotation = self._wingLeft.transform.eulerAngles
    self._wingRight = self:FindChildGO("Container_Common/Container_Success/Image_Wing_Right")
    self._wingRightRotation = self._wingRight:AddComponent(typeof(XGameObjectTweenRotation))
    self._wingRightInitRotation = self._wingRight.transform.eulerAngles

    --失败
    self._failContainer = self:FindChildGO("Container_Common/Container_Fail")
    self._failWordBg = self:FindChildGO("Container_Common/Container_Fail/Image_Pass_Word_Bg")
    self._failWordBgScale = self._failWordBg:AddComponent(typeof(XGameObjectTweenScale))
    self:GetChildComponent("Container_Common/Container_Fail/Image_Pass_Word_Bg", "ImageWrapper"):SetGray(0)
    self._failWingLeft = self:FindChildGO("Container_Common/Container_Fail/Image_Wing_Left")
    self._failWingLeftRotation = self._failWingLeft:AddComponent(typeof(XGameObjectTweenRotation))
    self._failWingLeftInitRotation = self._failWingLeft.transform.eulerAngles
    self:GetChildComponent("Container_Common/Container_Fail/Image_Wing_Left", "ImageWrapper"):SetGray(0)
    self._failWingRight = self:FindChildGO("Container_Common/Container_Fail/Image_Wing_Right")
    self._failWingRightRotation = self._failWingRight:AddComponent(typeof(XGameObjectTweenRotation))
    self._failWingRightInitRotation = self._failWingRight.transform.eulerAngles
    self:GetChildComponent("Container_Common/Container_Fail/Image_Wing_Right", "ImageWrapper"):SetGray(0)
    self._failWord1 = self:FindChildGO("Container_Common/Container_Fail/Image_Word_1")
    self._failWord1Scale = self._failWord1:AddComponent(typeof(XGameObjectTweenScale))
    self._failWord2 = self:FindChildGO("Container_Common/Container_Fail/Image_Word_2")
    self._failWord2Scale = self._failWord2:AddComponent(typeof(XGameObjectTweenScale))

    
    self._failWordTweenSuccessCallBack = function() self:FailWordTweenSuccessCallBack() end
    self._failWingLeftCallBack = function() self:FailWingLeftCallBack() end
    self._failWingRightCallBack = function() self:FailWingRightCallBack() end
    self._failWingCompleteCallBack = function() self:FailWingCompleteCallBack()end

    -- self._successWingstop = Vector3.New(0, 0, 0)
    -- self._successWingstart = Vector3.New(0, 0, 15)

    self._starts = {}
    self._startScales = {}
    for i = 1, 3 do
        table.insert(self._starts, self:GetChildComponent("Container_Common/Container_Start/Image_Start_" .. i, "ImageWrapper"))
        table.insert(self._startScales, self._starts[i].gameObject:AddComponent(typeof(XGameObjectTweenScale)))
    end
    self._containerStart = self:FindChildGO("Container_Common/Container_Start")
    self._successContainer = self:FindChildGO("Container_Common/Container_Success")
    self._failContainer = self:FindChildGO("Container_Common/Container_Fail")
    self._commonButtom = self:FindChildGO("Container_Buttom_Common")
    self._aiontowerButtom = self:FindChildGO("Container_Buttom_AionTower_Success")
    self._moneyRewardContainer = self:FindChildGO("Container_Common/Container_Reward/Container_Money")
    self._itemRewardContainer = self:FindChildGO("Container_Common/Container_Reward/Container_Item")
    self._pvpMoneyRewardContainer = self:FindChildGO("Container_Pvp/Container_Reward/Container_Money")
    self._pvpItemRewardContainer = self:FindChildGO("Container_Pvp/Container_Reward/Container_Item")

    self._commonContainer = self:FindChildGO("Container_Common")
    
    self._btnCommonQuit = self:FindChildGO("Container_Buttom_Common/Button_Quit")
    self._csBH:AddClick(self._btnCommonQuit, function()self:BtnQuitClick() end)
    
    self._btnAiontowerQuit = self:FindChildGO("Container_Buttom_AionTower_Success/Container_Buttons/Button_Quit")
    self._csBH:AddClick(self._btnAiontowerQuit, function()self:BtnQuitClick() end)
    
    self._btnAiontowerNext = self:FindChildGO("Container_Buttom_AionTower_Success/Container_Buttons/Button_Next")
    self._csBH:AddClick(self._btnAiontowerNext, function()self:BtnNextAiontowerClick() end)
    
    self._commonCountDownText = self:GetChildComponent("Container_Buttom_Common/Text_CountDown", 'TextMeshWrapper')
    self._aiontowerCountDownSuccessText = self:GetChildComponent("Container_Buttom_AionTower_Success/Text_CountDown_Success", 'TextMeshWrapper')
    self._aiontowerCountDownFailedText = self:GetChildComponent("Container_Buttom_AionTower_Success/Text_CountDown_Failed", 'TextMeshWrapper')
    self._tipsText = self:GetChildComponent("Container_Buttom_Common/Text_Tips", 'TextMeshWrapper')
    
    self._guardGoddnessContainer = self:FindChildGO("Container_GuardGoddness")
    self._guardGoddnessExpIcon = self:FindChildGO("Container_GuardGoddness/Container_Reward/Text_Exp/Container_Icon")
    self._guardGoddnessStarExpIcon = self:FindChildGO("Container_GuardGoddness/Container_Reward/Text_StarExp/Container_Icon")
    self._guardGoddnessKillMonsterText = self:GetChildComponent("Container_GuardGoddness/Container_Reward/Text_KillMonster/Text_Value", 'TextMeshWrapper')
    self._guardGoddnessExpText = self:GetChildComponent("Container_GuardGoddness/Container_Reward/Text_Exp/Text_Value", 'TextMeshWrapper')
    self._guardGoddnessStarExpText = self:GetChildComponent("Container_GuardGoddness/Container_Reward/Text_StarExp/Text_Value", 'TextMeshWrapper')
    
    self._pvpContainer = self:FindChildGO("Container_Pvp")
    self._textDefeat = self:GetChildComponent("Container_Pvp/Text_Defeat", 'TextMeshWrapper')
    self._textStageScore = self:GetChildComponent("Container_Pvp/Text_StageScore", 'TextMeshWrapper')
    self._textStageInfo = self:GetChildComponent("Container_Pvp/Text_StageInfo", 'TextMeshWrapper')
    self._textStageChangeInfo = self:GetChildComponent("Container_Pvp/Container_StageChange/Text_StageInfo", 'TextMeshWrapper')
    self._containerStageChange = self:FindChildGO("Container_Pvp/Container_StageChange")
    self._containerStageChangeStage = self:FindChildGO("Container_Pvp/Container_StageChange/Container_Stage")
    self._containerBG = self:FindChildGO("Container_Pvp/Container_StageChange/Image_BG")
    self._csBH:AddClick(self._containerBG, function()self:PvpSrageChangeBGClick() end)
    
    self._guildConquestContainer = self:FindChildGO("Container_GuildConquest")
    self._nobilityIcon = self:FindChildGO("Container_GuildConquest/Container_Self/Container_Icon")
    self._rankText = self:GetChildComponent("Container_GuildConquest/Container_Self/Text_Rank", 'TextMeshWrapper')
    self._nameText = self:GetChildComponent("Container_GuildConquest/Container_Self/Text_Name", 'TextMeshWrapper')
    self._killText = self:GetChildComponent("Container_GuildConquest/Container_Self/Text_Kill", 'TextMeshWrapper')
    self._deadText = self:GetChildComponent("Container_GuildConquest/Container_Self/Text_Dead", 'TextMeshWrapper')
    self._resourceText = self:GetChildComponent("Container_GuildConquest/Container_Self/Text_Resource", 'TextMeshWrapper')
    self._rewardText = self:GetChildComponent("Container_GuildConquest/Container_Self/Text_Reward", 'TextMeshWrapper')

    self._guildBanquetContainer = self:FindChildGO("Container_GuildBanquet")
    self._rankBanquetText = self:GetChildComponent("Container_GuildBanquet/Container_Self/Text_Rank", 'TextMeshWrapper')
    self._nameBanquetText = self:GetChildComponent("Container_GuildBanquet/Container_Self/Text_Name", 'TextMeshWrapper')
    self._resourceBanquetText = self:GetChildComponent("Container_GuildBanquet/Container_Self/Text_Resource", 'TextMeshWrapper')
    self._guildBanquetInfoText = self:FindChildGO("Container_GuildBanquet/Text_Info")
    self._guildBanquetSelf = self:FindChildGO("Container_GuildBanquet/Container_Self")

    local scrollViewBanquet, listBanquet = UIList.AddScrollViewList(self.gameObject, "Container_GuildBanquet/ScrollViewList")
    self._listBanquet = listBanquet
    self._scrollViewBanquet = scrollViewBanquet
    self._listBanquet:SetItemType(GuildBanquetBalanceItem)
    self._listBanquet:SetPadding(10, 0, 0, 0)
    self._listBanquet:SetGap(5, 10)
    self._listBanquet:SetDirection(UIList.DirectionTopToDown, 1, 1)
    
    --扫荡加入逻辑
    self._sweepContainer = self:FindChildGO("Container_Sweep")
    self._sweepCostText = self:GetChildComponent("Container_Sweep/Text_Cost", 'TextMeshWrapper')
    self._sweepCostNum = self:GetChildComponent("Container_Sweep/Text_Num", 'TextMeshWrapper')
    self._icon = self:FindChildGO("Container_Sweep/Container_Icon")
    
    self._btnSureSweep = self:FindChildGO("Container_Sweep/Container_Buttons/Button_Sure")
    self._csBH:AddClick(self._btnSureSweep, function()self:SureSweepClick() end)
    self._btnAgainSweep = self:FindChildGO("Container_Sweep/Container_Buttons/Button_Again")
    self._csBH:AddClick(self._btnAgainSweep, function()self:AgainSweepClick() end)
    self._sweepContainer:SetActive(false)
    
    
    
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_GuildConquest/ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(GuildConquestBalanceItem)
    self._list:SetPadding(10, 0, 0, 10)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
    
    self:InitMoneyRewardGridLayoutGroup()
    -- self:InitItemRewardGridLayoutGroup()
    self:InitItemRewardGridList()
    self:InitPvpMoneyRewardGridLayoutGroup()
    -- self:InitPvpItemRewardGridLayoutGroup()
    self:InitPvpItemRewardGridList()
    -- self._rewardGridLauoutGroup = self:GetChildComponent("Container_GuildConquest/Container_Self/Text_Reward", 'GridLayoutGroup')
    self._rewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Common/Container_Reward")
end

function InstBalancePanel:GuildBanquetBalance(balanceData)
    local selfGuildId = GuildManager:GetGuildId()
    --{rank:{RANK_KEY_GUILD_NAME, RANK_KEY_BONFIRE_SCORE, RANK_KEY_GUILD_DBID, RANK_KEY_RANK_VALUE}}
    local banquetList = balanceData:GetBanquetList()
    local selfData
    for i = 1, #banquetList do
        if selfGuildId == banquetList[i][public_config.RANK_KEY_GUILD_DBID] then
            selfData = banquetList[i]
        end
    end

    if #banquetList == 0 then
        self._guildBanquetInfoText:SetActive(true)
    else
        self._guildBanquetInfoText:SetActive(false)
    end
    if selfData then
        self._guildBanquetSelf:SetActive(true)
    else
        selfData = {}
        self._guildBanquetSelf:SetActive(false)
    end

    self._listBanquet:SetDataList(banquetList)
    self._rankBanquetText.text = selfData[public_config.RANK_KEY_RANK_VALUE]
    self._nameBanquetText.text = selfData[public_config.RANK_KEY_GUILD_NAME]
    self._resourceBanquetText.text = selfData[public_config.RANK_KEY_BONFIRE_SCORE]
end

function InstBalancePanel:GuildConquestBalance(balanceData)
    local selfGuildId = GuildManager:GetGuildId()
    local isWin = balanceData:GetResult()
    local listData = {}
    --{dbid:{guild_id,player_name,玩家爵位，击杀，死亡，夺点, 战力}}
    local conquestList = balanceData:GetConquestList()
    for k, v in pairs(conquestList) do
        local isFriend = true
        if selfGuildId ~= v[1] then
            isFriend = false
            isWin = not isWin
        end
        table.insert(listData, {v[2], v[3], v[4], v[5], v[6], v[7], k, isWin, isFriend})
    end
    table.sort(listData, SortByOrder)
    local selfData = {}
    for i = 1, #listData do
        table.insert(listData[i], i)
        local reward, extraReward = self:GetGuildConquestReward(listData[i])
        table.insert(listData[i], reward)
        table.insert(listData[i], extraReward)
        if listData[i][7] == GameWorld.Player().dbid then
            selfData = listData[i]
        end
    end
    --listData：{1-player_name, 2-玩家爵位，3-击杀，4-死亡，5-夺点, 6-战力，7-dbid, 8-是否获胜，9-是否友军，10-排名，11-奖励 12-额外奖励}
    self._list:SetDataList(listData)
    --爵位
    local nobility = selfData[2] or 0
    if nobility ~= 0 then
        local icon = NobilityDataHelper:GetNobilityShow(nobility)
        self._nobilityIcon:SetActive(true)
        GameWorld.AddIcon(self._nobilityIcon, icon)
    else
        self._nobilityIcon:SetActive(false)
    end
    self._rankText.text = selfData[10]
    self._nameText.text = selfData[1]
    self._killText.text = selfData[3]
    self._deadText.text = selfData[4]
    self._resourceText.text = selfData[5]
    local reward = selfData[11]
    local extraReward = selfData[12]
    self._rewardText.text = string.format("%s(%s)", reward, extraReward)
end

--胜负基础分+击杀数*k1+死亡数*k2+夺点数*k3
function InstBalancePanel:GetGuildConquestReward(data)
    local base = 0
    local extra = 0
    local _, basicData = next(GuildConquestManager:GetActionData(action_config.GUILD_CONQUEST_NOTIFY_BASIC_INFO))
    local grade = math.ceil(basicData[4] / 4)
    local t
    if data[8] then
        t, base = next(GuildDataHelper:GetGuildBanquetWinReward(grade))
    else
        t, base = next(GuildDataHelper:GetGuildBanquetLoseReward(grade))
    end
    local config = GlobalParamsHelper.GetParamValue(686)
    extra = data[3] * config[1] + data[4] * config[2] + data[5] * config[3]
    local total = base + extra
    return total, extra
end

function InstBalancePanel:ShowGuardGoddnessReward(state, killNum, exp, starExp)
    self._guardGoddnessContainer:SetActive(state)
    if state then
        local icon = ItemDataHelper.GetIcon(public_config.MONEY_TYPE_EXP)
        GameWorld.AddIcon(self._guardGoddnessExpIcon, icon)
        GameWorld.AddIcon(self._guardGoddnessStarExpIcon, icon)
        self._guardGoddnessKillMonsterText.text = killNum
        self._guardGoddnessExpText.text = StringStyleUtil.GetLongNumberString(exp)
        self._guardGoddnessStarExpText.text = StringStyleUtil.GetLongNumberString(starExp)
    end
end

function InstBalancePanel:InitMoneyRewardGridLayoutGroup()
    self._moneyRewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Common/Container_Reward/Container_Money")
    self._moneyRewardGridLauoutGroup:SetItemType(BalanceMoneyRewardItem)
end

function InstBalancePanel:InitItemRewardGridList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Common/Container_Reward/Container_Item/ScrollViewList")
    self._itemRewardGridList = list
    self._itemRewardGridListlView = scrollView
    -- self._itemRewardGridListlView:SetHorizontalMove(false)
    -- self._itemRewardGridListlView:SetVerticalMove(true)
    self._itemRewardGridList:SetItemType(BalanceItemRewardItem)
    self._itemRewardGridList:SetPadding(0, 0, 0, 20)
    self._itemRewardGridList:SetGap(10, 0)
    self._itemRewardGridList:SetDirection(UIList.DirectionLeftToRight, 10, -1)
end

function InstBalancePanel:InitItemRewardGridLayoutGroup()
    self._itemRewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Common/Container_Reward/Container_Item")
    self._itemRewardGridLauoutGroup:SetItemType(BalanceItemRewardItem)
end

function InstBalancePanel:InitPvpMoneyRewardGridLayoutGroup()
    self._pvpMoneyRewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Pvp/Container_Reward/Container_Money")
    self._pvpMoneyRewardGridLauoutGroup:SetItemType(BalanceMoneyRewardItem)
end

function InstBalancePanel:InitPvpItemRewardGridList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Pvp/Container_Reward/Container_Item/ScrollViewList")
    self._pvpItemRewardGridList = list
    self._pvpItemRewardGridListlView = scrollView
    -- self._pvpItemRewardGridListlView:SetHorizontalMove(false)
    -- self._pvpItemRewardGridListlView:SetVerticalMove(true)
    self._pvpItemRewardGridList:SetItemType(BalanceItemRewardItem)
    self._pvpItemRewardGridList:SetPadding(0, 0, 0, 20)
    self._pvpItemRewardGridList:SetGap(10, 0)
    self._pvpItemRewardGridList:SetDirection(UIList.DirectionLeftToRight, 10, -1)

end

function InstBalancePanel:InitPvpItemRewardGridLayoutGroup()
    self._pvpItemRewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Pvp/Container_Reward/Container_Item")
    self._pvpItemRewardGridLauoutGroup:SetItemType(BalanceItemRewardItem)
end

function InstBalancePanel:OnCountdownTick()
    self._countdownTime = self._countdownTime - 1
    if self._countdownTime == 0 then        
        if self._sceneType == SceneConfig.SCENE_TYPE_AION_TOWER and self._result and (not self._isExit) then
            self:BtnNextAiontowerClick()
        else
            self:BtnQuitClick()
        end
    end
    self:SetCountDownText(self._countdownTime)
end

function InstBalancePanel:BtnQuitClick()
    -- EventDispatcher:TriggerEvent(GameEvents.ON_INSTANCE_EXIT)
    GUIManager.ClosePanel(PanelsConfig.InstBalance)
    if self._callback then
        self._callback()
    else
        self:ExitInstance()
    end
end

function InstBalancePanel:ExitInstance()
    if not self._result then
        TaskCommonManager:SetCurAutoTask(nil, nil)
    end
    PlayerActionManager:StopAllAction()
    -- GameWorld.Player():StopMoveWithoutCallback()
    -- LoggerHelper.Log("Ash: InstBalancePanel:ExitInstance()")
    InstanceManager:OnInstanceExit()
end

function InstBalancePanel:PvpSrageChangeBGClick()
    self._containerStageChange:SetActive(false)
end

function InstBalancePanel:BtnNextAiontowerClick()
    AionTowerManager:OnAionEnter()
-- self:ClearTimer()
end

--扫荡确认
function InstBalancePanel:SureSweepClick()
    GUIManager.ClosePanel(PanelsConfig.InstBalance)
end

--再来一次
function InstBalancePanel:AgainSweepClick()
    --GameManager.GUIManager.ClosePanel(PanelsConfig.InstBalance)
    InstanceManager:SweepAgain()
end
