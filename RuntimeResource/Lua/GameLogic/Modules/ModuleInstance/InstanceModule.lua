-- InstanceModule.lua
InstanceModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function InstanceModule.Init()
    GUIManager.AddPanel(PanelsConfig.InstBalance, "Panel_InstBalance", GUILayer.LayerUICommon, "Modules.ModuleInstance.InstBalancePanel", true, false)
    GUIManager.AddPanel(PanelsConfig.InstBalanceCG, "Panel_InstBalance", GUILayer.LayerUICG, "Modules.ModuleInstance.InstBalancePanel", true, false)
    GUIManager.AddPanel(PanelsConfig.ExpInstance, "Panel_ExpInstance", GUILayer.LayerUIMiddle, "Modules.ModuleInstance.ExpInstancePanel", true, PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.PetInstanceInfo, "Panel_PetInstance", GUILayer.LayerUIMiddle, "Modules.ModuleInstance.PetInstanceInfoPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.EquipmentInstanceInfo, "Panel_EquipmentInstance", GUILayer.LayerUIMiddle, "Modules.ModuleInstance.EquipmentInstanceInfoPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.MoneyInstanceInfo, "Panel_MoneyInstance", GUILayer.LayerUIMiddle, "Modules.ModuleInstance.MoneyInstanceInfoPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.GuildMonsterInstanceInfo, "Panel_GuildMonsterInstance", GUILayer.LayerUIMiddle, "Modules.ModuleInstance.GuildMonsterInstanceInfoPanel", true, PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.CloudPeak, "Panel_CloudPeak", GUILayer.LayerUIPanel, "Modules.ModuleInstance.CloudPeakPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.CloudPeakInfo, "Panel_CloudPeakInfo", GUILayer.LayerUIMiddle, "Modules.ModuleInstance.CloudPeakInfoPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.TowerInstanceInfo, "Panel_TowerInstanceInfo", GUILayer.LayerUIMiddle, "Modules.ModuleInstance.TowerInstanceInfoPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.Answer, "Panel_Answer", GUILayer.LayerUIPanel, "Modules.ModuleInstance.AnswerPanel", true, false)
end

return InstanceModule
