-- RankModule.lua
RankModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function RankModule.Init()
    GUIManager.AddPanel(PanelsConfig.Rank, "Panel_Rank", GUILayer.LayerUIPanel, "Modules.ModuleRank.RankPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return RankModule
