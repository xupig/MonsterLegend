local RankPanel = Class.RankPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local RankExManager = PlayerManager.RankExManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local TransformDataHelper = GameDataHelper.TransformDataHelper
local ActorModelComponent = GameMain.ActorModelComponent
local EquipModelComponent = GameMain.EquipModelComponent
local public_config = GameWorld.public_config
local CheckOtherPlayerManager = PlayerManager.CheckOtherPlayerManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
require "Modules.ModuleRank.ChildComponent.RankContentListItem"
local RankContentListItem = ClassTypes.RankContentListItem

require "Modules.ModuleRank.ChildComponent.RankListItem"
local RankListItem = ClassTypes.RankListItem

function RankPanel:Awake()
	self:InitCallBack()
	self:InitComps()
end

function RankPanel:InitCallBack()
    self._getRankListResp = function() self:GetRankListResp() end
end

function RankPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.RANK_LIST_GET_RESP, self._getRankListResp)
end

function RankPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.RANK_LIST_GET_RESP, self._getRankListResp)
end

function RankPanel:InitComps()
    self._selectIndex = nil
    self._selectContentIndex = nil
    self._rankText = self:GetChildComponent("Container_Content/Container_My/Text_Rank",'TextMeshWrapper')
    self._rankOutTextGo = self:FindChildGO("Container_Content/Container_My/Text_Rank_Out")

    self._playerModelComponent = self:AddChildComponent('Container_Models/Container_Model_Player', ActorModelComponent)
    self._playerModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)

    self._lookPlay = self:FindChildGO("Container_Look/Button_Look")
    self._csBH:AddClick(self._lookPlay, function() self:OnLookPlayer() end)

    self:InitRankContentList()
    self:InitList()
end

function RankPanel:InitRankContentList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/Container_List/ScrollViewList")
	self._rankContentList = list
	self._rankContentListlView = scrollView 
    self._rankContentListlView:SetHorizontalMove(false)
    self._rankContentListlView:SetVerticalMove(true)
    self._rankContentList:SetItemType(RankContentListItem)
    self._rankContentList:SetPadding(0, 0, 0, 0)
    self._rankContentList:SetGap(0, 0)
    self._rankContentList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    local itemClickedCB = function(idx)
		self:OnContentListItemClicked(idx)
	end
	self._rankContentList:SetItemClickedCB(itemClickedCB)
end

function RankPanel:OnContentListItemClicked(idx)
    if self._selectContentIndex == idx then return end
    if self._selectContentIndex ~= nil then
        self._rankContentList:GetItem(self._selectContentIndex):SetSelect(false)
    end
    self._selectContentIndex = idx
    self._rankContentList:GetItem(self._selectContentIndex):SetSelect(true)


    -- LoggerHelper.Log(self._listData[self._selectIndex + 1].rankType.."::"..public_config.RANK_TYPE_PET)

    if self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_PET then
        self:UpdatePetModel(idx)
    elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_HORSE then
        self:UpdateRideModel(idx)
    elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_WING then --翅膀
        self:UpdateModel(public_config.TREASURE_TYPE_WING)
    elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_TALISMAN then  --法宝
        self:UpdateModel(public_config.TREASURE_TYPE_TALISMAN)
    elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_WEAPON then --神兵
        self:UpdateModel(public_config.TREASURE_TYPE_WEAPON)
   
    else
        if self._equipModelComponent then
            self._equipModelComponent:Hide()
        end   
        
        if self._rideModelComponent then
            self._rideModelComponent:Hide()
        end

        if self._petModelComponent then
            self._petModelComponent:Hide()
        end

        self._playerModelComponent:LoadAvatarModel(self._rankContentListData[idx + 1]:GetVocation(), self._rankContentListData[idx + 1]:GetFacade(), false)
        self._playerModelComponent:Show()
    end
    
end


function RankPanel:UpdatePetModel(idx)
    if self._equipModelComponent then
        self._equipModelComponent:Hide()
    end   
    
    if self._playerModelComponent then
        self._playerModelComponent:Hide()
    end

    if self._rideModelComponent then
        self._rideModelComponent:Hide()
    end

    if self._petModelComponent then
        --return
    else
        self._containerPetModel = self:FindChildGO("Container_Models/Container_Model_Pet")
        self._petModelComponent = self:AddChildComponent("Container_Models/Container_Model_Pet", ActorModelComponent)
    end
    
    local scale = PartnerDataHelper.GetPetScale(self._rankContentListData[idx + 1]:GetPetGrade())
    local y = PartnerDataHelper.GetPetRotate(self._rankContentListData[idx + 1]:GetPetGrade())
    self._petModelComponent:SetStartSetting(Vector3(0, 0, -150), Vector3(0, y, 0), scale)
    local modelId = PartnerDataHelper.GetPetModel(self._rankContentListData[idx + 1]:GetPetGrade())
    self._petModelComponent:LoadModel(modelId)
    self._petModelComponent:Show()
end

function RankPanel:UpdateRideModel(idx)
    if self._equipModelComponent then
        self._equipModelComponent:Hide()
    end

    if self._playerModelComponent then
        self._playerModelComponent:Hide()
    end

        
    if self._petModelComponent then
        self._petModelComponent:Hide()
    end

    if self._rideModelComponent then
        --return
    else
        self._containerRideModel = self:FindChildGO("Container_Models/Container_Model_Ride")
        self._rideModelComponent = self:AddChildComponent("Container_Models/Container_Model_Ride", ActorModelComponent)
    end
    
    local scale = PartnerDataHelper.GetRideScale(self._rankContentListData[idx + 1]:GetHorseGrade())
    self._rideModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 90, 0), scale)
    local modelId = PartnerDataHelper.GetRideModel(self._rankContentListData[idx + 1]:GetHorseGrade())
    self._rideModelComponent:LoadModel(modelId)
    self._rideModelComponent:Show()

end


function RankPanel:UpdateModel(treasureType)
    if self._playerModelComponent then
        self._playerModelComponent:Hide()
    end

    if self._rideModelComponent then
        self._rideModelComponent:Hide()
    end

    if self._petModelComponent then
        self._petModelComponent:Hide()
    end


    if self._equipModelComponent then
        --return
    else
        self._containerModel = self:FindChildGO("Container_Models/Container_Model_Equip")
        self._equipModelComponent = self:AddChildComponent("Container_Models/Container_Model_Equip", EquipModelComponent)
    end

    local vocationGroup = math.floor(GameWorld.Player().vocation/100)
    local uiModelCfgId = GlobalParamsHelper.GetParamValue(703+treasureType)[vocationGroup]
    local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
    
    local modelId = modelInfo.model
    local posx = modelInfo.pos[1]
    local posy = modelInfo.pos[2]
    local posz = modelInfo.pos[3]
    local rotationx = modelInfo.rotation[1]
    local rotationy = modelInfo.rotation[2]
    local rotationz = modelInfo.rotation[3]
    local scale = modelInfo.scale
    self._equipModelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
    
    --神兵自转
    if treasureType == public_config.TREASURE_TYPE_WEAPON then
        --self._modelComponent:SetSelfRotate(true)
    end

    if treasureType == public_config.TREASURE_TYPE_TALISMAN then
        --法宝替换controller
        local controllerPath = GlobalParamsHelper.GetParamValue(692)
        self._equipModelComponent:LoadEquipModel(modelId, controllerPath)
    else
        self._equipModelComponent:LoadEquipModel(modelId, "")
    end

    self._equipModelComponent:Show()
end



function RankPanel:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_List/ScrollViewList")
	self._list = list
	self._listlView = scrollView 
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(true)
    self._list:SetItemType(RankListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._listData = RankExManager:GetRankList()
    self._list:SetDataList(self._listData)

    local itemClickedCB = function(idx)
		self:OnListItemClicked(idx)
	end
	self._list:SetItemClickedCB(itemClickedCB)
end

function RankPanel:OnListItemClicked(idx)
    if self._selectIndex == idx then return end
    if self._selectIndex ~= nil then
        self._list:GetItem(self._selectIndex):SetSelect(false)
    end
    self._selectIndex = idx
    self._list:GetItem(self._selectIndex):SetSelect(true)

    local rankType = self._listData[self._selectIndex + 1].rankType
    RankExManager:GetRank(rankType)
    if self._selectContentIndex ~= nil then
        self._rankContentList:GetItem(self._selectContentIndex):SetSelect(false)
        self._selectContentIndex = nil
    end
end

function RankPanel:CloseBtnClick()
	GUIManager.ClosePanel(PanelsConfig.Rank)
end

function RankPanel:OnShow(data)
    self:AddEventListeners()
    self._list:OnItemClicked(self._list:GetItem(0))
    self._list:SetToZero()
end

function RankPanel:OnClose()
    self._list:GetItem(self._selectIndex):SetSelect(false)
    if self._selectContentIndex ~= nil then
        self._rankContentList:GetItem(self._selectContentIndex):SetSelect(false)
    end
    self._selectContentIndex = nil
    self._selectIndex = nil
    self:RemoveEventListeners()
end

function RankPanel:GetRankListResp()
    self._rankContentListData = RankExManager:GetRankContentListData()
    local myRank = RankExManager:GetMyRank()
    if myRank == nil then
        self._rankOutTextGo:SetActive(true)
        self._rankText.gameObject:SetActive(false)
    else
        self._rankOutTextGo:SetActive(false)
        self._rankText.gameObject:SetActive(true)
        self._rankText.text = LanguageDataHelper.CreateContent(56217, {["0"] = myRank})
    end
    self._rankContentList:SetDataList(self._rankContentListData)
    if not table.isEmpty(self._rankContentListData) then
        self._rankContentList:OnItemClicked(self._rankContentList:GetItem(0))
        self._rankContentList:SetPositionByNum(0)
    else

    end
    
    -- self._playerModelComponent:LoadAvatarModel(self._rankContentListData[1]:GetVocation(), self._rankContentListData[1]:GetFacade(), true)
    -- LoggerHelper.Error("self._playerModelComponent:LoadAvatarModel(1, self._rankContentListData[1]:GetFacade(), true) ===" .. tostring(self._rankContentListData[1]:GetFacade()))
end

function RankPanel:WinBGType()
    return PanelWinBGType.FullSceenBG
end

function RankPanel:OnLookPlayer()
    local data =  self._rankContentListData[self._selectContentIndex + 1]
    -- CheckOtherPlayerManager:RequestCheckPlayer(data:GetName())
    --显示玩家TIPS信息
    local d = {}
    d.playerName = data:GetName()
    d.dbid = BaseUtil.GetDBIdFromCrossUUId(data:GetUUID())
    -- d.teamInfo = true
    d.rawListPos = Vector2(544,349)
	GUIManager.ShowPanel(PanelsConfig.PlayerInfoMenu, d)


    -- local d = {}
    -- d.playerName = data:GetName()
    -- d.dbid =  data:GetUUID()
    -- -- d.listPos = posVec
    -- GUIManager.ShowPanel(PanelsConfig.PlayerInfoMenu, d)
end

function RankPanel:BaseShowModel()
    if self._selectIndex then
        if self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_PET then
            self._petModelComponent:Show()
        elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_HORSE then
            self._rideModelComponent:Show()
        elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_WING then --翅膀
            self._equipModelComponent:Show()
        elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_TALISMAN then  --法宝
            self._equipModelComponent:Show()
        elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_WEAPON then --神兵
            self._equipModelComponent:Show()
        else
            self._playerModelComponent:Show()
        end
    end
end

function RankPanel:BaseHideModel()
    if self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_PET then
        self._petModelComponent:Hide()
    elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_HORSE then
        self._rideModelComponent:Hide()
    elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_WING then --翅膀
        self._equipModelComponent:Hide()
    elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_TALISMAN then  --法宝
        self._equipModelComponent:Hide()
    elseif self._listData[self._selectIndex + 1].rankType == public_config.RANK_TYPE_WEAPON then --神兵
        self._equipModelComponent:Hide()
    else
        self._playerModelComponent:Hide()
    end
end