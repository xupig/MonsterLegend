-- RankListItem.lua
local RankListItem = Class.RankListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RankListItem.interface = GameConfig.ComponentsConfig.Component
RankListItem.classPath = "Modules.ModuleRank.ChildComponent.RankListItem"

local UIParticle = ClassTypes.UIParticle

function RankListItem:Awake()
    self._unSelectContainer = self:FindChildGO("Container_UnSelect")
    self._selectContainer = self:FindChildGO("Container_Select")

    self._unSelectNameText = self:GetChildComponent("Container_UnSelect/Text_Name",'TextMeshWrapper')
    self._selectNameText = self:GetChildComponent("Container_Select/Text_Name",'TextMeshWrapper')

    local containerFx = self:FindChildGO("Container_Select/Container_Fx")
    UIParticle.AddParticle(containerFx, "fx_ui_30019")
    self:SetSelect(false)
end

function RankListItem:OnDestroy() 

end

--override
function RankListItem:OnRefreshData(data)
    -- if self:GetIndex() == 0 then
    --     self:SetSelect(true)
    -- else
    --     self:SetSelect(false)
    -- end
    self._unSelectNameText.text = data.name
    self._selectNameText.text = data.name
end

function RankListItem:SetSelect(flag)
    self._selectContainer:SetActive(flag)
    self._unSelectContainer:SetActive(not flag)
end
