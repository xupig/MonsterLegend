-- RankContentListItem.lua
local RankContentListItem = Class.RankContentListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RankContentListItem.interface = GameConfig.ComponentsConfig.Component
RankContentListItem.classPath = "Modules.ModuleRank.ChildComponent.RankContentListItem"

local NobilityDataHelper = GameDataHelper.NobilityDataHelper

function RankContentListItem:Awake()
    self._nameText = self:GetChildComponent("Container_content/Container_Content/Text_Name",'TextMeshWrapper')
    self._vipText = self:GetChildComponent("Container_content/Container_Content/Text_Vip",'TextMeshWrapper')
    self._numText = self:GetChildComponent("Container_content/Container_Rank/Text_Num",'TextMeshWrapper')
    self._rank1Image = self:FindChildGO("Container_content/Container_Rank/Image_Rank_1")
    self._rank2Image = self:FindChildGO("Container_content/Container_Rank/Image_Rank_2")
    self._rank3Image = self:FindChildGO("Container_content/Container_Rank/Image_Rank_3")
    self._rankBgImage = self:FindChildGO("Container_content/Container_Rank/Image_Rank_Bg2")

    self._selectImage = self:FindChildGO("Container_content/Image_Select")

    self._name1Text = self:GetChildComponent("Container_content/Container_Content/Container_Info/Text_Key_1",'TextMeshWrapper')
    self._value1Text = self:GetChildComponent("Container_content/Container_Content/Container_Info/Text_Value_1",'TextMeshWrapper')
    self._name2Text = self:GetChildComponent("Container_content/Container_Content/Container_Info/Text_Key_2",'TextMeshWrapper')
    self._value2Text = self:GetChildComponent("Container_content/Container_Content/Container_Info/Text_Value_2",'TextMeshWrapper')

    self._nobilityContainer = self:FindChildGO("Container_content/Container_Content/Container_Nobility")
end

function RankContentListItem:OnDestroy() 

end

--override
function RankContentListItem:OnRefreshData(data)
    local rank = self:GetIndex() + 1
    if rank == 1 then
        self._rank1Image:SetActive(true)
        self._rank2Image:SetActive(false)
        self._rank3Image:SetActive(false)
        self._rankBgImage:SetActive(false)
    elseif rank == 2 then        
        self._rank1Image:SetActive(false)
        self._rank2Image:SetActive(true)
        self._rank3Image:SetActive(false)
        self._rankBgImage:SetActive(false)
    elseif rank == 3 then
        self._rank1Image:SetActive(false)
        self._rank2Image:SetActive(false)
        self._rank3Image:SetActive(true)
        self._rankBgImage:SetActive(false)
    else
        self._rank1Image:SetActive(false)
        self._rank2Image:SetActive(false)
        self._rank3Image:SetActive(false)
        self._rankBgImage:SetActive(true)
        self._numText.text = rank
    end

    self._name1Text.text = data:GetFirstKey()
    self._value1Text.text = data:GetFirstValue()
    self._name2Text.text = data:GetSecondKey()
    self._value2Text.text = data:GetSecondValue()

    self._vipText.text = data:GetVipStr()

    
    -- if self:GetIndex() == 0 then
    --     self._selectImage:SetActive(true)
    -- else
    --     self._selectImage:SetActive(false)
    -- end
    self._nameText.text = data:GetName()
    GameWorld.AddIcon(self._nobilityContainer, NobilityDataHelper:GetNobilityShow(data:GeNobility()))
end

function RankContentListItem:SetSelect(flag)
    self._selectImage:SetActive(flag)
end
