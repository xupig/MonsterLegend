-- OpenRingModule.lua
OpenRingModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function OpenRingModule.Init()
    GUIManager.AddPanel(PanelsConfig.OpenRing, "Panel_OpenRing", GUILayer.LayerUIPanel, "Modules.ModuleOpenRing.OpenRingPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)    
end

return OpenRingModule