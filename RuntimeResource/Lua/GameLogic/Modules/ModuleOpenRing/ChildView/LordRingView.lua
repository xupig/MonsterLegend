local LordRingView = Class.LordRingView(ClassTypes.BaseComponent)

LordRingView.interface = GameConfig.ComponentsConfig.Component
LordRingView.classPath = "Modules.ModuleOpenRing.ChildView.LordRingView"
require "Modules.ModuleOpenRing.ChildComponent.KillBossItem"
require "Modules.ModuleOpenRing.ChildComponent.LordRingItem"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local KillBossItem = ClassTypes.KillBossItem
local LordRingItem = ClassTypes.LordRingItem
local LordRingTotalDataHelper = GameDataHelper.LordRingTotalDataHelper
local LordRingDetailDataHelper = GameDataHelper.LordRingDetailDataHelper
local ServiceActivityConfig = GameConfig.ServiceActivityConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local StringStyleUtil = GameUtil.StringStyleUtil
local XImageFlowLight = GameMain.XImageFlowLight
local UIParticle = ClassTypes.UIParticle

function LordRingView:Awake()
    --self._txtLordName = self:GetChildComponent("Container_Right/Text_Title", 'TextMeshWrapper')
    self._txtLordDesc = self:GetChildComponent("Container_Right/Text_Effect_Desc", 'TextMeshWrapper')
    self._txtLordPro = self:GetChildComponent("Container_Right/Text_Progress", 'TextMeshWrapper')
    self._icon = self:FindChildGO("Container_Right/Container_Ring")
    self._alreadyGo = self:FindChildGO("Container_Right/Image_Already_Reward")

    self._imgRing1 = self:FindChildGO("Container_Right/Image_1")
    self._imgRing2 = self:FindChildGO("Container_Right/Image_2")
    self._imgRing3 = self:FindChildGO("Container_Right/Image_3")
    self._imgRing4 = self:FindChildGO("Container_Right/Image_4")
    self._imgRing5 = self:FindChildGO("Container_Right/Image_5")

    self._goButtonFlowLight = self:AddChildComponent("Container_Right/Button_Receive", XImageFlowLight)
    self._receiveButton = self:FindChildGO("Container_Right/Button_Receive")
    self._csBH:AddClick(self._receiveButton, function()self:OnReceiveRewardClick() end)

    self._imgRed = self:FindChildGO("Container_Right/Button_Receive/Image_Red")
    
    
    self._btnAnnounceRewardGetWrapper = self:GetChildComponent("Container_Right/Button_Receive", "ButtonWrapper")

    self._fx_Container = self:FindChildGO("Container_Right/Container_Fx")
    self._fx_ui_30036 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30036")
    self:StopFx()

    --self._btnAnnounceRewardGetWrapper.interactable = false
    self:SetButtonActive(false)
    self._alreadyGo:SetActive(false)
    self:InitView()
    self._refreshListView = function()self:RefreshListData() end
    self._refreshRingProgress = function()self:RefreshRingProgress() end
end

function LordRingView:OnDestroy()
    self._fx_ui_30036:Stop()
end

function LordRingView:PlayFx()
    self._fx_ui_30036:Play(true, true)
end

function LordRingView:StopFx()
    self._fx_ui_30036:Stop()
end

function LordRingView:SetButtonActive(flag)
    self._btnAnnounceRewardGetWrapper.interactable = flag 
    self._goButtonFlowLight.enabled = flag
end

function LordRingView:ShowView()
    self:AddEventListeners()
    self:PlayFx()

    if self._wearlistlView then
        self._wearlistlView:SetScrollRectState(true)
    end

    if self._killlistlView then
        self._killlistlView:SetScrollRectState(true)
    end
    
end

function LordRingView:CloseView()
    self:RemoveEventListeners()
    self:StopFx()

    if self._wearlistlView then
        self._wearlistlView:SetScrollRectState(false)
    end

    if self._killlistlView then
        self._killlistlView:SetScrollRectState(false)
    end
end

function LordRingView:OnReceiveRewardClick()
    if ServiceActivityData:GetCurOpenType() ~= 5 then
        ServiceActivityManager:SendUnlockRingReq()
    else
        ServiceActivityManager:SendUnlockRingReq(5)
    end
end

function LordRingView:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.GetRingProgressResp, self._refreshRingProgress)
    EventDispatcher:AddEventListener(GameEvents.GetRingTargetRewardResp, self._refreshListView)
    EventDispatcher:AddEventListener(GameEvents.GetRingUnlockResp, self._refreshListView)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RING_UNLOCK_STEP, self._refreshListView)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEARN_PASSIVE_SPELL, self._refreshListView)
end

function LordRingView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.GetRingProgressResp, self._refreshRingProgress)
    EventDispatcher:RemoveEventListener(GameEvents.GetRingTargetRewardResp, self._refreshListView)
    EventDispatcher:RemoveEventListener(GameEvents.GetRingUnlockResp, self._refreshListView)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RING_UNLOCK_STEP, self._refreshListView)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEARN_PASSIVE_SPELL, self._refreshListView)
end


function LordRingView:RefreshRingProgress()
    self:RefreshListData()
    --EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleOpenRing.ChildView.LordRingView")
end

function LordRingView:RefreshListData()
    
    local needIds = {}
    local ids = LordRingDetailDataHelper:GetAllId()
    for i, v in ipairs(ids) do
        local type = LordRingDetailDataHelper:GetType(v)
        if type ~= 3 then
            if type == ServiceActivityData:GetCurOpenType() then
                table.insert(needIds, v)
            end
        end
    end
    self._needIds = needIds
    local progressTb = ServiceActivityData:GetProgressTable()
    local num = 0
    for i, needId in ipairs(needIds) do
        for k, data in pairs(progressTb) do
            if needId == k then
                if data[2] == 3 then
                    num = num + 1
                end
            end
        end
    end
    
    
    local argsTable = LanguageDataHelper.GetArgsTable()
    if num ~= #needIds then
        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(num .. "/" .. #needIds, StringStyleUtil.red)
    else
        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(num .. "/" .. #needIds, StringStyleUtil.green)
    end
    
    self._txtLordPro.text = LanguageDataHelper.CreateContent(76938, argsTable)
    
    if num == #needIds then
        self:SetButtonActive(true)
        --self._btnAnnounceRewardGetWrapper.interactable = true
    else
        self:SetButtonActive(false)
        --self._btnAnnounceRewardGetWrapper.interactable = false
    end
    
    if ServiceActivityData:GetCurOpenType() ~= 5 then
        if ServiceActivityData:GetCurOpenType() < ServiceActivityManager:GetRingUnlockStep() then
            self._receiveButton.gameObject:SetActive(false)
            self._imgRed:SetActive(false)
            self._alreadyGo:SetActive(true)
        elseif ServiceActivityData:GetCurOpenType() == ServiceActivityManager:GetRingUnlockStep() then
            self._receiveButton.gameObject:SetActive(true)
            self._imgRed:SetActive(true and self._btnAnnounceRewardGetWrapper.interactable)
            self._alreadyGo:SetActive(false)
        end
    else
        local learnPassive = ServiceActivityManager:GetLearnedPassive()
        if learnPassive[15003] ~= nil then
            self._receiveButton.gameObject:SetActive(false)
            self._imgRed:SetActive(false)
            self._alreadyGo:SetActive(true)
        else
            self._receiveButton.gameObject:SetActive(true)
            
            self._imgRed:SetActive(true and self._btnAnnounceRewardGetWrapper.interactable)
            self._alreadyGo:SetActive(false)
        end
    end
    
    self._wearlistlView.gameObject:SetActive(true)


    local progressTb = ServiceActivityData:GetProgressTable()
    --LoggerHelper.Error("progressTb"..PrintTable:TableToStr(progressTb))
    local alreadyId = {}
    local noGetId = {}
    local canGetId = {}
    for i,id in ipairs(needIds) do
        if progressTb[id] then
            if progressTb[id][2] == 2 then
                table.insert(canGetId,id)
            elseif progressTb[id][2] == 3 then
                table.insert(alreadyId,id)
            else
                table.insert(noGetId,id)
            end
        else
            table.insert(noGetId,id)
        end
    end

    local ordIds = {}
    for i,id in ipairs(canGetId) do
        table.insert(ordIds,id)
    end
    --LoggerHelper.Error("11ordIds"..PrintTable:TableToStr(ordIds))
    for i,id in ipairs(noGetId) do
        table.insert(ordIds,id)
    end
    --LoggerHelper.Error("22ordIds"..PrintTable:TableToStr(ordIds))
    for i,id in ipairs(alreadyId) do
        table.insert(ordIds,id)
    end
    --LoggerHelper.Error("33ordIds"..PrintTable:TableToStr(ordIds))
    self._wearList:SetDataList(ordIds)
    self._killlistlView.gameObject:SetActive(false)

    ServiceActivityManager:RefreshRedPointForRing()
end



function LordRingView:InitData(secIndex)

    ServiceActivityManager:SendRingGetInfoReq()
    --LoggerHelper.Log("LordRingTotalDataHelper:GetName()========== .." .. LordRingTotalDataHelper:GetName(secIndex))
    --self._txtLordName.text = LanguageDataHelper.CreateContent(LordRingTotalDataHelper:GetName(secIndex))




    self._txtLordDesc.text = LanguageDataHelper.CreateContent(LordRingTotalDataHelper:GetDes(secIndex))
    self:SetTextType(secIndex)
    GameWorld.AddIcon(self._icon, LordRingTotalDataHelper:GetIcon(secIndex))
    self:RefreshListData()
end

function LordRingView:SetTextType(secIndex)
    self._imgRing1:SetActive(secIndex==1)
    self._imgRing2:SetActive(secIndex==2)
    self._imgRing3:SetActive(secIndex==3)
    self._imgRing4:SetActive(secIndex==4)
    self._imgRing5:SetActive(secIndex==5)
end


function LordRingView:SetSuitType(secIndex)
    
    self:InitData(secIndex)
end




function LordRingView:OnToggleGroupClick()

end


function LordRingView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._killList = list
    self._killlistlView = scrollView
    self._killlistlView:SetHorizontalMove(false)
    self._killlistlView:SetVerticalMove(true)
    self._killList:SetItemType(KillBossItem)
    self._killList:SetPadding(0, 0, 0, 0)
    self._killList:SetGap(10, 0)
    self._killList:SetDirection(UIList.DirectionTopToDown, 1, -1)
    
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewRing")
    self._wearList = list
    self._wearlistlView = scrollView
    self._wearlistlView:SetHorizontalMove(false)
    self._wearlistlView:SetVerticalMove(true)
    self._wearList:SetItemType(LordRingItem)
    self._wearList:SetPadding(0, 0, 0, 0)
    self._wearList:SetGap(10, 0)
    self._wearList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end
