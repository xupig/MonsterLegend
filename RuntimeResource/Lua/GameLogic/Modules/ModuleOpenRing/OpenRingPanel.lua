-- OpenRingPanel.lua
local OpenRingPanel = Class.OpenRingPanel(ClassTypes.BasePanel)

require "Modules.ModuleOpenRing.ChildView.LordRingView"

local LordRingView = ClassTypes.LordRingView
local DateTimeUtil = GameUtil.DateTimeUtil
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local ServiceActivityType = GameConfig.EnumType.ServiceActivityType
local WelfareRankWayDataHelper = GameDataHelper.WelfareRankWayDataHelper
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local OpenRankType = GameConfig.EnumType.OpenRankType
local public_config = GameWorld.public_config


function OpenRingPanel:Awake()
    self:InitComps()
    self:InitCallBack()
    self._onRefreshTab = function()self:RefreshTab() end
end


function OpenRingPanel:InitText()
end

--初始化回调
function OpenRingPanel:InitCallBack()
    -- self:SetTabClickAfterUpdateSecTabCB(function(index)
    --     self:SelectTab(index)
    -- end)
    self:SetSecondTabClickCallBack(function(index)
        self:SelectSecTab(index)
    end)

    self:SetCustomizeFunctionOpenTextCB(function(index,secIndex)
        self:OpenTips(index,secIndex)
    end)
end



function OpenRingPanel:SelectSecTab(secIndex)
    local typeRing = secIndex
    if secIndex == 3 or secIndex == 4 then
         typeRing = secIndex+1
    end

    ServiceActivityData:SetCurOpenType(typeRing)
    self._selectedView:SetSuitType(typeRing)
    for i = 1, 4 do
        if i>ServiceActivityManager:GetRingUnlockStep() and i~=1 and i~=4 then  
        --self._secTabs[i]:SetName("???")
        self:SetSecTabFunctionActive(i, false)
        else
        self:SetSecTabFunctionActive(i, true)
        end
    end
end

function OpenRingPanel:OpenTips(index,secIndex)   
        if secIndex == 2 then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76977))
        end

        if secIndex == 3 then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76978))
        end
end

function OpenRingPanel:InitComps()
    
    self:InitView("Container_LordRings", LordRingView, 1, 1)
    self:InitView("Container_LordRings", LordRingView, 1, 2)
    self:InitView("Container_LordRings", LordRingView, 1, 3)
    self:InitView("Container_LordRings", LordRingView, 1, 4)

--self:SetSecondTabClickCallBack(self._secTabCb)
end



function OpenRingPanel:OnShow(data)
    --LoggerHelper.Error(" OpenRingPanel:OnShow(data)")
    self:RightBgIsActive(false)
    self._levelBg = self:FindChildGO("Container_LordRings/Container_Right/Image_Bg")
    GameWorld.AddIcon(self._levelBg,13635)
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
    EventDispatcher:AddEventListener(GameEvents.GetRingUnlockResp, self._onRefreshTab)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RING_UNLOCK_STEP, self._onRefreshTab) 
end

function OpenRingPanel:OnClose()
    EventDispatcher:RemoveEventListener(GameEvents.GetRingUnlockResp, self._onRefreshTab)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RING_UNLOCK_STEP, self._onRefreshTab)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_service_activity_close_button")
end

function OpenRingPanel:RefreshTab()
        for i = 1, 4 do
            if i>ServiceActivityManager:GetRingUnlockStep() and i~=1 and i~=4 then  
                --self._secTabs[i]:SetName("???")
                self:SetSecTabFunctionActive(i, false)
            else
                self._ringTitle = GlobalParamsHelper.GetParamValue(823)
                -- if  i == 3 or i == 4 then
                --     self._secTabs[i]:SetName(LanguageDataHelper.CreateContent(self._ringTitle[i+1]))
                -- else
                --     self._secTabs[i]:SetName(LanguageDataHelper.CreateContent(self._ringTitle[i]))
                -- end
                --self._secTabs[i]:SetName(LanguageDataHelper.CreateContent(self._ringTitle[i]))
                self:SetSecTabFunctionActive(i, true)
            end
        end
    
end



function OpenRingPanel:StructingViewInit()
    return true
end



function OpenRingPanel:WinBGType()
    return PanelWinBGType.RedNewBG
end
