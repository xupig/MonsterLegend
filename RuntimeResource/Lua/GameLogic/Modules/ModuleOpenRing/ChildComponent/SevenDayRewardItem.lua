-- SevenDayRewardItem.lua
local SevenDayRewardItem = Class.SevenDayRewardItem(ClassTypes.BaseComponent)

SevenDayRewardItem.interface = GameConfig.ComponentsConfig.PointableComponent
SevenDayRewardItem.classPath = "Modules.ModuleActivity.ChildComponent.SevenDayRewardItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

local RectTransform = UnityEngine.RectTransform
local Welfare7dayRewardDataHelper = GameDataHelper.Welfare7dayRewardDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local SevenDayLoginManager = PlayerManager.SevenDayLoginManager
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local stop = Vector3.New(0, 0, 360)
local start = Vector3.New(0, 0, 0)
local UIParticle = ClassTypes.UIParticle

--override
function SevenDayRewardItem:Awake()
    self._canGetImage = self:FindChildGO("Image_Can_Get")
    self._alreadyGetContainer = self:FindChildGO("Container_Already_Get")
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._icon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)  
    self._dayContainer = self:FindChildGO("Container_Day")
    self._guangxiaoImage = self:FindChildGO("Image_Guangxiao")
    self._autoFightLightRotation = self._guangxiaoImage:AddComponent(typeof(XGameObjectTweenRotation))

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_BigCircleButton")
    self._fx:Stop()
    -- self._icon:ActivateTipsById()
    self._weekDay = 7
end

--override
function SevenDayRewardItem:OnDestroy()
end


--override
function SevenDayRewardItem:OnPointerDown(pointerEventData)
end

--override
function SevenDayRewardItem:OnPointerUp(pointerEventData)
    if self._puFunc == nil then return end
    self._puFunc(self._id)
end

function SevenDayRewardItem:SetPointerUpCB(func)
    self._puFunc = func
end

function SevenDayRewardItem:InitData(id, length)
    local iconsData = Welfare7dayRewardDataHelper:GetIcons(id)
    GameWorld.AddIcon(self._itemContainer, iconsData[4])
    local icons = Welfare7dayRewardDataHelper:GetIcons(id)
    -- local group = GameWorld.Player():GetVocationGroup()
    -- local rewardDatas = Welfare7dayRewardDataHelper:GetRewardByVocationGroup(group, id)
    -- if rewardDatas ~= nil and (not table.isEmpty(rewardDatas)) then
    --     local itemId = rewardDatas[1].id
    --     -- self._nameText.text = ItemDataHelper.GetItemName(itemId)
    --     self._icon:SetValues({
    --         ["noFrame"] = true,
    --         ["noAnimFrame"] = true
    --     })
    --     self._icon:SetItem(itemId)
    -- end
    self._id = id     
    GameWorld.AddIcon(self._dayContainer, icons[1])

    local index = id
    while(index > self._weekDay) do
        index = index - self._weekDay
    end
    self.gameObject:GetComponent(typeof(RectTransform)).transform.localPosition = Vector3.New(length * (index-1)/(self._weekDay-1), 0, 0)
    self:UpdateData()
end

function SevenDayRewardItem:UpdateData()
    local get_reward_day = GameWorld.Player().get_reward_day 
    local canGetNum = SevenDayLoginManager:GetCanGetRewardNum()
    local get_reward_num = SevenDayLoginManager:GetRewardDays()
    local isCanGet = SevenDayLoginManager:IsCanGetReward()
    local week = math.ceil(get_reward_num / self._weekDay) - 1
    local login_days = GameWorld.Player().login_days
    if get_reward_num ~=0 and get_reward_num % self._weekDay == 0 then
        week = week + 1
    end
    local index = 0
    if week > 0 then
        index = index + self._weekDay * week
    end

    if get_reward_day[self._id] then 
        self._alreadyGetContainer:SetActive(true)
        -- self._guangxiaoImage:SetActive(false)
        self._fx:Stop()
    else
        self._alreadyGetContainer:SetActive(false)
        if self._id <= login_days  then
            self._fx:Play(true)
            -- self._guangxiaoImage:SetActive(true)
            -- self._autoFightLightRotation:SetFromToRotation(start, stop, 2, 2,nil)
        else
            -- self._guangxiaoImage:SetActive(false)
            self._fx:Stop()
        end
    end
end

