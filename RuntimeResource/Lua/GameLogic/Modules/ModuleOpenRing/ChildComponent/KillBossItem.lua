-- KillBossItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
require "Modules.ModuleActivity.ChildComponent.BossTypeItem"
local KillBossItem = Class.KillBossItem(ClassTypes.UIListItem)
KillBossItem.interface = GameConfig.ComponentsConfig.Component
KillBossItem.classPath = "Modules.ModuleActivity.ChildComponent.KillBossItem"

local BossTypeItem = ClassTypes.BossTypeItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local LordRingTotalDataHelper = GameDataHelper.LordRingTotalDataHelper
local LordRingDetailDataHelper = GameDataHelper.LordRingDetailDataHelper
local EventDataHelper = GameDataHelper.EventDataHelper
local public_config = GameWorld.public_config
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local WorldBossManager = PlayerManager.WorldBossManager
local BossHomeManager = PlayerManager.BossHomeManager

local BossHomeDataHelper = GameDataHelper.BossHomeDataHelper
local WorldBossDataHelper = GameDataHelper.WorldBossDataHelper
local UIParticle = ClassTypes.UIParticle
local UIList = ClassTypes.UIList
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local StringStyleUtil = GameUtil.StringStyleUtil
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local XImageFlowLight = GameMain.XImageFlowLight

function KillBossItem:Awake()
    self._txtKillDesc = self:GetChildComponent("Text_Kill_Desc", 'TextMeshWrapper')

    self._goButtonFlowLight = self:AddChildComponent("Container_State/Button_Get_Reward", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Container_State/Button_Get_Reward")
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)


    self._iconRewardContainer = self:FindChildGO("Container_Icon")
    self._iconReward = ItemManager():GetLuaUIComponent(nil, self._iconRewardContainer)

    self._notCompleteImgGo = self:FindChildGO("Container_State/Image_No_Complete")
    self._alreadyGetImgGo = self:FindChildGO("Container_State/Image_Already_Get")
    self._imgRed = self:FindChildGO("Container_State/Button_Get_Reward/Image_Red")
    

    self._getToKill = self:FindChildGO("Text_ToKill")
    self._getToKill = self:GetChildComponent("Text_ToKill", "TextMeshWrapper")
    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject, "Text_ToKill")
    self._linkTextMesh:SetOnClickCB(function(linkIndex, postion)self:OnGoKillBoss(linkIndex, postion) end)
    local killDesc = '<u><link="CLICK_LINK">' .. LanguageDataHelper.CreateContent(76937) .. '</link></u>'
    self._getToKill.text =  StringStyleUtil.GetColorStringWithTextMesh(killDesc,StringStyleUtil.green)
    self:InitView()

end

function KillBossItem:OnDestroy()

end

function KillBossItem:InitItem()

end

function KillBossItem:SetButtonActive(flag)
    self._getRewardButton:SetActive(flag)    
    self._goButtonFlowLight.enabled = flag
end

function KillBossItem:AddFriendClick()

end


function KillBossItem:OnGetRewardClick()
    if self._id then
        ServiceActivityManager:SendGetTargetRewardReq(self._id)
    end
end


function KillBossItem:OnGoKillBoss()
    GameManager.GUIManager.ClosePanel(PanelsConfig.Activity)
    local iconList = LordRingDetailDataHelper:GetTargets(self._id)
    --LoggerHelper.Log("KillBossItem:OnGoKillBoss()A========== .." ..PrintTable:TableToStr(iconList[1][1]))
    local bossTb = EventDataHelper:GetConds(iconList[1][1],public_config.EVENT_PARAM_MONSTER_ID)
    --LoggerHelper.Log("KillBossItem:OnGoKillBoss()========== .." ..PrintTable:TableToStr(bossTb))

    local otherTb = LordRingDetailDataHelper:GetOther(self._id)
    local bossFollowId = 0
    for i,bossItem in ipairs(self.targetsList) do
        if bossItem[2] == 0 then
            bossFollowId = i
            break
        end
    end

    for k,v in pairs(otherTb) do
        if k == 1 then           
            WorldBossManager:GotoBossById(v[bossFollowId])
        elseif k == 2 then
            BossHomeManager:GotoBossById(v[bossFollowId])
        end
    end

    if bossFollowId == 0 then 
        GUIManager.ShowPanelByFunctionId(401)
    end
    -- LoggerHelper.Log("KillBossItem:bossFollowId()========== .." ..bossFollowId)

    --
end


function KillBossItem:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._bossList = list
    self._bossView = scrollView
    self._bossView:SetHorizontalMove(false)
    self._bossView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._bossList:SetItemType(BossTypeItem)
    self._bossList:SetPadding(0, 0, 0, 0)
    self._bossList:SetGap(0, 0)
    self._bossList:SetDirection(UIList.DirectionLeftToRight, -1, 1)

    -- self._alreadyGetImgGo:SetActive(false)
    -- self._getRewardButton:SetActive(true)
    -- self._notCompleteImgGo:SetActive(false)
end



function KillBossItem:OnRefreshData(id)
    if id then
        self._id = id
        self._notCompleteImgGo:SetActive(true)
        self._alreadyGetImgGo:SetActive(false)
        self:SetButtonActive(false)
        --self._getRewardButton:SetActive(false)
        local targets = LordRingDetailDataHelper:GetTargets(id)
        local argsTable = LanguageDataHelper.GetArgsTable()
        if ServiceActivityData:GetCurOpenType()==1 then
            argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(targets[1][2], StringStyleUtil.green)
        else
            argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh("0".."/"..#targets, StringStyleUtil.red)
        end
        self._txtKillDesc.text = LanguageDataHelper.CreateContent(LordRingDetailDataHelper:GetName(id), argsTable)


        local reward = LordRingDetailDataHelper:GetReward(id)
        self._iconReward:SetItem(reward[1][1], reward[1][2], 0, true)
    
        local progressTb = ServiceActivityData:GetProgressTable()
        -- LoggerHelper.Log("LordRingItem:targets"..PrintTable:TableToStr(targets))
        -- LoggerHelper.Log("LordRingItem:GetProgressTable[1]"..PrintTable:TableToStr(progressTb))

        self.targetsList = LordRingDetailDataHelper:GetTargets(id)
        for i,bossItem in ipairs(self.targetsList) do
            bossItem[2] = 0
        end

        for i,bossItem in ipairs(self.targetsList) do
            bossItem[3] = 1
        end


        for k,progress in pairs(progressTb) do
            if k == id then
                if progress[2] == 1 then
                    self._notCompleteImgGo:SetActive(true)
                    self._alreadyGetImgGo:SetActive(false)
                    --self._getRewardButton:SetActive(false)
                    self:SetButtonActive(false)

                    local argsTable = LanguageDataHelper.GetArgsTable()
                    -- LoggerHelper.Log("LordRingItem:targets"..PrintTable:TableToStr(targets))

                    for i,bossItem in ipairs(self.targetsList) do
                        for k,num in pairs(progress[1]) do
                            if k == bossItem[1] then
                                bossItem[2] = num
                            end
                        end
                    end

                    local killNum = 0
                    for i,bossItem in ipairs(self.targetsList) do
                        if bossItem[2] == 1 then
                            killNum=killNum+1
                        end
                    end

                    local argsTable = LanguageDataHelper.GetArgsTable()
                    if ServiceActivityData:GetCurOpenType()==1 then
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(targets[1][2], StringStyleUtil.green)
                    else
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(killNum.."/"..#targets, StringStyleUtil.red)
                    end
                    self._txtKillDesc.text = LanguageDataHelper.CreateContent(LordRingDetailDataHelper:GetName(id), argsTable)


                elseif  progress[2] == 2 then
                    self._notCompleteImgGo:SetActive(false)
                    self._alreadyGetImgGo:SetActive(false)
                    --self._getRewardButton:SetActive(true)
                    self:SetButtonActive(true)
                    for i,bossItem in ipairs(self.targetsList) do
                        bossItem[2] = 1
                    end

                    local targets = LordRingDetailDataHelper:GetTargets(id)
                    local argsTable = LanguageDataHelper.GetArgsTable()
                    if ServiceActivityData:GetCurOpenType()==1 then
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(targets[1][2], StringStyleUtil.green)
                    else
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(#targets.."/"..#targets, StringStyleUtil.green)
                    end
                    self._txtKillDesc.text = LanguageDataHelper.CreateContent(LordRingDetailDataHelper:GetName(id), argsTable)
                elseif  progress[2] == 3 then     
                    self._notCompleteImgGo:SetActive(false)
                    self._alreadyGetImgGo:SetActive(true)
                    --self._getRewardButton:SetActive(false)
                    self:SetButtonActive(false)
                    
                    for i,bossItem in ipairs(self.targetsList) do
                        bossItem[2] = 1
                    end

                    local targets = LordRingDetailDataHelper:GetTargets(id)
                    local argsTable = LanguageDataHelper.GetArgsTable()
                    if ServiceActivityData:GetCurOpenType()==1 then
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(targets[1][2], StringStyleUtil.green)
                    else
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(#targets.."/"..#targets, StringStyleUtil.green)
                    end
                    self._txtKillDesc.text = LanguageDataHelper.CreateContent(LordRingDetailDataHelper:GetName(id), argsTable)
                end
            end
        end


        --local iconList = LordRingDetailDataHelper:GetIcon(id)
        
        self._bossList:SetDataList(self.targetsList)
    end
end
