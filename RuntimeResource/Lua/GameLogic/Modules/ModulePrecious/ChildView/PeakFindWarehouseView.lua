require "Modules.ModulePrecious.ChildComponent.EquipFindWarehouseItem"

local PeakFindWarehouseView = Class.PeakFindWarehouseView(ClassTypes.BaseLuaUIComponent)

PeakFindWarehouseView.interface = GameConfig.ComponentsConfig.Component
PeakFindWarehouseView.classPath = "Modules.ModulePrecious.ChildView.PeakFindWarehouseView"


local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local BagManager = PlayerManager.BagManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local EquipFindWarehouseItem = ClassTypes.EquipFindWarehouseItem
local UIList = ClassTypes.UIList
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemConfig = GameConfig.ItemConfig
local LotteryPeakManager = PlayerManager.LotteryPeakManager
local PanelsConfig = GameConfig.PanelsConfig

function PeakFindWarehouseView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._typeShow = false
    self._gradeShow = false
    self._selectedItemList = {}
    self:InitView()
    self:InitListenerFunc()
end

--override
function PeakFindWarehouseView:OnDestroy() 

end

function PeakFindWarehouseView:InitListenerFunc()
    self._updateBag = function (pkgType) self:UpdateData(pkgType) end
end

function PeakFindWarehouseView:OnEnable()
    self:AddEventListeners()
end

function PeakFindWarehouseView:OnDisable()
    self:RemoveEventListeners()
end

function PeakFindWarehouseView:ShowView()
    self:RefreshView()
    EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Fx, false)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModulePrecious.ChildView.PeakFindWarehouseView")
end

function PeakFindWarehouseView:CloseView()
    EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Fx, true)
end

function PeakFindWarehouseView:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE,self._updateBag)
end

function PeakFindWarehouseView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE,self._updateBag)
end

function PeakFindWarehouseView:InitView()
    self._btnSort = self:FindChildGO("Button_Sort")
    self._csBH:AddClick(self._btnSort,function () self:OnSortClick() end)

    self._btnGet = self:FindChildGO("Button_Get")
    self._csBH:AddClick(self._btnGet,function () self:OnGetClick() end)

    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose,function () self:OnCloseClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(EquipFindWarehouseItem)
    self._list:SetPadding(10, 0, 0, 16)
    self._list:SetGap(10, 12)
    self._list:SetDirection(UIList.DirectionLeftToRight, 8, 100)
end

function PeakFindWarehouseView:UpdateData(pkgType)
	if pkgType == public_config.PKG_TYPE_LOTTERY_EQUIP then
		self:RefreshView()
	end
end

function PeakFindWarehouseView:RefreshView()
    local t = bagData:GetPkg(public_config.PKG_TYPE_LOTTERY_EQUIP):GetItemInfos()
	self._bagData = {}
    self._curVolumn = GameWorld.Player().pkg_lottery_equip_size
	--填充空数据为了生成空格
	for i=1,self._curVolumn do
        if t[i] then
            self._bagData[i] = t[i]
        else
            self._bagData[i] = "nil"
        end
	end
    --self._list:SetDataList(self._bagData)
    --self._list:SetOnAllItemCreatedCB(function() self:OnContentListAllItemCreated() end)
	self._list:SetDataList(self._bagData, 5)--使用协程加载
end

--分帧加载完List后回调
-- function PeakFindWarehouseView:OnContentListAllItemCreated()
-- end

function PeakFindWarehouseView:OnGetClick()
    if GameManager.DramaManager:IsRunning("drama_guide", "__EquipTreasure") then
        self:OnCloseClick()
        GUIManager.ClosePanel(PanelsConfig.Precious)
    end
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_equip_find_warehouse_get_button")
    LotteryPeakManager:LotteryEquipTakeOutAllReq()
end

function PeakFindWarehouseView:OnSortClick()
    BagManager:RequestTiny(public_config.PKG_TYPE_LOTTERY_EQUIP)
end

function PeakFindWarehouseView:OnCloseClick()
    self:CloseView()
    self.gameObject:SetActive(false)
end