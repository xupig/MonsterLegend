-- RunePreView.lua
require "Modules.ModulePrecious.ChildComponent.PreciousRunePreListItem1"
local RunePreView = Class.RunePreView(ClassTypes.BaseComponent)
RunePreView.interface = GameConfig.ComponentsConfig.Component
RunePreView.classPath = "Modules.ModulePrecious.ChildView.RunePreView"
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local PreciousData = PlayerManager.PlayerDataManager.preciousData
local PreciousManager = PlayerManager.PreciousManager
local public_config = GameWorld.public_config
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local XArtNumber = GameMain.XArtNumber
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local PreciousRunePreListItem1 = ClassTypes.PreciousRunePreListItem1

function RunePreView:Awake()
    self._csBH = self:GetComponent("LuaUIComponent")
    self:InitView()
end

    
function RunePreView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Open")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(PreciousRunePreListItem1)
    self._list:SetPadding(0, 0, 0, 50)
    self._list:SetGap(0, 30)
    self._list:SetDirection(UIList.DirectionLeftToRight, 4, -1)
    local itemClickedCB =                
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._btnAll = self:FindChildGO("Button_Close") 
    self._csBH:AddClick(self._btnAll, function()self:BtnCloseClick() end)

    local title = self:GetChildComponent("Text_Title","TextMeshWrapper")
    local content = self:GetChildComponent("Text_Content","TextMeshWrapper")
    local tips = self:GetChildComponent("Text_Tips","TextMeshWrapper")
    title.text = LanguageDataHelper.GetContent(76203)
    content.text = LanguageDataHelper.GetContent(76213)
    tips.text = LanguageDataHelper.GetContent(76214)

end

function RunePreView:BtnCloseClick()
    self.gameObject:SetActive(false)
end


function RunePreView:OnDestroy()
    self._csBH = nil
end

function RunePreView:Update(data)
  
end

function RunePreView:Timer()
   
end

function RunePreView:OnClose()
 
end

function RunePreView:OnShow()
    local d = PreciousData:GetRunePre()
    self._list:SetDataList(d)
end


