-- RuneBagView.lua
require "Modules.ModulePrecious.ChildComponent.PreciousRuneBagListItem"
local RuneBagView = Class.RuneBagView(ClassTypes.BaseComponent)
RuneBagView.interface = GameConfig.ComponentsConfig.Component
RuneBagView.classPath = "Modules.ModulePrecious.ChildView.RuneBagView"
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local PreciousData = PlayerManager.PlayerDataManager.preciousData
local PreciousManager = PlayerManager.PreciousManager
local public_config = GameWorld.public_config
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local XArtNumber = GameMain.XArtNumber
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local PreciousRuneBagListItem = ClassTypes.PreciousRuneBagListItem
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function RuneBagView:Awake()
    self._timerid = -1
    self._csBH = self:GetComponent("LuaUIComponent")
    self:InitView()
end

function RuneBagView:UpdateCtril()
 
end
    
function RuneBagView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(PreciousRuneBagListItem)
    self._list:SetPadding(0, 0, 0, 5)
    self._list:SetGap(0, 6)
    self._list:SetDirection(UIList.DirectionLeftToRight, 2, -1)
    local itemClickedCB =                
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._btnAll = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnAll, function()self:BtnCloseClick() end)

    local title = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    title.text = LanguageDataHelper.GetContent(76202)
    self._bagText = self:GetChildComponent("Text_Bag","TextMeshWrapper")
end
function RuneBagView:OnListItemClicked(idx)

end
function RuneBagView:BtnCloseClick()
    self.gameObject:SetActive(false)        
end


function RuneBagView:OnDestroy()
    self._csBH = nil
end

function RuneBagView:Update(data)
  
end

function RuneBagView:Timer()
   
end

function RuneBagView:OnClose()
 
end

function RuneBagView:OnShow()
    PreciousManager:HandlePkg()
    local d = PreciousData:GetRunePkg()
    --self._list:SetOnAllItemCreatedCB(function() self:OnContentListAllItemCreated() end)
    self._list:SetDataList(d, 5)--使用协程加载
    
    --self._list:SetDataList(d)
    self._bagText.text = LanguageDataHelper.GetContent(76209)..' '..BaseUtil.GetColorString(#d..'/'..GlobalParamsHelper.GetParamValue(478),ColorConfig.A)
end

-- function RuneBagView:OnContentListAllItemCreated()
-- end


