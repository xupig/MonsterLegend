require "Modules.ModulePrecious.ChildComponent.PeakFindExchangeItem"

local PeakFindExcchangeView = Class.PeakFindExcchangeView(ClassTypes.BaseLuaUIComponent)

PeakFindExcchangeView.interface = GameConfig.ComponentsConfig.Component
PeakFindExcchangeView.classPath = "Modules.ModulePrecious.ChildView.PeakFindExcchangeView"


local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local BagManager = PlayerManager.BagManager
local UIComponentUtil = GameUtil.UIComponentUtil
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EntityConfig = GameConfig.EntityConfig
local ItemDataHelper = GameDataHelper.ItemDataHelper
local PeakFindExchangeItem = ClassTypes.PeakFindExchangeItem
local UIList = ClassTypes.UIList
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemConfig = GameConfig.ItemConfig
local PeakFindDataHelper = GameDataHelper.PeakFindDataHelper

local Min_Item_Count = 32

function PeakFindExcchangeView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self:InitListenerFunc()
end
--override
function PeakFindExcchangeView:OnDestroy() 

end

function PeakFindExcchangeView:InitListenerFunc()
    self._update = function() self:ShowScore() end
end

function PeakFindExcchangeView:OnEnable()
    self:AddEventListeners()
end

function PeakFindExcchangeView:OnDisable()
    self:RemoveEventListeners()
end

function PeakFindExcchangeView:ShowView() 
    self:RefreshView()
    EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Fx, false)
end

function PeakFindExcchangeView:CloseView()
    EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Fx, true)
end

function PeakFindExcchangeView:AddEventListeners()

    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LOTTERY_EQUIP_SCORE, self._update)
end

function PeakFindExcchangeView:RemoveEventListeners()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LOTTERY_EQUIP_SCORE, self._update)
end

function PeakFindExcchangeView:InitView()
    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose,function () self:OnCloseClick() end)

    self._scoreText = self:GetChildComponent("Text_Score/Text_Value", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._scrollView:SetHorizontalMove(true)
    self._scrollView:SetVerticalMove(false)
    self._list:SetItemType(PeakFindExchangeItem)
    self._list:SetPadding(0, 10, 0, 10)
    self._list:SetGap(10, 12)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
end

function PeakFindExcchangeView:OnListItemClicked(idx)
    self._list:GetItem(idx):ShowSelected(true)
end

function PeakFindExcchangeView:RefreshView()
    local needData = PeakFindDataHelper:GetAllEquipFindPoint(GameWorld.Player():GetVocationGroup())
    self._list:SetDataList(needData)
    self:ShowScore()
end

function PeakFindExcchangeView:ShowScore()
    local score = GameWorld.Player().lottery_equip_score
    self._scoreText.text = score
end

function PeakFindExcchangeView:OnCloseClick()
    self:CloseView()
    self.gameObject:SetActive(false)
end