-- PeakFindView.lua
require "Modules.ModulePrecious.ChildView.PeakFindWarehouseView"
require "Modules.ModulePrecious.ChildView.PeakFindExcchangeView"
require "Modules.ModulePrecious.ChildComponent.PreciousEquipGetItem"
require "UIComponent.Extend.TipsCom"
require "Modules.ModulePrecious.ChildComponent.PreciousPeakRewardItem"
require "UIComponent.Extend.ItemGrid"
require "Modules.ModulePrecious.ChildView.PeakFindReportView"
require"UIComponent.Extend.CurrencyCom"
require "UIComponent.Extend.SimpleCurrencyCom"

local PeakFindView = Class.PeakFindView(ClassTypes.BaseComponent)
PeakFindView.interface = GameConfig.ComponentsConfig.Component
PeakFindView.classPath = "Modules.ModulePrecious.ChildView.PeakFindView"

local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local lotteryEquipData = PlayerManager.PlayerDataManager.lotteryEquipData
local LotteryPeakManager = PlayerManager.LotteryPeakManager
local public_config = GameWorld.public_config
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local XArtNumber = GameMain.XArtNumber
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PeakFindWarehouseView = ClassTypes.PeakFindWarehouseView
local PeakFindExcchangeView = ClassTypes.PeakFindExcchangeView
local PreciousEquipGetItem = ClassTypes.PreciousEquipGetItem
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local Vector3 = Vector3
local EventDispatcher = EventDispatcher
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local TipsCom = ClassTypes.TipsCom
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local PreciousPeakRewardItem = ClassTypes.PreciousPeakRewardItem
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local UIRoller = ClassTypes.UIRoller
local ItemGrid = ClassTypes.ItemGrid
local ItemDataHelper = GameDataHelper.ItemDataHelper
local TipsManager = GameManager.TipsManager
local PeakFindDataHelper = GameDataHelper.PeakFindDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local PeakFindReportView = ClassTypes.PeakFindReportView
local UIToggle = ClassTypes.UIToggle
local EntityConfig = GameConfig.EntityConfig
local UIParticle = ClassTypes.UIParticle
local ActorModelComponent = GameMain.ActorModelComponent
local TimerHeap = GameWorld.TimerHeap
local PanelsConfig = GameConfig.PanelsConfig
local EquipModelComponent = GameMain.EquipModelComponent
local SimpleCurrencyCom = ClassTypes.SimpleCurrencyCom

local MAX_REWARD_COUNT = 3
local MODE_POS_HIDE = Vector3.New(-1000, -1000 , 0)
local CurrencyCom = ClassTypes.CurrencyCom
local VocGroupToLuckyInfo = {[1]=76230, [2]=76231}

function PeakFindView:Awake()
    self:InitView()
    self:InitListenerFunc()
    self.canClick = true
end

function PeakFindView:OnDestroy()
    GameWorld.DestroyEntity(self._avatarId)
end

function PeakFindView:InitListenerFunc()
    self._stopRoller = function() self:StopRoller() end
    self._onCloseItemTips = function() self:ContinueRoller() end
    self._refreshEquipFindSelfRecord = function() self:RefreshSelfRewardRecord() end
    self._refreshEquipFindServerRecord = function() self:RefreshAllRewardRecord() end
    self._showEquipFindReport = function() self:ShowReport() end
    self._showEquipFindFx = function(state) self:ShowFx(state) end
    self._onRefreshEquipFindKeyInfo = function() self:ShowKeyInfo() end
    self._onRefreshEquipFindRedPoint = function() self:RefreshRedPoint() end
    self._lotteryEquipLuckyValueChange = function() self:OnLuckyValueChange() end
    self._countDownFunc = function() self:CountDown() end
    self._onLotteryEquipLastFreeTimeChange = function()self:RefreshFreeUse() end
    self._onRefreshEquipFindKeyRedPoint = function() self:RefreshFreeUse() end
    self._resetClickState = function() self:ResetClickState() end
end

function PeakFindView:OnEnable()
    self:AddEventListeners()
end

function PeakFindView:OnDisable()
    self:RemoveEventListeners()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer=nil
    end
    if self._stateTimer then
        TimerHeap:DelTimer(self._stateTimer)
        self._stateTimer = nil
    end
end

function PeakFindView:OnClose()
    -- self:OnDisable()
end

function PeakFindView:OnShow()
    -- self:OnEnable()
    self.canClick = true
    self:RefreshFreeUse()
    LotteryPeakManager:LotteryEquipSelfRecordReq()
    LotteryPeakManager:LotteryEquipServerRecordReq()
    self:ShowNormalReward()
    self:ShowSpecialReward()
    self:RefreshButtonState()
    self._currency:OnShow()
    --每次打开更新，以防顺序变化
    if self._entity ~= nil then
        self._entity:SetActorSortingOrder(GUIManager.GetPanelSortOrder(PanelsConfig.Precious))
    end
    self:ShowKeyInfo()
    self:RefreshRedPoint()
    self:ShowLuckyInfo()
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModulePrecious.ChildView.PeakFindView")
end

function PeakFindView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Peak_Find_Stop_Roller, self._stopRoller)
    EventDispatcher:AddEventListener(GameEvents.On_Close_Item_Tips, self._onCloseItemTips)  
    EventDispatcher:AddEventListener(GameEvents.Refresh_Peak_Find_Self_Record, self._refreshEquipFindSelfRecord)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Peak_Find_Server_Record, self._refreshEquipFindServerRecord)
    EventDispatcher:AddEventListener(GameEvents.Show_Equip_Find_Report, self._showEquipFindReport)
    EventDispatcher:AddEventListener(GameEvents.Show_Equip_Find_Fx, self._showEquipFindFx)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Equip_Find_Key_Info, self._onRefreshEquipFindKeyInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Equip_Find_Red_Point, self._onRefreshEquipFindRedPoint)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LOTTERY_EQUIP_LUCKY_VALUE, self._lotteryEquipLuckyValueChange)
    EventDispatcher:AddEventListener(GameEvents.ON_LOTTERY_EQUIP_LAST_FREE_TIME_CHANGE, self._onLotteryEquipLastFreeTimeChange)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Equip_Find_Key_Red_Point, self._onRefreshEquipFindKeyRedPoint)
end

function PeakFindView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Peak_Find_Stop_Roller, self._stopRoller)
    EventDispatcher:RemoveEventListener(GameEvents.On_Close_Item_Tips, self._onCloseItemTips)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Peak_Find_Self_Record, self._refreshEquipFindSelfRecord)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Peak_Find_Server_Record, self._refreshEquipFindServerRecord)
    EventDispatcher:RemoveEventListener(GameEvents.Show_Equip_Find_Report, self._showEquipFindReport)
    EventDispatcher:RemoveEventListener(GameEvents.Show_Equip_Find_Fx, self._showEquipFindFx)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Equip_Find_Key_Info, self._onRefreshEquipFindKeyInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Equip_Find_Red_Point, self._onRefreshEquipFindRedPoint)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LOTTERY_EQUIP_LUCKY_VALUE, self._lotteryEquipLuckyValueChange)
    EventDispatcher:RemoveEventListener(GameEvents.ON_LOTTERY_EQUIP_LAST_FREE_TIME_CHANGE, self._onLotteryEquipLastFreeTimeChange)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Equip_Find_Key_Red_Point, self._onRefreshEquipFindKeyRedPoint)
end

function PeakFindView:InitView()
    self._btnBag = self:FindChildGO("Button_Bag")
    self._csBH:AddClick(self._btnBag, function()self:BtnBagClick() end)
    self._bagRedPoint = self:FindChildGO("Button_Bag/Image_Red")

    self._btnExchange = self:FindChildGO("Button_Exchange")
    self._csBH:AddClick(self._btnExchange, function()self:BtnExchangeClick() end)
    self._tipsButton = self:FindChildGO("Button_Tips")
    self._csBH:AddClick(self._tipsButton,function() self:OnTipsButtonClick() end)

    self._btnGetTypeOne = self:FindChildGO("Container_TypeOne/Button_Get")
    self._csBH:AddClick(self._btnGetTypeOne, function()self:BtnGetTypeOneClick() end)
    self._getTypeOneIcon = self:FindChildGO("Container_TypeOne/Container_Icon")
    self._getTypeOneText = self:GetChildComponent("Container_TypeOne/Text_Info", "TextMeshWrapper")
    self._redPointTypeOne = self:FindChildGO("Container_TypeOne/Button_Get/Image_RedPoint")

    self._textTime = self:GetChildComponent("Container_TypeOne/Text_Time","TextMeshWrapper")

    self._btnGetTypeTwo = self:FindChildGO("Container_TypeTwo/Button_Get")
    self._csBH:AddClick(self._btnGetTypeTwo, function()self:BtnGetTypeTwoClick() end)
    self._getTypeTwoIcon = self:FindChildGO("Container_TypeTwo/Container_Icon")
    self._getTypeTwoText = self:GetChildComponent("Container_TypeTwo/Text_Info", "TextMeshWrapper")
    self._redPointTypeTwo = self:FindChildGO("Container_TypeTwo/Button_Get/Image_RedPoint")

    self._btnGetTypeThree = self:FindChildGO("Container_TypeThree/Button_Get")
    self._csBH:AddClick(self._btnGetTypeThree, function()self:BtnGetTypeThreeClick() end)
    self._getTypeThreeIcon = self:FindChildGO("Container_TypeThree/Container_Icon")
    self._getTypeThreeText = self:GetChildComponent("Container_TypeThree/Text_Info", "TextMeshWrapper")
    self._redPointTypeThree = self:FindChildGO("Container_TypeThree/Button_Get/Image_RedPoint")

    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(76233))
    self._tipsGO:SetActive(false)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_AllRewardInfo/ScrollViewList_Get")
    self._listGet = list
    self._scrollViewGet = scrollView
    self._listGet:SetItemType(PreciousEquipGetItem)
    self._listGet:SetPadding(0, 0, 0, 0)
    self._listGet:SetGap(0, 6)
    self._listGet:SetDirection(UIList.DirectionTopToDown, 1, -1)

    local scrollViewSelf, listSelf = UIList.AddScrollViewList(self.gameObject, "Container_SelfRewardInfo/ScrollViewList_Get")
    self._listGetSelf = listSelf
    self._scrollViewGetSelf = scrollViewSelf
    self._listGetSelf:SetItemType(PreciousEquipGetItem)
    self._listGetSelf:SetPadding(0, 0, 0, 0)
    self._listGetSelf:SetGap(0, 6)
    self._listGetSelf:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._rollerUpComp = UIRoller.AddRoller(self.gameObject,"Container_RollerUp")
    self._rollerUpComp:SetItemType(PreciousPeakRewardItem)
    self._rollerUpComp:SetFromToPos(Vector3.New(-86,0,0), Vector3.New(600,0,0), 5, 90, 6, nil)

    self._rollerDownComp = UIRoller.AddRoller(self.gameObject,"Container_RollerDown")
    self._rollerDownComp:SetItemType(PreciousPeakRewardItem)
    self._rollerDownComp:SetFromToPos(Vector3.New(600,0,0), Vector3.New(-86,0,0), 5, 90, 6, nil)

    self._bagGo = self:FindChildGO("Container_Bag")
    self._bagView = self:AddChildLuaUIComponent("Container_Bag", PeakFindWarehouseView)
    -- self._bagGo:SetActive(false)

    self._exchangeGo = self:FindChildGO("Container_Exchange")
    self._exchangeView = self:AddChildLuaUIComponent("Container_Exchange", PeakFindExcchangeView)
    -- self._exchangeGo:SetActive(false)

    self._reportGo = self:FindChildGO("Container_Report")
    self._reportView = self:AddChildLuaUIComponent("Container_Report", PeakFindReportView)
    -- self._reportGo:SetActive(false)

    self:InitSpecialReward()

    self._ticketContainerGO = self:FindChildGO("Container_Ticket")
    self._ticketIconGO = self:FindChildGO("Container_Ticket/Container_Icon")
    local itemGo = GUIManager.AddItem(self._ticketIconGO, 1)
    self._ticketIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._ticketInfoText = self:GetChildComponent("Container_Ticket/Text_Info", "TextMeshWrapper")
    self._ticketCountText = self:GetChildComponent("Container_Ticket/Text_Count", "TextMeshWrapper")
    self._ticketOKButton = self:FindChildGO("Container_Ticket/Button_OK")
    self._csBH:AddClick(self._ticketOKButton, function()self:TicketOKBtnClick() end)
    self._ticketCloseButton = self:FindChildGO("Container_Ticket/Button_Close")
    self._csBH:AddClick(self._ticketCloseButton, function()self:TicketCloseBtnClick() end)
    self._toggleIntrestTips = UIToggle.AddToggle(self.gameObject, "Container_Ticket/Toggle_Intrest")
    self._toggleIntrestTips:SetOnValueChangedCB(function(state)self:OnClickIntrestTips(state) end)
    self._ticketContainerGO:SetActive(false)

    self._animGO = self:FindChildGO("Container_Anim")
    self._boxModel = self:FindChildGO("Container_Anim/Container_Model")
    self._showPos = self._boxModel.transform.position
    -- self._animGO:SetActive(false)

    self._fx1 = UIParticle.AddParticle(self.gameObject, "Container_Reward/Container_Item1/fx_ui_30026")
    self._fx2 = UIParticle.AddParticle(self.gameObject, "Container_Reward/Container_Item2/fx_ui_30026")
    self._fx3 = UIParticle.AddParticle(self.gameObject, "Container_Reward/Container_Item3/Image_BG/fx_ui_30027")
    self._fx4 = UIParticle.AddParticle(self.gameObject, "ProgressBar/fx_ui_30028")

    self._modelComponent = self:AddChildComponent('Container_Reward/Container_Item3/Container_Model', EquipModelComponent)
    local cfg = GlobalParamsHelper.GetParamValue(995)[GameWorld.Player():GetVocationGroup()]
    self._modelComponent:SetStartSetting(Vector3(cfg[2], cfg[3], cfg[4]), Vector3(cfg[5], cfg[6], cfg[7]), cfg[8])
    self._modelComponent:LoadEquipModel(cfg[1], "")

    self._currency = self:AddChildLuaUIComponent("Container_Currency", CurrencyCom)
    self._currency:SetCurrencyTypes({public_config.MONEY_TYPE_COUPONS,public_config.MONEY_TYPE_COUPONS_BIND})

    local itemId, _ = next(GlobalParamsHelper.GetParamValue(750))
    self._keyCom = self:AddChildLuaUIComponent("Container_Key", SimpleCurrencyCom)
    self._keyCom:InitComps(itemId)

    self:CreateModel()
    self:InitProgress()
end

function PeakFindView:InitSpecialReward()
    self._rewardText = {}
    self._rewardIcon = {}
    self._func = {}
    self._rewardData = {}
    for i=1,MAX_REWARD_COUNT do
        self._rewardText[i] = self:GetChildComponent("Container_Reward/Container_Item"..i.."/Text_Name", "TextMeshWrapper")
        local parent = self:FindChildGO("Container_Reward/Container_Item"..i.."/Container_Icon")
        local itemGo = GUIManager.AddItem(parent, 1)
        self._rewardIcon[i] = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
        self._rewardIcon[i]:SetValues({["noFrame"] = true, ["noAnimFrame"] = true})
        self._func[i] = function() TipsManager:ShowItemTipsById(PeakFindDataHelper:GetEquipFindItemId(self._rewardData[i])) end
        self._rewardIcon[i]:SetPointerDownCB(self._func[i])
    end
end

function PeakFindView:InitProgress()
    self._Comp = UIScaleProgressBar.AddProgressBar(self.gameObject, "ProgressBar")
    self._Comp:SetProgress(0)
    self._Comp:ShowTweenAnimate(true)
    self._Comp:SetMutipleTween(true)
    self._Comp:SetIsResetToZero(false)
    self:OnLuckyValueChange()

    self._luckyInfoText = self:GetChildComponent("ProgressBar/Text_LuckyInfo","TextMeshWrapper")
end

function PeakFindView:ShowKeyInfo()
    local itemId, _ = next(GlobalParamsHelper.GetParamValue(750))
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    self._keyCom:SetMoneyNum(ownCount)

    self:RefreshKeyRedPoint()
end

function PeakFindView:OnLuckyValueChange()
    local cur = GameWorld.Player().lottery_equip_lucky_value
    local max = GlobalParamsHelper.GetParamValue(749)
    if cur == max then
        self.showProgressFx = true
        self._fx4:Play(true)
    else
        self.showProgressFx = false
        self._fx4:Stop()
    end
    self._Comp:SetProgressByValue(cur , max)
    self._Comp:SetProgressText(tostring(cur).."/"..tostring(max))
end

function PeakFindView:RefreshRedPoint()
    self._bagRedPoint:SetActive(LotteryPeakManager:GetWarehouseRedPoint())
end

function PeakFindView:RefreshKeyRedPoint()
    local itemId, needCount = next(GlobalParamsHelper.GetParamValue(750))
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    if ownCount >= needCount or self._leftTime <= 0 then
        self._redPointTypeOne:SetActive(true)
    else
        self._redPointTypeOne:SetActive(false)
    end

    itemId, needCount = next(GlobalParamsHelper.GetParamValue(751))
    if ownCount >= needCount then
        self._redPointTypeTwo:SetActive(true)
    else
        self._redPointTypeTwo:SetActive(false)
    end

    itemId, needCount = next(GlobalParamsHelper.GetParamValue(752))
    if ownCount >= needCount then
        self._redPointTypeThree:SetActive(true)
    else
        self._redPointTypeThree:SetActive(false)
    end
end

function PeakFindView:BtnBagClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_equip_find_bag_button")
    self._bagGo:SetActive(true)
    self._bagView:ShowView()
end

function PeakFindView:BtnExchangeClick()
    self._exchangeGo:SetActive(true)
    self._exchangeView:ShowView()
end

function PeakFindView:OnTipsButtonClick()
    self._tipsGO:SetActive(true)
end

function PeakFindView:OnClickIntrestTips(state)
    self._notShowTip = state
end

function PeakFindView:OnUse(itemId, needCount)
    if not self.canClick then
        return
    end
    self:ShowFx(false)
    self._ticketContainerGO:SetActive(true)
    local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    self._ticketIcon:SetItem(itemId)
    self._ticketCountText.text = BaseUtil.GetColorStringByCount(bagCount, needCount)
    local diff = needCount - bagCount
    if diff > 0 then
        local money = GlobalParamsHelper.GetParamValue(754)[itemId] * diff
        self._ticketInfoText.text = LanguageDataHelper.CreateContentWithArgs(58704, {["0"]=money, ["1"]=diff})
    else
        self._ticketInfoText.text = ""
    end
end

function PeakFindView:BtnGetTypeOneClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_equip_find_type_one_button")
    self._type = 1
    local itemId, count = next(GlobalParamsHelper.GetParamValue(750)) 
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    if self._leftTime <= 0 then
        self:BeginGetReq()
        return
    end
    if GameWorld.Player().lottery_equip_remind_flag == 0 and ownCount < count then
        self:OnUse(itemId, count)
    else
        self:BeginGetReq()
    end
end

function PeakFindView:BtnGetTypeTwoClick()
    self._type = 2
    local itemId, count = next(GlobalParamsHelper.GetParamValue(751))
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    if GameWorld.Player().lottery_equip_remind_flag == 0 and ownCount < count then
        self:OnUse(itemId, count)
    else
        self:BeginGetReq()
    end
end

function PeakFindView:BtnGetTypeThreeClick()
    self._type = 3
    local itemId, count = next(GlobalParamsHelper.GetParamValue(752))
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    if GameWorld.Player().lottery_equip_remind_flag == 0 and ownCount < count then
        self:OnUse(itemId, count)
    else
        self:BeginGetReq()
    end
end

function PeakFindView:TicketOKBtnClick()
    self:ShowFx(true)
    if self._notShowTip == true then
        LotteryPeakManager:LotteryEquipSetRemindReq(1)
    end
    self._ticketContainerGO:SetActive(false)

    self:BeginGetReq()
end

function PeakFindView:TicketCloseBtnClick()
    self:ShowFx(true)
    self._ticketContainerGO:SetActive(false)
end

function PeakFindView:ResetClickState()
    self.canClick = true
end

function PeakFindView:BeginGetReq()
    if not self.canClick then
        return
    end
    if self._type == 1 then
        LotteryPeakManager:LotteryEquipOneReq()
    elseif self._type == 2 then
        LotteryPeakManager:LotteryEquipTenReq()
    elseif self._type == 3 then
        LotteryPeakManager:LotteryEquipFiftyReq()
    end
    self.canClick = false
    if self._stateTimer then
        TimerHeap:DelTimer(self._stateTimer)
        self._stateTimer = nil
    end
    self._stateTimer = TimerHeap:AddSecTimer(0.5, 0, 0, self._resetClickState)
end

function PeakFindView:RefreshAllRewardRecord()
    local data = lotteryEquipData.serverRecordData or {}
    self._listGet:SetDataList(data)
    self._listGet:MoveToBottom()
end

function PeakFindView:RefreshSelfRewardRecord()
    local data = lotteryEquipData.selfRecordData or {}
    self._listGetSelf:SetDataList(data)
    self._listGetSelf:MoveToBottom()
end

function PeakFindView:ShowLuckyInfo()
    local vocGroup = GameWorld.Player():GetVocationGroup() or 1
    self._luckyInfoText.text = LanguageDataHelper.CreateContent(VocGroupToLuckyInfo[vocGroup])
end

function PeakFindView:StopRoller()
    self._rollerUpComp:OnStop()
    self._rollerDownComp:OnStop()
end

function PeakFindView:ContinueRoller()
    self._rollerUpComp:Continue()
    self._rollerDownComp:Continue()
end

function PeakFindView:ShowNormalReward()
    local data = PeakFindDataHelper:GetEquipFindNormalData(GameWorld.Player().level, GameWorld.Player():GetVocationGroup())
    --上下各显示一半奖励
    local upData = {}
    for i=1,math.ceil(#data/2) do
        table.insert(upData, data[i])
    end
    self._rollerUpComp:SetDataList(upData)
    self._rollerUpComp:OnStart()

    local downData = {}
    for i=math.ceil(#data/2)+1,#data do
        table.insert(downData, data[i])
    end
    self._rollerDownComp:SetDataList(downData)
    self._rollerDownComp:OnStart()
end

function PeakFindView:ShowSpecialReward()
    self._rewardData = PeakFindDataHelper:GetEquipFindSpecialData(GameWorld.Player().level, GameWorld.Player():GetVocationGroup())
    for i=1,MAX_REWARD_COUNT do
        local itemId = PeakFindDataHelper:GetEquipFindItemId(self._rewardData[i])
        self._rewardText[i].text = ItemDataHelper.GetItemNameWithColor(itemId)
        self._rewardIcon[i]:SetItem(itemId)
    end
end

function PeakFindView:RefreshButtonState()
    local itemId, count = next(GlobalParamsHelper.GetParamValue(750))
    GameWorld.AddIcon(self._getTypeOneIcon, ItemDataHelper.GetIcon(itemId))
    if self._leftTime <= 0 then
        self._getTypeOneText.text = LanguageDataHelper.CreateContent(74901)
    else
        self._getTypeOneText.text = "×"..count
    end

    itemId, count = next(GlobalParamsHelper.GetParamValue(751))
    GameWorld.AddIcon(self._getTypeTwoIcon, ItemDataHelper.GetIcon(itemId))
    self._getTypeTwoText.text = "×"..count

    itemId, count = next(GlobalParamsHelper.GetParamValue(752))
    GameWorld.AddIcon(self._getTypeThreeIcon, ItemDataHelper.GetIcon(itemId))
    self._getTypeThreeText.text = "×"..count
end

function PeakFindView:RefreshFreeUse()
    self._textTime.text = ""
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer=nil
    end
    local freeTime = GameWorld.Player().lottery_equip_last_free_time + GlobalParamsHelper.GetParamValue(920)
    self._leftTime = freeTime - DateTimeUtil.GetServerTime()
    if self._leftTime > 0 then
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, self._countDownFunc)
    end
    self:RefreshButtonState()
    self:ShowKeyInfo()
end

function PeakFindView:CountDown()
    self._leftTime=self._leftTime-1
    self._textTime.text= DateTimeUtil:FormatFullTime(self._leftTime)..LanguageDataHelper.GetContent(76208)
    if self._leftTime<=0 then
        TimerHeap:DelTimer(self._timer)
        self._timer=nil
        self._textTime.text = ""
        self:RefreshButtonState()
        self:ShowKeyInfo()
    end
end

function PeakFindView:ShowReport()
    self:ShowFx(false)
    self:ShowAnim()
    self.canClick = true
    if self._stateTimer then
        TimerHeap:DelTimer(self._stateTimer)
        self._stateTimer = nil
    end
end

function PeakFindView:ShowAnim()
    self._animGO:SetActive(true)
    self:PlayFx()
    TimerHeap:AddSecTimer(1.5, 0, 0, function() self:CloseAnim() end)
end

function PeakFindView:CloseAnim()
    self._animGO:SetActive(false)
    self._entity:TeleportClient(MODE_POS_HIDE)

    local data = lotteryEquipData.getRewardData or {}
    self._reportGo:SetActive(true)
    self._reportView:ShowView(data)
end

function PeakFindView:ShowFx(state)
    if state then
        self._fx1:Play(true)
        self._fx2:Play(true)
        self._fx3:Play(true)
        if self.showProgressFx then
            self._fx4:Play(true)
        end
        self._modelComponent:Show()
    else
        self._fx1:Stop()
        self._fx2:Stop()
        self._fx3:Stop()
        self._fx4:Stop()
        self._modelComponent:Hide()
    end
end


-----------------------------------------------------------------------模型相关----------------------------------------
function PeakFindView:CreateModel()
    local actorID = 2417
    local info = {}
    info.isDramaEntity = true
    info.scale = 8
    --info.face = Vector3(25, 180, 0)
    info.face = Vector3(18, 230, 18)
    info.sort = GUIManager.GetPanelSortOrder(PanelsConfig.Precious)
    self._avatarId = GameWorld.CreateEntity("UIShowEntity", info).id
    local avatar = GameWorld.GetEntity(self._avatarId)
    avatar:SetActor(actorID)
    avatar:Teleport(MODE_POS_HIDE)
    self._entity = avatar
end

function PeakFindView:PlayFx()
    if self._entity ~= nil then
        self._entity:TeleportClient(self._showPos)
        self._entity:PlaySkillByUIShowEntity(2417)
    end
end