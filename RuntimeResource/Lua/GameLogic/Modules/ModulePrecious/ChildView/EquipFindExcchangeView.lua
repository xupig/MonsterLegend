require "Modules.ModulePrecious.ChildComponent.EquipFindExcchangeItem"

local EquipFindExcchangeView = Class.EquipFindExcchangeView(ClassTypes.BaseLuaUIComponent)

EquipFindExcchangeView.interface = GameConfig.ComponentsConfig.Component
EquipFindExcchangeView.classPath = "Modules.ModulePrecious.ChildView.EquipFindExcchangeView"


local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local BagManager = PlayerManager.BagManager
local UIComponentUtil = GameUtil.UIComponentUtil
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EntityConfig = GameConfig.EntityConfig
local ItemDataHelper = GameDataHelper.ItemDataHelper
local EquipFindExcchangeItem = ClassTypes.EquipFindExcchangeItem
local UIList = ClassTypes.UIList
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemConfig = GameConfig.ItemConfig
local EquipFindDataHelper = GameDataHelper.EquipFindDataHelper

local Min_Item_Count = 32

function EquipFindExcchangeView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self:InitListenerFunc()
end
--override
function EquipFindExcchangeView:OnDestroy() 

end

function EquipFindExcchangeView:InitListenerFunc()
    self._update = function() self:ShowScore() end
end

function EquipFindExcchangeView:OnEnable()
    self:AddEventListeners()
end

function EquipFindExcchangeView:OnDisable()
    self:RemoveEventListeners()
end

function EquipFindExcchangeView:ShowView() 
    self:RefreshView()
    EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Fx, false)
end

function EquipFindExcchangeView:CloseView()
    EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Fx, true)
end

function EquipFindExcchangeView:AddEventListeners()

    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LOTTERY_EQUIP_SCORE, self._update)
end

function EquipFindExcchangeView:RemoveEventListeners()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LOTTERY_EQUIP_SCORE, self._update)
end

function EquipFindExcchangeView:InitView()
    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose,function () self:OnCloseClick() end)

    self._scoreText = self:GetChildComponent("Text_Score/Text_Value", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._scrollView:SetHorizontalMove(true)
    self._scrollView:SetVerticalMove(false)
    self._list:SetItemType(EquipFindExcchangeItem)
    self._list:SetPadding(0, 10, 0, 10)
    self._list:SetGap(10, 12)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
end

function EquipFindExcchangeView:OnListItemClicked(idx)
    self._list:GetItem(idx):ShowSelected(true)
end

function EquipFindExcchangeView:RefreshView()
    local needData = EquipFindDataHelper:GetAllEquipFindPoint(GameWorld.Player():GetVocationGroup())
    self._list:SetDataList(needData)
    self:ShowScore()
end

function EquipFindExcchangeView:ShowScore()
    local score = GameWorld.Player().lottery_equip_score
    self._scoreText.text = score
end

function EquipFindExcchangeView:OnCloseClick()
    self:CloseView()
    self.gameObject:SetActive(false)
end