-- RuneFindView.lua
require "Modules.ModulePrecious.ChildView.RuneBagView"
require "Modules.ModulePrecious.ChildView.RunePreView"
require "Modules.ModulePrecious.ChildView.RuneReportView"
require "Modules.ModulePrecious.ChildComponent.PreciousRunePreListItem2"
require "Modules.ModulePrecious.ChildComponent.PreciousRuneGetItem"
require "UIComponent.Extend.ItemGrid"
require"UIComponent.Extend.CurrencyCom"
require "UIComponent.Extend.SimpleCurrencyCom"

local RuneFindView = Class.RuneFindView(ClassTypes.BaseComponent)
RuneFindView.interface = GameConfig.ComponentsConfig.Component
RuneFindView.classPath = "Modules.ModulePrecious.ChildView.RuneFindView"
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local PreciousData = PlayerManager.PlayerDataManager.preciousData
local PreciousManager = PlayerManager.PreciousManager
local public_config = GameWorld.public_config
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local XArtNumber = GameMain.XArtNumber
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RuneBagView = ClassTypes.RuneBagView
local RunePreView = ClassTypes.RunePreView
local RuneReportView = ClassTypes.RuneReportView
local PreciousRunePreListItem2 = ClassTypes.PreciousRunePreListItem2 
local PreciousRuneGetItem = ClassTypes.PreciousRuneGetItem
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local Vector3 = Vector3
local EventDispatcher = EventDispatcher
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local UIParticle = ClassTypes.UIParticle
local bagData = PlayerManager.PlayerDataManager.bagData
local ItemGrid = ClassTypes.ItemGrid
local UIToggle = ClassTypes.UIToggle
local CurrencyCom = ClassTypes.CurrencyCom
local SimpleCurrencyCom = ClassTypes.SimpleCurrencyCom
local action_config = GameWorld.action_config

function RuneFindView:Awake()
    self._timerid = -1
    self._views = {}
    self._viewGos = {}
    self._csBH = self:GetComponent("LuaUIComponent")
    self._timerfun = function() self:FreeTimer() end
    self._updatePre = function()self:UpdatePre()end
    self._updateGetList = function(data)self:UpdateGetList(data)end
    self._runefindRpc = function(data)self:RuneFindRpc(data)end
    self._runefindFx = function()self:FindFX()end
    self._findGet = function()self:FindGet()end
    self._onRefreshEquipFindKeyInfo = function() self:ShowKeyInfo() end
    self._timerid1 = -1
    self:InitView()
end

function RuneFindView:UpdateCtril()
 
end
    
function RuneFindView:RuneFindRpc(data)
    if data == action_config.RUNE_TEN_LUCK then
        self:BtnFind2()
    else
        self:BtnFind1()        
    end
end

function RuneFindView:InitView()
    self._runeBagView = self:AddChildLuaUIComponent("Container_Bag", RuneBagView)
    self._views[1]=self._runeBagView
    self._runePreView = self:AddChildLuaUIComponent("Container_Pre", RunePreView)
    self._views[2]=self._runePreView    
    self._runeReportView = self:AddChildLuaUIComponent("Container_Report", RuneReportView)
    self._views[3]=self._runeReportView

    self._viewGos[1]=self:FindChildGO("Container_Bag")
    self._viewGos[2]=self:FindChildGO("Container_Pre")
    self._viewGos[3]=self:FindChildGO("Container_Report")
    self._viewGos[1]:SetActive(false)
    self._viewGos[2]:SetActive(false)
    self._viewGos[3]:SetActive(false)


    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(PreciousRunePreListItem2)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 6)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, -1)
    local itemClickedCB =                
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
    self._scrollView:SetHorizontalMove(true)
    self._scrollView:SetVerticalMove(false)
    self._btnBag = self:FindChildGO("Button_RuneBag")
    self._csBH:AddClick(self._btnBag, function()self:BtnBagClick() end)

    self._btnAll = self:FindChildGO("Button_AllLook")
    self._csBH:AddClick(self._btnAll, function()self:BtnAllClick() end)

    self._btnFind1 = self:FindChildGO("Button_Find1")
    self._csBH:AddClick(self._btnFind1, function()self:BtnFind1() end)

    self._btnFind2 = self:FindChildGO("Button_Find2")
    self._csBH:AddClick(self._btnFind2, function()self:BtnFind2() end)

    self._btnEx = self:FindChildGO("Button_RuneEx")
    self._csBH:AddClick(self._btnEx, function()self:BtnExClick() end)

    self._fin1P = self:GetChildComponent("Button_Find1/Text",'TextMeshWrapper')
    self._fin2P = self:GetChildComponent("Button_Find2/Text_Price",'TextMeshWrapper')
    self._fzt = self:GetChildComponent("Button_Find2/Text_Tips","TextMeshWrapper")
    self._textTime = self:GetChildComponent("Text_Time","TextMeshWrapper")
    self._fzt.text = LanguageDataHelper.GetContent(76207)
    local d1 = GlobalParamsHelper.GetParamValue(610)
    local d2 = GlobalParamsHelper.GetParamValue(611)
    local sigId = 131
    local sigNum = 0
    for k,v in pairs(d1) do
        sigId = tonumber(k)
        sigNum = v
    end
    self._needSigleCost = sigNum
    self._itemIdSigleCost = sigId
    local tenId = 131
    local tenNum = 0
    for k,v in pairs(d2) do
        tenId = tonumber(k)
        tenNum = v
    end
    self._needTenCost = tenNum
    self._itemIdTenCost = tenId
    local sigIcongo = self:FindChildGO("Button_Find1/Image")
    local tenIcongo = self:FindChildGO("Button_Find2/Image")
    GameWorld.AddIcon(sigIcongo,ItemDataHelper.GetIcon(sigId))
    GameWorld.AddIcon(tenIcongo,ItemDataHelper.GetIcon(tenId))

    self._fin1P.text = 'x'..sigNum
    self._fin2P.text = 'x'..tenNum
    self._redP1 = self:FindChildGO("Button_Find1/Image_Red")
    self._redP2 = self:FindChildGO("Button_Find2/Image_Red")

    self.stop = Vector3.New(0, 0, 360)
    self.start = Vector3.New(0, 0, 0)

    local _rotation1 = self:FindChildGO("Container_Ro1")
    local _rotation2 = self:FindChildGO("Image_Ro2")
    local _rotation3 = self:FindChildGO("Image_Ro3")
    self.fxtarget = UIParticle.AddParticle(self.gameObject,"Container_Fx/fx_ui_30008")
    self.fxtarget:Stop()

    self._rotation1 = _rotation1:AddComponent(typeof(XGameObjectTweenRotation))
    self._rotation2 = _rotation2:AddComponent(typeof(XGameObjectTweenRotation))
    self._rotation3 = _rotation3:AddComponent(typeof(XGameObjectTweenRotation))
    self._rotation1Trans = self._rotation1.transform

    self._rotation={}
    self._rotation[1]=self._rotation1
    self._rotation[2]=self._rotation2
    self._rotation[3]=self._rotation3

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Get")
    self._listGet = list
    self._scrollViewGet = scrollView
    self._listGet:SetItemType(PreciousRuneGetItem)
    self._listGet:SetPadding(0, 0, 0, 0)
    self._listGet:SetGap(0, 6)
    self._listGet:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self:Rotation(1,self.start,self.stop,{20,2})
    self.fxtarget:SetLifeTime(2)

    self._ticketContainerGO = self:FindChildGO("Container_Ticket")
    self._ticketIconGO = self:FindChildGO("Container_Ticket/Container_Icon")
    local itemGo = GUIManager.AddItem(self._ticketIconGO, 1)
    self._ticketIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._ticketInfoText = self:GetChildComponent("Container_Ticket/Text_Info", "TextMeshWrapper")
    self._ticketCountText = self:GetChildComponent("Container_Ticket/Text_Count", "TextMeshWrapper")
    self._ticketOKButton = self:FindChildGO("Container_Ticket/Button_OK")
    self._csBH:AddClick(self._ticketOKButton, function()self:TicketOKBtnClick() end)
    self._ticketCloseButton = self:FindChildGO("Container_Ticket/Button_Close")
    self._csBH:AddClick(self._ticketCloseButton, function()self:TicketCloseBtnClick() end)
    self._toggleIntrestTips = UIToggle.AddToggle(self.gameObject, "Container_Ticket/Toggle_Intrest")
    self._toggleIntrestTips:SetOnValueChangedCB(function(state)self:OnClickIntrestTips(state) end)
    self._ticketContainerGO:SetActive(false)


    self._currency = self:AddChildLuaUIComponent("Container_Currency", CurrencyCom)
    self._currency:SetCurrencyTypes({public_config.MONEY_TYPE_COUPONS,public_config.MONEY_TYPE_COUPONS_BIND})
    local itemId, _ = next(GlobalParamsHelper.GetParamValue(610))
    self._keyCom = self:AddChildLuaUIComponent("Container_Key", SimpleCurrencyCom)
    self._keyCom:InitComps(itemId)
    self._runeFind=action_config.RUNE_SINGLE_LUCK
    self._countnum= GlobalParamsHelper.GetParamValue(616)
    self._counttext = self:GetChildComponent("Text_Count",'TextMeshWrapper')
end


function RuneFindView:OnClickIntrestTips(state)
    self._notShowTip = state
end

function RuneFindView:ShowKeyInfo()
    local itemId, _ = next(GlobalParamsHelper.GetParamValue(610))
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    self._keyCom:SetMoneyNum(ownCount)
end
function RuneFindView:InitRuneFindRo()
    local z = self._rotation1Trans.eulerAngles.z
    if z>180 then
        z = z-360
    end
    self:Rotation(1,Vector3.New(0,0,z),self.stop,{20,2})
    self._rotation2.IsTweening = false
end

function RuneFindView:Rotation(target,start,stop,data)
    self._rotation[target].IsTweening = false
    self._rotation[target]:SetFromToRotation(self.start, self.stop, data[1], data[2],nil)
end

function RuneFindView:UpdateGetList(data)
    self._listGet:SetDataList(data)
end



function RuneFindView:FreeTimer()
    self._lefttime=self._lefttime-1
    self._textTime.text= DateTimeUtil:FormatFullTime(self._lefttime)..LanguageDataHelper.GetContent(76208)
    if self._lefttime==0 then
        TimerHeap:DelTimer(self._timerid)
        self._timerid=-1
        self._textTime.text = ''
    end
end

function RuneFindView:BtnFind1()
    self._runeFind=action_config.RUNE_SINGLE_LUCK
    if self._redP1.activeSelf==true then
        self:FindOne()
        return
    end
    self:FindRpcTips(self._itemIdSigleCost,self._needSigleCost,1)
end

function RuneFindView:BtnFind2()
    self._runeFind=action_config.RUNE_TEN_LUCK
    self:FindRpcTips(self._itemIdTenCost,self._needTenCost,2)
end

function RuneFindView:FindRpcTips(costid,needcost,type)
    local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(costid)
    self._ticketCountText.text = BaseUtil.GetColorStringByCount(bagCount, needcost)
    local diff = needcost - bagCount
    if diff > 0 and GameWorld.Player().rune_remind_flag == 0 then
        self._ticketContainerGO:SetActive(true)
        self._ticketIcon:SetItem(self._itemIdSigleCost)
        local money = self:CostDiam(self._itemIdSigleCost,diff,0)
        self._ticketInfoText.text = LanguageDataHelper.CreateContentWithArgs(58703, {["0"]=money, ["1"]=diff})
    else
        if type==1 then
            self._runeFind=action_config.RUNE_SINGLE_LUCK   
            self:FindOne()
        else
            self._runeFind=action_config.RUNE_TEN_LUCK
            self:FindTen()    
        end
    end
end

function RuneFindView:FindOne()
    self:FindFX()
    self._timerid2 = TimerHeap:AddSecTimer(1,0,0,function()
        self:Find1Rpc()  
        TimerHeap:DelTimer(self._timerid2)              
    end) 
end

function RuneFindView:FindTen()
    self:FindFX()
    self._timerid2 = TimerHeap:AddSecTimer(1,0,0,function()
        self:Find2Rpc()  
        TimerHeap:DelTimer(self._timerid2)              
    end)
end

function RuneFindView:CostDiam(id,dis,t)
    local money = 0
    local d = GlobalParamsHelper.GetParamValue(787)
    if t==0 then
        money = d[id] * dis
    elseif t==1 then
        money = d[id] * dis
    end
    return money
end


function RuneFindView:TicketOKBtnClick()
    self._ticketContainerGO:SetActive(false)
    if self._notShowTip == true then
        PreciousManager:RuneFindSetRemindReq(1)
    end
    local a = GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS]]
    if self._runeFind==action_config.RUNE_SINGLE_LUCK then
        local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._itemIdSigleCost)
        local d = self._needSigleCost - bagCount
        local money = self:CostDiam(self._itemIdSigleCost,d,0)
        if money>a then
            self:Find1Rpc()
        else
            self:FindOne()
        end
    else
        local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._itemIdTenCost)
        local d = self._needTenCost - bagCount
        local money = self:CostDiam(self._itemIdSigleCost,d,0)
        if money>a then
            self:Find2Rpc()
        else
            self:FindTen()
        end
    end
end

function RuneFindView:TicketCloseBtnClick()
    self._ticketContainerGO:SetActive(false)
end

function RuneFindView:Find1Rpc()
    self:InitRuneFindRo() 
    PreciousManager.SigleFindRpc()  
    PreciousManager.RuneLucklistRpc() 
end
function RuneFindView:Find2Rpc()
    self:InitRuneFindRo() 
    PreciousManager.FindTenRpc() 
    PreciousManager.RuneLucklistRpc() 
end

function RuneFindView:FindFX()
    self:SetRotation()
    self.fxtarget:ForceReorderAll()
    self.fxtarget:Play(false,true)
end

function RuneFindView:SetRotation()
    local z = self._rotation1Trans.eulerAngles.z
    if z>180 then
        z = z-360
    end
    self:Rotation(1,Vector3.New(0,0,z),self.stop,{2,2})
    self:Rotation(2,self.start,self.stop,{2,1})
    self:Rotation(3,self.start,self.stop,{2,1})
end
function RuneFindView:BtnBagClick()
    self:SeleView(1)
end
function RuneFindView:BtnExClick()
    local data={}
    data.firstTabIndex=3
    GUIManager.ShowPanel(PanelsConfig.Rune,data)
end
function RuneFindView:BtnAllClick()
    self:SeleView(2)
end
function RuneFindView:SeleView(index)
    for i=1,#self._viewGos do 
        if i==index then
            self._viewGos[i]:SetActive(true)
            self._views[i]:OnShow()
        else
            self._viewGos[i]:SetActive(false)
            self._views[i]:OnClose()
        end
    end
end

function RuneFindView:OnDestroy()
    self._csBH = nil
end

function RuneFindView:Update()
     
end

function RuneFindView:OnClose()
    self:RemoveListener()
    TimerHeap:DelTimer(self._timerid1)
end

function RuneFindView:UpdatePre()
    local d1=PreciousData:GetRunePre()
    self._list:SetDataList(d1)
end
function RuneFindView:OnShow()
    PreciousManager:HandleData()
    self:UpdataCount()
    self._currency:OnShow()
    self:ShowKeyInfo()
    self:Rotation(1,self.start,self.stop,{20,2})
    self:UpdatePre()
    self:AddListener()
    self:UpdateFindGet()
    PreciousManager.RuneLucklistRpc()
    self._timerid1=TimerHeap:AddSecTimer(0,5,0,function()PreciousManager.RuneLucklistRpc()end)
end

-- 更新寻宝的时间
function RuneFindView:UpdateFindGet()
    local data = PreciousData:GetRuneFind()
    local a = data[1]
    local b = data[1]
    self._redP1:SetActive(false)
    self._redP2:SetActive(false)
    if a._curtype==0 then
        -- self._redP1:SetActive(true)
        self._textTime.text = ''
        self._fin1P.text = LanguageDataHelper.GetContent(74901)
    else 
        local d = GlobalParamsHelper.GetParamValue(610)
        local id = 0
        local num = 0
        for k,v in pairs(d) do
            num=v
        end
        self._needCost = num
        self._itemIdCost = id
        self._fin1P.text = 'x'..num
        if self._timerid==-1 then
            local entity = GameWorld.Player()
            local needtime = GlobalParamsHelper.GetParamValue(620)--免费时间间隔
            local runefreetime = entity.rune_free_time
            local _d = DateTimeUtil.MinusServerDay(runefreetime)
            local f = needtime-(_d['day']*24*60*60+_d['hour']*60*60+_d['min']*60+_d['sec'])
            self._lefttime = f
            self._timerid = TimerHeap:AddSecTimer(0,1,0,self._timerfun)
        end
        self:FreeTimer()
    end
    if a._curtype==0 or a._curtype==1 then
        self._redP1:SetActive(true)
    end
    if b._curtype==0 then
        self._redP2:SetActive(true)
    else 

    end
end
function RuneFindView:FindGet()
    self._viewGos[3]:SetActive(true)
    self._views[3]:OnShow()
    self:UpdateFindGet()
    self:InitRuneFindRo()
    self:UpdataCount()
end
function RuneFindView:UpdataCount()
    local s = GameWorld.Player().rune_luck_times%self._countnum
    local c = self._countnum-s
    if c<=0 then
        self._counttext.text = self._countnum..''
    else
        self._counttext.text = c..''
    end
end
function RuneFindView:AddListener()
    -- EventDispatcher:AddEventListener(GameEvents.On_Refresh_Precious_Panel_Red_Point,self._runefindFx)
    EventDispatcher:AddEventListener(GameEvents.RuneFindRpc,self._runefindRpc)
    EventDispatcher:AddEventListener(GameEvents.RuneGetList,self._updateGetList)
    EventDispatcher:AddEventListener(GameEvents.RunePre,self._updatePre)
    EventDispatcher:AddEventListener(GameEvents.RuneFindGet,self._findGet)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Rune_Find_Key_Info, self._onRefreshEquipFindKeyInfo)
end

function RuneFindView:RemoveListener()
    -- EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Precious_Panel_Red_Point,self._runefindFx)
    EventDispatcher:RemoveEventListener(GameEvents.RuneFindRpc,self._runefindRpc)
    EventDispatcher:RemoveEventListener(GameEvents.RuneGetList,self._updateGetList)
    EventDispatcher:RemoveEventListener(GameEvents.RunePre,self._updatePre)
    EventDispatcher:RemoveEventListener(GameEvents.RuneFindGet,self._findGet)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Rune_Find_Key_Info, self._onRefreshEquipFindKeyInfo)
end



