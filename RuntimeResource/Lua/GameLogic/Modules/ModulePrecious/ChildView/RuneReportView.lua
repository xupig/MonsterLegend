-- RuneReportView.lua
require "Modules.ModulePrecious.ChildComponent.PreciousRuneRepListItem"
local RuneReportView = Class.RuneReportView(ClassTypes.BaseComponent)
RuneReportView.interface = GameConfig.ComponentsConfig.Component
RuneReportView.classPath = "Modules.ModulePrecious.ChildView.RuneReportView"
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local PreciousData = PlayerManager.PlayerDataManager.preciousData
local PreciousManager = PlayerManager.PreciousManager
local public_config = GameWorld.public_config
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local XArtNumber = GameMain.XArtNumber
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PreciousRuneRepListItem = ClassTypes.PreciousRuneRepListItem
local UIList = ClassTypes.UIList
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local action_config = GameWorld.action_config
local UIParticle = ClassTypes.UIParticle

local perShowRewardNum = 1
local perTime = 0.1
local Count_Per_Row = 5

function RuneReportView:Awake()
    self._csBH = self:GetComponent("LuaUIComponent")
    self:InitView()
end
    
function RuneReportView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(PreciousRuneRepListItem)
    self._list:SetPadding(10, 0, 0, 25)
    self._list:SetGap(10, 20)
    self._list:SetDirection(UIList.DirectionLeftToRight, 5, -1)
    local itemClickedCB =                
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    local text1 = self:GetChildComponent("Button_Confirm/Text","TextMeshWrapper")
    self.textbuy = self:GetChildComponent("Button_Bug/Text","TextMeshWrapper")
    self._text3 = self:GetChildComponent("Container_Cost/Text_Count","TextMeshWrapper")
    local text4 = self:GetChildComponent("Text_Tips","TextMeshWrapper")
    text1.text = LanguageDataHelper.GetContent(13)
    text4.text = LanguageDataHelper.GetContent(76210)

    self._iconGo = self:FindChildGO("Container_Fragment/Container_Icon")
    self._textFragment= self:GetChildComponent("Container_Fragment/Text_Count","TextMeshWrapper")

    self._costiconGo = self:FindChildGO("Container_Cost/Container_Icon")

    local btnCon = self:FindChildGO("Button_Confirm")
    self.btnBuy = self:FindChildGO("Button_Bug")
    self._csBH:AddClick(btnCon,function() self:onBtnCon() end)
    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30009")

    self._sigeRpc = function() self:onBtnBuySigle()end
    self._tenRpc = function() self:onBtnBuyTen()end
end

function RuneReportView:onBtnCon()
    self:OnClose()
end

function RuneReportView:onBtnBuyTen()
    self:OnClose()    
    EventDispatcher:TriggerEvent(GameEvents.RuneFindRpc,action_config.RUNE_TEN_LUCK)
end

function RuneReportView:onBtnBuySigle()
    self:OnClose()    
    EventDispatcher:TriggerEvent(GameEvents.RuneFindRpc,action_config.RUNE_SINGLE_LUCK)
end

function RuneReportView:OnDestroy()
    self._csBH = nil
end

function RuneReportView:Update()
  
end

function RuneReportView:OnClose()
    self._iconBg1=nil
    self._iconBg2=nil
    self.gameObject:SetActive(false) 
    self._list:RemoveAll()
    self._fx:Stop()
    self._csBH:RemoveClick(self.btnBuy)
end

function RuneReportView:OnShow()
    self._iconBg1 = self:FindChildGO("Image_Bg1")
    self._iconBg2 = self:FindChildGO("Image_Bg2")
    GameWorld.AddIcon(self._iconBg1,13523)
    GameWorld.AddIcon(self._iconBg2,13523)
    GameWorld.PlaySound(15)
    local CostId=0
    local CostNum=0
    self._fx:Play(true,false)
    local data = PreciousData:GetRuneFindGet()
    local d1 = data[1]
    local d2 = data[2]
    local action_id = data[3]
    if action_id == action_config.RUNE_SINGLE_LUCK then
        self.textbuy.text = LanguageDataHelper.GetContent(76227)
        self._csBH:AddClick(self.btnBuy,function() self:onBtnBuySigle() end)
        local cost = GlobalParamsHelper.GetParamValue(610)
        CostId,CostNum=next(cost)
        GameWorld.AddIcon(self._costiconGo,ItemDataHelper.GetIcon(CostId))
        self._text3.text = CostNum..''
    else
        self.textbuy.text = LanguageDataHelper.GetContent(76211)
        self._csBH:AddClick(self.btnBuy,function() self:onBtnBuyTen() end)
        local cost = GlobalParamsHelper.GetParamValue(611)
        CostId,CostNum=next(cost)
        GameWorld.AddIcon(self._costiconGo,ItemDataHelper.GetIcon(CostId))
        self._text3.text = CostNum..''
    end
    GameWorld.AddIcon(self._iconGo,d1[1])
    self._textFragment.text = d1[2]
    self._list:RemoveAll()
    local a={}
    if #d2 < 4 then
        local itemdata={}
        for i=#d2+1,3 do
            table.insert(itemdata,-1)
        end
        for k,v in pairs(d2) do
            table.insert(itemdata,v)
        end
        a=itemdata
        self._list:SetDataList(itemdata)
    else
        a=d2
        self._list:SetDataList(d2)
    end
    local len = self._list:GetLength()
    for i = 1, len do
        self._list:GetItem(i-1):Hide()
    end
    self:ShowTweenReward(self._list, a, 0)
end

function RuneReportView:ShowTweenReward(list, listData, index)
    local len = list:GetLength()
    if index ~= len then
        local num = perShowRewardNum
        if index + perShowRewardNum >= len then
            --最终剩余数量少于设定值
            num = len - index
        end
        
        for i = 1, num do
            list:GetItem(index + i - 1):DoShow(listData[index + i])
            if i == num and index + i ~= len then
                if index ~= 0 and (index + 1) % Count_Per_Row == 0 then
                    list:SetRow(index / Count_Per_Row + 1)
                end
                TimerHeap:AddSecTimer(perTime, 0, 0, function()
                    self:ShowTweenReward(list, listData, index + i)
                end)
            end
        end
    end
end
