-- PeakFindReportView.lua
require "Modules.ModulePrecious.ChildComponent.EquipFindReportItem"
local PeakFindReportView = Class.PeakFindReportView(ClassTypes.BaseComponent)
PeakFindReportView.interface = GameConfig.ComponentsConfig.Component
PeakFindReportView.classPath = "Modules.ModulePrecious.ChildView.PeakFindReportView"
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local PreciousData = PlayerManager.PlayerDataManager.preciousData
local PreciousManager = PlayerManager.PreciousManager
local public_config = GameWorld.public_config
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local XArtNumber = GameMain.XArtNumber
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local EquipFindReportItem = ClassTypes.EquipFindReportItem
local UIList = ClassTypes.UIList
local UIParticle = ClassTypes.UIParticle
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local perShowRewardNum = 1
local perTime = 0.1
local Count_Per_Row = 5

function PeakFindReportView:Awake()
    self:InitView()
    self.show = false
end
    
function PeakFindReportView:OnDestroy()

end

function PeakFindReportView:ShowView(data)
    self._iconBg1 = self:FindChildGO("Image_Bg1")
    self._iconBg2 = self:FindChildGO("Image_Bg2")
    GameWorld.AddIcon(self._iconBg1,13523)
    GameWorld.AddIcon(self._iconBg2,13523)
    GameWorld.PlaySound(15)

    local listData = self:GetFixData(data)
    self._list:SetDataList(listData)
    self._list:SetCenter(-40, false)
    self._list:MoveToTop()

    local len = self._list:GetLength()
    for i = 1, len do
        self._list:GetItem(i - 1):Hide()
    end
    self.show = true
    self:ShowTweenReward(self._list, listData, 0)

    EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Fx, false)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModulePrecious.ChildView.PeakFindReportView")
end

function PeakFindReportView:CloseView()
    self._iconBg1=nil
    self._iconBg2=nil
    EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Fx, true)
    self.show = false
end

function PeakFindReportView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(EquipFindReportItem)
    self._list:SetPadding(10, 0, 0, 25)
    self._list:SetGap(10, 20)
    self._list:SetDirection(UIList.DirectionLeftToRight, 5, -1)

    local btnCon = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(btnCon,function() self:onBtnCon() end)

    self._fx1 = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30009")
end

function PeakFindReportView:onBtnCon()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_peak_find_report_ok_button")
    self:CloseView()
    self.gameObject:SetActive(false)
end

function PeakFindReportView:GetFixData(data)
    local result = {}
    for i=1,#data do
        if self:IsSpecailItem(data[i]) then
            table.insert(result, 1, data[i])
        else
            table.insert(result, data[i])
        end
    end
    return result
end

function PeakFindReportView:IsSpecailItem(itemId)
    local cfg = GlobalParamsHelper.GetParamValue(858)
    for i=1,#cfg do
        if cfg[i] == itemId then
            return true
        end
    end
    return false
end

function PeakFindReportView:ShowTweenReward(list, listData, index)
    if not self.show then
        return
    end
    local len = list:GetLength()
    if index ~= len then
        local num = perShowRewardNum
        if index + perShowRewardNum >= len then
            --最终剩余数量少于设定值
            num = len - index
        end
        
        for i = 1, num do
            list:GetItem(index + i - 1):DoShow(listData[index + i])
            if i == num and index + i ~= len then
                if index ~= 0 and (index + 1) % Count_Per_Row == 0 then
                    list:SetRow(index / Count_Per_Row + 1)
                end
                TimerHeap:AddSecTimer(perTime, 0, 0, function()
                    self:ShowTweenReward(list, listData, index + i)
                end)
            end
        end
    end
end