local EquipFindWarehouseItem = Class.EquipFindWarehouseItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
EquipFindWarehouseItem.interface = GameConfig.ComponentsConfig.Component
EquipFindWarehouseItem.classPath = "Modules.ModulePrecious.ChildComponent.EquipFindWarehouseItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local TipsManager = GameManager.TipsManager

function EquipFindWarehouseItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function EquipFindWarehouseItem:OnDestroy()

end

--override
function EquipFindWarehouseItem:OnRefreshData(data)
    self.data = data
    if data == "nil" then
        self._icon:SetItemData(nil)
        return
    end
    self._icon:SetItemData(data)
end

function EquipFindWarehouseItem:InitView()
    local parent = self:FindChildGO("Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function() 
        if self.data == nil or self.data.cfg_id == nil then
            return
        end
        local btnArgs = {}
	    btnArgs.get = true
	    TipsManager:ShowItemTips(self.data, btnArgs)
    end
    self._icon:SetPointerDownCB(self._func)
end