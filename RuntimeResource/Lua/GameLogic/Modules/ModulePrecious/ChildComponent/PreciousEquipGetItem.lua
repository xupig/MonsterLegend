local PreciousEquipGetItem = Class.PreciousEquipGetItem(ClassTypes.UIListItem)
PreciousEquipGetItem.interface = GameConfig.ComponentsConfig.Component
PreciousEquipGetItem.classPath = "Modules.ModulePrecious.ChildComponent.PreciousEquipGetItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RuneDataHelper = GameDataHelper.RuneDataHelper
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local TipsManager = GameManager.TipsManager
local ItemConfig = GameConfig.ItemConfig
local FontStyleHelper = GameDataHelper.FontStyleHelper

function PreciousEquipGetItem:Awake()
    self:InitView()
end

function PreciousEquipGetItem:InitView()
    self._infoText = self:GetChildComponent("Text_Get", "TextMeshWrapper")

    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Text_Get')
    self._linkTextMesh:SetOnClickCB(function(linkIndex) self:OnClickLink(linkIndex) end)
end

function PreciousEquipGetItem:OnDestroy() 

end
--data {dbid,name,items}
function PreciousEquipGetItem:OnRefreshData(data)
    local itemId, count = next(data[3]) 
    local itemName = ItemDataHelper.GetItemNameWithColor(itemId)
    local quality = ItemDataHelper.GetQuality(itemId)
    local colorId = ItemConfig.QualityTextMap[quality]
    local color = FontStyleHelper.GetFontStyleData(colorId).color
    local arg = {}
    arg["0"] = itemId
    arg["1"] = color
    arg["2"] = itemName
    local sendText = LanguageDataHelper.CreateContentWithArgs(80532,arg)
    local showStr = LanguageDataHelper.CreateContentWithArgs(58706, {["0"]=data[2], ["1"]=sendText, ["2"]=count})
    self._infoText.text = showStr
end

function PreciousEquipGetItem:OnClickLink(linkId)
    if linkId ~= "" then
        TipsManager:ShowItemTipsById(tonumber(linkId))
    end
end



