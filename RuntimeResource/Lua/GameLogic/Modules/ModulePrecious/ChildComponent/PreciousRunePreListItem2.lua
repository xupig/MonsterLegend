-- PreciousRunePreListItem2.lua
require "UIComponent.Extend.ItemGrid"
local PreciousRunePreListItem2 = Class.PreciousRunePreListItem2(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
PreciousRunePreListItem2.interface = GameConfig.ComponentsConfig.Component
PreciousRunePreListItem2.classPath = "Modules.ModulePrecious.ChildComponent.PreciousRunePreListItem2"
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil

function PreciousRunePreListItem2:Awake()
    self._ContainerItem = self:FindChildGO("Container_Item")
    local a = GUIManager.AddItem(self._ContainerItem, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._icon:ActivateTipsById()
end

function PreciousRunePreListItem2:OnDestroy() 

end

--override
function PreciousRunePreListItem2:OnRefreshData(data)
    self._icon:SetItem(data)
end

