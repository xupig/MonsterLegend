-- EquipFindReportItem.lua
require "UIComponent.Extend.ItemGrid"
local EquipFindReportItem = Class.EquipFindReportItem(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
EquipFindReportItem.interface = GameConfig.ComponentsConfig.Component
EquipFindReportItem.classPath = "Modules.ModulePrecious.ChildComponent.EquipFindReportItem"
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIParticle = ClassTypes.UIParticle

function EquipFindReportItem:Awake()
    self._ContainerItem = self:FindChildGO("Container_Item")
    local a = GUIManager.AddItem(self._ContainerItem, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._icon:ActivateTipsById()
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._textName.text = ""

    self._fx_Container = self:FindChildGO("Container_Fx")
    self._fx_ui_30037 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30037")
    self._fx_ui_30037:Stop()
end

function EquipFindReportItem:OnDestroy() 

end

--override
function EquipFindReportItem:OnRefreshData(data)

end

function EquipFindReportItem:DoShow(data)
    self._ContainerItem:SetActive(true)
    self._icon:SetItem(data)
    self._textName.text = ItemDataHelper.GetItemNameWithColor(data)
    self._fx_ui_30037:Play(true, true)
end

function EquipFindReportItem:Hide()
    self._fx_ui_30037:Stop()
    self._ContainerItem:SetActive(false)
    self._textName.text = ""
end
