require "UIComponent.Extend.ItemGrid"

local PreciousEquipRewardItem = Class.PreciousEquipRewardItem(ClassTypes.UIRollerItem)
PreciousEquipRewardItem.interface = GameConfig.ComponentsConfig.Component
PreciousEquipRewardItem.classPath = "Modules.ModulePrecious.ChildComponent.PreciousEquipRewardItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local EquipFindDataHelper = GameDataHelper.EquipFindDataHelper

function PreciousEquipRewardItem:Awake()
    self:InitView()
end

function PreciousEquipRewardItem:InitView()
    self._parent = self:FindChildGO("Container_Item")
    local itemGo = GUIManager.AddItem(self._parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function() EventDispatcher:TriggerEvent(GameEvents.Equip_Find_Stop_Roller)
                    TipsManager:ShowItemTipsById(self.itemId) 
                end
    self._icon:SetPointerDownCB(self._func)
end

function PreciousEquipRewardItem:OnDestroy() 

end

function PreciousEquipRewardItem:OnRefreshData(data)
    self.data = data
    self.itemId = EquipFindDataHelper:GetEquipFindItemId(data)
    self._icon:SetItem(self.itemId)
end



