-- PreciousListItem.lua
local PreciousListItem = Class.PreciousListItem(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
PreciousListItem.interface = GameConfig.ComponentsConfig.Component
PreciousListItem.classPath = "Modules.ModulePrecious.ChildComponent.PreciousListItem"
local UIParticle = ClassTypes.UIParticle
function PreciousListItem:Awake()
    self._unSelectContainer = self:FindChildGO("Container_UnSelect")
    self._selectContainer = self:FindChildGO("Container_Select")
    self._unSelectNameText = self:GetChildComponent("Container_UnSelect/Text_Name",'TextMeshWrapper')
    self._selectNameText = self:GetChildComponent("Container_Select/Text_Name",'TextMeshWrapper')
    local containerFx = self:FindChildGO("Container_Select/Container_Fx")
    UIParticle.AddParticle(containerFx, "fx_ui_30019")

    self._redPointImage = self:FindChildGO("Image_Red")
end

function PreciousListItem:OnDestroy() 

end

--override
function PreciousListItem:OnRefreshData(data)
    self._data=data
    self._unSelectNameText.text = data.name
    self._selectNameText.text = data.name
end

function PreciousListItem:SetSelect(flag)
    self._selectContainer:SetActive(flag)
    self._unSelectContainer:SetActive(not flag)
end

function PreciousListItem:ShowRedPoint(state)
    self._redPointImage:SetActive(state)
end

function PreciousListItem:GetType()
    if self._data and self._data.funcId then
        return self._data.funcId
    end
end