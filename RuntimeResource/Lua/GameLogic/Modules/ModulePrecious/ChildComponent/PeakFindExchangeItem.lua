local PeakFindExchangeItem = Class.PeakFindExchangeItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
PeakFindExchangeItem.interface = GameConfig.ComponentsConfig.Component
PeakFindExchangeItem.classPath = "Modules.ModulePrecious.ChildComponent.PeakFindExchangeItem"

local PeakFindDataHelper = GameDataHelper.PeakFindDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local QualityType = GameConfig.EnumType.QualityType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local TipsManager = GameManager.TipsManager
local LotteryPeakManager = PlayerManager.LotteryPeakManager

local MAX_TAG_COUNT = 4
local QUALITY_TO_ICON = {[QualityType.Green]=4120, [QualityType.Blue]=4121, [QualityType.Purple]=4122, [QualityType.Orange]=4123,
        [QualityType.Red]=4124, [QualityType.Pink]=4125}

function PeakFindExchangeItem:Awake()
    self:InitView()
end

function PeakFindExchangeItem:OnDestroy()

end

--override {id}
function PeakFindExchangeItem:OnRefreshData(data)
    self.data = data
    self:ShowItem(self.data[1])
end

function PeakFindExchangeItem:InitView()
    self._btnExchange = self:FindChildGO("Button_Exchange")
    self._csBH:AddClick(self._btnExchange, function () self:OnExchangeClick() end)

    self._lightIcon = self:FindChildGO("Container_Icon")
    self._iconContainer = self:FindChildGO("Container_Item")

    self.nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self.scoreText = self:GetChildComponent("Text_Score/Text_Value", "TextMeshWrapper")
end

function PeakFindExchangeItem:ShowSelected(state)
    if state then
        --显示tips
        local itemId = PeakFindDataHelper:GetEquipFindPointItemId(self.data[1])
        TipsManager:ShowItemTipsById(itemId)
    end
end

function PeakFindExchangeItem:ShowItem(id)
    local itemId = PeakFindDataHelper:GetEquipFindPointItemId(id)
    GameWorld.AddIcon(self._iconContainer, ItemDataHelper.GetIcon(itemId))
    local qualityIcon = QUALITY_TO_ICON[ItemDataHelper.GetQuality(itemId)]
    GameWorld.AddIcon(self._lightIcon, qualityIcon)

    local name = ItemDataHelper.GetItemNameWithColor(itemId)
    local score = PeakFindDataHelper:GetEquipFindPoint(id)
    self.nameText.text = name
    self.scoreText.text = score
end

function PeakFindExchangeItem:OnExchangeClick()
    LotteryPeakManager:LotteryEquipExchangeReq(self.data[1])
end