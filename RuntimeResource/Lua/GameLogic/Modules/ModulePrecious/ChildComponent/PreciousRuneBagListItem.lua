require "UIComponent.Extend.ItemGrid"
local PreciousRuneBagListItem = Class.PreciousRuneBagListItem(ClassTypes.UIListItem)
PreciousRuneBagListItem.interface = GameConfig.ComponentsConfig.Component
PreciousRuneBagListItem.classPath = "Modules.ModulePrecious.ChildComponent.PreciousRuneBagListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemGrid = ClassTypes.ItemGrid
local ItemDataHelper = GameDataHelper.ItemDataHelper

function PreciousRuneBagListItem:Awake()
    self._base.Awake(self)
    self._runeAtt = {};
    self._runetAttsGo = {};
    self:InitView()
end

function PreciousRuneBagListItem:InitView()
    local parent = self:FindChildGO("Container_Item")
    self._runetAttsGo[1] = self:FindChildGO("Text_Att1")
    self._runetAttsGo[2] = self:FindChildGO("Text_Att2")
    self._runeAtt[1] = self:GetChildComponent("Text_Att1", "TextMeshWrapper")
    self._runeAtt[2] = self:GetChildComponent("Text_Att2", "TextMeshWrapper")
    self._runeName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
end

function PreciousRuneBagListItem:OnDestroy() 
    self.Itemdata = nil
end

function PreciousRuneBagListItem:OnRefreshData(data)
    self.Itemdata = data
    self:Update(data)   
end

function PreciousRuneBagListItem:Update(data)
    local i = 1    
    local level = data.le
    local itemid = data.itemid
    local atts = data.atts
    local type = atts[0]
    local _data = atts[1]
    self._runetAttsGo[1]:SetActive(false)
    self._runetAttsGo[2]:SetActive(false)
    if type==0 then
        for k,v in pairs(_data) do 
            local content = v[0]
            local num = v[1]
            if v[2] == 1 then
                self._runetAttsGo[i]:SetActive(true)
                self._runeAtt[i].text = content.."  "..BaseUtil.GetColorString('+'..num,ColorConfig.J)
            else
                self._runetAttsGo[i]:SetActive(true)
                self._runeAtt[i].text = content.."  "..'+'..BaseUtil.GetColorString('+'..(num/100-num/100%0.01),ColorConfig.J).."%"
            end
            i = i+1                            
        end            
    elseif type==1 then
        local text = ''
        text = '+'..(_data/100-_data/100%0.01).."%"
        local msg  = BaseUtil.GetColorString(LanguageDataHelper.GetContent(1019)..' ',ColorConfig.B)
        self._runetAttsGo[1]:SetActive(true)
        self._runeAtt[1].text = msg..BaseUtil.GetColorString(text,ColorConfig.J)
    elseif type==2 then
        local text = ''        
        for k,v in pairs(_data) do 
            local id = v[0]
            local num = v[1]
            text = ItemDataHelper.GetItemName(id)
            text = text..''..num
        end
        text = '+'..text
        local msg  = BaseUtil.GetColorString(LanguageDataHelper.GetContent(229)..' ',ColorConfig.B)
        self._runetAttsGo[1]:SetActive(true)   
        self._runeAtt[1].text = msg..BaseUtil.GetColorString(text,ColorConfig.J)
    else
        self._runetAttsGo[1]:SetActive(true)   
        self._runetAttsGo[2]:SetActive(true)
        for k,v in pairs(_data[1]) do 
            local content = v[0]
            local num = v[1]
            if v[2] == 1 then
                self._runeAtt[0].text = content.."  "..BaseUtil.GetColorString('+'..num,ColorConfig.J)
            else
                self._runeAtt[0].text = content.."  "..BaseUtil.GetColorString('+'..(num/100-num/100%0.01)..'%',ColorConfig.J)
            end
            break                    
        end 
        local text = ''
        text = '+'..(_data[2]/100-_data[2]/100%0.01).."%"
        local msg  = BaseUtil.GetColorString(LanguageDataHelper.GetContent(1019)..' ',ColorConfig.B)     
        self._runeAtt[1].text = msg.."  "..BaseUtil.GetColorString(text,ColorConfig.J)                    
    end
    self._runeName.text = ItemDataHelper.GetItemNameWithColor(data.itemid).."Lv."..level
    self._icon:SetItem(itemid)    
end



