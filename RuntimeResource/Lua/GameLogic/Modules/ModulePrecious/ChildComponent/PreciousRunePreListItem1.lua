-- PreciousRunePreListItem1.lua
local PreciousRunePreListItem1 = Class.PreciousRunePreListItem1(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
PreciousRunePreListItem1.interface = GameConfig.ComponentsConfig.Component
PreciousRunePreListItem1.classPath = "Modules.ModulePrecious.ChildComponent.PreciousRunePreListItem1"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemGrid = ClassTypes.ItemGrid
local ItemDataHelper = GameDataHelper.ItemDataHelper
function PreciousRunePreListItem1:Awake()
    self._ContainerItem = self:FindChildGO("Container_Icon")
    local a = GUIManager.AddItem(self._ContainerItem, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._icon:ActivateTipsById()
    self._textName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
end

function PreciousRunePreListItem1:OnDestroy() 

end

--override
function PreciousRunePreListItem1:OnRefreshData(data)
    self._icon:SetItem(data)
    self._textName.text = ItemDataHelper.GetItemNameWithColor(data)
end

