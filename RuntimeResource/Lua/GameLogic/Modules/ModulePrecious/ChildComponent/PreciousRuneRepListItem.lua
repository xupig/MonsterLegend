-- PreciousRuneRepListItem.lua
require "UIComponent.Extend.ItemGrid"
local PreciousRuneRepListItem = Class.PreciousRuneRepListItem(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
PreciousRuneRepListItem.interface = GameConfig.ComponentsConfig.Component
PreciousRuneRepListItem.classPath = "Modules.ModulePrecious.ChildComponent.PreciousRuneRepListItem"
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIParticle = ClassTypes.UIParticle
function PreciousRuneRepListItem:Awake()
    self._ContainerItem = self:FindChildGO("Container_Item")
    local a = GUIManager.AddItem(self._ContainerItem, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._icon:ActivateTipsById()
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")

    self._fx_Container = self:FindChildGO("Container_Fx")
    self._fx_ui_30037 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30037")
    self._fx_ui_30037:Stop()
end

function PreciousRuneRepListItem:OnDestroy() 

end

function PreciousRuneRepListItem:DoShow(data) 
    if data~=-1 then
        self._ContainerItem:SetActive(true)
        self._fx_ui_30037:Play(true, true)
        self._icon:SetItem(data)
        self._textName.text = ItemDataHelper.GetItemNameWithColor(data)
    end
end

function PreciousRuneRepListItem:Hide()
    self._ContainerItem:SetActive(false)
    self._textName.text = ""
end

--override
function PreciousRuneRepListItem:OnRefreshData(data)
    if data== -1 then
        self._textName.text=''
        return 
    end
    self._icon:SetItem(data)
    self._textName.text = ItemDataHelper.GetItemNameWithColor(data)
end

