require "UIComponent.Extend.ItemGrid"
local PreciousRuneGetItem = Class.PreciousRuneGetItem(ClassTypes.UIListItem)
PreciousRuneGetItem.interface = GameConfig.ComponentsConfig.Component
PreciousRuneGetItem.classPath = "Modules.ModulePrecious.ChildComponent.PreciousRuneGetItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemGrid = ClassTypes.ItemGrid
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RuneDataHelper = GameDataHelper.RuneDataHelper

function PreciousRuneGetItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function PreciousRuneGetItem:InitView()
    self._runeget = self:GetChildComponent("Text_Get", "TextMeshWrapper")
end

function PreciousRuneGetItem:OnDestroy() 
    self._runeget=nil
end

function PreciousRuneGetItem:OnRefreshData(data)
    self._runeget.text = data[3]..'  寻宝获得'..ItemDataHelper.GetItemNameWithColor(RuneDataHelper:GetItemCom(data[2]))..'x1'
end

function PreciousRuneGetItem:Update()  

end



