require "UIComponent.Extend.ItemGrid"

local PreciousPeakRewardItem = Class.PreciousPeakRewardItem(ClassTypes.UIRollerItem)
PreciousPeakRewardItem.interface = GameConfig.ComponentsConfig.Component
PreciousPeakRewardItem.classPath = "Modules.ModulePrecious.ChildComponent.PreciousPeakRewardItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local PeakFindDataHelper = GameDataHelper.PeakFindDataHelper

function PreciousPeakRewardItem:Awake()
    self:InitView()
end

function PreciousPeakRewardItem:InitView()
    self._parent = self:FindChildGO("Container_Item")
    local itemGo = GUIManager.AddItem(self._parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function() EventDispatcher:TriggerEvent(GameEvents.Peak_Find_Stop_Roller)
                    TipsManager:ShowItemTipsById(self.itemId) 
                end
    self._icon:SetPointerDownCB(self._func)
end

function PreciousPeakRewardItem:OnDestroy() 

end

function PreciousPeakRewardItem:OnRefreshData(data)
    self.data = data
    self.itemId = PeakFindDataHelper:GetEquipFindItemId(data)
    self._icon:SetItem(self.itemId)
end



