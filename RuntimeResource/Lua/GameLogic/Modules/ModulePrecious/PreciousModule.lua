PreciousModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function PreciousModule.Init()
    GUIManager.AddPanel(PanelsConfig.Precious, "Panel_Precious", GUILayer.LayerUIPanel, "Modules.ModulePrecious.PreciousPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return PreciousModule