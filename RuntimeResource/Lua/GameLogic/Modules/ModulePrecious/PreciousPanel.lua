--PreciousPanel.lua
require "Modules.ModulePrecious.ChildView.RuneFindView"
require "Modules.ModulePrecious.ChildView.EquipFindView"
require "Modules.ModulePrecious.ChildView.PeakFindView"
require "Modules.ModulePrecious.ChildComponent.PreciousListItem"

local ClassTypes = ClassTypes
local PreciousPanel = Class.PreciousPanel(ClassTypes.BasePanel)
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local RuneFindView = ClassTypes.RuneFindView
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local public_config = GameWorld.public_config
local PreciousListItem = ClassTypes.PreciousListItem
local PreciousManager = PlayerManager.PreciousManager
local LotteryEquipManager = PlayerManager.LotteryEquipManager
local UIList = ClassTypes.UIList
local EquipFindView = ClassTypes.EquipFindView
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local RedPointManager = GameManager.RedPointManager
local PeakFindView = ClassTypes.PeakFindView

local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

function PreciousPanel:Awake()
    self._preselect = -1
    self._redNodes = {}
    self:InitFun()
    self:InitViews() 

end

function PreciousPanel:InitViews()
	self._views = {}
    self._viewGos = {}
    self._viewGos[public_config.FUNCTION_ID_SYMBOL_TREASURE] = self:FindChildGO("Container_RuneFind")
    self._runefindView = self:AddChildLuaUIComponent("Container_RuneFind", RuneFindView)
    self._views[public_config.FUNCTION_ID_SYMBOL_TREASURE]=self._runefindView

    self._viewGos[public_config.FUNCTION_ID_EQUIP_TREASURE] = self:FindChildGO("Container_EquipFind")
    self._equipfindView = self:AddChildLuaUIComponent("Container_EquipFind", EquipFindView)
    self._views[public_config.FUNCTION_ID_EQUIP_TREASURE]=self._equipfindView

    self._viewGos[public_config.FUNCTION_ID_PEAK_TREASURE] = self:FindChildGO("Container_PeakFind")
    self._peakfindView = self:AddChildLuaUIComponent("Container_PeakFind", PeakFindView)
    self._views[public_config.FUNCTION_ID_PEAK_TREASURE]=self._peakfindView

    self._bgGo = self:FindChildGO("Image_Bg")
    self:InitContentList() 
end

function PreciousPanel:InitFun()
    self._redFuns={}
    self._redFuns[public_config.FUNCTION_ID_EQUIP_TREASURE] = function(status) self:UpdateRedStatus(public_config.FUNCTION_ID_EQUIP_TREASURE,status) end
    self._redFuns[public_config.FUNCTION_ID_SYMBOL_TREASURE] = function(status) self:UpdateRedStatus(public_config.FUNCTION_ID_SYMBOL_TREASURE,status) end
end

function PreciousPanel:OnDestroy()
end

function PreciousPanel:OnShow(data)
    self._listData = PreciousManager:GetRankList()
    self._list:SetDataList(self._listData)
    self:AddRedNodesFun()
    if data and data['functionId'] then
        self:SeleView(data['functionId'])
    else
        self:SeleView(self:SeleUI())
    end   
end

function PreciousPanel:AddRedNodesFun()
    self._redNodes[public_config.FUNCTION_ID_EQUIP_TREASURE]=RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_EQUIP_TREASURE)
    self._redNodes[public_config.FUNCTION_ID_EQUIP_TREASURE]:SetUpdateRedPointFunc(self._redFuns[public_config.FUNCTION_ID_EQUIP_TREASURE])
    self._redNodes[public_config.FUNCTION_ID_SYMBOL_TREASURE]=RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_SYMBOL_TREASURE)
    self._redNodes[public_config.FUNCTION_ID_SYMBOL_TREASURE]:SetUpdateRedPointFunc(self._redFuns[public_config.FUNCTION_ID_SYMBOL_TREASURE])
end

function PreciousPanel:RemoveRedNodesFun()
    self._redNodes[public_config.FUNCTION_ID_EQUIP_TREASURE]=RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_EQUIP_TREASURE)
    self._redNodes[public_config.FUNCTION_ID_EQUIP_TREASURE]:SetUpdateRedPointFunc(nil)
    self._redNodes[public_config.FUNCTION_ID_SYMBOL_TREASURE]=RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_SYMBOL_TREASURE)
    self._redNodes[public_config.FUNCTION_ID_SYMBOL_TREASURE]:SetUpdateRedPointFunc(nil)
end
-- 
function PreciousPanel:SeleUI()
    local f1=FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_SYMBOL_TREASURE)
    local f2=FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_EQUIP_TREASURE)
    local f3=FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_PEAK_TREASURE)
    if f1 and f2 and f3 then
        -- local condition1 = FunctionOpenDataHelper:GetCondition(public_config.FUNCTION_ID_SYMBOL_TREASURE)
        -- local condition2 = FunctionOpenDataHelper:GetCondition(public_config.FUNCTION_ID_EQUIP_TREASURE)      
        -- if public_config.FUNCTION_ID_SYMBOL_TREASURE<public_config.FUNCTION_ID_EQUIP_TREASURE then
        --     return public_config.FUNCTION_ID_SYMBOL_TREASURE
        -- else
            return public_config.FUNCTION_ID_EQUIP_TREASURE
        -- end
    else
        if f2 then
            return public_config.FUNCTION_ID_EQUIP_TREASURE
        elseif f1 then
            return public_config.FUNCTION_ID_SYMBOL_TREASURE
        elseif f3 then
            return public_config.FUNCTION_ID_PEAK_TREASURE
        end
    end
end

function PreciousPanel:OnListItemClicked(idx)
    if self._preselect==idx then
        return
    end
    local item = self._list:GetItem(idx)
    item:SetSelect(true)
    local t=item:GetType()
    if not self._viewGos[t].activeSelf then
        self._viewGos[t]:SetActive(true)
    end
    if self._preselect~=-1 then
        self._list:GetItem(self._preselect):SetSelect(false)
        if t==public_config.FUNCTION_ID_EQUIP_TREASURE then   
            self._viewGos[public_config.FUNCTION_ID_SYMBOL_TREASURE]:SetActive(false)
            self._views[public_config.FUNCTION_ID_SYMBOL_TREASURE]:OnClose()
            self._viewGos[public_config.FUNCTION_ID_PEAK_TREASURE]:SetActive(false)
            self._views[public_config.FUNCTION_ID_PEAK_TREASURE]:OnClose()
            self._views[public_config.FUNCTION_ID_EQUIP_TREASURE]:ShowFx(true)   
            self._bgGo:SetActive(false) 
        elseif t==public_config.FUNCTION_ID_SYMBOL_TREASURE then
            self._viewGos[public_config.FUNCTION_ID_EQUIP_TREASURE]:SetActive(false)
            self._views[public_config.FUNCTION_ID_EQUIP_TREASURE]:OnClose()
            self._viewGos[public_config.FUNCTION_ID_PEAK_TREASURE]:SetActive(false)
            self._views[public_config.FUNCTION_ID_PEAK_TREASURE]:OnClose()
            self._views[public_config.FUNCTION_ID_EQUIP_TREASURE]:ShowFx(false)    
            self._bgGo:SetActive(true)
        elseif t==public_config.FUNCTION_ID_PEAK_TREASURE then
            self._viewGos[public_config.FUNCTION_ID_EQUIP_TREASURE]:SetActive(false)
            self._views[public_config.FUNCTION_ID_EQUIP_TREASURE]:OnClose()
            self._viewGos[public_config.FUNCTION_ID_SYMBOL_TREASURE]:SetActive(false)
            self._views[public_config.FUNCTION_ID_SYMBOL_TREASURE]:OnClose()
            self._views[public_config.FUNCTION_ID_PEAK_TREASURE]:ShowFx(true)    
            self._bgGo:SetActive(false)
        end
    end
    self._preselect = idx
    self._views[t]:OnShow()
end

function PreciousPanel:SeleView(index)
    if not self._viewGos[index].activeSelf then
        self._viewGos[index]:SetActive(true)
    end
    self._views[index]:OnShow()        
    if self._listData[1].funcId== index then
        self._preselect=0
        self._list:GetItem(0):SetSelect(true)
    else
        self._preselect=1
        self._list:GetItem(1):SetSelect(true)
    end
    if index==public_config.FUNCTION_ID_EQUIP_TREASURE then
        self._views[public_config.FUNCTION_ID_EQUIP_TREASURE]:ShowFx(true)    
        self._bgGo:SetActive(false)
    else
        self._bgGo:SetActive(true)
    end
end

function PreciousPanel:OnClose()
    if self._preselect~=-1 then
        self._list:GetItem(self._preselect):SetSelect(false)
    end
    self._viewGos[public_config.FUNCTION_ID_SYMBOL_TREASURE]:SetActive(false)
    self._views[public_config.FUNCTION_ID_SYMBOL_TREASURE]:OnClose()

    self._viewGos[public_config.FUNCTION_ID_EQUIP_TREASURE]:SetActive(false)
    self._views[public_config.FUNCTION_ID_EQUIP_TREASURE]:OnClose()

    self._viewGos[public_config.FUNCTION_ID_PEAK_TREASURE]:SetActive(false)
    self._views[public_config.FUNCTION_ID_PEAK_TREASURE]:OnClose()

    self._preselect=-1
    self:RemoveRedNodesFun() 
end


function PreciousPanel:InitContentList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Tab")
	self._list = list
	self._listlView = scrollView 
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(true)
    self._list:SetItemType(PreciousListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)
    local itemClickedCB = function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
end

function PreciousPanel:UpdateRedStatus(funid,status)
    for i=1,#self._listData do
        local itemdata = self._listData[i]
        local item = self._list:GetItem(i-1)
        if itemdata.funcId==funid then
            item:ShowRedPoint(status)       
        end
    end 
end

--tab点击处理
function PreciousPanel:OnTabClick(index)
    
end

function PreciousPanel:CloseBtnClick()
    
end

function PreciousPanel:BaseShowModel()
    if self._preselect~=-1 then
        local item = self._list:GetItem(self._preselect)
        local t=item:GetType()
        if t==public_config.FUNCTION_ID_EQUIP_TREASURE then
            self._views[public_config.FUNCTION_ID_EQUIP_TREASURE]._modelComponent:Show()
        end
    end
end

function PreciousPanel:BaseHideModel()
    if self._preselect~=-1 then
        local item = self._list:GetItem(self._preselect)
        local t=item:GetType()
        if t==public_config.FUNCTION_ID_EQUIP_TREASURE then
            self._views[public_config.FUNCTION_ID_EQUIP_TREASURE]._modelComponent:Hide()
        end
    end
end

-- -- 0 没有  1 普通底板
function PreciousPanel:WinBGType()
    return PanelWinBGType.FullSceenBG
end

