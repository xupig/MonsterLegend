--仓库记录
local GuildWarehouseRecordItem = Class.GuildWarehouseRecordItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildWarehouseRecordItem.interface = GameConfig.ComponentsConfig.Component
GuildWarehouseRecordItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildWarehouseRecordItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local TipsManager = GameManager.TipsManager
local ItemConfig = GameConfig.ItemConfig
local FontStyleHelper = GameDataHelper.FontStyleHelper

function GuildWarehouseRecordItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function GuildWarehouseRecordItem:OnDestroy()
    self.data = nil
end

function GuildWarehouseRecordItem:InitView()
    self._rectTransform = self:GetComponent(typeof(UnityEngine.RectTransform))
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")

    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Text_Desc')
    self._linkTextMesh:SetOnClickCB(function(linkIndex) self:OnClickLink(linkIndex) end)
end

--override 
function GuildWarehouseRecordItem:OnRefreshData(data)
    self.data = data
    local showStr = ""
    local type = self.data[public_config.GUILD_WAREHOUSE_RECORD_TYPE]
    local desc = GlobalParamsHelper.GetParamValue(598)[type]
    local time = DateTimeUtil.SomeDay(self.data[public_config.GUILD_WAREHOUSE_RECORD_TIME])
    local name = self.data[public_config.GUILD_WAREHOUSE_RECORD_AVATAR_NAME]
    local itemId = self.data[public_config.GUILD_WAREHOUSE_RECORD_ITEM_ID]
    local itemName = ItemDataHelper.GetItemNameWithColor(itemId)
    local timeStr = string.format("%02d-%02d %02d:%02d", time.month, time.day, time.hour, time.min)

    local quality = ItemDataHelper.GetQuality(itemId)
    local colorId = ItemConfig.QualityTextMap[quality]
    local color = FontStyleHelper.GetFontStyleData(colorId).color
    local arg = {}
    arg["0"] = itemId
    arg["1"] = color
    arg["2"] = itemName
    local sendText = LanguageDataHelper.CreateContentWithArgs(80532,arg)

    showStr = LanguageDataHelper.CreateContentWithArgs(desc, {["0"]=timeStr, ["1"]=name, ["2"]=sendText})
    self._descText.text = showStr

    local height = self._descText.preferredHeight
	self:UpdateItemHeight(height)
end

function GuildWarehouseRecordItem:OnClickLink(linkId)
    if linkId ~= "" then
        TipsManager:ShowItemTipsById(tonumber(linkId))
    end
end

function GuildWarehouseRecordItem:UpdateItemHeight(height)
    local size = self._rectTransform.sizeDelta
    size.y = height + 5
    self._rectTransform.sizeDelta = size
end