local GuildRedEnvelopeItem = Class.GuildRedEnvelopeItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildRedEnvelopeItem.interface = GameConfig.ComponentsConfig.Component
GuildRedEnvelopeItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildRedEnvelopeItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local guildData = PlayerManager.PlayerDataManager.guildData

--0:未领取 1:自己可发放 2：已领取 3：别人未发放 4：已领完
local Can_Get = 0
local Can_Send = 1
local Has_Gotten = 2
local Wait_Send = 3
local No_Leave = 4

function GuildRedEnvelopeItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self.state = No_Leave
    self:InitView()
end

function GuildRedEnvelopeItem:OnDestroy()
    self.data = nil
end

function GuildRedEnvelopeItem:InitView()
    self._actionButton = self:FindChildGO("Button_Action")
    self._csBH:AddClick(self._actionButton,function() self:OnActionButtonClick() end)
    self._sendImage = self:FindChildGO("Image_Send")
    self._getImage = self:FindChildGO("Image_Get")

    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
    self._tipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._redPointImage = self:FindChildGO("Image_RedPoint")

    self._rpIconObj = self:FindChildGO("Container_RedPaperIcon")
    GameWorld.AddIcon(self._rpIconObj,13552)
end

--override 
function GuildRedEnvelopeItem:OnRefreshData(data)
    self.data = data[1] or {}
    self:RefreshRedEnvelopeState()
    self:ShowItemInfo()
end

function GuildRedEnvelopeItem:OnActionButtonClick()
    if self.state == Can_Get then
        --领取
        EventDispatcher:TriggerEvent(GameEvents.Show_Get_Red_Envelope_View, self.data)
    elseif self.state == Can_Send then
        --发放
        EventDispatcher:TriggerEvent(GameEvents.Show_Fix_Send_Red_Envelope_View, self.data)
    elseif self.state == No_Leave then
        --已领完
        EventDispatcher:TriggerEvent(GameEvents.Show_Red_Envelope_Info_View, self.data)
    elseif self.state == Has_Gotten then
        EventDispatcher:TriggerEvent(GameEvents.Show_Red_Envelope_Info_View, self.data)
    end
end

function GuildRedEnvelopeItem:ShowItemInfo()
    local vocation = self.data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_VOC] or 100
    local headIcon = RoleDataHelper.GetHeadIconByVocation(vocation)
    local cfgId = self.data[public_config.GUILD_RED_ENVELOPE_INFO_CFG_ID]
    local name = self.data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_NAME]
    local desc = self.data[public_config.GUILD_RED_ENVELOPE_INFO_BLESSING] or GuildDataHelper:GetGuildRedEnvelopeName(cfgId)
    GameWorld.AddIcon(self._iconContainer, headIcon, nil)
    self._nameText.text = name
    self._descText.text = desc

    if self.state == Can_Get then
        self._tipsText.text = ""
        self._sendImage:SetActive(false)
        self._getImage:SetActive(true)
        self._redPointImage:SetActive(true)
    elseif self.state == Can_Send then
        self._tipsText.text = ""
        self._sendImage:SetActive(true)
        self._getImage:SetActive(false)
        self._redPointImage:SetActive(true)
    elseif self.state == Has_Gotten then
        self._tipsText.text = LanguageDataHelper.CreateContent(53138)
        self._sendImage:SetActive(false)
        self._getImage:SetActive(false)
        self._redPointImage:SetActive(false)
    elseif self.state == Wait_Send then
        self._tipsText.text = LanguageDataHelper.CreateContent(53139)
        self._sendImage:SetActive(false)
        self._getImage:SetActive(false)
        self._redPointImage:SetActive(false)
    elseif self.state == No_Leave then
        self._tipsText.text = LanguageDataHelper.CreateContent(53140)
        self._sendImage:SetActive(false)
        self._getImage:SetActive(false)
        self._redPointImage:SetActive(false)
    end
end

function GuildRedEnvelopeItem:RefreshRedEnvelopeState()
    local count = self.data[public_config.GUILD_RED_ENVELOPE_INFO_GOTTEN_MONEY]
    if count >= self.data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_COUNT] then
        self.state = No_Leave
        return
    end
    if self.data[public_config.GUILD_RED_ENVELOPE_INFO_IS_SEND] == 0 then
        if self:IsOwnRedEnvelope() then
            self.state = Can_Send
            return
        else
            self.state = Wait_Send
            return
        end
    end
    if self:HasGotten() then
        self.state = Has_Gotten
        return
    end
    self.state = Can_Get
end

function GuildRedEnvelopeItem:IsOwnRedEnvelope()
    if GameWorld.Player().dbid == self.data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_DBID] then
        return true
    end
    return false
end

function GuildRedEnvelopeItem:HasGotten()
    local record = self.data[public_config.GUILD_RED_ENVELOPE_INFO_SET] or {}
    for k,v in pairs(record) do
        if GameWorld.Player().dbid == v[1] then
            return true
        end
    end
    return false
end