
local GuildQuitConfirmItem = Class.GuildQuitConfirmItem(ClassTypes.BaseLuaUIComponent)

GuildQuitConfirmItem.interface = GameConfig.ComponentsConfig.Component
GuildQuitConfirmItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildQuitConfirmItem"

local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local guildData = PlayerManager.PlayerDataManager.guildData
local GuildManager = PlayerManager.GuildManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function GuildQuitConfirmItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end

function GuildQuitConfirmItem:OnShow()
    self:ShowInfo()
end

function GuildQuitConfirmItem:InitView()
    self._bgButton = self:FindChildGO("Container_PopupSmall/Button_Close")
    self._csBH:AddClick(self._bgButton,function() self:OnCloseButtonClick() end)
    self._confirmButton = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(self._confirmButton, function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Button_Cancel")
    self._csBH:AddClick(self._cancelButton, function() self:OnCancelButtonClick() end)

    self._noticeText = self:GetChildComponent("Text_Notice", "TextMeshWrapper")
end

function GuildQuitConfirmItem:OnConfirmButtonClick()
    GuildManager:GuildQuitdReq()
    self.gameObject:SetActive(false)
end

function GuildQuitConfirmItem:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function GuildQuitConfirmItem:OnCancelButtonClick()
    self.gameObject:SetActive(false)
end

function GuildQuitConfirmItem:ShowInfo()
    if GuildManager:IsPresident() then
        self._noticeText.text = LanguageDataHelper.CreateContent(53134)
    else
        self._noticeText.text = LanguageDataHelper.CreateContent(53133)
    end
end