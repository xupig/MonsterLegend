local GuildPlayerActionItem = Class.GuildPlayerActionItem(ClassTypes.BaseLuaUIComponent)

GuildPlayerActionItem.interface = GameConfig.ComponentsConfig.PointableComponent
GuildPlayerActionItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildPlayerActionItem"

local GlobalParamsHelper =  GameDataHelper.GlobalParamsHelper
local LanguageDataHelper =  GameDataHelper.LanguageDataHelper

function GuildPlayerActionItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function GuildPlayerActionItem:OnDestroy() 

end

function GuildPlayerActionItem:InitView()
    self._descText = self:GetChildComponent("Text", "TextMeshWrapper")
end

function GuildPlayerActionItem:OnPointerDown(pointerEventData)
    EventDispatcher:TriggerEvent(GameEvents.Guild_Action_Resp, self.data)
end

function GuildPlayerActionItem:OnPointerUp(pointerEventData)

end

--data 公会成员信息
function GuildPlayerActionItem:SetData(data)
    self.data = data
    local actionTextData = GlobalParamsHelper.GetParamValue(601)
    self._descText.text = LanguageDataHelper.GetContent(actionTextData[data])
end