local SkillListItem = Class.SkillListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
SkillListItem.interface = GameConfig.ComponentsConfig.Component
SkillListItem.classPath = "Modules.ModuleGuild.ChildComponent.SkillListItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper

function SkillListItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function SkillListItem:OnDestroy()
    self.data = nil
end

function SkillListItem:InitView()
    self._iconContainer = self:FindChildGO("Container_content/Image_Skill/Container_Icon")
    self._selectImage = self:FindChildGO("Container_content/Image_Selected")
    self._lockImage = self:FindChildGO("Container_content/Image_Lock")
    self._levelText = self:GetChildComponent("Container_content/Text_Level", "TextMeshWrapper")
    self._lockText = self:GetChildComponent("Container_content/Text_Lock", "TextMeshWrapper")
    self._redPointImage = self:FindChildGO("Container_content/Image_RedPoint")
end

--override
function SkillListItem:OnRefreshData(data)
    self.data = data
    self._redPointImage:SetActive(false)
    local cfgData = GuildDataHelper:GetGuildSkillDataBy2(data.type, data.level)
    local maxLevel = GuildDataHelper:GetGuildSkillMaxLevel(data.type)
    local name = LanguageDataHelper.GetContent(cfgData.name)
    if data.unLockLevel == 0 then
        self._lockImage:SetActive(false)
        self._lockText.text = ""
        self._levelText.text = string.format("%s  %s%s", name, data.level, LanguageDataHelper.GetContent(69))
        GameWorld.AddIcon(self._iconContainer, cfgData.icon, nil)
        if GameWorld.Player().money_guild_contribute >= cfgData.cost and data.level < maxLevel then
            self._redPointImage:SetActive(true)
        end
    else
        self._lockImage:SetActive(true)
        self._lockText.text = LanguageDataHelper.CreateContentWithArgs(53081, {["0"]=data.unLockLevel})
        self._levelText.text = name
        GameWorld.AddIcon(self._iconContainer, cfgData.icon, 12)
    end
end

function SkillListItem:ShowSelected(state)
    self._selectImage:SetActive(state)
end