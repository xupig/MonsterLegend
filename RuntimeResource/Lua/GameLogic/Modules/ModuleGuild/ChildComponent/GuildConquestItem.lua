local GuildConquestItem = Class.GuildConquestItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildConquestItem.interface = GameConfig.ComponentsConfig.Component
GuildConquestItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildConquestItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildManager = PlayerManager.GuildManager
local StringStyleUtil = GameUtil.StringStyleUtil
local GuildConquestManager = PlayerManager.GuildConquestManager
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper

local Delta_Count = 3
local Unit_Count = 4
local Index_To_Grade = {[1]=53398, [2]=53399, [3]=53400, [4]=53401, [5]=53402}
local Index_To_Rank = {[1]="1-4", [2]="5-8", [3]="9-12", [4]="13-16", [5]="17-20"}

function GuildConquestItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function GuildConquestItem:OnDestroy()
    self.data = nil
end

function GuildConquestItem:InitView()
    self._playerInfoContainer = self:FindChildGO("Container_PlayerInfo")
    self._tipsInfoContainer = self:FindChildGO("Container_TipsInfo")

    self._titleText = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    self._tipsText = self:GetChildComponent("Container_TipsInfo/Text_Info", "TextMeshWrapper")

    self._playerText = {}
    self._playerVictor = {}
    for i=1,Unit_Count do
        self._playerText[i] = self:GetChildComponent("Container_PlayerInfo/Text_Player"..i, "TextMeshWrapper")
        self._playerVictor[i] = self:FindChildGO("Container_PlayerInfo/Text_Player"..i.."/Image_Victory")
    end
end

--override data:序号id 1-5档
function GuildConquestItem:OnRefreshData(data)
    self:RefreshTime()
    self.data = data
    local index = data
    local lang = Index_To_Grade[index]
    self._titleText.text = LanguageDataHelper.CreateContentWithArgs(lang)

    local listData = GuildConquestManager:GetListData()
    local beginIndex = self.data * Unit_Count - Delta_Count
    local endIndex = self.data * Unit_Count
    local idx = 0
    for i=beginIndex,endIndex do
        idx = i % Unit_Count
        if idx == 0 then
            idx = Unit_Count
        end
        local itemData = listData[i]
        if itemData == nil then
            self._playerText[idx].text = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53428), ColorConfig.C)
        else
            local colorStr = ColorConfig.A
            if itemData[3] == GuildManager:GetGuildId() then
                colorStr = ColorConfig.J
            end
            if itemData[2] == 0 then
                self._playerVictor[idx]:SetActive(false)
            elseif itemData[2] == 1 then
                self._playerVictor[idx]:SetActive(true)
            else
                self._playerVictor[idx]:SetActive(false)
                colorStr = ColorConfig.C
            end
            self._playerText[idx].text = BaseUtil.GetColorString(itemData[1], colorStr)
        end
    end

    if self._beforeFirstOpenDay then
        self._tipsText.text = LanguageDataHelper.CreateContentWithArgs(53416, {["0"]=Index_To_Rank[index]})
        self._playerInfoContainer:SetActive(false)
        self._tipsInfoContainer:SetActive(true)
    elseif self._openTime then
        self._playerInfoContainer:SetActive(true)
        self._tipsInfoContainer:SetActive(false)
    else
        if self.data == 5 then
            self._tipsText.text = LanguageDataHelper.CreateContentWithArgs(53178)
            self._playerInfoContainer:SetActive(false)
            self._tipsInfoContainer:SetActive(true)
        else
            self._playerInfoContainer:SetActive(true)
            self._tipsInfoContainer:SetActive(false)
        end
    end
end

function GuildConquestItem:RefreshTime()
    local cfg = GlobalParamsHelper.GetParamValue(673)
    local setDay = cfg[1]
    local openServerTime = DateTimeUtil.GetOpenServerTime()
    local curTime = DateTimeUtil.GetServerTime()

    local startTime
    local isSameDay = false
    for i=1,#cfg do
        startTime = DateTimeUtil.GetOpenServerTime() + (cfg[i] - 1) * 86400
        isSameDay = DateTimeUtil.SameDay(startTime)
        if isSameDay then
            break
        end
    end

    startTime = DateTimeUtil.GetOpenServerTime() + (cfg[1] - 1) * 86400  --开服第一次活动

    local setWDay = ActivityDailyDataHelper:GetDayLimit(public_config.ACTIVITY_ID_GUILD_CONQUEST)[1]

    self._openTime = false
    self._beforeFirstOpenDay = false
    if (not isSameDay) and curTime < startTime then
        self._beforeFirstOpenDay = true
    elseif isSameDay or DateTimeUtil.GetWday() == setWDay then
        --活动当天
        if self:IsInTime({0, 0, 19, 0}) then
            if isSameDay then
                self._beforeFirstOpenDay = true
            end
            self._openTime = false
        else
            self._openTime = true
        end
    end
end

--开始时、分与结束时、分
function GuildConquestItem:IsInTime(config)
    local now = DateTimeUtil.Now()
    if (now.hour > config[1] or (now.hour == config[1] and now.min >= config[2])) and
        (now.hour < config[3] or (now.hour == config[3] and now.min < config[4])) then
        return true
    end
    return false
end