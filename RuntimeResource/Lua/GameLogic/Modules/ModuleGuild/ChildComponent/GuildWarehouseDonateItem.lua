local GuildWarehouseDonateItem = Class.GuildWarehouseDonateItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildWarehouseDonateItem.interface = GameConfig.ComponentsConfig.Component
GuildWarehouseDonateItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildWarehouseDonateItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local bagData = PlayerManager.PlayerDataManager.bagData
local TipsManager = GameManager.TipsManager

function GuildWarehouseDonateItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function GuildWarehouseDonateItem:OnDestroy()
    self.data = nil
end

--override
function GuildWarehouseDonateItem:OnRefreshData(data)
    self.data = data
    local count = 0
    --兼容空格子
    if data.cfg_id ~= nil then
        count = self.data:GetCount()
    end
    self._icon:SetItem(data.cfg_id, count)
    self:ShowSelected(false)
end

function GuildWarehouseDonateItem:InitView()
    local parent = self:FindChildGO("Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._selectImage = self:FindChildGO("Image_Selected")
end

function GuildWarehouseDonateItem:ShowSelected(state)
    self._selectImage:SetActive(state)
end

function GuildWarehouseDonateItem:SetUseAndPowerCompare(showEquipUI,canNotUse,isUp)
	if self._imgCanNotUse == nil then
	 	self._imgCanNotUse = self:FindChildGO("Image_CanNotUse")
	 	self._imgArrowUp = self:FindChildGO("Image_ArrowUp")
	 	self._imgArrowDown = self:FindChildGO("Image_ArrowDown")
	end
	if showEquipUI then
		if canNotUse then
			self._imgCanNotUse:SetActive(true)
			self._imgArrowUp:SetActive(false)
			self._imgArrowDown:SetActive(false)
		else
			self._imgCanNotUse:SetActive(false)
			if isUp then
				self._imgArrowUp:SetActive(true)
				self._imgArrowDown:SetActive(false)
			else
				self._imgArrowUp:SetActive(false)
				self._imgArrowDown:SetActive(true)
			end
		end
	else
		self._imgCanNotUse:SetActive(false)
		self._imgArrowUp:SetActive(false)
		self._imgArrowDown:SetActive(false)
	end
end