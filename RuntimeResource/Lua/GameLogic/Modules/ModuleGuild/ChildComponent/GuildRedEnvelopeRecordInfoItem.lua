--红包发送记录
local GuildRedEnvelopeRecordInfoItem = Class.GuildRedEnvelopeRecordInfoItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildRedEnvelopeRecordInfoItem.interface = GameConfig.ComponentsConfig.Component
GuildRedEnvelopeRecordInfoItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildRedEnvelopeRecordInfoItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil

function GuildRedEnvelopeRecordInfoItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function GuildRedEnvelopeRecordInfoItem:OnDestroy()
    self.data = nil
end

function GuildRedEnvelopeRecordInfoItem:InitView()
    self._rectTransform = self:GetComponent(typeof(UnityEngine.RectTransform))
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
end

--override 
function GuildRedEnvelopeRecordInfoItem:OnRefreshData(data)
    self.data = data
    local showStr = ""
    local cfgId = self.data[public_config.GUILD_RED_ENVELOPE_RECORD_CFG_ID]
    local desc = GuildDataHelper:GetGuildRedEnvelopeRecordDesc(cfgId)
    local time = DateTimeUtil.SomeDay(self.data[public_config.GUILD_RED_ENVELOPE_RECORD_TIME])
    local name = self.data[public_config.GUILD_RED_ENVELOPE_RECORD_AVATAR_NAME]
    local count = self.data[public_config.GUILD_RED_ENVELOPE_RECORD_MONEY_COUNT]
    local timeStr = string.format("%02d-%02d %02d:%02d", time.month, time.day, time.hour, time.min)
    showStr = LanguageDataHelper.CreateContentWithArgs(desc, {["0"]=timeStr, ["1"]=name, ["2"]=count})
    self._descText.text = showStr

    local height = self._descText.preferredHeight
	self:UpdateItemHeight(height)
end

function GuildRedEnvelopeRecordInfoItem:UpdateItemHeight(height)
    local size = self._rectTransform.sizeDelta
    size.y = height + 5
    self._rectTransform.sizeDelta = size
end