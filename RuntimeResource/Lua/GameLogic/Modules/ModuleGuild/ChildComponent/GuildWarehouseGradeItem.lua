local GuildWarehouseGradeItem = Class.GuildWarehouseGradeItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildWarehouseGradeItem.interface = GameConfig.ComponentsConfig.Component
GuildWarehouseGradeItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildWarehouseGradeItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function GuildWarehouseGradeItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function GuildWarehouseGradeItem:OnDestroy()
    self.data = nil
end

--override --{阶数，中文字} 0表示清空选择 -1表示任意
function GuildWarehouseGradeItem:OnRefreshData(data)
    self.data = data
    self._infoText.text = LanguageDataHelper.GetLanguageData(data[2])
end

function GuildWarehouseGradeItem:InitView()
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
end

function GuildWarehouseGradeItem:ShowSelected(state)
    self._selected:SetActive(state)
end