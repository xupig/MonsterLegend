local SkillInfoItem = Class.SkillInfoItem(ClassTypes.BaseLuaUIComponent)

SkillInfoItem.interface = GameConfig.ComponentsConfig.Component
SkillInfoItem.classPath = "Modules.ModuleGuild.ChildComponent.SkillInfoItem"

local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager
local AttriDataHelper = GameDataHelper.AttriDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local BaseUtil = GameUtil.BaseUtil

function SkillInfoItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self._curPage = 0
end
--override
function SkillInfoItem:OnDestroy() 

end

function SkillInfoItem:InitView()
    self._levelUpButton = self:FindChildGO("Button_LevelUp")
    self._csBH:AddClick(self._levelUpButton,function () self:OnLevelUpButtonClick() end)
    self._redPointImage = self:FindChildGO("Button_LevelUp/Image_RedPoint")

    self._nameText = self:GetChildComponent("Image_Title/Text_Info", "TextMeshWrapper")
    self._levelText = self:GetChildComponent("Text_Level/Text_Value", "TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Image_Skill/Container_Icon")
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._nextLevelText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")

    self._costIcon = self:FindChildGO("Text_Cost/Container_Icon")
    self._costCountText = self:GetChildComponent("Text_Cost/Text_Value", "TextMeshWrapper")

    GameWorld.AddIcon(self._costIcon, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_GUILD_CONTRIBUTE), nil)
end

function SkillInfoItem:RefreshData(data)
    self.data = data
    self:ShowInfo()
end

function SkillInfoItem:OnLevelUpButtonClick()
    GuildManager:GuildSkillLevelUpReq(self.data.type)
end

function SkillInfoItem:ShowInfo()
    local cfgData = GuildDataHelper:GetGuildSkillDataBy2(self.data.type, self.data.level)
    local name = LanguageDataHelper.GetContent(cfgData.name)
    local maxLevel = GuildDataHelper:GetGuildSkillMaxLevel(self.data.type)
    local nextLevel = self.data.level + 1
    if nextLevel > maxLevel then
        nextLevel = maxLevel
    end
    local cfgDataNext = GuildDataHelper:GetGuildSkillDataBy2(self.data.type, nextLevel)

    GameWorld.AddIcon(self._iconContainer, cfgData.icon, nil)
    self._nameText.text = name
    self._levelText.text = self.data.level
    self._costCountText.text = BaseUtil.GetColorStringByCount(GameWorld.Player().money_guild_contribute, cfgData.cost)

    local attri, attriValue = next(cfgData.attri)
    local attriName = AttriDataHelper:GetName(attri)
    local attriType = AttriDataHelper:GetShowType(attri)
    local showValue = ""
    if attriType == 1 then
        showValue = attriValue
    else
        showValue = (attriValue*0.01) .."%"
    end
    self._infoText.text = string.format("%s +%s", attriName, showValue)

    local attriNext, attriValueNext = next(cfgDataNext.attri)
    local attriNameNext = AttriDataHelper:GetName(attriNext)
    local attriTypeNext = AttriDataHelper:GetShowType(attriNext)
    local showValueNext = ""
    if attriTypeNext == 1 then
        showValueNext = attriValueNext
    else
        showValueNext = (attriValueNext*0.01) .."%"
    end
    self._nextLevelText.text = string.format("%s +%s", attriNameNext, showValueNext)

    if GameWorld.Player().money_guild_contribute >= cfgData.cost and self.data.level < maxLevel then
        self._redPointImage:SetActive(true)
    else
        self._redPointImage:SetActive(false)
    end

end