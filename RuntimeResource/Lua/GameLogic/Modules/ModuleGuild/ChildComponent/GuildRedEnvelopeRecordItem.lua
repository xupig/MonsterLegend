local GuildRedEnvelopeRecordItem = Class.GuildRedEnvelopeRecordItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildRedEnvelopeRecordItem.interface = GameConfig.ComponentsConfig.Component
GuildRedEnvelopeRecordItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildRedEnvelopeRecordItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function GuildRedEnvelopeRecordItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function GuildRedEnvelopeRecordItem:OnDestroy()
    self.data = nil
end

function GuildRedEnvelopeRecordItem:InitView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._countText = self:GetChildComponent("Text_Count", "TextMeshWrapper")
    self._tipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Container_Icon")
end

--override data:{dbid,name,count,best,moneyType}
function GuildRedEnvelopeRecordItem:OnRefreshData(data)
    self.data = data
    if self.data[4] then
        self._tipsText.text = LanguageDataHelper.CreateContent(53141)
    else
        self._tipsText.text = ""
    end
    self._nameText.text = self.data[2]
    self._countText.text = self.data[3]
    local moneyIcon = ItemDataHelper.GetIcon(self.data[5])
    GameWorld.AddIcon(self._iconContainer, moneyIcon)
end