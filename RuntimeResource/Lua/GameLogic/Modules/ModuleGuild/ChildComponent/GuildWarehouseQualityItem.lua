local GuildWarehouseQualityItem = Class.GuildWarehouseQualityItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildWarehouseQualityItem.interface = GameConfig.ComponentsConfig.Component
GuildWarehouseQualityItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildWarehouseQualityItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function GuildWarehouseQualityItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function GuildWarehouseQualityItem:OnDestroy()
    self.data = nil
end

--override --{品质，中文字} 0表示清空选择
function GuildWarehouseQualityItem:OnRefreshData(data)
    self.data = data
    self._infoText.text = LanguageDataHelper.GetLanguageData(data[2])
end

function GuildWarehouseQualityItem:InitView()
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
end

function GuildWarehouseQualityItem:ShowSelected(state)
    self._selected:SetActive(state)
end