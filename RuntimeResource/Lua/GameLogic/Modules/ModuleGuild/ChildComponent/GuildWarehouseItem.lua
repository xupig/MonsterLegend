local GuildWarehouseItem = Class.GuildWarehouseItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildWarehouseItem.interface = GameConfig.ComponentsConfig.Component
GuildWarehouseItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildWarehouseItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local bagData = PlayerManager.PlayerDataManager.bagData
local TipsManager = GameManager.TipsManager

function GuildWarehouseItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self:InitView()
end

function GuildWarehouseItem:OnDestroy()
    self.data = nil
end

--override
function GuildWarehouseItem:OnRefreshData(data)
    self.data = data
    local count = 0
    --兼容空格子
    if data.cfg_id ~= nil then
        count = self.data:GetCount()
    end
    self._icon:SetItem(data.cfg_id, count)
    self:Reset()
end

function GuildWarehouseItem:InitView()
    local parent = self:FindChildGO("Container_content/Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._selectImage = self:FindChildGO("Container_content/Image_Selected")
end

function GuildWarehouseItem:ChangeState()
    if self.data.cfg_id == nil then
        return
    end
    self._select = not self._select
    self._selectImage:SetActive(self._select)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Warehouse_Selected_Equip_Item, self.data.bagPos, self._select)
end

function GuildWarehouseItem:ShowSelected(state)
    self._select = state
    self._selectImage:SetActive(state)
end

function GuildWarehouseItem:Reset()
    self._select = false
    self._selectImage:SetActive(self._select)
end

function GuildWarehouseItem:SetUseAndPowerCompare(showEquipUI,canNotUse,isUp)
	if self._imgCanNotUse == nil then
	 	self._imgCanNotUse = self:FindChildGO("Container_content/Image_CanNotUse")
	 	self._imgArrowUp = self:FindChildGO("Container_content/Image_ArrowUp")
	 	self._imgArrowDown = self:FindChildGO("Container_content/Image_ArrowDown")
	end
	if showEquipUI then
		if canNotUse then
			self._imgCanNotUse:SetActive(true)
			self._imgArrowUp:SetActive(false)
			self._imgArrowDown:SetActive(false)
		else
			self._imgCanNotUse:SetActive(false)
			if isUp then
				self._imgArrowUp:SetActive(true)
				self._imgArrowDown:SetActive(false)
			else
				self._imgArrowUp:SetActive(false)
				self._imgArrowDown:SetActive(true)
			end
		end
	else
		self._imgCanNotUse:SetActive(false)
		self._imgArrowUp:SetActive(false)
		self._imgArrowDown:SetActive(false)
	end
end