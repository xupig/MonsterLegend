local ApproveInfoItem = Class.ApproveInfoItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
ApproveInfoItem.interface = GameConfig.ComponentsConfig.Component
ApproveInfoItem.classPath = "Modules.ModuleGuild.ChildComponent.ApproveInfoItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildManager = PlayerManager.GuildManager
local RoleDataHelper = GameDataHelper.RoleDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local NobilityDataHelper = GameDataHelper.NobilityDataHelper

function ApproveInfoItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function ApproveInfoItem:OnDestroy()
    self.data = nil
end

function ApproveInfoItem:InitView()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._levelText = self:GetChildComponent("Text_Level", "TextMeshWrapper")
    self._vocationText = self:GetChildComponent("Text_Vocation", "TextMeshWrapper")
    self._fightPowerText = self:GetChildComponent("Text_FightPower", "TextMeshWrapper")
    self._acceptButton = self:FindChildGO("Button_Accept")
    self._csBH:AddClick(self._acceptButton,function() self:OnAcceptButtonClick() end)
    self._refuseButton = self:FindChildGO("Button_Refuse")
    self._csBH:AddClick(self._refuseButton,function() self:OnRefuseButtonClick() end)
end

--override
function ApproveInfoItem:OnRefreshData(data)
    self.data = data
    --爵位
    local nobility = data[public_config.GUILD_APPLY_INFO_NOBILITY] or 0
    if nobility ~= 0 then
        local icon = NobilityDataHelper:GetNobilityShow(nobility)
        self._iconContainer:SetActive(true)
        GameWorld.AddIcon(self._iconContainer, icon, nil)
    else
        self._iconContainer:SetActive(false)
    end

    local name = data[public_config.GUILD_APPLY_INFO_NAME]
    local level = data[public_config.GUILD_APPLY_INFO_LEVEL]
    local vocation = data[public_config.GUILD_APPLY_INFO_VOCATION]
    self._nameText.text = name
    local lvMark = LanguageDataHelper.GetContent(157)
    self._levelText.text = string.format("%s%d", lvMark, level)
    self._vocationText.text = RoleDataHelper.GetRoleNameTextByVocation(vocation)
    self._fightPowerText.text = StringStyleUtil.GetLongNumberString(data[public_config.GUILD_APPLY_INFO_FIGHT_FORCE])
end

function ApproveInfoItem:ShowSelected(state)

end

function ApproveInfoItem:OnAcceptButtonClick()
    local dbid = self.data[public_config.GUILD_APPLY_INFO_DBID]
    GuildManager:ReplyGuildApplyReq(dbid, 0)
end

function ApproveInfoItem:OnRefuseButtonClick()
    local dbid = self.data[public_config.GUILD_APPLY_INFO_DBID]
    GuildManager:ReplyGuildApplyReq(dbid, 1)
end