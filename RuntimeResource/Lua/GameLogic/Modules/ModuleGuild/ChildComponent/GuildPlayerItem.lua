local GuildPlayerItem = Class.GuildPlayerItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildPlayerItem.interface = GameConfig.ComponentsConfig.Component
GuildPlayerItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildPlayerItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local RoleDataHelper = GameDataHelper.RoleDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local guildData = PlayerManager.PlayerDataManager.guildData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local NobilityDataHelper = GameDataHelper.NobilityDataHelper
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig

function GuildPlayerItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function GuildPlayerItem:OnDestroy()
    self.data = nil
end

function GuildPlayerItem:InitView()
    self._iconContainer = self:FindChildGO("Container_content/Container_Icon")
    self._nameText = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._positionText = self:GetChildComponent("Container_content/Text_Position", "TextMeshWrapper")
    self._contributionText = self:GetChildComponent("Container_content/Text_Contribution", "TextMeshWrapper")
    self._fightText = self:GetChildComponent("Container_content/Text_FightPower", "TextMeshWrapper")
    self._stateText = self:GetChildComponent("Container_content/Text_Time", "TextMeshWrapper")
    self._levelText = self:GetChildComponent("Container_content/Text_Level", "TextMeshWrapper")
end

--override
function GuildPlayerItem:OnRefreshData(data)
    self.data = data
    local vocation = data[public_config.GUILD_MEMBER_INFO_VOCATION]
    local name = data[public_config.GUILD_MEMBER_INFO_NAME]
    local position = data[public_config.GUILD_MEMBER_INFO_POSITION]
    local contribution = data[public_config.GUILD_MEMBER_INFO_CONTRIBUTE]
    local fight = data[public_config.GUILD_MEMBER_INFO_FIGHT_FORCE]
    local state = data[public_config.GUILD_MEMBER_INFO_IS_ONLINE]
    local level = data[public_config.GUILD_MEMBER_INFO_LEVEL]
    local offlineTime = data[public_config.GUILD_MEMBER_INFO_OFFLINE_TIME]
    local nobility = data[public_config.GUILD_MEMBER_INFO_NOBILITY] or 0
    --爵位
    if nobility ~= 0 then
        local icon = NobilityDataHelper:GetNobilityShow(nobility)
        self._iconContainer:SetActive(true)
        GameWorld.AddIcon(self._iconContainer, icon, nil)
    else
        self._iconContainer:SetActive(false)
    end
    self._nameText.text = name
    self._positionText.text = LanguageDataHelper.GetContent(GlobalParamsHelper.GetParamValue(570)[position])
    self._contributionText.text = contribution
    self._fightText.text = fight
    if state == 1 then
        self._stateText.text = BaseUtil.GetColorString(LanguageDataHelper.GetContent(53048), ColorConfig.J)
    else
        local timeData = DateTimeUtil.MinusServerDay(offlineTime)
        if timeData.day > 0 then
            local showStr = LanguageDataHelper.CreateContentWithArgs(53050, {["0"]=timeData.day})
            self._stateText.text = BaseUtil.GetColorString(showStr, ColorConfig.C)
        elseif timeData.hour > 0 then
            local showStr = LanguageDataHelper.CreateContentWithArgs(53049, {["0"]=timeData.hour})
            self._stateText.text = BaseUtil.GetColorString(showStr, ColorConfig.C)
        else
            local showStr = LanguageDataHelper.CreateContentWithArgs(53051, {["0"]=timeData.min})
            self._stateText.text = BaseUtil.GetColorString(showStr, ColorConfig.C)
        end
    end

    local lvMark = LanguageDataHelper.GetContent(157)
    self._levelText.text = string.format("%s%d", lvMark, level)
end