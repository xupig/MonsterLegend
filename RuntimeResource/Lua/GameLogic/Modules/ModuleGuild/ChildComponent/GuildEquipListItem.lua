local GuildEquipListItem = Class.GuildEquipListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
GuildEquipListItem.interface = GameConfig.ComponentsConfig.Component
GuildEquipListItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildEquipListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function GuildEquipListItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._icon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._selectImage = self:FindChildGO("Image_Select")
end

function GuildEquipListItem:OnDestroy()
    self._itemContainer = nil
    self._icon = nil
end

--override
function GuildEquipListItem:OnRefreshData(data)
    local itemId = data:GetItemID()
    local name = data:GetNameText()
    self._icon:SetItem(itemId)
    self._nameText.text = name
    self._selectImage:SetActive(false)
end

function GuildEquipListItem:SetSelect(flag)
    self._selectImage:SetActive(flag)
end