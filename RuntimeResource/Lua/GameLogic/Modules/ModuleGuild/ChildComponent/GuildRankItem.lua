
local GuildRankItem = Class.GuildRankItem(ClassTypes.UIListItem)

GuildRankItem.interface = GameConfig.ComponentsConfig.Component
GuildRankItem.classPath = "Modules.ModuleGuild.ChildComponent.GuildRankItem"

local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local guildData = PlayerManager.PlayerDataManager.guildData
UIListItem = ClassTypes.UIListItem
local GuildManager = PlayerManager.GuildManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function GuildRankItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end

function GuildRankItem:InitView()
    self._rankText = self:GetChildComponent("Container_content/Text_Rank", "TextMeshWrapper")
    self._nameText = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._levelText = self:GetChildComponent("Container_content/Text_Level", "TextMeshWrapper")
    self._numText = self:GetChildComponent("Container_content/Text_Count", "TextMeshWrapper")
    self._leaderText = self:GetChildComponent("Container_content/Text_Leader", "TextMeshWrapper")
    self._contributionText = self:GetChildComponent("Container_content/Text_Contribution", "TextMeshWrapper")
    self._gradeText = self:GetChildComponent("Container_content/Text_Grade", "TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Container_content/Container_Icon")
    self._iconBGContainer = self:FindChildGO("Container_content/Container_IconBG")
end

--override
function GuildRankItem:OnRefreshData(data)
    self.data = data
    self:ShowInfo(data)
end

function GuildRankItem:ShowInfo(data)
    self._rankText.text = data[public_config.GUILD_DETAIL_INFO_RANK]
    self._nameText.text = data[public_config.GUILD_DETAIL_INFO_NAME]
    self._levelText.text = data[public_config.GUILD_DETAIL_INFO_LEVEL]
    local curNum = data[public_config.GUILD_DETAIL_INFO_COUNT]
    local maxNum = data[public_config.GUILD_DETAIL_INFO_MAX_COUNT]
    local mark = LanguageDataHelper.GetContent(900)
    local grade = data[public_config.GUILD_DETAIL_INFO_GRADE] or 0
    self._numText.text = string.format("%d%s%d", curNum, mark, maxNum)
    self._leaderText.text = data[public_config.GUILD_DETAIL_INFO_PRESIDENT_NAME]
    self._contributionText.text = data[public_config.GUILD_DETAIL_INFO_GUILD_FIGHT_FORCE] or 0
    self._gradeText.text = LanguageDataHelper.CreateContentWithArgs(GlobalParamsHelper.GetParamValue(825)[grade])
    local badge = data[public_config.GUILD_DETAIL_INFO_ICON_ID]
    local flag = data[public_config.GUILD_DETAIL_INFO_FLAG_ID]
    GameWorld.AddIcon(self._iconContainer, badge, nil)
    GameWorld.AddIcon(self._iconBGContainer, flag, nil)
end