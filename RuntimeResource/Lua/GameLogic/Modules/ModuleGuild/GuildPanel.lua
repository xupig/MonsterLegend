require"Modules.ModuleGuild.ChildView.GuildNormalView"
require"Modules.ModuleGuild.ChildView.GuildWarehouseView"
require"Modules.ModuleGuild.ChildView.GuildSkillView"
require"Modules.ModuleGuild.ChildView.GuildActivityView"

local GuildPanel = Class.GuildPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local GuildNormalView = ClassTypes.GuildNormalView
local GuildManager = PlayerManager.GuildManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GuildType = GameConfig.EnumType.GuildType
local GuildWarehouseView = ClassTypes.GuildWarehouseView
local GuildSkillView = ClassTypes.GuildSkillView
local GuildActivityView = ClassTypes.GuildActivityView

--override
function GuildPanel:Awake()
    self._selectTab = -1
    self._indexToGO = {}
    self._indexToView = {}
    self:InitPanel()
end

--override
function GuildPanel:OnDestroy()

end

--override 
function GuildPanel:OnShow(data)
    GuildManager:GetSelfGuildInfo()
    self:AddEventListeners()
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
    self:ShowView(data)
    self:SetSecTabContainerZ(-6000)
end
--override
function GuildPanel:SetData(data)
    self:ShowView(data)
end

--override
function GuildPanel:OnClose()
    self:RemoveEventListeners()
end

function GuildPanel:AddEventListeners()
   
end

function GuildPanel:RemoveEventListeners()

end

function GuildPanel:InitPanel()
    self._indexToGO[GuildType.Normal] = self:FindChildGO("Container_Normal")
    self._indexToGO[GuildType.Warehouse] = self:FindChildGO("Container_Warehouse")
    self._indexToGO[GuildType.Skill] = self:FindChildGO("Container_Skill")
    self._indexToGO[GuildType.Activity] = self:FindChildGO("Container_Activity")

    self._indexToView[GuildType.Normal] = self:AddChildLuaUIComponent("Container_Normal", GuildNormalView)
    self._indexToView[GuildType.Warehouse] = self:AddChildLuaUIComponent("Container_Warehouse", GuildWarehouseView)
    self._indexToView[GuildType.Skill] = self:AddChildLuaUIComponent("Container_Skill", GuildSkillView)
    self._indexToView[GuildType.Activity] = self:AddChildLuaUIComponent("Container_Activity", GuildActivityView)

    for i=1,#self._indexToGO do
        self._indexToGO[i]:SetActive(false)
    end
    
    self._tabCB = function (index)
        self:OnToggleGroupClick(index)
    end

    self._tabCB2 = function (index)
        self:CheckApplyTabActive(index)
    end

    self:SetTabClickCallBack(self._tabCB)
    self:SetTabClickAfterUpdateSecTabCB(self._tabCB2)

    self._secTabCb = function (index)
		self:SelectSecTab(index)
	end
    self:SetSecondTabClickCallBack(self._secTabCb)
end

--隐藏申请功能，不用于实用
function GuildPanel:CheckApplyTabActive(index)
    if index == GuildType.Normal then
        self:SetSecTabActive(5, false)
    end
end

function GuildPanel:OnToggleGroupClick(index)
    if self._selectTab ~= -1 then
        if self._indexToView[index] ~= nil then
            self._indexToView[index]:CloseView()
        end
        self._indexToGO[self._selectTab]:SetActive(false)
    end
    self._indexToGO[index]:SetActive(true)
    if self._indexToView[index] ~= nil then
        self._indexToView[index]:ShowView()
    end
    self._selectTab = index
end

function GuildPanel:SelectSecTab(secIndex)
    if self._indexToView[self._selectTab] ~= nil then
        self._indexToView[self._selectTab]:SelectSecTab(secIndex)
    end
end

function GuildPanel:ShowView(data)
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
end

function GuildPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--关闭的时候重新开启二级主菜单
function GuildPanel:ReopenSubMainPanel()
    return true
end