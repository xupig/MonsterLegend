GuildModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function GuildModule.Init()
    GUIManager.AddPanel(PanelsConfig.Guild,"Panel_Guild",GUILayer.LayerUIPanel,"Modules.ModuleGuild.GuildPanel",true,false, nil, false)
    GUIManager.AddPanel(PanelsConfig.GuildTaskCommitEquip,"Panel_GuildTaskCommitEquip",GUILayer.LayerUIPanel,"Modules.ModuleGuild.GuildTaskCommitEquipPanel",true,PanelsConfig.IS_NOT_CENTER_ADJUST, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return GuildModule