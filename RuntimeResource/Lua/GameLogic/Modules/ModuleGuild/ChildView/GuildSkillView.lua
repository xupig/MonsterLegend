require "Modules.ModuleGuild.ChildComponent.SkillListItem"
require "Modules.ModuleGuild.ChildComponent.SkillInfoItem"

local GuildSkillView = Class.GuildSkillView(ClassTypes.BaseLuaUIComponent)

GuildSkillView.interface = GameConfig.ComponentsConfig.Component
GuildSkillView.classPath = "Modules.ModuleGuild.ChildView.GuildSkillView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local SkillListItem = ClassTypes.SkillListItem
local SkillInfoItem = ClassTypes.SkillInfoItem
local guildData = PlayerManager.PlayerDataManager.guildData
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager

function GuildSkillView:Awake()
    self:InitView()
    self._preSelected = -1
    self:InitListenerFunc()
end

function GuildSkillView:InitListenerFunc()
    self._refreshguildSkillInfo = function() self:RefreshData() end
end


function GuildSkillView:OnDestroy() 

end

function GuildSkillView:OnEnable()
    self:AddEventListeners()
end

function GuildSkillView:OnDisable()
    self:RemoveEventListeners()
end

function GuildSkillView:CloseView()

end

function GuildSkillView:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Skill_Info, self._refreshguildSkillInfo)
end

function GuildSkillView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Skill_Info, self._refreshguildSkillInfo)
end

function GuildSkillView:ShowView(data)
    self:RefreshData()
end

function GuildSkillView:RefreshData()
    self._listData = guildData:GetSkillInfo()
    self._list:SetDataList(self._listData)
    local index = 0
    if self._preSelected ~= -1 then
        index = self._preSelected
    end
    self:OnListItemClicked(index, true)
end

function GuildSkillView:InitView()
    self._infoItem = self:AddChildLuaUIComponent("Container_SkillInfo", SkillInfoItem)

    self:InitScrollViewList()
end

function GuildSkillView:InitScrollViewList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(SkillListItem)
    self._list:SetPadding(20, 0, 0, 10)
    self._list:SetGap(10, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 2, 1)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
	self._list:SetItemClickedCB(itemClickedCB)
end

function GuildSkillView:OnListItemClicked(idx, force)
    local itemData = self._list:GetDataByIndex(idx)
    if itemData.unLockLevel > 0 then
        --未解锁不切换
        GUIManager.ShowText(2, LanguageDataHelper.CreateContentWithArgs(53137, {["0"]=itemData.unLockLevel}))
        return
    end
    if self._preSelected == idx and force ~= true then
        return
    end
    if self._preSelected ~= idx and self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelected(false)
    end

    self._list:GetItem(idx):ShowSelected(true)
    self._infoItem:RefreshData(self._list:GetDataByIndex(idx))
    self._preSelected = idx
end

function GuildSkillView:Clear(idx)
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelected(false)
    end
    self._preSelected = -1
end

function GuildSkillView:SelectSecTab(idx)

end