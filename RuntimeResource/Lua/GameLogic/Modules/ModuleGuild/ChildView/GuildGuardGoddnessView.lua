require "UIComponent.Extend.TipsCom"

local GuildGuardGoddnessView = Class.GuildGuardGoddnessView(ClassTypes.BaseLuaUIComponent)

GuildGuardGoddnessView.interface = GameConfig.ComponentsConfig.Component
GuildGuardGoddnessView.classPath = "Modules.ModuleGuild.ChildView.GuildGuardGoddnessView"

local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local DateTimeUtil = GameUtil.DateTimeUtil
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local TipsCom = ClassTypes.TipsCom

function GuildGuardGoddnessView:Awake()
    self:InitView()
end

function GuildGuardGoddnessView:OnDestroy()

end

function GuildGuardGoddnessView:CloseView()
    self._iconBg = nil
end

function GuildGuardGoddnessView:ShowView(data)
    self._iconBg = self:FindChildGO("Image_BG")
    GameWorld.AddIcon(self._iconBg,3314)
    self:RefreshView()
end

function GuildGuardGoddnessView:OnEnable()

end

function GuildGuardGoddnessView:OnDisable()

end

function GuildGuardGoddnessView:InitView()
    self._enterButton = self:FindChildGO("Button_Enter")
    self._csBH:AddClick(self._enterButton,function() self:OnEnterButtonClick() end)
    self._tipsButton = self:FindChildGO("Button_Tips")
    self._csBH:AddClick(self._tipsButton,function() self:OnTipsButtonClick() end)

    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(53221))
    self._tipsGO:SetActive(false)
    
    self._tipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")
    self._tipsText.text = LanguageDataHelper.CreateContentWithArgs(53220)
end

function GuildGuardGoddnessView:OnEnterButtonClick()
    GuildManager:GuildGuardGoddnessEnterReq()
end

function GuildGuardGoddnessView:OnTipsButtonClick()
    self._tipsGO:SetActive(true)
end

function GuildGuardGoddnessView:RefreshView()

end