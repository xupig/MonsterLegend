

local GuildApproveSettingView = Class.GuildApproveSettingView(ClassTypes.BaseLuaUIComponent)

GuildApproveSettingView.interface = GameConfig.ComponentsConfig.Component
GuildApproveSettingView.classPath = "Modules.ModuleGuild.ChildView.GuildApproveSettingView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local MIN_FIGHT_POWER = 1
local MAX_FIGHT_POWER = 999999999
local DELTA_FIGHT_POWER = 10000

local MIN_LEVEL = 1
local MAX_LEVEL = 700
local DELTA_LEVEL = 5

function GuildApproveSettingView:Awake()
    self:InitView()
end

function GuildApproveSettingView:OnDestroy()

end

function GuildApproveSettingView:CloseView()

end

function GuildApproveSettingView:ShowView(data)
    self.power = MIN_FIGHT_POWER
    self.level = MIN_LEVEL
    self:RefreshFightPower()
    self:RefreshLevel()
end

function GuildApproveSettingView:InitView()
    self._bgButton = self:FindChildGO("Container_PopupSmall/Button_Close")
    self._csBH:AddClick(self._bgButton,function() self:OnCloseButtonClick() end)
    self._confirmButton = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Button_Cancel")
    self._csBH:AddClick(self._cancelButton, function() self:OnCancelButtonClick() end)

    self._incButton = self:FindChildGO("Container_FightPower/Button_Increase")
    self._csBH:AddClick(self._incButton,function() self:OnIncButtonClick() end)
    self._decButton = self:FindChildGO("Container_FightPower/Button_Decrease")
    self._csBH:AddClick(self._decButton,function() self:OnDecButtonClick() end)

    self._inputFieldMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_FightPower/InputField_Count")
    self._inputFieldMesh:SetOnEndMeshEditCB(function(text) self:OnEndEdit(text) end)
    self._inputText = self:GetChildComponent("Container_FightPower/InputField_Count", 'InputFieldMeshWrapper')
    self._inputText.placeholder.text = ""
    self._inputText.characterLimit = 9
    self._inputText.text = 1

    self._incLevelButton = self:FindChildGO("Container_Level/Button_Increase")
    self._csBH:AddClick(self._incLevelButton,function() self:OnIncLevelButtonClick() end)
    self._decLevelButton = self:FindChildGO("Container_Level/Button_Decrease")
    self._csBH:AddClick(self._decLevelButton,function() self:OnDecLevelButtonClick() end)

    self._inputFieldLevelMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_Level/InputField_Count")
    self._inputFieldLevelMesh:SetOnEndMeshEditCB(function(text) self:OnLevelEndEdit(text) end)
    self._inputLevelText = self:GetChildComponent("Container_Level/InputField_Count", 'InputFieldMeshWrapper')
    self._inputLevelText.placeholder.text = ""
    self._inputLevelText.characterLimit = 3
    self._inputLevelText.text = 1

end

function GuildApproveSettingView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function GuildApproveSettingView:OnConfirmButtonClick()
    --设置
    self.gameObject:SetActive(false)
    GuildManager:SetGuildApproveLevelAndFightReq(self.level, self.power)
end

function GuildApproveSettingView:OnCancelButtonClick()
    self.gameObject:SetActive(false)
end

function GuildApproveSettingView:OnIncButtonClick()
    if self.power + DELTA_FIGHT_POWER > MAX_FIGHT_POWER then
        GUIManager.ShowText(2, LanguageDataHelper.CreateContentWithArgs(53217))
        return
    end
    self.power = self.power + DELTA_FIGHT_POWER
    self:RefreshFightPower()
end

function GuildApproveSettingView:OnDecButtonClick()
    if self.power - DELTA_FIGHT_POWER < MIN_FIGHT_POWER then
        GUIManager.ShowText(2, LanguageDataHelper.CreateContentWithArgs(53217))
        return
    end
    self.power = self.power - DELTA_FIGHT_POWER
    self:RefreshFightPower()
end

function GuildApproveSettingView:OnEndEdit(text)
    if string.IsNullOrEmpty(text) then
        text = MIN_FIGHT_POWER
    end
    local count = tonumber(text)
    if count < MIN_FIGHT_POWER or count > MAX_FIGHT_POWER then
        self._inputText.text = MIN_FIGHT_POWER
        count = MIN_FIGHT_POWER
    end
    self.power = count
end

function GuildApproveSettingView:RefreshFightPower()
    self._inputText.text = self.power
end

function GuildApproveSettingView:OnLevelEndEdit(text)
    if string.IsNullOrEmpty(text) then
        text = MIN_LEVEL
    end
    local count = tonumber(text)
    if count < MIN_LEVEL or count > MAX_LEVEL then
        self._inputLevelText.text = MIN_LEVEL
        count = MIN_LEVEL
    end
    self.level = count
end

function GuildApproveSettingView:OnIncLevelButtonClick()
    if self.level + DELTA_LEVEL > MAX_LEVEL then
        GUIManager.ShowText(2, LanguageDataHelper.CreateContentWithArgs(53218))
        return
    end
    self.level = self.level + DELTA_LEVEL
    self:RefreshLevel()
end

function GuildApproveSettingView:OnDecLevelButtonClick()
    if self.level - DELTA_LEVEL < MIN_LEVEL then
        GUIManager.ShowText(2, LanguageDataHelper.CreateContentWithArgs(53218))
        return
    end
    self.level = self.level - DELTA_LEVEL
    self:RefreshLevel()
end

function GuildApproveSettingView:RefreshLevel()
    self._inputLevelText.text = self.level
end