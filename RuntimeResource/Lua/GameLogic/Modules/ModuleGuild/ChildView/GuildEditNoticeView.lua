local GuildEditNoticeView = Class.GuildEditNoticeView(ClassTypes.BaseLuaUIComponent)

GuildEditNoticeView.interface = GameConfig.ComponentsConfig.Component
GuildEditNoticeView.classPath = "Modules.ModuleGuild.ChildView.GuildEditNoticeView"

local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function GuildEditNoticeView:Awake()
    self:InitView()
    self.notice = ""
end
--override
function GuildEditNoticeView:OnDestroy() 

end

function GuildEditNoticeView:InitView()
    self._closeButton = self:FindChildGO("Container_PopupMiddle/Button_Close")
    self._csBH:AddClick(self._closeButton,function() self:OnCloseButtonClick() end)
    self._confirmButton = self:FindChildGO("Button_Save")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._noticeButton = self:FindChildGO("Button_SaveAndNotice")
    self._csBH:AddClick(self._noticeButton,function() self:OnNoticeButtonClick() end)

    self._inputText = self:GetChildComponent("InputField_Text", 'InputFieldMeshWrapper')
    self._inputText.characterLimit = 120
    self._inputText.text = ""
    self._inputFieldMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputField_Text")
    self._inputFieldMesh:SetOnEndMeshEditCB(function(text) self:OnEndEdit(text) end)

    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
end

function GuildEditNoticeView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function GuildEditNoticeView:OnConfirmButtonClick()
    if string.IsNullOrEmpty(self.notice) then
        return
    end
    GuildManager:SetGuildBroadcastSettingReq(self.notice)
    self.gameObject:SetActive(false)
end

function GuildEditNoticeView:OnNoticeButtonClick()
    if string.IsNullOrEmpty(self.notice) then
        return
    end
    GuildManager:SetGuildBroadcastSettingReq(self.notice, 1)
    self.gameObject:SetActive(false)
end

function GuildEditNoticeView:OnEndEdit(text)
    self.notice = text
end

function GuildEditNoticeView:ShowInfo()
    local maxCount = GlobalParamsHelper.GetParamValue(567)
    if guildData.guildSetAnnoCount > maxCount then
        self._infoText.text = LanguageDataHelper.CreateContentWithArgs(53136, {["0"]=GlobalParamsHelper.GetParamValue(568)})
    else
        local str = string.format("%d/%d", guildData.guildSetAnnoCount, maxCount)
        self._infoText.text = LanguageDataHelper.CreateContentWithArgs(53135, {["0"]=str})
    end
end

function GuildEditNoticeView:OnShow()
    self:ShowInfo()
end