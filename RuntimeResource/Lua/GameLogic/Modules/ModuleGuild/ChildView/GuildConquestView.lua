require "UIComponent.Extend.TipsCom"
require "Modules.ModuleGuild.ChildComponent.GuildConquestItem"

local GuildConquestView = Class.GuildConquestView(ClassTypes.BaseLuaUIComponent)

GuildConquestView.interface = GameConfig.ComponentsConfig.Component
GuildConquestView.classPath = "Modules.ModuleGuild.ChildView.GuildConquestView"

local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildConquestManager = PlayerManager.GuildConquestManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local TipsCom = ClassTypes.TipsCom
local GuildConquestItem = ClassTypes.GuildConquestItem
local UIList = ClassTypes.UIList
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper

function GuildConquestView:Awake()
    self:InitView()
    self._openDay = false
    self:InitListenerFunc()
end

function GuildConquestView:InitListenerFunc()
    self._refreshGuildConquestList = function() self:RefreshGuildConquestList() end
end



function GuildConquestView:OnDestroy()

end

function GuildConquestView:CloseView()

end

function GuildConquestView:ShowView(data)
    GuildConquestManager:GuildConquestListReq()
    self:RefreshView()
end

function GuildConquestView:OnEnable()
    self:AddEventListeners()
    self._timer = TimerHeap:AddSecTimer(0, 60, 0, function() self:RefreshDataReq() end)
end

function GuildConquestView:OnDisable()
    self:RemoveEventListeners()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function GuildConquestView:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Conquest_List, self._refreshGuildConquestList)
end

function GuildConquestView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Conquest_List, self._refreshGuildConquestList)
end

function GuildConquestView:InitView()
    self._enterButton = self:FindChildGO("Button_Enter")
    self._csBH:AddClick(self._enterButton,function() self:OnEnterButtonClick() end)
    self._godPalaceButton = self:FindChildGO("Button_GodPalace")
    self._csBH:AddClick(self._godPalaceButton,function() self:OnGodPalaceButtonClick() end)
    self._tipsButton = self:FindChildGO("Button_Tips")
    self._csBH:AddClick(self._tipsButton,function() self:OnTipsButtonClick() end)

    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(53110))
    self._tipsGO:SetActive(false)

    self._notOpenGO = self:FindChildGO("Container_NotOpen")
    self._openGO = self:FindChildGO("Container_Open")

    self._openInfoText = self:GetChildComponent("Container_Open/Text_Info", "TextMeshWrapper")
    self._infoText = self:GetChildComponent("Container_NotOpen/Text_Info", "TextMeshWrapper")
    self._deadlineTimeText = self:GetChildComponent("Container_NotOpen/Text_DeadlineTime", "TextMeshWrapper")
    self._startTimeText = self:GetChildComponent("Text_StartTime", "TextMeshWrapper")
    self._roundTime1Text = self:GetChildComponent("Text_RoundTime1", "TextMeshWrapper")
    self._roundTime2Text = self:GetChildComponent("Text_RoundTime2", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(GuildConquestItem)
    self._list:SetPadding(10, 0, 0, 12)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
    self._scrollView:SetScrollRectEnable(false)

    self._infoText.text = LanguageDataHelper.CreateContentWithArgs(53101)
end

function GuildConquestView:OnEnterButtonClick()
    GuildConquestManager:GuildConquestEnterReq()
end

function GuildConquestView:OnGodPalaceButtonClick()
    GUIManager.ClosePanel(PanelsConfig.Guild)
    GUIManager.ShowPanel(PanelsConfig.GodsTemple)
end

function GuildConquestView:OnTipsButtonClick()
    self._tipsGO:SetActive(true)
end

function GuildConquestView:RefreshView()
    self._list:SetDataList({1,2,3,4,5})
    self:RefreshTime()
end

function GuildConquestView:RefreshGuildConquestList()
    local data = {1,2,3,4,5} --五档
    self._list:SetDataList(data)
    self:RefreshTime()
end

function GuildConquestView:RefreshTime()
    local cfg = GlobalParamsHelper.GetParamValue(673)
    local setDay = cfg[1]
    local openServerTime = DateTimeUtil.GetOpenServerTime()
    local curTime = DateTimeUtil.GetServerTime()
    local startTime
    local isSameDay = false
    local index = 1
    for i=1,#cfg do
        startTime = DateTimeUtil.GetOpenServerTime() + (cfg[i] - 1) * 86400
        isSameDay = DateTimeUtil.SameDay(startTime)
        index = i
        if isSameDay or curTime < startTime then
            break
        end
    end

    local setWDay = ActivityDailyDataHelper:GetDayLimit(public_config.ACTIVITY_ID_GUILD_CONQUEST)[1]
    local openConfig = ActivityDailyDataHelper:GetTimeLimit(public_config.ACTIVITY_ID_GUILD_CONQUEST)

    local deadlineStr = LanguageDataHelper.CreateContent(53405)
    local startTimeStr = LanguageDataHelper.CreateContent(53408)
    local roundTime1Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53409), ColorConfig.B)
    local roundTime2Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53410), ColorConfig.B)

    self._notOpenGO:SetActive(true)
    self._openGO:SetActive(false)

    self._openDay = false
    if (not isSameDay) and curTime < startTime then
        deadlineStr = LanguageDataHelper.CreateContentWithArgs(53403, {["0"]=cfg[index]})
        startTimeStr = LanguageDataHelper.CreateContentWithArgs(53406, {["0"]=cfg[index]})
    elseif isSameDay or DateTimeUtil.GetWday() == setWDay then
        --活动当天
        self._openDay = true
        deadlineStr = LanguageDataHelper.CreateContent(53404)
        startTimeStr = LanguageDataHelper.CreateContent(53407)
        --19:00-21:00
        if self:IsInTime({19, 0, openConfig[1], openConfig[2]}) then
            self._notOpenGO:SetActive(false)
            self._openGO:SetActive(true)
            self._openInfoText.text = LanguageDataHelper.CreateContent(53411)
            roundTime1Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53409), ColorConfig.J)
        elseif self:IsInTime({openConfig[1], openConfig[2], openConfig[1]+openConfig[3], openConfig[2]+openConfig[4]}) then
            --第一轮 21:00-21:20
            self._notOpenGO:SetActive(false)
            self._openGO:SetActive(true)
            self._openInfoText.text = LanguageDataHelper.CreateContent(53412)
            roundTime1Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53409), ColorConfig.J)
        elseif self:IsInTime({openConfig[1]+openConfig[3], openConfig[2]+openConfig[4], openConfig[5], openConfig[6]}) then
            --第一轮到第二轮之间 21:20-21:30
            self._notOpenGO:SetActive(false)
            self._openGO:SetActive(true)
            self._openInfoText.text = LanguageDataHelper.CreateContent(53413)
            roundTime1Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53252), ColorConfig.B)
            roundTime2Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53410), ColorConfig.J)
        elseif self:IsInTime({openConfig[5], openConfig[6], openConfig[5]+openConfig[7], openConfig[6]+openConfig[8]}) then
            --第二轮 21:30-21:50
            self._notOpenGO:SetActive(false)
            self._openGO:SetActive(true)
            self._openInfoText.text = LanguageDataHelper.CreateContent(53414)
            roundTime1Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53252), ColorConfig.B)
            roundTime2Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53410), ColorConfig.J)
        elseif self:IsInTime({openConfig[5]+openConfig[7], openConfig[6]+openConfig[8], 24, 0}) then
            --第二轮结束 21:50-24:00
            self._notOpenGO:SetActive(false)
            self._openGO:SetActive(true)
            self._openInfoText.text = LanguageDataHelper.CreateContent(53415)
            roundTime1Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53252), ColorConfig.B)
            roundTime2Str = BaseUtil.GetColorString(LanguageDataHelper.CreateContent(53252), ColorConfig.B)
        end
    end
    self._deadlineTimeText.text = LanguageDataHelper.CreateContentWithArgs(53102, {["0"]=deadlineStr})
    self._startTimeText.text = LanguageDataHelper.CreateContentWithArgs(53104, {["0"]=startTimeStr})
    self._roundTime1Text.text = LanguageDataHelper.CreateContentWithArgs(53105, {["0"]=roundTime1Str})
    self._roundTime2Text.text = LanguageDataHelper.CreateContentWithArgs(53106, {["0"]=roundTime2Str})

end

--开始时、分与结束时、分
function GuildConquestView:IsInTime(config)
    local now = DateTimeUtil.Now()
    --local now = {year=2018, month=07, day=15, hour=21, min=40, sec=00}
    if (now.hour > config[1] or (now.hour == config[1] and now.min >= config[2])) and
        (now.hour < config[3] or (now.hour == config[3] and now.min < config[4])) then
        return true
    end
    return false
end

--每分钟刷新数据
function GuildConquestView:RefreshDataReq()
    if self._openDay then
        GuildConquestManager:GuildConquestListReq()
    end
end