local GuildFixSendRedEnvelopeView = Class.GuildFixSendRedEnvelopeView(ClassTypes.BaseLuaUIComponent)

GuildFixSendRedEnvelopeView.interface = GameConfig.ComponentsConfig.Component
GuildFixSendRedEnvelopeView.classPath = "Modules.ModuleGuild.ChildView.GuildFixSendRedEnvelopeView"

local GUIManager = GameManager.GUIManager
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local ItemDataHelper = GameDataHelper.ItemDataHelper

local MIN_COUNT = 10
local RECOMMEND_COUNT = 10

function GuildFixSendRedEnvelopeView:Awake()
    self:InitView()
end

function GuildFixSendRedEnvelopeView:OnDestroy()

end

function GuildFixSendRedEnvelopeView:CloseView()

end

function GuildFixSendRedEnvelopeView:ShowView(data)
    self.data = data
    self:RefreshView()
end

function GuildFixSendRedEnvelopeView:InitView()
    self._icon = self:FindChildGO("Container_Icon")
    self._moneyCountText = self:GetChildComponent("Text_MoneyCount", "TextMeshWrapper")
    self._tipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")
    self._tipsText.text = LanguageDataHelper.CreateContent(53142)

    self._countInfoText = self:GetChildComponent("Container_MoneyCount/Text_Info", "TextMeshWrapper")
    
    self._sendButton = self:FindChildGO("Button_Send")
    self._csBH:AddClick(self._sendButton,function() self:OnSendButtonClick() end)
    self._getButton = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self._getButton,function() self:OnBGButtonClick() end)

    self._incButton = self:FindChildGO("Container_Count/Button_Increase")
    self._csBH:AddClick(self._incButton,function() self:OnIncButtonClick() end)
    self._decButton = self:FindChildGO("Container_Count/Button_Decrease")
    self._csBH:AddClick(self._decButton,function() self:OnDecButtonClick() end)

    self._inputFieldMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_Count/InputField_Count")
    self._inputFieldMesh:SetOnEndMeshEditCB(function(text) self:OnEndEdit(text) end)
    self._inputText = self:GetChildComponent("Container_Count/InputField_Count", 'InputFieldMeshWrapper')
    self._inputText.placeholder.text = ""
    self._inputText.characterLimit = 4
    self._inputText.text = 1
end

function GuildFixSendRedEnvelopeView:OnEndEdit(text)
    if string.IsNullOrEmpty(text) then
        text = MIN_COUNT
    end
    local count = tonumber(text)
    if count < MIN_COUNT or count > math.max(guildData.guildMaxMemberCount, MIN_COUNT) then
        self._inputText.text = MIN_COUNT
        count = MIN_COUNT
    end
    self.count = count
end

function GuildFixSendRedEnvelopeView:RefreshCount()
    self._inputText.text = self.count
end

function GuildFixSendRedEnvelopeView:OnIncButtonClick()
    if self.count == math.max(guildData.guildMaxMemberCount, MIN_COUNT) then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53212))
        return
    end
    self.count = self.count + 1
    self:RefreshCount()
end

function GuildFixSendRedEnvelopeView:OnDecButtonClick()
    if self.count == MIN_COUNT then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53213))
        return
    end
    self.count = self.count - 1
    self:RefreshCount()
end

function GuildFixSendRedEnvelopeView:RefreshMoneyCount()
    self._moneyCountText.text = self.data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_COUNT]
    local moneyType = self.data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_TYPE] or public_config.MONEY_TYPE_COUPONS_BIND
    GameWorld.AddIcon(self._icon, ItemDataHelper.GetIcon(moneyType))
    self._countInfoText.text = self.data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_COUNT]..ItemDataHelper.GetItemName(moneyType)
end

function GuildFixSendRedEnvelopeView:RefreshView()
    self.count = RECOMMEND_COUNT
    self:RefreshCount()
    self:RefreshMoneyCount()
end

function GuildFixSendRedEnvelopeView:OnSendButtonClick()
    --发红包
    local moneyCount = self.data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_COUNT]
    if moneyCount < self.count then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53214))
        return
    end
    local id = self.data[public_config.GUILD_RED_ENVELOPE_INFO_ID]
    GuildManager:GuildSendSysRedEnvelopeReq(id, self.count)
    self:OnBGButtonClick()
end

function GuildFixSendRedEnvelopeView:OnBGButtonClick()
    self.gameObject:SetActive(false)
end