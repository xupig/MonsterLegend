local GuildSendRedEnvelopeView = Class.GuildSendRedEnvelopeView(ClassTypes.BaseLuaUIComponent)

GuildSendRedEnvelopeView.interface = GameConfig.ComponentsConfig.Component
GuildSendRedEnvelopeView.classPath = "Modules.ModuleGuild.ChildView.GuildSendRedEnvelopeView"

local GUIManager = GameManager.GUIManager
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local MIN_COUNT = 1
local RECOMMEND_COUNT = 10
local MAX_MONEY_COUNT = 9999

function GuildSendRedEnvelopeView:Awake()
    self:InitView()
end

function GuildSendRedEnvelopeView:OnDestroy()

end

function GuildSendRedEnvelopeView:CloseView()

end

function GuildSendRedEnvelopeView:ShowView()
    self:RefreshView()
end

function GuildSendRedEnvelopeView:InitView()
    self._icon = self:FindChildGO("Container_Icon")
    self._moneyCountText = self:GetChildComponent("Text_MoneyCount", "TextMeshWrapper")
    self._tipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")

    self._tipsText.text = LanguageDataHelper.CreateContent(53219)
    
    self._sendButton = self:FindChildGO("Button_Send")
    self._csBH:AddClick(self._sendButton,function() self:OnSendButtonClick() end)
    self._getButton = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self._getButton,function() self:OnBGButtonClick() end)

    self._incButton = self:FindChildGO("Container_Count/Button_Increase")
    self._csBH:AddClick(self._incButton,function() self:OnIncButtonClick() end)
    self._decButton = self:FindChildGO("Container_Count/Button_Decrease")
    self._csBH:AddClick(self._decButton,function() self:OnDecButtonClick() end)

    self._inputFieldMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_Count/InputField_Count")
    self._inputFieldMesh:SetOnEndMeshEditCB(function(text) self:OnEndEdit(text) end)
    self._inputText = self:GetChildComponent("Container_Count/InputField_Count", 'InputFieldMeshWrapper')
    self._inputText.placeholder.text = ""
    self._inputText.characterLimit = 4
    self._inputText.text = 1

    self._incMoneyButton = self:FindChildGO("Container_MoneyCount/Button_Increase")
    self._csBH:AddClick(self._incMoneyButton,function() self:OnIncMoneyButtonClick() end)
    self._decMoneyButton = self:FindChildGO("Container_MoneyCount/Button_Decrease")
    self._csBH:AddClick(self._decMoneyButton,function() self:OnDecMoneyButtonClick() end)

    self._inputFieldMoneyMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_MoneyCount/InputField_Count")
    self._inputFieldMoneyMesh:SetOnEndMeshEditCB(function(text) self:OnMoneyEndEdit(text) end)
    self._inputMoneyText = self:GetChildComponent("Container_MoneyCount/InputField_Count", 'InputFieldMeshWrapper')
    self._inputMoneyText.placeholder.text = ""
    self._inputMoneyText.characterLimit = 4
    self._inputMoneyText.text = 1

    self._inputFieldBlessMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputField_Blessing")
    self._inputFieldBlessMesh:SetOnEndMeshEditCB(function(text) self:OnBlessEndEdit(text) end)
    self._inputBlessText = self:GetChildComponent("InputField_Blessing", 'InputFieldMeshWrapper')
    self._inputBlessText.placeholder.text = ""
    self._inputBlessText.characterLimit = 10
    self._inputBlessText.text = LanguageDataHelper.CreateContent(53126)
end

function GuildSendRedEnvelopeView:OnBlessEndEdit(text)
    if string.IsNullOrEmpty(text) then
        text = LanguageDataHelper.CreateContent(53126)
    end
    self.bless = text
    self._inputBlessText.text = text
end

function GuildSendRedEnvelopeView:OnEndEdit(text)
    if string.IsNullOrEmpty(text) then
        text = MIN_COUNT
    end
    local count = tonumber(text)
    if count < MIN_COUNT or count > math.max(guildData.guildMaxMemberCount, MIN_COUNT) then
        self._inputText.text = MIN_COUNT
        count = MIN_COUNT
    end
    self.count = count
end

function GuildSendRedEnvelopeView:RefreshCount()
    self._inputText.text = self.count
end

function GuildSendRedEnvelopeView:OnIncButtonClick()
    if self.count == math.max(guildData.guildMaxMemberCount, MIN_COUNT) then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53212))
        return
    end
    self.count = self.count + 1
    self:RefreshCount()
end

function GuildSendRedEnvelopeView:OnDecButtonClick()
    if self.count == MIN_COUNT then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53213))
        return
    end
    self.count = self.count - 1
    self:RefreshCount()
end

function GuildSendRedEnvelopeView:OnMoneyEndEdit(text)
    if string.IsNullOrEmpty(text) then
        text = MIN_COUNT
    end
    local count = tonumber(text)
    if count < MIN_COUNT or count > MAX_MONEY_COUNT then
        self._inputMoneyText.text = MIN_COUNT
        count = MIN_COUNT
    end
    self.moneyCount = count
    self:RefreshSetMoneyCount()
end

function GuildSendRedEnvelopeView:OnIncMoneyButtonClick()
    if self.moneyCount == MAX_MONEY_COUNT then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53215))
        return
    end
    self.moneyCount = self.moneyCount + 1
    self:RefreshSetMoneyCount()
end

function GuildSendRedEnvelopeView:OnDecMoneyButtonClick()
    if self.moneyCount == MIN_COUNT then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53216))
        return
    end
    self.moneyCount = self.moneyCount - 1
    self:RefreshSetMoneyCount()
end

function GuildSendRedEnvelopeView:RefreshSetMoneyCount()
    self._inputMoneyText.text = self.moneyCount
    self._moneyCountText.text = self.moneyCount
    local moneyType = public_config.MONEY_TYPE_COUPONS
    GameWorld.AddIcon(self._icon, ItemDataHelper.GetIcon(moneyType))
end

function GuildSendRedEnvelopeView:RefreshView()
    self.count = RECOMMEND_COUNT
    self.moneyCount = RECOMMEND_COUNT
    self.bless = LanguageDataHelper.CreateContent(53126)
    self:RefreshCount()
    self:RefreshSetMoneyCount()
    self:RefreshCanSendCount()
end

function GuildSendRedEnvelopeView:RefreshCanSendCount()
    local totalCount = GlobalParamsHelper.GetParamValue(591)
    local canSendCount = totalCount - guildData.sendRedEnvelopeCount
    self._descText.text = LanguageDataHelper.CreateContentWithArgs(53146, {["0"]=string.format("%d/%d", canSendCount, totalCount)})
end

function GuildSendRedEnvelopeView:OnSendButtonClick()
    --发个人红包
    if self.moneyCount < self.count then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53214))
        return
    end
    GuildManager:GuildSendRedEnvelopeReq(self.moneyCount, self.count, self.bless)
    self:OnBGButtonClick()
end

function GuildSendRedEnvelopeView:OnBGButtonClick()
    self.gameObject:SetActive(false)
end