require"Modules.ModuleGuild.ChildView.GuildMythicalCreaturesView"
require"Modules.ModuleGuild.ChildView.GuildGuardGoddnessView"
require"Modules.ModuleGuild.ChildView.GuildBanquetView"
require"Modules.ModuleGuild.ChildView.GuildConquestView"

local GuildActivityView = Class.GuildActivityView(ClassTypes.BaseLuaUIComponent)

GuildActivityView.interface = GameConfig.ComponentsConfig.Component
GuildActivityView.classPath = "Modules.ModuleGuild.ChildView.GuildActivityView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggleGroup = ClassTypes.UIToggleGroup
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local GuildMythicalCreaturesView = ClassTypes.GuildMythicalCreaturesView
local GuildGuardGoddnessView = ClassTypes.GuildGuardGoddnessView
local GuildBanquetView = ClassTypes.GuildBanquetView
local GuildConquestView = ClassTypes.GuildConquestView

function GuildActivityView:Awake()
    self._selectTab = -1
    self:InitView()
    self:InitListenerFunc()
end

function GuildActivityView:InitListenerFunc()
    self._refreshGuildMonsterInfo = function() self:RefreshGuildMonsterInfo() end
    self._refreshGuildMonsterHP = function() self:RefreshGuildMonsterHP() end
end

function GuildActivityView:OnDestroy()

end

function GuildActivityView:OnEnable()
    self:AddEventListeners()
end

function GuildActivityView:OnDisable()
    self:RemoveEventListeners()
end

function GuildActivityView:CloseView()

end

function GuildActivityView:ShowView(data)

end

function GuildActivityView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Monster_Info, self._refreshGuildMonsterInfo)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Monster_HP, self._refreshGuildMonsterHP)
end

function GuildActivityView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Monster_Info, self._refreshGuildMonsterInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Monster_HP, self._refreshGuildMonsterHP)
end

function GuildActivityView:InitView()
    self._indexToGO = {}
    self._indexToView = {}
    self._indexToGO[1] = self:FindChildGO("Container_MythicalCreatures")
    self._indexToGO[2] = self:FindChildGO("Container_Banquet")
    self._indexToGO[3] = self:FindChildGO("Container_Conquest")
    self._indexToGO[4] = self:FindChildGO("Container_Goddess")

    self._indexToView[1] = self:AddChildLuaUIComponent("Container_MythicalCreatures", GuildMythicalCreaturesView)
    self._indexToView[2] = self:AddChildLuaUIComponent("Container_Banquet", GuildBanquetView)
    self._indexToView[3] = self:AddChildLuaUIComponent("Container_Conquest", GuildConquestView)
    self._indexToView[4] = self:AddChildLuaUIComponent("Container_Goddess", GuildGuardGoddnessView)

    for i=1,#self._indexToGO do
        self._indexToGO[i]:SetActive(false)
    end
end

function GuildActivityView:SelectSecTab(index)
    if self._selectTab ~= -1 then
        if self._indexToView[index] ~= nil then
            self._indexToView[index]:CloseView()
        end
        self._indexToGO[self._selectTab]:SetActive(false)
    end
    self._indexToGO[index]:SetActive(true)
    if self._indexToView[index] ~= nil then
        self._indexToView[index]:ShowView()
    end
    self._selectTab = index
end

function GuildActivityView:RefreshGuildMonsterInfo()
    self._indexToView[1]:RefreshGuildMonsterInfo()
end

function GuildActivityView:RefreshGuildMonsterHP()
    self._indexToView[1]:RefreshGuildMonsterHP()
end