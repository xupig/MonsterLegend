require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
require "UIComponent.Extend.ItemGrid"

local GuildMythicalCreaturesView = Class.GuildMythicalCreaturesView(ClassTypes.BaseLuaUIComponent)

GuildMythicalCreaturesView.interface = GameConfig.ComponentsConfig.Component
GuildMythicalCreaturesView.classPath = "Modules.ModuleGuild.ChildView.GuildMythicalCreaturesView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggleGroup = ClassTypes.UIToggleGroup
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TaskRewardItem = ClassTypes.TaskRewardItem
local TaskRewardData = ClassTypes.TaskRewardData
local ActorModelComponent = GameMain.ActorModelComponent
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemGrid = ClassTypes.ItemGrid
local bagData = PlayerManager.PlayerDataManager.bagData
local DateTimeUtil = GameUtil.DateTimeUtil
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper


function GuildMythicalCreaturesView:Awake()
    self._hasStarted = false
    self:InitCallBack()
    self:InitView()
end

function GuildMythicalCreaturesView:OnDestroy()

end

function GuildMythicalCreaturesView:CloseView()

end

function GuildMythicalCreaturesView:ShowView(data)
    GuildManager:GetGuildMonsterInfoReq()
    GuildManager:GetGuildMonsterHPReq()
    self:RefreshView()
end

function GuildMythicalCreaturesView:OnEnable()
    self:AddEventListeners()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateCountDown() end)
end

function GuildMythicalCreaturesView:OnDisable()
    self:RemoveEventListeners()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function GuildMythicalCreaturesView:InitCallBack()
    self._onGuildMythicalCreaturesSoulItemChangeCB = function() self:RefreshGuildMonsterInfo() end
end

function GuildMythicalCreaturesView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Mythical_Creatures_Soul_Item_Info, self._onGuildMythicalCreaturesSoulItemChangeCB)
end

function GuildMythicalCreaturesView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Mythical_Creatures_Soul_Item_Info, self._onGuildMythicalCreaturesSoulItemChangeCB)
end

function GuildMythicalCreaturesView:InitView()
    self._commitButton = self:FindChildGO("Button_Commit")
    self._csBH:AddClick(self._commitButton,function() self:OnCommitButtonClick() end)
    self._fightButton = self:FindChildGO("Button_Fight")
    self._csBH:AddClick(self._fightButton,function() self:OnFightButtonClick() end)
    self._fightText = self:GetChildComponent("Button_Fight/Text", "TextMeshWrapper")

    self._commitRedPointImage = self:FindChildGO("Button_Commit/Image_RedPoint")

    self._startGO = self:FindChildGO("Container_Start")
    self._tipsGO = self:FindChildGO("Text_Tips")
    self._itemGO = self:FindChildGO("Container_Item")
    local item = GUIManager.AddItem(self._itemGO, 1)
    self._itemIcon = UIComponentUtil.AddLuaUIComponent(item, ItemGrid)
    self._itemCountText = self:GetChildComponent("Text_ItemCount", "TextMeshWrapper")
    self._tipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")

    self._tipsText.text = self:GetShowTips()

    self._nameIcon = self:FindChildGO("Container_Info/Container_Name")
    self._countText = self:GetChildComponent("Container_Info/Text_Count", "TextMeshWrapper")
    self._inventoryText = self:GetChildComponent("Container_Info/Text_Inventory", "TextMeshWrapper")

    self._infoText = self:GetChildComponent("Container_Start/Text_Info", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Rewards/ScrollViewList")
    self._listRewards = list
    self._scrollViewRewards = scrollView
    self._listRewards:SetItemType(TaskRewardItem)
    self._listRewards:SetPadding(0, 0, 0, 16)
    self._listRewards:SetGap(5, 5)
    self._listRewards:SetDirection(UIList.DirectionTopToDown, 4, 1)

    self._bossModelComponent = self:AddChildComponent('Container_Monster', ActorModelComponent)
    self._bossModelComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 145, 0), 1)

    self:InitProgress()
    self._startGO:SetActive(false)
end

function GuildMythicalCreaturesView:InitProgress()
    self._Comp = UIScaleProgressBar.AddProgressBar(self.gameObject, "Container_Start/ProgressBar")
    self._Comp:SetProgress(0)
    self._Comp:ShowTweenAnimate(true)
    self._Comp:SetMutipleTween(true)
    self._Comp:SetIsResetToZero(false)
end

function GuildMythicalCreaturesView:OnHPChange()
    local curHP = guildData.guildMonsterHP[2] or 0
    local maxHP = guildData.guildMonsterHP[1] or 1
    self._Comp:SetProgressByValue(curHP, maxHP)
    self._Comp:SetProgressText(tostring(curHP).."/"..tostring(maxHP))
end

function GuildMythicalCreaturesView:OnCommitButtonClick()
    GuildManager:GuildMonsterSubmitSoulReq()
end

function GuildMythicalCreaturesView:OnFightButtonClick()
    if self._hasStarted then
        GuildManager:GuildMonsterEnterReq()
    else
        if (not GuildManager:IsPresident()) and (not GuildManager:IsVicePresident()) then
            GUIManager.ShowText(2, LanguageDataHelper.GetContent(53208))
            return
        end
        if not GuildManager:IsGuildMonsterOpen() then
            GUIManager.ShowText(2, LanguageDataHelper.GetContent(53209))
            return
        end
        GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(53207, {["0"] = GlobalParamsHelper.GetParamValue(604)}), 
        function() GuildManager:GuildMonsterStartReq() end, nil)
    end
end

function GuildMythicalCreaturesView:RefreshView()
    self:ShowRewardInfo()
    local modelId = GuildDataHelper:GetGuildMonsterModel(1)
    self._bossModelComponent:LoadModel(modelId)
    local icon = GuildDataHelper:GetGuildMonsterNameIcon(1)
    GameWorld.AddIcon(self._nameIcon, icon)
    self:RefreshState()
end

function GuildMythicalCreaturesView:ShowRewardInfo()
    local drops = GuildDataHelper:GetGuildMonsterReward(GameWorld.Player().world_level)[GameWorld.Player():GetVocationGroup()]
    local items = {}
    for i, v in ipairs(drops) do
        table.insert(items, TaskRewardData(v, 1))
    end
    self._listRewards:SetDataList(items)
end

function GuildMythicalCreaturesView:ShowInfo()
    local maxCount = GlobalParamsHelper.GetParamValue(603)
    local curCount = maxCount - guildData.guildMonsterInfo[1]
    local countStr = BaseUtil.GetColorStringByCount(curCount, maxCount, true)
    self._countText.text = LanguageDataHelper.CreateContentWithArgs(53089, {["0"]=countStr})

    local needInventory = GlobalParamsHelper.GetParamValue(604)
    local curInventory = guildData.guildMonsterInfo[2]
    local inventoryStr = BaseUtil.GetColorStringByCount(curInventory, needInventory)
    self._inventoryText.text = LanguageDataHelper.CreateContentWithArgs(53090, {["0"]=inventoryStr})

    local itemId = GlobalParamsHelper.GetParamValue(606)
    local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    self._itemIcon:ActivateTipsById()
    self._itemIcon:SetItem(itemId)
    self._itemCountText.text = bagNum
end

function GuildMythicalCreaturesView:GetShowTips()
    local config = ActivityDailyDataHelper:GetTimeLimit(public_config.ACTIVITY_ID_GUILD_BEAST)
    --local config = GlobalParamsHelper.GetParamValue(607)
    local startH, endH ,startM, endM, startStr, endStr
    startH = config[1]
    startM = config[2]
    endH = config[1]+config[3]
    endM = config[2]+config[4]
    if endM >= 60 then
        endH = endH + 1
        endM = endM - 60
    end

    startStr = string.format("%d:%s", startH, DateTimeUtil:FormatNumber(startM))
    endStr = string.format("%d:%s", endH, DateTimeUtil:FormatNumber(endM))
    local showStr = BaseUtil.GetColorString(string.format("%s-%s", startStr, endStr), ColorConfig.J)
    local text = LanguageDataHelper.CreateContentWithArgs(53091, {["0"]=showStr})
    return text
end

function GuildMythicalCreaturesView:RefreshState()
    if self._hasStarted then
        self._fightText.text = LanguageDataHelper.CreateContentWithArgs(53194)
        self._startGO:SetActive(true)
        self._tipsGO:SetActive(false)
        self:UpdateCountDown()
    else
        self._fightText.text = LanguageDataHelper.CreateContentWithArgs(53094)
        self._startGO:SetActive(false)
        self._tipsGO:SetActive(true)
    end
end

function GuildMythicalCreaturesView:UpdateCountDown()
    if not self._hasStarted then
        return
    end
    local endTime = guildData.guildMonsterInfo[3] or 0
    endTime = endTime + GuildDataHelper:GetGuildMonsterTime(1) + GlobalParamsHelper.GetParamValue(621)
    local leftTime = endTime - DateTimeUtil:GetServerTime()
    if leftTime < 0 then
        self._hasStarted = false
        self:RefreshState()
    else
        self._infoText.text = LanguageDataHelper.CreateContentWithArgs(53195, {["0"]=DateTimeUtil:FormatFullTime(leftTime)})
    end
end

function GuildMythicalCreaturesView:RefreshGuildMonsterInfo()
    self:ShowInfo()
    self:RefreshGuildMonsterRedPoint()
    local endTime = guildData.guildMonsterInfo[3] or 0
    endTime = endTime + GuildDataHelper:GetGuildMonsterTime(1) + GlobalParamsHelper.GetParamValue(621)
    local leftTime = endTime - DateTimeUtil:GetServerTime()
    if leftTime < 0 then
        self._hasStarted = false
    else
        self._hasStarted = true
    end
    self:RefreshState()
end

function GuildMythicalCreaturesView:RefreshGuildMonsterHP()
    self:OnHPChange()
end

function GuildMythicalCreaturesView:RefreshGuildMonsterRedPoint()
    local itemId = GlobalParamsHelper.GetParamValue(606)
    local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    self._commitRedPointImage:SetActive(bagNum>0)
end