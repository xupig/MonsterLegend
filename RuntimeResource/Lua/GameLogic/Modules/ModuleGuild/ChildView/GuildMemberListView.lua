require "Modules.ModuleGuild.ChildComponent.GuildPlayerItem"
require "Modules.ModuleGuild.ChildView.GuildPlayerActionView"

local GuildMemberListView = Class.GuildMemberListView(ClassTypes.BaseLuaUIComponent)

GuildMemberListView.interface = GameConfig.ComponentsConfig.Component
GuildMemberListView.classPath = "Modules.ModuleGuild.ChildView.GuildMemberListView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GuildPlayerItem = ClassTypes.GuildPlayerItem
local GuildPlayerActionView = ClassTypes.GuildPlayerActionView

function GuildMemberListView:Awake()
    self:InitView()
end

function GuildMemberListView:OnDestroy()
end

function GuildMemberListView:OnEnable()
    self:AddEventListeners()
end

function GuildMemberListView:OnDisable()
    self:RemoveEventListeners()
end

function GuildMemberListView:CloseView()

end

function GuildMemberListView:ShowView(data)
    GuildManager:GuildSelfMemberInfoReq()
end

function GuildMemberListView:AddEventListeners()
    -- self._guildActionResp = function(action) self:GuildActionResp(action) end
    -- EventDispatcher:AddEventListener(GameEvents.Guild_Action_Resp, self._guildActionResp)
end

function GuildMemberListView:RemoveEventListeners()
    --EventDispatcher:RemoveEventListener(GameEvents.Guild_Action_Resp, self._guildActionResp)
end

function GuildMemberListView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(GuildPlayerItem)
    self._list:SetPadding(10, 0, 0, 10)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
	self._list:SetItemClickedCB(itemClickedCB)

    self._actionTipsGO = self:FindChildGO("Container_ActionTips")
    --self._actionTipsView = self:AddChildLuaUIComponent("Container_ActionTips", GuildPlayerActionView)
    self._actionTipsGO:SetActive(false)
end

function GuildMemberListView:OnListItemClicked(idx)
    self:ShowActionTips(self._list:GetDataByIndex(idx))
end

function GuildMemberListView:RefreshView()
    self._list:SetDataList(guildData.guildSelfMemberList)
end

local posVec = Vector2.New(640,-60)
function GuildMemberListView:ShowActionTips(data)
    local dbid = data[public_config.GUILD_MEMBER_INFO_DBID]
    if dbid == GameWorld.Player().dbid then
        return
    end
    local d = {}
    d.playerName = data[public_config.GUILD_MEMBER_INFO_NAME]
    d.dbid = dbid
    d.guildInfo = data
    d.rawListPos = posVec
    GUIManager.ShowPanel(PanelsConfig.PlayerInfoMenu,d)
    --self._actionTipsGO:SetActive(true)
    --self._actionTipsView:OnShow(data)
end

-- function GuildMemberListView:GuildActionResp(action)
--     self._actionTipsView:GuildActionResp(action)
-- end