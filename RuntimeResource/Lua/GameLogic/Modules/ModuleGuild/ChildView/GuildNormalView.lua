

require"Modules.ModuleGuild.ChildView.GuildBasicInfoView"
require"Modules.ModuleGuild.ChildView.GuildMemberListView"
require"Modules.ModuleGuild.ChildView.GuildListView"
require"Modules.ModuleGuild.ChildView.GuildRedEnvelopeView"

local GuildNormalView = Class.GuildNormalView(ClassTypes.BaseLuaUIComponent)

GuildNormalView.interface = GameConfig.ComponentsConfig.Component
GuildNormalView.classPath = "Modules.ModuleGuild.ChildView.GuildNormalView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggleGroup = ClassTypes.UIToggleGroup
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local GuildBasicInfoView = ClassTypes.GuildBasicInfoView
local GuildMemberListView = ClassTypes.GuildMemberListView
local GuildListView = ClassTypes.GuildListView
local GuildRedEnvelopeView = ClassTypes.GuildRedEnvelopeView

function GuildNormalView:Awake()
    self._selectTab = -1
    self:InitView()
    self:InitListenerFunc()
end


function GuildNormalView:InitListenerFunc()
    self._refreshGuildSelfMemberInfo = function() self:RefreshGuildSelfMemberInfo() end
    self._refreshGuildRankList = function() self:RefreshGuildRankList() end
    self._showGetRedEnvelopeView = function(data) self:ShowGetRedEnvelopeView(data) end
    self._showFixSendRedEnvelopeView = function(data) self:ShowFixSendRedEnvelopeView(data) end
    self._showRedEnvelopeInfoView = function(data) self:ShowRedEnvelopeInfoView(data) end
    self._refreshGuildRedEnvelopeList = function() self:RefreshRedEnvelopeList() end
    self._refreshGuildRedEnvelopeRecord = function() self:RefreshGuildRedEnvelopeRecord() end
end

function GuildNormalView:OnDestroy()

end

function GuildNormalView:OnEnable()
    self:AddEventListeners()
end

function GuildNormalView:OnDisable()
    self:RemoveEventListeners()
end

function GuildNormalView:CloseView()

end

function GuildNormalView:ShowView(data)

end

function GuildNormalView:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Self_Member_Info, self._refreshGuildSelfMemberInfo)

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Rank_Data, self._refreshGuildRankList)

    EventDispatcher:AddEventListener(GameEvents.Show_Get_Red_Envelope_View, self._showGetRedEnvelopeView)

    EventDispatcher:AddEventListener(GameEvents.Show_Fix_Send_Red_Envelope_View, self._showFixSendRedEnvelopeView)

    EventDispatcher:AddEventListener(GameEvents.Show_Red_Envelope_Info_View, self._showRedEnvelopeInfoView)

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Red_Envelope_List, self._refreshGuildRedEnvelopeList)

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Red_Envelope_Record, self._refreshGuildRedEnvelopeRecord)
end

function GuildNormalView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Self_Member_Info, self._refreshGuildSelfMemberInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Rank_Data, self._refreshGuildRankList)
    EventDispatcher:RemoveEventListener(GameEvents.Show_Get_Red_Envelope_View, self._showGetRedEnvelopeView)
    EventDispatcher:RemoveEventListener(GameEvents.Show_Fix_Send_Red_Envelope_View, self._showFixSendRedEnvelopeView)
    EventDispatcher:RemoveEventListener(GameEvents.Show_Red_Envelope_Info_View, self._showRedEnvelopeInfoView)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Red_Envelope_List, self._refreshGuildRedEnvelopeList)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Red_Envelope_Record, self._refreshGuildRedEnvelopeRecord)
end

function GuildNormalView:InitView()
    self._indexToGO = {}
    self._indexToView = {}
    self._indexToGO[1] = self:FindChildGO("Container_Basic")
    self._indexToGO[2] = self:FindChildGO("Container_MemberList")
    self._indexToGO[3] = self:FindChildGO("Container_GuildList")
    self._indexToGO[4] = self:FindChildGO("Container_LuckyMoney")

    self._indexToView[1] = self:AddChildLuaUIComponent("Container_Basic", GuildBasicInfoView)
    self._indexToView[2] = self:AddChildLuaUIComponent("Container_MemberList", GuildMemberListView)
    self._indexToView[3] = self:AddChildLuaUIComponent("Container_GuildList", GuildListView)
    self._indexToView[4] = self:AddChildLuaUIComponent("Container_LuckyMoney", GuildRedEnvelopeView)

    for i=1,#self._indexToGO do
        self._indexToGO[i]:SetActive(false)
    end
end

function GuildNormalView:SelectSecTab(index)
    if self._selectTab ~= -1 then
        if self._indexToView[index] ~= nil then
            self._indexToView[index]:CloseView()
        end
        self._indexToGO[self._selectTab]:SetActive(false)
    end
    self._indexToGO[index]:SetActive(true)
    if self._indexToView[index] ~= nil then
        self._indexToView[index]:ShowView()
    end
    self._selectTab = index
end

function GuildNormalView:RefreshGuildSelfMemberInfo()
    self._indexToView[2]:RefreshView()
end

function GuildNormalView:RefreshGuildRankList()
    self._indexToView[3]:RefreshView()
end

function GuildNormalView:ShowGetRedEnvelopeView(data)
    self._indexToView[4]:ShowGetRedEnvelopeView(data)
end

function GuildNormalView:ShowFixSendRedEnvelopeView(data)
    self._indexToView[4]:ShowFixSendRedEnvelopeView(data)
end

function GuildNormalView:ShowRedEnvelopeInfoView(data)
    self._indexToView[4]:ShowRedEnvelopeInfoView(data)
end

function GuildNormalView:RefreshRedEnvelopeList()
    self._indexToView[4]:RefreshRedEnvelopeList()
end

function GuildNormalView:RefreshGuildRedEnvelopeRecord()
    self._indexToView[4]:RefreshRedEnvelopeRecord()
end