
require "Modules.ModuleGuild.ChildView.ApproveMessageView"
require "Modules.ModuleGuild.ChildComponent.GuildQuitConfirmItem"
require "Modules.ModuleGuild.ChildView.GuildEditNoticeView"
require "UIComponent.Extend.TipsCom"

local GuildBasicInfoView = Class.GuildBasicInfoView(ClassTypes.BaseLuaUIComponent)

GuildBasicInfoView.interface = GameConfig.ComponentsConfig.Component
GuildBasicInfoView.classPath = "Modules.ModuleGuild.ChildView.GuildBasicInfoView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggleGroup = ClassTypes.UIToggleGroup
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local ApproveMessageView = ClassTypes.ApproveMessageView
local GuildQuitConfirmItem = ClassTypes.GuildQuitConfirmItem
local GuildEditNoticeView = ClassTypes.GuildEditNoticeView
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TipsCom = ClassTypes.TipsCom
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function GuildBasicInfoView:Awake()
    self._preSelected = -1
    self:InitView()
    self:InitListenerFunc()
    self:AddEventListeners()
end

function GuildBasicInfoView:InitListenerFunc()
    self._refreshSelfGuildInfo = function() self:RefreshSelfGuildInfo() end
    self._refreshGuildNotice = function() self:RefreshGuildNotice() end
    self._showApproveRedDot = function() self:ShowApproveRedDot() end
    self._refreshGuildHasDailyReward = function() self:RefreshGuildHasDailyReward() end
end

function GuildBasicInfoView:OnDestroy()

end

function GuildBasicInfoView:OnEnable()
    self:AddEventListeners()
end

function GuildBasicInfoView:OnDisable()
    self:RemoveEventListeners()
end

function GuildBasicInfoView:CloseView()

end

function GuildBasicInfoView:ShowView(data)
    GuildManager:GetSelfGuildInfo()
    if GuildManager:IsPresident() or GuildManager:IsVicePresident() then
        self._editButton:SetActive(true)
    else
        self._editButton:SetActive(false)
    end
end

function GuildBasicInfoView:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.Refresh_Self_Guild_Info, self._refreshSelfGuildInfo)
 
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Notice, self._refreshGuildNotice)

    EventDispatcher:AddEventListener(GameEvents.Show_Approve_Red_Dot, self._showApproveRedDot)

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Has_Daily_Reward, self._refreshGuildHasDailyReward)
end

function GuildBasicInfoView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Self_Guild_Info, self._refreshSelfGuildInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Notice, self._refreshGuildNotice)
    EventDispatcher:RemoveEventListener(GameEvents.Show_Approve_Red_Dot, self._showApproveRedDot)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Has_Daily_Reward, self._refreshGuildHasDailyReward)
end

function GuildBasicInfoView:InitView()
    self._levelUpButton = self:FindChildGO("Button_LevelUp")
    self._csBH:AddClick(self._levelUpButton,function() self:OnLevelUpButtonClick() end)
    self._getButton = self:FindChildGO("Button_Get")
    self._csBH:AddClick(self._getButton,function() self:OnGetButtonClick() end)
    self._getWrapper = self._getButton:GetComponent("ButtonWrapper")
    self._getRedPoint = self:FindChildGO("Button_Get/Image_RedPoint")
    self._tipsButton = self:FindChildGO("Button_Tips")
    self._csBH:AddClick(self._tipsButton,function() self:OnTipsButtonClick() end)

    self._approveButtonGO = self:FindChildGO("Container_GuildInfo/Button_Approve")
    self._approveButton = self:FindChildGO("Container_GuildInfo/Button_Approve")
    self._csBH:AddClick(self._approveButton,function() self:OnApproveButtonClick() end)
    self._quitButton = self:FindChildGO("Button_Quit")
    self._csBH:AddClick(self._quitButton,function() self:OnQuitButtonClick() end)

    self._approveButtonGO = self:FindChildGO("Container_GuildInfo/Button_Approve")
    self._approveRedDot = self:FindChildGO("Container_GuildInfo/Button_Approve/Image_RedPoint")

    self._iconContainer = self:FindChildGO("Container_Icon")
    self._iconBadgeContainer = self:FindChildGO("Container_IconBadge")
    self._guildNameText = self:GetChildComponent("Container_GuildInfo/Text_Name/Text_Value", "TextMeshWrapper")
    self._levelText = self:GetChildComponent("Container_GuildInfo/Text_Level/Text_Value", "TextMeshWrapper")
    self._leaderNameText = self:GetChildComponent("Container_GuildInfo/Text_Leader/Text_Value", "TextMeshWrapper")
    self._numText = self:GetChildComponent("Container_GuildInfo/Text_Num/Text_Value", "TextMeshWrapper")
    self._rankText = self:GetChildComponent("Container_GuildInfo/Text_Rank/Text_Value", "TextMeshWrapper")
    self._leaderNameText = self:GetChildComponent("Container_GuildInfo/Text_Leader/Text_Value", "TextMeshWrapper")
    self._fightPowerText = self:GetChildComponent("Container_GuildInfo/Text_FightPower/Text_Value", "TextMeshWrapper")

    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._infoText.text = LanguageDataHelper.CreateContent(53034)

    self._editButton = self:FindChildGO("Container_Notice/Button_Edit")
    self._csBH:AddClick(self._editButton,function() self:OnEditButtonClick() end)
    self._noticeText = self:GetChildComponent("Container_Notice/Text_Notice", "TextMeshWrapper")

    self._approveMessageGO = self:FindChildGO("Container_Approve")
    self._approveMessageView = self:AddChildLuaUIComponent("Container_Approve", ApproveMessageView)
    self._approveMessageGO:SetActive(false)

    self._quitConfirmGO = self:FindChildGO("Container_QuitGuild")
    self._quitConfirmView = self:AddChildLuaUIComponent("Container_QuitGuild", GuildQuitConfirmItem)

    self._editNoticeGO = self:FindChildGO("Container_EditNotice")
    self._editNoticeView = self:AddChildLuaUIComponent("Container_EditNotice", GuildEditNoticeView)

    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(53166))
    self._tipsGO:SetActive(false)

    self:InitProgress()
end

function GuildBasicInfoView:InitProgress()
    self._Comp = UIScaleProgressBar.AddProgressBar(self.gameObject, "Container_GuildInfo/Text_Money/ProgressBar")
    self._Comp:SetProgress(0)
    self._Comp:ShowTweenAnimate(true)
    self._Comp:SetMutipleTween(true)
    self._Comp:SetIsResetToZero(false)
end

function GuildBasicInfoView:OnMoneyChange()
    local level = guildData.guildLevel
    local curMoney = guildData.guildExperience
    local maxMoney = GuildDataHelper:GetGuildLevelCost(level)
    self._Comp:SetProgressText(tostring(curMoney).."/"..tostring(maxMoney))
    if curMoney > maxMoney then
        curMoney = maxMoney
    end
    self._Comp:SetProgressByValue(curMoney , maxMoney)
end

--退出公会
function GuildBasicInfoView:OnQuitButtonClick()
    self._quitConfirmGO:SetActive(true)
    self._quitConfirmView:OnShow()
end

function GuildBasicInfoView:OnGetButtonClick()
    GuildManager:GuildGetDailyRewardReq()
end

function GuildBasicInfoView:OnLevelUpButtonClick()
    GuildManager:GuildLevelUpReq()
end

function GuildBasicInfoView:OnTipsButtonClick()
    self._tipsGO:SetActive(true)
end

function GuildBasicInfoView:OnApproveButtonClick()
    if GuildManager:CanApprove() then
        --打开审批界面
        self._approveMessageGO:SetActive(true)
        self._approveMessageView:OnShow()
    end
end

function GuildBasicInfoView:OnEditButtonClick()
    self._editNoticeGO:SetActive(true)
    self._editNoticeView:OnShow()
end

function GuildBasicInfoView:RefreshSelfGuildInfo()
    self:ShowGuildInfo()
    self:ShowNotice()
    self:ShowApproveState()
    self:OnMoneyChange()
    self:RefreshButtonState()
end

function GuildBasicInfoView:RefreshButtonState()
    if GuildManager:HasGottenDailyReward() then
        self._getWrapper.interactable = false
        self._getRedPoint:SetActive(false)
    else
        self._getWrapper.interactable = true
        self._getRedPoint:SetActive(true)
    end
    
    self._levelUpButton:SetActive(GuildManager:IsPresident())
end

function GuildBasicInfoView:RefreshGuildNotice()
    self:ShowNotice()
end

function GuildBasicInfoView:ShowGuildInfo()
    self._fightPowerText.text = guildData.guildFightPower
    self._guildNameText.text = guildData.guildName
    self._levelText.text = string.format("Lv.%d", guildData.guildLevel) --待中文配置
    self._leaderNameText.text = guildData.guildLeaderName
    self._numText.text = string.format("%d/%d", guildData.guildMemberCount, guildData.guildMaxMemberCount) --待中文配置
    self._rankText.text = LanguageDataHelper.CreateContentWithArgs(GlobalParamsHelper.GetParamValue(825)[guildData.guildGrade])
    local icon = guildData.guildIcon
    local bgIcon = guildData.guildFlagIcon
    GameWorld.AddIcon(self._iconContainer, bgIcon, nil)
    GameWorld.AddIcon(self._iconBadgeContainer, icon, nil)
end

function GuildBasicInfoView:ShowNotice()
    self._noticeText.text = guildData.guildAnnouncement
end

function GuildBasicInfoView:ShowApproveState()
    if GuildManager:CanApprove() then
        self._approveButtonGO:SetActive(true)
        GuildManager:GetApplyGuildListReq()
    else
        self._approveButtonGO:SetActive(false)
    end
end

function GuildBasicInfoView:ShowApproveRedDot()
    if guildData.approveMessage ~= nil and #guildData.approveMessage ~= 0 then
        self._approveRedDot:SetActive(true)
    else
        self._approveRedDot:SetActive(false)
    end
end

function GuildBasicInfoView:ShowActionTips(data)
    local dbid = data[public_config.GUILD_INFO_DBID]
    if guildData.guildPosition == public_config.GUILD_POSITION_NORMAL 
        or dbid == GameWorld.Player().dbid then
        return
    end
    self._actionTipsGO:SetActive(true)
    self._actionTipsView:OnShow(data)
end

function GuildBasicInfoView:RefreshGuildHasDailyReward()
    self:RefreshButtonState()
end