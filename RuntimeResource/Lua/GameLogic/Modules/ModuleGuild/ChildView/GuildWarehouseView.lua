

require "Modules.ModuleGuild.ChildComponent.GuildWarehouseRecordItem"
require "Modules.ModuleGuild.ChildComponent.GuildWarehouseItem"
require "Modules.ModuleGuild.ChildComponent.GuildWarehouseGradeItem"
require "Modules.ModuleGuild.ChildComponent.GuildWarehouseQualityItem"
require "Modules.ModuleGuild.ChildView.GuildWarehouseDonateView"

local GuildWarehouseView = Class.GuildWarehouseView(ClassTypes.BaseLuaUIComponent)

GuildWarehouseView.interface = GameConfig.ComponentsConfig.Component
GuildWarehouseView.classPath = "Modules.ModuleGuild.ChildView.GuildWarehouseView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggleGroup = ClassTypes.UIToggleGroup
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local GuildWarehouseRecordItem = ClassTypes.GuildWarehouseRecordItem
local UIToggle = ClassTypes.UIToggle
local GuildWarehouseItem = ClassTypes.GuildWarehouseItem
local GuildWarehouseGradeItem = ClassTypes.GuildWarehouseGradeItem
local GuildWarehouseQualityItem = ClassTypes.GuildWarehouseQualityItem
local QualityType = GameConfig.EnumType.QualityType
local bagData = PlayerManager.PlayerDataManager.bagData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GuildWarehouseDonateView = ClassTypes.GuildWarehouseDonateView
local TipsManager = GameManager.TipsManager
local UIComplexList = ClassTypes.UIComplexList

local Min_Item_Count = 50

function GuildWarehouseView:Awake()
    self._preSelected = -1
    self._batchProc = false
    self:InitView()
    self:InitListenerFunc()
end

function GuildWarehouseView:InitListenerFunc()
    self._refreshGuildWarehouseSelectedEquipItem = function(pos, selectState) self:RefreshGuildWarehouseSelectedEquipItem(pos, selectState) end
    self._refreshGuildWarehouseBag = function(pos, selectState) self:RefreshGuildWarehouseBag() end
    self._refreshGuildWarehousePoint = function(pos, selectState) self:RefreshGuildWarehousePoint() end
    self._refreshGuildWarehouseRecordData = function(pos, selectState) self:RefreshRecordData() end
end

function GuildWarehouseView:OnDestroy()
end

function GuildWarehouseView:OnEnable()
    self:AddEventListeners()
end

function GuildWarehouseView:OnDisable()
    self:RemoveEventListeners()
end

function GuildWarehouseView:CloseView()
    self._preSelectedGradeIdx = 0
    self._preSelectedQualityIdx = 0
    self._selectedOwnVocation = false
end

function GuildWarehouseView:ShowView(data)
    GuildManager:GuildGetWarehouseBagReq()
    GuildManager:GuildWarehouseRecordReq()

    self:RefreshGuildWarehousePoint()
    --self:RefreshRecordData()
    local gradeData = GlobalParamsHelper.GetMSortValue(593)
    local qualityData = GlobalParamsHelper.GetMSortValue(594)
    self._listGrade:SetDataList(gradeData)
    self._listQuality:SetDataList(qualityData)
    self:RefreshButtonState()
end

function GuildWarehouseView:AddEventListeners()
   
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Warehouse_Selected_Equip_Item, self._refreshGuildWarehouseSelectedEquipItem)
   
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Warehouse_Bag, self._refreshGuildWarehouseBag)
  
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Warehouse_Point, self._refreshGuildWarehousePoint)
   
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Warehouse_Record_Data, self._refreshGuildWarehouseRecordData)
end

function GuildWarehouseView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Warehouse_Selected_Equip_Item, self._refreshGuildWarehouseSelectedEquipItem)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Warehouse_Bag, self._refreshGuildWarehouseBag)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Warehouse_Point, self._refreshGuildWarehousePoint)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Warehouse_Record_Data, self._refreshGuildWarehouseRecordData)
end

function GuildWarehouseView:InitView()
    self._donate = self:FindChildGO("Button_Donate")
    self._csBH:AddClick(self._donate,function () self:OnDonateButtonClick() end)
    self._donateButtonText = self:GetChildComponent("Button_Donate/Text", "TextMeshWrapper")

    self._solve = self:FindChildGO("Button_Solve")
    self._csBH:AddClick(self._solve,function () self:OnSolveButtonClick() end)
    self._solveButtonText = self:GetChildComponent("Button_Solve/Text", "TextMeshWrapper")
    
    local scrollViewRecord, listRecord = UIComplexList.AddScrollViewComplexList(self.gameObject,'Container_Record/ScrollViewList')
    self._listRecord = listRecord
	self._scrollViewRecord = scrollViewRecord
    self._listRecord:SetItemType(GuildWarehouseRecordItem)
    self._listRecord:SetPadding(0, 0, 0, 0)
    self._listRecord:SetGap(0, 5)
    self._listRecord:SetDirection(UIList.DirectionTopToDown, -1, -1)

    self._selectToggle = UIToggle.AddToggle(self.gameObject, "Toggle_Select")
    self._selectToggle:SetIsOn(false)
    self._selectToggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(GuildWarehouseItem)
    self._list:SetPadding(20, 0, 0, 24)
    self._list:SetGap(10, 5)
    self._list:SetDirection(UIList.DirectionTopToDown, 5, 1)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._qualityScrollViewGo = self:FindChildGO("Container_Quality/ScrollViewList")
    local scrollViewQuality, listQuality = UIList.AddScrollViewList(self.gameObject, "Container_Quality/ScrollViewList")
    self._listQuality = listQuality
	self._scrollViewQuality = scrollViewQuality
    self._listQuality:SetItemType(GuildWarehouseQualityItem)
    self._listQuality:SetPadding(0, 0, 0, 0)
    self._listQuality:SetGap(4, 0)
    self._listQuality:SetDirection(UIList.DirectionTopToDown, 1, 100)
	local itemClickedCB = 
	function(idx)
		self:OnQualityListItemClicked(idx)
	end
    self._listQuality:SetItemClickedCB(itemClickedCB)
    self._qualitySelect = self:FindChildGO("Container_Quality/Button_Show")
    self._csBH:AddClick(self._qualitySelect,function () self:OnQualitySelectButtonClick() end)
    self._qualityText = self:GetChildComponent("Container_Quality/Text_Select", "TextMeshWrapper")
    self._qualityScrollViewGo:SetActive(false)

    self._gradeScrollViewGo = self:FindChildGO("Container_Grade/ScrollViewList")
    local scrollViewGrade, listGrade = UIList.AddScrollViewList(self.gameObject, "Container_Grade/ScrollViewList")
    self._listGrade = listGrade
	self._scrollViewGrade = scrollViewGrade
    self._listGrade:SetItemType(GuildWarehouseGradeItem)
    self._listGrade:SetPadding(0, 0, 0, 0)
    self._listGrade:SetGap(4, 0)
    self._listGrade:SetDirection(UIList.DirectionTopToDown, 1, 100)
	local itemClickedCB = 
	function(idx)
		self:OnGradeListItemClicked(idx)
	end
    self._listGrade:SetItemClickedCB(itemClickedCB)
    self._gradeSelect = self:FindChildGO("Container_Grade/Button_Show")
    self._csBH:AddClick(self._gradeSelect,function () self:OnGradeSelectButtonClick() end)
    self._gradeText = self:GetChildComponent("Container_Grade/Text_Select", "TextMeshWrapper")
    self._gradeScrollViewGo:SetActive(false)

    self._donateGO = self:FindChildGO("Container_Donate")
    self._donateView = self:AddChildLuaUIComponent("Container_Donate", GuildWarehouseDonateView)
    self._donateGO:SetActive(false)

    self._infoText = self:GetChildComponent("Text_Info/Text_Value", "TextMeshWrapper")

    self._destroyConfirmGO = self:FindChildGO("Container_DestroyItem")
    self._destroyClose = self:FindChildGO("Container_DestroyItem/Container_PopupSmall/Button_Close")
    self._csBH:AddClick(self._destroyClose,function () self:OnDestroyCloseButtonClick() end)
    self._destroyClose = self:FindChildGO("Container_DestroyItem/Button_Confirm")
    self._csBH:AddClick(self._destroyClose,function () self:OnDestroyConfirmButtonClick() end)
    self._destroyClose = self:FindChildGO("Container_DestroyItem/Button_Cancel")
    self._csBH:AddClick(self._destroyClose,function () self:OnDestroyCancelButtonClick() end)

    self._destroyConfirmGO:SetActive(false)
end

function GuildWarehouseView:OnToggleValueChanged(state)
    if state then
        self._selectedOwnVocation = true
    else
        self._selectedOwnVocation = false
    end
    self:RefreshShowData()
end

function GuildWarehouseView:OnListItemClicked(idx)
    local item = self._list:GetItem(idx)
    local itemData = self._list:GetDataByIndex(idx)
    if itemData.cfg_id == nil then
        return
    end
    --item:ChangeState()
    if not self._batchProc then
        local btnArgs = {}
        btnArgs.guildExchange = true
        --宠物丹不显示销毁
        local itemId = next(GlobalParamsHelper.GetParamValue(592))
        if (GuildManager:IsPresident() or GuildManager:IsVicePresident()) and itemData.cfg_id ~= itemId then
            btnArgs.guildDestroy = true
        end
        TipsManager:ShowItemTips(itemData, btnArgs, true)
        if self._preSelected ~= -1 then
            self._list:GetItem(self._preSelected):ShowSelected(false)
        end
        self._preSelected = idx
        item:ShowSelected(true)
    else
        local itemId = next(GlobalParamsHelper.GetParamValue(592))
        if itemData.cfg_id ~= itemId then
            item:ChangeState()
        end
    end
end

function GuildWarehouseView:OnQualityListItemClicked(idx)
    local data = self._listQuality:GetDataByIndex(idx)
    self._qualityText.text = LanguageDataHelper.GetLanguageData(data[2])
    self:ResetQualityScrollViewGo()
    self._curSelectedQuality = data[1]
    self:RefreshShowData()
    self._preSelectedQualityIdx = idx
end

function GuildWarehouseView:OnGradeListItemClicked(idx)
    local data = self._listGrade:GetDataByIndex(idx)
    self._gradeText.text = LanguageDataHelper.GetLanguageData(data[2])
    self:ResetGradeScrollViewGo()
    self._curSelectedGrade = data[1]
    self:RefreshShowData()
    self._preSelectedGradeIdx = idx
end

function GuildWarehouseView:OnQualitySelectButtonClick()
    self._qualityShow = not self._qualityShow
    self._qualityScrollViewGo:SetActive(self._qualityShow)
end

function GuildWarehouseView:OnGradeSelectButtonClick()
    self._gradeShow = not self._gradeShow
    self._gradeScrollViewGo:SetActive(self._gradeShow)
end

function GuildWarehouseView:ResetQualityScrollViewGo()
    self._qualityShow = false
    self._qualityScrollViewGo:SetActive(self._qualityShow)
end

function GuildWarehouseView:ResetGradeScrollViewGo()
    self._gradeShow = false
    self._gradeScrollViewGo:SetActive(self._gradeShow)
end

function GuildWarehouseView:RefreshGuildWarehouseSelectedEquipItem(pos, selectState)
    if not self._batchProc then
        return
    end

    if selectState then
        self._selectedItemList[pos] = 1
    else
        self._selectedItemList[pos] = nil
    end
end

function GuildWarehouseView:RefreshView()
    self:OnGradeListItemClicked(self._preSelectedGradeIdx or 0) --默认任意
    self:OnQualityListItemClicked(self._preSelectedQualityIdx or 0) --默认任意
    self._selectToggle:SetIsOn(self._selectedOwnVocation or false) --默认不选中
    self:OnToggleValueChanged(self._selectedOwnVocation or false)
end

function GuildWarehouseView:RefreshShowData()
    local equips = bagData:GetPkg(public_config.PKG_TYPE_GUILD_WAREHOUSE):GetItemInfos()
    local needData = {}
    local count = 0

    --宠物丹显示在第一个
    local itemId = next(GlobalParamsHelper.GetParamValue(592))
    if self._curSelectedQuality == -1 and self._curSelectedGrade == -1 and not self._selectedOwnVocation then
        table.insert(needData, equips[-2])
        count = count + 1
    end

    for k,v in pairs(equips) do
        while(true) do
            if v.cfg_id == itemId then
                break
            end
            -- -1 表示任意
            if self._curSelectedQuality ~= -1 and v.cfgData.quality ~= self._curSelectedQuality then
                break
            end
            if self._curSelectedGrade ~= -1 and v.cfgData.grade ~= self._curSelectedGrade then
                break
            end
            if self._selectedOwnVocation and (not v:CheckVocationMatchGeneral()) then
                break
            end
            table.insert(needData, v)
            count = count + 1
            break
        end
    end

    --填充空数据为了生成空格
    local addCount = math.max(Min_Item_Count, count) - count
	for i=1, addCount do
        table.insert(needData, {})
    end
    self._preSelected = -1
    self._selectedItemList = {}
    self.needData = needData
    self._list:SetDataList(needData)
    self:UpdatePowerCompare()
end

function GuildWarehouseView:OnDonateButtonClick()
    if self._batchProc then
        --确认销毁装备
        self._destroyConfirmGO:SetActive(true)
    else
        --捐献装备
        self._donateGO:SetActive(true)
        self._donateView:ShowView()
    end
end

function GuildWarehouseView:OnSolveButtonClick()
    self._batchProc = not self._batchProc
    self:RefreshButtonState()
    self:RefreshShowData()
end

function GuildWarehouseView:UpdatePowerCompare()
	--检查装备是否能装备/战力是否更强
	for i=1,#self.needData do
		local itemData = self.needData[i]
		if itemData.cfg_id ~= nil and itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP then
			if itemData:CheckVocationMatchGeneral() then
				if itemData:CheckBetterThanLoaded() then
					self._list:GetItem(i-1):SetUseAndPowerCompare(true,false,true)
				else
					self._list:GetItem(i-1):SetUseAndPowerCompare(true,false,false)
				end
			else
				self._list:GetItem(i-1):SetUseAndPowerCompare(true,true)
			end
		else
			self._list:GetItem(i-1):SetUseAndPowerCompare(false)
		end
	end
end

function GuildWarehouseView:RefreshGuildWarehouseBag()
    self:RefreshView()
end

function GuildWarehouseView:RefreshGuildWarehousePoint()
    self._infoText.text = guildData.warehousePoints
end

function GuildWarehouseView:RefreshButtonState()
    if GuildManager:IsPresident() or GuildManager:IsVicePresident() then
        self._solve:SetActive(true)
        if self._batchProc then
            self._solveButtonText.text = LanguageDataHelper.CreateContent(53058)
            self._donateButtonText.text = LanguageDataHelper.CreateContent(53059)
        else
            self._solveButtonText.text = LanguageDataHelper.CreateContent(53069)
            self._donateButtonText.text = LanguageDataHelper.CreateContent(53070)
        end
    else
        self._solve:SetActive(false)
    end
end

function GuildWarehouseView:RefreshRecordData()
    local data = guildData.guildWarehouseRecordData or {}
    self._listRecord:RemoveAll()
    self._listRecord:AppendItemDataList(data)
    self._listRecord:MoveToBottom()
end

function GuildWarehouseView:OnDestroyConfirmButtonClick()
    --销毁装备
    local list = {}
    for k,v in pairs(self._selectedItemList) do
        table.insert(list, k)
    end
    if #list > 0 then
        GuildManager:GuildWarehouseDestoryListReq(list)
    end
    self._destroyConfirmGO:SetActive(false)
end

function GuildWarehouseView:OnDestroyCloseButtonClick()
    self._destroyConfirmGO:SetActive(false)
end

function GuildWarehouseView:OnDestroyCancelButtonClick()
    self._destroyConfirmGO:SetActive(false)
end


function GuildWarehouseView:SelectSecTab(idx)
    
end