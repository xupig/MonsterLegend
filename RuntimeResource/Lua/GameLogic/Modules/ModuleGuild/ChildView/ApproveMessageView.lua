
require "Modules.ModuleGuild.ChildComponent.ApproveInfoItem"
require "Modules.ModuleGuild.ChildView.GuildApproveSettingView"

local ApproveMessageView = Class.ApproveMessageView(ClassTypes.BaseLuaUIComponent)

ApproveMessageView.interface = GameConfig.ComponentsConfig.Component
ApproveMessageView.classPath = "Modules.ModuleGuild.ChildView.ApproveMessageView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local ApproveInfoItem = ClassTypes.ApproveInfoItem
local UIToggle = ClassTypes.UIToggle
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildApproveSettingView = ClassTypes.GuildApproveSettingView

function ApproveMessageView:Awake()
    self:InitView()
    self:InitListenerFunc()
end

function ApproveMessageView:InitListenerFunc()
    self._refreshGuildApproveMessage = function() self:RefreshData() end
end

function ApproveMessageView:OnDestroy()

end

function ApproveMessageView:OnEnable()
    self:AddEventListeners()
end

function ApproveMessageView:OnDisable()
    self:RemoveEventListeners()
end

function ApproveMessageView:OnShow(data)
    GuildManager:GetApplyGuildListReq()
end

function ApproveMessageView:AddEventListeners()
  
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Approve_Message, self._refreshGuildApproveMessage)
end

function ApproveMessageView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Approve_Message, self._refreshGuildApproveMessage)
end

function ApproveMessageView:InitView()
    self._bgButton = self:FindChildGO("Container_PopupBig/Button_Close")
    self._csBH:AddClick(self._bgButton,function() self:OnCloseButtonClick() end)
    self._settingButton = self:FindChildGO("Button_Setting")
    self._csBH:AddClick(self._settingButton,function() self:OnSettingButtonClick() end)
    self._acceptAllButton = self:FindChildGO("Button_AcceptAll")
    self._csBH:AddClick(self._acceptAllButton,function() self:OnAcceptAllButtonClick() end)
    self._refuseAllButton = self:FindChildGO("Button_RefuseAll")
    self._csBH:AddClick(self._refuseAllButton,function() self:OnRefuseAllButtonClick() end)

    self._approveToggle = UIToggle.AddToggle(self.gameObject, "Toggle_SetApprove")
    self._approveToggle:SetIsOn(guildData:IsAutoAccept())
    self._approveToggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(ApproveInfoItem)
    self._list:SetPadding(10, 0, 0, 12)
    self._list:SetGap(15, 20)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self._nextCB = function() self:OnRequestNext() end
    self._scrollView:SetOnRequestNextCB(self._nextCB)

    self._approveSettingGo = self:FindChildGO("Container_ApproveSetting")
    self._approveSettingView = self:AddChildLuaUIComponent("Container_ApproveSetting", GuildApproveSettingView)
    self._approveSettingGo:SetActive(false)
end

function ApproveMessageView:OnToggleValueChanged(state)
    if state then
        GuildManager:SetGuildApproveReq(public_config.GUILD_NEED_VERIFY_NO)
    else
        GuildManager:SetGuildApproveReq(public_config.GUILD_NEED_VERIFY_YES)
    end
end

function ApproveMessageView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function ApproveMessageView:OnSettingButtonClick()
    self._approveSettingGo:SetActive(true)
    self._approveSettingView:ShowView()
end

function ApproveMessageView:OnAcceptAllButtonClick()
    GuildManager:ReplyAllGuildApplyReq(0)
end

function ApproveMessageView:OnRefuseAllButtonClick()
    GuildManager:ReplyAllGuildApplyReq(1)
end

function ApproveMessageView:OnRequestNext()
    --GuildManager:GetApplyGuildListReq()
end

function ApproveMessageView:OnListItemClicked(idx)
    if self._preSelected == idx then
        return
    end
    self._list:GetItem(idx):ShowSelected(true)
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelected(false)
    end
    self._preSelected = idx
end

function ApproveMessageView:RefreshData()
    self._list:SetDataList(guildData.approveMessage)
end

