
require "Modules.ModuleGuild.ChildComponent.GuildPlayerActionItem"

local GuildPlayerActionView = Class.GuildPlayerActionView(ClassTypes.BaseLuaUIComponent)

GuildPlayerActionView.interface = GameConfig.ComponentsConfig.Component
GuildPlayerActionView.classPath = "Modules.ModuleGuild.ChildView.GuildPlayerActionView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local guildData = PlayerManager.PlayerDataManager.guildData
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager
local public_config = require("ServerConfig/public_config")
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildPlayerActionItem = ClassTypes.GuildPlayerActionItem
local GuildAction = GameConfig.EnumType.GuildAction
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function GuildPlayerActionView:Awake()
    self._itemPool = {}
    self._items = {}
    self:InitView()
end

function GuildPlayerActionView:OnDestroy() 

end

function GuildPlayerActionView:OnClose()

end

function GuildPlayerActionView:OnShow(data)
    self.data = data
    self:RefreshView()
end

function GuildPlayerActionView:InitView()
    self._buttonBGGO = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self._buttonBGGO,function() self:OnBGButtonClick() end)

    self.image1 = self:GetChildComponent("Container_Action/Image_BG1", 'ImageWrapper')
    self.image1:SetContinuousDimensionDirty(true)
    self.image2 = self:GetChildComponent("Container_Action/Image_BG1/Image_BG2", 'ImageWrapper')
    self.image2:SetContinuousDimensionDirty(true)

    self._itemTemp = self:FindChildGO("Container_Action/Container_Item")
    self._itemTemp:SetActive(false)

    self:InitActionResp()
end

function GuildPlayerActionView:OnBGButtonClick()
    self.gameObject:SetActive(false)
end

function GuildPlayerActionView:RefreshView()
    self.dbid = self.data[public_config.GUILD_MEMBER_INFO_DBID]
    self.position = self.data[public_config.GUILD_MEMBER_INFO_POSITION]
    self:SetActionList()
end

function GuildPlayerActionView:InitActionResp()
    self._actions = {}
    self._actions[GuildAction.UpToViceLeader] = function() GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_VICE_PRESIDENT) end
    self._actions[GuildAction.KickOutViceLeader] = function() GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_NORMAL) end
    self._actions[GuildAction.MakeOverLeader] = function() GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_PRESIDENT)  end
    self._actions[GuildAction.KickOutElite] = function() GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_NORMAL)  end
    self._actions[GuildAction.DownToElite] = function() GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_ELITE)  end
    self._actions[GuildAction.UpToElite] = function() GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_ELITE)  end
    self._actions[GuildAction.KickOutGuild] = function() GuildManager:GuildKickReq(self.dbid) end
end

function GuildPlayerActionView:SetActionList()
    self.actionData = {}
    if guildData.guildPosition == public_config.GUILD_POSITION_PRESIDENT and self.position == public_config.GUILD_POSITION_VICE_PRESIDENT then
        --自己是会长，选中副会长
        self.actionData = {GuildAction.MakeOverLeader, GuildAction.KickOutViceLeader, GuildAction.DownToElite, GuildAction.KickOutGuild}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_PRESIDENT and self.position == public_config.GUILD_POSITION_ELITE then
        --自己是会长，选中长老
        self.actionData = {GuildAction.UpToViceLeader, GuildAction.KickOutElite, GuildAction.KickOutGuild}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_PRESIDENT and self.position == public_config.GUILD_POSITION_NORMAL then
        --自己是会长，选中成员
        self.actionData = {GuildAction.UpToViceLeader, GuildAction.UpToElite, GuildAction.KickOutGuild}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_VICE_PRESIDENT and self.position == public_config.GUILD_POSITION_PRESIDENT then
        --自己是副会长，选中会长
        self.actionData = {}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_VICE_PRESIDENT and self.position == public_config.GUILD_POSITION_VICE_PRESIDENT then
        --自己是副会长，选中副会长
        self.actionData = {}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_VICE_PRESIDENT and self.position == public_config.GUILD_POSITION_ELITE then
        --自己是副会长，选中长老
        self.actionData = {GuildAction.UpToViceLeader, GuildAction.KickOutElite, GuildAction.KickOutGuild}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_VICE_PRESIDENT and self.position == public_config.GUILD_POSITION_NORMAL then
        --自己是副会长，选中成员
        self.actionData = {GuildAction.UpToViceLeader, GuildAction.UpToElite, GuildAction.KickOutGuild}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_ELITE and self.position == public_config.GUILD_POSITION_PRESIDENT then
        --自己是长老，选中会长
        self.actionData = {}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_ELITE and self.position == public_config.GUILD_POSITION_VICE_PRESIDENT then
        --自己是长老，选中副会长
        self.actionData = {}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_ELITE and self.position == public_config.GUILD_POSITION_ELITE then
        --自己是长老，选中长老
        self.actionData = {}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_ELITE and self.position == public_config.GUILD_POSITION_NORMAL then
        --自己是长老，选中成员
        self.actionData = {GuildAction.KickOutGuild}
    elseif guildData.guildPosition == public_config.GUILD_POSITION_NORMAL then
        --自己是成员
        self.actionData = {}
    end

    self:SetData(self.actionData)
end

function GuildPlayerActionView:GuildActionResp(action)
    self:OnBGButtonClick()
    if self._actions[action] ~= nil then
        if action == GuildAction.KickOutGuild then
            --踢出公会弹框
            local name = self.data[public_config.GUILD_INFO_NAME]
            GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(53186, {["0"] = name}), 
            function() self._actions[action]() end, nil)
            return
        end
        if action == GuildAction.MakeOverLeader then
            --转让会长弹框
            GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(53185), 
            function() self._actions[action]() end, nil)
            return
        end
        
        self._actions[action]()
    end
end


function GuildPlayerActionView:SetData(data)
    self:RemoveAll()
    for i, v in pairs(data) do
        self:AddItem(v)
    end
end

function GuildPlayerActionView:CreateItem()
    local itemPool = self._itemPool
    if #itemPool > 0 then
        return table.remove(itemPool, 1)
    end
    local go = self:CopyUIGameObject("Container_Action/Container_Item","Container_Action")
    local item = UIComponentUtil.AddLuaUIComponent(go, GuildPlayerActionItem)
    return item
end

function GuildPlayerActionView:AddItem(data)
    local item = self:CreateItem()
    item.gameObject:SetActive(true)
    table.insert(self._items, item)
    item:SetData(data)
end

function GuildPlayerActionView:RemoveAll()
    for i=#self._items,1,-1 do
        local item = self._items[i]
        item.gameObject:SetActive(false)
        self:ReleaseItem(item)
    end
    self._items = {}
end

function GuildPlayerActionView:ReleaseItem(item)
    table.insert(self._itemPool, 1, item)
end
