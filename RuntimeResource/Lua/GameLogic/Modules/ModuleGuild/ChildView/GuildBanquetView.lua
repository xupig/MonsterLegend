local GuildBanquetView = Class.GuildBanquetView(ClassTypes.BaseLuaUIComponent)

GuildBanquetView.interface = GameConfig.ComponentsConfig.Component
GuildBanquetView.classPath = "Modules.ModuleGuild.ChildView.GuildBanquetView"

local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildBanquetManager = PlayerManager.GuildBanquetManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local DateTimeUtil = GameUtil.DateTimeUtil
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig

function GuildBanquetView:Awake()
    self:InitView()
end

function GuildBanquetView:OnDestroy()

end

function GuildBanquetView:CloseView()
    self._iconBg = nil
end

function GuildBanquetView:ShowView(data)
    self._iconBg = self:FindChildGO("Image_BG")
    GameWorld.AddIcon(self._iconBg,3313)
    self:RefreshView()
end

function GuildBanquetView:OnEnable()

end

function GuildBanquetView:OnDisable()

end

function GuildBanquetView:InitView()
    self._enterButton = self:FindChildGO("Button_Enter")
    self._csBH:AddClick(self._enterButton,function() self:OnEnterButtonClick() end)
    
    self._tipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")
    self._tipsText.text = LanguageDataHelper.CreateContentWithArgs(53249)
end

function GuildBanquetView:OnEnterButtonClick()
    GuildBanquetManager:GuildBanquetEnterReq()
end

function GuildBanquetView:RefreshView()

end