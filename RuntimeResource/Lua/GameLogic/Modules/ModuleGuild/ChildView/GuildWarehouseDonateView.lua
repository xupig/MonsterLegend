require "Modules.ModuleGuild.ChildComponent.GuildWarehouseDonateItem"

local GuildWarehouseDonateView = Class.GuildWarehouseDonateView(ClassTypes.BaseLuaUIComponent)

GuildWarehouseDonateView.interface = GameConfig.ComponentsConfig.Component
GuildWarehouseDonateView.classPath = "Modules.ModuleGuild.ChildView.GuildWarehouseDonateView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local UIToggle = ClassTypes.UIToggle
local GuildWarehouseDonateItem = ClassTypes.GuildWarehouseDonateItem
local QualityType = GameConfig.EnumType.QualityType
local bagData = PlayerManager.PlayerDataManager.bagData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TipsManager = GameManager.TipsManager

local Min_Item_Count = 21

function GuildWarehouseDonateView:Awake()
    self._preSelected = -1
    self:InitView()
    self:InitListenerFunc()
end

function GuildWarehouseDonateView:InitListenerFunc()
    self._refreshGuildDonateSelectedEquipItem = function(pos, selectState) self:RefreshGuildDonateSelectedEquipItem(pos, selectState) end
    self._guildWarehouseSellSucc = function(pos, selectState) self:RefreshShowData() end
end

function GuildWarehouseDonateView:OnDestroy()
end

function GuildWarehouseDonateView:OnEnable()
    self:AddEventListeners()
end

function GuildWarehouseDonateView:OnDisable()
    self:RemoveEventListeners()
    self._preSelected = -1
end

function GuildWarehouseDonateView:CloseView()

end

function GuildWarehouseDonateView:ShowView(data)
    self:RefreshView()
end

function GuildWarehouseDonateView:AddEventListeners()
    
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Donate_Selected_Equip_Item, self._refreshGuildDonateSelectedEquipItem)

    EventDispatcher:AddEventListener(GameEvents.Guild_Warehouse_Sell_Succ, self._guildWarehouseSellSucc)
end

function GuildWarehouseDonateView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Donate_Selected_Equip_Item, self._refreshGuildDonateSelectedEquipItem)
    EventDispatcher:RemoveEventListener(GameEvents.Guild_Warehouse_Sell_Succ, self._guildWarehouseSellSucc)
end

function GuildWarehouseDonateView:InitView()
    self._close = self:FindChildGO("Container_PopupMiddle/Button_Close")
    self._csBH:AddClick(self._close, function() self:OnCloseButtonClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(GuildWarehouseDonateItem)
    self._list:SetPadding(20, 0, 0, 12)
    self._list:SetGap(10, 8)
    self._list:SetDirection(UIList.DirectionTopToDown, 7, 1)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._descText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._descText.text = LanguageDataHelper.CreateContentWithArgs(53072)
end

function GuildWarehouseDonateView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function GuildWarehouseDonateView:OnListItemClicked(idx)
    local data = self._list:GetDataByIndex(idx)
    if data.cfg_id == nil then
        return
    end
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelected(false)
    end
    local item = self._list:GetItem(idx)
    item:ShowSelected(true)
    self._preSelected = idx

    local btnArgs = {}
	btnArgs.guildDonate = true
    TipsManager:ShowItemTips(data, btnArgs)
end

function GuildWarehouseDonateView:RefreshView()
    self:RefreshShowData()
end

function GuildWarehouseDonateView:RefreshShowData()
    local equips = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemListByItemType(public_config.ITEM_ITEMTYPE_EQUIP)
    local needData = {}
    local count = 0

    for k,v in pairs(equips) do
        while(true) do
            if v:BindFlag() then
                break
            end
            if v.cfgData.quality < QualityType.Purple  then
                break
            end
            if v.cfgData.star < 1 then
                break
            end
            if v.cfgData.grade < 4 then
                break
            end
            table.insert(needData, v)
            count = count + 1
            break
        end
    end

    --填充空数据为了生成空格
    local addCount = math.max(Min_Item_Count, count) - count
	for i=1, addCount do
        table.insert(needData, {})
    end
    self.needData = needData
    self._list:SetDataList(needData)
    self:UpdatePowerCompare()
end

function GuildWarehouseDonateView:UpdatePowerCompare()
	--检查装备是否能装备/战力是否更强
	for i=1,#self.needData do
		local itemData = self.needData[i]
		if itemData.cfg_id ~= nil and itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP then
			if itemData:CheckVocationMatchGeneral() then
				if itemData:CheckBetterThanLoaded() then
					self._list:GetItem(i-1):SetUseAndPowerCompare(true,false,true)
				else
					self._list:GetItem(i-1):SetUseAndPowerCompare(true,false,false)
				end
			else
				self._list:GetItem(i-1):SetUseAndPowerCompare(true,true)
			end
		else
			self._list:GetItem(i-1):SetUseAndPowerCompare(false)
		end
	end
end