require "Modules.ModuleGuild.ChildComponent.GuildRedEnvelopeRecordItem"

local GuildRedEnvelopeInfoView = Class.GuildRedEnvelopeInfoView(ClassTypes.BaseLuaUIComponent)

GuildRedEnvelopeInfoView.interface = GameConfig.ComponentsConfig.Component
GuildRedEnvelopeInfoView.classPath = "Modules.ModuleGuild.ChildView.GuildRedEnvelopeInfoView"

local GUIManager = GameManager.GUIManager
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GuildRedEnvelopeRecordItem = ClassTypes.GuildRedEnvelopeRecordItem
local UIList = ClassTypes.UIList

function GuildRedEnvelopeInfoView:Awake()
    self:InitView()
end

function GuildRedEnvelopeInfoView:OnDestroy()

end

function GuildRedEnvelopeInfoView:CloseView()

end

function GuildRedEnvelopeInfoView:ShowView(data)
    self.data = data
    self:RefreshView()
end

function GuildRedEnvelopeInfoView:InitView()
    self._getButton = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self._getButton,function() self:OnBGButtonClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(GuildRedEnvelopeRecordItem)
    self._list:SetPadding(10, 0, 0, 12)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Container_Icon")

    self._selfGottenGo = self:FindChildGO("Container_SelfGotten")
    self._notGetGo = self:FindChildGO("Container_NotGet")
    self._moneyIcon = self:FindChildGO("Container_SelfGotten/Container_MoneyIcon")
    self._countText = self:GetChildComponent("Container_SelfGotten/Text_Count", "TextMeshWrapper")
    self._notGetText = self:GetChildComponent("Container_NotGet/Text_Count", "TextMeshWrapper")
    self._hasGottenText = self:GetChildComponent("Text_HasGotten", "TextMeshWrapper")
    self._totalCountText = self:GetChildComponent("Text_MoneyCount", "TextMeshWrapper")
    self._moneyIconOne = self:FindChildGO("Text_MoneyCount/Container_Icon")

    self._notGetText.text = LanguageDataHelper.CreateContent(53140)
end

function GuildRedEnvelopeInfoView:RefreshView()
    local vocation = self.data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_VOC] or 100
    local headIcon = RoleDataHelper.GetHeadIconByVocation(vocation)
    local cfgId = self.data[public_config.GUILD_RED_ENVELOPE_INFO_CFG_ID]
    local name = self.data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_NAME]
    local desc = self.data[public_config.GUILD_RED_ENVELOPE_INFO_BLESSING] or GuildDataHelper:GetGuildRedEnvelopeName(cfgId)
    GameWorld.AddIcon(self._iconContainer, headIcon, nil)
    self._nameText.text = name
    self._descText.text = desc

    local moneyType = next(GuildDataHelper:GetGuildRedEnvelopeReward(cfgId))
    local moneyIcon = ItemDataHelper.GetIcon(moneyType)
    GameWorld.AddIcon(self._moneyIcon, moneyIcon)
    GameWorld.AddIcon(self._moneyIconOne, moneyIcon)

    local totalCount = self.data[public_config.GUILD_RED_ENVELOPE_INFO_PACKET]
    local gottenCount = self.data[public_config.GUILD_RED_ENVELOPE_INFO_GET_PACKET]
    self._hasGottenText.text = LanguageDataHelper.CreateContentWithArgs(53143, {["0"]=string.format("%d/%d", gottenCount, totalCount)})

    local totalMoneyCount = self.data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_COUNT]
    local gottenMoneyCount = self.data[public_config.GUILD_RED_ENVELOPE_INFO_GOTTEN_MONEY]
    self._totalCountText.text = LanguageDataHelper.CreateContentWithArgs(53144, {["0"]=string.format("%d/%d", gottenMoneyCount, totalMoneyCount)})

    self:RefreshRecordData()
end

function GuildRedEnvelopeInfoView:OnBGButtonClick()
    self.gameObject:SetActive(false)
end

function GuildRedEnvelopeInfoView:RefreshRecordData()
    local recordData = {}
    local bestDbid = 0
    local maxCount = 0
    local selfGotten = 0
    local cfgId = self.data[public_config.GUILD_RED_ENVELOPE_INFO_CFG_ID]
    local moneyType = next(GuildDataHelper:GetGuildRedEnvelopeReward(cfgId))
    local data = self.data[public_config.GUILD_RED_ENVELOPE_INFO_SET] or {}

    local totalMoneyCount = self.data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_COUNT]
    local gottenMoneyCount = self.data[public_config.GUILD_RED_ENVELOPE_INFO_GOTTEN_MONEY]
    if gottenMoneyCount >= totalMoneyCount then
        for k,v in ipairs(data) do
            if v[3] > maxCount then
                maxCount = v[3]
                bestDbid = v[1]
            end
        end
    end
    for k,v in ipairs(data) do
        local best = false
        if bestDbid == v[1] then
            best = true
        end
        if v[1] == GameWorld.Player().dbid then
            table.insert(recordData, 1, {v[1], LanguageDataHelper.CreateContent(53145), v[3], best, moneyType})
            selfGotten = v[3]
        else
            table.insert(recordData, {v[1], v[2], v[3], best, moneyType})
        end
    end
    if selfGotten > 0 then
        --自己领取
        self._selfGottenGo:SetActive(true)
        self._notGetGo:SetActive(false)
        self._countText.text = selfGotten
    else
        self._selfGottenGo:SetActive(false)
        self._notGetGo:SetActive(true)
    end
    self._list:SetDataList(recordData)
end