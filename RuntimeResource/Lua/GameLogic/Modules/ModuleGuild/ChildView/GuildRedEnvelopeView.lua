require "Modules.ModuleGuild.ChildComponent.GuildRedEnvelopeItem"
require "Modules.ModuleGuild.ChildView.GuildFixSendRedEnvelopeView"
require "Modules.ModuleGuild.ChildView.GuildRedEnvelopeInfoView"
require "Modules.ModuleGuild.ChildView.GuildSendRedEnvelopeView"
require "Modules.ModuleGuild.ChildComponent.GuildRedEnvelopeRecordInfoItem"
require "UIComponent.Extend.TipsCom"

local GuildRedEnvelopeView = Class.GuildRedEnvelopeView(ClassTypes.BaseLuaUIComponent)

GuildRedEnvelopeView.interface = GameConfig.ComponentsConfig.Component
GuildRedEnvelopeView.classPath = "Modules.ModuleGuild.ChildView.GuildRedEnvelopeView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GuildRedEnvelopeItem = ClassTypes.GuildRedEnvelopeItem
local GuildFixSendRedEnvelopeView = ClassTypes.GuildFixSendRedEnvelopeView
local GuildRedEnvelopeInfoView = ClassTypes.GuildRedEnvelopeInfoView
local GuildSendRedEnvelopeView = ClassTypes.GuildSendRedEnvelopeView
local GuildRedEnvelopeRecordInfoItem = ClassTypes.GuildRedEnvelopeRecordInfoItem
local TipsCom = ClassTypes.TipsCom
local UIComplexList = ClassTypes.UIComplexList
local UIParticle = ClassTypes.UIParticle

local VIP_LEVEL_LIMIT = 6

local function SortByTime(a,b)
    return tonumber(a[public_config.GUILD_RED_ENVELOPE_INFO_TIME]) < tonumber(b[public_config.GUILD_RED_ENVELOPE_INFO_TIME])
end

function GuildRedEnvelopeView:Awake()
    self:InitView()
end

function GuildRedEnvelopeView:OnDestroy()

end

function GuildRedEnvelopeView:CloseView()

end

function GuildRedEnvelopeView:ShowView(data)
    GuildManager:GetGuildRedEnvelopeListReq()
    GuildManager:GetGuildRedEnvelopeRecordReq()
end

function GuildRedEnvelopeView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(GuildRedEnvelopeItem)
    self._list:SetPadding(10, 0, 0, 12)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 4, 1)

    self._list:SetDataList(GuildManager:GetRedEnvelopeList())

    self._sendButton = self:FindChildGO("Button_Send")
    self._csBH:AddClick(self._sendButton, function() self:OnSendButtonClick() end)
    self._tipsButton = self:FindChildGO("Button_Tips")
    self._csBH:AddClick(self._tipsButton, function() self:OnTipsButtonClick() end)

    self._getRedEnvelopeGo = self:FindChildGO("Container_GetRedEnvelope")
    self._getRedEnvelopeIcon = self:FindChildGO("Container_GetRedEnvelope/Container_Icon")
    self._nameText = self:GetChildComponent("Container_GetRedEnvelope/Text_Name", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Container_GetRedEnvelope/Text_Desc", "TextMeshWrapper")
    self._getButton = self:FindChildGO("Container_GetRedEnvelope/Button_Get")
    self._csBH:AddClick(self._getButton,function() self:OnGetButtonClick() end)
    self._getButton = self:FindChildGO("Container_GetRedEnvelope/Button_BG")
    self._csBH:AddClick(self._getButton,function() self:OnBGButtonClick() end)
    self._getRedEnvelopeGo:SetActive(false)
    self._fx = UIParticle.AddParticle(self.gameObject,"Container_GetRedEnvelope/Button_Get/Container_Fx/fx_ui_GetdRedEnvelope")

    self._redEnvelopeInfoGo = self:FindChildGO("Container_RedEnvelopeInfo")
    self._redEnvelopeInfoView = self:AddChildLuaUIComponent("Container_RedEnvelopeInfo", GuildRedEnvelopeInfoView)
    self._redEnvelopeInfoGo:SetActive(false)

    self._fixSendRedEnvelopeGo = self:FindChildGO("Container_FixSendRedEnvelope")
    self._fixSendRedEnvelopeView = self:AddChildLuaUIComponent("Container_FixSendRedEnvelope", GuildFixSendRedEnvelopeView)
    self._fixSendRedEnvelopeGo:SetActive(false)

    self._sendRedEnvelopeGo = self:FindChildGO("Container_SendRedEnvelope")
    self._sendRedEnvelopeView = self:AddChildLuaUIComponent("Container_SendRedEnvelope", GuildSendRedEnvelopeView)
    self._sendRedEnvelopeGo:SetActive(false)

    local scrollViewRecord,listRecord = UIComplexList.AddScrollViewComplexList(self.gameObject,'Container_Record/ScrollViewList')
    self._listRecord = listRecord
	self._scrollViewRecord = scrollViewRecord
    self._listRecord:SetItemType(GuildRedEnvelopeRecordInfoItem)
    self._listRecord:SetPadding(10, 0, 10, 12)
    self._listRecord:SetGap(5, 10)
    self._listRecord:SetDirection(UIList.DirectionTopToDown, -1, -1)

    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(53064))
    self._tipsGO:SetActive(false)

    self._rpIconObj = self:FindChildGO("Container_GetRedEnvelope/Container_RedPaperIcon")
    GameWorld.AddIcon(self._rpIconObj,13552)
end

function GuildRedEnvelopeView:OnSendButtonClick()
    if GameWorld.Player().vip_level < VIP_LEVEL_LIMIT then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53191))
        return
    end
    self._sendRedEnvelopeGo:SetActive(true)
    self._sendRedEnvelopeView:ShowView()
end

function GuildRedEnvelopeView:OnTipsButtonClick()
    self._tipsGO:SetActive(true)
end

function GuildRedEnvelopeView:RefreshRedEnvelopeList()
    local result = GuildManager:GetRedEnvelopeList()
    self.data = result
    if self._list then
        self._list:SetDataList(self.data)
    end
end

function GuildRedEnvelopeView:RefreshRedEnvelopeRecord()
    local recordInfoData = guildData.guildRedEnvelopeRecordData or {}
    self._listRecord:RemoveAll()
    self._listRecord:AppendItemDataList(recordInfoData)
    self._listRecord:MoveToBottom()
end

function GuildRedEnvelopeView:OnGetButtonClick()
    --抢红包
    self:OnBGButtonClick()
    if self.grapId > 0 then
        GuildManager:GuildGetRedEnvelopeReq(self.grapId)
    end
end

function GuildRedEnvelopeView:OnBGButtonClick()
    self._getRedEnvelopeGo:SetActive(false)
end

function GuildRedEnvelopeView:ShowGetRedEnvelopeView(data)
    self._getRedEnvelopeGo:SetActive(true)
    local cfgId = data[public_config.GUILD_RED_ENVELOPE_INFO_CFG_ID]
    local name = data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_NAME]
    local desc = GuildDataHelper:GetGuildRedEnvelopeName(cfgId)
    local vocation = data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_VOC] or 100
    local headIcon = RoleDataHelper.GetHeadIconByVocation(vocation)
    GameWorld.AddIcon(self._getRedEnvelopeIcon, headIcon, nil)
    self._nameText.text = name
    self._descText.text = desc
    self.grapId = data[public_config.GUILD_RED_ENVELOPE_INFO_ID] or 0
end

function GuildRedEnvelopeView:ShowFixSendRedEnvelopeView(data)
    self._fixSendRedEnvelopeGo:SetActive(true)
    self._fixSendRedEnvelopeView:ShowView(data)
end

function GuildRedEnvelopeView:ShowRedEnvelopeInfoView(data)
    self._redEnvelopeInfoGo:SetActive(true)
    self._redEnvelopeInfoView:ShowView(data)
end