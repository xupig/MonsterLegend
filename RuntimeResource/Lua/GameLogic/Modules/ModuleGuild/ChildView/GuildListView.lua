require "Modules.ModuleGuild.ChildComponent.GuildRankItem"

local GuildListView = Class.GuildListView(ClassTypes.BaseLuaUIComponent)

GuildListView.interface = GameConfig.ComponentsConfig.Component
GuildListView.classPath = "Modules.ModuleGuild.ChildView.GuildListView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GuildRankItem = ClassTypes.GuildRankItem

function GuildListView:Awake()
    self:InitView()
end

function GuildListView:OnDestroy()

end

function GuildListView:CloseView()

end

function GuildListView:ShowView(data)
    GuildManager:GuildRankListReq()
end

function GuildListView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(GuildRankItem)
    self._list:SetPadding(10, 0, 0, 12)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function GuildListView:RefreshView()
    self._list:SetDataList(guildData.guildRankList)
end