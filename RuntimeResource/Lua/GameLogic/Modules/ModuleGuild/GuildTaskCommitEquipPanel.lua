local GuildTaskCommitEquipPanel = Class.GuildTaskCommitEquipPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local BagManager = PlayerManager.BagManager
local WeekTaskManager = PlayerManager.WeekTaskManager
local TaskManager = PlayerManager.TaskManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

require "Modules.ModuleGuild.ChildComponent.GuildEquipListItem"
local GuildEquipListItem = ClassTypes.GuildEquipListItem

--override
function GuildTaskCommitEquipPanel:Awake()
    self.disableCameraCulling = true
    self._csBH = self:GetComponent('LuaUIPanel')
    self:InitViews()
end

--override
function GuildTaskCommitEquipPanel:OnDestroy()
	self._csBH = nil
    self:RemoveEventListeners()
end

--override 
function GuildTaskCommitEquipPanel:OnShow(data)
    self:UpdateData(data)
    self:AddEventListeners()
end

--override
function GuildTaskCommitEquipPanel:OnClose()

end

function GuildTaskCommitEquipPanel:AddEventListeners()
   
end

function GuildTaskCommitEquipPanel:RemoveEventListeners()

end

function GuildTaskCommitEquipPanel:InitViews()
    self._noneContainer = self:FindChildGO("Container_None")
    self._equipContainer = self:FindChildGO("Container_Equip")

    self._shadeButton = self:FindChildGO("Button_Shade")
    self._csBH:AddClick(self._shadeButton, function() self:OnCloseClick() end)

    self._closeButton = self:FindChildGO("Container_Equip/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnCloseClick() end)
    
    self._marketButton = self:FindChildGO("Container_None/Button_Market")
    self._csBH:AddClick(self._marketButton, function() self:OnMarketClick() end)
    
    self._repertoryButton = self:FindChildGO("Container_None/Button_Repertory")
    self._csBH:AddClick(self._repertoryButton, function() self:OnRepertoryButtonClick() end)
    
    self._fastCompleteButton = self:FindChildGO("Container_None/Button_FastComplete")
    self._csBH:AddClick(self._fastCompleteButton, function() self:OnFastCompleteClick() end)
    
    self._commitButton = self:FindChildGO("Container_Equip/Button_Commit")
    self._csBH:AddClick(self._commitButton, function() self:OnCommitClick() end)

    self:InitEquipList()
end

function GuildTaskCommitEquipPanel:InitEquipList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Equip/ScrollViewList")
	self._equipList = list
	self._equipListlView = scrollView 
    self._equipListlView:SetHorizontalMove(false)
    self._equipListlView:SetVerticalMove(true)
    self._equipList:SetItemType(GuildEquipListItem)
    self._equipList:SetPadding(6, 0, 0, 0)
    self._equipList:SetGap(2, 0)
    self._equipList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    local itemClickedCB = function(idx)
		self:OnListItemClicked(idx)
	end
	self._equipList:SetItemClickedCB(itemClickedCB)
end

function GuildTaskCommitEquipPanel:OnListItemClicked(idx)
    if self._selectIndex ~= nil then
        self._equipList:GetItem(self._selectIndex):SetSelect(false)
    end
    self._selectIndex = idx
    self._equipList:GetItem(self._selectIndex):SetSelect(true)
end

function GuildTaskCommitEquipPanel:UpdateData(data)
    self._equipListData = BagManager:GetEquipByQulityAndLevel(data.quality, data.level, GlobalParamsHelper.GetParamValue(850))
    if #self._equipListData == 0 then
        self._noneContainer:SetActive(true)
        self._equipContainer:SetActive(false)
    else
        self._noneContainer:SetActive(false)
        self._equipContainer:SetActive(true)
        self._equipList:SetDataList(self._equipListData)
        self._selectIndex = nil
    end
end

--提交装备
function GuildTaskCommitEquipPanel:OnCommitClick()
    -- LoggerHelper.Error("self._selectIndex ===" .. tostring(self._selectIndex))
    if self._selectIndex == nil then
        GUIManager.ShowText(2, LanguageDataHelper.CreateContent(53394))
        return
    end
    local pos = self._equipListData[self._selectIndex + 1]:GetPos()
    -- LoggerHelper.Error("pos ===" .. tostring(pos))
    WeekTaskManager:CommitEquip(pos, 1)
    self:OnCloseClick()
end

--快速完成
function GuildTaskCommitEquipPanel:OnFastCompleteClick()
    self:OnCloseClick()
    local cb = function() WeekTaskManager:FastCommitTask() end
    local text = LanguageDataHelper.CreateContent(53462, {["0"] = GlobalParamsHelper.GetParamValue(836)})
    GUIManager.ShowDailyTips(SettingType.WEEK_COMMIT_EQUIP_FAST_COMPLETE, cb,nil, text, true)
end

--公会仓库
function GuildTaskCommitEquipPanel:OnRepertoryButtonClick()
    self:OnCloseClick()
    GUIManager.ShowPanelByFunctionId(413)
end

--市场
function GuildTaskCommitEquipPanel:OnMarketClick()
    self:OnCloseClick()
    GUIManager.ShowPanelByFunctionId(419)
end

--关闭
function GuildTaskCommitEquipPanel:OnCloseClick()
    GUIManager.ClosePanel(PanelsConfig.GuildTaskCommitEquip)
end