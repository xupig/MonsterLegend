VocationChangeModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function VocationChangeModule.Init()
    GUIManager.AddPanel(PanelsConfig.VocationChange,"Panel_VocationChange",GUILayer.LayerUIPanel,"Modules.ModuleVocationChange.VocationChangePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return VocationChangeModule