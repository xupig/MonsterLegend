local VocationChangeView = Class.VocationChangeView(ClassTypes.BaseLuaUIComponent)

require "Modules.ModuleVocationChange.ChildComponent.VocationChangeValueItem"
local VocationChangeValueItem = ClassTypes.VocationChangeValueItem

require "Modules.ModuleVocationChange.ChildComponent.VocationChangeStepItem"
local VocationChangeStepItem = ClassTypes.VocationChangeStepItem

require "Modules.ModuleVocationChange.ChildComponent.VocationSkillUpgradeItem"
local VocationSkillUpgradeItem = ClassTypes.VocationSkillUpgradeItem

require "Modules.ModuleVocationChange.ChildComponent.VocationSkillAddItem"
local VocationSkillAddItem = ClassTypes.VocationSkillAddItem

require "Modules.ModuleVocationChange.ChildView.VocationChangeOneView"
local VocationChangeOneView = ClassTypes.VocationChangeOneView

require "Modules.ModuleVocationChange.ChildView.VocationChangeTwoView"
local VocationChangeTwoView = ClassTypes.VocationChangeTwoView

require "Modules.ModuleVocationChange.ChildView.VocationChangeThreeView"
local VocationChangeThreeView = ClassTypes.VocationChangeThreeView

-- require "Modules.ModuleVocationChange.ChildComponent.VocationChangeRewardItem"
-- local VocationChangeRewardItem = ClassTypes.VocationChangeRewardItem


VocationChangeView.interface = GameConfig.ComponentsConfig.Component
VocationChangeView.classPath = "Modules.ModuleVocationChange.ChildView.VocationChangeView"

local VocationChangeManager = PlayerManager.VocationChangeManager
local VocationChangeConfig = GameConfig.VocationChangeConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local ChangeVocationDataHelper = GameDataHelper.ChangeVocationDataHelper
local ChangeVocationViewDataHelper = GameDataHelper.ChangeVocationViewDataHelper
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local TaskManager = PlayerManager.TaskManager
local TracingPointData = ClassTypes.TracingPointData
local UIList = ClassTypes.UIList
local TaskCommonManager = PlayerManager.TaskCommonManager
local PlayerModelComponent = GameMain.PlayerModelComponent
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemConfig = GameConfig.ItemConfig
local TaskConfig = GameConfig.TaskConfig

function VocationChangeView:Awake()
    self:InitView()
    self:InitCallBack()
end
--override
function VocationChangeView:OnDestroy() 
    self:RemoveEventListeners()
end

function VocationChangeView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.OnTaskChanged, self._onTaskChange)
end

function VocationChangeView:RemoveEventListeners()  
    EventDispatcher:RemoveEventListener(GameEvents.OnTaskChanged, self._onTaskChange)
end

function VocationChangeView:InitCallBack()
    self._onTaskChange = function() 
        -- LoggerHelper.Error("任务刷新")
        -- self:RefreshTaskStepInfo() 
        self:RefreshView() 
    end
end

function VocationChangeView:InitView()
    self._vocationChangeViews = {}
    self._vocationChangeOneView = self:AddChildLuaUIComponent("Container_Vocation_1", VocationChangeOneView)
    self._vocationChangeTwoView = self:AddChildLuaUIComponent("Container_Vocation_2", VocationChangeTwoView)
    self._vocationChangeThreeView = self:AddChildLuaUIComponent("Container_Vocation_3", VocationChangeThreeView)
    table.insert(self._vocationChangeViews, self._vocationChangeOneView)
    table.insert(self._vocationChangeViews, self._vocationChangeTwoView)
    table.insert(self._vocationChangeViews, self._vocationChangeThreeView)

    self._modelContainer = self:FindChildGO("Container_Common/Container_Model")
    self._modelComponent = self:AddChildComponent('Container_Common/Container_Model', PlayerModelComponent)
    
    self._srcNameText = self:GetChildComponent("Container_Common/Container_Title/Text_Src",'TextMeshWrapper')    
    self._dstNameText = self:GetChildComponent("Container_Common/Container_Title/Text_Dst",'TextMeshWrapper')
    self._stageText = self:GetChildComponent("Container_Common/Container_Content/Image_Title_Bg/Text_Title",'TextMeshWrapper')
    self._taskTargetText = self:GetChildComponent("Container_Common/Container_Content/Text_Target",'TextMeshWrapper')
    self._completeTextGo = self:FindChildGO("Container_Common/Container_Content/Text_Complete")

    self._taskGoButton = self:FindChildGO("Container_Common/Container_Content/Button")
    self._csBH:AddClick(self._taskGoButton, function() self:OnTskButtonClick() end)

    self._stagePreButton = self:FindChildGO("Container_Common/Container_Content/Button_Pre")
    self._csBH:AddClick(self._stagePreButton, function() self:OnStagePreButtonClick() end)

    self._stageNextButton = self:FindChildGO("Container_Common/Container_Content/Button_Next")
    self._csBH:AddClick(self._stageNextButton, function() self:OnStageNextButtonClick() end)

    self._fastCompleteButton = self:FindChildGO("Container_Common/Button_Fast_Change")
    self._csBH:AddClick(self._fastCompleteButton, function() self:OnFastCompleteButtonClick() end)

    -- --普攻强化
    -- self._commonContainer = self:FindChildGO("Container_Change/Container_Common")
    -- self._nowCommonIcon = self:FindChildGO("Container_Change/Container_Common/Container_Now_Icon")
    -- self._upgradeCommonIcon = self:FindChildGO("Container_Change/Container_Common/Container_Upgrade_Icon")
    -- self._nowCommonText = self:GetChildComponent("Container_Change/Container_Common/Text_Now",'TextMeshWrapper')    
    -- self._upgradeCommonText = self:GetChildComponent("Container_Change/Container_Common/Text_Upgrade",'TextMeshWrapper')  
    
    -- --解锁装备
    -- self._equipmentText = self:GetChildComponent("Container_Change/Container_Equipment/Text_Equipment",'TextMeshWrapper')  

    -- --开放幻化
    -- self._openText = self:GetChildComponent("Container_Change/Container_Open/Text_Open",'TextMeshWrapper')  

    -- self._upgradeSkillContainer = self:FindChildGO("Container_Change/Container_Upgrade_Skill")
    -- self._addSkillContainer = self:FindChildGO("Container_Change/Container_Add_Skill")

    -- self._titleText = self:GetChildComponent("Text_Title",'TextMeshWrapper')    
    -- self._nowNameText = self:GetChildComponent("Container_Change_Title/Text_Now",'TextMeshWrapper')    
    -- self._upgradeNameText = self:GetChildComponent("Container_Change_Title/Text_Change",'TextMeshWrapper')    
    -- self._taskDescText = self:GetChildComponent("Container_Change/Container_Step/Text_Desc",'TextMeshWrapper')   
    -- self._targetText = self:GetChildComponent("Container_Change/Container_Step/Image_Target/Text_Target",'TextMeshWrapper')   
    -- self._target = self:FindChildGO("Container_Change/Container_Step/Image_Target")

    -- --阶段奖励
    -- -- self._stageRewardButton = self:FindChildGO("Container_Step_Reward/Text_Step_Reward")
    -- -- self._stageRewardContainer = self:FindChildGO("Container_Step_Reward/Container_Reward")

    -- self._taskGoButton = self:FindChildGO("Container_Change/Container_Step/Button_Go")
    -- self._csBH:AddClick(self._taskGoButton, function() self:OnTskButtonClick() end)

    -- self._taskCompleteButton = self:FindChildGO("Container_Change/Container_Step/Button_Change_Success")
    -- self._csBH:AddClick(self._taskCompleteButton, function() self:OnCloseButtonClick() end)

    -- self._closeButton = self:FindChildGO("Button_Close")
    -- self._csBH:AddClick(self._closeButton, function() self:OnCloseButtonClick() end)

    -- self:InitValueGridLayoutGroup()
    -- self:InitStepGridLayoutGroup()
    -- self:InitSkillUpgradeList()
    -- self:InitSkillAddList()
    -- self:InitStageGetRewardGridLayoutGroup()
    -- self:InitStageNextRewardGridLayoutGroup()
end

function VocationChangeView:InitStageNextRewardGridLayoutGroup()
    -- self._stageNextRewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Step_Reward/Container_Reward/Container_Next")
    -- self._stageNextRewardGridLauoutGroup:SetItemType(VocationChangeRewardItem)
end

function VocationChangeView:InitStageGetRewardGridLayoutGroup()
    -- self._stageGetRewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Step_Reward/Container_Reward/Container_Get")
    -- self._stageGetRewardGridLauoutGroup:SetItemType(VocationChangeRewardItem)
end

function VocationChangeView:InitSkillAddList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Change/Container_Add_Skill/ScrollViewList")
	self._skillAddList = list
	self._skillAddListtView = scrollView 
    self._skillAddListtView:SetHorizontalMove(false)
    self._skillAddListtView:SetVerticalMove(false)
    self._skillAddList:SetItemType(VocationSkillAddItem)
    self._skillAddList:SetPadding(0, 0, 0, 0)
    self._skillAddList:SetGap(0, 0)
    self._skillAddList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function VocationChangeView:InitSkillUpgradeList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Change/Container_Upgrade_Skill/ScrollViewList")
	self._skillUpgradeList = list
	self._skillUpgradeListView = scrollView 
    self._skillUpgradeListView:SetHorizontalMove(false)
    self._skillUpgradeListView:SetVerticalMove(false)
    self._skillUpgradeList:SetItemType(VocationSkillUpgradeItem)
    self._skillUpgradeList:SetPadding(0, 0, 0, 0)
    self._skillUpgradeList:SetGap(0, 0)
    self._skillUpgradeList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function VocationChangeView:InitStepGridLayoutGroup()
    self._stepGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Change/Container_Step/Container_Step")
    self._stepGridLauoutGroup:SetItemType(VocationChangeStepItem)

    local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
	self._stepGridLauoutGroup:SetItemClickedCB(itemClickedCB)
end

function VocationChangeView:InitValueGridLayoutGroup()
    self._valueGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Change/Container_Atrri/Container_Info")
    self._valueGridLauoutGroup:SetItemType(VocationChangeValueItem)
end

function VocationChangeView:OnListItemClicked(idx)
    -- LoggerHelper.Error("VocationChangeView:OnListItemClicked(idx) .." .. tostring(idx))
    self:RefreshTaskStepInfo(idx + 1)
end

function VocationChangeView:OnEnable()
    -- LoggerHelper.Error("转职任务数据=====" .. PrintTable:TableToStr(TaskManager:GetVocationTask()))
    self:AddEventListeners()
    
    GameWorld.ShowPlayer():ShowModel(1)
    self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
end

--vocationNum:有值时，代表转职成功，为转职前的num
function VocationChangeView:RefreshView(vocationNum)
    -- LoggerHelper.Error("RefreshView :vocationNum ====" .. tostring(vocationNum))
    local stage
    local isComplete    
    if vocationNum ~= nil then
        --显示已经完成了的转职信息
        stage = ChangeVocationDataHelper:GetMaxStageByChangeNum(vocationNum)
        self._isComplete = true
        self._fastCompleteButton:SetActive(false)
    else
        --现在当前阶段的转职信息
        local task = VocationChangeManager:GetNextStepTask(vocationNum)
        stage = ChangeVocationDataHelper:GetStage(ChangeVocationDataHelper:GetIdByTaskId(task:GetId()))
        -- LoggerHelper.Error("stage ==== " .. tostring(stage) .. "==" .. type(stage))
        -- LoggerHelper.Error("taskid ==== " .. tostring(task:GetId()))
        vocationNum = GameWorld.Player():GetVocationChange()
        --LoggerHelper.Error("RefreshView :vocationNum ====" .. tostring(vocationNum))
        --LoggerHelper.Error("flag ====" .. tostring(vocationNum == 2))
        self._isComplete = false
        if vocationNum == 2 then
            local vocationTask = TaskManager:GetVocationTask()
            if table.isEmpty(vocationTask) then
                --LoggerHelper.Error("转职任务id ====== 空")
                self._fastCompleteButton:SetActive(false)
            else
                local taskId = GlobalParamsHelper.GetParamValue(866)
                local flag = true
                for i, task in pairs(vocationTask) do
                    --LoggerHelper.Error("转职任务id ====== " .. task:GetId())
                    if task:GetId() >= taskId then
                        flag= false
                        break
                    end            
                end
                self._fastCompleteButton:SetActive(flag)
            end
        else
            self._fastCompleteButton:SetActive(false)
        end
    end
    -- LoggerHelper.Error("vocationnum =====" .. tostring(vocationNum))
    
    self:SetTitle(vocationNum)
    self:RefreshInfo(self._isComplete, vocationNum, stage)
    -- if vocationNum ~= nil then
    --     self._taskCompleteButton:SetActive(true)
    --     self._taskGoButton:SetActive(false) 
    --     self._changeVocation = true
    -- else
    --     self._taskCompleteButton:SetActive(false)
    --     self._taskGoButton:SetActive(true) 
    -- end

    -- self:RefreshTaskStepInfo()
    -- self:SetValue(vocationNum)
end

function VocationChangeView:OnDisable()
    self._taskData = nil
    self._changeVocation = nil
    self:RemoveEventListeners()
    GameWorld.ShowPlayer():ShowModel(3)
end


--isComplete：是否显示已经完成的转职信息
function VocationChangeView:RefreshInfo(isComplete, vocationNum, stage)
    self._stage = stage
    self._vocationNum = vocationNum
    local nowVocationNum = GameWorld.Player():GetVocationChange()
    local task = VocationChangeManager:GetNextStepTask()
    local nowStage = ChangeVocationDataHelper:GetStage(ChangeVocationDataHelper:GetIdByTaskId(task:GetId()))
    local maxStage = ChangeVocationDataHelper:GetMaxStageByChangeNum(vocationNum)
    if self._stage > stage then
        self._stageNextButton:SetActive(true)
        if stage == 1 then
            self._stagePreButton:SetActive(false)
        else
            self._stagePreButton:SetActive(true)
        end
    elseif self._stage == stage then        
        if nowVocationNum == vocationNum then
            self._stageNextButton:SetActive(false)
            if stage == 1 then
                self._stagePreButton:SetActive(false)
            else
                self._stagePreButton:SetActive(true)
            end
        else
            if stage == 1 then
                self._stagePreButton:SetActive(false)
                self._stageNextButton:SetActive(true)
            elseif stage == ChangeVocationDataHelper:GetMaxStageByChangeNum(vocationNum) then
                self._stagePreButton:SetActive(true)
                self._stageNextButton:SetActive(false)
            else
                self._stagePreButton:SetActive(true)
                self._stageNextButton:SetActive(true)
            end
        end
    else
        if stage == 1 then
            self._stagePreButton:SetActive(false)
            self._stageNextButton:SetActive(true)
        elseif stage == ChangeVocationDataHelper:GetMaxStageByChangeNum(vocationNum) then
            self._stagePreButton:SetActive(true)
            self._stageNextButton:SetActive(false)
        else
            self._stagePreButton:SetActive(true)
            self._stageNextButton:SetActive(true)
        end
    end

    if (not isComplete) and nowVocationNum == vocationNum and nowStage == stage and (not table.isEmpty(TaskManager:GetVocationTask())) then
        self._taskGoButton:SetActive(true)
    else
        self._taskGoButton:SetActive(false)
    end

    self._stageText.text =  VocationChangeConfig.STAGE_CHINESE_MAP[stage]
    self._taskTargetText.text = VocationChangeManager:GetVocationTargetText(vocationNum, stage)
    if vocationNum ~= nowVocationNum and stage == maxStage then
        self._completeTextGo:SetActive(true)
    else
        self._completeTextGo:SetActive(false)
    end

    for i=1,3 do
        self._vocationChangeViews[i]:SetActive(i == (vocationNum + 1))
        if i == (vocationNum + 1) then
            self._vocationChangeViews[i]:SetValues(vocationNum)
        end
    end

    if vocationNum == 2 then
        self._vocationChangeThreeView:SetAddAttri(stage)
    end
end

function VocationChangeView:SetTitle(vocationNum)
    self._vocationGroupId = GameWorld.Player():GetVocationGroup()
    self._vocationNum = vocationNum or GameWorld.Player():GetVocationChange()
    local nowName = VocationChangeManager:GetVocationName(self._vocationGroupId, self._vocationNum)
    local upgradeName = VocationChangeManager:GetVocationName(self._vocationGroupId, (self._vocationNum + 1))
    self._srcNameText.text = nowName
    self._dstNameText.text = upgradeName
end

function VocationChangeView:SetValue(vocationNum)    
    self._vocationGroupId = GameWorld.Player():GetVocationGroup()
    self._vocationNum = vocationNum or GameWorld.Player():GetVocationChange()
    local nowName = VocationChangeManager:GetVocationName(self._vocationGroupId, self._vocationNum)
    local upgradeName = VocationChangeManager:GetVocationName(self._vocationGroupId, (self._vocationNum + 1))
    self._nowNameText.text = nowName
    self._upgradeNameText.text = upgradeName
    self._titleText.text = LanguageDataHelper.CreateContent(VocationChangeConfig.TITLE_NAME_MAP[self._vocationNum])
    local id = self._vocationGroupId * 100 + self._vocationNum + 1
    local flag = true

    local commonData = ChangeVocationViewDataHelper:GetAttackUp(id)
    local upgradeData = ChangeVocationViewDataHelper:GetSkillUp(id)
    local newSkillData = ChangeVocationViewDataHelper:GetNewSkill(id)
    local equipmentData = ChangeVocationViewDataHelper:GetEquipOpen(id)
    local openData = ChangeVocationViewDataHelper:GetModelOpen(id)
    local atrriData = ChangeVocationViewDataHelper:GetAtrri(id)

    --普攻强化
    if commonData ~= nil then
        flag = false
        self._commonContainer:SetActive(true)
        self._upgradeSkillContainer:SetActive(false)
        self._addSkillContainer:SetActive(false)

        self:RefreshCommonData(commonData)
    end

    --技能强化
    if flag and  upgradeData ~= nil then
        flag = false
        self._commonContainer:SetActive(false)
        self._upgradeSkillContainer:SetActive(true)
        self._addSkillContainer:SetActive(false)

        self:RefreshSkillUpgradeData(upgradeData)
    end
    
    --新增技能
    if flag and  newSkillData ~= nil then
        self._commonContainer:SetActive(false)
        self._upgradeSkillContainer:SetActive(false)
        self._addSkillContainer:SetActive(true)

        self:RefreshSkillAddData(newSkillData)
    end

    local equipmentText = ""
    for i,v in ipairs(equipmentData) do
        equipmentText = equipmentText .. LanguageDataHelper.CreateContent(v) .. "  "
    end
    self._equipmentText.text = equipmentText

    local openText = ""
    for _, openValue in ipairs(openData) do
        openText = openText .. LanguageDataHelper.CreateContent(openValue) .. "  "
    end
    self._openText.text = openText

    local valueDatas = {}
    for k,v in pairs(atrriData) do
        local valueData = {}
        valueData.name = AttriDataHelper:GetName(k)
        valueData.text = "+" .. v
        table.insert(valueDatas, valueData)
    end
    self._valueGridLauoutGroup:SetDataList(valueDatas)
end

--普攻强化
function VocationChangeView:RefreshCommonData(commonData)
    GameWorld.AddIcon(self._nowCommonIcon, commonData[1][1])
    self._nowCommonText.text = LanguageDataHelper.CreateContent(commonData[1][2])
    GameWorld.AddIcon(self._upgradeCommonIcon, commonData[1][3])
    self._upgradeCommonText.text = LanguageDataHelper.CreateContent(commonData[1][4])
end

--技能强化
function VocationChangeView:RefreshSkillUpgradeData(upgradeData)
    self._skillUpgradeList:SetDataList(upgradeData)
end

--新增技能
function VocationChangeView:RefreshSkillAddData(newSkillData)
    self._skillAddList:SetDataList(newSkillData)
end

function VocationChangeView:RefreshTaskStepInfo(idx)
    --self._taskData 现在选中的task实体
    self._taskData = nil
    local isEmptyTask = false
    local maxIndex--最大的能选的阶段
    local isRefresh = idx == nil
    local maxData

    if self._changeVocation then
        isEmptyTask = true        
        self._taskData = VocationChangeManager:GetNextStepTask(ChangeVocationDataHelper:GetMaxStageByChangeNum(self._vocationNum))
        maxData = VocationChangeManager:GetNextStepTask()
    else
        for k,v in pairs(TaskManager:GetVocationTask()) do
            self._taskData = v
        end
        
        maxData = self._taskData
        if self._taskData == nil then
            isEmptyTask = true
            -- LoggerHelper.Error("我是构造粗来的")
            self._taskData = VocationChangeManager:GetNextStepTask(idx)
            maxData = VocationChangeManager:GetNextStepTask()
        end
    end

    local commitedOptionalTask = TaskManager:GetCommitedOptionalTask()
    -- LoggerHelper.Error("完成任务数据=====" .. PrintTable:TableToStr(commitedOptionalTask))
    local ids = ChangeVocationDataHelper:GetIdsByTaskId(self._taskData:GetId())
    local datas = {}
    --现在做到的最新的任务id
    local tId = self._taskData:GetId()
    maxIndex = ChangeVocationDataHelper:GetStage(ChangeVocationDataHelper:GetIdByTaskId(maxData:GetId()))
    if idx == nil then
        idx = ChangeVocationDataHelper:GetStage(ChangeVocationDataHelper:GetIdByTaskId(self._taskData:GetId()))
        if idx < maxIndex then
            isEmptyTask = true
        end
        if idx == nil then return end
    end

    for i,id in ipairs(ids) do
        local taskId = ChangeVocationDataHelper:GetTaskId(id)
        -- LoggerHelper.Error("taskId ====" .. tostring(taskId))
        local data = {}
        data.select = idx == i
        if commitedOptionalTask[taskId] ~= nil then
            data.type = VocationChangeConfig.VOCATION_STATE_COMPLETE
        else
            if i == maxIndex then
                data.type = VocationChangeConfig.VOCATION_STATE_DOING
            end

            if i > maxIndex then
                data.type = VocationChangeConfig.VOCATION_STATE_NONE
            end
        end
        data.id = id
        table.insert(datas, data)
    end
    

    -- LoggerHelper.Error("转化的任务数据=====" .. PrintTable:TableToStr(commitedOptionalTask))
    -- LoggerHelper.Error("idx ====" .. tostring(idx))
    -- LoggerHelper.Error("datas =====" .. PrintTable:TableToStr(datas))
    -- LoggerHelper.Error(" isEmptyTask ==" .. tostring(isEmptyTask))
    -- LoggerHelper.Error("self._taskData =====" .. PrintTable:TableToStr(self._taskData))

    if datas[idx].type == VocationChangeConfig.VOCATION_STATE_NONE then
        do return end
    elseif datas[idx].type == VocationChangeConfig.VOCATION_STATE_COMPLETE then
        self._target:SetActive(false)
    else
        self._target:SetActive(true)
        self._targetText.text = TaskCommonManager:GetTaskCurStepText(self._taskData, false)
    end
    
    self._stepGridLauoutGroup:SetDataList(datas)
    self._taskDescText.text = LanguageDataHelper.CreateContent(self._taskData:GetDesc())
end

function VocationChangeView:OnCloseButtonClick()
    GameManager.GUIManager.ClosePanel(PanelsConfig.VocationChange)
end

function VocationChangeView:OnTskButtonClick()    
    if table.isEmpty(TaskManager:GetVocationTask()) then
        return GameManager.GUIManager.ShowText(1, "等级不足")
    end

    local tasks =  PlayerManager.TaskManager:GetVocationTask() 
    local task
    for i, t in pairs(tasks) do
        task = t
    end

    -- local eventItemData = TaskCommonManager:GetNextEvent(self._taskData)
    local eventItemData = TaskCommonManager:GetNextEvent(task)
    local trackTask = TracingPointData(task, eventItemData)
    local fly = false
    if GameWorld.Player().vip_level > 0 then
        fly = true
    end
    TaskCommonManager:OnManualHandleTask(trackTask, fly, TaskConfig.VOCATION_TO_TASK)    
    self:OnCloseButtonClick()
    -- self:OnCloseButtonClick()
end

function VocationChangeView:OnStagePreButtonClick()
    self:RefreshInfo(self._isComplete, self._vocationNum, self._stage - 1)
end

function VocationChangeView:OnStageNextButtonClick()
    self:RefreshInfo(self._isComplete, self._vocationNum, self._stage + 1)
end

--一键完成三转
function VocationChangeView:OnFastCompleteButtonClick()
    VocationChangeManager:FastCompleteVocation3()
    -- local a = {}
    -- local data = {}
    -- a.id = MessageBoxType.Tip
    -- a.value =  data
    -- data.confirmCB=function()self:FastVocationChange() end
    -- data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.Tips) end
    -- data.text = LanguageDataHelper.CreateContent(73135,{["0"] = GlobalParamsHelper.GetParamValue(854)})
    -- GUIManager.ShowPanel(PanelsConfig.MessageBox, a)
end

function VocationChangeView:FastVocationChange()
    local cost = GlobalParamsHelper.GetParamValue(854)
    local haveCount = GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS]]
    if haveCount < cost then
        local a = {}
        local data = {}
        a.id = MessageBoxType.Tip
        a.value =  data
        data.confirmCB=function()self:GotoCharge() end
        data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.Tips) end
        data.text = LanguageDataHelper.CreateContent(73137)
        GUIManager.ShowPanel(PanelsConfig.MessageBox, a)
    else
        VocationChangeManager:FastVocationChange()
        GUIManager.ClosePanel(PanelsConfig.Tips)
    end
end

function VocationChangeView:GotoCharge()
    GUIManager.ClosePanel(PanelsConfig.VocationChange)
    GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_CHARGE)
end

function VocationChangeView:ShowModel()
    self._modelContainer:SetActive(true)
end

function VocationChangeView:HideModel()
    self._modelContainer:SetActive(false)
end