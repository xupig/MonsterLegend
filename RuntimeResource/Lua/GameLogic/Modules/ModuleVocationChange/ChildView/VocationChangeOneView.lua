local VocationChangeOneView = Class.VocationChangeOneView(ClassTypes.BaseLuaUIComponent)

VocationChangeOneView.interface = GameConfig.ComponentsConfig.Component
VocationChangeOneView.classPath = "Modules.ModuleVocationChange.ChildView.VocationChangeOneView"

local ChangeVocationViewDataHelper = GameDataHelper.ChangeVocationViewDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup

require "Modules.ModuleVocationChange.ChildComponent.VocationChangeValueItem"
local VocationChangeValueItem = ClassTypes.VocationChangeValueItem

function VocationChangeOneView:Awake()
    self:InitView()
    self:InitCallBack()
end
--override
function VocationChangeOneView:OnDestroy() 
    self:RemoveEventListeners()
end

function VocationChangeOneView:AddEventListeners()
end

function VocationChangeOneView:RemoveEventListeners()  
end

function VocationChangeOneView:InitCallBack()
end

function VocationChangeOneView:InitView()
    --普攻强化
    self._nowCommonIcon = self:FindChildGO("Container_Left/Container_Now_Icon")
    self._upgradeCommonIcon = self:FindChildGO("Container_Left/Container_Upgrade_Icon")
    self._nowCommonText = self:GetChildComponent("Container_Left/Text_Now",'TextMeshWrapper')    
    self._upgradeCommonText = self:GetChildComponent("Container_Left/Text_Upgrade",'TextMeshWrapper')

    --解锁装备
    self._equipmentText = self:GetChildComponent("Container_Right/Container_Equipment/Text_Equipment",'TextMeshWrapper')

    --开放幻化
    self._openText = self:GetChildComponent("Container_Right/Container_Open/Text_Open",'TextMeshWrapper')
    
    self:InitValueGridLayoutGroup()
end

function VocationChangeOneView:InitValueGridLayoutGroup()
    self._valueGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Right/Container_Atrri/Container_Info")
    self._valueGridLauoutGroup:SetItemType(VocationChangeValueItem)
end

function VocationChangeOneView:OnEnable()
    self:AddEventListeners()
    -- self:SetValues()
end

function VocationChangeOneView:OnDisable()
    self:RemoveEventListeners()
end

function VocationChangeOneView:SetValues(vocationNum)    
    self._vocationGroupId = GameWorld.Player():GetVocationGroup()
    local id = self._vocationGroupId * 100 + vocationNum + 1
    -- LoggerHelper.Error("id ======" .. tostring(id))
    local commonData = ChangeVocationViewDataHelper:GetAttackUp(id)
    self:RefreshCommonData(commonData)
    
    local equipmentData = ChangeVocationViewDataHelper:GetEquipOpen(id)
    local equipmentText = ""
    for i,v in ipairs(equipmentData) do
        equipmentText = equipmentText .. LanguageDataHelper.CreateContent(v) .. "  "
    end
    self._equipmentText.text = equipmentText

    
    local openData = ChangeVocationViewDataHelper:GetModelOpen(id)
    local openText = ""
    for _, openValue in ipairs(openData) do
        openText = openText .. LanguageDataHelper.CreateContent(openValue) .. "  "
    end
    self._openText.text = openText

    local atrriData = ChangeVocationViewDataHelper:GetAtrri(id)
    local valueDatas = {}
    for k,v in pairs(atrriData) do
        local valueData = {}
        valueData.name = AttriDataHelper:GetName(k)
        valueData.text = "+" .. v
        table.insert(valueDatas, valueData)
    end
    self._valueGridLauoutGroup:SetDataList(valueDatas)
end

function VocationChangeOneView:RefreshCommonData(commonData)
    GameWorld.AddIcon(self._nowCommonIcon, commonData[1][1])
    self._nowCommonText.text = LanguageDataHelper.CreateContent(commonData[1][2])
    GameWorld.AddIcon(self._upgradeCommonIcon, commonData[1][3])
    self._upgradeCommonText.text = LanguageDataHelper.CreateContent(commonData[1][4])
end