local VocationChangeTwoView = Class.VocationChangeTwoView(ClassTypes.BaseLuaUIComponent)

VocationChangeTwoView.interface = GameConfig.ComponentsConfig.Component
VocationChangeTwoView.classPath = "Modules.ModuleVocationChange.ChildView.VocationChangeTwoView"

local ChangeVocationViewDataHelper = GameDataHelper.ChangeVocationViewDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup

require "Modules.ModuleVocationChange.ChildComponent.VocationChangeValueItem"
local VocationChangeValueItem = ClassTypes.VocationChangeValueItem

function VocationChangeTwoView:Awake()
    self:InitView()
    self:InitCallBack()
end
--override
function VocationChangeTwoView:OnDestroy() 
    self:RemoveEventListeners()
end

function VocationChangeTwoView:AddEventListeners()
end

function VocationChangeTwoView:RemoveEventListeners()  
end

function VocationChangeTwoView:InitCallBack()
end

function VocationChangeTwoView:InitView()
    --普攻强化
    self._nowCommonIcon = self:FindChildGO("Container_Left/Container_Common/Container_Now_Icon")
    self._upgradeCommonIcon = self:FindChildGO("Container_Left/Container_Common/Container_Upgrade_Icon")
    self._nowCommonText = self:GetChildComponent("Container_Left/Container_Common/Text_Now",'TextMeshWrapper')    
    self._upgradeCommonText = self:GetChildComponent("Container_Left/Container_Common/Text_Upgrade",'TextMeshWrapper')

    --技能强化
    self._upgradeOneIcon = self:FindChildGO("Container_Left/Container_Upgrade/Container_Upgrade_Icon_One")
    self._upgradeTwoIcon = self:FindChildGO("Container_Left/Container_Upgrade/Container_Upgrade_Icon_Two")
    self._upgradeOneText = self:GetChildComponent("Container_Left/Container_Upgrade/Text_One",'TextMeshWrapper')    
    self._upgradeTwoText = self:GetChildComponent("Container_Left/Container_Upgrade/Text_Two",'TextMeshWrapper')

    --解锁装备
    self._equipmentText = self:GetChildComponent("Container_Right/Container_Equipment/Text_Equipment",'TextMeshWrapper')

    --开放幻化
    self._openText = self:GetChildComponent("Container_Right/Container_Open/Text_Open",'TextMeshWrapper')
    
    self:InitValueGridLayoutGroup()
end

function VocationChangeTwoView:InitValueGridLayoutGroup()
    self._valueGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Right/Container_Atrri/Container_Info")
    self._valueGridLauoutGroup:SetItemType(VocationChangeValueItem)
end

function VocationChangeTwoView:OnEnable()
    self:AddEventListeners()
    -- self:SetValues()
end

function VocationChangeTwoView:OnDisable()
    self:RemoveEventListeners()
end

function VocationChangeTwoView:SetValues(vocationNum)    
    self._vocationGroupId = GameWorld.Player():GetVocationGroup()
    local id = self._vocationGroupId * 100 + vocationNum + 1
    -- LoggerHelper.Error("id ======" .. tostring(id))
    local commonData = ChangeVocationViewDataHelper:GetAttackUp(id)
    self:RefreshCommonData(commonData)

    local upgradeData = ChangeVocationViewDataHelper:GetSkillUp(id)
    self:RefreshUpgradeSkillData(upgradeData)
    
    local equipmentData = ChangeVocationViewDataHelper:GetEquipOpen(id)
    local equipmentText = ""
    for i,v in ipairs(equipmentData) do
        equipmentText = equipmentText .. LanguageDataHelper.CreateContent(v) .. "  "
    end
    self._equipmentText.text = equipmentText

    
    local openData = ChangeVocationViewDataHelper:GetModelOpen(id)
    local openText = ""
    for _, openValue in ipairs(openData) do
        openText = openText .. LanguageDataHelper.CreateContent(openValue) .. "  "
    end
    self._openText.text = openText

    local atrriData = ChangeVocationViewDataHelper:GetAtrri(id)
    local valueDatas = {}
    for k,v in pairs(atrriData) do
        local valueData = {}
        valueData.name = AttriDataHelper:GetName(k)
        valueData.text = "+" .. v
        table.insert(valueDatas, valueData)
    end
    self._valueGridLauoutGroup:SetDataList(valueDatas)
end

function VocationChangeTwoView:RefreshCommonData(commonData)
    GameWorld.AddIcon(self._nowCommonIcon, commonData[1][1])
    self._nowCommonText.text = LanguageDataHelper.CreateContent(commonData[1][2])
    GameWorld.AddIcon(self._upgradeCommonIcon, commonData[1][3])
    self._upgradeCommonText.text = LanguageDataHelper.CreateContent(commonData[1][4])
end

function VocationChangeTwoView:RefreshUpgradeSkillData(upgradeData)
    local data = upgradeData[1]
    local icon = data[1]
    local upText = data[2]
    local downText = data[3]
    GameWorld.AddIcon(self._upgradeOneIcon, icon)
    self._upgradeOneText.text = LanguageDataHelper.CreateContent(downText)

    data = upgradeData[2]
    icon = data[1]
    upText = data[2]
    downText = data[3]
    GameWorld.AddIcon(self._upgradeTwoIcon, icon)
    self._upgradeTwoText.text = LanguageDataHelper.CreateContent(downText)
end