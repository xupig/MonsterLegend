-- VocationChangeFourView.lua
local VocationChangeFourView = Class.VocationChangeFourView(ClassTypes.BaseComponent)

VocationChangeFourView.interface = GameConfig.ComponentsConfig.Component
VocationChangeFourView.classPath = "Modules.ModuleVocationChange.ChildView.VocationChangeFourView"

local VocationChangeManager = PlayerManager.VocationChangeManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local GUIManager = GameManager.GUIManager
local ChangeVocationNum4DataHelper = GameDataHelper.ChangeVocationNum4DataHelper
local public_config = GameWorld.public_config
local bagData = PlayerManager.PlayerDataManager.bagData
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local FontStyleHelper = GameDataHelper.FontStyleHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local UIParticle = ClassTypes.UIParticle

require "Modules.ModuleVocationChange.ChildComponent.VocationChangeAttrItem"
local VocationChangeAttrItem = ClassTypes.VocationChangeAttrItem

function VocationChangeFourView:Awake()
    self:InitViews()
    self:InitCallBack()
end

function VocationChangeFourView:InitViews()
    self._tips = false

    self._tipsIcons = {}
    for i=1,12 do
        table.insert(self._tipsIcons, self:FindChildGO("Container_Content/Container_Active/Container_Active_Tips/Container_Common/Container_Icon/Image_Frame/Image_Icon_" .. i))
    end

    self._activeButtonFxViews = {}
    for i=1,12 do
        table.insert(self._activeButtonFxViews, self:InitActiveButtonFx(i))
        self._activeButtonFxViews[i]:Stop()
    end

    self._activeButtonViews = {}
    for i=1,12 do
        table.insert(self._activeButtonViews, self:InitActiveButton(i))
    end

    self._activeSelectViews = {}
    for i=1,12 do
        table.insert(self._activeSelectViews, self:FindChildGO("Container_Content/Container_Active/Button_" .. i .. "/Image_Select"))

    end

    self._activeIconViews = {}
    for i=1,12 do
        table.insert(self._activeIconViews, self:FindChildGO("Container_Content/Container_Active/Button_" .. i .. "/Image_Active"))
    end

    
    self._nowNameText = self:GetChildComponent("Container_Left/Container_Info/Text_Src",'TextMeshWrapper') 
    self._upgradeNameText = self:GetChildComponent("Container_Left/Container_Info/Text_Dst",'TextMeshWrapper') 
    self._tipsText = self:GetChildComponent("Text_Tips",'TextMeshWrapper') 
    self._tipsText.text = LanguageDataHelper.CreateContent(73078, {["0"] = GlobalParamsHelper.GetParamValue(659)})

    --命格提示信息 start
    self._activeTipsMainContainer = self:FindChildGO("Container_Content/Container_Active/Container_Active_Tips")
    self._notActiveTipsContainer = self:FindChildGO("Container_Content/Container_Active/Container_Active_Tips/Container_Not_Active")
    self._activeTipsContainer = self:FindChildGO("Container_Content/Container_Active/Container_Active_Tips/Container_Active")
    --未激活
    self._name1Text = self:GetChildComponent("Container_Content/Container_Active/Container_Active_Tips/Container_Not_Active/Text_Name_1",'TextMeshWrapper')
    self._value1Text = self:GetChildComponent("Container_Content/Container_Active/Container_Active_Tips/Container_Not_Active/Text_Value_1",'TextMeshWrapper')
    self._name2Text = self:GetChildComponent("Container_Content/Container_Active/Container_Active_Tips/Container_Not_Active/Text_Name_2",'TextMeshWrapper')
    self._value2Text = self:GetChildComponent("Container_Content/Container_Active/Container_Active_Tips/Container_Not_Active/Text_Value_2",'TextMeshWrapper')
    self._conditionText = self:GetChildComponent("Container_Content/Container_Active/Container_Active_Tips/Container_Not_Active/Text_Condition",'TextMeshWrapper')
    --已激活
    self._acviveNumText = self:GetChildComponent("Container_Content/Container_Active/Container_Active_Tips/Container_Active/Text_Active_Num",'TextMeshWrapper')
    --命格提示信息 end

    self._activeText = self:GetChildComponent("Container_Left/Container_Require/Text_Active",'TextMeshWrapper')    
    self._levelText = self:GetChildComponent("Container_Left/Container_Require/Text_Level",'TextMeshWrapper')    
    self._titleText = self:GetChildComponent("Container_Content/Container_Active/Container_Active_Tips/Container_Common/Text_Title_Name",'TextMeshWrapper')    
    
    self._upgradeButton = self:FindChildGO("Container_Left/Container_Require/Button_Upgrade")
    self._csBH:AddClick(self._upgradeButton, function() self:OnUpgradeButtonClick() end)

    self._closeButton = self:FindChildGO("Container_Win/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnCloseButtonClick() end)
    
    self._tipsCloseButton = self:FindChildGO("Container_Content/Container_Active/Container_Active_Tips/Container_Common/Button_Close")
    self._csBH:AddClick(self._tipsCloseButton, function() self:OnTipsCloseButtonClick() end)

    self._tipsShadeButton = self:FindChildGO("Button_Shade_Active")
    self._csBH:AddClick(self._tipsShadeButton, function() self:OnActiveTipsShadeButtonClick() end)
    
    self._activeButton = self:FindChildGO("Container_Content/Container_Active/Container_Active_Tips/Container_Not_Active/Button_Active")
    self._csBH:AddClick(self._activeButton, function() self:OnActiveButtonClick() end)

    self._buyButton = self:FindChildGO("Container_Content/Container_Active/Container_Active_Tips/Container_Not_Active/Button_Buy")
    self._csBH:AddClick(self._buyButton, function() self:OnBuyButtonClick() end)
    
    --特效
    self._fx_ui_30022 = UIParticle.AddParticle(self, "Container/fx_ui_30022")--未全部点亮
    self._fx_ui_30024 = UIParticle.AddParticle(self, "Container/fx_ui_30024")--全部点亮
    self._fx_ui_30022:Stop()
    self._fx_ui_30024:Stop()

    self:InitOneAttrGridLayoutGroup()
    self:InitAllAttrGridLayoutGroup()

    self._bgIconObj = self:FindChildGO("Container_Content/Container_bgIcon")
    GameWorld.AddIcon(self._bgIconObj,13561)
end

function VocationChangeFourView:InitAllAttrGridLayoutGroup()
    self._attrAllGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Content/Container_Active/Container_Active_Tips/Container_Active/Container_Attr_All")
    self._attrAllGridLauoutGroup:SetItemType(VocationChangeAttrItem)
end

function VocationChangeFourView:InitOneAttrGridLayoutGroup()
    self._attrOneGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Content/Container_Active/Container_Active_Tips/Container_Common/Container_Attr")
    self._attrOneGridLauoutGroup:SetItemType(VocationChangeAttrItem)
end

function VocationChangeFourView:InitActiveButtonFx(id)
    return UIParticle.AddParticle(self, "Container_Content/Container_Active/Button_".. id .. "/fx_ui_30023")
end

function VocationChangeFourView:InitActiveButton(id)
    local button = self:FindChildGO("Container_Content/Container_Active/Button_" .. id)
    self._csBH:AddClick(button, function() self:OnActiveClick(id) end)
    return button
end

function VocationChangeFourView:OnEnable()    
    self._activeTipsMainContainer:SetActive(false)
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE, self._bagDataUpdate)
    
    self:AddEventListeners()
    self:RefreshDatas()
    self:SetName()
end

function VocationChangeFourView:OnDisable()
    self:RemoveEventListeners()
end

function VocationChangeFourView:InitCallBack()
    self._onStageChange = function() self:OnStageChange() end
    self._bagDataUpdate = function() self:OnBagDataUpdate() end
end

function VocationChangeFourView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.VOCATION_FOUR_STAGE_CHANGE, self._onStageChange)
end

function VocationChangeFourView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.VOCATION_FOUR_STAGE_CHANGE, self._onStageChange)
end

function VocationChangeFourView:OnDestroy()
end

function VocationChangeFourView:OnStageChange()
    --LoggerHelper.Error("VocationChangeFourView:OnStageChange()")
    self:OnTipsCloseButtonClick()
    self:RefreshDatas()
end

function VocationChangeFourView:SetName()
    self._vocationNum = 3
    self._vocationGroupId = GameWorld.Player():GetVocationGroup()
    local nowName = VocationChangeManager:GetVocationName(self._vocationGroupId, self._vocationNum)
    local upgradeName = VocationChangeManager:GetVocationName(self._vocationGroupId, (self._vocationNum + 1))

    self._nowNameText.text = nowName
    self._upgradeNameText.text = upgradeName
end

function VocationChangeFourView:RefreshDatas()
    self:SetActiveStatus()
    self:SetStatusText()
end

--设置当前转职状态信息
function VocationChangeFourView:SetStatusText()
    local totalNum = GlobalParamsHelper.GetParamValue(664)
    local nowNum = self:GetNowActiveNum()

    self._activeText.text = LanguageDataHelper.CreateContent(73076, {
        ["0"] = nowNum,
        ["1"] = totalNum
    })

    if nowNum == totalNum then
        self._fx_ui_30024:Play(true, true)
        self._fx_ui_30022:Stop()
    else
        self._fx_ui_30022:Play(true, true)
        self._fx_ui_30024:Stop()
    end

    if self:GetNowActiveNum() >= GlobalParamsHelper.GetParamValue(664) then
        self._activeText.color = FontStyleHelper:GetFontColor(102)
    else
        self._activeText.color = FontStyleHelper:GetFontColor(106)
    end

    self._levelText.text = LanguageDataHelper.CreateContent(73077, {
        ["0"] = GlobalParamsHelper.GetParamValue(659),
        ["1"] = GameWorld.Player().level
    })

    if GameWorld.Player().level >= GlobalParamsHelper.GetParamValue(659) then
        self._levelText.color = FontStyleHelper:GetFontColor(102)
    else
        self._levelText.color = FontStyleHelper:GetFontColor(106)
    end

end

--设置命格状态
function VocationChangeFourView:SetActiveStatus()
    local nowActiveNum = self:GetNowActiveNum()--已经激活的数量
    --LoggerHelper.Error("nowActiveNum === " .. tostring(nowActiveNum))
    for i=1,12 do
        if nowActiveNum >= i then
            self._activeIconViews[i]:SetActive(true)
            self._activeSelectViews[i]:SetActive(false)
        else
            self._activeIconViews[i]:SetActive(false)
            self._activeSelectViews[i]:SetActive(false)
        end
        
        if nowActiveNum + 1 == i then
            self._activeButtonFxViews[i]:Play(true, true)
        else
            self._activeButtonFxViews[i]:Stop()
        end
    end
    if nowActiveNum ~= 12 then
        self._activeIconViews[nowActiveNum + 1]:SetActive(false)
        self._activeSelectViews[nowActiveNum + 1]:SetActive(true)
    end
    if self:GetNowActiveNum() == GlobalParamsHelper.GetParamValue(664) and GlobalParamsHelper.GetParamValue(659) == GameWorld.Player().level then
        self._upgradeButton:GetComponent("ButtonWrapper"):SetInteractable(true)
    else
        self._upgradeButton:GetComponent("ButtonWrapper"):SetInteractable(false)
    end

    -- self._upgradeButton:GetComponent("ButtonWrapper"):SetInteractable(nowActiveNum == 12)
    --TODO:需要判断当天等级是否370以及是否有足够的经验进行升级
end

function VocationChangeFourView:OnBagDataUpdate()
    --LoggerHelper.Error("更新道具11111111111")
    self:UpdateItemNeedText(self._clickId)
end

--点击命格按钮事件
function VocationChangeFourView:OnActiveClick(id)
    self._activeTipsMainContainer:SetActive(true)
    local nowActiveNum = self:GetNowActiveNum()
    self._titleText.text = LanguageDataHelper.CreateContent(ChangeVocationNum4DataHelper:GetDescId(id))
    for i=1,12 do
        self._tipsIcons[i]:SetActive(id == i)
    end
    self._attrOneGridLauoutGroup:SetDataList(ChangeVocationNum4DataHelper:GetAtrri(id))
    if nowActiveNum >= id then
        --点击了已经激活的命格
        self:SetActiveTipsInfo(id)
    else
        --点击了未激活的命格
        self._id = id
        self:SetNotActiveTipsInfo(id)
    end
    self._tips = true
end

--设置未激活的命格信息
function VocationChangeFourView:SetNotActiveTipsInfo(id)
    self._clickId = id
    EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE, self._bagDataUpdate)
    self._activeTipsContainer:SetActive(false)
    self._notActiveTipsContainer:SetActive(true)
    
    self:UpdateItemNeedText(id)

    local two = ChangeVocationNum4DataHelper:GetExpNeed(id)
    for k,v in pairs(two) do
        local itemId = v[1]
        local nowNum = GameWorld.Player().money_exp
        local needNum = v[2]
        self._name2Text.text = ItemDataHelper.GetItemName(itemId)
        self._value2Text.text =  StringStyleUtil.GetLongNumberString(nowNum) .. "/" .. StringStyleUtil.GetLongNumberString(needNum)
        if nowNum >= needNum then
            self._name2Text.color = FontStyleHelper:GetFontColor(102)
            self._value2Text.color = FontStyleHelper:GetFontColor(102)
        else
            self._name2Text.color = FontStyleHelper:GetFontColor(106)
            self._value2Text.color = FontStyleHelper:GetFontColor(106)
        end
        break
    end
    if id == 1 or (self:GetNowActiveNum() + 1) == id then
        self._conditionText.gameObject:SetActive(false)
    else
        self._conditionText.gameObject:SetActive(true)
        local name = LanguageDataHelper.CreateContent(ChangeVocationNum4DataHelper:GetDescId(id-1))
        self._conditionText.text = LanguageDataHelper.CreateContent(73072, {["0"] = name})
    end
    self._activeButton:SetActive((self:GetNowActiveNum() + 1) == id)
    self._buyButton:SetActive((self:GetNowActiveNum() + 1) == id)    
end

function VocationChangeFourView:UpdateItemNeedText(id)
    local one = ChangeVocationNum4DataHelper:GetItemNeed(id)
    for k,v in pairs(one) do
        local itemId = v[1]
        self._name1Text.text = ItemDataHelper.GetItemName(itemId)
        local nowNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
        local needNum = v[2]
        self._value1Text.text = nowNum  .. "/" .. needNum
        if nowNum >= needNum then
            self._name1Text.color = FontStyleHelper:GetFontColor(102)
            self._value1Text.color = FontStyleHelper:GetFontColor(102)
        else
            self._name1Text.color = FontStyleHelper:GetFontColor(106)
            self._value1Text.color = FontStyleHelper:GetFontColor(106)
        end
        break
    end
end

--设置已激活的命格信息
function VocationChangeFourView:SetActiveTipsInfo(id)
    self._acviveNumText.text = self:GetNowActiveNum() .. "/" .. GlobalParamsHelper.GetParamValue(664)
    self._attrAllGridLauoutGroup:SetDataList(ChangeVocationNum4DataHelper:GetAllAttriBeforId(self:GetNowActiveNum()))
    self._activeTipsContainer:SetActive(true)
    self._notActiveTipsContainer:SetActive(false)
end

--转职请求按钮事件
function VocationChangeFourView:OnUpgradeButtonClick()
    local upgradeLevel = GlobalParamsHelper.GetParamValue(659)
    local upgradeExp = tonumber(RoleDataHelper.GetCurLevelExp(upgradeLevel))
    --LoggerHelper.Error("upgradeExp ===" .. tostring(upgradeExp))
    --LoggerHelper.Error("GameWorld.Player().money_exp ===" .. tostring(GameWorld.Player().money_exp))

    if GameWorld.Player().money_exp >= upgradeExp then
        VocationChangeManager:VocationFourChange()
    else
        GUIManager.ShowText(2, LanguageDataHelper.CreateContent(73079))
    end
end

--获取现在点亮的命格数
function VocationChangeFourView:GetNowActiveNum()
    return GameWorld.Player().change_four_stage
end

--关闭转职窗口
function VocationChangeFourView:OnCloseButtonClick()
    GUIManager.ClosePanel(PanelsConfig.VocationChange)
end

--命格信息点击空白处处理
function VocationChangeFourView:OnActiveTipsShadeButtonClick()
    if not self._tips then return end
    self._activeTipsMainContainer:SetActive(false)
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE, self._bagDataUpdate)
end

--命格tips信息关闭按钮点击处理
function VocationChangeFourView:OnTipsCloseButtonClick()
    self._activeTipsMainContainer:SetActive(false)
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE, self._bagDataUpdate)
end

--激活命格按钮点击事件
function VocationChangeFourView:OnActiveButtonClick()
    VocationChangeManager:VocationFourActive(self._id)
end

--购买按钮点击事件
function VocationChangeFourView:OnBuyButtonClick()
    local data ={["id"]=EnumType.ShopPageType.Common}
    GUIManager.ShowPanel(PanelsConfig.Shop, data)
end