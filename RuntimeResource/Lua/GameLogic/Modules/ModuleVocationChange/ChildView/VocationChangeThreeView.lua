local VocationChangeThreeView = Class.VocationChangeThreeView(ClassTypes.BaseLuaUIComponent)

VocationChangeThreeView.interface = GameConfig.ComponentsConfig.Component
VocationChangeThreeView.classPath = "Modules.ModuleVocationChange.ChildView.VocationChangeThreeView"

local ChangeVocationViewDataHelper = GameDataHelper.ChangeVocationViewDataHelper
local ChangeVocationDataHelper = GameDataHelper.ChangeVocationDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup

require "Modules.ModuleVocationChange.ChildComponent.VocationChangeValueItem"
local VocationChangeValueItem = ClassTypes.VocationChangeValueItem

function VocationChangeThreeView:Awake()
    self:InitView()
    self:InitCallBack()
end
--override
function VocationChangeThreeView:OnDestroy() 
    self:RemoveEventListeners()
end

function VocationChangeThreeView:AddEventListeners()
end

function VocationChangeThreeView:RemoveEventListeners()  
end

function VocationChangeThreeView:InitCallBack()
end

function VocationChangeThreeView:InitView()
    self._addSkillIcons = {}
    self._addSkillNames = {}
    for i=1,3 do
        table.insert(self._addSkillIcons, self:FindChildGO("Container_Left/Container_Add/Container_Skill_"..i.."/Container_Icon"))
        table.insert(self._addSkillNames, self:GetChildComponent("Container_Left/Container_Add/Container_Skill_"..i.."/Text_Name", 'TextMeshWrapper'))
    end

    self._attriContainer = self:FindChildGO("Container_Left/Container_Upgrade")
    self._attrSrcNames = {}
    self._attrSrcValues = {}
    for i=1,4 do
        table.insert(self._attrSrcNames, self:GetChildComponent("Container_Left/Container_Upgrade/Container_Attr_Now/item_"..i.."/Text_Attr_Name", 'TextMeshWrapper'))
        table.insert(self._attrSrcValues, self:GetChildComponent("Container_Left/Container_Upgrade/Container_Attr_Now/item_"..i.."/Text_Attr_Value", 'TextMeshWrapper'))
    end

    self._attrDstNames = {}
    self._attrDstValues = {}
    for i=1,4 do
        table.insert(self._attrDstNames, self:GetChildComponent("Container_Left/Container_Upgrade/Container_Attr_Next/item_"..i.."/Text_Attr_Name", 'TextMeshWrapper'))
        table.insert(self._attrDstValues, self:GetChildComponent("Container_Left/Container_Upgrade/Container_Attr_Next/item_"..i.."/Text_Attr_Value", 'TextMeshWrapper'))
    end


    --解锁装备
    self._equipmentText = self:GetChildComponent("Container_Right/Container_Equipment/Text_Equipment",'TextMeshWrapper')

    --开放幻化
    self._openText = self:GetChildComponent("Container_Right/Container_Open/Text_Open",'TextMeshWrapper')
    
    self:InitValueGridLayoutGroup()
end

function VocationChangeThreeView:InitValueGridLayoutGroup()
    self._valueGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Right/Container_Atrri/Container_Info")
    self._valueGridLauoutGroup:SetItemType(VocationChangeValueItem)
end

function VocationChangeThreeView:OnEnable()
    self:AddEventListeners()
end

function VocationChangeThreeView:OnDisable()
    self:RemoveEventListeners()
end

function VocationChangeThreeView:SetValues(vocationNum)    
    self._vocationGroupId = GameWorld.Player():GetVocationGroup()
    local id = self._vocationGroupId * 100 + vocationNum + 1
    -- LoggerHelper.Error("id ======" .. tostring(id))
    
    local newSkillData = ChangeVocationViewDataHelper:GetNewSkill(id)
    self:RefeshAddSkillData(newSkillData)
    
    local equipmentData = ChangeVocationViewDataHelper:GetEquipOpen(id)
    local equipmentText = ""
    for i,v in ipairs(equipmentData) do
        equipmentText = equipmentText .. LanguageDataHelper.CreateContent(v) .. "  "
    end
    self._equipmentText.text = equipmentText

    local openData = ChangeVocationViewDataHelper:GetModelOpen(id)
    local openText = ""
    for _, openValue in ipairs(openData) do
        openText = openText .. LanguageDataHelper.CreateContent(openValue) .. "  "
    end
    self._openText.text = openText

    local atrriData = ChangeVocationViewDataHelper:GetAtrri(id)
    local valueDatas = {}
    for k,v in pairs(atrriData) do
        local valueData = {}
        valueData.name = AttriDataHelper:GetName(k)
        valueData.text = "+" .. v
        table.insert(valueDatas, valueData)
    end
    self._valueGridLauoutGroup:SetDataList(valueDatas)
end

function VocationChangeThreeView:RefeshAddSkillData(newSkillData)
    for i=1,3 do
        local icon = newSkillData[i][1]
        local downText = newSkillData[i][2]
        GameWorld.AddIcon(self._addSkillIcons[i], icon)
        self._addSkillNames[i].text = LanguageDataHelper.CreateContent(downText)
    end
end

function VocationChangeThreeView:SetAddAttri(stage)
    local nowAttriData = ChangeVocationDataHelper:GetAttriDatasByChangeNumAndStage(2, stage)
    -- LoggerHelper.Error("nowAttriData" .. PrintTable:TableToStr(nowAttriData))
    if not table.isEmpty(nowAttriData) then
        for i=1,4 do
            self._attrSrcNames[i].text = AttriDataHelper:GetName(nowAttriData[i][1])
            self._attrSrcValues[i].text =  "+" .. nowAttriData[i][2]  
        end
    end

    local nextAttriData = ChangeVocationDataHelper:GetAttriDatasByChangeNumAndStage(2, stage+1)
    -- LoggerHelper.Error("nextAttriData" .. PrintTable:TableToStr(nextAttriData))
    if not table.isEmpty(nextAttriData) then
        for i=1,4 do
            self._attrDstNames[i].text = AttriDataHelper:GetName(nextAttriData[i][1])
            self._attrDstValues[i].text = "+" .. nextAttriData[i][2]     
        end
    end
end