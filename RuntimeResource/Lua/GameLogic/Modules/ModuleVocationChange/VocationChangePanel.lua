-- VocationChangePanel.lua
require "Modules.ModuleVocationChange.ChildView.VocationChangeView"
require "Modules.ModuleVocationChange.ChildView.VocationChangeFourView"

local VocationChangePanel = Class.VocationChangePanel(ClassTypes.BasePanel)

local VocationChangeView = ClassTypes.VocationChangeView
local VocationChangeFourView = ClassTypes.VocationChangeFourView
local GUIManager = GameManager.GUIManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIParticle = ClassTypes.UIParticle


function VocationChangePanel:Awake()
	self:InitView()
end

function VocationChangePanel:OnDestroy()
	self._csBH = nil
end

function VocationChangePanel:OnShow()
    local vocationChangeNum = GameWorld.Player():GetVocationChange()
    -- LoggerHelper.Error("vocationChangeNum ====" .. tostring(vocationChangeNum))
    for i=1,4 do
        self._toggleViews[i]:SetActive((vocationChangeNum + 1) >= i) 
    end

    self._toggleGroup:SetSelectIndex(vocationChangeNum)
    self:OnTabBarClick(vocationChangeNum)
end

function VocationChangePanel:OnClose()
end

function VocationChangePanel:AddEventListeners()
end

function VocationChangePanel:RemoveEventListeners()
end

function VocationChangePanel:ScaleToHide()
    return true
end

function VocationChangePanel:InitView()
    self._toggleViews = {}
    for i=1,4 do        
        local containerFx = self:FindChildGO("ToggleGroup_Function/Toggle_" .. i .. "/Checkmark/Container_Fx")
        UIParticle.AddParticle(containerFx, "fx_ui_30018")
        table.insert(self._toggleViews, self:FindChildGO("ToggleGroup_Function/Toggle_" .. i))
    end

    self._vocationChangeView = self:AddChildLuaUIComponent("Container_VocationChange", VocationChangeView)
    self._vocationChangeFourView = self:AddChildLuaUIComponent("Container_VocationChange_Four", VocationChangeFourView)

    
    self._closeButton = self:FindChildGO("Container_Win/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnCloseButtonClick() end)

    self._toggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup_Function")
    self._toggleGroup:SetOnSelectedIndexChangedCB(function(index)self:OnTabBarClick(index) end)
end

function VocationChangePanel:OnTabBarClick(idx)
    self._selectIndex = idx
    -- LoggerHelper.Error("idx  =====" .. tostring(idx))
    local nowVocationChangeNum = GameWorld.Player():GetVocationChange()
    local vocationChangeNum = idx + 1
    if vocationChangeNum <= 3 then
        self._vocationChangeView:SetActive(true)
        self._vocationChangeFourView:SetActive(false)
        if nowVocationChangeNum == idx then
            self._vocationChangeView:RefreshView()
        else    
            self._vocationChangeView:RefreshView(vocationChangeNum - 1)
        end
    elseif vocationChangeNum == 4 then
        self._vocationChangeView:SetActive(false)
        self._vocationChangeFourView:SetActive(true)
    end

end

--关闭转职窗口
function VocationChangePanel:OnCloseButtonClick()
    GUIManager.ClosePanel(PanelsConfig.VocationChange)
end

function VocationChangePanel:BaseShowModel()
    if self._selectIndex < 3 then
        self._vocationChangeView:ShowModel()
    end
end

function VocationChangePanel:BaseHideModel()
    if self._selectIndex < 3 then
        self._vocationChangeView:HideModel()
    end
end