-- VocationChangeStepItem.lua
local VocationChangeStepItem = Class.VocationChangeStepItem(ClassTypes.UIListItem)

VocationChangeStepItem.interface = GameConfig.ComponentsConfig.Component
VocationChangeStepItem.classPath = "Modules.ModuleVocationChange.ChildComponent.VocationChangeStepItem"

local ChangeVocationDataHelper = GameDataHelper.ChangeVocationDataHelper
local VocationChangeConfig = GameConfig.VocationChangeConfig

function VocationChangeStepItem:Awake()
    self._numText = self:GetChildComponent("Text_Num",'TextMeshWrapper')    
    self._selectImage = self:FindChildGO("Image_Select")
    self._completeImage = self:FindChildGO("Image_Complete")
end

function VocationChangeStepItem:OnDestroy() 

end

--override
function VocationChangeStepItem:OnRefreshData(data)
    self._numText.text = ChangeVocationDataHelper:GetStage(data.id)
    self._selectImage:SetActive(data.select)
    self._completeImage:SetActive(data.type == VocationChangeConfig.VOCATION_STATE_COMPLETE)
end