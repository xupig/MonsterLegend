-- VocationChangeAttrItem.lua
local VocationChangeAttrItem = Class.VocationChangeAttrItem(ClassTypes.UIListItem)

VocationChangeAttrItem.interface = GameConfig.ComponentsConfig.Component
VocationChangeAttrItem.classPath = "Modules.ModuleVocationChange.ChildComponent.VocationChangeAttrItem"

local AttriDataHelper = GameDataHelper.AttriDataHelper

--override
function VocationChangeAttrItem:Awake()
    self._nameText = self:GetChildComponent("Text_Attr_Name",'TextMeshWrapper')    
    self._valueText = self:GetChildComponent("Text_Attr_Value",'TextMeshWrapper')
end

--override
function VocationChangeAttrItem:OnDestroy() 
    self._nameText = nil
    self._valueText = nil
end

function VocationChangeAttrItem:OnRefreshData(data)
    self._nameText.text = AttriDataHelper:GetName(data[1])
    self._valueText.text = "+" .. data[2]
end