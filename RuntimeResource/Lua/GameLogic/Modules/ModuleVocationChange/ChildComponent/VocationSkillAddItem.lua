-- VocationSkillAddItem.lua
local VocationSkillAddItem = Class.VocationSkillAddItem(ClassTypes.UIListItem)

VocationSkillAddItem.interface = GameConfig.ComponentsConfig.Component
VocationSkillAddItem.classPath = "Modules.ModuleVocationChange.ChildComponent.VocationSkillAddItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function VocationSkillAddItem:Awake()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._downText = self:GetChildComponent("Text_Down",'TextMeshWrapper')
end

function VocationSkillAddItem:OnDestroy() 
    self._iconContainer = nil
    self._downText = nil
end

--override
function VocationSkillAddItem:OnRefreshData(data)
    local icon = data[1]
    local downText = data[2]
    GameWorld.AddIcon(self._iconContainer, icon)
    self._downText.text = LanguageDataHelper.CreateContent(downText)
end