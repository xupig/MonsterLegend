-- VocationChangeValueItem.lua
local VocationChangeValueItem = Class.VocationChangeValueItem(ClassTypes.UIListItem)

VocationChangeValueItem.interface = GameConfig.ComponentsConfig.Component
VocationChangeValueItem.classPath = "Modules.ModuleVocationChange.ChildComponent.VocationChangeValueItem"


function VocationChangeValueItem:Awake()
    self._nameText = self:GetChildComponent("Text_Key",'TextMeshWrapper')    
    self._valueText = self:GetChildComponent("Text_Value",'TextMeshWrapper')
end

function VocationChangeValueItem:OnDestroy() 

end

--override
function VocationChangeValueItem:OnRefreshData(data)
    self._nameText.text = data.name
    self._valueText.text = data.text
end