-- VocationChangeRewardItem.lua
local VocationChangeRewardItem = Class.VocationChangeRewardItem(ClassTypes.UIListItem)

VocationChangeRewardItem.interface = GameConfig.ComponentsConfig.Component
VocationChangeRewardItem.classPath = "Modules.ModuleVocationChange.ChildComponent.VocationChangeRewardItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function VocationChangeRewardItem:Awake()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._upText = self:GetChildComponent("Text_Up",'TextMeshWrapper')
    self._downText = self:GetChildComponent("Text_Down",'TextMeshWrapper')
end

function VocationChangeRewardItem:OnDestroy() 
end

--override
function VocationChangeRewardItem:OnRefreshData(data)
end