-- VocationSkillUpgradeItem.lua
local VocationSkillUpgradeItem = Class.VocationSkillUpgradeItem(ClassTypes.UIListItem)

VocationSkillUpgradeItem.interface = GameConfig.ComponentsConfig.Component
VocationSkillUpgradeItem.classPath = "Modules.ModuleVocationChange.ChildComponent.VocationSkillUpgradeItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function VocationSkillUpgradeItem:Awake()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._upText = self:GetChildComponent("Text_Up",'TextMeshWrapper')
    self._downText = self:GetChildComponent("Text_Down",'TextMeshWrapper')
end

function VocationSkillUpgradeItem:OnDestroy() 
    self._iconContainer = nil
    self._upText = nil
    self._downText = nil
end

--override
function VocationSkillUpgradeItem:OnRefreshData(data)
    local icon = data[1]
    local upText = data[2]
    local downText = data[3]
    GameWorld.AddIcon(self._iconContainer, icon)
    self._upText.text = LanguageDataHelper.CreateContent(upText)
    self._downText.text = LanguageDataHelper.CreateContent(downText)
end