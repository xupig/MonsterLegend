--AutoGameSettingView.lua
local AutoGameSettingView = Class.AutoGameSettingView(ClassTypes.BaseLuaUIComponent)

AutoGameSettingView.interface = GameConfig.ComponentsConfig.Component
AutoGameSettingView.classPath = "Modules.ModuleSystemSetting.ChildView.AutoGameSettingView"

local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil
local SystemSettingConfig = GameConfig.SystemSettingConfig
local UIToggle = ClassTypes.UIToggle
local SystemSettingManager = PlayerManager.SystemSettingManager
local public_config = GameWorld.public_config

require "Modules.ModuleSystemSetting.ChildComponent.SystemSettingImageItem"
local SystemSettingImageItem = ClassTypes.SystemSettingImageItem

function AutoGameSettingView:Awake()
    self:InitView()
    self:InitCallBack()
end

--override
function AutoGameSettingView:OnDestroy()
end

function AutoGameSettingView:InitCallBack()
end

function AutoGameSettingView:AddEventListeners()
end

function AutoGameSettingView:RemoveEventListeners()
end

function AutoGameSettingView:InitView()
    local root = "ScrollView/Mask/Content"

    self._scroll = UIComponentUtil.FindChild(self.gameObject, "ScrollView")
	self._scrollViewAttri = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
    self._scrollContent = self:FindChildGO(root)
    self._rectTransform = self._scrollContent:GetComponent(typeof(UnityEngine.RectTransform))
    self._initPosition = self._rectTransform.localPosition
    self._rectTransform.sizeDelta = Vector2(1006, 600)

    --自动拾取
    --白色装备
    self._whiteToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Auto_Get/Container_Auto_Get/Toggle_White")
    self._whiteToggle:SetOnValueChangedCB(function(state) self:OnWhiteToggleValueChanged(state) end)
    --蓝色装备
    self._blueToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Auto_Get/Container_Auto_Get/Toggle_Blue")
    self._blueToggle:SetOnValueChangedCB(function(state) self:OnBlueToggleValueChanged(state) end)
    --紫色装备
    self._purpleToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Auto_Get/Container_Auto_Get/Toggle_Purple")
    self._purpleToggle:SetOnValueChangedCB(function(state) self:OnPurpleToggleValueChanged(state) end)
    --橙色及以上装备
    self._orangeToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Auto_Get/Container_Auto_Get/Toggle_Up_Orange")
    self._orangeToggle:SetOnValueChangedCB(function(state) self:OnOrangeToggleValueChanged(state) end)
    --金币
    self._moneyToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Auto_Get/Container_Auto_Get/Toggle_Money")
    self._moneyToggle:SetOnValueChangedCB(function(state) self:OnMoneyToggleValueChanged(state) end)
    --其他
    self._otherToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Auto_Get/Container_Auto_Get/Toggle_Other")
    self._otherToggle:SetOnValueChangedCB(function(state) self:OnOtherToggleValueChanged(state) end)

    --自动出售
    self._sellToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Auto_Sell/Container_Auto_Sell/Toggle_Auto_Sell")
    self._sellToggle:SetOnValueChangedCB(function(state) self:OnSellToggleValueChanged(state) end)

    --自动吞噬
    self._devourToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Auto_Devour/Container_Auto_Devour/Toggle_Auto_Devour")
    self._devourToggle:SetOnValueChangedCB(function(state) self:OnDevourToggleValueChanged(state) end)

    --自动复活
    self._resurrectToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Auto_Resurrect/Container_Auto_Resurrect/Toggle_Auto_Resurrect")
    self._resurrectToggle:SetOnValueChangedCB(function(state) self:OnResurrectToggleValueChanged(state) end)
end

function AutoGameSettingView:ShowView()
    self:AddEventListeners()
    self:UpdateData()
end

function AutoGameSettingView:CloseView()
    self:RemoveEventListeners()
    self._rectTransform.localPosition = self._initPosition
end

function AutoGameSettingView:UpdateData()
    local hang_up_setting = GameWorld.Player().hang_up_setting
    local HANG_UP_KEY_AUTO_PICK = hang_up_setting[public_config.HANG_UP_KEY_AUTO_PICK]

    self._whiteToggle:SetIsOn(HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_WHITE] == 1)
    self._blueToggle:SetIsOn(HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_BLUE] == 1)
    self._purpleToggle:SetIsOn(HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_PURPLE] == 1)
    self._orangeToggle:SetIsOn(HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_ORANGE] == 1)
    self._moneyToggle:SetIsOn(HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_GOLD] == 1)
    self._otherToggle:SetIsOn(HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_OTHER] == 1)
    self._sellToggle:SetIsOn(hang_up_setting[public_config.HANG_UP_KEY_AUTO_SELL] == 1)
    self._devourToggle:SetIsOn(hang_up_setting[public_config.HANG_UP_KEY_AUTO_EAT] == 1)
    self._resurrectToggle:SetIsOn(hang_up_setting[public_config.HANG_UP_KEY_AUTO_BUY_REVIVAL] == 1)
end

function AutoGameSettingView:OnWhiteToggleValueChanged(state)
    local hang_up_setting = GameWorld.Player().hang_up_setting
    local HANG_UP_KEY_AUTO_PICK = hang_up_setting[public_config.HANG_UP_KEY_AUTO_PICK]

    self._whiteToggle:SetIsOn(state)
    if state ~= (HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_WHITE] == 1) then
        SystemSettingManager:SetAutoGetSetting(public_config.AUTO_PICK_WHITE)
    end
end

function AutoGameSettingView:OnBlueToggleValueChanged(state)
    local hang_up_setting = GameWorld.Player().hang_up_setting
    local HANG_UP_KEY_AUTO_PICK = hang_up_setting[public_config.HANG_UP_KEY_AUTO_PICK]

    self._blueToggle:SetIsOn(state)
    if state ~= (HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_BLUE] == 1) then
        SystemSettingManager:SetAutoGetSetting(public_config.AUTO_PICK_BLUE)
    end
end

function AutoGameSettingView:OnPurpleToggleValueChanged(state)
    local hang_up_setting = GameWorld.Player().hang_up_setting
    local HANG_UP_KEY_AUTO_PICK = hang_up_setting[public_config.HANG_UP_KEY_AUTO_PICK]

    self._purpleToggle:SetIsOn(state)
    if state ~= (HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_PURPLE] == 1) then
        SystemSettingManager:SetAutoGetSetting(public_config.AUTO_PICK_PURPLE)
    end
end

function AutoGameSettingView:OnOrangeToggleValueChanged(state)
    local hang_up_setting = GameWorld.Player().hang_up_setting
    local HANG_UP_KEY_AUTO_PICK = hang_up_setting[public_config.HANG_UP_KEY_AUTO_PICK]

    self._orangeToggle:SetIsOn(state)
    if state ~= (HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_ORANGE] == 1) then
        SystemSettingManager:SetAutoGetSetting(public_config.AUTO_PICK_ORANGE)
    end
end

function AutoGameSettingView:OnMoneyToggleValueChanged(state)
    local hang_up_setting = GameWorld.Player().hang_up_setting
    local HANG_UP_KEY_AUTO_PICK = hang_up_setting[public_config.HANG_UP_KEY_AUTO_PICK]

    self._moneyToggle:SetIsOn(state)
    if state ~= (HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_GOLD] == 1) then
        SystemSettingManager:SetAutoGetSetting(public_config.AUTO_PICK_GOLD)
    end
end

function AutoGameSettingView:OnOtherToggleValueChanged(state)
    local hang_up_setting = GameWorld.Player().hang_up_setting
    local HANG_UP_KEY_AUTO_PICK = hang_up_setting[public_config.HANG_UP_KEY_AUTO_PICK]

    self._otherToggle:SetIsOn(state)
    if state ~= (HANG_UP_KEY_AUTO_PICK[public_config.AUTO_PICK_OTHER] == 1) then
        SystemSettingManager:SetAutoGetSetting(public_config.AUTO_PICK_OTHER)
    end
end

function AutoGameSettingView:OnSellToggleValueChanged(state)
    local hang_up_setting = GameWorld.Player().hang_up_setting

    self._sellToggle:SetIsOn(state)
    if state ~= (hang_up_setting[public_config.HANG_UP_KEY_AUTO_SELL] == 1) then
        SystemSettingManager:SetAutoSetting(public_config.HANG_UP_KEY_AUTO_SELL )
    end
end

function AutoGameSettingView:OnDevourToggleValueChanged(state)
    local hang_up_setting = GameWorld.Player().hang_up_setting
    
    self._devourToggle:SetIsOn(state)
    if state ~= (hang_up_setting[public_config.HANG_UP_KEY_AUTO_EAT] == 1) then
        SystemSettingManager:SetAutoSetting(public_config.HANG_UP_KEY_AUTO_EAT)
    end
end

function AutoGameSettingView:OnResurrectToggleValueChanged(state)
    local hang_up_setting = GameWorld.Player().hang_up_setting
    
    self._resurrectToggle:SetIsOn(state)
    if state ~= (hang_up_setting[public_config.HANG_UP_KEY_AUTO_BUY_REVIVAL] == 1) then
        SystemSettingManager:SetAutoSetting(public_config.HANG_UP_KEY_AUTO_BUY_REVIVAL )
    end
end
