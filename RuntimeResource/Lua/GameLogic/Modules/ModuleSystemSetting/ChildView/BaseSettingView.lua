--BaseSettingView.lua
local BaseSettingView = Class.BaseSettingView(ClassTypes.BaseLuaUIComponent)

BaseSettingView.interface = GameConfig.ComponentsConfig.Component
BaseSettingView.classPath = "Modules.ModuleSystemSetting.ChildView.BaseSettingView"

local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil
local SystemSettingConfig = GameConfig.SystemSettingConfig
local UIToggle = ClassTypes.UIToggle
local SystemSettingManager = PlayerManager.SystemSettingManager

require "UIComponent.Base.UISlider"
local UISlider = ClassTypes.UISlider    

require "Modules.ModuleSystemSetting.ChildComponent.SystemSettingImageItem"
local SystemSettingImageItem = ClassTypes.SystemSettingImageItem

function BaseSettingView:Awake()
    self:InitView()
    self:InitCallBack()
end

--override
function BaseSettingView:OnDestroy()
end

function BaseSettingView:InitCallBack()
    self._imageChange = function(num) self:OnImageChange(num) end
end

function BaseSettingView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.SYSTEMSETTING_IMAGE_CHANGE, self._imageChange)
end

function BaseSettingView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.SYSTEMSETTING_IMAGE_CHANGE, self._imageChange)
end

function BaseSettingView:InitView()
    local root = "ScrollView/Mask/Content"

    self._scroll = UIComponentUtil.FindChild(self.gameObject, "ScrollView")
	self._scrollViewAttri = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
    self._scrollContent = self:FindChildGO(root)
    self._rectTransform = self._scrollContent:GetComponent(typeof(UnityEngine.RectTransform))
    self._initPosition = self._rectTransform.localPosition
    self._rectTransform.sizeDelta = Vector2(1006, 900)

    --同屏人数
    self._playerNumSlider = UISlider.AddSlider(self.gameObject, root .. "/Container_Image_Quality/Container_Player_Num/Slider")
    self._playerNumSlider:SetOnValueChangedCB(function() self:PlayerNumChange() end)
    self._playerNumSlider:SetMinValue(SystemSettingConfig.PLAYER_MIN_NUM)
    self._playerNumSlider:SetMaxValue(SystemSettingConfig.PLAYER_MAX_NUM)
    self._playerNum  = self:GetChildComponent(root .. "/Container_Image_Quality/Container_Player_Num/Text_Num",'TextMeshWrapper')

    --实时阴影 
    self._realTimeShadows = UIToggle.AddToggle(self.gameObject, string.format("%s/Container_Image_Quality/Toggle_Hide_RealTime_Shadows", root))    
    self._realTimeShadows:SetOnValueChangedCB(function(state) self:OnRealTimeShadowsChanged(state) end)

    --画质
    self._imageViews = {}
    for i=0,2 do
        local go = self:FindChildGO(root .. "/Container_Image_Quality/Container_Content/Container_" .. i)
        table.insert(self._imageViews, UIComponentUtil.AddLuaUIComponent(go, SystemSettingImageItem))
        self._imageViews[i+1]:SetNum(i)
    end

    --音乐
    self._musicSlider = UISlider.AddSlider(self.gameObject, root .. "/Container_Sound_Setting/Container_Music/Slider")
    self._musicSlider:SetOnValueChangedCB(function() self:MusicChange() end)
    self._musicSlider:SetMinValue(SystemSettingConfig.MUSIC_MIN_NUM)
    self._musicSlider:SetMaxValue(SystemSettingConfig.MUSIC_MAX_NUM)
    self._musicNum  = self:GetChildComponent(root .. "/Container_Sound_Setting/Container_Music/Text_Num",'TextMeshWrapper')
    
    --音效
    self._soundSlider = UISlider.AddSlider(self.gameObject, root .. "/Container_Sound_Setting/Container_Sound_Effect/Slider")
    self._soundSlider:SetOnValueChangedCB(function() self:SoundChange() end)
    self._soundSlider:SetMinValue(SystemSettingConfig.SOUND_MIN_NUM)
    self._soundSlider:SetMaxValue(SystemSettingConfig.SOUND_MAX_NUM)
    self._soundNum  = self:GetChildComponent(root .. "/Container_Sound_Setting/Container_Sound_Effect/Text_Num",'TextMeshWrapper')
    
    --功能设置    
    --宠物
    self._petToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Func_Setting/Container_Func/Toggle_Hide_Other_Pet")
    self._petToggle:SetOnValueChangedCB(function(state) self:OnPetToggleValueChanged(state) end)
    --翅膀
    self._wingToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Func_Setting/Container_Func/Toggle_Hide_Other_Wing")
    self._wingToggle:SetOnValueChangedCB(function(state) self:OnWingToggleValueChanged(state) end)
    --魂器
    self._hunqiToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Func_Setting/Container_Func/Toggle_Hide_Other_Hunqi")
    self._hunqiToggle:SetOnValueChangedCB(function(state) self:OnHunqiToggleValueChanged(state) end)
    --称号
    self._titleToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Func_Setting/Container_Func/Toggle_Hide_Other_Title")
    self._titleToggle:SetOnValueChangedCB(function(state) self:OnTitleToggleValueChanged(state) end)
    --技能特效
    self._skillToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Func_Setting/Container_Func/Toggle_Hide_Other_SkillFx")
    self._skillToggle:SetOnValueChangedCB(function(state) self:OnSkillFxToggleValueChanged(state) end)
    --装备流光
    self._equipToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Func_Setting/Container_Func/Toggle_Hide_Other_Equip_Flow")
    self._equipToggle:SetOnValueChangedCB(function(state) self:OnEquipFlowToggleValueChanged(state) end)
    --怪物
    self._monsterToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Func_Setting/Container_Func/Toggle_Hide_Other_Monster")
    self._monsterToggle:SetOnValueChangedCB(function(state) self:OnMonsterToggleValueChanged(state) end)
    --送花特效
    self._flowerToggle = UIToggle.AddToggle(self.gameObject, root .. "/Container_Func_Setting/Container_Func/Toggle_Hide_Other_FlowerFx")
    self._flowerToggle:SetOnValueChangedCB(function(state) self:OnFlowerFxToggleValueChanged(state) end)
    -- 省电模式
    self._savePowerToggle = UIToggle.AddToggle(self.gameObject, string.format("%s/Container_PowerMode_Setting/Toggle_Save_Power_Mode", root))
    self._savePowerToggle:SetOnValueChangedCB(function(state) self:OnSavePowerToggleValueChanged(state) end)

    -- 组队自动同意
    self._autoTeamToggle = UIToggle.AddToggle(self.gameObject, string.format("%s/Container_Team_Setting/Toggle_Auto_Team_Confirm", root))
    self._autoTeamToggle:SetOnValueChangedCB(function(state) self:OnAutoTeamConfirmChanged(state) end)

    self._returnLoginButton = self:FindChildGO("Button_Goto_Login")
    self._csBH:AddClick(self._returnLoginButton, function() self:OnReturnLoginButton() end)
    if not GameWorld.isEditor then
        self._returnLoginButton:SetActive(false)
    end
end

function BaseSettingView:ShowView()
    self:AddEventListeners()
    self:UpdateData()
end

function BaseSettingView:CloseView()
    self:RemoveEventListeners()
    self._rectTransform.localPosition = self._initPosition
end

-- function BaseSettingView:OnEnable()
--     -- self:AddEventListeners()
--     -- self:UpdateData()
-- end

-- function BaseSettingView:OnDisable()
--     -- self:RemoveEventListeners()
--     -- self._rectTransform.localPosition = self._initPosition
-- end

function BaseSettingView:PlayerNumChange()
    local num = self._playerNumSlider:GetValue()
    self._playerNum.text =  num .. "人"
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.PLAYER_NUM, num)
end

function BaseSettingView:MusicChange()
    local num = self._musicSlider:GetValue()
    self._musicNum.text = num .. "%"
    -- SystemSettingManager:SetPlayerSetting(SystemSettingConfig.MUSIC_VOLUME, num/100)
    EventDispatcher:TriggerEvent(GameEvents.OnMusicChanged, num/100)
end

function BaseSettingView:SoundChange()
    local num = self._soundSlider:GetValue()
    self._soundNum.text = num .. "%"
    -- SystemSettingManager:SetPlayerSetting(SystemSettingConfig.SOUND_VOLUME, num/100)
    EventDispatcher:TriggerEvent(GameEvents.OnSoundChanged, num/100)
end

function BaseSettingView:UpdateData()
    self._playerNumSlider:SetValue(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.PLAYER_NUM))
    self._musicSlider:SetValue(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.MUSIC_VOLUME) * 100)
    self._soundSlider:SetValue(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.SOUND_VOLUME) * 100)

    self._petToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_PET))
    self._wingToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_WING))
    self._hunqiToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_HUNQI))
    self._titleToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_TITLE))
    self._skillToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_SKILLFX))
    self._equipToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_EQUIPFLOW))
    self._monsterToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_MONSTER))
    self._flowerToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_FLOWERFX))
    self._realTimeShadows:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.REALTIME_SHADOWS))
    self._autoTeamToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.AUTO_TEAMCONFIRM))
    self._savePowerToggle:SetIsOn(SystemSettingManager:GetPlayerSetting(SystemSettingConfig.SAVE_POWER_MODE))

    self._imageSelect = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.RENDER_QUALITY)
    for i=1,3 do
        self._imageViews[i]:SetSelect((i-1) == self._imageSelect)
    end
    
end

function BaseSettingView:OnImageChange(num)
    self._imageViews[self._imageSelect+1]:SetSelect(false)
    self._imageSelect = num
    self._imageViews[self._imageSelect+1]:SetSelect(true)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.RENDER_QUALITY, num)
    EventDispatcher:TriggerEvent(GameEvents.OnQualityChanged)
end

function BaseSettingView:OnPetToggleValueChanged(state)
    self._petToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.OTHER_PET,state)
end

function BaseSettingView:OnWingToggleValueChanged(state)
    self._wingToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.OTHER_WING,state)
end

function BaseSettingView:OnHunqiToggleValueChanged(state)
    self._hunqiToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.OTHER_HUNQI,state)
end

function BaseSettingView:OnTitleToggleValueChanged(state)
    self._titleToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.OTHER_TITLE,state)
end

function BaseSettingView:OnSkillFxToggleValueChanged(state)
    self._skillToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.OTHER_SKILLFX,state)
end

function BaseSettingView:OnEquipFlowToggleValueChanged(state)
    self._equipToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.OTHER_EQUIPFLOW,state)
end

function BaseSettingView:OnMonsterToggleValueChanged(state)
    self._monsterToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.OTHER_MONSTER,state)
end

function BaseSettingView:OnFlowerFxToggleValueChanged(state)
    self._flowerToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.OTHER_FLOWERFX,state)
end

function BaseSettingView:OnReturnLoginButton()
    GameManager.GameStateManager.ReturnLogin(true)
end

function BaseSettingView:OnRealTimeShadowsChanged(state)
    self._realTimeShadows:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.REALTIME_SHADOWS,state)
end

function BaseSettingView:OnAutoTeamConfirmChanged(state)
    self._autoTeamToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.AUTO_TEAMCONFIRM,state)
end

function BaseSettingView:OnSavePowerToggleValueChanged(state)
    self._savePowerToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.SAVE_POWER_MODE,state)
end