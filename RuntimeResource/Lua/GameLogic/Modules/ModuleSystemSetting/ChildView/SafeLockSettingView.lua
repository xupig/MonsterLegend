--SafeLockSettingView.lua
local SafeLockSettingView = Class.SafeLockSettingView(ClassTypes.BaseLuaUIComponent)

SafeLockSettingView.interface = GameConfig.ComponentsConfig.Component
SafeLockSettingView.classPath = "Modules.ModuleSystemSetting.ChildView.SafeLockSettingView"

local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local SystemSettingManager = PlayerManager.SystemSettingManager
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local XImageFlowLight = GameMain.XImageFlowLight
local UIList = ClassTypes.UIList

require "Modules.ModuleSystemSetting.ChildComponent.QuestionListItem"
local QuestionListItem = ClassTypes.QuestionListItem

function SafeLockSettingView:Awake()
    self:InitView()
    self:InitCallBack()
end

--override
function SafeLockSettingView:OnDestroy()
end

function SafeLockSettingView:InitCallBack()
    self._setPasswordCallBack = function() self:SetPasswordSuccess() end
    self._unLockCallBack = function() self:UnLockSuccess() end
    self._lockCallBack = function() self:LockSuccess() end
    self._modifyCallBack = function() self:ModifySuccess() end
    self._clearCallBack = function() self:ClearSuccess() end
end

function SafeLockSettingView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.SYSTEM_SAFE_SETTING_SET_PASSWORD, self._setPasswordCallBack)
    EventDispatcher:AddEventListener(GameEvents.SYSTEM_SAFE_SETTING_UNLOCK, self._unLockCallBack)
    EventDispatcher:AddEventListener(GameEvents.SYSTEM_SAFE_SETTING_LOCK, self._lockCallBack)
    EventDispatcher:AddEventListener(GameEvents.SYSTEM_SAFE_SETTING_MODIFY, self._modifyCallBack)
    EventDispatcher:AddEventListener(GameEvents.SYSTEM_SAFE_SETTING_CLEAR_PASSWORD, self._clearCallBack)
end

function SafeLockSettingView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.SYSTEM_SAFE_SETTING_SET_PASSWORD, self._setPasswordCallBack)
    EventDispatcher:RemoveEventListener(GameEvents.SYSTEM_SAFE_SETTING_UNLOCK, self._unLockCallBack)
    EventDispatcher:RemoveEventListener(GameEvents.SYSTEM_SAFE_SETTING_LOCK, self._lockCallBack)
    EventDispatcher:RemoveEventListener(GameEvents.SYSTEM_SAFE_SETTING_MODIFY, self._modifyCallBack)
    EventDispatcher:RemoveEventListener(GameEvents.SYSTEM_SAFE_SETTING_CLEAR_PASSWORD, self._clearCallBack)
end

function SafeLockSettingView:InitView()
    self._imageBGActiveFlowLight = self:AddChildComponent("Button_Modify_Password", XImageFlowLight)


    self._setPasswordContainer = self:FindChildGO("Container_Set_PassWord")
    self._modifyPasswordContainer = self:FindChildGO("Container_Modify_PassWord")
    self._clearPasswordContainer = self:FindChildGO("Container_Clear_PassWord")
    self._unLockContainer = self:FindChildGO("Container_UnLock")
    self._questionListContainer = self:FindChildGO("Container_Set_PassWord/Container_PopupSmall/Container_Question_List")

    --设定密码
    self._inputFieldMeshPassword = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_Set_PassWord/Container_PopupSmall/Container_Content/InputFieldPassword")
    self._inputPasswordText = self:GetChildComponent("Container_Set_PassWord/Container_PopupSmall/Container_Content/InputFieldPassword", 'InputFieldMeshWrapper')
    -- self._inputFieldMeshPassword:SetOnEndMeshEditCB(function(text) self:OnPasswordEndEdit(text) end)
    self._inputFieldMeshSafeAnswer = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_Set_PassWord/Container_PopupSmall/Container_Content/InputFieldSafeAnswer")
    self._inputSafeAnswerText = self:GetChildComponent("Container_Set_PassWord/Container_PopupSmall/Container_Content/InputFieldSafeAnswer", 'InputFieldMeshWrapper')
    self._questionText = self:GetChildComponent("Container_Set_PassWord/Container_PopupSmall/Container_Content/Text_Question",'TextMeshWrapper')
    -- self._inputFieldMeshSafeAnswer:SetOnEndMeshEditCB(function(text) self:OnSafeAnswerEndEdit(text) end)

    --解除绑定
    self._inputFieldMeshUnLockPassword = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_UnLock/Container_PopupSmall/Container_Content/InputFieldPassword")
    self._inputUnLockPasswordText = self:GetChildComponent("Container_UnLock/Container_PopupSmall/Container_Content/InputFieldPassword", 'InputFieldMeshWrapper')
    
    --修改密码
    self._modifyQuestionText = self:GetChildComponent("Container_Modify_PassWord/Container_PopupSmall/Container_Content/Text_Question",'TextMeshWrapper') 
    self._modifyAnswerText = self:GetChildComponent("Container_Modify_PassWord/Container_PopupSmall/Container_Content/InputFieldSafeAnswer",'InputFieldMeshWrapper') 
    self._modifyPasswordText = self:GetChildComponent("Container_Modify_PassWord/Container_PopupSmall/Container_Content/InputFieldPassword",'InputFieldMeshWrapper') 

    --清空密码
    self._clearQuestionText = self:GetChildComponent("Container_Clear_PassWord/Container_PopupSmall/Container_Content/Text_Question",'TextMeshWrapper') 
    self._clearAnswerText = self:GetChildComponent("Container_Clear_PassWord/Container_PopupSmall/Container_Content/InputFieldSafeAnswer",'InputFieldMeshWrapper') 

    self._passwordTipsText = self:GetChildComponent("ScrollView/Mask/Content/Container_Status/Text_Pass",'TextMeshWrapper') 
    self._lockTipsText = self:GetChildComponent("ScrollView/Mask/Content/Container_Status/Text_Lock",'TextMeshWrapper') 
    self._lockButtonText = self:GetChildComponent("Button_Safe_Lock/Text",'TextMeshWrapper') 

    --清空密码
    self._clearPasswordButton = self:FindChildGO("Button_Clear_Password")
    self._csBH:AddClick(self._clearPasswordButton, function() self:OnClearPasswordButton() end)

    --修改密码
    self._modifyPasswordButton = self:FindChildGO("Button_Modify_Password")
    self._csBH:AddClick(self._modifyPasswordButton, function() self:OnModifyPasswordButton() end)

    --修改锁定状态
    self._modifyLockStatusButton = self:FindChildGO("Button_Safe_Lock")
    self._csBH:AddClick(self._modifyLockStatusButton, function() self:OnModifyLockStatusButton() end)

    --设定密码
    self._setPasswordButton = self:FindChildGO("Button_Set_Password")
    self._csBH:AddClick(self._setPasswordButton, function() self:OnSetPasswordButton() end)

    --关闭设定密码
    self._closeSetPasswordButton = self:FindChildGO("Container_Set_PassWord/Container_PopupSmall/Button_Close")
    self._csBH:AddClick(self._closeSetPasswordButton, function() self:OnCloseSetPasswordButton() end)

    --发送设定密码请求
    self._setPasswordReqButton = self:FindChildGO("Container_Set_PassWord/Container_PopupSmall/Button_Set_Password")
    self._csBH:AddClick(self._setPasswordReqButton, function() self:OnSetPasswordReqButton() end)

    --发送解锁请求
    self._unLockReqButton = self:FindChildGO("Container_UnLock/Container_PopupSmall/Button_Confirm_UnLock")
    self._csBH:AddClick(self._unLockReqButton, function() self:OnUnLockReqButton() end)

    --关闭解除锁定窗口
    self._closeUnLockButton = self:FindChildGO("Container_UnLock/Container_PopupSmall/Button_Unlock_Close")
    self._csBH:AddClick(self._closeUnLockButton, function() self:OnCloseUnLockButton() end)

    --关闭修改密码窗口
    self._closeModifyPasswordButton = self:FindChildGO("Container_Modify_PassWord/Container_PopupSmall/Button_Modify_Password_Close")
    self._csBH:AddClick(self._closeModifyPasswordButton, function() self:OnCloseModifyPasswordButton() end)

    --发送修改密码请求
    self._modifyPasswordReqButton = self:FindChildGO("Container_Modify_PassWord/Container_PopupSmall/Button_Modify_Password")
    self._csBH:AddClick(self._modifyPasswordReqButton, function() self:OnModifyPasswordReqButton() end)

    --关闭清空密码窗口
    self._closeClearPasswordButton = self:FindChildGO("Container_Clear_PassWord/Container_PopupSmall/Button_Clear_Password_Close")
    self._csBH:AddClick(self._closeClearPasswordButton, function() self:OnCloseClearPasswordButton() end)

    --发送清空密码请求
    self._clearPasswordReqButton = self:FindChildGO("Container_Clear_PassWord/Container_PopupSmall/Button_Clear_Password")
    self._csBH:AddClick(self._clearPasswordReqButton, function() self:OnClearPasswordReqButton() end)

    --选择问题按钮事件
    self._selectAnswerButton = self:FindChildGO("Container_Set_PassWord/Container_PopupSmall/Container_Content/Button_Select")
    self._csBH:AddClick(self._selectAnswerButton, function() self:OnSelectAnswerButton() end)

    --选择问题bg按钮事件
    self._selectAnswerBgButton = self:FindChildGO("Container_Set_PassWord/Container_PopupSmall/Container_Content/Image_Bg")
    self._csBH:AddClick(self._selectAnswerBgButton, function() self:OnSelectAnswerButton() end)
    
    --选择问题列表出现后点击空白处的处理事件
    self._selectAnswerShadeButton = self:FindChildGO("Container_Set_PassWord/Container_PopupSmall/Container_Question_List/Container_Shade")
    self._csBH:AddClick(self._selectAnswerShadeButton, function() self:OnSelectAnswerShadeButton() end)


    self:InitQuesitionList()
    self._questionListContainer:SetActive(false)
    self._selectAnswerShadeButton:SetActive(false)
    self._setPasswordContainer:SetActive(false)
    self._modifyPasswordContainer:SetActive(false)
    self._clearPasswordContainer:SetActive(false)
    self._unLockContainer:SetActive(false)
end

function SafeLockSettingView:InitQuesitionList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Set_PassWord/Container_PopupSmall/Container_Question_List/ScrollViewList")
	self._qusetionList = list
	self._qusetionListlView = scrollView 
    self._qusetionListlView:SetHorizontalMove(false)
    self._qusetionListlView:SetVerticalMove(true)
    self._qusetionList:SetItemType(QuestionListItem)
    self._qusetionList:SetPadding(0, 0, 0, 5)
    self._qusetionList:SetGap(0, 5)
    self._qusetionList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._questionListData = GlobalParamsHelper.GetParamValue(852)
    self._qusetionList:SetDataList(self._questionListData)

    local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
	self._qusetionList:SetItemClickedCB(itemClickedCB)
end

function SafeLockSettingView:OnListItemClicked(idx)
    local data = GameWorld.Player().security_lock_info
    self._selectQuestionId = data and data[public_config.SECURITY_QUESTION_ID] or (idx+1)
    self._questionText.text = LanguageDataHelper.CreateContent(self._questionListData[self._selectQuestionId])
    self._questionListContainer:SetActive(false)
    self._selectAnswerShadeButton:SetActive(false)
end

function SafeLockSettingView:ShowView()
    self._imageBGActiveFlowLight.enabled = true
    self:AddEventListeners()
    self:UpdateData()
end

function SafeLockSettingView:CloseView()
    self._imageBGActiveFlowLight.enabled = false
    self:RemoveEventListeners()
end

function SafeLockSettingView:UpdateData()
    local info = GameWorld.Player().security_lock_info
    -- LoggerHelper.Error("info ===" .. PrintTable:TableToStr(info))
    if not SystemSettingManager:IsHasPassword() then
        self._clearPasswordButton:SetActive(false)
        self._modifyLockStatusButton:SetActive(false)
        self._modifyPasswordButton:SetActive(false)
        self._setPasswordButton:SetActive(true)

        self._passwordTipsText.text = LanguageDataHelper.CreateContent(80640)
        self._lockTipsText.text = LanguageDataHelper.CreateContent(80637)
    else
        self._clearPasswordButton:SetActive(true)
        self._modifyLockStatusButton:SetActive(true)
        self._modifyPasswordButton:SetActive(true)
        self._setPasswordButton:SetActive(false)

        local lockFlag = info[public_config.SECURITY_LOCK_FLAG]
        if lockFlag and lockFlag == 1 then
            self._lockTipsText.text = LanguageDataHelper.CreateContent(80639)
            self._lockButtonText.text = LanguageDataHelper.CreateContent(80647)
        else
            self._lockTipsText.text = LanguageDataHelper.CreateContent(80637)
            self._lockButtonText.text = LanguageDataHelper.CreateContent(80646)
        end

        local passwordFlag = info[public_config.SECURITY_LOCK_PWD]
        if passwordFlag then
            self._passwordTipsText.text = LanguageDataHelper.CreateContent(80636)
        else
            self._passwordTipsText.text = LanguageDataHelper.CreateContent(80640)
        end
    end    
end

--清空密码
function SafeLockSettingView:OnClearPasswordButton()
    self._clearPasswordContainer:SetActive(true) 
    local cId = GlobalParamsHelper.GetParamValue(852)[GameWorld.Player().security_lock_info[public_config.SECURITY_QUESTION_ID]]
    self._clearQuestionText.text = LanguageDataHelper.CreateContent(cId)
end

--修改密码
function SafeLockSettingView:OnModifyPasswordButton()
    self._modifyPasswordContainer:SetActive(true)    
    local cId = GlobalParamsHelper.GetParamValue(852)[GameWorld.Player().security_lock_info[public_config.SECURITY_QUESTION_ID]]
    self._modifyQuestionText.text = LanguageDataHelper.CreateContent(cId)
end

--修改锁定状态
function SafeLockSettingView:OnModifyLockStatusButton()
    local info = GameWorld.Player().security_lock_info
    if (not table.isEmpty(info) and info[public_config.SECURITY_LOCK_FLAG] and info[public_config.SECURITY_LOCK_FLAG] == 1) then
        --解除锁定
        self._unLockContainer:SetActive(true)
    else
        --锁定
        SystemSettingManager:Lock()
    end
end

--设定密码
function SafeLockSettingView:OnSetPasswordButton()
    self._setPasswordContainer:SetActive(true)
    self:OnListItemClicked(0)
end

--关闭设定密码
function SafeLockSettingView:OnCloseSetPasswordButton()
    self._setPasswordContainer:SetActive(false)
    self._inputPasswordText.text = ""
    self._inputSafeAnswerText.text = ""
end

--关闭解除锁定窗口
function SafeLockSettingView:OnCloseUnLockButton()
    self._unLockContainer:SetActive(false)
    self._inputUnLockPasswordText.text = ""
end

--关闭修改密码窗口
function SafeLockSettingView:OnCloseModifyPasswordButton()
    self._modifyPasswordContainer:SetActive(false)
    self._modifyAnswerText.text = ""
    self._modifyPasswordText.text = ""
end

--关闭清空密码窗口
function  SafeLockSettingView:OnCloseClearPasswordButton()
    self._clearPasswordContainer:SetActive(false)
    self._clearAnswerText.text = ""
end

--请求设定密码按钮
function SafeLockSettingView:OnSetPasswordReqButton()
    SystemSettingManager:SetLockPassword(self._inputPasswordText.text, self._selectQuestionId, self._inputSafeAnswerText.text)
end

--发送解锁请求
function SafeLockSettingView:OnUnLockReqButton()
    SystemSettingManager:Unlock(self._inputUnLockPasswordText.text)
end

--发送修改密码请求
function SafeLockSettingView:OnModifyPasswordReqButton()
    SystemSettingManager:ModifyPassword(self._modifyAnswerText.text, self._modifyPasswordText.text)
end

--发送清空密码请求
function SafeLockSettingView:OnClearPasswordReqButton()
    SystemSettingManager:ClearPassword(self._clearAnswerText.text)
end

--选择问题按钮事件
function SafeLockSettingView:OnSelectAnswerButton()
    self._questionListContainer:SetActive(true)
    self._selectAnswerShadeButton:SetActive(true)
end

--选择问题列表出现后点击空白处的处理事件
function SafeLockSettingView:OnSelectAnswerShadeButton()
    self._questionListContainer:SetActive(false)
    self._selectAnswerShadeButton:SetActive(false)
end

-- --密码结束输入
-- function SafeLockSettingView:OnPasswordEndEdit(text)
--     local len = string.utf8len(text)
--     if len ~= 6 then
--         LoggerHelper.Log("password must be six digits")
--     end
--     LoggerHelper.Error("输入长度 ===" .. string.utf8len(text))
-- end

function SafeLockSettingView:SetPasswordSuccess()
    self:OnCloseSetPasswordButton()
    self:UpdateData()
end

function SafeLockSettingView:UnLockSuccess()
    self:OnCloseUnLockButton()
    self:UpdateData()
end

function SafeLockSettingView:LockSuccess()
    self:UpdateData()
end

function SafeLockSettingView:ModifySuccess()
    self:OnCloseModifyPasswordButton()
    self:UpdateData()
end

function SafeLockSettingView:ClearSuccess()
    self:OnCloseClearPasswordButton()
    self:UpdateData()
end