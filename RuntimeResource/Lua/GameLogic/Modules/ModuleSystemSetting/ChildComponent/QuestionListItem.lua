-- QuestionListItem.lua
local QuestionListItem = Class.QuestionListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
QuestionListItem.interface = GameConfig.ComponentsConfig.Component
QuestionListItem.classPath = "Modules.ModuleSystemSetting.ChildComponent.QuestionListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function QuestionListItem:Awake()
    self._qusetionText = self:GetChildComponent("Text_Question",'TextMeshWrapper')
end

function QuestionListItem:OnDestroy() 

end

--override
function QuestionListItem:OnRefreshData(id)
    self._qusetionText.text = LanguageDataHelper.CreateContent(id)    
end
