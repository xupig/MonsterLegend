-- SystemSettingImageItem.lua
local SystemSettingImageItem = Class.SystemSettingImageItem(ClassTypes.BaseComponent)

SystemSettingImageItem.interface = GameConfig.ComponentsConfig.Component
SystemSettingImageItem.classPath = "Modules.ModuleSystemSetting.ChildComponent.SystemSettingImageItem"


--override
function SystemSettingImageItem:Awake()
    self._selectButton = self:FindChildGO("Button_Set")
    self._csBH:AddClick(self._selectButton, function() self:OnSetSelect() end)

    self._suggestImage = self:FindChildGO("Image_Suggest")

    self._selectImage = self:FindChildGO("Image_Select")
end

--override
function SystemSettingImageItem:OnDestroy()
end

function SystemSettingImageItem:SetNum(num)
    self._num = num
end

function SystemSettingImageItem:SetSelect(flag)
    self._selectImage:SetActive(flag)
    self._suggestImage:SetActive(self._num == GameWorld.GetAutoQualityLevel())
end

function SystemSettingImageItem:OnSetSelect()
    EventDispatcher:TriggerEvent(GameEvents.SYSTEMSETTING_IMAGE_CHANGE, self._num)
end