-- SystemSettingModule.lua
SystemSettingModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function SystemSettingModule.Init()
    GUIManager.AddPanel(PanelsConfig.SystemSetting, "Panel_SystemSetting", GUILayer.LayerUIPanel, "Modules.ModuleSystemSetting.SystemSettingPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.SystemSettingShow, "Panel_SystemSettingShow", GUILayer.LayerUIPanel, "Modules.ModuleSystemSetting.SystemSettingShowPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return SystemSettingModule
