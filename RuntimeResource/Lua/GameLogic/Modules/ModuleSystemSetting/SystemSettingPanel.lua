--SystemSettingPanel.lua
local SystemSettingPanel = Class.SystemSettingPanel(ClassTypes.BasePanel)

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType

require "Modules.ModuleSystemSetting.ChildView.BaseSettingView"
local BaseSettingView = ClassTypes.BaseSettingView

require "Modules.ModuleSystemSetting.ChildView.SafeLockSettingView"
local SafeLockSettingView = ClassTypes.SafeLockSettingView

require "Modules.ModuleSystemSetting.ChildView.AutoGameSettingView"
local AutoGameSettingView = ClassTypes.AutoGameSettingView

function SystemSettingPanel:Awake()
    self:InitCallback()
    self:InitViews()
end

function SystemSettingPanel:InitCallback()
end

function SystemSettingPanel:InitViews()
    self:InitView("Container_Base_Setting", BaseSettingView, 1)
    self:InitView("Container_Auto_Game", AutoGameSettingView, 2)
    self:InitView("Container_Safe_Lock", SafeLockSettingView, 3)
end

function SystemSettingPanel:StructingViewInit()
    return true
end

function SystemSettingPanel:OnShow(data)
    self:AddEventListeners()
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
end

function SystemSettingPanel:SetData(data)
end

function SystemSettingPanel:OnClose()
    self:RemoveEventListeners()
end

function SystemSettingPanel:AddEventListeners()
end

function SystemSettingPanel:RemoveEventListeners()
end

function SystemSettingPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--关闭的时候重新开启二级主菜单
function SystemSettingPanel:ReopenSubMainPanel()
    return true
end