--SystemSettingShowPanel.lua
local SystemSettingShowPanel = Class.SystemSettingShowPanel(ClassTypes.BasePanel)

local SystemSettingManager = PlayerManager.SystemSettingManager
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local GUIManager = GameManager.GUIManager

function SystemSettingShowPanel:Awake()
    self:InitCallback()
    self:InitViews()
end

function SystemSettingShowPanel:InitCallback()
end

function SystemSettingShowPanel:InitViews()
    self._inputFieldMeshUnLockPassword = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_UnLock/Container_PopupSmall/Container_Content/InputFieldPassword")
    self._inputUnLockPasswordText = self:GetChildComponent("Container_UnLock/Container_PopupSmall/Container_Content/InputFieldPassword", 'InputFieldMeshWrapper')

    self._confirmButton = self:FindChildGO("Container_UnLock/Container_PopupSmall/Button_Confirm_UnLock")
    self._csBH:AddClick(self._confirmButton, function() self:OnConfirmButton() end)
    
    self._closeButton = self:FindChildGO("Container_UnLock/Container_PopupSmall/Button_Unlock_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnCloseButton() end)
end

function SystemSettingShowPanel:OnShow(cb)
    self._cb = cb
    self:AddEventListeners()
end

function SystemSettingShowPanel:SetData(data)
end

function SystemSettingShowPanel:OnClose()
    self._inputUnLockPasswordText.text = ""
    self:RemoveEventListeners()
end

function SystemSettingShowPanel:AddEventListeners()
end

function SystemSettingShowPanel:RemoveEventListeners()
end

function SystemSettingShowPanel:OnConfirmButton()
    SystemSettingManager:CheckPasswordToDo(self._inputUnLockPasswordText.text, self._cb)
end

function SystemSettingShowPanel:OnCloseButton()
    GUIManager.ClosePanel(PanelsConfig.SystemSettingShow)
end