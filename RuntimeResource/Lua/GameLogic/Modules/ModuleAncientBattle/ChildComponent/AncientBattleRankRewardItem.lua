require "UIComponent.Extend.ItemGrid"
require "Modules.ModuleGodsTemple.ChildComponent.TempleRewardItem"

local AncientBattleRankRewardItem = Class.AncientBattleRankRewardItem(ClassTypes.UIListItem)

AncientBattleRankRewardItem.interface = GameConfig.ComponentsConfig.Component
AncientBattleRankRewardItem.classPath = "Modules.ModuleAncientBattle.ChildComponent.AncientBattleRankRewardItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local GUIManager = GameManager.GUIManager
local GuildDataHelper = GameDataHelper.GuildDataHelper
local guildConquestData = PlayerManager.PlayerDataManager.guildConquestData
local TempleRewardItem = ClassTypes.TempleRewardItem
local UIList = ClassTypes.UIList
local AncientBattleDataHelper = GameDataHelper.AncientBattleDataHelper

function AncientBattleRankRewardItem:Awake()
    self:InitView()
end

--override
function AncientBattleRankRewardItem:OnDestroy() 

end

function AncientBattleRankRewardItem:InitView()
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Reward")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(TempleRewardItem)
    self._list:SetPadding(10, 0, 0, 10)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
end

--data :id
function AncientBattleRankRewardItem:OnRefreshData(data)
    self.data = data
    local rank = AncientBattleDataHelper:GetAncientBattleFieldRank(self.data)
    self._infoText.text = string.format("%d-%d", rank[1], rank[2])--LanguageDataHelper.CreateContentWithArgs(string.format("%d-%d", rank[1], rank[2]))
    local str
    if rank[1] == rank[2] then
        str = string.format("%d", rank[1])
    else
        str = string.format("%d-%d", rank[1], rank[2])
    end
    self._infoText.text = LanguageDataHelper.CreateContentWithArgs(51233, {["0"]=str})

    local listData = {}
    local cfg = AncientBattleDataHelper:GetAncientBattleFieldRankAward(self.data, GameWorld.Player().level)
    for i=3,#cfg,2 do
        table.insert(listData, {cfg[i], cfg[i+1]})
    end

    self._list:SetDataList(listData)
    if #listData > 5 then
        self._scrollView:SetScrollRectEnable(true)
    else
        self._scrollView:SetScrollRectEnable(false)
    end
end