-- AncientBattleModule.lua
AncientBattleModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function AncientBattleModule.Init()
    GUIManager.AddPanel(PanelsConfig.AncientBattle, "Panel_AncientBattle", GUILayer.LayerUIPanel, "Modules.ModuleAncientBattle.AncientBattlePanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return AncientBattleModule