require "Modules.ModuleAncientBattle.ChildView.AncientBattleRankRewardView"

--AncientBattlePanel.lua
local AncientBattlePanel = Class.AncientBattlePanel(ClassTypes.BasePanel)

local CloudPeakInstanceManager = PlayerManager.CloudPeakInstanceManager
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local AncientBattleRankRewardView = ClassTypes.AncientBattleRankRewardView
local AncientBattleManager = PlayerManager.AncientBattleManager

require"Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem

function AncientBattlePanel:Awake()
	self:InitCallback()
	self:InitComps()
end

function AncientBattlePanel:InitCallback()
end

function AncientBattlePanel:InitComps()
    self:InitRewardList()

	self._enterButton = self:FindChildGO("Container_CommonWindow/Container_Win/Button_Enter")
    self._csBH:AddClick(self._enterButton, function() self:OnEnter() end )

    self._closeButton = self:FindChildGO("Container_CommonWindow/Container_Win/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnClosePanel() end )

    self._bgIconObj = self:FindChildGO("Container_CommonWindow/Container_Win/Container_BgIcon")
    GameWorld.AddIcon(self._bgIconObj,13637)

    self._rankRewardButton = self:FindChildGO("Container_Content/Container_Reward/Button_Reward")
    self._csBH:AddClick(self._rankRewardButton,function() self:OnRankRewardButtonClick() end)

    self._rankRewardGO = self:FindChildGO("Container_RankReward")
    self._rankRewardView = self:AddChildLuaUIComponent("Container_RankReward", AncientBattleRankRewardView)
    self._rankRewardGO:SetActive(false)
end
	
function AncientBattlePanel:InitRewardList()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/ScrollViewList")
	self._dropList = list
	self._dropListView = scrollView 
    self._dropListView:SetHorizontalMove(true)
    self._dropListView:SetVerticalMove(false)
    self._dropList:SetItemType(RewardItem)
    self._dropList:SetPadding(0, 0, 0, 0)
    self._dropList:SetGap(0, 5)
    self._dropList:SetDirection(UIList.DirectionLeftToRight, -1, 1)

    local datas = GlobalParamsHelper.GetParamValue(961)
    self._datas = {}
    for i=1,#datas do
        table.insert(self._datas, {datas[i], 1})
    end
    self._dropList:SetDataList(self._datas)
    if #self._datas > 4 then
        self._dropListView:SetScrollRectEnable(true)
    else
        self._dropListView:SetScrollRectEnable(false)
    end
end

function AncientBattlePanel:OnShow(data)
end

function AncientBattlePanel:SetData(data)
end

function AncientBattlePanel:OnClose()
end

function AncientBattlePanel:OnEnter()
    AncientBattleManager:OnSpaceEnterReq()
end

function AncientBattlePanel:OnClosePanel()
    GUIManager.ClosePanel(PanelsConfig.AncientBattle)
end

function AncientBattlePanel:OnRankRewardButtonClick()
    self._rankRewardGO:SetActive(true)
    self._rankRewardView:ShowView()
end