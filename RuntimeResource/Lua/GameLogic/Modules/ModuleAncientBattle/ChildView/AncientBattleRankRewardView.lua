require "Modules.ModuleAncientBattle.ChildComponent.AncientBattleRankRewardItem"

local AncientBattleRankRewardView = Class.AncientBattleRankRewardView(ClassTypes.BaseLuaUIComponent)

AncientBattleRankRewardView.interface = GameConfig.ComponentsConfig.Component
AncientBattleRankRewardView.classPath = "Modules.ModuleAncientBattle.ChildView.AncientBattleRankRewardView"

local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local GuildDataHelper = GameDataHelper.GuildDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local AncientBattleRankRewardItem = ClassTypes.AncientBattleRankRewardItem
local guildConquestData = PlayerManager.PlayerDataManager.guildConquestData
local UIList = ClassTypes.UIList
local AncientBattleDataHelper = GameDataHelper.AncientBattleDataHelper

function AncientBattleRankRewardView:Awake()
    self:InitView()
end
--override
function AncientBattleRankRewardView:OnDestroy() 

end

function AncientBattleRankRewardView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function() self:OnCloseButtonClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(AncientBattleRankRewardItem)
    self._list:SetPadding(10, 0, 0, 16)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function AncientBattleRankRewardView:ShowView()
    local data = AncientBattleDataHelper:GetAllRankAwardId()
    self._list:SetDataList(data)
end

function AncientBattleRankRewardView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end