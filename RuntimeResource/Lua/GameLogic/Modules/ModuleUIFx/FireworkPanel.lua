-- FireworkPanel.lua
local FireworkPanel = Class.FireworkPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIParticle = ClassTypes.UIParticle
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function FireworkPanel:Awake()
    self._fx_Container = self:FindChildGO("Container_Fx")
    self._particleFireworks = {}
    local p1 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30021")
    p1:SetLifeTime(30)
    p1:Stop()
    table.insert(self._particleFireworks, p1)
    local p2 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30021_2")
    p2:SetLifeTime(30)
    p2:Stop()
    table.insert(self._particleFireworks, p2)
end

function FireworkPanel:OnShow(data)
    self:PlayFirework(data)
end

function FireworkPanel:SetData(data)
    self:PlayFirework(data)
end

function FireworkPanel:PlayFirework(data)
    local hitProps = GlobalParamsHelper.GetParamValue(785) or {}-- 婚礼用于加热度的道具ID（烟花）
    if data and data[1] then
        for i,v in ipairs(hitProps) do
            if v == data[1] and self._particleFireworks[i] then
                self._particleFireworks[i]:Play()
                return
            end
        end
    end
    self._particleFireworks[1]:Play()
end

function FireworkPanel:OnClose()
end

function FireworkPanel:OnDestroy()

end
