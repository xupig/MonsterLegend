-- UIFxModule.lua
UIFxModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function UIFxModule.Init()
    GUIManager.AddPanel(PanelsConfig.Firework, "Panel_FxFirework", GUILayer.LayerUIFx, "Modules.ModuleUIFx.FireworkPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.GiveFlowerFx, "Panel_GiveFlowerFx", GUILayer.LayerUIFx, "Modules.ModuleUIFx.GiveFlowerFxPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return UIFxModule
