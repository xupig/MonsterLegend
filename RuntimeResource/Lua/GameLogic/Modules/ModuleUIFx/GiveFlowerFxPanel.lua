-- GiveFlowerFxPanel.lua
local GiveFlowerFxPanel = Class.GiveFlowerFxPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIParticle = ClassTypes.UIParticle
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
function GiveFlowerFxPanel:Awake()
    self._fx_Container = self:FindChildGO("Container_Fx")
    self._particleFireworks = {}
    self._particleFirework = UIParticle.AddParticle(self._fx_Container, "fx_ui_30020")
    self._particleFirework:SetLifeTime(30)
    self._particleFirework:Stop()
end

function GiveFlowerFxPanel:OnShow(data)
    self:PlayFirework(data)
end

function GiveFlowerFxPanel:SetData(data)
    self:PlayFirework(data)
end

function GiveFlowerFxPanel:PlayFirework(data)
    self._particleFirework:Play()
end

function GiveFlowerFxPanel:OnClose()
end

function GiveFlowerFxPanel:OnDestroy()

end
