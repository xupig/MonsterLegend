-- TeamEnterInstancePanel.lua
local TeamEnterInstancePanel =  Class.TeamEnterInstancePanel(ClassTypes.BasePanel)

require "Modules.ModuleTeam.ChildComponent.EnterInstanceMemberItem"

local UIComponentUtil = GameUtil.UIComponentUtil
local UIProgressBar = ClassTypes.UIProgressBar
local EnterInstanceMemberItem = ClassTypes.EnterInstanceMemberItem
local TeamManager = PlayerManager.TeamManager
local teamData = PlayerManager.PlayerDataManager.teamData
local MapDataHelper = GameDataHelper.MapDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TeamDataHelper = GameDataHelper.TeamDataHelper
local UIToggle = ClassTypes.UIToggle
local SystemSettingConfig = GameConfig.SystemSettingConfig
local SystemSettingManager = PlayerManager.SystemSettingManager
local CDTIME = 10
local INTERVAL = 1
local confirmTxt = LanguageDataHelper.CreateContent(51148)
local CDAUTOTIME = 5

function TeamEnterInstancePanel:Awake()
	self._csBH = self:GetComponent('LuaUIPanel')
	self:Init()
	self:InitCallBackFunc()
end

function TeamEnterInstancePanel:InitCallBackFunc()
	self._updateTeamInfo = function() self:UpdateTeamInfo() end
	self._isAgreeClickable = false
	self._hasShowTips = false
end

function TeamEnterInstancePanel:OnShow()
	EventDispatcher:AddEventListener(GameEvents.TEAM_ENTER_COPY_UPDATE, self._updateTeamInfo)
	self._timerId = TimerHeap:AddSecTimer(0, INTERVAL, 0, function() self:CdTimer() end)
	self._timeLeft = CDTIME
	self._cdComp:SetProgress(1)
	if GameWorld.Player().team_status == 2 then
		self:InteractiveButton(false)
	else
		self:InteractiveButton(true)
	end
	self:InitAutoTeamStatus()
end

function TeamEnterInstancePanel:OnClose()
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_ENTER_COPY_UPDATE, self._updateTeamInfo)
	TimerHeap:DelTimer(self._timerId)
	self._isAgreeClickable = false
	self._hasShowTips = false
	self:StopAutoConfirmCounter()
end

function TeamEnterInstancePanel:Init()
	self._txtMapName = self:GetChildComponent("Text_Title","TextMeshWrapper")
	self._txtTipMapName = self:GetChildComponent("Text_TipMapName","TextMeshWrapper")

	self._btnAgree = self:FindChildGO("Button_Agree")
	self._btnRefuse = self:FindChildGO("Button_Refuse")
	self._btnAgreeWrapper = self:GetChildComponent("Button_Agree","ButtonWrapper")
	self._btnRefuseWrapper = self:GetChildComponent("Button_Refuse","ButtonWrapper")
	self._btnAgreeTxt = self:GetChildComponent("Button_Agree/Text", "TextMeshWrapper")

	self._btnClose = self:FindChildGO("Button_Close")

	self._csBH:AddClick(self._btnAgree, function() self:OnAgressBtnClick() end )
	self._csBH:AddClick(self._btnRefuse, function() self:OnRefuseBtnClick() end )
	self._csBH:AddClick(self._btnClose, function() 
		if self._isAgreeClickable == true then--当确定取消按钮可以点击时才能关闭
			self:OnRefuseBtnClick()
		end
	end)

	self._memberItems = {}
	for i=1,3 do
    	local go = self:FindChildGO("Container_Member/Container_Member"..i)
    	local item = UIComponentUtil.AddLuaUIComponent(go, EnterInstanceMemberItem)
    	self._memberItems[i] = item
    end

	self._cdComp = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar_CD")
    self._cdComp:ShowTweenAnimate(false)
    self._cdComp:SetMutipleTween(false)
    self._cdComp:SetIsResetToZero(false)

	self._txtCD = self:GetChildComponent("Text_CD","TextMeshWrapper")

	self._autoTeamToggle = UIToggle.AddToggle(self.gameObject, "Toggle_Auto_Team_Confirm")
    self._autoTeamToggle:SetOnValueChangedCB(function(state) self:OnAutoConfirmStateChanged(state) end)
end

function TeamEnterInstancePanel:OnAgressBtnClick()
	TeamManager:TeamAgreeEnterCopy(1,teamData.curMapId)
	self:InteractiveButton(false)
end

function TeamEnterInstancePanel:OnRefuseBtnClick()
	TeamManager:TeamAgreeEnterCopy(0,teamData.curMapId)
	self:InteractiveButton(false)
end

function TeamEnterInstancePanel:InteractiveButton(interactive)
	self._btnAgreeWrapper.interactable = interactive
	self._btnRefuseWrapper.interactable = interactive

	self._isAgreeClickable = interactive
end

function TeamEnterInstancePanel:UpdateTeamInfo()
	local myTeamData = teamData:GetMyTeam()
	local myTeamMemberData = teamData:GetMyTeamMemberInfos()
	for i=1,3 do
		local eci = self._memberItems[i]
		eci:UpdateData(myTeamMemberData[i])
	end
	if myTeamData.targetId then
		local mapId = TeamDataHelper.GetMapId(myTeamData.targetId)
		local mapName = MapDataHelper.GetChinese(mapId)
		if mapName then
			local str = LanguageDataHelper.CreateContent(mapName)
			self._txtMapName.text = str
			self._txtTipMapName.text = "【"..str.."】"
		end
	end
end

function TeamEnterInstancePanel:CdTimer()
	self._timeLeft = self._timeLeft - INTERVAL
	self._cdComp:SetProgress(self._timeLeft/CDTIME)
	self._txtCD.text = self._timeLeft.."s"
	if self._timeLeft <= 0 then
		self:OnRefuseBtnClick()
		TimerHeap:DelTimer(self._timerId)
		GameManager.GUIManager.ClosePanel(PanelsConfig.TeamEnterInstance)
	end
end

-- 切换状态检查
function TeamEnterInstancePanel:OnAutoConfirmStateChanged(state)
	self._autoTeamToggle:SetIsOn(state)
    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.AUTO_TEAMCONFIRM,state)
	
	if state==true and self._isAgreeClickable and self._autoConfirmTimer == nil then--
		self:StartAutoConfirmCounter()
	end
	-- 取消
	if state == false then
		-- 设置描述
		self:StopAutoConfirmCounter()
	end
end
-- 第一次进来检查
function TeamEnterInstancePanel:InitAutoTeamStatus()
	local canAutoConfirm = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.AUTO_TEAMCONFIRM)
	self._autoTeamToggle:SetIsOn(canAutoConfirm)
	if canAutoConfirm==true and self._isAgreeClickable then
		self:StartAutoConfirmCounter()
	end
end

-- 自动开始倒计时
function TeamEnterInstancePanel:AutoConfirmCountDown()
	local passTime = CDTIME - self._timeLeft
	--设置描述
	self._btnAgreeTxt.text = string.format("%s(%d)", confirmTxt, CDAUTOTIME - passTime)
	if passTime >= CDAUTOTIME and self._isAgreeClickable then--过了5秒
		self:OnAgressBtnClick()
		self:StopAutoConfirmCounter()
	end
end
-- 开启自动同意倒计时
function TeamEnterInstancePanel:StartAutoConfirmCounter()
	if self._autoConfirmTimer ~= nil then return end
	self._autoConfirmTimer = TimerHeap:AddSecTimer(0, INTERVAL, 0, function() self:AutoConfirmCountDown() end)

	if self._hasShowTips == false then--一次提示
		local duration = CDAUTOTIME
		if CDAUTOTIME > self._timeLeft then
			duration = self._timeLeft
		end
		if duration > 0 then
			GameManager.GUIManager.ShowText(2,LanguageDataHelper.CreateContent(58796), duration)
			self._hasShowTips = true
		end
		-- LoggerHelper.Error("提示信息：" .. LanguageDataHelper.CreateContent(58796) .. ":sec:" .. tostring(duration))
	end
end
-- 关闭自动同意倒计时
function TeamEnterInstancePanel:StopAutoConfirmCounter()
	TimerHeap:DelTimer(self._autoConfirmTimer)
	self._autoConfirmTimer = nil
	self._btnAgreeTxt.text = confirmTxt
end
