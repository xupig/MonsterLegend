-- TeamPanel.lua
local TeamPanel = Class.TeamPanel(ClassTypes.BasePanel)

require "Modules.ModuleTeam.ChildView.MyTeamView"
require "Modules.ModuleTeam.ChildView.TeamListView"
require "Modules.ModuleTeam.ChildView.TeamInviteView"
require "Modules.ModuleTeam.ChildView.TeamTargetView"

local MyTeamView = ClassTypes.MyTeamView
local TeamListView = ClassTypes.TeamListView
local TeamInviteView = ClassTypes.TeamInviteView
local TeamTargetView = ClassTypes.TeamTargetView
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager

function TeamPanel:Awake()
	self._csBH = self:GetComponent('LuaUIPanel')
	self:InitCallBack()
	self:InitComps()
end

function TeamPanel:OnDestroy()
	self._csBH = nil
	self._myTeamView = nil
	self._teamListView = nil
end

function TeamPanel:InitCallBack()
	self._tabClickCB = function (index)
		self:OnTabClick(index)
	end

	self._showInviteViewCB = function (shouldShow)
		self:ShowInviteView(shouldShow)
	end

	self._showTargetViewCB = function (shouldShow,showType)
		self:ShowTargetView(shouldShow,showType)
	end

	self._checkShowMyTeamCB = function ()
		self:CheckShowMyTeam()
	end

	-- self._createTeamCb = function ()
	-- 	self:OnFirstTabClick(2)
	-- end
end

function TeamPanel:OnShow(data)
	EventDispatcher:AddEventListener(GameEvents.TEAM_MY_UPDATE,self._checkShowMyTeamCB)
	-- EventDispatcher:AddEventListener(GameEvents.TEAM_TEAM_CREATE,self._createTeamCb)
	EventDispatcher:AddEventListener(GameEvents.TEAM_SHOW_TARGET_VIEW,self._showTargetViewCB)
	EventDispatcher:AddEventListener(GameEvents.TEAM_SHOW_INVITE_VIEW,self._showInviteViewCB)
	self:SetData(data)
end

function TeamPanel:SetData(data)
	if data and data.targetId then
		self._targetId = data.targetId
	end
	self:UpdateFunctionOpen()
	self:OnShowSelectTab(data)
	self:CheckShowMyTeam()
	if data and data.invite then
		self:ShowInviteView(true)
	end

end

function TeamPanel:OnClose()
	self._myTeamView:SetActive(false)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_MY_UPDATE,self._checkShowMyTeamCB)
	-- EventDispatcher:RemoveEventListener(GameEvents.TEAM_TEAM_CREATE,self._createTeamCb)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_SHOW_TARGET_VIEW,self._showTargetViewCB)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_SHOW_INVITE_VIEW,self._showInviteViewCB)
end

--初始化
function TeamPanel:InitComps()
	--两个子View
	self._myTeamView = self:AddChildLuaUIComponent("Container_MyTeam", MyTeamView)
	self._teamListView = self:AddChildLuaUIComponent("Container_TeamList", TeamListView)
	self._myTeamView:SetActive(false)
	self._teamListView:SetActive(false)

	self._targetView = self:AddChildLuaUIComponent("Container_TeamTarget", TeamTargetView)
	self._inviteView = self:AddChildLuaUIComponent("Container_TeamInvite", TeamInviteView)
	self._targetView:SetActive(false)
	self._inviteView:SetActive(false)

	self:SetTabClickCallBack(self._tabClickCB)
end

function TeamPanel:OnTabClick(index)
	if index == 2 then
		self:ShowMyTeam()
	else
		self:ShowTeamList()
	end
end

function TeamPanel:CheckShowMyTeam()
	if GameWorld.Player().team_id > 0 then
		self:SetFirstTabActive(2,true)
	else
		self:SetFirstTabActive(2,false)
	end
end

--显示我的队伍
function TeamPanel:ShowMyTeam()
	self._myTeamView:SetActive(true)
	self._teamListView:SetActive(false)
end

--显示队伍列表
function TeamPanel:ShowTeamList()
	self._myTeamView:SetActive(false)
	self._teamListView:SetActive(true)
	if self._targetId then
		self._teamListView:SetTargetId(self._targetId)
		self._targetId = nil
	end
end

function TeamPanel:ShowTargetView(shouldShow,showType)
	self._targetView:SetActive(shouldShow)
	if shouldShow then
		self._targetView:UpdateView(showType)
	end
end

function TeamPanel:ShowInviteView(shouldShow)
	self._inviteView:SetActive(shouldShow)
	if shouldShow then
		self._inviteView:OnShow()
	else
		self._inviteView:OnClose()
	end
	
end

--从副本进入组队
function TeamPanel:ShowByInstance(instId)
	if GameWorld.Player().team_status > 0 then
		self._myTeamView:SetActive(true)
		self._teamListView:SetActive(false)
		self._myTeamView:SetTargetIdByInstance(instId)
	else
		self._myTeamView:SetActive(false)
		self._teamListView:SetActive(true)
		self._teamListView:SetTargetIdByInstance(instId)
	end
end

-- -- 0 没有  1 普通底板
function TeamPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function TeamPanel:BaseShowModel()
	if self._myTeamView.gameObject.activeSelf then
		self._myTeamView:ShowModel()
	end
end

function TeamPanel:BaseHideModel()
	if self._myTeamView.gameObject.activeSelf then
		self._myTeamView:HideModel()
	end
end