-- EnterInstanceMemberItem.lua

local EnterInstanceMemberItem = Class.EnterInstanceMemberItem(ClassTypes.BaseLuaUIComponent)
EnterInstanceMemberItem.interface = GameConfig.ComponentsConfig.Component
EnterInstanceMemberItem.classPath = "Modules.ModuleTeam.ChildComponent.EnterInstanceMemberItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local ColorConfig = GameConfig.ColorConfig
local StringStyleUtil = GameUtil.StringStyleUtil

function EnterInstanceMemberItem:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self._headContainer = self:FindChildGO("Container_Head")

	self._txtName = self:GetChildComponent("Text_Name","TextMeshWrapper")
	self._imgCheck = self:FindChildGO("Image_Check")
end


function EnterInstanceMemberItem:UpdateData(memberInfo)
	if memberInfo == nil then
		self._headContainer:SetActive(false)
		self._txtName.text = ""
		self._imgCheck:SetActive(false)
		return
	else
		self._headContainer:SetActive(true)
	end
	local name = memberInfo:GetName()
	local headIcon = RoleDataHelper.GetHeadIconByVocation(memberInfo:GetVocation())
	if memberInfo._agreeToInstance or memberInfo._isCaptain then
		GameWorld.AddIcon(self._headContainer,headIcon)
		self._txtName.text = name
		self._imgCheck:SetActive(true)
	else
		GameWorld.AddIcon(self._headContainer,headIcon,0,false,0)
		self._txtName.text = StringStyleUtil.GetColorStringWithTextMesh(name,ColorConfig.C)
		self._imgCheck:SetActive(false)
	end
end