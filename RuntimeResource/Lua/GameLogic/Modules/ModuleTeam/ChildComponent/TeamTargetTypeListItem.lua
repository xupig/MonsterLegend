--队伍目标列表ListItem
-- TeamTargetTypeListItem.lua
local TeamTargetTypeListItem = Class.TeamTargetTypeListItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
TeamTargetTypeListItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
TeamTargetTypeListItem.classPath = "Modules.ModuleTeam.ChildComponent.TeamTargetTypeListItem"
local ColorConfig = GameConfig.ColorConfig
local StringStyleUtil = GameUtil.StringStyleUtil

function TeamTargetTypeListItem:Awake()
    self._base.Awake(self)
    self._txtType = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._imgSelected = self:FindChildGO("Image_Selected")
    self._isSelected = false
end

--override
function TeamTargetTypeListItem:OnRefreshData(data)
	self._name = LanguageDataHelper.CreateContent(data)
	self:UpdateText()
end

function TeamTargetTypeListItem:SetSelected(isSelected)
	self._isSelected = isSelected
	self._imgSelected:SetActive(isSelected)
	self:UpdateText()
end

function TeamTargetTypeListItem:UpdateText()
	if self._isSelected then
		self._txtType.text = StringStyleUtil.GetColorStringWithTextMesh(self._name, ColorConfig.A)
	else
		self._txtType.text = StringStyleUtil.GetColorStringWithTextMesh(self._name, ColorConfig.B)
	end
end