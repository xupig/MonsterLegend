--队伍目标列表ListItem
-- TeamTargetListItem.lua
local TeamTargetListItem = Class.TeamTargetListItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
TeamTargetListItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
TeamTargetListItem.classPath = "Modules.ModuleTeam.ChildComponent.TeamTargetListItem"
-- local ColorConfig = GameConfig.ColorConfig
-- local StringStyleUtil = GameUtil.StringStyleUtil

function TeamTargetListItem:Awake()
    self._base.Awake(self)
    self._containerIcon = self:FindChildGO("Container_Icon")
    self._txtTargetName = self:GetChildComponent("Text_TargetName", "TextMeshWrapper")
    self._txtLevel = self:GetChildComponent("Text_Level", "TextMeshWrapper")
end

--override
function TeamTargetListItem:OnRefreshData(data)
	self._txtTargetName.text = LanguageDataHelper.CreateContent(data.sub_type) 
	self._txtLevel.text = data.level..LanguageDataHelper.CreateContent(69) 
	GameWorld.AddIcon(self._containerIcon,data.icon)
end