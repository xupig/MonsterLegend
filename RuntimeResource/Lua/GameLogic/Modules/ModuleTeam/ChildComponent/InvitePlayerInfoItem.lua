--邀请面板上的玩家ListItem
-- InvitePlayerInfoItem.lua
local TeamManager = PlayerManager.TeamManager
local InvitePlayerInfoItem = Class.InvitePlayerInfoItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local public_config = GameWorld.public_config
local teamData = PlayerManager.PlayerDataManager.teamData
local ColorConfig = GameConfig.ColorConfig
local StringStyleUtil = GameUtil.StringStyleUtil

InvitePlayerInfoItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
InvitePlayerInfoItem.classPath = "Modules.ModuleTeam.ChildComponent.InvitePlayerInfoItem"

function InvitePlayerInfoItem:Awake()
    self._base.Awake(self)
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    --self._textLevel = self:GetChildComponent("Text_Level", "TextMeshWrapper")
    self._textFightPower = self:GetChildComponent("Text_FightPower", "TextMeshWrapper")
    self._headIcon = self:FindChildGO("Container_HeadIcon")
    self._buttonInvite = self:FindChildGO("Button_Invite");
    self._buttonInviteTxt = self:GetChildComponent("Button_Invite/Text","TextMeshWrapper")
    self._csBH:AddClick(self._buttonInvite , function() self:OnClickInvite() end)
end

function InvitePlayerInfoItem:OnDestroy() 
	self._textName = nil
    self._textFightPower = nil
    self._buttonInvite = nil
    self._headIcon = nil
    self._csBH = nil
end

function InvitePlayerInfoItem:OnClickInvite()
	teamData:SetHasInvited(self._playerDBId)
	self:SetBtnText()
	TeamManager:TeamInvite(self._playerDBId)
end

--override
function InvitePlayerInfoItem:OnRefreshData(data)
	self._data = data
	local type = data[1]
	local d = data[2] --玩家数据
	if d ~= nil then
		local headId
		local vocation
		local name
		local level
		local fightPower
		if type == 1 then
			vocation = d:GetVocation()
			name = d:GetName()
			level = d:GetLevel()
			fightPower = d:GetFightForce()
			self._playerDBId = d:GetDBId()
		elseif type == 2 then
			vocation = d:GetVocation()
    		name = d:GetName()
    		level = d:GetLevel()
    		fightPower = d:GetFightForce()
    		self._playerDBId = d:GetDbid()
    	elseif type == 3 then
    		vocation = d[public_config.GUILD_MEMBER_INFO_VOCATION]
    		name = d[public_config.GUILD_MEMBER_INFO_NAME]
    		level = d[public_config.GUILD_MEMBER_INFO_LEVEL]
    		fightPower = d[public_config.GUILD_MEMBER_INFO_FIGHT_FORCE]
    		self._playerDBId = d[public_config.GUILD_MEMBER_INFO_DBID]
		end
		local level = StringStyleUtil.GetLevelString(level)
		self._textName.text = name..StringStyleUtil.GetColorStringWithTextMesh(level,ColorConfig.A)
		--self._textJob.text = RoleDataHelper.GetRoleNameTextByVocation(vocation)
		self._textFightPower.text = tostring(fightPower)
		headId = RoleDataHelper.GetHeadIconByVocation(vocation) 
		GameWorld.AddIcon(self._headIcon, headId)
	end
	self:SetBtnText()
end

--设定按钮上文本
function InvitePlayerInfoItem:SetBtnText()
	local hasInvited = teamData:GetHasInvited(self._playerDBId)
	if hasInvited then
		self._buttonInviteTxt.text = LanguageDataHelper.GetContent(10025)
	else
		self._buttonInviteTxt.text = LanguageDataHelper.GetContent(10015)
	end
end
