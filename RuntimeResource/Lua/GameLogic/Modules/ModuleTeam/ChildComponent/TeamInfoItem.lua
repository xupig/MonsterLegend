-- TeamInfoItem.lua
local UIListItem = ClassTypes.UIListItem
local TeamInfoItem = Class.TeamInfoItem(UIListItem)
TeamInfoItem.interface = GameConfig.ComponentsConfig.PointableComponent
TeamInfoItem.classPath = "Modules.ModuleTeam.ChildComponent.TeamInfoItem"

local TeamManager = PlayerManager.TeamManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TeamDataHelper = GameDataHelper.TeamDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
require 'UIComponent.Extend.PlayerHead'
local PlayerHead = ClassTypes.PlayerHead
local StringStyleUtil = GameUtil.StringStyleUtil

function TeamInfoItem:Awake()
	UIListItem.Awake(self)
	self:Init()
end

function TeamInfoItem:Init()
	self._playerHead = self:AddChildLuaUIComponent('Container_Head', PlayerHead)
	self._txtMapName = self:GetChildComponent("Text_Target","TextMeshWrapper")
	self._txtMapLevel = self:GetChildComponent("Text_LevelLimit","TextMeshWrapper")
	self._btnApply = self:FindChildGO("Button_Apply")
	self._btnApplyWrapper = self:GetChildComponent("Button_Apply","ButtonWrapper")
	self._btnApplyTxt = self:GetChildComponent("Button_Apply/Text","TextMeshWrapper")
	self._csBH:AddClick(self._btnApply,function() self:OnClickApply() end)

	self._memberCountList = {}
	for i=1,3 do
		self._memberCountList[i] = self:FindChildGO("Container_MemberCount/Image"..i)
	end
end

function TeamInfoItem:OnDestroy()
	self._txtMapName = nil
	self._btnApply = nil
end

function TeamInfoItem:OnPointerDown(pointerEventData)

end

function TeamInfoItem:OnClickApply()
	--LoggerHelper.Error("self._teamId"..self._teamId)
	if self._data and self._data:GetHasApplied() == false then
		self._data:SetHasApplied(true)
		self:SetBtnText()
		TeamManager:TeamApply(self._teamId)
	end
end

--override
function TeamInfoItem:OnRefreshData(data)
	self._data = data
	if data then
		-- LoggerHelper.Log(data)
		self._teamId = data:GetTeamId()
		--local captainInfo = data:GetCaptainInfo()

		local headIcon = RoleDataHelper.GetHeadIconByVocation(data:GetCaptainVocation())
		self._playerHead:SetPlayerHeadData(data:GetCaptainName(), StringStyleUtil.GetLevelString(data:GetCaptainLevel()),headIcon)

		local targetId = data:GetTargetId()
		if targetId > 0 then
			local cfgData = TeamDataHelper.GetTeamTarget(targetId)
			self._txtMapName.text = LanguageDataHelper.CreateContent(cfgData.sub_type)
			self._txtMapLevel.text = cfgData.level..LanguageDataHelper.CreateContent(69)
		else
			self._txtMapName.text = LanguageDataHelper.CreateContent(0)
			self._txtMapLevel.text = "1"..LanguageDataHelper.CreateContent(69)
		end
		
		local memberCount = data:GetTeamMemberCount()
		for i=1,3 do
			self._memberCountList[i]:SetActive(i<= memberCount)
		end

		if GameWorld.Player().team_status > 0 then
			self._btnApply:SetActive(false)
		else
			self._btnApply:SetActive(true)
			self:SetBtnText()
		end
    	
	end
end

--设定按钮上文本
function TeamInfoItem:SetBtnText()
	if self._data and self._data:GetHasApplied() then
		self._btnApplyTxt.text = LanguageDataHelper.GetContent(10024)
		self._btnApplyWrapper.interactable = false
	else
		self._btnApplyTxt.text = LanguageDataHelper.GetContent(10011)
		if self._data:GetTeamMemberCount() == 3 then
			self._btnApplyWrapper.interactable = false
		else
			self._btnApplyWrapper.interactable = true
		end
	end
end