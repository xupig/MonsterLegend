-- TeamMemberItem.lua
-- 我的队伍面板里面成员的ListItem
local TeamMemberItem = Class.TeamMemberItem(ClassTypes.BaseComponent)
TeamMemberItem.interface = GameConfig.ComponentsConfig.PointableComponent
TeamMemberItem.classPath = "Modules.ModuleTeam.ChildComponent.TeamMemberItem"
local TeamManager = PlayerManager.TeamManager
local ActorModelComponent = GameMain.ActorModelComponent
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local SceneConfig = GameConfig.SceneConfig
local MapDataHelper = GameDataHelper.MapDataHelper

function TeamMemberItem:Awake()
    self._base.Awake(self)
    self._containerModel = self:FindChildGO("Container_Model")
    self._playerModelComponent = self:AddChildComponent('Container_Model', ActorModelComponent)
    self._playerModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 0.8)

    self._txtName 		= self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._txtLevel 	    = self:GetChildComponent("Text_Level", "TextMeshWrapper")
    self._txtTag 	    = self:GetChildComponent("Text_Tag", "TextMeshWrapper")
    self._leaderIcon 	= self:FindChildGO("Image_LeaderIcon")
    self._leaderIconTrans = self._leaderIcon.transform

    self._locationContainer = self:FindChildGO("Container_Location")
	self._locationText = self:GetChildComponent("Container_Location/Container_Arrow/Image_Tail/Text_Des","TextMeshWrapper")

    self._btnAdd     = self:FindChildGO("Button_Add")
    self._csBH:AddClick(self._btnAdd , function() self:OnButtonAddClick() end)

    -- self._btnTouch     = self:FindChildGO("Button_Touch")
    -- self._csBH:AddClick(self._btnTouch , function() self:OnClickTouch() end)

    self._playerId = 0
end

function TeamMemberItem:OnDestroy() 
	self._btnAdd 	    = nil
    self._txtName 		= nil
    self._txtLevel  	= nil
    self._btnTouch 		= nil
    self._leaderIcon 	= nil
    self._txtTag        = nil
end

--override
function TeamMemberItem:UpdateData(data)
    self._data = data
    if data then
    	self._playerId = data:GetDBId()
    	--LoggerHelper.Error("TeamMemberItemId:"..self._playerId)
    	self._txtName.text = data:GetName()
        self._txtLevel.text = data:GetLevel()..LanguageDataHelper.CreateContent(69)
        if data:GetOnline() == 0 then--离线
			self._txtTag.text = LanguageDataHelper.CreateContent(616)
            self._locationContainer:SetActive(false)
		else
			local mapId = data:GetMapId()
			local myMapId = TeamManager:GetMyMapId()
            local sceneType = MapDataHelper.GetSceneType(mapId) 
            if myMapId == mapId then--附近
                if sceneType == SceneConfig.SCENE_TYPE_MAIN or sceneType == SceneConfig.SCENE_TYPE_WILD then
					self._locationContainer:SetActive(false)
				else
					self._locationContainer:SetActive(true)
					self._locationText.text = LanguageDataHelper.CreateContent(MapDataHelper.GetChinese(mapId))
				end
				self._txtTag.text = LanguageDataHelper.CreateContent(614)
            else--远离
                if data:GetDBId() ~= GameWorld.Player().dbid then
					--队员
					if sceneType ~= SceneConfig.SCENE_TYPE_MAIN and sceneType ~= SceneConfig.SCENE_TYPE_WILD then
						self._locationContainer:SetActive(true)
						self._locationText.text = LanguageDataHelper.CreateContent(MapDataHelper.GetChinese(mapId))
					else
						self._locationContainer:SetActive(false)
					end
				else
					--自己
					self._locationContainer:SetActive(false)
				end
				self._txtTag.text = LanguageDataHelper.CreateContent(615)
			end
		end

        if data:GetIsCaptain() then
            self._leaderIcon:SetActive(true)
            self._leaderIconTrans.localPosition = Vector2(133-self._txtName.preferredWidth/2,27)
        else
            self._leaderIcon:SetActive(false)
        end

        self._containerModel:SetActive(true)
        local vocation = data:GetVocation()
        local facade = data:GetFacade()
        self._playerModelComponent:LoadAvatarModel(vocation, facade, false)

        self._btnAdd:SetActive(false)
       
        --队长才能踢人
		-- if GameWorld.Player().team_status == 2 then
		-- 	if self._playerId == GameWorld.Player().cs.dbid then
		-- 		self._btnKick:SetActive(false)
		-- 	else
		-- 		self._btnKick:SetActive(true)
		-- 	end
		-- else
		-- 	self._btnKick:SetActive(false)
		-- end
    else
        if GameWorld.Player().team_status == 2 then
            self._btnAdd:SetActive(true)
        else
            self._btnAdd:SetActive(false)
        end
        
        self._containerModel:SetActive(false)
    	self._txtName.text = ""
        self._txtLevel.text = ""
        self._txtTag.text = ""
    	self._leaderIcon:SetActive(false)
		self._locationContainer:SetActive(false)
    end
end

--打开邀请面板
function TeamMemberItem:OnButtonAddClick()
    if GameWorld.Player().team_status == 2 and self._data == nil then
	   EventDispatcher:TriggerEvent(GameEvents.TEAM_SHOW_INVITE_VIEW,true)
    end
end

function TeamMemberItem:OnPointerDown(pointerEventData)
    
end

function TeamMemberItem:OnPointerUp(pointerEventData)
    if self._data == nil or self._data:GetDBId() == GameWorld.Player().dbid then
        return
    end
    if self._data then
        local d = {}
        d.playerName = self._data:GetName()
        d.dbid = self._data:GetDBId()
        d.teamInfo = true
        --LoggerHelper.Error(..","..pointerEventData.position.y)
        local posVec = Vector2.New(0,0)
        posVec.x = pointerEventData.position.x
        posVec.y = pointerEventData.position.y-720
        d.listPos = posVec
        GUIManager.ShowPanel(PanelsConfig.PlayerInfoMenu,d)
    end
end

function TeamMemberItem:OnClickTouch()
    
end

function TeamMemberItem:ShowModel()
	self._containerModel:SetActive(true)
end

function TeamMemberItem:HideModel()
	self._containerModel:SetActive(false)
end

--踢出成员
-- function TeamMemberItem:OnButtonKickClick()
-- 	if self._playerId > 0 then
--         TeamManager:TeamKick(self._playerId)
-- 	end
-- end