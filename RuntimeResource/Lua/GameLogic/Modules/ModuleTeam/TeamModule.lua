-- TeamModule.lua
TeamModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function TeamModule.Init()
    GUIManager.AddPanel(PanelsConfig.Team, "Panel_Team", GUILayer.LayerUIPanel, "Modules.ModuleTeam.TeamPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.TeamEnterInstance, "Panel_TeamEnterInstance", GUILayer.LayerUICommon, "Modules.ModuleTeam.TeamEnterInstancePanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return TeamModule