-- TeamInviteView.lua
local TeamInviteView = Class.TeamInviteView(ClassTypes.BaseComponent)

TeamInviteView.interface = GameConfig.ComponentsConfig.Component
TeamInviteView.classPath = "Modules.ModuleTeam.ChildView.TeamInviteView"

require "Modules.ModuleTeam.ChildComponent.InvitePlayerInfoItem"
local TeamData = PlayerManager.PlayerDataManager.teamData
local GuildData = PlayerManager.PlayerDataManager.guildData
local friendData = PlayerManager.PlayerDataManager.friendData
local InvitePlayerInfoItem = ClassTypes.InvitePlayerInfoItem
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIList = ClassTypes.UIList
local TeamManager = PlayerManager.TeamManager
local GuildManager = PlayerManager.GuildManager
local FriendManager = PlayerManager.FriendManager
local public_config = GameWorld.public_config

function TeamInviteView:Awake()
	self._csBH = self:GetComponent('LuaUIComponent')
	self._selectTabIndex = 0
	self:InitCallBack()
	self:InitComps()
end

function TeamInviteView:InitCallBack()
	self._updateNearPlayerCB = function () self:UpdateNearPlayer() end
	self._updateGuildMemberCB = function () self:UpdateGuildMember() end
	self._updateFriendPlayerCB = function () self:UpdateFriendPlayer() end
end

function TeamInviteView:OnShow()
	self._guildPlayerInited = false
	self._nearPlayerInited = false
	self._friendPlayerInited = false
	EventDispatcher:AddEventListener(GameEvents.TEAM_ALL_NEAR_PLAYER,self._updateNearPlayerCB)
	EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Self_Online_Member_Info,self._updateGuildMemberCB)
	EventDispatcher:AddEventListener(GameEvents.OnFriendListResp,self._updateFriendPlayerCB)

	self._toggleGroup:SetSelectIndex(0)
end

function TeamInviteView:OnClose()
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_ALL_NEAR_PLAYER,self._updateNearPlayerCB)
	EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Self_Online_Member_Info,self._updateGuildMemberCB)
	EventDispatcher:RemoveEventListener(GameEvents.OnFriendListResp,self._updateFriendPlayerCB)
end

function TeamInviteView:OnDestroy()
	self._csBH = nil
end

function TeamInviteView:InitComps()
	self._btnClickClose = self:FindChildGO("Button_Close")
	self._csBH:AddClick(self._btnClickClose, function() self:OnClickClose() end )

	self._toggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup_Tab")
	self._toggleGroup:SetRepeatClick(true)
	self._toggleGroup:SetOnSelectedIndexChangedCB(function(index) self:OnToggleGroupClick(index) end)

	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Player")
	self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(InvitePlayerInfoItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 5)

    self._nextCB = function() self:OnRequestNext() end
    self._scrollView:SetOnRequestNextCB(self._nextCB)
end

--切换标签
function TeamInviteView:OnToggleGroupClick(index)
	--LoggerHelper.Error("OnToggleGroupClick"..index)
	self._selectTabIndex = index
	--附近玩家
	if index == 0 then
		self:GetNearPlayerData()
	elseif index == 1 then
		self:UpdateFriendPlayer()
	elseif index == 2 then
		self:GetGuildMember()
	end
end


function TeamInviteView:OnRequestNext()
	
end

--好友玩家
function TeamInviteView:GetFriendPlayer()
	if self._friendPlayerInited then
		self:UpdateFriendPlayer()
	else
		FriendManager:SendFriendListReq()
	end
end

function TeamInviteView:UpdateFriendPlayer()
	self._friendPlayerInited = true
	local playerInfos = friendData:GetOnlineListForFriendList()

	local listdata = {}
	for i=1,#playerInfos do
		--LoggerHelper.Error("#playerInfos" .. #playerInfos[i])
		listdata[i] = {2,playerInfos[i]}
	end
	self._list:SetDataList(listdata)
end

--公会玩家
function TeamInviteView:GetGuildMember()
	if self._guildPlayerInited then
		self:UpdateGuildMember()
	else
		GuildManager:GuildSelfOnlineMemberInfoReq()
	end
end

function TeamInviteView:UpdateGuildMember()
	self._guildPlayerInited = true
	local playerInfos = GuildData.guildSelfOnlineMemberList
	local myDbid = GameWorld.Player().dbid
	local listdata = {}
	local h = 1
	for i=1,#playerInfos do
		local d = playerInfos[i]
		if d[public_config.GUILD_MEMBER_INFO_DBID] ~= myDbid then
			listdata[h] = {3,d}
			h = h + 1
		end
	end
	self._list:SetDataList(listdata)
end

--最近组队玩家
-- function TeamInviteView:GetRecentPlayerData()
-- 	if self._recentPlayerInited then
-- 		self:UpdateRecentPlayerData()
-- 	else
-- 		TeamManager:RequestRecentPlayers()
-- 	end
-- end

-- function TeamInviteView:UpdateRecentPlayer()
-- 	self._recentPlayerInited = true
-- end

--附近玩家列表数据
function TeamInviteView:GetNearPlayerData()
	if self._nearPlayerInited then
		self:UpdateNearPlayer()
	else
		TeamManager:RequestNearPlayers(0)
	end
end

function TeamInviteView:UpdateNearPlayer()
	self._nearPlayerInited = true
	local playerInfos = TeamData.NearPlayer
	local listdata = {}
	for i=1,#playerInfos do
		--LoggerHelper.Error("#playerInfos" .. #playerInfos[i])
		listdata[i] = {1,playerInfos[i]}
	end
	self._list:SetDataList(listdata)
end

function TeamInviteView:OnClickClose()
	EventDispatcher:TriggerEvent(GameEvents.TEAM_SHOW_INVITE_VIEW,false)
end