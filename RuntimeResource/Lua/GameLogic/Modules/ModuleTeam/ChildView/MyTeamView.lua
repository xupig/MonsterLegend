-- MyTeamView.lua
local MyTeamView = Class.MyTeamView(ClassTypes.BaseComponent)

MyTeamView.interface = GameConfig.ComponentsConfig.Component
MyTeamView.classPath = "Modules.ModuleTeam.ChildView.MyTeamView"

require "Modules.ModuleTeam.ChildComponent.TeamMemberItem"
local TeamMemberItem = ClassTypes.TeamMemberItem

local UIToggle  = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local teamData = PlayerManager.PlayerDataManager.teamData
local TeamManager = PlayerManager.TeamManager
local ChatManager = PlayerManager.ChatManager
local UIComponentUtil = GameUtil.UIComponentUtil
local public_config = GameWorld.public_config
local TeamDataHelper = GameDataHelper.TeamDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InstanceDataHelper = GameDataHelper.InstanceDataHelper
--local InstanceManager = PlayerManager.InstanceManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function MyTeamView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self:InitCallBack()
	self:InitComps()
end

function MyTeamView:OnEnable()
	if teamData:GetMyTeamMemberInfos() then
		self:UpdateTeamInfo()
		self:SetTargetType()
	end

	self:AddEventListener()
end

function MyTeamView:OnDisable()
	self:RemoveEventListener()
end

function MyTeamView:OnDestroy()
end

function MyTeamView:InitComps()
	self._memberItems = {}
	local itemGOs = {}
	itemGOs[1] = self:FindChildGO("Container_TeamMember/MemberItem")
    for i = 2, 3 do
        local go = self:CopyUIGameObject("Container_TeamMember/MemberItem", "Container_TeamMember")
    	itemGOs[i] = go
    end

    for i=1,3 do
    	local go = itemGOs[i]
    	local item = UIComponentUtil.AddLuaUIComponent(go, TeamMemberItem)
    	--item:SetIndex(i)
    	self._memberItems[i] = item
    end

	local toggle = UIToggle.AddToggle(self.gameObject, "Toggle_AutoReceiveInvitation")
	toggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)
	self._isAutoAccept = true

	self._btnOneClickCall = self:FindChildGO("Button_OneClickCall")
	self._btnCallMember = self:FindChildGO("Button_CallMember")
	self._btnLeaveTeam = self:FindChildGO("Button_LeaveTeam")
	self._btnEnterInst = self:FindChildGO("Button_EnterInst")

	self._csBH:AddClick(self._btnOneClickCall, function() self:OnClickCall() end )
	self._csBH:AddClick(self._btnLeaveTeam, function() self:OnClickLeaveTeam() end )
	self._csBH:AddClick(self._btnCallMember, function() self:OnClickCallMember() end )
	self._csBH:AddClick(self._btnEnterInst, function() self:OnClickEnterInst() end )
	self._inCallCD = false

	self._memberCountList = {}
	for i=1,3 do
		self._memberCountList[i] = self:FindChildGO("Container_MemberCount/Image"..i)
	end

	self._txtExp = self:GetChildComponent("Text_Exp","TextMeshWrapper")

	self._txtLevel = self:GetChildComponent("Text_Level","TextMeshWrapper")
	self._txtTargetName = self:GetChildComponent("Button_Target/Text","TextMeshWrapper")
	self._btnTarget = self:FindChildGO("Button_Target")
	self._btnTargetWrapper = self._btnTarget:GetComponent("ButtonWrapper")
	self._csBH:AddClick(self._btnTarget, function() self:OnClickTarget() end )
end

--一键喊话
function MyTeamView:OnClickCall()
	if self._inCallCD then
		GameManager.SystemInfoManager:ShowClientTip(2514)
	else
		if self._targetId > 0 then
			-- local cfgData = TeamDataHelper.GetTeamTarget(self._targetId)
			local arg = {}
			arg["0"] = LanguageDataHelper.CreateContent(TeamDataHelper.GetSubType(self._targetId))
			arg["1"] =  ChatConfig.LinkTypesMap.Team..","..GameWorld.Player().team_id
			arg["2"] =  TeamDataHelper.GetLevel(self._targetId)
			local str = LanguageDataHelper.CreateContentWithArgs(10027,arg)
			ChatManager.SendChatContent(str,public_config.CHAT_CHANNEL_SEEK_TEAM)
			self._timerId = TimerHeap:AddSecTimer(1, 0, 10, function() self:CallCoolDown() end)
			self._inCallCD = true
		end
	end
end

function MyTeamView:CallCoolDown()
	TimerHeap:DelTimer(self._timerId)
	self._inCallCD = false
end

--召唤队员
function MyTeamView:OnClickCallMember()
	TeamManager:RequestCallAll()
end

--跟随队长(传送到队长身边)
-- function MyTeamView:OnClickFollowCaptain()
-- 	TeamManager:RequestAcceptCall()
-- end

--离开队伍
function MyTeamView:OnClickLeaveTeam()
	TeamManager:TeamLeave()
	GameManager.GUIManager.ClosePanel(PanelsConfig.Team)
end

function MyTeamView:OnClickEnterInst()
	if self._targetId and self._targetId > 0 then
		local mapId =  TeamDataHelper.GetMapId(self._targetId)
		--local instId = InstanceDataHelper.GetInstanceIdByMapId(mapId)
		--LoggerHelper.Error("mapId"..mapId..","..instId)
		TeamManager:RequestTeamStart(mapId)
	end
end

--选组队目标
function MyTeamView:OnClickTarget()
	EventDispatcher:TriggerEvent(GameEvents.TEAM_SHOW_TARGET_VIEW,true,1)
end

--设定文字
function MyTeamView:SetTargetType()
	self._targetId = teamData:GetMyTeam().targetId
	if self._targetId > 0 then
		self:UpdateTargetInfo()
	else
		local readyTargetId = teamData:GetReadyTarget()
		if GameWorld.Player().team_status == 2 and readyTargetId then
			self._targetId = readyTargetId
			self:UpdateTargetInfo()
		else
			--没有目标默认野外挂机
			TeamManager:RequestChangeTargetId(1)
		end
	end
end

function MyTeamView:UpdateTargetInfo()
	-- local cfgData = TeamDataHelper.GetTeamTarget(self._targetId)
	self._txtTargetName.text = LanguageDataHelper.CreateContent(TeamDataHelper.GetSubType(self._targetId))
	self._txtLevel.text = TeamDataHelper.GetLevel(self._targetId)..LanguageDataHelper.CreateContent(69)
end

--是否自动接受申请
function MyTeamView:OnToggleValueChanged(state)
	--LoggerHelper.Error("state"..state)
	self._isAutoAccept = state
	self:RequestSetAutoAcceptApply()
end

function MyTeamView:RequestSetAutoAcceptApply()
	if self._isAutoAccept then
		TeamManager:TeamSetAutoAcceptApply(1)
	else
		TeamManager:TeamSetAutoAcceptApply(0)
	end
end

function MyTeamView:InitCallBack()
 	self._updateTeamInfoCB = function () self:UpdateTeamInfo() end
	self._updateTargetCB = function () self:SetTargetType() end
	self._myTeamUpdate = function() self:UpdateTeamInfo() end
end 

--初始化监听
function MyTeamView:AddEventListener()
	EventDispatcher:AddEventListener(GameEvents.TEAM_REFRESH_TARGET_ID,self._updateTargetCB)
	EventDispatcher:AddEventListener(GameEvents.TEAM_MY_UPDATE,self._updateTeamInfoCB)
	EventDispatcher:AddEventListener(GameEvents.TEAM_MEMBER_MAPINFO_UPDATE, self._myTeamUpdate)
end

--移除监听
function MyTeamView:RemoveEventListener()
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_REFRESH_TARGET_ID,self._updateTargetCB)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_MY_UPDATE,self._updateTeamInfoCB)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_MEMBER_MAPINFO_UPDATE, self._myTeamUpdate)
end

--更新队伍数据
function MyTeamView:UpdateTeamInfo()
	local memberInfos = teamData:GetMyTeamMemberInfos()
	local avartarData = {}
	local memberCount = 0
	for i=1,3 do
		local memberInfo = memberInfos[i]
		self._memberItems[i]:UpdateData(memberInfo)
		if memberInfo then
			memberCount = memberCount + 1
		end
	end

	for i=1,3 do
		self._memberCountList[i]:SetActive(i<= memberCount)
	end

	self:UpdateExpInfo()

	-- self.RenderTextureMember = ShowAvatarManager:GetRawImage()
	-- ShowAvatarManager:UpdateAvatars(avartarData)
	self:UpdateButtonStatus()
end

function MyTeamView:UpdateExpInfo()
	local count = teamData:GetNearbyMemberCount()
	if count > 1 then
		local rate = GlobalParamsHelper.GetParamValue(10)[count]
		self._txtExp.text = "EXP+"..tostring(rate*100).."%"
	else
		self._txtExp.text = "EXP+0%"
	end
end



-- --更新按钮状态team_status:0无队伍/1有队伍/2队长等
function MyTeamView:UpdateButtonStatus()
	if GameWorld.Player().team_status == 2 then
		self._btnOneClickCall:SetActive(true)
		self._btnCallMember:SetActive(true)
		self._btnEnterInst:SetActive(true)
		self._btnTargetWrapper.interactable = true
	else
		self._btnOneClickCall:SetActive(false)
		self._btnCallMember:SetActive(false)
		self._btnEnterInst:SetActive(false)
		self._btnTargetWrapper.interactable = false
	end
end

function MyTeamView:ShowModel()
	for i=1,3 do
    	self._memberItems[i]:ShowModel()
    end
end

function MyTeamView:HideModel()
	for i=1,3 do
    	self._memberItems[i]:HideModel()
    end
end