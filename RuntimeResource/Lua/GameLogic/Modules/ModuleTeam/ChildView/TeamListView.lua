-- TeamListView.lua
require "Modules.ModuleTeam.ChildComponent.TeamInfoItem"
local TeamListView = Class.TeamListView(ClassTypes.BaseComponent)
local TeamInfoItem = ClassTypes.TeamInfoItem
local UIList = ClassTypes.UIList

TeamListView.interface = GameConfig.ComponentsConfig.Component
TeamListView.classPath = "Modules.ModuleTeam.ChildView.TeamListView"

local teamData = PlayerManager.PlayerDataManager.teamData
local TeamManager = PlayerManager.TeamManager
local TeamDataHelper = GameDataHelper.TeamDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InstanceDataHelper = GameDataHelper.InstanceDataHelper

function TeamListView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self:InitCallBack()
	self:InitComps()
end

-- function TeamListView:OnDestroy()
-- 	self._list = nil
-- end

function TeamListView:InitCallBack()
	self._updateTeamsCB = function ()
		self:HandleTeamData()
	end
	self._updateTargetCB = function (targetId) 
		--LoggerHelper.Error("_updateTargetCB")
		self:SetTargetId(targetId) 
	end
	self._teamApplyFailCB = function()
		self:TeamApplyFailCB()
	end
end

function TeamListView:OnEnable()
	--EventDispatcher:AddEventListener(GameEvents.TEAM_NEAR_UPDATE,self._updateTeamsCB)
	EventDispatcher:AddEventListener(GameEvents.TEAM_ALL_UPDATE,self._updateTeamsCB)
	EventDispatcher:AddEventListener(GameEvents.TEAM_REFRESH_TEAM_LIST_TARGET_ID,self._updateTargetCB)
	EventDispatcher:AddEventListener(GameEvents.TEAM_APPLY_FAIL,self._teamApplyFailCB)
	self:GetData()
	self:UpdateButtonState()
end

function TeamListView:OnDisable()
	--EventDispatcher:RemoveEventListener(GameEvents.TEAM_NEAR_UPDATE,self._updateTeamsCB)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_ALL_UPDATE,self._updateTeamsCB)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_REFRESH_TEAM_LIST_TARGET_ID,self._updateTargetCB)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_APPLY_FAIL,self._teamApplyFailCB)
end

function TeamListView:InitComps()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Team")
	self._list = list
	self._scrollView = scrollView 
    self._list:SetItemType(TeamInfoItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
    
	-- local itemClickedCB = 
	-- function(idx)
	-- 	self:OnListItemClicked(idx)
	-- end
	-- self._list:SetItemClickedCB(itemClickedCB)
	self._btnRefreshTeamList = self:FindChildGO("Button_Refresh")
	self._csBH:AddClick(self._btnRefreshTeamList, function() self:GetData() end )

	self._btnCreateTeam = self:FindChildGO("Button_CreateTeam")
	self._csBH:AddClick(self._btnCreateTeam, function() self:OnClickCreate() end )
	self._btnCreateTeamWrapper = self:GetChildComponent("Button_CreateTeam","ButtonWrapper")
	
	self._lookAll = self:FindChildGO("Button_Look_All")
	self._csBH:AddClick(self._lookAll, function() self:OnLookAllTeam() end )

    self._txtTargetName = self:GetChildComponent("Button_Target/Text","TextMeshWrapper")
	self._btnTarget = self:FindChildGO("Button_Target")
	self._btnTargetWrapper = self._btnTarget:GetComponent("ButtonWrapper")
	self._csBH:AddClick(self._btnTarget, function() self:OnClickTarget() end )
    self:SetTargetId(0)--0代表全部队伍，当创建队伍时，0时传1
end

--选组队目标
function TeamListView:OnClickTarget()
	EventDispatcher:TriggerEvent(GameEvents.TEAM_SHOW_TARGET_VIEW,true,2)
end

function TeamListView:OnLookAllTeam()
	self._targetId = 0
	self:GetData()
end

function TeamListView:OnClickCreate()
	if GameWorld.Player().team_status == 0 then
		if self._targetId == 0 then
			self._targetId = 1
		end
		TeamManager:TeamCreate(self._targetId)
	end
end

function TeamListView:UpdateButtonState()
	self._btnCreateTeamWrapper.interactable = GameWorld.Player().team_status <= 0
end

--获取队伍数据
function TeamListView:GetData()
	TeamManager:RequestAllTeams(self._targetId)
end

function TeamListView:SetTargetId(targetId)
	self._targetId = targetId
	if self._targetId > 0 then
		local cfgData = TeamDataHelper.GetTeamTarget(self._targetId)
		self._txtTargetName.text = LanguageDataHelper.CreateContent(cfgData.sub_type)
	else
		self._txtTargetName.text = LanguageDataHelper.CreateContent(144)
	end
	self:UpdateTeams()
end

function TeamListView:HandleTeamData()
	-- local readyTargetId = teamData:GetReadyTarget()
	-- if readyTargetId then
	-- 	self:SetTargetId(readyTargetId)
	-- else
	-- 	self:UpdateTeams()
	-- end
	self:UpdateTeams()
	self:SetTargetId(self._targetId)
end

--更新队伍信息
function TeamListView:UpdateTeams()
	local d = teamData.AllTeamList
	--LoggerHelper.Error("更新队伍信息=====" .. PrintTable:TableToStr(d))
	local listData = {}
	for i=1,#d do
		if self._targetId == 0 then
			table.insert(listData,d[i])
		--筛选符合条件的队伍
		elseif self._targetId == d[i]:GetTargetId() then
			table.insert(listData,d[i])
		end
	end
	self._teamListData = listData
	self._list:SetDataList(listData)
end

function TeamListView:TeamApplyFailCB()
	self:GetData()
end