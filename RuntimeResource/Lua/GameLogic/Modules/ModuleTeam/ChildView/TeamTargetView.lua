-- TeamTargetView.lua
local TeamTargetView = Class.TeamTargetView(ClassTypes.BaseComponent)

TeamTargetView.interface = GameConfig.ComponentsConfig.Component
TeamTargetView.classPath = "Modules.ModuleTeam.ChildView.TeamTargetView"

require "Modules.ModuleTeam.ChildComponent.TeamTargetTypeListItem"
require "Modules.ModuleTeam.ChildComponent.TeamTargetListItem"
local TeamTargetTypeListItem = ClassTypes.TeamTargetTypeListItem
local TeamTargetListItem = ClassTypes.TeamTargetListItem
local UIList = ClassTypes.UIList
local TeamDataHelper = GameDataHelper.TeamDataHelper
local TeamManager = PlayerManager.TeamManager

function TeamTargetView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	--self:InitCallBack()
	self:InitComps()
end

function TeamTargetView:InitComps()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Type")
	self._listType = list
	self._scrollView = scrollView
    self._listType:SetItemType(TeamTargetTypeListItem)
    self._listType:SetPadding(0, 0, 0, 0)
    self._listType:SetGap(5, 5)
    self._listType:SetDirection(UIList.DirectionTopToDown, 1, 100)

    local itemClickedCB = 
	function(idx)
		self:OnTypeListItemClicked(idx)
	end
	self._listType:SetItemClickedCB(itemClickedCB)

	self._allData = TeamDataHelper.GetTeamTargetMap()
	self._allTarget = {}
	local typeData = {}
	--全部目标类型
	typeData[1] = 73408
	--全部
	for k,v in pairs(self._allData) do
		table.insert(typeData,k)
		for i=1,#v do
			table.insert(self._allTarget,v[i])
		end
	end
	self._listType:SetDataList(typeData)

    local scrollView1, list1 = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Target")
	self._listSubType = list1
	self._scrollView1 = scrollView1
    self._listSubType:SetItemType(TeamTargetListItem)
    self._listSubType:SetPadding(0, 0, 0, 0)
    self._listSubType:SetGap(5, 5)
    self._listSubType:SetDirection(UIList.DirectionTopToDown, 1, 100)

    local itemClickedCB1 = 
	function(idx)
		self:OnTargetListItemClicked(idx)
	end
	self._listSubType:SetItemClickedCB(itemClickedCB1)


    self._btnClickClose = self:FindChildGO("Button_Close")
	self._csBH:AddClick(self._btnClickClose, function() self:OnClickClose() end )
end

function TeamTargetView:OnTypeListItemClicked(idx)
	if self._selectedTypeItem then
		self._selectedTypeItem:SetSelected(false)
	end

	self._selectedTypeItem = self._listType:GetItem(idx)
	self._selectedTypeItem:SetSelected(true)

	if idx == 0 then
		self._listSubType:SetDataList(self._allTarget)
	else 
		local targetType = self._listType:GetDataByIndex(idx)

		self._listSubType:SetDataList(self._allData[targetType])
	end
end

function TeamTargetView:OnTargetListItemClicked(idx)
	local targetId = self._listSubType:GetDataByIndex(idx).id
	--我的队伍里点击
	if self._showType == 1 then
		TeamManager:RequestChangeTargetId(targetId)
	--队伍列表里点击
	else
		EventDispatcher:TriggerEvent(GameEvents.TEAM_REFRESH_TEAM_LIST_TARGET_ID,targetId)
	end
	self:OnClickClose()
end

function TeamTargetView:OnEnable()
end

function TeamTargetView:OnDisable()
end

function TeamTargetView:UpdateView(showType)
	self._showType = showType
	self:OnTypeListItemClicked(0)
end

function TeamTargetView:OnClickClose()
	EventDispatcher:TriggerEvent(GameEvents.TEAM_SHOW_TARGET_VIEW,false)
end

-- --队伍目标
-- --队长才能点
-- function TeamTargetView:OnClickShowType()
-- 	if GameWorld.Player().team_status ~= 2 then
-- 		return
-- 	end
-- 	self._isShowType = not self._isShowType
-- 	self._containerType:SetActive(self._isShowType)
-- end

-- function TeamTargetView:UpdateTargetList()
-- 	local targetMap = TeamDataHelper.GetTeamTargetMap()
-- 	local typeDataList = {}
-- 	for type,v in pairs(targetMap) do
-- 		table.insert(typeDataList,type)
-- 	end
-- 	self._typeList:SetDataList(typeDataList)
-- 	--默认选中第一项
-- 	self:OnListTypeItemClicked(0)
-- end

-- --选择目标类型
-- function TeamTargetView:OnListTypeItemClicked(idx)
-- 	if GameWorld.Player().team_status ~= 2 then
-- 		return
-- 	end
-- 	self._selectedType = self._typeList:GetDataByIndex(idx)
-- 	self._isShowType = false
-- 	self._containerType:SetActive(false)
-- 	local subTypeDataList = {}
-- 	local subTypeMap = TeamDataHelper.GetTeamTargetSubTypeList(self._selectedType)
-- 	for subType,v in pairs(subTypeMap) do
-- 		table.insert(subTypeDataList,subType)
-- 	end
	
-- 	self._subTypeList:SetDataList(subTypeDataList)
-- 	--默认选中第一项
-- 	self:OnListSubTypeItemClicked(0)
-- end

-- ------------------------目标子类型-------------------------
-- function TeamTargetView:OnClickShowSubType()
-- 	self._isShowSubType = not self._isShowSubType
-- 	self._containerSubType:SetActive(self._isShowSubType)
-- end

-- --根据副本ID进入此页面处理
-- function TeamTargetView:OnListSubTypeItemClicked(idx)
-- 	self._selectedSubType = self._subTypeList:GetDataByIndex(idx)
-- 	self._isShowSubType = false
-- 	self._containerSubType:SetActive(false)
-- 	self._targetId = TeamDataHelper.GetTeamTargetByType(self._selectedType,self._selectedSubType)
-- 	TeamManager:RequestChangeTargetId(self._targetId)
-- end

-- --根据副本ID进入此页面
-- function TeamTargetView:SetTargetIdByInstance(instId)
-- 	local mapId = InstanceDataHelper.GetInstanceMapId(instId)
-- 	local targetData = TeamDataHelper.GetTeamTargetByMap(mapId)
-- 	self._targetId = targetData.id
-- 	TeamManager:RequestChangeTargetId(self._targetId)
-- end

-- ------------------------目标类型-------------------------

-- --设定文字
-- function TeamTargetView:SetTargetType()
-- 	local targetId = TeamData:GetMyTeam().targetId
-- 	if targetId > 0 then
-- 		local cfgData = TeamDataHelper.GetTeamTarget(targetId)
-- 		self._txtType.text = LanguageDataHelper.CreateContent(cfgData.type)
-- 		self._txtSubType.text = LanguageDataHelper.CreateContent(cfgData.sub_type)
-- 	end
-- end