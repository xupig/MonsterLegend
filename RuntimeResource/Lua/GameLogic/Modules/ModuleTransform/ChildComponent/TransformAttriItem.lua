local TransformAttriItem = Class.TransformAttriItem(ClassTypes.BaseLuaUIComponent)

TransformAttriItem.interface = GameConfig.ComponentsConfig.Component
TransformAttriItem.classPath = "Modules.ModuleTransform.ChildComponent.TransformAttriItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local AttriDataHelper = GameDataHelper.AttriDataHelper

function TransformAttriItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function TransformAttriItem:OnDestroy()
    self.data = nil
end

function TransformAttriItem:InitView()
    self._nameText = self:GetChildComponent("Text_AttrName", "TextMeshWrapper")
    self._valueText = self:GetChildComponent("Text_AttrValue", "TextMeshWrapper")
    self._addValueText = self:GetChildComponent("Text_AddAttrValue", "TextMeshWrapper")
    self._imgAdd = self:FindChildGO("Image_Add")
end

--data: {attriId, curValue, delta}
function TransformAttriItem:OnRefreshData(data)
    self.data = data
    self._nameText.text = AttriDataHelper:GetName(data[1])
    self._valueText.text = AttriDataHelper:ConvertAttriValue(data[1],data[2])
    if data[3] then
        self._addValueText.text = AttriDataHelper:ConvertAttriValue(data[1],data[3])
        self._imgAdd:SetActive(true)
    else
        self._addValueText.text = ""
        self._imgAdd:SetActive(false)
    end
end