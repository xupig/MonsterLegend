-- TreasureTransformListItem.lua
--宝物幻化Item类
local TreasureTransformListItem = Class.TreasureTransformListItem(ClassTypes.UIListItem)

TreasureTransformListItem.interface = GameConfig.ComponentsConfig.PointableComponent
TreasureTransformListItem.classPath = "Modules.ModuleTransform.ChildComponent.TreasureTransformListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig
local TransformManager = PlayerManager.TransformManager
local TransformDataHelper = GameDataHelper.TransformDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

function TreasureTransformListItem:Awake()
	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected:SetActive(false)
	self._txtOnShowGO = self:FindChildGO("Text_OnShow")
	self._txtOpenTip = self:GetChildComponent("Text_OpenTip",'TextMeshWrapper')

	self._imgRedPoint = self:FindChildGO("Image_RedPoint")
	self._imgRedPoint:SetActive(false)

	local parent = self:FindChildGO("Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    local arg = {}
    arg.noAnimFrame = true
    self._icon:SetValues(arg)

    self._imgStarBg = self:FindChildGO("Image_StarBg")
	self._imgStar = self:FindChildGO("Image_Star")
	self._starRectTransform = self._imgStar:GetComponent(typeof(UnityEngine.RectTransform))
end

function TreasureTransformListItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end

--override
function TreasureTransformListItem:OnRefreshData(data)
	self._cfgData = data[1]
	self._systemType = data[2]
	self:UpdateData()
end

function TreasureTransformListItem:UpdateData()
	local treasureInfo = TransformManager:GetTreasureActivateData(self._systemType)

	local vocationGroup = math.floor(GameWorld.Player().vocation/100)
	local itemId = self._cfgData.activate_num[vocationGroup][1]
	self._txtName.text = LanguageDataHelper.CreateContent(self._cfgData.name[vocationGroup])
	local star = treasureInfo[self._cfgData.id]
	if star then
		self._icon:SetItem(itemId, 0, 0, false, 1)
		--GameWorld.AddIcon(self._containerIcon,icon,0,false,1)
		self._txtOpenTip.text = ""
		self._imgStar:SetActive(false)
		self._starRectTransform.sizeDelta = Vector2(star*40,40)
		self._imgStarBg:SetActive(true)
		self._imgStar:SetActive(true)
	else
		self._icon:SetItem(itemId, 0, 0, false, 1)
		--GameWorld.AddIcon(self._containerIcon,icon,0,false,0)
		if self._cfgData.activate_limit then
			local vocationGroup = math.floor(GameWorld.Player().vocation/100)
			local curVocationChange = GameWorld.Player().vocation%100
			local vocationChange = self._cfgData.activate_limit[vocationGroup] - vocationGroup*100
			local cn = StringStyleUtil.GetChineseNumber(vocationChange)
			if curVocationChange >= vocationChange then
				self._txtOpenTip.text = LanguageDataHelper.CreateContent(536)
			else
				self._txtOpenTip.text = LanguageDataHelper.CreateContentWithArgs(73101,{["0"] = cn})
			end
		else
			--获取的提示
			if self._cfgData.unlock_chinese then
				self._txtOpenTip.text = LanguageDataHelper.CreateContent(self._cfgData.unlock_chinese)
			else
				self._txtOpenTip.text = LanguageDataHelper.CreateContent(536)
			end
		end
		self._imgStarBg:SetActive(false)
		self._imgStar:SetActive(false)
	end

	self:UpdateOnShow()
	self:UpdateRedPoint()
end

function TreasureTransformListItem:UpdateOnShow()
	if self._txtOnShowGO and self._cfgData then
		self._txtOnShowGO:SetActive(TransformManager:CheckTransformShow(self._systemType,self._cfgData.id))
	end
end

function TreasureTransformListItem:UpdateRedPoint()
	local treasureInfo = TransformManager:GetTreasureActivateData(self._systemType)
	local vocationGroup = math.floor(GameWorld.Player().vocation/100)
	local cfg = self._cfgData
	local star = treasureInfo[cfg.id] or 0
	local attriCfg = TransformDataHelper:GetTreasureStarCfg(self._systemType,cfg.id,star)
	local costItem
	local needNum
	local vocationLimit = true
	if star == 0 then
		if cfg.activate_limit then
			local needVocation = cfg.activate_limit[vocationGroup]
			if GameWorld.Player().vocation < needVocation then
				vocationLimit = false
			end
		end
	end
	if star <  5 then
		costItem = attriCfg.star_lvup_cost[vocationGroup]
		needNum = costItem[2]
	else
		costItem = cfg.activate_num[vocationGroup]
		needNum = 1
	end

	local itemId = costItem[1]
	local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
	if bagNum >= needNum and vocationLimit then
		self._imgRedPoint:SetActive(true)
	else
		self._imgRedPoint:SetActive(false)
	end
end