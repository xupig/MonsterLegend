local TransformDecomposeListItem = Class.TransformDecomposeListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
TransformDecomposeListItem.interface = GameConfig.ComponentsConfig.Component
TransformDecomposeListItem.classPath = "Modules.ModuleTransform.ChildComponent.TransformDecomposeListItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil

function TransformDecomposeListItem:Awake()
    self._base.Awake(self)
    self.data = nil
    --self._select = false
    self:InitView()
end

function TransformDecomposeListItem:OnDestroy()
    self.data = nil
end

--override
function TransformDecomposeListItem:OnRefreshData(data)
    self.data = data
    local count = 0
    --兼容空格子
    if data.cfg_id ~= nil then
        count = self.data:GetCount()
    end
    self:Reset()
    self._icon:SetItem(data.cfg_id, count)
    -- if self._select then
    --     self:Selected()
    -- end
end

function TransformDecomposeListItem:InitView()
    local parent = self:FindChildGO("Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._selectImage = self:FindChildGO("Image_Selected")
end

function TransformDecomposeListItem:ChangeState()
    if self.data.cfg_id == nil then
        return
    end
    self._select = not self._select
    self._selectImage:SetActive(self._select)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Transform_Selected_Decompose_Item, self.data, self._select)
end

function TransformDecomposeListItem:Selected()
    self._select = true
    self._selectImage:SetActive(self._select)
end

function TransformDecomposeListItem:Reset()
    self._select = false
    self._selectImage:SetActive(self._select)
end