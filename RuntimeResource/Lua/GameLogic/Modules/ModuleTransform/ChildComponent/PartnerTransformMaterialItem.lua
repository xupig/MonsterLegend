local PartnerTransformMaterialItem = Class.PartnerTransformMaterialItem(ClassTypes.BaseLuaUIComponent)

PartnerTransformMaterialItem.interface = GameConfig.ComponentsConfig.PointableComponent
PartnerTransformMaterialItem.classPath = "Modules.ModuleTransform.ChildComponent.PartnerTransformMaterialItem"
require "UIComponent.Extend.ItemManager"
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemManager = ClassTypes.ItemManager
local bagData = PlayerManager.PlayerDataManager.bagData
local GUIManager = GameManager.GUIManager
local PartnerManager = PlayerManager.PartnerManager
local TipsManager = GameManager.TipsManager

function PartnerTransformMaterialItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self.canUse = false
    self.isSelected = false
    self:InitView()
end

function PartnerTransformMaterialItem:OnDestroy()
    self.data = nil
end

function PartnerTransformMaterialItem:InitView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._countText = self:GetChildComponent("Text_Num", "TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._itemManager = ItemManager()
    self._itemIcon = self._itemManager:GetLuaUIComponent(nil, self._iconContainer)

    self._imageSelected = self:FindChildGO("Image_Selected")
    self._nameGo = self:FindChildGO("Text_Name")
end

function PartnerTransformMaterialItem:InitIndex(index)
    self.index = index
end

--data: {itemId, blessValue}
function PartnerTransformMaterialItem:OnRefreshData(data)
    self.data = data
    self:RefreshView()
end

function PartnerTransformMaterialItem:RefreshView()
    local itemId = self.data[1]
    self._itemIcon:SetItem(itemId)
    self._nameText.text = ItemDataHelper.GetItemNameWithColor(itemId)
    self:UpdateCount()
end

function PartnerTransformMaterialItem:UpdateCount()
    local itemId = self.data[1]
    local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    self._countText.text = bagNum
    return bagNum
end

function PartnerTransformMaterialItem:ShowSelected(state)
    self._imageSelected:SetActive(state)
    self._nameGo:SetActive(state)
    self.isSelected = state
    -- if state then
    --     local itemId = self.data[1]
    --     PartnerManager:SetStarUpSelectedItem(itemId)
    -- end
end

function PartnerTransformMaterialItem:OnPointerDown(pointerEventData)
    if self.isSelected then
        TipsManager:ShowItemTipsById(self.data[1])
    end
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Selected_Transform_Bless_Item, self.index)
end

function PartnerTransformMaterialItem:OnPointerUp(pointerEventData)

end