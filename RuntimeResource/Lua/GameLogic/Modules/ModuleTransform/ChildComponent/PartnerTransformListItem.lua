-- PartnerTransformListItem.lua
--宝物幻化Item类
local PartnerTransformListItem = Class.PartnerTransformListItem(ClassTypes.UIListItem)

PartnerTransformListItem.interface = GameConfig.ComponentsConfig.PointableComponent
PartnerTransformListItem.classPath = "Modules.ModuleTransform.ChildComponent.PartnerTransformListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig
local TransformManager = PlayerManager.TransformManager
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

function PartnerTransformListItem:Awake()
	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected:SetActive(false)
	self._txtOnShowGO = self:FindChildGO("Text_OnShow")
	self._txtGrade = self:GetChildComponent("Text_Grade",'TextMeshWrapper')

	self._imgRedPoint = self:FindChildGO("Image_RedPoint")
	self._imgRedPoint:SetActive(false)

	local parent = self:FindChildGO("Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    local arg = {}
    arg.noAnimFrame = true
    self._icon:SetValues(arg)
end

function PartnerTransformListItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end

--override
function PartnerTransformListItem:OnRefreshData(data)
	self._cfgData = data[1]
	self._systemType = data[2]
	self:UpdateData()
end

function PartnerTransformListItem:UpdateData()
	local parnterInfoList = TransformManager:GetPartnerTransformData(self._systemType)
	
	local vocationGroup = math.floor(GameWorld.Player().vocation/100)
	local itemId = self._cfgData.activate_num[vocationGroup][1]
	self._txtName.text = LanguageDataHelper.CreateContent(self._cfgData.name[vocationGroup])
	local parnterInfo = parnterInfoList[self._cfgData.id]

	if parnterInfo then
		local grade = parnterInfo[public_config.PET_ILLUSION_KEY_GRADE]
		self._icon:SetItem(itemId, 0, 0, false, 1)
		--GameWorld.AddIcon(self._containerIcon,icon,0,false,1)
		local colorId = ItemConfig.ItemAttriValue
		local str = StringStyleUtil.GetChineseNumber(grade)..LanguageDataHelper.CreateContent(305)
		self._txtGrade.text = StringStyleUtil.GetColorStringWithColorId(str,colorId)
	else
		self._icon:SetItem(itemId, 0, 0, false, 1)
		--GameWorld.AddIcon(self._containerIcon,icon,0,false,0)
		--获取的提示
		if self._cfgData.unlock_chinese then
			self._txtGrade.text = LanguageDataHelper.CreateContent(self._cfgData.unlock_chinese)
		else
			self._txtGrade.text = LanguageDataHelper.CreateContent(536)
		end
	end

	self:UpdateOnShow()
	self:UpdateRedPoint()
end

function PartnerTransformListItem:UpdateOnShow()
	if self._txtOnShowGO and self._cfgData then
		self._txtOnShowGO:SetActive(TransformManager:CheckTransformShow(self._systemType,self._cfgData.id))
	end
end

function PartnerTransformListItem:UpdateRedPoint()
	local state = false
	local partnerInfoList = TransformManager:GetPartnerTransformData(self._systemType)
	local cfg = self._cfgData
	local partnerInfo = partnerInfoList[cfg.id]
	--未激活
	if partnerInfo == nil then
		local vocationGroup = math.floor(GameWorld.Player().vocation/100)
		local costItem = cfg.activate_num[vocationGroup]
		local itemId = costItem[1]
		local needNum = costItem[2]
		local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
		if bagNum >= needNum then
			state = true
		end
	-- --已激活
	-- else
	-- 	local blessItemData = TransformManager:GetBlessItemData(self._systemType)
 --        for i=1,#blessItemData do
 --        	local itemId = blessItemData[i][1]
 --        	local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
 --        	if bagNum >= 1 then
	-- 			state = true
	-- 		end
 --        end
	end
	self._imgRedPoint:SetActive(state)
end