local TransformSkillItem = Class.TransformSkillItem(ClassTypes.BaseLuaUIComponent)

TransformSkillItem.interface = GameConfig.ComponentsConfig.PointableComponent
TransformSkillItem.classPath = "Modules.ModuleTransform.ChildComponent.TransformSkillItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local SkillDataHelper = GameDataHelper.SkillDataHelper
local TipsManager = GameManager.TipsManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function TransformSkillItem:Awake()
    self._base.Awake(self)
    self._isLock = false
    self:InitView()
end

function TransformSkillItem:InitView()
    self._iconContainer = self:FindChildGO("Container_Icon")
end

--设置技能tips类型:宠物5，坐骑6
function TransformSkillItem:SetType(type)
    self._type = type
end

--data:{skill,lock, grade} lock:0解锁 1：锁定
function TransformSkillItem:OnRefreshData(data)
    self.data = data
    self:RefreshView()
end

function TransformSkillItem:RefreshView()
    if self.data.lock == 1 then
        self._isLock = true
    else
        self._isLock = false
    end
    local icon = SkillDataHelper.GetSkillIcon(self.data.skill)
    if self._isLock then
        GameWorld.AddIcon(self._iconContainer, icon, 12)
    else
        GameWorld.AddIcon(self._iconContainer, icon)
    end
end

function TransformSkillItem:OnPointerDown(pointerEventData)
    local conditionLevel
    if self._isLock then
        conditionLevel = self.data.grade
    end
    TipsManager:ShowSkillTips(self.data.skill, conditionLevel,self._type,LanguageDataHelper.CreateContent(4509))
end

function TransformSkillItem:OnPointerUp(pointerEventData)
    if self._isLock then
        return
    end
end