TransformModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function TransformModule.Init()
    GUIManager.AddPanel(PanelsConfig.TreasureTransform,"Panel_TreasureTransform",GUILayer.LayerUIPanel,"Modules.ModuleTransform.TreasureTransformPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.PartnerTransform,"Panel_PartnerTransform",GUILayer.LayerUIPanel,"Modules.ModuleTransform.PartnerTransformPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return TransformModule