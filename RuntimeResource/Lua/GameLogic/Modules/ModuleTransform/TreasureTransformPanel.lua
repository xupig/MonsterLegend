local TreasureTransformPanel = Class.TreasureTransformPanel(ClassTypes.BasePanel)

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager

require "Modules.ModuleTransform.ChildView.TreasureTransformView"
require "Modules.ModuleTransform.ChildView.TransformDecomposeView"

local TreasureTransformView = ClassTypes.TreasureTransformView
local TransformDecomposeView = ClassTypes.TransformDecomposeView

function TreasureTransformPanel:Awake()
	self:InitCallBack()
	self:InitComps()
end

function TreasureTransformPanel:InitCallBack()

	self._tabClickCB = function (index)
		self:OnTabClick(index)
	end

	self._showDecomposeCb = function (isShow)
		self:ShowDecomposeView(isShow)
	end

	self:SetCloseCallBack(function ()
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SHOW_TREASURE_MODEL)
		GUIManager.ClosePanel(GameConfig.PanelsConfig.TreasureTransform)
	end)
end

--override 
function TreasureTransformPanel:OnShow(data)
	--外部传入道具id
	if data and data.args then
		self._srcItemId = data.args[1]
	else
		self._srcItemId = nil
	end
	self._iconBg = self:FindChildGO('Image_Bg')
    GameWorld.AddIcon(self._iconBg,3317)
	self:UpdateFunctionOpen()
	self:OnShowSelectTab(data)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_SHOW_TRANSFORM_DECOMPOSE,self._showDecomposeCb)
end

--override
function TreasureTransformPanel:OnClose()
    self._iconBg = nil
    self._lastIndex = nil
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_SHOW_TRANSFORM_DECOMPOSE,self._showDecomposeCb)
end

function TreasureTransformPanel:InitComps()
	self._transformView = self:AddChildLuaUIComponent("Container_Transform", TreasureTransformView)
	self._decomposeView = self:AddChildLuaUIComponent("Container_Decompose", TransformDecomposeView)

	self._decomposeView:SetActive(false)
    self:SetTabClickCallBack(self._tabClickCB)
end

--tab点击处理
function TreasureTransformPanel:OnTabClick(index)
	if self._lastIndex and self._lastIndex == index then
		return
	end
    self._transformView:UpdateView(index,self._srcItemId)
    self._lastIndex = index
    self._srcItemId = nil
end

function TreasureTransformPanel:ShowDecomposeView(isShow)
	if isShow then
		self._decomposeView:SetActive(true)
		self._decomposeView:UpdateView(self._lastIndex)
		self._transformView:HideModel()
	else
		self._decomposeView:SetActive(false)
		self._transformView:ShowModel()
	end
end

-- -- 0 没有  1 普通底板
function TreasureTransformPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function TreasureTransformPanel:BaseShowModel()
	self._transformView:ShowModel()
end

function TreasureTransformPanel:BaseHideModel()
	self._transformView:HideModel()
end