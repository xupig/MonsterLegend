local PartnerTransformPanel = Class.PartnerTransformPanel(ClassTypes.BasePanel)

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager

require "Modules.ModuleTransform.ChildView.PartnerTransformView"
require "Modules.ModuleTransform.ChildView.TransformDecomposeView"

local PartnerTransformView = ClassTypes.PartnerTransformView
local TransformDecomposeView = ClassTypes.TransformDecomposeView

function PartnerTransformPanel:Awake()
	self:InitCallBack()
	self:InitComps()
end

function PartnerTransformPanel:InitCallBack()
	self._tabClickCB = function (index)
		self:OnTabClick(index)
	end

	self._showDecomposeCb = function (isShow)
		self:ShowDecomposeView(isShow)
	end

	self:SetCloseCallBack(function ()
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SHOW_PARTNER_MODEL)
		GUIManager.ClosePanel(GameConfig.PanelsConfig.PartnerTransform)
	end)
end

--override 
function PartnerTransformPanel:OnShow(data)
	--外部传入道具id
	if data and data.args then
		self._srcItemId = data.args[1]
	else
		self._srcItemId = nil
	end
	self._iconBg = self:FindChildGO('Image_Bg')
    GameWorld.AddIcon(self._iconBg,3317)
	self:UpdateFunctionOpen()
	self:OnShowSelectTab(data)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_SHOW_TRANSFORM_DECOMPOSE,self._showDecomposeCb)
end

--override
function PartnerTransformPanel:OnClose()
    self._iconBg = nil
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_SHOW_TRANSFORM_DECOMPOSE,self._showDecomposeCb)
end

function PartnerTransformPanel:InitComps()
	self._transformView = self:AddChildLuaUIComponent("Container_Transform", PartnerTransformView)
	self._decomposeView = self:AddChildLuaUIComponent("Container_Decompose", TransformDecomposeView)

	self._decomposeView:SetActive(false)
    self:SetTabClickCallBack(self._tabClickCB)
end

--tab点击处理
function PartnerTransformPanel:OnTabClick(index)
	-- if self._lastIndex and self._lastIndex == index then
	-- 	return
	-- end
    self._transformView:UpdateView(index+4,self._srcItemId)
    self._lastIndex = index
    self._srcItemId = nil
end

function PartnerTransformPanel:ShowDecomposeView(isShow)
	if isShow then
		self._decomposeView:SetActive(true)
		self._decomposeView:UpdateView(self._lastIndex+4)
		self._transformView:HideModel()
	else
		self._decomposeView:SetActive(false)
		self._transformView:ShowModel()
	end
end

-- -- 0 没有  1 普通底板
function PartnerTransformPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function PartnerTransformPanel:BaseShowModel()
	self._transformView:ShowModel()
end

function PartnerTransformPanel:BaseHideModel()
	self._transformView:HideModel()
end
