local TransformDecomposeView = Class.TransformDecomposeView(ClassTypes.BaseLuaUIComponent)

TransformDecomposeView.interface = GameConfig.ComponentsConfig.Component
TransformDecomposeView.classPath = "Modules.ModuleTransform.ChildView.TransformDecomposeView"
require "Modules.ModuleTransform.ChildComponent.TransformAttriItem"
require "Modules.ModuleTransform.ChildComponent.TransformDecomposeListItem"
require "UIComponent.Extend.AttriCom"

local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local XArtNumber = GameMain.XArtNumber
local TransformDataHelper = GameDataHelper.TransformDataHelper
local AttriCom = ClassTypes.AttriCom
local UIProgressBar = ClassTypes.UIProgressBar
local UIList = ClassTypes.UIList
local TransformAttriItem = ClassTypes.TransformAttriItem
local TransformManager = PlayerManager.TransformManager
local TransformDecomposeListItem = ClassTypes.TransformDecomposeListItem
local AttriDataHelper = GameDataHelper.AttriDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig
local UIToggle = ClassTypes.UIToggle
local public_config = GameWorld.public_config

local Max_Attr_Count = 4

function TransformDecomposeView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitCallback()
    self:InitView()
end

function TransformDecomposeView:InitCallback()
    self._refreshItemCb = function (itemData, selectState)
        self:RefreshSelectedItem(itemData, selectState)
    end

    self._updateViewCb = function ()
        self:UpdateView(self._systemType)
    end
end

function TransformDecomposeView:OnEnable()
	EventDispatcher:AddEventListener(GameEvents.Refresh_Transform_Selected_Decompose_Item, self._refreshItemCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_DCOMPOSE_COMPLETE, self._updateViewCb)
    --EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_UPLEVEL_COMPLETE, self._upLevelCb)
end

function TransformDecomposeView:OnDisable()
	self._lastLevel = nil
    self._title = nil
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Transform_Selected_Decompose_Item, self._refreshItemCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_DCOMPOSE_COMPLETE, self._updateViewCb)
    --EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_UPLEVEL_COMPLETE, self._upLevelCb)
end

function TransformDecomposeView:InitView()
	self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self._pbLevel = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar_Exp")
    self._pbLevel:SetProgress(0)
    self._pbLevel:ShowTweenAnimate(true)
    self._pbLevel:SetMutipleTween(true)
    self._pbLevel:SetIsResetToZero(false)
    self._pbLevel:SetTweenTime(0.2)

    self._attrBaseList = {}
    for i=1, Max_Attr_Count do
        self._attrBaseList[i] = self:AddChildLuaUIComponent("Container_AttrsBase/Container_Item"..i, AttriCom)
    end

    self._attrAdditionList = {}
    for i=1, Max_Attr_Count do
        self._attrAdditionList[i] = self:AddChildLuaUIComponent("Container_AttrsAddition/Container_Item"..i, TransformAttriItem)
    end

    self._txtTitle = self:GetChildComponent("Text_Title",'TextMeshWrapper')
    self._txtLevel = self:GetChildComponent("Text_Level",'TextMeshWrapper')

    self._btnDecompose = self:FindChildGO("Button_Decompose")
    self._csBH:AddClick(self._btnDecompose,function () self:OnDecomposeClick() end)
    self._decomposeRedPoint = self:FindChildGO("Button_Decompose/Image_Tip")
    self._decomposeRedPoint:SetActive(false)

    -- self._btnLevelUp = self:FindChildGO("Button_LevelUp")
    -- self._csBH:AddClick(self._btnLevelUp,function () self:OnLevelUpClick() end)
    -- self._levelUpRedPoint = self:FindChildGO("Button_LevelUp/Image_Tip")
    -- self._levelUpRedPoint:SetActive(false)

    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose,function () self:OnCloseClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(TransformDecomposeListItem)
    self._list:SetPadding(40, 0, 0, 20)
    self._list:SetGap(15, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, 4, 100)
    local itemClickedCB = 
    function(idx)
        self:OnListItemClicked(idx)
    end
    self._list:SetItemClickedCB(itemClickedCB)
    self._selectedItemList = {}

    local toggle = UIToggle.AddToggle(self.gameObject,"Toggle_All")
    toggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)
    toggle:SetIsOn(false)
end

function TransformDecomposeView:UpdateView(systemType)
    self._systemType = systemType

    if self._title == nil then
        self._title = TransformManager:GetSystemCn(systemType)
        self._txtTitle.text = self._title..LanguageDataHelper.CreateContent(229)
    end

    local exp
    local maxExp
    --法宝
	if systemType < 5 then
        self._level = TransformManager:GetTreasureLevel(systemType)
        exp = TransformManager:GetTreasureExp(systemType)
    --伙伴
    elseif systemType == public_config.TREASURE_TYPE_PET then
        self._level = GameWorld.Player().illusion_pet_level or 0
        exp = GameWorld.Player().illusion_pet_exp or 0
    elseif systemType == public_config.TREASURE_TYPE_HORSE then
        self._level = GameWorld.Player().illusion_horse_level or 0
        exp = GameWorld.Player().illusion_horse_exp or 0
    end
    --LoggerHelper.Error(systemType.."/"..self._level)
    local levelUpCfg = TransformDataHelper:GetTransformLevelUpCfg(systemType,self._level)
    local attrisList = levelUpCfg.attri_percent_up
    local nextCfg = TransformDataHelper:GetTransformLevelUpCfg(systemType,self._level+1)
    local nextAttrisList
    if nextCfg then
        nextAttrisList = nextCfg.attri_percent_up
    end

    maxExp = levelUpCfg.levelup_cost or 0
    local dLevel = 0
    local shouldUpdateAttri = true
    if self._lastLevel then
        dLevel = self._level - self._lastLevel
        if dLevel == 0 then
            shouldUpdateAttri = false
        end
    end
    self._txtLevel.text = "Lv."..self._level
    self._lastLevel = self._level

    local progress = exp/maxExp + dLevel
    --LoggerHelper.Error("progress"..progress)
    -- if useMultiExp then
    --     if exp > maxExp then
    --         progress = math.min(0.99,progress)
    --     end
    -- else
    --     progress = math.min(0.99,progress)
    -- end
    --LoggerHelper.Error("progressaaaaaaaaa"..progress)
    self._pbLevel:SetProgress(progress)
    self._expStr = exp.."/"..maxExp

    self:UpdateComposeList()

    if shouldUpdateAttri == false then
        return
    end

    self:UpdateBaseAttri()
    for i=1, Max_Attr_Count do
        if attrisList[i] then
            self._attrAdditionList[i]:SetActive(true)
            local attriData = {}
            attriData[1] = attrisList[i][1]
            attriData[2] = attrisList[i][2]
            if nextAttrisList then
                attriData[3] = nextAttrisList[i][2] - attriData[2]
            end
            self._attrAdditionList[i]:OnRefreshData(attriData)
        else
            self._attrAdditionList[i]:SetActive(false)
        end
    end
end

function TransformDecomposeView:UpdateBaseAttri()
    --激活过的幻化信息
    local activateInfo
    if self._systemType <= 4 then 
        activateInfo = TransformManager:GetTreasureActivateData(self._systemType)
    elseif self._systemType == public_config.TREASURE_TYPE_PET then
        activateInfo = GameWorld.Player().illusion_pet_info
    elseif self._systemType == public_config.TREASURE_TYPE_HORSE then
        activateInfo = GameWorld.Player().illusion_horse_info
    end

    local t = {}
    local attriPowerList = {}
    --宝物
    if self._systemType <= 4 then 
        for transformId,star in pairs(activateInfo) do
            local attriCfg = TransformDataHelper:GetTreasureStarCfg(self._systemType,transformId,star)
            --只取三条属性
            for i=1,3 do
                local key = attriCfg.attri[i][1]
                local value = attriCfg.attri[i][2]
                if t[i] == nil then
                    t[i] = {key,value}
                else
                    t[i][2] = t[i][2] + value
                end
            end
        end
        for i=1,#t do
            table.insert(attriPowerList, {["attri"]=t[i][1], ["value"]=t[i][2]})
        end
    --伙伴
    else
        for transformId,partnerData in pairs(activateInfo) do
            local grade = partnerData[public_config.PET_ILLUSION_KEY_GRADE]
            local attriCfg = TransformDataHelper:GetPartnerGradeCfg(self._systemType,transformId,grade)
            --只取三条属性
            for i=1,3 do
                local key = attriCfg.attri[i][1]
                local value = attriCfg.attri[i][2]
                if t[i] == nil then
                    t[i] = {key,value}
                else
                    t[i][2] = t[i][2] + value
                end
            end
        end
        for i=1,#t do
            table.insert(attriPowerList, {["attri"]=t[i][1], ["value"]=t[i][2]})
        end
    end
    
    for i=1, Max_Attr_Count do
        if t[i] then
            self._attrBaseList[i]:SetActive(true)
            local keyName = AttriDataHelper:GetName(t[i][1])
            local value = AttriDataHelper:ConvertAttriValue(t[i][1],t[i][2])
            self._attrBaseList[i]:UpdateItem(keyName,value)
        else
            self._attrBaseList[i]:SetActive(false)
        end
    end

    local fightPower = BaseUtil.CalculateFightPower(attriPowerList, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)
end

-- function TransformDecomposeView:OnLevelUpClick()
--     TransformManager:RequestTransformUpLevel(self._systemType)
-- end

--分解
function TransformDecomposeView:OnDecomposeClick()
    local posList = {}
    for k,_ in pairs(self._selectedItemList) do
        table.insert(posList, k)
    end
    if #posList <= 0 then
        return
    end
    --LoggerHelper.Error("OnDecomposeClick111")
    local params = tostring(self._systemType)..","
    for i=1, #posList-1 do
        params = params .. posList[i]..","
    end

    params = params .. posList[#posList]
    TransformManager:RequestTransformDecompose(params)
end

function TransformDecomposeView:UpdateComposeList()
    self._selectedItemList = {}
    local composeItemIds,decomposeListData = TransformManager:GetComposeList(self._systemType)
    self._composeItemIds = composeItemIds
    self._listData = decomposeListData
    for i=#self._listData+1,12 do
        self._listData[i] = {}
    end
    self._list:SetDataList(self._listData)
    self:ShowAllComposeGet()
end

function TransformDecomposeView:OnToggleValueChanged(state)
    if self._listData == nil then
        return
    end
    if state then
        self._selectedItemList = {}
        for i=1,#self._listData do
            local bagPos = self._listData[i].bagPos
            if bagPos then
                self._selectedItemList[bagPos] = self._listData[i]
                self._list:GetItem(i-1):Selected()
            else
                self._list:GetItem(i-1):Reset()
            end
        end
    else
        self._selectedItemList = {}
        for i=1,#self._listData do
            self._list:GetItem(i-1):Reset()
        end
    end
    self:ShowAllComposeGet()
end

function TransformDecomposeView:OnListItemClicked(idx)
    local item = self._list:GetItem(idx)
    item:ChangeState()
end

function TransformDecomposeView:RefreshSelectedItem(itemData, selectState)
    if selectState then
        self._selectedItemList[itemData.bagPos] = itemData
    else
        self._selectedItemList[itemData.bagPos] = nil
    end
    self:ShowAllComposeGet()
end

function TransformDecomposeView:ShowAllComposeGet()
    local sum = 0
    for k,v in pairs(self._selectedItemList) do
        local itemId = v.cfg_id
        local count = self._composeItemIds[itemId]*v:GetCount()
        sum = sum + count
    end

    local str = self._expStr
    if sum > 0 then
        str = str.."+"..StringStyleUtil.GetColorStringWithColorId(tostring(sum),ItemConfig.ItemAttriValue)
    end
    self._pbLevel:SetProgressText(str)
end

-- function TransformDecomposeView:ShowLevelUpPoint(state)
--     self._levelUpRedPoint:SetActive(state)
-- end

function TransformDecomposeView:ShowDecomposeRedPoint(state)
    self._decomposeRedPoint:SetActive(state)
end

function TransformDecomposeView:OnCloseClick()
    EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SHOW_TRANSFORM_DECOMPOSE,false)
end