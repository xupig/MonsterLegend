local PartnerTransformView = Class.PartnerTransformView(ClassTypes.BaseLuaUIComponent)

PartnerTransformView.interface = GameConfig.ComponentsConfig.Component
PartnerTransformView.classPath = "Modules.ModuleTransform.ChildView.PartnerTransformView"

require "Modules.ModuleTransform.ChildComponent.PartnerTransformListItem"
require "Modules.ModuleTransform.ChildComponent.PartnerTransformMaterialItem"
require "Modules.ModuleTransform.ChildComponent.TransformAttriItem"
require "Modules.ModuleTransform.ChildComponent.TransformSkillItem"
require "Modules.ModuleTransform.ChildView.PartnerTransformExpTipsView"

require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local UIList = ClassTypes.UIList
local UIProgressBar = ClassTypes.UIProgressBar
local PartnerTransformListItem = ClassTypes.PartnerTransformListItem
local PartnerTransformMaterialItem = ClassTypes.PartnerTransformMaterialItem
local TransformAttriItem = ClassTypes.TransformAttriItem
local TransformSkillItem = ClassTypes.TransformSkillItem

local XArtNumber = GameMain.XArtNumber
local TransformDataHelper = GameDataHelper.TransformDataHelper
local EntityConfig = GameConfig.EntityConfig
local BaseUtil = GameUtil.BaseUtil
local ActorModelComponent = GameMain.ActorModelComponent
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TransformManager = PlayerManager.TransformManager
local bagData = PlayerManager.PlayerDataManager.bagData
local UIParticle = ClassTypes.UIParticle
local PartnerTransformExpTipsView = ClassTypes.PartnerTransformExpTipsView
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

local Max_Attr_Count = 4
local Max_Material_Count = 2
local Max_Skill_Count = 3

function PartnerTransformView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitCallback()
    self:InitView()
end

function PartnerTransformView:InitCallback()

    self._activeCompleteCb = function ()
        if self._selectedPartnerItem then
            self._selectedPartnerItem:UpdateData()
        end
        self:UpdateSelectedPartner()
    end


    self._updatePartnerCb = function ()
        if self._selectedPartnerItem then
            self._selectedPartnerItem:UpdateData()
        end
        self:UpdateSelectedPartner()
    end

    self._upgradeCompleteCb = function (args)
        self:UpateExpTip(args)
        self:UpdatePartnerInfo()
    end

    self._refreshMatItemCb = function (index)
        self:RefreshSelectedBlessItem(index)
    end

    self._updateOnShowCb =function ()
        self:UpdateListOnShow()
        self:UpdateOnShow()
    end
end

function PartnerTransformView:OnEnable()
    self._inAuto = false
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_ACTIVATE_COMPLETE,self._activeCompleteCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_UPGRADE_COMPLETE,self._upgradeCompleteCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Selected_Transform_Bless_Item, self._refreshMatItemCb)
end

function PartnerTransformView:OnDisable()
    self:OnStop()
    --self._selectedPartnerItem = nil
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_ACTIVATE_COMPLETE,self._activeCompleteCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_UPGRADE_COMPLETE,self._upgradeCompleteCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Selected_Transform_Bless_Item, self._refreshMatItemCb)
end

function PartnerTransformView:InitView()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Partner")
    self._listPartner = list
    self._scrollView = scrollView
    self._listPartner:SetItemType(PartnerTransformListItem)
    self._listPartner:SetPadding(8, 0, 0, 0)
    self._listPartner:SetGap(1, 0)
    self._listPartner:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB =                
    function(idx)
        self:OnListItemClicked(idx)
    end
    self._listPartner:SetItemClickedCB(itemClickedCB)

     self._skillList = {}
    for i=1, Max_Skill_Count do
        self._skillList[i] = self:AddChildLuaUIComponent("Container_Skills/Container_Skill"..i, TransformSkillItem)
        self._skillList[i]:SetType(7)
    end

    self._attrList = {}
    for i=1, Max_Attr_Count do
        self._attrList[i] = self:AddChildLuaUIComponent("Container_Attrs/Container_Item"..i, TransformAttriItem)
    end

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    -- self._containerFightPowerUpTrans = self:FindChildGO("Container_FightPowerUp").transform
    -- self._fpCompareNumber = self:AddChildComponent('Container_FightPowerUp/Container_FightPowerUp', XArtNumber)
    -- self._fpCompareNumber:SetNumber(0)
    self._tipsShowView = self:AddChildLuaUIComponent("Container_TipsShow", PartnerTransformExpTipsView)

    self._levelUpTip = self:FindChildGO("Image_LevelUp").transform
    self._levelUpTip.localScale = Vector3.zero
    self._tweenScaleLevelUp = self:AddChildComponent('Image_LevelUp', XGameObjectTweenScale)

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30034")
    self._fx:SetLifeTime(2)
    self._fx:Stop()

    self._containerModel = self:FindChildGO("Container_Model")
    self._modelComponent = self:AddChildComponent('Container_Model', ActorModelComponent)
    self._txtName = self:GetChildComponent("Container_Name/Text_Name",'TextMeshWrapper')
    
    --已激活状态
    self._containerUpgrade = self:FindChildGO("Container_Upgrade")

    self._btnUpgrade = self:FindChildGO("Container_Upgrade/Button_Upgrade")
    self._csBH:AddClick(self._btnUpgrade,function () self:OnUpgradeButtonClick() end)
    self._upgradeRedPoint = self:FindChildGO("Container_Upgrade/Button_Upgrade/Image_Tip")
    self._upgradeRedPoint:SetActive(false)

    self._btnStop = self:FindChildGO("Container_Upgrade/Button_Stop")
    self._csBH:AddClick(self._btnStop,function () self:OnStop() end)
    self._btnStop:SetActive(false)

    self._btnDecompose = self:FindChildGO("Container_Upgrade/Button_Decompose")
    self._csBH:AddClick(self._btnDecompose,function () self:OnDecomposeClick() end)
    self._decomposeRedPoint = self:FindChildGO("Container_Upgrade/Button_Decompose/Image_Tip")
    self._decomposeRedPoint:SetActive(false)

    self._btnOnShow = self:FindChildGO("Container_Upgrade/Toggle_OnShow/Background")
    self._csBH:AddClick(self._btnOnShow,function () self:OnShowClick() end)
    self._checkmarkShow = self:FindChildGO("Container_Upgrade/Toggle_OnShow/Checkmark")

    self._materialList = {}
    for i=1, Max_Material_Count do
        self._materialList[i] = self:AddChildLuaUIComponent("Container_Upgrade/Container_Material/Container_Item"..i, PartnerTransformMaterialItem)
        self._materialList[i]:InitIndex(i)
    end

    self._expComp = UIProgressBar.AddProgressBar(self.gameObject,"Container_Upgrade/ProgressBar_Bless")
    self._expComp:SetProgress(0)
    self._expComp:ShowTweenAnimate(true)
    self._expComp:SetMutipleTween(true)
    self._expComp:SetIsResetToZero(false)
    self._expComp:SetTweenTime(0.2)

    --未激活状态
    self._containerActivate = self:FindChildGO("Container_Activate")

    self._btnActivate = self:FindChildGO("Container_Activate/Button_Activate")
    self._csBH:AddClick(self._btnActivate,function () self:OnActivateClick() end)
    self._activateRedPoint = self:FindChildGO("Container_Activate/Button_Activate/Image_Tip")
    self._activateRedPoint:SetActive(false)

    self._activateMatIcon = self:AddChildLuaUIComponent("Container_Activate/Container_Material",ItemGridEx)
    self._activateMatIcon:ActivateTips()
end

function PartnerTransformView:OnUpgradeButtonClick()
    if self._selectedTransformId then
        if self._inAuto == false then
            self._inAuto = true
            self._timer = TimerHeap:AddSecTimer(0, 0.5, 0, function() self:AutoUpgrade() end)
        end
    end
end

function PartnerTransformView:AutoUpgrade()
    if self._selectMaterialId then
        local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._selectMaterialId)
        if pos > 0 then
            --LoggerHelper.Error("PartnerTransformView()"..self._selectMaterialId..","..pos)
            TransformManager:RequestTransformUpgrade(self._selectedTransformId,pos)
            self._btnUpgrade:SetActive(false)
            self._btnStop:SetActive(true)
        else
            self:OnStop()
        end
    end
end

function PartnerTransformView:OnStop()
    if self._inAuto then
        self._inAuto = false
        TimerHeap:DelTimer(self._timer)
        self._btnUpgrade:SetActive(true)
        self._btnStop:SetActive(false)
    end
end

function PartnerTransformView:OnActivateClick()
    if self._selectedTransformId then
        --LoggerHelper.Error("self._selectedTransformId"..self._selectedTransformId)
        TransformManager:RequestActivateTransform(self._selectedTransformId)
    end
end

function PartnerTransformView:OnShowClick()
    if self._selectedTransformId then
        TransformManager:RequestTransformShow(self._partnerType,2,self._selectedTransformId)
    end
end

function PartnerTransformView:OnDecomposeClick()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SHOW_TRANSFORM_DECOMPOSE,true)
end

function PartnerTransformView:OnListItemClicked(idx)
    if self._selectedPartnerItem then
        self._selectedPartnerItem:SetSelected(false)
        self:OnStop()
    end
    self._selectedPartnerItem = self._listPartner:GetItem(idx)
    self._selectedPartnerItem:SetSelected(true)
    self._selectedPartnerData =  self._listPartner:GetDataByIndex(idx)[1]
    self._selectedTransformId = self._selectedPartnerData.id

    self:UpdateSelectedPartner() 
end

function PartnerTransformView:UpdateSelectedPartner()
    self._lastGrade = 0
    self._dGrade = 0

    local vocationGroup = math.floor(GameWorld.Player().vocation/100)
    local nameId = self._selectedPartnerData.name[vocationGroup]
    self._txtName.text = LanguageDataHelper.CreateContent(nameId)

    for i=1,Max_Skill_Count do
        self._skillList[i]:SetType(self._partnerType)
    end

    self:UpdatePartnerInfo()
    self:UpdateModel()
end

function PartnerTransformView:UpdatePartnerInfo()
    local partnerInfoList = TransformManager:GetPartnerTransformData(self._partnerType)
    local partnerInfo = partnerInfoList[self._selectedTransformId]
    local isActivated = not (partnerInfo == nil)
    local grade
    if isActivated then
        grade = partnerInfo[public_config.PET_ILLUSION_KEY_GRADE]

        if self._lastGrade > 0 then
            self._dGrade = grade - self._lastGrade
        else
            self:UpdateAttri(grade,isActivated)
        end
        --升级了
        if self._dGrade > 0 then
            if self._selectedPartnerItem then
                self._selectedPartnerItem:UpdateData()
            end
            self._tweenScaleLevelUp:OnceOutQuad(1, 1, function()  
                self._levelUpTip.localScale = Vector3.zero
            end)
            self._fx:Play()
            self:UpdateAttri(grade,isActivated)
        end
        self._lastGrade = grade
    else
        grade = 1
        self:UpdateAttri(grade,isActivated)
    end
    -- if nextAttrisList then
    --     local x = 760 + string.len(tostring(fightPower))*20
    --     self._containerFightPowerUpTrans.localPosition = Vector3(x,200,-600)
    --     local fightPowerAdd = BaseUtil.CalculateFightPower(nextPowerList, GameWorld.Player().vocation)
    --     self._fpCompareNumber:SetNumber(fightPowerAdd)
    -- else
    --     self._containerFightPowerUpTrans.localPosition = Vector3(-1000,200,-600)
    -- end

    --已激活
    if isActivated then
        self._containerActivate:SetActive(false)
        self._containerUpgrade:SetActive(true)

        if self._materialInited == false then
            if self._partnerType == public_config.TREASURE_TYPE_PET then
                self._blessItemData = PartnerDataHelper.GetPetBlessItems()
            else
                self._blessItemData = PartnerDataHelper.GetRideBlessItems()
            end
            self:UpdateMat()
            self:RefreshSelectedBlessItem(1)
            self._materialInited = true
        end
        local matCount = self._selectMaterialItem:UpdateCount()
        --self:ShowUpgradePoint(matCount >= 1)

        local blessing = partnerInfo[public_config.PET_ILLUSION_KEY_BLESS] or 0
        local maxBlessing = self._attriCfg.bless
        self._expComp:SetProgressByValue(blessing,maxBlessing,true)
        self:UpdateOnShow()

    --未激活
    else
        self._containerActivate:SetActive(true)
        self._containerUpgrade:SetActive(false)
        local canActivate
        local vocationGroup = math.floor(GameWorld.Player().vocation/100)
        local itemInfo = self._selectedPartnerData.activate_num[vocationGroup]
        self._activateMatIcon:SetActive(true)
        self._activateMatIcon:UpdateData(itemInfo[1],itemInfo[2])
        local count,haveCount = self._activateMatIcon:GetCount()
        canActivate = count <= haveCount
    
        self:ShowActiveRedPoint(canActivate)
    end

    self:UpdateSkill(grade,isActivated)
end

function PartnerTransformView:UpdateAttri(grade,isActivated)
    --LoggerHelper.Error(self._partnerType..","..self._selectedTransformId..","..grade)
    local attriCfg = TransformDataHelper:GetPartnerGradeCfg(self._partnerType,self._selectedTransformId,grade)
    self._attriCfg = attriCfg
    local attrisList = attriCfg.attri
    --LoggerHelper.Log(attrisList)
    local nextAttrisCfg
    if isActivated then
        nextAttrisCfg = TransformDataHelper:GetPartnerGradeCfg(self._partnerType,self._selectedTransformId,grade+1)
    else
        nextAttrisCfg = TransformDataHelper:GetPartnerGradeCfg(self._partnerType,self._selectedTransformId,grade)
    end

    local nextAttrisList
    if nextAttrisCfg then
        nextAttrisList = nextAttrisCfg.attri
    end

    local attriPowerList = {}
    --local nextPowerList = {}
    for i=1, Max_Attr_Count do
        if attrisList[i] then
            local attriData = {}
            attriData[1] = attrisList[i][1]
            if isActivated then
                attriData[2] = attrisList[i][2]
            else
                attriData[2] = 0
            end

            table.insert(attriPowerList, {["attri"]=attriData[1], ["value"]=attriData[2]})
            if nextAttrisList then
                attriData[3] = nextAttrisList[i][2] - attriData[2]
            end
            -- table.insert(nextPowerList, {["attri"]=attriData[1], ["value"]=attriData[3]})
            self._attrList[i]:SetActive(true)
            self._attrList[i]:OnRefreshData(attriData)
        else
            self._attrList[i]:SetActive(false)
        end
    end

    local fightPower = BaseUtil.CalculateFightPower(attriPowerList, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)
end

function PartnerTransformView:UpateExpTip(args)
    if args and args[1] then
        self._tipsShowView:ShowView(args[1])
    end
end

function PartnerTransformView:RefreshSelectedBlessItem(index)
    for i=1, Max_Material_Count do
        if index == i then
            self._selectMaterialId = self._blessItemData[index][1]
            self._selectMaterialItem = self._materialList[i]
            self._materialList[i]:ShowSelected(true)
        else
            self._materialList[i]:ShowSelected(false)
        end
    end
end

function PartnerTransformView:UpdateListOnShow()
    for i=1,#self._listData do
        local item = self._listPartner:GetItem(i-1)
        item:UpdateOnShow()
    end
end

function PartnerTransformView:UpdateOnShow()
    local showInfo = GameWorld.Player().illusion_show_info[self._partnerType]
    if showInfo[public_config.TREASURE_ILLUSION_SHOW_SYSTEM] == public_config.TREASURE_SHOW_ORIGIN_ILLUSION and 
        showInfo[public_config.TREASURE_ILLUSION_SHOW_ID] == self._selectedTransformId then
        self._btnOnShow:SetActive(false)
        self._checkmarkShow:SetActive(true)
    else
        self._btnOnShow:SetActive(true)
        self._checkmarkShow:SetActive(false)
    end
end

function PartnerTransformView:UpdateSkill(grade,isActivated)
    for i=1, Max_Skill_Count do
        local skillInfo = self._selectedPartnerData.passive_skill[i]
        local lock = 0
        if isActivated == false or skillInfo[2] > grade then
            lock = 1
        end
        local skillsData = {["skill"]=skillInfo[1], ["lock"]=lock, ["grade"]=skillInfo[2]}
        self._skillList[i]:OnRefreshData(skillsData)
    end
end

function PartnerTransformView:UpdateModel()
    local vocationGroup = math.floor(GameWorld.Player().vocation/100)
    -- local modelId = self._selectedPartnerData.model_vocation[vocationGroup]

    -- if self._partnerType == public_config.TREASURE_TYPE_PET then
    --     self._modelComponent:SetStartSetting(Vector3(0, 0, -150), Vector3(0, 180, 0), 1)
    -- else
    --     self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 90, 0), 1)
    -- end
    local uiModelCfgId = self._selectedPartnerData.model_vocation[vocationGroup]
    local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
    
    local modelId = modelInfo.model_ui
    local posx = modelInfo.pos[1]
    local posy = modelInfo.pos[2]
    local posz = modelInfo.pos[3]
    local rotationx = modelInfo.rotation[1]
    local rotationy = modelInfo.rotation[2]
    local rotationz = modelInfo.rotation[3]
    local scale = modelInfo.scale
    self._modelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
    self._modelComponent:LoadModel(modelId)
end

function PartnerTransformView:ShowModel()
    self._containerModel:SetActive(true)
end

function PartnerTransformView:HideModel()
    self._containerModel:SetActive(false)
end

function PartnerTransformView:UpdateMat()
    -- LoggerHelper.Error("PartnerTransformView:UpdateMat()")
    for i=1, Max_Material_Count do
        self._materialList[i]:OnRefreshData(self._blessItemData[i])
    end
end

function PartnerTransformView:UpdateView(partnerType,itemId)
    self._materialInited = false

	self._partnerType = partnerType
	self._listData = {}
    local srcData = TransformDataHelper:GetTransformTotalCfgList(partnerType)
    for i=1,#srcData do
        local shouldShow = true
        -- if i >  1 then
        --     local itemId = srcData[i].item_id
        --     local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
        --     if bagNum < srcData[i].activate_num[itemId] then
        --         shouldShow = false
        --     end
        -- end
        
        if shouldShow then
            local d = {}
            d[1] = srcData[i]
            d[2] = partnerType
            table.insert(self._listData,d)
        end
    end
	self._listPartner:SetDataList(self._listData)
    -- if self._materialInited then
    --     self:UpdateMat()
    -- end
    if itemId then
        local vocationGroup = math.floor(GameWorld.Player().vocation/100)
        for i=1,#srcData do
            local cfgItemId = srcData[i].activate_num[vocationGroup][1]
            if cfgItemId == itemId then
                self:OnListItemClicked(i-1)
                self._listPartner:SetPositionByNum(i)
                break
            end
        end
    else
        self:OnListItemClicked(0)
        self._listPartner:SetPositionByNum(1)
    end
end

function PartnerTransformView:ShowUpgradePoint(state)
    self._upgradeRedPoint:SetActive(state)
end

function PartnerTransformView:ShowActiveRedPoint(state)
    self._activateRedPoint:SetActive(state)
end

function PartnerTransformView:ShowDecomposeRedPoint(state)
    self._decomposeRedPoint:SetActive(state)
end