local PartnerTransformExpTipsView = Class.PartnerTransformExpTipsView(ClassTypes.BaseLuaUIComponent)

PartnerTransformExpTipsView.interface = GameConfig.ComponentsConfig.Component
PartnerTransformExpTipsView.classPath = "Modules.ModuleTransform.ChildView.PartnerTransformExpTipsView"

local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local Max_Tips_Count = 3

function PartnerTransformExpTipsView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._queue = {}
    self:InitView()
end
--override
function PartnerTransformExpTipsView:OnDestroy() 

end
--data结构
function PartnerTransformExpTipsView:ShowView(data)
    self:RefreshView(data)
end

function PartnerTransformExpTipsView:InitView()
    self._tipsList = {}
    self._tipsTextList = {}
    self._showState = {}
    self._pos = {}
    self._tweenPosTipsList = {}
    for i=1, Max_Tips_Count do
        self._tipsList[i] = self:FindChildGO("Text_Tip"..i)
        self._tipsTextList[i] = self:GetChildComponent("Text_Tip"..i, "TextMeshWrapper")
        self._pos[i] = self._tipsList[i].transform.localPosition
        self._tweenPosTipsList[i] = self:AddChildComponent("Text_Tip"..i, XGameObjectTweenPosition)
        self:ResetItem(i)
    end
end

function PartnerTransformExpTipsView:GetItemId()
    for i=1, Max_Tips_Count do
        if self._showState[i] == 0 then
            self._showState[i] = 1
            return i
        end
    end
    return 0
end

function PartnerTransformExpTipsView:ResetItem(id)
    self._showState[id] = 0
    self._tipsList[id].transform.localPosition = self.HIDE_POSITION
end

function PartnerTransformExpTipsView:RefreshView(data)
    table.insert(self._queue, data)
    self:CheckShow()
end

function PartnerTransformExpTipsView:CheckShow()
    if #self._queue > 0 then
        local id = self:GetItemId()
        if id == 0 then
            return
        end
        local data = table.remove(self._queue, 1)
        
        self._tipsTextList[id].text = "+"..data
        
        self._tipsList[id].transform.localPosition = self._pos[id]
        local moveTo = Vector3.New(0, 50, 0) + self._pos[id]
        self._tweenPosTipsList[id]:OnceOutQuad(moveTo, 1, function()
            self:ResetItem(id)
            self:CheckShow()
        end)
    end
end