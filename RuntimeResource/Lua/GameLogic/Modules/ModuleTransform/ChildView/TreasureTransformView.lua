local TreasureTransformView = Class.TreasureTransformView(ClassTypes.BaseLuaUIComponent)

TreasureTransformView.interface = GameConfig.ComponentsConfig.Component
TreasureTransformView.classPath = "Modules.ModuleTransform.ChildView.TreasureTransformView"

require "Modules.ModuleTransform.ChildComponent.TreasureTransformListItem"
require "Modules.ModuleTransform.ChildComponent.TransformAttriItem"
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local UIList = ClassTypes.UIList
local TreasureTransformListItem = ClassTypes.TreasureTransformListItem
local TransformAttriItem = ClassTypes.TransformAttriItem

local XArtNumber = GameMain.XArtNumber
local TransformDataHelper = GameDataHelper.TransformDataHelper
local EntityConfig = GameConfig.EntityConfig
local BaseUtil = GameUtil.BaseUtil
local EquipModelComponent = GameMain.EquipModelComponent
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TransformManager = PlayerManager.TransformManager
local Max_Attr_Count = 6

function TreasureTransformView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitCallback()
    self:InitView()
end

function TreasureTransformView:InitCallback()
    self._activeCompleteCb = function ()
        if self._selectedTreasureItem then
            self._selectedTreasureItem:UpdateData()
        end
        self:UpdateSelectedTreasure()
    end

    self._updateTreasureCb = function ()
        if self._selectedTreasureItem then
            self._selectedTreasureItem:UpdateData()
        end
        self:UpdateSelectedTreasure()
    end

    self._updateOnShowCb =function ()
        self:UpdateListOnShow()
        self:UpdateOnShow()
    end
end

function TreasureTransformView:OnEnable()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_ACTIVATE_COMPLETE,self._activeCompleteCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_UPSTAR_COMPLETE,self._updateTreasureCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)
end

function TreasureTransformView:OnDisable()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_ACTIVATE_COMPLETE,self._activeCompleteCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_UPSTAR_COMPLETE,self._updateTreasureCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)
end

function TreasureTransformView:InitView()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Treasure")
    self._listTreasure = list
    self._scrollView = scrollView
    self._listTreasure:SetItemType(TreasureTransformListItem)
    self._listTreasure:SetPadding(8, 0, 0, 0)
    self._listTreasure:SetGap(1, 0)
    self._listTreasure:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB =                
    function(idx)
        self:OnListItemClicked(idx)
    end
    self._listTreasure:SetItemClickedCB(itemClickedCB)

    self._attrList = {}
    for i=1, Max_Attr_Count do
        self._attrList[i] = self:AddChildLuaUIComponent("Container_Attrs/Container_Item"..i, TransformAttriItem)
    end

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self._containerFightPowerUpTrans = self:FindChildGO("Container_FightPowerUp").transform
    self._fpCompareNumber = self:AddChildComponent('Container_FightPowerUp/Container_FightPowerUp', XArtNumber)
    self._fpCompareNumber:SetNumber(0)

    self._starUpTip = self:FindChildGO("Image_StarUp").transform
    self._starUpTip.localScale = Vector3.zero

    self._containerModel = self:FindChildGO("Container_Model")
    self._modelComponent = self:AddChildComponent('Container_Model', EquipModelComponent)
    --self._modelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
    self._txtName = self:GetChildComponent("Container_Name/Text_Name",'TextMeshWrapper')
    
    --已激活状态
    self._containerUpgrade = self:FindChildGO("Container_Upgrade")

    self._btnUpgrade = self:FindChildGO("Container_Upgrade/Button_Upstar")
    self._csBH:AddClick(self._btnUpgrade,function () self:OnUpgradeButtonClick() end)
    self._upgradeRedPoint = self:FindChildGO("Container_Upgrade/Button_Upstar/Image_Tip")
    self._upgradeRedPoint:SetActive(false)

    self._btnDecompose = self:FindChildGO("Container_Upgrade/Button_Decompose")
    self._csBH:AddClick(self._btnDecompose,function () self:OnDecomposeClick() end)
    self._decomposeRedPoint = self:FindChildGO("Container_Upgrade/Button_Decompose/Image_Tip")
    self._decomposeRedPoint:SetActive(false)

    self._btnOnShow = self:FindChildGO("Container_Upgrade/Toggle_OnShow/Background")
    self._csBH:AddClick(self._btnOnShow,function () self:OnShowClick() end)
    self._checkmarkShow = self:FindChildGO("Container_Upgrade/Toggle_OnShow/Checkmark")

    self._upgradeMatIcon = self:AddChildLuaUIComponent("Container_Upgrade/Container_Material",ItemGridEx)
    self._upgradeMatIcon:SetShowName(false)
    self._upgradeMatIcon:ActivateTips()

    self._stars = {}
    for i=1,5 do
    	self._stars[i] = self:FindChildGO("Container_Upgrade/Container_Star/Image_Star"..i)
    end

    --未激活状态
    self._containerActivate = self:FindChildGO("Container_Activate")

    self._btnActivate = self:FindChildGO("Container_Activate/Button_Activate")
    self._csBH:AddClick(self._btnActivate,function () self:OnActivateClick() end)
    self._activateRedPoint = self:FindChildGO("Container_Activate/Button_Activate/Image_Tip")
    self._activateRedPoint:SetActive(false)

    self._activateMatIcon = self:AddChildLuaUIComponent("Container_Activate/Container_Material",ItemGridEx)
    self._activateMatIcon:SetShowName(false)
    self._activateMatIcon:ActivateTips()
end

function TreasureTransformView:OnUpgradeButtonClick()
    if self._selectedTransformId then
        TransformManager:RequestTransformUpStar(self._selectedTransformId)
    end
end

function TreasureTransformView:OnActivateClick()
    if self._selectedTransformId then
        --LoggerHelper.Error("self._selectedTransformId"..self._selectedTransformId)
        TransformManager:RequestActivateTransform(self._selectedTransformId)
    end
end

function TreasureTransformView:OnShowClick()
    if self._selectedTransformId then
        TransformManager:RequestTransformShow(self._treasureType,public_config.TREASURE_SHOW_ORIGIN_ILLUSION,self._selectedTransformId)
    end
end

function TreasureTransformView:OnDecomposeClick()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SHOW_TRANSFORM_DECOMPOSE,true)
end

function TreasureTransformView:OnListItemClicked(idx)
    if self._selectedTreasureItem then
        self._selectedTreasureItem:SetSelected(false)
    end
    self._selectedTreasureItem = self._listTreasure:GetItem(idx)
    self._selectedTreasureItem:SetSelected(true)
    self._selectedTreasureData =  self._listTreasure:GetDataByIndex(idx)[1]
    self._selectedTransformId = self._selectedTreasureData.id

    self:UpdateSelectedTreasure() 
end

function TreasureTransformView:UpdateSelectedTreasure()
    local treasureInfo = TransformManager:GetTreasureActivateData(self._treasureType)
    local star = treasureInfo[self._selectedTransformId]
    local isActivated = not (star == nil)
    star = star or 0

    local attriCfg = TransformDataHelper:GetTreasureStarCfg(self._treasureType,self._selectedTransformId,star)
    local attrisList = attriCfg.attri
    local nextAttrisCfg
    if isActivated then
    	nextAttrisCfg = TransformDataHelper:GetTreasureStarCfg(self._treasureType,self._selectedTransformId,star+1)
    else
    	nextAttrisCfg = TransformDataHelper:GetTreasureStarCfg(self._treasureType,self._selectedTransformId,star)
    end

    local nextAttrisList
    if nextAttrisCfg then
        nextAttrisList = nextAttrisCfg.attri
    end

    local attriPowerList = {}
    local nextPowerList = {}
    for i=1, Max_Attr_Count do
        if attrisList[i] then
            local attriData = {}
            attriData[1] = attrisList[i][1]
            if isActivated then
            	attriData[2] = attrisList[i][2]
            else
            	attriData[2] = 0
            end

            table.insert(attriPowerList, {["attri"]=attriData[1], ["value"]=attriData[2]})
            if nextAttrisList then
                attriData[3] = nextAttrisList[i][2] - attriData[2]
            end
            table.insert(nextPowerList, {["attri"]=attriData[1], ["value"]=attriData[3]})
            self._attrList[i]:SetActive(true)
            self._attrList[i]:OnRefreshData(attriData)
        else
            self._attrList[i]:SetActive(false)
        end
    end

    local fightPower = BaseUtil.CalculateFightPower(attriPowerList, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)

    if nextAttrisList then
        local x = 760 + string.len(tostring(fightPower))*20
        self._containerFightPowerUpTrans.localPosition = Vector3(x,200,-600)
        local fightPowerAdd = BaseUtil.CalculateFightPower(nextPowerList, GameWorld.Player().vocation)
        self._fpCompareNumber:SetNumber(fightPowerAdd)
    else
        self._containerFightPowerUpTrans.localPosition = Vector3(-1000,200,-600)
    end
    
    local vocationGroup = math.floor(GameWorld.Player().vocation/100)
    local nameId = self._selectedTreasureData.name[vocationGroup]
    self._txtName.text = LanguageDataHelper.CreateContent(nameId)
    
    self:UpdateModel()

    --已激活
    if isActivated then
        self._containerActivate:SetActive(false)
        self._containerUpgrade:SetActive(true)

        if star ==  5 then
            self._upgradeMatIcon:SetActive(false)
            self._btnDecompose:SetActive(true)
            self._btnUpgrade:SetActive(false)
        else
            self._upgradeMatIcon:SetActive(true)
            local costItem = attriCfg.star_lvup_cost[vocationGroup]
            self._upgradeMatIcon:UpdateData(costItem[1],costItem[2])
            
            local count,haveCount = self._upgradeMatIcon:GetCount()
            self:ShowUpgradePoint(count <= haveCount)

            self._btnDecompose:SetActive(false)
            self._btnUpgrade:SetActive(true)
        end

        self:SetStar(star)
        self:UpdateOnShow()
    --未激活
    else
        self._containerActivate:SetActive(true)
        self._containerUpgrade:SetActive(false)
        local canActivate
        local itemInfo = self._selectedTreasureData.activate_num[vocationGroup]
        self._activateMatIcon:SetActive(true)
        self._activateMatIcon:UpdateData(itemInfo[1],itemInfo[2])
        local count,haveCount = self._activateMatIcon:GetCount()
        local vocationLimit = true
        if self._selectedTreasureData.activate_limit then
            local needVocation = self._selectedTreasureData.activate_limit[vocationGroup]
            if GameWorld.Player().vocation < needVocation then
                vocationLimit = false
            end
        end
        canActivate = (count <= haveCount) and vocationLimit
    
        self:ShowActiveRedPoint(canActivate)

    end
end

function TreasureTransformView:UpdateListOnShow()
    for i=1,#self._listData do
        local item = self._listTreasure:GetItem(i-1)
        item:UpdateOnShow()
    end
end

function TreasureTransformView:UpdateOnShow()
    local showInfo = GameWorld.Player().illusion_show_info[self._treasureType]
    if showInfo[public_config.TREASURE_ILLUSION_SHOW_SYSTEM] == public_config.TREASURE_SHOW_ORIGIN_ILLUSION and 
        showInfo[public_config.TREASURE_ILLUSION_SHOW_ID] == self._selectedTransformId then
        self._btnOnShow:SetActive(false)
        self._checkmarkShow:SetActive(true)
    else
        self._btnOnShow:SetActive(true)
        self._checkmarkShow:SetActive(false)
    end
end

function TreasureTransformView:SetStar(star)
	for i=1,#self._stars do
		if i <= star then
			self._stars[i]:SetActive(true)
		else
			self._stars[i]:SetActive(false)
		end
	end
end

function TreasureTransformView:UpdateModel()
    local vocationGroup = math.floor(GameWorld.Player().vocation/100)
    local uiModelCfgId = self._selectedTreasureData.model_vocation[vocationGroup]
    local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
    
    local modelId = modelInfo.model_ui
    local posx = modelInfo.pos[1]
    local posy = modelInfo.pos[2]
    local posz = modelInfo.pos[3]
    local rotationx = modelInfo.rotation[1]
    local rotationy = modelInfo.rotation[2]
    local rotationz = modelInfo.rotation[3]
    local scale = modelInfo.scale
    self._modelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
    
    --神兵自转
    if self._treasureType == public_config.TREASURE_TYPE_WEAPON then
        self._modelComponent:SetSelfRotate(true)
    else
        self._modelComponent:SetSelfRotate(false)
    end
    
    if self._treasureType == public_config.TREASURE_TYPE_TALISMAN then
        --法宝替换controller
        local controllerPath = GlobalParamsHelper.GetParamValue(692)
        self._modelComponent:LoadEquipModel(modelId, controllerPath)
    else
        self._modelComponent:LoadEquipModel(modelId, "")
    end
end

function TreasureTransformView:UpdateView(index,itemId)
	self._treasureType = index
	self._listData = {}
    local srcData = TransformDataHelper:GetTransformTotalCfgList(index)
    for i=1,#srcData do
        local d = {}
        d[1] = srcData[i]
        d[2] = index
        table.insert(self._listData,d)
    end
	self._listTreasure:SetDataList(self._listData)

    if itemId then
        local vocationGroup = math.floor(GameWorld.Player().vocation/100)
        for i=1,#srcData do
            local cfgItemId = srcData[i].activate_num[vocationGroup][1]
            if cfgItemId == itemId then
                self:OnListItemClicked(i-1)
                self._listTreasure:SetPositionByNum(i)
                break
            end
        end
    else
        self:OnListItemClicked(0)
        self._listTreasure:SetPositionByNum(1)
    end
end

function TreasureTransformView:ShowUpgradePoint(state)
    self._upgradeRedPoint:SetActive(state)
end

function TreasureTransformView:ShowActiveRedPoint(state)
    self._activateRedPoint:SetActive(state)
end

function TreasureTransformView:ShowModel()
    self._containerModel:SetActive(true)
end

function TreasureTransformView:HideModel()
    self._containerModel:SetActive(false)
end