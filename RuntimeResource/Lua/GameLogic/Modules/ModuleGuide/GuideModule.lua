--GuideModule.lua
GuideModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function GuideModule.Init()
	--GUIManager.AddPanel(PanelsConfig.Guide, "Panel_Guide", GUILayer.LayerUICommon, "Modules.ModuleGuide.GuidePanel", true, false)
	GUIManager.AddPanel(PanelsConfig.GuideArrow, "Panel_GuideArrow", GUILayer.LayerUIFloat, "Modules.ModuleGuide.GuideArrowPanel", true, false)
	GUIManager.AddPanel(PanelsConfig.GuideSkip, "Panel_GuideSkip", GUILayer.LayerUIFloat, "Modules.ModuleGuide.GuideSkipPanel", true, PanelsConfig.EXTEND_TO_FIT)
end

return GuideModule