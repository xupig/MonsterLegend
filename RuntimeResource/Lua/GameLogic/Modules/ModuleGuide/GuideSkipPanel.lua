--GuideSkipPanel.lua
local GuideSkipPanel = Class.GuideSkipPanel(ClassTypes.BasePanel)
local LocalSetting = GameConfig.LocalSetting
local GuideManager = GameManager.GuideManager
local DramaManager = GameManager.DramaManager

function GuideSkipPanel:Awake()
    self:InitPanel()
end

function GuideSkipPanel:OnDestroy()

end

function GuideSkipPanel:InitPanel()
    self._button = self:FindChildGO("Button_Skip")
	self._csBH:AddClick(self._button,function() self:OnSkipButtonClick() end)
end

function GuideSkipPanel:OnSkipButtonClick(text)
    DramaManager:SkipCurRunningDrama()
end