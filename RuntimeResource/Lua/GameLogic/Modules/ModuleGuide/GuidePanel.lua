--GuidePanel.lua
local GuidePanel = Class.GuidePanel(ClassTypes.BasePanel)
local LocalSetting = GameConfig.LocalSetting
local GuideManager = GameManager.GuideManager

function GuidePanel:Awake()
	self._csBH = self:GetComponent('LuaUIPanel')
	self._Container = self:FindChildGO("Container_Guide")
	self._text = self:FindChildGO("Container_Guide/Guide_Context")
	self._buttonNext = self:FindChildGO("Container_Guide/Button_Next")
	self._csBH:AddClick(self._buttonNext,function() self:OnNextClick() end)
	GuideManager.ShowTextAction = function (text)
		self:ShowText(text)
	end
end

function GuidePanel:OnNextClick()
	self._Container:SetActive(false)
end

function GuidePanel:ShowText(text)
	LoggerHelper.Error(text)
	self._text:GetComponent("TextMeshWrapper").text = text
	self._Container:SetActive(true)
end

function GuidePanel:OnDestroy()
	self._buttonNext = nil
	self._text = nil
	self._csBH = nil
end