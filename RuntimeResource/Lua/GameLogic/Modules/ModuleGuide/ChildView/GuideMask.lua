-- GuideMask.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local GuideMask = Class.GuideMask(ClassTypes.BaseLuaUIComponent)
-- 指定实现的接口类，对应C#中实现接口的类
GuideMask.interface = GameConfig.ComponentsConfig.Component
GuideMask.classPath = "Modules.ModuleGuide.ChildView.GuideMask"

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

local WIDTH = 1280
local HEIGHT = 720
local NORMAL = 1000

local COLOR_ALPHA_0 = Color.New(0, 0, 0, 0)
local COLOR_ALPHA_96 = Color.New(0, 0, 0, 96/255)
local COLOR_ALPHA_255 = Color.New(1, 1, 1, 1)

function GuideMask:Awake()
    self:InitComps()
end

--override
function GuideMask:OnDestroy()
end

function GuideMask:OnEnable()
    self._skipNum = 0
end

function GuideMask:OnDisable()
    self._layerName = nil
    self._panelName = nil
end

function GuideMask:InitComps()
    self._leftTopMaskRect = self:GetChildComponent("Image_LT", "RectTransform")
    self._rightTopMaskRect = self:GetChildComponent("Image_RT", "RectTransform")
    self._leftBottomMaskRect = self:GetChildComponent("Image_LB", "RectTransform")
    self._rightBottomMaskRect = self:GetChildComponent("Image_RB", "RectTransform")

    self._leftTopMaskGO = self:FindChildGO("Image_LT")
    self._csBH:AddClick(self._leftTopMaskGO, function()self:PlayFx() end)
    self._rightTopMaskGO = self:FindChildGO("Image_RT")
    self._csBH:AddClick(self._rightTopMaskGO, function()self:PlayFx() end)
    self._leftBottomMaskGO = self:FindChildGO("Image_LB")
    self._csBH:AddClick(self._leftBottomMaskGO, function()self:PlayFx() end)
    self._rightBottomMaskGO = self:FindChildGO("Image_RB")
    self._csBH:AddClick(self._rightBottomMaskGO, function()self:PlayFx() end)

    self._LTImage = self:GetChildComponent("Image_LT", "ImageWrapper")
    self._RTImage = self:GetChildComponent("Image_RT", "ImageWrapper")
    self._LBImage = self:GetChildComponent("Image_LB", "ImageWrapper")
    self._RBImage = self:GetChildComponent("Image_RB", "ImageWrapper")

    self._centreMaskRect = self:GetChildComponent("Image_Centre", "RectTransform")
    self._centreImage = self:GetChildComponent("Image_Centre", "ImageWrapper")
end

function GuideMask:PlayFx()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON,"guide_dialogue_click_button")
    EventDispatcher:TriggerEvent(GameEvents.PlayeGuideFx)
    
    if self._layerName ~= nil and self._panelName ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.SHOWING_GUIDE_MASK,self._layerName,self._panelName)
    end
end

----------------------------------------------------------------------------【接口】-------------------------------------------------------------------------

function GuideMask:UpdateMask(arrowPos, targetWidth, targetHeight, offsetX, offsetY, layerName, panelName, isCentre)
    if arrowPos == nil then
        return
    end
    self._layerName = layerName
    self._panelName = panelName

    local posX = arrowPos.x - offsetX
    local posY = arrowPos.y - offsetY

    self._leftTopMaskRect.sizeDelta = Vector2.New(NORMAL + posX + targetWidth, NORMAL + HEIGHT - posY - targetHeight)
    self._rightTopMaskRect.sizeDelta = Vector2.New(NORMAL + WIDTH - posX - targetWidth, math.floor( NORMAL+ HEIGHT- posY)+1 )
    self._rightBottomMaskRect.sizeDelta = Vector2.New(NORMAL + WIDTH - posX, math.ceil(NORMAL + posY)-1 )
    self._leftBottomMaskRect.sizeDelta = Vector2.New(NORMAL + posX , NORMAL + posY + targetHeight)

    if isCentre == 1 then
        self._centreMaskRect.sizeDelta = Vector2.New(targetWidth, targetHeight)
        self._centreMaskRect.localPosition = arrowPos
        self:ShowCentreImage(true)
    else
        self:ShowCentreImage(false)
    end
end

--简化版UpdateMask，不用传偏移值、layerName、panelName
function GuideMask:UpdateMaskRegion(posX, posY, targetWidth, targetHeight)
    self._leftTopMaskRect.sizeDelta = Vector2.New(NORMAL + posX + targetWidth, NORMAL + HEIGHT - posY - targetHeight)
    self._rightTopMaskRect.sizeDelta = Vector2.New(NORMAL + WIDTH - posX - targetWidth, math.floor( NORMAL+ HEIGHT- posY ) )
    self._rightBottomMaskRect.sizeDelta = Vector2.New(NORMAL + WIDTH - posX, math.ceil(NORMAL + posY) )
    self._leftBottomMaskRect.sizeDelta = Vector2.New(NORMAL + posX , NORMAL + posY + targetHeight)
    self._layerName = nil
    self._panelName = nil
    self:SetInvisible(COLOR_ALPHA_96)
end

--设置背景是否为透明
function GuideMask:SetBgColor(isBlack)
    if isBlack then
        self:SetInvisible(COLOR_ALPHA_255)
    else
        self:SetInvisible(COLOR_ALPHA_0)
    end
end

function GuideMask:SetInvisible(colorValue)
    if self._LTImage ~= nil then
        self._LTImage.color = colorValue
        self._RTImage.color = colorValue
        self._LBImage.color = colorValue
        self._RBImage.color = colorValue
    end  
end

function GuideMask:ShowCentreImage(state)
    if state then
        self._centreImage.color = COLOR_ALPHA_255
    else
        self._centreImage.color = COLOR_ALPHA_0
    end
end