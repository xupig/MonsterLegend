-- GuideContent.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local GuideContent = Class.GuideContent(ClassTypes.BaseLuaUIComponent)
-- 指定实现的接口类，对应C#中实现接口的类
GuideContent.interface = GameConfig.ComponentsConfig.Component
GuideContent.classPath = "Modules.ModuleGuide.ChildView.GuideContent"

local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local RectTransformUtility = UnityEngine.RectTransformUtility
local GUILayerManager = GameManager.GUILayerManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIParticle = ClassTypes.UIParticle
local GuideConfig = GameConfig.GuideConfig

function GuideContent:Awake()
    self:InitComps()
end

--override
function GuideContent:OnDestroy()
end

function GuideContent:OnDisable()
    self._particle1:Stop()
end

function GuideContent:InitComps()

    self.parentGo = self.transform.parent

    self._CircleGo = self:FindChildGO('Image_circle')
    self._ArrowGo = self:FindChildGO('Container_Offset')

    self._particle = UIParticle.AddParticle(self.gameObject, "fx_ui_30039")
    self._particle:SetTimeScaleEnable(false)
    self._particle:SetPosition(Vector3.New(50,50,0))

    self._particle1 = UIParticle.AddParticle(self.gameObject, "fx_ui_30040")
    self._particle1:SetTimeScaleEnable(false)
    self._particle1:SetPosition(Vector3.New(50,50,0))
    self._particle1:Stop()
    self._particle1:SetLifeTime(1)
    self._particle1:SetPlayEndCB(function()  self._particle1:Stop() end)

    self._textTips = self:GetChildComponent("Container_Offset/Container_Left/Container_Arrow/Image_Tail/Text_Des", "TextMeshWrapper")
    self._textRightTips = self:GetChildComponent("Container_Offset/Container_Right/Container_Arrow/Image_Tail/Text_Des", "TextMeshWrapper")

    self._leftArrow = self:FindChildGO('Container_Offset/Container_Left')
    self._rightArrow = self:FindChildGO('Container_Offset/Container_Right')

    self:SetActive(false)
end

---------------------------------------------------------------------------------【接口】----------------------------------------------------------------------
--播放一下,变大圆圈特效
function GuideContent:PlayFx()
    self._particle1:Play(false,true)
end

--重置父节点
function GuideContent:ReSetParent()
    self.transform:SetParent(self.parentGo, false)
    self.transform.localScale = Vector3.one
end

--设置父节点
function GuideContent:SetParent(targetGo, circleOffsetX, circleOffsetY)
    self.transform:SetParent(targetGo.transform, false)
    self.transform.localPosition = Vector3.New(circleOffsetX,circleOffsetY,0)
    self.transform.localScale = Vector3.one
    self.transform.Rotation = Vector3.zero
end

--设置localPosition
function GuideContent:SetPostion(pos)
    self:ReSetParent()
    self.transform.localPosition = pos
end

--设置anchoredPosition
function GuideContent:SetAnchoredPosition(pos)
    self:ReSetParent()
    self.transform.anchoredPosition = pos
end

--设置特效
function GuideContent:SetParticleScale(fxScale,width,height)
    if fxScale == 1 then
        if width > height then
            self._particle:SetScale(height / 150)
        else
            self._particle:SetScale(width / 150)
        end
    else
        self._particle:SetScale(fxScale)  
    end
end

--设置提示文字
function GuideContent:SetText(direction, contentTextId, offsetX, offsetY)
    if contentTextId ~= nil and contentTextId ~= "0" then
        self._ArrowGo:SetActive(true)
        self._ArrowGo.transform.localPosition = Vector3.New(offsetX,offsetY,0)
        self:SetTextTips(direction,contentTextId)
    else
        self._ArrowGo:SetActive(false)
    end
end

function GuideContent:SetTextTips(direction,contentTextId)
    if direction == nil or direction == GuideConfig.ArrowDirection.Left then --用左箭头
        self._leftArrow:SetActive(true)
        self._rightArrow:SetActive(false)
    else
        self._leftArrow:SetActive(false)
        self._rightArrow:SetActive(true)
    end
    self:SetTips(tonumber(contentTextId))
end

function GuideContent:SetTips(tipsId)
    if tipsId > 0 then
        self._textTips.text =  LanguageDataHelper.CreateContent(tipsId)
        self._textRightTips.text = LanguageDataHelper.CreateContent(tipsId)
    else
        self._textTips.text = ''
        self._textRightTips.text = ''
    end
end