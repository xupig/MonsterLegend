-- GuideDialogue.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local GuideDialogue = Class.GuideDialogue(ClassTypes.BaseLuaUIComponent)
-- 指定实现的接口类，对应C#中实现接口的类
GuideDialogue.interface = GameConfig.ComponentsConfig.Component
GuideDialogue.classPath = "Modules.ModuleGuide.ChildView.GuideDialogue"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ActorModelComponent = GameMain.ActorModelComponent

function GuideDialogue:Awake()
    self:InitComps()
    self:AddEventListeners()
end

--override
function GuideDialogue:OnDestroy()
end

function GuideDialogue:AddEventListeners()
end

function GuideDialogue:OnEnable()
end

function GuideDialogue:OnDisable()
    self:Reset()
end

function GuideDialogue:InitComps()
    self._buttonMask = self:FindChildGO("Button_Mask")
    self._csBH:AddClick(self._buttonMask, function() self:OnClickNext() end)
    

    self._dialogue = self:FindChildGO('Container_Dialogue')

    self._leftGo = self:FindChildGO('Container_Dialogue/Image_Left')
    self._rightGo = self:FindChildGO('Container_Dialogue/Image_Right')
    self._textRight = self:GetChildComponent("Container_Dialogue/Image_Right/Text_Desc", "TextMeshWrapper")
    self._textLeft = self:GetChildComponent("Container_Dialogue/Image_Left/Text_Desc", "TextMeshWrapper")

    self._empty = self:FindChildGO("Image_EmptyShine")
    self._emptyImage = self:GetChildComponent("Image_EmptyShine", "ImageWrapper")
    
    self._arrowGo = self:FindChildGO("Image_Arrow")
    self._arrowRect = self:GetChildComponent("Image_Arrow", "RectTransform")

    self._nextButton = self:FindChildGO("Container_Dialogue/Button_Next")
    self._csBH:AddClick(self._nextButton, function() self:OnClickNext() end)

    self._modelComponent = self:AddChildComponent('Container_Dialogue/Container_Model', ActorModelComponent)
    self._modelComponent:SetDragable(false)
    
    self:Reset()
end

function GuideDialogue:OnCloseDialogue()
    -- EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_GUIDE_DIALOGUE)
    -- LoggerHelper.Log('抛事件：GameEvents.ON_CLOSE_GUIDE_DIALOGUE')
end

function GuideDialogue:OnClickNext()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON,"guide_dialogue_click_button")
end

function GuideDialogue:Reset()
    self._empty:SetActive(false)
    self._arrowGo:SetActive(false)
    self._buttonMask:SetActive(false)
end

------------------------------------------------------------------------【接口】-----------------------------------------------------------------------
function GuideDialogue:UpdateDialogue(textId,offsetX,offsetY,roleId,modelX,modelY,modelZ,modelScale,direction)
    self:SetDialoguePosition(offsetX,offsetY)
    self:SetDialogueDes(textId,direction)
    self:SetRoleModel(roleId,modelX,modelY,modelZ,modelScale)
end

--设置对话位置
function GuideDialogue:SetDialoguePosition(offsetX,offsetY)
    self._dialogue.transform.localPosition = Vector3.New( offsetX,offsetY,0)
end

--设置对话内容
function GuideDialogue:SetDialogueDes(textId,direction)
    if direction == "Left" then
        self._leftGo:SetActive(true)
        self._rightGo:SetActive(false)
        self._textLeft.text = LanguageDataHelper.CreateContent(textId)
    else
        self._leftGo:SetActive(false)
        self._rightGo:SetActive(true)
        self._textRight.text = LanguageDataHelper.CreateContent(textId)
    end
end

--设置npc模型
function GuideDialogue:SetRoleModel(modelId,modelX,modelY,modelZ,modelScale)
    self._modelComponent:SetStartSetting(Vector3(modelX, modelY, modelZ), Vector3(0, 180, 0), modelScale)
    self._modelComponent:LoadModel(modelId)
end

--设置箭头
function GuideDialogue:SetArrow(x,y,rotation)
    self._arrowGo:SetActive(true)
    self._arrowRect.localRotation = Vector3.New(0, 0, rotation)
    self._arrowRect.localPosition = Vector3.New(x, y, 0)
end

--设置长方形
function GuideDialogue:SetEmptyShine(x,y,w,h)
    if x > -1000 then
        self._empty:SetActive(true)
        self._empty.transform.localPosition = Vector3.New( x,y,0)
        self._empty.transform.sizeDelta = Vector3.New( w,h,0)
        self._emptyImage:SetAllDirty()
    else
        self._empty:SetActive(false)
    end
end

--设置是否显示全屏黑底
function GuideDialogue:SetFullScreenMask(value)
    self._buttonMask:SetActive(value)
end


