--GuideArrowPanel.lua
local GuideArrowPanel = Class.GuideArrowPanel(ClassTypes.BasePanel)

require 'Modules.ModuleGuide.ChildView.GuideContent'
local GuideContent = ClassTypes.GuideContent

require 'Modules.ModuleGuide.ChildView.GuideMask'
local GuideMask = ClassTypes.GuideMask

require 'Modules.ModuleGuide.ChildView.GuideMaskHit'
local GuideMaskHit = ClassTypes.GuideMaskHit

require 'Modules.ModuleGuide.ChildView.GuideDialogue'
local GuideDialogue = ClassTypes.GuideDialogue

local GuideConfig = GameConfig.GuideConfig
local TimerHeap = GameWorld.TimerHeap
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local RectTransformUtility = UnityEngine.RectTransformUtility

local Hit_Name = "/Container_Hit"

function GuideArrowPanel:Awake()
	self:InitComps()
end

function GuideArrowPanel:OnDestroy()
end

function GuideArrowPanel:InitComps()
	self._containerArrow = self:AddChildLuaUIComponent("Container_Arrow", GuideContent)
	self._containerMask = self:AddChildLuaUIComponent('Container_Mask',GuideMask)
	self._containerDialogue = self:AddChildLuaUIComponent('Container_Dialogue',GuideDialogue)
	self._containerMaskHit = self:AddChildLuaUIComponent('Container_MaskHit',GuideMaskHit)
	
	self._onClickPlayFx = function() self._containerArrow:PlayFx() end
	self._onLeaveMap = function() self:ClearAllUI() end
end

function GuideArrowPanel:OnShow(data)
	if data == nil then
		return
	end
    EventDispatcher:TriggerEvent(GameEvents.StopPlayerMoveAction)
	self:UpdateGuideData(data)
end

function GuideArrowPanel:OnClose()
	self:ClearAllUI()
end

function GuideArrowPanel:OnEnable()
	self:InitEvent()
end

function GuideArrowPanel:OnDisable()
	self:RemoveEvent()
end

function GuideArrowPanel:InitEvent()
	--EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
	EventDispatcher:AddEventListener(GameEvents.PlayeGuideFx,self._onClickPlayFx)
end

function GuideArrowPanel:RemoveEvent()
	--EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
	EventDispatcher:RemoveEventListener(GameEvents.PlayeGuideFx,self._onClickPlayFx)
end

function GuideArrowPanel:SetData(data)
	if data == nil then
		return
	end
	self:UpdateGuideData(data)
end

function GuideArrowPanel:UpdateGuideData(data)
	GameManager.GuideManager.ShowGuide(false)
	local shield = GameManager.GuideManager.GetShield()
	if shield then
		data[1] = '0'
	end

	self:DelTimer()

	if data[1] == '0' then
		self:ClearAllUI()
	else
		self:UpdateByType(data)
	end
end

function GuideArrowPanel:ClearAllUI()
	-- GameWorld.LoggerHelper.Error("清理", true)
	self._containerArrow:SetActive(false)
	self._containerArrow:ReSetParent()
	self._containerMask:SetActive(false)
	self._containerMaskHit:SetActive(false)
	self._containerDialogue:SetActive(false)
	self:DelTimer()
	GameManager.GuideManager.ShowGuide(false)
end

function GuideArrowPanel:DelTimer()
	if self._timerId ~= nil then
		TimerHeap:DelTimer(self._timerId)
	end
	self._timerId = nil
end

function GuideArrowPanel:InitTimer()
	self:DelTimer()
	local time = GlobalParamsHelper.GetParamValue(765)
	self._timerId = TimerHeap:AddTimer(time, 0, 0, function() self:ClearAllUI() end)
end

-----------------------------------------------------------------------------------------NEW------------------------------------------------------------

function GuideArrowPanel:UpdateByType(data)
	local dataList = string.split(data[1], ",")
	if dataList == nil then
		return
	end
	local guideType = dataList[1]

	--潜规则,只有DialogueWithMask能和其他类型搭配，其他类型之间都是互斥
	if guideType ~= GuideConfig.DialogueWithMask then
		self:ClearAllUI()
	end

	if guideType == GuideConfig.Mask then
		self:Mask(dataList)

	elseif guideType == GuideConfig.NoMask then
		self:NoMask(dataList)

	elseif guideType == GuideConfig.Dialogue then
		self:Dialogue(data, dataList)

	elseif guideType == GuideConfig.DialogueWithMask then
		self:DialogueWithMask(data,dataList)
	end

	-- LoggerHelper.Log("指引数据！！！")
	-- LoggerHelper.Log(data)
end

function GuideArrowPanel:Mask(dataList)
	local LayerName = dataList[2]
	local targetPath = dataList[3]
	local contentTextId = tostring(dataList[4])
	local offsetX = tonumber(dataList[5])
	local offsetY = tonumber(dataList[6])
	local direction = dataList[7]
	local circleOffsetX = tonumber(dataList[8] or 0) 
	local circleOffsetY = tonumber(dataList[9] or 0) 
	local npcWidht = tonumber(dataList[10] or 0) 
	local npcHeight = tonumber(dataList[11] or 0) 
	local fxScale = tonumber(dataList[12] or 1)
	local isCentre = tonumber(dataList[13] or 0)
	local nameList = string.split(targetPath, "/")
	local name = nameList[2]
	local pos = nil
	local anchoredPos = nil
	local targetWidth = nil
	local targetHeight = nil
	local isBlack = true
	local posHit = nil
	local anchoredPosHit = nil
	local targetWidthHit = nil
	local targetHeightHit = nil

	self._containerArrow:SetActive(true)
	if LayerName == 'NPC' then
		pos = self:GetNpcPosition(tonumber(targetPath), circleOffsetX, circleOffsetY)
		targetWidth = npcWidht
		targetHeight = npcHeight
		self._containerArrow:SetPostion(pos)

		posHit = pos
		targetWidthHit = targetWidth
		targetHeightHit = targetHeight
	else
		anchoredPos, pos, targetWidth, targetHeight = self:GetUIPosition(LayerName, targetPath ,circleOffsetX, circleOffsetY)
		self._containerArrow:SetAnchoredPosition(anchoredPos)

		local hitPath = targetPath..Hit_Name
		if self:ExistTarget(LayerName, hitPath) then
			targetPath = hitPath
		end
		anchoredPosHit, posHit, targetWidthHit, targetHeightHit = self:GetUIPosition(LayerName, targetPath ,circleOffsetX, circleOffsetY)
	end
	self._containerArrow:SetParticleScale(fxScale,targetWidth,targetHeight)
	self._containerArrow:SetText(direction,contentTextId,offsetX,offsetY)

	self._containerMask:SetActive(true)
	self._containerMask:SetBgColor(isBlack)
	self._containerMask:UpdateMask(pos, targetWidth, targetHeight, 0,0,LayerName, name, isCentre)

	self._containerMaskHit:SetActive(true)
	self._containerMaskHit:UpdateMask(posHit, targetWidthHit, targetHeightHit, 0,0,LayerName, name, isCentre)

	GameManager.GuideManager.ShowGuide(true)
	self:InitTimer()
end

--与Mask的区别：没有遮罩,指引组件移到目标路径下
function GuideArrowPanel:NoMask(dataList)
	local LayerName = dataList[2]
	local targetPath = dataList[3]
	local contentTextId = tostring(dataList[4])
	local offsetX = tonumber(dataList[5])
	local offsetY = tonumber(dataList[6])
	local direction = dataList[7]
	local circleOffsetX = tonumber(dataList[8] or 0) 
	local circleOffsetY = tonumber(dataList[9] or 0) 
	local npcWidht = tonumber(dataList[10] or 0) 
	local npcHeight = tonumber(dataList[11] or 0) 
	local fxScale = tonumber(dataList[12] or 1)

	local targetGo, targetWidth, targetHeight, targetRect = self:GetTargetObj(LayerName, targetPath)
	if targetGo == nil then
		return 
	end

	self._containerArrow:SetActive(true)
	self._containerArrow:SetParent(targetGo, circleOffsetX, circleOffsetY)
	self._containerArrow:SetParticleScale(fxScale,targetWidth,targetHeight)
	self._containerArrow:SetText(direction,contentTextId,offsetX,offsetY)
end

function GuideArrowPanel:Dialogue(data, dataList)
	--金铃子
	if dataList ~= nil then
		local textId = tonumber(dataList[2])
		local offsetX = tonumber(dataList[3])
		local offsetY = tonumber(dataList[4])
		local roleId = tonumber(dataList[5])

		local modelX = tonumber(dataList[10])
		local modelY = tonumber(dataList[11])
		local modelZ = tonumber(dataList[12])
		local modelScale = tonumber(dataList[13])
		local direction = dataList[14]

		self._containerDialogue:SetActive(true)
		self._containerDialogue:UpdateDialogue(textId,offsetX,offsetY,roleId,modelX,modelY,modelZ,modelScale,direction)
	end
	
	--长方形框
	if dataList[6] ~= nil then
		local emptyShineX = tonumber(dataList[6]) or -10000
		local emptyShineY = tonumber(dataList[7])
		local emptyShineW = tonumber(dataList[8])
		local emptyShineH = tonumber(dataList[9])
		self._containerDialogue:SetEmptyShine(emptyShineX,emptyShineY,emptyShineW,emptyShineH)
	end

	--显示黑色背景 + 亮的范围
	if data[2] ~= nil and data[2] ~= "nil" then
		local maskDataList = string.split(data[2], ",")
		local posX = maskDataList[1]
		local posY = maskDataList[2]
		local targetWidth = maskDataList[3]
		local targetHeight = maskDataList[4]
		self._containerMask:SetActive(true)
		self._containerMask:SetBgColor(true)
		self._containerMask:UpdateMaskRegion(posX, posY, targetWidth, targetHeight)

		self._containerMaskHit:SetActive(true)
		self._containerMaskHit:UpdateMaskRegion(posX, posY, targetWidth, targetHeight)
	else
		self._containerDialogue:SetFullScreenMask(true)
	end

	--特效位置
	if data[4] ~= nil then
		local fxDataList = string.split(data[4], ",")
		local posX = fxDataList[1]
		local posY = fxDataList[2]
		local pos = Vector3.New(posX, posY, 0)
		self._containerArrow:SetActive(true)
		self._containerArrow:SetPostion(pos)
	end
	
	--箭头位置 + 角度
	if data[3] ~= nil then
		local arrowDataList = string.split(data[3], ",")
		local posX = arrowDataList[1]
		local posY = arrowDataList[2]
		local rotation = arrowDataList[3]
		self._containerDialogue:SetArrow(posX, posY, rotation)
	end
end

function GuideArrowPanel:DialogueWithMask(data,dataList)
	local textId = tonumber(dataList[2])
	local offsetX = tonumber(dataList[3])
	local offsetY = tonumber(dataList[4])
	local roleId = tonumber(dataList[5])
	local emptyShineX = tonumber(dataList[6]) or -10000
	local emptyShineY = tonumber(dataList[7])
	local emptyShineW = tonumber(dataList[8])
	local emptyShineH = tonumber(dataList[9])

	local modelX = tonumber(dataList[10])
	local modelY = tonumber(dataList[11])
	local modelZ = tonumber(dataList[12])
	local modelScale = tonumber(dataList[13])
	local direction = dataList[14]

	self._containerDialogue:SetActive(true)
	self._containerDialogue:UpdateDialogue(textId,offsetX,offsetY,roleId,modelX,modelY,modelZ,modelScale,direction)
	self._containerDialogue:SetEmptyShine(emptyShineX,emptyShineY,emptyShineW,emptyShineH)
	self._containerDialogue:SetFullScreenMask(false)

	--箭头位置 + 角度
	if data[3] ~= nil then
		local arrowDataList = string.split(data[3], ",")
		local posX = arrowDataList[1]
		local posY = arrowDataList[2]
		local rotation = arrowDataList[3]
		self._containerDialogue:SetArrow(posX, posY, rotation)
	end
end

-------------------------------------------------------------------------------------工具类-------------------------------------------------------------
function GuideArrowPanel:GetNpcPosition(npcId, circleOffsetX, circleOffsetY)
	local npcList = GameWorld.GetEntityByNpcId(npcId)
    local npcEntityId = 0
	if #npcList > 0 then
		npcEntityId = npcList[1].id
	end

	local npc = GameWorld.GetEntity(npcEntityId)
	if npc ~= nil then
		local position = npc:GetPosition()
	end
	if position == nil then
        return
	end
	
	position = GameWorld.WorldToScreenPoint(position)
	-- 使用UI摄像机转换到UGUI的世界坐标
	local layerUIMain = GUILayerManager.GetUILayerTransform('LayerUIMain')
	local layerUIMainCanvas = layerUIMain.transform:GetComponent('Canvas')
	local screenPos = layerUIMainCanvas.worldCamera:ScreenToWorldPoint(position)
	local pos = screenPos + Vector3.New(circleOffsetX,circleOffsetY,0)

	return pos
end

--获取ui位置
function GuideArrowPanel:GetUIPosition(LayerName, targetPath, circleOffsetX, circleOffsetY)
	local targetGo, targetWidth, targetHeight, targetRect = self:GetTargetObj(LayerName, targetPath)
	if targetGo == nil then --不存在指向的目标
        return
	end
	
    local layerUIMain = GUILayerManager.GetUILayerTransform(LayerName)
	local layerUIMainCanvas = layerUIMain.transform:GetComponent('Canvas')

	local targetPos = self:TransformToCanvasLocalPosition(targetGo.transform, layerUIMainCanvas)
	targetPos = targetPos + Vector2.New(GameWorld.CANVAS_WIDTH / 2, GameWorld.CANVAS_HEIGHT / 2)
	targetPos.x = targetPos.x + (targetWidth * (self._containerArrow.transform.pivot.x - targetRect.pivot.x)) + circleOffsetX
    targetPos.y = targetPos.y + (targetHeight * (self._containerArrow.transform.pivot.y - targetRect.pivot.y)) + circleOffsetY
    targetPos.z = 0
	local anchoredPos = targetPos - Vector3.New(50 - targetWidth / 2,50 - targetHeight / 2,0)
	return anchoredPos, targetPos, targetWidth, targetHeight
end

function GuideArrowPanel:TransformToCanvasLocalPosition(targetRect, uicanvas)
	--得出屏幕坐标
    local screenPos = uicanvas.worldCamera:WorldToScreenPoint(targetRect.position)
    local localPos = nil 
	local result = nil
	--UGUI屏幕坐标转UI坐标
	result, localPos = RectTransformUtility.ScreenPointToLocalPointInRectangle(uicanvas.transform, screenPos, uicanvas.worldCamera, localPos)
    return localPos
end

--获取目标Obj
function GuideArrowPanel:GetTargetObj(LayerName, targetPath)
	local uiRoot = GUIManager.GetUIRoot()
	local targetGo = UIComponentUtil.FindChild(uiRoot, LayerName .. targetPath)
	if targetGo == nil then
		return nil
	end

	local targetRect = targetGo.transform:GetComponent('RectTransform')
    local targetWidth = targetRect.rect.width
	local targetHeight = targetRect.rect.height
	return targetGo, targetWidth, targetHeight, targetRect
end

function GuideArrowPanel:ExistTarget(LayerName, targetPath)
	local uiRoot = GUIManager.GetUIRoot()
	return UIComponentUtil.ExistChild(uiRoot, LayerName .. targetPath)
end