PKModeModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function PKModeModule.Init()
	GUIManager.AddPanel(PanelsConfig.PKMode,"Panel_PKMode",GUILayer.LayerUIPanel,"Modules.ModulePKMode.PKModePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return PKModeModule