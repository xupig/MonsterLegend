local PKModePanel = Class.PKModePanel(ClassTypes.BasePanel)
local PanelBGType = GameConfig.PanelsConfig.PanelBGType

local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local PKModeManager = PlayerManager.PKModeManager
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

--override
function PKModePanel:Awake()
    self:InitView()
end

--override
function PKModePanel:OnDestroy()
end

--override
function PKModePanel:OnClose()
	self:RemoveEventListeners()
end

function PKModePanel:OnShow()
    self:AddEventListeners()
end

function PKModePanel:AddEventListeners()

end

function PKModePanel:RemoveEventListeners()

end

function PKModePanel:InitView()
    self._backButton = self:FindChildGO("Container_Tips/Button_Close")
    self._csBH:AddClick(self._backButton,function () self:OnBackButtonClick() end)
    self._oneButton = self:FindChildGO("Container_Tips/Container_Tip1/Button_Action")
    self._csBH:AddClick(self._oneButton,function () self:OnButtonOneClick() end)
    self._twoButton = self:FindChildGO("Container_Tips/Container_Tip2/Button_Action")
    self._csBH:AddClick(self._twoButton,function () self:OnButtonTwoClick() end)
    self._threeButton = self:FindChildGO("Container_Tips/Container_Tip3/Button_Action")
    self._csBH:AddClick(self._threeButton,function () self:OnButtonThreeClick() end)
end

function PKModePanel:OnBackButtonClick()
    GUIManager.ClosePanel(PanelsConfig.PKMode)
end

function PKModePanel:OnButtonOneClick()
    PKModeManager:ChangeModeReq(public_config.PK_MODE_PEACE)
    self:OnBackButtonClick()
end

function PKModePanel:OnButtonTwoClick()
    local first = (LocalSetting.settingData.FirstChangePKMode ~= 1)
    if first then
        LocalSetting.settingData.FirstChangePKMode = 1
        LocalSetting:Save()
        GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContent(58774),
        function()
            GUIManager.ClosePanel(PanelsConfig.MessageBox)
            PKModeManager:ChangeModeReq(public_config.PK_MODE_FORCE)
            self:OnBackButtonClick()
        end,
        function()GUIManager.ClosePanel(PanelsConfig.MessageBox) end)
    else
        PKModeManager:ChangeModeReq(public_config.PK_MODE_FORCE)
        self:OnBackButtonClick()
    end
end

function PKModePanel:OnButtonThreeClick()
    local first = (LocalSetting.settingData.FirstChangePKMode ~= 1)
    if first then
        LocalSetting.settingData.FirstChangePKMode = 1
        LocalSetting:Save()
        GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContent(58774),
        function()
            GUIManager.ClosePanel(PanelsConfig.MessageBox)
            PKModeManager:ChangeModeReq(public_config.PK_MODE_ALL)
            self:OnBackButtonClick()
        end,
        function()GUIManager.ClosePanel(PanelsConfig.MessageBox) end)
    else
        PKModeManager:ChangeModeReq(public_config.PK_MODE_ALL)
        self:OnBackButtonClick()
    end
end

-- -- 0 没有  1 暗红底板  2 蓝色底板  3 遮罩底板
function PKModePanel:CommonBGType()
    return PanelBGType.NoBG
end