-- LoginModule.lua
LoginModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function LoginModule.Init()
    GUIManager.AddPanel(PanelsConfig.Login, "Panel_Login", 
    GUILayer.LayerUIPanel, "Modules.ModuleLogin.LoginPanel", true, 
    PanelsConfig.EXTEND_TO_FIT, nil, false, 
    PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    -- GUIManager.AddPanel(PanelsConfig.SelectServer, "Panel_SelectServer", GUILayer.LayerUIPanel, "Modules.ModuleLogin.SelectServerPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return LoginModule



