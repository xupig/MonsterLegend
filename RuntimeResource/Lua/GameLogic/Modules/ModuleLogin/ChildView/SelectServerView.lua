-- SelectServerView.lua
require "Modules.ModuleLogin.ChildComponent.ServerTypeItem"
require "Modules.ModuleLogin.ChildComponent.ServerStateItem"
require "Modules.ModuleLogin.ChildComponent.ServerInfoItem"

local SelectServerView = Class.SelectServerView(ClassTypes.BaseComponent)
SelectServerView.interface = GameConfig.ComponentsConfig.Component
SelectServerView.classPath = "Modules.ModuleLogin.ChildView.SelectServerView"
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local XMLManager = GameManager.XMLManager
local XMLParser = GameWorld.XMLParser
local ServerListManager = GameManager.ServerListManager
local LoginManager = GameManager.LoginManager
local ServerTypeItem = ClassTypes.ServerTypeItem
local ServerStateItem = ClassTypes.ServerStateItem
local ServerInfoItem = ClassTypes.ServerInfoItem

local ShowInfoData = {{67,572}, {68,573}, {69,574}, {70,575}}  --{icon,中文id}

function SelectServerView:Awake()
    self._preSelected = -1
    self:InitView()
end
    
function SelectServerView:OnDestroy()

end

function SelectServerView:ShowView(data)
    LoggerHelper.Log("__________SelectServerView:ShowView__________")
    local serverGroupList = ServerListManager:GetServerGroupList()
    self._list:SetDataList(serverGroupList)

    local listData = ServerListManager:GetServerListByGroup(serverGroupList[1].groupId)
    if #listData > 0 then
        self:OnListItemClicked(0)
    else
        self:OnListItemClicked(1)
    end
end

function SelectServerView:CloseView()

end

function SelectServerView:OnEnable()

end

function SelectServerView:OnDisable()

end

function SelectServerView:InitView()
    self.buttonClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self.buttonClose, function( )self:OnCloseClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._listlView = scrollView
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(true)
    self._list:SetItemType(ServerTypeItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(6, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)
    local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
    
    local scrollViewInfo, listInfo = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Info")
    self._listInfo = listInfo
    self._listlViewInfo = scrollViewInfo
    self._listlViewInfo:SetHorizontalMove(true)
    self._listlViewInfo:SetVerticalMove(false)
    self._listlViewInfo:SetScrollRectEnable(false)
    self._listInfo:SetItemType(ServerStateItem)
    self._listInfo:SetPadding(0, 0, 0, 0)
    self._listInfo:SetGap(120, 0)
    self._listInfo:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    self._listInfo:SetDataList(ShowInfoData)

    local scrollViewServer, listServer = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Server")
    self._listServer = listServer
    self._listlViewServer = scrollViewServer
    self._listlViewServer:SetHorizontalMove(false)
    self._listlViewServer:SetVerticalMove(true)
    self._listServer:SetItemType(ServerInfoItem)
    self._listServer:SetPadding(0, 0, 0, 0)
    self._listServer:SetGap(5, 10)
    self._listServer:SetDirection(UIList.DirectionTopToDown, 2, 1)
    local itemClickedCBServer = 
	function(idx)
		self:OnServerListItemClicked(idx)
	end
    self._listServer:SetItemClickedCB(itemClickedCBServer)

end

function SelectServerView:OnCloseClick()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_23)
    self.gameObject:SetActive(false)
end

function SelectServerView:SetupServerList(serverData)
    self._serverListData = serverData
end

function SelectServerView:OnListItemClicked(idx)
    if self._preSelected == idx then
        return
    end
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelect(false)
    end
    self._list:GetItem(idx):ShowSelect(true)
    self._preSelected = idx
    local data = self._list:GetDataByIndex(idx)
    local listData = ServerListManager:GetServerListByGroup(tonumber(data.groupId))
    self._listServer:SetDataList(listData)
end

function SelectServerView:OnServerListItemClicked(idx)
    local data = self._listServer:GetDataByIndex(idx)
    ServerListManager:SetSelectServerId(data)
    self:OnCloseClick()
end