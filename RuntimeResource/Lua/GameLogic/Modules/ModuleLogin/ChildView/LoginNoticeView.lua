-- LoginNoticeView.lua
require "Modules.ModuleLogin.ChildComponent.LoginNoticeItem"
require "UIComponent.Base.UIScrollView"

local LoginNoticeView = Class.LoginNoticeView(ClassTypes.BaseComponent)
LoginNoticeView.interface = GameConfig.ComponentsConfig.Component
LoginNoticeView.classPath = "Modules.ModuleLogin.ChildView.LoginNoticeView"
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil
local LoginNoticeItem = ClassTypes.LoginNoticeItem
local XMLParser = GameWorld.XMLParser
local LocalSetting = GameConfig.LocalSetting

function LoginNoticeView:Awake()
    self:InitView()
    self._preSelected = -1
end
    
function LoginNoticeView:OnDestroy()
    
end

function LoginNoticeView:ShowView(data)
    self.data = ServerListManager:GetNoticeData()
    self._list:SetDataList(self.data)
    self:OnListItemClicked(0)
end

function LoginNoticeView:CloseView()

end

function LoginNoticeView:InitView()
    self.buttonClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self.buttonClose, function( )self:OnCloseClick() end)
    self.buttonOK = self:FindChildGO("Button_OK")
    self._csBH:AddClick(self.buttonOK, function( )self:OnOKClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._listlView = scrollView
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(true)
    self._list:SetItemType(LoginNoticeItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(6, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)
    local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
	self._list:SetItemClickedCB(itemClickedCB)

    self._scroll = UIComponentUtil.FindChild(self.gameObject, "ScrollView")
    self._scrollView = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
    self._scrollContent = self:FindChildGO("ScrollView/Mask/Content")
    self._rectTransform = self._scrollContent:GetComponent(typeof(UnityEngine.RectTransform))
    self._txtDesc = self:GetChildComponent("ScrollView/Mask/Content/Text", 'TextMeshWrapper')
    self._rectTransformScroll = self._scroll:GetComponent(typeof(UnityEngine.RectTransform))
    self._height = self._rectTransform.sizeDelta.y
    self._width = self._rectTransform.sizeDelta.x
end

function LoginNoticeView:OnListItemClicked(idx)
    if self._preSelected == idx then
        return
    end
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelect(false)
    end
    self._list:GetItem(idx):ShowSelect(true)
    self._preSelected = idx
    local data = self._list:GetDataByIndex(idx)
    self._scrollView:ResetContentPosition()
    self:RefreshText(data)
end

function LoginNoticeView:OnCloseClick()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_21)
    self.gameObject:SetActive(false)
end

function LoginNoticeView:OnOKClick()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_22)
    self.gameObject:SetActive(false)
end

function LoginNoticeView:RefreshText(data)
    local notice = data.notice[1]
    local content = tostring(notice.content)
    content = string.gsub( content,'@{','<' )
    content = string.gsub( content,'}@','>' )

    self._txtDesc.text = content
    if self._txtDesc.preferredHeight > self._height then
        self._rectTransform.sizeDelta = Vector2(self._width, self._txtDesc.preferredHeight)
    else
        self._rectTransform.sizeDelta = Vector2(self._width, self._height)
    end
end