-- ServerItem.lua
local ServerItem = Class.ServerItem(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
ServerItem.interface = GameConfig.ComponentsConfig.Component
ServerItem.classPath = "Modules.ModuleLogin.ChildComponent.ServerItem"

function ServerItem:Awake()
    UIListItem.Awake(self)
    self.skillPos = 0
    self._text = self:GetChildComponent("Text_ServerName", "TextMeshWrapper")
end

function ServerItem:OnDestroy() 
    self._skillIcon = nil
end

--override
function ServerItem:OnRefreshData(data)
    self._text.text = tostring(data)
end

