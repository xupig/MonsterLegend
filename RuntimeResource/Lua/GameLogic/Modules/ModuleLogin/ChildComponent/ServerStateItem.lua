-- ServerStateItem.lua
local ServerStateItem = Class.ServerStateItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
ServerStateItem.interface = GameConfig.ComponentsConfig.Component
ServerStateItem.classPath = "Modules.ModuleLogin.ChildComponent.ServerStateItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList

function ServerStateItem:Awake()
    self:InitView()
end

function ServerStateItem:OnDestroy() 

end

function ServerStateItem:InitView() 
    self._icon = self:FindChildGO("Container_Icon")
    self._text = self:GetChildComponent("Text_Info", "TextMeshWrapper")
end

--override {icon,中文id}
function ServerStateItem:OnRefreshData(data)
    self._text.text = LanguageDataHelper.CreateContent(data[2])
    GameWorld.AddIcon(self._icon, data[1])
end