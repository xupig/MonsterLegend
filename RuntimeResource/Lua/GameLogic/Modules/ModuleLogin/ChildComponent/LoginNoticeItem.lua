-- LoginNoticeItem.lua
local LoginNoticeItem = Class.LoginNoticeItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
LoginNoticeItem.interface = GameConfig.ComponentsConfig.Component
LoginNoticeItem.classPath = "Modules.ModuleLogin.ChildComponent.LoginNoticeItem"

local UIList = ClassTypes.UIList

function LoginNoticeItem:Awake()
    self:InitView()
end

function LoginNoticeItem:OnDestroy() 

end

function LoginNoticeItem:InitView() 
    self._selectedImage = self:FindChildGO("Image_Selected")
    self._text = self:GetChildComponent("Text", "TextMeshWrapper")
end

--override
function LoginNoticeItem:OnRefreshData(data)
    self._text.text = data.tabname
end

function LoginNoticeItem:ShowSelect(state)
    self._selectedImage:SetActive(state)
end