-- ServerInfoItem.lua
local ServerInfoItem = Class.ServerInfoItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
ServerInfoItem.interface = GameConfig.ComponentsConfig.Component
ServerInfoItem.classPath = "Modules.ModuleLogin.ChildComponent.ServerInfoItem"

local UIList = ClassTypes.UIList
local ServerListManager = GameManager.ServerListManager
local ServerStateType = GameConfig.EnumType.ServerStateType

local Pos_To_Chinese = {[ServerStateType.Close]=572, [ServerStateType.Fluency]=573, [ServerStateType.Crowd]=574, [ServerStateType.Hot]=575}

function ServerInfoItem:Awake()
    self:InitView()
end

function ServerInfoItem:OnDestroy() 

end

function ServerInfoItem:InitView()
    self._icon = self:FindChildGO("Container_Icon")
    self._newImage = self:FindChildGO("Image_New")
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._typeText = self:GetChildComponent("Text_Type", "TextMeshWrapper")
end

--override {serverId}
function ServerInfoItem:OnRefreshData(data)
    local serverData = ServerListManager:GetServerDataById(data)
    self._nameText.text = serverData.serverName
    local isNew = ServerListManager:GetServerNewByFlag(serverData.serverFlag)
    local isRecommend = ServerListManager:GetServerRecommendByFlag(serverData.serverFlag)
    local state = ServerListManager:GetServerStateByFlag(serverData.serverFlag)
    local iconId = ServerListManager:GetServerStateIcon(serverData.serverFlag)
    self._newImage:SetActive(isNew)
    self._typeText.text = LanguageDataHelper.CreateContent(Pos_To_Chinese[state])
    GameWorld.AddIcon(self._icon, iconId)
end