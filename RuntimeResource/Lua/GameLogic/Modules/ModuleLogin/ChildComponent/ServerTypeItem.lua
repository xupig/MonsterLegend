-- ServerTypeItem.lua
local ServerTypeItem = Class.ServerTypeItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
ServerTypeItem.interface = GameConfig.ComponentsConfig.Component
ServerTypeItem.classPath = "Modules.ModuleLogin.ChildComponent.ServerTypeItem"

local UIList = ClassTypes.UIList
local StringStyleUtil = GameUtil.StringStyleUtil

function ServerTypeItem:Awake()
    self:InitView()
end

function ServerTypeItem:OnDestroy() 

end

function ServerTypeItem:InitView() 
    self._selectedImage = self:FindChildGO("Image_Selected")
    self._text = self:GetChildComponent("Image_BG/Text", "TextMeshWrapper")
    self._selectedText = self:GetChildComponent("Image_Selected/Text", "TextMeshWrapper")
end

--override {[sort],[groupName],[groupId]}
function ServerTypeItem:OnRefreshData(data)
    self.data = data
    self._text.text = data.groupName
    self._selectedText.text = data.groupName
end

function ServerTypeItem:ShowSelect(state)
    self._selectedImage:SetActive(state)
end