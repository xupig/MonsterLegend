-- LoginPanel.lua
require "Modules.ModuleLogin.ChildView.SelectServerView"
require "Modules.ModuleLogin.ChildView.LoginNoticeView"

local LoginPanel = Class.LoginPanel(ClassTypes.BasePanel)
local LocalSetting = GameConfig.LocalSetting
local LoginManager = GameManager.LoginManager
local KeyboardManager = GameManager.KeyboardManager
local Vector2 = Vector2
local EventDispatcher = EventDispatcher
local ServerListManager = GameManager.ServerListManager
local CameraClearFlags = UnityEngine.CameraClearFlags
-- local ProgressBar = GameWorld.ProgressBar
local LuaGameState = GameWorld.LuaGameState
local GUIManager = GameManager.GUIManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local SelectServerView = ClassTypes.SelectServerView
local LoginNoticeView = ClassTypes.LoginNoticeView
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
--local XLogoImage = GameMain.XLogoImage
local Time_Out_Sec = 20

function LoginPanel:Awake()
    self:InitView()
    self:InitLastSetting()
    self:InitVars()
    ServerListManager:LoadNoticeFromWeb()
    ServerListManager:GetLocalLastServerData()
    ServerListManager:GetServerListFromWeb()
    ServerListManager:GetServerGroupListFromWeb()
    self:IsNeedInputUserName()
end

function LoginPanel:InitView()
    self._rect = self:GetComponent('RectTransform')
    self._userNameInputGo = self:FindChildGO('Inputs/InputFieldUsename')
    self._userNameInput = self:GetChildComponent("Inputs/InputFieldUsename", "InputFieldMeshWrapper")
    self.buttonQuick = self:FindChildGO("Buttons/ButtonQuick");
    self.buttonLogin = self:FindChildGO("Buttons/ButtonLogin");
    self.buttonServer = self:FindChildGO("Inputs/ButtonServer");
    self._textServerName = self:GetChildComponent("Inputs/Container_ServerInfo/Text_ServerName", 'TextMeshWrapper')
    self._icon = self:FindChildGO('Inputs/Container_ServerInfo/Container_Icon')
    self._inputs = self:FindChildGO('Inputs')
    self._csBH:AddClick(self.buttonQuick, function()self:OnClick() end)
    self._csBH:AddClick(self.buttonLogin, function()self:OnLoginClick() end)
    self._csBH:AddClick(self.buttonServer, function()self:OnServerClick() end)
    
    self.buttonNotice = self:FindChildGO("Buttons/Button_Notice")
    self._csBH:AddClick(self.buttonNotice, function()self:OnNoticeClick() end)
    self.buttonAccount = self:FindChildGO("Buttons/Button_Account")
    self._csBH:AddClick(self.buttonAccount, function()self:OnAccountClick() end)
    self.buttonAccount:SetActive(false)
    
    self._noticeGO = self:FindChildGO("Container_Notice")
    self._selectServerGO = self:FindChildGO("Container_SelectServer")
    self._noticeView = self:AddChildLuaUIComponent("Container_Notice", LoginNoticeView)
    self._selectServerView = self:AddChildLuaUIComponent("Container_SelectServer", SelectServerView)
    self._noticeGO:SetActive(false)
    self._selectServerGO:SetActive(false)
    
    self._waitingImage = self:FindChildGO("Image_Waiting")
    self._waitingText = self:GetChildComponent("Image_Waiting/Text_Desc", 'TextMeshWrapper')
    self._waitingImage:SetActive(false)
    
    self._bg = self:FindChildGO('Image_BG')
    self._bg:SetActive(false)
    
    self._logoImage = self:FindChildGO("Image_Logo")
    self._logo = self._logoImage:AddComponent(typeof(GameMain.XLogoImage))
    self._copyRightText = self:GetChildComponent("Text_CopyRight", 'TextMeshWrapper')
    self._onSetSelectServer = function()self:OnSetSelectServer() end
    self._onShowNoticeView = function()self:OnShowNoticeView() end
    self._onShowWaitingImage = function(state)self:ShowWaitingImage(state) end
end

function LoginPanel:IsNeedInputUserName()
    if GameLoader.SystemConfig.IsUsePlatformSdk then
        self._userNameInputGo:SetActive(false)
    else
        self._userNameInputGo:SetActive(true)
    end
end

function LoginPanel:InitVars()
    -- self._sendHttping = false
    self._lastClickLoginTime = -50000
    self._CheckSDKInitState = function()self:CheckSDKInitState() end
end

function LoginPanel:Start()
end

function LoginPanel:OnDestroy()
    self.buttonQuick = nil
    self.buttonLogin = nil
    self.buttonServer = nil
    self._textServerName = nil
    self._csBH = nil
end

function LoginPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.OnSetSelectServer, self._onSetSelectServer)
    EventDispatcher:AddEventListener(GameEvents.OnShowNoticeView, self._onShowNoticeView)
    EventDispatcher:AddEventListener(GameEvents.OnShowWaitingImage, self._onShowWaitingImage)
end

function LoginPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.OnSetSelectServer, self._onSetSelectServer)
    EventDispatcher:RemoveEventListener(GameEvents.OnShowNoticeView, self._onShowNoticeView)
    EventDispatcher:RemoveEventListener(GameEvents.OnShowWaitingImage, self._onShowWaitingImage)
end

function LoginPanel:OnSetSelectServer()
    local id = ServerListManager:SelectedServerId()
    self:SetServerInfo(id)
end

function LoginPanel:OnShowNoticeView()
    if GameLoader.SystemConfig.GetValueInCfg('HideLogin') == "1" then
        return
    end
    self:OnNoticeClick()
end

function LoginPanel:OnEnable()
    
end

function LoginPanel:SetData(data)
    -- LoggerHelper.Error('====LoginPanel:SetData================')
    self:OnInitPanelData()
end

function LoginPanel:OnShow(data)
-- LoggerHelper.Error('====LoginPanel:OnShow================')
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_28)
    self:OnInitPanelData()
end

function LoginPanel:OnInitPanelData()
    TimerHeap:DelTimer(self._sdkInitState)
    TimerHeap:DelTimer(self._timer)
    self:AddEventListeners()
    self:ShowButtons()
    self:ShowWaitingImage(false)
    self.buttonLogin:SetActive(true)
    self.buttonQuick:SetActive(false)
    self._inputs:SetActive(false)
    -- if GameLoader.SystemConfig.IsUsePlatformSdk then
    self:OnLoginClick()

    self._logo:SetImage()
    self:ShowCopyRight()
end

function LoginPanel:OnDisable()
    -- GUIManager._uiCameraComponent.clearFlags = CameraClearFlags.Depth
    -- LoggerHelper.Error('====LoginPanel:OnDisable================')
    self:RemoveEventListeners()
    TimerHeap:DelTimer(self._sdkInitState)
    TimerHeap:DelTimer(self._timer)
end

function LoginPanel:ShowCopyRight()
    local copyRight = GameLoader.SystemConfig.GetValueInCfg('CopyRight')
    if copyRight then
        self._copyRightText.text = copyRight
    end
end

function LoginPanel:ShowButtons()
    if GameLoader.SystemConfig.GetValueInCfg('HideLogin') == "1" then
        self.buttonNotice:SetActive(false)
        --self.buttonAccount:SetActive(false)
        self._bg:SetActive(true)
    else
        self.buttonNotice:SetActive(true)
        --self.buttonAccount:SetActive(true)
        self._bg:SetActive(false)
    end
end

function LoginPanel:InitLastSetting()
    local userAccount = LocalSetting.settingData.UserAccount
    self:SetUserName(tostring(userAccount))
-- LoggerHelper.Log("+++++++++++++++++++", tostring(userAccount))
end

function LoginPanel:OnLoginClick()
    if not(GameLoader.SystemConfig.IsUsePlatformSdk) then

        local isClose = ServerListManager:SelectedCloseState()
        if isClose then
            GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_29)
            self._lastClickLoginTime = 0
            local value = {["text"] = ServerListManager:SelectedDetail()}
            GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
            return
        end

    end
    
    if GameLoader.SystemConfig.IsUsePlatformSdk then
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_30)
        self:ShowWaitingImage(true)
        TimerHeap:DelTimer(self._sdkInitState)
        --定时检查SDK是否初始化完成
        self._sdkInitState = TimerHeap:AddSecTimer(0, 1, 0, self._CheckSDKInitState)
    -- self:OnLoginSDK()
    else
        self.buttonLogin:SetActive(false)
        self.buttonQuick:SetActive(true)
        self._inputs:SetActive(true)
    end
end

function LoginPanel:CheckSDKInitState()
    -- 0初始化未完成  1 成功  2失败
    local initState = GameWorld.GetSDKInitState()
    GameWorld.WriteInfoLog("初始化状态:" .. tostring(initState))
    if initState == 1 then
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_31)
        TimerHeap:DelTimer(self._sdkInitState)
        self:OnLoginSDK()
        self:ShowWaitingImage(false)
    elseif initState == 2 then
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_32)
        TimerHeap:DelTimer(self._sdkInitState)
        self:ShowWaitingImage(false)
    end
end

function LoginPanel:OnClick()
    -- local isClose = ServerListManager:SelectedCloseState()
    -- if isClose then
    --     self._lastClickLoginTime = 0
    --     local value = {["text"] = ServerListManager:SelectedDetail()}
    --     GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
    --     return
    -- end
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_33)
    if ServerListManager:SelectedServerId() == 0 then
        GameWorld.WriteInfoLog("服务器列表为空")
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_34)
        return
    end

    
    if GameLoader.SystemConfig.IsUsePlatformSdk then
        
        if ServerListManager:SelectedCloseState() then --关服了,要走白名单 登录流程
            self:OnCheckSeverIsClose()
        else
            self:OnSelectedServerLogin()
        end
    else
        EventDispatcher:TriggerEvent(GameEvents.Login, self._userNameInput.text, 2)
    end
    
    self:ShowWaitingImage(true)
--GameManager.LoginManager:Login()
-- ProgressBar:Show();
-- ProgressBar:UpdateProgress(0.88, false);
end

function LoginPanel:OnNoticeClick()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_35)
    self._noticeGO:SetActive(true)
    self._noticeView:ShowView()
end

function LoginPanel:OnAccountClick()

end


function LoginPanel:OnLoginWebDone(result)
    --OnLoginWebDone:0,192.168.11.175,11011,7a98522d87eb341b9c44c62c0d1bb216
    GameWorld.WriteInfoLog("OnLoginWebDonex:" .. result)
    local lstRes = string.split(result, ';')
    
    local lst = string.split(lstRes[1], ',')
    local errorId = tonumber(lst[1])
    if errorId == 0 then
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_36)
        GameManager.LoginManager.baseIP = lst[2]
        GameManager.LoginManager.basePort = tonumber(lst[3])
        GameManager.LoginManager.baseLoginKey = lst[4]
        GameManager.LoginManager.baseSDKJson = lstRes[2]
        GameWorld.WriteInfoLog("OnLoginWebDone baseIP:" .. GameManager.LoginManager.baseIP
            .. "OnLoginWebDone basePort:" .. GameManager.LoginManager.basePort
            .. "OnLoginWebDone baseLoginKey:" .. GameManager.LoginManager.baseLoginKey
            .. "OnLoginWebDone baseSDKJson:" .. GameManager.LoginManager.baseSDKJson)
        -- GameManager.LoginManager:LoginPC()
        -- GameWorld.SetSwitchAccountCB()

        GameWorld.OnLoginRspSDK(GameManager.LoginManager.baseSDKJson, function(code)
            if code == '1' then --成功登录游戏

                -- --验证白名单
                -- if ServerListManager:SelectedCloseState() then --关服了,看下白名单
                --     EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, true)
                --     GameManager.WhiteListManager.IsInWhiteList()
                --     return
                -- end

                GameManager.LoginManager:LoginPC()
            else
                GameWorld.LoginOutToLoginPanel()
            end
        end)
    
    else
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_37, errorId)
        EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, false)
        GameWorld.WriteInfoLog("errorID:" .. tostring(errorId))
        local text = LanguageDataHelper.CreateContent(584, errorId)
        local value = {["confirmCB"] = function()
            GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
            --GameWorld.LoginOutSDK()
            self:SetData()
        end,
        ["cancelCB"] = function()
            GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
            --GameWorld.LoginOutSDK()
            self:SetData()
        end,
        ["text"] = text}
        GameManager.GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
    end
end

function LoginPanel:OnLoginWebFail(result)
    GameWorld.WriteInfoLog("OnLoginWebFailx:" .. result)
    EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, false)
    local cfg = GlobalParamsHelper.GetParamValue(884)
    local text = ""
    if cfg[tonumber(result)] == nil then
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = result
        text = LanguageDataHelper.CreateContent(584, argsTable)
    else
        text = LanguageDataHelper.CreateContent(cfg[result])
    end
    local value = {["confirmCB"] = function()
        GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
        -- GameWorld.LoginOutSDK()

    end,
    ["cancelCB"] = function()
        GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
        -- GameWorld.LoginOutSDK()
    end,
    ["text"] = text}
    GameManager.GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
    local data = {}
--一下文字后面加入中文表
-- data.type = 1
-- data.content = LanguageDataHelper.CreateContentWithArgs(6934)--"服务器链接失败，请稍后再试！"
-- GameManager.GUIManager.ShowPanel(PanelsConfig.CommonTemp, {[1]=1, [2]=data})
end


function LoginPanel:OnServerClick()
    --GameManager.GUIManager.ShowPanel(PanelsConfig.SelectServer)
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_38)
    self._selectServerGO:SetActive(true)
    self._selectServerView:ShowView()
end

function LoginPanel:ShowWaitingImage(state)
    self._waitingImage:SetActive(state)
    if state then
        self._endTime = Time_Out_Sec
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnWaitingUpdate() end)
    else
        if self._timer then
            TimerHeap:DelTimer(self._timer)
        end
    end
end

function LoginPanel:OnWaitingUpdate()
    self._endTime = self._endTime - 1
    self._waitingText.text = LanguageDataHelper.CreateContent(585, self._endTime)
    if self._endTime < 0 then
        self:ShowWaitingImage(false)
    end
end

function LoginPanel:Update()

end

function LoginPanel:Timer()
--LoggerHelper.Log("LoginPanel:Timer")
end

function LoginPanel:SetUserName(userName)
    self._userNameInput.text = userName
end

function LoginPanel:SetServerInfo()
    self._textServerName.text = ServerListManager:SelectedServerName()
    local flag = ServerListManager:SelectedServerFlag()
    local iconId = ServerListManager:GetServerStateIcon(flag)
    GameWorld.AddIcon(self._icon, iconId)
end


function LoginPanel:OnLoginSDK()
    if Time.realtimeSinceStartup - self._lastClickLoginTime < 5 then
        return
    end
    self._lastClickLoginTime = Time.realtimeSinceStartup
    
    -- GameWorld.WriteInfoLog("登录状态:开始登录SDK")
    GameWorld.LoginSDK(function(uid, sid, gameId, channelId, appId, extra)
        self._loginUid = uid
        self._loginSid = sid
        self._loginGameId = gameId
        self._loginChannelId = channelId
        self._loginAppId = appId
        self._loginExtra = extra
        
        self.buttonLogin:SetActive(false)
        self.buttonQuick:SetActive(true)
        self._inputs:SetActive(true)
    -- GameWorld.WriteInfoLog("登录状态:开始登录SDK 登录成功")
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_39)
    end, function(code)
        GameWorld.WriteInfoLog("login code:" .. code)
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_40, code)
        local data = {}
        if code == 33 then
            -- //otherState.text = "提交扩展参数成功";
            data.content = LanguageDataHelper.CreateContent(6927)
        elseif code == 1 then
            -- //初始化成功
            data.content = LanguageDataHelper.CreateContent(6928)
        elseif code == 2 then
            -- //初始化失败，请重新初始化
            data.content = LanguageDataHelper.CreateContent(6929)
        elseif code == 5 then
            -- //登录失败
            self:ShowConfirm(LanguageDataHelper.CreateContent(6930))
            self:SendHttpCB()
            self._lastClickLoginTime = 0
        elseif code == 10 then
            -- //otherState.text = "支付成功，请等服务器回调";
            self:ShowConfirm(LanguageDataHelper.CreateContent(6931))
        elseif code == 11 then
            -- //otherState.text = "支付失败";
            self:ShowConfirm(LanguageDataHelper.CreateContent(6932))
        end
    end)
end

-- 选服后登录游戏
function LoginPanel:OnSelectedServerLogin()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_41)
    local uid = self._loginUid
    local sid = self._loginSid
    local gameId = self._loginGameId
    local channelId = self._loginChannelId
    local appId = self._loginAppId
    local extra = self._loginExtra
    
    GameWorld.WriteInfoLog("uid: " .. uid .. " sid: " .. sid .. " gameId: " .. gameId .. " channelId: " .. channelId .. " appId: " .. appId .. " extra: " .. extra)
    -- loginUID = uid
    -- LocalSetting.settingData.UserAccount = self._userNameInput.text
    local serverId = ServerListManager:SelectedServerId()
    local serverIp = ServerListManager:SelectedServerIp()
    local serverPort = ServerListManager:SelectedServerPort()
    LocalSetting.settingData.SelectedServerID = serverId
    LocalSetting.settingData.ServerIp = serverIp
    LocalSetting.settingData.ServerPort = serverPort
    LocalSetting.settingData.ChannelId = channelId

    local timestamp = tostring(os.time())
    -- GameWorld.UploadLog({"log_type", "5", "reach_login_time", timestamp, "account_id", username})
    local account = uid
    LocalSetting.settingData.UserAccount = account
    -- LoggerHelper.Log("account: " .. account)
    LocalSetting:Save()
    GameWorld.SetServerId(serverId .. " " .. serverIp .. " " .. serverPort)
    GameWorld.SetAccount(uid)
    
    GameWorld.SetSwitchAccountCB()

    --GameWorld.WriteInfoLog("account:"..account)
    --sign = md5(account+username+channelID+token+"abc")
    --string template = http://192.168.11.175/cgi-bin/login.cgi?token=xxx&sign=xxx&appID=xxx&channelID=xxx;
    -- local ip = ServerListManager:SelectedServerIp()--"s999.zhj.15166.com" --"192.168.11.175"
    local sign = ''
    local template = ''
    local url = ''
    if LuaGameState.PlatfromIsAndroid() then
        sign = GameWorld.StrToMd5String(gameId .. channelId .. appId .. sid .. uid .. extra .. serverId .. "akj%^Ja@39#)10!+")
        template = GameLoader.SystemConfig.LoginDomainName .. "gameId=%s&channelId=%s&appId=%s&sid=%s&userId=%s&serverId=%s&sign=%s&extra=%s"
        url = string.format(template, serverIp, gameId, channelId, appId, sid, uid, serverId, sign, extra)
    else
        sign = GameWorld.StrToMd5String(gameId .. channelId .. appId .. sid .. uid .. extra .. serverId .. "akj%^Ja@39#)10!+")
        template = GameLoader.SystemConfig.LoginDomainName ..  "gameId=%s&channelId=%s&appId=%s&sid=%s&userId=%s&serverId=%s&sign=%s&extra=%s"
        url = string.format(template, serverIp, gameId, channelId, appId, sid, uid, serverId, sign, extra)
    end
    --GameWorld.WriteInfoLog("sign:"..sign)
    -- local template = "http://%s/cgi-bin/login?token=%s&sign=%s&uid=%s&username=%s&account=%s&channelID=%s"
    -- local url = string.format(template, ip, currentToken, sign, Uid, username, account, channelIdx)
    --url="http://192.168.11.175/cgi-bin/login.cgi?token=1b997dff399493490b64efc9e5f8894b&sign=c87ee8fc8f19e7984f2acca83e7955bd&uid=1&username=1.test&account=22222&channelID=9998"
    GameWorld.WriteInfoLog("LoginWebx:" .. url)
    local onDone = function(result)self:OnLoginWebDone(result) end
    local onFail = function(result)self:OnLoginWebFail(result) end
    
    -- self:SendHttpCB()
    GameWorld.GetHttpUrl(url, onDone, onFail)
    -- self._sendHttping = true
    -- if self._sendHttpTimer ~= nil then
    --     TimerHeap:DelTimer(self._sendHttpTimer)
    -- end
    -- self._sendHttpTimer = TimerHeap:AddSecTimer(8, 0, 0,self._sendHttpCB)
    self._lastClickLoginTime = Time.realtimeSinceStartup
end


-- ====================================================关服时  走的登录流程=============================================================================
function LoginPanel:OnCheckSeverIsClose()
    local uid = self._loginUid
    local sid = self._loginSid
    local gameId = self._loginGameId
    local channelId = self._loginChannelId
    local appId = self._loginAppId
    local extra = self._loginExtra
    
    GameWorld.WriteInfoLog("uid: " .. uid .. " sid: " .. sid .. " gameId: " .. gameId .. " channelId: " .. channelId .. " appId: " .. appId .. " extra: " .. extra)

    local serverId = ServerListManager:SelectedServerId()
    local serverIp = ServerListManager:SelectedServerIp()
    local serverPort = ServerListManager:SelectedServerPort()
    LocalSetting.settingData.SelectedServerID = serverId
    LocalSetting.settingData.ServerIp = serverIp
    LocalSetting.settingData.ServerPort = serverPort
    LocalSetting.settingData.ChannelId = channelId

    local timestamp = tostring(os.time())
    local account = uid
    LocalSetting.settingData.UserAccount = account
    LocalSetting:Save()
    GameWorld.SetServerId(serverId .. " " .. serverIp .. " " .. serverPort)
    GameWorld.SetAccount(uid)
    
    GameWorld.SetSwitchAccountCB()

    local sign = ''
    local template = ''
    local url = ''
    local serverCloseLoginDomainName = GameLoader.SystemConfig.GetValueInCfg('ServerCloseLoginDomainName')
    if LuaGameState.PlatfromIsAndroid() then
        sign = GameWorld.StrToMd5String(gameId .. channelId .. appId .. sid .. uid .. extra .. serverId .. "akj%^Ja@39#)10!+")
        template = serverCloseLoginDomainName .. "gameId=%s&channelId=%s&appId=%s&sid=%s&userId=%s&serverId=%s&sign=%s&extra=%s"
        url = string.format(template, serverIp, gameId, channelId, appId, sid, uid, serverId, sign, extra)
    else
        sign = GameWorld.StrToMd5String(gameId .. channelId .. appId .. sid .. uid .. extra .. serverId .. "akj%^Ja@39#)10!+")
        template = serverCloseLoginDomainName ..  "gameId=%s&channelId=%s&appId=%s&sid=%s&userId=%s&serverId=%s&sign=%s&extra=%s"
        url = string.format(template, serverIp, gameId, channelId, appId, sid, uid, serverId, sign, extra)
    end

    GameWorld.WriteInfoLog("LoginWebx:" .. url)
    local onDone = function(result)self:OnServerCloseLoginWebDone(result) end
    local onFail = function(result)self:OnLoginWebFail(result) end
    
    GameWorld.GetHttpUrl(url, onDone, onFail)
    self._lastClickLoginTime = Time.realtimeSinceStartup
end



function LoginPanel:OnServerCloseLoginWebDone(result)
    GameWorld.WriteInfoLog("OnServerCloseLoginWebDone:" .. result)
    -- local lstRes = string.split(result, ';')
    
    -- local lst = string.split(lstRes[1], ',')
    local errorId = tonumber(result) or 0
    if errorId == 0 then
        -- GameManager.LoginManager.baseIP = lst[2]
        -- GameManager.LoginManager.basePort = tonumber(lst[3])
        -- GameManager.LoginManager.baseLoginKey = lst[4]
        -- GameManager.LoginManager.baseSDKJson = lstRes[2]
        -- GameWorld.WriteInfoLog("OnLoginWebDone baseIP:" .. GameManager.LoginManager.baseIP
        --     .. "OnLoginWebDone basePort:" .. GameManager.LoginManager.basePort
        --     .. "OnLoginWebDone baseLoginKey:" .. GameManager.LoginManager.baseLoginKey
        --     .. "OnLoginWebDone baseSDKJson:" .. GameManager.LoginManager.baseSDKJson)
        
        -- GameWorld.OnServerCloseLogin(GameManager.LoginManager.baseSDKJson)
        local checkUserId = result
        -- 验证白名单
        EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, true)
        GameManager.WhiteListManager.IsInWhiteList(checkUserId,function()  self:OnSelectedServerLogin()   end)

    else
        EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, false)
        GameWorld.WriteInfoLog("errorID:" .. tostring(errorId))
        local text = LanguageDataHelper.CreateContent(584, errorId)
        local value = {["confirmCB"] = function()
            GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
            --GameWorld.LoginOutSDK()
            self:SetData()
        end,
        ["cancelCB"] = function()
            GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
            --GameWorld.LoginOutSDK()
            self:SetData()
        end,
        ["text"] = text}
        GameManager.GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
    end
end
