-- OpenOtherPanel.lua
local OpenOtherPanel = Class.OpenOtherPanel(ClassTypes.BasePanel)


require "Modules.ModuleOpenOther.ChildView.PerfectLoverView"
require "Modules.ModuleOpenOther.ChildView.UnionHegemonyView"
require "Modules.ModuleOpenOther.ChildView.CollectWordView"
require "Modules.ModuleOpenOther.ChildView.OpenHappyView"
require "Modules.ModuleOpenOther.ChildView.OpenSectView"

local PerfectLoverView = ClassTypes.PerfectLoverView
local UnionHegemonyView = ClassTypes.UnionHegemonyView
local CollectWordView = ClassTypes.CollectWordView
local OpenSectView = ClassTypes.OpenSectView
local OpenHappyView = ClassTypes.OpenHappyView
local DateTimeUtil = GameUtil.DateTimeUtil
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local ServiceActivityType = GameConfig.EnumType.ServiceActivityType
local WelfareRankWayDataHelper = GameDataHelper.WelfareRankWayDataHelper
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local OpenRankType = GameConfig.EnumType.OpenRankType
local public_config = GameWorld.public_config


function OpenOtherPanel:Awake()
    self:InitComps()
    self:InitCallBack()
    self._onRefreshTab = function()self:RefreshTab() end
end


function OpenOtherPanel:InitText()
end

--初始化回调
function OpenOtherPanel:InitCallBack()
    -- self:SetTabClickAfterUpdateSecTabCB(function(index)
    --     self:SelectTab(index)
    -- end)
    self:SetSecondTabClickCallBack(function(index)
        self:SelectSecTab(index)
    end)

    self:SetCustomizeFunctionOpenTextCB(function(index,secIndex)
        self:OpenTips(index,secIndex)
    end)
end





function OpenOtherPanel:SelectSecTab(secIndex)
    --LoggerHelper.Error("SelectSecTab"..secIndex)

end

function OpenOtherPanel:OpenTips(index,secIndex)
        if secIndex == 4 then
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetFunctionOpenName(public_config.FUNCTION_ID_FREAK)) 
            if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_FREAK) then
                if ServiceActivityManager:GetIsOpenHappyValue() == -1 then
                    GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80916,argsTable))
                elseif ServiceActivityManager:GetIsOpenHappyValue() == -2 then
                    --LoggerHelper.Error("ServiceActivityManager:GetIsOpenHappyValue()")
                    GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80917,argsTable))
                end
            else
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80916,argsTable))
            end
        end

        if secIndex == 1 then
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetFunctionOpenName(public_config.FUNCTION_ID_COLLECT_WORD)) 
            if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_COLLECT_WORD) then
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80917,argsTable))
            else
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80916,argsTable))
            end
        end

        if secIndex == 5 then
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetFunctionOpenName(public_config.FUNCTION_ID_SECT)) 
            if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_SECT) then
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80917,argsTable))
            else
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80916,argsTable))
            end
        end

        if secIndex == 3 then
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetFunctionOpenName(public_config.FUNCTION_ID_PERFER_LOVER)) 
            if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_PERFER_LOVER) then
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80917,argsTable))
            else
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80916,argsTable))
            end
        end

        if secIndex == 2 then
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetFunctionOpenName(public_config.FUNCTION_ID_GUILD_FIGHT_OPENSER)) 
            if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_GUILD_FIGHT_OPENSER) then
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80917,argsTable))
            else
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(80916,argsTable))
            end
        end
    

    -- self._serTimeSetting = GlobalParamsHelper.GetParamValue(758)
    -- self._openRankTitle = GlobalParamsHelper.GetParamValue(813)
    -- if index == 2 then
    --         -- LoggerHelper.Log("OpenOtherPanel:OpenTips"..index..","..secIndex)
    --         local argsTable = LanguageDataHelper.GetArgsTable()
    --         argsTable["0"] = LanguageDataHelper.CreateContent(self._openRankTitle[secIndex])
    --         argsTable["1"] = self._serTimeSetting[secIndex][1]
    --         argsTable["2"] = self._serTimeSetting[secIndex][2] 
    --         GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76980,argsTable))
    -- end
end

function OpenOtherPanel:InitComps()
    self:InitView("Container_CollectWord", CollectWordView,1,1)
    self:InitView("Container_UnionHegemony", UnionHegemonyView,1,2)
    self:InitView("Container_PerfectLover", PerfectLoverView,1,3)
    self:InitView("Container_Happy", OpenHappyView,1,4)
    self:InitView("Container_OpenSect", OpenSectView,1,5)
    
--self:SetSecondTabClickCallBack(self._secTabCb)
end



function OpenOtherPanel:OnShow(data)
    self:RightBgIsActive(false)
    self:UpdateFunctionOpen()
    self:SetSecTabActive(1, FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_COLLECT_WORD) and ServiceActivityManager:GetIsOpenWord())
    self:SetSecTabActive(2, FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_GUILD_FIGHT_OPENSER) and ServiceActivityManager:GetIsOpenActivity())
    self:SetSecTabActive(3, FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_PERFER_LOVER) and ServiceActivityManager:GetIsOpenLoveOver())
    self:SetSecTabActive(4, FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_FREAK) and ServiceActivityManager:GetIsOpenHappy())
    self:SetSecTabActive(5, FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_SECT) and ServiceActivityManager:GetIsOpenSect())
    self:OnShowSelectTab(data)
end

function OpenOtherPanel:OnClose()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_service_activity_close_button")
end





function OpenOtherPanel:StructingViewInit()
    return true
end



function OpenOtherPanel:WinBGType()
    return PanelWinBGType.RedNewBG
end
