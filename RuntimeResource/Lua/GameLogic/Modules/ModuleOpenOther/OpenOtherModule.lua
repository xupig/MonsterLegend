-- OpenOtherModule.lua
OpenOtherModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function OpenOtherModule.Init()
    GUIManager.AddPanel(PanelsConfig.OpenOther, "Panel_OpenOther", GUILayer.LayerUIPanel, "Modules.ModuleOpenOther.OpenOtherPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)    
end

return OpenOtherModule