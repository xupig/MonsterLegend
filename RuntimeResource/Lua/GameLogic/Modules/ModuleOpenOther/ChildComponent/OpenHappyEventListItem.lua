-- OpenHappyEventListItem.lua
local UIListItem = ClassTypes.UIListItem
local OpenHappyEventListItem = Class.OpenHappyEventListItem(UIListItem)

OpenHappyEventListItem.interface = GameConfig.ComponentsConfig.Component
OpenHappyEventListItem.classPath = "Modules.ModuleOpenOther.ChildComponent.OpenHappyEventListItem"

require "Modules.ModuleOpenOther.ChildComponent.OpenHappyRewardItem"
local OpenHappyRewardItem = ClassTypes.OpenHappyRewardItem

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local OpenHappyConfig = GameConfig.OpenHappyConfig

function OpenHappyEventListItem:Awake()
    self:InitComps()
end

function OpenHappyEventListItem:OnDestroy()
end

function OpenHappyEventListItem:InitComps()
    self._txtName = self:GetChildComponent("Text_Tips", 'TextMeshWrapper')
    self._txtTimes = self:GetChildComponent("Text _Times", 'TextMeshWrapper')
    self._icon = self:FindChildGO("Container_Icon")
    
    self._buttonEvent = self:FindChildGO("Container_State/Button_Event")
    self._csBH:AddClick(self._buttonEvent, function()self:OnButtonEventClick() end)

    self._goTextNotOpen = self:FindChildGO("Container_State/Text_Not_Open")
    self._txtNotOpen = self:GetChildComponent("Container_State/Text_Not_Open", 'TextMeshWrapper')
    self._imgFinish = self:FindChildGO("Container_State/Image_Finish")
    
    self._buttonEvent:SetActive(false)
    self._goTextNotOpen:SetActive(false)
    self._imgFinish:SetActive(false)

end

function OpenHappyEventListItem:OnButtonEventClick()
    if not self._data then
        return
    end
    PlayerManager.OpenHappyManager:DoFollowEvent(self._data:GetFollow())
end

function OpenHappyEventListItem:OnRefreshData(data) -- OpenHappyEventData
    self._data = data
    GameWorld.AddIcon(self._icon, data:GetIcon())
    self._txtName.text = data:GetNameStr()
    self._txtTimes.text = data:GetTimesStr()
    
    -- LoggerHelper.Log({
    --     data:GetNameStr(),
    --     })

    local state = data:GetState()
    if state == OpenHappyConfig.EventState_NotOpen then
        self._buttonEvent:SetActive(false)
        self._goTextNotOpen:SetActive(true)
        self._txtNotOpen.text = data:GetNotOpenStr()
        self._imgFinish:SetActive(false)

    elseif state == OpenHappyConfig.EventState_Open then
        self._buttonEvent:SetActive(true)
        self._goTextNotOpen:SetActive(false)
        self._imgFinish:SetActive(false)
    else
        local finishStr = data:GetFinishStr()
        if finishStr ~= "" then
            self._buttonEvent:SetActive(false)
            self._goTextNotOpen:SetActive(true)
            self._txtNotOpen.text = finishStr
            self._imgFinish:SetActive(false)
        else
            self._buttonEvent:SetActive(false)
            self._goTextNotOpen:SetActive(false)
            self._imgFinish:SetActive(true)
        end
    end
end
