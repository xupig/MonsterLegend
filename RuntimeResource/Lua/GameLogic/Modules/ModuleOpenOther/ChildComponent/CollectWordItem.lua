-- CollectWordItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

local UIListItem = ClassTypes.UIListItem
local CollectWordItem = Class.CollectWordItem(UIListItem)
CollectWordItem.interface = GameConfig.ComponentsConfig.Component
CollectWordItem.classPath = "Modules.ModuleOpenOther.ChildComponent.CollectWordItem"
require "Modules.ModuleOpenOther.ChildComponent.CollectIconTypeItem"
local CollectIconTypeItem = ClassTypes.CollectIconTypeItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local WordCollectionDataHelper = GameDataHelper.WordCollectionDataHelper
local UIParticle = ClassTypes.UIParticle
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local UIList = ClassTypes.UIList
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local BagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local TipsManager = GameManager.TipsManager
local XImageFlowLight = GameMain.XImageFlowLight

function CollectWordItem:Awake()
    self._txtDesc = self:GetChildComponent("Container_State/Text_Desc", 'TextMeshWrapper')
    
    self._goButtonFlowLight = self:AddChildComponent("Container_State/Button_Get_Reward", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Container_State/Button_Get_Reward")
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)


    self._btnAnnounceRewardGetWrapper = self:GetChildComponent("Container_State/Button_Get_Reward", "ButtonWrapper")
    self._imgRed = self:FindChildGO("Container_State/Button_Get_Reward/Image_Red")
    -- self._iconContainer = self:FindChildGO("Container_Icon")
    -- self._icon = ItemManager():GetLuaUIComponent(nil, self._iconContainer)

    --self._btnAnnounceRewardGetWrapper.interactable = true
    self:SetButtonActive(true)
    self:InitCom()
end

function CollectWordItem:OnDestroy()

end

function CollectWordItem:SetButtonActive(flag)
    self._btnAnnounceRewardGetWrapper.interactable = flag 
    self._goButtonFlowLight.enabled = flag
end

function CollectWordItem:OnGetRewardClick()
    if self._id then
        --LoggerHelper.Log("CollectWordItem:OnGetRewardClick========== .."..self._id)
        ServiceActivityManager:SendCollectWordReq(self._id)

    end
end





function CollectWordItem:AddFriendClick()

end



function CollectWordItem:InitCom()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._iconList = list
    self._iconView = scrollView
    self._iconView:SetHorizontalMove(false)
    self._iconView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._iconList:SetItemType(CollectIconTypeItem)
    self._iconList:SetPadding(0, 0, 0, 0)
    self._iconList:SetGap(-10, 0)
    self._iconList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end





function CollectWordItem:OnRefreshData(id)
    if id then
        self._id = id
        local argsTableRank = LanguageDataHelper.GetArgsTable()
        local wordTab = ServiceActivityManager:GetCollectWordTime()
        local time = WordCollectionDataHelper:GetTime(id)
        local wordTime = WordCollectionDataHelper:GetTime(id)


        argsTableRank["0"] = wordTab[id]
        
        local allSvrData = ServiceActivityData:GetAllSerNumData()
        if allSvrData then
            for k,v in pairs(allSvrData) do
                if k==id then
                    argsTableRank["0"] = v
                end
            end
        end
        argsTableRank["1"] = time[2]
        if wordTime[1] == 1 then
            self._txtDesc.text = LanguageDataHelper.CreateContent(76968, argsTableRank)
        elseif wordTime[1] == 2 then
            self._txtDesc.text = LanguageDataHelper.CreateContent(76969, argsTableRank)
        end
        
        

        
        
        local _iconList = {}          
        local costTab = WordCollectionDataHelper:GetCost(id)
        for i, v in ipairs(costTab) do
            local iconId = v[1]
            local num = v[2]
            local type = 1
            local _iconItem = {_id = iconId, _num = num,_type = type}
            table.insert( _iconList, _iconItem)
        end

        local iconId = -1
        local num = -1
        local type = -1
        local _iconItem = {_id = iconId, _num = num,_type = type}
        table.insert( _iconList, _iconItem)
        local reward = WordCollectionDataHelper:GetReward(id)
        local iconId = reward[1][1]
        local num = reward[1][2]
        local type = 2
        local _iconItem = {_id = iconId, _num = num,_type = type}
        table.insert( _iconList, _iconItem)

        self:SetButtonActive(true)
        --self._btnAnnounceRewardGetWrapper.interactable =  true
        local costTab = WordCollectionDataHelper:GetCost(id)
        for i, v in ipairs(costTab) do
            local bagCount = BagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(v[1])
            --LoggerHelper.Log("CollectWordItem:_iconList========== .." ..bagCount)
            if bagCount < v[2] then
                self:SetButtonActive(false)
                --self._btnAnnounceRewardGetWrapper.interactable = false
            end
        end

        if self._btnAnnounceRewardGetWrapper.interactable ==  true then
            if wordTab[id] == time[2] then
                self:SetButtonActive(false)
                --self._btnAnnounceRewardGetWrapper.interactable = false
            else 
                self:SetButtonActive(true)
                --self._btnAnnounceRewardGetWrapper.interactable = true
            end
        end

        self._imgRed:SetActive(self._btnAnnounceRewardGetWrapper.interactable)

        self._iconList:SetDataList(_iconList)
    end
end
