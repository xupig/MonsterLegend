-- OpenHappyRewardItem.lua
local UIListItem = ClassTypes.UIListItem
local OpenHappyRewardItem = Class.OpenHappyRewardItem(UIListItem)

OpenHappyRewardItem.interface = GameConfig.ComponentsConfig.Component
OpenHappyRewardItem.classPath = "Modules.ModuleOpenOther.ChildComponent.OpenHappyRewardItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function OpenHappyRewardItem:Awake()
    self:InitComps()
end

function OpenHappyRewardItem:OnDestroy()
end

function OpenHappyRewardItem:InitComps()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._icon = ItemManager():GetLuaUIComponent(nil, self._iconContainer)
    self._icon:ActivateTipsById()
end

function OpenHappyRewardItem:OnRefreshData(data)
    self._icon:SetItem(data.itemId, data.itemNum, 0, true)
end
