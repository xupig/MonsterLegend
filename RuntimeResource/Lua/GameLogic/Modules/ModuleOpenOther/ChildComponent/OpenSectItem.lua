-- OpenSectItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local OpenSectItem = Class.OpenSectItem(ClassTypes.UIListItem)
OpenSectItem.interface = GameConfig.ComponentsConfig.Component
OpenSectItem.classPath = "Modules.ModuleOpenOther.ChildComponent.OpenSectItem"
require "Modules.ModuleOpenOther.ChildComponent.UnionIconTypeItem"
local UnionIconTypeItem = ClassTypes.UnionIconTypeItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local UIList = ClassTypes.UIList
local GuildCallDataHelper = GameDataHelper.GuildCallDataHelper
local public_config = GameWorld.public_config
local XImageFlowLight = GameMain.XImageFlowLight
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local ServiceActivityManager = PlayerManager.ServiceActivityManager

function OpenSectItem:Awake()
    self._txtDesc = self:GetChildComponent("Text_Target_Desc", 'TextMeshWrapper')
    self._txtNumDesc = self:GetChildComponent("Text_Target_Num", 'TextMeshWrapper')
    self._containerState = self:FindChildGO("Container_State")
    self._containerState:SetActive(true)
    self._notCompleteImgGo = self:FindChildGO("Container_State/Image_No_Complete")
    self._alreadyGetImgGo = self:FindChildGO("Container_State/Image_Already_Get")
    --self._txtDesc = self:GetChildComponent("Text_Wear_Desc", 'TextMeshWrapper')

    self._goButtonFlowLight = self:AddChildComponent("Container_State/Button_Get_Reward", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Container_State/Button_Get_Reward")

    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)
    self:InitCom()
end

function OpenSectItem:OnDestroy()

end

function OpenSectItem:InitCom()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._iconList = list
    self._iconView = scrollView
    self._iconView:SetHorizontalMove(false)
    self._iconView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._iconList:SetItemType(UnionIconTypeItem)
    self._iconList:SetPadding(0, 0, 0, 0)
    self._iconList:SetGap(10, 10)
    self._iconList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function OpenSectItem:OnGetRewardClick()
    if self._id then
        ServiceActivityManager:SendGetRewardSectReq(self._id)
    end
end



function OpenSectItem:SetButtonActive(flag)
    self._getRewardButton:SetActive(flag)    
    self._goButtonFlowLight.enabled = flag
end

function OpenSectItem:OnRefreshData(id)
    if id then
        self._id = id
        self._txtDesc.text = LanguageDataHelper.CreateContent(GuildCallDataHelper:GetName(id))
        
        local rewardData = GuildCallDataHelper:GetReward(id)
        self._iconList:SetDataList(rewardData)
        local stateInfo =  ServiceActivityData:GetRewardStateSectTable()
        local stateNumInfo =  ServiceActivityData:GetRewardInfoSectTable()
        local taskUpdateInfo =  ServiceActivityData:GetTaskInfoSectTable()
        
        if stateNumInfo[id] then
            self._txtNumDesc.text = GuildCallDataHelper:GetCount(id)-stateNumInfo[id]
        else
            self._txtNumDesc.text = GuildCallDataHelper:GetCount(id)
        end
        
        if stateInfo[id] then
            if stateInfo[id][2] == 2 then
                self:SetButtonActive(true)
                self._notCompleteImgGo:SetActive(false)
                self._alreadyGetImgGo:SetActive(false)
            elseif stateInfo[id][2] == 3 then
                self:SetButtonActive(false)
                self._notCompleteImgGo:SetActive(false)
                self._alreadyGetImgGo:SetActive(true)
            else
                self:SetButtonActive(false)
                self._notCompleteImgGo:SetActive(true)
                self._alreadyGetImgGo:SetActive(false)
            end
        else
            self:SetButtonActive(false)
            self._notCompleteImgGo:SetActive(true)
            self._alreadyGetImgGo:SetActive(false)
        end

        if taskUpdateInfo[id] then
            if taskUpdateInfo[id][2] == 2 then
                self:SetButtonActive(true)
                self._notCompleteImgGo:SetActive(false)
                self._alreadyGetImgGo:SetActive(false)
            elseif taskUpdateInfo[id][2] == 3 then
                self:SetButtonActive(false)
                self._notCompleteImgGo:SetActive(false)
                self._alreadyGetImgGo:SetActive(true)
            end
        end


    end
end
