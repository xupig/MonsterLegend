-- PerfectLoveItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local PerfectLoveItem = Class.PerfectLoveItem(ClassTypes.UIListItem)
PerfectLoveItem.interface = GameConfig.ComponentsConfig.Component
PerfectLoveItem.classPath = "Modules.ModuleOpenOther.ChildComponent.PerfectLoveItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local XImageFlowLight = GameMain.XImageFlowLight
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData


function PerfectLoveItem:Awake()
    self._rankDesc = self:GetChildComponent("Text_Index", 'TextMeshWrapper')
    self._containerState = self:FindChildGO("Container_State")
    self._containerState:SetActive(true)
    self._notCompleteImgGo = self:FindChildGO("Container_State/Image_No_Complete")
    self._alreadyGetImgGo = self:FindChildGO("Container_State/Image_Already_Get")
end

function PerfectLoveItem:OnDestroy()

end


function PerfectLoveItem:SetButtonActive(flag)
    self._getRewardButton:SetActive(flag)    
    self._goButtonFlowLight.enabled = flag
end



function PerfectLoveItem:OnRefreshData(data)
    if data then
        self.data = data
        --self._data = data
        local rank = self:GetIndex() + 1
        local loveinfo =  ServiceActivityData:GetLoveInfoTable()
        --LoggerHelper.Error("PerfectLoveItem:OnRefreshData(data)"..PrintTable:TableToStr(loveinfo))

        if loveinfo[rank] then
            if loveinfo[rank] == 1 then
                self._notCompleteImgGo:SetActive(false)
                self._alreadyGetImgGo:SetActive(true)
            else
                self._notCompleteImgGo:SetActive(true)
                self._alreadyGetImgGo:SetActive(false)
            end
        else
            self._notCompleteImgGo:SetActive(true)
            self._alreadyGetImgGo:SetActive(false)
        end
        
        self._rankDesc.text = LanguageDataHelper.CreateContent(data[2][1])

    end
end

function PerfectLoveItem:OnGetRewardClick()

end
