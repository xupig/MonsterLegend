-- OpenHappyRewardListItem.lua
local UIListItem = ClassTypes.UIListItem
local OpenHappyRewardListItem = Class.OpenHappyRewardListItem(UIListItem)

OpenHappyRewardListItem.interface = GameConfig.ComponentsConfig.Component
OpenHappyRewardListItem.classPath = "Modules.ModuleOpenOther.ChildComponent.OpenHappyRewardListItem"

require "Modules.ModuleOpenOther.ChildComponent.OpenHappyRewardItem"
local OpenHappyRewardItem = ClassTypes.OpenHappyRewardItem

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local XImageFlowLight = GameMain.XImageFlowLight
local OpenHappyConfig = GameConfig.OpenHappyConfig

function OpenHappyRewardListItem:Awake()
    self:InitComps()
end

function OpenHappyRewardListItem:OnDestroy()
end

function OpenHappyRewardListItem:InitComps()
    self._txtDesc = self:GetChildComponent("Text", 'TextMeshWrapper')
    
    self._flowLight = self:AddChildComponent("Container_State/Button_Get_Reward", XImageFlowLight)
    self._buttonReward = self:FindChildGO("Container_State/Button_Get_Reward")
    self._csBH:AddClick(self._buttonReward, function()self:OnGetRewardClick() end)

    -- self._btnAnnounceRewardGetWrapper = self:GetChildComponent("Container_State/Button_Get_Reward", "ButtonWrapper")
    self._imgRed = self:FindChildGO("Container_State/Button_Get_Reward/Image_Red")
    
    self._imgAlreadyGet = self:FindChildGO("Container_State/Image_Already_Get")
    self._imgNotComplete = self:FindChildGO("Container_State/Image_No_Complete")
    
    -- self:SetButtonActive(true)
    self._buttonReward:SetActive(false)
    self._imgAlreadyGet:SetActive(false)
    self._imgNotComplete:SetActive(false)
    self._imgRed:SetActive(false)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._itemList = list
    scrollView:SetHorizontalMove(false)
    scrollView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._itemList:SetItemType(OpenHappyRewardItem)
    self._itemList:SetPadding(0, 0, 0, 0)
    self._itemList:SetGap(0, 0)
    self._itemList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

-- function OpenHappyRewardListItem:SetButtonActive(flag)
--     self._btnAnnounceRewardGetWrapper.interactable = flag 
--     self._flowLight.enabled = flag
-- end

function OpenHappyRewardListItem:OnGetRewardClick()
    if not self._data then
        return
    end
    PlayerManager.OpenHappyManager:RequestReward(self._data:GetId())
end

function OpenHappyRewardListItem:OnRefreshData(data) -- OpenHappyRewardData
    self._data = data
    self._itemList:SetDataList(data:GetItemList())
    self._txtDesc.text = data:GetNameStr()

    local state = data:GetState()
    self._imgRed:SetActive(state == OpenHappyConfig.State_CanReward)
    if state == OpenHappyConfig.State_Rewarded then
        self._buttonReward:SetActive(false)
        self._imgAlreadyGet:SetActive(true)
        self._imgNotComplete:SetActive(false)
    elseif state == OpenHappyConfig.State_CanReward then
        self._buttonReward:SetActive(true)
        self._imgAlreadyGet:SetActive(false)
        self._imgNotComplete:SetActive(false)
    else
        self._buttonReward:SetActive(false)
        self._imgAlreadyGet:SetActive(false)
        self._imgNotComplete:SetActive(true)
    end
end
