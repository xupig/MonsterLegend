local PerfectLoverView = Class.PerfectLoverView(ClassTypes.BaseComponent)

PerfectLoverView.interface = GameConfig.ComponentsConfig.Component
PerfectLoverView.classPath = "Modules.ModuleOpenOther.ChildView.PerfectLoverView"
require "Modules.ModuleOpenOther.ChildComponent.PerfectLoveItem"
local UIList = ClassTypes.UIList
local StringStyleUtil = GameUtil.StringStyleUtil
local PerfectLoveItem = ClassTypes.PerfectLoveItem
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local DateTimeUtil = GameUtil.DateTimeUtil
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local MarriageManager = PlayerManager.MarriageManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PlayerActionManager = GameManager.PlayerActionManager
local XArtNumber = GameMain.XArtNumber

function PerfectLoverView:Awake()
    self._txtTime = self:GetChildComponent("Text_Activity_Time", 'TextMeshWrapper')

    self._txtDesc1 = self:GetChildComponent("Container_Right/Text_1", 'TextMeshWrapper')
    self._txtDesc2 = self:GetChildComponent("Container_Right/Text_2", 'TextMeshWrapper')
    self._txtDesc3 = self:GetChildComponent("Container_Right/Text_3", 'TextMeshWrapper')

    self._iconContainer = self:FindChildGO("Container_Right/Container")
    self._getGoMarry = self:FindChildGO("Container_Right/Button_ToMarry")
    self._csBH:AddClick(self._getGoMarry, function()self:OnGoMarryClick() end)
    self:InitCom()
    self._onRefreshList = function()self:RefreshLoveList() end

    self._fashionNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fashionNumber:SetNumber(15000)
end


function PerfectLoverView:InitCom()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._rankList = list
    self._openRanklistlView = scrollView
    self._openRanklistlView:SetHorizontalMove(false)
    self._openRanklistlView:SetVerticalMove(true)
    self._rankList:SetItemType(PerfectLoveItem)
    self._rankList:SetPadding(0, 0, 0, 0)
    self._rankList:SetGap(10, 0)
    self._rankList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function PerfectLoverView:OnGoMarryClick()


    GameManager.GUIManager.ClosePanel(PanelsConfig.OpenOther)

    MarriageManager:GotoMarriage()
    --TaskCommonManager:GoToNpc(GlobalParamsHelper.GetParamValue(714))--结婚唯一指定NPC
end


function PerfectLoverView:OnDestroy()

end

function PerfectLoverView:ShowView()
    ServiceActivityManager:SendLoveInfoReq()
    local loveList = ServiceActivityData:GetLoveList()
    if loveList then
        if loveList[#loveList] then
            local argsRankTable = LanguageDataHelper.GetArgsTable()
            argsRankTable["0"] = StringStyleUtil.GetColorString(loveList[#loveList] or "", StringStyleUtil.yellow1) 
            self._txtDesc1.text = LanguageDataHelper.CreateContent(80894,argsRankTable)
        else
            self._txtDesc1.text = ""
            self._txtDesc3.text = ""
        end

        if loveList[#loveList-1] then
            local argsRankTable = LanguageDataHelper.GetArgsTable()
            argsRankTable["0"] = StringStyleUtil.GetColorString(loveList[#loveList-1] or "", StringStyleUtil.yellow1) 
            self._txtDesc2.text = LanguageDataHelper.CreateContent(80894,argsRankTable)
        else
            self._txtDesc2.text = ""
        end
    else 
        self._txtDesc1.text = ""
        self._txtDesc2.text = ""
        self._txtDesc3.text = LanguageDataHelper.CreateContent(80915)
    end


    if self._openRanklistlView then
        self._openRanklistlView:SetScrollRectState(true)
    end

    self._lovego = self:FindChildGO("Container_Right/Image_Love")
    GameWorld.AddIcon(self._lovego,13631)
    GameWorld.AddIcon(self._iconContainer,13636)
    ServiceActivityManager:SendMarryRankReq()

    
    local closeTimeSec = DateTimeUtil.GetOpenServerTime()+7*24*60*60
    local serTime = DateTimeUtil.GetOpenServerTime()

    -- LoggerHelper.Log("PerfectLoverView:ShowView()" ..DateTimeUtil.GetDateMonthDayStr(serTime))
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = DateTimeUtil.GetDateMonthDayStr(serTime)
    argsTable["1"] = DateTimeUtil.GetDateHourMinStr(serTime)
    argsTable["2"] = DateTimeUtil.GetDateMonthDayStr(closeTimeSec)
    argsTable["3"] = DateTimeUtil.GetDateHourMinStr(closeTimeSec)
    self._txtTime.text = LanguageDataHelper.CreateContent(76976, argsTable)
    
    EventDispatcher:AddEventListener(GameEvents.GetMarryRankGetInfoResp, self._onRefreshList)
    EventDispatcher:AddEventListener(GameEvents.GetLoveInfoResp, self._onRefreshList)
end

function PerfectLoverView:CloseView()
    if self._openRanklistlView then
        self._openRanklistlView:SetScrollRectState(false)
    end
    self._lovego=nil
    EventDispatcher:RemoveEventListener(GameEvents.GetMarryRankGetInfoResp, self._onRefreshList)
    EventDispatcher:RemoveEventListener(GameEvents.GetLoveInfoResp, self._onRefreshList)
end


function PerfectLoverView:RefreshLoveList()
    local lovelist = GlobalParamsHelper.GetNSortValue(973)
    self._rankList:SetDataList(lovelist)
    

    local loveList = ServiceActivityData:GetLoveList()
    if loveList then
        if loveList[#loveList] then
            local argsRankTable = LanguageDataHelper.GetArgsTable()
            argsRankTable["0"] =loveList[#loveList] or ""
            self._txtDesc1.text = LanguageDataHelper.CreateContent(80894,argsRankTable)
        else
            self._txtDesc1.text = ""
        end

        if loveList[#loveList-1] then
            local argsRankTable = LanguageDataHelper.GetArgsTable()
            argsRankTable["0"] =loveList[#loveList-1] or ""
            self._txtDesc2.text = LanguageDataHelper.CreateContent(80894,argsRankTable)
        else
            self._txtDesc2.text = ""
        end
    else 
        self._txtDesc1.text = ""
        self._txtDesc2.text = ""
    end
    --LoggerHelper.Error("UnionHegemonyItem:OnRefreshDataQ" .. PrintTable:TableToStr(lovelist))
end
