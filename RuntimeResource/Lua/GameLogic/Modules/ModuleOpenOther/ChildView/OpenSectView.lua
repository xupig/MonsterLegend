local OpenSectView = Class.OpenSectView(ClassTypes.BaseComponent)

OpenSectView.interface = GameConfig.ComponentsConfig.Component
OpenSectView.classPath = "Modules.ModuleOpenOther.ChildView.OpenSectView"
require "Modules.ModuleOpenOther.ChildComponent.OpenSectItem"

local OpenSectItem = ClassTypes.OpenSectItem
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GuildCallDataHelper = GameDataHelper.GuildCallDataHelper

local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildConquestOpenServerDataHelper = GameDataHelper.GuildConquestOpenServerDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local ServiceActivityManager = PlayerManager.ServiceActivityManager

function OpenSectView:Awake()
    -- self._txtUnionTime = self:GetChildComponent("Container_Left/Text_Activity_Time", 'TextMeshWrapper')
     self._txtTime = self:GetChildComponent("Container_Left/Text_Activity_Time", 'TextMeshWrapper')
     self:InitCom()
     self._refreshView = function()self:RefreshViewData() end
end
--override
function OpenSectView:OnDestroy() 

end

function OpenSectView:ShowView()

    ServiceActivityManager:SendRewardStateSectReq()
    ServiceActivityManager:SendRewardInfoSectReq()
    self:AddEventListeners()
    if self._unionlistlView then
        self._unionlistlView:SetScrollRectState(true)
    end

    self._icongo0 = self:FindChildGO("Container_Right/Image_Rignt")
    GameWorld.AddIcon(self._icongo0,13634)
   local ids = GuildCallDataHelper:GetAllId()
   self._unionList:SetDataList(ids)

   local closeTimeSec = DateTimeUtil.GetOpenServerTime()+7*24*60*60
   local serTime = DateTimeUtil.GetOpenServerTime()
   local argsTable = LanguageDataHelper.GetArgsTable()
   argsTable["0"] = DateTimeUtil.GetDateMonthDayStr(serTime)
   argsTable["1"] = DateTimeUtil.GetDateHourMinStr(serTime)
   argsTable["2"] = DateTimeUtil.GetDateMonthDayStr(closeTimeSec)
   argsTable["3"] = DateTimeUtil.GetDateHourMinStr(closeTimeSec)
   self._txtTime.text = LanguageDataHelper.CreateContent(76976, argsTable)
   --self._txtUnionTime.text = LanguageDataHelper.CreateContent(76973)
end

function OpenSectView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.GetRewardStateSectResp, self._refreshView)
    EventDispatcher:AddEventListener(GameEvents.GetRewardInfoSectResp, self._refreshView)
    EventDispatcher:AddEventListener(GameEvents.GetTaskInfoUpdateSectResp, self._refreshView)
end

function OpenSectView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.GetOpenSeverGetInfoResp, self._refreshView)
    EventDispatcher:RemoveEventListener(GameEvents.GetRewardInfoSectResp, self._refreshView)
    EventDispatcher:RemoveEventListener(GameEvents.GetTaskInfoUpdateSectResp, self._refreshView)
end
 

function OpenSectView:RefreshViewData()
    local ids = GuildCallDataHelper:GetAllId()
    self._unionList:SetDataList(ids)
end

function OpenSectView:CloseView()
    self:RemoveEventListeners()
    if self._unionlistlView then
        self._unionlistlView:SetScrollRectState(false)
    end
end

function OpenSectView:InitCom()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewUnion")
    self._unionList = list
    self._unionlistlView = scrollView
    self._unionlistlView:SetHorizontalMove(false)
    self._unionlistlView:SetVerticalMove(true)
    self._unionList:SetItemType(OpenSectItem)
    self._unionList:SetPadding(0, 0, 0, 0)
    self._unionList:SetGap(10, 0)
    self._unionList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function OpenSectView:OnListItemClicked(idx)

end

function OpenSectView:UpdateView()

end