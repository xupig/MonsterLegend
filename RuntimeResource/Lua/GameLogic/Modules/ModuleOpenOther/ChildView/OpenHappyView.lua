local OpenHappyView = Class.OpenHappyView(ClassTypes.BaseComponent)

OpenHappyView.interface = GameConfig.ComponentsConfig.Component
OpenHappyView.classPath = "Modules.ModuleOpenOther.ChildView.OpenHappyView"
require "Modules.ModuleOpenOther.ChildComponent.OpenHappyRewardListItem"
require "Modules.ModuleOpenOther.ChildComponent.OpenHappyEventListItem"
require "UIComponent.Extend.TipsCom"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local OpenHappyRewardListItem = ClassTypes.OpenHappyRewardListItem
local OpenHappyEventListItem = ClassTypes.OpenHappyEventListItem
local WordCollectionDataHelper = GameDataHelper.WordCollectionDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local TipsCom = ClassTypes.TipsCom
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local OpenHappyDataHelper = GameDataHelper.OpenHappyDataHelper
local PlayerDataManager = PlayerManager.PlayerDataManager

function OpenHappyView:Awake()
    self:InitView()
    self:InitFunc()
end

--override
function OpenHappyView:OnDestroy() 
end

-- override
function OpenHappyView:ShowView()
    self:AddEventListeners()
    self:RequestData()
    self:RefreshView()
end

-- override
function OpenHappyView:CloseView()
    self:RemoveEventListeners()
    self:RemoveRemainTimer()
end

function OpenHappyView:InitFunc()
    self._onStateRecv = function() self:RefreshView() end
    self._onRewarded = function() self:RefreshRewardList() end
end

function OpenHappyView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.FREAK_GET_INFO_REQ, self._onStateRecv)
    EventDispatcher:AddEventListener(GameEvents.FREAK_REWARD_REQ, self._onRewarded)
end

function OpenHappyView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.FREAK_GET_INFO_REQ, self._onStateRecv)
    EventDispatcher:RemoveEventListener(GameEvents.FREAK_REWARD_REQ, self._onRewarded)
end


function OpenHappyView:InitView()
    -- self._txtTips = self:GetChildComponent("Container_Bottom/Text_Tips", 'TextMeshWrapper')
    self._txtTime = self:GetChildComponent("Container_Bottom/Text_Time", 'TextMeshWrapper')
    self._txtValue = self:GetChildComponent("Container_Right/Text_Value", 'TextMeshWrapper')
    
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Left/ScrollViewList")
    self._rewardList = list
    self._rewardListView = scrollView
    self._rewardListView:SetHorizontalMove(false)
    self._rewardListView:SetVerticalMove(true)
    self._rewardList:SetItemType(OpenHappyRewardListItem)
    self._rewardList:SetPadding(0, 0, 0, 0)
    self._rewardList:SetGap(0, 0)
    self._rewardList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Right/ScrollViewList")
    self._eventList = list
    self._eventListView = scrollView
    self._eventListView:SetHorizontalMove(false)
    self._eventListView:SetVerticalMove(true)
    self._eventList:SetItemType(OpenHappyEventListItem)
    self._eventList:SetPadding(0, 0, 0, 0)
    self._eventList:SetGap(0, 0)
    self._eventList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function OpenHappyView:RequestData()
    PlayerManager.OpenHappyManager:RequestState()
end

function OpenHappyView:RefreshView()
    self:RefreshRewardList()
    self:RefreshEventList()

    self._txtValue.text = PlayerDataManager.openHappyData:GetPointValueStr()
    self._remainTime = PlayerDataManager.openHappyData:GetRemainTime()
    self:RefreshRemainTime()
    self:AddRemainTimer()
end

function OpenHappyView:OnPlayDescClick()
    self._tipsGO:SetActive(true)
end

function OpenHappyView:RefreshRewardList()
    self._rewardList:SetDataList(PlayerDataManager.openHappyData:GetRewardDataList())
end

function OpenHappyView:RefreshEventList()
    self._eventList:SetDataList(PlayerDataManager.openHappyData:GetEventDataList())
end

function OpenHappyView:AddRemainTimer()
    self:RemoveRemainTimer()
    if self._remainTime > 0 then
        self._RemainTimerId = TimerHeap:AddSecTimer(1, 1, 0, function() self:RefreshRemainTime() end)
    else
        self._txtTime.text = LanguageDataHelper.CreateContent(76951)
    end
end

function OpenHappyView:RemoveRemainTimer()
    if self._RemainTimerId then
        TimerHeap:DelTimer(self._RemainTimerId)
        self._RemainTimerId = nil
    end
end

function OpenHappyView:RefreshRemainTime()
    self._txtTime.text = PlayerDataManager.openHappyData:GetRemainTimeStr(self._remainTime)
    self._remainTime = self._remainTime - 1
    if self._remainTime < 0 then
        self:RemoveRemainTimer()
        self._txtTime.text = LanguageDataHelper.CreateContent(76951)
    end
end