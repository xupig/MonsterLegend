local CollectWordView = Class.CollectWordView(ClassTypes.BaseComponent)

CollectWordView.interface = GameConfig.ComponentsConfig.Component
CollectWordView.classPath = "Modules.ModuleOpenOther.ChildView.CollectWordView"
require "Modules.ModuleOpenOther.ChildComponent.CollectWordItem"
require "UIComponent.Extend.TipsCom"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local CollectWordItem = ClassTypes.CollectWordItem
local WordCollectionDataHelper = GameDataHelper.WordCollectionDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local TipsCom = ClassTypes.TipsCom
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData

function CollectWordView:Awake()
    self._playButton = self:FindChildGO("Container_Top/Button_Help")
    self._csBH:AddClick(self._playButton, function()self:OnPlayDescClick() end)
    self._txtTime = self:GetChildComponent("Container_Top/Text_Time", 'TextMeshWrapper')
    self._txtDesc = self:GetChildComponent("Container_Top/Text_Desc", 'TextMeshWrapper')
    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(76972))
    self._tipsGO:SetActive(false)
    self:InitView()
    self._onRefreshList = function()self:RefreshWordList() end
end
--override
function CollectWordView:OnDestroy() 

end

function CollectWordView:ShowView()
    self._txtDesc.text = LanguageDataHelper.CreateContentWithArgs(76972)
    if self._wordListView then
        self._wordListView:SetScrollRectState(true)
    end
    self._icongo = self:FindChildGO("Container_Top/Image_Bg")
    GameWorld.AddIcon(self._icongo,13632)

    ServiceActivityManager:SendAllSerNumReq()
    self:RefreshWordList()


    local closeTimeSec = DateTimeUtil.GetOpenServerTime()+7*24*60*60
    local serTime = DateTimeUtil.GetOpenServerTime()
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = DateTimeUtil.GetDateMonthDayStr(serTime)
    argsTable["1"] = DateTimeUtil.GetDateHourMinStr(serTime)
    argsTable["2"] = DateTimeUtil.GetDateMonthDayStr(closeTimeSec)
    argsTable["3"] = DateTimeUtil.GetDateHourMinStr(closeTimeSec)
    self._txtTime.text = LanguageDataHelper.CreateContent(76976, argsTable)
    EventDispatcher:AddEventListener(GameEvents.GetCollectWordResp, self._onRefreshList)
    EventDispatcher:AddEventListener(GameEvents.GetAllSerNumReq, self._onRefreshList)
    EventDispatcher:AddEventListener(GameEvents.ItemNumUpdate, self._onRefreshList)
end

function CollectWordView:OnPlayDescClick()
    self._tipsGO:SetActive(true)
end

function CollectWordView:RefreshWordList()
    local ids = WordCollectionDataHelper:GetAllId()
    self._wordList:SetDataList(ids)
end

function CollectWordView:CloseView()
    if self._wordListView then
        self._wordListView:SetScrollRectState(true)
    end
    self._icongo=nil
    EventDispatcher:RemoveEventListener(GameEvents.GetCollectWordResp, self._onRefreshList)
    EventDispatcher:RemoveEventListener(GameEvents.GetAllSerNumReq, self._onRefreshList)
    EventDispatcher:RemoveEventListener(GameEvents.ItemNumUpdate, self._onRefreshList)
end

function CollectWordView:InitDataView()

end

function CollectWordView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "WordScrollView")
    self._wordList = list
    self._wordListView = scrollView
    self._wordListView:SetHorizontalMove(false)
    self._wordListView:SetVerticalMove(true)
    self._wordList:SetItemType(CollectWordItem)
    self._wordList:SetPadding(0, 0, 0, 0)
    self._wordList:SetGap(10, 0)
    self._wordList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function CollectWordView:OnListItemClicked(idx)

end

function CollectWordView:UpdateView()

end