local UnionHegemonyView = Class.UnionHegemonyView(ClassTypes.BaseComponent)

UnionHegemonyView.interface = GameConfig.ComponentsConfig.Component
UnionHegemonyView.classPath = "Modules.ModuleOpenOther.ChildView.UnionHegemonyView"
require "Modules.ModuleOpenOther.ChildComponent.UnionHegemonyItem"

local UnionHegemonyItem = ClassTypes.UnionHegemonyItem
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildConquestOpenServerDataHelper = GameDataHelper.GuildConquestOpenServerDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil

function UnionHegemonyView:Awake()
    self._txtUnionTime = self:GetChildComponent("Container_Left/Text_Activity_Time", 'TextMeshWrapper')
    self:InitCom()
end
--override
function UnionHegemonyView:OnDestroy() 

end

function UnionHegemonyView:ShowView()
    if self._unionlistlView then
        self._unionlistlView:SetScrollRectState(true)
    end

    self._icongo0 = self:FindChildGO("Container_Right/Image_Rignt")
    GameWorld.AddIcon(self._icongo0,13633)
    -- self._icongo1 = self:FindChildGO("Container_Left/Image_Rignt")
    -- GameWorld.AddIcon(self._icongo1,13538)
   local ids = GuildConquestOpenServerDataHelper:GetAllId()
   self._unionList:SetDataList(ids)
--    local argsTable = LanguageDataHelper.GetArgsTable()
--    argsTable["0"] = DateTimeUtil.GetServerOpenDays()
--    local hour = DateTimeUtil.Now()["hour"]
--    local min = DateTimeUtil.Now()["min"] 
--    argsTable["1"] = hour..":"..min
   self._txtUnionTime.text = LanguageDataHelper.CreateContent(76973)
end

function UnionHegemonyView:CloseView()

    if self._unionlistlView then
        self._unionlistlView:SetScrollRectState(false)
    end
    self._icongo0=nil
    self._icongo1=nil
end

function UnionHegemonyView:InitCom()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewUnion")
    self._unionList = list
    self._unionlistlView = scrollView
    self._unionlistlView:SetHorizontalMove(false)
    self._unionlistlView:SetVerticalMove(true)
    self._unionList:SetItemType(UnionHegemonyItem)
    self._unionList:SetPadding(0, 0, 0, 0)
    self._unionList:SetGap(10, 0)
    self._unionList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function UnionHegemonyView:OnListItemClicked(idx)

end

function UnionHegemonyView:UpdateView()

end