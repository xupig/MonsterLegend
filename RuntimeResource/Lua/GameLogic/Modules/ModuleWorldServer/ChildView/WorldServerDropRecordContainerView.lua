-- WorldServerDropRecordContainerView.lua
local WorldServerDropRecordContainerView = Class.WorldServerDropRecordContainerView(ClassTypes.BaseComponent)
require "Modules.ModuleWorldServer.ChildComponent.WorldServerDropRecordListInfoItem"
local WorldServerDropRecordListInfoItem = ClassTypes.WorldServerDropRecordListInfoItem
WorldServerDropRecordContainerView.interface = GameConfig.ComponentsConfig.Component
WorldServerDropRecordContainerView.classPath = "Modules.ModuleWorldServer.ChildView.WorldServerDropRecordContainerView"
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local DropRecordManager = PlayerManager.DropRecordManager

function WorldServerDropRecordContainerView:Awake()
    self:InitList()
    self._onDropRecordGetCross = function(args)self:OnDropRecordGetCross(args) end
    self._containerNone = self:FindChildGO("Container_None")
end

function WorldServerDropRecordContainerView:OnDestroy()

end

function WorldServerDropRecordContainerView:ShowView()
    EventDispatcher:AddEventListener(GameEvents.OnDropRecordGetCross, self._onDropRecordGetCross)
    DropRecordManager:GetCross()
    if self._scrollView then
        self._scrollView:SetScrollRectState(true)
    end
end

function WorldServerDropRecordContainerView:CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.OnDropRecordGetCross, self._onDropRecordGetCross)
    if self._scrollView then
        self._scrollView:SetScrollRectState(false)
    end
end

function WorldServerDropRecordContainerView:OnDropRecordGetCross(args)
    self._list:SetDataList(args)
    self._containerNone:SetActive(#args == 0)
end

function WorldServerDropRecordContainerView:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._scrollView:SetHorizontalMove(false)
    self._scrollView:SetVerticalMove(true)
    self._list:SetItemType(WorldServerDropRecordListInfoItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 100)
end

function WorldServerDropRecordContainerView:ShowModel()
end

function WorldServerDropRecordContainerView:HideModel()
end
