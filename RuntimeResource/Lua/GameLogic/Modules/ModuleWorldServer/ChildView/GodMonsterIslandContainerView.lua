-- GodMonsterIslandContainerView.lua
local GodMonsterIslandContainerView = Class.GodMonsterIslandContainerView(ClassTypes.BaseComponent)
require "Modules.ModuleWorldServer.ChildComponent.GodIslandListInfoItem"
local GodIslandListInfoItem = ClassTypes.GodIslandListInfoItem
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local TaskRewardItem = ClassTypes.TaskRewardItem
require "PlayerManager/PlayerData/EventData"
local PanelTabCom = ClassTypes.PanelTabCom
GodMonsterIslandContainerView.interface = GameConfig.ComponentsConfig.Component
GodMonsterIslandContainerView.classPath = "Modules.ModuleWorldServer.ChildView.GodMonsterIslandContainerView"
local UIList = ClassTypes.UIList
local UIComponentUtil = GameUtil.UIComponentUtil
local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local GodIslandManager = PlayerManager.GodIslandManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local ActorModelComponent = GameMain.ActorModelComponent


function GodMonsterIslandContainerView:Awake()
    self._containerBossName = self:FindChildGO("Container_Info/Container_Name")
    self._btnHelp = self:FindChildGO("Container_Info/Button_Help")
    self._btnFight = self:FindChildGO("Container_Info/Button_Fight")
    self._csBH:AddClick(self._btnHelp, function()self:HelpBtnClick() end)
    self._csBH:AddClick(self._btnFight, function()self:FightBtnClick() end)
    
    self._containerBoss = self:FindChildGO("Container_Info/Container_Boss")
    self._btnKillRecord = self:FindChildGO("Container_Info/Container_Boss/Button_KillRecord")
    self._csBH:AddClick(self._btnKillRecord, function()self:KillRecordBtnClick() end)
    self._textBossTireValue = self:GetChildComponent("Container_Info/Container_Boss/Text_BossTireValue", "TextMeshWrapper")
    self:InitRewardsList()
    self._containerBoss:SetActive(false)
    
    self._containerCrystal = self:FindChildGO("Container_Info/Container_Crystal")
    self._textCanCollectCristal = self:GetChildComponent("Container_Info/Container_Crystal/Text_CanCollectCristal", "TextMeshWrapper")
    self._textCristalInfo = self:GetChildComponent("Container_Info/Container_Crystal/Text_Info", "TextMeshWrapper")
    self._textCristalReward = self:GetChildComponent("Container_Info/Container_Crystal/Text_Reward", "TextMeshWrapper")
    self._textLeftCollectCristal = self:GetChildComponent("Container_Info/Container_Crystal/Text_LeftCollectCristal", "TextMeshWrapper")
    self._containerCrystal:SetActive(false)
    
    self._containerElite = self:FindChildGO("Container_Info/Container_Elite")
    self._textEliteInfo = self:GetChildComponent("Container_Info/Container_Elite/Text_Info", "TextMeshWrapper")
    self._textLeftElite = self:GetChildComponent("Container_Info/Container_Elite/Text_LeftElite", "TextMeshWrapper")
    self._containerElite:SetActive(false)
    
    self._textTipsInfo = self:GetChildComponent("Container_Tips/Text_Info", "TextMeshWrapper")
    self._textTipsTitle = self:GetChildComponent("Container_Tips/Text_Title", "TextMeshWrapper")
    self._btnCloseTips = self:FindChildGO("Container_Tips/Button_Close")
    self._btnCloseBG = self:FindChildGO("Container_Tips/Image_BG")
    self._csBH:AddClick(self._btnCloseTips, function()self:CloseTipsBtnClick() end)
    self._csBH:AddClick(self._btnCloseBG, function()self:CloseTipsBtnClick() end)
    self._containerTips = self:FindChildGO("Container_Tips")
    self._containerTips:SetActive(false)
    
    self._bossModelComponent = self:AddChildComponent('Container_Info/Container_Monster', ActorModelComponent)
    self._bossModelComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 1)
    self:InitBossList()
    self._onGodIslandGetInfo = function()self:OnGodIslandGetInfo() end
    self._onGodIslandGetKillerRecords = function(args)self:OnGodIslandGetKillerRecords(args) end
end

function GodMonsterIslandContainerView:ShowLayerTabs(layerCount)
    if self._layerTabs == nil then
        self._layerTabs = {}
        local layerTabGos = {}
        layerTabGos[1] = self:FindChildGO("Container_BossList/ToggleGroup_Layer/Toggle")
        for i = 2, layerCount do
            local go = self:CopyUIGameObject("Container_BossList/ToggleGroup_Layer/Toggle", "Container_BossList/ToggleGroup_Layer")
            layerTabGos[i] = go
        end
        for i = 1, layerCount do
            local go = layerTabGos[i]
            local item = UIComponentUtil.AddLuaUIComponent(go, PanelTabCom)
            item:SetName(self:GetFloorName(i))
            self._csBH:AddClick(item.gameObject, function()self:OnLayerTabClick(i) end)
            item:SetSelected(false)
            self._layerTabs[i] = item
        end
    end
    
    self:OnLayerTabClick(1)
end

function GodMonsterIslandContainerView:GetFloorName(floor)
    if floor == 1 then
        return LanguageDataHelper.CreateContent(56061)
    else
        return LanguageDataHelper.CreateContent(56062)
    end
end

function GodMonsterIslandContainerView:OnLayerTabClick(index)
    local item = self._layerTabs[index]
    if item == nil then
        return
    end
    
    self._selectedLayerIndex = index
    
    if self._selectedLayerTabItem then
        self._selectedLayerTabItem:SetSelected(false)
    end
    self._selectedLayerTabItem = self._layerTabs[index]
    self._selectedLayerTabItem:SetSelected(true)
    
    -- if self._secTabCB then
    --     self._secTabCB(index)
    -- end
    GodIslandManager:GetData(index)
end

function GodMonsterIslandContainerView:OnDestroy()

end

function GodMonsterIslandContainerView:ShowView(args)
    self._selectedBossId = args
    EventDispatcher:AddEventListener(GameEvents.OnGodIslandGetInfo, self._onGodIslandGetInfo)
    EventDispatcher:AddEventListener(GameEvents.OnGodIslandGetKillerRecords, self._onGodIslandGetKillerRecords)
    local floors = GodIslandManager:GetAllFloor()
    self:ShowLayerTabs(#floors)
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(true)
    end
    self:ShowModel()
end

function GodMonsterIslandContainerView:CloseView()
    self._selectedBossId = nil
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    EventDispatcher:RemoveEventListener(GameEvents.OnGodIslandGetInfo, self._onGodIslandGetInfo)
    EventDispatcher:RemoveEventListener(GameEvents.OnGodIslandGetKillerRecords, self._onGodIslandGetKillerRecords)
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(false)
    end
    self:HideModel()
end

function GodMonsterIslandContainerView:OnGodIslandGetInfo()
    if self._selectedBossId then
        local monster = GodIslandManager:GetMonsterItem(self._selectedBossId)
        local floor = monster:GetFloor()
        if floor then
            GodIslandManager:SetCurrentFloor(floor)
        end
    end
    self:ShowMonsterData()
end

function GodMonsterIslandContainerView:OnGodIslandGetKillerRecords(args)
    local text = ""
    for i, v in ipairs(args) do
        text = text .. DateTimeUtil.GetDateTimeStr(v[1]) .. LanguageDataHelper.CreateContent(56022) .. v[2] .. LanguageDataHelper.CreateContent(56023)
    end
    self:ShowTips(LanguageDataHelper.CreateContent(56024), text)
end

function GodMonsterIslandContainerView:ShowMonsterData()
    self._textBossTireValue.text = GodIslandManager:GetTire() .. "/" .. GlobalParamsHelper.GetParamValue(681)--神兽岛疲劳值最大值
    local bossInfos = GodIslandManager:GetCurFloorMonsterInfo()
    local level = GameWorld.Player().level
    local selectedIndex = nil
    for i, v in ipairs(bossInfos) do
        v:SetIsSelected(false)
        if self._selectedBossId then
            if v:GetId() == self._selectedBossId then
                selectedIndex = i
            end
        else
            if level < v:GetLevel() and not selectedIndex then
                selectedIndex = i
            end
        end
    end
    
    -- LoggerHelper.Log("Ash: selectedIndex: " .. selectedIndex)
    local selectedBoss = bossInfos[selectedIndex or 1]
    selectedBoss:SetIsSelected(true)
    self._currentSelectedBoss = selectedBoss
    -- self:OnListItemClicked(selectedIndex)
    self:ShowCurrentBoss()
    self._listBoss:SetDataList(bossInfos)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateBossCountDown() end)
end

function GodMonsterIslandContainerView:ShowCurrentBoss()
    local bossInfo = self._currentSelectedBoss
    
    self:UpdateModel(bossInfo:GetModel())
    GameWorld.AddIcon(self._containerBossName, bossInfo:GetNameIcon(), nil, nil, nil, nil, true)
    
    local monsterType = bossInfo:GetMonsterType()
    if monsterType == 2 then --精英
        self._containerBoss:SetActive(false)
        self._containerElite:SetActive(true)
        self._containerCrystal:SetActive(false)
        local name = bossInfo:GetNameChinese()
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = bossInfo:GetLeftCount()
        -- self._textEliteInfo.text = LanguageDataHelper.CreateContent(56059)
        self._textLeftElite.text = LanguageDataHelper.CreateContent(56060, argsTable)--当前剩余精英怪数量：{0}
    elseif monsterType == 3 then --水晶
        self._containerBoss:SetActive(false)
        self._containerElite:SetActive(false)
        self._containerCrystal:SetActive(true)
        local name = bossInfo:GetNameChinese()
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = name
        argsTable["1"] = GodIslandManager:GetCrystalBigLimit() - GodIslandManager:GetBigCrystalCollectTimes()
        self._textCanCollectCristal.text = LanguageDataHelper.CreateContent(56067, argsTable)--今日可采集{0}数量：{1}
        argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = name
        self._textCristalInfo.text = LanguageDataHelper.CreateContent(56056, argsTable)--{0}随机刷新
        self._textCristalReward.text = LanguageDataHelper.CreateContent(GodIslandManager:GetCollectRewardLanguageId(bossInfo:GetId()))
        argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = name
        argsTable["1"] = bossInfo:GetLeftCount()
        self._textLeftCollectCristal.text = LanguageDataHelper.CreateContent(56058, argsTable)--当前剩余{0}数量：{1}
    else
        self._containerBoss:SetActive(true)
        self._containerElite:SetActive(false)
        self._containerCrystal:SetActive(false)
        self._listRewards:SetDataList(bossInfo:GetBossDrops())
    end
end

function GodMonsterIslandContainerView:HelpBtnClick()
    self:ShowTips(LanguageDataHelper.CreateContent(56070), LanguageDataHelper.CreateContent(56071))
end

function GodMonsterIslandContainerView:FightBtnClick()
    if self._currentSelectedBoss then
        local levelLimit = self._currentSelectedBoss:GetLevelLimit()
        if levelLimit > GameWorld.Player().level then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(56016) .. levelLimit .. LanguageDataHelper.CreateContent(56017) .. self._currentSelectedBoss:GetSenceName(), 1)
        else
            GodIslandManager:GotoBoss(self._currentSelectedBoss)
        end
    end
end

function GodMonsterIslandContainerView:KillRecordBtnClick()
    if self._currentSelectedBoss then
        GodIslandManager:GetKillerRecords(self._currentSelectedBoss:GetId())
    end
end

function GodMonsterIslandContainerView:ShowTips(title, text)
    self._containerTips:SetActive(true)
    self._textTipsInfo.text = text
    self._textTipsTitle.text = title
end

function GodMonsterIslandContainerView:CloseTipsBtnClick()
    self._containerTips:SetActive(false)
end

function GodMonsterIslandContainerView:InitBossList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_BossList/ScrollViewList_BossList")
    self._listBoss = list
    self._scrollViewBoss = scrollView
    self._scrollViewBoss:SetHorizontalMove(false)
    self._scrollViewBoss:SetVerticalMove(true)
    self._listBoss:SetItemType(GodIslandListInfoItem)
    self._listBoss:SetPadding(0, 0, 0, 0)
    self._listBoss:SetGap(0, 0)
    self._listBoss:SetDirection(UIList.DirectionTopToDown, 1, 100)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._listBoss:SetItemClickedCB(itemClickedCB)
end

function GodMonsterIslandContainerView:InitRewardsList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Info/Container_Boss/Container_Rewards/ScrollViewList")
    self._listRewards = list
    self._scrollViewRewards = scrollView
    self._scrollViewRewards:SetHorizontalMove(true)
    self._scrollViewRewards:SetVerticalMove(false)
    self._listRewards:SetItemType(TaskRewardItem)
    self._listRewards:SetPadding(0, 0, 0, 0)
    self._listRewards:SetGap(0, 10)
    self._listRewards:SetDirection(UIList.DirectionLeftToRight, 100, 1)
end

function GodMonsterIslandContainerView:OnListItemClicked(idx)
    -- local _text = self._listData[idx + 1]
    -- LoggerHelper.Log("OnListItemClicked:" .. idx .. PrintTable:TableToStr(self._listBoss:GetDataByIndex(idx)))
    self._currentSelectedBoss = self._listBoss:GetDataByIndex(idx)
    self:SetListItemSelected(idx)
    self:ShowCurrentBoss()
end

function GodMonsterIslandContainerView:UpdateBossCountDown()
    local length = self._listBoss:GetLength() - 1
    for i = 0, length do
        -- local data = self._listBoss:GetDataByIndex(i)
        local item = self._listBoss:GetItem(i)
        item:UpdateCountDown()
    end
end

function GodMonsterIslandContainerView:SetListItemSelected(index)
    local length = self._listBoss:GetLength() - 1
    for i = 0, length do
        local item = self._listBoss:GetItem(i)
        item:SetSelected(i == index)
    end
end

function GodMonsterIslandContainerView:UpdateModel(modelId)
    self._bossModelComponent:LoadModel(modelId)
end

function GodMonsterIslandContainerView:ShowModel()
    self._bossModelComponent.gameObject:SetActive(true)
end

function GodMonsterIslandContainerView:HideModel()
    self._bossModelComponent.gameObject:SetActive(false)
end
