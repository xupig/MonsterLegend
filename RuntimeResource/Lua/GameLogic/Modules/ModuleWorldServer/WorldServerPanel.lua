-- WorldServerPanel.lua
local WorldServerPanel = Class.WorldServerPanel(ClassTypes.BasePanel)

require "Modules.ModuleWorldServer.ChildView.GodMonsterIslandContainerView"
require "Modules.ModuleWorldServer.ChildView.WorldServerDropRecordContainerView"

local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local WorldServerPanelType = GameConfig.EnumType.WorldServerPanelType
local GodMonsterIslandContainerView = ClassTypes.GodMonsterIslandContainerView
local WorldServerDropRecordContainerView = ClassTypes.WorldServerDropRecordContainerView

function WorldServerPanel:Awake()
    
    self:InitComps()
end

function WorldServerPanel:OnShow(data)
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
end

function WorldServerPanel:OnClose()
end

function WorldServerPanel:OnDestroy()

end

function WorldServerPanel:InitComps()
    self:InitView("Container_GodMonsterIsland", GodMonsterIslandContainerView, WorldServerPanelType.GodMonsterIsland)
    self:InitView("Container_DropRecord", WorldServerDropRecordContainerView, WorldServerPanelType.DropRecord)

    self._bgIconObj = self:FindChildGO("Container_BgIcon")
    GameWorld.AddIcon(self._bgIconObj,13547)
end

function WorldServerPanel:StructingViewInit()
    return true
end

function WorldServerPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--基于结构化View初始化方式，隐藏View使用Scale的方式处理
function WorldServerPanel:ScaleToHide()
    return true
end

function WorldServerPanel:BaseShowModel()
    self._selectedView:ShowModel()
end

function WorldServerPanel:BaseHideModel()
    self._selectedView:HideModel()
end
