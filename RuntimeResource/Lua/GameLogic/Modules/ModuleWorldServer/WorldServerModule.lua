-- WorldServerModule.lua
WorldServerModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function WorldServerModule.Init()
    GUIManager.AddPanel(PanelsConfig.WorldServer, "Panel_WorldServer", GUILayer.LayerUIPanel, "Modules.ModuleWorldServer.WorldServerPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return WorldServerModule
