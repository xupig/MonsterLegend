-- WorldServerDropRecordListInfoItem.lua
local WorldServerDropRecordListInfoItem = Class.WorldServerDropRecordListInfoItem(ClassTypes.UIListItem)
WorldServerDropRecordListInfoItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
WorldServerDropRecordListInfoItem.classPath = "Modules.ModuleWorldServer.ChildComponent.WorldServerDropRecordListInfoItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
local DateTimeUtil = GameUtil.DateTimeUtil
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local TipsManager = GameManager.TipsManager

function WorldServerDropRecordListInfoItem:Awake()
    self._base.Awake(self)
    
    self._textTime = self:GetChildComponent("Container_content/Text_Time", "TextMeshWrapper")
    self._textContent = self:GetChildComponent("Container_content/Text_Content", "TextMeshWrapper")
    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject, "Container_content/Text_Content")
    self._linkTextMesh:SetOnClickCB(function(linkIndex, postion)self:OnClickLink(linkIndex, postion) end)
    self._containerDisActive = self:FindChildGO("Container_content/Container_DisActive")
    self._containerActive = self:FindChildGO("Container_content/Container_Active")
end

function WorldServerDropRecordListInfoItem:OnDestroy()
    self._textTime = nil
    self._textContent = nil
    self._containerDisActive = nil
    self._containerActive = nil
    self._base.OnDestroy(self)
end

function WorldServerDropRecordListInfoItem:OnClickLink(linkId, postion)
    -- LoggerHelper.Log("OnClickLink .." .. tostring(linkId) .. " " .. PrintTable:TableToStr(postion))
    if linkId == "PLAYER_LINK" then
        elseif linkId == "ITEM_LINK" then
        TipsManager:ShowItemTipsById(self._data[5])
        end
end

function WorldServerDropRecordListInfoItem:SetActive(isActive)
    self._containerDisActive:SetActive(not isActive)
    self._containerActive:SetActive(isActive)
end

--override
function WorldServerDropRecordListInfoItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        local index = self:GetIndex()
        if index % 2 == 0 then
            self:SetActive(true)
        else
            self:SetActive(false)
        end
        -- LoggerHelper.Log("WorldServerDropRecordListInfoItem .." .. PrintTable:TableToStr(data))
        --时间，玩家名,地图id,怪物id,获得道具,cross_uuid
        self._textTime.text = DateTimeUtil.GetDateFullStr(data[1])
        local sceneName = LanguageDataHelper.GetLanguageData(MapDataHelper.GetChinese(data[3]) or 0)
        local monsterName = MonsterDataHelper.GetMonsterName(data[4])
        local itemId = data[5]
        local itemName = '<u><link="ITEM_LINK">' .. ItemDataHelper.GetItemName(itemId) .. '</link></u>'
        local itemNameWithColor = ItemDataHelper.GetItemColorFormat(itemId, itemName)
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = sceneName
        argsTable["1"] = monsterName
        local content = LanguageDataHelper.CreateContent(56074, argsTable)
        self._textContent.text = '<#9BB4D7><u><link="PLAYER_LINK">' .. data[2] .. '</link></u></color>'
            .. content .. itemNameWithColor
    
    -- self._textBossName.text = LanguageDataHelper.GetContent(data:GetName())
    -- GameWorld.AddIcon(self._containerIcon, data:GetIcon(), nil, true)
    -- -- self:SetInstrest(data:GetIsIntrest())
    -- self:SetSelected(data:GetIsSelected())
    -- self:UpdateCountDown()
    -- local monsterType = data:GetMonsterType()
    -- if monsterType == 2 then
    --     self._textBossLevel.text = LanguageDataHelper.GetContent(56063)
    -- elseif monsterType == 3 then
    --     self._textBossLevel.text = LanguageDataHelper.GetContent(56063)
    -- else
    --     self._textBossLevel.text = data:GetLevel() .. LanguageDataHelper.GetContent(69)
    -- end
    end
end
