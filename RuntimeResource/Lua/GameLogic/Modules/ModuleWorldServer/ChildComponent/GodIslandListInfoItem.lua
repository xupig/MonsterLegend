-- GodIslandListInfoItem.lua
local GodIslandListInfoItem = Class.GodIslandListInfoItem(ClassTypes.UIListItem)
GodIslandListInfoItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
GodIslandListInfoItem.classPath = "Modules.ModuleWorldServer.ChildComponent.GodIslandListInfoItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
-- local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local UIToggle = ClassTypes.UIToggle
local WorldBossManager = PlayerManager.WorldBossManager
local DateTimeUtil = GameUtil.DateTimeUtil

function GodIslandListInfoItem:Awake()
    self._base.Awake(self)
    
    self._textBossName = self:GetChildComponent("Container_content/Text_BossName", "TextMeshWrapper")
    --self._textBossLevel = self:GetChildComponent("Container_content/Text_BossLevel", "TextMeshWrapper")
    self._textRebornCountDown = self:GetChildComponent("Container_content/Text_RebornCountDown", "TextMeshWrapper")
    self._goRebornCountDown = self:FindChildGO("Container_content/Text_RebornCountDown")
    self._containerIcon = self:FindChildGO("Container_content/Container_Icon")
    self._imageSelected = self:FindChildGO("Container_content/Image_Selected")
    self._gradeImage = self:FindChildGO("Container_content/Image_Grade")
    self._gradeText = self:GetChildComponent("Container_content/Image_Grade/Text", "TextMeshWrapper")
    -- self._imageInstrest = self:FindChildGO("Container_content/Text_BossIntrest")
    self:SetSelected(false)
    -- self._onWorldBossIsIntrestChanged = function(id)self:OnWorldBossIsIntrestChanged(id) end
    -- EventDispatcher:AddEventListener(GameEvents.OnWorldBossIsIntrestChanged, self._onWorldBossIsIntrestChanged)
end

function GodIslandListInfoItem:OnDestroy()
    -- EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossIsIntrestChanged, self._onWorldBossIsIntrestChanged)
    self._textBossName = nil
    --self._textBossLevel = nil
    self._textRebornCountDown = nil
    self._goRebornCountDown = nil
    self._containerIcon = nil
    self._imageSelected = nil
    self._base.OnDestroy(self)
end

-- function GodIslandListInfoItem:OnWorldBossIsIntrestChanged(id)
--     -- LoggerHelper.Log("Ash: OnWorldBossIsIntrestChanged: " .. id)
--     if self._data and self._data:GetId() == id then
--         self:SetInstrest(self._data:GetIsIntrest())
--     end
-- end

function GodIslandListInfoItem:SetSelected(isSeleted)
    self._imageSelected:SetActive(isSeleted)
    if self._data then
        self._data:SetIsSelected(isSeleted)
    end
end

-- function GodIslandListInfoItem:SetInstrest(isInstrest)
--     self._imageInstrest:SetActive(isInstrest)
-- end
function GodIslandListInfoItem:UpdateCountDown()
    -- LoggerHelper.Error("OnClickBoss")
    local data = self._data -- WorldBossManager:GetBossInfo(data)
    if data then
        local isDead = data:IsDead()
        if self._lastIsDead ~= isDead then
            self._goRebornCountDown:SetActive(isDead)
        end
        self._lastIsDead = isDead
        if isDead then
            local leftTime = data:GetRebornTime() - DateTimeUtil:GetServerTime()
            self._textRebornCountDown.text = DateTimeUtil:FormatFullTime(leftTime)
        end
    end
end

--override
function GodIslandListInfoItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        self._gradeImage:SetActive(false)
        --self._textBossName.text = LanguageDataHelper.GetContent(data:GetName())
        GameWorld.AddIcon(self._containerIcon, data:GetIcon(), nil, true)
        -- self:SetInstrest(data:GetIsIntrest())
        self:SetSelected(data:GetIsSelected())
        self:UpdateCountDown()
        local monsterType = data:GetMonsterType()
        if monsterType == 2 then
            --self._textBossLevel.text = LanguageDataHelper.GetContent(56063)
            self._textBossName.text = LanguageDataHelper.GetContent(data:GetName())
        elseif monsterType == 3 then
            --self._textBossLevel.text = LanguageDataHelper.GetContent(56063)
            self._textBossName.text = LanguageDataHelper.GetContent(data:GetName())
        else
            --self._textBossLevel.text = data:GetLevel() .. LanguageDataHelper.GetContent(69)
            self._textBossName.text = string.format("%s %s%s", LanguageDataHelper.GetContent(data:GetName()), data:GetLevel(), LanguageDataHelper.GetContent(69))
            self._gradeImage:SetActive(data:GetLevelShow() > 0)
            self._gradeText.text = LanguageDataHelper.CreateContentWithArgs(56084, {["0"]=data:GetLevelShow()})
        end
    
    -- self:SetNumber(self:GetIndex() + 1)
    -- self._textName.text = data[public_config.WORLD_BOSS_PLAYER_INFO_NAME]
    -- self._textValue.text = data[public_config.WORLD_BOSS_PLAYER_INFO_HARM]
    -- if data[5] == 0 then
    --     self._pbValue:SetProgressByValue(0, 1)
    -- else
    --     self._pbValue:SetProgressByValue(data[public_config.WORLD_BOSS_PLAYER_INFO_HARM], data[5])
    -- end
    else
        -- self._textName.text = ""
        -- self._textValue.text = ""
        -- self:SetNumber("")
        end
end
