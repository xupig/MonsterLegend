--DailyConsumeModule.lua
DailyConsumeModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function DailyConsumeModule.Init()
     GUIManager.AddPanel(PanelsConfig.DailyConsume,"Panel_DailyConsume",GUILayer.LayerUIPanel,"Modules.ModuleDailyConsume.DailyConsumePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return DailyConsumeModule