local DailyConsumeGradeItem = Class.DailyConsumeGradeItem(ClassTypes.UIListItem)

DailyConsumeGradeItem.interface = GameConfig.ComponentsConfig.Component
DailyConsumeGradeItem.classPath = "Modules.ModuleDailyConsume.ChildComponent.DailyConsumeGradeItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager

function DailyConsumeGradeItem:Awake()
    self:InitView()
end

--override
function DailyConsumeGradeItem:OnDestroy() 

end

function DailyConsumeGradeItem:InitView()
    self._notSelected = self:FindChildGO("Background")
    self._selected = self:FindChildGO("Checkmark")
    self._selected:SetActive(false)
    self._notSelected:SetActive(true)

    self._notSelectedText = self:GetChildComponent("Background/Label", "TextMeshWrapper")
    self._selectedText = self:GetChildComponent("Checkmark/Label", "TextMeshWrapper")
end

--data {money}
function DailyConsumeGradeItem:OnRefreshData(data)
    self.data = data
    self._notSelectedText.text = LanguageDataHelper.CreateContentWithArgs(81544, {["0"]=data})
    self._selectedText.text = LanguageDataHelper.CreateContentWithArgs(81544, {["0"]=data})
end

function DailyConsumeGradeItem:ShowSelected(state)
    self._selected:SetActive(state)
    self._notSelected:SetActive(not state)
end