require "UIComponent.Extend.ItemGrid"

local DailyConsumeRewardTotalItem = Class.DailyConsumeRewardTotalItem(ClassTypes.UIListItem)

DailyConsumeRewardTotalItem.interface = GameConfig.ComponentsConfig.Component
DailyConsumeRewardTotalItem.classPath = "Modules.ModuleDailyConsume.ChildComponent.DailyConsumeRewardTotalItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local GUIManager = GameManager.GUIManager
local DailyConsumeDataHelper = GameDataHelper.DailyConsumeDataHelper
local DailyConsumeManager = PlayerManager.DailyConsumeManager

function DailyConsumeRewardTotalItem:Awake()
    self:InitView()
end

--override
function DailyConsumeRewardTotalItem:OnDestroy() 

end

function DailyConsumeRewardTotalItem:InitView()
    self._parent = self:FindChildGO("Container_Icon")
    local itemGo = GUIManager.AddItem(self._parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function()
                    TipsManager:ShowItemTipsById(self.itemId)
                end
    self._icon:SetPointerDownCB(self._func)

    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")

    self._getBtn = self:FindChildGO("Button_Get")
    self._csBH:AddClick(self._getBtn, function() self:OnGetButton() end)
    self._getBtnText = self:GetChildComponent("Button_Get/Text", "TextMeshWrapper")
    self._getBtnWrapper = self._getBtn:GetComponent("ButtonWrapper")
    self._getBtnWrapper.interactable = false
end

--data {id}
function DailyConsumeRewardTotalItem:OnRefreshData(data)
    self.data = data

    local chargeDay = 0
    for k,v in pairs(GameWorld.Player().cumulative_consume_record) do
        if v ~= nil then
            chargeDay = chargeDay + 1
        end
    end

    local count
    local cfg = DailyConsumeDataHelper:GetDailyRewardData(data[1])
    self.day = cfg.day
    self.itemId, count = next(cfg.reward)
    self._icon:SetItem(self.itemId, count)
    self._icon:SetBind(true)
    self._infoText.text = LanguageDataHelper.CreateContentWithArgs(81546, {["0"]= cfg.day, ["1"]= chargeDay, ["2"]= cfg.day, ["3"]= cfg.cost})
    self:RefreshButtonState()
end

function DailyConsumeRewardTotalItem:RefreshButtonState()
    if GameWorld.Player().cumulative_consume_record[self.day] == 0 then
        self._getBtnWrapper.interactable = true
        self._getBtnText.text = LanguageDataHelper.CreateContent(123)
    elseif GameWorld.Player().cumulative_consume_record[self.day] == 1 then
        self._getBtnWrapper.interactable = false
        self._getBtnText.text = LanguageDataHelper.CreateContent(76903)
    else
        self._getBtnWrapper.interactable = false
        self._getBtnText.text = LanguageDataHelper.CreateContent(123)
    end
end

function DailyConsumeRewardTotalItem:OnGetButton()
    if GameWorld.Player().cumulative_consume_record[self.day] == 0 then
        DailyConsumeManager:GetConsumeCumulativeReward(self.day)
    end
end