require "Modules.ModuleDailyConsume.ChildComponent.DailyConsumeRewardItem"
require "Modules.ModuleDailyConsume.ChildComponent.DailyConsumeRewardTotalItem"
require "Modules.ModuleDailyConsume.ChildComponent.DailyConsumeGradeItem"

local DailyConsumePanel = Class.DailyConsumePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DailyConsumeRewardItem = ClassTypes.DailyConsumeRewardItem
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DailyConsumeRewardTotalItem = ClassTypes.DailyConsumeRewardTotalItem
local DailyConsumeGradeItem = ClassTypes.DailyConsumeGradeItem
local DateTimeUtil = GameUtil.DateTimeUtil
local DailyConsumeManager = PlayerManager.DailyConsumeManager
local XImageFlowLight = GameMain.XImageFlowLight
local DailyConsumeDataHelper = GameDataHelper.DailyConsumeDataHelper

--override
function DailyConsumePanel:Awake()
    self._preSelected = -1
    self.state = 0
    self:InitView()
    self:InitListenerFunc()
end

function DailyConsumePanel:InitListenerFunc()
    self._refreshDailyConsumeRewardRecord = function() self:ShowReward() end
    self._refreshDailyConsumeCumulativeRecord = function() self:RefreshCumulativeRecord() end
end


--override
function DailyConsumePanel:OnDestroy()

end

function DailyConsumePanel:OnShow(data)
    self:AddEventListeners()
    self:RefreshView()
end

function DailyConsumePanel:OnClose()
    self:RemoveEventListeners()
end

function DailyConsumePanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Daily_Consume_Reward_Record, self._refreshDailyConsumeRewardRecord)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Daily_Consume_Cumulative_Record, self._refreshDailyConsumeCumulativeRecord)
end

function DailyConsumePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Daily_Consume_Reward_Record, self._refreshDailyConsumeRewardRecord)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Daily_Consume_Cumulative_Record, self._refreshDailyConsumeCumulativeRecord)
end

function DailyConsumePanel:InitView()
    self._closeBtn = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeBtn, function() self:OnCloseButton() end)
    self._actionBtn = self:FindChildGO("Button_Action")
    self._csBH:AddClick(self._actionBtn, function() self:OnActionButton() end)
    self._actionBtnText = self:GetChildComponent("Button_Action/Text", "TextMeshWrapper")
    self._actionBtnWrapper = self._actionBtn:GetComponent("ButtonWrapper")
    self._actionBtnWrapper.interactable = false

    self._actionButtonFlowLight = self:AddChildComponent("Button_Action", XImageFlowLight)
    self._actionButtonFlowLight.enabled = false

    self._redPointImage = self:FindChildGO("Button_Action/Image_RedPoint")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(DailyConsumeRewardItem)
    self._list:SetPadding(16, 0, 0, 12)
    self._list:SetGap(5, 20)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 2)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)

    local scrollViewTotal, listTotal = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Reward")
    self._listTotal = listTotal
	self._scrollViewTotal = scrollViewTotal
    self._listTotal:SetItemType(DailyConsumeRewardTotalItem)
    self._listTotal:SetPadding(10, 0, 5, 18)
    self._listTotal:SetGap(5, 0)
    self._listTotal:SetDirection(UIList.DirectionTopToDown, 1, 1)

    local scrollViewGrade, listGrade = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Grade")
    self._listGrade = listGrade
	self._scrollViewGrade = scrollViewGrade
    self._listGrade:SetItemType(DailyConsumeGradeItem)
    self._listGrade:SetPadding(0, 0, 0, 6)
    self._listGrade:SetGap(0, 10)
    self._listGrade:SetDirection(UIList.DirectionLeftToRight, 100, 1)
    scrollViewGrade:SetHorizontalMove(true)
    scrollViewGrade:SetVerticalMove(false)
    scrollViewGrade:SetScrollRectEnable(false)

    local itemClickedCB = function(idx) self:OnListItemClicked(idx) end
	self._listGrade:SetItemClickedCB(itemClickedCB)
end

function DailyConsumePanel:OnListItemClicked(idx)
    if self._preSelected ~= -1 then
        self._listGrade:GetItem(self._preSelected):ShowSelected(false)
    end
    self._listGrade:GetItem(idx):ShowSelected(true)
    self._preSelected = idx
    local itemData = self._listGrade:GetDataByIndex(idx)

    local cfg = DailyConsumeDataHelper:GetDailyConsumeDataByArgs(itemData, GameWorld.Player().daily_cunsume_world_level)
    self._list:SetDataList(cfg.reward)

    self:RefreshButtonState(itemData)
end

function DailyConsumePanel:OnCloseButton()
    GUIManager.ClosePanel(PanelsConfig.DailyConsume)
end

function DailyConsumePanel:OnActionButton()
    if self.state == 0 then
        self:OnCloseButton()
        --GUIManager.ShowPanelByFunctionId(43)
        local data ={["id"]=1}
        GUIManager.ShowPanel(PanelsConfig.Shop, data)
    elseif self.state == 2 then
        local itemData = self._listGrade:GetDataByIndex(self._preSelected)
        DailyConsumeManager:GetConsumeReward(itemData)
    end
end

function DailyConsumePanel:RefreshView()
    self:RefreshCumulativeRecord()
    self._listGrade:SetDataList(GlobalParamsHelper.GetParamValue(897))
    self:ShowReward()
end

function DailyConsumePanel:ShowReward()
    local gradeData = GlobalParamsHelper.GetParamValue(897)
    local showIndex = #gradeData
    for i=#gradeData,1,-1 do
        if GameWorld.Player().daily_consume_record[gradeData[i]] == 0 then
            showIndex = i
            break
        end
    end
    self:OnListItemClicked(showIndex-1)
end

function DailyConsumePanel:RefreshButtonState(cost)
    if GameWorld.Player().daily_consume_record[cost] == 1 then
        --已领取
        self._actionBtnText.text = LanguageDataHelper.CreateContent(76903)
        self._actionBtnWrapper.interactable = false
        self.state = 1
        self._redPointImage:SetActive(false)
        self._actionButtonFlowLight.enabled = false
    elseif GameWorld.Player().daily_consume_record[cost] == nil then
        --未达到
        self._actionBtnText.text = LanguageDataHelper.CreateContent(81545)
        self._actionBtnWrapper.interactable = true
        self.state = 0
        self._redPointImage:SetActive(false)
        self._actionButtonFlowLight.enabled = true
    elseif GameWorld.Player().daily_consume_record[cost] == 0 then
        --可领取
        self.state = 2
        self._actionBtnText.text = LanguageDataHelper.CreateContent(76902)
        self._actionBtnWrapper.interactable = true
        self._redPointImage:SetActive(true)
        self._actionButtonFlowLight.enabled = true
    end
end

function DailyConsumePanel:RefreshCumulativeRecord()
    local totalData = DailyConsumeDataHelper:GetAllCumulativeDailyConsumeData()
    self._listTotal:SetDataList(totalData)
end

function DailyConsumePanel:WinBGType()
    return PanelWinBGType.NoBG
end