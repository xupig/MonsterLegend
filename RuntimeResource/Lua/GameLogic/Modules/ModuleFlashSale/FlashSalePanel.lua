require "UIComponent.Extend.ItemGrid"

local FlashSalePanel = Class.FlashSalePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local EquipModelComponent = GameMain.EquipModelComponent
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PlayerModelComponent = GameMain.PlayerModelComponent
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local UIParticle = ClassTypes.UIParticle
local XImageFlowLight = GameMain.XImageFlowLight
local ItemDataHelper = GameDataHelper.ItemDataHelper
local FlashSaleDataHelper = GameDataHelper.FlashSaleDataHelper
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local TipsManager = GameManager.TipsManager
local ActorModelComponent = GameMain.ActorModelComponent
local DateTimeUtil = GameUtil.DateTimeUtil
local FlashSaleManager = PlayerManager.FlashSaleManager
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

local Move_Y = 20
--override
function FlashSalePanel:Awake()
    self:InitView()
    self:InitListenerFunc()
end

function FlashSalePanel:InitListenerFunc()
    self._refreshFlashSalePanel = function()self:RefreshView() end
end

--override
function FlashSalePanel:OnDestroy()
    self._iconbg = nil
end

function FlashSalePanel:OnShow(data)
    self.data = data --档数
    self._iconbg = self:FindChildGO("Image_BG2")
    GameWorld.AddIcon(self._iconbg, 13512)
    self:AddEventListeners()
    if not FlashSaleManager:CanShowFlashSale(self.data) then
        return
    end
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateCountDown() end)
    self:RefreshView()
end

function FlashSalePanel:OnClose()
    self._iconbg = nil
    self:RemoveEventListeners()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self.data = nil
end

function FlashSalePanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Flash_Sale_Data, self._refreshFlashSalePanel)
end

function FlashSalePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Flash_Sale_Data, self._refreshFlashSalePanel)
end

function FlashSalePanel:InitView()
    self._closeBtn = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeBtn, function()self:OnCloseButton() end)
    
    self._timeText = self:GetChildComponent("Text_Time", "TextMeshWrapper")
    
    self._leftModelGO = self:FindChildGO('Container_LeftItem/Container_Model')
    self._leftModelComponent = self:AddChildComponent('Container_LeftItem/Container_Model', ActorModelComponent)
    self._leftIcon = self:FindChildGO("Container_LeftItem/Container_Icon")
    self._leftItemIcon = self:FindChildGO("Container_LeftItem/Container_Item/Container_Icon")
    self._leftMoneyIcon1 = self:FindChildGO("Container_LeftItem/Container_Item/Container_Money1")
    self._leftMoneyIcon2 = self:FindChildGO("Container_LeftItem/Container_Item/Container_Money2")
    self._leftPriceText1 = self:GetChildComponent("Container_LeftItem/Container_Item/Text_Price1", "TextMeshWrapper")
    self._leftPriceText2 = self:GetChildComponent("Container_LeftItem/Container_Item/Text_Price2", "TextMeshWrapper")
    self._leftBuyBtn = self:FindChildGO("Container_LeftItem/Container_Item/Button_Buy")
    self._csBH:AddClick(self._leftBuyBtn, function()self:OnLeftBuyButton() end)
    self._leftBuyBtnText = self:GetChildComponent("Container_LeftItem/Container_Item/Button_Buy/Text", "TextMeshWrapper")
    self._leftBuyBtnWrapper = self._leftBuyBtn:GetComponent("ButtonWrapper")
    self._leftBuyBtnWrapper.interactable = false
    
    local moveTo = self._leftIcon.transform.localPosition
    moveTo.y = moveTo.y + Move_Y
    self._tweenPosLeft = self:AddChildComponent('Container_LeftItem/Container_Icon', XGameObjectTweenPosition)
    self._tweenPosLeft:SetToPosTween(moveTo, 2, 4, 4, nil)
    
    GameWorld.AddIcon(self._leftMoneyIcon1, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    GameWorld.AddIcon(self._leftMoneyIcon2, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    self._leftItemGo = GUIManager.AddItem(self._leftItemIcon, 1)
    self._leftIconGrid = UIComponentUtil.AddLuaUIComponent(self._leftItemGo, ItemGrid)
    self._funcLeft = function()
        TipsManager:ShowItemTipsById(self.leftItemId)
    end
    self._leftIconGrid:SetPointerDownCB(self._funcLeft)
    
    self._rightModelGO = self:FindChildGO('Container_RightItem/Container_Model')
    self._rightModelComponent = self:AddChildComponent('Container_RightItem/Container_Model', ActorModelComponent)
    self._rightIcon = self:FindChildGO("Container_RightItem/Container_Icon")
    self._rightItemIcon = self:FindChildGO("Container_RightItem/Container_Item/Container_Icon")
    self._rightMoneyIcon1 = self:FindChildGO("Container_RightItem/Container_Item/Container_Money1")
    self._rightMoneyIcon2 = self:FindChildGO("Container_RightItem/Container_Item/Container_Money2")
    self._rightPriceText1 = self:GetChildComponent("Container_RightItem/Container_Item/Text_Price1", "TextMeshWrapper")
    self._rightPriceText2 = self:GetChildComponent("Container_RightItem/Container_Item/Text_Price2", "TextMeshWrapper")
    self._rightBuyBtn = self:FindChildGO("Container_RightItem/Container_Item/Button_Buy")
    self._csBH:AddClick(self._rightBuyBtn, function()self:OnRightBuyButton() end)
    self._rightBuyBtnText = self:GetChildComponent("Container_RightItem/Container_Item/Button_Buy/Text", "TextMeshWrapper")
    self._rightBuyBtnWrapper = self._rightBuyBtn:GetComponent("ButtonWrapper")
    self._rightBuyBtnWrapper.interactable = false
    
    local moveTo = self._rightIcon.transform.localPosition
    moveTo.y = moveTo.y + Move_Y
    self._tweenPosRight = self:AddChildComponent('Container_RightItem/Container_Icon', XGameObjectTweenPosition)
    self._tweenPosRight:SetToPosTween(moveTo, 2, 4, 4, nil)
    
    GameWorld.AddIcon(self._rightMoneyIcon1, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    GameWorld.AddIcon(self._rightMoneyIcon2, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    self._rightItemGo = GUIManager.AddItem(self._rightItemIcon, 1)
    self._rightIconGrid = UIComponentUtil.AddLuaUIComponent(self._rightItemGo, ItemGrid)
    self._funcRight = function()
        TipsManager:ShowItemTipsById(self.rightItemId)
    end
    self._rightIconGrid:SetPointerDownCB(self._funcRight)
    
    self._firstImage = self:FindChildGO("Image_First")
    self._secondImage = self:FindChildGO("Image_Second")
    
    self._fxLeft = UIParticle.AddParticle(self.gameObject, "Container_LeftItem/Container_Fx/FirstCharge")
    self._fxRight = UIParticle.AddParticle(self.gameObject, "Container_RightItem/Container_Fx/FirstCharge")

    --测试动画代码
    -- GameWorld.AddIcon(self._leftIcon, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    -- GameWorld.AddIcon(self._rightIcon, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
end

function FlashSalePanel:OnCloseButton()
    GUIManager.ClosePanel(PanelsConfig.FlashSale)
end

function FlashSalePanel:OnLeftBuyButton()
    FlashSaleManager:FlashSaleBuyReq(self.data, public_config.LEVEL_UP_SALE_NORMAL)
end

function FlashSalePanel:OnRightBuyButton()
    FlashSaleManager:FlashSaleBuyReq(self.data, public_config.LEVEL_UP_SALE_SUPREME)
end

function FlashSalePanel:ShowLeftItemInfo()
    local id = GameWorld.Player().level_up_sale_info[self.data][public_config.LEVEL_UP_SALE_STEP]
    local cfgData = FlashSaleDataHelper.GetData(id)
    local itemId, itemData = next(cfgData.low_reward)
    self.leftItemId = itemId
    self._leftPriceText1.text = itemData[2]
    self._leftPriceText2.text = itemData[3]
    self._leftIconGrid:SetItem(itemId, itemData[1])
    
    if GameWorld.Player().level_up_sale_info[self.data][public_config.LEVEL_UP_SALE_NORMAL] == 1 then
        self._leftBuyBtnWrapper.interactable = false
    else
        self._leftBuyBtnWrapper.interactable = true
    end
    
    if itemData[4] > 0 then
        self._leftModelGO:SetActive(true)
        self._leftIcon:SetActive(false)
        self._leftModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 90, 0), 1)
        self._leftModelComponent:LoadModel(itemData[4])
    else
        self._leftModelGO:SetActive(false)
        self._leftIcon:SetActive(true)
        GameWorld.AddIcon(self._leftIcon, ItemDataHelper.GetIcon(itemId))
    end
end

function FlashSalePanel:ShowRightItemInfo()
    local id = GameWorld.Player().level_up_sale_info[self.data][public_config.LEVEL_UP_SALE_STEP]
    local cfgData = FlashSaleDataHelper.GetData(id)
    local itemId, itemData = next(cfgData.high_reward)
    self.rightItemId = itemId
    self._rightPriceText1.text = itemData[2]
    self._rightPriceText2.text = itemData[3]
    self._rightIconGrid:SetItem(itemId, itemData[1])
    
    if GameWorld.Player().level_up_sale_info[self.data][public_config.LEVEL_UP_SALE_SUPREME] == 1 then
        self._rightBuyBtnWrapper.interactable = false
    else
        self._rightBuyBtnWrapper.interactable = true
    end
    
    if itemData[4] > 0 then
        self._rightModelGO:SetActive(true)
        self._rightIcon:SetActive(false)
        self._rightModelComponent:SetStartSetting(Vector3(0, 60, -300), Vector3(0, 90, 0), 1)
        self._rightModelComponent:LoadModel(itemData[4])
    else
        self._rightModelGO:SetActive(false)
        self._rightIcon:SetActive(true)
        GameWorld.AddIcon(self._rightIcon, ItemDataHelper.GetIcon(itemId))
    end
end

function FlashSalePanel:ShowADImage()
    local id = GameWorld.Player().level_up_sale_info[self.data][public_config.LEVEL_UP_SALE_STEP]
    if id <= 1 then
        self._firstImage:SetActive(true)
        self._secondImage:SetActive(false)
    else
        self._firstImage:SetActive(false)
        self._secondImage:SetActive(true)
    end
end

function FlashSalePanel:UpdateCountDown()
    local id = GameWorld.Player().level_up_sale_info[self.data][public_config.LEVEL_UP_SALE_STEP]
    local timestamp = GameWorld.Player().level_up_sale_info[self.data][public_config.LEVEL_UP_SALE_TIME_STAMP]
    local endTime = timestamp + FlashSaleDataHelper.GetTime(id)
    local now = DateTimeUtil.GetServerTime()
    local leaveTime = math.max(endTime - now, 0)
    self._timeText.text = DateTimeUtil:FormatFullTime(leaveTime)
end

function FlashSalePanel:CanShow()
    self:ShowLeftItemInfo()
    self:ShowRightItemInfo()
    self:ShowADImage()
end

function FlashSalePanel:RefreshView()
    if self.data == nil or GameWorld.Player().level_up_sale_info[self.data] == nil then
        return
    end
    self:ShowLeftItemInfo()
    self:ShowRightItemInfo()
    self:ShowADImage()
end

function FlashSalePanel:BaseShowModel()
    self._leftModelComponent:Show()
    self._rightModelComponent:Show()
end

function FlashSalePanel:BaseHideModel()
    self._leftModelComponent:Hide()
    self._rightModelComponent:Hide()
end

function FlashSalePanel:WinBGType()
    return PanelWinBGType.NoBG
end
