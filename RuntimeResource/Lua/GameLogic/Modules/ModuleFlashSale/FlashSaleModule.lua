--FlashSaleModule.lua
FlashSaleModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function FlashSaleModule.Init()
    GUIManager.AddPanel(PanelsConfig.FlashSale,"Panel_FlashSale",GUILayer.LayerUIPanel,"Modules.ModuleFlashSale.FlashSalePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return FlashSaleModule