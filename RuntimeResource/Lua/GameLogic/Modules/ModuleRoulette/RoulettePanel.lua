require "UIComponent.Extend.ItemGrid"
require "UIComponent.Extend.TipsCom"
require "Modules.ModuleRoulette.ChildComponent.RouletteRecordListItem"
local RouletteRecordListItem = ClassTypes.RouletteRecordListItem
require "Modules.ModuleRoulette.ChildComponent.RouletteRewardListItem"
local RouletteRewardListItem = ClassTypes.RouletteRewardListItem
local RoulettePanel = Class.RoulettePanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local RankExManager = PlayerManager.RankExManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ActorModelComponent = GameMain.ActorModelComponent
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local RouletteData = PlayerManager.PlayerDataManager.rouletteData
local public_config = GameWorld.public_config
local RouletteManager = PlayerManager.RouletteManager
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local TipsCom = ClassTypes.TipsCom
local VipYunpanDataHelper = GameDataHelper.VipYunpanDataHelper
local sTime=0.2

function RoulettePanel:Awake()
    self._timerCountDownId=-1
    self._timerRatationid = -1
	self:InitCallBack()
	self:InitComps()
end

function RoulettePanel:InitCallBack()
    self._updateBigRecrod=function()self:UpdateBigRewardRecrod()end
    self.timeFunc = function()RouletteManager.RouletteTotalCountRpc() RouletteManager.RoulettePersonalListRpc() end
    self.timrCountTimer = function()self:CountDownTimer()end
    self._updateRecord = function() self:GetRankListResp() end
    self._updateTotalCount = function(count) self:OnUpdateTotal(count) end
    self._rotationCB = function() self:RotationTimerCB()end
    self._updateBuyCountEvent=function()self:UpdateBuyCount()end
    self._updateRward = function() self:UpdateRewrd()end
    self._OlockTimer = function()self:OlockUpdata()end
end

function RoulettePanel:AddEventListeners()
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_ROULETTE_TIMES, self._updateBuyCountEvent)   
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._OlockTimer)	
    EventDispatcher:AddEventListener(GameEvents.RouleBigRecordUpdate,self._updateBigRecrod)
    EventDispatcher:AddEventListener(GameEvents.RouleRecordUpdate,self._updateRecord)
    EventDispatcher:AddEventListener(GameEvents.RouletteTotalCount,self._updateTotalCount)
    EventDispatcher:AddEventListener(GameEvents.RecordRewardUpdate,self._updateRward)
end

function RoulettePanel:RemoveEventListeners()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_ROULETTE_TIMES, self._updateBuyCountEvent)    
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._OlockTimer)	    
    EventDispatcher:RemoveEventListener(GameEvents.RouleBigRecordUpdate, self._updateBigRecrod)
    EventDispatcher:RemoveEventListener(GameEvents.RouleRecordUpdate, self._updateRecord)
    EventDispatcher:RemoveEventListener(GameEvents.RouletteTotalCount, self._updateTotalCount)
    EventDispatcher:RemoveEventListener(GameEvents.RecordRewardUpdate, self._updateRward)
end

function RoulettePanel:OnIntroduce()
    self._IntrodueGO:SetActive(not self._IntrodueGO.activeSelf)
end
function RoulettePanel:UpdateRewrd()
    self._isTweening = true
    self._tweenIndex=0
    local z = -self:GetRewardAngles()
    self.stopsEnd:Set(0,0,z+360)
    self:RouleTimer()
    self:RotationTimerCB()
   
end
function RoulettePanel:OnUpdateTotal(count)
    local arg = LanguageDataHelper.GetArgsTable()
    local a = count[1] or 0
    local b = RouletteData:GetTotalCounts()
    arg['0']= a
    arg['1']= b
    self.residueText.text = LanguageDataHelper.CreateContent(76940, arg)
end
function RoulettePanel:OnBuy()
    RouletteManager.RouletteRewardRpc()
    RouletteManager.RoulettePersonalListRpc()
    RouletteManager.RouletteTotalCountRpc()
end

function RoulettePanel:OnReward()
    RouletteManager.RouletteBigRewardListRpc()
    self:UpdateBigRewardRecrod()
    self.tipsgo:SetActive(true)
end

function RoulettePanel:UpdateBigRewardRecrod()
    local d = RouletteData:GetPreRouleRewardData()
    self._rewardlist:SetDataList(d)
end
function RoulettePanel:InitComps()
    local btnGo = self:FindChildGO("Button_Close")
    self._csBH:AddClick(btnGo,function() self:OnCloseClick()end)

    local rewardTextGo = self:FindChildGO("Button_Reward")
    self._csBH:AddClick(rewardTextGo,function() self:OnReward()end)

    local introduceTextGo = self:FindChildGO("Button_Introduce")
    self._csBH:AddClick(introduceTextGo,function() self:OnIntroduce()end)
    local buyTextGo = self:FindChildGO("Button_Buy")
    self._csBH:AddClick(buyTextGo,function() self:OnBuy()end)

    self.timeText = self:GetChildComponent("Text_Time",'TextMeshWrapper')
    self.residueText = self:GetChildComponent("Text_Residue",'TextMeshWrapper')
    self.tipcountText = self:GetChildComponent("Text_TipsCount",'TextMeshWrapper')
    self.costText = self:GetChildComponent("Text_Cost",'TextMeshWrapper')
    self.costText.text = RouletteData:GetRoleCost()..''


    self.rewardssele = {}
    self.rewards = {}
    for i=1,9 do
        local g = GUIManager.AddItem(self:FindChildGO('Container_Reward'..i), 1)
        self.rewards[i] =  UIComponentUtil.AddLuaUIComponent(g,ItemGrid)
        self.rewards[i]:ActivateTipsById()
        self.rewardssele[i]=self:FindChildGO('Container_Reward'..i.."/Image_Sele")
    end

    self.tipsgo = self:FindChildGO('Container_Tips')

    local closeGo = self:FindChildGO("Container_Tips/Button_Close")
    self._csBH:AddClick(closeGo,function() self:OnTipsClose()end)

    local costGo = self:FindChildGO("Image_Cost")
    GameWorld.AddIcon(costGo,ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))

    self.rotationRewards={}
    self._tweenIndex=0
    self.stops={}
    self.stops[1] = Vector3.New(0, 0, 360)
    self.stops[2] = Vector3.New(0, 0, 360)
    self.stops[3] = Vector3.New(0, 0, 360)
 
    self.speeds={}
    self.speeds[1] = sTime
    self.speeds[2] = sTime
    self.speeds[3] = sTime
    self.stopsEnd = Vector3.New(0, 0, 0)
    self.start = Vector3.New(0, 0, 0)
    self.containerRotationGo = self:FindChildGO("Container_Rotation")
    self.containerRotation = self.containerRotationGo:AddComponent(typeof(XGameObjectTweenRotation))
    
    self._IntrodueGO = self:FindChildGO("Container_Introdue")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Introdue", TipsCom)
    local arg = LanguageDataHelper.GetArgsTable()
    arg['0']=RouletteData:GetRoleCost()
    arg['1']=RouletteData:GetLimitTime()
    arg['2']=RouletteData:GetTotalCounts()
    arg['3']=RouletteData:GetTimeEnd()
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(76970,arg))
    
    self:InitRewardList()
    self:InitContentList()
    self._endtimestamp = DateTimeUtil.TodayTimeStamp(RouletteData:GetTimeEnd(),0) 
end

function RoulettePanel:Rotation(start,stop,speed)
    local x = speed or 1
    self.containerRotation.IsTweening = false
    self.containerRotation:SetFromToRotation(start, stop, x, 1,function()
        local g = self.rewardssele[self._seleIndex]
        if not g.activeSelf then
            g:SetActive(true)
        end
    end)
end

function RoulettePanel:RotationTimerCB()
    self._tweenIndex = self._tweenIndex+1
    if self.stops[self._tweenIndex] or self._tweenIndex==#self.stops then
        local z = self.containerRotationGo.transform.eulerAngles.z
        if z>180 then
            z = z-360
        end
        self.start:Set(0,0,z)	
        if self._tweenIndex== #self.stops then
            self:Rotation(self.start,self.stopsEnd,self.speeds[self._tweenIndex])
        else
            self:Rotation(self.start,self.stops[self._tweenIndex],self.speeds[self._tweenIndex])
        end
    else
        self._isTweening=false
        self._tweenIndex=0
        TimerHeap:DelTimer(self._timerRatationid)
        self._timerRatationid=-1
    end
end

function RoulettePanel:RouleTimer()
    if self._timerRatationid==-1 then
        self._timerRatationid = TimerHeap:AddSecTimer(0,sTime,0,self._rotationCB)
    end
end

function RoulettePanel:OnTipsClose()
    self.tipsgo:SetActive(false)
end

function RoulettePanel:InitContentList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._ContentList = list
	self._ContentListlView = scrollView 
    self._ContentListlView:SetHorizontalMove(false)
    self._ContentListlView:SetVerticalMove(true)
    self._ContentList:SetItemType(RouletteRecordListItem)
    self._ContentList:SetPadding(0, 0, 0, 0)
    self._ContentList:SetGap(0, 0)
    self._ContentList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    local itemClickedCB = function(idx)
		self:OnContentListItemClicked(idx)
	end
	-- self._ContentList:SetItemClickedCB(itemClickedCB)
end

function RoulettePanel:OnContentListItemClicked(idx)
   
end

function RoulettePanel:InitRewardList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Tips/ScrollViewList")
	self._rewardlist = list
	self._rewardlistlView = scrollView 
    self._rewardlistlView:SetHorizontalMove(false)
    self._rewardlistlView:SetVerticalMove(true)
    self._rewardlist:SetItemType(RouletteRewardListItem)
    self._rewardlist:SetPadding(20, 0, 0, 0)
    self._rewardlist:SetGap(0, 0)
    self._rewardlist:SetDirection(UIList.DirectionTopToDown, 1, -1)
    self.tipsgo:SetActive(false)
    local itemClickedCB = function(idx)
		self:OnListItemClicked(idx)
	end
	-- self._rewardlist:SetItemClickedCB(itemClickedCB)
end

function RoulettePanel:OnListItemClicked(idx)

end

function RoulettePanel:CloseBtnClick()
	GUIManager.ClosePanel(PanelsConfig.Rank)
end

function RoulettePanel:OnShow(data)
    self._iconBg = self:FindChildGO("Image_Icon")
    GameWorld.AddIcon(self._iconBg,13512)
    self:RightBgIsActive(false)
    self:InitCountDownTimer()
    RouletteManager.RouletteTotalCountRpc()
    RouletteManager.RoulettePersonalListRpc()
    RouletteManager.RouletteBigRewardListRpc()
    self:AddEventListeners()
    self:InitTimerUI()    
    self:UpdateBuyCount()
    self.rotationRewards = RouletteData:GetRoleRewards()
    for i=1,9 do
        local v = self.rotationRewards[i]
        if v then
            self.rewards[i]:SetItem(v[1],v[2])
        end
    end
end
function RoulettePanel:GetRewardAngles()
    local r = RouletteData:GetRecordReward()
    for i,v in ipairs(self.rotationRewards) do
        if v[1]==r[1] then
            self._seleIndex = i
            return (tonumber(i)-1)*45
        end
    end
end
function RoulettePanel:InitCountDownTimer()
    local lefttime = DateTimeUtil.MinusSec(self._endtimestamp)
    self:InitLefetime(lefttime)
end

function RoulettePanel:InitLefetime(lefttime)
    if self._timerCountDownId~=-1 then
        TimerHeap:DelTimer(self._timerCountDownId)
        self._timerCountDownId=-1
    end
    self._seconds = lefttime
    if self._seconds>0 then
        if self._timerCountDownId==-1 then
            self._timerCountDownId=TimerHeap:AddSecTimer(0,1,0,self.timrCountTimer)
        end
        self.timeText.text=DateTimeUtil:FormatFullTime(self._seconds)
    else
        if RouletteManager:IsOnOpenTime() then
            self._seconds = DateTimeUtil.OneHourTime*RouletteData:GetTimeEnd() 
            if self._timerCountDownId==-1 then
                self._timerCountDownId=TimerHeap:AddSecTimer(0,1,0,self.timrCountTimer)
            end
            self.timeText.text=DateTimeUtil:FormatFullTime(self._seconds)
        else
            self.timeText.text=LanguageDataHelper.GetContent(80817)
        end
    end  
end

function RoulettePanel:OlockUpdata()
    self._endtimestamp = DateTimeUtil.TodayTimeStamp(RouletteData:GetTimeEnd(),0) 
    local left = DateTimeUtil.OneHourTime*RouletteData:GetTimeEnd()
    self:InitLefetime(left)
end

function RoulettePanel:CountDownTimer()
    self._seconds=self._seconds-1
    if self._seconds<=0 then
        if self._timerCountDownId~=-1 then
            TimerHeap:DelTimer(self._timerCountDownId)
            self._timerCountDownId=-1
        end
    end
    self.timeText.text=DateTimeUtil:FormatFullTime(self._seconds)
end

function RoulettePanel:InitTimerUI()
    self._timerid=TimerHeap:AddSecTimer(0,10,0,self.timeFunc)
end
function RoulettePanel:UpdateBuyCount()
    local entity = GameWorld.Player()
    local a1 = entity.roulette_times or 0
    local a2 = RouletteData:GetLimitTime()..':00'
    local arg = LanguageDataHelper.GetArgsTable()
    local v = entity.vip_level
    arg['0']= BaseUtil.GetColorString(a1,ColorConfig.J)
    arg['1']= BaseUtil.GetColorString(a2,ColorConfig.A)
    arg['2']= v
    arg['3']= VipYunpanDataHelper:GetNumByVip(v)
    self.tipcountText.text = LanguageDataHelper.CreateContent(76942, arg)
end

function RoulettePanel:OnClose()
    self._iconBg = nil
    self:RemoveEventListeners()
    TimerHeap:DelTimer(self._timerid)
    TimerHeap:DelTimer(self._timerCountDownId)
    TimerHeap:DelTimer(self._timerRatationid)
    self.containerRotationGo.transform.eulerAngles.z=0
    self._timerCountDownId=-1
    self._timerid=-1
    self._tweenIndex=0
    self._isTweening=false
    self._timerRatationid = -1

    for i,g in pairs(self.rewardssele) do
        if g and g.activeSelf then
            g:SetActive(false)
        end
    end
end

function RoulettePanel:OnCloseClick()
    GUIManager.ClosePanel(PanelsConfig.Roulette)
end
function RoulettePanel:GetRankListResp()
    local listData = RouletteData:GetRouleRecordData()
    self._ContentList:SetDataList(listData)
end

function RoulettePanel:WinBGType()
    return PanelWinBGType.NoBG
end