-- RouletteModule.lua
RouletteModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function RouletteModule.Init()
    GUIManager.AddPanel(PanelsConfig.Roulette, "Panel_Roulette", GUILayer.LayerUIPanel, "Modules.ModuleRoulette.RoulettePanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return RouletteModule
