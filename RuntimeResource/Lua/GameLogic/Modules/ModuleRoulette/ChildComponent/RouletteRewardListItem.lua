-- RouletteRewardListItem.lua
require "Modules.ModuleRoulette.ChildComponent.RouletteRewardItem"
local RouletteRewardItem = ClassTypes.RouletteRewardItem
local RouletteRewardListItem = Class.RouletteRewardListItem(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
RouletteRewardListItem.interface = GameConfig.ComponentsConfig.Component
RouletteRewardListItem.classPath = "Modules.ModuleRoulette.ChildComponent.RouletteRewardListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local ColorConfig = GameConfig.ColorConfig

function RouletteRewardListItem:Awake()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._List = list
	self._ListlView = scrollView 
    self._ListlView:SetHorizontalMove(true)
    self._ListlView:SetVerticalMove(false)
    self._List:SetItemType(RouletteRewardItem)
    self._List:SetPadding(15, 0, 0, 20)
    self._List:SetGap(0, 10)
    self._List:SetDirection(UIList.DirectionLeftToRight, 10, -1)
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper')  
    self._timeText = self:GetChildComponent("Text_Time",'TextMeshWrapper')  

end

function RouletteRewardListItem:OnDestroy() 

end

--override
function RouletteRewardListItem:OnRefreshData(data)
    if data._name then
        self._nameText.text=BaseUtil.GetColorString(data._name,ColorConfig.E)
    else
        self._nameText.text=BaseUtil.GetColorString(LanguageDataHelper.GetContent(80833),ColorConfig.B)
    end
    self._timeText.text=data:GetRewardTime()
    self._List:SetDataList(data:GetRewards())
end
