-- RouletteRecordListItem.lua
local RouletteRecordListItem = Class.RouletteRecordListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RouletteRecordListItem.interface = GameConfig.ComponentsConfig.Component
RouletteRecordListItem.classPath = "Modules.ModuleRoulette.ChildComponent.RouletteRecordListItem"


function RouletteRecordListItem:Awake()
    self._nameText = self:GetChildComponent("Text_Info",'TextMeshWrapper')  
end

function RouletteRecordListItem:OnDestroy() 

end

--override
function RouletteRecordListItem:OnRefreshData(data)
    local arg = LanguageDataHelper.GetArgsTable()
    arg['0']= BaseUtil.GetColorString(data,ColorConfig.E)
    self._nameText.text = LanguageDataHelper.CreateContent(76941, arg)
end

