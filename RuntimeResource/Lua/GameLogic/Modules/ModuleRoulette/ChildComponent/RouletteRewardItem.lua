-- RouletteRewardItem.lua
require "UIComponent.Extend.ItemGrid"
local RouletteRewardItem = Class.RouletteRewardItem(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
RouletteRewardItem.interface = GameConfig.ComponentsConfig.Component
RouletteRewardItem.classPath = "Modules.ModuleRoulette.ChildComponent.RouletteRewardItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local public_config = GameWorld.public_config
function RouletteRewardItem:Awake()
    local g = GUIManager.AddItem(self:FindChildGO('Container_Item'), 1)
    self.reward =  UIComponentUtil.AddLuaUIComponent(g,ItemGrid)
    self.reward:ActivateTipsById()
end

function RouletteRewardItem:OnDestroy() 

end

--override
function RouletteRewardItem:OnRefreshData(data)
    self.reward:SetItem(data[public_config.ITEM_KEY_ID],data[public_config.ITEM_KEY_COUNT])
end
