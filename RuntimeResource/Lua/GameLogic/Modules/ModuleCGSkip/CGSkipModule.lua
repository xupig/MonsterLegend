--CGSkipModule.lua
CGSkipModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function CGSkipModule.Init()
	GUIManager.AddPanel(PanelsConfig.CGSkip, "Panel_CGSkip", GUILayer.LayerUICG, "Modules.ModuleCGSkip.CGSkipPanel", true, PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return CGSkipModule