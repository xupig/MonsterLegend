--CGSkipPanel.lua
local CGSkipPanel = Class.CGSkipPanel(ClassTypes.BasePanel)
local LocalSetting = GameConfig.LocalSetting
local GuideManager = GameManager.GuideManager
local DramaManager = GameManager.DramaManager

function CGSkipPanel:Awake()
    self:InitPanel()
end

function CGSkipPanel:OnDestroy()

end

function CGSkipPanel:InitPanel()
    self._button = self:FindChildGO("Button_Skip")
	self._csBH:AddClick(self._button,function() self:OnSkipButtonClick() end)
end

function CGSkipPanel:OnSkipButtonClick(text)
    DramaManager:SkipCurRunningDrama()
end