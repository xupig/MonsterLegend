-- GuildBanquetRankInfoItem.lua
local GuildBanquetRankInfoItem = Class.GuildBanquetRankInfoItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local StringStyleUtil = GameUtil.StringStyleUtil

GuildBanquetRankInfoItem.interface = GameConfig.ComponentsConfig.Component
GuildBanquetRankInfoItem.classPath = "Modules.ModuleGuildBanquetSpace.ChildComponent.GuildBanquetRankInfoItem"

function GuildBanquetRankInfoItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function GuildBanquetRankInfoItem:OnDestroy()
end

function GuildBanquetRankInfoItem:InitView()
    self._rankText = self:GetChildComponent("Text_Rank", "TextMeshWrapper")
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._scoreText = self:GetChildComponent("Text_Score", "TextMeshWrapper")
end

--override {rank,name,score}
function GuildBanquetRankInfoItem:OnRefreshData(data)
    self._rankText.text = data[1]
    local guildName = data[2] or "---"
    self._nameText.text = LanguageDataHelper.CreateContentWithArgs(53174, {["0"]=guildName})
    local score = data[3] or 0
    score = StringStyleUtil.GetLongNumberString(score)
    self._scoreText.text = LanguageDataHelper.CreateContentWithArgs(53175, {["0"]=score})
end
