GuildBanquetSpaceModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function GuildBanquetSpaceModule.Init()
    GUIManager.AddPanel(PanelsConfig.GuildBanquetSpace,"Panel_GuildBanquetSpace",GUILayer.LayerUIMiddle,"Modules.ModuleGuildBanquetSpace.GuildBanquetSpacePanel",true,PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return GuildBanquetSpaceModule