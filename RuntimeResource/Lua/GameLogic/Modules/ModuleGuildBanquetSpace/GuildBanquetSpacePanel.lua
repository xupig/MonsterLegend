require "Modules.ModuleGuildBanquetSpace.ChildComponent.GuildBanquetRankInfoItem"

local GuildBanquetSpacePanel = Class.GuildBanquetSpacePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InstanceManager = PlayerManager.InstanceManager
local UIProgressBar = ClassTypes.UIProgressBar
local UIToggleGroup = ClassTypes.UIToggleGroup
local GuildBanquetRankInfoItem = ClassTypes.GuildBanquetRankInfoItem
local StringStyleUtil = GameUtil.StringStyleUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper
local public_config = require("ServerConfig/public_config")
local GuildBanquetManager = PlayerManager.GuildBanquetManager
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper

local MAX_COUNT = 3

--override
function GuildBanquetSpacePanel:Awake()
    self.disableCameraCulling = true
    self:InitView()
    self:InitListenerFunc()
end


function GuildBanquetSpacePanel:InitListenerFunc()
    self._setGuildBanquetInfo = function(data) self:SetGuildBanquetInfo(data) end
    self._setGuildBanquetState = function(data) self:SetGuildBanquetState(data) end
    self._setGuildBanquetRank = function(data) self:SetGuildBanquetRank(data) end
    self._onClientEntityCollectComplete = function(entityCfgId, eventId)self:OnClientEntityCollectComplete(entityCfgId, eventId) end
end

--override
function GuildBanquetSpacePanel:OnDestroy()

end

function GuildBanquetSpacePanel:OnShow(data)
    self:SetData(data)
end

function GuildBanquetSpacePanel:SetData(data)
    self:RefreshView(data)
end

function GuildBanquetSpacePanel:OnClose()
    self:Reset()
end

function GuildBanquetSpacePanel:OnEnable()
    self:AddEventListeners()
    self._endTimeShow = false
    local openConfig = ActivityDailyDataHelper:GetTimeLimit(public_config.ACTIVITY_ID_GUILD_BONFIRE)
    self.endTime =  DateTimeUtil.TodayTimeStamp(openConfig[1]+openConfig[3], openConfig[2]+openConfig[4])
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateCountDown() end)
end

function GuildBanquetSpacePanel:OnDisable()
    self:RemoveEventListeners()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function GuildBanquetSpacePanel:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.Set_Guild_Banquet_Info, self._setGuildBanquetInfo)

    EventDispatcher:AddEventListener(GameEvents.Set_Guild_Banquet_State, self._setGuildBanquetState)
   
    EventDispatcher:AddEventListener(GameEvents.Set_Guild_Banquet_Rank, self._setGuildBanquetRank)
   
    EventDispatcher:AddEventListener(GameEvents.CLIENT_ENTITY_COLLECT_COMPLETE, self._onClientEntityCollectComplete)
end

function GuildBanquetSpacePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guild_Banquet_Info, self._setGuildBanquetInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guild_Banquet_State, self._setGuildBanquetState)
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guild_Banquet_Rank, self._setGuildBanquetRank)
    EventDispatcher:RemoveEventListener(GameEvents.CLIENT_ENTITY_COLLECT_COMPLETE, self._onClientEntityCollectComplete)
end

function GuildBanquetSpacePanel:InitView()
    self._contributionText = self:GetChildComponent("Container_Rank/Text_Contribution", "TextMeshWrapper")
    self._expText = self:GetChildComponent("Container_Rank/Text_Exp", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Rank/ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(GuildBanquetRankInfoItem)
    self._list:SetPadding(5, 0, 0, 2)
    self._list:SetGap(2, 2)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
    self._scrollView:SetScrollRectEnable(false)

    self._pigButton = self:FindChildGO("Container_Rank/Button_Pig")
    self._csBH:AddClick(self._pigButton,function() self:OnPigButtonClick() end)
    self._questionButton = self:FindChildGO("Container_Rank/Button_Question")
    self._csBH:AddClick(self._questionButton,function() self:OnQuestionButtonClick() end)

    self._instanceEndTimeContainer = self:FindChildGO("Container_InstanceEndTime")
    self._instanceEndTimeContainer:SetActive(false)
    self._endTimeText = self:GetChildComponent("Container_InstanceEndTime/Text_Time", "TextMeshWrapper")
end

function GuildBanquetSpacePanel:RefreshView(data)

end

function GuildBanquetSpacePanel:UpdateCountDown()
    self:ShowInstanceEndTime()
end

function GuildBanquetSpacePanel:ShowInstanceEndTime()
    local now = DateTimeUtil.GetServerTime()
    local leftTime = math.max(self.endTime - DateTimeUtil.GetServerTime(), 0)
    local state = false
    if leftTime <= GlobalParamsHelper.GetParamValue(885) then
        state = true
    end
    if self._endTimeShow ~= state then
        self._endTimeShow = state
        self._instanceEndTimeContainer:SetActive(state)
    end
    if self._endTimeShow then
        self._endTimeText.text = LanguageDataHelper.CreateContentWithArgs(53461, {["0"]=leftTime})
    end
end

function GuildBanquetSpacePanel:SetGuildBanquetInfo(data)
    self._expText.text = LanguageDataHelper.CreateContentWithArgs(53395, {["0"]=StringStyleUtil.GetLongNumberString(data[1])})
    self._contributionText.text = LanguageDataHelper.CreateContentWithArgs(53396, {["0"]=data[2]})
end

function GuildBanquetSpacePanel:SetGuildBanquetState(data)
    if data[1] == public_config.BONFIRE_STATE_ANSWER then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Banquet_Question_Info, data)
    end
end

function GuildBanquetSpacePanel:SetGuildBanquetRank(data)
    local result = {}
    for i=1,MAX_COUNT do
        if data[i] == nil then
            table.insert(result, {i})
        else
            table.insert(result, {i, data[i][public_config.RANK_KEY_GUILD_NAME], data[i][public_config.RANK_KEY_BONFIRE_SCORE]})
        end
    end
    self._list:SetDataList(result)
end

function GuildBanquetSpacePanel:OnClientEntityCollectComplete(entityCfgId, eventId)
    GuildBanquetManager:GuildBanquetGetItemReq()
end

function GuildBanquetSpacePanel:Reset()
    self._endTimeShow = false
    self._instanceEndTimeContainer:SetActive(false)
end

function GuildBanquetSpacePanel:OnCloseButtonClick()
    self._questionContainer:SetActive(false)
end

function GuildBanquetSpacePanel:OnPigButtonClick()
    local x = GlobalParamsHelper.GetParamValue(668)[1]
    local z = GlobalParamsHelper.GetParamValue(668)[2]
    local pos = Vector3.New(x, 0, z)
    GameWorld.Player():Move(pos, -1)
end

function GuildBanquetSpacePanel:OnQuestionButtonClick()
    GUIManager.ShowPanel(PanelsConfig.Chat,{channelIndex=6})
end

function GuildBanquetSpacePanel:WinBGType()
    return PanelWinBGType.NoBG
end