-- TradeMySeekListItem.lua
--求购列表Item类
local TradeMySeekListItem = Class.TradeMySeekListItem(ClassTypes.UIListItem)

TradeMySeekListItem.interface = GameConfig.ComponentsConfig.PointableComponent
TradeMySeekListItem.classPath = "Modules.ModuleTrade.ChildComponent.TradeMySeekListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemConfig = GameConfig.ItemConfig
local TradeDataHelper = GameDataHelper.TradeDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function TradeMySeekListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._iconContainer:ActivateTipsById()

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	--self._txtPlayerName = self:GetChildComponent("Text_PlayerName",'TextMeshWrapper')
	self._txtGradeOrLevel = self:GetChildComponent("Text_GradeOrLevel", 'TextMeshWrapper')

	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected:SetActive(false)
end

--override
function TradeMySeekListItem:OnRefreshData(data)
	local itemCfg = data[1]
	local itemId = itemCfg.id
	self._iconContainer:SetItem(itemId)
	self._txtName.text = ItemDataHelper.GetItemNameWithColor(itemId)
	--self._txtPlayerName.text =  ""--GameWorld.Player().name
	--LoggerHelper.Error(tostring(itemId))
	local auctionType = ItemDataHelper.GetAuctionType(itemId)
	local auctionSubType = ItemDataHelper.GetAuctionSubType(itemId)
	local gradeType = TradeDataHelper:GetTradeCfgByType(auctionType,auctionSubType).gradeType

	if gradeType == 1 then
		local grade = ItemDataHelper.GetEquipGrade(itemId)
		self._txtGradeOrLevel.text = StringStyleUtil.GetChineseNumber(grade)..LanguageDataHelper.CreateContent(305)
	else
		self._txtGradeOrLevel.text = "Lv."..ItemDataHelper.GetLevelNeed(itemId)
	end
end

function TradeMySeekListItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end