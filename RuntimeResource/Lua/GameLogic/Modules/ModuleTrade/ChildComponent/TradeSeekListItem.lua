-- TradeSeekListItem.lua
--求购列表Item类
local TradeSeekListItem = Class.TradeSeekListItem(ClassTypes.UIListItem)

TradeSeekListItem.interface = GameConfig.ComponentsConfig.PointableComponent
TradeSeekListItem.classPath = "Modules.ModuleTrade.ChildComponent.TradeSeekListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ConvertStyle = GameMain.LuaFacade.ConvertStyle
local ItemConfig = GameConfig.ItemConfig
local TradeDataHelper = GameDataHelper.TradeDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TradeManager = PlayerManager.TradeManager
local GUIManager = GameManager.GUIManager

function TradeSeekListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._iconContainer:ActivateTipsByItemData()

	local iconDiamond = self:FindChildGO("Container_Diamond")
	GameWorld.AddIcon(iconDiamond, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtPlayerName = self:GetChildComponent("Text_PlayerName",'TextMeshWrapper')
	self._txtTotalPrice = self:GetChildComponent("Text_TotalPrice",'TextMeshWrapper')
	self._txtGradeOrLevel = self:GetChildComponent("Text_GradeOrLevel", 'TextMeshWrapper')

	self._btnOperate = self:FindChildGO("Button_Operate")
    self._csBH:AddClick(self._btnOperate, function() self:OnClickOperate() end)
    self._txtOperate = self:GetChildComponent("Button_Operate/Text", 'TextMeshWrapper')
    self._requestSellCb = function ()
    	self:RequestSell()
    end
end

--override
function TradeSeekListItem:OnRefreshData(data)
	local playerName = data:GetPlayerName()
	local itemData = data:GetItemData()
	local itemId = itemData.cfg_id
	self._begDBId = data:GetDBId()
	self._iconContainer:SetItemData(itemData)
	self._txtName.text = ItemDataHelper.GetItemNameWithColor(itemId)
	self._txtPlayerName.text = playerName
	local auctionType = ItemDataHelper.GetAuctionType(itemId)
	local auctionSubType = ItemDataHelper.GetAuctionSubType(itemId)
	local gradeType = TradeDataHelper:GetTradeCfgByType(auctionType,auctionSubType).gradeType

	if gradeType == 1 then
		local grade = ItemDataHelper.GetEquipGrade(itemId)
		self._txtGradeOrLevel.text = StringStyleUtil.GetChineseNumber(grade)..LanguageDataHelper.CreateContent(305)
	else
		self._txtGradeOrLevel.text = "Lv."..ItemDataHelper.GetLevelNeed(itemId)
	end

	local price = data:GetPrice()
	self._txtTotalPrice.text = tostring(price)

	if playerName == GameWorld.Player().name then
		self._txtOperate.text = LanguageDataHelper.CreateContent(73515)
		self._isMe = true
	else
		self._txtOperate.text = LanguageDataHelper.CreateContent(73498)
		self._isMe = false
	end
end

function TradeSeekListItem:OnClickOperate()
	if self._isMe then
		TradeManager:RequestBegCancel(self._begDBId)
	else
		GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContent(73525),self._requestSellCb)
	end
end

function TradeSeekListItem:RequestSell()
	TradeManager:RequestBegSell(self._begDBId)
end