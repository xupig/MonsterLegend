-- TradeFilterComboboxItem.lua
--交易筛选列表Item类
local TradeFilterComboboxItem = Class.TradeFilterComboboxItem(ClassTypes.UIListItem)

TradeFilterComboboxItem.interface = GameConfig.ComponentsConfig.PointableComponent
TradeFilterComboboxItem.classPath = "Modules.ModuleTrade.ChildComponent.TradeFilterComboboxItem"

function TradeFilterComboboxItem:Awake()
	self._txtInfo = self:GetChildComponent("Text_Info",'TextMeshWrapper')
end

--override
function TradeFilterComboboxItem:OnRefreshData(data)
	self._txtInfo.text = data
end