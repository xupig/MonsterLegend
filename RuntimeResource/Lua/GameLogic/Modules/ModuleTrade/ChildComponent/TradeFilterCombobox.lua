-- TradeFilterCombobox.lua
--交易筛选列表Combobox类
local TradeFilterCombobox = Class.TradeFilterCombobox(ClassTypes.BaseComponent)

TradeFilterCombobox.interface = GameConfig.ComponentsConfig.Component
TradeFilterCombobox.classPath = "Modules.ModuleTrade.ChildComponent.TradeFilterCombobox"

require "Modules.ModuleTrade.ChildComponent.TradeFilterComboboxItem"
local TradeFilterComboboxItem = ClassTypes.TradeFilterComboboxItem
local UIList = ClassTypes.UIList

function TradeFilterCombobox:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self:InitComps()
end

function TradeFilterCombobox:InitComps()
	self._btnExpand = self:FindChildGO("Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnEpandClick() end)

	self._txtFilterName = self:GetChildComponent("Text_FilterName",'TextMeshWrapper')

	self._imgExpand = self:FindChildGO("Image_Expand")
	self._imgContract = self:FindChildGO("Image_Contract")
	---------------------------------------------------
	self._containerList = self:FindChildGO("Container_List")
	self._bg = self:FindChildGO("Container_List/Image_Bg")
	self._bgRectTransform = self._bg:GetComponent(typeof(UnityEngine.RectTransform))


	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_List/ScrollViewList_Type")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(TradeFilterComboboxItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 6)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB =                
	function(idx)
		self:OnListItemClicked(idx)
	end
	self._list:SetItemClickedCB(itemClickedCB)
	self._listTrans = self:FindChildGO("Container_List/ScrollViewList_Type").transform
end

function TradeFilterCombobox:UpdateData(listData,comboboxType)
	self._list:SetDataList(listData)
	self._comboboxType = comboboxType

	local len = #listData
	if len >= 5 then
		self._bgRectTransform.sizeDelta = Vector2(128,228)
		self._listTrans.anchoredPosition = Vector3(0,2,0)
	else
		self._bgRectTransform.sizeDelta = Vector2(128,len*44+8)
		self._listTrans.anchoredPosition = Vector3(0,2 - 44*(5-len) ,0)
	end

	self._listData = listData
	self:Reset()
end

function TradeFilterCombobox:Reset()
	self:SetExpand(false)
	self._txtFilterName.text = self._listData[#self._listData]
end

function TradeFilterCombobox:OnEpandClick()
	self:SetExpand(not self._isExpand)
end

function TradeFilterCombobox:SetExpand(isExpand)
	self._isExpand = isExpand
	self._containerList:SetActive(isExpand)
	self._imgExpand:SetActive(isExpand)
	self._imgContract:SetActive(not isExpand)
end

function TradeFilterCombobox:OnListItemClicked(idx)
	self._txtFilterName.text = self._list:GetDataByIndex(idx)
	self:SetExpand(false)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRADE_UPDATE_FILTER,idx,self._comboboxType)
end