-- TradeSellListItem.lua
--背包列表Item类
local TradeSellListItem = Class.TradeSellListItem(ClassTypes.UIListItem)

TradeSellListItem.interface = GameConfig.ComponentsConfig.PointableComponent
TradeSellListItem.classPath = "Modules.ModuleTrade.ChildComponent.TradeSellListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config

function TradeSellListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)

	local iconDiamond = self:FindChildGO("Container_Money")
	GameWorld.AddIcon(iconDiamond, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtPrice = self:GetChildComponent("Text_Price",'TextMeshWrapper')
end

--override
function TradeSellListItem:OnRefreshData(data)
	local itemData = data:GetItemData()
	self._iconContainer:SetItemData(itemData)
	self._txtName.text = ItemDataHelper.GetItemNameWithColor(itemData.cfg_id)
	self._txtPrice.text = tostring(data:GetPrice())
end


function TradeSellListItem:OnPointerDown(pointerEventData)
	--LoggerHelper.Error("OnPointerDown")
end