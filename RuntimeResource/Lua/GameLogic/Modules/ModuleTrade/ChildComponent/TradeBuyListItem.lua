-- TradeBuyListItem.lua
--背包列表Item类
local TradeBuyListItem = Class.TradeBuyListItem(ClassTypes.UIListItem)

TradeBuyListItem.interface = GameConfig.ComponentsConfig.PointableComponent
TradeBuyListItem.classPath = "Modules.ModuleTrade.ChildComponent.TradeBuyListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ConvertStyle = GameMain.LuaFacade.ConvertStyle
local ItemConfig = GameConfig.ItemConfig
local TradeDataHelper = GameDataHelper.TradeDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TipsManager = GameManager.TipsManager

function TradeBuyListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)

	local iconDiamond1 = self:FindChildGO("Container_Diamond1")
	GameWorld.AddIcon(iconDiamond1, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
	local iconDiamond2 = self:FindChildGO("Container_Diamond2")
	GameWorld.AddIcon(iconDiamond2, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtSinglePrice = self:GetChildComponent("Text_SinglePrice",'TextMeshWrapper')
	self._txtTotalPrice = self:GetChildComponent("Text_TotalPrice",'TextMeshWrapper')
	self._txtGradeOrLevel = self:GetChildComponent("Text_GradeOrLevel", 'TextMeshWrapper')

	self._containerScore = self:FindChildGO("Container_EquipScore")
	self._txtScore = self:GetChildComponent("Container_EquipScore/Text_Score",'TextMeshWrapper')
	self._imgLock = self:FindChildGO("Image_Lock")

	self._imgCanNotUse = self:FindChildGO("Image_CanNotUse")
	self._imgArrowUp = self:FindChildGO("Image_ArrowUp")
	self._imgArrowDown = self:FindChildGO("Image_ArrowDown")
end

--override
function TradeBuyListItem:OnRefreshData(data)
	local itemData = data:GetItemData()
	self._itemData = itemData
	local itemId = itemData.cfg_id
	self._iconContainer:SetItemData(itemData)
	self._txtName.text = ItemDataHelper.GetItemNameWithColor(itemId)
	local auctionType = ItemDataHelper.GetAuctionType(itemId)
	local auctionSubType = ItemDataHelper.GetAuctionSubType(itemId)
	local gradeType = TradeDataHelper:GetTradeCfgByType(auctionType,auctionSubType).gradeType

	if gradeType == 1 then
		local grade = ItemDataHelper.GetEquipGrade(itemId)
		self._txtGradeOrLevel.text = StringStyleUtil.GetChineseNumber(grade)..LanguageDataHelper.CreateContent(305)
	else
		self._txtGradeOrLevel.text = "Lv."..ItemDataHelper.GetLevelNeed(itemId)
	end

	if itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP then
		self._containerScore:SetActive(true)
		self._txtScore.text = itemData:GetBaseFightPower()
		if itemData:CheckVocationMatchGeneral() then
			self._imgCanNotUse:SetActive(false)
			if itemData:CheckBetterThanLoaded() then
				self._imgArrowUp:SetActive(true)
				self._imgArrowDown:SetActive(false)
			else
				self._imgArrowUp:SetActive(false)
				self._imgArrowDown:SetActive(true)
			end
		else
			self._imgCanNotUse:SetActive(true)
			self._imgArrowUp:SetActive(false)
			self._imgArrowDown:SetActive(false)
		end
	else
		self._containerScore:SetActive(false)
		self._imgCanNotUse:SetActive(false)
		self._imgArrowUp:SetActive(false)
		self._imgArrowDown:SetActive(false)
	end

	if data:GetPassword() == -1 then
		self._imgLock:SetActive(false)
	else
		self._imgLock:SetActive(true)
	end

	local price = data:GetPrice()
	local singlePrice = math.ceil(price/itemData:GetCount())
	self._txtSinglePrice.text = tostring(singlePrice)
	if price > GameWorld.Player().money_coupons then
		self._txtTotalPrice.text = ConvertStyle(string.format("$(%d,%s)",ItemConfig.ItemForbidden,price))
	else
		self._txtTotalPrice.text = tostring(price)
	end
end