-- TradeRecordListItem.lua
--背包列表Item类
local TradeRecordListItem = Class.TradeRecordListItem(ClassTypes.UIListItem)

TradeRecordListItem.interface = GameConfig.ComponentsConfig.PointableComponent
TradeRecordListItem.classPath = "Modules.ModuleTrade.ChildComponent.TradeRecordListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local DateTimeUtil = GameUtil.DateTimeUtil
local CommonItemData = ClassTypes.CommonItemData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TradeDataHelper = GameDataHelper.TradeDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

function TradeRecordListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._iconContainer:ActivateTipsByItemData()

	local iconDiamond1 = self:FindChildGO("Container_Diamond1")
	GameWorld.AddIcon(iconDiamond1, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
	local iconDiamondIncome = self:FindChildGO("Container_DiamondIncome")
	GameWorld.AddIcon(iconDiamondIncome, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtType = self:GetChildComponent("Text_Type",'TextMeshWrapper')
	self._txtTime = self:GetChildComponent("Text_Time",'TextMeshWrapper')
	self._txtTax  = self:GetChildComponent("Text_Tax",'TextMeshWrapper')
	self._txtIncome  = self:GetChildComponent("Text_Income",'TextMeshWrapper')
	self._txtGradeOrLevel = self:GetChildComponent("Text_GradeOrLevel",'TextMeshWrapper')
	self._txtTitleGradeOrLevel = self:GetChildComponent("Text_TitleGradeOrLevel",'TextMeshWrapper')
end

--override
function TradeRecordListItem:OnRefreshData(data)
	-- LoggerHelper.Log(data)
	local operation_type = data[1]
	local itemData = CommonItemData.CreateItemData(data[2])
	local itemId = itemData.cfg_id
	local deal_time  = data[3]
	local tax_price = data[4]
	local price = data[5]

	self._iconContainer:SetItemData(itemData)

	self._txtName.text = ItemDataHelper.GetItemNameWithColor(itemId)
	self._txtTime.text = DateTimeUtil.GetDateFullStr(deal_time)
	self._txtTax.text = tostring(tax_price)
	self._txtIncome.text = tostring(price)
	if operation_type == public_config.AUCTION_OPERATION_BUY or operation_type == public_config.AUCTION_OPERATION_BEG then
		self._txtType.text = LanguageDataHelper.CreateContent(73497)
	else
		self._txtType.text = LanguageDataHelper.CreateContent(73498)
	end

	local auctionType = ItemDataHelper.GetAuctionType(itemId)
	local auctionSubType = ItemDataHelper.GetAuctionSubType(itemId)
	local gradeType = TradeDataHelper:GetTradeCfgByType(auctionType,auctionSubType).gradeType

	if gradeType == 1 then
		self._txtTitleGradeOrLevel.text = LanguageDataHelper.CreateContent(53147)
		local grade = ItemDataHelper.GetEquipGrade(itemId)
		self._txtGradeOrLevel.text = StringStyleUtil.GetChineseNumber(grade)..LanguageDataHelper.CreateContent(305)
	else
		self._txtTitleGradeOrLevel.text = LanguageDataHelper.CreateContent(40007)
		self._txtGradeOrLevel.text = "Lv."..ItemDataHelper.GetLevelNeed(itemId)
	end
end