-- TradeSellBagListItem.lua
--交易出售背包列表Item类
local TradeSellBagListItem = Class.TradeSellBagListItem(ClassTypes.UIListItem)

TradeSellBagListItem.interface = GameConfig.ComponentsConfig.PointableComponent
TradeSellBagListItem.classPath = "Modules.ModuleTrade.ChildComponent.TradeSellBagListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local BaseUtil = GameUtil.BaseUtil

function TradeSellBagListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_content/Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
end

--override
function TradeSellBagListItem:OnRefreshData(data)
	if data ~= "nil" then
		self._iconContainer:SetItemData(data)
	else
		self._iconContainer:SetItemData(nil)
	end
end

function TradeSellBagListItem:SetUseAndPowerCompare(showEquipUI,canNotUse,isUp)
	if self._imgCanNotUse == nil then
	 	self._imgCanNotUse = self:FindChildGO("Container_content/Image_CanNotUse")
	 	self._imgArrowUp = self:FindChildGO("Container_content/Image_ArrowUp")
	 	self._imgArrowDown = self:FindChildGO("Container_content/Image_ArrowDown")
	end
	if showEquipUI then
		if canNotUse then
			BaseUtil.SetObjectActive(self._imgCanNotUse,true)
			BaseUtil.SetObjectActive(self._imgArrowUp,false)
			BaseUtil.SetObjectActive(self._imgArrowDown,false)
		else
			BaseUtil.SetObjectActive(self._imgCanNotUse,false)
			if isUp then
				BaseUtil.SetObjectActive(self._imgArrowUp,true)
				BaseUtil.SetObjectActive(self._imgArrowDown,false)
			else
				BaseUtil.SetObjectActive(self._imgArrowUp,false)
				BaseUtil.SetObjectActive(self._imgArrowDown,true)
			end
		end
	else
		BaseUtil.SetObjectActive(self._imgCanNotUse,false)
		BaseUtil.SetObjectActive(self._imgArrowUp,false)
		BaseUtil.SetObjectActive(self._imgArrowDown,false)
	end
end


function TradeSellBagListItem:OnPointerDown(pointerEventData)
	--LoggerHelper.Error("OnPointerDown")
end