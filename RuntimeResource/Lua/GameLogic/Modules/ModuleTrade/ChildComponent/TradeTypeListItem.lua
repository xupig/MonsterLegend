-- TradeTypeListItem.lua
--交易道具类型列表Item类
local TradeTypeListItem = Class.TradeTypeListItem(ClassTypes.UIListItem)

TradeTypeListItem.interface = GameConfig.ComponentsConfig.PointableComponent
TradeTypeListItem.classPath = "Modules.ModuleTrade.ChildComponent.TradeTypeListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil

function TradeTypeListItem:Awake()
	self._txtTypeName = self:GetChildComponent("Text_TypeName",'TextMeshWrapper')
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected:SetActive(false)
end

--override
function TradeTypeListItem:OnRefreshData(data)
	if data.all then
		self._name = LanguageDataHelper.CreateContent(73408)
	elseif data.mySeek then
		self._name = LanguageDataHelper.CreateContent(73409)
	else
		for k,v in pairs(data) do
			self._name = LanguageDataHelper.CreateContent(v.typeName)
			break
		end
	end
	self:SetText()
end

function TradeTypeListItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
	self._isSelected = isSelected
	self:SetText()
end

function TradeTypeListItem:SetText()
	if self._isSelected then
		self._txtTypeName.text = BaseUtil.GetColorString(self._name,ColorConfig.A)
	else
		self._txtTypeName.text = self._name
	end
end