-- TradeSubTypeListItem.lua
--交易道具子类型列表Item类
local TradeSubTypeListItem = Class.TradeSubTypeListItem(ClassTypes.UIListItem)

TradeSubTypeListItem.interface = GameConfig.ComponentsConfig.PointableComponent
TradeSubTypeListItem.classPath = "Modules.ModuleTrade.ChildComponent.TradeSubTypeListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function TradeSubTypeListItem:Awake()
	self._containerIcon = self:FindChildGO("Container_Icon")
	self._txtCount = self:GetChildComponent("Text_Count",'TextMeshWrapper')
	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtTitle = self:GetChildComponent("Text_Title",'TextMeshWrapper')
end

--override
function TradeSubTypeListItem:OnRefreshData(data)
	GameWorld.AddIcon(self._containerIcon,data.subtypeIcon)
	if data.pageType == 1 then
		self._txtTitle.text = LanguageDataHelper.CreateContent(73512)..":"
	else
		self._txtTitle.text = LanguageDataHelper.CreateContent(73513)..":"
	end
	self._txtName.text = LanguageDataHelper.CreateContent(data.subtypeName)
	self._txtCount.text = tostring(data.count)
end