-- TradeModule.lua
TradeModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function TradeModule.Init()
	GUIManager.AddPanel(PanelsConfig.Trade, "Panel_Trade", GUILayer.LayerUIPanel, "Modules.ModuleTrade.TradePanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return TradeModule