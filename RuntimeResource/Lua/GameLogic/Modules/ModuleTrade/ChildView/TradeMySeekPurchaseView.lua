-- TradeMySeekPurchaseView.lua
local TradeMySeekPurchaseView = Class.TradeMySeekPurchaseView(ClassTypes.BaseComponent)

TradeMySeekPurchaseView.interface = GameConfig.ComponentsConfig.Component
TradeMySeekPurchaseView.classPath = "Modules.ModuleTrade.ChildView.TradeMySeekPurchaseView"

require "Modules.ModuleTrade.ChildComponent.TradeTypeListItem"
require "Modules.ModuleTrade.ChildComponent.TradeMySeekListItem"
require "Modules.ModuleTrade.ChildComponent.TradeFilterCombobox"

local TradeTypeListItem = ClassTypes.TradeTypeListItem
local TradeMySeekListItem = ClassTypes.TradeMySeekListItem
local TradeFilterCombobox = ClassTypes.TradeFilterCombobox

local UIList = ClassTypes.UIList
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local TradeDataHelper = GameDataHelper.TradeDataHelper
local TradeManager = PlayerManager.TradeManager
local tradeData = PlayerManager.PlayerDataManager.tradeData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

function TradeMySeekPurchaseView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function TradeMySeekPurchaseView:InitCallBack()
	self._updateSeekListCb = function (idx)
		self:UpdateSeekList(idx)
	end

	self._updateSubTypeCb = function ()
		self:UpdateSubType()
	end

	self._updateSelectStarCb = function (idx,comboboxType)
		self:UpdateSelectStar(idx,comboboxType)
	end
end

function TradeMySeekPurchaseView:InitComps()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "List_ItemType")
    self._listType = list
    self._scrollView = scrollView
    self._listType:SetItemType(TradeTypeListItem)
    self._listType:SetPadding(0, 0, 0, 0)
    self._listType:SetGap(1, 0)
    self._listType:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local typeItemClickedCB =  
	function(idx)
		self:OnTypeListItemClicked(idx)
	end
	self._listType:SetItemClickedCB(typeItemClickedCB)

	local t = TradeDataHelper:GetTradeItemTypeList()
	self._typeListData = {}
	for i=1,#t do
		table.insert(self._typeListData,t[i])
	end
	self._listType:SetDataList(self._typeListData)

	self._inputFieldMeshSearch = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputFieldMesh_Search")
    --self._inputFieldMeshSearch:SetOnEndMeshEditCB(function(text) self:OnEndEditPw(text) end)
    self._inputTextSearch = self:GetChildComponent("InputFieldMesh_Search", 'InputFieldMeshWrapper')

    self._btnSearch = self:FindChildGO("Button_Search")
    self._csBH:AddClick(self._btnSearch, function() self:OnSearchClick() end)

    ------------------------------求购列表--------------------------------------
	self._containerSeekPurchase = self:FindChildGO("Container_SeekPurchase")

    local scrollView1, list1 = UIList.AddScrollViewList(self.gameObject, "Container_SeekPurchase/List_MySeekPurchase")
    self._listSeekItems = list1
    self._scrollView1 = scrollView1
    self._listSeekItems:SetItemType(TradeMySeekListItem)
    self._listSeekItems:SetPadding(0, 0, 0, 0)
    self._listSeekItems:SetGap(1, 0)
    self._listSeekItems:SetDirection(UIList.DirectionTopToDown, 1, 1)
 	local seekItemClickedCB =                
	function(idx)
		self:OnSeekListItemClicked(idx)
	end
	self._listSeekItems:SetItemClickedCB(seekItemClickedCB)

	local iconDiamond1 = self:FindChildGO("Container_SeekPurchase/Container_Diamond1")
	GameWorld.AddIcon(iconDiamond1, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
	local iconDiamond2 = self:FindChildGO("Container_SeekPurchase/Container_Diamond2")
	GameWorld.AddIcon(iconDiamond2, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))

	self._inputFieldMeshCount = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_SeekPurchase/InputFieldMesh_Count")
    self._inputFieldMeshCount:SetOnEndMeshEditCB(function(text) self:OnEndEditCount(text) end)
    self._inputTextCount = self:GetChildComponent("Container_SeekPurchase/InputFieldMesh_Count", 'InputFieldMeshWrapper')

    self._inputFieldMeshPrice = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_SeekPurchase/InputFieldMesh_Price")
    self._inputFieldMeshPrice:SetOnEndMeshEditCB(function(text) self:OnEndEditPrice(text) end)
    self._inputTextPrice = self:GetChildComponent("Container_SeekPurchase/InputFieldMesh_Price", 'InputFieldMeshWrapper')

    self._txtDeposit = self:GetChildComponent("Container_SeekPurchase/Text_Deposit", 'TextMeshWrapper')
    local txtTip = self:GetChildComponent("Container_SeekPurchase/Text_Tip", 'TextMeshWrapper')
    txtTip.text = LanguageDataHelper.CreateContent(73511)
    
    self._comboboxStar = self:AddChildLuaUIComponent("Container_SeekPurchase/Container_Star",TradeFilterCombobox)
    self._comboboxStar:SetActive(false)

    self._btnReset = self:FindChildGO("Container_SeekPurchase/Button_Reset")
    self._csBH:AddClick(self._btnReset, function() self:OnResetClick() end)

    self._btnPublish = self:FindChildGO("Container_SeekPurchase/Button_Publish")
    self._csBH:AddClick(self._btnPublish, function() self:OnPublishClick() end)

	self._txtNoItemGO = self:FindChildGO("Container_SeekPurchase/Text_NoItem")
	
	self._scrollView:SetScrollRectState(false)
	self._scrollView1:SetScrollRectState(false)
end

function TradeMySeekPurchaseView:OnTypeListItemClicked(idx)
	if self._selectedTypeItem then
		self._selectedTypeItem:SetSelected(false)
	end
	self._selectedTypeItem = self._listType:GetItem(idx)
	self._selectedTypeItem:SetSelected(true)

	local subTypeData = self._listType:GetDataByIndex(idx)
	
	for k,v in pairs(subTypeData) do
		self._selectedAuctionType = v.auctionType
	end
	TradeManager:RequestBegMyList()
end

--更新子类数据
function TradeMySeekPurchaseView:UpdateSubType()
	local subTypeListData = {}
	local t = tradeData.myBegItemList
	local cfgList = self._typeListData[self._selectedAuctionType]
	local gradeType
	local all = {}
	all.subtypeIcon = 5033
	all.subtypeName = 73408
	all.pageType = 1
	all.count = 0
	table.insert(subTypeListData,all)
	for k,tradeCfg in pairs(cfgList) do
		tradeCfg.pageType = 2
		tradeCfg.count = 0
		table.insert(subTypeListData,tradeCfg)
		gradeType = tradeCfg.gradeType
		for i=1,#t do
			local itemData = t[i]:GetItemData()
			if itemData:GetAuctionSubType() == tradeCfg.auctionSubtype then
				tradeCfg.count = tradeCfg.count + itemData:GetCount()
			end
		end
		all.count = all.count + tradeCfg.count
	end

	self._containerSeekPurchase:SetActive(false)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_OPEN_TRADE_SELECT_SUBTYPE,subTypeListData,false)
end

--选择完子类
function TradeMySeekPurchaseView:UpdateSeekList(idx)
	self:ClearView()

	local t = ItemDataHelper:GetTradeItemTypeList()[self._selectedAuctionType]

	if idx == 0 then
		self._listBaseData = {}
		
		for k,v in pairs(t) do
			for k1,v1 in pairs(v) do
				table.insert(self._listBaseData,v1)
			end
		end
	else
		local subType = self._typeListData[self._selectedAuctionType][idx].auctionSubtype
		self._listBaseData = t[subType]
	end
	self._listData = self._listBaseData
	self:SetSeekList(self._listData)
end

function TradeMySeekPurchaseView:OnSeekListItemClicked(idx)
	if self._selectedBegItem then
		self._selectedBegItem:SetSelected(false)
	end
	self._selectedBegItem = self._listSeekItems:GetItem(idx)
	self._selectedBegItem:SetSelected(true)

	self._selectedItemDatas = self._listSeekItems:GetDataByIndex(idx)
	self._selectedItemId = self._selectedItemDatas[#self._selectedItemDatas].id

	if ItemDataHelper.GetItemType(self._selectedItemId) == public_config.ITEM_ITEMTYPE_EQUIP then
		self._comboboxStar:SetActive(true)
		local t = {}
		for i=1,#self._selectedItemDatas do
			local star = self._selectedItemDatas[i].star
			local str
			if star == 0 then
				str = LanguageDataHelper.CreateContent(0)
			else
				str = LanguageDataHelper.CreateContent(StringStyleUtil.cnNumber[star])
				str = str..LanguageDataHelper.CreateContent(4544)
			end
			table.insert(t, str)
		end
		self._comboboxStar:UpdateData(t,3)
	else
		self._comboboxStar:SetActive(false)
	end
end

function TradeMySeekPurchaseView:UpdateSelectStar(idx,comboboxType)
	self._selectedItemId = self._selectedItemDatas[idx+1].id
	--LoggerHelper.Error("self._selectedItemId"..self._selectedItemId)
end

function TradeMySeekPurchaseView:OnEndEditCount(text)
	if self._selectedItemId and ItemDataHelper.GetItemType(self._selectedItemId) == public_config.ITEM_ITEMTYPE_EQUIP then
		if self._inputTextCount.text ~= "1" then
			self._inputTextCount.text = "1"
		end
	end
end

function TradeMySeekPurchaseView:OnEndEditPrice(text)
	self._txtDeposit.text = self._inputTextPrice.text
end

function TradeMySeekPurchaseView:OnPublishClick()
	if self._selectedItemId and self._inputTextCount.text ~= "" and tonumber(self._inputTextCount.text)>0 
		and self._inputTextPrice.text ~= "" and tonumber(self._inputTextPrice.text)>0 then
		TradeManager:RequestBegPublish(self._selectedItemId,tonumber(self._inputTextCount.text),tonumber(self._inputTextPrice.text))
	end
end

function TradeMySeekPurchaseView:OnResetClick()
	self._inputTextCount.text = ""
	self._inputTextPrice.text = ""
	self._txtDeposit.text = ""
end

function TradeMySeekPurchaseView:OnSearchClick()
	self:ClearView()
	local str = self._inputTextSearch.text
	if str ~= "" then
		local listData = ItemDataHelper:BegSearchByName(str)
		self:SetSeekList(listData)
	elseif self._listBaseData then
		self:SetSeekList(self._listBaseData)
	end
end

function TradeMySeekPurchaseView:SetSeekList(listData)
	self._listSeekItems:SetDataList(listData)
	if #listData > 0 then
		self:OnSeekListItemClicked(0)
		self._txtNoItemGO:SetActive(false)
	else
		self._txtNoItemGO:SetActive(true)
	end
end


function TradeMySeekPurchaseView:ClearView()
	self._containerSeekPurchase:SetActive(true)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_HIDE_TRADE_SELECT_SUBTYPE)
	self:OnResetClick()
	self._txtNoItemGO:SetActive(false)
end

function TradeMySeekPurchaseView:ShowView()
	self:ClearView()
	self._selectedAuctionType = nil
	self._listSeekItems:SetDataList({})
	self:OnTypeListItemClicked(0)
	self._scrollView:SetScrollRectState(true)
	self._scrollView1:SetScrollRectState(true)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRADE_SELECT_SUBTYPE,self._updateSeekListCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_MY_BEG_DATA,self._updateSubTypeCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRADE_UPDATE_FILTER,self._updateSelectStarCb)
end

function TradeMySeekPurchaseView:CloseView()
	if self._selectedTypeItem then
		self._selectedTypeItem:SetSelected(false)
		self._selectedTypeItem = nil
	end
	if self._selectedBegItem then
		self._selectedBegItem:SetSelected(false)
		self._selectedBegItem = nil
	end
	self._scrollView:SetScrollRectState(false)
	self._scrollView1:SetScrollRectState(false)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRADE_SELECT_SUBTYPE,self._updateSeekListCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_MY_BEG_DATA,self._updateSubTypeCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRADE_UPDATE_FILTER,self._updateSelectStarCb)
end