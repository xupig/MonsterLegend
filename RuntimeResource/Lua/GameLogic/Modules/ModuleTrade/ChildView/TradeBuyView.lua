-- TradeBuyView.lua
local TradeBuyView = Class.TradeBuyView(ClassTypes.BaseComponent)

TradeBuyView.interface = GameConfig.ComponentsConfig.Component
TradeBuyView.classPath = "Modules.ModuleTrade.ChildView.TradeBuyView"

require "Modules.ModuleTrade.ChildComponent.TradeTypeListItem"
require "Modules.ModuleTrade.ChildComponent.TradeBuyListItem"
require "Modules.ModuleTrade.ChildComponent.TradeFilterCombobox"

local TradeFilterCombobox = ClassTypes.TradeFilterCombobox
local TradeTypeListItem = ClassTypes.TradeTypeListItem
local TradeBuyListItem = ClassTypes.TradeBuyListItem
local UIList = ClassTypes.UIList
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local TradeDataHelper = GameDataHelper.TradeDataHelper
local TradeManager = PlayerManager.TradeManager
local TipsManager = GameManager.TipsManager
local tradeData = PlayerManager.PlayerDataManager.tradeData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

function TradeBuyView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function TradeBuyView:InitCallBack()
	self._buyCb = function ()
		self:BuyItem()
	end

	self._updateSubTypeCb = function (auctionType)
		self:UpdateSubType(auctionType)
	end

	self._updateBuyListCb = function (idx)
		self:UpdateBuyList(idx)
	end

	self._updateSearchCb = function ()
		self:UpdateSearch()
	end

	self._updateBuyCompleteCb = function ()
		self:OnBuyComplete()
	end

	self._updateFitlerCb = function (idx,comboboxType)
		self:UpdateFilter(idx,comboboxType)
	end
end

local qualityMap = {1,3,4,5,6,7}
local qualityCN = {137,139,140,141,53164,142}


function TradeBuyView:InitComps()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "List_ItemType")
    self._listType = list
    self._scrollView = scrollView
    self._listType:SetItemType(TradeTypeListItem)
    self._listType:SetPadding(0, 0, 0, 0)
    self._listType:SetGap(1, 0)
    self._listType:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local typeItemClickedCB =                
	function(idx)
		self:OnTypeListItemClicked(idx)
	end
	self._listType:SetItemClickedCB(typeItemClickedCB)

	local t = TradeDataHelper:GetTradeItemTypeList()
	self._typeListData = {}
	for i=1,#t do
		table.insert(self._typeListData,t[i])
	end
	self._listType:SetDataList(self._typeListData)
		

	self._inputFieldMeshSearch = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputFieldMesh_Search")
    --self._inputFieldMeshSearch:SetOnEndMeshEditCB(function(text) self:OnEndEditPw(text) end)
    self._inputTextSearch = self:GetChildComponent("InputFieldMesh_Search", 'InputFieldMeshWrapper')

    self._btnSearch = self:FindChildGO("Button_Search")
    self._csBH:AddClick(self._btnSearch, function() self:OnSearchClick() end)

    ---------------------------选择购买--------------------------------
    self._containerSelectBuy = self:FindChildGO("Container_SelectBuy")

    self._txtGradeOrLevel = self:GetChildComponent("Container_SelectBuy/Container_Title/Text_GradeOrLevel", 'TextMeshWrapper')

    self._btnSinglePriceSort = self:FindChildGO("Container_SelectBuy/Container_Title/Button_SinglePriceSort")
    self._csBH:AddClick(self._btnSinglePriceSort, function() self:OnSinglePriceSort() end)
    self._imgSinglePriceSortAscend = self:FindChildGO("Container_SelectBuy/Container_Title/Button_SinglePriceSort/Image_Ascend")
    self._imgSinglePriceSortDescend = self:FindChildGO("Container_SelectBuy/Container_Title/Button_SinglePriceSort/Image_Descend")
    
    self._btnTotalPriceSort = self:FindChildGO("Container_SelectBuy/Container_Title/Button_TotalPriceSort")
    self._csBH:AddClick(self._btnTotalPriceSort, function() self:OnTotalPriceSort() end)
    self._imgTotalPriceSortAscend = self:FindChildGO("Container_SelectBuy/Container_Title/Button_TotalPriceSort/Image_Ascend")
    self._imgTotalPriceSortDescend = self:FindChildGO("Container_SelectBuy/Container_Title/Button_TotalPriceSort/Image_Descend")


    local scrollView1, list1 = UIList.AddScrollViewList(self.gameObject, "Container_SelectBuy/List_Items")
    self._listBuyItems = list1
    self._scrollView1 = scrollView1
    self._listBuyItems:SetItemType(TradeBuyListItem)
    self._listBuyItems:SetPadding(0, 0, 0, 0)
    self._listBuyItems:SetGap(1, 0)
    self._listBuyItems:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local buyItemClickedCB =                
	function(idx)
		self:OnBuyListItemClicked(idx)
	end
	self._listBuyItems:SetItemClickedCB(buyItemClickedCB)

	self._txtNoItemGO = self:FindChildGO("Container_SelectBuy/Text_NoItem")

	--------------------------筛选combobox-------------------------
	self._containerFilterGrade = self:AddChildLuaUIComponent("Container_SelectBuy/Container_FilterComboboxes/Container_Grade", TradeFilterCombobox)
	self._dGrade = {}
	for i=1,#StringStyleUtil.cnNumber do
		self._dGrade[i] = LanguageDataHelper.CreateContent(StringStyleUtil.cnNumber[i])..LanguageDataHelper.CreateContent(4543)
	end
	self._dGrade[#StringStyleUtil.cnNumber+1] = LanguageDataHelper.CreateContent(73516)
	self._containerFilterGrade:UpdateData(self._dGrade,1)


	self._containerFilterQuality = self:AddChildLuaUIComponent("Container_SelectBuy/Container_FilterComboboxes/Container_Quality", TradeFilterCombobox)
	self._dQuality = {}
	for i=1,#qualityCN do
		self._dQuality[i] = LanguageDataHelper.CreateContent(qualityCN[i])
	end
	self._dQuality[#qualityCN+1] = LanguageDataHelper.CreateContent(73517)
	self._containerFilterQuality:UpdateData(self._dQuality,2)

	self._containerFilterPassword = self:AddChildLuaUIComponent("Container_SelectBuy/Container_FilterComboboxes/Container_Password", TradeFilterCombobox)
	self._dPassword = {}
	self._dPassword[1] = LanguageDataHelper.CreateContent(73520)
	self._dPassword[2] = LanguageDataHelper.CreateContent(73521)
	self._dPassword[3] = LanguageDataHelper.CreateContent(73518)
	self._containerFilterPassword:UpdateData(self._dPassword,4)

	--------------------------输入密码弹窗-------------------------
	self._containerPassword = self:FindChildGO("Container_Password")

	self._btnBuy = self:FindChildGO("Container_Password/Button_Buy")
    self._csBH:AddClick(self._btnBuy, function() self:OnBuyClick() end)

    self._btnCancelPassword = self:FindChildGO("Container_Password/Button_Cancel")
    self._csBH:AddClick(self._btnCancelPassword, function() self:OnClosePassword() end)

    self._btnClosePassword = self:FindChildGO("Container_Password/Button_Close")
    self._csBH:AddClick(self._btnClosePassword, function() self:OnClosePassword() end)

    self._inputFieldMeshPw = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_Password/InputFieldMesh_Pw")
    --self._inputFieldMeshPw:SetOnEndMeshEditCB(function(text)  end)
	self._inputTextPw = self:GetChildComponent("Container_Password/InputFieldMesh_Pw", 'InputFieldMeshWrapper')
	

	self._scrollView:SetScrollRectState(false)
	self._scrollView1:SetScrollRectState(false)

end

function TradeBuyView:OnTypeListItemClicked(idx)
	if self._selectedTypeItem then
		self._selectedTypeItem:SetSelected(false)
	end
	self._selectedTypeItem = self._listType:GetItem(idx)
	self._selectedTypeItem:SetSelected(true)

	local subTypeData = self._listType:GetDataByIndex(idx)
	local auctionType
	for k,v in pairs(subTypeData) do
		auctionType = v.auctionType
	end
	
	TradeManager:RequestAuctionItemList(auctionType)
end

function TradeBuyView:OnBuyComplete()
	if self._selectedAuctionType then
		TradeManager:RequestAuctionItemList(self._selectedAuctionType)
	end
end

--更新子类数据
function TradeBuyView:UpdateSubType(auctionType)
	self._selectedAuctionType = auctionType
	local subTypeListData = {}
	local t = tradeData.allTradeItemList[auctionType]
	local cfgList = self._typeListData[auctionType]
	local gradeType
	local all = {}
	all.subtypeIcon = 5033
	all.subtypeName = 73408
	all.pageType = 1
	all.count = 0
	table.insert(subTypeListData,all)
	for k,tradeCfg in pairs(cfgList) do
		tradeCfg.pageType = 1
		tradeCfg.count = 0
		table.insert(subTypeListData,tradeCfg)
		gradeType = tradeCfg.gradeType
		for i=1,#t do
			local itemData = t[i]:GetItemData()
			if itemData:GetAuctionSubType() == tradeCfg.auctionSubtype then
				tradeCfg.count = tradeCfg.count + itemData:GetCount()
			end
		end
		all.count = all.count + tradeCfg.count
	end

	if gradeType == 1 then
		self._txtGradeOrLevel.text = LanguageDataHelper.CreateContent(53147)
	else
		self._txtGradeOrLevel.text = LanguageDataHelper.CreateContent(40007)
	end

	self._containerSelectBuy:SetActive(false)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_OPEN_TRADE_SELECT_SUBTYPE,subTypeListData,true)
end

--选择完子类
function TradeBuyView:UpdateBuyList(idx)
	self:ClearView()
	
	local t = tradeData.allTradeItemList[self._selectedAuctionType]
	if idx == 0 then
		self._listBaseData = t
	else
		local subType = self._typeListData[self._selectedAuctionType][idx].auctionSubtype
		self._listBaseData = {}
		for i=1,#t do
			if t[i]:GetItemData():GetAuctionSubType() == subType then
				table.insert(self._listBaseData,t[i])
			end
		end
	end
	self._listData = self._listBaseData
	self._listBuyItems:SetDataList(self._listData)
	self._selectedFiltGrade = #self._dGrade
	self._selectedFiltQuality = #self._dQuality
	self._selectedFiltPassword = #self._dPassword

	self._containerFilterGrade:Reset()
	self._containerFilterQuality:Reset()
	self._containerFilterPassword:Reset()

	if TradeDataHelper:GetGradeTypeByType(self._selectedAuctionType) == 1 then
		self._containerFilterGrade:SetActive(true)
	else
		self._containerFilterGrade:SetActive(false)
	end
end

function TradeBuyView:OnBuyListItemClicked(idx)
	self._selectedTradeData = self._listBuyItems:GetDataByIndex(idx)
	local itemData = self._selectedTradeData:GetItemData()
	
	local btnArgs = {}
	btnArgs.buy = true
	TipsManager:ShowItemTips(itemData,btnArgs,true)
end

--tips上点击购买处理
function TradeBuyView:BuyItem()
	if self._selectedTradeData then
		if self._selectedTradeData:GetPassword() == -1 then
			TradeManager:RequestAuctionBuy(self._selectedTradeData:GetDBId(),-1)
		else
			self._containerPassword:SetActive(true)
			self._inputTextPw.text = ""
		end
	end
end

function TradeBuyView:OnBuyClick()
	if self._selectedTradeData then
		local pw = self._inputTextPw.text
		if pw ~= "" then
			TradeManager:RequestAuctionBuy(self._selectedTradeData:GetDBId(),pw)
			self:OnClosePassword()
		end
	end
end

function TradeBuyView:OnClosePassword()
	self._containerPassword:SetActive(false)
end

--单价排序
function TradeBuyView:OnSinglePriceSort()
	self._singlePriceSortAscend = not self._singlePriceSortAscend
	self._imgSinglePriceSortAscend:SetActive(self._singlePriceSortAscend)
	self._imgSinglePriceSortDescend:SetActive(not self._singlePriceSortAscend)
	self:UpdateListSort(1)
end

--总价排序
function TradeBuyView:OnTotalPriceSort()
	self._totalPriceSortAscend = not self._totalPriceSortAscend
	self._imgTotalPriceSortAscend:SetActive(self._totalPriceSortAscend)
	self._imgTotalPriceSortDescend:SetActive(not self._totalPriceSortAscend)
	self:UpdateListSort(2)
end

function TradeBuyView:UpdateSearch()
	self:ClearView()
	self._listData  = tradeData.searchByNameList
	self._listBuyItems:SetDataList(self._listData)
end

local function SortByTotalPriceAscend(a,b)
    return a:GetPrice() < b:GetPrice()
end

local function SortByTotalPriceDescend(a,b)
    return a:GetPrice() > b:GetPrice()
end

local function SortBySinglePriceAscend(a,b)
    return a:GetPrice()/a:GetNum() < b:GetPrice()/b:GetNum()
end

local function SortBySinglePriceDescend(a,b)
    return a:GetPrice()/a:GetNum() > b:GetPrice()/b:GetNum()
end

--排序处理
function TradeBuyView:UpdateListSort(sortType)
	--单价排序
	if sortType == 1 then
		if self._singlePriceSortAscend then
			table.sort( self._listData, SortBySinglePriceAscend)
		else
			table.sort( self._listData, SortBySinglePriceDescend)
		end
	--总价排序
	else
		if self._totalPriceSortAscend then
			table.sort( self._listData, SortByTotalPriceAscend)
		else
			table.sort( self._listData, SortByTotalPriceDescend)
		end
	end
	self._listBuyItems:SetDataList(self._listData)
end

function TradeBuyView:UpdateFilter(idx,comboboxType)
	if comboboxType == 1 then
		self._selectedFiltGrade = idx + 1
	elseif comboboxType == 2 then
		self._selectedFiltQuality = idx + 1
	else
		self._selectedFiltPassword = idx + 1
	end
	local t = {}
	for i=1,#self._listBaseData do
		local inFilter = true
		local itemData = self._listBaseData[i]:GetItemData()
		if self._selectedFiltGrade ~= #self._dGrade then
			if itemData.cfgData.grade ~= self._selectedFiltGrade then
				inFilter = false
			end
		end

		if self._selectedFiltQuality ~= #self._dQuality then
			if itemData:GetQuality() ~= qualityMap[self._selectedFiltQuality] then
				inFilter = false
			end
		end

		if self._selectedFiltPassword == 1 and self._listBaseData[i]:GetPassword() == -1 then
			inFilter = false
		end

		if self._selectedFiltPassword == 2 and self._listBaseData[i]:GetPassword() ~= -1 then
			inFilter = false
		end

		if inFilter then
			table.insert(t,self._listBaseData[i])
		end
	end

	self._listData = t
	self:SetBuyList(self._listData)
end

function TradeBuyView:OnSearchClick()
	local str = self._inputTextSearch.text
	if str ~= "" then
		TradeManager:RequestAuctionSearch(str)
	elseif self._listData then
		self:SetBuyList(self._listData)
	end
end

function TradeBuyView:SetBuyList(listData)
	self._listBuyItems:SetDataList(listData)
	self._txtNoItemGO:SetActive(#listData == 0)
end

function TradeBuyView:ClearView()
	self._containerSelectBuy:SetActive(true)
	self._imgSinglePriceSortAscend:SetActive(false)
	self._imgSinglePriceSortDescend:SetActive(true)
	self._imgTotalPriceSortAscend:SetActive(false)
	self._imgTotalPriceSortDescend:SetActive(true)
	self._singlePriceSortAscend = false --单价排行升序
	self._totalPriceSortAscend = false --总价排行升序

	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_HIDE_TRADE_SELECT_SUBTYPE)

	self._txtNoItemGO:SetActive(false)
end

function TradeBuyView:ShowView()
	self._scrollView:SetScrollRectState(true)
	self._scrollView1:SetScrollRectState(true)
	self:ClearView()
	self._selectedAuctionType = nil
	self._listData = nil
	self._listBaseData = nil
	self._listBuyItems:SetDataList({})
	self:OnTypeListItemClicked(0)

	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRADE_CONFIRM_BUY,self._buyCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_ALL_SELL_DATA,self._updateSubTypeCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_TRADE_SEARCH_BY_NAME,self._updateSearchCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRADE_SELECT_SUBTYPE,self._updateBuyListCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_TRADE_BUY_COMPLETE,self._updateBuyCompleteCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRADE_UPDATE_FILTER,self._updateFitlerCb)
end

function TradeBuyView:CloseView()
	if self._selectedTypeItem then
		self._selectedTypeItem:SetSelected(false)
		self._selectedTypeItem = nil
	end
	self._scrollView:SetScrollRectState(false)
	self._scrollView1:SetScrollRectState(false)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRADE_CONFIRM_BUY,self._buyCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_ALL_SELL_DATA,self._updateSubTypeCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_TRADE_SEARCH_BY_NAME,self._updateSearchCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRADE_SELECT_SUBTYPE,self._updateBuyListCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_TRADE_BUY_COMPLETE,self._updateBuyCompleteCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRADE_UPDATE_FILTER,self._updateFitlerCb)
end