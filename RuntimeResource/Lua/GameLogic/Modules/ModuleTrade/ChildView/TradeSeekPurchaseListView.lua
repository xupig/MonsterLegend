-- TradeSeekPurchaseListView.lua
local TradeSeekPurchaseListView = Class.TradeSeekPurchaseListView(ClassTypes.BaseComponent)

TradeSeekPurchaseListView.interface = GameConfig.ComponentsConfig.Component
TradeSeekPurchaseListView.classPath = "Modules.ModuleTrade.ChildView.TradeSeekPurchaseListView"

require "Modules.ModuleTrade.ChildComponent.TradeTypeListItem"
require "Modules.ModuleTrade.ChildComponent.TradeSeekListItem"
require "Modules.ModuleTrade.ChildComponent.TradeFilterCombobox"

local TradeFilterCombobox = ClassTypes.TradeFilterCombobox
local TradeTypeListItem = ClassTypes.TradeTypeListItem
local TradeSeekListItem = ClassTypes.TradeSeekListItem

local UIList = ClassTypes.UIList
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local TradeDataHelper = GameDataHelper.TradeDataHelper
local TradeManager = PlayerManager.TradeManager
local tradeData = PlayerManager.PlayerDataManager.tradeData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

function TradeSeekPurchaseListView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function TradeSeekPurchaseListView:InitCallBack()
	self._updateMyListCb = function ()
		self:UpdateMyList()
	end

	self._updateListAllCb = function ()
		self:UpdateListAll()
	end

	self._updateListByTypeCb = function (idx)
		self:UpdateListByType(idx)
	end

	self._updateSubType = function (auctionType)
		self:UpdateSubType(auctionType)
	end

	self._updateSearchCb = function ()
		self:UpdateSearch()
	end

	self._sellCompleteCb = function ()
		self:SendListRequest()
	end

	self._updateFitlerCb = function (idx,comboboxType)
		self:UpdateFilter(idx,comboboxType)
	end
end

local qualityMap = {1,3,4,5,6,7}
local qualityCN = {137,139,140,141,53164,142}

function TradeSeekPurchaseListView:InitComps()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "List_ItemType")
    self._listType = list
    self._scrollView = scrollView
    self._listType:SetItemType(TradeTypeListItem)
    self._listType:SetPadding(0, 0, 0, 0)
    self._listType:SetGap(1, 0)
    self._listType:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local typeItemClickedCB =                
	function(idx)
		self:OnTypeListItemClicked(idx)
	end
	self._listType:SetItemClickedCB(typeItemClickedCB)

	local t = TradeDataHelper:GetTradeItemTypeList()
	self._typeListData = {}
	local d1 = {}
	d1.all = true
	local d2 = {}
	d2.mySeek = true
	self._typeListData[1] = d1
	self._typeListData[2] = d2
	for i=1,#t do
		table.insert(self._typeListData,t[i])
	end
	self._listType:SetDataList(self._typeListData)

	self._inputFieldMeshSearch = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputFieldMesh_Search")
    --self._inputFieldMeshSearch:SetOnEndMeshEditCB(function(text) self:OnEndEditPw(text) end)
    self._inputTextSearch = self:GetChildComponent("InputFieldMesh_Search", 'InputFieldMeshWrapper')

    self._btnSearch = self:FindChildGO("Button_Search")
    self._csBH:AddClick(self._btnSearch, function() self:OnSearchClick() end)

    ------------------------------求购列表--------------------------------------
	self._containerSeekPurchase = self:FindChildGO("Container_SeekPurchase")

    self._txtGradeOrLevel = self:GetChildComponent("Container_SeekPurchase/Container_Title/Text_GradeOrLevel", 'TextMeshWrapper')

    
    self._btnTotalPriceSort = self:FindChildGO("Container_SeekPurchase/Container_Title/Button_TotalPriceSort")
    self._csBH:AddClick(self._btnTotalPriceSort, function() self:OnTotalPriceSort() end)
    self._imgTotalPriceSortAscend = self:FindChildGO("Container_SeekPurchase/Container_Title/Button_TotalPriceSort/Image_Ascend")
    self._imgTotalPriceSortDescend = self:FindChildGO("Container_SeekPurchase/Container_Title/Button_TotalPriceSort/Image_Descend")

    local scrollView1, list1 = UIList.AddScrollViewList(self.gameObject, "Container_SeekPurchase/List_SeekPurchase")
    self._listSeekItems = list1
    self._scrollView1 = scrollView1
    self._listSeekItems:SetItemType(TradeSeekListItem)
    self._listSeekItems:SetPadding(0, 0, 0, 0)
    self._listSeekItems:SetGap(1, 0)
    self._listSeekItems:SetDirection(UIList.DirectionTopToDown, 1, 1)
 --    local seekItemClickedCB =                
	-- function(idx)
	-- 	self:OnBuyListItemClicked(idx)
	-- end
	-- self._listSeekItems:SetItemClickedCB(seekItemClickedCB)

	self._txtNoItemGO = self:FindChildGO("Container_SeekPurchase/Text_NoItem")

	-------------------------------筛选combobox---------------------------------------
	self._containerFilterGrade = self:AddChildLuaUIComponent("Container_SeekPurchase/Container_FilterComboboxes/Container_Grade", TradeFilterCombobox)
	self._dGrade = {}
	for i=1,#StringStyleUtil.cnNumber do
		self._dGrade[i] = LanguageDataHelper.CreateContent(StringStyleUtil.cnNumber[i])..LanguageDataHelper.CreateContent(4543)
	end
	self._dGrade[#StringStyleUtil.cnNumber+1] = LanguageDataHelper.CreateContent(73516)
	self._containerFilterGrade:UpdateData(self._dGrade,1)


	self._containerFilterQuality = self:AddChildLuaUIComponent("Container_SeekPurchase/Container_FilterComboboxes/Container_Quality", TradeFilterCombobox)
	self._dQuality = {}
	for i=1,#qualityCN do
		self._dQuality[i] = LanguageDataHelper.CreateContent(qualityCN[i])
	end
	self._dQuality[#qualityCN+1] = LanguageDataHelper.CreateContent(73517)
	self._containerFilterQuality:UpdateData(self._dQuality,2)

	self._scrollView:SetScrollRectState(false)
	self._scrollView1:SetScrollRectState(false)
end

function TradeSeekPurchaseListView:OnTypeListItemClicked(idx)
	if self._selectedTypeItem then
		self._selectedTypeItem:SetSelected(false)
	end
	self._selectedTypeItem = self._listType:GetItem(idx)
	self._selectedTypeItem:SetSelected(true)

	self._selectedSubTypeData = self._listType:GetDataByIndex(idx)
	self:SendListRequest()
end

function TradeSeekPurchaseListView:SendListRequest()
	if self._selectedSubTypeData == nil then
		return
	end
	if self._selectedSubTypeData.all then
		TradeManager:RequestBegItemListAll()
	elseif self._selectedSubTypeData.mySeek then
		TradeManager:RequestBegMyList()
	else
		local auctionType
		for k,v in pairs(self._selectedSubTypeData) do
			auctionType = v.auctionType
		end
		TradeManager:RequestBegItemList(auctionType)
	end
end

--更新子类数据
function TradeSeekPurchaseListView:UpdateSubType(auctionType)
	self._selectedAuctionType = auctionType
	local subTypeListData = {}
	local t = tradeData.begItemListByType[auctionType]
	local cfgList = self._typeListData[auctionType+2]
	local gradeType
	local all = {}
	all.subtypeIcon = 5033
	all.subtypeName = 73408
	all.pageType = 1
	all.count = 0
	table.insert(subTypeListData,all)
	for k,tradeCfg in pairs(cfgList) do
		tradeCfg.pageType = 1
		tradeCfg.count = 0
		table.insert(subTypeListData,tradeCfg)
		gradeType = tradeCfg.gradeType
		for i=1,#t do
			local itemData = t[i]:GetItemData()
			if itemData:GetAuctionSubType() == tradeCfg.auctionSubtype then
				tradeCfg.count = tradeCfg.count + itemData:GetCount()
			end
		end
		all.count = all.count + tradeCfg.count
	end

	if gradeType == 1 then
		self._txtGradeOrLevel.text = LanguageDataHelper.CreateContent(53147)
	else
		self._txtGradeOrLevel.text = LanguageDataHelper.CreateContent(40007)
	end

	self._containerSeekPurchase:SetActive(false)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_OPEN_TRADE_SELECT_SUBTYPE,subTypeListData,true)
end

--选择完子类
function TradeSeekPurchaseListView:UpdateListByType(idx)
	self:ClearView()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_HIDE_TRADE_SELECT_SUBTYPE)
	local t = tradeData.begItemListByType[self._selectedAuctionType]
	if idx == 0 then
		self._listBaseData = t
	else
		local subType = self._typeListData[self._selectedAuctionType+2][idx].auctionSubtype
		self._listBaseData = {}
		for i=1,#t do
			if t[i]:GetItemData():GetAuctionSubType() == subType then
				table.insert(self._listBaseData,t[i])
			end
		end
	end
	self._listData = self._listBaseData
	self._listSeekItems:SetDataList(self._listData)

	self._selectedFiltGrade = #self._dGrade
	self._selectedFiltQuality = #self._dQuality

	self._containerFilterGrade:Reset()
	self._containerFilterQuality:Reset()

	if TradeDataHelper:GetGradeTypeByType(self._selectedAuctionType) == 1 then
		self._containerFilterGrade:SetActive(true)
	else
		self._containerFilterGrade:SetActive(false)
	end
end

function TradeSeekPurchaseListView:UpdateListAll()
	self:ClearView()
	self._listBaseData  = tradeData.allBegItemList
	self._listData = self._listBaseData
	self._listSeekItems:SetDataList(self._listData)
	
	self._selectedFiltGrade = #self._dGrade
	self._selectedFiltQuality = #self._dQuality
end

function TradeSeekPurchaseListView:UpdateMyList()
	self:ClearView()
	self._listBaseData  = tradeData.myBegItemList
	self._listData = self._listBaseData
	self._listSeekItems:SetDataList(self._listData)

	self._selectedFiltGrade = #self._dGrade
	self._selectedFiltQuality = #self._dQuality
end

--总价排序
function TradeSeekPurchaseListView:OnTotalPriceSort()
	self._totalPriceSortAscend = not self._totalPriceSortAscend
	self._imgTotalPriceSortAscend:SetActive(self._totalPriceSortAscend)
	self._imgTotalPriceSortDescend:SetActive(not self._totalPriceSortAscend)
	self:UpdateListSort()
end

local function SortByTotalPriceAscend(a,b)
    return tonumber(a:GetPrice()) < tonumber(b:GetPrice())
end

local function SortByTotalPriceDescend(a,b)
    return tonumber(a:GetPrice()) > tonumber(b:GetPrice())
end

function TradeSeekPurchaseListView:UpdateListSort()
	if self._listData == nil or #self._listData == 0 then
		return
	end
	if self._totalPriceSortAscend then
		table.sort( self._listData, SortByTotalPriceAscend)
	else
		table.sort( self._listData, SortByTotalPriceDescend)
	end
	self._listSeekItems:SetDataList(self._listData)
end

function TradeSeekPurchaseListView:UpdateFilter(idx,comboboxType)
	if comboboxType == 1 then
		self._selectedFiltGrade = idx + 1
	elseif comboboxType == 2 then
		self._selectedFiltQuality = idx + 1
	end

	local t = {}
	for i=1,#self._listBaseData do
		local inFilter = true
		local itemData = self._listBaseData[i]:GetItemData()
		if self._selectedFiltGrade ~= #self._dGrade then
			if itemData.cfgData.grade ~= self._selectedFiltGrade then
				LoggerHelper.Error("1")
				inFilter = false
			end
		end

		if self._selectedFiltQuality ~= #self._dQuality then
			if itemData:GetQuality() ~= qualityMap[self._selectedFiltQuality] then
				LoggerHelper.Error("2")
				inFilter = false
			end
		end

		if inFilter then
			table.insert(t,self._listBaseData[i])
		end
	end

	self._listData = t
	self:SetSeekList(self._listData)
end

function TradeSeekPurchaseListView:UpdateSearch()
	self:ClearView()
	self._listData  = tradeData.searchByNameBegList
	self._listSeekItems:SetDataList(self._listData)
end

function TradeSeekPurchaseListView:OnSearchClick()
	local str = self._inputTextSearch.text
	if str ~= "" then
		TradeManager:RequestBegSearch(str)
	else
		self:SetSeekList(self._listBaseData)
	end
end

function TradeSeekPurchaseListView:SetSeekList(listData)
	self._listSeekItems:SetDataList(listData)
	self._txtNoItemGO:SetActive(#listData == 0)
end

function TradeSeekPurchaseListView:ClearView()
	self._containerSeekPurchase:SetActive(true)
	self._imgTotalPriceSortAscend:SetActive(false)
	self._imgTotalPriceSortDescend:SetActive(true)
	self._totalPriceSortAscend = false --总价排行升序
	self._txtNoItemGO:SetActive(false)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_HIDE_TRADE_SELECT_SUBTYPE)
end

function TradeSeekPurchaseListView:ShowView()
	self._scrollView:SetScrollRectState(true)
	self._scrollView1:SetScrollRectState(true)
	self:ClearView()
	self._selectedAuctionType = nil
	self._selectedSubTypeData = nil
	self._listData = nil
	self._listBaseData = nil
	self._listSeekItems:SetDataList({})
	self:OnTypeListItemClicked(0)

	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRADE_SELECT_SUBTYPE,self._updateListByTypeCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_MY_BEG_DATA,self._updateMyListCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_ALL_BEG_DATA,self._updateListAllCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_BEG_SEARCH_BY_NAME,self._updateSearchCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_BEG_SELL_COMPLETE,self._sellCompleteCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRADE_UPDATE_FILTER,self._updateFitlerCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_BEG_TYPE_DATA,self._updateSubType)
end

function TradeSeekPurchaseListView:CloseView()
	if self._selectedTypeItem then
		self._selectedTypeItem:SetSelected(false)
		self._selectedTypeItem = nil
	end
	self._scrollView:SetScrollRectState(false)
	self._scrollView1:SetScrollRectState(false)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRADE_SELECT_SUBTYPE,self._updateListByTypeCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_MY_BEG_DATA,self._updateMyListCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_ALL_BEG_DATA,self._updateListAllCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_BEG_SEARCH_BY_NAME,self._updateSearchCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_BEG_SELL_COMPLETE,self._sellCompleteCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRADE_UPDATE_FILTER,self._updateFitlerCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_BEG_TYPE_DATA,self._updateSubType)
end