-- TradeTradeListView.lua
local TradeTradeListView = Class.TradeTradeListView(ClassTypes.BaseComponent)

TradeTradeListView.interface = GameConfig.ComponentsConfig.Component
TradeTradeListView.classPath = "Modules.ModuleTrade.ChildView.TradeTradeListView"

require "Modules.ModuleTrade.ChildComponent.TradeRecordListItem"
local TradeRecordListItem = ClassTypes.TradeRecordListItem
local UIList = ClassTypes.UIList
local x = 1

function TradeTradeListView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function TradeTradeListView:InitCallBack()
end

function TradeTradeListView:InitComps()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "List_Trade")
    self._listRecord = list
    self._scrollView = scrollView
    self._listRecord:SetItemType(TradeRecordListItem)
    self._listRecord:SetPadding(0, 0, 0, 0)
    self._listRecord:SetGap(1, 5)
    self._listRecord:SetDirection(UIList.DirectionTopToDown, 1, 10)
    local itemClickedCB =            
	function(idx)
		self:OnTradeListItemClicked(idx)
	end
	self._listRecord:SetItemClickedCB(itemClickedCB)
end

function TradeTradeListView:OnTradeListItemClicked(idx)
	
end

function TradeTradeListView:UpdateView()
	local listData = GameWorld.Player().auction_logs or {}
	self._listRecord:SetDataList(listData)
end


function TradeTradeListView:ShowView()
	self:UpdateView()
end

function TradeTradeListView:CloseView()
end