-- TradeSelectSubTypeView.lua
local TradeSelectSubTypeView = Class.TradeSelectSubTypeView(ClassTypes.BaseComponent)

TradeSelectSubTypeView.interface = GameConfig.ComponentsConfig.Component
TradeSelectSubTypeView.classPath = "Modules.ModuleTrade.ChildView.TradeSelectSubTypeView"

local UIList = ClassTypes.UIList
require "Modules.ModuleTrade.ChildComponent.TradeSubTypeListItem"

local TradeSubTypeListItem = ClassTypes.TradeSubTypeListItem

function TradeSelectSubTypeView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self:InitCallBack()
	self:InitComps()
end

function TradeSelectSubTypeView:InitCallBack()
end

function TradeSelectSubTypeView:InitComps()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "List_SubType")
    self._listSubType = list
    self._scrollViewGemBag = scrollView
    self._listSubType:SetItemType(TradeSubTypeListItem)
    self._listSubType:SetPadding(10, 0, 0, 0)
    self._listSubType:SetGap(1, 0)
    self._listSubType:SetDirection(UIList.DirectionLeftToRight, 3, 10)
    local itemClickedCB =                --设置按钮点击的回调
	function(idx)
		self:OnTypeListItemClicked(idx)
	end
	self._listSubType:SetItemClickedCB(itemClickedCB)

	self._containerTips = self:FindChildGO("Container_Tips")
	self:OnEnableScr(false)

end

function TradeSelectSubTypeView:OnTypeListItemClicked(idx)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRADE_SELECT_SUBTYPE,idx)
end

function TradeSelectSubTypeView:UpdateView(listData,showTips)
	self:OnEnableScr(true)
	self._listSubType:SetDataList(listData)
	self._containerTips:SetActive(showTips)
end

function TradeSelectSubTypeView:OnEnableScr(state)
	if self._scrollViewGemBag then
		self._scrollViewGemBag:SetScrollRectState(state)
	end
end
