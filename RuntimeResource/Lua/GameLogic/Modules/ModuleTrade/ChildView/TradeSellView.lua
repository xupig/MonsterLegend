-- TradeSellView.lua
local TradeSellView = Class.TradeSellView(ClassTypes.BaseComponent)

TradeSellView.interface = GameConfig.ComponentsConfig.Component
TradeSellView.classPath = "Modules.ModuleTrade.ChildView.TradeSellView"

require "Modules.ModuleTrade.ChildComponent.TradeSellBagListItem"
require "Modules.ModuleTrade.ChildComponent.TradeSellListItem"
local TradeSellBagListItem = ClassTypes.TradeSellBagListItem
local TradeSellListItem = ClassTypes.TradeSellListItem

local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local TradeManager = PlayerManager.TradeManager
local tradeData = PlayerManager.PlayerDataManager.tradeData
local TipsManager = GameManager.TipsManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function TradeSellView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function TradeSellView:InitCallBack()

	self._offSellItemCb = function ()
		self:OffSellItem()
	end

	self._updateViewCb = function ()
		self:UpdateView()
	end
end

function TradeSellView:InitComps()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "List_Bag")
    self._listBag = list
    self._scrollViewBag = scrollView
    self._listBag:SetItemType(TradeSellBagListItem)
    self._listBag:SetPadding(0, 0, 0, 0)
    self._listBag:SetGap(7, 7)
    self._listBag:SetDirection(UIList.DirectionLeftToRight, 5, 100)
    local bagItemClickedCB =                --设置按钮点击的回调
	function(idx)
		self:OnBagListItemClicked(idx)
	end
	self._listBag:SetItemClickedCB(bagItemClickedCB)

	local scrollView1, list1 = UIList.AddScrollViewList(self.gameObject, "List_ForSale")
    self._listForSale = list1
    self._scrollViewForSale = scrollView1
    self._listForSale:SetItemType(TradeSellListItem)
    self._listForSale:SetPadding(0, 0, 0, 0)
    self._listForSale:SetGap(1, 5)
    self._listForSale:SetDirection(UIList.DirectionLeftToRight, 2, 10)
    local saleItemClickedCB =                --设置按钮点击的回调
	function(idx)
		self:OnForSaleListItemClicked(idx)
	end
	self._listForSale:SetItemClickedCB(saleItemClickedCB)

	self._txtForSaleNum = self:GetChildComponent("Text_ForSaleNum",'TextMeshWrapper')
	self._sellSlot = GlobalParamsHelper.GetParamValue(628)
	self._scrollViewBag:SetScrollRectState(false)
	self._scrollViewForSale:SetScrollRectState(false)
end

function TradeSellView:OnForSaleListItemClicked(idx)
	self._selectedSellData = self._myTradeItemList[idx+1]

	local btnArgs = {}
	btnArgs.offSell = true
	TipsManager:ShowItemTips(self._selectedSellData:GetItemData(),btnArgs,true)
end

function TradeSellView:OffSellItem()
	if self._selectedSellData then
		TradeManager:RequestAuctionOff(self._selectedSellData:GetDBId())
	end
end

function TradeSellView:OnBagListItemClicked(idx)
	local data = self._bagData[idx+1]
	if data == "nil" then
		return
	end

	local btnArgs = {}
	btnArgs.forSale = true
	TipsManager:ShowItemTips(data,btnArgs,true)
end

function TradeSellView:UpdateBagList()
	--整个背包的数据
	local t = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
	local curVolumn = GameWorld.Player().pkg_items_size
	self._bagData = {}
	for i=1,curVolumn do
		local itemData = t[i]
	 	if itemData then
	 		local bind = itemData:BindFlag() or 0
	 		if itemData.cfgData.auctionType and itemData.cfgData.auctionType > 0 and bind == 0 then
	 			table.insert(self._bagData,itemData)
	 		end
	 	end
	end

	local t1 = bagData:GetPkg(public_config.PKG_TYPE_CARD_PIECE):GetItemInfos()
	for i=1,#t1 do
		local itemData = t1[i]
	 	if itemData then
	 		local bind = itemData:BindFlag() or 0
	 		if itemData.cfgData.auctionType and itemData.cfgData.auctionType > 0  and bind == 0 then
	 			table.insert(self._bagData,itemData)
	 		end
	 	end
	end

	local count = #self._bagData
	self._maxVolumn = (math.ceil(count/5))*5
	for i=count + 1,self._maxVolumn do
		table.insert(self._bagData,"nil")
	end

	self._listBag:SetOnAllItemCreatedCB(function() self:UpdatePowerCompare() end)
	self._listBag:SetDataList(self._bagData, 5)--使用协程加载
	
	-- self._listBag:SetDataList(self._bagData)
	-- self:UpdatePowerCompare()
end

function TradeSellView:UpdatePowerCompare()
	--检查装备是否能装备/战力是否更强
	for i=1,self._maxVolumn do
		local itemData = self._bagData[i]
		if itemData ~= "nil" and itemData ~= "lock" and itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP then
			if itemData:CheckVocationMatchGeneral() then
				if itemData:CheckBetterThanLoaded() then
					self._listBag:GetItem(i-1):SetUseAndPowerCompare(true,false,true)
				else
					self._listBag:GetItem(i-1):SetUseAndPowerCompare(true,false,false)
				end
			else
				self._listBag:GetItem(i-1):SetUseAndPowerCompare(true,true)
			end
		else
			self._listBag:GetItem(i-1):SetUseAndPowerCompare(false)
		end
	end
end

function TradeSellView:UpdateView()
	self:UpdateSellItems()
	self:UpdateBagList()
end

function TradeSellView:UpdateSellItems()
	self._myTradeItemList = tradeData.myTradeItemList
	self._listForSale:SetDataList(self._myTradeItemList)
	self._txtForSaleNum.text = tostring(#tradeData.myTradeItemList).."/"..self._sellSlot
end

function TradeSellView:ShowView()
	self._scrollViewBag:SetScrollRectState(true)
	self._scrollViewForSale:SetScrollRectState(true)
	TradeManager:RequestAuctionItemMyList()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRADE_OFF_SELL_ITEM,self._offSellItemCb)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_MY_SELL_DATA,self._updateViewCb)
end

function TradeSellView:CloseView()
	self._scrollViewBag:SetScrollRectState(false)
	self._scrollViewForSale:SetScrollRectState(false)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRADE_OFF_SELL_ITEM,self._offSellItemCb)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_MY_SELL_DATA,self._updateViewCb)
end