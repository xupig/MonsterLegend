local TradePanel = Class.TradePanel(ClassTypes.BasePanel)

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager

require "Modules.ModuleTrade.ChildView.TradeBuyView"
require "Modules.ModuleTrade.ChildView.TradeSellView"
require "Modules.ModuleTrade.ChildView.TradeSeekPurchaseListView"
require "Modules.ModuleTrade.ChildView.TradeMySeekPurchaseView"
require "Modules.ModuleTrade.ChildView.TradeTradeListView"
require "Modules.ModuleTrade.ChildView.TradeSelectSubTypeView"

local TradeBuyView = ClassTypes.TradeBuyView
local TradeSellView = ClassTypes.TradeSellView
local TradeSeekPurchaseListView = ClassTypes.TradeSeekPurchaseListView
local TradeMySeekPurchaseView = ClassTypes.TradeMySeekPurchaseView
local TradeTradeListView = ClassTypes.TradeTradeListView
local TradeSelectSubTypeView = ClassTypes.TradeSelectSubTypeView
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local StringStyleUtil = GameUtil.StringStyleUtil
local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

function TradePanel:Awake()
	self:InitCallBack()
	self:InitComps()
end

function TradePanel:InitCallBack()
	self._tabClickCB = function (index)
		self:OnTabClick(index)
	end

	self._onDiamondChange = function() self:UpdateDiamond() end
    self._onBindDiamondChange = function() self:UpdateBindDiamond() end

    self._openSelectSubTypeCb = function (listData,showTips)
    	self:ShowSelectSubTypeView(listData,showTips)
    end

    self._hideSelectSubTypeCb = function ()
    	self:HideSelectTypeView()
    end
end

--override 
function TradePanel:OnShow(data)
	self:UpdateFunctionOpen()
	self:OnShowSelectTab(data)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS,self._onDiamondChange)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS_BIND,self._onBindDiamondChange)
    self:UpdateDiamond()
    self:UpdateBindDiamond()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_OPEN_TRADE_SELECT_SUBTYPE,self._openSelectSubTypeCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_HIDE_TRADE_SELECT_SUBTYPE,self._hideSelectSubTypeCb)
end

--override
function TradePanel:OnClose()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS,self._onDiamondChange)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS_BIND,self._onBindDiamondChange)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_OPEN_TRADE_SELECT_SUBTYPE,self._openSelectSubTypeCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_HIDE_TRADE_SELECT_SUBTYPE,self._hideSelectSubTypeCb)
end

function TradePanel:InitComps()
	self:InitView("Container_Buy", TradeBuyView,1,1)
	self:InitView("Container_Sell", TradeSellView,1,2)
	self:InitView("Container_SeekPurchaseList", TradeSeekPurchaseListView,1,3)
	self:InitView("Container_MySeekPurchase", TradeMySeekPurchaseView,1,4)
	self:InitView("Container_TradeList", TradeTradeListView,1,5)

    self:SetSecondTabClickCallBack(self._tabClickCB)

    self._txtDiamond = self:GetChildComponent("Container_Currency/Text_Diamond",'TextMeshWrapper')
    self._txtBindDiamond = self:GetChildComponent("Container_Currency/Text_BindDiamond",'TextMeshWrapper')

    local iconDiamond = self:FindChildGO("Container_Currency/Container_Diamond")
    local iconBindDiamond = self:FindChildGO("Container_Currency/Container_BindDiamond")

    GameWorld.AddIcon(iconDiamond, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    GameWorld.AddIcon(iconBindDiamond, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS_BIND))

    self._selectedSubTypeView = self:AddChildLuaUIComponent("Container_SelectSubType", TradeSelectSubTypeView)
end

-- function TradePanel:OnDestroy()
--     self._selectedSubTypeView = nil
-- end

--tab点击处理
function TradePanel:OnTabClick(index)
	self:HideSelectTypeView()
end

function TradePanel:ShowSelectSubTypeView(listData,showTips)
    -- self._selectedSubTypeView:SetActive(true)
    if not self._selectedSubTypeView.activeSelf then
        self._selectedSubTypeView:SetActive(true)
    end
    self._selectedSubTypeView.transform.localScale = ShowScale
	self._selectedSubTypeView:UpdateView(listData,showTips)
end

function TradePanel:HideSelectTypeView()
    -- self._selectedSubTypeView:SetActive(false)
    self._selectedSubTypeView.transform.localScale = HideScale
    self._selectedSubTypeView:OnEnableScr(false)
end

function TradePanel:UpdateDiamond()
    local money = GameWorld.Player().money_coupons
    self._txtDiamond.text = StringStyleUtil.GetLongNumberString(money)
end

function TradePanel:UpdateBindDiamond()
    local money = GameWorld.Player().money_coupons_bind
    self._txtBindDiamond.text = StringStyleUtil.GetLongNumberString(money)
end

-- -- 0 没有  1 普通底板
function TradePanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function TradePanel:StructingViewInit()
     return true
end

-- function TradePanel:ScaleToHide()
--     return true
-- end

