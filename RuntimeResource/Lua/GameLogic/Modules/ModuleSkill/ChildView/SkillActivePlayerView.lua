require "Modules.ModuleSkill.ChildComponent.SkillActivePlayerItem"
require "Modules.ModuleSkill.ChildView.SkillActiveConfigView"

local SkillActivePlayerView = Class.SkillActivePlayerView(ClassTypes.BaseLuaUIComponent)

SkillActivePlayerView.interface = GameConfig.ComponentsConfig.Component
SkillActivePlayerView.classPath = "Modules.ModuleSkill.ChildView.SkillActivePlayerView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local LuaUIRawImage = GameMain.LuaUIRawImage
local UIToggle = ClassTypes.UIToggle
local SkillActivePlayerItem = ClassTypes.SkillActivePlayerItem
local SkillDataHelper = GameDataHelper.SkillDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local SkillActiveConfigView = ClassTypes.SkillActiveConfigView
local SkillManager = PlayerManager.SkillManager

local Max_Skill_Count = 8

function SkillActivePlayerView:Awake()
    self._preSelected = -1
    self._selectedId = -1
    self:InitView()
end
--override
function SkillActivePlayerView:OnDestroy() 

end

function SkillActivePlayerView:OnEnable()
    self:AddEventListeners()
    self:RefreshDataList()
end

function SkillActivePlayerView:OnDisable()
    self:RemoveEventListeners()
end

--data结构
function SkillActivePlayerView:ShowView(data)
    self:RefreshView()
end

function SkillActivePlayerView:CloseView()

end

function SkillActivePlayerView:AddEventListeners()

end

function SkillActivePlayerView:RemoveEventListeners()

end

function SkillActivePlayerView:InitView()
    self._configButton = self:FindChildGO("Button_Config")
    self._csBH:AddClick(self._configButton,function () self:OnConfigButtonClick() end)
    self._iconContainer = self:FindChildGO("Image_Vocation/Container_Icon")

    self._skillIcon = self:FindChildGO("Container_SkillInfo/Image_Skill/Container_Icon")
    self._cdText = self:GetChildComponent("Container_SkillInfo/Text_CD/Text_Value", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Container_SkillInfo/Text_Desc", "TextMeshWrapper")
    self._nameText = self:GetChildComponent("Container_SkillInfo/Text_Name", "TextMeshWrapper")

    self._unlockDescText = self:GetChildComponent("Container_SkillInfo/Text_UnlockDesc", "TextMeshWrapper")

    self._autoToggleGo = self:FindChildGO("Container_SkillInfo/Toggle_Auto")
    self._autoToggle = UIToggle.AddToggle(self.gameObject, "Container_SkillInfo/Toggle_Auto")
    self._autoToggle:SetIsOn(false)
    self._autoToggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

    self._rawImageObj = self:FindChildGO("Container_SkillInfo/Container_Model/RawImage")
    self.rawImage = self._rawImageObj:AddComponent(typeof(LuaUIRawImage))
    self.rawImage.texture = GameWorld.ShowPlayer().RenderTexture
    
    self._skillItems = {}
    for i=1, Max_Skill_Count do
        self._skillItems[i] = self:AddChildLuaUIComponent("Container_Skills/Container_Skill"..i, SkillActivePlayerItem)
    end

    self._configGo = self:FindChildGO("Container_SkillConfig")
    self._configView = self:AddChildLuaUIComponent("Container_SkillConfig", SkillActiveConfigView)
    self._configGo:SetActive(false)
end

function SkillActivePlayerView:OnToggleValueChanged(state)
    local auto = SkillManager:GetSkillsAutoCastState(self._selectedId)
    if state == auto then
        return
    end
    SkillManager:SkillChangeAutoCastReq(self._selectedId)
end

function SkillActivePlayerView:OnConfigButtonClick()
    self._configGo:SetActive(true)
    self._configView:ShowView()
end

function SkillActivePlayerView:RefreshDataList()
    local data = SkillDataHelper.GetAllSkillActiveData()
    for i=1, Max_Skill_Count do
        self._skillItems[i]:SetData(data[i])
    end
end

function SkillActivePlayerView:RefreshView()
    local data = SkillDataHelper.GetAllSkillActiveData()
    for i=1, Max_Skill_Count do
        self._skillItems[i]:SetData(data[i])
    end
    if self._preSelected ~= -1 then
        self:SelectActiveSkillPlayerItem(self._preSelected)
    else
        self:SelectActiveSkillPlayerItem(1)
    end
    GameWorld.AddIcon(self._iconContainer, RoleDataHelper.GetRoleBigIconByVocation(GameWorld.Player().vocation))
end

function SkillActivePlayerView:SelectActiveSkillPlayerItem(idx)
    if self._preSelected ~= -1 then
        self._skillItems[self._preSelected]:ShowSelected(false)
    end
    self._skillItems[idx]:ShowSelected(true)
    self._preSelected = idx
    self:RefreshSelectedInfo(idx)
end

function SkillActivePlayerView:RefreshSelectedInfo(idx)
    local data = SkillDataHelper.GetAllSkillActiveData()
    local id = data[idx][1]
    self._selectedId = id
    self.isLock = not SkillManager:IsActiveSkillGot(self._selectedId)
    local skillId = SkillDataHelper.GetActiveSkillId(id, GameWorld.Player().vocation)[1]
    local iconId = SkillDataHelper.GetSkillIcon(skillId)
    GameWorld.AddIcon(self._skillIcon, iconId)
    self._cdText.text = SkillDataHelper.GetSkillSelfCD(skillId)*0.001 .. LanguageDataHelper.GetContent(150)
    self._descText.text = SkillDataHelper.GetDesc(skillId)
    self._nameText.text = SkillDataHelper.GetName(skillId)
    local previewSkill = SkillDataHelper.GetPreviewSkill(skillId)
    GameWorld.ShowPlayer():PlaySkill(previewSkill)
    self:RefreshAutoState()
end

function SkillActivePlayerView:SelectActiveSkillConfigItem(idx)
    self._configView:OnSelectedConfigItem(idx)
end

function SkillActivePlayerView:RefreshActiveSkillConfigView()
    self._configView:RefreshView()
    self:RefreshView()
end

function SkillActivePlayerView:RefreshAutoState()
    if self._preSelected == 1 then
        self._autoToggleGo:SetActive(false)
        self._unlockDescText.text = ""
    else
        if self.isLock then
            local desc = SkillDataHelper.GetActiveSkillDesc(self._selectedId)
            self._unlockDescText.text = desc
            self._autoToggleGo:SetActive(false)
        else
            self._autoToggleGo:SetActive(true)
            self._unlockDescText.text = ""
            local auto = SkillManager:GetSkillsAutoCastState(self._selectedId)
            self._autoToggle:SetIsOn(auto)
        end
    end
end