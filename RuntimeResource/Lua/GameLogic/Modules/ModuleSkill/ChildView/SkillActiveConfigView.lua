require "Modules.ModuleSkill.ChildComponent.SkillActiveConfigItem"

local SkillActiveConfigView = Class.SkillActiveConfigView(ClassTypes.BaseLuaUIComponent)

SkillActiveConfigView.interface = GameConfig.ComponentsConfig.Component
SkillActiveConfigView.classPath = "Modules.ModuleSkill.ChildView.SkillActiveConfigView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local SkillActiveConfigItem = ClassTypes.SkillActiveConfigItem
local SkillDataHelper = GameDataHelper.SkillDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local SkillManager = PlayerManager.SkillManager

local Max_Skill_Count = 7
local setV2 = Vector2.New(0,0)
local Unit_Width = 110

function SkillActiveConfigView:Awake()
    self._preSelected = -1
    self:InitView()
end
--override
function SkillActiveConfigView:OnDestroy() 
end

function SkillActiveConfigView:OnEnable()
    self:AddEventListeners()
end

function SkillActiveConfigView:OnDisable()
    self:RemoveEventListeners()
end

--data结构
function SkillActiveConfigView:ShowView(data)
    self:RefreshView()
end

function SkillActiveConfigView:CloseView()

end

function SkillActiveConfigView:AddEventListeners()

end

function SkillActiveConfigView:RemoveEventListeners()

end

function SkillActiveConfigView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function () self:OnCloseButtonClick() end)

    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._infoText.text = LanguageDataHelper.CreateContent(58777)

    self._lineRect = self:GetChildComponent("Image_Line/Image_Line1", 'RectTransform')
    self._lineImage = self:GetChildComponent("Image_Line/Image_Line1", 'ImageWrapper')
    self._lineImage:SetContinuousDimensionDirty(true)
    setV2.y = self._lineRect.sizeDelta.y

    self._skillItems = {}
    for i=1,Max_Skill_Count do
        self._skillItems[i] = self:AddChildLuaUIComponent("Container_Skills/Container_Skill"..i, SkillActiveConfigItem)
        self._skillItems[i]:SetIndex(i)
    end
end

function SkillActiveConfigView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function SkillActiveConfigView:RefreshView()
    self._configData = SkillManager:GetSlotConfigData()
    local count = 0
    for i=1,Max_Skill_Count do
        self._skillItems[i]:SetData(self._configData[i+1])
        if self._configData[i+1] > 0 then
            count = count + 1
        end
    end
    count = math.max(count-1, 0)
    setV2.x = Unit_Width * count
    self._lineRect.sizeDelta = setV2
end

function SkillActiveConfigView:OnSelectedConfigItem(idx)
    if self._preSelected ~= -1 then
        if self._preSelected ~= idx then
            SkillManager:SkillChangeSlotReq(self._configData[self._preSelected+1] , idx+1)
        end
        self._skillItems[self._preSelected]:ShowSelected(false)
        self._preSelected = -1
    else
        self._preSelected = idx
        self._skillItems[self._preSelected]:ShowSelected(true)
    end
end