require "Modules.ModuleSkill.ChildComponent.SkillPassiveItem"

local SkillPassiveTabView = Class.SkillPassiveTabView(ClassTypes.BaseLuaUIComponent)

SkillPassiveTabView.interface = GameConfig.ComponentsConfig.Component
SkillPassiveTabView.classPath = "Modules.ModuleSkill.ChildView.SkillPassiveTabView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local SkillPassiveItem = ClassTypes.SkillPassiveItem
local SkillDataHelper = GameDataHelper.SkillDataHelper
local UIList = ClassTypes.UIList
local SkillManager = PlayerManager.SkillManager

function SkillPassiveTabView:Awake()
    self._preSelected = -1
    self._selectedId = -1
    self:InitView()
end
--override
function SkillPassiveTabView:OnDestroy() 

end

function SkillPassiveTabView:OnEnable()
    self:AddEventListeners()
end

function SkillPassiveTabView:OnDisable()
    self:RemoveEventListeners()
end

--data结构
function SkillPassiveTabView:ShowView(data)
    self:RefreshView()
end

function SkillPassiveTabView:CloseView()

end

function SkillPassiveTabView:AddEventListeners()

end

function SkillPassiveTabView:RemoveEventListeners()

end

function SkillPassiveTabView:InitView()
    self._iconContainer = self:FindChildGO("Container_SkillInfo/Image_Skill/Container_Icon")
    self._lockImage = self:FindChildGO("Container_SkillInfo/Image_Lock")
    self._lockDescText = self:GetChildComponent("Container_SkillInfo/Image_Lock/Text_Desc", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Container_SkillInfo/Text_Desc", "TextMeshWrapper")
    self._nameText = self:GetChildComponent("Container_SkillInfo/Text_Name", "TextMeshWrapper")
    
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(SkillPassiveItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 5)
    self._list:SetDirection(UIList.DirectionTopToDown, 2, 2)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
end

function SkillPassiveTabView:OnListItemClicked(idx)
    if self._preSelected == idx then
        return
    end
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelect(false)
    end
    self._list:GetItem(idx):ShowSelect(true)
    self._preSelected = idx
    local data = self._list:GetDataByIndex(idx)
    self._selectedId = data[1]
    self:RefreshSelectedInfo()
end

function SkillPassiveTabView:RefreshView()
    local data = SkillDataHelper.GetAllSkillPassiveData(GameWorld.Player().vocation)
    self._list:SetDataList(data)
    self:OnListItemClicked(0)
end

function SkillPassiveTabView:RefreshSelectedInfo()
    local skillId = SkillDataHelper.GetPassiveSkillId(self._selectedId)
    local isLock = not SkillManager:IsPassiveSkillGot(skillId)
    local iconId = SkillDataHelper.GetSkillIcon(skillId)
    self._descText.text = SkillDataHelper.GetDesc(skillId)
    self._nameText.text = SkillDataHelper.GetName(skillId)
    if isLock then
        self._lockImage:SetActive(true)
        self._lockDescText.text = SkillDataHelper.GetPassiveSkillLockDesc(self._selectedId)
        GameWorld.AddIcon(self._iconContainer, iconId, 12)
    else
        self._lockImage:SetActive(false)
        GameWorld.AddIcon(self._iconContainer, iconId)
    end
end