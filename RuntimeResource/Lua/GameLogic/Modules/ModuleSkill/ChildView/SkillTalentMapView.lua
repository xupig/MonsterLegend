require "Modules.ModuleSkill.ChildComponent.SkillTalentMapItem"

local SkillTalentMapView = Class.SkillTalentMapView(ClassTypes.BaseLuaUIComponent)

SkillTalentMapView.interface = GameConfig.ComponentsConfig.Component
SkillTalentMapView.classPath = "Modules.ModuleSkill.ChildView.SkillTalentMapView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local SkillTalentMapItem = ClassTypes.SkillTalentMapItem
local SkillDataHelper = GameDataHelper.SkillDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local SkillManager = PlayerManager.SkillManager

local Max_Talent_Count = 15
local setV2 = Vector2.New(0,0)
local Unit_Width = 130

function SkillTalentMapView:Awake()
    self._preSelected = -1
    self._preId = -1
    self:InitView()
end
--override
function SkillTalentMapView:OnDestroy() 

end

function SkillTalentMapView:OnEnable()
    self:AddEventListeners()
end

function SkillTalentMapView:OnDisable()
    self:RemoveEventListeners()
end

--data结构
function SkillTalentMapView:ShowView(data)
    self:RefreshView(data)
end

function SkillTalentMapView:CloseView()

end

function SkillTalentMapView:AddEventListeners()

end

function SkillTalentMapView:RemoveEventListeners()

end

function SkillTalentMapView:InitView()
    self._lineGos = {}
    self._lineRects = {}
    self._lineImages = {}
    for i=1,4 do
        self._lineGos[i] = self:FindChildGO("Container_SkillMap/ScrollView/Mask/Content/Image_Line"..i)
        self._lineRects[i] = self:GetChildComponent("Container_SkillMap/ScrollView/Mask/Content/Image_Line"..i.."/Image_Progress", 'RectTransform')
        self._lineImages[i] = self:GetChildComponent("Container_SkillMap/ScrollView/Mask/Content/Image_Line"..i.."/Image_Progress", 'ImageWrapper')
        self._lineImages[i]:SetContinuousDimensionDirty(true)
    end

    self._talentItems = {}
    for i=1,Max_Talent_Count do
        self._talentItems[i] = self:AddChildLuaUIComponent("Container_SkillMap/ScrollView/Mask/Content/Container_Skill"..i, SkillTalentMapItem)
        self._talentItems[i]:SetIndex(i)
    end
end

-- data{[type],[list]}
function SkillTalentMapView:RefreshView(data)
    self.data = data
    for i=1,Max_Talent_Count do
        self._talentItems[i]:IsShow(false)
    end
    for i=1,#data.list do
        local id = data.list[i]
        self._talentItems[id]:IsShow(true)
        self._talentItems[id]:SetData(data.type)
    end
    self:ShowByType(data.type)
    EventDispatcher:TriggerEvent(GameEvents.Select_Skill_Talent_Map_Item, 1)
end

function SkillTalentMapView:OnSelected(idx)

end

function SkillTalentMapView:ShowByType(type)
    self._lineGos[1]:SetActive(true)
    self._lineGos[2]:SetActive(false)
    self._lineGos[3]:SetActive(true)
    self._lineGos[4]:SetActive(false)
    if type == 1 then
        self:RefreshLineTypeOne()
    elseif type == 2 then
        self:RefreshLineTypeOne()
    elseif type == 3 then
        self._lineGos[4]:SetActive(true)
        self:RefreshLineTypeTwo()
    else
        self._lineGos[2]:SetActive(true)
        self:RefreshLineTypeThree()
    end
end

function SkillTalentMapView:RefreshLineTypeOne()
    local data = {0,0,0}
    for i=1,#self.data.list do
        local id = self.data.list[i]
        if id % 3 == 1 then
            if self._talentItems[id]:IsActive() then
                if data[1] < id then
                    data[1] = id
                end
            end
        elseif id % 3 == 0 then
            if self._talentItems[id]:IsActive() then
                if data[3] < id then
                    data[3] = id
                end
            end
        end
    end
    --line1
    data[1] = (data[1]-1)/3
    setV2.x = self._lineRects[1].sizeDelta.x
    setV2.y = Unit_Width * data[1]
    self._lineRects[1].sizeDelta = setV2
    --line3
    data[3] = data[3]/3 - 1
    setV2.x = self._lineRects[3].sizeDelta.x
    setV2.y = Unit_Width * data[3]
    self._lineRects[3].sizeDelta = setV2
end

function SkillTalentMapView:RefreshLineTypeTwo()
    local data = {0,0,0}
    for i=1,#self.data.list do
        local id = self.data.list[i]
        if id % 3 == 1 then
            if self._talentItems[id]:IsActive() then
                if data[1] < id then
                    data[1] = id
                end
            end
        elseif id % 3 == 0 then
            if self._talentItems[id]:IsActive() then
                if data[3] < id then
                    data[3] = id
                end
            end
        end
    end
    --line1
    data[1] = (data[1]-1)/3
    setV2.x = self._lineRects[1].sizeDelta.x
    setV2.y = Unit_Width * data[1]
    self._lineRects[1].sizeDelta = setV2
    --line3
    data[3] = data[3]/3 - 1
    setV2.x = self._lineRects[3].sizeDelta.x
    setV2.y = Unit_Width * data[3]
    self._lineRects[3].sizeDelta = setV2
    --line4
    self._lineRects[4].gameObject:SetActive(self._talentItems[8]:IsActive())
end

function SkillTalentMapView:RefreshLineTypeThree()
    local data = {0,0,0}
    for i=1,#self.data.list do
        local id = self.data.list[i]
        if id % 3 == 1 then
            if self._talentItems[id]:IsActive() then
                if data[1] < id then
                    data[1] = id
                end
            end
        elseif id % 2 == 0 then
            if self._talentItems[id]:IsActive() then
                if data[2] < id then
                    data[2] = id
                end
            end
        elseif id % 3 == 0 then
            if self._talentItems[id]:IsActive() then
                if data[3] < id then
                    data[3] = id
                end
            end
        end
    end
    --line1
    data[1] = (data[1]-1)/3
    setV2.x = self._lineRects[1].sizeDelta.x
    setV2.y = Unit_Width * data[1]
    self._lineRects[1].sizeDelta = setV2
    --line2
    data[2] = (data[1]+1)/3 - 1
    setV2.x = self._lineRects[2].sizeDelta.x
    setV2.y = Unit_Width * data[2]
    self._lineRects[2].sizeDelta = setV2
    --line3
    data[3] = data[3]/3 - 1
    setV2.x = self._lineRects[3].sizeDelta.x
    setV2.y = Unit_Width * data[3]
    self._lineRects[3].sizeDelta = setV2
end

function SkillTalentMapView:SelectSkillTalentMapItem(id)
    if self._preId ~= -1 then
        self._talentItems[self._preId]:ShowSelected(false)
    end
    self._talentItems[id]:ShowSelected(true)
    self._preId = id
end

function SkillTalentMapView:RefreshAllData()
    for i=1,#self.data.list do
        local id = self.data.list[i]
        self._talentItems[id]:SetData(self.data.type)
    end
    self:ShowByType(self.data.type)
end