require "Modules.ModuleSkill.ChildComponent.SkillTalentTypeItem"
require "Modules.ModuleSkill.ChildView.SkillTalentMapView"
require "UIComponent.Extend.ItemGrid"

local SkillTalentTabView = Class.SkillTalentTabView(ClassTypes.BaseLuaUIComponent)

SkillTalentTabView.interface = GameConfig.ComponentsConfig.Component
SkillTalentTabView.classPath = "Modules.ModuleSkill.ChildView.SkillTalentTabView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local SkillTalentTypeItem = ClassTypes.SkillTalentTypeItem
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local SkillTalentMapView = ClassTypes.SkillTalentMapView
local SkillDataHelper = GameDataHelper.SkillDataHelper
local SkillManager = PlayerManager.SkillManager
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local bagData = PlayerManager.PlayerDataManager.bagData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local Type_To_Id = {[1]=25001, [2]=25002, [3]=25101, [4]=25006} --解锁条件对应中文
local Condition_To_Talent_Type = {[1]=1, [2]=2} --解锁条件对应天赋类型

function SkillTalentTabView:Awake()
    self._preSelected = -1
    self:InitView()
end
--override
function SkillTalentTabView:OnDestroy() 

end

function SkillTalentTabView:OnEnable()
    self:AddEventListeners()
end

function SkillTalentTabView:OnDisable()
    self:RemoveEventListeners()
end

--data结构
function SkillTalentTabView:ShowView(data)
    self:RefreshView()
end

function SkillTalentTabView:CloseView()

end

function SkillTalentTabView:AddEventListeners()
    self._selectSkillTalentMapItem = function(id) self:SelectSkillTalentMapItem(id) end
    EventDispatcher:AddEventListener(GameEvents.Select_Skill_Talent_Map_Item, self._selectSkillTalentMapItem)

    self._onRefreshSkillTalentInfo = function() self:OnRefreshSkillTalentInfo() end
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Skill_Talent_Info, self._onRefreshSkillTalentInfo)

    self._onRefreshSkillTalentSpendInfo = function() self:OnRefreshSkillTalentSpendInfo() end
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Skill_Talent_Spend_Info, self._onRefreshSkillTalentSpendInfo)

    self._onMoneyTalentChangeCB = function() self:ShowMoneyTalent() end
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_TALENT, self._onMoneyTalentChangeCB)

    self._onRefreshSkillTalentRedPoint = function() self:OnRefreshSkillTalentRedPoint() end
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Skill_Talent_Red_Point, self._onRefreshSkillTalentRedPoint)
end

function SkillTalentTabView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Select_Skill_Talent_Map_Item, self._selectSkillTalentMapItem)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Skill_Talent_Info, self._onRefreshSkillTalentInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Skill_Talent_Spend_Info, self._onRefreshSkillTalentSpendInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Skill_Talent_Red_Point, self._onRefreshSkillTalentRedPoint)

    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_TALENT, self._onMoneyTalentChangeCB)
end

function SkillTalentTabView:InitView()
    self._resetButton = self:FindChildGO("Button_Reset")
    self._csBH:AddClick(self._resetButton,function () self:OnResetButtonClick() end)
    self._addButton = self:FindChildGO("Button_Add")
    self._csBH:AddClick(self._addButton,function () self:OnAddButtonClick() end)

    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
    self._descText.text = LanguageDataHelper.GetContent(58602)

    self._pointText = self:GetChildComponent("Image_Point/Text_Value", "TextMeshWrapper")
    self._talentText = self:GetChildComponent("Image_Talent/Image_BG/Text_Value", "TextMeshWrapper")

    self._talentMapView = self:AddChildLuaUIComponent("Container_SkillMaps", SkillTalentMapView)

    self._iconContainer = self:FindChildGO("Container_SkillInfo/Container_Icon")
    self._activeImage = self:FindChildGO("Container_SkillInfo/Image_Active")
    self._lockImage = self:FindChildGO("Container_SkillInfo/Image_Lock")
    self._talentNameText = self:GetChildComponent("Container_SkillInfo/Text_Name", "TextMeshWrapper")
    self._talentLevelText = self:GetChildComponent("Container_SkillInfo/Text_Level/Text_Value", "TextMeshWrapper")

    self._descOneText = self:GetChildComponent("Container_SkillInfo/Container_Info1/Text_Desc", "TextMeshWrapper")
    self._descTwoText = self:GetChildComponent("Container_SkillInfo/Container_Info2/Text_Desc", "TextMeshWrapper")
    self._descThreeText = self:GetChildComponent("Container_SkillInfo/Container_Info3/Text_Desc", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(SkillTalentTypeItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 5)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._ticketContainerGO = self:FindChildGO("Container_Ticket")
    self._ticketIconGO = self:FindChildGO("Container_Ticket/Container_Icon")
    local itemGo = GUIManager.AddItem(self._ticketIconGO, 1)
    self._ticketIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._ticketInfoText = self:GetChildComponent("Container_Ticket/Text_Info", "TextMeshWrapper")
    self._ticketTipsText = self:GetChildComponent("Container_Ticket/Text_Tips", "TextMeshWrapper")
    self._ticketCountText = self:GetChildComponent("Container_Ticket/Text_Count", "TextMeshWrapper")
    self._ticketOKButton = self:FindChildGO("Container_Ticket/Button_OK")
    self._csBH:AddClick(self._ticketOKButton, function()self:TicketOKBtnClick() end)
    self._ticketCloseButton = self:FindChildGO("Container_Ticket/Button_Close")
    self._csBH:AddClick(self._ticketCloseButton, function()self:TicketCloseBtnClick() end)
    self._ticketContainerGO:SetActive(false)
    self._ticketTitleText = self:GetChildComponent("Container_Ticket/Text_Title", "TextMeshWrapper")
    self._ticketTitleText.text = LanguageDataHelper.CreateContent(517)

    self._bgIconObj = self:FindChildGO("Container_SkillMaps/Container_BgIcon")
    GameWorld.AddIcon(self._bgIconObj,13559)
end

function SkillTalentTabView:OnListItemClicked(idx)
    if self._preSelected == idx then
        return
    end
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelect(false)
    end
    self._list:GetItem(idx):ShowSelect(true)
    self._preSelected = idx
    local data = self._list:GetDataByIndex(idx)
    self.type = data[1]
    local slotData = SkillDataHelper.GetAllTalentLvDataByType(self.type)
    local showData = {type=self.type, list=slotData}
    self._talentMapView:ShowView(showData)
end

function SkillTalentTabView:TicketOKBtnClick()
    SkillManager:TalentResetReq()
    self._ticketContainerGO:SetActive(false)
end

function SkillTalentTabView:TicketCloseBtnClick()
    self._ticketContainerGO:SetActive(false)
end

function SkillTalentTabView:OnResetButtonClick()
    self._ticketContainerGO:SetActive(true)
    self._ticketTipsText.text = LanguageDataHelper.CreateContentWithArgs(22048, {["0"]=1})
    local itemId, needCount = next(GlobalParamsHelper.GetParamValue(456))
    local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    self._ticketIcon:SetItem(itemId)
    self._ticketCountText.text = BaseUtil.GetColorStringByCount(bagCount, needCount)
    self._ticketInfoText.text = LanguageDataHelper.CreateContentWithArgs(22047, {["0"]=needCount})
    local diff = needCount - bagCount
    if diff > 0 then
        local money = GlobalParamsHelper.GetParamValue(457)[itemId] * diff
        self._ticketTipsText.text = LanguageDataHelper.CreateContentWithArgs(22048, {["0"]=money})
    else
        self._ticketTipsText.text = ""
    end
end

function SkillTalentTabView:OnAddButtonClick()
    SkillManager:TalentLevelUpReq(self.type, self.selectedId)
end

function SkillTalentTabView:RefreshView()
    local data = SkillDataHelper.GetAllTalentTypes()
    self._list:SetDataList(data)
    self:OnListItemClicked(0)
    self:ShowMoneyTalent()
    self:OnRefreshSkillTalentRedPoint()
end

function SkillTalentTabView:ShowInfo()
    local vocGroup = RoleDataHelper.GetVocGroup(GameWorld.Player().vocation)
    local level = SkillManager:GetTalentLevel(self.type, self.selectedId)
    local maxLevel = SkillDataHelper.GetTalentLvMaxLevel(self.type, self.selectedId)
    local cfg = SkillDataHelper.GetSkillTalentData(vocGroup, self.type, self.selectedId, level)
    local nextDesc = ""
    local nextLevel = 0
    local nextCfg
    if level == 0 then
        --策划调整+2为+1
        nextLevel = level + 1
        if nextLevel > maxLevel then
            nextDesc = ""
        else
            nextCfg = SkillDataHelper.GetSkillTalentData(vocGroup, self.type, self.selectedId, nextLevel)
            nextDesc = SkillDataHelper.GetDesc(nextCfg.skill_id)
        end
    elseif level < maxLevel then
        nextLevel = level + 1
        nextCfg = SkillDataHelper.GetSkillTalentData(vocGroup, self.type, self.selectedId, nextLevel)
        nextDesc = SkillDataHelper.GetDesc(nextCfg.skill_id)
    else
        nextDesc = ""
    end

    local skillId = cfg.skill_id
    local desc = SkillDataHelper.GetDesc(skillId)
    local iconId = SkillDataHelper.GetSkillIcon(skillId)
    
    local state = SkillManager:GetActiveState(self.type, self.selectedId)
    if state == 0 then
        self._activeImage:SetActive(false)
        self._lockImage:SetActive(true)
        GameWorld.AddIcon(self._iconContainer, iconId, 12)
    elseif state == 1 then
        self._activeImage:SetActive(false)
        self._lockImage:SetActive(false)
        GameWorld.AddIcon(self._iconContainer, iconId, 12)
    elseif state == 2 then
        self._activeImage:SetActive(true)
        self._lockImage:SetActive(false)
        GameWorld.AddIcon(self._iconContainer, iconId)
    end

    local info = ""
    for i=1, #cfg.condition do
        local config = cfg.condition[i]
        if config[1] == 1 or config[1] == 2 then
            local points = SkillManager:GetTalentSpend(Condition_To_Talent_Type[config[1]])
            points = math.min(points, config[2])
            info = info .. LanguageDataHelper.GetContent(Type_To_Id[config[1]]) ..BaseUtil.GetColorString(string.format(" %d/%d", points, config[2]), ColorConfig.F).."\n"
        end
        if config[1] == 3 then
            local data = SkillDataHelper.GetSkillTalentCfg(config[2])
            local name = SkillDataHelper.GetName(data.skill_id)
            local curLevel = SkillManager:GetTalentLevel(data.type, data.group)
            curLevel = math.min(curLevel, data.level)
            local showStr = name.." ".. string.format("%d/%d", curLevel, data.level)
            info = info .. LanguageDataHelper.GetContent(Type_To_Id[config[1]]) ..BaseUtil.GetColorString(showStr, ColorConfig.F)
        end
        if config[1] == 4 then
            --总天赋条件
            local points = SkillManager:GetAllTalentSpend()
            points = math.min(points, config[2])
            info = info .. LanguageDataHelper.GetContent(Type_To_Id[config[1]]) ..BaseUtil.GetColorString(string.format(" %d/%d", points, config[2]), ColorConfig.F).."\n"
        end
    end

    local name = SkillDataHelper.GetName(skillId)
    self._talentNameText.text = name
    self._talentLevelText.text = string.format("%d%s", level, LanguageDataHelper.GetContent(69))
    self._descOneText.text = info
    self._descTwoText.text = desc
    self._descThreeText.text = nextDesc
    self._pointText.text = cfg.levelup_cost
end

function SkillTalentTabView:SelectSkillTalentMapItem(id)
    self._talentMapView:SelectSkillTalentMapItem(id)
    self.selectedId = id
    self:ShowInfo()
end

function SkillTalentTabView:ShowMoneyTalent()
    self._talentText.text = GameWorld.Player().money_talent
end

function SkillTalentTabView:OnRefreshSkillTalentInfo()
    self._talentMapView:RefreshAllData()
    self:ShowInfo()
end

function SkillTalentTabView:OnRefreshSkillTalentSpendInfo()
    local data = SkillDataHelper.GetAllTalentTypes()
    self._list:SetDataList(data)
end

function SkillTalentTabView:OnRefreshSkillTalentRedPoint()
    local data = SkillDataHelper.GetAllTalentTypes()
    local redPointData = SkillManager:GetTalentRedPointTypeData()
    for k,v in pairs(data) do
        if redPointData[k] ~= nil then
            self._list:GetItem(k-1):ShowRedPoint(true)
        else
            self._list:GetItem(k-1):ShowRedPoint(false)
        end
    end
end