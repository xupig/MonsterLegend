require "Modules.ModuleSkill.ChildView.SkillActivePlayerView"

local SkillActiveTabView = Class.SkillActiveTabView(ClassTypes.BaseLuaUIComponent)

SkillActiveTabView.interface = GameConfig.ComponentsConfig.Component
SkillActiveTabView.classPath = "Modules.ModuleSkill.ChildView.SkillActiveTabView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local SkillDataHelper = GameDataHelper.SkillDataHelper
local SkillActivePlayerView = ClassTypes.SkillActivePlayerView
local UIToggleGroup = ClassTypes.UIToggleGroup

function SkillActiveTabView:Awake()
    self._selectTab = -1
    self._indexToGO = {}
    self._indexToView = {}
    self:InitView()
end
--override
function SkillActiveTabView:OnDestroy() 

end

function SkillActiveTabView:OnEnable()
    self:AddEventListeners()
end

function SkillActiveTabView:OnDisable()
    self:RemoveEventListeners()
end

--data结构
function SkillActiveTabView:ShowView(data)
    self:RefreshView()
end

function SkillActiveTabView:CloseView()

end

function SkillActiveTabView:AddEventListeners()
    self._selectActiveSkillPlayerItem = function(idx) self:SelectActiveSkillPlayerItem(idx) end
    EventDispatcher:AddEventListener(GameEvents.Select_Active_Skill_Player_Item, self._selectActiveSkillPlayerItem)
    self._onSelectedConfigItem = function(idx) self:SelectActiveSkillConfigItem(idx) end
    EventDispatcher:AddEventListener(GameEvents.On_Selected_Active_Skill_Config_Item, self._onSelectedConfigItem)
    self._onRefreshActiveSkillData = function() self:RefreshActiveSkillConfigView() end
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Active_Skill_Data, self._onRefreshActiveSkillData)
    self._refreshActivePlayerAutoState = function() self:RefreshActivePlayerAutoState() end
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Active_Skill_Auto_Cast, self._refreshActivePlayerAutoState)
end

function SkillActiveTabView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Select_Active_Skill_Player_Item, self._selectActiveSkillPlayerItem)
    EventDispatcher:RemoveEventListener(GameEvents.On_Selected_Active_Skill_Config_Item, self._onSelectedConfigItem)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Active_Skill_Data, self._onRefreshActiveSkillData)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Active_Skill_Auto_Cast, self._refreshActivePlayerAutoState)
end

function SkillActiveTabView:InitView()
    self._indexToGO[0] = self:FindChildGO("Container_PlayerSkill")

    self._indexToView[0] = self:AddChildLuaUIComponent("Container_PlayerSkill", SkillActivePlayerView)

    self:InitToggleGroup()

    self._vocationIconObj = self:FindChildGO("Container_PlayerSkill/Container_VocationIcon")
    GameWorld.AddIcon(self._vocationIconObj,13560)
end

function SkillActiveTabView:InitToggleGroup()
    self.toggle = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup_Function")
	self.toggle:SetOnSelectedIndexChangedCB(function(index) self:OnToggleGroupClick(index) end)
    self.toggle:SetSelectIndex(0)
    self:OnToggleGroupClick(0)
end

function SkillActiveTabView:OnToggleGroupClick(index)
    if self._selectTab == index then
        return
    end
    if self._selectTab ~= -1 then
        self._indexToGO[self._selectTab]:SetActive(false)
    end
    self._indexToGO[index]:SetActive(true)
    if self._indexToView[index] ~= nil then
        self._indexToView[index]:ShowView()
    end
    self._selectTab = index
end

function SkillActiveTabView:RefreshView()
    self:OnToggleGroupClick(0)
end

function SkillActiveTabView:SelectActiveSkillPlayerItem(idx)
    self._indexToView[0]:SelectActiveSkillPlayerItem(idx)
end

function SkillActiveTabView:SelectActiveSkillConfigItem(idx)
    self._indexToView[0]:SelectActiveSkillConfigItem(idx)
end

function SkillActiveTabView:RefreshActiveSkillConfigView()
    self._indexToView[0]:RefreshActiveSkillConfigView()
end

function SkillActiveTabView:RefreshActivePlayerAutoState()
    self._indexToView[0]:RefreshAutoState()
end