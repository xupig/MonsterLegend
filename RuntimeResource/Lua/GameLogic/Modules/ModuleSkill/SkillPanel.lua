require "Modules.ModuleSkill.ChildView.SkillActiveTabView"
require "Modules.ModuleSkill.ChildView.SkillPassiveTabView"
require "Modules.ModuleSkill.ChildView.SkillTalentTabView"

local SkillPanel = Class.SkillPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local SkillManager = PlayerManager.SkillManager
local skillData = PlayerManager.PlayerDataManager.skillData
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local SkillActiveTabView = ClassTypes.SkillActiveTabView
local SkillPassiveTabView = ClassTypes.SkillPassiveTabView
local SkillTalentTabView = ClassTypes.SkillTalentTabView
local UIToggleGroup = ClassTypes.UIToggleGroup

function SkillPanel:Awake()
	self._selectTab = -1
    self._indexToGO = {}
    self._indexToView = {}
    self._tabClickCB = function (index)
        self:OnToggleGroupClick(index-1)
    end
	self:InitView()
end

--override
function SkillPanel:OnDestroy()
end

function SkillPanel:OnShow(data)
    self:AddEventListeners()
	self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
    GameWorld.ShowPlayer():ShowModel(2)
end

function SkillPanel:OnClose()
    GameWorld.ShowPlayer():ShowModel(3)
    self:RemoveEventListeners()
end

function SkillPanel:AddEventListeners()

end

function SkillPanel:RemoveEventListeners()

end

function SkillPanel:InitView()
	self._indexToGO[0] = self:FindChildGO("Container_ActiveSkill")
	self._indexToGO[1] = self:FindChildGO("Container_PassiveSkill")
	self._indexToGO[2] = self:FindChildGO("Container_TalentSkill")

	self._indexToView[0] = self:AddChildLuaUIComponent("Container_ActiveSkill", SkillActiveTabView)
	self._indexToView[1] = self:AddChildLuaUIComponent("Container_PassiveSkill", SkillPassiveTabView)
	self._indexToView[2] = self:AddChildLuaUIComponent("Container_TalentSkill", SkillTalentTabView)

    self:SetTabClickCallBack(self._tabClickCB)
end

function SkillPanel:OnToggleGroupClick(index)
    if self._selectTab == index then
        return
    end
    if self._selectTab ~= -1 then
        if self._indexToView[index] ~= nil then
            self._indexToView[index]:CloseView()
        end
        self._indexToGO[self._selectTab]:SetActive(false)
    end
    self._indexToGO[index]:SetActive(true)
    if self._indexToView[index] ~= nil then
        self._indexToView[index]:ShowView()
    end
    self._selectTab = index
end

function SkillPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--关闭的时候重新开启二级主菜单
function SkillPanel:ReopenSubMainPanel()
    return true
end