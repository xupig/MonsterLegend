local SkillPassiveItem = Class.SkillPassiveItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
SkillPassiveItem.interface = GameConfig.ComponentsConfig.Component
SkillPassiveItem.classPath = "Modules.ModuleSkill.ChildComponent.SkillPassiveItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local BaseUtil = GameUtil.BaseUtil
local SkillDataHelper = GameDataHelper.SkillDataHelper
local SkillManager = PlayerManager.SkillManager

function SkillPassiveItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self._active = false
    self:InitView()
end

function SkillPassiveItem:OnDestroy()
    self.data = nil
end

--override data {id}
function SkillPassiveItem:OnRefreshData(data)
    self.data = data
    self:ShowView()
end

function SkillPassiveItem:InitView()
    self._skillIcon = self:FindChildGO("Image_Skill/Container_Icon")
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._selectImage = self:FindChildGO("Image_Selected")
    self._lockText = self:GetChildComponent("Image_Lock/Text_LockInfo", "TextMeshWrapper")
    self._lockImage = self:FindChildGO("Image_Lock")
end

function SkillPassiveItem:ShowView()
    local skillId = SkillDataHelper.GetPassiveSkillId(self.data[1])
    local icon = SkillDataHelper.GetSkillIcon(skillId)
    local name = SkillDataHelper.GetName(skillId)
    self._nameText.text = name
    local isLock = not SkillManager:IsPassiveSkillGot(skillId)
    if isLock then
        self._lockImage:SetActive(true)
        self._lockText.text = SkillDataHelper.GetPassiveSkillLockDesc(self.data[1])
        GameWorld.AddIcon(self._skillIcon, icon, 12)
    else
        self._lockImage:SetActive(false)
        GameWorld.AddIcon(self._skillIcon, icon)
    end
end

function SkillPassiveItem:ShowSelect(state)
    self._selectImage:SetActive(state)
end