local SkillActiveConfigItem = Class.SkillActiveConfigItem(ClassTypes.BaseLuaUIComponent)

SkillActiveConfigItem.interface = GameConfig.ComponentsConfig.PointableComponent
SkillActiveConfigItem.classPath = "Modules.ModuleSkill.ChildComponent.SkillActiveConfigItem"

local SkillDataHelper = GameDataHelper.SkillDataHelper
local SkillManager = PlayerManager.SkillManager

function SkillActiveConfigItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function SkillActiveConfigItem:OnDestroy() 

end

function SkillActiveConfigItem:InitView()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._selectedImage = self:FindChildGO("Image_Selected")
    self._lockImage = self:FindChildGO("Image_Lock")
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
end

function SkillActiveConfigItem:SetIndex(idx)
    self.index = idx
end

--主动技能配置表id，槽 0表示无槽位数据
function SkillActiveConfigItem:SetData(data)
    self.data = data
    local id = self.data
    if id > 0 then
        self._lock = false
        local skillId = SkillDataHelper.GetActiveSkillId(id, GameWorld.Player().vocation)[1]
        local iconId = SkillDataHelper.GetSkillIcon(skillId)
        self._lockImage:SetActive(false)
        self._iconContainer:SetActive(true)
        GameWorld.AddIcon(self._iconContainer, iconId)
        self._nameText.text = SkillDataHelper.GetName(skillId)
    else
        self._lock = true
        self._nameText.text = ""
        self._lockImage:SetActive(true)
        self._iconContainer:SetActive(false)
    end
end

function SkillActiveConfigItem:OnPointerDown(pointerEventData)
    if not self._lock then
        EventDispatcher:TriggerEvent(GameEvents.On_Selected_Active_Skill_Config_Item, self.index)
    end
end

function SkillActiveConfigItem:OnPointerUp(pointerEventData)

end

function SkillActiveConfigItem:ShowSelected(state)
    self._selectedImage:SetActive(state)
end