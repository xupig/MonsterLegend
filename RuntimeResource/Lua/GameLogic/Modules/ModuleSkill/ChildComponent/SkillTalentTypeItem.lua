local SkillTalentTypeItem = Class.SkillTalentTypeItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
SkillTalentTypeItem.interface = GameConfig.ComponentsConfig.Component
SkillTalentTypeItem.classPath = "Modules.ModuleSkill.ChildComponent.SkillTalentTypeItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local BaseUtil = GameUtil.BaseUtil
local SkillDataHelper = GameDataHelper.SkillDataHelper
local SkillManager = PlayerManager.SkillManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function SkillTalentTypeItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self:InitView()
end

function SkillTalentTypeItem:OnDestroy()
    self.data = nil
end

--override data {id}
function SkillTalentTypeItem:OnRefreshData(data)
    self.data = data
    self:ShowView()
end

function SkillTalentTypeItem:InitView()
    self._icon = self:FindChildGO("Container_Icon")
    self._countText = self:GetChildComponent("Text_Count", "TextMeshWrapper")
    self._selectImage = self:FindChildGO("Image_Selected")
    self._redPointImage = self:FindChildGO("Image_RedPoint")
end

function SkillTalentTypeItem:ShowView()
    local icon = GlobalParamsHelper.GetParamValue(467)[self.data[1]]
    local count = SkillManager:GetTalentSpend(self.data[1])
    GameWorld.AddIcon(self._icon, icon)
    self._countText.text = count
end

function SkillTalentTypeItem:ShowSelect(state)
    self._selectImage:SetActive(state)
end

function SkillTalentTypeItem:ShowRedPoint(state)
    self._redPointImage:SetActive(state)
end