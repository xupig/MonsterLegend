local SkillActivePlayerItem = Class.SkillActivePlayerItem(ClassTypes.BaseLuaUIComponent)

SkillActivePlayerItem.interface = GameConfig.ComponentsConfig.PointableComponent
SkillActivePlayerItem.classPath = "Modules.ModuleSkill.ChildComponent.SkillActivePlayerItem"

local SkillDataHelper = GameDataHelper.SkillDataHelper
local SkillManager = PlayerManager.SkillManager
local RectTransform = UnityEngine.RectTransform

local Original_Scale = Vector3(1, 1, 1)
local Bigger_Scale = Vector3(1.1, 1.1, 1)

function SkillActivePlayerItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function SkillActivePlayerItem:OnDestroy() 

end

function SkillActivePlayerItem:InitView()
    self._skillImage = self:FindChildGO("Image_Skill")
    self._iconContainer = self:FindChildGO("Image_Skill/Container_Icon")
    self._selectedImage = self:FindChildGO("Image_Selected")
    self._lockImage = self:FindChildGO("Image_Lock")
end

-- {id} --主动技能配置表id，槽
function SkillActivePlayerItem:SetData(data)
    self.data = data
    local skillId = SkillDataHelper.GetActiveSkillId(data[1], GameWorld.Player().vocation)[1]
    local iconId = SkillDataHelper.GetSkillIcon(skillId)
    self.isLock = not SkillManager:IsActiveSkillGot(data[1])
    self._lockImage:SetActive(self.isLock)
    if self.isLock then
        GameWorld.AddIcon(self._iconContainer, iconId, 12)
    else
        GameWorld.AddIcon(self._iconContainer, iconId)
    end
end

function SkillActivePlayerItem:OnPointerDown(pointerEventData)
    EventDispatcher:TriggerEvent(GameEvents.Select_Active_Skill_Player_Item, self.data[1])
end

function SkillActivePlayerItem:OnPointerUp(pointerEventData)

end

function SkillActivePlayerItem:ShowSelected(state)
    self._selectedImage:SetActive(state)
    if state then
        self._skillImage.transform.localScale = Bigger_Scale
    else
        self._skillImage.transform.localScale = Original_Scale
    end
end