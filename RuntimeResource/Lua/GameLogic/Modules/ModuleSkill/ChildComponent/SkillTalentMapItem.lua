local SkillTalentMapItem = Class.SkillTalentMapItem(ClassTypes.BaseLuaUIComponent)

SkillTalentMapItem.interface = GameConfig.ComponentsConfig.PointableComponent
SkillTalentMapItem.classPath = "Modules.ModuleSkill.ChildComponent.SkillTalentMapItem"

local SkillDataHelper = GameDataHelper.SkillDataHelper
local SkillManager = PlayerManager.SkillManager

function SkillTalentMapItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._activeState = 0 --0:未激活 1：可激活 2：已激活
    self:InitView()
end
--override
function SkillTalentMapItem:OnDestroy() 

end

function SkillTalentMapItem:InitView()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._selectedImage = self:FindChildGO("Image_Selected")
    self._activeImage = self:FindChildGO("Image_Active")
    self._lockImage = self:FindChildGO("Image_Lock")
    self._levelText = self:GetChildComponent("Image_Value/Text_Value", "TextMeshWrapper")
    self._activeImage:SetActive(false)
end

function SkillTalentMapItem:SetIndex(idx)
    self.index = idx
end

-- type
function SkillTalentMapItem:SetData(data)
    self.data = data
    local vocGroup = RoleDataHelper.GetVocGroup(GameWorld.Player().vocation)
    local level = SkillManager:GetTalentLevel(self.data, self.index)
    local maxLevel = SkillDataHelper.GetTalentLvMaxLevel(data, self.index)
    local cfg = SkillDataHelper.GetSkillTalentData(vocGroup, data, self.index, level)
    local skillId = cfg.skill_id
    local iconId = SkillDataHelper.GetSkillIcon(skillId)
    self._levelText.text = level.."/"..maxLevel

    self._activeState = SkillManager:GetActiveState(self.data, self.index)
    if self._activeState == 0 then
        self._lockImage:SetActive(true)
        self._activeImage:SetActive(false)
        GameWorld.AddIcon(self._iconContainer, iconId, 12)
    elseif self._activeState == 1 then
        self._lockImage:SetActive(false)
        self._activeImage:SetActive(false)
        GameWorld.AddIcon(self._iconContainer, iconId, 12)
    elseif self._activeState == 2 then
        self._lockImage:SetActive(false)
        self._activeImage:SetActive(true)
        GameWorld.AddIcon(self._iconContainer, iconId)
    end
end

function SkillTalentMapItem:OnPointerDown(pointerEventData)
    EventDispatcher:TriggerEvent(GameEvents.Select_Skill_Talent_Map_Item, self.index)
end

function SkillTalentMapItem:OnPointerUp(pointerEventData)

end

function SkillTalentMapItem:ShowSelected(state)
    self._selectedImage:SetActive(state)
end

function SkillTalentMapItem:IsShow(state)
    self.gameObject:SetActive(state)
end

function SkillTalentMapItem:IsActive()
    return self._activeState == 2
end