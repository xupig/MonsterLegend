-- SkillModule.lua
SkillModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function SkillModule.Init()
	GUIManager.AddPanel(PanelsConfig.Skill, "Panel_Skill", GUILayer.LayerUIPanel, "Modules.ModuleSkill.SkillPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return SkillModule