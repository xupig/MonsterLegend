--幕间剧的ListItem
-- MyLayerItem.lua
local MyLayerItem = Class.MyLayerItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AionDataHelper = GameDataHelper.AionDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local action_config = require("ServerConfig/action_config")
local UIListItem = ClassTypes.UIListItem
MyLayerItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
MyLayerItem.classPath = "Modules.ModuleAion.ChildComponent.MyLayerItem"
local AionConfig = GameConfig.AionConfig

function MyLayerItem:Awake()
    self._base.Awake(self)
    
    self._textLayerName = self:GetChildComponent("Text_LayerName", "TextMeshWrapper")
    self._textTime = self:GetChildComponent("Text_Time", "TextMeshWrapper")
end

function MyLayerItem:OnDestroy()
    self._textLayerName = nil
    self._textTime = nil
    self._base.OnDestroy(self)
end

--override
function MyLayerItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] =  data.layer
        self._textLayerName.text = LanguageDataHelper.CreateContent(55001, argsTable)
        self._textTime.text = data.time .. LanguageDataHelper.CreateContent(150)
    else
        self._textLayerName.text = ""
        self._textTime.text = ""
    end
end
