-- AionPassRewardItem.lua
local AionPassRewardItem = Class.AionPassRewardItem(ClassTypes.UIListItem)
AionPassRewardItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
AionPassRewardItem.classPath = "Modules.ModuleAion.ChildComponent.AionPassRewardItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

local AionConfig = GameConfig.AionConfig

function AionPassRewardItem:Awake()
    self._base.Awake(self)
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._icon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
    self._newImage = self:FindChildGO("Image_New_Rune")
    self._icon:ActivateTipsById()
end

function AionPassRewardItem:OnDestroy()
    self._base.OnDestroy(self)
end

--override
function AionPassRewardItem:OnRefreshData(data)
    local type = data.type
    if type == AionConfig.AION_UNLOCK_RUNE then--解锁符文槽
        self._newImage:SetActive(true)
        self._itemContainer:SetActive(false)
    elseif type == AionConfig.AION_UNLOCK_NEW then--解锁新符文
        self._newImage:SetActive(false)
        self._itemContainer:SetActive(true)
        self._icon:SetItem(data.id)
    end
end