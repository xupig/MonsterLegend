--幕间剧的ListItem
-- TowerLevelItem.lua
local TowerLevelItem = Class.TowerLevelItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AionDataHelper = GameDataHelper.AionDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local action_config = require("ServerConfig/action_config")
local UIListItem = ClassTypes.UIListItem
TowerLevelItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
TowerLevelItem.classPath = "Modules.ModuleAion.ChildComponent.TowerLevelItem"
local AionConfig = GameConfig.AionConfig
local Vector2 = Vector2
local posX = {50, 150, 250, 350, 450}

function TowerLevelItem:Awake()
    self._base.Awake(self)
    
    self._textLevel = self:GetChildComponent("Container_content/Container_Level/Text_Level", "TextMeshWrapper")
    self._textNowLevel = self:GetChildComponent("Container_content/Container_Level/Text_Now", "TextMeshWrapper")
    self._containerLevel = self:FindChildGO("Container_content/Container_Level")
    self._transLevel = self._containerLevel.transform
    self._levelPosY = self._transLevel.localPosition.y
    -- self._containerBoss = self:FindChildGO("Container_content/Container_Level/Text_Boss")
    -- self._containerCur = self:FindChildGO("Container_content/Container_Level/Image_Cur")
    self._containerForbid = self:FindChildGO("Container_content/Container_Level/Image_Forbid")
    self._containerForbid:SetActive(false)
    -- self._csBH:AddClick(self._containerLevel, function()self:LevelBtnClick() end)
end

function TowerLevelItem:OnDestroy()
    self._textLevel = nil
    self._containerLevel = nil
    -- self._containerBoss = nil
    -- self._containerCur = nil
    self._containerForbid = nil
    self._base.OnDestroy(self)
end

--override
function TowerLevelItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = data.id
        self._textLevel.text = data.id--LanguageDataHelper.CreateContent(55001, argsTable)
        self._textNowLevel.text = data.id
        -- self:SetIsBoss(AionDataHelper:GetType(data.id) == 2)-- BOSS层
        self:SetDoorState(data.state)
    else
        -- self._textTitle.text = ""
        -- self._textTimes.text = ""
        end
end

-- function TowerLevelItem:LevelBtnClick()

-- end

-- function TowerLevelItem:SetIsBoss(isBoss)
--     if isBoss then
--         self._containerBoss:SetActive(true)
--     else
--         self._containerBoss:SetActive(false)
--     end
-- end
function TowerLevelItem:SetDoorState(state)
    self._state = state
    if state == AionConfig.LOCK then
        -- self._containerForbid:SetActive(true)
        self._textLevel.gameObject:SetActive(true)
        self._textNowLevel.gameObject:SetActive(false)
        -- self._containerCur:SetActive(false)
    elseif state == AionConfig.CURRENT then
        -- self._containerForbid:SetActive(false)
        self._textLevel.gameObject:SetActive(false)
        self._textNowLevel.gameObject:SetActive(true)
        -- self._containerCur:SetActive(true)
    elseif state == AionConfig.UNLOCK then
        -- self._containerForbid:SetActive(false)
        self._textLevel.gameObject:SetActive(true)
        self._textNowLevel.gameObject:SetActive(false)
        -- self._containerCur:SetActive(false)
    end
end

function TowerLevelItem:SetLevelPos()
    local index = self:GetIndex()
    local value = index % 6
    if value == 0 then
        self._transLevel.localPosition = Vector2(posX[4], self._levelPosY)
    elseif value == 1 then
        self._transLevel.localPosition = Vector2(posX[5], self._levelPosY)
    elseif value == 2 then
        self._transLevel.localPosition = Vector2(posX[1], self._levelPosY)
    elseif value == 3 then
        self._transLevel.localPosition = Vector2(posX[3], self._levelPosY)
    elseif value == 4 then
        self._transLevel.localPosition = Vector2(posX[5], self._levelPosY)
    elseif value == 5 then
        self._transLevel.localPosition = Vector2(posX[2], self._levelPosY)
    end
end

function TowerLevelItem:Hide()
    self:SetActive(false)
end