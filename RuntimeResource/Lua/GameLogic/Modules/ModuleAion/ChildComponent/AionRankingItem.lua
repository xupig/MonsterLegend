--幕间剧的ListItem
-- AionRankingItem.lua
local AionRankingItem = Class.AionRankingItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AionDataHelper = GameDataHelper.AionDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local action_config = require("ServerConfig/action_config")
local UIListItem = ClassTypes.UIListItem
AionRankingItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
AionRankingItem.classPath = "Modules.ModuleAion.ChildComponent.AionRankingItem"
local AionConfig = GameConfig.AionConfig
local public_config = GameWorld.public_config

function AionRankingItem:Awake()
    self._base.Awake(self)
    
    self._textNumber = self:GetChildComponent("Text_Number", "TextMeshWrapper")
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._textLays = self:GetChildComponent("Text_Lays", "TextMeshWrapper")
    self._textTimes = self:GetChildComponent("Text_Times", "TextMeshWrapper")
    self._imageIcon1 = self:FindChildGO("Image_Icon1")
    self._imageIcon2 = self:FindChildGO("Image_Icon2")
    self._imageIcon3 = self:FindChildGO("Image_Icon3")
end

function AionRankingItem:OnDestroy()
    self._textNumber = nil
    self._textName = nil
    self._textLays = nil
    self._textTimes = nil
    self._imageIcon1 = nil
    self._imageIcon2 = nil
    self._imageIcon3 = nil
    self._base.OnDestroy(self)
end

--override
function AionRankingItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        self:SetNumber(self:GetIndex() + 1)
        self._textName.text = data[public_config.RANK_KEY_NAME]
        self._textLays.text = data[public_config.RANK_KEY_HIGHEST_LAYER]
        self._textTimes.text = data[public_config.RANK_KEY_AION_TOTAL_TIME]
    else
        self._textName.text = ""
        self._textLays.text = ""
        self._textTimes.text = ""
        self:SetNumber("")
    end
end

function AionRankingItem:SetNumber(number)
    if number == 1 then
        self._imageIcon1:SetActive(true)
        self._imageIcon2:SetActive(false)
        self._imageIcon3:SetActive(false)
        self._textNumber.gameObject:SetActive(false)
    elseif number == 2 then
        self._imageIcon1:SetActive(false)
        self._imageIcon2:SetActive(true)
        self._imageIcon3:SetActive(false)
        self._textNumber.gameObject:SetActive(false)
    elseif number == 3 then
        self._imageIcon1:SetActive(false)
        self._imageIcon2:SetActive(false)
        self._imageIcon3:SetActive(true)
        self._textNumber.gameObject:SetActive(false)
    else
        self._imageIcon1:SetActive(false)
        self._imageIcon2:SetActive(false)
        self._imageIcon3:SetActive(false)
        self._textNumber.gameObject:SetActive(true)
        self._textNumber.text = number
    end
end
