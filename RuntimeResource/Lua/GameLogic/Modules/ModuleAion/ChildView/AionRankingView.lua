-- AionRankingView.lua
local AionRankingView = Class.AionRankingView(ClassTypes.BaseComponent)

require "Modules.ModuleAion.ChildComponent.AionRankingItem"
local AionRankingItem = ClassTypes.AionRankingItem

AionRankingView.interface = GameConfig.ComponentsConfig.Component
AionRankingView.classPath = "Modules.ModuleAion.ChildView.AionRankingView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AionDataHelper = GameDataHelper.AionDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil

local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local AionTowerManager = PlayerManager.AionTowerManager
local AionConfig = GameConfig.AionConfig


function AionRankingView:Awake()
    self._textNumber = self:GetChildComponent("Container_CurRanking/Text_Number", "TextMeshWrapper")
    self._textName = self:GetChildComponent("Container_CurRanking/Text_Name", "TextMeshWrapper")
    self._textLays = self:GetChildComponent("Container_CurRanking/Text_Lays", "TextMeshWrapper")
    self._textTimes = self:GetChildComponent("Container_CurRanking/Text_Times", "TextMeshWrapper")
    self._imageIcon1 = self:FindChildGO("Container_CurRanking/Image_Icon1")
    self._imageIcon2 = self:FindChildGO("Container_CurRanking/Image_Icon2")
    self._imageIcon3 = self:FindChildGO("Container_CurRanking/Image_Icon3")
    self._textNoRanking = self:FindChildGO("Text_NoRanking")
    self._containerCurRanking = self:FindChildGO("Container_CurRanking")
    
    self:InitRankingList()
end

function AionRankingView:InitRankingList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._scrollView:SetHorizontalMove(false)
    self._scrollView:SetVerticalMove(true)
    self._list:SetItemType(AionRankingItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 100)
end

function AionRankingView:ShowInfo(args)
    -- LoggerHelper.Log("Ash: ShowInfo: " .. PrintTable:TableToStr(args))
    self._list:SetDataList(args)
    local index, myRankingData = self:GetMyRanking(args)
    if myRankingData ~= nil then
        self._containerCurRanking:SetActive(true)
        self._textNoRanking:SetActive(false)
        self:SetNumber(index)
        self._textName.text = myRankingData[public_config.RANK_KEY_NAME]
        self._textLays.text = myRankingData[public_config.RANK_KEY_HIGHEST_LAYER]
        self._textTimes.text = myRankingData[public_config.RANK_KEY_AION_TOTAL_TIME]
    else
        self._containerCurRanking:SetActive(false)
        self._textNoRanking:SetActive(true)
    end
end

function AionRankingView:GetMyRanking(rankingList)
    local dbid = GameWorld.Player().dbid
    for k, v in pairs(rankingList) do
        if v[public_config.RANK_KEY_DBID] == dbid then
            return k, v
        end
    end
end

function AionRankingView:OnDestroy()
    self._list = nil
    self._scrollView = nil
end

function AionRankingView:SetNumber(number)
    if number == 1 then
        self._imageIcon1:SetActive(true)
        self._imageIcon2:SetActive(false)
        self._imageIcon3:SetActive(false)
        self._textNumber.gameObject:SetActive(false)
    elseif number == 2 then
        self._imageIcon1:SetActive(false)
        self._imageIcon2:SetActive(true)
        self._imageIcon3:SetActive(false)
        self._textNumber.gameObject:SetActive(false)
    elseif number == 3 then
        self._imageIcon1:SetActive(false)
        self._imageIcon2:SetActive(false)
        self._imageIcon3:SetActive(true)
        self._textNumber.gameObject:SetActive(false)
    else
        self._imageIcon1:SetActive(false)
        self._imageIcon2:SetActive(false)
        self._imageIcon3:SetActive(false)
        self._textNumber.gameObject:SetActive(true)
        self._textNumber.text = number
    end
end
