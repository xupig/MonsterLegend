-- MyLayerDataView.lua
local MyLayerDataView = Class.MyLayerDataView(ClassTypes.BaseComponent)

require "Modules.ModuleAion.ChildComponent.MyLayerItem"
local MyLayerItem = ClassTypes.MyLayerItem

MyLayerDataView.interface = GameConfig.ComponentsConfig.Component
MyLayerDataView.classPath = "Modules.ModuleAion.ChildView.MyLayerDataView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AionDataHelper = GameDataHelper.AionDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil

local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local AionTowerManager = PlayerManager.AionTowerManager
local AionConfig = GameConfig.AionConfig
local DateTimeUtil = GameUtil.DateTimeUtil


function MyLayerDataView:Awake()
    self._textTopLevel = self:GetChildComponent("Text_TopLevelValue", "TextMeshWrapper")
    self._textTotalTime = self:GetChildComponent("Text_TotalTimeValue", "TextMeshWrapper")
    
    self:InitList()
end

function MyLayerDataView:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._scrollView:SetHorizontalMove(false)
    self._scrollView:SetVerticalMove(true)
    self._list:SetItemType(MyLayerItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 100)
end

function MyLayerDataView:ShowInfo()
    local aionInfo = AionTowerManager:GetAionInfo()
    local cleanRanceRecords = aionInfo[public_config.AION_CLEANRANCE_RECORDS]
    -- LoggerHelper.Log("Ash: cleanRanceRecords: " .. PrintTable:TableToStr(cleanRanceRecords))
    local totalTime = 0
    local layerData = {}
    local allIdCount = #cleanRanceRecords
    local allIdIndex = allIdCount
    local result = {}
    for i = 1, allIdCount do
        table.insert(result, {layer = allIdIndex, time = cleanRanceRecords[allIdIndex]})
    -- LoggerHelper.Log("Ash: cleanRanceRecords[allIdIndex]: " .. allIdIndex .. " " .. tostring(cleanRanceRecords[allIdIndex]))
        totalTime = totalTime + cleanRanceRecords[allIdIndex]
        allIdIndex = allIdIndex - 1
    end
    self._textTopLevel.text = aionInfo[public_config.AION_HIGHEST_LAYER]
    self._textTotalTime.text = DateTimeUtil:FormatFullTime(totalTime, false, true, true)
    self._list:SetDataList(result)
end

function MyLayerDataView:OnDestroy()
    self._list = nil
    self._scrollView = nil
    self._textTopLevel = nil
    self._textTotalTime = nil
end
