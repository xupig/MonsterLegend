-- AionTowerView.lua
local AionTowerView = Class.AionTowerView(ClassTypes.BaseComponent)

require "Modules.ModuleAion.ChildComponent.TowerLevelItem"
local TowerLevelItem = ClassTypes.TowerLevelItem
require "Modules.ModuleAion.ChildComponent.AionPassRewardItem"
local AionPassRewardItem = ClassTypes.AionPassRewardItem
require"Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem

AionTowerView.interface = GameConfig.ComponentsConfig.Component
AionTowerView.classPath = "Modules.ModuleAion.ChildView.AionTowerView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AionDataHelper = GameDataHelper.AionDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil

local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local AionTowerManager = PlayerManager.AionTowerManager
local RankManager = PlayerManager.RankManager
local AionConfig = GameConfig.AionConfig
local XArtNumber = GameMain.XArtNumber
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local FontStyleHelper = GameDataHelper.FontStyleHelper
local UIParticle = ClassTypes.UIParticle

function AionTowerView:Awake()
    self._containerLayerInfo = self:FindChildGO("Container_Right/Container_LayerInfo")
    self._textFirstRewardFloor = self:GetChildComponent("Container_Right/Container_PassInfo/Text_Floor", "TextMeshWrapper")
    self._textFloor = self:GetChildComponent("Container_Right/Container_LayerInfo/Text_Floor", "TextMeshWrapper")
    -- self._textPowerValue = self:GetChildComponent("Container_Right/Container_LayerInfo/Text_PowerValue", "TextMeshWrapper")
    self._btnChallenge = self:FindChildGO("Container_Right/Container_LayerInfo/Button_Challenge")
    self._textOpenNew = self:GetChildComponent("Container_Right/Container_PassInfo/Text_Open_New", "TextMeshWrapper")
    self._textUnLock = self:GetChildComponent("Container_Right/Container_PassInfo/Text_UnLock", "TextMeshWrapper")
    self._csBH:AddClick(self._btnChallenge, function()self:ChallengeBtnClick() end)

    self._fx_Container = self:FindChildGO("Container_Fx")
    self._fx_ui_30010 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30010")
    self._fx_ui_30010:Stop()

    self:InitTowerList()
    self:InitLayerInfoRewardsList()
    self:InitPassRewardList()

    self._leftBgIconObj = self:FindChildGO("Container_Tower/Container_LeftBgIcon")
    GameWorld.AddIcon(self._leftBgIconObj,13553)

    self._rightBgIconObj = self:FindChildGO("Container_Tower/Container_RightBgIcon")
    GameWorld.AddIcon(self._rightBgIconObj,13553)
end

function AionTowerView:ShowView()
    self._scroGo:SetActive(true)
    self:ShowTowerInfo()
    self._fx_ui_30010:Play(true, true)
end

function AionTowerView:CloseView()
    if self._fx_ui_30010 then
        self._fx_ui_30010:Stop()
    end
    self._scroGo:SetActive(false)
end

function AionTowerView:InitPassRewardList()
    local passRewardsScrollView, passRewardsList = UIList.AddScrollViewList(self.gameObject, "Container_Right/Container_PassInfo/Container_Content/ScrollViewList")
    self._passRewardsList = passRewardsList
    self._passRewardsScrollView = passRewardsScrollView
    self._passRewardsScrollView:SetHorizontalMove(true)
    self._passRewardsScrollView:SetVerticalMove(false)
    self._passRewardsList:SetItemType(AionPassRewardItem)
    self._passRewardsList:SetPadding(0, 0, 0, 0)
    self._passRewardsList:SetGap(0, 0)
    self._passRewardsList:SetDirection(UIList.DirectionLeftToRight, 100, 1)
end

function AionTowerView:InitTowerList()
    self._scroGo = self:FindChildGO("Container_Tower/ScrollViewList")
    local towerScrollView, towerList = UIList.AddScrollViewList(self.gameObject, "Container_Tower/ScrollViewList")
    self._towerList = towerList
    self._towerScrollView = towerScrollView
    self._towerScrollView:SetHorizontalMove(false)
    self._towerScrollView:SetVerticalMove(false)
    self._towerList:SetItemType(TowerLevelItem)
    self._towerList:SetPadding(0, 0, 0, 0)
    self._towerList:SetGap(-50, 0)
    self._towerList:SetDirection(UIList.DirectionTopToDown, 1, 100)
end

function AionTowerView:InitLayerInfoRewardsList()
    local rewardsScrollView, rewardsList = UIList.AddScrollViewList(self.gameObject, "Container_Right/Container_LayerInfo/Container_Rewards/ScrollViewList")
    self._layerRewardsList = rewardsList
    self._layerRewardsScrollView = rewardsScrollView
    self._layerRewardsScrollView:SetHorizontalMove(true)
    self._layerRewardsScrollView:SetVerticalMove(false)
    self._layerRewardsList:SetItemType(RewardItem)
    self._layerRewardsList:SetPadding(0, 0, 0, 0)
    self._layerRewardsList:SetGap(0, 0)
    self._layerRewardsList:SetDirection(UIList.DirectionLeftToRight, 100, 1)
end

function AionTowerView:ShowTowerInfo()
    self:ShowLayerInfo()
end

function AionTowerView:ShowLayerInfo()
    local aionInfo = AionTowerManager:GetAionInfo()
    local curLayer = AionTowerManager:GetNowPos()--aionInfo[public_config.AION_NOW_LAYER] or 1

    
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = curLayer
    self._textFloor.text = LanguageDataHelper.CreateContent(55004, argsTable)
    local nowFight = GameWorld.Player().fight_force
    local needFight = AionDataHelper:GetScorePropese(curLayer)
    -- self._textPowerValue.text = needFight
    -- if nowFight >= needFight then
    --     self._textPowerValue.color = FontStyleHelper:GetFontColor(102)
    -- else
    --     self._textPowerValue.color = FontStyleHelper:GetFontColor(106)
    -- end

    --LoggerHelper.Error("任务奖励=====" .. PrintTable:TableToStr(AionDataHelper:GetRewardShow(curLayer)))
    self._layerRewardsList:SetDataList(AionDataHelper:GetRewardShow(curLayer))
    self:SetTowerData(curLayer)
    
    local curFirstPassReward = AionTowerManager:GetNextGetRewardLayer(curLayer)
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = curFirstPassReward
    self._textFirstRewardFloor.text = LanguageDataHelper.CreateContent(55001, argsTable)

    local targetReward = AionDataHelper:GetTargetReward(curFirstPassReward)
    local targetRewardDatas = {}
    if targetReward[AionConfig.AION_UNLOCK_RUNE] ~= nil then--解锁符文槽
        local data = {}
        data.type = AionConfig.AION_UNLOCK_RUNE
        table.insert(targetRewardDatas, data)
        self._textOpenNew.gameObject:SetActive(true)
    else
        self._textOpenNew.gameObject:SetActive(false) 
    end

    if targetReward[AionConfig.AION_UNLOCK_NEW] ~= nil then--解锁新符文
        for i,v in ipairs(targetReward[AionConfig.AION_UNLOCK_NEW]) do
            local data = {}
            data.type = AionConfig.AION_UNLOCK_NEW
            data.id = v
            table.insert(targetRewardDatas, data)
        end
        self._textUnLock.gameObject:SetActive(true)
    else
        self._textUnLock.gameObject:SetActive(false)       
    end

    self._passRewardsList:SetDataList(targetRewardDatas)
end

function AionTowerView:SetTowerData(curLayer)
    local allId = AionDataHelper:GetAllId()
    local allIdCount = #allId
    local allIdIndex = allIdCount
    local result = {}
    for i = 1, allIdCount do
        local state = AionConfig.LOCK
        if allId[allIdIndex] < curLayer then
            state = AionConfig.UNLOCK
        elseif allId[allIdIndex] == curLayer then
            state = AionConfig.CURRENT
        end
        table.insert(result, {id = allId[allIdIndex], state = state})
        allIdIndex = allIdIndex - 1
    end

    self._towerList:SetOnAllItemCreatedCB(function() 
        self._towerList:SetPositionByNum(allIdCount - (curLayer + 2))
        if curLayer ~= 1 then
            self._towerList:GetItem(allIdCount - curLayer + 1):Hide()
        end
    end)
    self._towerList:SetDataList(result,4)
end

function AionTowerView:ChallengeBtnClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_aion_instance_battle_button")
    AionTowerManager:OnAionEnter()
end

function AionTowerView:OnDestroy()
    self._towerList = nil
    self._towerScrollView = nil
end
