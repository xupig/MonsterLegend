-- IdentifyRewardItem.lua
local IdentifyRewardItem = Class.IdentifyRewardItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
IdentifyRewardItem.interface = GameConfig.ComponentsConfig.Component
IdentifyRewardItem.classPath = "Modules.ModuleIdentify.ChildComponent.IdentifyRewardItem"
local OpenCardRewardDataHelper = GameDataHelper.OpenCardRewardDataHelper

local UIList = ClassTypes.UIList
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function IdentifyRewardItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function IdentifyRewardItem:OnDestroy() 

end

--override
function IdentifyRewardItem:OnRefreshData(data)
    if data then
        self._itemIcon:SetItem(data[1], data[2][1], 0, true)
        self._itemIcon:ActivateTipsById()
    end
end