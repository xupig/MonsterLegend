﻿local ClassTypes = ClassTypes
local IdentifyPanel = Class.IdentifyPanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local GUIManager = GameManager.GUIManager

local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local OpenCardRewardDataHelper = GameDataHelper.OpenCardRewardDataHelper
local CardLuckManager = PlayerManager.CardLuckManager
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local WholePeopleJianbaoDataHelper = GameDataHelper.WholePeopleJianbaoDataHelper
local PromisImportantDataHelper = GameDataHelper.PromisImportantDataHelper
local IdentifyManager = PlayerManager.IdentifyManager
local TimerHeap = GameWorld.TimerHeap
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local DateTimeUtil = GameUtil.DateTimeUtil
local ActorModelComponent = GameMain.ActorModelComponent
local UIParticle = ClassTypes.UIParticle
local SettingType = GameConfig.SettingType
local TransformDataHelper = GameDataHelper.TransformDataHelper

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

require "Modules.ModuleIdentify.ChildComponent.IdentifyRewardItem"
local IdentifyRewardItem = ClassTypes.IdentifyRewardItem

-- require "Modules.ModuleCardLuck.ChildComponent.IdentifyPanel"
-- local IdentifyPanel = ClassTypes.IdentifyPanel


function IdentifyPanel:Awake()
    -- self._csBH = self:GetComponent('LuaUIPanel')
     self:InitCom()
     self:InitView()
     
     self._happyWishResp = function()self:RefreshView() end
    -- self._toggleInit = function()self:ToggeleStateInit() end

   
end

function IdentifyPanel:InitCom()
    self._btnClose = self:FindChildGO("Container_Total/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:OnCloseWishClick() end)

    self._btnWish = self:FindChildGO("Container_Wish/Button_Wish")
    self._csBH:AddClick(self._btnWish, function()self:OnWishClick() end)

    self._btnAutoWish = self:FindChildGO("Container_Wish/Button_AutoWish")
    self._csBH:AddClick(self._btnAutoWish, function()self:OnAutoWishClick() end)

    self._btnStopWish = self:FindChildGO("Container_Wish/Button_StopWish")
    self._csBH:AddClick(self._btnStopWish, function()self:OnStopWishClick() end)


    self._btnWishWrapper = self:GetChildComponent("Container_Wish/Button_Wish", "ButtonWrapper")
    self._btnAutoWishWrapper = self:GetChildComponent("Container_Wish/Button_AutoWish", "ButtonWrapper")
    self._btnStopWishWrapper = self:GetChildComponent("Container_Wish/Button_StopWish", "ButtonWrapper")
    
    self._txtWishProgress = self:GetChildComponent("Container_Wish/Text_Percent", 'TextMeshWrapper')

    self._txtWishTime = self:GetChildComponent("Container_Wish/Text_Time", 'TextMeshWrapper')

    self._itemContainer = self:FindChildGO("Container_Wish/Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)

    self._itemBigContainer = self:FindChildGO("Container_Wish/Container_Icon_Big")

    self._leftModelGO = self:FindChildGO('Container_Wish/Container_Model')
    self._leftModelComponent = self:AddChildComponent('Container_Wish/Container_Model', ActorModelComponent)

    self._fx1 = UIParticle.AddParticle(self.gameObject, "Container_Wish/Container_Fx/FirstCharge")
    self._fx1:Play(true)
    self._btnStopWish:SetActive(false)
    self._btnAutoWish:SetActive(true)

    self._wishBar = UIScaleProgressBar.AddProgressBar(self.gameObject, "Container_Wish/ProgressBar")
    self._wishBar:SetProgress(0)
    self._wishBar:ShowTweenAnimate(true)
    self._wishBar:SetMutipleTween(true)
    self._wishBar:SetIsResetToZero(false)
    self._wishBar:SetTweenTime(0.2)

    self._fx_Container = self:FindChildGO("Container_Wish/Container_Fx_Icon")
    self._fx_ui_30011 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30011")
    self:StopFx()
end

function IdentifyPanel:PlayFx()
    self._fx_ui_30011:Play(true, true)
end

function IdentifyPanel:StopFx()
    self._fx_ui_30011:Stop()
end

function IdentifyPanel:OnWishClick()
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = GlobalParamsHelper.GetParamValue(892)
    GUIManager.ShowDailyTips(SettingType.IDENTIFY_NEED, function() IdentifyManager:SendIdentifyReq() end, function() self:OnStopWishClick() end,LanguageDataHelper.CreateContent(81588,argsTable), true) 
end

function IdentifyPanel:OnAutoWishClick()
    if GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS]]-GlobalParamsHelper.GetParamValue(892) >=0 then
        self._inAuto = true
        self._btnStopWish:SetActive(true)
        self._btnAutoWish:SetActive(false)
        self:OnWishClick()
    else 
        self:OnStopWishClick()
        self:OnWishClick()
    end
end

function IdentifyPanel:OnStopWishClick()
    if self._inAuto then
        self._inAuto = false
        TimerHeap:DelTimer(self._autoTimer)
        self._btnStopWish:SetActive(false)
        self._btnAutoWish:SetActive(true)
    end
end


function IdentifyPanel:OnCloseTicketClick()
    --CardLuckManager:SendOpenCardReq(self._curIndex+1)
end

function IdentifyPanel:ToggeleStateInit()
   -- self._toggleIntrest:
end


function IdentifyPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.IdentifyResp, self._happyWishResp)
end

function IdentifyPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.IdentifyResp, self._happyWishResp)
end




function IdentifyPanel:Start()

end

function IdentifyPanel:UpdateFunOpen()


end

function IdentifyPanel:OnCloseWishClick()
    GUIManager.ClosePanel(PanelsConfig.Identify)
end

function IdentifyPanel:OnClose()
    self._iconBg = nil   
    self:OnStopWishClick()
    self:RemoveEventListeners()
end

function IdentifyPanel:RefreshView()
    local wishInfo = IdentifyManager:GetIdentifyInfo()
    local displayId = IdentifyManager:GetNeedId()
    local max = GlobalParamsHelper.GetParamValue(891)
    local identifyInfo = IdentifyManager:GetIdentifyInfo()

    self._wishBar:SetProgress(identifyInfo[public_config.JIANBAO_LUCK]/max)
    self._txtWishProgress.text = identifyInfo[public_config.JIANBAO_LUCK].."/"..max

    --LoggerHelper.Error("displayId:"..displayId)
    local importantIconId =  WholePeopleJianbaoDataHelper:GetImportant(displayId)

    local turnTime =  WholePeopleJianbaoDataHelper:GetTurn(displayId)


    if identifyInfo[public_config.JIANBAO_LOOP_FLAG] > 0 then
        self._itemIcon:SetItem(importantIconId[1], 0, 0, true)
    else
        self._itemIcon:SetItem(importantIconId[2], 0, 0, true)
    end
    self._itemIcon:ActivateTipsById()



    local iconList = {}
    if identifyInfo[public_config.JIANBAO_LOOP_FLAG] > 0 then
        iconList = WholePeopleJianbaoDataHelper:GetDislpayFirst(displayId)
    else
        iconList = WholePeopleJianbaoDataHelper:GetDislpayTurn(displayId)
    end


    if iconList[1] == 1 then
        self._leftModelGO:SetActive(true)
        self._itemBigContainer:SetActive(false)
        local partnerTransformCfg = TransformDataHelper.GetTreasureTotalCfg(iconList[2])
        local vocationGroup = math.floor(GameWorld.Player().vocation/100)
        local uiModelCfgId = partnerTransformCfg.model_vocation[vocationGroup]
        local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
        local modelId = modelInfo.model_ui
        local posx = modelInfo.pos[1]
        local posy = modelInfo.pos[2]
        local posz = modelInfo.pos[3]
        local rotationx = modelInfo.rotation[1]
        local rotationy = modelInfo.rotation[2]
        local rotationz = modelInfo.rotation[3]
        local scale = modelInfo.scale
        self._leftModelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
        self._leftModelComponent:LoadModel(modelId)
    else
        self._leftModelGO:SetActive(false)
        self._itemBigContainer:SetActive(true)
        GameWorld.AddIcon(self._itemBigContainer,iconList[2])
    end

    if GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS]]-GlobalParamsHelper.GetParamValue(892) >=0 then
        if self._inAuto then
            if tonumber(identifyInfo[public_config.JIANBAO_LUCK])  == 0 then
                self:OnStopWishClick()
            else
                if self._autoTimer then
                    TimerHeap:DelTimer(self._autoTimer)
                    self._autoTimer = nil
                end
                self._autoTimer = TimerHeap:AddSecTimer(0.5,0,0,function ()
                    self:OnWishClick()
                end)
            end
        end
    else 
        if self._inAuto~=nil then
            self:OnWishClick()
        end
        self:OnStopWishClick()
    end
end



function IdentifyPanel:InitView()
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_Wish/ScrollViewList_Item")
    self._cardList = list
    self._cardView = scrollview
    self._cardList:SetItemType(IdentifyRewardItem)
    self._cardList:SetPadding(5, 0, 0, 0)
    self._cardList:SetGap(15, 50)
    self._cardList:SetDirection(UIList.DirectionLeftToRight, 5, 100)
    local itemBTClickedCB =
        function(index)
            self:OnCardItemBTClicked(index)
        end
    self._cardList:SetItemClickedCB(itemBTClickedCB)

end

function IdentifyPanel:OnItemBTClicked(index)

end

function IdentifyPanel:OnCardItemBTClicked(index)

end



function IdentifyPanel:GuideGift(funId)

end




--override
function IdentifyPanel:OnDestroy()
    self._fx_ui_30011:Stop()
end

--override
function IdentifyPanel:OnShow(data)
    self._iconBg = self:FindChildGO("Container_Total/Image")
    GameWorld.AddIcon(self._iconBg,13512)
    -- self:RefreshView()
    self:AddEventListeners()
    self:PlayFx()
    local displayId = IdentifyManager:GetNeedId()
    local itemList = WholePeopleJianbaoDataHelper:GetReward(displayId)
    local showList = {}
    for i=2,#itemList do
        table.insert( showList, itemList[i])
    end
    self._cardList:SetDataList(showList)

    local leftTime = IdentifyManager:GetLeftTime()
    if leftTime > 0 then
        if self._timer then
            TimerHeap:DelTimer(self._timer)
            self._timer = nil
        end
        self:UpdateTimeLeft()
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnBackToActivityTime() end)
    else
        EventDispatcher:TriggerEvent(GameEvents.UpdateIdentify)
        self._txtWishTime.text = LanguageDataHelper.CreateContent(76951)
        -- self._btnWishWrapper.interactable = false
        -- self._btnAutoWishWrapper.interactable = false
        -- self._btnStopWishWrapper.interactable = false
        self._wishBar:SetProgress(0)
    end

  
    -- local wishEndInfo = IdentifyManager:GetWishEnd()
    -- if tonumber(wishEndInfo) == 1 then
    --     self._btnWishWrapper.interactable = false
    --     self._btnAutoWishWrapper.interactable = false
    --     self._btnStopWishWrapper.interactable = false
    --     self._wishBar:SetProgress(0)
    -- end
        self:RefreshView()
    --LoggerHelper.Error("IdentifyManager:OnPlayerEnterWorld()"..PrintTable:TableToStr(itemList))
end

function IdentifyPanel:SetData(data)
    --self:RefreshView()
end

function IdentifyPanel:ShowView(index, data)

end

function IdentifyPanel:UpdateTimeLeft()
    self._leftTime = IdentifyManager:GetLeftTime()
    local argsTableRank = LanguageDataHelper.GetArgsTable()
    argsTableRank["0"] = DateTimeUtil.FormatParseTimeStr(self._leftTime%(24*60*60))
    if self._leftTime <= 24*60*60 then
        self._txtWishTime.text = LanguageDataHelper.CreateContent(81653, argsTableRank)
    else
        self._txtWishTime.text = LanguageDataHelper.CreateContent(81652, argsTableRank)
    end
    
end

function IdentifyPanel:OnBackToActivityTime()
    if (self._leftTime > 0) then
        self:UpdateTimeLeft()
    else
        EventDispatcher:TriggerEvent(GameEvents.UpdateIdentify)
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
        self._txtWishTime.text = LanguageDataHelper.CreateContent(76951)
        -- self._btnWishWrapper.interactable = false
        -- self._btnAutoWishWrapper.interactable = false
        -- self._btnStopWishWrapper.interactable = false
        self._wishBar:SetProgress(0)
    end
end

function IdentifyPanel:BaseShowModel()
    self._leftModelGO:SetActive(true)
end

function IdentifyPanel:BaseHideModel()
    self._leftModelGO:SetActive(false)
end