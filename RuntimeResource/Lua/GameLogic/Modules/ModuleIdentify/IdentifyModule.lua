--CardLuckModule.lua
IdentifyModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function IdentifyModule.Init()
	GUIManager.AddPanel(PanelsConfig.Identify,"Panel_Identify",GUILayer.LayerUIPanel,"Modules.ModuleIdentify.IdentifyPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return IdentifyModule
