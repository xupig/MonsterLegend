-- PointingArrowPanel.lua
local PointingArrowPanel = Class.PointingArrowPanel(ClassTypes.BasePanel)

local ComponentsConfig = GameConfig.ComponentsConfig

require "Modules.ModuleBillboard.ChildView.PointingArrowView"
local PointingArrowView = ClassTypes.PointingArrowView

function PointingArrowPanel:Awake()
    self:AddChildLuaUIComponent("Container_PointingArrow", PointingArrowView)
end

function PointingArrowPanel:OnDestroy() 
end