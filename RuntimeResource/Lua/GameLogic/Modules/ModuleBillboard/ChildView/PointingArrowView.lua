-- PointingArrowView.lua
local PointingArrowView = Class.PointingArrowView(ClassTypes.BaseLuaUIComponent)

PointingArrowView.interface = GameConfig.ComponentsConfig.Component
PointingArrowView.classPath = "Modules.ModuleBillboard.ChildView.PointingArrowView"

require "Modules.ModuleBillboard.ChildComponent.PointingArrowItem"
require "Modules.ModuleBillboard.ChildComponent.EntityPointingArrowItem"

local PointerInputModule = UnityEngine.EventSystems.PointerInputModule
local GameObject = UnityEngine.GameObject
local UIComponentUtil = GameUtil.UIComponentUtil
local PosPointingArrow = GameMain.PosPointingArrow
local PointingArrowItem = ClassTypes.PointingArrowItem
local EntityPointingArrow = GameMain.EntityPointingArrow
local EntityPointingArrowItem = ClassTypes.EntityPointingArrowItem

function PointingArrowView:Awake()
    self:InitTemplate()
    self:InitItemList()
    self:AddListeners()
end

function PointingArrowView:InitTemplate()
    self._templateObj = self:FindChildGO("Container_Template")
    self._templateObj:SetActive(false)
end

function PointingArrowView:InitItemList()
    self._listTrans = self.transform:Find("Container_List")
    self._posItemPool = {}
    self._posItems = {}
    self._entityItemPool = {}
    self._entityItems = {}
end

function PointingArrowView:AddListeners()
    self._addPointingArrowCB = function(id, iconId, pos)self:OnAddPointingArrow(id, iconId, pos) end
    EventDispatcher:AddEventListener(GameEvents.ADD_POINTING_ARROW, self._addPointingArrowCB)
    self._removePointingArrowCB = function(id)self:OnRemovePointingArrow(id) end
    EventDispatcher:AddEventListener(GameEvents.REMOVE_POINTING_ARROW, self._removePointingArrowCB)
    self._addEntityPointingArrowCB = function(entityId, iconId, addtiveHeight)self:OnAddEntityPointingArrow(entityId, iconId, addtiveHeight) end
    EventDispatcher:AddEventListener(GameEvents.ADD_ENTITY_POINTING_ARROW, self._addEntityPointingArrowCB)
    self._removeEntityPointingArrowCB = function(entityId)self:OnRemoveEntityPointingArrow(entityId) end
    EventDispatcher:AddEventListener(GameEvents.REMOVE_ENTITY_POINTING_ARROW, self._removeEntityPointingArrowCB)
end

function PointingArrowView:OnDestroy()
    self._templateObj = nil
    self._listTrans = nil
    self:RemoveListeners()
end

function PointingArrowView:RemoveListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ADD_POINTING_ARROW, self._addPointingArrowCB)
    EventDispatcher:RemoveEventListener(GameEvents.REMOVE_POINTING_ARROW, self._removePointingArrowCB)
    EventDispatcher:RemoveEventListener(GameEvents.ADD_ENTITY_POINTING_ARROW, self._addEntityPointingArrowCB)
    EventDispatcher:RemoveEventListener(GameEvents.REMOVE_ENTITY_POINTING_ARROW, self._removeEntityPointingArrowCB)
end

function PointingArrowView:CreatePointingArrowItem()
    local itemPool = self._posItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = GameObject.Instantiate(self._templateObj)
    go.transform:SetParent(self._listTrans, false);
    go.gameObject:SetActive(true)
    
    go:AddComponent(typeof(PosPointingArrow))
    local item = UIComponentUtil.AddLuaUIComponent(go, PointingArrowItem)
    return item
end

function PointingArrowView:ReleasePointingArrowItem(item)
    local itemPool = self._posItemPool
    table.insert(itemPool, item)
end

function PointingArrowView:OnAddPointingArrow(id, iconId, pos)
    local item = self:CreatePointingArrowItem()
    self._posItems[id] = item
    item:SetIconAndPos(iconId, pos)
end

function PointingArrowView:OnRemovePointingArrow(id)
    local item = self._posItems[id]
    if item ~= nil then
        item:Hide()
        self:ReleasePointingArrowItem(item)
        self._posItems[id] = nil
    end
end

function PointingArrowView:CreateEntityPointingArrowItem()
    local itemPool = self._entityItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = GameObject.Instantiate(self._templateObj)
    go.transform:SetParent(self._listTrans, false);
    go.gameObject:SetActive(true)
    
    go:AddComponent(typeof(EntityPointingArrow))
    local item = UIComponentUtil.AddLuaUIComponent(go, EntityPointingArrowItem)
    return item
end

function PointingArrowView:ReleaseEntityPointingArrowItem(item)
    local itemPool = self._entityItemPool
    table.insert(itemPool, item)
end

function PointingArrowView:OnAddEntityPointingArrow(entityId, iconId, addtiveHeight)
    local item = self:CreateEntityPointingArrowItem()
    self._entityItems[entityId] = item
    item:SetEntityId(entityId, iconId, addtiveHeight)
end

function PointingArrowView:OnRemoveEntityPointingArrow(entityId)
    local item = self._entityItems[entityId]
    if item ~= nil then
        item:Hide()
        self:ReleaseEntityPointingArrowItem(item)
        self._entityItems[entityId] = nil
    end
end
