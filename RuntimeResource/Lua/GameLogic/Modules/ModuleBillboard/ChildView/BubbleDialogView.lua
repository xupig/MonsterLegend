-- BubbleDialogView.lua
local BubbleDialogView = Class.BubbleDialogView(ClassTypes.BaseLuaUIComponent)

BubbleDialogView.interface = GameConfig.ComponentsConfig.Component
BubbleDialogView.classPath = "Modules.ModuleBillboard.ChildView.BubbleDialogView"

require "Modules.ModuleBillboard.ChildComponent.BubbleDialogItem"
require "Modules.ModuleBillboard.ChildComponent.EntityBubbleDialogItem"

local PointerInputModule = UnityEngine.EventSystems.PointerInputModule
local GameObject = UnityEngine.GameObject
local UIComponentUtil = GameUtil.UIComponentUtil
local PosBubbleDialog = GameMain.PosBubbleDialog
local BubbleDialogItem = ClassTypes.BubbleDialogItem
local EntityBubbleDialog = GameMain.EntityBubbleDialog
local EntityBubbleDialogItem = ClassTypes.EntityBubbleDialogItem

function BubbleDialogView:Awake()
    self:InitTemplate()
    self:InitItemList()
    self:AddListeners()
end

function BubbleDialogView:InitTemplate()
    self._templateObj = self:FindChildGO("Container_Template")
    self._templateObj:SetActive(false)
end

function BubbleDialogView:InitItemList()
    self._listTrans = self.transform:Find("Container_List")
    self._posItemPool = {}
    self._posItems = {}
    self._entityItemPool = {}
    self._entityItems = {}
end

function BubbleDialogView:AddListeners()
    self._addBubbleDialogCB = function(id, textId, pos)self:OnAddBubbleDialog(id, textId, pos) end
    EventDispatcher:AddEventListener(GameEvents.ADD_BUBBLE_DIALOG, self._addBubbleDialogCB)
    self._removeBubbleDialogCB = function(id)self:OnRemoveBubbleDialog(id) end
    EventDispatcher:AddEventListener(GameEvents.REMOVE_BUBBLE_DIALOG, self._removeBubbleDialogCB)
    self._addEntityBubbleDialogCB = function(entityId, textId)self:OnAddEntityBubbleDialog(entityId, textId) end
    EventDispatcher:AddEventListener(GameEvents.ADD_ENTITY_BUBBLE_DIALOG, self._addEntityBubbleDialogCB)
    self._removeEntityBubbleDialogCB = function(entityId)self:OnRemoveEntityBubbleDialog(entityId) end
    EventDispatcher:AddEventListener(GameEvents.REMOVE_ENTITY_BUBBLE_DIALOG, self._removeEntityBubbleDialogCB)
end

function BubbleDialogView:OnDestroy()
    self._templateObj = nil
    self._listTrans = nil
    self:RemoveListeners()
end

function BubbleDialogView:RemoveListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ADD_BUBBLE_DIALOG, self._addBubbleDialogCB)
    EventDispatcher:RemoveEventListener(GameEvents.REMOVE_BUBBLE_DIALOG, self._removeBubbleDialogCB)
    EventDispatcher:RemoveEventListener(GameEvents.ADD_ENTITY_BUBBLE_DIALOG, self._addEntityBubbleDialogCB)
    EventDispatcher:RemoveEventListener(GameEvents.REMOVE_ENTITY_BUBBLE_DIALOG, self._removeEntityBubbleDialogCB)
end

function BubbleDialogView:CreateBubbleDialogItem()
    local itemPool = self._posItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = GameObject.Instantiate(self._templateObj)
    go.transform:SetParent(self._listTrans, false);
    go.gameObject:SetActive(true)
    
    go:AddComponent(typeof(PosBubbleDialog))
    local item = UIComponentUtil.AddLuaUIComponent(go, BubbleDialogItem)
    return item
end

function BubbleDialogView:ReleaseBubbleDialogItem(item)
    local itemPool = self._posItemPool
    table.insert(itemPool, item)
end

function BubbleDialogView:OnAddBubbleDialog(id, textId, pos)
    --LoggerHelper.Log("Ash: OnAddBubbleDialog: " .. id .. " " .. textId .. " " .. PrintTable:TableToStr(pos))
    local item = self:CreateBubbleDialogItem()
    self._posItems[id] = item
    item:SetTextAndPos(textId, pos)
end

function BubbleDialogView:OnRemoveBubbleDialog(id)
    local item = self._posItems[id]
    if item ~= nil then
        item:Hide()
        self:ReleaseBubbleDialogItem(item)
        self._posItems[id] = nil
    end
end

function BubbleDialogView:CreateEntityBubbleDialogItem()
    local itemPool = self._entityItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = GameObject.Instantiate(self._templateObj)
    go.transform:SetParent(self._listTrans, false);
    go.gameObject:SetActive(true)
    
    go:AddComponent(typeof(EntityBubbleDialog))
    local item = UIComponentUtil.AddLuaUIComponent(go, EntityBubbleDialogItem)
    return item
end

function BubbleDialogView:ReleaseEntityBubbleDialogItem(item)
    local itemPool = self._entityItemPool
    table.insert(itemPool, item)
end

function BubbleDialogView:OnAddEntityBubbleDialog(entityId, textId)
    --LoggerHelper.Log("Ash: OnAddEntityBubbleDialog: " .. entityId .. " " .. textId)
    local item = self:CreateEntityBubbleDialogItem()
    self._entityItems[entityId] = item
    item:SetEntityId(entityId, textId, 1)
end

function BubbleDialogView:OnRemoveEntityBubbleDialog(entityId)
    local item = self._entityItems[entityId]
    if item ~= nil then
        item:Hide()
        self:ReleaseEntityBubbleDialogItem(item)
        self._entityItems[entityId] = nil
    end
end
