-- CreatureBillboardView.lua
local CreatureBillboardView = Class.CreatureBillboardView(ClassTypes.BaseLuaUIComponent)

CreatureBillboardView.interface = GameConfig.ComponentsConfig.Component
CreatureBillboardView.classPath = "Modules.ModuleBillboard.ChildView.CreatureBillboardView"

require "Modules.ModuleBillboard.ChildComponent.CreatureBillboardItem"
require "Modules.ModuleBillboard.ChildComponent.MonsterBillboardItem"
require "Modules.ModuleBillboard.ChildComponent.NpcBillboardItem"
require "Modules.ModuleBillboard.ChildComponent.SpaceDropBillboardItem"


local GameObject = UnityEngine.GameObject
local UIComponentUtil = GameUtil.UIComponentUtil
local EntityBillboard = GameMain.EntityBillboard
local CreatureBillboardItem = ClassTypes.CreatureBillboardItem
local MonsterBillboardItem = ClassTypes.MonsterBillboardItem
local NpcBillboardItem = ClassTypes.NpcBillboardItem
local SpaceDropBillboardItem = ClassTypes.SpaceDropBillboardItem
local EntityType = GameConfig.EnumType.EntityType
local Time = Time
local _Abs = math.abs

-- 对象池key
local Key_Monster = "Monster"
local Key_SpaceDrop = "SpaceDrop"
local Key_NPC = "NPC"
local Key_Default = "Default"
-- 实体类型所使用的对象池key
local EntityTypeKey = {
    [EntityType.Monster] = Key_Monster,
    [EntityType.Dummy] = Key_Monster,
    [EntityType.SpaceDrop] = Key_SpaceDrop,
    [EntityType.Collect] = Key_SpaceDrop,
    [EntityType.NPC] = Key_NPC,
}
local CHECK_POOL_TIME = 25 -- 检查时间
local CLEAR_PER_NUM = 1000 -- 每次清除数量
local DEACTIVE_PER_NUM = 5 -- 每次隐藏数量
local POOL_RESERVE_NUM = { -- 保留数量
    [Key_Monster] = 30,
    [Key_SpaceDrop] = 25,
    [Key_Default] = 10,
    [Key_NPC] = 25,
}

function CreatureBillboardView:Awake()
    self._checkIndex = 0
    self:InitTemplate()
    self:InitItemList()
    self:AddListeners()
end

function CreatureBillboardView:OnEnable()
    self:AddPoolTimer()
end

function CreatureBillboardView:OnDisable()
    self:RemovePoolTimer()
end


function CreatureBillboardView:InitTemplate()
    self._templateObjects = {
        [Key_Monster] = self:FindChildGO("Template_Monster"),
        [Key_SpaceDrop] = self:FindChildGO("Template_Drop"),
        [Key_Default] = self:FindChildGO("Template_Default"),
        [Key_NPC] = self:FindChildGO("Template_NPC"),
    }
end

function CreatureBillboardView:InitItemList()
    self._listTrans = self.transform:Find("Container_List")
    self._entityItems = {}
    -- 对象池按类型区分
    self._itemPool = {
        [Key_Monster] = {},
        [Key_SpaceDrop] = {},
        [Key_Default] = {},
        [Key_NPC] = {},
    }
end

function CreatureBillboardView:AddListeners()
    self._addCreatureBillboardCB = function(entid, entityType)self:OnAddCreatureBillboard(entid, entityType) end
    EventDispatcher:AddEventListener(GameEvents.ADD_CREATURE_BILLBOARD, self._addCreatureBillboardCB)
    self._removeCreatureBillboardCB = function(entid, entityType)self:OnRemoveCreatureBillboard(entid, entityType) end
    EventDispatcher:AddEventListener(GameEvents.REMOVE_CREATURE_BILLBOARD, self._removeCreatureBillboardCB)
    self._hideCreatureBillboard = function(entid)self:OnHideCreatureBillboard(entid) end
    EventDispatcher:AddEventListener(GameEvents.HIDE_CREATURE_BILLBOARD, self._hideCreatureBillboard)
    self._displayCreatureBillboard = function(entid)self:OnDisplayCreatureBillboard(entid) end
    EventDispatcher:AddEventListener(GameEvents.DISPLAY_CREATURE_BILLBOARD, self._displayCreatureBillboard)
    self._refreshCreatureBillboard = function(entid)self:OnRefreshCreatureBillboard(entid) end
    EventDispatcher:AddEventListener(GameEvents.REFRESH_CREATURE_BILLBOARD, self._refreshCreatureBillboard)
end


function CreatureBillboardView:OnDestroy()
    self._templateObjects = nil
    self._list = nil
    self:RemoveListeners()
    self:RemovePoolTimer()
    self:ClearItemPool()
end


function CreatureBillboardView:RemoveListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ADD_CREATURE_BILLBOARD, self._addCreatureBillboardCB)
    EventDispatcher:RemoveEventListener(GameEvents.REMOVE_CREATURE_BILLBOARD, self._removeCreatureBillboardCB)
    EventDispatcher:RemoveEventListener(GameEvents.HIDE_CREATURE_BILLBOARD, self._hideCreatureBillboard)
    EventDispatcher:RemoveEventListener(GameEvents.DISPLAY_CREATURE_BILLBOARD, self._displayCreatureBillboard)
    EventDispatcher:RemoveEventListener(GameEvents.REFRESH_CREATURE_BILLBOARD, self._refreshCreatureBillboard)
end


function CreatureBillboardView:CreateCreatureBillboardItem(entityType)
    local key = EntityTypeKey[entityType] or Key_Default
    local itemPool = self._itemPool[key]
    if #itemPool > 0 then
        return table.remove(itemPool, #itemPool)
    end
    local go = GameObject.Instantiate(self._templateObjects[key])
    go.transform:SetParent(self._listTrans, false)
    -- go.gameObject:SetActive(true)
    go:AddComponent(typeof(EntityBillboard))

    local item
    if entityType == EntityType.Monster or entityType == EntityType.Dummy then
        item = UIComponentUtil.AddLuaUIComponent(go, MonsterBillboardItem)
    elseif entityType == EntityType.SpaceDrop or entityType == EntityType.Collect then
        item = UIComponentUtil.AddLuaUIComponent(go, SpaceDropBillboardItem)
    elseif entityType == EntityType.NPC then
        item = UIComponentUtil.AddLuaUIComponent(go, NpcBillboardItem)
    else
        item = UIComponentUtil.AddLuaUIComponent(go, CreatureBillboardItem)
    end
    return item
end

function CreatureBillboardView:ReleaseCreatureBillboardItem(item, entityType)
    local key = EntityTypeKey[entityType] or Key_Default
    item:SetCheckIndex(self._checkIndex)
    table.insert(self._itemPool[key], item)
end

function CreatureBillboardView:OnAddCreatureBillboard(eniID, entityType)
    -- LoggerHelper.Log(" ++ OnAddCreatureBillboard(eniID= ".. eniID .. ", entityType= " .. tostring(entityType))
    local item = self:CreateCreatureBillboardItem(entityType)
    if not item then return end
    item:SetIsActive(true)
    self._entityItems[eniID] = item
    item:Show(eniID)
end

function CreatureBillboardView:OnRemoveCreatureBillboard(eniID, entityType)
    -- LoggerHelper.Log(" -- OnRemoveCreatureBillboardc(eniID= ".. eniID .. ", entityType= " .. tostring(entityType))
    local item = self._entityItems[eniID]
    if item ~= nil then
        item:Close()
        self:ReleaseCreatureBillboardItem(item, entityType)
        self._entityItems[eniID] = nil
    end
end
--隐藏
function CreatureBillboardView:OnHideCreatureBillboard(eniID)
    local item = self._entityItems[eniID]
    if item ~= nil then
        item:Hide()
    end
end
--重新显示
function CreatureBillboardView:OnDisplayCreatureBillboard(eniID)
    local item = self._entityItems[eniID]
    if item ~= nil then
        item:Display()
    end
end

--刷新显示
function CreatureBillboardView:OnRefreshCreatureBillboard(eniID)
    local item = self._entityItems[eniID]
    if item ~= nil then
        item:Show(eniID)
    end
end


--======================== 内存池监控\回收
function CreatureBillboardView:AddPoolTimer()
    self:RemovePoolTimer()
    self._itemPoolTimer = TimerHeap:AddSecTimer(0, CHECK_POOL_TIME, 0, function() self:CheckItemPool() end)
end

function CreatureBillboardView:RemovePoolTimer()
    if self._itemPoolTimer  then
        TimerHeap.DelTimer(self._itemPoolTimer)
        self._itemPoolTimer = nil
        self._checkIndex = 0
    end
end

function CreatureBillboardView:ClearPool()
    while #self._damageGridStack > 0 do
        local obj = table.remove(self._damageGridStack)
        GameWorld.Destroy(obj.gameObject)
    end
    self._damageGridStack = {}
    for k,v in pairs(self._damageItemStackDic) do
        while #v > 0 do
            local obj = table.remove(v)
            GameWorld.Destroy(obj.gameObject)
        end
        v = {}
    end
end

function CreatureBillboardView:CheckItemPool()
    local count = 0
    local reserveNum
    for key, pool in pairs(self._itemPool) do
        reserveNum = POOL_RESERVE_NUM[key]
        if #pool > reserveNum then
            for i = #pool, reserveNum + 1, -1 do
                local item = pool[i]
                pool[i] = nil
                GameWorld.Destroy(item.gameObject)
                -- LoggerHelper.Log("==== Clear key="..key, "#ff0000ff")
 				count = count + 1
				if count > CLEAR_PER_NUM then
                    return
                end
            end
        end
    end

    -- 对一定时间后没使用的对象setActive(false)
    self._checkIndex = (self._checkIndex + 1) % 10000

    local count = 0
    for key, pool in pairs(self._itemPool) do
        for i = 1, #pool do
            local item = pool[i]
            if item:GetIsActive() == true and (_Abs(self._checkIndex - item:GetCheckIndex()) >= 2) then
                -- LoggerHelper.Log("==== SetActive False curIndex = "..self._checkIndex..", index= " .. item:GetCheckIndex() .. ", key="..key, "#ff0000ff")
                item:SetIsActive(false)
                count = count + 1
            end
            if count >= DEACTIVE_PER_NUM then
                return
            end
        end
    end
end

function CreatureBillboardView:ClearItemPool()
    for key, pool in pairs(self._itemPool) do
        for i = #pool, 1, -1 do
            local item = pool[i]
            pool[i] = nil
            GameWorld.Destroy(item.gameObject)
        end
    end
end
--========================