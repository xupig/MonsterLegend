-- EntityPointingArrowItem.lua
local EntityPointingArrowItem = Class.EntityPointingArrowItem(ClassTypes.BaseLuaUIComponent)

EntityPointingArrowItem.interface = GameConfig.ComponentsConfig.Component
EntityPointingArrowItem.classPath = "Modules.ModuleBillboard.ChildComponent.EntityPointingArrowItem"

local EntityPointingArrow = GameMain.EntityPointingArrow

function EntityPointingArrowItem:Awake()
    self._imagePointing = self:FindChildGO("Image_Pointing")
    self._entityPosPointingArrow = self.gameObject:GetComponent(typeof(EntityPointingArrow))

end

function EntityPointingArrowItem:OnDestroy()
    self._imagePointing = nil
    self._entityPosPointingArrow = nil
end

function EntityPointingArrowItem:SetEntityId(entityId, iconId, addtiveHeight)
    GameWorld.AddIcon(self._imagePointing, iconId, nil)
    self._entityPosPointingArrow:Show(entityId, addtiveHeight)
end

function EntityPointingArrowItem:Hide()
    self._entityPosPointingArrow:Hide()
end
