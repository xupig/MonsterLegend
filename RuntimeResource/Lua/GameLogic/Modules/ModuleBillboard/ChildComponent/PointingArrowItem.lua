-- PointingArrowItem.lua
local PointingArrowItem = Class.PointingArrowItem(ClassTypes.BaseLuaUIComponent)

PointingArrowItem.interface = GameConfig.ComponentsConfig.Component
PointingArrowItem.classPath = "Modules.ModuleBillboard.ChildComponent.PointingArrowItem"

local PosPointingArrow = GameMain.PosPointingArrow

function PointingArrowItem:Awake()
    self._imagePointing = self:FindChildGO("Image_Pointing")
    self._posPointingArrow = self.gameObject:GetComponent(typeof(PosPointingArrow))
    
end

function PointingArrowItem:OnDestroy()
    self._imagePointing = nil
    self._posPointingArrow = nil
end

function PointingArrowItem:SetIconAndPos(iconID, pos)
    GameWorld.AddIcon(self._imagePointing, iconID, nil)
    self._posPointingArrow:Show(pos)
end

function PointingArrowItem:Hide()
    self._posPointingArrow:Hide()
end