-- NpcBillboardItem.lua
require "Modules.ModuleBillboard.ChildComponent.BaseBillboardItem"
local NpcBillboardItem = Class.NpcBillboardItem(ClassTypes.BaseBillboardItem)

NpcBillboardItem.interface = GameConfig.ComponentsConfig.Component
NpcBillboardItem.classPath = "Modules.ModuleBillboard.ChildComponent.NpcBillboardItem"

local Image = UnityEngine.UI.Image
local EntityConfig = GameConfig.EntityConfig
local EntityBillboard = GameMain.EntityBillboard
local BillBoardType = GameConfig.EnumType.BillBoardType
local BaseUtil = GameUtil.BaseUtil
local EntityType = GameConfig.EnumType.EntityType
local BillBoardManager = PlayerManager.BillBoardManager

function NpcBillboardItem:Awake()
    self._entityID = 0
    self:InitHeapInfo()
    self:InitBillboard()
    self:InitCallbackFunc()
end

function NpcBillboardItem:Show(eniID)
    self:SetShowType(eniID)
    self:ShowByEntityType(eniID)
    self:OnTitleChange()
end

function NpcBillboardItem:Close()
    self:SetEntityID(0)
end

function NpcBillboardItem:InitHeapInfo()
    local visibleTransform = {
        ["headInfoObj"] = self.transform:Find("Container_HeapInfo").transform,
        ["nameOneGo"] = self:FindChildGO("Container_HeapInfo/Container_NameOne").transform,
        ["titleGo"] = self:FindChildGO("Container_HeapInfo/Container_Title").transform,
    }
    self._visibleTransform = visibleTransform

    local LuaUILayoutElement = typeof(GameMain.LuaUILayoutElement)
    self._layoutElements = {
        ["nameOneGo"] = visibleTransform.nameOneGo.gameObject:AddComponent(LuaUILayoutElement),
        ["titleGo"] = visibleTransform.titleGo.gameObject:AddComponent(LuaUILayoutElement),
    }

    self._textOneName = self:GetChildComponent("Container_HeapInfo/Container_NameOne/Image_BG/Text_Name", "TextMeshWrapper")
    self._honorName = self:GetChildComponent("Container_HeapInfo/Container_Title/Text_Title", "TextMeshWrapper")
    self._visibleTransform["imageOneGo"] = self:FindChildGO("Container_HeapInfo/Container_NameOne/Image_BG").transform
    local image1 = self:GetChildComponent("Container_HeapInfo/Container_NameOne/Image_BG", 'ImageWrapper')
    image1:SetContinuousDimensionDirty(true)

    self._visibleState = {}
    self:SetGOVisible("nameOneGo", false)
    self:SetGOVisible("titleGo", false)
    self:SetGOVisible("imageOneGo", false)
end

--override
function NpcBillboardItem:SetGOVisible(name, state)
    return NpcBillboardItem._base.SetGOVisible(self, name, state)
end

function NpcBillboardItem:InitBillboard()
    self._entityBillboard = self.gameObject:GetComponent(typeof(EntityBillboard))
end

function NpcBillboardItem:InitCallbackFunc()
end

function NpcBillboardItem:OnDestroy()
    self._dropBelongGo = nil
    self._textName = nil
    self._honorName = nil
    self:ClearEntityID()
end

function NpcBillboardItem:ShowByEntityType(eniID)
    local entity = GameWorld.GetEntity(eniID)
    self:ShowHeadInfo(true)
    self:SetColor(eniID)
    if entity == nil then return end
    self:SetName(entity.name)
    self:SetEntityID(eniID)
end


function NpcBillboardItem:ShowHeadInfo(state)
    self:SetGOVisible("headInfoObj", state)
end

function NpcBillboardItem:SetShowType(eniID)
    self.type = BillBoardManager:GetShowType(eniID)
end

function NpcBillboardItem:SetColor(eniID)
    self._honorColor, self._guildColor, self._nameColor = BillBoardManager:GetColor(eniID)
end


function NpcBillboardItem:SetName(value)
    if string.IsNullOrEmpty(value) then
        self:SetGOVisible("imageOneGo", false)
        self:SetGOVisible("nameOneGo", false)
        return
    end
    self:SetGOVisible("imageOneGo", true)
    self:SetGOVisible("nameOneGo", true)
    self._textOneName.text = BaseUtil.GetColorString(value, self._nameColor)
end


function NpcBillboardItem:SetHonor(value)
    if string.IsNullOrEmpty(value) then
        self:SetGOVisible("titleGo", false)
        return
    end
    self:SetGOVisible("titleGo", true)
    if type(value) == "number" then
        self._honorName.text = ""
    else
        self._honorName.text = value
    end
end

function NpcBillboardItem:ClearEntityID()
    if self._entityID > 0 then
        self._entityID = 0
    end
end

function NpcBillboardItem:SetEntityID(eniID)
    self:ClearEntityID()
    self._entityID = eniID
    self._entityBillboard:SetEntityID(eniID, self.type)
    local entity = GameWorld.GetEntity(eniID)
    if entity ~= nil then
        self:OnTitleChange()
    end
end

function NpcBillboardItem:OnTitleChange()
    local entity = GameWorld.GetEntity(self._entityID)
    if entity ~= nil then
        local hide = PlayerManager.SystemSettingManager:GetPlayerSetting(GameConfig.SystemSettingConfig.OTHER_TITLE)
        if entity.entityType == EntityType.Avatar and hide then
            self:SetHonor()
            return
        end

        local title = entity.honorName
        self:SetHonor(title)
    end
end
