-- BaseBillboardItem.lua
local BaseBillboardItem = Class.BaseBillboardItem(ClassTypes.BaseLuaUIComponent)

BaseBillboardItem.interface = GameConfig.ComponentsConfig.Component
BaseBillboardItem.classPath = "Modules.ModuleBillboard.ChildComponent.BaseBillboardItem"

local Scale_Hide = Vector3.New(0, 1, 1)
local Scale_Display = Vector3.one

function BaseBillboardItem:Awake()
    self._isActive = false
    self._checkIndex = 0
end

function BaseBillboardItem:Show(eniID)
end

function BaseBillboardItem:Close()
end

-- function BaseBillboardItem:InitHeapInfo()
-- 	self._visibleTransform = {}
-- 	self._layoutElements = {}
-- 	self._visibleState = {}
-- end

function BaseBillboardItem:SetGOVisible(name, state)
    if state ~= self._visibleState[name] then
        self._visibleState[name] = state
        self._visibleTransform[name].localScale = state and Scale_Display or Scale_Hide
        if self._layoutElements[name] then
            self._layoutElements[name]:SetIgnoreLayout(not state)
        end
    end
end

function BaseBillboardItem:Hide()
    self.localScale = Scale_Hide
end

function BaseBillboardItem:Display()
    self.localScale = Scale_Display
end


function BaseBillboardItem:SetCheckIndex(value)
    self._checkIndex = value
end

function BaseBillboardItem:GetCheckIndex()
    return self._checkIndex
end

function BaseBillboardItem:GetIsActive()
    return self._isActive
end

function BaseBillboardItem:SetIsActive(value)
    if self._isActive ~= value then
        self._isActive = value
        self.gameObject:SetActive(value)        
    end
end
