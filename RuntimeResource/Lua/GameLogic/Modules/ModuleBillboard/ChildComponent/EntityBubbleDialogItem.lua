-- EntityBubbleDialogItem.lua
local EntityBubbleDialogItem = Class.EntityBubbleDialogItem(ClassTypes.BaseLuaUIComponent)

EntityBubbleDialogItem.interface = GameConfig.ComponentsConfig.Component
EntityBubbleDialogItem.classPath = "Modules.ModuleBillboard.ChildComponent.EntityBubbleDialogItem"

local EntityBubbleDialog = GameMain.EntityBubbleDialog
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function EntityBubbleDialogItem:Awake()
    self._textContent = self:GetChildComponent("Container_Content/Text_Content", "TextMeshWrapper")
    self._entityBubbleDialog = self.gameObject:GetComponent(typeof(EntityBubbleDialog))

end

function EntityBubbleDialogItem:OnDestroy()
    self._textContent = nil
    self._entityBubbleDialog = nil
end

function EntityBubbleDialogItem:SetEntityId(entityId, textId, addtiveHeight)
    self._textContent.text = LanguageDataHelper.CreateContent(textId)
    -- LoggerHelper.Log("Ash: EntityBubbleDialogItem SetEntityId: " .. entityId .. " " .. addtiveHeight)
    self._entityBubbleDialog:Show(entityId, addtiveHeight)
end

function EntityBubbleDialogItem:Hide()
    self._entityBubbleDialog:Hide()
end
