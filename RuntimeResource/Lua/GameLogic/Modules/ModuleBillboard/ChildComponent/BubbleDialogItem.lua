-- BubbleDialogItem.lua
local BubbleDialogItem = Class.BubbleDialogItem(ClassTypes.BaseLuaUIComponent)

BubbleDialogItem.interface = GameConfig.ComponentsConfig.Component
BubbleDialogItem.classPath = "Modules.ModuleBillboard.ChildComponent.BubbleDialogItem"

local PosBubbleDialog = GameMain.PosBubbleDialog
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ChineseVoiceDataHelper = GameDataHelper.ChineseVoiceDataHelper

function BubbleDialogItem:Awake()
    self._textContent = self:GetChildComponent("Container_Content/Text_Content", "TextMeshWrapper")
    self._posBubbleDialog = self.gameObject:GetComponent(typeof(PosBubbleDialog))
    
end

function BubbleDialogItem:OnDestroy()
    self._textContent = nil
    self._posBubbleDialog = nil
end

function BubbleDialogItem:SetTextAndPos(textId, pos)
    self._textContent.text = LanguageDataHelper.CreateContent(textId)
    local voice = ChineseVoiceDataHelper:GetVoice(textId)
    if voice ~= nil and voice ~= 0 then
        GameWorld.PlaySound(voice)
    end
    self._posBubbleDialog:Show(pos)
end

function BubbleDialogItem:Hide()
    self._posBubbleDialog:Hide()
end