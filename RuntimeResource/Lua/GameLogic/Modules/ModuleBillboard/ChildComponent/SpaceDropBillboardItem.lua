-- SpaceDropBillboardItem.lua
require "Modules.ModuleBillboard.ChildComponent.BaseBillboardItem"
local SpaceDropBillboardItem = Class.SpaceDropBillboardItem(ClassTypes.BaseBillboardItem)

SpaceDropBillboardItem.interface = GameConfig.ComponentsConfig.Component
SpaceDropBillboardItem.classPath = "Modules.ModuleBillboard.ChildComponent.SpaceDropBillboardItem"

local Image = UnityEngine.UI.Image
local XImageFilling = GameMain.XImageFilling
local EntityConfig = GameConfig.EntityConfig
local EntityBillboard = GameMain.EntityBillboard
local BillBoardType = GameConfig.EnumType.BillBoardType
local BaseUtil = GameUtil.BaseUtil
local EntityType = GameConfig.EnumType.EntityType
local BillBoardManager = PlayerManager.BillBoardManager

function SpaceDropBillboardItem:Awake()
    self._entityID = 0
    self:InitHeapInfo()
    self:InitBillboard()
end

function SpaceDropBillboardItem:Show(eniID)
    self:SetShowType(eniID)
    self:ShowByEntityType(eniID)
end

function SpaceDropBillboardItem:Close()
    self:SetEntityID(0)
end

function SpaceDropBillboardItem:InitHeapInfo()
    local visibleTransform = {
        ["headInfoObj"] = self.transform:Find("Container_HeapInfo").transform,
        ["nameTwoGo"] = self:FindChildGO("Container_HeapInfo/Container_NameTwo").transform,
    }
    self._visibleTransform = visibleTransform

    local LuaUILayoutElement = typeof(GameMain.LuaUILayoutElement)
    self._layoutElements = {
        ["nameTwoGo"] = visibleTransform.nameTwoGo.gameObject:AddComponent(LuaUILayoutElement),
    }

    self._textTwoName = self:GetChildComponent("Container_HeapInfo/Container_NameTwo/Image_BG/Text_Name", "TextMeshWrapper")
    local image2 = self:GetChildComponent("Container_HeapInfo/Container_NameTwo/Image_BG", 'ImageWrapper')
    image2:SetContinuousDimensionDirty(true)

    self._visibleState = {}
    self:SetGOVisible("nameTwoGo", false)
end

function SpaceDropBillboardItem:InitBillboard()
    self._entityBillboard = self.gameObject:GetComponent(typeof(EntityBillboard))
end

function SpaceDropBillboardItem:OnDestroy()
    self._textTwoName = nil
    self:ClearEntityID()
end

--override
function SpaceDropBillboardItem:SetGOVisible(name, state)
    return SpaceDropBillboardItem._base.SetGOVisible(self, name, state)
end


function SpaceDropBillboardItem:ShowByEntityType(eniID)
    local entity = GameWorld.GetEntity(eniID)
    if entity == nil then return end
    self:ShowHeadInfo(true)
    self:SetColor(eniID)
    self:SetName(entity.name)
    self:SetEntityID(eniID)
end

function SpaceDropBillboardItem:ShowHeadInfo(state)
    self:SetGOVisible("headInfoObj", state)
end

function SpaceDropBillboardItem:SetShowType(eniID)
    self.type = BillBoardManager:GetShowType(eniID)
end

function SpaceDropBillboardItem:SetColor(eniID)
    local honorColor, guildColor, nameColor = BillBoardManager:GetColor(eniID)
    self._nameColor = nameColor
end

function SpaceDropBillboardItem:SetName(value)
    if string.IsNullOrEmpty(value) then
        self:SetGOVisible("nameTwoGo", false)
        return
    end
    if self.type == BillBoardType.SpaceDrop or self.type == BillBoardType.Collect then
        self:SetGOVisible("nameTwoGo", true)
        self._textTwoName.text = BaseUtil.GetColorString(value, self._nameColor)
    end
end

function SpaceDropBillboardItem:ClearEntityID()
    if self._entityID > 0 then
        self._entityID = 0
    end
end

function SpaceDropBillboardItem:SetEntityID(eniID)
    self:ClearEntityID()
    self._entityID = eniID
    self._entityBillboard:SetEntityID(eniID, self.type)
end
