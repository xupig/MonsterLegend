-- CreatureBillboardItem.lua
require "Modules.ModuleBillboard.ChildComponent.BaseBillboardItem"
local CreatureBillboardItem = Class.CreatureBillboardItem(ClassTypes.BaseBillboardItem)

CreatureBillboardItem.interface = GameConfig.ComponentsConfig.Component
CreatureBillboardItem.classPath = "Modules.ModuleBillboard.ChildComponent.CreatureBillboardItem"

local Image = UnityEngine.UI.Image
local XImageFilling = GameMain.XImageFilling
local EntityConfig = GameConfig.EntityConfig
local EntityBillboard = GameMain.EntityBillboard
local BillBoardType = GameConfig.EnumType.BillBoardType
local BaseUtil = GameUtil.BaseUtil
local EntityType = GameConfig.EnumType.EntityType
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local UIProgressBar = ClassTypes.UIProgressBar
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local TitleDataHelper = GameDataHelper.TitleDataHelper
local NobilityDataHelper = GameDataHelper.NobilityDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local BillBoardManager = PlayerManager.BillBoardManager

function CreatureBillboardItem:Awake()
    self._entityID = 0
    self._timerid = -1
    self:InitHeapInfo()
    self:InitHPBar()
    self:InitBillboard()
    self:InitCallbackFunc()
end

function CreatureBillboardItem:Show(eniID)
    self:SetShowType(eniID)
    self:ShowByEntityType(eniID)
    self:OnTitleChange()
end

function CreatureBillboardItem:Close()
    self:SetEntityID(0)
end

function CreatureBillboardItem:InitHeapInfo()
    self._titleIcon = self:FindChildGO("Container_HeapInfo/Container_Title/Container_Icon")
    local visibleTransform = {
        ["headInfoObj"] = self.transform:Find("Container_HeapInfo").transform,
        ["nameOneGo"] = self:FindChildGO("Container_HeapInfo/Container_NameOne").transform,
        ["nameTwoGo"] = self:FindChildGO("Container_HeapInfo/Container_NameTwo").transform,
        ["guildGo"] = self:FindChildGO("Container_HeapInfo/Container_Guild").transform,
        ["serverNameGo"] = self:FindChildGO("Container_HeapInfo/Container_ServerName").transform,
        ["mateGo"] = self:FindChildGO("Container_HeapInfo/Container_Mate").transform,
        ["titleGo"] = self:FindChildGO("Container_HeapInfo/Container_Title").transform,
        ["dropBelongGo"] = self:FindChildGO("Container_HeapInfo/Container_DropBelong").transform,
        ["timeGo"] = self:FindChildGO("Container_HeapInfo/Container_Time").transform,
        ["titleIconTransform"] = self._titleIcon.transform,
    }
    self._visibleTransform = visibleTransform

    local LuaUILayoutElement = typeof(GameMain.LuaUILayoutElement)
    self._layoutElements = {
        ["nameOneGo"] = visibleTransform.nameOneGo.gameObject:AddComponent(LuaUILayoutElement),
        ["nameTwoGo"] = visibleTransform.nameTwoGo.gameObject:AddComponent(LuaUILayoutElement),
        ["guildGo"] = visibleTransform.guildGo.gameObject:AddComponent(LuaUILayoutElement),
        ["serverNameGo"] = visibleTransform.serverNameGo.gameObject:AddComponent(LuaUILayoutElement),
        ["mateGo"] = visibleTransform.mateGo.gameObject:AddComponent(LuaUILayoutElement),
        ["titleGo"] = visibleTransform.titleGo.gameObject:AddComponent(LuaUILayoutElement),
        ["dropBelongGo"] = visibleTransform.dropBelongGo.gameObject:AddComponent(LuaUILayoutElement),
        ["timeGo"] = visibleTransform.timeGo.gameObject:AddComponent(LuaUILayoutElement),
    }

    self._textOneName = self:GetChildComponent("Container_HeapInfo/Container_NameOne/Image_BG/Text_Name", "TextMeshWrapper")
    local containerNobility = self:FindChildGO("Container_HeapInfo/Container_NameOne/Image_BG/Container_Nobility")
    self._visibleTransform["containerNobility"] = containerNobility.transform
    self._layoutElements["containerNobility"] = containerNobility:AddComponent(LuaUILayoutElement)
    self._iconNobility = self:FindChildGO("Container_HeapInfo/Container_NameOne/Image_BG/Container_Nobility/Container_Icon")
    self._textTwoName = self:GetChildComponent("Container_HeapInfo/Container_NameTwo/Image_BG/Text_Name", "TextMeshWrapper")
    self._honorName = self:GetChildComponent("Container_HeapInfo/Container_Title/Text_Title", "TextMeshWrapper")
    self._guildName = self:GetChildComponent("Container_HeapInfo/Container_Guild/Text_Name", "TextMeshWrapper")
    self._serverName = self:GetChildComponent("Container_HeapInfo/Container_ServerName/Text_Name", "TextMeshWrapper")
    self._mateName = self:GetChildComponent("Container_HeapInfo/Container_Mate/Text_Name", "TextMeshWrapper")
    self._visibleTransform["imageOneGo"] = self:FindChildGO("Container_HeapInfo/Container_NameOne/Image_BG").transform
    local image1 = self:GetChildComponent("Container_HeapInfo/Container_NameOne/Image_BG", 'ImageWrapper')
    image1:SetContinuousDimensionDirty(true)
    local image2 = self:GetChildComponent("Container_HeapInfo/Container_NameTwo/Image_BG", 'ImageWrapper')
    image2:SetContinuousDimensionDirty(true)
    self._timeleft = self:GetChildComponent("Container_HeapInfo/Container_Time/Text_Time", "TextMeshWrapper")

    self._visibleState = {}
    self:SetGOVisible("dropBelongGo", false)
    self:SetGOVisible("timeGo", false)
    self:SetGOVisible("nameOneGo", false)
    self:SetGOVisible("nameTwoGo", false)
    self:SetGOVisible("guildGo", false)
    self:SetGOVisible("serverNameGo", false)
    self:SetGOVisible("mateGo", false)
    self:SetGOVisible("titleGo", false)
    self:SetGOVisible("containerNobility", false)
    self:SetGOVisible("imageOneGo", false)
end

--override
function CreatureBillboardItem:SetGOVisible(name, state)
    return CreatureBillboardItem._base.SetGOVisible(self, name, state)
end

function CreatureBillboardItem:InitHPBar()
    self._visibleTransform["hpBarObj"] = self.transform:Find("Container_HpBar").transform
    local imageSlowObj = self.transform:Find("Container_HpBar/Container_Bar/Image_Slow").gameObject
    self._imageSlow = imageSlowObj:GetComponent("ImageWrapper")
    local imageFastObj = self.transform:Find("Container_HpBar/Container_Bar/Image_Fast").gameObject
    self._imageFast = imageFastObj:GetComponent("ImageWrapper")
    self._imageSlowFilling = imageSlowObj:AddComponent(typeof(XImageFilling))
    
    self._angryText = self:GetChildComponent("Container_HpBar/Text_Angry", "TextMeshWrapper")
end

function CreatureBillboardItem:InitBillboard()
    self._entityBillboard = self.gameObject:GetComponent(typeof(EntityBillboard))
end

function CreatureBillboardItem:InitCallbackFunc()
    self._onHPChangeCB = function()self:OnHPChange() end
    self._onDropBelongChangeCB = function()self:OnDropBelongChange() end
    self._onTitleChangeCB = function()self:OnTitleChange() end
    self._onNobilityChangeCB = function()self:OnNobilityChange() end
    self._onMateNameChangeCB = function()self:OnMateNameChange() end
end

function CreatureBillboardItem:OnDestroy()
    self._dropBelongGo = nil
    self._textName = nil
    self._honorName = nil
    self._imageSlow = nil
    self._imageFast = nil
    self._imageSlowFilling = nil
    self._timerid = -1
    self:ClearEntityID()
end

function CreatureBillboardItem:ShowByEntityType(eniID)
    local entity = GameWorld.GetEntity(eniID)
    self:ShowHeadInfo(true)
    self:SetColor(eniID)
    if entity == nil then return end
    
    if entity.entityType == EntityType.Monster or entity.entityType == EntityType.Dummy then
        if eniID == GameWorld.Player():GetTargetId() then
            self:SetName(string.format("Lv.%d %s", entity.level, entity.name))
        else
            self:SetName()
        end
    elseif entity.entityType == EntityType.BossTomb then
        self._reviveTime = DateTimeUtil.MinusSec(entity.reviveTime)
        if self._reviveTime > 0 then
            if self._timerid == -1 then
                self._timerFun = function()self:TimerFunCB() end
                self._timerid = TimerHeap:AddSecTimer(0, 1, self._reviveTime, self._timerFun)
            end
            self._timeleft.text = DateTimeUtil:FormatFullTime(self._reviveTime)
        else
            if self._timerid ~= -1 then
                TimerHeap:DelTimer(self._timerid)
                self._timerid = -1
            end
        end
        self:SetName(entity.bossname)
    else
        self:SetName(entity.name)
    end
    self:SetGuild(entity.guild_name)
    self:SetServerName(entity.server_id)
    self:ShowHP(true, entity)
    self:SetEntityID(eniID)
end


function CreatureBillboardItem:TimerFunCB()
    self._reviveTime = self._reviveTime - 1
    if self._reviveTime <= 0 then
        if self._timerid ~= -1 then
            TimerHeap:DelTimer(self._timerid)
            self._timerid = -1
        end
        self._timeleft.text = ''
    else
        self._timeleft.text = DateTimeUtil:FormatFullTime(self._reviveTime)
    end
end

function CreatureBillboardItem:ShowHeadInfo(state)
    self:SetGOVisible("headInfoObj", state)
end

function CreatureBillboardItem:ShowHP(state, entity)
    self._angryText.text = ""
    self.type = self.type or BillBoardType.Normal
    if self.type == BillBoardType.Normal or self.type == BillBoardType.Elite then
        if entity.entityType == EntityType.Monster or entity.entityType == EntityType.Dummy then
            local angry = MonsterDataHelper.GetMonsterAngry(entity.monster_id)
            if angry > 0 then
                self._angryText.text = angry
            end
        end
        self:ShowHPNormal(state)
    end
    if self.type == BillBoardType.Npc then
        self:ShowHPNormal(false)
    end
    if self.type == BillBoardType.OtherPlayer then
        self:ShowHPNormal(true)
    end
    if self.type == BillBoardType.Boss then
        self:ShowHPNormal(false)
    end
    if self.type == BillBoardType.SpaceDrop then
        self:ShowHPNormal(false)
    end
    
    if self.type == BillBoardType.Collect then
        self:ShowHPNormal(false)
    end
    if self.type == BillBoardType.DeathFlage then
        self:ShowHPNormal(false)
    end
end

function CreatureBillboardItem:ShowHPNormal(state)
    self:SetGOVisible("hpBarObj", state)
end

function CreatureBillboardItem:SetHPValue(value)
    if self.type == BillBoardType.Normal or self.type == BillBoardType.Elite
        or self.type == BillBoardType.OtherPlayer then
        self._imageFast.fillAmount = value
        self._imageSlowFilling:SetValue(value)
    end
end

function CreatureBillboardItem:SetShowType(eniID)
    self.type = BillBoardManager:GetShowType(eniID)
end

function CreatureBillboardItem:SetColor(eniID)
    self._honorColor, self._guildColor, self._nameColor = BillBoardManager:GetColor(eniID)
end


function CreatureBillboardItem:SetName(value)
    if string.IsNullOrEmpty(value) then
        self:SetGOVisible("imageOneGo", false)
        self:SetGOVisible("nameOneGo", false)
        self:SetGOVisible("nameTwoGo", false)
        return
    end
    if self.type == BillBoardType.DeathFlage then
        self:SetGOVisible("nameOneGo", false)
        self:SetGOVisible("nameTwoGo", true)
        self:SetGOVisible("timeGo", true)
        self._textTwoName.text = BaseUtil.GetColorString(value, self._nameColor)
    elseif self.type == BillBoardType.SpaceDrop or self.type == BillBoardType.Collect then
        self:SetGOVisible("nameOneGo", false)
        self:SetGOVisible("nameTwoGo", true)
        self:SetGOVisible("timeGo", false)
        self._textTwoName.text = BaseUtil.GetColorString(value, self._nameColor)
    elseif self.type == BillBoardType.Normal or self.type == BillBoardType.Elite then
        self:SetGOVisible("imageOneGo", true)
        self:SetGOVisible("nameOneGo", true)
        self:SetGOVisible("nameTwoGo", false)
        self:SetGOVisible("timeGo", false)
        self._textOneName.text = BaseUtil.GetColorString(value, self._nameColor)
    else
        self:SetGOVisible("imageOneGo", true)
        self:SetGOVisible("nameOneGo", true)
        self:SetGOVisible("nameTwoGo", false)
        self:SetGOVisible("timeGo", false)
        self._textOneName.text = BaseUtil.GetColorString(value, self._nameColor)
    end
end


function CreatureBillboardItem:SetHonor(value)
    if string.IsNullOrEmpty(value) then
        self:SetGOVisible("titleGo", false)
        return
    end
    self:SetGOVisible("titleGo", true)
    if type(value) == "number" then
        self:SetGOVisible("titleIconTransform", true)
        GameWorld.AddIconAnim(self._titleIcon, value)
        GameWorld.SetIconAnimSpeed(self._titleIcon, 5)
        self._honorName.text = ""
    else
        self:SetGOVisible("titleIconTransform", false)
        self._honorName.text = value
    end
end

function CreatureBillboardItem:SetGuild(value)
    if string.IsNullOrEmpty(value) then
        self._guildName.text = ""
        self:SetGOVisible("guildGo", false)
        return
    end
    self:SetGOVisible("guildGo", true)
    value = "<" .. value .. ">"
    self._guildName.text = BaseUtil.GetColorString(value, self._guildColor)
end

function CreatureBillboardItem:SetServerName(serverId)
    local isInCrossScene = PlayerManager.CrossServerManager:IsInCrossScene()
    if isInCrossScene and serverId and serverId ~= 0 then
        local name = GameManager.ServerListManager:GetServerNameById(tostring(serverId))
        self:SetGOVisible("serverNameGo", true)
        self._serverName.text = BaseUtil.GetColorString(name, self._guildColor)
    else
        self._serverName.text = ""
        self:SetGOVisible("serverNameGo", false)
    end    
end

function CreatureBillboardItem:ClearEntityID()
    if self._entityID > 0 then
        local entity = GameWorld.GetEntity(self._entityID)
        if entity ~= nil then
            entity:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
            entity:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
            entity:RemovePropertyListener(EntityConfig.PROPERTY_TITLE_ID, self._onTitleChangeCB)
            entity:RemovePropertyListener(EntityConfig.PROPERTY_BOSS_DROP_FLAG, self._onDropBelongChangeCB)
            entity:RemovePropertyListener(EntityConfig.PROPERTY_NOBILITY_RANK, self._onNobilityChangeCB)
            entity:RemovePropertyListener(EntityConfig.PROPERTY_MATE_NAME, self._onMateNameChangeCB)
        end
        self._entityID = 0
    end
end

function CreatureBillboardItem:SetEntityID(eniID)
    self:ClearEntityID()
    self._entityID = eniID
    self._entityBillboard:SetEntityID(eniID, self.type)
    local entity = GameWorld.GetEntity(eniID)
    if entity ~= nil then
        entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
        entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
        entity:AddPropertyListener(EntityConfig.PROPERTY_TITLE_ID, self._onTitleChangeCB)
        entity:AddPropertyListener(EntityConfig.PROPERTY_BOSS_DROP_FLAG, self._onDropBelongChangeCB)
        entity:AddPropertyListener(EntityConfig.PROPERTY_NOBILITY_RANK, self._onNobilityChangeCB)
        entity:AddPropertyListener(EntityConfig.PROPERTY_MATE_NAME, self._onMateNameChangeCB)
        self:OnHPChange()
        self:OnTitleChange()
        self:OnDropBelongChange()
        self:OnNobilityChange()
        self:OnMateNameChange()
    end
end

function CreatureBillboardItem:OnHPChange()
    local entity = GameWorld.GetEntity(self._entityID)
    if entity ~= nil then
        self:SetHPValue((entity.cur_hp or 1) / (entity.max_hp or 1))
    end
end

function CreatureBillboardItem:OnTitleChange()
    local entity = GameWorld.GetEntity(self._entityID)
    if entity ~= nil then
        if GameSceneManager:GetCurSceneType() == SceneConfig.SCENE_TYPE_GUILD_CONQUEST and 
            (entity.entityType == EntityType.Avatar or entity.entityType == EntityType.PlayerAvatar) then
            --公会争霸屏蔽所有玩家称号
            self:SetHonor()
            return
        end

        if GameSceneManager:GetCurSceneType() == SceneConfig.SCENE_TYPE_ANCIENT_BATTLE and 
            (entity.entityType == EntityType.Avatar or entity.entityType == EntityType.PlayerAvatar) then
            --地域战场屏蔽所有玩家称号
            self:SetHonor()
            return
        end

        local hide = PlayerManager.SystemSettingManager:GetPlayerSetting(GameConfig.SystemSettingConfig.OTHER_TITLE)
        if entity.entityType == EntityType.Avatar and hide then
            self:SetHonor()
            return
        end
        
        local title = entity.honorName
        if entity.entityType == EntityType.PlayerAvatar or entity.entityType == EntityType.Avatar then
            if entity.title_id ~= 0 then
                title = TitleDataHelper.GetTitleShowName(entity.title_id)
            end
        end
        self:SetHonor(title)
    end
end

function CreatureBillboardItem:OnDropBelongChange()
    local entity = GameWorld.GetEntity(self._entityID)
    if entity ~= nil then
        self:SetGOVisible("dropBelongGo", (entity.boss_drop_flag == 1))
    end
end

--伴侣变化
function CreatureBillboardItem:OnMateNameChange()
    local entity = GameWorld.GetEntity(self._entityID)
    if entity ~= nil then
        local mate_name = entity.mate_name
        if mate_name ~= nil and mate_name ~= "" then
            self:SetGOVisible("mateGo", true)
            local langId = entity.marry_seq == 1 and 59057 or 59058 --有配偶情况下，1是丈夫，2是妻子
            -- LoggerHelper.Error("marry_seq: " .. entity.name .. " " .. entity.marry_seq)
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = mate_name
            self._mateName.text = LanguageDataHelper.CreateContent(langId, argsTable)
        else
            self:SetGOVisible("mateGo", false)
        end
    end
end


--爵位变化
function CreatureBillboardItem:OnNobilityChange()
    local entity = GameWorld.GetEntity(self._entityID)
    if entity ~= nil then
        local nobility = entity.nobility_rank
        if nobility and FunctionOpenDataHelper:CheckFunctionOpen(3) then
            self:SetGOVisible("containerNobility", true)
            local icon = NobilityDataHelper:GetNobilityShow(nobility)
            if icon then
                GameWorld.AddIcon(self._iconNobility, icon)
            end
        else
            self:SetGOVisible("containerNobility", false)
        end
    end
end
