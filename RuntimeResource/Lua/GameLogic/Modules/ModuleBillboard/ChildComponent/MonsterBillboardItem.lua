-- MonsterBillboardItem.lua
require "Modules.ModuleBillboard.ChildComponent.BaseBillboardItem"
local MonsterBillboardItem = Class.MonsterBillboardItem(ClassTypes.BaseBillboardItem)

MonsterBillboardItem.interface = GameConfig.ComponentsConfig.Component
MonsterBillboardItem.classPath = "Modules.ModuleBillboard.ChildComponent.MonsterBillboardItem"

local Image = UnityEngine.UI.Image
local XHPBar = GameMain.XHPBar
local EntityConfig = GameConfig.EntityConfig
local EntityBillboard = GameMain.EntityBillboard
local BillBoardType = GameConfig.EnumType.BillBoardType
local BaseUtil = GameUtil.BaseUtil
local EntityType = GameConfig.EnumType.EntityType
local UIProgressBar = ClassTypes.UIProgressBar
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local BillBoardManager = PlayerManager.BillBoardManager

function MonsterBillboardItem:Awake()
    self._entityID = 0
    self:InitHeapInfo()
    self:InitHPBar()
    self:InitBillboard()
    self:InitCallbackFunc()
end

function MonsterBillboardItem:Show(eniID)
    self:SetShowType(eniID)
    self:ShowByEntityType(eniID)
end

function MonsterBillboardItem:Close()
    self:SetEntityID(0)
end

function MonsterBillboardItem:InitHeapInfo()
    local visibleTransform = {
        ["headInfoObj"] = self.transform:Find("Container_HeapInfo").transform,
        ["nameOneGo"] = self:FindChildGO("Container_HeapInfo/Container_NameOne").transform,
    }
    self._visibleTransform = visibleTransform

    local LuaUILayoutElement = typeof(GameMain.LuaUILayoutElement)
    self._layoutElements = {
        ["nameOneGo"] = visibleTransform.nameOneGo.gameObject:AddComponent(LuaUILayoutElement),
    }

    self._textOneName = self:GetChildComponent("Container_HeapInfo/Container_NameOne/Image_BG/Text_Name", "TextMeshWrapper")
    self._visibleTransform["imageOneGo"] = self:FindChildGO("Container_HeapInfo/Container_NameOne/Image_BG").transform
    local image1 = self:GetChildComponent("Container_HeapInfo/Container_NameOne/Image_BG", 'ImageWrapper')
    image1:SetContinuousDimensionDirty(true)
    
    self._visibleState = {}
    self:SetGOVisible("nameOneGo", false)
    self:SetGOVisible("imageOneGo", false)
end

--override
function MonsterBillboardItem:SetGOVisible(name, state)
    return MonsterBillboardItem._base.SetGOVisible(self, name, state)
end

function MonsterBillboardItem:InitHPBar()
    self._hpBar = self:AddChildComponent("Container_HpBar", XHPBar)
    self._visibleTransform["hpBarObj"] = self.transform:Find("Container_HpBar").transform
    self._angryText = self:GetChildComponent("Container_HpBar/Text_Angry", "TextMeshWrapper")
end

function MonsterBillboardItem:InitBillboard()
    self._entityBillboard = self.gameObject:GetComponent(typeof(EntityBillboard))
end

function MonsterBillboardItem:InitCallbackFunc()
    -- self._onHPChangeCB = function()self:OnHPChange() end
end

function MonsterBillboardItem:OnDestroy()
    self._dropBelongGo = nil
    self._textName = nil
    self._imageSlow = nil
    self._imageFast = nil
    self._imageSlowFilling = nil
    self:ClearEntityID()
end

function MonsterBillboardItem:ShowByEntityType(eniID)
    local entity = GameWorld.GetEntity(eniID)
    self:ShowHeadInfo(true)
    self:SetColor(eniID)
    if entity == nil then return end
    
    if entity.entityType == EntityType.Monster or entity.entityType == EntityType.Dummy then
        if eniID == GameWorld.Player():GetTargetId() then
            self:SetName(string.format("Lv.%d %s", entity.level, entity.name))
        else
            self:SetName()
        end
    else
        self:SetName(entity.name)
    end
    self:ShowHP(true, entity)
    self:SetEntityID(eniID)
end


function MonsterBillboardItem:ShowHeadInfo(state)
    self:SetGOVisible("headInfoObj", state)
end

function MonsterBillboardItem:ShowHP(state, entity)
    self._angryText.text = ""
    self.type = self.type or BillBoardType.Normal
    if self.type == BillBoardType.Normal or self.type == BillBoardType.Elite then
        if entity.entityType == EntityType.Monster or entity.entityType == EntityType.Dummy then
            local angry = MonsterDataHelper.GetMonsterAngry(entity.monster_id)
            if angry > 0 then
                self._angryText.text = angry
            end
        end
        self:ShowHPNormal(state)
    else
        self:ShowHPNormal(false)
    end
end

function MonsterBillboardItem:ShowHPNormal(state)
    self:SetGOVisible("hpBarObj", state)
end

function MonsterBillboardItem:SetShowType(eniID)
    self.type = BillBoardManager:GetShowType(eniID)
end

function MonsterBillboardItem:SetColor(eniID)
    local honorColor, guildColor, nameColor = BillBoardManager:GetColor(eniID)
    self._nameColor = nameColor
end


function MonsterBillboardItem:SetName(value)
    if string.IsNullOrEmpty(value) then
        self:SetGOVisible("imageOneGo", false)
        self:SetGOVisible("nameOneGo", false)
        return
    end
    self:SetGOVisible("imageOneGo", true)
    self:SetGOVisible("nameOneGo", true)
    self._textOneName.text = BaseUtil.GetColorString(value, self._nameColor)
end



function MonsterBillboardItem:ClearEntityID()
    if self._entityID > 0 then
        if self:NeedHpChange() then
            self._hpBar:RemoveHPListener()
        end
        self._entityID = 0
    end
end

function MonsterBillboardItem:SetEntityID(eniID)
    self:ClearEntityID()
    self._entityID = eniID
    self._entityBillboard:SetEntityID(eniID, self.type)
    local entity = GameWorld.GetEntity(eniID)
    if entity ~= nil then
        if self:NeedHpChange() then

            self._hpBar:AddHPListener(self._entityID)
        end
    end
end

function MonsterBillboardItem:NeedHpChange()
    return (self.type == BillBoardType.Normal or self.type == BillBoardType.Elite
        or self.type == BillBoardType.OtherPlayer)
end