-- BillboardPanel.lua
local BillboardPanel = Class.BillboardPanel(ClassTypes.BasePanel)

local ComponentsConfig = GameConfig.ComponentsConfig

require "Modules.ModuleBillboard.ChildView.CreatureBillboardView"
local CreatureBillboardView = ClassTypes.CreatureBillboardView

function BillboardPanel:Awake()
    self.panelBH = self:GetComponent('LuaUIPanel')
    self:AddChildLuaUIComponent("Container_CreatureBillboard", CreatureBillboardView)
end

function BillboardPanel:OnDestroy() 
    self.panelBH = nil
end


