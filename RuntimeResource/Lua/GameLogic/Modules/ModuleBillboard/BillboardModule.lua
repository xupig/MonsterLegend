-- BillboardModule.lua
BillboardModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function BillboardModule.Init()
    GUIManager.AddPanel(PanelsConfig.Billboard, "Panel_Billboard", GUILayer.LayerUIBillboard, "Modules.ModuleBillboard.BillboardPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST, nil, true)
    -- GUIManager.AddPanel(PanelsConfig.PointingArrow, "Panel_PointingArrow", GUILayer.LayerUIBillboard, "Modules.ModuleBillboard.PointingArrowPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.BubbleDialog, "Panel_BubbleDialog", GUILayer.LayerUIBillboard, "Modules.ModuleBillboard.BubbleDialogPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
end

return BillboardModule



