-- BubbleDialogPanel.lua
local BubbleDialogPanel = Class.BubbleDialogPanel(ClassTypes.BasePanel)

local ComponentsConfig = GameConfig.ComponentsConfig

require "Modules.ModuleBillboard.ChildView.BubbleDialogView"
local BubbleDialogView = ClassTypes.BubbleDialogView

function BubbleDialogPanel:Awake()
    self:AddChildLuaUIComponent("Container_BubbleDialog", BubbleDialogView)
end

function BubbleDialogPanel:OnDestroy() 
end