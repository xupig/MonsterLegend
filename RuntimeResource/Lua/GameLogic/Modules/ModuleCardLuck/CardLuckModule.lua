--CardLuckModule.lua
CardLuckModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function CardLuckModule.Init()
	--LoggerHelper.Error("CardLuckModule.Init()")
	GUIManager.AddPanel(PanelsConfig.CardLuck,"Panel_CardLuck",GUILayer.LayerUIPanel,"Modules.ModuleCardLuck.CardLuckPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return CardLuckModule
