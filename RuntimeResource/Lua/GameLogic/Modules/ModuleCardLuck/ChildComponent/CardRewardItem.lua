-- CardRewardItem.lua
local CardRewardItem = Class.CardRewardItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
CardRewardItem.interface = GameConfig.ComponentsConfig.Component
CardRewardItem.classPath = "Modules.ModuleCardLuck.ChildComponent.CardRewardItem"
local OpenCardRewardDataHelper = GameDataHelper.OpenCardRewardDataHelper

local UIList = ClassTypes.UIList
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function CardRewardItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function CardRewardItem:OnDestroy() 

end

--override
function CardRewardItem:OnRefreshData(id)
    if id then
        --LoggerHelper.Log("CardRewardItem:OnRefreshData(data)"..PrintTable:TableToStr(data))
        local reward = OpenCardRewardDataHelper:GetReward(id)
        --LoggerHelper.Error("CardRewardItem:OnRefreshData(id)"..PrintTable:TableToStr(reward))
        self._itemIcon:SetItem(reward[1][1], 0, 0, true)
        self._itemIcon:ActivateTipsById()
    end
end