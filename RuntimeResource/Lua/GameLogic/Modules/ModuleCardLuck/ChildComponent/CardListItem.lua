-- CardListItem.lua
local CardListItem = Class.CardListItem(ClassTypes.UIListItem)
CardListItem.interface = GameConfig.ComponentsConfig.Component
CardListItem.classPath = "Modules.ModuleCardLuck.ChildComponent.CardListItem"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local CardLuckManager = PlayerManager.CardLuckManager
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local TimerHeap = GameWorld.TimerHeap
local Original_Pos = Vector3(0, 0, 0)
local Tmp_Pos = Vector3(0, 90, 0)
local Tmp_Pos_2 = Vector3(0, 180, 0)

function CardListItem:__ctor__()
    self._state = nil
end

function CardListItem:Awake()
    self:InitItem()
    
    
    if self._state == nil then
        return
    end
    self:SetState(self._state)

end

function CardListItem:OnDestroy()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function CardListItem:InitItem()
    self._itemContainer = self:FindChildGO("Image_Bg_2/Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
    
    self._imgBg1 = self:FindChildGO("Image_Bg_1")
    self._imgBg2 = self:FindChildGO("Image_Bg_2")
    self._imgBgCrown = self:FindChildGO("Image_Bg_2/Image_Crown")
    
    
    self._imgRotation1 = self._imgBg1:AddComponent(typeof(XGameObjectTweenRotation))
    self._imgRotation2 = self._imgBg2:AddComponent(typeof(XGameObjectTweenRotation))
    
    self._itemRotation = self._itemContainer:AddComponent(typeof(XGameObjectTweenRotation))
    self._imgCrownRotation = self._imgBgCrown:AddComponent(typeof(XGameObjectTweenRotation))
end

function CardListItem:OnRefreshData(data)
    if data then
        local positionList = CardLuckManager:GetPositionInfo()
        --LoggerHelper.Error("OpenRankView:CloseView()"..PrintTable:TableToStr(positionList))
        self._posId = data._posId
        if positionList[data._posId] ~= nil then
            if positionList[data._posId][1] == -1 then
                self._imgBgCrown:SetActive(true)
                self._imgBg2.transform.localEulerAngles = Original_Pos
                self._imgBg1.transform.localEulerAngles = Tmp_Pos
            
            else
                --LoggerHelper.Error("OpenRankView:CloseView()"..positionList[data._posId])
                self._imgBgCrown:SetActive(false)
                self._imgBg2.transform.localEulerAngles = Original_Pos
                self._imgBg1.transform.localEulerAngles = Tmp_Pos
                
                
                self._itemIcon:ActivateTipsById()
                self._itemIcon:SetItem(tonumber(positionList[data._posId][1]), tonumber(positionList[data._posId][2]))
            --self._itemIcon:SetItem(tonumber(3103), 1)
            end
        else
            self._imgBgCrown:SetActive(false)
            self._imgBg2.transform.localEulerAngles = Tmp_Pos
            self._imgBg1.transform.localEulerAngles = Original_Pos
        end
        
        self:UpdateRed()
    end
end

function CardListItem:UpdateRed()

end

function CardListItem:CardRotation()
    if self._posId then
        local positionList = CardLuckManager:GetPositionInfo()
        if positionList[self._posId] ~= nil then
            if positionList[self._posId][1] == -1 then
                self._imgBgCrown:SetActive(true)
            else
                self._imgBgCrown:SetActive(false)
                self._itemIcon:ActivateTipsById()
                self._itemIcon:SetItem(tonumber(positionList[self._posId][1]), tonumber(positionList[self._posId][2]))
            end
        else
            self._imgBgCrown:SetActive(false)
        end
        self._imgRotation1:SetFromToRotationOnce(Original_Pos, Tmp_Pos, 0.2, function()self:CardRotationNext() end)
    end
end

function CardListItem:CardRotationNext()
    self._imgRotation2:SetFromToRotationOnce(Tmp_Pos, Original_Pos, 0.1, nil)
end


function CardListItem:SetState(state)

end
