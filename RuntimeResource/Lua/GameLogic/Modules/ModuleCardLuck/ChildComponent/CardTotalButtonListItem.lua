-- CardTotalButtonListItem.lua
local CardTotalButtonListItem = Class.CardTotalButtonListItem(ClassTypes.UIListItem)
CardTotalButtonListItem.interface = GameConfig.ComponentsConfig.Component
CardTotalButtonListItem.classPath = "Modules.ModuleCardLuck.ChildComponent.CardTotalButtonListItem"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local OpenCardCrystalDataHelper = GameDataHelper.OpenCardCrystalDataHelper
local public_config = GameWorld.public_config
local CardLuckManager = PlayerManager.CardLuckManager
local SevenDayLoginManager = PlayerManager.SevenDayLoginManager
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local TipsManager = GameManager.TipsManager
local UIParticle = ClassTypes.UIParticle

function CardTotalButtonListItem:__ctor__()
    self._state=nil
end

function CardTotalButtonListItem:Awake()
    self:InitItem()


    if self._state==nil then
        return
    end
    self:SetState(self._state)

end

function CardTotalButtonListItem:OnDestroy()

end

function CardTotalButtonListItem:InitItem()
    self._imgIndex1 = self:FindChildGO("Image_Index_1")
    self._imgIndex2 = self:FindChildGO("Image_Index_2")
    self._imgIndex3 = self:FindChildGO("Image_Index_3")
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)


    -- self._imgReward = self:FindChildGO("Image_Bg")
    -- self._txtReward = self:GetChildComponent("Image_Bg/Text_Num", 'TextMeshWrapper')
    
    self._alreadyGet = self:FindChildGO("Container_Already_Get")
    self._imgDayBg = self:FindChildGO("Container_Already_Get/Image_Bg")
    self._imgDayComplete = self:FindChildGO("Container_Already_Get/Image_Complete")
    
    self._alreadyGet:SetActive(false)
    self._fx_Container = self:FindChildGO("Container_Fx")
    self._fx_ui_30011 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30011")
    self:StopFx()
    
    self._canGet = self:FindChildGO("Image_Can_Get")
    self._canGet:SetActive(false)

    self._imageRed = self:FindChildGO("Image_Red")
    self._imageRed:SetActive(false)
    self._onClick = function(pointerEventData)self:OnPointerUp(pointerEventData) end
    self._itemIcon:SetPointerUpCB(self._onClick)

end

function CardTotalButtonListItem:OnDestroy()
    self._itemContainer = nil
    self._itemIcon = nil
    self._fx_ui_30011:Stop()
end

function CardTotalButtonListItem:PlayFx()
    self._fx_ui_30011:Play(true, true)
end

function CardTotalButtonListItem:StopFx()
    self._fx_ui_30011:Stop()
end

--override
function CardTotalButtonListItem:OnPointerDown(pointerEventData)
end

--override
function CardTotalButtonListItem:OnPointerUp(pointerEventData)
    if self._itemId then
        local cardInfo = CardLuckManager:GetCardInfo()
        local cardNum = cardInfo[public_config.OPEN_CARD_STEP]
        local luckNum = cardInfo[public_config.CHAR_OBTAINED_COUNT]
        local rewardList = CardLuckManager:GetRewardInfo()
        if luckNum~=nil then
            if luckNum >= self:GetIndex()+1 then
                if rewardList[self:GetIndex()+1] == 1 then
                    TipsManager:ShowItemTipsById(self._itemId)
                else
                    CardLuckManager:SendBlessRewardReq(self:GetIndex()+1)
                end
            else
                TipsManager:ShowItemTipsById(self._itemId)
            end
        else
            TipsManager:ShowItemTipsById(self._itemId)
        end
    end
end

function CardTotalButtonListItem:OnRefreshData(data)
    if data then
        self._imgIndex1:SetActive(self:GetIndex()+1 == 1)
        self._imgIndex2:SetActive(self:GetIndex()+1 == 2)
        self._imgIndex3:SetActive(self:GetIndex()+1 == 3)
        
        local reward = OpenCardCrystalDataHelper:GetReward(self:GetIndex()+1)
        --LoggerHelper.Error("reward"..PrintTable:TableToStr(reward))
        --self._itemIcon:ActivateTipsById()
        self._itemId = reward[1][1]
        self._itemIcon:SetItem(reward[1][1], reward[1][2])

        local cardInfo = CardLuckManager:GetCardInfo()
        local cardNum = cardInfo[public_config.OPEN_CARD_STEP]
        local luckNum = cardInfo[public_config.CHAR_OBTAINED_COUNT]
        --local step = OpenCardCrystalDataHelper:GetStep(self:GetIndex()+1)
        local rewardList = CardLuckManager:GetRewardInfo()
        self:StopFx()
        self._canGet:SetActive(false)
        if luckNum~=nil then
            if luckNum >= self:GetIndex()+1 then
                if rewardList[self:GetIndex()+1] == 1 then
                    self:StopFx()
                    self._canGet:SetActive(false)
                    self._alreadyGet:SetActive(true)
                    self._imgDayBg:SetActive(true)
                    self._imgDayComplete:SetActive(true)
                else
                    self:PlayFx()
                    self._canGet:SetActive(true)
                    --LoggerHelper.Error("self:PlayFx()============================")
                end
            else
    
            end
        end


        self:UpdateRed()
    end
end

function CardTotalButtonListItem:UpdateRed()

end



function CardTotalButtonListItem:SetState(state)

end