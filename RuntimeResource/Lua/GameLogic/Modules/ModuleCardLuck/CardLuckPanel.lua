﻿
local ClassTypes = ClassTypes
local CardLuckPanel = Class.CardLuckPanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local GUIManager = GameManager.GUIManager

local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local OpenCardRewardDataHelper = GameDataHelper.OpenCardRewardDataHelper
local CardLuckManager = PlayerManager.CardLuckManager
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local SettingType = GameConfig.SettingType

require "UIComponent.Extend.TipsCom"
local TipsCom = ClassTypes.TipsCom

require "Modules.ModuleCardLuck.ChildComponent.CardLuckTotalButtonListItem"
local CardLuckTotalButtonListItem = ClassTypes.CardLuckTotalButtonListItem

require "Modules.ModuleCardLuck.ChildComponent.CardListItem"
local CardListItem = ClassTypes.CardListItem

require "Modules.ModuleCardLuck.ChildComponent.CardRewardItem"
local CardRewardItem = ClassTypes.CardRewardItem

local StringStyleUtil = GameUtil.StringStyleUtil
local UILinkTextMesh = ClassTypes.UILinkTextMesh
function CardLuckPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self:InitCom()
    self:InitView()
    self._openCardResp = function()self:RefreshView() end
    self._blessCardResp = function()self:RefreshTitle() end
end

function CardLuckPanel:InitCom()
    -- self._btnClose = self:FindChildGO("Container_Total/Button_Close")
    -- self._csBH:AddClick(self._btnClose, function()self:OnCloseLuckClick() end)

    
    self._txtLuckNeed = self:GetChildComponent("Container_Card/Text_Need", 'TextMeshWrapper')
    self._txtLuckNum = self:GetChildComponent("Container_Card/Text_Num", 'TextMeshWrapper')
    self._txtLuckDesc = self:GetChildComponent("Container_Card/Text_Desc", 'TextMeshWrapper')
    self._txtLuckTime = self:GetChildComponent("Container_Card/Text_Time", 'TextMeshWrapper')

    self._txtLuckDesc = self:FindChildGO("Container_Card/Text_Desc")
    self._txtLuckDesc = self:GetChildComponent("Container_Card/Text_Desc", "TextMeshWrapper")
    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject, "Container_Card/Text_Desc")
    self._linkTextMesh:SetOnClickCB(function(linkIndex, postion)self:OnRuleDesc(linkIndex, postion) end)
    local killDesc = '<u><link="CLICK_LINK">' .. LanguageDataHelper.CreateContent(81592) .. '</link></u>'
    self._txtLuckDesc.text =  StringStyleUtil.GetColorStringWithTextMesh(killDesc,StringStyleUtil.green)
    
    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(81591))
    self._tipsGO:SetActive(false)
   
end

function CardLuckPanel:OnRuleDesc()
    self._tipsGO:SetActive(true)
end


function CardLuckPanel:OnCloseTicketClick()
    CardLuckManager:SendOpenCardReq(self._curIndex+1)
end

function CardLuckPanel:ToggeleStateInit()

end






function CardLuckPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.OpenCardResp, self._openCardResp)
    EventDispatcher:AddEventListener(GameEvents.GetBlessRewardResp, self._blessCardResp)
    
end

function CardLuckPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.OpenCardResp, self._openCardResp)
    EventDispatcher:RemoveEventListener(GameEvents.GetBlessRewardResp, self._blessCardResp)
end


function CardLuckPanel:Start()
    

end

function CardLuckPanel:UpdateFunOpen()


end

-- function CardLuckPanel:OnCloseLuckClick()
--     GUIManager.ClosePanel(PanelsConfig.CardLuck)
-- end

function CardLuckPanel:OnClose()
    self._iconBg = nil
    self:RemoveEventListeners()
end

function CardLuckPanel:RefreshView()
    local getPosition = CardLuckManager:GetPositionInfo()
    if self._curIndex then
        local cardItem = self._cardList:GetItem(self._curIndex)
        cardItem:CardRotation()
    end
    
    local ids = OpenCardRewardDataHelper:GetAllId()
    local idsBack = {}
    for i=#ids,1,-1 do
        table.insert(idsBack,ids[i])
    end

    self._cardTitleList:SetDataList(idsBack)

    local ids = OpenCardRewardDataHelper:GetAllId()
    self._cardRewardList:SetDataList(ids)
end

function CardLuckPanel:RefreshTitle()
    local ids = OpenCardRewardDataHelper:GetAllId()
    local idsBack = {}
    for i=#ids,1,-1 do
        table.insert(idsBack,ids[i])
    end
    self._cardTitleList:SetDataList(idsBack)

    local ids = OpenCardRewardDataHelper:GetAllId()
    self._cardRewardList:SetDataList(ids)
end





function CardLuckPanel:InitView()
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_Total/ScrollViewList_Buttons")
    self._cardTitleList = list
    self._cardTitleView = scrollview
    self._cardTitleList:SetItemType(CardLuckTotalButtonListItem)
    self._cardTitleList:SetPadding(5, 0, 0, 0)
    self._cardTitleList:SetGap(5, 0)
    self._cardTitleList:SetDirection(UIList.DirectionTopToDown, 1, 100)
    local itemBTClickedCB =
        function(index)
            self:OnItemBTClicked(index)
        end
    self._cardTitleList:SetItemClickedCB(itemBTClickedCB)





    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_Card/Container_List/ScrollViewList")
    self._cardList = list
    self._cardView = scrollview
    self._cardList:SetItemType(CardListItem)
    self._cardList:SetPadding(0, 10, 20, 25)
    self._cardList:SetGap(0, 0)
    self._cardList:SetDirection(UIList.DirectionLeftToRight, 5, 100)
    local itemBTClickedCB =
        function(index)
            self:OnCardItemBTClicked(index)
        end
    self._cardList:SetItemClickedCB(itemBTClickedCB)

    
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_Total/Container_Reward/ScrollViewList")
    self._cardRewardList = list
    self._cardRewardView = scrollview
    self._cardRewardList:SetItemType(CardRewardItem)
    self._cardRewardList:SetPadding(5, 0, 0, 10)
    self._cardRewardList:SetGap(5, 20)
    self._cardRewardList:SetDirection(UIList.DirectionLeftToRight, 5, 100)
    local itemBTClickedCB =
        function(index)
            self:OnCardItemBTClicked(index)
        end
    self._cardRewardList:SetItemClickedCB(itemBTClickedCB)

end

function CardLuckPanel:OnItemBTClicked(index)

end

function CardLuckPanel:OnCardItemBTClicked(index)
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = GlobalParamsHelper.GetParamValue(860)
    self._curIndex = index
    GUIManager.ShowDailyTips(SettingType.CARD_LUCK_NEED, function() self:OnCloseTicketClick() end,nil, LanguageDataHelper.CreateContent(81538,argsTable), true)

end



function CardLuckPanel:GuideGift(funId)

end




--override
function CardLuckPanel:OnDestroy()

end

--override
function CardLuckPanel:OnShow(data)
    -- self._iconBg = self:FindChildGO("Container_Total/Image")
    -- GameWorld.AddIcon(self._iconBg,13512)
    self:RightBgIsActive(true)
    self:InitData()
    self:AddEventListeners()
    self._txtLuckNum.text = GlobalParamsHelper.GetParamValue(860)
    
    local leftTime = CardLuckManager:GetLeftTime()
    if leftTime > 0 then
        if self._timer then
            TimerHeap:DelTimer(self._timer)
            self._timer = nil
        end
        self:UpdateTimeLeft()
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnBackToActivityTime() end)
    else
        EventDispatcher:TriggerEvent(GameEvents.UpdateLuckCard)
        self._txtLuckTime.text = LanguageDataHelper.CreateContent(76951)
    end
end



-- function FriendPanel:OnClose()
--     self._views[self._select]:CloseViewEx()
--     self._select = nil
--     --self._friendView:SetActive(false)
--     self._views[1].transform.localScale = HideScale
-- end


function CardLuckPanel:UpdateTimeLeft()
    self._leftTime = CardLuckManager:GetLeftTime()
    local argsTableRank = LanguageDataHelper.GetArgsTable()
    argsTableRank["0"] = DateTimeUtil.FormatParseTimeStr(self._leftTime)
    self._txtLuckTime.text = LanguageDataHelper.CreateContent(81547, argsTableRank)
end

function CardLuckPanel:OnBackToActivityTime()
    if (self._leftTime > 0) then
        self:UpdateTimeLeft()
    else
        EventDispatcher:TriggerEvent(GameEvents.UpdateLuckCard)
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
        self._txtLuckTime.text = LanguageDataHelper.CreateContent(76951)
    end
end

function CardLuckPanel:SetData(data)
    self:InitData()
end

function CardLuckPanel:InitData()
    local positionList = {}
    for i=1,10 do
        local postItem = {_posId = i}
        table.insert( positionList, postItem)
    end
    self._cardList:SetDataList(positionList)    
    
    local ids = OpenCardRewardDataHelper:GetAllId()
    local idsBack = {}
    for i=#ids,1,-1 do
        table.insert(idsBack,ids[i])
    end
    self._cardTitleList:SetDataList(idsBack)

    local ids = OpenCardRewardDataHelper:GetAllId()
    self._cardRewardList:SetDataList(ids)
end

function CardLuckPanel:ShowView(index, data)

end



function CardLuckPanel:WinBGType()
    return PanelWinBGType.RedNewBG
end
