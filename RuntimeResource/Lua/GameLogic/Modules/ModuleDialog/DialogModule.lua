-- DialogModule.lua
DialogModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function DialogModule.Init()
    GUIManager.AddPanel(PanelsConfig.NPCDialog, "Panel_NPCDialog", GUILayer.LayerUIPanel, "Modules.ModuleDialog.NPCDialogPanel", true, PanelsConfig.EXTEND_TO_FIT)
end

return DialogModule

