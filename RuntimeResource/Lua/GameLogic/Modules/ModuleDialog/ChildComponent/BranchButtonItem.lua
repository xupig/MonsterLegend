-- BranchButtonItem.lua
local BranchButtonItem = Class.BranchButtonItem(ClassTypes.BaseLuaUIComponent)

BranchButtonItem.interface = GameConfig.ComponentsConfig.Component
BranchButtonItem.classPath = "Modules.ModuleDialog.ChildComponent.BranchButtonItem"

function BranchButtonItem:Awake()
    self._textBranch = self:GetChildComponent("Button_Branch/Text", "TextMeshWrapper")
    self._btnBranch = self:FindChildGO("Button_Branch")
    self._csBH:AddClick(self._btnBranch, function()self:BtnClick() end)
end

function BranchButtonItem:OnDestroy()
    self._textBranch = nil
    self._btnBranch = nil
end

function BranchButtonItem:SetTaskIdAndName(taskId, name)
    self._taskId = taskId
    self._textBranch.text = name
end

function BranchButtonItem:BtnClick()
    EventDispatcher:TriggerEvent(GameEvents.OnBranchButtonClick, self._taskId)
end
