-- DialogContentView.lua
local DialogContentView = Class.DialogContentView(ClassTypes.BaseComponent)

DialogContentView.interface = GameConfig.ComponentsConfig.Component
DialogContentView.classPath = "Modules.ModuleDialog.ChildView.DialogContentView"
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local TaskRewardItem = ClassTypes.TaskRewardItem

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local XImageTweenAlpha = GameMain.XImageTweenAlpha
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale
local Vector3 = Vector3
-- local UIList = ClassTypes.UIList
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local ActorModelComponent = GameMain.ActorModelComponent
local PlayerModelComponent = GameMain.PlayerModelComponent
local PlayerModelType = GameConfig.EnumType.PlayerModelType
local Scale_Hide = Vector3.New(0, 1, 1)
local Scale_Display = Vector3.one

function DialogContentView:Awake()
    self:InitLeft()
    self:InitLeftDialog()
    self:InitConfirm()
    self:InitRight()
end

function DialogContentView:OnEnable()
    GameWorld.ShowPlayer():ShowModel(PlayerModelType.ModelAndNoHead)
end

function DialogContentView:OnDisable()
    GameWorld.ShowPlayer():ShowModel(PlayerModelType.DynamicHead)
end

function DialogContentView:InitLeft()
    self._containerLeft = self:FindChildGO("Container_Left").transform
    self._containerLeftState = nil
    self._textLeftContent = self:GetChildComponent("Container_Left/Container_Content/Text_Content", "TextMeshWrapper")
    self._containerLeftModel = self:FindChildGO("Container_Left/Container_NPC/Container_Model")
    self._textLeftNPCName = self:GetChildComponent("Container_Left/Container_NPC/Text_Name", "TextMeshWrapper")
    self._leftPlayerComponent = self:AddChildComponent('Container_Left/Container_NPC/Container_Player', PlayerModelComponent)
    self._leftModelComponent = self:AddChildComponent('Container_Left/Container_NPC/Container_Model', ActorModelComponent)
    self._leftModelComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 0.8)
end

function DialogContentView:InitLeftDialog()
    self._containerLeftDialog = self:FindChildGO("Container_LeftDialog").transform
    self._containerLeftDialogState = nil
    self._txtLeftDialogCountDown = self:GetChildComponent("Container_LeftDialog/Text_CountDown", "TextMeshWrapper")
    local imgLeftDialogArrow = self:FindChildGO("Container_LeftDialog/Container_Next/Image_Arrow")
    self._imgLeftDialogArrowPos = imgLeftDialogArrow:AddComponent(typeof(XGameObjectTweenPosition))
    local imgLeftDialogShadow = self:FindChildGO("Container_LeftDialog/Container_Next/Image_Shadow")
    self._imgLeftDialogShadowAlpha = imgLeftDialogShadow:AddComponent(typeof(XImageTweenAlpha))
    self._curTxtCountDown = self._txtRightDialogCountDown
end

function DialogContentView:InitConfirm()
    self._containerConfirm = self:FindChildGO("Container_Confirm").transform
    self._containerConfirmState = nil
    self._containerItems = self:FindChildGO("Container_Confirm/Container_Items").transform
    self._containerItemsState = nil
    self._txtConfirmCountDown = self:GetChildComponent("Container_Confirm/Text_CountDown", "TextMeshWrapper")
    self._textConfirmText = self:GetChildComponent("Container_Confirm/Button_Confirm/Text", "TextMeshWrapper")
    local btnConfirm = self:FindChildGO("Container_Confirm/Button_Confirm")
    self._csBH:AddClick(btnConfirm, function()self:ConfirmBtnClick() end)

    self._tipsCongtainer = self:FindChildGO("Container_Confirm/Container_Tips")
    self._kuangImage = self:FindChildGO("Container_Confirm/Container_Tips/Image_Kuang")
    self._kuangImageScale = self._kuangImage:AddComponent(typeof(XGameObjectTweenScale))
    self._jianTouImage = self:FindChildGO("Container_Confirm/Container_Tips/Image_Jiantou")
    self._jianTouImagePos = self._jianTouImage:AddComponent(typeof(XGameObjectTweenPosition))
    self._jianTouInitPosition = self._jianTouImage.transform.localPosition

    self:InitList()
end

function DialogContentView:InitRight()
    self._containerRight = self:FindChildGO("Container_Right").transform
    self._containerRightState = nil
    self._textRightContent = self:GetChildComponent("Container_Right/Container_Content/Text_Content", "TextMeshWrapper")
    -- self._containerRightModel = self:FindChildGO("Container_Right/Container_NPC/Container_Model")
    self._textRightNPCName = self:GetChildComponent("Container_Right/Container_NPC/Text_Name", "TextMeshWrapper")
    self._rightPlayerComponent = self:AddChildComponent('Container_Right/Container_NPC/Container_Player', PlayerModelComponent)
    self._rightPlayerComponent:SetStartSetting(Vector3(0, 0, 0), Vector3(0, 180, 0), 0.8)
    self._rightModelComponent = self:AddChildComponent('Container_Right/Container_NPC/Container_Model', ActorModelComponent)
    self._rightModelComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 0.8)
    self._txtRightDialogCountDown = self:GetChildComponent("Container_Right/Text_CountDown", "TextMeshWrapper")
    local imgRightDialogArrow = self:FindChildGO("Container_Right/Container_Next/Image_Arrow")
    self._imgRightDialogArrowPos = imgRightDialogArrow:AddComponent(typeof(XGameObjectTweenPosition))
    local imgRightDialogShadow = self:FindChildGO("Container_Right/Container_Next/Image_Shadow")
    self._imgRightDialogShadowAlpha = imgRightDialogShadow:AddComponent(typeof(XImageTweenAlpha))

end

function DialogContentView:InitList()
    self._gridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Confirm/Container_Items")
    self._gridLauoutGroup:SetItemType(TaskRewardItem)
-- self._gridLauoutGroup:SetDataList(valueDatas)
-- local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Confirm/Container_Items/ScrollViewList")
-- self._list = list
-- self._scrollView = scrollView
-- self._scrollView:SetHorizontalMove(true)
-- self._scrollView:SetVerticalMove(false)
-- self._list:SetItemType(TaskRewardItem)
-- self._list:SetPadding(0, 0, 0, 0)
-- self._list:SetGap(0, 0)
-- self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function DialogContentView:ConfirmBtnClick()
    -- LoggerHelper.Log("Ash: ConfirmBtnClick:" .. tostring(self._confirmCB ~= nil))
    if self._confirmCB then
        self._confirmCB()
    -- self._confirmCB = nil
    end
end

function DialogContentView:SetBtnClose(btnClose)
    -- LoggerHelper.Log("Ash: ConfirmBtnClick:" .. tostring(self._confirmCB ~= nil))
    self._btnClose = btnClose
end

function DialogContentView:SetCownDownActive(flag)
    if self._curTxtCountDown then
        self._curTxtCountDown.gameObject:SetActive(flag)
    end
end

function DialogContentView:SetCownDownText(text)
    if self._curTxtCountDown then
        self._curTxtCountDown.text = text
    end
end

-- 设置可见
function DialogContentView:SetContainerLeftVisible(state)
    if self._containerLeftState ~= state then
        self._containerLeft.localScale = state and Scale_Display or Scale_Hide        
        self._containerLeftState = state
    end
end

function DialogContentView:SetContainerLeftDialogVisible(state)
    if self._containerLeftDialogState ~= state then
        self._containerLeftDialog.localScale = state and Scale_Display or Scale_Hide        
        self._containerLeftDialogState = state
    end
end

function DialogContentView:SetContainerConfirmVisible(state)
    if self._containerConfirmState ~= state then
        self._containerConfirm.localScale = state and Scale_Display or Scale_Hide        
        self._containerConfirmState = state
        if GlobalParamsHelper.GetParamValue(986) > GameWorld.Player().level then
            if state then
                self._tipsCongtainer:SetActive(true)
                self._kuangImageScale:SetFromToScale(1.2, 1, 0.25, 4, nil)
                self._jianTouImagePos:SetFromToPos(self._jianTouInitPosition , self._jianTouInitPosition- Vector3.New(20,0,0), 0.25, 4, nil)
            else
                self._tipsCongtainer:SetActive(false)
            self._kuangImageScale.IsTweening = false
            self._jianTouImagePos.IsTweening = false
            end
        else
            self._tipsCongtainer:SetActive(false)
            self._kuangImageScale.IsTweening = false
            self._jianTouImagePos.IsTweening = false
        end
    end
end

function DialogContentView:SetContainerItemsVisible(state)
    if self._containerItemsState ~= state then
        self._containerItems.localScale = state and Scale_Display or Scale_Hide        
        self._containerItemsState = state
        if state then

        else

        end
    end
end

function DialogContentView:SetContainerRightVisible(state)
    if self._containerRightState ~= state then
        self._containerRight.localScale = state and Scale_Display or Scale_Hide        
        self._containerRightState = state
    end
end



-- function DialogContentView:SetMainMode(dialog, confirmCB)
--     self._confirmCB = confirmCB
--     self:ShowDialog(dialog)
--     self._containerNext:SetActive(false)
--     self:SetContainerItemsVisible(false)
--     self._btnConfirm:SetActive(false)
--     self._btnComplete:SetActive(false)
--     self._containerNext:SetActive(false)
--     self._containerHead:SetActive(false)
-- end
function DialogContentView:SetLeftDialogMode(dialog, confirmCB)
    self._isLeftMode = true
    -- self._btnClose:SetActive(true)
    self:SetContainerLeftVisible(true)
    self:SetContainerLeftDialogVisible(true)
    self:SetContainerConfirmVisible(false)
    self:SetContainerRightVisible(false)
    self._curTxtNPCName = self._textLeftNPCName
    self._curTxtCountDown = self._txtLeftDialogCountDown
    self._confirmCB = confirmCB
    self:ShowDialog(self._textLeftContent, dialog)
    self._imgLeftDialogShadowAlpha:SetFromToAlpha(0.25, 1, 0.5, 4)--PingPong = 4
    self._imgLeftDialogArrowPos:SetFromToPos(Vector3(2, 20, 0), Vector3(2, 7, 0), 0.5, 4, nil)--PingPong = 4
end

function DialogContentView:SetRightDialogMode(dialog, confirmCB)
    self._isLeftMode = false
    -- self._btnClose:SetActive(true)
    self:SetContainerLeftVisible(false)
    self:SetContainerLeftDialogVisible(false)
    self:SetContainerConfirmVisible(false)
    self:SetContainerRightVisible(true)
    self._curTxtNPCName = self._textRightNPCName
    self._curTxtCountDown = self._txtRightDialogCountDown
    self._confirmCB = confirmCB
    self:ShowDialog(self._textRightContent, dialog)
    self._imgRightDialogShadowAlpha:SetFromToAlpha(0.25, 1, 0.5, 4)--PingPong = 4
    self._imgRightDialogArrowPos:SetFromToPos(Vector3(2, 20, 0), Vector3(2, 7, 0), 0.5, 4, nil)--PingPong = 4
end

function DialogContentView:SetFeedbackMode(dialog, confirmText, confirmCB)
    self._isLeftMode = true
    -- self._btnClose:SetActive(false)
    self:SetContainerLeftVisible(true)
    self:SetContainerLeftDialogVisible(false)
    self:SetContainerConfirmVisible(true)
    self:SetContainerRightVisible(false)
    self._curTxtNPCName = self._textLeftNPCName
    self._curTxtCountDown = self._txtConfirmCountDown
    self._confirmCB = confirmCB
    self:ShowDialog(self._textLeftContent, dialog)
    self:SetContainerItemsVisible(false)
    self._textConfirmText.text = LanguageDataHelper.CreateContent(confirmText)
end

function DialogContentView:SetItemsMode(dialog, itemsData, confirmText, confirmCB)
    self._isLeftMode = true
    local isSkip = self:ShowDialog(self._textLeftContent, dialog)
    if isSkip then
        if confirmCB then
            confirmCB()
            return
        end
    end
    -- self._btnClose:SetActive(false)
    self:SetContainerLeftVisible(true)
    self:SetContainerLeftDialogVisible(false)
    self:SetContainerConfirmVisible(true)
    self:SetContainerRightVisible(false)
    self._curTxtNPCName = self._textLeftNPCName
    self._curTxtCountDown = self._txtConfirmCountDown
    self._confirmCB = confirmCB
    -- self:ShowDialog(self._textRightContent, dialog)
    if itemsData ~= nil then
        self:SetContainerItemsVisible(true)
        self._gridLauoutGroup:SetDataList(itemsData)
    else
        self:SetContainerItemsVisible(false)
    end
    self._textConfirmText.text = LanguageDataHelper.CreateContent(confirmText)
end

function DialogContentView:SetConfirmMode(dialog, itemsData, confirmText, confirmCB)
    self._isLeftMode = true
    -- self._btnClose:SetActive(false)
    self:SetContainerLeftVisible(true)
    self:SetContainerLeftDialogVisible(false)
    self:SetContainerConfirmVisible(true)
    self:SetContainerRightVisible(false)
    self._curTxtNPCName = self._textLeftNPCName
    self._curTxtCountDown = self._txtConfirmCountDown
    self._confirmCB = confirmCB
    self:ShowDialog(self._textLeftContent, dialog)
    if itemsData ~= nil then
        self:SetContainerItemsVisible(true)
        self._gridLauoutGroup:SetDataList(itemsData)
    else
        self:SetContainerItemsVisible(false)
    end
    self._textConfirmText.text = LanguageDataHelper.CreateContent(confirmText)
end

function DialogContentView:ShowDialog(textContent, dialog)
    -- LoggerHelper.Log("Ash: DialogContentView:ShowDialog " .. dialog[1])
    if dialog[1] ~= nil then -- 内容填0，则作为跳过直接完成
        if dialog[1] == 0 then
            return true
        end
        textContent.gameObject:SetActive(true)
        local argsTable = LanguageDataHelper.GetArgsTable()
        if dialog[3] then
            argsTable["0"] = dialog[3]
        end
        textContent.text = LanguageDataHelper.CreateContent(dialog[1], argsTable)
    else
        textContent.gameObject:SetActive(false)
    end
    if dialog[2] ~= nil and dialog[2] ~= 0 then
        if self._dialogVoice ~= nil then
            GameWorld.StopSound(self._dialogVoice)
        end
        self._dialogVoice = GameWorld.PlaySound(dialog[2])
    end
end

function DialogContentView:SetNpc(npcId)
    if npcId == 999 then -- 999代表自己
        if self._curTxtNPCName then
            self._curTxtNPCName.text = GameWorld.Player().name
        end
        if self._isLeftMode then
            -- LoggerHelper.Log("Ash:DialogContentView SetNpc " .. tostring(npcId))
            self._leftPlayerComponent.gameObject:SetActive(true)
            self._leftModelComponent.gameObject:SetActive(false)
            self._leftPlayerComponent:SetStartSetting(Vector3(0, 0, 0), Vector3(0, 180, 0), 0.8)
        else
            self._rightPlayerComponent.gameObject:SetActive(true)
            self._rightModelComponent.gameObject:SetActive(false)
            self._rightPlayerComponent:SetStartSetting(Vector3(0, 0, 0), Vector3(0, 180, 0), 0.8)
        end
    else
        local npcData = NPCDataHelper.GetNPCData(npcId)
        if self._curTxtNPCName then
            self._curTxtNPCName.text = LanguageDataHelper.CreateContent(npcData.name)
        end
        if self._isLeftMode then
            self._leftPlayerComponent.gameObject:SetActive(false)
            self._leftModelComponent.gameObject:SetActive(true)
            if npcData.npc_distinguish ~= 0 then
                self._leftModelComponent:LoadNpcModel(npcData.model, npcData.npc_facade, true, npcData.npc_distinguish)
            else
                self._leftModelComponent:LoadModel(npcData.model)
            end
        else
            self._rightPlayerComponent.gameObject:SetActive(false)
            self._rightModelComponent.gameObject:SetActive(true)
            if npcData.npc_distinguish ~= 0 then
                self._rightModelComponent:LoadNpcModel(npcData.model, npcData.npc_facade, true, npcData.npc_distinguish)
            else
                self._rightModelComponent:LoadModel(npcData.model)
            end
       end
    end
-- local npcs = GameSceneManager:GetNPCs()
-- local npcEId = nil
-- -- LoggerHelper.Log("Ash: TurnToNpc:" .. #npcs)
-- for i, npc in pairs(npcs) do
--     -- LoggerHelper.Log("Ash: TurnToNpc for npc.npc_id:" .. npc.npc_id)
--     if npc.npc_id == npcId then
--         -- LoggerHelper.Log("Ash: TurnToNpc npc.id:" .. npc.id)
--         npcEId = npc.id
--     end
-- end
-- if npcEId then
--     local npcCamera = NPCDataHelper.GetNPCCamera(npcId)
--     local distance = npcCamera[1]
--     local height = npcCamera[2]
--     GameWorld.FaceToNPC(npcEId, distance, height)
-- end
end



function DialogContentView:AutoDoNext()
    -- LoggerHelper.Log("Ash: AutoDoNext:" .. tostring(self._confirmCB ~= nil))
    if self._confirmCB then
        self._confirmCB()
    -- self._confirmCB = nil
    end
end

function DialogContentView:OnDestroy()
    self._textContent = nil
    self._containerNext = nil
    self._containerItems = nil
    self._btnComplete = nil
    self._textConfirmText = nil
    self._textCancelText = nil
    self._imgArrowPos = nil
    self._imgShadowAlpha = nil
end
