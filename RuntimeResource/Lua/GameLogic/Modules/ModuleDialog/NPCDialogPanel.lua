-- NPCDialogPanel.lua
local NPCDialogPanel = Class.NPCDialogPanel(ClassTypes.BasePanel)

require "Modules.ModuleDialog.ChildView.DialogContentView"
local DialogContentView = ClassTypes.DialogContentView
local TaskManager = PlayerManager.TaskManager
local BagManager = PlayerManager.BagManager
local public_config = GameWorld.public_config
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ButtonWrapper = UIExtension.ButtonWrapper
local ActorModelComponent = GameMain.ActorModelComponent
local DialogConfig = GameConfig.DialogConfig
local UIComponentUtil = GameUtil.UIComponentUtil
local NPCDataHelper = GameDataHelper.NPCDataHelper
local ChineseVoiceDataHelper = GameDataHelper.ChineseVoiceDataHelper
local DialogData = ClassTypes.DialogData
local FontStyleHelper = GameDataHelper.FontStyleHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local EscortPeriManager = PlayerManager.EscortPeriManager
local CircleTaskManager = PlayerManager.CircleTaskManager
local GUIManager = GameManager.GUIManager
local WeekTaskManager = PlayerManager.WeekTaskManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function NPCDialogPanel:Awake()
    self.disableCameraCulling = true
    self:Init()
    self:InitCallbackFunc()
end

function NPCDialogPanel:OnShow(data)
    GUILayerManager.HideLayer(GUILayer.LayerUIMain)
    GUILayerManager.HideLayer(GUILayer.LayerUIMiddle)
    self._npcId = data
    self:ShowMain()
    self:AddEventListener()
end

function NPCDialogPanel:OnClose()
    GUILayerManager.ShowLayer(GUILayer.LayerUIMain)
    GUILayerManager.ShowLayer(GUILayer.LayerUIMiddle)
    self:RemoveEventListener()
    self._dialogData = nil
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function NPCDialogPanel:InitCallbackFunc()
    self._onClosePanel = function()self:ClosePanel() end
end

function NPCDialogPanel:AddEventListener()
end

function NPCDialogPanel:RemoveEventListener()
end

function NPCDialogPanel:OnDestroy()
    self._dialogContentView = nil
    self._textNpcName = nil
    self._dialogData = nil
    self._onClosePanel = nil
end

function NPCDialogPanel:Init()
    self:InitView()
end

--初始化
function NPCDialogPanel:InitView()
    self._dialogContentView = self:AddChildLuaUIComponent("Container_Dialog", DialogContentView)
    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    self._dialogContentView:SetBtnClose(self._btnClose)
end

function NPCDialogPanel:ShowMain()
    local curEscortPeriTask, curEscortPeriTaskEvenItem = EscortPeriManager:GetTaskInfo(self._npcId)
    --LoggerHelper.Error("curEscortPeriTask ====" .. tostring(curEscortPeriTask) )
    if curEscortPeriTask then
        if self:CheckNPC(curEscortPeriTask, curEscortPeriTaskEvenItem) then
            self:HandleTask(curEscortPeriTask, curEscortPeriTaskEvenItem)
            return
        end
    end
    
    local curAutoTask, curEvenItem = TaskCommonManager:GetCurAutoTask()
    if curAutoTask then
        if self:CheckNPC(curAutoTask, curEvenItem) then
            self:HandleTask(curAutoTask, curEvenItem)
            return
        end
    end
    
    local taskList, eventItemList = TaskManager:GetNPCTasks(self._npcId)
    local npcData = NPCDataHelper.GetNPCData(self._npcId)
    local taskCount = 0
    for k, v in pairs(taskList) do
        taskCount = taskCount + 1
    end
    -- LoggerHelper.Log("Ash:ShowMain taskCount " .. taskCount)
    for k, v in pairs(taskList) do
        if self:CheckNPC(v, eventItemList[k]) then
            self:HandleTask(v, eventItemList[k])
            return
        end
    end
    
    
    local curCircleTask, curCircleTaskEvenItem = CircleTaskManager:GetCircleTaskInfo(self._npcId)
    if curCircleTask then
        if self:CheckNPC(curCircleTask, curCircleTaskEvenItem) then
            self:HandleTask(curCircleTask, curCircleTaskEvenItem)
            return
        end
    end
    
    local weekTask, weekTaskEvenItem = WeekTaskManager:GetCircleTaskInfo(self._npcId)
    if curCircleTask then
        if self:CheckNPC(curCircleTask, curCircleTaskEvenItem) then
            self:HandleTask(curCircleTask, curCircleTaskEvenItem)
            return
        end
    end
    
    self:ShowNPCCommonDialog()
end

function NPCDialogPanel:ShowNPCCommonDialog()
    self._lastDialogType = nil --清空对话框类型，用于识别关闭状态
    
    if self._npcId == GlobalParamsHelper.GetParamValue(714) then
        --请求结婚
        EventDispatcher:TriggerEvent(GameEvents.SHOW_GET_MARRY, self._npcId)
        self:ClosePanel()
        return
    end
    local npcData = NPCDataHelper.GetNPCData(self._npcId)
    local dialogs = npcData.dialog_txt
    if dialogs == nil then
        self:ClosePanel()
        return
    end
    local index = math.random(1, #dialogs)
    local dialogAndVoice = {}
    table.insert(dialogAndVoice, dialogs[index])
    table.insert(dialogAndVoice, ChineseVoiceDataHelper:GetVoice(dialogs[index]))
    self._dialogContentView:SetLeftDialogMode(dialogAndVoice)
    self._dialogContentView:SetNpc(self._npcId)
    self._dialogContentView:SetCownDownActive(false)
end

function NPCDialogPanel:CheckNPC(task, eventItemData)
    local npcId = TaskCommonManager:GetTaskNPCId(task, eventItemData)
    return npcId == self._npcId
end

function NPCDialogPanel:HandleTask(task, eventItemData)
    -- LoggerHelper.Log("Ash:NPCDialogPanel HandleTask " .. PrintTable:TableToStr(task))
    -- LoggerHelper.Log("Ash:NPCDialogPanel eventItemData " .. PrintTable:TableToStr(eventItemData))
    if task ~= nil then
        -- local taskType = task:GetTaskType()
        local state = task:GetState()
        if state == public_config.TASK_STATE_ACCEPTABLE or state == public_config.TASK_STATE_COMPLETED then
            self:NPCDoTask(task)
        else
            local eventID = eventItemData:GetEventID()
            if eventID == public_config.EVENT_ID_GIVE_ITEM_TO_NPC then
                self:NPCDoHandOverTask(task, self._npcId, eventItemData)
            elseif eventID == public_config.EVENT_ID_CLIENT_NPC_DIALOG_FINISH then
                self:NPCDoDialogTask(task, self._npcId, eventItemData)
            elseif eventID == public_config.EVENT_ID_PASS_INST then
                self:NPCDoEnterInstTask(task, self._npcId, eventItemData)
            end
        end
    end
end

function NPCDialogPanel:UpdateTimeLeft()
    local leftTime = self._reviveTime - DateTimeUtil:GetServerTime()
    self._dialogContentView:SetCownDownText(leftTime .. LanguageDataHelper.CreateContent(10569))
end

function NPCDialogPanel:OnCountDownTick()
    if (self._reviveTime - DateTimeUtil:GetServerTime() > 0) then
        self:UpdateTimeLeft()
    else
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
        if self._curCountDownTickCB then
            self._curCountDownTickCB()
        end
    end
end

function NPCDialogPanel:ShowNext()
    local curDialog = self._dialogData:GetNext()
    if curDialog ~= nil then
        if self._timer then
            TimerHeap:DelTimer(self._timer)
            self._timer = nil
        end
        if curDialog:GetIsAutoTask() then
            self._dialogContentView:SetCownDownActive(true)
            self._reviveTime = DateTimeUtil:GetServerTime() + 5
            self:UpdateTimeLeft()
            self._curCountDownTickCB = function()self._dialogContentView:AutoDoNext() end
            self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnCountDownTick() end)
        else
            self._dialogContentView:SetCownDownActive(false)
        end
        -- LoggerHelper.Log("Ash:NPCDialogPanel ShowNext " .. PrintTable:TableToStr(curDialog))
        local dialogType = curDialog:GetType()
        self._lastDialogType = dialogType
        local npcId = curDialog:GetTurnToNpc()
        if dialogType == DialogConfig.DIALOG_MODE then
            local confirmCB = function()self:ShowNext(); end
            if npcId == 999 then --自己说话
                self._dialogContentView:SetRightDialogMode(curDialog:GetDialog(), confirmCB)
            else
                self._dialogContentView:SetLeftDialogMode(curDialog:GetDialog(), confirmCB)
            end
        elseif dialogType == DialogConfig.HAND_OVER_MODE then
            local confirmCB = function()curDialog:DoCallback(); self:ShowNext(); end
            self._dialogContentView:SetItemsMode(curDialog:GetDialog(), curDialog:GetItemsData(), curDialog:GetConfirmText(), confirmCB)
        elseif dialogType == DialogConfig.REWARD_MODE then
            local confirmCB = function()curDialog:DoCallback(); self:ShowNext(); end
            self._dialogContentView:SetItemsMode(curDialog:GetDialog(), curDialog:GetItemsData(), curDialog:GetConfirmText(), confirmCB)
        elseif dialogType == DialogConfig.CONFIRM_MODE then
            local confirmCB = curDialog:HasCallback() and function()curDialog:DoCallback(); self:ShowNext(); end or self._onClosePanel
            self._dialogContentView:SetConfirmMode(curDialog:GetDialog(), curDialog:GetItemsData(), curDialog:GetConfirmText(), confirmCB)
        elseif dialogType == DialogConfig.FEEDBACK_MODE then
            local confirmCB = function()self:ShowNext() end
            self._dialogContentView:SetFeedbackMode(curDialog:GetDialog(), curDialog:GetConfirmText(), confirmCB)
        elseif dialogType == DialogConfig.ENTER_INST_MODE then
            local confirmCB = function()curDialog:DoCallback(); self:ShowNext(); end
            self._dialogContentView:SetFeedbackMode(curDialog:GetDialog(), curDialog:GetConfirmText(), confirmCB)
        end
        -- LoggerHelper.Log("Ash:NPCDialogPanel SetNpc " .. tostring(npcId))
        self._dialogContentView:SetNpc(npcId or self._npcId)
    else
        self:ClosePanel()
    end
end

function NPCDialogPanel:GetDialogData(dialogData, dialog, specialAction, endAction)
    -- LoggerHelper.Log("Ash:NPCDialogPanel GetDialogData " .. PrintTable:TableToStr(dialog))
    local count = #dialog
    for i, v in ipairs(dialog) do
        local dialogAndVoice = {}
        table.insert(dialogAndVoice, v[2][1])
        table.insert(dialogAndVoice, ChineseVoiceDataHelper:GetVoice(v[2][1]))
        local dialogItem = nil
        if v[2][3] ~= nil and v[2][3] ~= 0 then --暂时非0就当接受任务
            if specialAction then
                dialogItem = specialAction(dialogAndVoice, dialogItem)
            end
        elseif v[2][4] ~= nil and v[2][4] ~= 0 then
            dialogItem = dialogData:AddFeedback(dialogAndVoice, v[2][4])
        else
            if i == count then
                if endAction then
                    dialogItem = endAction(dialogAndVoice, dialogItem)
                end
            else
                dialogItem = dialogData:AddDialog(dialogAndVoice)
            end
        end
        -- LoggerHelper.Log("Ash:dialogData dialogItem " .. tostring(dialogItem ~= nil))
        -- LoggerHelper.Log("Ash:dialogData SetTurnToNpc " .. tostring(v[2][5]))
        if dialogItem and v[2][5] ~= nil and v[2][5] ~= 0 then --转向单独的NPC
            dialogItem:SetTurnToNpc(v[2][5])
        end
    end
end

function NPCDialogPanel:NPCDoTask(task)
    local state = task:GetState()
    local dialogData = DialogData(task:IsAutoTask())
    if state == public_config.TASK_STATE_ACCEPTABLE then
        local npcData = NPCDataHelper.GetNPCData(task:GetAcceptNPC())
        local dialog = task:GetAcceptDialog()
        self:GetDialogData(dialogData, dialog,
            function(dialogAndVoice, dialogItem)
                -- local confirmCB = function(id)EventDispatcher:TriggerEvent(GameEvents.ACCEPT_TASK, id) end
                local confirmCB = task:GetAcceptTaskCallBack()
                dialogItem = dialogData:AddConfirm(dialogAndVoice, task:GetMyRewards(), confirmCB, task:GetId())
                return dialogItem
            end,
            function(dialogAndVoice, dialogItem)
                dialogItem = dialogData:AddLeave(dialogAndVoice)-- 最后一条提示离开
                return dialogItem
            end)
        self._dialogData = dialogData
        self:ShowTask()
    elseif state == public_config.TASK_STATE_COMPLETED then
        local npcData = NPCDataHelper.GetNPCData(task:GetCommitNPC())
        local dialog = task:GetCommitDialog()
        self:GetDialogData(dialogData, dialog,
            function(dialogAndVoice, dialogItem)
                -- local confirmCB = function(id)EventDispatcher:TriggerEvent(GameEvents.COMMIT_TASK, id) end
                local confirmCB = task:GetCommitTaskCallBack()
                -- LoggerHelper.Log("Ash:dialogData AddReward " .. PrintTable:TableToStr(dialogAndVoice))
                local dialogArgs = task:GetCommitDialogArgs()
                if dialogArgs then
                    table.insert(dialogAndVoice, dialogArgs)
                end
                dialogItem = dialogData:AddReward(dialogAndVoice, task:GetMyRewards(), confirmCB, task:GetId())
                return dialogItem
            end,
            function(dialogAndVoice, dialogItem)
                dialogItem = dialogData:AddLeave(dialogAndVoice)-- 最后一条提示离开
                return dialogItem
            end)
        self._dialogData = dialogData
        self:ShowTask()
    end
end

function NPCDialogPanel:NPCDoHandOverTask(task, npcID, eventItemData)
    local follow = eventItemData:GetFollow()
    local dialog = follow[npcID]
    if dialog ~= nil then
        local dialogData = DialogData(task:IsAutoTask())
        local count = #dialog
        for i, v in ipairs(dialog) do
            if v ~= 0 then
                local dialogAndVoice = {}
                table.insert(dialogAndVoice, v)
                table.insert(dialogAndVoice, ChineseVoiceDataHelper:GetVoice(v))
                if i == count then
                    local confirmCB = function(items)EventDispatcher:TriggerEvent(GameEvents.HAND_OVER_ITEM, items) end
                    dialogData:AddHandOver(dialogAndVoice, eventItemData:GetItem(), confirmCB, eventItemData:GetItem())
                else
                    dialogData:AddDialog(dialogAndVoice)
                end
            else
                LoggerHelper.Error("Ash:NPCDoHandOverTask dialog is zero: " .. task:GetId() .. " " .. PrintTable:TableToStr(eventItemData))
            end
        end
        self._dialogData = dialogData
        self:ShowTask()
    end
end

function NPCDialogPanel:NPCDoDialogTask(task, npcID, eventItemData)
    local follow = eventItemData:GetFollow()
    local dialog = follow[npcID]
    if dialog ~= nil then
        local dialogData = DialogData(task:IsAutoTask())
        local count = #dialog
        for i, v in ipairs(dialog) do
            local dialogAndVoice = {}
            table.insert(dialogAndVoice, v)
            table.insert(dialogAndVoice, ChineseVoiceDataHelper:GetVoice(v))
            if i == count then
                local confirmCB = function(eventItemData)EventDispatcher:TriggerEvent(GameEvents.TASK_DO_EVENT, eventItemData) end
                dialogData:AddLeave(dialogAndVoice, confirmCB, eventItemData)-- 最后一条提示离开
            else
                dialogData:AddDialog(dialogAndVoice)
            end
        end
        self._dialogData = dialogData
        self:ShowTask()
    end
end

function NPCDialogPanel:NPCDoEnterInstTask(task, npcID, eventItemData)
    local follow = eventItemData:GetFollow()
    if follow[1] then --npc对话进入副本
        local dialog = {}
        table.insert(dialog, follow[1][2])
        if not table.isEmpty(dialog) then
            local dialogData = DialogData(task:IsAutoTask())
            local count = #dialog
            for i, v in ipairs(dialog) do
                local dialogAndVoice = {}
                table.insert(dialogAndVoice, v)
                table.insert(dialogAndVoice, ChineseVoiceDataHelper:GetVoice(v))
                if i == count then
                    local confirmCB = function(eventItemData)TaskCommonManager:EnterInst(eventItemData) end
                    dialogData:AddEnterInst(dialogAndVoice, confirmCB, eventItemData)-- 最后一条出现按钮
                else
                    dialogData:AddDialog(dialogAndVoice)
                end
            end
            self._dialogData = dialogData
            self:ShowTask()
        end
    end
end

function NPCDialogPanel:ShowTask()
    self:ShowNext()
end

function NPCDialogPanel:CloseBtnClick()
    if self._lastDialogType == nil then -- 普通对话此值为空
        self:ClosePanel()
    elseif self._lastDialogType == DialogConfig.DIALOG_MODE then
        -- 按要求改成点空白地方都是下一步任务 功能 #4469
        self:ShowNext()
    else
        -- 【前端】任务流程领取奖励点击范围改为点任意位置即可 功能 #7514
        self._dialogContentView:ConfirmBtnClick()
    end
end

function NPCDialogPanel:ClosePanel()
    GUIManager.ClosePanel(GameConfig.PanelsConfig.NPCDialog)
-- GameWorld.ResetToNormal(true)
end

-- -- 是否有背景 0 没有  1 清晰底板  2 模糊底板  3 二级面板遮罩
function NPCDialogPanel:CommonBGType()
    return PanelBGType.NoBG
end

function NPCDialogPanel:HandlerBGEvent(BgGo)
    BgGo:AddComponent(typeof(ButtonWrapper))
    self._csBH:AddClick(BgGo, function()self:BgBtnClick() end)
end
