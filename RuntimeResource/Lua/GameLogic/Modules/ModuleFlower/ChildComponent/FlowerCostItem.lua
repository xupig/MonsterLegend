-- FlowerCostItem.lua
local FlowerCostItem = Class.FlowerCostItem(ClassTypes.BaseLuaUIComponent)
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
FlowerCostItem.interface = GameConfig.ComponentsConfig.Component
FlowerCostItem.classPath = "Modules.ModuleFlower.ChildComponent.FlowerCostItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local bagData = PlayerManager.PlayerDataManager.bagData

function FlowerCostItem:Awake()
    self._base.Awake(self)
    local itemGo = GUIManager.AddItem(self:FindChildGO("Container_ItemIcon"), 1)
    self._iconContainer = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._iconContainer:ActivateTipsById()
    self._TextName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._TextName.text = ''
end

function FlowerCostItem:OnDestroy()

end

--override
function FlowerCostItem:OnRefreshData(data)
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(data)
    self._iconContainer:SetItem(data)
    self._iconContainer:SetCountString(ownCount)
    self._TextName.text = ItemDataHelper.GetItemNameWithColor(data)
end

