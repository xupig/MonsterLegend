-- SendFlowerNameListItem.lua
local SendFlowerNameListItem = Class.SendFlowerNameListItem(ClassTypes.UIListItem)
SendFlowerNameListItem.interface = GameConfig.ComponentsConfig.Component
SendFlowerNameListItem.classPath = "Modules.ModuleFlower.ChildComponent.SendFlowerNameListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
require 'UIComponent.Extend.PlayerHead'
local PlayerHead = ClassTypes.PlayerHead
local RoleDataHelper = GameDataHelper.RoleDataHelper
function SendFlowerNameListItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function SendFlowerNameListItem:InitView()
    self._txtVip = self:GetChildComponent("Text_Vip", 'TextMeshWrapper')
    self._txtLevel = self:GetChildComponent("Text_Level", 'TextMeshWrapper')

    self._containerHead = self:AddChildLuaUIComponent('Container_Icon', PlayerHead)
	self._txtInfo = self:GetChildComponent("Text_Info",'TextMeshWrapper')
end
function SendFlowerNameListItem:OnDestroy()

end

function SendFlowerNameListItem:OnRefreshData(data)
    if data then
        if data[1] then
            self._txtInfo.text = data[1]
        else
            self._txtInfo.text = ''
        end
        self._txtVip.text = data[4]
        local icon = RoleDataHelper.GetHeadIconByVocation(data[6])
        self._containerHead:SetPlayerHeadData(data[1],data[5], icon, function()end,data[7])
    end
end

