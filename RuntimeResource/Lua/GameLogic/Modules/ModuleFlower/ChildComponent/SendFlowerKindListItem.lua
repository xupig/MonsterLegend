-- SendFlowerKindListItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require 'UIComponent.Extend.PlayerHead'
local SendFlowerKindListItem = Class.SendFlowerKindListItem(ClassTypes.UIListItem)
SendFlowerKindListItem.interface = GameConfig.ComponentsConfig.Component
SendFlowerKindListItem.classPath = "Modules.ModuleFlower.ChildComponent.SendFlowerKindListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local BagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local PlayerHead = ClassTypes.PlayerHead
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
function SendFlowerKindListItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function SendFlowerKindListItem:InitView()
    self._txtCount = self:GetChildComponent("Text_Count", 'TextMeshWrapper')
    self._txtName = self:GetChildComponent("Text_Name", 'TextMeshWrapper')

    self._containerItem = self:FindChildGO("Container_Icon")
    local a = GUIManager.AddItem(self._containerItem, 1)
    self._item = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    -- self._item:ActivateTipsById()
end
function SendFlowerKindListItem:OnDestroy()

end

function SendFlowerKindListItem:OnRefreshData(data)
    if data then
        self._txtName.text = data[1]
        self._txtCount.text = LanguageDataHelper.GetContent(80224)..BagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(data[4])
        self._item:SetItem(data[4])
    end
end

