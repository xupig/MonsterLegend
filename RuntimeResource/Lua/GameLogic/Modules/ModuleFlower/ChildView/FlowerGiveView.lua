require "Modules.ModuleFlower.ChildComponent.FlowerCostItem"
require "Modules.ModuleFlower.ChildComponent.SendFlowerNameListItem"
require "Modules.ModuleFlower.ChildComponent.SendFlowerKindListItem"
require "UIComponent.Extend.TipsCom"

local FlowerGiveView = Class.FlowerGiveView(ClassTypes.BaseLuaUIComponent)
FlowerGiveView.interface = GameConfig.ComponentsConfig.Component
FlowerGiveView.classPath = "Modules.ModuleFlower.ChildView.FlowerGiveView"
local StringStyleUtil = GameUtil.StringStyleUtil
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local public_config = GameWorld.public_config
local SendFlowerNameListItem = ClassTypes.SendFlowerNameListItem
local SendFlowerKindListItem = ClassTypes.SendFlowerKindListItem
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local FlowerManager = PlayerManager.FlowerManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local FlowerCostItem = ClassTypes.FlowerCostItem
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggle = ClassTypes.UIToggle
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FlowerData = PlayerManager.PlayerDataManager.flowerData
local FriendData = PlayerManager.PlayerDataManager.friendData
local bagData = PlayerManager.PlayerDataManager.bagData
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local TipsCom = ClassTypes.TipsCom

function FlowerGiveView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._kindExFlage = true
    self._nameExFlage = true
    self._togglestate = public_config.SEND_FLOWER_WAY_NAME
    self._prekind = 0
    self._preNum = 1
    self._isInputName = false
    self:InitFunCB()
    self:InitViews()
end

function FlowerGiveView:ShowView(data)
    self:AddEventListeners()
    self:UpdateFriends()
    if data then    
        self:UpdataKindInfo(self._prekind) 
        if data.isinput then
            self._sendflowername = data.name
            self._textName.text = self._sendflowername
            local d={}
            d={["0"]=self._sendflowername,["1"]=self._flowercfg[3],["2"]=self._flowercfg[3],["3"]=self._flowercfg[1]}
            self:UpdateTips(d)
        else
            if data.dbid==GameWorld.Player().dbid then
                data.name = GameWorld.Player().name
            end
            self:SetSendName(data.dbid,data.name)
        end
        if self._togglestate == public_config.SEND_FLOWER_WAY_NAME then
            self._checkToggle:SetActive(false)
            self._checkToggleBg:SetActive(true)
        else
            self._togglestate = public_config.SEND_FLOWER_WAY_HIDE
            self._checkToggle:SetActive(true)            
            self._checkToggleBg:SetActive(false)            
        end
    else
        self._togglestate = public_config.SEND_FLOWER_WAY_HIDE
        self:UpdataKindInfo(0)
        local item = self._listNameData[1]
        self:SetSendName(item[2],item[1])
        self:UpdataToggle(item[1])
    end
    self._nameExFlage=false
    self._kindExFlage=false
    self:UpdtaBtnName(false)
    self:UpdataBtnKind(false)
end

function FlowerGiveView:UpdateFriends()
    self._listNameData = {}
    if #FriendData:GetOnlineListForFriendList()==0 then
        local player = GameWorld.Player()
        local n = player.name
        self._isInputName = true
        self._textName.text = n
        self._sendflowername = n
        self._sendflowerdbid = player.dbid
        local itemdata={}
        itemdata[1]=player.name
        itemdata[2]=player.dbid
        itemdata[3]=0
        itemdata[4]="VIP"..player.vip_level
        itemdata[5]=StringStyleUtil.GetLevelString(player.level)
        itemdata[6]=player.vocation
        itemdata[7]=1
        table.insert(self._listNameData,itemdata) 
        self._listName:SetDataList(self._listNameData)
    else      
        local  d = FriendData:GetFriendListData()
        for i,v in pairs(d) do
            local itemdata={}
            itemdata[1]=v:GetName()
            itemdata[2]=v:GetDbid()
            itemdata[3]=v:GetFriendlyVal()
            itemdata[4]=v:GetVipStr()
            itemdata[5]=v:GetLevel()
            itemdata[6]=v:GetVocation()
            itemdata[7]=v:GetIsOnline()
           table.insert(self._listNameData,itemdata) 
        end
        local itemdata={}
        local player = GameWorld.Player()
        itemdata[1]=player.name
        itemdata[2]=player.dbid
        itemdata[3]=0
        itemdata[4]="VIP"..player.vip_level
        itemdata[5]=StringStyleUtil.GetLevelString(player.level)
        itemdata[6]=player.vocation
        itemdata[7]=1
        table.insert(self._listNameData,itemdata) 
        self._listName:RemoveAll()
        self._listName:SetDataList(self._listNameData)
    end
end

function FlowerGiveView:CloseView()
    self._kindExFlage = true
    self._nameExFlage = true
    self._preNum = 1
    self._isInputName = false
    self:RemoveEventListeners()
end

function FlowerGiveView:OnDestroy()
end

function FlowerGiveView:InitFunCB()
    self.btnGiveFun=function()self:onBtnGive()end
    self.updatecountFun=function()self:UpdateCount()end
    self.updateFriendFun = function()self:UpdateFriends()end 
    self.debtnFun = function()self:UpdateDeFun()end 
    self.addbtnFun = function()self:UpdateAddFun()end 
end

function FlowerGiveView:UpdateDeFun()
   self._preNum = self._preNum-1
   if self._preNum<1 then
        self._preNum=1
   end
   self._textNum.text = self._preNum
end

function FlowerGiveView:UpdateAddFun()
    self._preNum = self._preNum+1
    self._textNum.text = self._preNum
end
function FlowerGiveView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_UPDATE, self.updatecountFun)
    EventDispatcher:AddEventListener(GameEvents.FriendCountChance,self.updateFriendFun)
end

function FlowerGiveView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_UPDATE, self.updatecountFun)
    EventDispatcher:RemoveEventListener(GameEvents.FriendCountChance, self.updateFriendFun)    
end
function FlowerGiveView:UpdateCount()
    self._costitem:OnRefreshData(self._flowerid)
end

function FlowerGiveView:InitViews()
    local btn0 = self:FindChildGO("Container_Num/Button_Add")
    self._csBH:AddClick(btn0,self.addbtnFun)

    local btn1 = self:FindChildGO("Container_Num/Button_De")
    self._csBH:AddClick(btn1,self.debtnFun)

    local btnhelp = self:FindChildGO("Button_Help")
    self._csBH:AddClick(btnhelp,function()self:onBtnHelp()end)
    self._textNum = self:GetChildComponent('Container_Num/Text_Num','TextMeshWrapper')
    self._textNum.text=1

    self._inputFieldMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_Names/InputField")
    self._inputFieldMesh:SetOnEndMeshEditCB(function(text) self:OnEndEdit(text) end)
    self._inputText = self:GetChildComponent("Container_Names/InputField", 'InputFieldMeshWrapper')

    local itemGo = self:FindChildGO("Container_Item")
    self._costitem = UIComponentUtil.AddLuaUIComponent(itemGo, FlowerCostItem) 
    self._txttips = self:GetChildComponent("Text_Tips", "TextMeshWrapper")   

    local btngive = self:FindChildGO("Button_Give")
    self._csBH:AddClick(btngive,self.btnGiveFun)

    self._checkToggle= self:FindChildGO("Toggle_Name/Checkmark")
    self._checkToggleBg= self:FindChildGO("Toggle_Name/Background")
    
    self._toggleName = UIToggle.AddToggle(self.gameObject,"Toggle_Name")
    self._toggleName:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

    self._textName = self:GetChildComponent('Container_Names/InputField/Text Area/Text','TextMeshWrapper')
  
    local btnSele = self:FindChildGO("Container_Names/Button_Expand")
    self._ImgNameContract = self:FindChildGO("Container_Names/Button_Expand/Image_Contract")
    self._ImgNameExpand = self:FindChildGO("Container_Names/Button_Expand/Image_Expand")
    self._csBH:AddClick(btnSele,function() self:BtnNameSeleClick()end)

    self._containerscroName = self:FindChildGO("Container_Names/Container_List")
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Names/Container_List/ScrollViewList")
    self._listName = list
    self._scrollViewName = scrollView
    self._listName:SetItemType(SendFlowerNameListItem)
    self._listName:SetPadding(0, 0, 0, 0)
    self._listName:SetGap(0, 6)
    self._listName:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB =                
	function(idx)
		self:OnListNameItemClicked(idx)
	end
    self._listName:SetItemClickedCB(itemClickedCB)
    self._containerscroName:SetActive(false)

    self._textType = self:GetChildComponent('Container_Kinds/Text_FilterName','TextMeshWrapper')
    local btnSele = self:FindChildGO("Container_Kinds/Button_Expand")
    local btnkind = self:FindChildGO("Container_Kinds/Button")
    self._ImgKindContract = self:FindChildGO("Container_Kinds/Button/Image_Contract")
    self._ImgKindExpand = self:FindChildGO("Container_Kinds/Button/Image_Expand")
    self._csBH:AddClick(btnSele,function() self:BtnKindSeleClick()end)
    self._csBH:AddClick(btnkind,function() self:BtnKindSeleClick()end)

    self._containerscroKind = self:FindChildGO("Container_Kinds/Container_List")
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Kinds/Container_List/ScrollViewList")
    self._listKind = list
    self._scrollViewKind = scrollView
    self._listKind:SetItemType(SendFlowerKindListItem)
    self._listKind:SetPadding(0, 0, 0, 0)
    self._listKind:SetGap(0, 6)
    self._listKind:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB =                
	function(idx)
		self:OnListKindItemClicked(idx)
	end
    self._listKind:SetItemClickedCB(itemClickedCB)
    self._containerscroKind:SetActive(false)

    self._IntrodueGO = self:FindChildGO("Container_Introdue")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Introdue", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(80215))
    self._IntrodueGO:SetActive(false)

    self.flowerKinds = GlobalParamsHelper.GetParamValue(795)
    self._listKindData={}
    for k,v in pairs(self.flowerKinds) do 
        local i=tonumber(k)
        table.insert(self._listKindData,{ItemDataHelper.GetItemName(i),v[1],v[2],i})
    end
    table.sort(self._listKindData,function(a,b)
        if a[4]>b[4] then
            return true
        end
        return false
    end)
    self._listKind:SetDataList(self._listKindData)
end

function FlowerGiveView:OnEndEdit(text)
    self._isInputName = true
    self._sendflowername = text
    self._textName.text  = text
    self:UpdtaBtnName(false)
    local d={}
    d={["0"]=text,["1"]=self._flowercfg[3],["2"]=self._flowercfg[3],["3"]=self._flowercfg[1]}
    self:UpdateTips(d)
end

function FlowerGiveView:OnToggleValueChanged(state)
    if state==true then 
        self._togglestate = public_config.SEND_FLOWER_WAY_HIDE 
        return
    end
    self._togglestate = public_config.SEND_FLOWER_WAY_NAME
end

function FlowerGiveView:OnListNameItemClicked(idx)
    local item = self._listNameData[idx+1]
    self:SetSendName(item[2],item[1])
    self:UpdataToggle(item[1])
    self:BtnNameSeleClick()
end


function FlowerGiveView:SetSendName(dbid,name)
    self._sendflowerdbid = dbid
    self._sendflowername = name
    self._isInputName = false
    self._textName.text = self._sendflowername
    local d={}
    d={["0"]=self._sendflowername,["1"]=self._flowercfg[3],["2"]=self._flowercfg[3],["3"]=self._flowercfg[1]}
    self:UpdateTips(d)
end

function FlowerGiveView:UpdateTips(arg)
    self._txttips.text = LanguageDataHelper.CreateContentWithArgs(80205,arg)
    self._containerscroName:SetActive(false)
end

function FlowerGiveView:UpdataToggle(name)
    if name==GameWorld.Player().name then
        self._toggleName:SetIsOn(true)
    else
        self._toggleName:SetIsOn(false)
    end
end
function FlowerGiveView:OnListKindItemClicked(idx)
    self:UpdataKindInfo(idx)
    self:BtnKindSeleClick()
end

function FlowerGiveView:UpdataKindInfo(idx)
    self._prekind = idx
    self._flowercfg = self._listKindData[idx+1]
    self._flowerid = self._flowercfg[4]
    self._flowerName = ItemDataHelper.GetItemName(self._flowerid)
    self._textType.text = self._flowerName
    self._costitem:OnRefreshData(self._flowerid)
    local d={}
    d={["0"]=self._sendflowername,["1"]=self._flowercfg[3],["2"]=self._flowercfg[2],["3"]=self._flowercfg[1]}
    self._txttips.text = LanguageDataHelper.CreateContentWithArgs(80205,d)
end

function FlowerGiveView:BtnNameSeleClick()
    self._nameExFlage = not self._nameExFlage
    self:UpdtaBtnName(self._nameExFlage)
end

function FlowerGiveView:UpdtaBtnName(state)
    self._ImgNameExpand:SetActive(state)
    self._ImgNameContract:SetActive(not state)
    self._containerscroName:SetActive(state)
end

function FlowerGiveView:BtnKindSeleClick()
    self._kindExFlage = not self._kindExFlage
    self:UpdataBtnKind(self._kindExFlage)
end

function FlowerGiveView:UpdataBtnKind(state)
    self._ImgKindExpand:SetActive(state)
    self._ImgKindContract:SetActive(not state)
    self._containerscroKind:SetActive(state)
end

function FlowerGiveView:onBtnHelp()
    self._IntrodueGO:SetActive(not self._IntrodueGO.activeSelf)
end

function FlowerGiveView:onBtnGive()
    if (self._isInputName and self._sendflowername == GameWorld.Player().name) or (not self._isInputName and self._sendflowerdbid == GameWorld.Player().dbid) then
        if self._togglestate == public_config.SEND_FLOWER_WAY_NAME then
            local showText = LanguageDataHelper.GetContent(80223)
            GUIManager.ShowText(2, showText)
            return 
        end
    end
    local d = self.flowerKinds[self._flowerid]
    if d[3] then
        local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._flowerid)
        if ownCount<self._preNum then
		    local a = {}
		    local data = {}
		    a.id = MessageBoxType.Tip
            a.value =  data
            if not self._isInputName then
                data.confirmCB=function()FlowerManager:SendFlowerRpc(self._sendflowerdbid,self._flowerid,self._togglestate,self._preNum) GUIManager.ClosePanel(PanelsConfig.Tips) end               
            else
                data.confirmCB=function()FlowerManager:SendFlowerByNameRpc(self._sendflowername,self._flowerid,self._togglestate,self._preNum) GUIManager.ClosePanel(PanelsConfig.Tips) end
            end
		    data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.Tips) end
		    data.text = LanguageDataHelper.CreateContentWithArgs(80216,{["0"]=self._flowerName,["1"]=d[3]*(self._preNum-ownCount)})
		    GUIManager.ShowPanel(PanelsConfig.MessageBox, a)
		    return
        end
    else
        local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._flowerid)
        if ownCount==0 then
            local showText = LanguageDataHelper.GetContent(80219)
            GUIManager.ShowText(2, showText)
            return 
        end
    end
    if not self._isInputName then
        FlowerManager:SendFlowerRpc(self._sendflowerdbid,self._flowerid,self._togglestate,self._preNum)
    else
        FlowerManager:SendFlowerByNameRpc(self._sendflowername,self._flowerid,self._togglestate,self._preNum)
    end
end