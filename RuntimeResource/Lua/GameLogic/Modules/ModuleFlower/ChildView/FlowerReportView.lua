--FlowerReportView
local FlowerReportView = Class.FlowerReportView(ClassTypes.BaseComponent)
FlowerReportView.interface = GameConfig.ComponentsConfig.Component
FlowerReportView.classPath = "Modules.ModuleFlower.ChildView.FlowerReportView"
local UIList = ClassTypes.UIList
local UIComplexList = ClassTypes.UIComplexList
local public_config = GameWorld.public_config
local FlowerData = PlayerManager.PlayerDataManager.flowerData
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TipsCom = ClassTypes.TipsCom
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local FlowerManager = PlayerManager.FlowerManager

function FlowerReportView:Awake()
    self:InitComps()
end

function FlowerReportView:OnDestroy()
    
end

function FlowerReportView:OnClose()

end

function FlowerReportView:InitComps()
    self._txttips=self:GetChildComponent("Text_Info", "TextMeshWrapper")

    local btnrebate = self:FindChildGO("Button_Rebate")
    self._csBH:AddClick(btnrebate,function()self:onBtnRebate()end)

    local btnkiss = self:FindChildGO("Button_Kiss")
    self._csBH:AddClick(btnkiss,function()self:onBtnKiss()end)

    local btnget = self:FindChildGO("Button_Get")
    self._csBH:AddClick(btnget,function()self:onBtnClose()end)

    local txt1=self:GetChildComponent("Button_Rebate/Text", "TextMeshWrapper")
    txt1.text = LanguageDataHelper.GetContent(80211)
    local txt2=self:GetChildComponent("Button_Kiss/Text", "TextMeshWrapper")
    txt2.text = LanguageDataHelper.GetContent(80212)

    local txt3=self:GetChildComponent("Button_Get/Text_Get", "TextMeshWrapper")
    txt3.text = "<u>"..LanguageDataHelper.GetContent(80213).."</u>"

end

function FlowerReportView:onBtnClose()
    GUIManager.ClosePanel(PanelsConfig.Flower)    
end
function FlowerReportView:ShowView()
    self.data = FlowerData:GetReceiveFlowerData()
    local itemid = self.data[3]
    local flowerKinds = GlobalParamsHelper.GetParamValue(795)
    local itemname = ItemDataHelper.GetItemNameWithColor(itemid)
    local v = flowerKinds[itemid]
    self._txttips.text = LanguageDataHelper.CreateContentWithArgs(80210,{["0"]=self.data[1],["1"]=itemname,["2"]=v[2],["3"]=v[1]})
end

function FlowerReportView:CloseView()

end

function FlowerReportView:onBtnRebate()
    if self.data[4]==public_config.SEND_FLOWER_WAY_HIDE then
        local showText = LanguageDataHelper.GetContent(80220)
        GUIManager.ShowText(2, showText)
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.FlowerRebate)    
end

function FlowerReportView:onBtnKiss()
    FlowerManager:SendKissrRpc(self.data[2])
end
