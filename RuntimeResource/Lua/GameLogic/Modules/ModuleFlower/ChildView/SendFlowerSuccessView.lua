--SendFlowerSuccessView
local SendFlowerSuccessView = Class.SendFlowerSuccessView(ClassTypes.BaseComponent)
SendFlowerSuccessView.interface = GameConfig.ComponentsConfig.Component
SendFlowerSuccessView.classPath = "Modules.ModuleFlower.ChildView.SendFlowerSuccessView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local FlowerData = PlayerManager.PlayerDataManager.flowerData
local FlowerManager = PlayerManager.FlowerManager
function SendFlowerSuccessView:Awake()
    self:InitComps()
end

function SendFlowerSuccessView:OnDestroy()
end

function SendFlowerSuccessView:InitComps()
    self._txttips=self:GetChildComponent("Text_Info", "TextMeshWrapper")
    local btngive = self:FindChildGO("Button_ContinueGive")
    self._csBH:AddClick(btngive,function()self:onBtnGive()end)
    local txt=self:GetChildComponent("Button_ContinueGive/Text", "TextMeshWrapper")
    txt.text = LanguageDataHelper.GetContent(80209)
end

function SendFlowerSuccessView:ShowView()
    local  d = FlowerData:GetSendFlowerData()
    self._txttips.text = LanguageDataHelper.CreateContentWithArgs(80208,d)
end

function SendFlowerSuccessView:CloseView()

end
function SendFlowerSuccessView:onBtnGive()
    -- self.funCB()
    EventDispatcher:TriggerEvent(GameEvents.FlowerContinue)  
end