--FlowerPanel
require "Modules.ModuleFlower.ChildView.FlowerGiveView"
require "Modules.ModuleFlower.ChildView.SendFlowerSuccessView"
require "Modules.ModuleFlower.ChildView.FlowerReportView"

local FlowerPanel = Class.FlowerPanel(ClassTypes.BasePanel)
local UIToggleGroup = ClassTypes.UIToggleGroup
local FlowerGiveView = ClassTypes.FlowerGiveView
local SendFlowerSuccessView = ClassTypes.SendFlowerSuccessView
local FlowerReportView = ClassTypes.FlowerReportView
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local FlowerManager = PlayerManager.FlowerManager
local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local FlowerPanelType = GameConfig.EnumType.FlowerPanelType
local FlowerData = PlayerManager.PlayerDataManager.flowerData
local ItemDataHelper = GameDataHelper.ItemDataHelper
function FlowerPanel:Awake()
    self:InitFuns()
    self:InitComps()
end

function FlowerPanel:InitComps()   
    self._views={}
    self._viewGos={}
    self._viewGos[FlowerPanelType.Give]=self:FindChildGO("Container_Give")
    self._viewGos[FlowerPanelType.Success]=self:FindChildGO("Container_Success")
    self._viewGos[FlowerPanelType.Report]=self:FindChildGO("Container_Report")
    self._views[FlowerPanelType.Give]=self:AddChildLuaUIComponent("Container_Give",FlowerGiveView)
    self._views[FlowerPanelType.Success]=self:AddChildLuaUIComponent("Container_Success",SendFlowerSuccessView)
    self._views[FlowerPanelType.Report]=self:AddChildLuaUIComponent("Container_Report",FlowerReportView)
    local btnclose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(btnclose,function()self:onBtnClose()end)

end
function FlowerPanel:onBtnClose()
    GUIManager.ClosePanel(PanelsConfig.Flower)
end
--override
function FlowerPanel:OnDestroy()

end

--override
function FlowerPanel:OnShow(data)
    self:AddEventListeners()
    if data.functionId then
        data={}
        data._index=FlowerPanelType.Give
        data._data=nil
    end
    self:SeleUIView(data)

end

function FlowerPanel:SeleUIView(data)
    local _index = data._index
    local _data = data._data
    self:ShowUIView(_index,_data)
end

function FlowerPanel:ShowUIView(index,data)
    if self._selectItem and index==self._preIndex then 
        self._selectItem:ShowView(data)
        return
    end
    if self._selectItem then
        self._selectItemGo:SetActive(false)
        self._selectItem:CloseView()
    end
    self._selectItem=self._views[index]
    self._selectItemGo = self._viewGos[index]
    self._selectItemGo:SetActive(true)
    self._selectItem:ShowView(data)
    self._preIndex=index
end

--override
function FlowerPanel:OnClose()
    self:RemoveEventListeners()
    self._selectItem:CloseView()
end

function FlowerPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.FlowerContinue, self._continueFlowerFun)
    EventDispatcher:AddEventListener(GameEvents.FlowerRebate, self._rebateFlowerFun)
    EventDispatcher:AddEventListener(GameEvents.FlowerReceiveMessage, self._receiveFlowerFun)
    EventDispatcher:AddEventListener(GameEvents.FlowerSendResp, self._sendFlowerFun)
end

function FlowerPanel:RemoveEventListeners()   
    EventDispatcher:RemoveEventListener(GameEvents.FlowerContinue, self._continueFlowerFun)
    EventDispatcher:RemoveEventListener(GameEvents.FlowerRebate, self._rebateFlowerFun)
    EventDispatcher:RemoveEventListener(GameEvents.FlowerReceiveMessage, self._receiveFlowerFun)
    EventDispatcher:RemoveEventListener(GameEvents.FlowerSendResp, self._sendFlowerFun)
end

function FlowerPanel:InitFuns()
    self._sendFlowerFun = function() self:SendFlowerResp() end
    self._receiveFlowerFun = function() self:ReceiveFlowerMsg() end
    self._rebateFlowerFun = function() self:RebateFlowerMsg() end
    self._continueFlowerFun = function() self:ContinueFlowerMsg() end
end

function FlowerPanel:SendFlowerResp()
    self:ShowUIView(FlowerPanelType.Success,nil)
end

function FlowerPanel:ReceiveFlowerMsg()
    self:ShowUIView(FlowerPanelType.Report,nil)
end

function FlowerPanel:RebateFlowerMsg()
    local d=FlowerData:GetReceiveFlowerData()
    local data={}
    data.name=d[1]
    data.dbid=d[2]
    self:ShowUIView(FlowerPanelType.Give,data)
end

function FlowerPanel:ContinueFlowerMsg()
    local d=FlowerData:GetBackFlowerData()
    local data={}
    data.dbid=d[1]
    data.name=d[2]
    self:ShowUIView(FlowerPanelType.Give,data)
end

function FlowerPanel:WinBGType()
    return PanelWinBGType.NoBG
end

