--FlowerModule.lua
FlowerModule = {}
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
function FlowerModule.Init()
     GUIManager.AddPanel(PanelsConfig.Flower,"Panel_Flower",GUILayer.LayerUIPanel,"Modules.ModuleFlower.FlowerPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end
return FlowerModule