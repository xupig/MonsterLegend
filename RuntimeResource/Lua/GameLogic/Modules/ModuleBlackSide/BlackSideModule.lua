BlackSideModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function BlackSideModule.Init()
    GUIManager.AddPanel(PanelsConfig.BlackSide, "Panel_BlackSide", GUILayer.LayerUICG, "Modules.ModuleBlackSide.BlackSidePanel", true, PanelsConfig.EXTEND_TO_FIT)
end

return BlackSideModule