-- BlackSidePanel.lua
local BlackSidePanel = Class.BlackSidePanel(ClassTypes.BasePanel)

function BlackSidePanel:Awake()
    self._dramaKey = ""
    self:InitComps()
end

function BlackSidePanel:OnDestroy() 

end

function BlackSidePanel:OnClose() 

end

function BlackSidePanel:InitComps()
    self._skipButton = self:FindChildGO("Container_Content/Button_Skip")
    self._csBH:AddClick(self._skipButton, function() EventDispatcher:TriggerEvent(GameEvents.BLACK_SKIP_CG, self._dramaKey)  end )
end

function BlackSidePanel:OnShow(data)
    self:SetSideData(data)
end

function BlackSidePanel:SetData(data)
    self:SetSideData(data)
end

function BlackSidePanel:SetSideData(data)
     self._dramaKey = ""
    if data == nil then return end
    self._dramaKey = data.dramaKey
    if data.ShowSkip == true then
        self._skipButton:SetActive(true)
    else
        self._skipButton:SetActive(false)
    end
end
