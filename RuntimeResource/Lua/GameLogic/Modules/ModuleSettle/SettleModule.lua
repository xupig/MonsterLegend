SettleModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function SettleModule.Init()
    GUIManager.AddPanel(PanelsConfig.Settle,"Panel_Settle",GUILayer.LayerUIPanel,"Modules.ModuleSettle.SettlePanel",true,false)
end

return SettleModule