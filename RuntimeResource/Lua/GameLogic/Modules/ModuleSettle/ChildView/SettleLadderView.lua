local SettleLadderView = Class.SettleLadderView(ClassTypes.BaseLuaUIComponent)

SettleLadderView.interface = GameConfig.ComponentsConfig.Component
SettleLadderView.classPath = "Modules.ModuleSettle.ChildView.SettleLadderView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local DateTimeUtil = GameUtil.DateTimeUtil

function SettleLadderView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function SettleLadderView:OnDestroy() 
    self._csBH = nil
end

function SettleLadderView:RefreshData(data)
    self:HandleData(data)
    self:ShowEnemyInfo()
    self:ShowMatchTime()
    self:ShowSelfInfo()
    self:ShowWinOrLose()
end

function SettleLadderView:InitView()
    self._timeText = self:GetChildComponent("Text_Time", "TextMeshWrapper")
    self._winImage = self:FindChildGO("Container_Result/Image_Win")
    self._loseImage = self:FindChildGO("Container_Result/Image_Lose")

    self._backButton = self:FindChildGO("Button_Back")
    self._csBH:AddClick(self._backButton,function () self:OnBackButtonClick() end)
    self._shareButton = self:FindChildGO("Button_Share")
    self._csBH:AddClick(self._shareButton,function () self:OnShareButtonClick() end)

    self._iconContainerSelf = self:FindChildGO("Container_Info/Container_Self/Container_Icon")
    self._nameTextSelf = self:GetChildComponent("Container_Info/Container_Self/Text_Name", "TextMeshWrapper")
    self._killTextSelf = self:GetChildComponent("Container_Info/Container_Self/Text_Kill", "TextMeshWrapper")
    self._deathTextSelf = self:GetChildComponent("Container_Info/Container_Self/Text_Death", "TextMeshWrapper")
    self._assistTextSelf = self:GetChildComponent("Container_Info/Container_Self/Text_Assist", "TextMeshWrapper")

    self._iconContainerEnemy = self:FindChildGO("Container_Info/Container_Enemy/Container_Icon")
    self._nameTextEnemy = self:GetChildComponent("Container_Info/Container_Enemy/Text_Name", "TextMeshWrapper")
    self._killTextEnemy = self:GetChildComponent("Container_Info/Container_Enemy/Text_Kill", "TextMeshWrapper")
    self._deathTextEnemy = self:GetChildComponent("Container_Info/Container_Enemy/Text_Death", "TextMeshWrapper")
    self._assistTextEnemy = self:GetChildComponent("Container_Info/Container_Enemy/Text_Assist", "TextMeshWrapper")
end

function SettleLadderView:OnBackButtonClick()
    GUIManager.ClosePanel(PanelsConfig.Settle)
end

function SettleLadderView:OnShareButtonClick()

end

function SettleLadderView:HandleData(data)
    local player = GameWorld.Player()
    local playersData = data[public_config.LADDER_ROOM_KEY_PLAYERS]
    self.matchTime = data[public_config.LADDER_ROOM_KEY_MATCH_DURATION_TIME_SEC]
    local winFaction =  data[public_config.LADDER_ROOM_KEY_SETTLE_FLAG]
    local myFaction = player.faction_id
    if winFaction == myFaction then
        self.isWin = true
    else
        self.isWin = false
    end
    for k,v in pairs(playersData) do
        if k == myFaction then
            self.myInfo = v[1]
        else
            self.enemyInfo = v[1]
        end
    end
end

function SettleLadderView:ShowSelfInfo()
    local name = self.myInfo[public_config.LADDER_PLAYER_KEY_NAME]
    local kill = self.myInfo[public_config.LADDER_PLAYER_KEY_KILL] or 0
    local death = self.myInfo[public_config.LADDER_PLAYER_KEY_DEATH] or 0
    local assist = self.myInfo[public_config.LADDER_PLAYER_KEY_ASSIST] or 0
    local icon = 47
    GameWorld.AddIcon(self._iconContainerSelf, icon, nil)
    self._nameTextSelf.text =  name
    self._killTextSelf.text = kill
    self._deathTextSelf.text = death
    self._assistTextSelf.text = assist
end

function SettleLadderView:ShowEnemyInfo()
    local name = self.enemyInfo[public_config.LADDER_PLAYER_KEY_NAME]
    local kill = self.enemyInfo[public_config.LADDER_PLAYER_KEY_KILL] or 0
    local death = self.enemyInfo[public_config.LADDER_PLAYER_KEY_DEATH] or 0
    local assist = self.enemyInfo[public_config.LADDER_PLAYER_KEY_ASSIST] or 0
    local icon = 47
    GameWorld.AddIcon(self._iconContainerEnemy, icon, nil)
    self._nameTextEnemy.text =  name
    self._killTextEnemy.text = kill
    self._deathTextEnemy.text = death
    self._assistTextEnemy.text = assist
end

function SettleLadderView:ShowMatchTime()
    local timeText = DateTimeUtil:ParseTime(self.matchTime)
    self._timeText.text = timeText
end

function SettleLadderView:ShowWinOrLose()
    if self.isWin then
        self._winImage:SetActive(true)
        self._loseImage:SetActive(false)
    else
        self._winImage:SetActive(false)
        self._loseImage:SetActive(true)
    end
end