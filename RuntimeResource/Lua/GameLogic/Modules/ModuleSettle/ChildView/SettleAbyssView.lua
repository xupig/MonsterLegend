local SettleAbyssView = Class.SettleAbyssView(ClassTypes.BaseLuaUIComponent)

SettleAbyssView.interface = GameConfig.ComponentsConfig.Component
SettleAbyssView.classPath = "Modules.ModuleSettle.ChildView.SettleAbyssView"

local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local DateTimeUtil = GameUtil.DateTimeUtil

function SettleAbyssView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function SettleAbyssView:OnDestroy() 
    self._csBH = nil
end

function SettleAbyssView:RefreshData(data)
    self:HandleData(data)
    self:ShowWinOrLose()
end

function SettleAbyssView:InitView()
    self._winImage = self:FindChildGO("Container_Result/Image_Win")
    self._loseImage = self:FindChildGO("Container_Result/Image_Lose")

    self._layerText = self:GetChildComponent("Container_Info/Text_Layer", "TextMeshWrapper")

    self._infoContainer = self:FindChildGO("Container_Info")
    self._boxButton = self:FindChildGO("Container_Info/Container_RewardInfo/Button_Reward")
    self._csBH:AddClick(self._boxButton,function () self:OnBoxButtonClick() end)

end

function SettleAbyssView:OnBoxButtonClick()

end

function SettleAbyssView:HandleData(data)
    self.data = data
    self.isWin = data[public_config.ABYSS_ROOM_KEY_SETTLE_FLAG] == 1
end

function SettleAbyssView:ShowWinOrLose()
    if self.isWin then
        self._winImage:SetActive(true)
        self._loseImage:SetActive(false)
    else
        self._winImage:SetActive(false)
        self._loseImage:SetActive(true)
    end
end