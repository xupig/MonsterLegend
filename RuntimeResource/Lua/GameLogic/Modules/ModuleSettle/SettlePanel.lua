require "Modules.ModuleSettle.ChildView.SettleLadderView"
require "Modules.ModuleSettle.ChildView.SettleAbyssView"

local SettlePanel = Class.SettlePanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local SettleLadderView = ClassTypes.SettleLadderView
local SettleAbyssView = ClassTypes.SettleAbyssView
local SettleType = GameConfig.EnumType.SettleType

local Settle_Count = 2

--override
function SettlePanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self._showSettle = 1
    self:AddEventListeners()
    self:InitPanel()
end

--override
function SettlePanel:OnDestroy()
	self._csBH = nil
    self:RemoveEventListeners()
end

--override data结构 id：结算面板序号 value：结算面板数据
function SettlePanel:OnShow(data)
    local index = data.id or SettleType.Ladder
    self:ShowSettle(index)
    self:OnRefreshData(data)
end
--override
function SettlePanel:SetData(data)

end

--override
function SettlePanel:OnClose()
end

function SettlePanel:AddEventListeners()
    --self._onSelectChange = function (data) self:RefreshSelectedTip(data) end
	--EventDispatcher:AddEventListener(GameEvents.ON_SHOP_ITEM_SELECT, self._onSelectChange)
end

function SettlePanel:RemoveEventListeners()
    --EventDispatcher:RemoveEventListener(GameEvents.ON_SHOP_ITEM_SELECT, self._onSelectChange)
end

function SettlePanel:InitPanel()
    self._settles = {}
    self._settleViews = {}
    for i=1,Settle_Count do
        self._settles[i] = self:FindChildGO("Container_Settle" .. i)
        self._settles[i]:SetActive(false)
    end
    self._settleViews[1] = self:AddChildLuaUIComponent("Container_Settle1", SettleLadderView)
    self._settleViews[2] = self:AddChildLuaUIComponent("Container_Settle2", SettleAbyssView)
end

function SettlePanel:ShowSettle(index)
    if index ~= self._showSettle then
        self._settles[self._showSettle]:SetActive(false)
    end
    self._settles[index]:SetActive(true)
    self._showSettle = index
end

function SettlePanel:OnRefreshData(data)
    local index = data.id or 1
    self._settleViews[index]:RefreshData(data.value)
end