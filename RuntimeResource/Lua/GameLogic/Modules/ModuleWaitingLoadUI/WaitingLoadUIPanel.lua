-- WaitingLoadUIPanel.lua
local WaitingLoadUIPanel = Class.WaitingLoadUIPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local stop = Vector3.New(0, 0, 0)
local start = Vector3.New(0, 0, 360)

function WaitingLoadUIPanel:Awake()
    self._closeCb = function()
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        EventDispatcher:TriggerEvent(GameEvents.EndWaitLoadingFirst, function()
            self:OnShow()
        end)
    end
    self._loadingImage = self:FindChildGO("Image_Loading")
    self._loadingImageRotation = self._loadingImage:AddComponent(typeof(XGameObjectTweenRotation))
end

function WaitingLoadUIPanel:OnShow(data)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self._timer = TimerHeap:AddSecTimer(15, 0, 0, self._closeCb)
    self._loadingImageRotation:SetFromToRotation(start, stop, 2, 2,nil)
end

function WaitingLoadUIPanel:OnClose()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

--GameManager.GUIManager.ShowPanel(GameConfig.PanelsConfig.WaitingLoadUI)
function WaitingLoadUIPanel:OnDestroy()

end
