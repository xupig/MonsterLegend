-- WaitingLoadUIModule.lua
WaitingLoadUIModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function WaitingLoadUIModule.Init()
    GUIManager.AddPanel(PanelsConfig.WaitingLoadUI, "Panel_WaitingLoadUI", GUILayer.LayerUIPanel, "Modules.ModuleWaitingLoadUI.WaitingLoadUIPanel", true, false)
end

return WaitingLoadUIModule
