-- RedemptionModule.lua
RedemptionModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function RedemptionModule.Init()
    GUIManager.AddPanel(PanelsConfig.Redemption, "Panel_Redemption", GUILayer.LayerUICommon, "Modules.ModuleRedemption.RedemptionPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
 end

return RedemptionModule