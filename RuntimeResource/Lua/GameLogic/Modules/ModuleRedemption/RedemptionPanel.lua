-- RedemptionPanel.lua
local RedemptionPanel = Class.RedemptionPanel(ClassTypes.BasePanel)
RedemptionPanel.interface = GameConfig.ComponentsConfig.Panel
RedemptionPanel.classPath = "Modules.ModuleRedemption.RedemptionPanel"

local ReviveRescueManager = GameManager.ReviveRescueManager
local TimerHeap = GameWorld.TimerHeap
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local public_config = GameWorld.public_config
local ButtonWrapper = UIExtension.ButtonWrapper
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local SystemSettingManager = PlayerManager.SystemSettingManager
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig

function RedemptionPanel:Awake()
    self:Init()
    self:InitListener()
end

function RedemptionPanel:Init()
    self._containerDieView = self:FindChildGO("Container_DieView")
    self._btnReturnGO = self:FindChildGO("Container_DieView/Button_return")
    self._btnReturnWrapper = self:GetChildComponent("Container_DieView/Button_return", "ButtonWrapper")
    self._btnImmediateGO = self:FindChildGO("Container_DieView/Button_immediate")
    --self._txtReturn = self:GetChildComponent("Container_DieView/Button_return/Text","TextMeshWrapper")
    --self._txtImmediate = self:GetChildComponent("Container_DieView/Button_immediate/Text","TextMeshWrapper")
    self._txtCost = self:GetChildComponent("Container_DieView/Text_Cost", "TextMeshWrapper")
    local moneyIcon = self:FindChildGO("Container_DieView/Container_DiamondIcon")
    GameWorld.AddIcon(moneyIcon, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))

    self._imgInfoBg = self:FindChildGO("Container_DieView/Image_InfoBg")
    self._imgTireInfoBg = self:FindChildGO("Container_DieView/Image_TireInfoBg")
    self._txtTireInfo = self:GetChildComponent("Container_DieView/Image_TireInfoBg/Text_TireInfo", "TextMeshWrapper")

    self._imgInfoBg:SetActive(true)
    self._imgTireInfoBg:SetActive(false)
    --self._txtInfo = self:GetChildComponent("Container_DieView/Image_InfoBg/Text_Info", "TextMeshWrapper")
    
    -- self._btnTireReturnGO = self:FindChildGO("Container_DieTireView/Button_return")
    -- self._btnTireImmediateGO = self:FindChildGO("Container_DieTireView/Button_immediate")
    -- --self._btnTireCloseGO = self:FindChildGO("Container_DieTireView/Button_Close")
    -- -- self._txtTireDesc = self:GetChildComponent("Container_DieTireView/Text_Desc", "TextMeshWrapper")
    -- self._txtTireCost = self:GetChildComponent("Container_DieTireView/Text_Cost", "TextMeshWrapper")
    -- self._txtTireInfo = self:GetChildComponent("Container_DieTireView/Text_Info", "TextMeshWrapper")
    -- self._txtTireCountDown = self:GetChildComponent("Container_DieTireView/Text_CountDown", "TextMeshWrapper")
    
    -- self._containerDieTireView = self:FindChildGO("Container_DieTireView")
    -- self._containerDieTireView:SetActive(false)
    self._txtDesc = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
    self._txtCountDown = self:GetChildComponent("Text_CountDown", "TextMeshWrapper")
end

function RedemptionPanel:InitListener()
    self._csBH:AddClick(self._btnReturnGO, function()self:OnBackToRevivePoint() end)
    self._csBH:AddClick(self._btnImmediateGO, function()self:OnImmediate() end)
    -- self._csBH:AddClick(self._btnTireReturnGO, function()self:OnTireReturn() end)
    -- self._csBH:AddClick(self._btnTireImmediateGO, function()self:OnTireImmediate() end)
end

function RedemptionPanel:OnShow(args)
    --LoggerHelper.Log("Ash: RedemptionPanel OnShow: " .. PrintTable:TableToStr(args))
    local deadType = args[1]
    local killer = args[2]
    local cost = args[3]
    local tireTime = args[4]
    --LoggerHelper.Error("tireTime"..tireTime)
    if deadType == public_config.DIE_TYPE_WAIT_N_SEC then
        self:WaitNSecDead(killer)
    else
        self:NormalDead(killer, cost, tireTime)
    end
end

function RedemptionPanel:OnDestroy()
    self._btnReturn = nil
    self._btnImmediate = nil
    self._txtDesc = nil
    self._txtCost = nil
    --self._txtInfo = nil
end

function RedemptionPanel:NormalDead(killer, cost, tireTime)
    self._containerDieView:SetActive(true)
    
    if tireTime == 0 then
        self._reviveTime = DateTimeUtil:GetServerTime() + GlobalParamsHelper.GetParamValue(442)--revive_time_i
        self._imgInfoBg:SetActive(true)
        self._imgTireInfoBg:SetActive(false)
        self._btnReturnWrapper.interactable = true
    else
        self._reviveTime = tireTime
        self._imgInfoBg:SetActive(false)
        self._imgTireInfoBg:SetActive(true)
        self._btnReturnWrapper.interactable = false
        self._txtTireInfo.text = LanguageDataHelper.CreateContentWithArgs(56026,{["0"] = ReviveRescueManager:GetReviveTireCount()})
    end

    --不同场景按钮文字不同
    -- local sceneType = GameSceneManager:GetCurSceneType()
    -- if sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then
    --     self._txtReturn.text = LanguageDataHelper.CreateContent(53182)
    --     self._txtImmediate.text = LanguageDataHelper.CreateContent(53183)
    -- else
    --     self._txtReturn.text = LanguageDataHelper.CreateContent(581)
    --     self._txtImmediate.text = LanguageDataHelper.CreateContent(580)
    -- end

    self._reviveTime = self._reviveTime + 5 -- 统一加5秒，避免比后端快
    if killer then
        local killerStr = BaseUtil.GetColorString(killer, ColorConfig.H)
        self._txtDesc.text = LanguageDataHelper.CreateContent(56028) .. killerStr .. LanguageDataHelper.CreateContent(56029)
    else
        self._txtDesc.text = LanguageDataHelper.CreateContent(56030)
    end
    self._txtCost.text = tostring(cost)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    
    self:UpdateTimeLeft()
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnBackToRevivePointTime() end)

    --自动原地复活
    if SystemSettingManager:IsCanAutoRevival() then
        self._autoReviveTimer = TimerHeap:AddSecTimer(5, 0, 0, function()self:OnImmediate() end)
    end
end

function RedemptionPanel:WaitNSecDead(killer)
    self._containerDieView:SetActive(false)
    --不同场景按钮文字不同
    local sceneType = GameSceneManager:GetCurSceneType()
    local cd = 5
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then
        cd = GlobalParamsHelper.GetParamValue(912)
    end

    self:SetCoolDown(cd)
    if killer then
        local killerStr = BaseUtil.GetColorString(killer, ColorConfig.H)
        self._txtDesc.text = LanguageDataHelper.CreateContent(56028) .. killerStr .. LanguageDataHelper.CreateContent(56029)
    else
        self._txtDesc.text = LanguageDataHelper.CreateContent(56030)
    end
end

function RedemptionPanel:UpdateTimeLeft()
    local leftTime = self._reviveTime - DateTimeUtil:GetServerTime()
    --self._txtInfo.text = LanguageDataHelper.CreateContent(56032) .. leftTime .. LanguageDataHelper.CreateContent(56033)
    self._txtCountDown.text = tostring(leftTime)
end

--立刻复活
function RedemptionPanel:OnImmediate()
    --LoggerHelper.Error("OnImmediate")
    if self._autoReviveTimer then
        TimerHeap:DelTimer(self._autoReviveTimer)
        self._autoReviveTimer = nil
    end
    ReviveRescueManager:RequestRevive(2)
end

--回复活点复活
function RedemptionPanel:OnBackToRevivePoint()
    --LoggerHelper.Error("OnReturn")
    -- if self._isTire then
    --     self._containerDieTireView:SetActive(true)
    -- else
        
    -- end

    ReviveRescueManager:RequestRevive(1)
end

-- function RedemptionPanel:OnTireImmediate()
--     ReviveRescueManager:RequestRevive(2)
-- end

-- function RedemptionPanel:OnTireReturn()
--     self._containerDieTireView:SetActive(false)
-- end

-- --回复活点复活等待时间
function RedemptionPanel:OnBackToRevivePointTime()
    if (self._reviveTime - DateTimeUtil:GetServerTime() > 0) then
        -- self._reviveTime = self._reviveTime - 1
        self:UpdateTimeLeft()
    else
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
        --self._isTire = false
        self:OnBackToRevivePoint()
    end
end

--设定倒计时
function RedemptionPanel:SetCoolDown(totalTime)
    self._cdRemainTime = totalTime
    self._intervalTimerID = TimerHeap:AddSecTimer(0, 1, 0,
        function()
            if self._cdRemainTime <= 0 then
                self:CoolDownComplete()
            end
            self._txtCountDown.text = math.floor(self._cdRemainTime)
            self._cdRemainTime = self._cdRemainTime - 1
        end
    )
end
-- function RedemptionPanel:SetCoolDown(totalTime)
--     self._cdRemainTime = totalTime
--     self._cdTotalTime = totalTime
--     self._intervalTimerID = TimerHeap:AddTimer(0, CD_INTERVAL, 0,
--         function()
--             if self._cdRemainTime <= 0 then
--                 self:CoolDownComplete()
--             else
--                 self:ShowSelfCD()
--             end
--             self._cdRemainTime = self._cdRemainTime - CD_INTERVAL
--             self._txtCountDown.text = math.floor(self._cdRemainTime * 0.001) + 1
--         end
-- )
-- end

function RedemptionPanel:CoolDownComplete()
    self._cdRemainTime = 0
    TimerHeap:DelTimer(self._intervalTimerID)
    ReviveRescueManager:RequestRevive(1)
end

-- ---重置CD
-- function RedemptionPanel:CDReset()
--     if self._intervalTimerID ~= 0 then
--         TimerHeap:DelTimer(self._intervalTimerID)
--         self._intervalTimerID = 0
--     end
--     self._cdRemainTime = 0
--     self._cdTotalTime = 0
--     self._CooldownImage.fillAmount = 0
-- end

-- function RedemptionPanel:ShowSelfCD()
--     local rate = self._cdRemainTime / self._cdTotalTime
--     self._CooldownImage.fillAmount = rate
-- end

-- 是否有背景 0 没有  1 清晰底板  2 模糊底板  3 二级面板遮罩
function RedemptionPanel:CommonBGType()
    return PanelBGType.Mask
end

function RedemptionPanel:HandlerBGEvent(BgGo)
    BgGo:AddComponent(typeof(ButtonWrapper))
end
