require "Modules.ModulePartner.ChildView.PartnerPetView"
require "Modules.ModulePartner.ChildView.PartnerRideView"
require "Modules.ModulePartner.ChildView.PartnerGuardianView"
local PartnerPanel = Class.PartnerPanel(ClassTypes.BasePanel)
local PanelBGType = GameConfig.PanelsConfig.PanelBGType

local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local PKModeManager = PlayerManager.PKModeManager
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local UIToggleGroup = ClassTypes.UIToggleGroup
local PartnerType = GameConfig.EnumType.PartnerType
local PartnerPetView = ClassTypes.PartnerPetView
local PartnerRideView = ClassTypes.PartnerRideView
local PartnerGuardianView = ClassTypes.PartnerGuardianView
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local PartnerManager = PlayerManager.PartnerManager

--override
function PartnerPanel:Awake()
    self:InitComps()
    self:InitListenerFunc()
end

--override
function PartnerPanel:OnDestroy()
end

function PartnerPanel:OnEnable()
end

function PartnerPanel:OnDisable()
end

function PartnerPanel:InitComps()
    self:InitView("Container_Pet", PartnerPetView, 1)
    self:InitView("Container_Ride", PartnerRideView, 2)
    self:InitView("Container_Guardian", PartnerGuardianView, 3)

    self._bg1 = self:FindChildGO("Container_CommonBg1")
    self._bg1:SetActive(true)
    self._bg2 = self:FindChildGO("Container_CommonBg2")

    self._iconBg = self:FindChildGO('Image_Bg')
    GameWorld.AddIcon(self._iconBg,3317)
end

function PartnerPanel:StructingViewInit()
    return true
end

--基于结构化View初始化方式，隐藏View使用Scale的方式处理
-- function PartnerPanel:ScaleToHide()
--     return true
-- end

--关闭的时候重新开启二级主菜单
function PartnerPanel:ReopenSubMainPanel()
    return true
end

function PartnerPanel:InitListenerFunc()
    self._showModelCb = function () self:BaseShowModel() end
    self._onRefreshPartnerRedPoint = function(type) self:RefreshPartnerRedPoint(type) end
    self._tabClickCB = function (index) self:OnToggleGroupClick(index) end
    self:SetTabClickCallBack(self._tabClickCB)
end

function PartnerPanel:OnShow(data)
    self:AddEventListeners()
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
end

--override
function PartnerPanel:OnClose()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_partner_panel_close_button")
    self:RemoveEventListeners()
end

function PartnerPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_SHOW_PARTNER_MODEL,self._showModelCb)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Partner_Red_Point, self._onRefreshPartnerRedPoint)
end

function PartnerPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_SHOW_PARTNER_MODEL,self._showModelCb)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Partner_Red_Point, self._onRefreshPartnerRedPoint)    
end

function PartnerPanel:OnToggleGroupClick(index)
    --设置不同公共背景
    if index == PartnerType.Pet or index == PartnerType.Ride then
        self._bg1:SetActive(true)
        self._bg2:SetActive(false)
    else
        self._bg1:SetActive(false)
        self._bg2:SetActive(true)
    end
end

function PartnerPanel:RefreshPartnerRedPoint(type)
end

function PartnerPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function PartnerPanel:BaseShowModel()
    self._selectedView:ShowModel()
end

function PartnerPanel:BaseHideModel()
    self._selectedView:HideModel()
end