PartnerModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function PartnerModule.Init()
	GUIManager.AddPanel(PanelsConfig.Partner,"Panel_Partner",GUILayer.LayerUIPanel,"Modules.ModulePartner.PartnerPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return PartnerModule