local PartnerRideFeatherItem = Class.PartnerRideFeatherItem(ClassTypes.BaseLuaUIComponent)

PartnerRideFeatherItem.interface = GameConfig.ComponentsConfig.Component
PartnerRideFeatherItem.classPath = "Modules.ModulePartner.ChildComponent.PartnerRideFeatherItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local PartnerManager = PlayerManager.PartnerManager
local BagManager = PlayerManager.BagManager
local TipsManager = GameManager.TipsManager
local UIProgressBar = ClassTypes.UIProgressBar
local AttriDataHelper = GameDataHelper.AttriDataHelper
local ColorConfig = GameConfig.ColorConfig
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil

function PartnerRideFeatherItem:Awake()
    self._base.Awake(self)
    self.itemId = 0
    self:InitView()
end

function PartnerRideFeatherItem:InitView()
    self._useButton = self:FindChildGO("Button_Use")
    self._csBH:AddClick(self._useButton,function () self:OnUseButtonClick() end)

    self._tips = self:FindChildGO("Button_Use/Image_Tip")
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._countText = self:GetChildComponent("Text_Count", "TextMeshWrapper")
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    local itemGo = GUIManager.AddItem(self._iconContainer, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function()
        TipsManager:ShowItemTipsById(self.data[1])
    end
    self._icon:SetPointerDownCB(self._func)

    self:InitProgress()
end

function PartnerRideFeatherItem:InitProgress()
    self._Comp = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar")
    self._Comp:SetProgress(0)
    self._Comp:ShowTweenAnimate(true)
    self._Comp:SetMutipleTween(true)
    self._Comp:SetIsResetToZero(false)
    self._Comp:SetTweenTime(0.2)
end

function PartnerRideFeatherItem:OnUseTimeChange(data)
    local itemId = self.data[1]
    local cur_count = PartnerManager:GetRideItemUsedInfo(itemId)
    local max_count = self:GetCurMaxCount()
    self._Comp:SetProgress(cur_count/max_count)
    self._Comp:SetProgressText(tostring(cur_count).."/"..tostring(max_count))
end

function PartnerRideFeatherItem:GetCurMaxCount()
    local level = GameWorld.Player().level
    local index = 1
    for i=1,#self.data[2]/3 do
        if level <= self.data[2][i*3-1] then
            index = i
            break
        end
    end
    return self.data[2][index*3]
end

--data: {itemId,{等级下限，等级上限，数量,...}}
function PartnerRideFeatherItem:OnRefreshData(data)
    self.data = data
    self:RefreshView()
end

function PartnerRideFeatherItem:RefreshView()
    local itemId = self.data[1]
    self._icon:SetItem(itemId)
    local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    if bagNum > 0 then
        self._tips:SetActive(true)
    else
        self._tips:SetActive(false)
    end

    local showText = bagNum.."/1"
    if bagNum >= 1 then
        showText = BaseUtil.GetColorString(showText, ColorConfig.J)
    else
        showText = BaseUtil.GetColorString(showText, ColorConfig.H)
    end
    self._countText.text = showText
    self._nameText.text = ItemDataHelper.GetItemNameWithColor(itemId)
    self._infoText.text = self:GetAttriInfo(itemId)
    self:OnUseTimeChange()
end

function PartnerRideFeatherItem:OnUseButtonClick()
    local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self.data[1])
    if pos <= 0 then
        return
    end
    BagManager:RequsetUse(pos, 1)
end

function PartnerRideFeatherItem:GetAttriInfo(itemId)
    local effectData = ItemDataHelper.GetItemEffect(itemId)
    local result = ""
    for i=4,#effectData,2 do
        local attri = effectData[i]
        local value = effectData[i+1]
        result = result .. AttriDataHelper:GetName(attri).."+"..value.." "
    end
    if effectData[3] > 0 then
        result = result..LanguageDataHelper.CreateContentWithArgs(58619, {["0"]= effectData[3]})
    end
    return result
end