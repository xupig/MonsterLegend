local PartnerPetDevourEquipItem = Class.PartnerPetDevourEquipItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
PartnerPetDevourEquipItem.interface = GameConfig.ComponentsConfig.Component
PartnerPetDevourEquipItem.classPath = "Modules.ModulePartner.ChildComponent.PartnerPetDevourEquipItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local bagData = PlayerManager.PlayerDataManager.bagData

function PartnerPetDevourEquipItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self:InitView()
end

function PartnerPetDevourEquipItem:OnDestroy()
    self.data = nil
end

--override
function PartnerPetDevourEquipItem:OnRefreshData(data)
    self.data = data
    local count = 0
    --兼容空格子
    if data.cfg_id ~= nil then
        count = self.data:GetCount()
    end
    self._icon:SetItem(data.cfg_id, count)
    if self._select then
        self:Selected()
    end
end

function PartnerPetDevourEquipItem:InitView()
    local parent = self:FindChildGO("Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._selectImage = self:FindChildGO("Image_Selected")
end

function PartnerPetDevourEquipItem:ChangeState()
    if self.data.cfg_id == nil then
        return
    end
    self._select = not self._select
    self._selectImage:SetActive(self._select)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Ride_Selected_Equip_Item, self.data.bagPos, self._select)
end

function PartnerPetDevourEquipItem:Selected()
    self._select = true
    if self.data == nil or self.data.cfg_id == nil then
        return
    end
    self._selectImage:SetActive(self._select)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Ride_Selected_Equip_Item, self.data.bagPos, self._select)
end

function PartnerPetDevourEquipItem:UnSelected()
    self._select = false
    self._selectImage:SetActive(self._select)
end