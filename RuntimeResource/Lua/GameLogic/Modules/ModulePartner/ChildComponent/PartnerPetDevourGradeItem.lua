local PartnerPetDevourGradeItem = Class.PartnerPetDevourGradeItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
PartnerPetDevourGradeItem.interface = GameConfig.ComponentsConfig.Component
PartnerPetDevourGradeItem.classPath = "Modules.ModulePartner.ChildComponent.PartnerPetDevourGradeItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function PartnerPetDevourGradeItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function PartnerPetDevourGradeItem:OnDestroy()
    self.data = nil
end

--override --{阶数，中文字} 0表示清空选择 -1表示任意
function PartnerPetDevourGradeItem:OnRefreshData(data)
    self.data = data
    self._infoText.text = LanguageDataHelper.GetLanguageData(data[2])
end

function PartnerPetDevourGradeItem:InitView()
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
end

function PartnerPetDevourGradeItem:ShowSelected(state)
    self._selected:SetActive(state)
end