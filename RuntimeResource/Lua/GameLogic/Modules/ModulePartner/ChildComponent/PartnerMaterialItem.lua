local PartnerMaterialItem = Class.PartnerMaterialItem(ClassTypes.BaseLuaUIComponent)

PartnerMaterialItem.interface = GameConfig.ComponentsConfig.PointableComponent
PartnerMaterialItem.classPath = "Modules.ModulePartner.ChildComponent.PartnerMaterialItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemManager = ClassTypes.ItemManager
local bagData = PlayerManager.PlayerDataManager.bagData
local GUIManager = GameManager.GUIManager
local ShopType = GameConfig.EnumType.ShopType
local ShopPageType = GameConfig.EnumType.ShopPageType
local PartnerManager = PlayerManager.PartnerManager
local TipsManager = GameManager.TipsManager

function PartnerMaterialItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self.canUse = false
    self.isSelected = false
    self:InitView()
end

function PartnerMaterialItem:OnDestroy()
    self.data = nil
end

function PartnerMaterialItem:InitView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._countText = self:GetChildComponent("Text_Num", "TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._itemManager = ItemManager()
    self._itemIcon = self._itemManager:GetLuaUIComponent(nil, self._iconContainer)

    self._imageSelected = self:FindChildGO("Image_Selected")
    self._nameGo = self:FindChildGO("Text_Name")
end

function PartnerMaterialItem:InitIndex(index)
    self.index = index
end

--data: {itemId, blessValue}
function PartnerMaterialItem:OnRefreshData(data)
    self.data = data
    self:RefreshView()
end

function PartnerMaterialItem:RefreshView()
    local itemId = self.data[1]
    self._itemIcon:SetItem(itemId)
    self._nameText.text = ItemDataHelper.GetItemNameWithColor(itemId)
    local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    self._countText.text = bagNum
end

function PartnerMaterialItem:ShowSelected(state)
    self._imageSelected:SetActive(state)
    self._nameGo:SetActive(state)
    self.isSelected = state
    if state then
        local itemId = self.data[1]
        PartnerManager:SetStarUpSelectedItem(itemId)
    end
end

function PartnerMaterialItem:OnPointerDown(pointerEventData)
    if self.isSelected then
        TipsManager:ShowItemTipsById(self.data[1])
    end
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Selected_Bless_Item, self.index)
end

function PartnerMaterialItem:OnPointerUp(pointerEventData)

end