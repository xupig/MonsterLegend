-- PartnerGuardianListItem.lua
--守护列表Item类
local PartnerGuardianListItem = Class.PartnerGuardianListItem(ClassTypes.UIListItem)

PartnerGuardianListItem.interface = GameConfig.ComponentsConfig.PointableComponent
PartnerGuardianListItem.classPath = "Modules.ModulePartner.ChildComponent.PartnerGuardianListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig

function PartnerGuardianListItem:Awake()
	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected:SetActive(false)
	self._containerIcon = self:FindChildGO("Container_Icon")
	self._txtInFightGO = self:FindChildGO("Text_InFight")
	self._txtGrade = self:GetChildComponent("Text_Grade",'TextMeshWrapper')
end

function PartnerGuardianListItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end

--override
function PartnerGuardianListItem:OnRefreshData(data)
	self._data = data
	self:UpdateData()
end

function PartnerGuardianListItem:UpdateData()
	local guardInfo = GameWorld.Player().guardian_info
	local inFight = GameWorld.Player().guardian_on or 0

	self._txtName.text = LanguageDataHelper.CreateContent(self._data.name)
	local grade = guardInfo[self._data.id]
	if grade then
		GameWorld.AddIcon(self._containerIcon,self._data.icon,0,false,1)
		local colorId = ItemConfig.ItemAttriValue
		local str = StringStyleUtil.GetChineseNumber(grade)..LanguageDataHelper.CreateContent(305)
		self._txtGrade.text = StringStyleUtil.GetColorStringWithColorId(str,colorId)
	else
		GameWorld.AddIcon(self._containerIcon,self._data.icon,0,false,0)
		self._txtGrade.text = LanguageDataHelper.CreateContent(536)
	end

	self._txtInFightGO:SetActive(self._data.id == inFight)
end