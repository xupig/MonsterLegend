local PartnerSkillItem = Class.PartnerSkillItem(ClassTypes.BaseLuaUIComponent)

PartnerSkillItem.interface = GameConfig.ComponentsConfig.PointableComponent
PartnerSkillItem.classPath = "Modules.ModulePartner.ChildComponent.PartnerSkillItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local SkillDataHelper = GameDataHelper.SkillDataHelper
local TipsManager = GameManager.TipsManager

function PartnerSkillItem:Awake()
    self._base.Awake(self)
    self._isLock = false
    self:InitView()
end

function PartnerSkillItem:InitView()
    self._iconContainer = self:FindChildGO("Container_Icon")
end

--设置技能tips类型:宠物5，坐骑6
function PartnerSkillItem:SetType(type)
    self._type = type
end

--data:{skill,lock, grade} lock:0解锁 1：锁定
function PartnerSkillItem:OnRefreshData(data)
    self.data = data
    self:RefreshView()
end

function PartnerSkillItem:RefreshView()
    if self.data.lock == 1 then
        self._isLock = true
    else
        self._isLock = false
    end
    local icon = SkillDataHelper.GetSkillIcon(self.data.skill)
    if self._isLock then
        GameWorld.AddIcon(self._iconContainer,icon,0,false,0)
    else
        GameWorld.AddIcon(self._iconContainer, icon)
    end
end

function PartnerSkillItem:OnPointerDown(pointerEventData)
    local conditionLevel
    if self._isLock then
        conditionLevel = self.data.grade
    end
    TipsManager:ShowSkillTips(self.data.skill, conditionLevel,self._type)
end

function PartnerSkillItem:OnPointerUp(pointerEventData)
    if self._isLock then
        return
    end
end