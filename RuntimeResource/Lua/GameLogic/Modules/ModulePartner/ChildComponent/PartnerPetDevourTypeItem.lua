local PartnerPetDevourTypeItem = Class.PartnerPetDevourTypeItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
PartnerPetDevourTypeItem.interface = GameConfig.ComponentsConfig.Component
PartnerPetDevourTypeItem.classPath = "Modules.ModulePartner.ChildComponent.PartnerPetDevourTypeItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function PartnerPetDevourTypeItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function PartnerPetDevourTypeItem:OnDestroy()
    self.data = nil
end

--override --{品质，中文字} 0表示清空选择
function PartnerPetDevourTypeItem:OnRefreshData(data)
    self.data = data
    self._infoText.text = LanguageDataHelper.GetLanguageData(data[2])
end

function PartnerPetDevourTypeItem:InitView()
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
end

function PartnerPetDevourTypeItem:ShowSelected(state)
    self._selected:SetActive(state)
end