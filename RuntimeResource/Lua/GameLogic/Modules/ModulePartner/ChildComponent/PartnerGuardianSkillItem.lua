-- PartnerGuardianSkillItem.lua
local PartnerGuardianSkillItem = Class.PartnerGuardianSkillItem(ClassTypes.BaseComponent)

PartnerGuardianSkillItem.interface = GameConfig.ComponentsConfig.PointableComponent
PartnerGuardianSkillItem.classPath = "Modules.ModulePartner.ChildComponent.PartnerGuardianSkillItem"

local SkillDataHelper = GameDataHelper.SkillDataHelper
local TipsManager = GameManager.TipsManager

function PartnerGuardianSkillItem:Awake()
	self._base.Awake(self)
	self._containerIcon = self:FindChildGO("Container_Icon")
end

function PartnerGuardianSkillItem:SetType(skillType)
	self._skillType = skillType
end

function PartnerGuardianSkillItem:UpdateData(data)
	if data == nil then
		return
	end
	self._skillId = data[2] 
	self._gradeOpen = data[1] --开启等级
	self._icon = SkillDataHelper.GetSkillIcon(self._skillId)
end

function PartnerGuardianSkillItem:UpdateLock(guardianGrade)
	self._guardianGrade = guardianGrade
	if self._gradeOpen then
		if self._gradeOpen > guardianGrade then
			GameWorld.AddIcon(self._containerIcon,self._icon,0,false,0)
		else
			GameWorld.AddIcon(self._containerIcon,self._icon)
		end
	end
end

function PartnerGuardianSkillItem:OnPointerDown()
	local condition
	if self._gradeOpen > self._guardianGrade then
		condition = self._gradeOpen
	end
	TipsManager:ShowSkillTips(self._skillId,condition,self._skillType)
end

function PartnerGuardianSkillItem:OnPointerUp()
	
end