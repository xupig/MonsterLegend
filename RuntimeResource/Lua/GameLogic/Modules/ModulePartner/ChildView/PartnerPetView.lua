require "Modules.ModulePartner.ChildComponent.PartnerSkillItem"
require "Modules.ModulePartner.ChildComponent.PartnerFeatherItem"
require "Modules.ModulePartner.ChildComponent.PartnerAttrItem"
require "Modules.ModulePartner.ChildComponent.PartnerMaterialItem"
require "Modules.ModulePartner.ChildView.PartnerPetDevourView"
require "Modules.ModulePartner.ChildView.PartnerPetSoulView"
require "Modules.ModulePartner.ChildView.PartnerTipsShowView"

local PartnerPetView = Class.PartnerPetView(ClassTypes.BaseLuaUIComponent)

PartnerPetView.interface = GameConfig.ComponentsConfig.Component
PartnerPetView.classPath = "Modules.ModulePartner.ChildView.PartnerPetView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggle = ClassTypes.UIToggle
local UIProgressBar = ClassTypes.UIProgressBar
local PartnerSkillItem = ClassTypes.PartnerSkillItem
local PartnerFeatherItem = ClassTypes.PartnerFeatherItem
local PartnerAttrItem = ClassTypes.PartnerAttrItem
local PartnerMaterialItem = ClassTypes.PartnerMaterialItem
local XArtNumber = GameMain.XArtNumber
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EntityConfig = GameConfig.EntityConfig
local PartnerManager = PlayerManager.PartnerManager
local BaseUtil = GameUtil.BaseUtil
local ActorModelComponent = GameMain.ActorModelComponent
local partnerData = PlayerManager.PlayerDataManager.partnerData
local PartnerPetDevourView = ClassTypes.PartnerPetDevourView
local LuaUIRawImage = GameMain.LuaUIRawImage
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PartnerPetSoulView = ClassTypes.PartnerPetSoulView
local PartnerTipsShowView = ClassTypes.PartnerTipsShowView
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PartnerType = GameConfig.EnumType.PartnerType
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local TransformManager = PlayerManager.TransformManager
local UIParticle = ClassTypes.UIParticle
local bagData = PlayerManager.PlayerDataManager.bagData
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

local setV2 = Vector2.New(0,0)
local Unit_Width = 40
local Max_Skill_Count = 5
local Max_Feather_Count = 3
local Max_Attr_Count = 4
local Max_Material_Count = 2

local function SortByOrder(a,b)
    return tonumber(a[4]) < tonumber(b[4])
end

function PartnerPetView:Awake()
    self._selectedGrade = 1
    self._lastLevel = 1
    self._lastStar = 1
    self._inUpgradeState = false
    self:InitCallBack()
    self:InitView()
end
--override
function PartnerPetView:OnDestroy() 

end

--data结构
function PartnerPetView:ShowView(data)
    self:AddEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModulePartner.ChildView.PartnerPetView")
    self:RefreshView()
    self:ShowModel()
end

function PartnerPetView:CloseView()
    PartnerManager:EndStarUp()
    self:RemoveEventListeners()
    self:HideModel()
end

function PartnerPetView:InitCallBack()
    self._onRefreshSelectedBlessItem = function(index) self:RefreshSelectedBlessItem(index) end
    self._onRefreshPetItemData = function() self:OnRefreshPetItemData() end
    self._onRefreshPetInfo = function() self:OnRefreshPetInfo() end
    self._onPetDevourViewClose = function() self:OnPetDevourViewClose() end
    self._onRefreshPetDevourRedPoint = function(state) self:ShowPetDevourRedPoint(state) end
    self._onRefreshPetUpgradeRedPoint = function(state) self:ShowPetUpgradeRedPoint(state) end
    self._onRefreshPetSoulRedPoint = function(state) self:ShowPetSoulRedPoint(state) end
    self._onRefreshStarUpState = function() self:RefreshAutoStarUpState() end
    self._onPetSoulViewClose = function() self:OnSoulViewClose() end
    self._updateOnShowCb =function ()self:UpdateOnShow() end
    self._updateTransformRedPointCb = function ()self:UpdateTransformRedPoint()end

    self._onLevelChangeCB = function() self:OnLevelChange() end
    self._onPetStarChangeCB = function() self:SetStarLevel() end
    self._onGradeChangeCB = function() self:OnGradeChange() end
    self._onExpChangeCB = function() self:OnExpChange() end
    self._onBlessChangeCB = function() self:OnBlessChange() end
end

function PartnerPetView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Selected_Bless_Item, self._onRefreshSelectedBlessItem)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Pet_Item_Data, self._onRefreshPetItemData)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Pet_Info, self._onRefreshPetInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Pet_Devour_View_Close, self._onPetDevourViewClose)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Pet_Devour_Red_Point, self._onRefreshPetDevourRedPoint)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Pet_Upgrade_Red_Point, self._onRefreshPetUpgradeRedPoint)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Pet_Soul_Red_Point, self._onRefreshPetSoulRedPoint)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Star_Up_State, self._onRefreshStarUpState)
    EventDispatcher:AddEventListener(GameEvents.On_Pet_Soul_View_Close, self._onPetSoulViewClose)
    EventDispatcher:AddEventListener(GameEvents.Update_Transform_Red_Point_State,self._updateTransformRedPointCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)

    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PET_LEVEL, self._onLevelChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PET_STAR, self._onPetStarChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PET_GRADE, self._onGradeChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PET_EXP, self._onExpChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PET_BLESS, self._onBlessChangeCB)
end

function PartnerPetView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Selected_Bless_Item, self._onRefreshSelectedBlessItem)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Pet_Item_Data, self._onRefreshPetItemData)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Pet_Info, self._onRefreshPetInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Pet_Devour_View_Close, self._onPetDevourViewClose)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Pet_Devour_Red_Point, self._onRefreshPetDevourRedPoint)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Pet_Upgrade_Red_Point, self._onRefreshPetUpgradeRedPoint)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Pet_Soul_Red_Point, self._onRefreshPetSoulRedPoint)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Star_Up_State, self._onRefreshStarUpState)
    EventDispatcher:RemoveEventListener(GameEvents.On_Pet_Soul_View_Close, self._onPetSoulViewClose)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)
    EventDispatcher:RemoveEventListener(GameEvents.Update_Transform_Red_Point_State,self._updateTransformRedPointCb)

    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PET_LEVEL, self._onLevelChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PET_STAR, self._onPetStarChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PET_GRADE, self._onGradeChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PET_EXP, self._onExpChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PET_BLESS, self._onBlessChangeCB)
end

function PartnerPetView:InitView()
    self.transform = self:FindChildGO("Button_Transform")
    self._csBH:AddClick(self.transform,function () self:OnTransformButtonClick() end)
    self._imgTransformTip = self:FindChildGO("Button_Transform/Image_Tip")
    self._transformFuncId = 609

    self.soul = self:FindChildGO("Button_Soul")
    self._csBH:AddClick(self.soul,function () self:OnSoulButtonClick() end)
    self.soulRedPoint = self:FindChildGO("Button_Soul/Image_Tip")
    self.upgrade = self:FindChildGO("Container_Bless/Button_Upgrade")
    self._csBH:AddClick(self.upgrade,function () self:OnUpgradeButtonClick() end)
    self._upgradeText = self:GetChildComponent("Container_Bless/Button_Upgrade/Text", "TextMeshWrapper")
    self.devour = self:FindChildGO("Container_Bless/Button_Devour")
    self._csBH:AddClick(self.devour,function () self:OnDevourButtonClick() end)
    self._preGradeGo = self:FindChildGO("Button_PreGrade")
    self._csBH:AddClick(self._preGradeGo,function () self:OnPreGradeButtonClick() end)
    self._nextGradeGo = self:FindChildGO("Button_NextGrade")
    self._csBH:AddClick(self._nextGradeGo,function () self:OnNextGradeButtonClick() end)

    self._upgradeRedPoint = self:FindChildGO("Container_Bless/Button_Upgrade/Image_Tip")
    self._devourRedPoint = self:FindChildGO("Container_Bless/Button_Devour/Image_Tip")
    self._upgradeRedPoint:SetActive(false)
    self._devourRedPoint:SetActive(false)

    self.nameContainer = self:FindChildGO("Container_Name")
    self.degreeContainer = self:FindChildGO("Container_Degree")

    self.glodRect = self:GetChildComponent("Container_StarLevel/Image_Glod", 'RectTransform')
    self.glodImage = self:GetChildComponent("Container_StarLevel/Image_Glod", 'ImageWrapper')
    self.glodImage:SetContinuousDimensionDirty(true)
    setV2.y = self.glodRect.sizeDelta.y

    self.notActiveGo = self:FindChildGO("Image_NotActive")
    self.starUpTip = self:FindChildGO("Image_StarUp").transform
    self.levelUpTip = self:FindChildGO("Image_LevelUp").transform
    self.starUpTip.localScale = Vector3.zero
    self.levelUpTip.localScale = Vector3.zero
    self._animScaleStarUp = self:AddChildComponent('Image_StarUp', XGameObjectTweenScale)
    self._animScaleLevelUp = self:AddChildComponent('Image_LevelUp', XGameObjectTweenScale)

    -- 
    -- self._showToggle = UIToggle.AddToggle(self.gameObject, "Toggle_Hide")
    -- if GameWorld.Player().pet_grade_show == 0 then
    --     self._showToggle:SetIsOn(false)
    -- else
    --     self._showToggle:SetIsOn(true)
    -- end
    -- self._showToggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)
    self._showToggleGo = self:FindChildGO("Toggle_Show")
    self._btnOnShow = self:FindChildGO("Toggle_Show/Background")
    self._csBH:AddClick(self._btnOnShow,function () self:OnShowClick() end)
    self._checkmarkShow = self:FindChildGO("Toggle_Show/Checkmark")

    self._skillList = {}
    for i=1, Max_Skill_Count do
        self._skillList[i] = self:AddChildLuaUIComponent("Container_Skills/Container_Skill"..i, PartnerSkillItem)
        self._skillList[i]:SetType(5)
    end

    self._attrList = {}
    for i=1, Max_Attr_Count do
        self._attrList[i] = self:AddChildLuaUIComponent("Container_Attrs/Container_Item"..i, PartnerAttrItem)
    end

    self._materialList = {}
    for i=1, Max_Material_Count do
        self._materialList[i] = self:AddChildLuaUIComponent("Container_Bless/Container_Material/Container_Item"..i, PartnerMaterialItem)
        self._materialList[i]:InitIndex(i)
    end

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self._levelText = self:GetChildComponent("Container_Level/Text_Level", "TextMeshWrapper")
    self:OnLevelChange()

    self:InitProgress()

    self:SetStarLevel()

    self._containerModel = self:FindChildGO("Container_Model")
    self._modelComponent = self:AddChildComponent('Container_Model', ActorModelComponent)
    self._modelComponent:SetStartSetting(Vector3(0, 0, -500), Vector3(0, 180, 0), 1)

    self.soulContainer = self:FindChildGO("Container_Soul")
    self.soulView = self:AddChildLuaUIComponent("Container_Soul", PartnerPetSoulView)
    self.soulContainer:SetActive(false)

    self.devourContainer = self:FindChildGO("Container_Devour")
    self.devourView = self:AddChildLuaUIComponent("Container_Devour", PartnerPetDevourView)
    self.devourContainer:SetActive(false)

    self:OnGradeChange(true)

    self.tipsShowView = self:AddChildLuaUIComponent("Container_Bless/Container_TipsShow", PartnerTipsShowView)
    self.expTipsShowView = self:AddChildLuaUIComponent("Container_Level/Container_TipsShowExp", PartnerTipsShowView)

    self:RefreshAutoStarUpState()

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30034")
    self._fx:SetLifeTime(2)
    self._fx:Stop()
end

function PartnerPetView:InitProgress()
    self._lastLevel = GameWorld.Player().pet_level
    self._lastStar = GameWorld.Player().pet_star
    self._lastBless = GameWorld.Player().pet_bless
    self._lastExp = GameWorld.Player().pet_exp

    self._expComp = UIProgressBar.AddProgressBar(self.gameObject,"Container_Level/ProgressBar_Exp")
    self._expComp:SetProgress(0)
    self._expComp:ShowTweenAnimate(true)
    self._expComp:SetMutipleTween(true)
    self._expComp:SetIsResetToZero(false)
    self._expComp:SetTweenTime(0.2)

    self:OnExpChange()

    self._blessComp = UIProgressBar.AddProgressBar(self.gameObject,"Container_Bless/ProgressBar_Bless")
    self._blessComp:SetProgress(0)
    self._blessComp:ShowTweenAnimate(true)
    self._blessComp:SetMutipleTween(true)
    self._blessComp:SetIsResetToZero(false)
    self._blessComp:SetTweenTime(0.2)

    self:OnBlessChange()
end

function PartnerPetView:OnExpChange()
    local diff = GameWorld.Player().pet_level - self._lastLevel
    local entity = GameWorld.Player()
    local cur_exp = entity.pet_exp
    local max_exp = PartnerDataHelper.GetPetLevelExp(entity.pet_level)
    self._expComp:SetProgress(diff + cur_exp/max_exp)
    self._expComp:SetProgressText(tostring(cur_exp).."/"..tostring(max_exp))
    --增加的经验值
    local add = 0
    for i=self._lastLevel+1, GameWorld.Player().pet_level do
        add = add + PartnerDataHelper.GetPetLevelExp(i-1)
    end
    add = add + GameWorld.Player().pet_exp - self._lastExp
    if add > 0 then
        self.expTipsShowView:ShowView(add)
    end
    self._lastLevel = GameWorld.Player().pet_level
    self._lastExp = GameWorld.Player().pet_exp
end

function PartnerPetView:OnBlessChange()
    local diff = GameWorld.Player().pet_star - self._lastStar
    local entity = GameWorld.Player()
    local cur_bless = entity.pet_bless
    local max_bless = PartnerDataHelper.GetPetBless(entity.pet_grade, entity.pet_star)
    self._blessComp:SetProgress(diff + cur_bless/max_bless)
    self._blessComp:SetProgressText(tostring(cur_bless).."/"..tostring(max_bless))
    --增加的祝福值
    local add = 0
    for i=self._lastStar+1, GameWorld.Player().pet_star do
        add = add + PartnerDataHelper.GetPetBless(entity.pet_grade, i-1)
    end
    add = add + GameWorld.Player().pet_bless - self._lastBless
    if add > 0 then
        self.tipsShowView:ShowView(add)
    end

    self._lastStar = GameWorld.Player().pet_star
    self._lastBless = GameWorld.Player().pet_bless
end

function PartnerPetView:OnLevelChange()
    self._levelText.text = "Lv."..GameWorld.Player().pet_level
    if self._lastPetLevel ~= nil and self._lastPetLevel ~= GameWorld.Player().pet_level then
        self:ShowLevelUp()
    end
    self._lastPetLevel = GameWorld.Player().pet_level
    self:OnRefreshPetInfo()
end

function PartnerPetView:OnGradeChange(init)
    self._selectedGrade = GameWorld.Player().pet_grade
    self:ShowSkillInfo()
    self:RefreshButtonState()
    self:OnRefreshPetInfo()
    if init ~= true then
        GameWorld.PlaySound(20)
    end
end

function PartnerPetView:ShowStarUp()
    GameWorld.PlaySound(19)
    self._animScaleStarUp:SetScaleTween(0, 1, 1, 1, 3, function()  
            self.starUpTip.localScale = Vector3.zero
        end)
    self._fx:Play()
end

function PartnerPetView:ShowLevelUp()
    self._animScaleLevelUp:SetScaleTween(0, 1, 1, 1, 3, function()  
            self.levelUpTip.localScale = Vector3.zero
        end)
    self._fx:Play()
end

-- function PartnerPetView:OnToggleValueChanged(state)
--     if state then
--         PartnerManager:PetShowReq(self._selectedGrade)
--     elseif self._selectedGrade == GameWorld.Player().pet_grade_show then
--         PartnerManager:PetHideReq()
--     end
-- end

function PartnerPetView:OnShowClick()
    TransformManager:RequestTransformShow(public_config.TREASURE_TYPE_PET,public_config.TREASURE_SHOW_ORIGIN_COMMON,self._selectedGrade)
end

function PartnerPetView:UpdateOnShow()
    local showInfo = GameWorld.Player().illusion_show_info[public_config.TREASURE_TYPE_PET]
    if showInfo[public_config.TREASURE_ILLUSION_SHOW_SYSTEM] == public_config.TREASURE_SHOW_ORIGIN_COMMON and
        showInfo[public_config.TREASURE_ILLUSION_SHOW_ID] == self._selectedGrade then
        self._btnOnShow:SetActive(false)
        self._checkmarkShow:SetActive(true)
    else
        self._btnOnShow:SetActive(true)
        self._checkmarkShow:SetActive(false)
    end
end

function PartnerPetView:RefreshView()
    self._selectedGrade = GameWorld.Player().pet_grade
    self:ShowSkillInfo()
    self:ShowAttriItem()
    self:ShowBlessItem()
    self:ShowName()
    self:ShowGrade()
    self:ShowFightPower()
    self:ShowSelectedBlessItem()
    self:RefreshSelectModel()
    self:RefreshButtonState()
    self:CheckTransformButtonOpen()

    self:ShowPetUpgradeRedPoint(PartnerManager:GetPetUpgradeRedPoint())
    self:ShowPetDevourRedPoint(PartnerManager:GetPetDevourRedPoint())
    self:ShowPetSoulRedPoint(PartnerManager:GetPetSoulRedPoint())
end

function PartnerPetView:OnRefreshPetItemData()
    self:ShowBlessItem()
    local data = PartnerDataHelper.GetPetEvoItems()
    self.soulView:RefreshView(data)
end

function PartnerPetView:OnTransformButtonClick()
    GUIManager.ShowPanelByFunctionId(self._transformFuncId)
    self:HideModel()
end

function PartnerPetView:CheckTransformButtonOpen()
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(self._transformFuncId)
    self.transform:SetActive(isOpen)
    if isOpen then
        self:UpdateTransformRedPoint()
    end
end

function PartnerPetView:OnSoulButtonClick()
    self._modelComponent:Hide()
    self.soulContainer:SetActive(true)
    local data = PartnerDataHelper.GetPetEvoItems()
    self.soulView:ShowView(data)
    PartnerManager:EndStarUp()
end

function PartnerPetView:OnSoulViewClose()
    self._modelComponent:Show()
    self.soulContainer:SetActive(false)
end

function PartnerPetView:RefreshAutoStarUpState()
    self._inUpgradeState = PartnerManager:InStarUpState()
    if self._inUpgradeState then
        self._upgradeText.text = LanguageDataHelper.GetContent(58621)
    else
        self._upgradeText.text = LanguageDataHelper.GetContent(58620)
    end
end

--一键升星
function PartnerPetView:OnUpgradeButtonClick()
    if self._inUpgradeState then
        --升星中停止操作
        PartnerManager:EndStarUp()
    else
        PartnerManager:StartStarUp()
    end
end

function PartnerPetView:OnDevourButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_partner_pet_devour_button")
    self.devourContainer:SetActive(true)
    self.devourView:ShowView()
    PartnerManager:EndStarUp()
end

function PartnerPetView:SetStarLevel()
    setV2.x = Unit_Width * GameWorld.Player().pet_star
    self.glodRect.sizeDelta = setV2
    self:OnRefreshPetInfo()
    if self._lastPetStar ~= nil and self._lastPetStar ~= GameWorld.Player().pet_star then
        self:ShowStarUp()
    end
    self._lastPetStar = GameWorld.Player().pet_star
end

function PartnerPetView:ShowSkillInfo()
    local grade = GameWorld.Player().pet_grade
    local skillsData = PartnerDataHelper.GetPetSkills(grade)
    for i=1, Max_Skill_Count do
        self._skillList[i]:OnRefreshData(skillsData[i])
    end
end

function PartnerPetView:ShowBlessItem()
    local blessItemData = PartnerDataHelper.GetPetBlessItems()
    for i=1, Max_Material_Count do
        self._materialList[i]:OnRefreshData(blessItemData[i])
    end
end

function PartnerPetView:ShowAttriItem()
    local attrisList = partnerData:GetPetAttris()
    local player = GameWorld.Player()
    local deltaData = PartnerDataHelper.GetPetAttriNextDelta(player.pet_grade, player.pet_star)
    local sortData = GlobalParamsHelper.GetParamValue(433)
    local attriData = {}
    for k,v in pairs(attrisList) do
        if sortData[k] ~= nil then
            table.insert(attriData, {k, v, deltaData[k], sortData[k]})
        end
    end
    table.sort(attriData, SortByOrder)
    for i=1, Max_Attr_Count do
        self._attrList[i]:OnRefreshData(attriData[i])
    end
end

function PartnerPetView:ShowName()
    local grade = self._selectedGrade
    local iconId = PartnerDataHelper.GetPetNameIcon(grade)
    GameWorld.AddIcon(self.nameContainer, iconId)
end

function PartnerPetView:ShowGrade()
    local grade = self._selectedGrade
    local iconId = PartnerDataHelper.GetPetGradeIcon(grade)
    GameWorld.AddIcon(self.degreeContainer, iconId)
end

function PartnerPetView:ShowFightPower()
    local roleAttris = partnerData:GetPetAttris()
    local attriList = {}
    for k,v in pairs(roleAttris) do
        table.insert(attriList, {["attri"]=k, ["value"]=v})
    end
    local fightPower = BaseUtil.CalculateFightPower(attriList, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)
end

function PartnerPetView:ShowSelectedBlessItem()
    local blessItemData = PartnerDataHelper.GetPetBlessItems()
    local select = 1
    for i=1, Max_Material_Count do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(blessItemData[i][1])
        if bagNum > 0 then
            select = i
            break
        end
    end
    self:RefreshSelectedBlessItem(select)
end

function PartnerPetView:RefreshSelectedBlessItem(index)
    for i=1, Max_Material_Count do
        if index == i then
            self._materialList[i]:ShowSelected(true)
        else
            self._materialList[i]:ShowSelected(false)
        end
    end
end

function PartnerPetView:OnRefreshPetInfo()
    self:ShowFightPower()
    self:ShowAttriItem()
    if self.soulView ~= nil then
        local data = PartnerDataHelper.GetPetEvoItems()
        self.soulView:RefreshView(data)
    end
end

function PartnerPetView:RefreshSelectModel()
    local scale = PartnerDataHelper.GetPetScale(self._selectedGrade)
    local y = PartnerDataHelper.GetPetRotate(self._selectedGrade)
    self._modelComponent:SetStartSetting(Vector3(0, 0, -500), Vector3(0, y, 0), scale)
    local modelId = PartnerDataHelper.GetPetModel(self._selectedGrade)
    self._modelComponent:LoadModel(modelId)
end

function PartnerPetView:OnPreGradeButtonClick()
    self._selectedGrade = self._selectedGrade - 1
    self:RefreshButtonState()
end

function PartnerPetView:OnNextGradeButtonClick()
    self._selectedGrade = self._selectedGrade + 1
    self:RefreshButtonState()
end

function PartnerPetView:RefreshButtonState()
    if self._selectedGrade <= 1 then
        self._preGradeGo:SetActive(false)
    else
        self._preGradeGo:SetActive(true)
    end
    local grade = GameWorld.Player().pet_grade
    if PartnerDataHelper.GetPet(self._selectedGrade + 1) == nil or self._selectedGrade == grade + 1 then
        self._nextGradeGo:SetActive(false)
    else
        self._nextGradeGo:SetActive(true)
    end
    if self._selectedGrade > grade then
        self._showToggleGo:SetActive(false)
        self.notActiveGo:SetActive(true)
    else
        self._showToggleGo:SetActive(true)
        self.notActiveGo:SetActive(false)
    end

    self:UpdateOnShow()
    -- if self._selectedGrade == GameWorld.Player().pet_grade_show then
    --     self._showToggle:SetIsOn(true)
    -- else
    --     self._showToggle:SetIsOn(false)
    -- end

    self:ShowName()
    self:ShowGrade()
    self:RefreshSelectModel()
end

function PartnerPetView:OnPetDevourViewClose()
    self.devourContainer:SetActive(false)
end

function PartnerPetView:ShowModel()
    self._containerModel:SetActive(true)
end

function PartnerPetView:HideModel()
    self._containerModel:SetActive(false)
end

function PartnerPetView:ShowPetDevourRedPoint(state)
    self._devourRedPoint:SetActive(state)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Partner_Red_Point, PartnerType.Pet)
end

function PartnerPetView:ShowPetUpgradeRedPoint(state)
    self._upgradeRedPoint:SetActive(state)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Partner_Red_Point, PartnerType.Pet)
end

function PartnerPetView:ShowPetSoulRedPoint(state)
    self.soulRedPoint:SetActive(state)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Partner_Red_Point, PartnerType.Pet)
end

function PartnerPetView:NeedShowRedPoint()
    return PartnerManager:GetPetUpgradeRedPoint() or PartnerManager:GetPetDevourRedPoint()
            or PartnerManager:GetPetSoulRedPoint()
end

function PartnerPetView:UpdateTransformRedPoint()
    local shouldShow = TransformManager:GetTransformRedPointState(public_config.TREASURE_TYPE_PET)
    self._imgTransformTip:SetActive(shouldShow)
end
