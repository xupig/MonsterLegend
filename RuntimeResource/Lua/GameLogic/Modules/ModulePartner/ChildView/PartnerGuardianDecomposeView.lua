require "Modules.ModulePartner.ChildComponent.PartnerGuardianDecomposeItem"


local PartnerGuardianDecomposeView = Class.PartnerGuardianDecomposeView(ClassTypes.BaseLuaUIComponent)

PartnerGuardianDecomposeView.interface = GameConfig.ComponentsConfig.Component
PartnerGuardianDecomposeView.classPath = "Modules.ModulePartner.ChildView.PartnerGuardianDecomposeView"

local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local PartnerManager = PlayerManager.PartnerManager
local PartnerGuardianDecomposeItem = ClassTypes.PartnerGuardianDecomposeItem
local UIList = ClassTypes.UIList
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local QualityType = GameConfig.EnumType.QualityType
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData

local Min_Item_Count = 24

function PartnerGuardianDecomposeView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._selectedItemList = {}
    self:InitView()
    self:AddEventListeners()
end
--override
function PartnerGuardianDecomposeView:OnDestroy() 
    self:RemoveEventListeners()
end
--data结构
function PartnerGuardianDecomposeView:ShowView(listData)
    self:RefreshView(listData)
end

function PartnerGuardianDecomposeView:AddEventListeners()
    self._onRefreshSelectedItem = function(itemData, selectState) self:RefreshSelectedItem(itemData, selectState) end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guardian_Selected_Decompose_Item, self._onRefreshSelectedItem)
end

function PartnerGuardianDecomposeView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guardian_Selected_Decompose_Item, self._onRefreshSelectedItem)
end

function PartnerGuardianDecomposeView:InitView()
    self._btnDecompose = self:FindChildGO("Button_Decompose")
    self._csBH:AddClick(self._btnDecompose,function () self:OnDecomposeClick() end)
    self._close = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._close,function () self:OnCloseButtonClick() end)

    self._txtGet = self:GetChildComponent("Text_Get", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(PartnerGuardianDecomposeItem)
    self._list:SetPadding(20, 0, 0, 38)
    self._list:SetGap(10, 5)
    self._list:SetDirection(UIList.DirectionLeftToRight, 8, 100)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    local itemIcon = self:FindChildGO("Container_ItemIcon")
    local cost = PartnerDataHelper:GetGuardianAttriCfg(1,1).cost
    local icon
    for k,v in pairs(cost) do
        icon = ItemDataHelper.GetIcon(k)
    end
    GameWorld.AddIcon(itemIcon,icon)
end

function PartnerGuardianDecomposeView:RefreshView(listData)
    

    self._listData = listData
    local count = #self._listData

    --填充空数据为了生成空格
	for i=count+1, Min_Item_Count do
        table.insert(self._listData, {})
	end
    self._list:SetDataList(self._listData)

    self:AutoSelected()
end

function PartnerGuardianDecomposeView:AutoSelected()
    self._selectedItemList = {}
    for i=1,#self._listData do
        local bagPos = self._listData[i].bagPos
        if bagPos then
            self._selectedItemList[bagPos] = self._listData[i]
            self._list:GetItem(i-1):Selected()
        else
            self._list:GetItem(i-1):Reset()
        end
    end
    self:ShowAllComposeGet()
end

function PartnerGuardianDecomposeView:OnDecomposeClick()
    local posList = {}
    for k,_ in pairs(self._selectedItemList) do
        table.insert(posList, k)
    end
    if #posList <= 0 then
        return
    end
    
    local params = ""
    for i=1, #posList-1 do
        params = params .. posList[i]..","
    end

    params = params .. posList[#posList]
    PartnerManager:GuardianDecomposeReq(params)
    self:OnCloseButtonClick()
end

function PartnerGuardianDecomposeView:OnCloseButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.On_Guardian_Decompose_View_Close)
end

function PartnerGuardianDecomposeView:OnListItemClicked(idx)
    local item = self._list:GetItem(idx)
    item:ChangeState()
end


function PartnerGuardianDecomposeView:RefreshSelectedItem(itemData, selectState)
    if selectState then
        self._selectedItemList[itemData.bagPos] = itemData
    else
        self._selectedItemList[itemData.bagPos] = nil
    end
    self:ShowAllComposeGet()
end

function PartnerGuardianDecomposeView:ShowAllComposeGet()
    local sum = 0
    for k,v in pairs(self._selectedItemList) do
        local itemId = v.cfg_id
        local count = PartnerDataHelper.GetGuardianResolveCfg(itemId).resolve*v:GetCount()
        sum = sum + count
    end

    self._txtGet.text = tostring(sum)
end