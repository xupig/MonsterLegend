require "Modules.ModulePartner.ChildComponent.PartnerSkillItem"
require "Modules.ModulePartner.ChildComponent.PartnerRideFeatherItem"
require "Modules.ModulePartner.ChildComponent.PartnerAttrItem"
require "Modules.ModulePartner.ChildComponent.PartnerRideMaterialItem"
require "Modules.ModulePartner.ChildView.PartnerTipsShowView"
require "Modules.ModulePartner.ChildView.PartnerRideSoulView"

local PartnerRideView = Class.PartnerRideView(ClassTypes.BaseLuaUIComponent)

PartnerRideView.interface = GameConfig.ComponentsConfig.Component
PartnerRideView.classPath = "Modules.ModulePartner.ChildView.PartnerRideView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggle = ClassTypes.UIToggle
local UIProgressBar = ClassTypes.UIProgressBar
local PartnerSkillItem = ClassTypes.PartnerSkillItem
local PartnerRideFeatherItem = ClassTypes.PartnerRideFeatherItem
local PartnerAttrItem = ClassTypes.PartnerAttrItem
local PartnerRideMaterialItem = ClassTypes.PartnerRideMaterialItem
local XArtNumber = GameMain.XArtNumber
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EntityConfig = GameConfig.EntityConfig
local PartnerManager = PlayerManager.PartnerManager
local BaseUtil = GameUtil.BaseUtil
local ActorModelComponent = GameMain.ActorModelComponent
local partnerData = PlayerManager.PlayerDataManager.partnerData
local LuaUIRawImage = GameMain.LuaUIRawImage
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PartnerType = GameConfig.EnumType.PartnerType
local PartnerTipsShowView = ClassTypes.PartnerTipsShowView
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PartnerRideSoulView = ClassTypes.PartnerRideSoulView
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local TransformManager = PlayerManager.TransformManager
local UIParticle = ClassTypes.UIParticle
local bagData = PlayerManager.PlayerDataManager.bagData
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

local setV2 = Vector2.New(0,0)
local Unit_Width = 40
local Max_Skill_Count = 5
local Max_Attr_Count = 4
local Max_Material_Count = 2

local function SortByOrder(a,b)
    return tonumber(a[4]) < tonumber(b[4])
end

function PartnerRideView:Awake()
    self._selectedGrade = 1
    self._lastStar = 1
    self._inUpgradeState = false
    self:InitCallBack()
    self:InitView()
end
--override
function PartnerRideView:OnDestroy() 

end

--data结构
function PartnerRideView:ShowView(data)
    self:AddEventListeners()
    self:RefreshView()
    self:ShowModel()
end

function PartnerRideView:CloseView()
    PartnerManager:EndRideStarUp()
    self:RemoveEventListeners()
    self:HideModel()
end

function PartnerRideView:InitCallBack()
    self._onRefreshSelectedBlessItem = function(index) self:RefreshSelectedBlessItem(index) end
    self._onRefreshRideItemData = function() self:OnRefreshRideItemData() end
    self._onRefreshRideInfo = function() self:OnRefreshRideInfo() end
    self._onRefreshRideUpgradeRedPoint = function(state) self:ShowRideUpgradeRedPoint(state) end
    self._onRefreshRideSoulRedPoint = function(state) self:ShowRideSoulRedPoint(state) end
    self._onRefreshStarUpState = function() self:RefreshAutoStarUpState() end
    self._onRideSoulViewClose = function() self:OnSoulViewClose() end
    self._updateOnShowCb =function () self:UpdateOnShow() end
    self._updateTransformRedPointCb = function ()self:UpdateTransformRedPoint()end

    self._onRideStarChangeCB = function() self:SetStarLevel() end
    self._onGradeChangeCB = function() self:OnGradeChange() end
    self._onBlessChangeCB = function() self:OnBlessChange() end
end

function PartnerRideView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ride_Selected_Bless_Item, self._onRefreshSelectedBlessItem)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Ride_Item_Data, self._onRefreshRideItemData)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Ride_Info, self._onRefreshRideInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Ride_Upgrade_Red_Point, self._onRefreshRideUpgradeRedPoint)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Ride_Soul_Red_Point, self._onRefreshRideSoulRedPoint)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ride_Star_Up_State, self._onRefreshStarUpState)
    EventDispatcher:AddEventListener(GameEvents.On_Ride_Soul_View_Close, self._onRideSoulViewClose)
    EventDispatcher:AddEventListener(GameEvents.Update_Transform_Red_Point_State,self._updateTransformRedPointCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)

    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_HORSE_STAR, self._onRideStarChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_HORSE_GRADE, self._onGradeChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_HORSE_BLESS, self._onBlessChangeCB)
end

function PartnerRideView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ride_Selected_Bless_Item, self._onRefreshSelectedBlessItem)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Ride_Item_Data, self._onRefreshRideItemData)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Ride_Info, self._onRefreshRideInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Ride_Upgrade_Red_Point, self._onRefreshRideUpgradeRedPoint)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Ride_Soul_Red_Point, self._onRefreshRideSoulRedPoint)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ride_Star_Up_State, self._onRefreshStarUpState)
    EventDispatcher:RemoveEventListener(GameEvents.On_Ride_Soul_View_Close, self._onRideSoulViewClose)
    EventDispatcher:RemoveEventListener(GameEvents.Update_Transform_Red_Point_State,self._updateTransformRedPointCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)

    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_HORSE_STAR, self._onRideStarChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_HORSE_GRADE, self._onGradeChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_HORSE_BLESS, self._onBlessChangeCB)
end

function PartnerRideView:InitView()
    self.transform = self:FindChildGO("Button_Transform")
    self._csBH:AddClick(self.transform,function () self:OnTransformButtonClick() end)
    self._imgTransformTip = self:FindChildGO("Button_Transform/Image_Tip")
    self._transformFuncId = 610

    self.soul = self:FindChildGO("Button_Soul")
    self._csBH:AddClick(self.soul,function () self:OnSoulButtonClick() end)
    self.soulRedPoint = self:FindChildGO("Button_Soul/Image_Tip")
    self.upgrade = self:FindChildGO("Container_Bless/Button_Upgrade")
    self._csBH:AddClick(self.upgrade,function () self:OnUpgradeButtonClick() end)
    self._upgradeText = self:GetChildComponent("Container_Bless/Button_Upgrade/Text", "TextMeshWrapper")
    self._preGradeGo = self:FindChildGO("Button_PreGrade")
    self._csBH:AddClick(self._preGradeGo,function () self:OnPreGradeButtonClick() end)
    self._nextGradeGo = self:FindChildGO("Button_NextGrade")
    self._csBH:AddClick(self._nextGradeGo,function () self:OnNextGradeButtonClick() end)

    self._upgradeRedPoint = self:FindChildGO("Container_Bless/Button_Upgrade/Image_Tip")
    self._upgradeRedPoint:SetActive(false)

    self.nameContainer = self:FindChildGO("Container_Name")
    self.degreeContainer = self:FindChildGO("Container_Degree")

    self.glodRect = self:GetChildComponent("Container_StarLevel/Image_Glod", 'RectTransform')
    self.glodImage = self:GetChildComponent("Container_StarLevel/Image_Glod", 'ImageWrapper')
    self.glodImage:SetContinuousDimensionDirty(true)
    setV2.y = self.glodRect.sizeDelta.y

    self.notActiveGo = self:FindChildGO("Image_NotActive")
    self.starUpTip = self:FindChildGO("Image_StarUp").transform
    self.levelUpTip = self:FindChildGO("Image_LevelUp").transform
    self.starUpTip.localScale = Vector3.zero
    self.levelUpTip.localScale = Vector3.zero
    self._animScaleStarUp = self:AddChildComponent('Image_StarUp', XGameObjectTweenScale)

    self._showToggleGo = self:FindChildGO("Toggle_Show")
    self._btnOnShow = self:FindChildGO("Toggle_Show/Background")
    self._csBH:AddClick(self._btnOnShow,function () self:OnShowClick() end)
    self._checkmarkShow = self:FindChildGO("Toggle_Show/Checkmark")
    -- self._showToggle = UIToggle.AddToggle(self.gameObject, "Toggle_Hide")
    -- self:RefreshToggleValue()
    -- self._showToggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

    self._skillList = {}
    for i=1, Max_Skill_Count do
        self._skillList[i] = self:AddChildLuaUIComponent("Container_Skills/Container_Skill"..i, PartnerSkillItem)
        self._skillList[i]:SetType(6)
    end

    self._attrList = {}
    for i=1, Max_Attr_Count do
        self._attrList[i] = self:AddChildLuaUIComponent("Container_Attrs/Container_Item"..i, PartnerAttrItem)
    end

    self._materialList = {}
    for i=1, Max_Material_Count do
        self._materialList[i] = self:AddChildLuaUIComponent("Container_Bless/Container_Material/Container_Item"..i, PartnerRideMaterialItem)
        self._materialList[i]:InitIndex(i)
    end

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self:InitProgress()

    self:SetStarLevel()

    self._containerModel = self:FindChildGO("Container_Model")
    self._modelComponent = self:AddChildComponent('Container_Model', ActorModelComponent)
    self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 90, 0), 1)

    self.soulContainer = self:FindChildGO("Container_Soul")
    self.soulView = self:AddChildLuaUIComponent("Container_Soul", PartnerRideSoulView)
    self.soulContainer:SetActive(false)

    self:OnGradeChange(true)

    self.tipsShowView = self:AddChildLuaUIComponent("Container_Bless/Container_TipsShow", PartnerTipsShowView)

    self:RefreshAutoStarUpState()

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30034")
    self._fx:SetLifeTime(2)
    self._fx:Stop()
end

function PartnerRideView:InitProgress()
    self._lastStar = GameWorld.Player().horse_star
    self._lastBless = GameWorld.Player().horse_bless
    
    self._blessComp = UIProgressBar.AddProgressBar(self.gameObject,"Container_Bless/ProgressBar_Bless")
    self._blessComp:SetProgress(0)
    self._blessComp:ShowTweenAnimate(true)
    self._blessComp:SetMutipleTween(true)
    self._blessComp:SetIsResetToZero(false)
    self._blessComp:SetTweenTime(0.2)

    self:OnBlessChange()
end

function PartnerRideView:OnBlessChange()
    local diff = GameWorld.Player().horse_star - self._lastStar
    local entity = GameWorld.Player()
    local cur_bless = entity.horse_bless
    local max_bless = PartnerDataHelper.GetRideBless(entity.horse_grade, entity.horse_star)
    self._blessComp:SetProgress(diff + cur_bless/max_bless)
    self._blessComp:SetProgressText(tostring(cur_bless).."/"..tostring(max_bless))
    --增加的祝福值
    local add = 0
    for i=self._lastStar+1, GameWorld.Player().horse_star do
        add = add + PartnerDataHelper.GetPetBless(entity.horse_grade, i-1)
    end
    add = add + GameWorld.Player().horse_bless - self._lastBless
    if add > 0 then
        self.tipsShowView:ShowView(add)
    end

    self._lastStar = GameWorld.Player().horse_star
    self._lastBless = GameWorld.Player().horse_bless
end

function PartnerRideView:OnGradeChange(init)
    self._selectedGrade = GameWorld.Player().horse_grade
    self:ShowSkillInfo()
    self:RefreshButtonState()
    self:OnRefreshRideInfo()
    --self:RefreshToggleValue()
    self:UpdateOnShow()
    if init ~= true then
        GameWorld.PlaySound(20)
    end
end

function PartnerRideView:ShowStarUp()
    GameWorld.PlaySound(19)
    self._animScaleStarUp:OnceOutQuad(1, 1.2, function()  
        self.starUpTip.localScale = Vector3.zero
    end)
    self._fx:Play()
end

-- function PartnerRideView:RefreshToggleValue()
--     if GameWorld.Player().horse_grade_show ~= self._selectedGrade then
--         self._showToggle:SetIsOn(false)
--     else
--         self._showToggle:SetIsOn(true)
--     end
-- end

function PartnerRideView:UpdateOnShow()
    local showInfo = GameWorld.Player().illusion_show_info[public_config.TREASURE_TYPE_HORSE]
    if showInfo[public_config.TREASURE_ILLUSION_SHOW_SYSTEM] == public_config.TREASURE_SHOW_ORIGIN_COMMON and
        showInfo[public_config.TREASURE_ILLUSION_SHOW_ID] == self._selectedGrade then
        self._btnOnShow:SetActive(false)
        self._checkmarkShow:SetActive(true)
    else
        self._btnOnShow:SetActive(true)
        self._checkmarkShow:SetActive(false)
    end
end

function PartnerRideView:OnShowClick()
    TransformManager:RequestTransformShow(public_config.TREASURE_TYPE_HORSE,public_config.TREASURE_SHOW_ORIGIN_COMMON,self._selectedGrade)
end

-- function PartnerRideView:OnToggleValueChanged(state)
--     if self._selectedGrade == GameWorld.Player().horse_grade_show then
--         self:RefreshToggleValue()
--         return
--     end
--     if state then
--         PartnerManager:RideShowReq(self._selectedGrade)
--     end
-- end

function PartnerRideView:RefreshView()
    self._selectedGrade = GameWorld.Player().horse_grade
    self:ShowSkillInfo()
    self:ShowAttriItem()
    self:ShowBlessItem()
    self:ShowName()
    self:ShowGrade()
    self:ShowFightPower()
    self:ShowSelectedBlessItem(1)
    self:RefreshSelectModel()
    self:RefreshButtonState()
    self:CheckTransformButtonOpen()

    self:ShowRideUpgradeRedPoint(PartnerManager:GetRideUpgradeRedPoint())
    self:ShowRideSoulRedPoint(PartnerManager:GetRideSoulRedPoint())
end

function PartnerRideView:OnRefreshRideItemData()
    self:ShowBlessItem()
    local data = PartnerDataHelper.GetRideEvoItems()
    self.soulView:RefreshView(data)
end

--幻化按钮
function PartnerRideView:OnTransformButtonClick()
    GUIManager.ShowPanelByFunctionId(self._transformFuncId)
    self:HideModel()
end

--检查幻化按钮
function PartnerRideView:CheckTransformButtonOpen()
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(self._transformFuncId)
    self.transform:SetActive(isOpen)
    if isOpen then
        self:UpdateTransformRedPoint()
    end
end

function PartnerRideView:OnSoulButtonClick()
    self._modelComponent:Hide()
    self.soulContainer:SetActive(true)
    local data = PartnerDataHelper.GetRideEvoItems()
    self.soulView:ShowView(data)
    PartnerManager:EndRideStarUp()
end

function PartnerRideView:OnSoulViewClose()
    self._modelComponent:Show()
    self.soulContainer:SetActive(false)
end

function PartnerRideView:RefreshAutoStarUpState()
    self._inUpgradeState = PartnerManager:InRideStarUpState()
    if self._inUpgradeState then
        self._upgradeText.text = LanguageDataHelper.GetContent(58621)
    else
        self._upgradeText.text = LanguageDataHelper.GetContent(58620)
    end
end

--一键升星
function PartnerRideView:OnUpgradeButtonClick()
    if self._inUpgradeState then
        --升星中停止操作
        PartnerManager:EndRideStarUp()
    else
        PartnerManager:StartRideStarUp()
    end
end

function PartnerRideView:SetStarLevel()
    setV2.x = Unit_Width * GameWorld.Player().horse_star
    self.glodRect.sizeDelta = setV2
    self:OnRefreshRideInfo()
    if self._lastRideStar ~= nil and self._lastRideStar ~= GameWorld.Player().horse_star then
        self:ShowStarUp()
    end
    self._lastRideStar = GameWorld.Player().horse_star
end

function PartnerRideView:ShowSkillInfo()
    local grade = GameWorld.Player().horse_grade
    local skillsData = PartnerDataHelper.GetRideSkills(grade)
    for i=1, Max_Skill_Count do
        self._skillList[i]:OnRefreshData(skillsData[i])
    end
end

function PartnerRideView:ShowBlessItem()
    local blessItemData = PartnerDataHelper.GetRideBlessItems()
    for i=1, Max_Material_Count do
        self._materialList[i]:OnRefreshData(blessItemData[i])
    end
end

function PartnerRideView:ShowAttriItem()
    local attrisList = partnerData:GetRideAttris()
    local player = GameWorld.Player()
    local deltaData = PartnerDataHelper.GetRideAttriNextDelta(player.horse_grade, player.horse_star)
    local sortData = GlobalParamsHelper.GetParamValue(489)
    local attriData = {}
    for k,v in pairs(attrisList) do
        table.insert(attriData, {k, v, deltaData[k], sortData[k]})
    end
    table.sort(attriData, SortByOrder)

    for i=1, Max_Attr_Count do
        self._attrList[i]:OnRefreshData(attriData[i])
    end
end

function PartnerRideView:ShowName()
    local grade = self._selectedGrade
    local iconId = PartnerDataHelper.GetRideNameIcon(grade)
    GameWorld.AddIcon(self.nameContainer, iconId)
end

function PartnerRideView:ShowGrade()
    local grade = self._selectedGrade
    local iconId = PartnerDataHelper.GetRideGradeIcon(grade)
    GameWorld.AddIcon(self.degreeContainer, iconId)
end

function PartnerRideView:ShowFightPower()
    local roleAttris = partnerData:GetRideAttris()
    local attriList = {}
    for k,v in pairs(roleAttris) do
        table.insert(attriList, {["attri"]=k, ["value"]=v})
    end
    local fightPower = BaseUtil.CalculateFightPower(attriList, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)
end

function PartnerRideView:ShowSelectedBlessItem()
    local blessItemData = PartnerDataHelper.GetRideBlessItems()
    local select = 1
    for i=1, Max_Material_Count do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(blessItemData[i][1])
        if bagNum > 0 then
            select = i
            break
        end
    end
    self:RefreshSelectedBlessItem(select)
end

function PartnerRideView:RefreshSelectedBlessItem(index)
    for i=1, Max_Material_Count do
        if index == i then
            self._materialList[i]:ShowSelected(true)
        else
            self._materialList[i]:ShowSelected(false)
        end
    end
end

function PartnerRideView:OnRefreshRideInfo()
    self:ShowFightPower()
    self:ShowAttriItem()
    if self.soulView ~= nil then
        local data = PartnerDataHelper.GetRideEvoItems()
        self.soulView:RefreshView(data)
    end
end

function PartnerRideView:RefreshSelectModel()
    local scale = PartnerDataHelper.GetRideScale(self._selectedGrade)
    self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 90, 0), scale)
    local modelId = PartnerDataHelper.GetRideModel(self._selectedGrade)
    self._modelComponent:LoadModel(modelId)
end

function PartnerRideView:OnPreGradeButtonClick()
    self._selectedGrade = self._selectedGrade - 1
    self:RefreshButtonState()
end

function PartnerRideView:OnNextGradeButtonClick()
    self._selectedGrade = self._selectedGrade + 1
    self:RefreshButtonState()
end

function PartnerRideView:RefreshButtonState()
    if self._selectedGrade <= 1 then
        self._preGradeGo:SetActive(false)
    else
        self._preGradeGo:SetActive(true)
    end
    local grade = GameWorld.Player().horse_grade
    if PartnerDataHelper.GetPet(self._selectedGrade + 1) == nil or self._selectedGrade == grade + 1 then
        self._nextGradeGo:SetActive(false)
    else
        self._nextGradeGo:SetActive(true)
    end
    if self._selectedGrade > grade then
        self._showToggleGo:SetActive(false)
        self.notActiveGo:SetActive(true)
    else
        self._showToggleGo:SetActive(true)
        self.notActiveGo:SetActive(false)
    end

    self:UpdateOnShow()
    --self:RefreshToggleValue()
    self:ShowName()
    self:ShowGrade()
    self:RefreshSelectModel()
end

function PartnerRideView:ShowRideUpgradeRedPoint(state)
    self._upgradeRedPoint:SetActive(state)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Partner_Red_Point, PartnerType.Ride)
end

function PartnerRideView:ShowModel()
    self._containerModel:SetActive(true)
end

function PartnerRideView:HideModel()
    self._containerModel:SetActive(false)
end

function PartnerRideView:ShowRideSoulRedPoint(state)
    self.soulRedPoint:SetActive(state)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Partner_Red_Point, PartnerType.Ride)
end

function PartnerRideView:NeedShowRedPoint()
    return PartnerManager:GetRideUpgradeRedPoint() or PartnerManager:GetRideSoulRedPoint()
end

function PartnerRideView:UpdateTransformRedPoint()
    local shouldShow = TransformManager:GetTransformRedPointState(public_config.TREASURE_TYPE_HORSE)
    self._imgTransformTip:SetActive(shouldShow)
end