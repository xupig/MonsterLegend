require "Modules.ModulePartner.ChildComponent.PartnerFeatherItem"

local PartnerPetSoulView = Class.PartnerPetSoulView(ClassTypes.BaseLuaUIComponent)

PartnerPetSoulView.interface = GameConfig.ComponentsConfig.Component
PartnerPetSoulView.classPath = "Modules.ModulePartner.ChildView.PartnerPetSoulView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EntityConfig = GameConfig.EntityConfig
local PartnerManager = PlayerManager.PartnerManager
local partnerData = PlayerManager.PlayerDataManager.partnerData
local PartnerFeatherItem = ClassTypes.PartnerFeatherItem
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local QualityType = GameConfig.EnumType.QualityType
local StringStyleUtil = GameUtil.StringStyleUtil

local Max_Feather_Count = 3

function PartnerPetSoulView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self:AddEventListeners()
end
--override
function PartnerPetSoulView:OnDestroy() 
    self:RemoveEventListeners()
end
--data结构
function PartnerPetSoulView:ShowView(data)
    self:RefreshView(data)
end

function PartnerPetSoulView:AddEventListeners()

end

function PartnerPetSoulView:RemoveEventListeners()

end

function PartnerPetSoulView:InitView()
    self._close = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._close,function () self:OnCloseButtonClick() end)

    self._descText = self:GetChildComponent("Text_Info", "TextMeshWrapper")

    self._featherList = {}
    for i=1, Max_Feather_Count do
        self._featherList[i] = self:AddChildLuaUIComponent("Container_Items/Container_Item"..i, PartnerFeatherItem)
    end
end

function PartnerPetSoulView:GetNextMaxLevel(data)
    local level = GameWorld.Player().level
    local index = 0
    for i=1,#data[2]/3 do
        if level <= data[2][i*3-1] then
            index = i
            break
        end
    end
    return data[2][index*3+1]
end

function PartnerPetSoulView:RefreshView(data)
    local featherData = data
    for i=1, Max_Feather_Count do
        self._featherList[i]:OnRefreshData(featherData[i])
    end

    local nextPlayerLevel = self:GetNextMaxLevel(data[1])
    if nextPlayerLevel then
        local str = StringStyleUtil.GetLevelString(nextPlayerLevel,nil,true)
        self._descText.text = LanguageDataHelper.CreateContentWithArgs(70059,{["0"] = str})
    else
        self._descText.text = ""
    end
end

function PartnerPetSoulView:OnCloseButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.On_Pet_Soul_View_Close)
end
