local PartnerGuardianView = Class.PartnerGuardianView(ClassTypes.BaseLuaUIComponent)

PartnerGuardianView.interface = GameConfig.ComponentsConfig.Component
PartnerGuardianView.classPath = "Modules.ModulePartner.ChildView.PartnerGuardianView"

require "Modules.ModulePartner.ChildComponent.PartnerGuardianSkillItem"
require "Modules.ModulePartner.ChildComponent.PartnerAttrItem"
require "Modules.ModulePartner.ChildComponent.PartnerGuardianListItem"
require "Modules.ModulePartner.ChildView.PartnerGuardianDecomposeView"
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local UIList = ClassTypes.UIList
local PartnerGuardianSkillItem = ClassTypes.PartnerGuardianSkillItem
local PartnerGuardianListItem = ClassTypes.PartnerGuardianListItem
local PartnerAttrItem = ClassTypes.PartnerAttrItem

local XArtNumber = GameMain.XArtNumber
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EntityConfig = GameConfig.EntityConfig
local PartnerManager = PlayerManager.PartnerManager
local BaseUtil = GameUtil.BaseUtil
local ActorModelComponent = GameMain.ActorModelComponent
local PartnerGuardianDecomposeView = ClassTypes.PartnerGuardianDecomposeView
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PartnerType = GameConfig.EnumType.PartnerType
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

local Max_Skill_Count = 5
local Max_Attr_Count = 4

function PartnerGuardianView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self:AddEventListeners()
end

--override
function PartnerGuardianView:OnDestroy() 
    self:RemoveEventListeners()
end
--data结构
function PartnerGuardianView:ShowView(data)    
    self:RefreshView()
    self:ShowModel()
end

function PartnerGuardianView:CloseView()
    self:HideModel()
end

function PartnerGuardianView:AddEventListeners()
    -- self._onRefreshSelectedBlessItem = function(index) self:RefreshSelectedBlessItem(index) end
    -- EventDispatcher:AddEventListener(GameEvents.Refresh_Selected_Bless_Item, self._onRefreshSelectedBlessItem)

    self._onUpdateGuardianCb = function()  self:OnGuardianChange() end
    self._onUpdateDecomposeCb = function()  self:UpdateDecomposeData() end
    self._onFightCB = function () self:UpdateOnFight() end
    self._onDecomposeCloseCb = function() self:OnDecomposeViewClose() end
    self._onUpgradeComplete = function () self:ShowLevelUp() end

    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_GUARDIAN_INFO, self._onUpdateGuardianCb)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_GUARDIAN_ON, self._onFightCB)

    EventDispatcher:AddEventListener(GameEvents.On_Guardian_Decompose_View_Close,self._onDecomposeCloseCb)
    EventDispatcher:AddEventListener(GameEvents.Guardian_Decompose_Complete,self._onUpdateDecomposeCb)
    EventDispatcher:AddEventListener(GameEvents.Guardian_Upgrade_Complete,self._onUpgradeComplete)
end

function PartnerGuardianView:RemoveEventListeners()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_GUARDIAN_INFO, self._onUpdateGuardianCb)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_GUARDIAN_ON, self._onFightCB)
    EventDispatcher:RemoveEventListener(GameEvents.On_Guardian_Decompose_View_Close,self._onDecomposeCloseCb)
    EventDispatcher:RemoveEventListener(GameEvents.Guardian_Decompose_Complete,self._onUpdateDecomposeCb)
    EventDispatcher:RemoveEventListener(GameEvents.Guardian_Upgrade_Complete,self._onUpgradeComplete)
end

function PartnerGuardianView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Guardian")
    self._listGuardian = list
    self._scrollView = scrollView
    self._listGuardian:SetItemType(PartnerGuardianListItem)
    self._listGuardian:SetPadding(0, 0, 0, 0)
    self._listGuardian:SetGap(1, 0)
    self._listGuardian:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB =                
    function(idx)
        self:OnListItemClicked(idx)
    end
    self._listGuardian:SetItemClickedCB(itemClickedCB)

    self._skillList = {}
    for i=1, Max_Skill_Count do
        self._skillList[i] = self:AddChildLuaUIComponent("Container_Skills/Container_Skill"..i, PartnerGuardianSkillItem)
        self._skillList[i]:SetType(7)
    end

    self._attrList = {}
    for i=1, Max_Attr_Count do
        self._attrList[i] = self:AddChildLuaUIComponent("Container_Attrs/Container_Item"..i, PartnerAttrItem)
    end

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self._levelUpTip = self:FindChildGO("Image_LevelUp").transform
    self._animScaleLevelUp = self:AddChildComponent('Image_LevelUp', XGameObjectTweenScale)
    self._levelUpTip.localScale = Vector3.zero

    self._containerModel = self:FindChildGO("Container_Model")
    self._modelComponent = self:AddChildComponent('Container_Model', ActorModelComponent)

    self._txtName = self:GetChildComponent("Container_Name/Text_Name",'TextMeshWrapper')
    
    --已激活状态
    self._containerUpgrade = self:FindChildGO("Container_Upgrade")

    self._btnUpgrade = self:FindChildGO("Container_Upgrade/Button_Upgrade")
    self._csBH:AddClick(self._btnUpgrade,function () self:OnUpgradeButtonClick() end)
    self._upgradeRedPoint = self:FindChildGO("Container_Upgrade/Button_Upgrade/Image_Tip")
    self._upgradeRedPoint:SetActive(false)

    self._btnDecompose = self:FindChildGO("Container_Upgrade/Button_Decompose")
    self._csBH:AddClick(self._btnDecompose,function () self:OnDecomposeClick() end)
    self._decomposeRedPoint = self:FindChildGO("Container_Upgrade/Button_Decompose/Image_Tip")
    self._decomposeRedPoint:SetActive(false)

    self._btnOnFight = self:FindChildGO("Container_Upgrade/Toggle_OnFight/Background")
    self._csBH:AddClick(self._btnOnFight,function () self:OnFightClick() end)
    self._checkmarkFight = self:FindChildGO("Container_Upgrade/Toggle_OnFight/Checkmark")

    self._upgradeMatIcon = self:AddChildLuaUIComponent("Container_Upgrade/Container_Material",ItemGridEx)
    self._upgradeMatIcon:ActivateTips()

    --组合阶数
    self._iconGrade1 = self:FindChildGO("Container_Upgrade/Container_Grade/Container_Num1")
    self._iconGrade2 = self:FindChildGO("Container_Upgrade/Container_Grade/Container_Num2")
    self._iconGrade3 = self:FindChildGO("Container_Upgrade/Container_Grade/Container_Num3")
    local iconGrade = self:FindChildGO("Container_Upgrade/Container_Grade/Container_Grade")
    GameWorld.AddIcon(self._iconGrade2,4060)
    GameWorld.AddIcon(iconGrade,4050)

    --未激活状态
    self._containerActivate = self:FindChildGO("Container_Activate")

    self._btnActivate = self:FindChildGO("Container_Activate/Button_Activate")
    self._csBH:AddClick(self._btnActivate,function () self:OnActivateClick() end)
    self._activateRedPoint = self:FindChildGO("Container_Activate/Button_Activate/Image_Tip")
    self._activateRedPoint:SetActive(false)

    self._activateMatIcon = self:AddChildLuaUIComponent("Container_Activate/Container_Material",ItemGridEx)
    self._activateMatIcon:ActivateTips()

    self._txtActivateTips = self:GetChildComponent("Container_Activate/Text_Tip",'TextMeshWrapper')
    self._txtFirstTips = self:GetChildComponent("Container_Activate/Text_FirstTips",'TextMeshWrapper')

    --分解子弹窗
    self._containerDecompose = self:AddChildLuaUIComponent("Container_Decompose", PartnerGuardianDecomposeView)
    self._containerDecompose:SetActive(false)
end

function PartnerGuardianView:OnGuardianChange()
    self:UpdateSelectedGuardian()
    if self._selectedGuadianItem then
        self._selectedGuadianItem:UpdateData()
    end
end

function PartnerGuardianView:OnListItemClicked(idx)
    if self._selectedGuadianItem then
        self._selectedGuadianItem:SetSelected(false)
    end
    self._selectedGuadianItem = self._listGuardian:GetItem(idx)
    self._selectedGuadianItem:SetSelected(true)
    self._selectedGuadianData =  self._listGuardian:GetDataByIndex(idx)
    self._selectedGuadianId = self._selectedGuadianData.id

    self:UpdateSelectedGuardian() 
end

function PartnerGuardianView:UpdateSelectedGuardian()
    local guardianInfo = GameWorld.Player().guardian_info
    local grade = guardianInfo[self._selectedGuadianId] or 0
    
    for i=1, Max_Skill_Count do
        local skillData
        if i == 1 then
            skillData = self._selectedGuadianData.active_skill[1]
        else
            skillData = self._selectedGuadianData.passive_skill[i-1]
        end
        self._skillList[i]:UpdateData({skillData[2],skillData[1]})
        self._skillList[i]:UpdateLock(grade)
    end

    local attriCfg = PartnerDataHelper:GetGuardianAttriCfg(self._selectedGuadianId,grade)
    local attrisList = attriCfg.attri
    local nextAttrisCfg = PartnerDataHelper:GetGuardianAttriCfg(self._selectedGuadianId,grade+1)
    local nextAttrisList
    if nextAttrisCfg then
        nextAttrisList = nextAttrisCfg.attri
    end

    local attriList = {}
    for i=1, Max_Attr_Count do
        local attriData = {}
        attriData[1] = attrisList[i][1]
        attriData[2] = attrisList[i][2]
        table.insert(attriList, {["attri"]=attriData[1], ["value"]=attriData[2]})
        if nextAttrisList then
            attriData[3] = nextAttrisList[i][2] - attrisList[i][2]
        end
        self._attrList[i]:OnRefreshData(attriData)
    end

    local fightPower = BaseUtil.CalculateFightPower(attriList, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)
    self._txtName.text = LanguageDataHelper.CreateContent(self._selectedGuadianData.name)
    
    local scale = self._selectedGuadianData.scale

    self:UpdateModel()

    --已激活
    if grade > 0 then
        self._containerActivate:SetActive(false)
        self._containerUpgrade:SetActive(true)
        for k,v in pairs(attriCfg.cost) do
            self._upgradeMatIcon:UpdateData(k,v)
        end
        local count,haveCount = self._upgradeMatIcon:GetCount()
        self:ShowUpgradePoint(count <= haveCount)

        self:SetGrade(grade)
        self:UpdateOnFight()
    --未激活
    else
        self._containerActivate:SetActive(true)
        self._containerUpgrade:SetActive(false)
        local beforeId = self._selectedGuadianId - 1
        local beforeGrade = guardianInfo[beforeId] or 0
        local canActivate
        if attriCfg.cost then
            for k,v in pairs(attriCfg.cost) do
                self._activateMatIcon:SetActive(true)
                self._activateMatIcon:UpdateData(k,v)
                local count,haveCount = self._activateMatIcon:GetCount()
                canActivate = count <= haveCount and beforeGrade >= 10
            end
            self._txtFirstTips.text = ""
        else
            self._activateMatIcon:SetActive(false)
            canActivate = true
            self._txtFirstTips.text = LanguageDataHelper.CreateContent(58669)
        end
    
        self:ShowActiveRedPoint(canActivate)

        if self._selectedGuadianId == 1 then
            self._txtActivateTips.text = ""
        else
            local nameId = PartnerDataHelper.GetGuardianBaseCfg(self._selectedGuadianId-1).name
            local str = LanguageDataHelper.CreateContent(nameId)
            self._txtActivateTips.text = LanguageDataHelper.CreateContentWithArgs(58668,{["0"] = str})
        end
    end
end

function PartnerGuardianView:SetGrade(grade)
    local a = math.floor(grade/10)
    local b = grade%10

    if a < 2 then
        self._iconGrade3:SetActive(false)
    else
        self._iconGrade3:SetActive(true)
        GameWorld.AddIcon(self._iconGrade3,4050+a)
    end

    if grade < 10 then
        self._iconGrade2:SetActive(false)
    else
        self._iconGrade2:SetActive(true)
    end

    if b == 0 then
        self._iconGrade1:SetActive(false)
    else
        self._iconGrade1:SetActive(true)
        GameWorld.AddIcon(self._iconGrade1,4050+b)
    end
end

function PartnerGuardianView:UpdateModel()
    local modelId = self._selectedGuadianData.model

    local posx = self._selectedGuadianData.pos[1]
    local posy = self._selectedGuadianData.pos[2]
    local posz = self._selectedGuadianData.pos[3]
    local rotationx = self._selectedGuadianData.rotation[1]
    local rotationy = self._selectedGuadianData.rotation[2]
    local rotationz = self._selectedGuadianData.rotation[3]
    local scale = self._selectedGuadianData.scale
    self._modelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
    self._modelComponent:LoadModel(modelId)
end

function PartnerGuardianView:OnUpgradeButtonClick()
    if self._selectedGuadianId then
        PartnerManager:GuardianUpgradeReq(self._selectedGuadianId)
    end
end

--升阶成功
function PartnerGuardianView:ShowLevelUp()
    self._animScaleLevelUp:OnceOutQuad(1, 1.2, function()  
        self._levelUpTip.localScale = Vector3.zero
    end)
    self._modelComponent:PlayFx("Assets/Resources/Fx/UI/fx_born_01.prefab", Vector3.zero, 1)
    self:UpdateDecomposeData()
end

function PartnerGuardianView:OnActivateClick()
    if self._selectedGuadianId then
        PartnerManager:GuardianUpgradeReq(self._selectedGuadianId)
    end
    
end

function PartnerGuardianView:OnFightClick()
    if self._selectedGuadianId then
        --LoggerHelper.Error("self._selectedGuadianId"..self._selectedGuadianId)
        PartnerManager:GuardianSelectFightReq(self._selectedGuadianId)
    end
end

function PartnerGuardianView:UpdateOnFight()
    local inFightGurdian = GameWorld.Player().guardian_on or 0
    if inFightGurdian == self._selectedGuadianId then
        self._btnOnFight:SetActive(false)
        self._checkmarkFight:SetActive(true)
    else
        self._btnOnFight:SetActive(true)
        self._checkmarkFight:SetActive(false)
    end
end

function PartnerGuardianView:RefreshView()
    self:UpdateGuardianList()
    self:OnListItemClicked(0)
    self:UpdateDecomposeData()
    -- self:ShowPetUpgradeRedPoint(PartnerManager:GetPetUpgradeRedPoint())
    -- self:ShowPetDevourRedPoint(PartnerManager:GetPetDevourRedPoint())
end

function PartnerGuardianView:UpdateGuardianList()
   local listData = PartnerDataHelper:GetGuardianBaseList()
   self._listGuardian:SetDataList(listData)
end

function PartnerGuardianView:OnDecomposeClick()
    self:HideModel()
    self._containerDecompose:SetActive(true)
    self._containerDecompose:ShowView(self._decomposeListData)
end

function PartnerGuardianView:OnDecomposeViewClose()
    self:ShowModel()
    self._containerDecompose:SetActive(false)
end

function PartnerGuardianView:ShowModel()
    self._containerModel:SetActive(true)
end

function PartnerGuardianView:HideModel()
    self._containerModel:SetActive(false)
end

function PartnerGuardianView:UpdateDecomposeData()
    self._decomposeListData = PartnerManager:GetDecomposeData()
    local len = #self._decomposeListData
    self._decomposeRedPoint:SetActive(len > 0)
end

function PartnerGuardianView:ShowUpgradePoint(state)
    self._upgradeRedPoint:SetActive(state)
end

function PartnerGuardianView:ShowActiveRedPoint(state)
    self._activateRedPoint:SetActive(state)
end