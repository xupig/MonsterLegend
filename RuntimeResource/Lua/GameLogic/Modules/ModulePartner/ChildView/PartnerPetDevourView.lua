require "Modules.ModulePartner.ChildComponent.PartnerPetDevourEquipItem"
require "Modules.ModulePartner.ChildComponent.PartnerPetDevourTypeItem"
require "Modules.ModulePartner.ChildComponent.PartnerPetDevourGradeItem"


local PartnerPetDevourView = Class.PartnerPetDevourView(ClassTypes.BaseLuaUIComponent)

PartnerPetDevourView.interface = GameConfig.ComponentsConfig.Component
PartnerPetDevourView.classPath = "Modules.ModulePartner.ChildView.PartnerPetDevourView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EntityConfig = GameConfig.EntityConfig
local PartnerManager = PlayerManager.PartnerManager
local partnerData = PlayerManager.PlayerDataManager.partnerData
local PartnerPetDevourEquipItem = ClassTypes.PartnerPetDevourEquipItem
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local PartnerPetDevourTypeItem = ClassTypes.PartnerPetDevourTypeItem
local PartnerPetDevourGradeItem = ClassTypes.PartnerPetDevourGradeItem
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local QualityType = GameConfig.EnumType.QualityType

local Min_Item_Count = 24

local Type_Data = {{QualityType.Red,27026}, {QualityType.Orange,27027}, {QualityType.Purple,27028}, {-1,27029}}  --{品质，中文字} -1表示清空选择
local Grade_Data = {{15,27041}, {14,27040},{13,27039},{12,27038},{11,27037},{10,27036},{9,27035},
                    {8,27034},{7,27033},{6,27032},{4,27031},{100,27030}, {-1,27029}}  --{阶数，中文字} -1表示清空选择 100表示任意

function PartnerPetDevourView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._typeShow = false
    self._gradeShow = false
    self._selectedItemList = {}
    self:InitView()
    self:AddEventListeners()
end
--override
function PartnerPetDevourView:OnDestroy() 
    self:RemoveEventListeners()
end
--data结构
function PartnerPetDevourView:ShowView(data)
    self:RefreshView()
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModulePartner.ChildView.PartnerPetDevourView")
end

function PartnerPetDevourView:AddEventListeners()
    self._onRefreshSelectedEquipItem = function(pos, selectState) self:RefreshSelectedEquipItem(pos, selectState) end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ride_Selected_Equip_Item, self._onRefreshSelectedEquipItem)
end

function PartnerPetDevourView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ride_Selected_Equip_Item, self._onRefreshSelectedEquipItem)
end

function PartnerPetDevourView:InitView()
    self._devour = self:FindChildGO("Button_Devour")
    self._csBH:AddClick(self._devour,function () self:OnDevourButtonClick() end)
    self._close = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._close,function () self:OnCloseButtonClick() end)

    self._selectToggle = UIToggle.AddToggle(self.gameObject, "Toggle_Select")
    self._selectToggle:SetIsOn(false)
    self._selectToggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

    self._expText = self:GetChildComponent("Text_Exp/Text_Value", "TextMeshWrapper")
    self._vipText = self:GetChildComponent("Text_Vip", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(PartnerPetDevourEquipItem)
    self._list:SetPadding(20, 0, 0, 38)
    self._list:SetGap(10, 5)
    self._list:SetDirection(UIList.DirectionLeftToRight, 8, 100)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._typeScrollViewGo = self:FindChildGO("Container_Type/ScrollViewList")
    local scrollViewType, listType = UIList.AddScrollViewList(self.gameObject, "Container_Type/ScrollViewList")
    self._listType = listType
	self._scrollViewType = scrollViewType
    self._listType:SetItemType(PartnerPetDevourTypeItem)
    self._listType:SetPadding(0, 0, 0, 0)
    self._listType:SetGap(4, 0)
    self._listType:SetDirection(UIList.DirectionTopToDown, 1, 100)
	local itemClickedCB = 
	function(idx)
		self:OnTypeListItemClicked(idx)
	end
    self._listType:SetItemClickedCB(itemClickedCB)
    self._typeSelect = self:FindChildGO("Container_Type/Button_Show")
    self._csBH:AddClick(self._typeSelect,function () self:OnTypeSelectButtonClick() end)
    self._typeText = self:GetChildComponent("Container_Type/Text_Select", "TextMeshWrapper")
    self._typeScrollViewGo:SetActive(false)

    self._gradeScrollViewGo = self:FindChildGO("Container_Grade/ScrollViewList")
    local scrollViewGrade, listGrade = UIList.AddScrollViewList(self.gameObject, "Container_Grade/ScrollViewList")
    self._listGrade = listGrade
	self._scrollViewGrade = scrollViewGrade
    self._listGrade:SetItemType(PartnerPetDevourGradeItem)
    self._listGrade:SetPadding(0, 0, 0, 0)
    self._listGrade:SetGap(4, 0)
    self._listGrade:SetDirection(UIList.DirectionTopToDown, 1, 100)
	local itemClickedCB = 
	function(idx)
		self:OnGradeListItemClicked(idx)
	end
    self._listGrade:SetItemClickedCB(itemClickedCB)
    self._gradeSelect = self:FindChildGO("Container_Grade/Button_Show")
    self._csBH:AddClick(self._gradeSelect,function () self:OnGradeSelectButtonClick() end)
    self._gradeText = self:GetChildComponent("Container_Grade/Text_Select", "TextMeshWrapper")
    self._gradeScrollViewGo:SetActive(false)
end

function PartnerPetDevourView:RefreshView()
    self._selectedItemList = {}
    self._curSelectedStar = 0
    self._curSelectedType = QualityType.Purple
    self._curSelectedGrade = 100
    local levelData = PartnerDataHelper.GetPetLevelItems()
    local equips = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemListByItemType(public_config.ITEM_ITEMTYPE_EQUIP)
    local needData = {}
    local count = 0
    for k,v in pairs(equips) do
        --type的1至8可以默认选中吞噬，9和10只能手中选中吞噬，11和12不纳入吞噬范围
        if v.cfgData.type <= 10 and v.cfgData.quality >= QualityType.Purple then
            table.insert(needData, v)
            count = count + 1
        end
    end
    for k,v in pairs(levelData) do
        if bagData:GetPkg(public_config.PKG_TYPE_ITEM):HasItemByItemId(k) then
            local pkgPos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(k)
            local data = bagData:GetItem(public_config.PKG_TYPE_ITEM, pkgPos)
            table.insert(needData, data)
            count = count + 1
        end
    end
    --填充空数据为了生成空格
    local addCount = math.max(Min_Item_Count, count) - count
	for i=1, addCount do
        table.insert(needData, {})
    end
    self._list:SetDataList(needData)

    self._listGrade:SetDataList(Grade_Data)
    self._listType:SetDataList(Type_Data)
    self:OnGradeListItemClicked(11) --默认任意
    self:OnTypeListItemClicked(2) --默认紫色
    self._selectToggle:SetIsOn(false) --默认不选中
    self:OnToggleValueChanged(false)

    self:AutoSelected()
end

function PartnerPetDevourView:OnToggleValueChanged(state)
    if state then
        self._curSelectedStar = 1
    else
        self._curSelectedStar = 0
    end
    self:AutoSelected()
end

function PartnerPetDevourView:OnDevourButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_partner_pet_begin_devour_button")
    self:OnCloseButtonClick()
    local posList = {}
    for k,_ in pairs(self._selectedItemList) do
        table.insert(posList, k)
    end
    if #posList <= 0 then
        return
    end
    PartnerManager:PetLevelUpReq(posList)
end

function PartnerPetDevourView:OnCloseButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.On_Pet_Devour_View_Close)
    self._list:RemoveAll()
end

function PartnerPetDevourView:OnListItemClicked(idx)
    local item = self._list:GetItem(idx)
    item:ChangeState()
end

function PartnerPetDevourView:OnTypeListItemClicked(idx)
    local data = self._listType:GetDataByIndex(idx)
    self._typeText.text = LanguageDataHelper.GetLanguageData(data[2])
    self:ResetTypeScrollViewGo()
    self._curSelectedType = data[1]
    self:AutoSelected()
end

function PartnerPetDevourView:OnGradeListItemClicked(idx)
    local data = self._listGrade:GetDataByIndex(idx)
    self._gradeText.text = LanguageDataHelper.GetLanguageData(data[2])
    self:ResetGradeScrollViewGo()
    self._curSelectedGrade = data[1]
    self:AutoSelected()
end

function PartnerPetDevourView:OnTypeSelectButtonClick()
    self._typeShow = not self._typeShow
    self._typeScrollViewGo:SetActive(self._typeShow)
end

function PartnerPetDevourView:OnGradeSelectButtonClick()
    self._gradeShow = not self._gradeShow
    self._gradeScrollViewGo:SetActive(self._gradeShow)
end

function PartnerPetDevourView:ResetTypeScrollViewGo()
    self._typeShow = false
    self._typeScrollViewGo:SetActive(self._typeShow)
end

function PartnerPetDevourView:ResetGradeScrollViewGo()
    self._gradeShow = false
    self._gradeScrollViewGo:SetActive(self._gradeShow)
end

function PartnerPetDevourView:RefreshSelectedEquipItem(pos, selectState)
    if selectState then
        self._selectedItemList[pos] = 1
    else
        self._selectedItemList[pos] = nil
    end
    self:ShowAllSelectPetExp()
end

function PartnerPetDevourView:AutoSelected()
    self._selectedItemList = {}
    local levelData = PartnerDataHelper.GetPetLevelItems()
    for i=0, self._list:GetLength()-1  do
        local data = self._list:GetDataByIndex(i)
        local item = self._list:GetItem(i)
        if levelData[data.cfg_id] ~= nil then
            item:Selected()
            self:RefreshSelectedEquipItem(data.bagPos, true)  --由于UIList不会刷新看不到的item，在这里刷新下选中数据
        else
            --type的1至8可以默认选中吞噬，9和10只能手中选中吞噬
            if data.cfgData ~= nil and data.cfgData.quality == self._curSelectedType and data.cfgData.grade <= self._curSelectedGrade
                    and data.cfgData.star <= self._curSelectedStar and data.cfgData.type <= 8 then
                item:Selected()
                self:RefreshSelectedEquipItem(data.bagPos, true)  --由于UIList不会刷新看不到的item，在这里刷新下选中数据
            else
                item:UnSelected()
            end
        end

    end
    self:ShowAllSelectPetExp()
end

function PartnerPetDevourView:ShowAllSelectPetExp()
    local sum = 0
    local levelData = PartnerDataHelper.GetPetLevelItems()
    for k,v in pairs(self._selectedItemList) do
        local itemData = bagData:GetItem(public_config.PKG_TYPE_ITEM, k)
        if itemData ~= nil then
            if itemData.cfgData.petExp == nil then
                sum = sum + levelData[itemData.cfg_id] * itemData:GetCount()
            else
                sum = sum + itemData.cfgData.petExp
            end
        end
    end
    self._expText.text = "+"..sum
end