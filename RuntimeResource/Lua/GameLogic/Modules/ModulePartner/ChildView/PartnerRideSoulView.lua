require "Modules.ModulePartner.ChildComponent.PartnerRideFeatherItem"

local PartnerRideSoulView = Class.PartnerRideSoulView(ClassTypes.BaseLuaUIComponent)

PartnerRideSoulView.interface = GameConfig.ComponentsConfig.Component
PartnerRideSoulView.classPath = "Modules.ModulePartner.ChildView.PartnerRideSoulView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EntityConfig = GameConfig.EntityConfig
local PartnerManager = PlayerManager.PartnerManager
local partnerData = PlayerManager.PlayerDataManager.partnerData
local PartnerRideFeatherItem = ClassTypes.PartnerRideFeatherItem
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local QualityType = GameConfig.EnumType.QualityType
local StringStyleUtil = GameUtil.StringStyleUtil

local Max_Feather_Count = 3

function PartnerRideSoulView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self:AddEventListeners()
end
--override
function PartnerRideSoulView:OnDestroy() 
    self:RemoveEventListeners()
end
--data结构
function PartnerRideSoulView:ShowView(data)
    self:RefreshView(data)
end

function PartnerRideSoulView:AddEventListeners()

end

function PartnerRideSoulView:RemoveEventListeners()

end

function PartnerRideSoulView:InitView()
    self._close = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._close,function () self:OnCloseButtonClick() end)

    self._descText = self:GetChildComponent("Text_Info", "TextMeshWrapper")

    self._featherList = {}
    for i=1, Max_Feather_Count do
        self._featherList[i] = self:AddChildLuaUIComponent("Container_Items/Container_Item"..i, PartnerRideFeatherItem)
    end
end

function PartnerRideSoulView:GetNextMaxLevel(data)
    local level = GameWorld.Player().level
    local index = 0
    for i=1,#data[2]/3 do
        if level <= data[2][i*3-1] then
            index = i
            break
        end
    end
    return data[2][index*3+1]
end

function PartnerRideSoulView:RefreshView(data)
    local featherData = data
    for i=1, Max_Feather_Count do
        self._featherList[i]:OnRefreshData(featherData[i])
    end

    local nextPlayerLevel = self:GetNextMaxLevel(data[1])
    if nextPlayerLevel then
        local str = StringStyleUtil.GetLevelString(nextPlayerLevel,nil,true)
        self._descText.text = LanguageDataHelper.CreateContentWithArgs(70059,{["0"] = str})
    else
        self._descText.text = ""
    end
end

function PartnerRideSoulView:OnCloseButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.On_Ride_Soul_View_Close)
end
