-- AnimalListItem.lua
local AnimalListItem = Class.AnimalListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
AnimalListItem.interface = GameConfig.ComponentsConfig.Component
AnimalListItem.classPath = "Modules.ModuleAnimal.ChildComponent.AnimalListItem"

local MythicalAnimalsDataHelper = GameDataHelper.MythicalAnimalsDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AnimalManager = PlayerManager.AnimalManager
local StringStyleUtil = GameUtil.StringStyleUtil

function AnimalListItem:Awake()
    self._icon = self:FindChildGO("Container_Icon")
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper') 
    self._scoreText = self:GetChildComponent("Text_Score",'TextMeshWrapper') 
    self._selectImageGO = self:FindChildGO("Image_Select")   
    self._redImageGO = self:FindChildGO("Image_Red")   
    self._helpContainer = self:FindChildGO("Container_Help")
end

function AnimalListItem:OnDestroy() 

end

--override
function AnimalListItem:OnRefreshData(id)
    self._nameText.text = LanguageDataHelper.CreateContent(MythicalAnimalsDataHelper:GetName(id))
    local scoreText = LanguageDataHelper.CreateContent(76451, {["0"] = AnimalManager:GetAnimalTotalScore(id)})
    if AnimalManager:IsHelp(id) then
        self._scoreText.text = StringStyleUtil.GetColorStringWithColorId(scoreText, 102)
    else
        self._scoreText.text = scoreText
    end    

    --是否觉醒了
    if AnimalManager:IsAwake(id) then
        GameWorld.AddIcon(self._icon, MythicalAnimalsDataHelper:GetIcon(id))
    else
        GameWorld.AddIcon(self._icon, MythicalAnimalsDataHelper:GetIcon(id), 0, false, 0)
    end
    self._redImageGO:SetActive(false)

    --是否助战中
    self._helpContainer:SetActive(AnimalManager:IsHelp(id))
    
    self._redImageGO:SetActive(AnimalManager:AnimalNeedRedPoint(id))
end

function AnimalListItem:SetSelect(flag)
    self._selectImageGO:SetActive(flag)
end