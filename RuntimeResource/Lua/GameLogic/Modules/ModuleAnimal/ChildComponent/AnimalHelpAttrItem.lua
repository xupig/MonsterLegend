-- AnimalHelpAttrItem.lua
local AnimalHelpAttrItem = Class.AnimalHelpAttrItem(ClassTypes.UIListItem)

AnimalHelpAttrItem.interface = GameConfig.ComponentsConfig.Component
AnimalHelpAttrItem.classPath = "Modules.ModuleAnimal.ChildComponent.AnimalHelpAttrItem"

local AttriDataHelper = GameDataHelper.AttriDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

--override
function AnimalHelpAttrItem:Awake()
    self._nameText = self:GetChildComponent("Text_Attr_Name",'TextMeshWrapper')    
    self._valueText = self:GetChildComponent("Text_Attr_Value",'TextMeshWrapper')
end

--override
function AnimalHelpAttrItem:OnDestroy() 
    self._nameText = nil
    self._valueText = nil
end

function AnimalHelpAttrItem:OnRefreshData(data)
    -- LoggerHelper.Error("data =====" .. PrintTable:TableToStr(data) )
    local addValue = data[3] or 0
    self._nameText.text = AttriDataHelper:GetName(data[1])
    if addValue == 0 then
        self._valueText.text = data[2]
    else
        self._valueText.text = data[2] .. StringStyleUtil.GetColorStringWithColorId("+" .. addValue, 102) 
    end
    -- self._valueText.text = data[2] .. StringStyleUtil.GetColorStringWithColorId("+" .. data[3], StringStyleUtil.green) 
    -- self._valueText.text = tostring(data[2]) .. "+" .. tostring(addValue)
end