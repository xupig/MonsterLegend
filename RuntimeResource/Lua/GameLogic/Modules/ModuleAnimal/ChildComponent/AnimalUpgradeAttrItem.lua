-- AnimalUpgradeAttrItem.lua
local AnimalUpgradeAttrItem = Class.AnimalUpgradeAttrItem(ClassTypes.UIListItem)

AnimalUpgradeAttrItem.interface = GameConfig.ComponentsConfig.Component
AnimalUpgradeAttrItem.classPath = "Modules.ModuleAnimal.ChildComponent.AnimalUpgradeAttrItem"

local AttriDataHelper = GameDataHelper.AttriDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

--override
function AnimalUpgradeAttrItem:Awake()
    self._nameText = self:GetChildComponent("Text_Attr_Name",'TextMeshWrapper')    
    self._valueText = self:GetChildComponent("Text_Attr_Value",'TextMeshWrapper')
end

--override
function AnimalUpgradeAttrItem:OnDestroy() 
    self._nameText = nil
    self._valueText = nil
end

function AnimalUpgradeAttrItem:OnRefreshData(data)
    self._nameText.text = AttriDataHelper:GetName(data.attri)
    if data.add then
        local add = StringStyleUtil.GetColorStringWithColorId("+" .. data.add, 102)
        self._valueText.text = data.value .. add
    else
        self._valueText.text = data.value
    end
end