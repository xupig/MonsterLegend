local AnimalStrengthFxItem = Class.AnimalStrengthFxItem(ClassTypes.XObject)
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local UIComponentUtil = GameUtil.UIComponentUtil
function AnimalStrengthFxItem:__ctor__()

end
function AnimalStrengthFxItem:InitView()
    self:StopFx()
end

function AnimalStrengthFxItem:SetPosition(pos)
    self._go.transform.localPosition=Vector2(pos.x,pos.y)
    --LoggerHelper.Error("AnimalStrengthFxItem:"..self._go.transform.localPosition.x..":"..self._go.transform.localPosition.y)
end 

function AnimalStrengthFxItem:SetComponent(go)
    self._go = go
    self._Fx = UIComponentUtil.AddLuaUIComponent(go, UIParticle)
    self._An = go:AddComponent(typeof(XGameObjectTweenPosition))
end 

function AnimalStrengthFxItem:Start()
    self:StartFx()
    self:StartMove()
end

function AnimalStrengthFxItem:StartMove()
    self._An.IsTweening = false
    self._An:SetToPosOnce(Vector3(self._arrPos.x,self._arrPos.y,0), 1.0, nil)
end

function AnimalStrengthFxItem:SetArrivePosition(arrPos)
    self._arrPos = arrPos

end


function AnimalStrengthFxItem:StartFx(pos)
    self._Fx:Play(true,true)  
end

function AnimalStrengthFxItem:StopFx()
    self._Fx:Stop()
end

function AnimalStrengthFxItem:DestoryFx()
    if self._go then
        GameWorld.Destroy(self._go)
    end
end