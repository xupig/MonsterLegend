-- AnimalEquipListItem.lua
local AnimalEquipListItem = Class.AnimalEquipListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
AnimalEquipListItem.interface = GameConfig.ComponentsConfig.Component
AnimalEquipListItem.classPath = "Modules.ModuleAnimal.ChildComponent.AnimalEquipListItem"

local MythicalAnimalsDataHelper = GameDataHelper.MythicalAnimalsDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AnimalManager = PlayerManager.AnimalManager
local public_config = GameWorld.public_config
local AnimalConfig = GameConfig.AnimalConfig
local StringStyleUtil = GameUtil.StringStyleUtil

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function AnimalEquipListItem:Awake()
    self._icon = ItemManager():GetLuaUIComponent(nil, self:FindChildGO("Container_Icon"))
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper') 
    self._selectImageGO = self:FindChildGO("Image_Select")   
    self._select = false
    self._itemTrans = self.gameObject.transform
end

function AnimalEquipListItem:OnDestroy() 

end

--override
function AnimalEquipListItem:OnRefreshData(data)
    self._itemData = data.itemData
    local typeNum = data.itemCfgData.quality
    self._icon:SetItemData(self._itemData, public_config.PKG_TYPE_GOD_MOSNTER, self._itemData:GetPos())
    self._nameText.text = StringStyleUtil.GetColorStringWithColorId(self._itemData:GetNameText() .. "+" .. self._itemData:GetMythicalAnimalEquipLevel(), AnimalConfig.QULITY_COLOR_MAP[typeNum])
end


function AnimalEquipListItem:SetSelect(flag)
    self._selectImageGO:SetActive(flag)
    self._select = flag
end

function AnimalEquipListItem:GetSelfTrans()
    return self._itemTrans
end

function AnimalEquipListItem:GetSelectStatus()
    return self._select
end