-- AnimalBagListItem.lua
local AnimalBagListItem = Class.AnimalBagListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
AnimalBagListItem.interface = GameConfig.ComponentsConfig.Component
AnimalBagListItem.classPath = "Modules.ModuleAnimal.ChildComponent.AnimalBagListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData

local public_config = GameWorld.public_config
local TipsManager = GameManager.TipsManager
local MythicalAnimalsDataHelper = GameDataHelper.MythicalAnimalsDataHelper
local AnimalConfig = GameConfig.AnimalConfig

function AnimalBagListItem:Awake()
    self._icon = ItemManager():GetLuaUIComponent(nil, self:FindChildGO("Container_content/Container_Icon"))
    if self:ExistChild("Container_content/Image_ArrowUp") then
        self._arrowUpImage = self:FindChildGO("Container_content/Image_ArrowUp")
    end
    if self:ExistChild("Container_content/Image_ArrowDown") then
        self._arrowDownImage = self:FindChildGO("Container_content/Image_ArrowDown")
    end
    -- self._icon:SetPointerUpCB(function() 
    --     self:ShowItemTips()
    -- end)
end

function AnimalBagListItem:OnDestroy() 

end

--override
function AnimalBagListItem:OnRefreshData(data)
    self._data = data
    if table.isEmpty(data) then
        self._icon:SetItem(nil)
        self:SetDirectionImage(false, false)
    else
        local itemData = data.itemData
        self._icon:SetItemData(itemData, public_config.PKG_TYPE_GOD_MOSNTER, itemData:GetPos())
        local animalId = itemData:GetAnimalId()
        local god_monster_info = GameWorld.Player().god_monster_info
        local typeNum = data.itemCfgData.type
        local quality = data.itemCfgData.quality
        local quality_types =  MythicalAnimalsDataHelper:GetQualityType(animalId)
        if typeNum ~= AnimalConfig.EQUIP_UPGRADE_QULITY_6 then
            if quality < quality_types[typeNum] then
                self:SetDirectionImage(false, false)
            else
                if god_monster_info[animalId] and god_monster_info[animalId][typeNum]  then
                    local nowPower = CommonItemData.CreateItemData(god_monster_info[animalId][typeNum], 0, typeNum):GetFightPower()
                    local equipPower = itemData:GetFightPower()
                    self:SetDirectionImage(equipPower > nowPower, equipPower < nowPower)
                else
                    self:SetDirectionImage(true, false)
                end
            end
        else
            self:SetDirectionImage(false, false)
        end
    end
end

function AnimalBagListItem:SetDirectionImage(up, down)
    if self._arrowUpImage then
        self._arrowUpImage:SetActive(up)
    end

    if self._arrowDownImage then
        self._arrowDownImage:SetActive(down)
    end
end

-- function AnimalBagListItem:ShowItemTips()
-- end