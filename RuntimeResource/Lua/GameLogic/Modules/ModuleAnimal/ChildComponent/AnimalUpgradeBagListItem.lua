-- AnimalUpgradeBagListItem.lua
local AnimalUpgradeBagListItem = Class.AnimalUpgradeBagListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
AnimalUpgradeBagListItem.interface = GameConfig.ComponentsConfig.Component
AnimalUpgradeBagListItem.classPath = "Modules.ModuleAnimal.ChildComponent.AnimalUpgradeBagListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData

local public_config = GameWorld.public_config
local TipsManager = GameManager.TipsManager
local MythicalAnimalsDataHelper = GameDataHelper.MythicalAnimalsDataHelper

function AnimalUpgradeBagListItem:Awake()
    self._icon = ItemManager():GetLuaUIComponent(nil, self:FindChildGO("Container_content/Container_Icon"))
    self._selectImageGO = self:FindChildGO("Container_content/Image_Selected")
    self._itemTrans = self.gameObject.transform
    self._select = true
end

function AnimalUpgradeBagListItem:OnDestroy() 

end

function AnimalUpgradeBagListItem:GetSelfTrans()
    return self._itemTrans
end

--override
function AnimalUpgradeBagListItem:OnRefreshData(data)
    self._data = data
    if table.isEmpty(data) then
        self._icon:SetItem(nil)
        -- self._selectImageGO:SetActive(false)
        -- self._select = false
    else
        -- self._selectImageGO:SetActive(true)
        -- self._select = true
        local itemData = data.itemData
        self._icon:SetItemData(itemData, public_config.PKG_TYPE_GOD_MOSNTER, itemData:GetPos())
    end
end

function AnimalUpgradeBagListItem:GetItemName()
    return self.gameObject.name
end

function AnimalUpgradeBagListItem:ToggleClick()    
    self._select = not self._select
    self._selectImageGO:SetActive(self._select)
end

function AnimalUpgradeBagListItem:SetSelect(flag)
    self._select = flag
    self._selectImageGO:SetActive(self._select)
end

function AnimalUpgradeBagListItem:GetSelectStatus()
    return self._select
end