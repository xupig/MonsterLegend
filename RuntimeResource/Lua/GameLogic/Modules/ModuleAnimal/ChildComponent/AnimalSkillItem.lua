-- AnimalSkillItem.lua
local AnimalSkillItem = Class.AnimalSkillItem(ClassTypes.UIListItem)

AnimalSkillItem.interface = GameConfig.ComponentsConfig.Component
AnimalSkillItem.classPath = "Modules.ModuleAnimal.ChildComponent.AnimalSkillItem"

local SkillDataHelper = GameDataHelper.SkillDataHelper
local AnimalManager = PlayerManager.AnimalManager

--override
function AnimalSkillItem:Awake()
    self._icon = self:FindChildGO("Container_Icon")
    -- self._levelText = self:GetChildComponent("Text_Level",'TextMeshWrapper')
end

--override
function AnimalSkillItem:OnDestroy() 
    self._icon = nil
    -- self._levelText = nil
end

function AnimalSkillItem:OnRefreshData(data)
    local skill = data[1]
    local level = data[2]
    local id = data[3]
    local icon = SkillDataHelper.GetSkillIcon(skill)
    if AnimalManager:IsHelp(id) then
        GameWorld.AddIcon(self._icon, icon, 0 , true, 1)
    else
        GameWorld.AddIcon(self._icon, icon, 0 , true, 0)
    end
    
    
    -- self._levelText.text = LanguageDataHelper.CreateContent(76484, {["0"] = level})
end