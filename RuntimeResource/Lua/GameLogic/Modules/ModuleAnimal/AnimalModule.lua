-- AnimalModule.lua
AnimalModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function AnimalModule.Init()
    GUIManager.AddPanel(PanelsConfig.Animal, "Panel_Animal", GUILayer.LayerUIPanel, "Modules.ModuleAnimal.AnimalPanel", true, false,nil,false)
end

return AnimalModule
