--AnimalPanel.lua
local AnimalPanel = Class.AnimalPanel(ClassTypes.BasePanel)

require "Modules.ModuleAnimal.ChildView.AnimalMainView"
local AnimalMainView = ClassTypes.AnimalMainView

require "Modules.ModuleAnimal.ChildView.AnimalUpgradeView"
local AnimalUpgradeView = ClassTypes.AnimalUpgradeView

require "Modules.ModuleAnimal.ChildView.AnimalBagView"
local AnimalBagView = ClassTypes.AnimalBagView

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType

function AnimalPanel:Awake()
	self:InitCallback()
	self:InitViews()
end

function AnimalPanel:InitCallback()
    self._enterAnimalBag =  function(animalId, typeNum) self:EnterAnimalBag(animalId, typeNum) end
    self._upgradeEquip =  function() self:UpgradeEquip() end
    self._closeUpgradeWin = function() self:CloseUpgradeWin() end
end

function AnimalPanel:InitViews()
    self._animalMainView = self:AddChildLuaUIComponent("Container_Main", AnimalMainView)
    self._animalUpgradeView = self:AddChildLuaUIComponent("Container_Upgrade", AnimalUpgradeView)
    self._animalBagView = self:AddChildLuaUIComponent("Container_Bag", AnimalBagView)
end

function AnimalPanel:OnShow(data)
    self:AddEventListeners()
    -- self:RefreshViews()
end

function AnimalPanel:SetData(data)
end

function AnimalPanel:OnClose()
    self:RemoveEventListeners()
    self._animalMainView:SetActive(true)
    self._animalUpgradeView:SetActive(false)
    self._animalBagView:SetActive(false)
end

function AnimalPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ON_ADD_ANIMAL_EQUIP_CLICK, self._enterAnimalBag)
    EventDispatcher:AddEventListener(GameEvents.ON_UPGRADE_EQUIP_CLICK, self._upgradeEquip)
end

function AnimalPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_ADD_ANIMAL_EQUIP_CLICK, self._enterAnimalBag)
    EventDispatcher:RemoveEventListener(GameEvents.ON_UPGRADE_EQUIP_CLICK, self._upgradeEquip)
end

function AnimalPanel:EnterAnimalBag(animalId, typeNum)
    self._animalBagView:SetActive(true)
    self._animalBagView:RefreshViews(animalId, typeNum)
end

function AnimalPanel:UpgradeEquip()    
    self:SetCloseCallBack(self._closeUpgradeWin)
    self._animalMainView:SetActive(false)
    self._animalUpgradeView:SetActive(true)
    -- self._animalUpgradeView:RefreshViews()
end

function AnimalPanel:CloseUpgradeWin()
    self:SetCloseCallBack(nil)
    self._animalMainView:SetActive(true)
    self._animalUpgradeView:SetActive(false)
end

function AnimalPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--关闭的时候重新开启二级主菜单
function AnimalPanel:ReopenSubMainPanel()
    return true
end

