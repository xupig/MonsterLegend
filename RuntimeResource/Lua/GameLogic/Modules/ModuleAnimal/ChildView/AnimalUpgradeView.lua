--AnimalUpgradeView.lua
local AnimalUpgradeView = Class.AnimalUpgradeView(ClassTypes.BaseLuaUIComponent)

AnimalUpgradeView.interface = GameConfig.ComponentsConfig.Component
AnimalUpgradeView.classPath = "Modules.ModuleAnimal.ChildView.AnimalUpgradeView"

local UIList = ClassTypes.UIList
local AnimalConfig = GameConfig.AnimalConfig
local AnimalManager = PlayerManager.AnimalManager
local public_config = GameWorld.public_config
local MythicalAnimalsLevelDataHelper = GameDataHelper.MythicalAnimalsLevelDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local UIToggle = ClassTypes.UIToggle
local UIProgressBar = ClassTypes.UIProgressBar
local StringStyleUtil = GameUtil.StringStyleUtil
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local LuaUIParticle = GameMain.LuaUIParticle

require "Modules.ModuleAnimal.ChildComponent.AnimalUpgradeBagListItem"
local AnimalUpgradeBagListItem = ClassTypes.AnimalUpgradeBagListItem

require "Modules.ModuleAnimal.ChildComponent.AnimalEquipListItem"
local AnimalEquipListItem = ClassTypes.AnimalEquipListItem

require "Modules.ModuleAnimal.ChildComponent.AnimalUpgradeAttrItem"
local AnimalUpgradeAttrItem = ClassTypes.AnimalUpgradeAttrItem

require "Modules.ModuleAnimal.ChildComponent.AnimalStrengthFxItem"
local AnimalStrengthFxItem = ClassTypes.AnimalStrengthFxItem

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function AnimalUpgradeView:Awake()
    self:InitView()
    self:InitCallBack()
end

--override
function AnimalUpgradeView:OnDestroy()
end

function AnimalUpgradeView:InitCallBack()
    self._animalEquipStren = function() self:AnimalEquipStren() end
end

function AnimalUpgradeView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ANIMAL_EQUIP_stren, self._animalEquipStren)
end

function AnimalUpgradeView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ANIMAL_EQUIP_stren, self._animalEquipStren)
end

function AnimalUpgradeView:InitView()
    --特效
    self._fxItemPool = {}
    self._fxItems = {}
    self._offSetX = 804--X偏移
    self._offSetY = 479--Y偏移
    self._uplimitY = 486--Y上限
    self._downlimitY = 115--Y下限
    -- self._leftRight = 600 --左右分界
    self._offArriveX = 156--X偏移
    self._offArriveY = 481--Y偏移
    self._upArrivelimitY = 510--Y上限
    self._downArrivelimitY = 72--Y下限
    self._tweenOk = function() self:TweenOK()end


    self._qualityLevel = AnimalConfig.EQUIP_UPGRADE_QULITY_4

    self._qualitySelectContainer = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select")
    self._nowSelectQualityText = self:GetChildComponent("Container_Right/Container_Qulity_Select/Text_Name",'TextMeshWrapper')
    self._nowSelectQualityText.text = AnimalConfig.EQUIP_UPGRADE_QULITY__NAME_MAP[self._qualityLevel]

    --神兽强化装备信息
    self._icon = ItemManager():GetLuaUIComponent(nil, self:FindChildGO("Container_Middle/Container_Icon"))
    self._upgradeContainer = self:FindChildGO("Container_Middle/Container_Upgrade")
    self._srcLevelText = self:GetChildComponent("Container_Middle/Container_Upgrade/Text_Src",'TextMeshWrapper')
    self._dstLevelText = self:GetChildComponent("Container_Middle/Container_Upgrade/Text_Dst",'TextMeshWrapper')

    
    self._priceContainer = self:FindChildGO("Container_Buttom/Container_Price")
    self._priceText = self:GetChildComponent("Container_Buttom/Container_Price/Text_Price",'TextMeshWrapper')
    self._priceIconContainer = self:FindChildGO("Container_Buttom/Container_Price/Container_Icon")
    GameWorld.AddIcon(self._priceIconContainer, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    
    self._allSelectToggle = UIToggle.AddToggle(self.gameObject, "Container_Right/Toggle_Select_All")
    self._allSelectToggle:SetOnValueChangedCB(function(state) self:OnAllToggleValueChanged(state) end)
    self._allSelectToggle:SetIsOn(true)
    self._doubleToggle = UIToggle.AddToggle(self.gameObject, "Container_Buttom/Toggle_Double")
    self._doubleToggle:SetOnValueChangedCB(function(state) self:OnDoubleToggleValueChanged(state) end)
    self._doubleToggle:SetIsOn(false)

    self._priceContainer:SetActive(false)

    self._expComp = UIProgressBar.AddProgressBar(self.gameObject,"Container_Middle/Container_Exp/ProgressBar_Exp")
    self._expComp:SetProgress(0)
    self._expComp:ShowTweenAnimate(true)
    self._expComp:SetMutipleTween(true)
    self._expComp:SetIsResetToZero(true)
    self._expComp:SetTweenTime(0.2)
    self._expText = self:GetChildComponent("Container_Middle/Container_Exp/ProgressBar_Exp/Text_Attach",'TextMeshWrapper')  

    --强化按钮点击
    self._upgradeButton = self:FindChildGO("Container_Buttom/Button_Upgrade")
    self._csBH:AddClick(self._upgradeButton, function() self:OnUpgradeButtonClick() end)

    --品质筛选按钮点击
    self._qualitySelectButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Image_Bg")
    self._csBH:AddClick(self._qualitySelectButton, function() self:OnQualitySelectButtonClick() end)

    --空白处点击事件处理
    self._blankButton = self:FindChildGO("Button_Blank")
    self._csBH:AddClick(self._blankButton, function() self:OnBlankButtonClick() end)
    self._blankButton:SetActive(false)


    self:InitBagList()
    self:InitEquipList()
    self:InitQulalitySelectButtonEvent()
    self:InitAttrGridLayoutGroup()
end

function AnimalUpgradeView:InitAttrGridLayoutGroup()
    self._attrGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Middle/Container_Next/Container_Attr")
    self._attrGridLauoutGroup:SetItemType(AnimalUpgradeAttrItem)
end

function AnimalUpgradeView:InitQulalitySelectButtonEvent()
    --全部品质
    self._qualitySelectAllButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_All")
    self._csBH:AddClick(self._qualitySelectAllButton, function() self:OnSelectQuality(AnimalConfig.EQUIP_UPGRADE_QULITY_ALL) end)

    --兽血灵晶
    self._qualitySelectWhiteButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_6")
    self._csBH:AddClick(self._qualitySelectWhiteButton, function() self:OnSelectQuality(AnimalConfig.EQUIP_UPGRADE_QULITY_6) end)

    --蓝色及以下
    self._qualitySelectBlueButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_Blue")
    self._csBH:AddClick(self._qualitySelectBlueButton, function() self:OnSelectQuality(AnimalConfig.EQUIP_UPGRADE_QULITY_3) end)

    --紫色及以下
    self._qualitySelectPurpleButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_Purple")
    self._csBH:AddClick(self._qualitySelectPurpleButton, function() self:OnSelectQuality(AnimalConfig.EQUIP_UPGRADE_QULITY_4) end)

    --橙色及以下
    self._qualitySelectOrangeButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_Orange")
    self._csBH:AddClick(self._qualitySelectOrangeButton, function() self:OnSelectQuality(AnimalConfig.EQUIP_UPGRADE_QULITY_5) end)
end

function AnimalUpgradeView:InitEquipList()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Left/ScrollViewList")
	self._equipList = list
	self._equipListView = scrollView 
    self._equipListView:SetHorizontalMove(false)
    self._equipListView:SetVerticalMove(true)
    self._equipList:SetItemType(AnimalEquipListItem)
    self._equipList:SetPadding(0, 0, 0, 0)
    self._equipList:SetGap(0, 0)
	self._equipList:SetDirection(UIList.DirectionTopToDown, 1, -1)

	local itemClickedCB = 
	function(idx)
		self:OnEquipListItemClicked(idx)
	end
	self._equipList:SetItemClickedCB(itemClickedCB)
end

function AnimalUpgradeView:OnEquipListItemClicked(idx)
    if self._selectIndex then
        self._equipList:GetItem(self._selectIndex):SetSelect(false)
    end
    self._selectIndex = idx
    local item = self._equipList:GetItem(self._selectIndex)
    item:SetSelect(true)    
    local itemData = self._equipListData[self._selectIndex + 1].itemData
    self._animalId = self._equipListData[self._selectIndex + 1].animalId
    local level = itemData:GetMythicalAnimalEquipLevel()
    self._pos = self._equipListData[idx + 1].pos
    local maxLevel = MythicalAnimalsLevelDataHelper:GetMaxLevel(self._pos)
    local nexAttrValue = {}
    -- if level == maxLevel then
    --     self._srcLevelText.text = "+" .. level
    --     self._dstLevelText.text = ""
    -- else
    --     self._srcLevelText.text = "+" .. level
    --     local dstLevel = self:GetUpgradeLevel()
    --     self._dstLevelText.text = "+" .. dstLevel
    --     nexAttrValue = self:GetUpgradeAttr(level, dstLevel, pos)
    -- end
    self._icon:SetItemData(itemData, public_config.PKG_TYPE_GOD_MOSNTER, self._pos)
    self:UpdateExpInfo(itemData)
end

function AnimalUpgradeView:InitBagList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Right/ScrollViewList")
	self._bagList = list
	self._bagListlView = scrollView 
    self._bagListlView:SetHorizontalMove(false)
    self._bagListlView:SetVerticalMove(true)
    self._bagList:SetItemType(AnimalUpgradeBagListItem)
    self._bagList:SetPadding(10, 0, 0, 26)
    self._bagList:SetGap(16, 0)
    self._bagList:SetDirection(UIList.DirectionLeftToRight, 3, 100)

    self._bagList:SetItemClickedCB(function(idx) self:OnItemClicked(idx) end)
    self.listcontentTran = self._bagList.gameObject.transform 
end

function AnimalUpgradeView:OnItemClicked(idx)
    if self._bagListData and not table.isEmpty(self._bagListData[idx + 1]) then
        self._bagList:GetItem(idx):ToggleClick()
    end
    if self._equipListData and (not table.isEmpty(self._equipListData)) then
        self:UpdateExpInfo()
    end
    self:SetUpgradeButtonState()
end

function AnimalUpgradeView:OnEnable()
    self:AddEventListeners()
    self._selectIndex = 0
    self:UpdateEquipListData()--初始化神兽装备数据
    if self._equipListData and (not table.isEmpty(self._equipListData)) then
        self._equipList:SetPositionByNum(self._selectIndex + 1)
        self._equipList:GetItem(self._selectIndex):SetSelect(true)
    end
    self:RefreshViews()
    self:SetBagListSelect(true)
end

function AnimalUpgradeView:OnDisable()    
    if self._selectIndex ~= nil and #self._equipListData > 0 then
        self._equipList:GetItem(self._selectIndex):SetSelect(false)
        self._selectIndex = nil
    end

    self:RemoveEventListeners()
    self._qualityLevel = AnimalConfig.EQUIP_UPGRADE_QULITY_4
    self._qualitySelectContainer:SetActive(false)
    self._blankButton:SetActive(false)
    self._nowSelectQualityText.text = AnimalConfig.EQUIP_UPGRADE_QULITY__NAME_MAP[self._qualityLevel]
    self._allSelectToggle:SetIsOn(true)
    self._doubleToggle:SetIsOn(false)
    self._priceContainer:SetActive(false)
end

function AnimalUpgradeView:RefreshViews()
    self:UpdateBagListData()--刷新神兽装备背包数据
    self:UpdataEquipInfo()
    self:SetUpgradeButtonState()
end

--刷新熟练度信息
function AnimalUpgradeView:UpdateExpInfo(itemData)
    if self._selectIndex == nil then return end
    itemData = itemData or self._equipListData[self._selectIndex + 1].itemData
    local nowExp = itemData:GetExp()
    local nowLevel = itemData:GetMythicalAnimalEquipLevel()
    local typeNum = itemData:GetCfgItemData().type
    local maxLevel = MythicalAnimalsLevelDataHelper:GetMaxLevel(typeNum)
    local levelUpExp = MythicalAnimalsLevelDataHelper:GetLevelUpCostByTypeAndLevel(typeNum, nowLevel)
    local nexAttrValue = {}
    if levelUpExp == -1 then
        self._expText = LanguageDataHelper.CreateContent(76498)
    elseif levelUpExp == 0 then
        --查不到消耗值
        self._expText = ""
        LoggerHelper.Error("AnimalUpgradeView:UpdateExpInfo can not find cost typeNum ===== " .. tostring(typeNum) .. "nowLevel ===" .. tostring(nowLevel))
    else
        local upgradeExp = self:GetUpgradeExp()
        self._expText.text = nowExp .. StringStyleUtil.GetColorStringWithColorId("+" .. upgradeExp ..  "/" .. levelUpExp, 102)
    end

    if nowLevel == maxLevel then
        self._srcLevelText.text = "+" .. nowLevel
        self._dstLevelText.text = ""
    else
        self._srcLevelText.text = "+" .. nowLevel
        local dstLevel = self:GetUpgradeLevel()
        self._dstLevelText.text = "+" .. dstLevel
        nexAttrValue = self:GetUpgradeAttr(nowLevel, dstLevel, typeNum)
    end
    
    local baseAttrList = MythicalAnimalsLevelDataHelper:GetAttriByTypeAndLevel(typeNum, nowLevel)
    if table.isEmpty(baseAttrList) then return end

    local datas = {}
    for k,v in pairs(baseAttrList) do
        local data = {}
        data.attri = k
        data.value = v
        data.add = nexAttrValue[k]
        table.insert(datas, data)
    end
    self._attrGridLauoutGroupData = datas
    self._attrGridLauoutGroup:SetDataList(self._attrGridLauoutGroupData)

    local state = self._doubleToggle:GetToggleWrapper().isOn
    self._priceContainer:SetActive(state)
    if state then
        local exp = self:GetDoubleUpgradeExp() 
        local parms = GlobalParamsHelper.GetParamValue(729)
        if exp  % parms == 0 then
            self._priceText.text = math.floor(exp/parms)
        else
            self._priceText.text = math.floor(exp/parms) + 1
        end        
    end
end

--刷新装备强化相关信息
function AnimalUpgradeView:UpdataEquipInfo()
    if self._equipListData and (not table.isEmpty(self._equipListData)) then
        self._upgradeContainer:SetActive(true)
        self:OnEquipListItemClicked(self._selectIndex)
    else
        self._icon:SetItem(nil)
        self._expText.text = ""
        self._attrGridLauoutGroupData = {}
        self._attrGridLauoutGroup:SetDataList(self._attrGridLauoutGroupData)
        self._upgradeContainer:SetActive(false)
        self:SetUpgradeButtonState()
    end
end

--初始化神兽装备数据
function AnimalUpgradeView:UpdateEquipListData()
    self._equipListData =  AnimalManager:GetAnimalEquipListData()
    self._equipList:SetDataList(self._equipListData)
end

--刷新神兽装备背包数据
function AnimalUpgradeView:UpdateBagListData()
    self._bagListData =  AnimalManager:GetBadDataByQualityAndStart(nil, self._qualityLevel, nil, AnimalConfig.QUALITY_LESS_OR_EQUAL)
    self._bagList:SetDataList(self._bagListData)
    self._bagList:SetToZero()
end

--空白处点击处理事件
function AnimalUpgradeView:OnBlankButtonClick()
    self._qualitySelectContainer:SetActive(false)
    self._blankButton:SetActive(false)
end

--品质筛选按钮点击
function AnimalUpgradeView:OnQualitySelectButtonClick()
    self._qualitySelectContainer:SetActive(true)
    self._blankButton:SetActive(true)
end

--选择品质处理事件
function AnimalUpgradeView:OnSelectQuality(quality)
    local type
    if quality == AnimalConfig.EQUIP_UPGRADE_QULITY_3 or quality == AnimalConfig.EQUIP_UPGRADE_QULITY_4 or quality == AnimalConfig.EQUIP_UPGRADE_QULITY_5 then
        type = AnimalConfig.QUALITY_LESS_OR_EQUAL
    elseif quality == AnimalConfig.EQUIP_UPGRADE_QULITY_6 or quality == AnimalConfig.EQUIP_UPGRADE_QULITY_ALL then
        type = AnimalConfig.QUALITY_EQUAL
    end
    self._qualityLevel = quality
    if quality == AnimalConfig.EQUIP_UPGRADE_QULITY_6 then
        self._bagListData = AnimalManager:GetDataByType(AnimalConfig.EQUIP_UPGRADE_QULITY_6)
    else
        self._bagListData = AnimalManager:GetBadDataByQualityAndStart(nil, self._qualityLevel, nil, type)
    end
    self._bagList:SetDataList(self._bagListData)
    self._qualitySelectContainer:SetActive(false)
    self._blankButton:SetActive(false)
    self._nowSelectQualityText.text = AnimalConfig.EQUIP_UPGRADE_QULITY__NAME_MAP[quality]
    self:SetBagListSelect(false)
    self._allSelectToggle:SetIsOn(false)
    self._bagList:SetToZero()
    -- for i,v in ipairs(self._bagListData) do
    --     self._bagList:GetItem(i -1):SetSelect(false)
    -- end
    self:UpdateExpInfo()
    self:SetUpgradeButtonState()
end

--强化按钮点击处理事件
function AnimalUpgradeView:OnUpgradeButtonClick()
    -- LoggerHelper.Error("AnimalUpgradeView:OnUpgradeButtonClick() _doubleToggle == "  .. tostring(self._doubleToggle:GetToggleWrapper().isOn))
    -- LoggerHelper.Error("AnimalUpgradeView:OnUpgradeButtonClick() _allSelectToggle"  .. tostring(self._allSelectToggle:GetToggleWrapper().isOn))
    self:SetFxRune()

    local posList = self:GetUpgradeBagPosList()
    local double = 0--1是双倍
    if self._doubleToggle:GetToggleWrapper().isOn then 
        double = 1
    end
    self._timerid = TimerHeap:AddSecTimer(1.2,0,0,self._tweenOk)
    AnimalManager:UpgradeEquip(self._animalId, self._pos, double, self:GetUpgradeBagPosList())
end

function AnimalUpgradeView:OnAllToggleValueChanged(state)
    -- LoggerHelper.Error("AnimalUpgradeView:OnAllToggleValueChanged(state)" .. tostring(state))
    self:SetBagListSelect(state)
    if self._equipListData and (not table.isEmpty(self._equipListData)) then
        self:UpdateExpInfo()
    end
    self:SetUpgradeButtonState(state)
end

function AnimalUpgradeView:OnDoubleToggleValueChanged(state)
    if self._equipListData and (not table.isEmpty(self._equipListData)) then
        self:UpdateExpInfo()
    end
end

function AnimalUpgradeView:SetBagListSelect(flag)
    for i,v in ipairs(self._bagListData) do
        if table.isEmpty(v) then
            self._bagList:GetItem(i -1):SetSelect(false)
        else
            self._bagList:GetItem(i -1):SetSelect(flag)
        end
    end
end

--获取选取材料的背包位置列表
function AnimalUpgradeView:GetUpgradeBagPosList()
    local posList = {}
    for i,v in ipairs(self._bagListData) do
        if table.isEmpty(self._bagListData[i]) then
            break
        else
            local item = self._bagList:GetItem(i-1)
            if item:GetSelectStatus() then
                table.insert(posList, self._bagListData[i].itemData:GetPos())
            end
        end
    end
    return posList
end

--获取选取材料中属于双倍熟练度的总值(用于计算需要消耗的钻石数量)
function AnimalUpgradeView:GetDoubleUpgradeExp()
    local exp = 0
    for i,v in ipairs(self._bagListData) do
        if table.isEmpty(self._bagListData[i]) then
            break
        else
            local itemData = self._bagListData[i].itemData
            if self._bagList:GetItem(i - 1):GetSelectStatus() and  (not itemData:IsHasUpgrade()) then
                exp = exp + itemData:GetStrengthCostExp()
            end
        end
    end
    return exp
end

--获取选取材料的强化价值熟练度
function AnimalUpgradeView:GetUpgradeExp()
    local exp = 0
    for i,v in ipairs(self._bagListData) do
        if table.isEmpty(self._bagListData[i]) then
            break
        else
            local itemData = self._bagListData[i].itemData
            if self._bagList:GetItem(i - 1):GetSelectStatus() then
                exp = exp + itemData:GetStrengthCostExp(self._doubleToggle:GetToggleWrapper().isOn)
            end
        end
    end
    return exp
end

--获取选取材料强化能提升的等级
function AnimalUpgradeView:GetUpgradeLevel()
    local itemData = self._equipListData[self._selectIndex + 1].itemData
    local pos = self._equipListData[self._selectIndex + 1].pos
    local maxLevel = MythicalAnimalsLevelDataHelper:GetMaxLevel(pos)

    local nowExp = itemData:GetExp()
    local nowLevel = itemData:GetMythicalAnimalEquipLevel()
    local typeNum = itemData:GetCfgItemData().type
    local levelUpExp = MythicalAnimalsLevelDataHelper:GetLevelUpCostByTypeAndLevel(typeNum, nowLevel)
    local getExp = self:GetUpgradeExp()
    local totaleExp  = nowExp + getExp
    
    if totaleExp < levelUpExp then
        return nowLevel + 1
    else
        while(totaleExp >= levelUpExp) do
            if nowLevel >= maxLevel then
                return maxLevel
            end 
            nowLevel = nowLevel + 1
            totaleExp = totaleExp - levelUpExp
            levelUpExp = MythicalAnimalsLevelDataHelper:GetLevelUpCostByTypeAndLevel(typeNum, nowLevel)
        end
    end
    return nowLevel
end

--获取选取材料强化后等级的属性加成
function AnimalUpgradeView:GetUpgradeAttr(srcLevel, dstLevel, pos)
    if srcLevel == dstLevel then
        return MythicalAnimalsLevelDataHelper:GetAttriByTypeAndLevel(pos, dstLevel)
    else
        local baseAttrList = MythicalAnimalsLevelDataHelper:GetAttriByTypeAndLevel(pos, srcLevel)
        local datas = {}
        for i = srcLevel + 1,dstLevel do
            local attr = MythicalAnimalsLevelDataHelper:GetAttriByTypeAndLevel(pos, i)
            for k,v in pairs(baseAttrList) do
                datas[k] = v + attr[k]
            end
        end
        return datas
    end
end

function AnimalUpgradeView:SetUpgradeButtonState(state)
    if table.isEmpty(self._equipListData) then
        self._upgradeButton:GetComponent("ButtonWrapper"):SetInteractable(false)
    else
        if state then
            self._upgradeButton:GetComponent("ButtonWrapper"):SetInteractable(state)
        else
            self._upgradeButton:GetComponent("ButtonWrapper"):SetInteractable(not table.isEmpty(self:GetUpgradeBagPosList()))            
        end
    end
end

--装备强化成功
function AnimalUpgradeView:AnimalEquipStren()
    self:UpdateEquipListData()--初始化神兽装备数据
    -- if self._equipListData and (not table.isEmpty(self._equipListData)) then
    --     self._equipList:SetPositionByNum(self._selectIndex + 1)
    --     self._equipList:GetItem(self._selectIndex):SetSelect(true)
    -- end
    self:RefreshViews()
    self:SetBagListSelect(false)
end

function AnimalUpgradeView:Close()
    --self:Clear()
    
    -- if self._timerid~=-1 then
    --     TimerHeap:DelTimer(self._timerid)
    --     self._timerid=-1
    -- end

    -- for k,v in pairs(self._fxItems) do
    --     v:StopFx()
    --     self:ReleaseFxItem(v)
    --     self._fxItems[k]=nil
    -- end
    -- self._bagList:RemoveAll()

end

function AnimalUpgradeView:ReleaseFxItem(item)
    local itemPool = self._fxItemPool
    table.insert(itemPool, item)
end

-- 特效对象池
function AnimalUpgradeView:CreateFxItem()
    local itemPool = self._fxItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_FxCopy","Container_FxCopyPool")
    
    local item = AnimalStrengthFxItem()
    item:SetComponent(go)
    return item
end

function AnimalUpgradeView:AddFx(name,position,arrivePosition)
    local item = self:CreateFxItem()
    self._fxItems[name] = item
    item:InitView()
    item:SetPosition(position)
    item:SetArrivePosition(arrivePosition)
    --LoggerHelper.Error("AnimalUpgradeView:CreateFxItem()"..arrivePosition.x.."SS"..arrivePosition.y)
    return item
end

function AnimalUpgradeView:TweenOK()
    TimerHeap:DelTimer(self._timerid)
    self._timerid=-1
    for k,v in pairs(self._fxItems) do
        v:StopFx()
        self:ReleaseFxItem(v)
        self._fxItems[k]=nil
    end
end

--特效
function AnimalUpgradeView:SetFxRune()
    local arrivePos=Vector2(0,0)
    local equipLength = self._equipList:GetLength()-1
    for i=0,equipLength do
        local item = self._equipList:GetItem(i)
        local stage = item:GetSelectStatus()
        if stage then
            local itemPosX=578
            local itemPosY=484
            arrivePos = Vector2(itemPosX,itemPosY)
        end
    end

    local l = self._bagList:GetLength()-1
    for i=0,l do
        local item = self._bagList:GetItem(i)
        local stage = item:GetSelectStatus()
        if stage==true then
            local pos=Vector2(0,0)
            local name = item:GetItemName()
            local itemPos = item:GetSelfTrans().localPosition
            local itemPosX=itemPos.x+self._offSetX
            --local maskPosY=self.listcontentTran.localPosition.y-440
            local itemPosY=itemPos.y+self._offSetY--+maskPosY
            local k = 0
            local item
            if itemPosY>=self._downlimitY and itemPosY<=self._uplimitY then--在可视范围内
                pos = Vector2(itemPosX,itemPosY)
                item = self:AddFx(name,pos,arrivePos)
            else
            end

            if item then
                item:Start()
            end
        end
    end
end