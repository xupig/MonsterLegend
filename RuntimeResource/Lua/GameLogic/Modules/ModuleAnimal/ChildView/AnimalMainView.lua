local AnimalMainView = Class.AnimalMainView(ClassTypes.BaseLuaUIComponent)

AnimalMainView.interface = GameConfig.ComponentsConfig.Component
AnimalMainView.classPath = "Modules.ModuleAnimal.ChildView.AnimalMainView"

local UIList = ClassTypes.UIList
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local MythicalAnimalsDataHelper = GameDataHelper.MythicalAnimalsDataHelper
local MythicalAnimalsLevelDataHelper = GameDataHelper.MythicalAnimalsLevelDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local EquipDataHelper = GameDataHelper.EquipDataHelper
local public_config = GameWorld.public_config
local AnimalManager = PlayerManager.AnimalManager
local ActorModelComponent = GameMain.ActorModelComponent
local TipsManager = GameManager.TipsManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local RectTransform = UnityEngine.RectTransform
local UIParticle = ClassTypes.UIParticle

require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

require "Modules.ModuleAnimal.ChildComponent.AnimalListItem"
local AnimalListItem = ClassTypes.AnimalListItem

require "Modules.ModuleAnimal.ChildComponent.AnimalHelpAttrItem"
local AnimalHelpAttrItem = ClassTypes.AnimalHelpAttrItem

require "Modules.ModuleAnimal.ChildComponent.AnimalSkillItem"
local AnimalSkillItem = ClassTypes.AnimalSkillItem

require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

function AnimalMainView:Awake()
    self:InitView()
    self:InitCallBack()
end

--override
function AnimalMainView:OnDestroy() 
end

function AnimalMainView:InitCallBack()
    self._animalInfoRefresh = function() self:RefreshViews() end
end

function AnimalMainView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ANIMAL_INFO_REFRESH, self._animalInfoRefresh)
end

function AnimalMainView:RemoveEventListeners()    
    EventDispatcher:RemoveEventListener(GameEvents.ANIMAL_INFO_REFRESH, self._animalInfoRefresh)
end

function AnimalMainView:InitView()
    self._iconViews = {}
    self._plusViews = {}
    for i=1,5 do
        table.insert(self._iconViews, ItemManager():GetLuaUIComponent(nil, self:FindChildGO("Container_Middle/Container_Icons/Container_Icon" .. i)))
        -- self._iconViews[i]:ActivateTipsByItemData()
        self._iconViews[i]:SetPointerUpCB(function() 
            self:ShowItemTips(i)
        end)
        table.insert(self._plusViews, self:FindChildGO("Container_Middle/Container_Icons/Image_Plus_" .. i))
        self._csBH:AddClick(self._plusViews[i], function() self:OnAddEquipButtonClick(i) end)
    end
    
    self._helpNumText = self:GetChildComponent("Container_Buttom/Text_Num",'TextMeshWrapper') 

    -- self._modelRect = self:GetChildComponent("Container_Middle/Container_Model", "RectTransform")
    -- self._modelLocalPosition = self._modelRect.transform.localPosition
    -- self._modelComponent = self:AddChildComponent('Container_Middle/Container_Model', ActorModelComponent)
    -- self._modelComponent:SetStartSetting(Vector3(0, 0, -150), Vector3(0, 180, 0), 1)
    self._animalContainer = self:FindChildGO("Container_Middle/Container_Animal")

    self._upgradeRedImageGO = self:FindChildGO("Container_Buttom/Container_Buttons/Button_Upgrade/Image_Red")
    self._equipBagRedImageGO = self:FindChildGO("Container_Buttom/Container_Buttons/Button_Equip_Bag/Image_Red")
    self._helpRedImageGO = self:FindChildGO("Container_Buttom/Container_Buttons/Button_Help/Image_Red")

    -- 神兽提示框
    self._addAnimalTipsContainer = self:FindChildGO("Container_Add_Tips")
    self._costAddAnimalIcon = self:AddChildLuaUIComponent("Container_Add_Tips/Container_PopupMiddle/Container_Content/Container_ItemGridEx", ItemGridEx)
    self._costAddAnimalIcon:SetShowName(false)
    self._costAddAnimalIcon:ActivateTips()
    self._addAnimalTipsContainer:SetActive(false)

    --一键卸下按钮
    self._upLoadButton = self:FindChildGO("Container_Buttom/Container_Buttons/Button_Unload")
    self._csBH:AddClick(self._upLoadButton, function() self:OnUpLoadButtonClick() end)

    --神兽强化按钮
    self._upgradeButton = self:FindChildGO("Container_Buttom/Container_Buttons/Button_Upgrade")
    self._csBH:AddClick(self._upgradeButton, function() self:OnUpgradeButtonClick() end)

    --装备背包按钮
    self._equipBagButton = self:FindChildGO("Container_Buttom/Container_Buttons/Button_Equip_Bag")
    self._csBH:AddClick(self._equipBagButton, function() self:OnEquipBagButtonClick() end)

    --助战按钮
    self._helpButton = self:FindChildGO("Container_Buttom/Container_Buttons/Button_Help")
    self._csBH:AddClick(self._helpButton, function() self:OnHelpButtonClick() end)

    --找回按钮
    self._callBackButton = self:FindChildGO("Container_Buttom/Container_Buttons/Button_Call_Back")
    self._csBH:AddClick(self._callBackButton, function() self:OnCallBackButtonClick() end)

    --增加神兽按钮
    self._addAnimalButton = self:FindChildGO("Container_Buttom/Button_Plus")
    self._csBH:AddClick(self._addAnimalButton, function() self:OnAddAnimalButtonClick() end)

    --确定增加神兽按钮
    self._confirmAddAnimalButton = self:FindChildGO("Container_Add_Tips/Container_PopupMiddle/Button_Confirm")
    self._csBH:AddClick(self._confirmAddAnimalButton, function() self:OnConfirmAddAnimalButton() end)

    --关闭增加神兽提示框
    self._closeAddAnimalWinButton = self:FindChildGO("Container_Add_Tips/Container_PopupMiddle/Button_Close")
    self._csBH:AddClick(self._closeAddAnimalWinButton, function() self:OnCloseAddAnimalWinButton() end)

    self._fxCompose = UIParticle.AddParticle(self.gameObject,"Container_Help/fx_ui_30031")
    self._fxCompose:Stop()

    self:InitAnimalList()
    self:InitAttrGridLayoutGroup()
    self:InitSkillGridLayoutGroup()
end

function AnimalMainView:InitSkillGridLayoutGroup()
    self._skillGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Right/Container_Skill/Container_Skill")
    self._skillGridLauoutGroup:SetItemType(AnimalSkillItem)
    
	local itemClickedCB = 
	function(idx)
		self:OnAnimalSkillListItemClicked(idx)
	end
	self._skillGridLauoutGroup:SetItemClickedCB(itemClickedCB)
end

function AnimalMainView:OnAnimalSkillListItemClicked(idx)
    TipsManager:ShowSkillTips(self._skillGridLauoutGroupData[idx + 1][1], nil, 8)
end

function AnimalMainView:InitAttrGridLayoutGroup()
    self._attrGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Right/Container_Add/Container_Attr")
    self._attrGridLauoutGroup:SetItemType(AnimalHelpAttrItem)
end

function AnimalMainView:InitAnimalList()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Left/ScrollViewList")
	self._animalList = list
	self._animalListView = scrollView 
    self._animalListView:SetHorizontalMove(false)
    self._animalListView:SetVerticalMove(true)
    self._animalList:SetItemType(AnimalListItem)
    self._animalList:SetPadding(0, 0, 0, 0)
    self._animalList:SetGap(0, 0)
	self._animalList:SetDirection(UIList.DirectionTopToDown, 1, -1)

	local itemClickedCB = 
	function(idx)
		self:OnAnimalListItemClicked(idx)
	end
	self._animalList:SetItemClickedCB(itemClickedCB)
end

function AnimalMainView:ShowItemTips(i)
    -- LoggerHelper.Error("AnimalMainView:ShowItemTips(i) ====" .. tostring(i))
    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info[self._selectId] and god_monster_info[self._selectId][i] then
        --有装备
        local btnArgs = {}
        btnArgs.unload = true
        if AnimalManager:IsHelp(self._selectId) then
            --已助战
            btnArgs.strength = true
        end

        local itemData = CommonItemData.CreateItemData(god_monster_info[self._selectId][i], 0, i)
        itemData:SetAnimalId(self._selectId)
        TipsManager:ShowItemTips(itemData ,btnArgs)
    end
end 

function AnimalMainView:OnAnimalListItemClicked(idx)
    self._selectId = idx + 1
    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info[self._selectId] == nil or table.isEmpty(god_monster_info[self._selectId]) then
        for i=1,5 do
            self._iconViews[i]:SetItem(nil)
            self._plusViews[i]:SetActive(true)
        end
    else
        for i=1,5 do
            if god_monster_info[self._selectId][i] then
                self._iconViews[i]:SetItemData(CommonItemData.CreateItemData(god_monster_info[self._selectId][i], 0, i))
                self._plusViews[i]:SetActive(false)
            else
                self._iconViews[i]:SetItem(nil)
                self._plusViews[i]:SetActive(true)
            end
        end
    end

    if self._selectIndex ~= nil then
        self._animalList:GetItem(self._selectIndex):SetSelect(false)
    end
    self._selectIndex = idx
    self._animalList:GetItem(self._selectIndex):SetSelect(true)

    self:SetButtonState()--刷新按钮状态
    self:RefreshModel()--刷新模型
    self:RefreshHelpInfo()--刷新助战信息
    self:RefreshSkillInfo()--刷新技能信息
end

function AnimalMainView:SetButtonState()
    local awakeData = GameWorld.Player().god_monster_assist_info
    if awakeData[self._selectId] ~= nil then
        self._callBackButton:SetActive(true)
        self._helpButton:SetActive(false)
    else
        self._callBackButton:SetActive(false)
        self._helpButton:SetActive(true)
        if AnimalManager:IsAwake(self._selectId) then
            self._helpButton:GetComponent("ButtonWrapper"):SetInteractable(true)
            self._helpRedImageGO:SetActive(true)
        else
            self._helpButton:GetComponent("ButtonWrapper"):SetInteractable(false)
            self._helpRedImageGO:SetActive(false)
        end
    end

    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info[self._selectId] == nil or table.isEmpty(god_monster_info[self._selectId]) then
        self._upLoadButton:SetActive(false)
    else
        self._upLoadButton:SetActive(true)
    end

    if AnimalManager:IsHelp(self._selectId) then
        --助战中
        self._equipBagRedImageGO:SetActive(AnimalManager:IsHasHigherScore(self._selectId))
    else
        self._equipBagRedImageGO:SetActive(false)
    end
    self._upgradeRedImageGO:SetActive(AnimalManager:UpgradeNeedRedImage())    
end

--刷新技能信息
function AnimalMainView:RefreshSkillInfo()
    self._skillGridLauoutGroupData = MythicalAnimalsDataHelper:GetBuffId(self._selectId)
    for i,v in ipairs(self._skillGridLauoutGroupData) do
        v[3] = self._selectId
    end
    self._skillGridLauoutGroup:SetDataList(self._skillGridLauoutGroupData)
end

--刷新助战信息
function AnimalMainView:RefreshHelpInfo()
    self._attrGridLauoutGroupData = {}
    local showInfo = {}
    local baseInfo = MythicalAnimalsDataHelper:GetAttri(self._selectId)
    local god_monster_info = GameWorld.Player().god_monster_info
    for k,v in pairs(baseInfo) do
        local data = {}
        data.baseValue = v
        showInfo[k] = data
    end
    if god_monster_info[self._selectId] ~= nil then
        for i=1,5 do
            if god_monster_info[self._selectId][i] then
                local level = god_monster_info[self._selectId][i][public_config.ITEM_KEY_GOD_MONSTER_EQUIP_LEVEL] or 0
                local attri = MythicalAnimalsLevelDataHelper:GetAttriByTypeAndLevel(i, level)
                for k,v in pairs(attri) do
                    if showInfo[k] ~= nil then
                        if showInfo[k].addValue ~= nil then
                            showInfo[k].addValue = showInfo[k].addValue + v
                        else
                            showInfo[k].addValue = v
                        end
                    else
                        LoggerHelper.Error("AnimalMainView:RefreshHelpInfo():: animalid ===" .. tostring(self._selectId) .. "level =====" .. tostring(level) .. "type =====" .. tostring(i) .. "no this key" .. tostring(k))
                    end
                end
                --神兽装备的基础属性
                local equipId = god_monster_info[self._selectId][i][public_config.ITEM_KEY_ID]
                local equipData = EquipDataHelper:GetBaseAttris(equipId)
                for k,v in pairs(equipData) do
                    local attr = v.attri
                    local value = v.value
                    if showInfo[attr] ~= nil then
                        if showInfo[attr].addValue ~= nil then
                            showInfo[attr].addValue = showInfo[attr].addValue + value
                        else
                            showInfo[attr].addValue = value
                        end
                    else
                        LoggerHelper.Error("AnimalMainView:RefreshHelpInfo():: equipId=== " .. tostring(equipId) .. "type =====" .. tostring(i) .. "no this key" .. tostring(v[1]))
                    end
                end
            end
        end
    end
    for k,v in pairs(showInfo) do
        local data = {}
        data[1] = k
        data[2] = v.baseValue
        data[3] = v.addValue
        table.insert(self._attrGridLauoutGroupData, data)
    end

    self._attrGridLauoutGroup:SetDataList(self._attrGridLauoutGroupData)
end

--刷新模型
function AnimalMainView:RefreshModel()
    local posX = MythicalAnimalsDataHelper:GetPosX(self._selectId)
    local posY = MythicalAnimalsDataHelper:GetPosY(self._selectId)
    local scaleX = MythicalAnimalsDataHelper:GetScaleX(self._selectId)
    local scaleY = MythicalAnimalsDataHelper:GetScaleY(self._selectId)

    -- self._modelRect.transform.localPosition  = Vector3.New(self._modelLocalPosition.x + posX, self._modelLocalPosition.y + posY, self._modelLocalPosition.z) 
    -- self._modelRect.transform.localScale  = Vector3.New(scaleX/100, scaleY/100, 1)  

    -- local modelId = MythicalAnimalsDataHelper:GetModel(self._selectId)
    -- self._modelComponent:LoadModel(modelId)
    GameWorld.AddIcon(self._animalContainer, MythicalAnimalsDataHelper:GetMiddleIcon(self._selectId))
end

function AnimalMainView:OnEnable()
    self:AddEventListeners()

    self._selectIndex = AnimalManager:GetHigherScoreAnimalId() - 1
    self:RefreshViews()
    self._animalList:SetPositionByNum(self._selectIndex + 1)
end

function AnimalMainView:OnDisable()
    self:RemoveEventListeners()
    if self._selectIndex ~= 0 then
        self._animalList:GetItem(self._selectIndex):SetSelect(false)
    end
    self._selectIndex = nil
    self._addAnimalTipsContainer:SetActive(false)
end

function AnimalMainView:RefreshViews()
    for i=1,5 do
        self._iconViews[i]:SetItem(nil)
    end

    self._helpNumText.text = LanguageDataHelper.CreateContent(76408,{
        ["0"] = AnimalManager:GetHelpNum(),
        ["1"] = AnimalManager:GetCanHelpNum()
    })

    self._animalListData = MythicalAnimalsDataHelper:GetAllId()
    self._animalList:SetDataList(self._animalListData)

    self:OnAnimalListItemClicked(self._selectIndex)
end

--一键卸下点击事件
function AnimalMainView:OnUpLoadButtonClick()
    AnimalManager:UnloadAllEquip(self._selectId)
end

--神兽强化点击事件
function AnimalMainView:OnUpgradeButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_UPGRADE_EQUIP_CLICK)
end

--装备背包点击事件
function AnimalMainView:OnEquipBagButtonClick()
    self:OnAddEquipButtonClick()
end

--助战点击事件
function AnimalMainView:OnHelpButtonClick()
    AnimalManager:ASSISTAnimal(self._selectId)
    
    GameWorld.PlaySound(27)
    self._fxCompose:Play(true,true)
end

--召回点击事件
function AnimalMainView:OnCallBackButtonClick()
    AnimalManager:RecallAnimal(self._selectId)
end

--增加神兽点击事件
function AnimalMainView:OnAddAnimalButtonClick()
    --当前能助战的神兽数量
    local canHelpNum = AnimalManager:GetCanHelpNum()
    --最多能助战的神兽数量
    local maxNum = AnimalManager:GetMaxHelpNum()
    local baseNum = GlobalParamsHelper.GetParamValue(730)
    if maxNum == canHelpNum then
        GameManager.GUIManager.ShowText(2, LanguageDataHelper.CreateContent(76490))
    else
        local costDatas = GlobalParamsHelper.GetParamValue(731)
        self._addAnimalTipsContainer:SetActive(true)
        local costData = costDatas[canHelpNum - baseNum + 1]
        self._costAddAnimalIcon:UpdateData(costData[1],costData[2])
    end
end

--添加神兽装备按钮点击事件
function AnimalMainView:OnAddEquipButtonClick(i)
    EventDispatcher:TriggerEvent(GameEvents.ON_ADD_ANIMAL_EQUIP_CLICK, self._selectId, i)
end

--确定增加神兽按钮
function AnimalMainView:OnConfirmAddAnimalButton()
    AnimalManager:AddAssist()
    self:OnCloseAddAnimalWinButton()
end

--关闭增加神兽提示框
function AnimalMainView:OnCloseAddAnimalWinButton()
    self._addAnimalTipsContainer:SetActive(false)
end