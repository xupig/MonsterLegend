--AnimalBagView.lua
local AnimalBagView = Class.AnimalBagView(ClassTypes.BaseLuaUIComponent)

AnimalBagView.interface = GameConfig.ComponentsConfig.Component
AnimalBagView.classPath = "Modules.ModuleAnimal.ChildView.AnimalBagView"

local ActorModelComponent = GameMain.ActorModelComponent
local MythicalAnimalsDataHelper = GameDataHelper.MythicalAnimalsDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AnimalManager = PlayerManager.AnimalManager
local UIList = ClassTypes.UIList
local TipsManager = GameManager.TipsManager
local AnimalConfig = GameConfig.AnimalConfig
local GUIManager = GameManager.GUIManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType

require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

require "Modules.ModuleAnimal.ChildComponent.AnimalBagListItem"
local AnimalBagListItem = ClassTypes.AnimalBagListItem

function AnimalBagView:Awake()
    self:InitView()
    self:InitCallBack()
end

--override
function AnimalBagView:OnDestroy()
end

function AnimalBagView:InitCallBack()
    self._animalInfoRefresh = function() self:RefreshViews(self._animalId) end
end

function AnimalBagView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ANIMAL_INFO_REFRESH, self._animalInfoRefresh)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_BAG_SELL_COMPLETE, self._animalInfoRefresh)
end

function AnimalBagView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ANIMAL_INFO_REFRESH, self._animalInfoRefresh)
end

function AnimalBagView:InitView()
    self._iconViews = {}
    self._qulityTexts = {}
    self._nameTexts = {}
    self._posButtons = {}
    for i=1,5 do
        table.insert(self._iconViews, ItemManager():GetLuaUIComponent(nil, self:FindChildGO("Container_Left/Container_Icons/Container_Icon" .. i)))
        -- self._iconViews[i]:ActivateTipsByItemData()
        table.insert(self._qulityTexts, self:GetChildComponent("Container_Left/Container_Limit_Text/Container_" .. i .. "/Text_Qulity",'TextMeshWrapper')) 
        table.insert(self._nameTexts, self:GetChildComponent("Container_Left/Container_Limit_Text/Container_" .. i .. "/Text_Name",'TextMeshWrapper')) 
        table.insert(self._posButtons, self:FindChildGO("Container_Left/Container_Limit_Text/Container_" .. i))
        self._csBH:AddClick(self._posButtons[i], function() self:OnEquipPosClick(i) end)

        self._iconViews[i]:SetPointerUpCB(function() 
            self:ShowItemTips(i)
        end)
        -- table.insert(self._plusViews, self:FindChildGO("Container_Left/Container_Icons/Image_Plus_" .. i))
        -- self._csBH:AddClick(self._plusViews[i], function() self:OnAddEquipButtonClick(i) end)
    end

    -- self._modelRect = self:GetChildComponent("Container_Left/Container_Model", "RectTransform")
    -- self._modelLocalPosition = self._modelRect.transform.localPosition
    -- self._modelComponent = self:AddChildComponent('Container_Left/Container_Model', ActorModelComponent)
    -- self._modelComponent:SetStartSetting(Vector3(0, 0, -150), Vector3(0, 180, 0), 1)
    self._animalContainer = self:FindChildGO("Container_Left/Container_Animal")

    self._qulitySelectLevel = AnimalConfig.ALL_QUALITY_TYPE--全部品质
    self._startSelectLevel = AnimalConfig.ALL_START_TYPE--全部星级
    self._nowSelectQualityText = self:GetChildComponent("Container_Right/Container_Qulity_Select/Text_Name",'TextMeshWrapper')
    self._nowSelectStartText = self:GetChildComponent("Container_Right/Container_Start_Select/Text_Name",'TextMeshWrapper')
    self._nowSelectQualityText.text = AnimalConfig.QUALITY_SELECT_NAME_MAP[self._qulitySelectLevel]
    self._nowSelectStartText.text = AnimalConfig.QUALITY_SELECT_NAME_MAP[self._startSelectLevel]

    self._qualitySelectContainer = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select")
    self._startSelectContainer = self:FindChildGO("Container_Right/Container_Start_Select/Container_Select")

    --关闭窗口按钮
    self._closeButtton = self:FindChildGO("Container_PopupBig/Button_Close")
    self._csBH:AddClick(self._closeButtton, function() self:OnCloseWin() end)

    --品质筛选按钮点击
    self._qualitySelectButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Image_Bg")
    self._csBH:AddClick(self._qualitySelectButton, function() self:OnQualitySelectButtonClick() end)

    --星级筛选按钮点击
    self._startSelectButton = self:FindChildGO("Container_Right/Container_Start_Select/Image_Bg")
    self._csBH:AddClick(self._startSelectButton, function() self:OnStartSelectButtonClick() end)

    --空白处点击事件处理
    self._blankButton = self:FindChildGO("Container_PopupBig/Button_Blank")
    self._csBH:AddClick(self._blankButton, function() self:OnBlankButtonClick() end)

    --获取装备按钮事件
    self._getEquipButton = self:FindChildGO("Container_Right/Button_Get")
    self._csBH:AddClick(self._getEquipButton, function() self:OnGetEquipButtonClick() end)

    self:InitBagView()
    self:InitQulalitySelectButtonEvent()
    self:InitStartSelectButtonEvent()
end

function AnimalBagView:InitStartSelectButtonEvent()
    --全部星级
    self._startSelectAllButton = self:FindChildGO("Container_Right/Container_Start_Select/Container_Select/Container_Buttons/Button_All")
    self._csBH:AddClick(self._startSelectAllButton, function() self:OnSelectStart(AnimalConfig.ALL_START_TYPE) end)
    
    --1星装备
    self._startSelectOneButton = self:FindChildGO("Container_Right/Container_Start_Select/Container_Select/Container_Buttons/Button_Start_1")
    self._csBH:AddClick(self._startSelectOneButton, function() self:OnSelectStart(AnimalConfig.START_1) end)

    --2星装备
    self._startSelectTwoButton = self:FindChildGO("Container_Right/Container_Start_Select/Container_Select/Container_Buttons/Button_Start_2")
    self._csBH:AddClick(self._startSelectTwoButton, function() self:OnSelectStart(AnimalConfig.START_2) end)
    
    --3星装备
    self._startSelectThreeButton = self:FindChildGO("Container_Right/Container_Start_Select/Container_Select/Container_Buttons/Button_Start_3")
    self._csBH:AddClick(self._startSelectThreeButton, function() self:OnSelectStart(AnimalConfig.START_3) end)
end

function AnimalBagView:InitQulalitySelectButtonEvent()
    --全部品质
    self._qualitySelectAllButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_All")
    self._csBH:AddClick(self._qualitySelectAllButton, function() self:OnSelectQuality(AnimalConfig.ALL_QUALITY_TYPE) end)

    --白色品质
    self._qualitySelectWhiteButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_White")
    self._csBH:AddClick(self._qualitySelectWhiteButton, function() self:OnSelectQuality(AnimalConfig.QUALITY_1) end)

    --蓝色品质
    self._qualitySelectBlueButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_Blue")
    self._csBH:AddClick(self._qualitySelectBlueButton, function() self:OnSelectQuality(AnimalConfig.QUALITY_3) end)

    --紫色品质
    self._qualitySelectPurpleButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_Purple")
    self._csBH:AddClick(self._qualitySelectPurpleButton, function() self:OnSelectQuality(AnimalConfig.QUALITY_4) end)

    --橙色品质
    self._qualitySelectOrangeButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_Orange")
    self._csBH:AddClick(self._qualitySelectOrangeButton, function() self:OnSelectQuality(AnimalConfig.QUALITY_5) end)

    --红色品质
    self._qualitySelectRedButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_Red")
    self._csBH:AddClick(self._qualitySelectRedButton, function() self:OnSelectQuality(AnimalConfig.QUALITY_6) end)

    --金色品质
    self._qualitySelectGoldButton = self:FindChildGO("Container_Right/Container_Qulity_Select/Container_Select/Container_Buttons/Button_Gold")
    self._csBH:AddClick(self._qualitySelectGoldButton, function() self:OnSelectQuality(AnimalConfig.QUALITY_7) end)
end

function AnimalBagView:InitBagView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Right/ScrollViewList")
	self._bagList = list
	self._bagListlView = scrollView 
    self._bagListlView:SetHorizontalMove(false)
    self._bagListlView:SetVerticalMove(true)
    self._bagList:SetItemType(AnimalBagListItem)
    self._bagList:SetPadding(16, 0, 0, 16)
    self._bagList:SetGap(5, 0)
    self._bagList:SetDirection(UIList.DirectionLeftToRight, 4, 100)

    
	self._bagList:SetItemClickedCB(function(idx) self:OnItemClicked(idx) end)
end

function AnimalBagView:OnItemClicked(idx)
    local data = self._bagListData[idx +1]
    if table.isEmpty(data) then
        return 
    end
    local itemData = data.itemData
    local god_monster_info = GameWorld.Player().god_monster_info
    local animalEquipData = god_monster_info[self._animalId]
    local compare = nil
    local typeNum = data.itemCfgData.type
    if  animalEquipData and animalEquipData[typeNum] then
        compare = CommonItemData.CreateItemData(animalEquipData[typeNum], 0, typeNum)
    end

    local btnArgs = {}
    btnArgs.equip = typeNum ~= 6
    btnArgs.sell = true
    TipsManager:ShowItemTips(itemData , btnArgs, compare)
end

function AnimalBagView:OnEnable()
    self:AddEventListeners()
    
end

function AnimalBagView:OnDisable()
    self:RemoveEventListeners()
    self._qulitySelectLevel = AnimalConfig.ALL_QUALITY_TYPE     --全部品质
    self._startSelectLevel = AnimalConfig.ALL_START_TYPE        --全部星级
    self._qualitySelectContainer:SetActive(false)
    self._startSelectContainer:SetActive(false)
    self._nowSelectQualityText.text = AnimalConfig.QUALITY_SELECT_NAME_MAP[self._qulitySelectLevel]
    self._nowSelectStartText.text = AnimalConfig.QUALITY_SELECT_NAME_MAP[self._startSelectLevel]
end

function AnimalBagView:RefreshViews(animalId, typeNum)
    self._animalId = animalId
    local qualityType = MythicalAnimalsDataHelper:GetQualityType(animalId)

    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info[self._animalId] == nil or table.isEmpty(god_monster_info[self._animalId]) then
        for i=1,5 do
            self._iconViews[i]:SetItem(nil)
            self._posButtons[i]:SetActive(true)
            self._qulityTexts[i].text = AnimalManager:GetQulityNameByQulityId(qualityType[i])
            self._nameTexts[i].text = AnimalManager:GetNameByTypeAndQulity(i, qualityType[i])
        end
    else
        for i=1,5 do
            if god_monster_info[self._animalId][i] then
                self._iconViews[i]:SetItemData(CommonItemData.CreateItemData(god_monster_info[self._animalId][i], 0, i))
                self._posButtons[i]:SetActive(false)
            else
                self._iconViews[i]:SetItem(nil)
                self._posButtons[i]:SetActive(true)
                self._qulityTexts[i].text = AnimalManager:GetQulityNameByQulityId(qualityType[i])
                self._nameTexts[i].text = AnimalManager:GetNameByTypeAndQulity(i, qualityType[i])
            end
        end
    end

    self:RefreshModel()--刷新模型
    self:InitBagListData(typeNum)--刷新神兽装备背包数据
end

--刷新神兽装备背包数据
function AnimalBagView:InitBagListData(typeNum)
    self._bagListData =  AnimalManager:GetBadDataByType(self._animalId, typeNum)
    self._bagList:SetDataList(self._bagListData, 5)
end

--刷新模型
function AnimalBagView:RefreshModel()
    local posX = MythicalAnimalsDataHelper:GetPosX(self._animalId)
    local posY = MythicalAnimalsDataHelper:GetPosY(self._animalId)
    local scaleX = MythicalAnimalsDataHelper:GetScaleX(self._animalId)
    local scaleY = MythicalAnimalsDataHelper:GetScaleY(self._animalId)
    
    -- self._modelRect.transform.localPosition  = Vector3.New(self._modelLocalPosition.x + posX, self._modelLocalPosition.y + posY, self._modelLocalPosition.z) 
    -- self._modelRect.transform.localScale  = Vector3.New(scaleX/100, scaleY/100, 1) 

    -- local modelId = MythicalAnimalsDataHelper:GetModel(self._animalId)
    -- self._modelComponent:LoadModel(modelId)    
    GameWorld.AddIcon(self._animalContainer, MythicalAnimalsDataHelper:GetMiddleIcon(self._animalId))
end

function AnimalBagView:ShowItemTips(i)
    -- LoggerHelper.Error("AnimalMainView:ShowItemTips(i) ====" .. tostring(i))
    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info[self._animalId] and god_monster_info[self._animalId][i] then
        --有装备
        local btnArgs = {}
        if AnimalManager:IsHelp(self._animalId) then
            --已助战
            btnArgs.strength = true
        end
        btnArgs.unload = true
        local itemData = CommonItemData.CreateItemData(god_monster_info[self._animalId][i], 0, i)
        itemData:SetAnimalId(self._animalId)
        
        TipsManager:ShowItemTips(itemData,btnArgs)
    end
end 

--关闭界面
function AnimalBagView:OnCloseWin()
    self.gameObject:SetActive(false)
end

--点击未装备神兽装备的事件处理
function AnimalBagView:OnEquipPosClick(pos)
    -- self._bagList:SetDataList({})
    if AnimalManager:IsExitEquipByPos(self._animalId, pos) then
        self._bagListData =  AnimalManager:GetBadDataByType(self._animalId, pos)
        self._bagList:SetDataList(self._bagListData,5)
    else
        local value = {
            ["confirmCB"] = function() self:OnGetEquipButtonClick() end,--获取神兽装备
            ["cancelCB"] = nil,
            ["text"] = LanguageDataHelper.CreateContent(76488),
            ["confirmText"] = LanguageDataHelper.CreateContent(76436)
        }
        local msgData = {["id"] = MessageBoxType.Tip, ["value"] = value}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
    end
end

--选择品质处理事件
function AnimalBagView:OnSelectQuality(quality)
    self._qulitySelectLevel = quality
    self._bagListData = AnimalManager:GetBadDataByQualityAndStart(self._animalId, self._qulitySelectLevel, self._startSelectLevel, AnimalConfig.QUALITY_EQUAL)
    self._bagList:SetDataList(self._bagListData,5)
    self._qualitySelectContainer:SetActive(false)
    self._nowSelectQualityText.text = AnimalConfig.QUALITY_SELECT_NAME_MAP[quality]
end

--选择星级处理事件
function AnimalBagView:OnSelectStart(start)
    self._startSelectLevel = start
    self._bagListData = AnimalManager:GetBadDataByQualityAndStart(self._animalId, self._qulitySelectLevel, self._startSelectLevel, AnimalConfig.QUALITY_EQUAL)
    self._bagList:SetDataList(self._bagListData,5)
    self._startSelectContainer:SetActive(false)
    self._nowSelectStartText.text = AnimalConfig.START_SELECT_NAME_MAP[start]
end

--品质筛选按钮点击
function AnimalBagView:OnQualitySelectButtonClick()
    self._qualitySelectContainer:SetActive(true)
end

--星级筛选按钮点击
function AnimalBagView:OnStartSelectButtonClick()
    self._startSelectContainer:SetActive(true)
end

--空白处点击处理事件
function AnimalBagView:OnBlankButtonClick()
    self._qualitySelectContainer:SetActive(false)
    self._startSelectContainer:SetActive(false)
end

--获取装备按钮事件处理
function AnimalBagView:OnGetEquipButtonClick()
    GUIManager.ClosePanel(PanelsConfig.Animal)    
    GUIManager.ShowPanelByFunctionId(601)
end