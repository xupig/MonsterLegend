-- RuneUpReView.lua
local RuneUpReView = Class.RuneUpReView(ClassTypes.BaseComponent)

require "Modules.ModuleRune.ChildComponent.RuneAttHoleItem"
require "Modules.ModuleRune.ChildComponent.RuneEliteItem"
require "Modules.ModuleRune.ChildComponent.RuneReplaceListItem"
require "Modules.ModuleRune.ChildView.RuneAttriView"
require "UIComponent.Extend.ItemGrid"
require "Modules.ModuleRune.ChildView.RuneOverView"

RuneUpReView.interface = GameConfig.ComponentsConfig.Component
RuneUpReView.classPath = "Modules.ModuleRune.ChildView.RuneUpReView"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local RuneData = PlayerManager.PlayerDataManager.runeData

local GUIManager = GameManager.GUIManager
local RuneManager = PlayerManager.RuneManager

local public_config = GameWorld.public_config

local RuneAttHoleItem = ClassTypes.RuneAttHoleItem
local RuneEliteItem = ClassTypes.RuneEliteItem
local RuneReplaceListItem = ClassTypes.RuneReplaceListItem
local RuneAttriView = ClassTypes.RuneAttriView
local ItemGrid = ClassTypes.ItemGrid
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local RuneOverView = ClassTypes.RuneOverView
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIParticle = ClassTypes.UIParticle

function RuneUpReView:Awake()
    self._runetypes = {}
    self._runelevel = 0
    self._runeid = nil
    self._aGo = {}
    self._attGo = {}
    self._aText = {}
    self._arrows = {}
    self._attnextGo = {}
    self._attcurText = {}
    self._attnextText = {}
    self._attnextText = {}
    self._curHoleIndex = 1
    self._curHoleType = 0
    self._colors={}
    self._colors[1] = ColorConfig.A
    self._colors[3] = ColorConfig.K
    self._colors[4] = ColorConfig.L
    self._colors[5] = ColorConfig.F
    self._colors[6] = ColorConfig.H
    self._update = function()self:Update()end
    self._putonUpdate = function()self:PutonRuneUpdate()end
    self._eliteUpdate = function()self:UpdateElite()end
    self._setfxstate = function(state)self:SetFxState(state)end
    self._csBH = self:GetComponent("LuaUIComponent")
    self.fxtarget = UIParticle.AddParticle(self.gameObject,"Container_UpRe/Container_Up/Container_Fx/fx_ui_30007")
    self.fxtarget:Stop()
    
    self.fx = UIParticle.AddParticle(self.gameObject,"Container_Fx/fx_ui_30006")
    self.fx:Play(true,true)
    self:InitView()
end

function RuneUpReView:InitView()
    self._btnUpRed = self:FindChildGO('Container_UpRe/Container_Up/Button_Up/Image_BtnUpRed')
    self._repRed = self:FindChildGO('Container_UpRe/Container_Up/Button_Replace/Image_BtnReplaceRed')
    self._redUpRed = self:FindChildGO('Container_UpRe/Container_Up/Button_Up/Image_BtnUpRed')
    self._containerAtt = self:FindChildGO('Container_AttTips')
    self._containerUp = self:FindChildGO('Container_UpRe/Container_Up')
    self._containerRe = self:FindChildGO("Container_UpRe/Container_Replace")
    self._btngo = self:FindChildGO("Container_UpRe/Container_Replace/Button_Go")
    self._containerUpRe = self:FindChildGO('Container_UpRe')
    self._textunLockTips = self:FindChildGO('Text_UnLockTips')    
    self._textRuneNamego = self:FindChildGO("Container_UpRe/Container_Up/Text_UpName")
    self._buttonUp = self:FindChildGO("Container_UpRe/Container_Up/Button_Up")
    self._buttonAtt = self:FindChildGO("Button_Att")
    self._imgMateril = self:FindChildGO("Container_UpRe/Container_Up/Container_UpCost/Image_MaterilCost")
    self._textNumCostgo = self:FindChildGO("Container_UpRe/Container_Up/Container_UpCost/Text_NumCost")
    self._costgo = self:FindChildGO("Container_UpRe/Container_Up/Container_UpCost")

    self._a0go = self:FindChildGO("Container_UpRe/Container_Up/Container_A0")
    self._a1go = self:FindChildGO("Container_UpRe/Container_Up/Container_A1")
    self._att0go = self:FindChildGO("Container_UpRe/Container_Up/Container_Att0")
    self._att1go = self:FindChildGO("Container_UpRe/Container_Up/Container_Att1")

    self._textCurAtt0go = self:FindChildGO("Container_UpRe/Container_Up/Container_Att0/Text_CurAtt0")
    self._textCurAtt1go = self:FindChildGO("Container_UpRe/Container_Up/Container_Att1/Text_CurAtt1")
    self._imageArrows0 = self:FindChildGO("Container_UpRe/Container_Up/Container_Att0/Image_Arrows0")
    self._imageArrows1 = self:FindChildGO("Container_UpRe/Container_Up/Container_Att1/Image_Arrows1")
    self._textNextAttgo0 = self:FindChildGO("Container_UpRe/Container_Up/Container_Att0/Text_NextAtt0")
    self._textNextAttgo1 = self:FindChildGO("Container_UpRe/Container_Up/Container_Att1/Text_NextAtt1")

    self._buttonGoWatch = self:FindChildGO("Container_GoWatch/Button_GoWatch")
    self._textunlock = self:GetChildComponent("Text_UnLockTips", "TextMeshWrapper")

    self._textAtt0 = self:GetChildComponent("Container_UpRe/Container_Up/Container_A0/Text_Att0", "TextMeshWrapper")
    self._textAtt1 = self:GetChildComponent("Container_UpRe/Container_Up/Container_A1/Text_Att1", "TextMeshWrapper")

    self._textNextAtt0 = self:GetChildComponent("Container_UpRe/Container_Up/Container_Att0/Text_NextAtt0","TextMeshWrapper")
    self._textCurAtt0 = self:GetChildComponent("Container_UpRe/Container_Up/Container_Att0/Text_CurAtt0","TextMeshWrapper")

    self._textNextAtt1 = self:GetChildComponent("Container_UpRe/Container_Up/Container_Att1/Text_NextAtt1","TextMeshWrapper")
    self._textCurAtt1 = self:GetChildComponent("Container_UpRe/Container_Up/Container_Att1/Text_CurAtt1","TextMeshWrapper")

    self._textNumCost = self:GetChildComponent("Container_UpRe/Container_Up/Container_UpCost/Text_NumCost", "TextMeshWrapper")
    self._textRuneName = self:GetChildComponent("Container_UpRe/Container_Up/Text_UpName","TextMeshWrapper")
    self._textElite = self:GetChildComponent("Container_Elite/Text_Elite","TextMeshWrapper")
    self._textNext = self:GetChildComponent("Container_UpRe/Container_Up/Text_Next","TextMeshWrapper")
    self._btnUptext = self:GetChildComponent("Container_UpRe/Container_Up/Button_Up/Text","TextMeshWrapper")

    self._overViewGo = self:FindChildGO("Container_OverView")

    self._aGo[1] = self._a0go
    self._aGo[2] = self._a1go
    self._attGo[1] = self._att0go
    self._attGo[2] = self._att1go

    self._arrows[1] = self._imageArrows0
    self._arrows[2] = self._imageArrows1

    self._attnextGo[1] = self._textNextAttgo0
    self._attnextGo[2] = self._textNextAttgo1

    self._aText[1] = self._textAtt0
    self._aText[2] = self._textAtt1

    self._attcurText[1] = self._textCurAtt0
    self._attcurText[2] = self._textCurAtt1
    self._attnextText[1] = self._textNextAtt0
    self._attnextText[2] = self._textNextAtt1

    self._csBH:AddClick(self._buttonGoWatch,function() self:BtnGoWatchClick() end)
    self._csBH:AddClick(self._buttonUp,function() self:BtnUpClick() end)    
    self._holes = {}
    for i=0,7 do
        local _hole = self:AddChildLuaUIComponent('Container_Hole'..i,RuneAttHoleItem)
        self._holes[i+1] = _hole
        _hole:Selected(false)        
    end 
    self._elite = self:AddChildLuaUIComponent('Container_Elite',RuneEliteItem)
    self._att = self:AddChildLuaUIComponent('Container_AttTips',RuneAttriView) 
    self._clickCB = function(data) self:onClick(data) end
    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'Container_UpRe/Container_Replace/ScrollViewList_Re')
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(RuneReplaceListItem)
    self._list:SetPadding(20,0,0,20)
    self._list:SetGap(10,10)
    self._list:SetDirection(UIList.DirectionLeftToRight,2,-1)
    local itemClickedCB = 
	function(index)
		self:OnItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    local parent = self:FindChildGO("Container_UpRe/Container_Up/Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)

    self._textElite.text = LanguageDataHelper.GetContent(25234)
    self._textNext.text = LanguageDataHelper.GetContent(25233)

    self._overView = self:AddChildLuaUIComponent('Container_OverView',RuneOverView)
    self._textunLockTips:SetActive(false)

    self._csBH:AddClick(self._buttonAtt,function() self:ClickAtt() end)


    local btnreclose = self:FindChildGO('Container_UpRe/Container_Replace/Button_Close')
    self._csBH:AddClick(btnreclose,function() self:ClickReplaceBtn() end)

    local btnre = self:FindChildGO('Container_UpRe/Container_Up/Button_Replace')
    self._csBH:AddClick(btnre,function() self:OpenReplaceBtn() end)

    self._bagText = self:GetChildComponent("Container_UpRe/Container_Replace/Text_Bag","TextMeshWrapper")


    self._csBH:AddClick(self._btngo,function() self:OnbtnGo() end)
end


function RuneUpReView:OnbtnGo()
    GUIManager.ShowPanel(PanelsConfig.InstanceHall)
end

function RuneUpReView:OpenReplaceBtn()
    self._containerRe:SetActive(true)
    self.fx:Stop()
    self._flageRe = true
    self:UpdateRuneRe()
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleRune.ChildView.RuneUpReView")
end

function RuneUpReView:ClickAtt()
    self._att:SetActive(true)
    self.fx:Stop()
    self._att:Update()   
end

function RuneUpReView:ClickReplaceBtn()
    self._containerRe:SetActive(false)
    self._flageRe = true
    self.fx:Play(true,true)
end

function RuneUpReView:UpdateLockTips(layer)
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = layer
    self._textunLockTips:SetActive(true)        
    local _text = LanguageDataHelper.CreateContent(25227,argsTable)
    self._textunlock.text = _text
end

function RuneUpReView:ClickClose()
    self._containerAtt:SetActive(true)
    self._att:Update()
    self.fx:Stop()
    self._containerUpRe:SetActive(false)
end

function RuneUpReView:OnItemClicked(index)
    local item = self._list:GetItem(index)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_rune_replace_button")
    if item ~= nil then 
        local itemdata = item.Itemdata
        local pkgpos = itemdata._pkgPos
        if self._curHoleType == 1 then -- Lock
            return
        elseif self._curHoleType == 0 then --空
            self._runeid = nil
            self:RuneInsert(pkgpos,self._curHoleIndex)
        elseif self._curHoleType == 2 then --有符文
            self._runeid = itemdata._runeid
            self._runelevel = itemdata._level
            self:RuneReplace(self._curHoleIndex,pkgpos)
        end
    end
end


function RuneUpReView:onClick(data)
    if data == nil then
        return
    end  
    local _type = 0
    local _holeIndex = data.slot
    if not data._have then
        if RuneData:IsLayerLock(data.slot) then
            _type=1
        else
            _type=0
        end 

    else
        _type=2
    end 
    self._curHoleType = _type
    self._curHoleIndex = _holeIndex
    self:SelectedState(_holeIndex)
    self._containerAtt:SetActive(false)
    self._containerUpRe:SetActive(true)
    if _type == 1 then -- Lock
        self._containerAtt:SetActive(true)
        self._att:Update()
        self._containerUpRe:SetActive(false)
    else
        local  item = data:GetRuneItem() 
        if _type == 0 then --空
            self._runeid = nil   
            local haveCost = RuneData:GetRuneEliteNum()
            self._elite:SetEliteNum(haveCost,0)
            self._redUpRed:SetActive(false)
            self:OpenReplaceBtn()
        elseif _type == 2 then --有符文
            self._runeid = item._runeid 
            self._runelevel = item._level 
            self._redUpRed:SetActive(item:IsRed())
            local f = RuneData:GetRepRed() or RuneData:IsSameRuneAndUp(self._runeid,self._runelevel)
            self._repRed:SetActive(f)
            self:UpdateRuneUp(self._curHoleIndex)
        end  

    end
end

function RuneUpReView:UpdateRuneUp(holeIndex)
    local runeinfo = RuneData:GetRuneUpData(holeIndex)
    if runeinfo ~= nil then
        local level = runeinfo._level
        local costnum = runeinfo:GetUpCost()[12]
        local costid = ItemDataHelper.GetIcon(12)
        local curatt = RuneManager:GetRuneAtt(runeinfo:GetAtt())
        local nextatt = RuneManager:GetRuneAtt(runeinfo:GetNextAtt())
        self._textRuneName.text = runeinfo:GetName()    
        local d0 = self:GetAttContent(curatt)   
        local d1 = self:GetAttContent(nextatt)     
        if table.isEmpty(d0) and table.isEmpty(d1) and runeinfo:GetExp() ~= 0 then
            self._aGo[1]:SetActive(true)  
            self._aText[1].text = self:GetAttContent1(LanguageDataHelper.GetContent(1019),runeinfo:GetExp(),2)
        else
            if d0[1] ~=nil then
                self._aGo[1]:SetActive(true)
                
                self._aText[1].text = string.gsub(d0[1],"\n","  ")--d0[1]
            else
                self._aGo[1]:SetActive(false)      
            end 
            if d0[2] ~=nil then
                self._aGo[2]:SetActive(true)
                self._aText[2].text = string.gsub(d0[2],"\n","  ")--d0[2]
            else
                self._aGo[2]:SetActive(false)      
            end 
        end
        
        if runeinfo:IsFulllevel()then    
            if d0[1] ~=nil then
                self._attGo[1]:SetActive(true)
                self._attcurText[1].text = d0[1]
            else
                self._attGo[1]:SetActive(false)      
            end 
            if d0[2] ~=nil then
                self._attGo[2]:SetActive(true)
                self._attcurText[2].text = d0[2]
            else
                self._attGo[2]:SetActive(false)      
            end   
            self._btnUptext.text=LanguageDataHelper.GetContent(76489)
            self._textNext.text = LanguageDataHelper.GetContent(75527)
            self._arrows[1]:SetActive(false)
            self._arrows[2]:SetActive(false)
            self._costgo:SetActive(false)
        else
            self._costgo:SetActive(true)
            self._btnUptext.text=LanguageDataHelper.GetContent(25231)
            local f0=false
            local f1=false
            if d0[1] ~=nil then
                self._attGo[1]:SetActive(true)
                self._attcurText[1].text = d0[1]
            else
                if  runeinfo:GetExp()~= 0 then
                    f0=true
                    self._attGo[1]:SetActive(true)                              
                    self._attcurText[1].text = self:GetAttContent1(LanguageDataHelper.GetContent(1019),runeinfo:GetExp(),2)
                else
                    self._attGo[1]:SetActive(false)                              
                end
            end 
            if d0[2] ~=nil then
                self._attGo[2]:SetActive(true)
                self._attcurText[2].text = d0[2]
            else
                if  runeinfo:GetExp()~= 0 and not f0 then
                    self._attGo[2]:SetActive(true)                              
                    self._attcurText[2].text = self:GetAttContent1(LanguageDataHelper.GetContent(1019),runeinfo:GetExp(),2)
                else
                    self._attGo[2]:SetActive(false)                              
                end   
            end         
            if d1[1] ~=nil then
                self._attGo[1]:SetActive(true)
                self._attnextText[1].text = d1[1]
            else
                if runeinfo:GetNextExp()~= 0 then
                    self._attGo[1]:SetActive(true)  
                    f1 = true                            
                    self._attnextText[1].text = self:GetAttContent1(LanguageDataHelper.GetContent(1019),runeinfo:GetNextExp(),2)
                else
                    self._attGo[1]:SetActive(false)                              
                end  
            end 
            if d1[2] ~=nil then
                self._attGo[2]:SetActive(true)
                self._attnextText[2].text = d1[2]
            else
                if runeinfo:GetNextExp() ~= 0 and not f1 then
                    self._attGo[2]:SetActive(true)                              
                    self._attnextText[2].text = self:GetAttContent1(LanguageDataHelper.GetContent(1019),runeinfo:GetNextExp(),2)
                else
                    self._attGo[2]:SetActive(false)                              
                end     
            end         
            self._arrows[1]:SetActive(true)
            self._arrows[2]:SetActive(true) 
        end
        GameWorld.AddIcon(self._imgMateril,costid,nil,true)
        local haveCost = RuneData:GetRuneEliteNum()
        local msg = 'x'..costnum    
        if haveCost>=costnum then
            self._textNumCost.text = BaseUtil.GetColorString(msg,ColorConfig.J) 
            self._elite:SetEliteNum(haveCost,0)
        else 
            self._textNumCost.text = BaseUtil.GetColorString(msg,ColorConfig.H)     
            self._elite:SetEliteNum(haveCost,1)        
        end    
        self._icon:SetItem(runeinfo:GetItemComid())    
        local f = runeinfo:IsRed()
        self._redUpRed:SetActive(f)
    end
end
function RuneUpReView:PutonRuneUpdate()
    if self._curHoleType==0 then
        self._curHoleType=2
    end
end
function RuneUpReView:GetAttContent(att)
    local d = {}
    if att ~= nil then
        local i = 1
        for k,v in pairs(att) do 
            local content = v[0]
            local num = v[1]
            local t = v[2]
            local txt = ""
            local con = BaseUtil.GetColorString(content,ColorConfig.B)   
            if t == 2 then
                local s = string.format("%.2f",num/100).."%"
                local n = BaseUtil.GetColorString(s,ColorConfig.A)         
                txt = con.."\n"..n
            else
                local s = "+"..tostring(num)
                local n = BaseUtil.GetColorString(s,ColorConfig.A)         
                txt = con..'\n'..n
            end
            d[i] = txt
            i = i+1
        end
    end
    return d
end

function RuneUpReView:GetAttContent1(content,num,t)
    local txt = ""
    local con = BaseUtil.GetColorString(content,ColorConfig.B)   
    if t == 2 then
        local s = "+"..tostring(num/100-num/100%0.01)..'%'
        local n = BaseUtil.GetColorString(s,ColorConfig.A)         
        txt = con.."    "..n
    else
        local s = "+"..tostring(num)
        local n = BaseUtil.GetColorString(s,ColorConfig.A)         
        txt = con..'    '..n
    end
    return txt
end
function RuneUpReView:UpdateRuneRe()
    local data = {}
    local samerune = RuneData:GetSameRune(self._runeid,self._runelevel)
    local reinfo = RuneData:GetRuneReData()
    if samerune ~= nil then
        for k,v in pairs(samerune) do
            table.insert(data,v)
        end
    end
    if reinfo ~= nil then
        for k,v in pairs(reinfo) do
            table.insert(data,v)
        end
    end
    self._list:SetDataList(data)
    local f = RuneData:GetRepRed() or RuneData:IsSameRuneAndUp(self._runeid,self._runelevel)
    self._repRed:SetActive(f)
end


function RuneUpReView:SelectedState(index)
    for i=1,8 do 
        local _hole = self._holes[i]
        if index == i then
            _hole:Selected(true)
        else
            _hole:Selected(false)
        end
    end
end

function RuneUpReView:OnToggleGroupClick(index)
    -- self.fxtarget:Stop()
    -- if self._curHoleType == 0 then
    --     index = 1
    --     -- self._toggleReplace:SetIsOn(true)
    --     -- self._toggleUp:SetIsOn(false)
    -- end
    -- if index == 0 then
    --     self._curTab = 0
    --     self._containerUp:SetActive(true)
    --     self._containerRe:SetActive(false)
    --     self:UpdateRuneUp(self._curHoleIndex)
    --     EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_rune_up_view_button")
    -- else 
    --     self._curTab = 1        
    --     self._containerUp:SetActive(false)
    --     self._containerRe:SetActive(true)
    --     self:UpdateRuneRe()
    -- end
    -- EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleRune.ChildView.RuneUpReView")
end

-- 镶嵌
function RuneUpReView:RuneInsert(pkgpos,gridpos)
    RuneManager.RunePutOnRpc(pkgpos,gridpos)
end

-- 替换
function RuneUpReView:RuneReplace(gridpos,pkgpos)
    RuneManager.RuneReplaceRpc(gridpos,pkgpos)
end

-- 升级
function RuneUpReView:BtnUpClick()
    self.fxtarget:Play(true,true)
    RuneManager.RuneUpLevelRpc(self._curHoleIndex)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_rune_up_button")
end


function RuneUpReView:BtnGoWatchClick()
    self._overViewGo:SetActive(true)
    local da = RuneManager:RuneShowData()
    self._overView:Update(da)
    self.fx:Stop()
end
function RuneUpReView:OnDestroy()
    self._buttonGoWatch = nil
    self._holes = nil
    self._csBH = nil
end

function RuneUpReView:Update()
    self:UpdateHoles()
    if RuneData:IsAllLockEmptySlot() then  
        self._curHoleType = 1
        self._containerUp:SetActive(false)
    else
        self._containerUp:SetActive(true)
        self:UpdateRuneUp(self._curHoleIndex)  
        self:SelectedState(self._curHoleIndex)
        local runeinfo = RuneData:GetRuneUpData(self._curHoleIndex)
        if runeinfo then
            local f = runeinfo:IsRed()
            self._redUpRed:SetActive(f)
        end
        local f = RuneData:GetRepRed() or RuneData:IsSameRuneAndUp(self._runeid,self._runelevel)
        self._repRed:SetActive(f)
        if self._flageRe or RuneData:IsHaveEmptySlot()then
            self:ClickReplaceBtn()
        end   
        if RuneData:IsHaveEmptySlot() then
            self._curHoleType = 0
        elseif RuneData:IsHaveRuneSlot() then
            self._curHoleType = 2
        end       
    end
    -- self:OpenReplaceBtn()
    local pkg = GameWorld.Player().rune_pkg or {}
    self._bagText.text = LanguageDataHelper.GetContent(76209)..' '..BaseUtil.GetColorString(#pkg..'/'..GlobalParamsHelper.GetParamValue(478),ColorConfig.A)
end

function RuneUpReView:Close()
    self.fx:Stop()
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_SELECT_RUNE_HOLE,self._clickCB)    
    EventDispatcher:RemoveEventListener(GameEvents.RuneReUpdate,self._update)
    EventDispatcher:RemoveEventListener(GameEvents.RuneUpUpdate,self._update)
    EventDispatcher:RemoveEventListener(GameEvents.RunePutOnUpdate,self._putonUpdate)
    EventDispatcher:RemoveEventListener(GameEvents.RuneFx,self._setfxstate)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._update)   
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._runePieceChange)    
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RUNE_ESSENCE, self._eliteUpdate)   
end

function RuneUpReView:Open()
    self.fxtarget:Stop()
    self.fx:Play(true,true)
    self._repRed:SetActive(false)
    self._redUpRed:SetActive(false)
    self._a0go:SetActive(false)
    self._a1go:SetActive(false)
    self._att0go:SetActive(false)
    self._att1go:SetActive(false)
    self._imageArrows0:SetActive(false)
    self._imageArrows1:SetActive(false)
    self._overViewGo:SetActive(false)
    self:UpdateElite()
    self:Update()
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_SELECT_RUNE_HOLE,self._clickCB)
    EventDispatcher:AddEventListener(GameEvents.RuneReUpdate,self._update)
    EventDispatcher:AddEventListener(GameEvents.RuneUpUpdate,self._update)
    EventDispatcher:AddEventListener(GameEvents.RunePutOnUpdate,self._putonUpdate)
    EventDispatcher:AddEventListener(GameEvents.RuneFx,self._setfxstate)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._update)   
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._runePieceChange)    
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RUNE_ESSENCE, self._eliteUpdate)   
end

function RuneUpReView:SetFxState(state)
    if state then
        self.fx:Play(true,true)
    else
        self.fx:Stop()
    end
end
function RuneUpReView:UpdateElite()
    local d = {}
    d[1] = 1
    d[2] = RuneData:GetRuneEliteIcon()
    d[3] = RuneData:GetRuneEliteNum()
    d[4] = 1
    self._elite:Update(d)    
end

function RuneUpReView:UpdateHoles()
    local slot = -1
    for i=1,8 do
        local item = RuneData:GetSlotItemData(i)
        if slot==-1 and RuneData:IsLayerLock(i) then
            slot=i
        end
        self._holes[i]:Update(item)
    end
    if slot~=-1 then
        local unlocklevellist = GlobalParamsHelper.GetParamValue(475) -- 解锁levellist
        self:UpdateLockTips(unlocklevellist[slot])
    else
        self._textunLockTips:SetActive(false)
    end
end 