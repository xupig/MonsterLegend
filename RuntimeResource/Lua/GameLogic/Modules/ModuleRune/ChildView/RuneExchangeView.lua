-- RuneExchangeView.lua
local RuneExchangeView = Class.RuneExchangeView(ClassTypes.BaseComponent)

require "Modules.ModuleRune.ChildComponent.RuneExchangeListItem"

RuneExchangeView.interface = GameConfig.ComponentsConfig.Component
RuneExchangeView.classPath = "Modules.ModuleRune.ChildView.RuneExchangeView"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local RuneExchangeListItem = ClassTypes.RuneExchangeListItem
local RuneData = PlayerManager.PlayerDataManager.runeData

local GUIManager = GameManager.GUIManager

local public_config = GameWorld.public_config

function RuneExchangeView:Awake()
    self:InitView()
    self._update = function() self:Update()end
end

function RuneExchangeView:InitView()
    self._matericon = self:FindChildGO("Container_Material/Image_MaterIcon")
    self._maternum = self:GetChildComponent("Container_Material/Text_MaterilNum","TextMeshWrapper")
    self._textPiece = self:GetChildComponent("Container_Material/Text_Piece","TextMeshWrapper")
    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'ScrollViewList')
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(RuneExchangeListItem)
    self._list:SetPadding(15,0,0,5)
    self._list:SetGap(5,10)
    self._list:SetDirection(UIList.DirectionLeftToRight,2,-1)
    local itemClickedCB = 
	function(index)
		self:OnItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)
    self._textPiece.text=LanguageDataHelper.GetContent(25226)
end

function RuneExchangeView:OnItemClicked(index)

end
  

function RuneExchangeView:OnDestroy()
   
end


function RuneExchangeView:Update()
    local data = RuneData:GetExchangeData()
    local d = {}
    local i=1
    for k,v in ipairs(data[1]) do
        d[i]=v
        i=i+1
    end
    for k,v in ipairs(data[2]) do
        d[i]=v
        i=i+1        
    end
    self._list:SetDataList(d)
    self:UpdateMateril()
end


function RuneExchangeView:Close()
    self:AbleScr(false)
    self:Clear()
end

function RuneExchangeView:AbleScr(state)
    if self._scrollview then
        self._scrollview:SetScrollRectState(state)
    end
end

function RuneExchangeView:Open()
    self:AbleScr(true)    
    EventDispatcher:AddEventListener(GameEvents.RuneExchangeUpdate,self._update)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_AION_INFO, self._update)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._update)    
    self:Update()

end

function RuneExchangeView:UpdateMateril()
    local num = RuneData:GetRunePieceNum()
    local iconid = RuneData:GetRunePieceIcon()
    self._maternum.text = num
    GameWorld.AddIcon(self._matericon,iconid)
end

function RuneExchangeView:Clear()
    EventDispatcher:RemoveEventListener(GameEvents.RuneExchangeUpdate,self._update)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_AION_INFO, self._update)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._update)
end