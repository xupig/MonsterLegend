-- RuneOverView.lua
local RuneOverView = Class.RuneOverView(ClassTypes.BaseComponent)
-- require "Modules.ModuleRune.ChildComponent.RuneOverViewListItem"
require "Modules.ModuleRune.ChildComponent.RuneOverViewListItem1"
RuneOverView.interface = GameConfig.ComponentsConfig.Component
RuneOverView.classPath = "Modules.ModuleRune.ChildView.RuneOverView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local RuneData = PlayerManager.PlayerDataManager.runeData
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local UIComplexList = ClassTypes.UIComplexList
local public_config = GameWorld.public_config
-- local RuneOverViewListItem = ClassTypes.RuneOverViewListItem
local RuneOverViewListItem1 = ClassTypes.RuneOverViewListItem1

function RuneOverView:Awake()
    self._csBH = self:GetComponent("LuaUIComponent")
    self:InitView()
end

function RuneOverView:InitView()
    self._btnCloseG0 = self:FindChildGO("Button_Close")
    self._title = self:GetChildComponent("Text_Title","TextMeshWrapper")
    self._csBH:AddClick(self._btnCloseG0,function() self:BtnClose() end)
    self._title.text = LanguageDataHelper.GetContent(25282)
    local srollview,list = UIComplexList.AddScrollViewComplexList(self.gameObject,'ScrollViewList_Open')
    self._list = list
    self._scrollView = srollview
    self._list:SetItemType(RuneOverViewListItem1)
    self._list:SetPadding(0,0,0,0)
    self._list:SetGap(20,0)
    self._list:SetDirection(UIList.DirectionTopToDown,-1,-1)
    self._scrollView:SetHorizontalMove(false)
    self._scrollView:SetVerticalMove(true)
    self._scrollView:SetScrollRectEnable(true)
end

function RuneOverView:BtnClose()
    self.gameObject:SetActive(false)
    EventDispatcher:TriggerEvent(GameEvents.RuneFx,true)    
end
function RuneOverView:OnItemClicked()

end

function RuneOverView:OnDestroy()
    self._csBH = nil    
end


function RuneOverView:Update(data)
    self._list:RemoveAll()
    self._list:AppendItemDataList(data)
end    



