-- RuneResolveView.lua
local RuneResolveView = Class.RuneResolveView(ClassTypes.BaseComponent)
require "Modules.ModuleRune.ChildComponent.RuneResolveListItem"
require "Modules.ModuleRune.ChildComponent.RuneResovleFxItem"

RuneResolveView.interface = GameConfig.ComponentsConfig.Component
RuneResolveView.classPath = "Modules.ModuleRune.ChildView.RuneResolveView"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local RuneResovleFxItem = ClassTypes.RuneResovleFxItem
local RuneResolveListItem = ClassTypes.RuneResolveListItem
local RuneData = PlayerManager.PlayerDataManager.runeData

local GUIManager = GameManager.GUIManager
local RuneManager = PlayerManager.RuneManager

local public_config = GameWorld.public_config

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local LuaUIParticle = GameMain.LuaUIParticle
local Vector3 = Vector3
local Vector2 = Vector2
local HIDE_POSITION = Vector2(-1000, -1000)

function RuneResolveView:Awake()
    self._csBH = self:GetComponent("LuaUIComponent")
    self._pkgupdate = function() self:RunePkgUpdate()end
    self._resolveupdate = function(data) self:RuneResolveUpdate(data)end
    self._essenceupdate = function() self:RuneEssenceUpdate()end
    self._tweenOk = function() self:TweenOK()end
    self._posFIndexs = {}
    self._fxItemPool = {}
    self._fxItems = {}
    self._countRes = 0
    self._countResAcc = 0
    self._offSetX = 358--X偏移
    self._offSetY = 475--Y偏移
    self._uplimitY = 580--Y上限
    self._downlimitY = 20--Y下限
    self._leftRight = 600 --左右分界
    self._Fxposition = {
        [1]=Vector2(344,500),--左上
        [2]=Vector2(676,500),--右上
        [3]=Vector2(344, 34),--左下
        [4]=Vector2(676, 34),--右下
    }
    self:InitView()
end

function RuneResolveView:InitView()
    self._getRuneiconGo = self:FindChildGO("Image_GetRune")
    self._runeEliteGo = self:FindChildGO("Container_Get/Image_Elite")
    self._textNumGo = self:GetChildComponent("Container_Get/Text_Num","TextMeshWrapper")
    self._textGet = self:GetChildComponent("Container_Get/Text_Get","TextMeshWrapper")
    self._textReNumGo = self:GetChildComponent("Text_ReNum","TextMeshWrapper")
    self._textResolve = self:GetChildComponent("Text_Resolve","TextMeshWrapper")
    self._textElite = self:GetChildComponent("Text_Elite","TextMeshWrapper")
    self._textDes = self:GetChildComponent("Text_Des","TextMeshWrapper")
    self._toggleOr = UIToggle.AddToggle(self.gameObject,"Toggle1")
    self._toggleZi = UIToggle.AddToggle(self.gameObject,"Toggle2")
    self._toggleB = UIToggle.AddToggle(self.gameObject,"Toggle3")
    self._toggleW = UIToggle.AddToggle(self.gameObject,"Toggle4")  
    self._toggleOr:SetOnValueChangedCB(function(stage) self:OnToggleClick1(stage)end)
    self._toggleZi:SetOnValueChangedCB(function(stage) self:OnToggleClick2(stage)end)    
    self._toggleB:SetOnValueChangedCB(function(stage) self:OnToggleClick3(stage)end)
    self._toggleW:SetOnValueChangedCB(function(stage) self:OnToggleClick4(stage)end)

    self._btnUpGO = self:FindChildGO("Button_OneResolve")
    self._csBH:AddClick(self._btnUpGO, function() self:RuneResolve() end)
    self._resolveButton = self:GetChildComponent("Button_OneResolve", "ButtonWrapper")

    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'ScrollViewList')
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(RuneResolveListItem)
    self._list:SetPadding(10,0,0,25)
    self._list:SetGap(0,10)
    self._list:SetDirection(UIList.DirectionLeftToRight,2,-1)
    local itemClickedCB = 
	function(index)
		self:OnItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)  
    self._textReNumGo.text = 'x'..RuneData:GetRuneEliteNum()
    self._textResolve.text = LanguageDataHelper.GetContent(25237)
    self._textElite.text = LanguageDataHelper.GetContent(25225)
    self._textDes.text = LanguageDataHelper.GetContent(25236)
    self._textGet.text = LanguageDataHelper.GetContent(25235)

    self._toggleOr:SetIsOn(false)
    self._toggleZi:SetIsOn(false)
    self._toggleB:SetIsOn(false)        
    self._toggleW:SetIsOn(false)     
    
    self.fxtarget = UIParticle.AddParticle(self.gameObject,"Container_Fx/fx_ui_30012")
    self.fxtarget:Play(true,true)

    self._FxObjectSrcGo = self:FindChildGO("Container_FxObjectsSrc")

    self._FxObjectFxGo = self:FindChildGO("Container_FxObjectsSrc/Container_Fx")

    self._FxObjectDesGo = self:FindChildGO("Container_FxObjectsDes")
    self.listcontentTran = self._list.gameObject.transform 
    self._scrollview:SetScrollRectState(false)

    
end

function RuneResolveView:RuneEssenceUpdate()
    self._textReNumGo.text = 'x'..RuneData:GetRuneEliteNum()
end
function RuneResolveView:OnItemClicked(index)
    self._list:GetItem(index):ShowSele()
    self:UpdateReGet()
end


function RuneResolveView:OnToggleClick1(stage)
    self:listItemSele(5,stage)      
end

function RuneResolveView:OnToggleClick2(stage)
    self:listItemSele(4,stage)
end

function RuneResolveView:OnToggleClick3(stage)
    self:listItemSele(3,stage) 
end

function RuneResolveView:OnToggleClick4(stage)
    self:listItemSele(1,stage)
end

function RuneResolveView:listItemSele(quality,stage)
    local l = self._list:GetLength()-1
    for i=0,l do
        local itemdata = self._list:GetDataByIndex(i)
        local q = itemdata:GetQuality()
        if q == quality then
            self._list:GetItem(i):QuickSele(stage)
        end
    end      
    self:UpdateReGet()   
end

function RuneResolveView:listItemSeleBgType(type,stage)
    local l = self._list:GetLength()-1
    for i=0,l do
        local itemdata = self._list:GetDataByIndex(i)
        local q = itemdata:GetType()
        if q[1] == type then
            self._list:GetItem(i):QuickSele(stage)
        end
    end      
    self:UpdateReGet()   
end

function RuneResolveView:UpdateReGet()
    local d,num = self:GetSeleRune()
    self._textNumGo.text = 'x'..num
    local iconid = RuneData:GetRuneEliteIcon()
    GameWorld.AddIcon(self._getRuneiconGo,iconid)
    GameWorld.AddIcon(self._runeEliteGo,iconid)
end  

function RuneResolveView:GetSeleRune()
    local d = {}
    local num = 0
    local l = self._list:GetLength()-1
    for i=0,l do
        local item = self._list:GetItem(i)
        local stage = item:GetStage()
        if stage==true then
            local itemdata = self._list:GetDataByIndex(i)
            local resolvenum = itemdata:GetResolve()[GlobalParamsHelper.GetParamValue(484)]
            table.insert(d,itemdata._pkgPos)
            num = num + resolvenum
        end
    end
    return d,num 
end

function RuneResolveView:SetFxRune()
    local l = self._list:GetLength()-1
    for i=0,l do
        local item = self._list:GetItem(i)
        local stage = item:GetStage()
        if stage==true then
            local pos=Vector2(0,0)
            local name = item:GetItemName()
            local itemPos = item:GetSelfTrans().localPosition
            local itemPosX=itemPos.x+self._offSetX
            local maskPosY=self.listcontentTran.localPosition.y-440
            local itemPosY=itemPos.y+self._offSetY+maskPosY
            local k = 0
            if itemPosY>=self._downlimitY and itemPosY<=self._uplimitY then--在可视范围内
                pos = Vector2(itemPosX,itemPosY)
            else
                if itemPosY>self._uplimitY then
                    if itemPosX<=self._leftRight then
                        pos = self._Fxposition[1]
                        k=1
                    else
                        pos = self._Fxposition[2]  
                        k=2
                    end
                elseif itemPosY<self._downlimitY then
                    if itemPosX<=self._leftRight then
                        pos = self._Fxposition[3]
                        k=3
                    else
                        pos = self._Fxposition[4]    
                        k=4
                    end
                end
            end
            local item
            if k~=0 then
                if not self._posFIndexs[k] then
                    self._posFIndexs[k]=k
                    item = self:AddFx(name,pos)
                end
            else
                item = self:AddFx(name,pos)
            end
            if item then
                item:Start()
            end
        end
    end
end

-- 特效对象池
function RuneResolveView:CreateFxItem()
    local itemPool = self._fxItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_FxObjectsSrc/Container_Fx",'Container_FxObjectsDes')
    local item = RuneResovleFxItem()
    item:SetComponent(go)
    return item
end

function RuneResolveView:ReleaseFxItem(item)
    local itemPool = self._fxItemPool
    table.insert(itemPool, item)
end

function RuneResolveView:AddFx(name,position)
    local item = self:CreateFxItem()
    self._fxItems[name] = item
    item:InitView()
    item:SetPosition(position)
    return item
end

-- 分解
function RuneResolveView:RuneResolve()
    self._posFIndexs = {}
    self:SetFxRune()
    local l = self._list:GetLength()
    if l>0 then
        for i=0,l-1 do
            local item = self._list:GetItem(i)
            local stage = item:GetStage()
            if stage==true then
                local itemdata = self._list:GetDataByIndex(i)    
                item:ResetRuneResolve()
                self._countRes = self._countRes+1
                RuneManager.RuneExplodeRpc(itemdata._pkgPos)
                RuneManager:IsResolveNow(true)
            end
        end
    else
        RuneManager:IsResolveNow(false)
    end
    RuneManager:RefrenRuneEssence()
	GameWorld.PlaySound(26)		
    self._timerid = TimerHeap:AddSecTimer(1.8,0,0,self._tweenOk)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_rune_resolve_button")
end

function RuneResolveView:TweenOK()
    TimerHeap:DelTimer(self._timerid)
    self._timerid=-1
    for k,v in pairs(self._fxItems) do
        v:StopFx()
        self:ReleaseFxItem(v)
        self._fxItems[k]=nil
    end
end

function RuneResolveView:OnDestroy()
    self._csBH = nil
    for k,v in pairs(self._fxItems) do
        v:DestoryFx()
        self._fxItems[k]=nil
    end
    self._posFIndexs = {}
end


function RuneResolveView:RunePkgUpdate()
    if self._countRes==0 then
        self:RuneUpdata()
    end
end

function RuneResolveView:RuneResolveUpdate(data)
    if data==0 then
        self._countResAcc=self._countResAcc+1
        if self._countRes==self._countResAcc then
            self._countResAcc=0
            self._countRes=0          
            self:RuneUpdata()
            RuneManager:IsResolveNow(false)
            RuneManager:RuneEssenceUpdate()
            RuneManager:RefreshRuneResolveRedEvent()
        end
    else
        self._countResAcc=0
        self._countRes=0
        self:RuneUpdata()
        RuneManager:IsResolveNow(false)
        RuneManager:RuneEssenceUpdate()
        RuneManager:RefreshRuneResolveRedEvent()
    end
end

function RuneResolveView:Close()
    self._scrollview:SetScrollRectState(false)
    self:Clear()
    if self._timerid~=-1 then
        TimerHeap:DelTimer(self._timerid)
        self._timerid=-1
    end
    self:GetSeleRune()
    for k,v in pairs(self._fxItems) do
        v:StopFx()
        self:ReleaseFxItem(v)
        self._fxItems[k]=nil
    end
    self._posFIndexs = {}
    

end

function RuneResolveView:Open()
    self._scrollview:SetScrollRectState(true)
    self._countResAcc=0
    self._countRes=0
    self:RuneEssenceUpdate()
    self:RuneUpdata()
     
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RUNE_ESSENCE, self._essenceupdate)   
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._pkgupdate)   
    EventDispatcher:AddEventListener(GameEvents.RuneExploadeRespUpdate,self._resolveupdate)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleRune.ChildView.RuneResolveView")
end

function RuneResolveView:RuneUpdata()
    local reinfo = RuneData:GetRuneResolve()
    --self._list:SetDataList(reinfo)
    self._resolveButton.interactable = false
    self._list:SetOnAllItemCreatedCB(function() self:OnContentListAllItemCreated() end)
    self._list:SetDataList(reinfo, 4)--使用协程加载
end

--全部list加载完回调
function RuneResolveView:OnContentListAllItemCreated()
    self._resolveButton.interactable = true
    self:UpdateReGet()
    self:listItemSeleBgType(91,true)
    if RuneData:IsHaveQuality(3) then
        self._toggleB:SetIsOn(true)     
    else
        self._toggleB:SetIsOn(false)        
    end       
    if RuneData:IsHaveQuality(1) then
        self._toggleW:SetIsOn(true)
    else
        self._toggleW:SetIsOn(false)        
    end  
end

function RuneResolveView:Clear()
    RuneManager:IsResolveNow(false)
    self._toggleOr:SetIsOn(false)
    self._toggleZi:SetIsOn(false)
    self._toggleB:SetIsOn(false)
    self._toggleW:SetIsOn(false)
    local l = self._list:GetLength()-1
    for i=0,l do
        local itemdata = self._list:GetDataByIndex(i)
        self._list:GetItem(i):QuickSele(false)
    end 
    EventDispatcher:RemoveEventListener(GameEvents.PROPERTY_RUNE_ESSENCE,self._essenceupdate)
    EventDispatcher:RemoveEventListener(GameEvents.RuneExploadeRespUpdate,self._resolveupdate)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._pkgupdate)      
end