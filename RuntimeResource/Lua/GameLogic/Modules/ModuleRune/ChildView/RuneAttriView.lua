-- RuneAttriView.lua
local RuneAttriView = Class.RuneAttriView(ClassTypes.BaseComponent)
require "Modules.ModuleRune.ChildComponent.RuneAttListItem"
RuneAttriView.interface = GameConfig.ComponentsConfig.Component
RuneAttriView.classPath = "Modules.ModuleRune.ChildView.RuneAttriView"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local RuneData = PlayerManager.PlayerDataManager.runeData
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local RuneAttListItem = ClassTypes.RuneAttListItem
local RuneManager = PlayerManager.RuneManager

function RuneAttriView:Awake()
    self._csBH = self:GetComponent("LuaUIComponent")
    self:InitView()
end

function RuneAttriView:InitView()
    local srollview,list = UIList.AddScrollViewList(self.gameObject,'ScrollViewList_Att')
    self._list = list
    self._scrollView = srollview
    self._list:SetItemType(RuneAttListItem)
    self._list:SetPadding(0,0,0,55)
    self._list:SetGap(0,20)
    self._list:SetDirection(UIList.DirectionTopToDown,1,-1)
    local itemClickedCB = 
	function(idx)
		self:OnItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
    self:SetScroEnable(false)

    self._btnclose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnclose,function() self:ClickClose() end)

end

function RuneAttriView:OnItemClicked()

end

function RuneAttriView:SetScroEnable(state)
    if self._scrollView then
        self._scrollView:SetScrollRectState(state)
    end
end

function RuneAttriView:ClickClose()
    self.gameObject:SetActive(false)
    EventDispatcher:TriggerEvent(GameEvents.RuneFx,true)
end

function RuneAttriView:OnDestroy() 
    self:SetScroEnable(false)
end

function RuneAttriView:OnEnable() 
    self:SetScroEnable(true)    
end

function RuneAttriView:OnDestroy()
    self._csBH = nil    
end


function RuneAttriView:Update()
    local listdata = {}
    local atts = RuneData:GetAllRuneDes() 
    local des = RuneManager:GetRuneAtt(atts)
    local j = 1
    for i,v in pairs(des) do
        local content = v[0]
        local num = v[1]
        local type = v[2]
        local d = {}
        d[1] = content
        if type==1 then
            d[2] = num
        else
            d[2] = string.format( "%.2f",num/100).."%"
        end
        table.insert(listdata,d)
    end
    self._list:SetDataList(listdata)
end    



