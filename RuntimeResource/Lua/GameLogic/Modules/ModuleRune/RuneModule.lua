RuneModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function RuneModule.Init()
    GUIManager.AddPanel(PanelsConfig.Rune,"Panel_Rune",GUILayer.LayerUIPanel,"Modules.ModuleRune.RunePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.RuneOpenSlot,"Panel_RuneOpenSlot",GUILayer.LayerUIFloat,"Modules.ModuleRune.RuneOpenSlotPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return RuneModule