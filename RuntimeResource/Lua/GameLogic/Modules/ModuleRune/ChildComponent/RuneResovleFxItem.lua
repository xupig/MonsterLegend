local RuneResovleFxItem = Class.RuneResovleFxItem(ClassTypes.XObject)
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local UIComponentUtil = GameUtil.UIComponentUtil
function RuneResovleFxItem:__ctor__()

end
function RuneResovleFxItem:InitView()
    self:StopFx()
end

function RuneResovleFxItem:SetPosition(pos)
    self._go.transform.localPosition=pos
end 

function RuneResovleFxItem:SetComponent(go)
    self._go = go
    self._Fx = UIComponentUtil.AddLuaUIComponent(go, UIParticle)
    self._An = go:AddComponent(typeof(XGameObjectTweenPosition))
end 

function RuneResovleFxItem:Start()
    self:StartFx()
    self:StartMove()
end

function RuneResovleFxItem:StartMove()
    self._An.IsTweening = false
    self._An:SetToPosOnce(Vector3(165,311,0), 0.5, nil)
end

function RuneResovleFxItem:StartFx(pos)
    self._Fx:Play(true,true)  
end

function RuneResovleFxItem:StopFx()
    self._Fx:Stop()
end

function RuneResovleFxItem:DestoryFx()
    if self._go then
        GameWorld.Destroy(self._go)
    end
end