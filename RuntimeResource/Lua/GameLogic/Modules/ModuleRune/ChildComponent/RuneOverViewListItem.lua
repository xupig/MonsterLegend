require "UIComponent.Extend.ItemGrid"

local RuneOverViewListItem = Class.RuneOverViewListItem(ClassTypes.UIListItem)
RuneOverViewListItem.interface = GameConfig.ComponentsConfig.Component
RuneOverViewListItem.classPath = "Modules.ModuleRune.ChildComponent.RuneOverViewListItem"

local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil

function RuneOverViewListItem:Awake()
    self._base.Awake(self)
    self._colors = {};    
    self._colors[1] = ColorConfig.A
    self._colors[3] = ColorConfig.K
    self._colors[4] = ColorConfig.L
    self._colors[5] = ColorConfig.F
    self._colors[6] = ColorConfig.H
    self:InitView()
end

function RuneOverViewListItem:InitView()
    local parent= self:FindChildGO("Container_Icon")
    local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._icon:ActivateTipsById()
    self._runename = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    
end


function RuneOverViewListItem:OnRefreshData(data)
    self._icon:SetItem(data['runeid'])
     local name = LanguageDataHelper.GetContent(data['name']).."Lv."..data['level']
     self._runename.text=BaseUtil.GetColorString(name,self._colors[data["quality"]])
end