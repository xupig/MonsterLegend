local RuneAttListItem = Class.RuneAttListItem(ClassTypes.UIListItem)

RuneAttListItem.interface = GameConfig.ComponentsConfig.Component
RuneAttListItem.classPath = "Modules.ModuleRune.ChildComponent.RuneAttListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager


function RuneAttListItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function RuneAttListItem:InitView()
    self._runeAtt = self:GetChildComponent("Text_Att", "TextMeshWrapper")
    self._runeV = self:GetChildComponent("Text_Value", "TextMeshWrapper")
end

function RuneAttListItem:OnDestroy() 
    self.data = nil
end

function RuneAttListItem:OnRefreshData(data)
    self.data = data
    self:Update(data)
end

function RuneAttListItem:Update(data)
    if self._runeAtt ~= nil and self._runeV ~= nil then
        self._runeAtt.text = data[1]
        self._runeV.text = data[2]
    end
end