local RuneEliteItem = Class.RuneEliteItem(ClassTypes.BaseLuaUIComponent)

RuneEliteItem.interface = GameConfig.ComponentsConfig.Component
RuneEliteItem.classPath = "Modules.ModuleRune.ChildComponent.RuneEliteItem"

-- local LanguageDataHelper = GameDataHelper.LanguageDataHelper
-- local GUIManager = GameManager.GUIManager
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil

function RuneEliteItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function RuneEliteItem:InitView()
    self._runeEliteIcon = self:FindChildGO("Image_RuneEliteIcon")
    self._runele = self:GetChildComponent("Text_RuneEliNum", "TextMeshWrapper")
end

function RuneEliteItem:SetRuneElite(iconid)
    if self._runeEliteIcon ~= nil then
        GameWorld.AddIcon(self._runeEliteIcon,iconid)
    end
end 

-- type:0:够 1：不够
function RuneEliteItem:SetEliteNum(num,type)
    if type == 0 then
        local msg =  BaseUtil.GetColorString(num,ColorConfig.J)
        self._runele.text = msg        
    else
        local msg =  BaseUtil.GetColorString(num,ColorConfig.H)        
        self._runele.text = msg                
    end
end 

function RuneEliteItem:Update(data)
    self._runele.text = data[3]
    self:SetRuneElite(data[2]) 
end