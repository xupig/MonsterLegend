local RuneAttHoleItem = Class.RuneAttHoleItem(ClassTypes.BaseComponent)
RuneAttHoleItem.interface = GameConfig.ComponentsConfig.PointableComponent
RuneAttHoleItem.classPath = "Modules.ModuleRune.ChildComponent.RuneAttHoleItem"
local GUIManager = GameManager.GUIManager
local RuneData = PlayerManager.PlayerDataManager.runeData
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local RuneManager = PlayerManager.RuneManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemConfig = GameConfig.ItemConfig
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local UIParticle = ClassTypes.UIParticle

function RuneAttHoleItem:Awake()
    self:InitView()
    self.itemdata = {}
    self._colors = {}
    self._colors[1] = ColorConfig.A
    self._colors[3] = ColorConfig.K
    self._colors[4] = ColorConfig.L
    self._colors[5] = ColorConfig.F
    self._colors[6] = ColorConfig.H
    -- self.fxtarget = UIParticle.AddParticle(self.gameObject,"Container_Fx/fx_ui_30006")
    -- self.fxtarget:Stop()
end

function RuneAttHoleItem:InitView()
    self._containerLock = self:FindChildGO("Container_Lock")
    self._containerEmpty = self:FindChildGO("Container_Empty")
    self._containHave = self:FindChildGO("Container_Have")    
    self._seleimg = self:FindChildGO("Image_Selected") 
    self._red = self:FindChildGO('Image_Red')   
    self._runele = self:GetChildComponent("Container_Have/Text_RuneName", "TextMeshWrapper")
    self._runeIcon = self:FindChildGO("Container_Have/Image_RuneIcon")       
    self._runeBg = self:FindChildGO("Container_Have/Image_RuneBg")       
end

function RuneAttHoleItem:Selected(state)
    if self._seleimg ~= nil then
        self._seleimg:SetActive(state)
    end
end

function RuneAttHoleItem:SetRuneIcon(itemId)
    local itemCfg = ItemDataHelper.GetItem(itemId) 
    GameWorld.AddIcon(self._runeIcon, tonumber(itemCfg.icon))
    GameWorld.AddIcon(self._runeBg, ItemConfig.RuneBgMap[itemCfg.quality])
end
function RuneAttHoleItem:OnPointerDown()
    if not self.itemdata._have and RuneData:IsLayerLock(self.itemdata.slot) then
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_rune_hole_button")
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SELECT_RUNE_HOLE,self.itemdata)
end

function RuneAttHoleItem:OnPointerUp()
	
end

function RuneAttHoleItem:Update(data)
    self.itemdata = data
    if not data._have then
        if RuneData:IsLayerLock(data.slot) then
            self._containerEmpty:SetActive(false)
            self._containerLock:SetActive(true)
            self._containHave:SetActive(false)
            self._red:SetActive(false)
            -- self.fxtarget:Stop()
        else
            -- self.fxtarget:Play(true,true)
            self._containerEmpty:SetActive(true)
            self._containerLock:SetActive(false)
            self._containHave:SetActive(false)
            local  item = data:GetRuneItem()  
            self._red:SetActive(RuneData:GetRepRed() or RuneData:IsSameRuneAndUp(item._runeid,item._level))  
        end
    else
        -- self.fxtarget:Play(true,true)
        self._containerEmpty:SetActive(false)
        self._containerLock:SetActive(false)
        self._containHave:SetActive(true)     
        local  item = data:GetRuneItem()  
        self:SetRuneIcon(item:GetItemComid())
        self._runele.text = BaseUtil.GetColorString(item:GetName()..'Lv.'..item._level,self._colors[item:GetQuality()])
        local f = item:IsRed() or RuneData:GetRepRed() or RuneData:IsSameRuneAndUp(item._runeid,item._level)
        self._red:SetActive(f)
    end
end    