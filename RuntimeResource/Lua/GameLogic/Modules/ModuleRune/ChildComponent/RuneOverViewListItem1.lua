require "UIComponent.Extend.ItemGrid"
require "Modules.ModuleRune.ChildComponent.RuneOverViewListItem"

local RuneOverViewListItem1 = Class.RuneOverViewListItem1(ClassTypes.UIListItem)
RuneOverViewListItem1.interface = GameConfig.ComponentsConfig.Component
RuneOverViewListItem1.classPath = "Modules.ModuleRune.ChildComponent.RuneOverViewListItem1"

local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local RuneOverViewListItem = ClassTypes.RuneOverViewListItem
local UIList = ClassTypes.UIList

function RuneOverViewListItem1:Awake()
    self._base.Awake(self)
    self:InitView()
end

function RuneOverViewListItem1:InitView()
    self._rectTransform = self:GetComponent(typeof(UnityEngine.RectTransform))
    self._text = self:GetChildComponent("Text_Content","TextMeshWrapper")
    local srollview,list = UIList.AddScrollViewList(self.gameObject,'ScrollViewList_Runes')
    self._list = list
    self._scrollView = srollview
    self._list:SetItemType(RuneOverViewListItem)
    self._list:SetPadding(0,0,0,50)
    self._list:SetGap(0,60)
    self._list:SetDirection(UIList.DirectionLeftToRight,4,-1)
    self._scrollView:SetHorizontalMove(false)
    self._scrollView:SetVerticalMove(false)
    self._scrollView:SetScrollRectEnable(false)
    -- local itemClickedCB = 
	-- function(idx)
	-- 	self:OnItemClicked(idx)
	-- end
    -- self._list:SetItemClickedCB(itemClickedCB)
end


function RuneOverViewListItem1:OnRefreshData(data)
    if data[1]==1 then
        self._text.text = LanguageDataHelper.GetContent(25284)
    elseif data[1]==2 or data[1]==3 then
        local arg = LanguageDataHelper.GetArgsTable()
        arg['0'] = data[3]
        self._text.text = LanguageDataHelper.CreateContent(25283,arg)
    else

    end
    local length = #data[2]
    local f0 = length/4
    local f1 = length%4
    if f1>0 then
        f0=f0+1
    end
    local height = f0*103+60
    self.transform.localPosition.y=-height
    local size = self._rectTransform.sizeDelta
    size.y = height
    self._rectTransform.sizeDelta = size
    self._list:SetDataList(data[2])


end