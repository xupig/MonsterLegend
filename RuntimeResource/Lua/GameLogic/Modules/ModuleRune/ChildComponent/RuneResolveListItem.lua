require "UIComponent.Extend.ItemGrid"
local RuneResolveListItem = Class.RuneResolveListItem(ClassTypes.UIListItem)
RuneResolveListItem.interface = GameConfig.ComponentsConfig.Component
RuneResolveListItem.classPath = "Modules.ModuleRune.ChildComponent.RuneResolveListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemGrid = ClassTypes.ItemGrid
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local RuneManager = PlayerManager.RuneManager
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local RuneData = PlayerManager.PlayerDataManager.runeData


function RuneResolveListItem:Awake()
    self._atts = {};
    self._attNums = {};
    self._attgos = {};
    self._colors = {};
    self._attNumgos = {};   
    self._first=true
    self.prestage = false
    self:InitView()
end

function RuneResolveListItem:InitView()
    self._runeSeleIcon = self:FindChildGO("Container_content/Image_IconSele")
    self._runeSele = self:FindChildGO("Container_content/Image_Selected")
    self._runeName = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._runeTipsGo = self:FindChildGO("Container_content/Text_Tips") 
    self._runeSele:SetActive(false)
    self._runeSeleIcon:SetActive(false)
    local parent = self:FindChildGO("Container_content/Container_Icon")
    self._runeAttGo0 = self:FindChildGO("Container_content/Container_Att0/Text_Att0")
    self._runeAttGo1 = self:FindChildGO("Container_content/Container_Att1/Text_Att1")
    self._runeTips = self:GetChildComponent("Container_content/Text_Tips", "TextMeshWrapper")    
    self._runeAtt0 = self:GetChildComponent("Container_content/Container_Att0/Text_Att0", "TextMeshWrapper")
    self._runeAtt1 = self:GetChildComponent("Container_content/Container_Att1/Text_Att1", "TextMeshWrapper")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._atts[0] = self._runeAtt0
    self._atts[1] = self._runeAtt1
    self._attgos[0] = self._runeAttGo0
    self._attgos[1] = self._runeAttGo1
    self._colors[1] = ColorConfig.A
    self._colors[3] = ColorConfig.K
    self._colors[4] = ColorConfig.L
    self._colors[5] = ColorConfig.F
    self._colors[6] = ColorConfig.H

    self._containerAnim = self:FindChildGO("Container_content/Container_Fx")
    self._containerAnimTrans = self._containerAnim.transform
    self._itemTrans = self.gameObject.transform
end

function RuneResolveListItem:OnDestroy() 
    self.Itemdata = nil
end

function RuneResolveListItem:OnRefreshData(data)
    self.Itemdata = data
    self:Update(data)    
end

function RuneResolveListItem:Update(data)
    local att = RuneManager:GetRuneAtt(data:GetAtt())
    local text = ''
    self._runeAttGo0:SetActive(false)
    self._runeAttGo1:SetActive(false)
    self._runeTipsGo:SetActive(false)    
    local type = 0
    local runetypes = data:GetType()
    if att ~= nil and #att>0 then
        if runetypes[1]==5 or runetypes[2]==5 then
            type=3
        else
            type=0            
        end
    else
        if runetypes[1]==5 or runetypes[2]==5 then
            type=1          
        else
            type=2       
        end
    end
    local i = 0    
    if type==0 then
        for k,v in pairs(att) do 
            local content = v[0]
            local num = v[1]
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable['0'] = content
            argsTable['1'] = num
            self._attgos[i]:SetActive(true)
            if v[2] == 1 then
                self._atts[i].text = content.."  "..BaseUtil.GetColorString('+'..num,ColorConfig.J)
            else
                self._atts[i].text = content.."  "..BaseUtil.GetColorString('+'..(num/100-num/100%0.01).."%",ColorConfig.J)
            end
            i = i+1                            
        end            
    elseif type==1 then
        local text = ''
        local exp = data:GetExp()
        text = '+'..(exp/100-exp/100%0.01).."%"
        local msg  = BaseUtil.GetColorString(LanguageDataHelper.GetContent(1019)..' ',ColorConfig.B)
        self._runeTipsGo:SetActive(true)       
        self._runeTips.text = msg..BaseUtil.GetColorString(text,ColorConfig.J)
    elseif type==2 then
        local text = ''        
        local resolve = data:GetResolve()
        for k,v in pairs(resolve) do 
            local id = tonumber(k)
            text = ItemDataHelper.GetItemName(id)
            text = text..''..v
        end
        text = '+'..text
        local msg  = BaseUtil.GetColorString(LanguageDataHelper.GetContent(229)..' ',ColorConfig.B)
        self._runeTipsGo:SetActive(true)       
        self._runeTips.text = msg..BaseUtil.GetColorString(text,ColorConfig.J)
    else
        for k,v in pairs(att) do 
            local content = v[0]
            local num = v[1]
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable['0'] = content
            argsTable['1'] = num
            self._attgos[0]:SetActive(true)
            self._attgos[1]:SetActive(true)
            if v[2] == 1 then
                self._atts[0].text = content.."  "..BaseUtil.GetColorString('+'..num,ColorConfig.J)
            else
                self._atts[0].text = content.."  "..BaseUtil.GetColorString('+'..(num/100-num/100%0.01)..'%',ColorConfig.J)
            end
            break                    
        end 
        local exp = data:GetExp()
        local text = ''
        text = '+'..(exp/100-exp/100%0.01).."%"
        local msg  = BaseUtil.GetColorString(LanguageDataHelper.GetContent(1019)..' ',ColorConfig.B)     
        self._atts[1].text = msg.."  "..BaseUtil.GetColorString(text,ColorConfig.J)                    
    end
    self._runeName.text = data:GetName()..BaseUtil.GetColorString("Lv."..data._level,self._colors[data:GetQuality()])
    self._icon:SetItem(data:GetItemComid())   
    if self._first then
        local q = data:GetQuality()
        local type = data:GetType()[1]
        if q==1 or q==3 or type== 91 then
            self:QuickSele(true)
        else
            self:QuickSele(false)
        end
    else
        self:QuickSele(self.prestage)
    end
end

function RuneResolveListItem:GetSelfTrans()
    return self._itemTrans
end
function RuneResolveListItem:ShowSele()
    self.prestage = not self.prestage
    self:SeleState(self.prestage)
end

function RuneResolveListItem:QuickSele(stage)
    self.prestage = stage
    self._first=false
    self:SeleState(stage)
end

function RuneResolveListItem:SeleState(stage)
    self._runeSele:SetActive(stage)
    self._runeSeleIcon:SetActive(stage)
end

function RuneResolveListItem:GetStage()
    return self.prestage
end


function RuneResolveListItem:GetFxTran()
    return self._containerAnimTrans
end

function RuneResolveListItem:GetItemName()
    return self.gameObject.name
end

function RuneResolveListItem:ResetRuneResolve()
    self:QuickSele(false)
end

function RuneResolveListItem:OnDestroy()
    
end