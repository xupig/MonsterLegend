local RuneExchangeListItem = Class.RuneExchangeListItem(ClassTypes.UIListItem)

RuneExchangeListItem.interface = GameConfig.ComponentsConfig.Component
RuneExchangeListItem.classPath = "Modules.ModuleRune.ChildComponent.RuneExchangeListItem"
require "UIComponent.Extend.ItemGrid"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local RuneManager = PlayerManager.RuneManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemGrid = ClassTypes.ItemGrid
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local AionTowerManager = PlayerManager.AionTowerManager
local public_config = GameWorld.public_config

function RuneExchangeListItem:Awake()
    self.Itemdata = {}
    self._atts = {};
    self._attNums = {};
    self._attgos = {};
    self._colors = {};
    self._attNumgos = {};    
    self:InitView()
end

function RuneExchangeListItem:InitView()
    self._runeMaterilIcon = self:FindChildGO("Container_content/Container_Ex/Image_MaterilIcon")
    self._runeAttGo0 = self:FindChildGO("Container_content/Container_Att0/Text_Att0")
    self._runeAttGo1 = self:FindChildGO("Container_content/Container_Att1/Text_Att1")
    self._btnexchangeGo = self:FindChildGO("Container_content/Container_Ex/Button_Exchange") 
    self._runeTipsGo = self:FindChildGO("Container_content/Text_Tips") 
    self._runeName = self:GetChildComponent("Container_content/Text_Title", "TextMeshWrapper")
    self._runeTips = self:GetChildComponent("Container_content/Text_Tips", "TextMeshWrapper")
    self._runeAtt0 = self:GetChildComponent("Container_content/Container_Att0/Text_Att0", "TextMeshWrapper")
    self._runeAtt1 = self:GetChildComponent("Container_content/Container_Att1/Text_Att1", "TextMeshWrapper")
    self._runeNum = self:GetChildComponent("Container_content/Container_Ex/Text_Num", "TextMeshWrapper")
    self._btnexchange = self:GetChildComponent("Container_content/Container_Ex/Button_Exchange","ButtonWrapper")
    self._csBH:AddClick(self._btnexchangeGo,function() self:btnClick() end)

    local parent = self:FindChildGO("Container_content/Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._atts[0] = self._runeAtt0
    self._atts[1] = self._runeAtt1
    self._attgos[0] = self._runeAttGo0
    self._attgos[1] = self._runeAttGo1
    self._colors[1] = ColorConfig.A
    self._colors[3] = ColorConfig.K
    self._colors[4] = ColorConfig.L
    self._colors[5] = ColorConfig.F
    self._colors[6] = ColorConfig.H
end

function RuneExchangeListItem:btnClick()
    local d = self.Itemdata
    if d ~= nil then
        local id = d._id
        RuneManager.RuneExchangeRpc(id)        
    end
end

function RuneExchangeListItem:OnDestroy() 
    self.Itemdata = nil
end

function RuneExchangeListItem:OnRefreshData(data)
    self.Itemdata = data
    self:Update(data)   
end

function RuneExchangeListItem:Update(data)
    local entity = GameWorld.Player()
    local piece = entity.rune_piece
    local aionInfo = AionTowerManager:GetAionInfo()
	local curLayer = aionInfo[public_config.AION_NOW_LAYER] or 1 --当前永恒之塔层级	
    local _type 
    local price = data:GetPrice()
    local floor = data:GetFloor()
    if curLayer>=floor then
        if piece>=price then
            _type=1
        else
            _type=3
        end
    else
        _type=2
    end
    local att = data:GetAtt()
    local nameid = data:GetName()
    local priceid = data:GetPieceIcon()
    local quality = data:GetRuneQuality()
    local _name = LanguageDataHelper.GetContent(nameid).."Lv."..data:GetLevel()
    self._runeName.text = BaseUtil.GetColorString(_name,self._colors[quality])
    self._icon:SetItem(data:GetItemCom())
    GameWorld.AddIcon(self._runeMaterilIcon,priceid)
    self._runeAttGo0:SetActive(false)
    self._runeAttGo1:SetActive(false)
    self._runeTipsGo:SetActive(false)
    if _type == 1 then
        self._btnexchange.interactable = true
    else
        self._btnexchange.interactable = false
    end
    if _type == 1 or _type ==3 then
        local i = 0
        local type
        if att~=nil then
            if data:IsExpRune() then
                type=3
            else
                type=0
            end
        else
            if data:IsExpRune() then
                type=1
            else
                type=2
            end
        end
        if type==0 then   
            local d = RuneManager:GetRuneAtt(att)
            for k,v in pairs(d) do 
                local content = v[0]
                local num = v[1]
                self._attgos[i]:SetActive(true)
                if v[2] == 1 then
                    self._atts[i].text = content.."  "..BaseUtil.GetColorString("+"..num,ColorConfig.J)
                else
                    self._atts[i].text = content.."  "..BaseUtil.GetColorString("+"..(num/100-num/100%0.01).."%",ColorConfig.J)
                end 
                i = i+1
            end
        elseif type==1 then
            local text = ''
            local exp = data:GetExp()
            text = '+'..(exp/100-exp/100%0.01)..'%'
            local msg  = BaseUtil.GetColorString(LanguageDataHelper.GetContent(1019)..' ',ColorConfig.B)
            self._runeTipsGo:SetActive(true)       
            self._runeTips.text = msg..BaseUtil.GetColorString(text,ColorConfig.J)
        elseif type==2 then
            local text = ''
            local d = {}
            local res = data:GetRes()
            for i,v in pairs(res) do
                local id = tonumber(i)
                local num = tonumber(v)
                local _d = {}
                _d[0] = id
                _d[1] = num
                table.insert(d,_d)
            end
            for k,v in pairs(d) do 
                local id = v[0]
                local num = v[1]
                text = ItemDataHelper.GetItemName(id)
                text = text..''..num
            end
            text = '+'..text
            local msg  = BaseUtil.GetColorString(LanguageDataHelper.GetContent(229)..' ',ColorConfig.B)
            self._runeTipsGo:SetActive(true)       
            self._runeTips.text = msg..BaseUtil.GetColorString(text,ColorConfig.J)
        else
            local d = RuneManager:GetRuneAtt(att)
            for k,v in pairs(d) do 
                local content = v[0]
                local num = v[1]
                local argsTable = LanguageDataHelper.GetArgsTable()
                argsTable['0'] = content
                argsTable['1'] = num
                self._attgos[0]:SetActive(true)
                self._attgos[1]:SetActive(true)
                if v[2] == 1 then
                    self._atts[0].text = content.."  "..BaseUtil.GetColorString('+'..num,ColorConfig.J)
                else
                    self._atts[0].text = content.."  "..BaseUtil.GetColorString('+'..(num/100-num/100%0.01)..'%',ColorConfig.J)
                end
                break                    
            end   
            local text = ''
            text = '+'..tostring(data:GetExp()/100-data:GetExp()/100%0.01).."%"
            local msg  = BaseUtil.GetColorString(LanguageDataHelper.GetContent(1019)..' ',ColorConfig.B)     
            self._atts[1].text = msg.."  "..BaseUtil.GetColorString(text,ColorConfig.J)            
        end
        if _type == 1 then
            self._runeNum.text = BaseUtil.GetColorString(price,ColorConfig.J)
        else
            self._runeNum.text = BaseUtil.GetColorString(price,ColorConfig.H)
        end            
        
    else
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable['0'] = floor
        local text = LanguageDataHelper.CreateContent(25240,argsTable)
        self._runeTipsGo:SetActive(true)        
        self._runeTips.text = BaseUtil.GetColorString(text,ColorConfig.H)
        self._runeNum.text = BaseUtil.GetColorString(price,ColorConfig.H)
    end
    
end
