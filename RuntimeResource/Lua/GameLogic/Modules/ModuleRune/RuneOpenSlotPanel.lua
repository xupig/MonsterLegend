local RuneOpenSlotPanel = Class.RuneOpenSlotPanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager
local closeTime = 3
--override
function RuneOpenSlotPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self:InitView()
end

function RuneOpenSlotPanel:InitView()
    local btngo = self:FindChildGO('Button_Rune')
    self._csBH:AddClick(btngo, function() self:OnClick() end)
end

function RuneOpenSlotPanel:OnClick()
    GUIManager.ClosePanel(PanelsConfig.RuneOpenSlot)
    GUIManager.ShowPanel(PanelsConfig.Rune)
end

function RuneOpenSlotPanel:OnShow() 
    self._timer = TimerHeap:AddSecTimer(closeTime, 0, 0, function()      
        GUIManager.ClosePanel(PanelsConfig.RuneOpenSlot)       
    end)  
end

--override
function RuneOpenSlotPanel:OnDestroy()
	self._csBH = nil
end

function RuneOpenSlotPanel:OnClose()
    TimerHeap:DelTimer(self._timer)
end

function RuneOpenSlotPanel:WinBGType()
    return PanelWinBGType.NoBG
end
