require "Modules.ModuleRune.ChildView.RuneResolveView"
require "Modules.ModuleRune.ChildView.RuneUpReView"
require "Modules.ModuleRune.ChildView.RuneExchangeView"

local RunePanel = Class.RunePanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager

local RuneResolveView = ClassTypes.RuneResolveView
local RuneUpReView = ClassTypes.RuneUpReView
local RuneExchangeView = ClassTypes.RuneExchangeView

local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

--override
function RunePanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self._selectTab = -1
    self._views = {}
    self._gos = {}
    self:InitView()
    self:InitTabCB()
    self:InitChildView()
end

function RunePanel:InitView()
    self._tabClickCB = function(index) self:onSeleTab(index) end
    self._update = function() self:RuneUpdate()end
end

function RunePanel:InitTabCB()
    self:SetCloseCallBack(self._closeCB)
    self:SetTabClickCallBack(self._tabClickCB)
end

function RunePanel:InitChildView()
    self._gos[1] = self:FindChildGO("Container_Insert")
    self._gos[2] = self:FindChildGO("Container_Resolve")
    self._gos[3] = self:FindChildGO('Container_Exchange')
    self._views[1] = self:AddChildLuaUIComponent('Container_Insert',RuneUpReView)  
    self._views[2] = self:AddChildLuaUIComponent('Container_Resolve',RuneResolveView)
    self._views[3] = self:AddChildLuaUIComponent('Container_Exchange',RuneExchangeView)
    -- self._views[1].transform.localScale=HideScale
    -- self._views[2].transform.localScale=HideScale
    -- self._views[3].transform.localScale=HideScale
    self._views[3]:AbleScr(false)
    -- for i,v in ipairs(self._gos) do
    --     v:SetActive(false)
    -- end
    self:onSeleTab(1);
end

function RunePanel:OnShow(data)         
    self._iconBg = self:FindChildGO('Image_Bg')
    GameWorld.AddIcon(self._iconBg,3317)
    self:UpdateFunctionOpen()
    self:onSeleTab(1);
    self:OnShowSelectTab(data)
    self:AddEventListeners()  
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleRune.RunePanel")
end


function RunePanel:onSeleTab(index)
    if self._selectTab == index then
        if self._views[index] ~= nil then
            self._views[index]:Open()
            --self._views[index].transform.localScale=ShowScale
        end
        return
    end
    if self._selectTab ~= -1 then
        if self._views[self._selectTab] ~= nil then
            self._views[self._selectTab]:Close()
        end
        self._gos[self._selectTab]:SetActive(false)
        --self._views[self._selectTab].transform.localScale=HideScale
    end
    if not self._gos[index].activeSelf then
        self._gos[index]:SetActive(true)
    end
    self._gos[index]:SetActive(true)
    --self._gos[index].transform.localScale=ShowScale
    if index==2 then
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_rune_resolve_view_button")
    end
    if self._views[index] ~= nil then
        self._views[index]:Open()
    end
    self._selectTab = index
end
--override
function RunePanel:OnDestroy()
	self._csBH = nil
    self:RemoveEventListeners()
end

function RunePanel:OnClose()
    self._iconBg = nil
    self:RemoveEventListeners()
    self._views[self._selectTab]:Close()    
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_rune_close_button")
end

function RunePanel:AddEventListeners()
  
end

function RunePanel:RemoveEventListeners()

end

function RunePanel:RuneUpdate()
    if self._selectTab ~= -1 then
        self._views[self._selectTab]:Update()
    end
end 


function RunePanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--关闭的时候重新开启二级主菜单
function RunePanel:ReopenSubMainPanel()
    return true
end