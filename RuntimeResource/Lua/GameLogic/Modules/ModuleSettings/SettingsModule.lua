-- SettingsModule.lua
SettingsModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function SettingsModule.Init()
    GUIManager.AddPanel(PanelsConfig.Settings, "Panel_Settings", GUILayer.LayerUIMain, "Modules.ModuleSettings.SettingsPanel", true, false)
end

return SettingsModule
