-- SettingsPanel.lua
local SettingsPanel = Class.SettingsPanel(ClassTypes.BasePanel)
local LocalSetting = GameConfig.LocalSetting

function SettingsPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel');

    --EventDispatcher:AddEventListener(GameEvents.UNGINE_UPDATE, SettingsPanel.Update)
    self.buttonOK = self:FindChildGO("Buttons/ButtonOK");
    self.buttonCencel = self:FindChildGO("Buttons/ButtonCencel");
    --self.buttonServer = self:FindChildGO("Inputs/ButtonServer");
    self._csBH:AddClick(self.buttonOK, function() self:OnOKClick() end );
    self._csBH:AddClick(self.buttonCencel , function() self:OnCencelClick() end)
    self:InitLastSetting()
    --self:PositionFrom(self.transform.localPosition+Vector3.New(0,300,0),5):Play(true)
    --self:PositionTo(self.transform.localPosition+Vector3.New(0,300,0),5)
end

function SettingsPanel:OnDestroy() 
    self.buttonOK = nil
    self.buttonCencel = nil
    --self.buttonServer = nil
    self._csBH = nil
end

function SettingsPanel:InitLastSetting()
    
    local userAccount = LocalSetting.settingData.UserAccount
    self._userNameInput.text = tostring(userAccount)
    --LoggerHelper.Log("+++++++++++++++++++", tostring(userAccount))
end

function SettingsPanel:OnOKClick()
    --LoggerHelper.Log("SettingsPanel:OnClick()", self._userNameInput.text)
    --EventDispatcher:TriggerEvent(GameEvents.Login, self._userNameInput.text, 2)
    --GameManager.LoginManager:Login()
end

function SettingsPanel:OnCencelClick()
    --LoggerHelper.Log("SettingsPanel:OnServerClick")
    --GameManager.GUIManager.ShowPanel(PanelsConfig.SelectServer)
end

function SettingsPanel:Update()

end

function SettingsPanel:Timer()
    --LoggerHelper.Log("SettingsPanel:Timer")
end


