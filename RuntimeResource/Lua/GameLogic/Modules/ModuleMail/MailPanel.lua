--MailPanel.lua
require "Modules.ModuleMail.ChildComponent.MailItem"
require "Modules.ModuleMail.ChildComponent.DeleteButton"
require "Modules.ModuleMail.ChildComponent.ReceiveButton"
require "UIComponent.Extend.ItemManager"
require "Modules.ModuleMail.ChildComponent.MailGetListItem"

local ClassTypes = ClassTypes
local MailPanel = Class.MailPanel(ClassTypes.BaseLuaUIComponent)
MailPanel.interface = GameConfig.ComponentsConfig.Component
MailPanel.classPath = "Modules.ModuleMail.MailPanel"

local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local MailItem = ClassTypes.MailItem
local DeleteButton = ClassTypes.DeleteButton
local ReceiveButton = ClassTypes.ReceiveButton
local PanelBGType = GameConfig.PanelsConfig.PanelBGType

local table = table
local action_config = GameWorld.action_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local TOTAL_NUM = 4--附件最大数
local ItemManager = ClassTypes.ItemManager
local DateTimeUtil = GameUtil.DateTimeUtil
local MailDataHelper = GameDataHelper.MailDataHelper
local MailUtil = GameUtil.MailUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local tipsPosition = {x=850, y=510}
local MailManager = PlayerManager.MailManager
local MailConfig = GameConfig.MailConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local MailGetListItem = ClassTypes.MailGetListItem
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig


function MailPanel:Awake()
    self._onReceive = function() self:OnHandleMails() end
    EventDispatcher:AddEventListener(GameEvents.OnReceiveMails, self._onReceive)--接受到所有邮件
    self._onShortClick = function(data) self:OnHandleShortClick(data) end--删除按钮short click
    EventDispatcher:AddEventListener(GameEvents.OnShortClick, self._onShortClick)

    self._onDeleteMail = function(data) self:OnHandleDelete(data) end
    EventDispatcher:AddEventListener(GameEvents.OnDeleteMail, self._onDeleteMail)

    self._onColectMail = function(data,code) self:OnHandleColect(data,code) end
    EventDispatcher:AddEventListener(GameEvents.OnColectMail, self._onColectMail)

    self._onReceNewMail = function(data) self:OnNewMail(data) end
    EventDispatcher:AddEventListener(GameEvents.OnReceNewMail, self._onReceNewMail)


    self._scrollViewFun = function()self:ScrollMoveToEnd()end

    self:InitViews()
    self._count = 0
    self._isAllGet = false
end
--监听收到一封新邮件
function MailPanel:OnNewMail(data)
    if MailManager:GetMailCnt() < MailConfig.MAX_MAILS then--*********
        table.insert(self._listData, 1, data)
        MailManager:SetMailCnt(#self._listData)
        --设置高亮
        for i=1, #self._listData do
            if self._curMail ~= -1 and self._curMail == self._listData[i]:GetDBID() then
                self._listData[i].hightLight = true
            else
                self._listData[i].hightLight = false
            end
        end
        -- MailUtil:SortMail(self._listData)
        self._list:SetDataList(self._listData)
        self:OnCountMailNum(1, self._listData)
        self:ShowByMailNums(#self._listData)
    end
end
--长按删除时，若有一封可以删除返回false，若全部不可删除返回true
function MailPanel:CanDelAllMails()
    local canDel = true
    for k, v in pairs(self._listData) do
        --出现可以删除的邮件
        if v:GetInfoState()==public_config.MAIL_STATE_READ or v:GetInfoState()==public_config.MAIL_STATE_RECE then
            canDel = false
            return
        end
    end
    return canDel
end
-- 批量删
function MailPanel:OnHandleAllDeleteClick()
    if self:CanDelAllMails() then
        GameManager.SystemInfoManager:ShowClientTip(2951)
    else
        MailManager:BatchDeleteMails(self:GetReadMailList(0))
    end
end
-- 批量领
function MailPanel:OnHandleAllGetClick()
    if self._isAllGet then
        return
    end
    local d = self:GetReadMailList(1)
    self._allGetIds = self:SortIds(d)
    local id = table.remove(self._allGetIds)
    if id then
        self._isAllGet=true
        self:GetMailRpc(id)
    else
        self._isAllGet=false
        self._allGetIds={}
    end
end

function MailPanel:SortIds(data)
    local d = {}
    for i=#data,1,-1 do
        table.insert(d,data[i])
    end
    return d
end

--按钮short click事件：0为删除按钮触发、1为领取按钮触发
function MailPanel:OnHandleShortClick(_type)
    if _type[1] == 0 then
        --删除当前邮件
        if self._curMail ~= -1 then
            local status = self._listData[self._curIndex + 1]:GetInfoState()
            if status == public_config.MAIL_STATE_HERE then
                GameManager.SystemInfoManager:ShowClientTip(2952)
                return
            end
            MailManager:DeleteMailByDBId("" .. self._curMail)
        else
            GameManager.SystemInfoManager:ShowClientTip(2953)
        end
    elseif _type[1] == 1 then
        if self._curMail ~= -1 and self._curIndex ~= -1 then
            local status = self._listData[self._curIndex + 1]:GetInfoState()
            if status == public_config.MAIL_STATE_RECE or status == public_config.MAIL_STATE_READ then
                GameManager.SystemInfoManager:ShowClientTip(2954)
                return
            end
            self:GetMailRpc(self._curMail)
        else
            GameManager.SystemInfoManager:ShowClientTip(2956)
        end
    else
        
    end
end

 function MailPanel:GetMailRpc(id)
    MailManager:ColectMailByDBId("" .. id)
 end

--获取已读邮件、已读附件已领取邮件请求列表
function MailPanel:GetReadMailList(_type)
    local readmail = ""
    local data={}
    if self._listData then
        for k, v in pairs(self._listData) do
            if _type == 0 then
                if v:GetInfoState()==public_config.MAIL_STATE_READ or v:GetInfoState()==public_config.MAIL_STATE_RECE then
                    readmail = readmail .. v:GetDBID() .. ","
                end
            elseif _type == 1 then
                if v:GetInfoState()==public_config.MAIL_STATE_HAVE or v:GetInfoState()==public_config.MAIL_STATE_HERE then
                    table.insert(data,v:GetDBID())
                end
            end
        end
    end
    
    if _type == 1 then
        return data 
    elseif _type == 0 then
        readmail = string.sub(readmail, 1 , #readmail-1)
        return readmail
    end
 
end
--删除邮件回调函数
function MailPanel:OnHandleDelete(data)
    if data then
        if MailManager:GetMailCnt() >= MailConfig.MAX_MAILS then
            self:ShowView()
        else
            MailManager:SetMailCnt(MailManager:GetMailCnt() - #data)
            for k, v in pairs(data) do
                local delIndex = -1
                for i, j in pairs(self._listData) do
                    if j:GetDBID() == v then
                        delIndex = i
                        break
                    end
                end
                if delIndex ~= -1 then
                    table.remove(self._listData, delIndex)
                end
            end
                --取消所有高亮
            for i=1, #self._listData do
                self._listData[i].hightLight = false
            end
            -- MailUtil:SortMail(self._listData)
            self._list:SetDataList(self._listData)
            self:Clear()
            self:OnCountMailNum(0, self._listData)   
            if #self._listData > 0 then
                self:OnListItemClicked(0)
            else
                self:RequestForMails()
            end
            self:ShowByMailNums(#self._listData)
            -- self:CheckAllStatus()
        end
    end
end
--领取邮件附件回调函数
function MailPanel:OnHandleColect(data,code)
    if data then
        for i, j in pairs(self._listData) do
            if j:GetDBID() == data[1] then
                j:SetInfoState(data[2])
                self._list:GetItem(i - 1):ChangeMailIcon(data[2])--可去掉
                break
            end
        end
        --设置高亮
        for i=1, #self._listData do
            if self._curMail ~= -1 and self._curMail == self._listData[i]:GetDBID() then
                self._listData[i].hightLight = true
            else
                self._listData[i].hightLight = false
            end
        end
        self._list:SetDataList(self._listData)
        -- self:CheckAllStatus() 
        self._count = self._count + 1
    end
    if code>0 then
        self._isAllGet=false
        self._allGetIds={}
        self:RequestForMails()
    else
        if self._isAllGet then
            local id = table.remove(self._allGetIds)
            if id then
                self:GetMailRpc(id)
            else
                self:RequestForMails()
                self._isAllGet=false
                self._allGetIds={}
            end
        else
            self:RequestForMails()
        end
    end
end

function MailPanel:ScrollMoveToEnd()
    self:MoveEndRequestForMails()
end

function MailPanel:InitViews()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Middle/ScrollViewList_Left")
    self._list = list
    self._scrollView = scrollView
    self._scrollView:SetOnRequestNextCB(self._scrollViewFun)
    self._list:SetItemType(MailItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(5, 5)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, MailConfig.MAX_MAILS)
    local callBack = function(idx) self:OnListItemClicked(idx) end
    self._list:SetItemClickedCB(callBack)
    self:AbleScr(false)

    --middle-right
    self._mailTitle = self:GetChildComponent("Container_Middle/Container_Right/Text_Title", "TextMeshWrapper")
    self._mailTitle1 = self:GetChildComponent("Container_Middle/Container_Right/Text_Title1", "TextMeshWrapper")
    self._mailTime = self:GetChildComponent("Container_Middle/Container_Right/Text_Time", "TextMeshWrapper")
    self._mailContent = self:GetChildComponent("Container_Middle/Container_Right/Container_Content/Mask/Text", "TextMeshWrapper")
    self._additions = self:FindChildGO("Container_Middle/Container_Right/Container_Award")
    
    self._mailContentScrollRect = self:GetChildComponent("Container_Middle/Container_Right/Container_Content", "ScrollRect")
    self._mailMaskRect = self:GetChildComponent("Container_Middle/Container_Right/Container_Content/Mask", "RectTransform")

    self._bg0 = self:FindChildGO("Container_Middle/Container_BG/Container_None")
    self._bg1 = self:FindChildGO("Container_Middle/Container_Right")


    self._mailTitle1.text=LanguageDataHelper.GetContent(527)

    local scrollView,list = UIList.AddScrollViewList(self.gameObject,'Container_Middle/Container_Right/ScrollViewList_Get')
    self._listGet = list
    self._listGet:SetItemType(MailGetListItem)
    self._listGet:SetPadding(0, 0, 0, 0)
    self._listGet:SetGap(5, 5)
    self._listGet:SetDirection(UIList.DirectionLeftToRight, 6, -1)

    local btnAllDete = self:FindChildGO("Container_Bottom/Button_AllDete")
    local btnAllGet = self:FindChildGO("Container_Bottom/Button_AllGet")
    local btnDete = self:FindChildGO("Container_Bottom/Button_Dete")
    self.btnGet = self:FindChildGO("Container_Bottom/Button_Get")

    local btnAllDeteText = self:GetChildComponent("Container_Bottom/Button_AllDete/Text","TextMeshWrapper")
    local btnDeteText = self:GetChildComponent("Container_Bottom/Button_Dete/Text","TextMeshWrapper")
    local btnAllGetText = self:GetChildComponent("Container_Bottom/Button_AllGet/Text","TextMeshWrapper")
    local btnGetText = self:GetChildComponent("Container_Bottom/Button_Get/Text","TextMeshWrapper")

    btnAllDeteText.text = LanguageDataHelper.GetContent(530)
    btnDeteText.text = LanguageDataHelper.GetContent(528)
    btnAllGetText.text = LanguageDataHelper.GetContent(529)
    btnGetText.text = LanguageDataHelper.GetContent(123)

    self._csBH:AddClick(btnAllGet,function() self:OnHandleAllGetClick()end)
    self._csBH:AddClick(btnAllDete,function() self:OnHandleAllDeleteClick()end)
    self._csBH:AddClick(btnDete,function()self:OnHandleShortClick({0})end)
    self._csBH:AddClick(self.btnGet,function()self:OnHandleShortClick({1})end)
end
--根据是否有邮件显示布局
function MailPanel:ShowByMailNums(nums)
    if nums == 0 then--没有邮件
        self._bg0:SetActive(true)
        self._bg1:SetActive(false)
    else--有邮件
        self._bg0:SetActive(false)
        self._bg1:SetActive(true)
    end
end
--邮件列表点击事件
function MailPanel:OnListItemClicked(idx)
    if self._listData == nil then
        return
    end
    local item = self._list:GetItem(idx)
    local itemData = self._listData[idx+1]
    local text = ""
    local title = ""
    local from = ""
    
    if itemData:GetMailId() > 0 then
        local mail = MailDataHelper.GetMailById(itemData:GetMailId())
        if mail then
            title = mail:GetTitle()
            text = mail:GetText()
            from = mail:GetFrom()
        end
    else
        title = itemData:GetInfoTitle()
        text = itemData:GetInfoText()
        from = itemData:GetInfoFrom()
    end
    self._mailTitle.text = title

    if table.isEmpty(itemData:GetInfoExtern()) ~= true then
        local targetStr = text

        local argsTable = LanguageDataHelper.GetArgsTable()
        for k, v in pairs(itemData:GetInfoExtern()) do
            argsTable['' .. (k)] = v
        end
        local txt = LanguageDataHelper.CreateContentByString(targetStr, argsTable)
        txt = LanguageDataHelper.CreateContentByString(txt, argsTable)
        self._mailContent.text = txt 
    else
        self._mailContent.text = text
    end
    self._curMail = itemData:GetDBID()
    self:ShowValidTime(itemData)--有效期显示
    --高亮
    for i=0, #self._listData-1 do
        self._listData[i+1].hightLight = false
        self._list:GetItem(i):SetHightLight(false)
    end
    self._listData[idx+1].hightLight = true
    item:SetHightLight(true)

    self._curIndex = idx
    self:CheckScroll()
    --读取邮件:修改状态
    local curStatus = itemData:GetInfoState()
    if curStatus == public_config.MAIL_STATE_NONE then
        MailManager:ReadMailByDBId("" .. self._curMail)
        itemData:ChangeMailStatus(curStatus, public_config.MAIL_STATE_READ)--未读无附件->已读无附件
    elseif curStatus == public_config.MAIL_STATE_HAVE then
        MailManager:ReadMailByDBId("" .. self._curMail)
        itemData:ChangeMailStatus(curStatus, public_config.MAIL_STATE_HERE)--未读有附件->已读有附件
    end
    item:ChangeMailIcon(itemData:GetInfoState())
    self:ShowAdditions(itemData)--显示附件

    --是否全部已阅
    -- self:CheckAllStatus()
end
--
function MailPanel:CheckScroll()
    --当文字超出Mask时，可以滚动
    if self._mailContent.preferredHeight > self._mailMaskRect.sizeDelta.y then
        self._mailContentScrollRect.vertical = true
    else--否则不可滚动
        self._mailContentScrollRect.vertical = false
    end
end
--有效期显示
function MailPanel:ShowValidTime(item)
    local add15 = item:GetInfoTime() + 15 * 24 * 60 * 60--有效期到这里
    local osServerTime = DateTimeUtil.GetServerTime()
    local gap = add15 - osServerTime
    if gap <= 0 then       
        self._mailTime.text = BaseUtil.GetColorString(LanguageDataHelper.GetContent(595),ColorConfig.B)
    else
        local validTime = DateTimeUtil.MinusDay(gap, 0)
        if validTime ~= nil and validTime.day >= 15 then            
            self._mailTime.text = BaseUtil.GetColorString(LanguageDataHelper.GetContent(596),ColorConfig.B)..BaseUtil.GetColorString("15",ColorConfig.A)
        else
            self._mailTime.text = BaseUtil.GetColorString(LanguageDataHelper.GetContent(596),ColorConfig.B) .. validTime.day .. BaseUtil.GetColorString(LanguageDataHelper.GetContent(597),ColorConfig.A) .. validTime.hour .. BaseUtil.GetColorString(LanguageDataHelper.GetContent(598),ColorConfig.A)
        end
    end
end
--附件显示
function MailPanel:ShowAdditions(item)
    local listdata = {}
    local status = item:GetInfoState()
    if status==public_config.MAIL_STATE_NONE or status==public_config.MAIL_STATE_READ or status == public_config.MAIL_STATE_RECE then
        self.btnGet:SetActive(false)
    else
        self.btnGet:SetActive(true)
    end
    local adds = item:GetInfoAttachement()
    self._commonIdx = 0
    for i=1,#adds do
        local addition = adds[i]
        local itemId = addition:GetItemID()
        local itemCount = addition:GetCount()
        table.insert(listdata,{itemId,itemCount,status})
    end
    -- MailUtil:SortMail(self._listData)
    self._listGet:SetDataList(listdata)
end
--请求邮件callBack
function MailPanel:OnHandleMails()
    self._listData = MailManager:GetMailList()
    if self._listData then
        for k, v in pairs(self._listData) do
            v.hightLight = false
        end
        -- MailUtil:SortMail(self._listData)
        self._list:SetDataList(self._listData)
        self:OnCountMailNum(0, self._listData)
        if #self._listData > 0 then
            self:OnListItemClicked(0)
        end
        self:ShowByMailNums(#self._listData)
    else
        self:ShowByMailNums(0)        
    end
end

function MailPanel:OnCountMailNum(default, listData)

end

function MailPanel:OnDestroy()
    self._close = nil
    self._delete = nil
    self._receive = nil
    self._additions = nil
    EventDispatcher:RemoveEventListener(GameEvents.OnReceiveMails, self._onReceive)
    EventDispatcher:RemoveEventListener(GameEvents.OnShortClick, self._onShortClick)
    EventDispatcher:RemoveEventListener(GameEvents.OnDeleteMail, self._onDeleteMail)
    EventDispatcher:RemoveEventListener(GameEvents.OnColectMail, self._onColectMail)
    EventDispatcher:RemoveEventListener(GameEvents.OnReceNewMail, self._onReceNewMail)
end

-- function MailPanel:OnShow()
function MailPanel:ShowView()
    self:AbleScr(true)
    self:Clear()
    self:RequestForMails()
    MailManager:CleanRewardQueue()
end

function MailPanel:AbleScr(state)
    if self._scrollView then
        self._scrollView:SetScrollRectState(state)
    end
    
end
-- function MailPanel:OnClose()
function MailPanel:CloseView()
    self._isAllGet=false

    if self._scrollView then
        self._scrollView:SetScrollRectState(false)
    end
   
    MailManager:MailReq(1)
end

--检查是否所有邮件已阅
function MailPanel:CheckAllStatus()
    for k, v in pairs(self._listData) do
        --出现可以删除的邮件
        if v:GetInfoState()==public_config.MAIL_STATE_NONE or v:GetInfoState()==public_config.MAIL_STATE_HAVE then
            return
        end
    end
    -- self:RequestForMails()
end


--清除数据
function MailPanel:Clear()
    self._curMail = -1
    self._curIndex = -1
    self._isAllGet=false
    self._mailPage=1
    self:ClearText()
end

function MailPanel:ClearText()
    if self._mailTitle then
        self._mailTitle.text = ""
    end
    if self._mailTime then
        self._mailTime.text = ""
    end
    if self._mailContent then
        self._mailContent.text = ""
    end
end
--请求数据
function MailPanel:RequestForMails()  
    MailManager:MailReq(self._mailPage)
end

function MailPanel:MoveEndRequestForMails()
    self._mailPage = self._mailPage +1
    MailManager:MailReq(self._mailPage)
end

function MailPanel:WinBGType()
    return PanelWinBGType.NormalBG
end
