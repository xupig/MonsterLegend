--MailItem.lua
local MailItem = Class.MailItem(ClassTypes.UIListItem)
MailItem.interface = GameConfig.ComponentsConfig.Component
MailItem.classPath = "Modules.ModuleMail.ChildComponent.MailItem"
local UIComponentUtil = GameUtil.UIComponentUtil
local public_config = GameWorld.public_config
local DateTimeUtil = GameUtil.DateTimeUtil
local MailDataHelper = GameDataHelper.MailDataHelper
local MailConfig = GameConfig.MailConfig

function MailItem:Awake()
    self:InitViews()
end

function MailItem:InitViews()
    self._statusIcon = self:FindChildGO("Container_content/Container_Icon")
    self._imageRed = self:FindChildGO("Container_content/Image_Red")
    self._mailTime = self:GetChildComponent("Container_content/Text_MailTime", "TextMeshWrapper")
    self._mailName = self:GetChildComponent("Container_content/Text_MailName", "TextMeshWrapper")
    self._hightLight = self:FindChildGO("Container_content/Image_BGHL")
    self._readStatus = self:FindChildGO("Container_content/Container_Read")
end

function MailItem:OnDestroy()
    self._statusIcon = nil
    self._hightLight = nil
    self._readStatus = nil
end
--根据状态显示图片
function MailItem:OnRefreshData(mailData)
    self._data = mailData
    local title = ""
    if self._data:GetMailId() > 0 then
        local sysData = MailDataHelper.GetMailById(self._data:GetMailId())
        if sysData then
            title = sysData:GetTitle()
        end
    else
        title = self._data:GetInfoTitle()
    end
    self._mailName.text = title

    local receTime = DateTimeUtil.SomeDay(self._data:GetInfoTime())
    self._mailTime.text = receTime.year .. "-" .. receTime.month .. "-" .. receTime.day
    self._hightLight:SetActive(self._data.hightLight)
    --显示状态
    local status = self._data:GetInfoState()
    self:ChangeMailIcon(status)
end
--改变邮件状态Icon
function MailItem:ChangeMailIcon(status)
    if status == public_config.MAIL_STATE_NONE then
        GameWorld.AddIcon(self._statusIcon, MailConfig.MAIL_STATUS[1])
        self._imageRed:SetActive(true)
    elseif status == public_config.MAIL_STATE_HAVE then
        GameWorld.AddIcon(self._statusIcon, MailConfig.MAIL_STATUS[3])
        self._imageRed:SetActive(true)
    elseif status == public_config.MAIL_STATE_READ then
        GameWorld.AddIcon(self._statusIcon, MailConfig.MAIL_STATUS[2])
        self._imageRed:SetActive(false)
    elseif status == public_config.MAIL_STATE_HERE then
        GameWorld.AddIcon(self._statusIcon, MailConfig.MAIL_STATUS[3])
        self._imageRed:SetActive(true)
    else
        GameWorld.AddIcon(self._statusIcon, MailConfig.MAIL_STATUS[4])
        self._imageRed:SetActive(false)
    end
end

--修改状态、修改邮件图标
function MailItem:ChangeMailStatus(from, to)
    --0->2/1->3、4/3->4
    if from == public_config.MAIL_STATE_NONE then
        self._data:SetInfoState(public_config.MAIL_STATE_READ) 
    elseif from == public_config.MAIL_STATE_HAVE then
        self._data:SetInfoState(to)
    elseif from == public_config.MAIL_STATE_HERE then
        self._data:SetInfoState(public_config.MAIL_STATE_RECE)
    else
        return false
    end
    return true
end
--设置高亮状态
function MailItem:SetHightLightStatus(flag)
    self._inHightLight = flag
end
--选中效果
function MailItem:SetHightLight(flag)
    self._hightLight:SetActive(flag)
end

