--MailGetListItem.lua
require "UIComponent.Extend.ItemGrid"
local MailGetListItem = Class.MailGetListItem(ClassTypes.UIListItem)
MailGetListItem.interface = GameConfig.ComponentsConfig.Component
MailGetListItem.classPath = "Modules.ModuleMail.ChildComponent.MailGetListItem"
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config

function MailGetListItem:Awake()
    self._base.Awake(self)
    self:InitViews()
end

function MailGetListItem:InitViews()
    self._imagedropBg = self:FindChildGO("Container_ItemIcon/Image_dropBg")
    local parent = self:FindChildGO("Container_ItemIcon")
    local item = GUIManager.AddItem(parent,1)
    self._imagedropBg.transform:SetParent(item.transform)
    self._item = UIComponentUtil.AddLuaUIComponent(item,ItemGrid)
    self._item:ActivateTipsById()
end

function MailGetListItem:OnDestroy()

end

function MailGetListItem:OnRefreshData(data)
    if data[3]==public_config.MAIL_STATE_RECE then
        self._imagedropBg:SetActive(true)
    elseif data[3]==public_config.MAIL_STATE_HAVE or data[3]== public_config.MAIL_STATE_HERE then
        self._imagedropBg:SetActive(false)
    end
    self._item:SetItem(data[1],data[2])
end


