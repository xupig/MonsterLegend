--ReceiveButton.lua

local ClassTypes = ClassTypes
local ReceiveButton = Class.ReceiveButton(ClassTypes.BaseLuaUIComponent)

ReceiveButton.interface = GameConfig.ComponentsConfig.PointableComponent
ReceiveButton.classPath = "Modules.ModuleMail.ChildComponent.ReceiveButton"

local TimerHeap = GameWorld.TimerHeap
local CLICK_TYPE = {SHORT=1, LONG=2}
local DateTimeUtil = GameUtil.DateTimeUtil

function ReceiveButton:Awake()
    self._clickType = CLICK_TYPE.SHORT
    self._rece_limit_timer = -1
    self._lastClickTime = -1
end

function ReceiveButton:OnDestroy()

end

function ReceiveButton:OnPointerDown(pointerEventData)
    self._timer = TimerHeap:AddSecTimer(1.2, 0, 1, function() 
        self._clickType = CLICK_TYPE.LONG
        --trigger long click
        EventDispatcher:TriggerEvent(GameEvents.OnLongClick, {[1]=1})
    end)
end

function ReceiveButton:OnPointerUp(pointerEventData)
    TimerHeap:DelTimer(self._timer)
    if self._clickType == CLICK_TYPE.LONG then
        --long click was triggered, do nothing
    else
        --trigger short click
    
        --限制删除不能太快:快速点击两次能发送两次请求
        local curClickTime = DateTimeUtil.GetServerTime()
        if curClickTime - self._lastClickTime >= 1 then
            EventDispatcher:TriggerEvent(GameEvents.OnShortClick, {[1]=1})
            self._lastClickTime = curClickTime
        end
    end

    self._clickType = CLICK_TYPE.SHORT
end

