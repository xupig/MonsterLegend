-- OffPvpHeadPanel.lua
local OffPvpHeadPanel = Class.OffPvpHeadPanel(ClassTypes.BasePanel)
require "Modules.ModuleAthletics.ChildView.OfflinePvpHeadView"
local OfflinePvpHeadView = ClassTypes.OfflinePvpHeadView
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local DateTimeUtil = GameUtil.DateTimeUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TimerHeap = TimerHeap
local  DramaManager = GameManager.DramaManager
local AthleticsData = PlayerManager.PlayerDataManager.athleticsData
local PlayerDataManager = PlayerManager.PlayerDataManager
local AthleticsManager = PlayerManager.AthleticsManager
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
function OffPvpHeadPanel:Awake()
    self._report = function()self:Report() end
    self._hpfun = function(isplayer) self:HpFun(isplayer)end
    self._CGEndCB = function(name,dramaKey,id) self:CGEnd(name,dramaKey,id) end
    self._btnskip = self:FindChildGO("Button_Skip")
    self._csBH:AddClick(self._btnskip,function() self:onSkipDrama()end)
    self._selfHeadView = self:AddChildLuaUIComponent("Container_Self", OfflinePvpHeadView)
    self._enemyHeadView = self:AddChildLuaUIComponent("Container_Enemy", OfflinePvpHeadView)
    self._selfHeadView:IsSelf(true)
    self._enemyHeadView:IsSelf(false)
end

function OffPvpHeadPanel:HpFun(isplayer)
    local d = self:GetDrama()
    if isplayer then
        self._playerFig=self._playerFig-d
        if self._playerFig<0 then
            self._selfHeadView:OnHPChange(0)
        else
            self._selfHeadView:OnHPChange(self._playerFig)
        end
    else
        self._enemyFig=self._enemyFig-d
        if self._enemyFig<0 then
            self._enemyHeadView:OnHPChange(0)
        else
            self._enemyHeadView:OnHPChange(self._enemyFig)
        end
    end
end

function OffPvpHeadPanel:CGEnd(name,dramaKey,id)
    -- if name==self._data.cgname then
    --     GUIManager.ShowPanel(PanelsConfig.Athletics)
    -- end
end

function OffPvpHeadPanel:onSkipDrama()
    DramaManager:SkipDrama(self._data.cgname..self._data.cgf)
    self:Report()
end

function OffPvpHeadPanel:Report()
    self:ReportShow()
    AthleticsManager.PVPGetReward()
end

function OffPvpHeadPanel:ReportShow()
    AthleticsManager:RestoreFog()
    GUIManager.SetCameraCulling(PanelsConfig.OffPVPHead, true)
    GUIManager.ClosePanel(PanelsConfig.OffPVPHead)
    local serveData = AthleticsData:GetCgData()
    if serveData[public_config.INSTANCE_SETTLE_KEY_RESULT] == 1 then
        AthleticsManager.PVPRefresh()
    end

    local balanceData = PlayerDataManager.instanceBalanceData
    balanceData:UpdateData(serveData)
    local data = {}
    data.instanceBalanceData = PlayerDataManager.instanceBalanceData
    data.callBack = function()
            GUIManager.ClosePanel(PanelsConfig.InstBalanceCG)
            GUIManager.ShowPanel(PanelsConfig.Athletics)
            EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleAthletics.AthleticsPanel")
        end
    GUIManager.ShowPanel(PanelsConfig.InstBalanceCG, data)
end

function OffPvpHeadPanel:OnShow(data)
    EventDispatcher:AddEventListener(GameEvents.OnCGShowDamage,self._hpfun)
    EventDispatcher:AddEventListener(GameEvents.DramaPlayEnd,self._CGEndCB)
    EventDispatcher:AddEventListener(GameEvents.OnCGTriggerEventType,self._report)
    self._data = data
    local player = GameWorld.Player()
    local a = GlobalParamsHelper.GetParamValue(509)/100
    self._playerFig=player.fight_force+player.fight_force*a*AthleticsData:GetMInspire()
    self._enemyFig=data.fight_force
    self._rawplayerFig=player.fight_force+player.fight_force*a*AthleticsData:GetMInspire()
    self._rawenemyFig=data.fight_force
    if self._playerFig>self._enemyFig then
        self._type=1
    else
        self._type=2
    end
    local enity = {}
    enity.fight_force = self._playerFig
    enity.name = player.name
    enity.vocation = player.vocation
    self._selfHeadView:SetEntity(enity)
    self._enemyHeadView:SetEntity(data)
end

function OffPvpHeadPanel:GetDrama()
    local drama = 0
    if self._type==1 then
        drama=math.ceil(self._rawenemyFig*0.1435)
    else
        drama=math.ceil(self._rawplayerFig*0.1435)
    end
    return drama
end
function OffPvpHeadPanel:OnClose()
    EventDispatcher:RemoveEventListener(GameEvents.OnCGShowDamage,self._hpfun)
    EventDispatcher:RemoveEventListener(GameEvents.DramaPlayEnd,self._CGEndCB)
    EventDispatcher:RemoveEventListener(GameEvents.OnCGTriggerEventType,self._report)
end

function OffPvpHeadPanel:OnDestroy()
    self._selfHeadView = nil
    self._enemyHeadView = nil
end

function OffPvpHeadPanel:ScaleToHide()
    return true
end