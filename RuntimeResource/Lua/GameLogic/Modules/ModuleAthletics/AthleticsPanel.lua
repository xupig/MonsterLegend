-- AthleticsPanel.lua
require "Modules.ModuleAthletics.ChildView.AthleticsAionView"

local AthleticsPanel = Class.AthleticsPanel(ClassTypes.BasePanel)

local AthleticsManager = PlayerManager.AthleticsManager
local AthleticsData = PlayerManager.PlayerDataManager.athleticsData

local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType

local AthleticsAionView = ClassTypes.AthleticsAionView

require "PlayerManager/PlayerData/CommonTimeListData"
local CommonTimeListData = ClassTypes.CommonTimeListData

local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

function AthleticsPanel:Awake()
    self:InitViews()
end

function AthleticsPanel:InitViews()
    self._containerAionGo = self:FindChildGO('Container_Aion')
    self._containerAion = self:AddChildLuaUIComponent('Container_Aion',AthleticsAionView)
    self._containerAion.transform.localScale = HideScale
end

function AthleticsPanel:AddEventListeners()
   
end

function AthleticsPanel:RemoveEventListeners()
    
end
    
function AthleticsPanel:OnDestroy()
end

function AthleticsPanel:OnShow()
    self._containerAion:Open()
    self._containerAion.transform.localScale = ShowScale
    self:AddEventListeners()
end

function AthleticsPanel:OnClose()
    self:RemoveEventListeners()
    self._containerAion:Close()
    GUIManager.ShowPanel(PanelsConfig.AthleticsEnter)
end

function AthleticsPanel:ScaleToHide()
    return true
end

function AthleticsPanel:BaseShowModel()
    self._containerAion:ShowModel()
end

function AthleticsPanel:BaseHideModel()
    self._containerAion:HideModel()
end


function AthleticsPanel:CloseBtnClick()
    GUIManager.ClosePanel(PanelsConfig.Athletics)
end

-- -- 0 没有  1 暗红底板  2 蓝色底板  3 遮罩底板
function AthleticsPanel:CommonBGType()
    return PanelBGType.NoBG
end



function AthleticsPanel:WinBGType()
    return PanelWinBGType.FullSceenBG
end

function AthleticsPanel:ScaleToHide()
    return true
end