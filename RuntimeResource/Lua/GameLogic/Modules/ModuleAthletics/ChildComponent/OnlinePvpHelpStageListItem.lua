-- OnlinePvpHelpStageListItem.lua
local OnlinePvpHelpStageListItem = Class.OnlinePvpHelpStageListItem(ClassTypes.UIListItem)
OnlinePvpHelpStageListItem.interface = GameConfig.ComponentsConfig.Component
OnlinePvpHelpStageListItem.classPath = "Modules.ModuleAthletics.ChildComponent.OnlinePvpHelpStageListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local OnlinePvpManager = PlayerManager.OnlinePvpManager

function OnlinePvpHelpStageListItem:Awake()
    self:InitItem()
end

function OnlinePvpHelpStageListItem:OnDestroy()
    self._textStage = nil
    self._textPoint = nil
    self._textReward = nil
end

function OnlinePvpHelpStageListItem:InitItem()
    self._textStage = self:GetChildComponent("Text_Stage", "TextMeshWrapper")
    self._textPoint = self:GetChildComponent("Text_Point", "TextMeshWrapper")
    self._textReward = self:GetChildComponent("Text_Reward", "TextMeshWrapper")
end

function OnlinePvpHelpStageListItem:OnRefreshData(data)
    self._data = data
   local OnlinePvpDataHelper = OnlinePvpManager:GetOnlinePvpDataHelper()
   self._textStage.text = LanguageDataHelper.CreateContent(OnlinePvpDataHelper:GetName(data))
   self._textPoint.text = tostring(OnlinePvpDataHelper:GetPoint(data))
   self._textReward.text = tostring(OnlinePvpDataHelper:GetCreditPointReward(data))
end
