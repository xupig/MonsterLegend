-- AthleticsTagItem.lua
local CommonTimeListConfig = GameConfig.CommonTimeListConfig
local AthleticsTagItem = Class.AthleticsTagItem(ClassTypes.BaseComponent)
AthleticsTagItem.interface = GameConfig.ComponentsConfig.PointableComponent
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
AthleticsTagItem.classPath = "Modules.ModuleAthletics.ChildComponent.AthleticsTagItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function AthleticsTagItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function AthleticsTagItem:InitView()
    self._data = {}
    self._imageFlageGo = self:FindChildGO('Image_Flage')
    self._imageIconGo = self:FindChildGO('Image_Icon')
    self._imgLockGo = self:FindChildGO('Image_Lock')
    self._imgDiGo = self:FindChildGO('Image_Di')
    self._imgRedGo = self:FindChildGO('Image_RedPoint')
    self._textFlage = self:GetChildComponent('Image_Di/Text_Flage', 'TextMeshWrapper')
end
function AthleticsTagItem:OnDestroy()

end

function AthleticsTagItem:Update(data)
    if data ~= nil then
        self._data = data
        GameWorld.AddIcon(self._imageIconGo, data:GetIcon())
        if data:IsLock() then
            self._imageFlageGo:SetActive(false)
            self._imgLockGo:SetActive(true)
            self._imgDiGo:SetActive(false)
            self._imgRedGo:SetActive(false)
        else
            self._imgDiGo:SetActive(true)
            self._imgLockGo:SetActive(false)
            self._curOpenStatus = data:GetOpenStatus()
            if self._curOpenStatus == CommonTimeListConfig.OPENNING then
                self._imageFlageGo:SetActive(true)
            else
                self._imageFlageGo:SetActive(false)
            end
        end
        self._textFlage.text = data:GetDescText()
    end
end

function AthleticsTagItem:UpdateRed(status)
    self._imgRedGo:SetActive(status)
end

function AthleticsTagItem:OnPointerDown()
    local id  = self._data:GetFunid()
    local f = FunctionOpenDataHelper:CheckFunctionOpen(id)
    if f then
        local fun = FunctionOpenDataHelper:GetButtonFunc3(id)  
        if fun and fun[2] and fun[2][1] then
            local uiname = fun[2][1]
            GUIManager.ShowPanel(uiname)
            GUIManager.ClosePanel(PanelsConfig.AthleticsEnter)
        else
            local showText = LanguageDataHelper.GetContent(51223)
            GUIManager.ShowText(2, showText)
        end
    else
        local showText = LanguageDataHelper.GetContent(51223)
        GUIManager.ShowText(2, showText)
    end
end
function AthleticsTagItem:OnPointerUp()

end
