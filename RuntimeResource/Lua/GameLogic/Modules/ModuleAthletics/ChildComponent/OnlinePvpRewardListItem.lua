-- OnlinePvpRewardListItem.lua
require "UIComponent.Extend.ItemGrid"
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local OnlinePvpRewardListItem = Class.OnlinePvpRewardListItem(ClassTypes.UIListItem)
OnlinePvpRewardListItem.interface = GameConfig.ComponentsConfig.Component
OnlinePvpRewardListItem.classPath = "Modules.ModuleAthletics.ChildComponent.OnlinePvpRewardListItem"
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local TaskRewardItem = ClassTypes.TaskRewardItem
local TaskRewardData = ClassTypes.TaskRewardData
local OnlinePvpReward2DataHelper = GameDataHelper.OnlinePvpReward2DataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local OnlinePvpManager = PlayerManager.OnlinePvpManager

function OnlinePvpRewardListItem:Awake()
    self:InitItem()
    self._onOnlinePvpGetTotalReward = function(id)self:OnOnlinePvpGetTotalReward(id) end
    EventDispatcher:AddEventListener(GameEvents.OnlinePvpGetTotalReward, self._onOnlinePvpGetTotalReward)
end

function OnlinePvpRewardListItem:OnDestroy()
    EventDispatcher:RemoveEventListener(GameEvents.OnlinePvpGetTotalReward, self._onOnlinePvpGetTotalReward)
end

function OnlinePvpRewardListItem:OnOnlinePvpGetTotalReward(id)
    if self._id == id then
        self._btnGet:SetActive(false)
    end
end

function OnlinePvpRewardListItem:InitItem()
    self._textStage = self:GetChildComponent("Text_Stage", "TextMeshWrapper")
    self._containerActive = self:FindChildGO("Container_Active")
    self._containerDisActive = self:FindChildGO("Container_DisActive")
    self._btnGet = self:FindChildGO("Container_Active/Button_Get")
    self._csBH:AddClick(self._btnGet, function()self:GetBtnClick() end)
    self._gridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Items")
    self._gridLauoutGroup:SetItemType(TaskRewardItem)
end

function OnlinePvpRewardListItem:GetBtnClick()
    OnlinePvpManager:GetSeasonReward(self._id)
end

function OnlinePvpRewardListItem:OnRefreshData(data)
    local result = {}
    local id = data["id"]
    self._id = id
    local rewards = OnlinePvpReward2DataHelper:GetReward(id)
    for i, v in ipairs(rewards) do
        table.insert(result, TaskRewardData(v[1], v[2]))
    end
    local exploitValue = OnlinePvpReward2DataHelper:GetExploit(id)
    local argsTable = LanguageDataHelper.GetArgsTable()
    if data["finish"] then
        self._containerActive:SetActive(true)
        self._containerDisActive:SetActive(false)
        self._btnGet:SetActive(not data["hasGet"])
        argsTable["0"] = StringStyleUtil.GetColorString(exploitValue, StringStyleUtil.green)
    else
        self._containerActive:SetActive(false)
        self._containerDisActive:SetActive(true)
        argsTable["0"] = StringStyleUtil.GetColorString(exploitValue, StringStyleUtil.red)
    end
    self._textStage.text = LanguageDataHelper.CreateContent(OnlinePvpReward2DataHelper:GetDesc(id), argsTable)
    
    self._gridLauoutGroup:SetDataList(result)
end
