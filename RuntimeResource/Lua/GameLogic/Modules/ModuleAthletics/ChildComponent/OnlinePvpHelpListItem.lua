-- OnlinePvpHelpListItem.lua
local OnlinePvpHelpListItem = Class.OnlinePvpHelpListItem(ClassTypes.UIListItem)
OnlinePvpHelpListItem.interface = GameConfig.ComponentsConfig.Component
OnlinePvpHelpListItem.classPath = "Modules.ModuleAthletics.ChildComponent.OnlinePvpHelpListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function OnlinePvpHelpListItem:Awake()
    self:InitItem()
end

function OnlinePvpHelpListItem:OnDestroy()
    self._textInfo = nil
    self._containerDisActive = nil
    self._containerActive = nil
end


function OnlinePvpHelpListItem:InitItem()
    self._textInfo = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._containerDisActive = self:FindChildGO("Container_DisActive")
    self._containerActive = self:FindChildGO("Container_Active")
end

function OnlinePvpHelpListItem:SetActive(flag)
    self._containerActive:SetActive(flag)
    self._containerDisActive:SetActive(not flag)
end

function OnlinePvpHelpListItem:OnRefreshData(data)
    -- LoggerHelper.Log("data .." .. PrintTable:TableToStr(data))
    self._textInfo.text = LanguageDataHelper.CreateContent(data[1])
    self:SetActive(data[3] == true)
end
