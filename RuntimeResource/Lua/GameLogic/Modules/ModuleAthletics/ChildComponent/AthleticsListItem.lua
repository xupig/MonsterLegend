-- AthleticsListItem.lua
local AthleticsListItem = Class.AthleticsListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
AthleticsListItem.interface = GameConfig.ComponentsConfig.Component
AthleticsListItem.classPath = "Modules.ModuleAthletics.ChildComponent.AthleticsListItem"

local AthleticsDataHelper = GameDataHelper.AthleticsDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local AthleticsManager = PlayerManager.AthleticsManager
local DailyActivityConfig = GameConfig.DailyActivityConfig
local UIList = ClassTypes.UIList

require "Modules.ModuleAthletics.ChildComponent.AthleticsTagItem"
local AthleticsTagItem = ClassTypes.AthleticsTagItem


function AthleticsListItem:Awake()
    self._titleText = self:GetChildComponent("Container_Title/Text_Title", 'TextMeshWrapper')
    self._nameText = self:GetChildComponent("Container_Middle/Text_Name", 'TextMeshWrapper')
    self._descText = self:GetChildComponent("Container_Buttom/Text_Desc", 'TextMeshWrapper')

    self._openContainer = self:FindChildGO("Container_Middle/Container_Open")
    self._selectImage = self:FindChildGO("Container_Middle/Image_Select")
    self._lockContainer = self:FindChildGO("Container_Middle/Container_Lock")
    self._notOpenContainer = self:FindChildGO("Container_Middle/Container_NotOPen")
    self._iconContainer = self:FindChildGO("Container_Middle/Container_Bg")

    self._button = self:FindChildGO("Container_Middle/Button")
    self._csBH:AddClick(self._button, function()self:OnSelfClick() end)

    self:InitTagList()
end

function AthleticsListItem:InitTagList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Middle/ScrollViewList_Tag")
	self._list = list
	self._listlView = scrollView 
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    self._list:SetItemType(AthleticsTagItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._listlView:SetOnClickCB(function()self:OnSelfClick() end)
end

function AthleticsListItem:OnSelfClick()
    -- LoggerHelper.Log("sam 点击了tag List")
    self:GetListObj():OnItemClicked(self)
end

function AthleticsListItem:OnDestroy()
    self._titleText = nil
    self._nameText = nil
    self._descText = nil
    self._openContainer = nil
    self._selectImage = nil
    self._lockContainer = nil
end

--override
function AthleticsListItem:OnRefreshData(data)
    local id = data[1]
    local time = data[2]
    local lock = data[3]
    self._status = data[4]

    local icon = AthleticsDataHelper:GetIcon(id)
    GameWorld.AddIcon(self._iconContainer, icon, 0, true)

    local tagId = AthleticsDataHelper:GetTag(id)
    local datas = {}
    if tagId ~= 0 then
        table.insert(datas, tagId)
    end
    self._list:SetDataList(datas)

    self._nameText.text = AthleticsDataHelper:GetName(id)
    self._descText.text = AthleticsDataHelper:GetDesc(id)
    self._titleText.text = time:GetDescription()
    self:SetLock(lock)
    if lock then--未开启
        -- self._titleText.text = time:GetDescription()
        self._notOpenContainer:SetActive(false)
    else
        if time == nil then
            LoggerHelper.Error("time nil " .. tostring(id))
            self._titleText.text = ""
        else
            if self._status == DailyActivityConfig.OPEN then--开启中
                -- self._titleText.text = ""
                self._openContainer:SetActive(true)
                self:SetOpen(true)
                self._notOpenContainer:SetActive(false)
            elseif self._status == DailyActivityConfig.OPEN_SOON then--即将开启
                self._openContainer:SetActive(false)
                self:SetOpen(false)
                self._titleText.text = time:GetDescription()
                self._notOpenContainer:SetActive(true)
            elseif self._status == DailyActivityConfig.NOT_OPEN then--未开启
                self._openContainer:SetActive(false)
                self:SetOpen(false)
                self._notOpenContainer:SetActive(true)
                self._titleText.text = time:GetDescription()
            end                
        end      
    end
end

function AthleticsListItem:SetOpen(value)
    self._selectImage:SetActive(value)
end

function AthleticsListItem:SetLock(lock)
    self._lockContainer:SetActive(lock)
    self._lock = lock
end

function AthleticsListItem:IsLock()
    return self._lock
end

function AthleticsListItem:GetStatus()
    return self._status
end