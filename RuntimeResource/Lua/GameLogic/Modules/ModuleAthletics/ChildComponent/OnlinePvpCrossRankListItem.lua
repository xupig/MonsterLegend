-- OnlinePvpCrossRankListItem.lua
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local OnlinePvpCrossRankListItem = Class.OnlinePvpCrossRankListItem(ClassTypes.UIListItem)
OnlinePvpCrossRankListItem.interface = GameConfig.ComponentsConfig.Component
OnlinePvpCrossRankListItem.classPath = "Modules.ModuleAthletics.ChildComponent.OnlinePvpCrossRankListItem"

local RoleDataHelper = GameDataHelper.RoleDataHelper
local ServerListManager = GameManager.ServerListManager

local RANK_KEY_UUID = 1
local RANK_KEY_POINT = 2
local RANK_KEY_NAME = 3
local RANK_KEY_VOCATION = 4
local RANK_KEY_FORCE = 5
local RANK_KEY_CREATE_SERVER_ID = 6
local RANK_KEY_LOGIN_SERVER_ID = 7
local RANK_KEY_DBID = 8

function OnlinePvpCrossRankListItem:Awake()
    self:InitItem()
end

function OnlinePvpCrossRankListItem:OnDestroy()

end

function OnlinePvpCrossRankListItem:InitItem()
    self._textRank = self:GetChildComponent("Container_Rank/Text_Rank", "TextMeshWrapper")
    self._imageHigh = self:FindChildGO("Container_Rank/Image_High")
    self._imageNormal = self:FindChildGO("Container_Rank/Image_Normal")
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._textVocation = self:GetChildComponent("Text_Vocation", "TextMeshWrapper")
    self._textFightForce = self:GetChildComponent("Text_FightForce", "TextMeshWrapper")
    self._textServer = self:GetChildComponent("Text_Server", "TextMeshWrapper")
    self._textPoint = self:GetChildComponent("Text_Point", "TextMeshWrapper")
end

function OnlinePvpCrossRankListItem:OnRefreshData(data)
    self._data = data
    local rank = self:GetIndex() + 1
    self._textRank.text = rank
    if rank <= 3 then
        self._imageHigh:SetActive(true)
        self._imageNormal:SetActive(false)
    else
        self._imageHigh:SetActive(false)
        self._imageNormal:SetActive(true)
   end
   self._textName.text = data[RANK_KEY_NAME]
   self._textVocation.text = RoleDataHelper.GetRoleNameTextByVocation(data[RANK_KEY_VOCATION])
   self._textFightForce.text = data[RANK_KEY_FORCE]
   self._textServer.text = ServerListManager:GetServerNameById(data[RANK_KEY_CREATE_SERVER_ID])
   self._textPoint.text = data[RANK_KEY_POINT]
end
