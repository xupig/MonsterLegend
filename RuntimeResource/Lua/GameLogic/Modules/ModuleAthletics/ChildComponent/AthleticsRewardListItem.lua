-- AthleticsRewardListItem.lua
require "UIComponent.Extend.ItemGrid"
local AthleticsRewardListItem = Class.AthleticsRewardListItem(ClassTypes.UIListItem)
AthleticsRewardListItem.interface = GameConfig.ComponentsConfig.Component
AthleticsRewardListItem.classPath = "Modules.ModuleAthletics.ChildComponent.AthleticsRewardListItem"
local AthleticsDataHelper = GameDataHelper.AthleticsDataHelper
local AthleticsManager = PlayerManager.AthleticsManager
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager

function AthleticsRewardListItem:Awake()
    self:InitItem()
end

function AthleticsRewardListItem:OnDestroy()
    
end
function AthleticsRewardListItem:InitItem()
    self._rewards = {}
    self._icons = {}
    self._textRank = self:GetChildComponent("Text_Rank","TextMeshWrapper")
    self._textRankMe = self:GetChildComponent("Text_RankMe","TextMeshWrapper")
    self._rewards[1] = self:FindChildGO("Container_Re0")
    self._rewards[2] = self:FindChildGO("Container_Re1")
    self._rewards[3] = self:FindChildGO("Container_Re2")
    local a0 = GUIManager.AddItem(self._rewards[1], 1)
    local a1 = GUIManager.AddItem(self._rewards[2], 1)
    local a2 = GUIManager.AddItem(self._rewards[3], 1)
    self._icons[1] = UIComponentUtil.AddLuaUIComponent(a0,ItemGrid)
    self._icons[2] = UIComponentUtil.AddLuaUIComponent(a1,ItemGrid)
    self._icons[3] = UIComponentUtil.AddLuaUIComponent(a2,ItemGrid)
        
    for i=1,3 do
        self._rewards[i]:SetActive(false)
        self._icons[i]:ActivateTipsById()
    end

    self.btnGetGo = self:FindChildGO("Button_Get")
    self._csBH:AddClick(self.btnGetGo,function()self:onBtnGet()end)

    self._btngetwrap = self:GetChildComponent("Button_Get","ButtonWrapper")
    self.imgGetGo = self:FindChildGO("Image_Geted")

    local txtget = self:GetChildComponent("Button_Get/Text", "TextMeshWrapper")
    txtget.text = LanguageDataHelper.GetContent(123)
end

function AthleticsRewardListItem:onBtnGet()
    AthleticsManager.PVPSettle()
end

function AthleticsRewardListItem:OnRefreshData(data)
    if data['id']<=3 then
        self._textRankMe.text = BaseUtil.GetColorString(data['name'],ColorConfig.E)
    else
        self._textRankMe.text = data['name']        
    end
    self._textRank.text = tostring(data['id'])
    local rewards = data['reward']
    local j=1
    for i,v in pairs(rewards) do
        self._rewards[j]:SetActive(true)
        self._icons[j]:SetItem(i,v)
        j = j+1
    end
    if data["isCanGet"]==true then
        self._btngetwrap.interactable = true
        self.imgGetGo:SetActive(false)
        self.btnGetGo:SetActive(true)
    else
        if data["isGeted"] then
            self.imgGetGo:SetActive(true)
            self.btnGetGo:SetActive(false)
        else
            self.imgGetGo:SetActive(false)
            self.btnGetGo:SetActive(true)
            self._btngetwrap.interactable = false
        end
    end
end
