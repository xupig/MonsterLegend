-- OnlinePvpTargetListItem.lua
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local OnlinePvpTargetListItem = Class.OnlinePvpTargetListItem(ClassTypes.UIListItem)
OnlinePvpTargetListItem.interface = GameConfig.ComponentsConfig.Component
OnlinePvpTargetListItem.classPath = "Modules.ModuleAthletics.ChildComponent.OnlinePvpTargetListItem"
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local TaskRewardItem = ClassTypes.TaskRewardItem
local TaskRewardData = ClassTypes.TaskRewardData

function OnlinePvpTargetListItem:Awake()
    self:InitItem()
end

function OnlinePvpTargetListItem:OnDestroy()

end

function OnlinePvpTargetListItem:InitItem()
    self._textStage = self:GetChildComponent("Text_Stage", "TextMeshWrapper")
    self._gridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Items")
    self._gridLauoutGroup:SetItemType(TaskRewardItem)
end


function OnlinePvpTargetListItem:OnRefreshData(data)
    local result = {}
    -- local id = data["id"]
    self._data = data
    local rewards = data["rewards"]
    for i, v in ipairs(rewards) do
        table.insert(result, TaskRewardData(v[1], v[2]))
    end
    self._textStage.text = data["text"]
    
    self._gridLauoutGroup:SetDataList(result)
end
