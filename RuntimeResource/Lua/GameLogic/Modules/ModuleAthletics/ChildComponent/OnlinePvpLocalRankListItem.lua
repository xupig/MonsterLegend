-- OnlinePvpLocalRankListItem.lua
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local OnlinePvpLocalRankListItem = Class.OnlinePvpLocalRankListItem(ClassTypes.UIListItem)
OnlinePvpLocalRankListItem.interface = GameConfig.ComponentsConfig.Component
OnlinePvpLocalRankListItem.classPath = "Modules.ModuleAthletics.ChildComponent.OnlinePvpLocalRankListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local OnlinePvpManager = PlayerManager.OnlinePvpManager

function OnlinePvpLocalRankListItem:Awake()
    self:InitItem()
end

function OnlinePvpLocalRankListItem:OnDestroy()

end

function OnlinePvpLocalRankListItem:InitItem()
    self._textRank = self:GetChildComponent("Container_Rank/Text_Rank", "TextMeshWrapper")
    self._imageHigh = self:FindChildGO("Container_Rank/Image_High")
    self._imageNormal = self:FindChildGO("Container_Rank/Image_Normal")
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._textStage = self:GetChildComponent("Text_Stage", "TextMeshWrapper")
    self._textPoint = self:GetChildComponent("Text_Point", "TextMeshWrapper")
end

function OnlinePvpLocalRankListItem:OnRefreshData(data)
    self._data = data
    local rank = self:GetIndex() + 1
    self._textRank.text = rank
    if rank <= 3 then
        self._imageHigh:SetActive(true)
        self._imageNormal:SetActive(false)
    else
        self._imageHigh:SetActive(false)
        self._imageNormal:SetActive(true)
   end
   self._textName.text = data:GetName()
   local point = data:GetCross1V1Point()
   local OnlinePvpDataHelper = OnlinePvpManager:GetOnlinePvpDataHelper()
   local orgStageId, orgNextStageId = OnlinePvpDataHelper:GetIdByPoint(point)
   self._textStage.text = LanguageDataHelper.CreateContent(OnlinePvpDataHelper:GetName(orgStageId))
   self._textPoint.text = point
end
