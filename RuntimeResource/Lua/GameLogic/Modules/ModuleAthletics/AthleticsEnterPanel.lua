-- AthleticsPanel.lua
require "Modules.ModuleAthletics.ChildView.AthleticsTotalView"
local AthleticsEnterPanel = Class.AthleticsEnterPanel(ClassTypes.BasePanel)
local AthleticsManager = PlayerManager.AthleticsManager
local AthleticsData = PlayerManager.PlayerDataManager.athleticsData

local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType

local AthleticsTotalView = ClassTypes.AthleticsTotalView

require "PlayerManager/PlayerData/CommonTimeListData"
local CommonTimeListData = ClassTypes.CommonTimeListData

function AthleticsEnterPanel:Awake()
    self:InitFun()
    self:InitViews()
end

function AthleticsEnterPanel:InitFun()
    self._update = function() self:UpdateTotal() end
    self._tagefun = function(data) self:tageClick(data) end    
    self._redfun = function(index,status) self:UpdateRed(index,status) end    
end

function AthleticsEnterPanel:InitViews()
    self._containerEnterGo = self:FindChildGO('Container_Enter')
    self._containerEnter = self:AddChildLuaUIComponent('Container_Enter',AthleticsTotalView)
    self._containerEnterGo:SetActive(true)
    self:UpdateTotal()
end

function AthleticsEnterPanel:UpdateTotal()
    local data = AthleticsData:UpdateOpenTime()
    self._containerEnter:ShowView(data)
end
    
function AthleticsEnterPanel:OnDestroy()
   
end

function AthleticsEnterPanel:OnShow()
    -- self:FindChildGO('Container_WinBg').transform.localPosition = Vector3(-640,-360,7000)
    AthleticsManager:InitData()
    self._containerEnterGo:SetActive(true)
    self:UpdateTotal()
    self:Addlistener()
end

function AthleticsEnterPanel:UpdateRed(index,status)
    self._containerEnter:UpdateRedPoint(index,status)
end

function AthleticsEnterPanel:OnClose()
   self:Removelistener()
   self._containerEnter:CloseView()
end

function AthleticsEnterPanel:Addlistener()
    EventDispatcher:AddEventListener(GameEvents.AthleticsRedEvent,self._redfun)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._update)       
end
function AthleticsEnterPanel:Removelistener()
    EventDispatcher:RemoveEventListener(GameEvents.AthleticsRedEvent,self._redfun)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._update)
end
function AthleticsEnterPanel:CloseBtnClick()
    GUIManager.ClosePanel(PanelsConfig.AthleticsEnter)
end

-- -- 0 没有  1 暗红底板  2 蓝色底板  3 遮罩底板
function AthleticsEnterPanel:CommonBGType()
    return PanelBGType.NoBG
end

function AthleticsEnterPanel:WinBGType()
    return PanelWinBGType.AthleticsBG
end

function AthleticsEnterPanel:ScaleToHide()
    return true
end