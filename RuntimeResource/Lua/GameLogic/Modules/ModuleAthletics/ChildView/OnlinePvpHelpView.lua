--OnlinePvpHelpView.lua
require "Modules.ModuleAthletics.ChildComponent.OnlinePvpHelpListItem"
require "Modules.ModuleAthletics.ChildComponent.OnlinePvpHelpStageListItem"

local public_config = require("ServerConfig/public_config")
local OnlinePvpHelpView = Class.OnlinePvpHelpView(ClassTypes.BaseComponent)
OnlinePvpHelpView.interface = GameConfig.ComponentsConfig.Component
OnlinePvpHelpView.classPath = "Modules.ModuleAthletics.ChildView.OnlinePvpHelpView"
local UIList = ClassTypes.UIList
local OnlinePvpHelpListItem = ClassTypes.OnlinePvpHelpListItem
local OnlinePvpHelpStageListItem = ClassTypes.OnlinePvpHelpStageListItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local OnlinePvpReward2DataHelper = GameDataHelper.OnlinePvpReward2DataHelper
local OnlinePvpReward4DataHelper = GameDataHelper.OnlinePvpReward4DataHelper
local OnlinePvpReward5DataHelper = GameDataHelper.OnlinePvpReward5DataHelper
local UIToggleGroup = ClassTypes.UIToggleGroup
local OnlinePvpManager = PlayerManager.OnlinePvpManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local OnlinePvpSingleServerDataHelper = GameDataHelper.OnlinePvpSingleServerDataHelper

function OnlinePvpHelpView:Awake()
    self._textInfo = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    self._containerTitle = self:FindChildGO("Container_Title")
    
    self:InitHelpBtn()
    self:InitStage()
end

function OnlinePvpHelpView:InitHelpBtn()
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Btn")
    self._listHelpBtn = list
    self._scrollviewHelpBtn = scrollview
    self._listHelpBtn:SetItemType(OnlinePvpHelpListItem)
    self._listHelpBtn:SetPadding(0, 0, 0, 0)
    self._listHelpBtn:SetGap(0, 0)
    self._listHelpBtn:SetDirection(UIList.DirectionTopToDown, 1, 1)
    self._listHelpBtn:SetItemClickedCB(function(idx)self:OnItemClicked(idx) end)
end

function OnlinePvpHelpView:InitStage()
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Stage")
    self._listStage = list
    self._scrollviewStage = scrollview
    self._listStage:SetItemType(OnlinePvpHelpStageListItem)
    self._listStage:SetPadding(0, 0, 0, 0)
    self._listStage:SetGap(0, 0)
    self._listStage:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function OnlinePvpHelpView:OnItemClicked(idx)
    local data = self._listHelpBtn:GetDataByIndex(idx)
    if type(data[2]) == "number" then
        self._textInfo.gameObject:SetActive(true)
        self._listStage.gameObject:SetActive(false)
        self._containerTitle:SetActive(false)
        self._textInfo.text = LanguageDataHelper.CreateContent(data[2])
    else
        self._textInfo.gameObject:SetActive(false)
        self._listStage.gameObject:SetActive(true)
        self._containerTitle:SetActive(true)
        self._listStage:SetDataList(data[2])
    end
    data[3] = true
    if self._selectedIndex then
        local item = self._listHelpBtn:GetItem(self._selectedIndex)
        data = self._listHelpBtn:GetDataByIndex(idx)
        data[3] = false
        item:SetActive(false)
    end
    self._selectedIndex = idx
    local selectedItem = self._listHelpBtn:GetItem(idx)
    selectedItem:SetActive(true)
end

function OnlinePvpHelpView:ShowView()
    local OnlinePvpDataHelper = OnlinePvpManager:GetOnlinePvpDataHelper()
    local allData = OnlinePvpDataHelper:GetAllId()
    local typeStage = {}
    local typeStageDic = {}
    local detailsText = OnlinePvpManager:IsCrossServer() and 51192 or 51193
    table.insert(typeStage, {51007, detailsText})
    for i, id in ipairs(allData) do
        local typeName = OnlinePvpDataHelper:GetTypeName(id)
        if typeStageDic[typeName] == nil then
            typeStageDic[typeName] = {}
            table.insert(typeStage, {typeName, typeStageDic[typeName]})
        end
        table.insert(typeStageDic[typeName], id)
    end
    -- LoggerHelper.Log("typeStage .." .. PrintTable:TableToStr(typeStage))
    -- LoggerHelper.Log("typeStageDic .." .. PrintTable:TableToStr(typeStageDic))
    self._listHelpBtn:SetDataList(typeStage)
    self:OnItemClicked(0)
end

function OnlinePvpHelpView:CloseBtnClick()
    self.gameObject:SetActive(false)
end

function OnlinePvpHelpView:OnEnable()
end

function OnlinePvpHelpView:OnDisable()
end
