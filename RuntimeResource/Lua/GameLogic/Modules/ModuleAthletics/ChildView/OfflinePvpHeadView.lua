-- OfflinePvpHeadView.lua
local OfflinePvpHeadView = Class.OfflinePvpHeadView(ClassTypes.BaseComponent)
OfflinePvpHeadView.interface = GameConfig.ComponentsConfig.Component
OfflinePvpHeadView.classPath = "Modules.ModuleAthletics.ChildView.OfflinePvpHeadView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local public_config = GameWorld.public_config
local RoleDataHelper = GameDataHelper.RoleDataHelper
local GameSceneManager = GameManager.GameSceneManager
local XArtNumber = GameMain.XArtNumber
local UIProgressBar = ClassTypes.UIProgressBar
local OnlinePvpManager = PlayerManager.OnlinePvpManager

function OfflinePvpHeadView:Awake()
    self._fpNumber = self:AddChildComponent('Container_F/Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)
    self._containerHead = self:FindChildGO("Container_Head")
    self._containerFight = self:FindChildGO("Container_F")
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self:InitHPProgress()
end

function OfflinePvpHeadView:OnEnable()
   
end

function OfflinePvpHeadView:OnDisable()
   
end

function OfflinePvpHeadView:OnDestroy()

end

function OfflinePvpHeadView:InitHPProgress()
    self._hpComp = UIProgressBar.AddProgressBar(self.gameObject, "ProgressBar_Hp")
    self._hpComp:SetProgress(0)
    self._hpComp:ShowTweenAnimate(true)
    self._hpComp:SetMutipleTween(true)
    self._hpComp:SetIsResetToZero(false)
    self._hpComp:SetTweenTime(0.2)
end

function OfflinePvpHeadView:SetEntity(entity)
    self._entity = entity
    self._textName.text = entity.name
    local icon = RoleDataHelper.GetHeadIconByVocation(entity.vocation)
    GameWorld.AddIcon(self._containerHead, icon)
    local cur_hp = entity.fight_force
    self.max_hp = entity.fight_force
    self:OnHPChange(cur_hp)
    self:OnFightPowerChange()
end

function OfflinePvpHeadView:OnFightPowerChange()
    if self._entity ~= nil and self._entity.fight_force ~= nil and self._entity.fight_force ~= 0 then
        self._fpNumber:SetNumber(self._entity.fight_force)
    end
    if self._isself then
        local len = string.len(tostring(self._entity.fight_force))*16
        self._containerFight.transform.anchoredPosition = Vector3.New(460-len,630,0)
    end
end

function OfflinePvpHeadView:OnHPChange(curHp)
    self._hpComp:SetProgressByValue(curHp, self.max_hp)
end

function OfflinePvpHeadView:IsSelf(state)
    self._isself = state
end
