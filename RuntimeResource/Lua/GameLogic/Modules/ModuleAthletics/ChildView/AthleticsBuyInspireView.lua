--带两个按钮的一般显示信息消息框
local AthleticsBuyInspireView = Class.AthleticsBuyInspireView(ClassTypes.BaseLuaUIComponent)
AthleticsBuyInspireView.interface = GameConfig.ComponentsConfig.Component
AthleticsBuyInspireView.classPath = "Modules.ModuleAthletics.ChildView.AthleticsBuyInspireView"
local GUIManager = GameManager.GUIManager
local ColorConfig = GameConfig.ColorConfig
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local AthleticsData =  PlayerManager.PlayerDataManager.athleticsData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function AthleticsBuyInspireView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._confirmButtonCB = nil
    self._cancelButtonCB = nil
    self._updateCount = function() self:UpdateCount()end
    self:InitView()
end
--override
function AthleticsBuyInspireView:OnDestroy() 

end

function AthleticsBuyInspireView:ShowView(data)
    self._confirmButtonCB = data.confirmCB
    self._cancelButtonCB = data.cancelCB
    self._time = data.time
    self:ShowDesc(data.text0,data.text1)
end

function AthleticsBuyInspireView:InitView()
    self._confirmButton = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Button_Cancel")
    self._csBH:AddClick(self._cancelButton,function() self:OnCancelButtonClick() end)
    self.descText0 = self:GetChildComponent("Container_Desc0/Text_Desc0", "TextMeshWrapper")
    self.descText1 = self:GetChildComponent("Container_Desc1/Text_Desc1", "TextMeshWrapper")
    self._timetext = self:GetChildComponent("Button_Cancel/Text","TextMeshWrapper")
    self.descText0.text = ""
    self.descText1.text = ""
end

function AthleticsBuyInspireView:OnConfirmButtonClick()
    if self._confirmButtonCB ~= nil then
        self._confirmButtonCB()
    end
end

function AthleticsBuyInspireView:OnCancelButtonClick() 
    if self._cancelButtonCB ~= nil then
        self._cancelButtonCB()
    end
    EventDispatcher:TriggerEvent(GameEvents.AthleShowFx)
    self.gameObject:SetActive(false)
end

function AthleticsBuyInspireView:ShowDesc(text0,text1)
    self.descText0.text = text0
    self.descText1.text = text1
end

function AthleticsBuyInspireView:UpdateCount()
    local arg1 = LanguageDataHelper.GetArgsTable()
    local c = GlobalParamsHelper.GetParamValue(510)-AthleticsData:GetMInspire()
    if c==0 then
        arg1["0"] = BaseUtil.GetColorString(c,ColorConfig.H)..'/'..GlobalParamsHelper.GetParamValue(510)
    else
        arg1["0"] = BaseUtil.GetColorString(c,ColorConfig.J)..'/'..GlobalParamsHelper.GetParamValue(510)
    end
    self.descText1.text = LanguageDataHelper.CreateContent(51149,arg1)  
end

function AthleticsBuyInspireView:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.PVPOffUpdateInfo,self._updateCount)
end

function AthleticsBuyInspireView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.PVPOffUpdateInfo,self._updateCount)
 end