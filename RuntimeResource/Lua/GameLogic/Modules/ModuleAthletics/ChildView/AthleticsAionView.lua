--AthleticsAionView.lua
require "Modules.ModuleAthletics.ChildView.AthleticsRewardView"
require "Modules.ModuleAthletics.ChildView.AthleticsBuyInspireView"

local AthleticsAionView = Class.AthleticsAionView(ClassTypes.BaseComponent)
AthleticsAionView.interface = GameConfig.ComponentsConfig.Component
AthleticsAionView.classPath = "Modules.ModuleAthletics.ChildView.AthleticsAionView"
local AthleticsData =  PlayerManager.PlayerDataManager.athleticsData
local AthleticsManager = PlayerManager.AthleticsManager
local RoleDataHelper = GameDataHelper.RoleDataHelper
local ActorModelComponent = GameMain.ActorModelComponent
local PanelsConfig = GameConfig.PanelsConfig
local XArtNumber = GameMain.XArtNumber
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local AthleticsRewardView = ClassTypes.AthleticsRewardView
local AthleticsBuyInspireView = ClassTypes.AthleticsBuyInspireView
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local GUIManager = GameManager.GUIManager

function AthleticsAionView:Awake()
    self._base.Awake(self)
    self:InitFun()
    self:InitView()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:AddClick('Button_Add',self._onadd)
    self:AddClick('Button_Rank',self._onrank)
    self:AddClick('Button_Fight',self._oninspire)
    self:AddClick('Button_Shop',self._onshop)
    self:AddClick('Container_Role0/Button_Fight0',self._onChallenge)    
    self:AddClick('Container_Role1/Button_Fight1',self._onChallenge)    
    self:AddClick('Container_Role2/Button_Fight2',self._onChallenge)    
    self:AddClick('Container_Role3/Button_Fight3',self._onChallenge) 

    self._btnFight0 = self:GetChildComponent("Container_Role0/Button_Fight0", "ButtonWrapper")
    self._btnFight1 = self:GetChildComponent("Container_Role1/Button_Fight1", "ButtonWrapper")
    self._btnFight2 = self:GetChildComponent("Container_Role2/Button_Fight2", "ButtonWrapper")
    self._btnFight3 = self:GetChildComponent("Container_Role3/Button_Fight3", "ButtonWrapper")
    

    self:AddClick('Button_Update',self._refresh)
end

function AthleticsAionView:InitView()
    self._rolesMode = {}
    self._rolesModeGo = {}
    self._fights = {}
    self._textNames = {}
    self._textRanks = {}

    self._rolesModeGo[1]=self:FindChildGO("Container_Role0/Container_Model")
    self._rolesModeGo[2]=self:FindChildGO("Container_Role1/Container_Model")
    self._rolesModeGo[3]=self:FindChildGO("Container_Role2/Container_Model")
    self._rolesModeGo[4]=self:FindChildGO("Container_Role3/Container_Model")

    self._bgmask=self:FindChildGO("Button_Bg")
    

    self._textMNum = self:GetChildComponent('Container_Num/Text_MNum','TextMeshWrapper')
    self._textMFight = self:GetChildComponent('Container_Fight/Text_MFight','TextMeshWrapper')
    self._textMRank = self:GetChildComponent('Container_Rank/Text_MRank','TextMeshWrapper')

    self._textRanks[1] = self:GetChildComponent("Container_Role0/Text_Rank","TextMeshWrapper")
    self._textRanks[2] = self:GetChildComponent("Container_Role1/Text_Rank","TextMeshWrapper")
    self._textRanks[3] = self:GetChildComponent("Container_Role2/Text_Rank","TextMeshWrapper")
    self._textRanks[4] = self:GetChildComponent("Container_Role3/Text_Rank","TextMeshWrapper")

    self._textNames[1] = self:GetChildComponent("Container_Role0/Text_Name","TextMeshWrapper")
    self._textNames[2] = self:GetChildComponent("Container_Role1/Text_Name","TextMeshWrapper")
    self._textNames[3] = self:GetChildComponent("Container_Role2/Text_Name","TextMeshWrapper")
    self._textNames[4] = self:GetChildComponent("Container_Role3/Text_Name","TextMeshWrapper")

    self._fights[1] = self:AddChildComponent('Container_Role0/Container_FightPower', XArtNumber)
    self._fights[2] = self:AddChildComponent('Container_Role1/Container_FightPower', XArtNumber)
    self._fights[3] = self:AddChildComponent('Container_Role2/Container_FightPower', XArtNumber)
    self._fights[4] = self:AddChildComponent('Container_Role3/Container_FightPower', XArtNumber)

    self._roleModel0 = self:AddChildComponent("Container_Role0/Container_Model",ActorModelComponent);
    self._roleModel1 = self:AddChildComponent("Container_Role1/Container_Model",ActorModelComponent);
    self._roleModel2 = self:AddChildComponent("Container_Role2/Container_Model",ActorModelComponent);
    self._roleModel3 = self:AddChildComponent("Container_Role3/Container_Model",ActorModelComponent);
    self._roleModel0:SetStartSetting(Vector3(0,0,80),Vector3(0,180,0),1)
    self._roleModel1:SetStartSetting(Vector3(0,0,80),Vector3(0,180,0),1)
    self._roleModel2:SetStartSetting(Vector3(0,0,80),Vector3(0,180,0),1)
    self._roleModel3:SetStartSetting(Vector3(0,0,80),Vector3(0,180,0),1)
    self._rolesMode[1]=self._roleModel0
    self._rolesMode[2]=self._roleModel1
    self._rolesMode[3]=self._roleModel2
    self._rolesMode[4]=self._roleModel3

    self._containerCurRankGo = self:FindChildGO("Container_CurRank")
    self._containerCurRank = self:AddChildLuaUIComponent('Container_CurRank',AthleticsRewardView)
    self._containerCurRankGo:SetActive(false)    
    self._rewardRedGo = self:FindChildGO("Button_Rank/Image_Red")

    self._containerBuyInspireGo = self:FindChildGO("Container_Inspire")
    self._containerBuyInspire= self:AddChildLuaUIComponent('Container_Inspire',AthleticsBuyInspireView)
    self._containerBuyInspireGo:SetActive(false)   

end
function AthleticsAionView:onAddFight()
    local d = {}
    local data = {}
    d.id = MessageBoxType.BuyCountView
    d.value =  data
    local info = AthleticsData:GetTotalInfo()       
    local entity = GameWorld.Player()
    local buycount = entity.offline_pvp_buy_count       
    data['title'] = info['rawchacount']+buycount-info['chacount']
    data['count'] = 1
    data['buyCanNum'] = GlobalParamsHelper.GetParamValue(518)-buycount
    data['price'] = GlobalParamsHelper.GetParamValue(519)
    data['confirmCB'] = function(count) AthleticsManager.PVPBuy(tostring(count)) end
    GUIManager.ShowPanel(PanelsConfig.MessageBox,d,nil)
    self:HideActorFx()
end
function AthleticsAionView:onRank()
    self._containerCurRankGo:SetActive(true)
    self._containerCurRank:Open()
    self._containerCurRank:Update()
    self:HideActorFx()
end

function AthleticsAionView:onShop()
    local data ={["id"]=6}
    GUIManager.ShowPanel(PanelsConfig.Shop,data,nil)
end

function AthleticsAionView:onRefresh()
    AthleticsManager.PVPRefresh()
end

function AthleticsAionView:onInspire()
    local data = {}
    local arg0 = LanguageDataHelper.GetArgsTable()
    arg0["0"] = BaseUtil.GetColorString(GlobalParamsHelper.GetParamValue(511),ColorConfig.J)
    arg0["1"] = BaseUtil.GetColorString(GlobalParamsHelper.GetParamValue(509),ColorConfig.J)
    local t0 = LanguageDataHelper.CreateContent(51145,arg0)
    local t1 = LanguageDataHelper.GetLanguageData(51146)
    data['text0'] = t0..'\n'..t1
    local arg1 = LanguageDataHelper.GetArgsTable()
    arg1["0"] = BaseUtil.GetColorString((GlobalParamsHelper.GetParamValue(510)-AthleticsData:GetMInspire()),ColorConfig.J)..'/'..GlobalParamsHelper.GetParamValue(510)
    data['text1'] =LanguageDataHelper.CreateContent(51149,arg1)     
    data['confirmCB'] = function() AthleticsManager.PVPInspire() end
    data['cancelCB'] = self._showFx
    self._containerBuyInspireGo:SetActive(true)   
    self._containerBuyInspire:ShowView(data)

    self:HideActorFx()
end


function AthleticsAionView:ShowActorFx()
    for i=1,4 do
        self._rolesMode[i]:ShowFx()
    end
end

function AthleticsAionView:HideActorFx()
    for i=1,4 do
        self._rolesMode[i]:HideFx()
    end
end

function AthleticsAionView:Challenge(ev)
    local name = ev.name
    if self._roles ~= nil then
        if not self._nocha then
            GUIManager.ShowText(1, LanguageDataHelper.GetContent(56053))
            return
        end
        local d = nil
        if name == 'Button_Fight0' then
            d = self._roles[1]
            self._curFightBtn = self._btnFight0
            self._btnFight0.interactable = false
        elseif name == 'Button_Fight1' then  
            d = self._roles[2]
            self._curFightBtn = self._btnFight1
            self._btnFight1.interactable = false
        elseif name == 'Button_Fight2' then
            d = self._roles[3]    
            self._curFightBtn = self._btnFight2
            self._btnFight2.interactable = false
        else
            d = self._roles[4]
            self._curFightBtn = self._btnFight3
            self._btnFight3.interactable = false
        end
        AthleticsManager:SeleChallRole(d)
        self._bgmask:SetActive(true)
        AthleticsManager:PVPChallenge(tostring(d:GetUseRank()),tostring(d:GetFight()))            
    end
end

function AthleticsAionView:InitFun()
   self._showFx = function()self:ShowActorFx()end
   self._hideFx = function()self:HideActorFx()end
    self._updateplayer = function() self:UpdatePlayer()end
    self._refresh = function()self:onRefresh()end
    self._onChallenge = function(ev) self:Challenge(ev)end
    self._onadd = function() self:onAddFight() end
    self._onrank  = function() self:onRank()end
    self._oninspire = function() self:onInspire()end
    self._onshop = function() self:onShop()end
    self._updataRoles = function() self:UpdataRoles()end
    self._fightFail = function() self:FightFail()end
end

function AthleticsAionView:Open()
    self._containerCurRankGo:SetActive(false)
    self:UpdatePlayer()
    self:UpdataRoles()
end

function AthleticsAionView:Close()
    self:ResetBtn()
end

function AthleticsAionView:ResetBtn()
    if self._curFightBtn then
        self._curFightBtn.interactable = true
    end
    self._bgmask:SetActive(false)
end

function AthleticsAionView:FightFail()
    self:ResetBtn()
end

function AthleticsAionView:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.AthleShowFx,self._showFx)
    EventDispatcher:AddEventListener(GameEvents.FightFail,self._fightFail)
    EventDispatcher:AddEventListener(GameEvents.PVPOffUpdateInfo,self._updateplayer)
    EventDispatcher:AddEventListener(GameEvents.PVPOffUpdateRoles,self._updataRoles)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_FIGHT_FORCE, self._updateplayer)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_OFFLINE_PVP_CHALLENGE_COUNT, self._updateplayer)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_OFFLINE_PVP_BUY_COUNT, self._updateplayer)
end

function AthleticsAionView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.AthleShowFx,self._showFx)
    EventDispatcher:RemoveEventListener(GameEvents.FightFail,self._fightFail)
    EventDispatcher:RemoveEventListener(GameEvents.PVPOffUpdateInfo,self._updateplayer)
    EventDispatcher:RemoveEventListener(GameEvents.PVPOffUpdateRoles,self._updataRoles)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_FIGHT_FORCE, self._updateplayer)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_OFFLINE_PVP_CHALLENGE_COUNT, self._updateplayer)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_OFFLINE_PVP_BUY_COUNT, self._updateplayer)
 end
-- 更新玩家信息
function AthleticsAionView:UpdatePlayer()   
    local entity = GameWorld.Player()
    local buycount = entity.offline_pvp_buy_count
    local a = GlobalParamsHelper.GetParamValue(509)/100
    self._textMFight.text = string.format("%d",math.floor(entity.fight_force*(1+a*AthleticsData:GetMInspire())))
    self._textMRank.text = tostring(AthleticsData:GetMRank())
    local rawchacount = AthleticsData:GetRawChaCount()
    local chacount = entity.offline_pvp_challenge_count
    local cnum = rawchacount+buycount-chacount
    self._nocha = true
    if cnum<=0 then
        self._nocha = false
        cnum = 0
        self._textMNum.text = BaseUtil.GetColorString(cnum,ColorConfig.H)..'/'..(rawchacount+buycount)
    else
        self._textMNum.text = BaseUtil.GetColorString(cnum,ColorConfig.J)..'/'..(rawchacount+buycount)
    end
    self._rewardRedGo:SetActive(AthleticsManager:CanGetReward())
end

-- 更新人的信息
function AthleticsAionView:UpdataRoles()
    self._roles = AthleticsData:UpdateRoles()
    for i=1,4 do 
        local v=self._roles[i]
        if v then
            self._rolesModeGo[i]:SetActive(true)
            self._rolesMode[i]:SetStartSetting(Vector3(0,0,-350),Vector3(0,180,0),1)        
            if v:IsRobot() then
                self._rolesMode[i]:LoadNpcModel(v:GetModel(),v:GetFacade(),false,v:GetVacation())
            else
                self._rolesMode[i]:LoadAvatarModel(v:GetVacation(),v:GetFacade(),true)
            end
            self._textNames[i].text = v:GetName()
            local arg = LanguageDataHelper.GetArgsTable()
            arg['0'] = v:GetVisRank()
            self._textRanks[i].text = LanguageDataHelper.CreateContent(56217,arg)
            self._fights[i]:SetNumber(v:GetFight())
        else
            self._rolesModeGo[i]:SetActive(false)
            self._textNames[i].text = ''
            self._textRanks[i].text = ''
            self._fights[i]:SetNumber(0)
        end
    end
end

function AthleticsAionView:OnDestory()
    self._csBH = nil
end

function AthleticsAionView:ShowModel()
    for k,v in ipairs(self._rolesMode) do
        v:Show()
    end
end

function AthleticsAionView:HideModel()
    for k,v in ipairs(self._rolesMode) do
        v:Hide()
    end
end