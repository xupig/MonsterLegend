--OnlinePvpRewardView.lua
require "Modules.ModuleAthletics.ChildComponent.OnlinePvpRewardListItem"
require "Modules.ModuleAthletics.ChildComponent.OnlinePvpTargetListItem"
require "Modules.ModuleAthletics.ChildComponent.OnlinePvpCrossRankListItem"
require "Modules.ModuleAthletics.ChildComponent.OnlinePvpLocalRankListItem"

local public_config = require("ServerConfig/public_config")
local OnlinePvpRewardView = Class.OnlinePvpRewardView(ClassTypes.BaseComponent)
OnlinePvpRewardView.interface = GameConfig.ComponentsConfig.Component
OnlinePvpRewardView.classPath = "Modules.ModuleAthletics.ChildView.OnlinePvpRewardView"
local UIList = ClassTypes.UIList
local OnlinePvpRewardListItem = ClassTypes.OnlinePvpRewardListItem
local OnlinePvpTargetListItem = ClassTypes.OnlinePvpTargetListItem
local OnlinePvpCrossRankListItem = ClassTypes.OnlinePvpCrossRankListItem
local OnlinePvpLocalRankListItem = ClassTypes.OnlinePvpLocalRankListItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local OnlinePvpReward4DataHelper = GameDataHelper.OnlinePvpReward4DataHelper
local OnlinePvpReward5DataHelper = GameDataHelper.OnlinePvpReward5DataHelper
local UIToggleGroup = ClassTypes.UIToggleGroup
local OnlinePvpManager = PlayerManager.OnlinePvpManager
local RankExManager = PlayerManager.RankExManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function OnlinePvpRewardView:Awake()
    self._toggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup_Tab")
    self._toggleGroup:SetOnSelectedIndexChangedCB(function(index)self:OnTabBarClick(index) end)
    self._toggleGroup:SetSelectIndex(0)
    self._textTitle = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    self._containerStageReward = self:FindChildGO("Container_StageReward")
    self._containerTarget = self:FindChildGO("Container_Target")
    self._containerCrossRank = self:FindChildGO("Container_CrossRank")
    self._containerLocalRank = self:FindChildGO("Container_LocalRank")
    self._btnClose = self:FindChildGO("Button_Close")
    self._imageRedPointStageReward = self:FindChildGO("ToggleGroup_Tab/Toggle_StageReward/Image_RedPoint")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    self._onOnlinePvpCrossRank = function(args)self:OnOnlinePvpCrossRank(args) end
    self._getRankListResp = function()self:GetRankListResp() end
    
    self:InitStageReward()
    self:InitTarget()
    self:InitCrossRank()
    self:InitLocalRank()
end

function OnlinePvpRewardView:InitStageReward()
    self._textMyStage = self:GetChildComponent("Container_StageReward/Text_MyStage", "TextMeshWrapper")
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_StageReward/ScrollViewList")
    self._listStageReward = list
    self._scrollviewStageReward = scrollview
    self._listStageReward:SetItemType(OnlinePvpRewardListItem)
    self._listStageReward:SetPadding(0, 0, 0, 0)
    self._listStageReward:SetGap(0, 0)
    self._listStageReward:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function OnlinePvpRewardView:InitTarget()
    self._textTarget = self:GetChildComponent("Container_Target/Text_Target", "TextMeshWrapper")
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_Target/ScrollViewList")
    self._listTarget = list
    self._scrollviewTarget = scrollview
    self._listTarget:SetItemType(OnlinePvpTargetListItem)
    self._listTarget:SetPadding(0, 0, 0, 0)
    self._listTarget:SetGap(0, 0)
    self._listTarget:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function OnlinePvpRewardView:InitCrossRank()
    self._textMyPoint = self:GetChildComponent("Container_CrossRank/Text_MyPoint", "TextMeshWrapper")
    self._textMyRank = self:GetChildComponent("Container_CrossRank/Text_MyRank", "TextMeshWrapper")
    self._goNoRank = self:FindChildGO("Container_CrossRank/Text_NoRank")
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_CrossRank/ScrollViewList")
    self._listCrossRank = list
    self._scrollviewCrossRank = scrollview
    self._listCrossRank:SetItemType(OnlinePvpCrossRankListItem)
    self._listCrossRank:SetPadding(0, 0, 0, 0)
    self._listCrossRank:SetGap(0, 0)
    self._listCrossRank:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function OnlinePvpRewardView:InitLocalRank()
    self._textMyLocalPoint = self:GetChildComponent("Container_LocalRank/Text_MyPoint", "TextMeshWrapper")
    self._textMyLocalRank = self:GetChildComponent("Container_LocalRank/Text_MyRank", "TextMeshWrapper")
    self._goNoLocalRank = self:FindChildGO("Container_LocalRank/Text_NoRank")
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_LocalRank/ScrollViewList")
    self._listLocalRank = list
    self._scrollviewLocalRank = scrollview
    self._listLocalRank:SetItemType(OnlinePvpLocalRankListItem)
    self._listLocalRank:SetPadding(0, 0, 0, 0)
    self._listLocalRank:SetGap(0, 0)
    self._listLocalRank:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function OnlinePvpRewardView:CloseBtnClick()
    self.gameObject:SetActive(false)
end

function OnlinePvpRewardView:OnTabBarClick(index)
    if index == 0 then
        self._containerStageReward:SetActive(true)
        self._containerTarget:SetActive(false)
        self._containerCrossRank:SetActive(false)
        self._containerLocalRank:SetActive(false)
        self:SetStageRewardData()
    elseif index == 1 then
        self._containerStageReward:SetActive(false)
        self._containerTarget:SetActive(true)
        self._containerCrossRank:SetActive(false)
        self._containerLocalRank:SetActive(false)
        self:SetTargetData()
    else
        self._containerStageReward:SetActive(false)
        self._containerTarget:SetActive(false)
        if OnlinePvpManager:IsCrossServer() then
            self._containerCrossRank:SetActive(true)
            self._containerLocalRank:SetActive(false)
            self:SetCrossRankData()
        else
            self._containerCrossRank:SetActive(false)
            self._containerLocalRank:SetActive(true)
            self:SetLocalRankData()
        end
    end
end

function OnlinePvpRewardView:OnEnable()
    if OnlinePvpManager:IsCrossServer() then
        self._toggleGroup.gameObject:SetActive(true)
        self._toggleGroup:SetSelectIndex(0)
        self._imageRedPointStageReward:SetActive(OnlinePvpManager:HasNotGetStageRewards())
        EventDispatcher:AddEventListener(GameEvents.OnlinePvpCrossRank, self._onOnlinePvpCrossRank)
        self._textTitle.text = LanguageDataHelper.CreateContent(51215)
    else
        EventDispatcher:AddEventListener(GameEvents.RANK_LIST_GET_RESP, self._getRankListResp)
        self._toggleGroup.gameObject:SetActive(false)
        self._textTitle.text = LanguageDataHelper.CreateContent(51217)
    end
    self._isEnable = true
end

function OnlinePvpRewardView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.OnlinePvpCrossRank, self._onOnlinePvpCrossRank)
    EventDispatcher:RemoveEventListener(GameEvents.RANK_LIST_GET_RESP, self._getRankListResp)
    self._isEnable = false
end

function OnlinePvpRewardView:SetStageRewardData()
    self._listStageReward:SetDataList(OnlinePvpManager:GetStateRewardData())
    
    local args = OnlinePvpManager:GetServerData()
    local totalExploit = args[public_config.CROSS_VS11_KEY_DATA_TOTAL_EXPLOIT]
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = totalExploit
    self._textMyStage.text = LanguageDataHelper.CreateContent(51191, argsTable)
end


function OnlinePvpRewardView:SetTargetData()
    local result = {}
    local rank1 = GlobalParamsHelper.GetParamValue(669)--巅峰竞技实时1v1赛季排名区间第1名
    local rank2 = GlobalParamsHelper.GetParamValue(670)--巅峰竞技实时1v1赛季排名区间第2-4名
    local rank3 = GlobalParamsHelper.GetParamValue(671)--巅峰竞技实时1v1赛季排名区间第5-16名
    local rank4 = GlobalParamsHelper.GetParamValue(672)--巅峰竞技实时1v1赛季排名区间第17-32名
    table.insert(result, {["text"] = LanguageDataHelper.CreateContent(51182), ["rewards"] = OnlinePvpReward4DataHelper:GetRewardShow(rank1)})
    table.insert(result, {["text"] = LanguageDataHelper.CreateContent(51183), ["rewards"] = OnlinePvpReward4DataHelper:GetRewardShow(rank2)})
    table.insert(result, {["text"] = LanguageDataHelper.CreateContent(51184), ["rewards"] = OnlinePvpReward4DataHelper:GetRewardShow(rank3)})
    table.insert(result, {["text"] = LanguageDataHelper.CreateContent(51185), ["rewards"] = OnlinePvpReward4DataHelper:GetRewardShow(rank4)})
    
    local stageTargets = OnlinePvpReward5DataHelper:GetAllId()
    local OnlinePvpDataHelper = OnlinePvpManager:GetOnlinePvpDataHelper()
    for i, id in ipairs(stageTargets) do
        local data = {}
        local pvpId = OnlinePvpReward5DataHelper:GetPvpId(id)
        data["text"] = LanguageDataHelper.CreateContent(OnlinePvpDataHelper:GetName(pvpId))
        data["rewards"] = OnlinePvpReward5DataHelper:GetReward(id)
        table.insert(result, data)
    end
    
    self._listTarget:SetDataList(result)
    local args = OnlinePvpManager:GetServerData()
    local totalCount = GlobalParamsHelper.GetParamValue(582)--巅峰竞技实时1v1跨服赛季冲段奖励需要参与次数
    
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = args[public_config.CROSS_VS11_KEY_DATA_TOTAL_COUNT] .. "/" .. totalCount
    self._textTarget.text = LanguageDataHelper.CreateContent(51186, argsTable)
end

function OnlinePvpRewardView:SetLocalRankData()
    RankExManager:GetRank(public_config.RANK_TYPE_CROSS_1V1)
end

function OnlinePvpRewardView:SetCrossRankData()
    OnlinePvpManager:GetCrossRank()
end

function OnlinePvpRewardView:OnOnlinePvpCrossRank(rankData)
    self._listCrossRank:SetDataList(rankData)
    local args = OnlinePvpManager:GetServerData()
    local playerDbid = GameWorld.Player().dbid
    local playerRank = 0
    for i, v in ipairs(rankData) do
        if playerDbid == v[8] then
            playerRank = i
            break
        end
    end
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = args[public_config.CROSS_VS11_KEY_DATA_TOTAL_POINT]
    self._textMyPoint.text = LanguageDataHelper.CreateContent(51190, argsTable)
    
    argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = playerRank == 0 and "" or tostring(playerRank)
    self._textMyRank.text = LanguageDataHelper.CreateContent(51187, argsTable)
    self._goNoRank:SetActive(playerRank == 0)
end

function OnlinePvpRewardView:GetRankListResp()
    self._textMyLocalRank = self:GetChildComponent("Container_LocalRank/Text_MyRank", "TextMeshWrapper")
    local args = OnlinePvpManager:GetServerData()
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = args[public_config.CROSS_VS11_KEY_DATA_TOTAL_POINT]
    self._textMyLocalPoint.text = LanguageDataHelper.CreateContent(51190, argsTable)
    local rankContentListData = RankExManager:GetRankContentListData()
    local myRank = RankExManager:GetMyRank()
    argsTable = LanguageDataHelper.GetArgsTable()
    if myRank == nil then
        self._goNoLocalRank:SetActive(true)
        argsTable["0"] = ""
    else
        self._goNoLocalRank:SetActive(false)
        argsTable["0"] = myRank
    end
    self._textMyLocalRank.text = LanguageDataHelper.CreateContent(51187, argsTable)
    self._listLocalRank:SetDataList(rankContentListData)
end

function OnlinePvpRewardView:OnDestory()

end
