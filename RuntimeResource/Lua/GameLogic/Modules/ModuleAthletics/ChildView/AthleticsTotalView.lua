--AthleticsTotalView.lua
require "Modules.ModuleAthletics.ChildComponent.AthleticsTagItem"
local AthleticsTotalView = Class.AthleticsTotalView(ClassTypes.BaseComponent)
local AthleticsTagItem = ClassTypes.AthleticsTagItem
AthleticsTotalView.interface = GameConfig.ComponentsConfig.Component
AthleticsTotalView.classPath = "Modules.ModuleAthletics.ChildView.AthleticsTotalView"
local AthleticsData =  PlayerManager.PlayerDataManager.athleticsData
local AthleticsConfig = GameConfig.AthleticsConfig
local RedPointManager = GameManager.RedPointManager
function AthleticsTotalView:Awake()
    self:InitView()
end

function AthleticsTotalView:InitView()
    self._views = {}
    self._views[AthleticsConfig.Aion] = self:AddChildLuaUIComponent('Container_Aion',AthleticsTagItem)
    self._views[AthleticsConfig.Heaven] = self:AddChildLuaUIComponent('Container_Heaven',AthleticsTagItem)
    self._views[AthleticsConfig.Peak] = self:AddChildLuaUIComponent('Container_Peak',AthleticsTagItem)
    self._views[AthleticsConfig.Hell] = self:AddChildLuaUIComponent('Container_Hell',AthleticsTagItem)
    self._views[AthleticsConfig.Dark] = self:AddChildLuaUIComponent('Container_Dark',AthleticsTagItem)
    self._views[AthleticsConfig.Expect] = self:AddChildLuaUIComponent('Container_Expect',AthleticsTagItem)

    self._redFuns={}
    self._redFuns[AthleticsConfig.Aion] = function(status) self:UpdateRedPoint(AthleticsConfig.Aion,status) end
    self._redFuns[AthleticsConfig.Heaven] = function(status) self:UpdateRedPoint(AthleticsConfig.Heaven,status) end
    self._redFuns[AthleticsConfig.Peak] = function(status) self:UpdateRedPoint(AthleticsConfig.Peak,status)  end
    self._redFuns[AthleticsConfig.Hell] = function(status) self:UpdateRedPoint(AthleticsConfig.Hell,status)  end
    self._redFuns[AthleticsConfig.Dark] = function(status) self:UpdateRedPoint(AthleticsConfig.Dark,status) end
    self._redFuns[AthleticsConfig.Expect] = function(status) self:UpdateRedPoint(AthleticsConfig.Expect,status)  end
end

function AthleticsTotalView:ShowView(data)
    self:AddRedFun()
    self:UpdateView(data)
    self:UpdateReds()
end



function AthleticsTotalView:CloseView()
    self:RemoveRedFun()
end


function AthleticsTotalView:RemoveRedFun()
    for i,v in ipairs(AthleticsConfig.funids) do
        local node = RedPointManager:GetRedPointDataByFuncId(v)
        if node then
            node:SetUpdateRedPointFunc(nil)
        end
    end
end

function AthleticsTotalView:AddRedFun()
    self._redNodes = {}
    for i,v in ipairs(AthleticsConfig.funids) do
        local node = RedPointManager:GetRedPointDataByFuncId(v)
        if node then
            self._redNodes[i]=node
            node:SetUpdateRedPointFunc(self._redFuns[i])
        end
    end
end

function AthleticsTotalView:UpdateReds()
    for index,v in ipairs(self._redFuns) do 
        local node = self._redNodes[index]
        if node then
            self:UpdateRedPoint(index,node:GetRedPoint())
        end
    end
end

function AthleticsTotalView:UpdateView(data)
    for k,v in pairs(data) do 
        self._views[k]:Update(v)
    end
end

function AthleticsTotalView:UpdateRedPoint(index,status)
    if self._views[index] then
        self._views[index]:UpdateRed(status)
    end
end

function AthleticsTotalView:OnDestory()

end