--AthleticsRewardView.lua
require"Modules.ModuleAthletics.ChildComponent.AthleticsRewardListItem"
local AthleticsRewardView = Class.AthleticsRewardView(ClassTypes.BaseComponent)
AthleticsRewardView.interface = GameConfig.ComponentsConfig.Component
AthleticsRewardView.classPath = "Modules.ModuleAthletics.ChildView.AthleticsRewardView"
local AthleticsData =  PlayerManager.PlayerDataManager.athleticsData
local UIList = ClassTypes.UIList
local AthleticsRewardListItem = ClassTypes.AthleticsRewardListItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
function AthleticsRewardView:Awake()
    self:InitView()
end
function AthleticsRewardView:InitView()
    self._textTitle = self:GetChildComponent("Text_Title","TextMeshWrapper")
    local scrollview,list = UIList.AddScrollViewList(self.gameObject,"ScrollViewList_Rank")
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(AthleticsRewardListItem)
    self._list:SetPadding(0,0,0,0)
    self._list:SetGap(2,0)
    self._list:SetDirection(UIList.DirectionTopToDown,1,1)
    self._textTitle.text = LanguageDataHelper.GetContent(51152)
    self._csBH = self:GetComponent("LuaUIComponent")    
    self._btnclose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnclose,function() self:btnclose() end)

    self._update = function() self:Update() end
end

function AthleticsRewardView:btnclose()
    self:Close()
    self.gameObject:SetActive(false)
    EventDispatcher:TriggerEvent(GameEvents.AthleShowFx)
end
function AthleticsRewardView:Open()
    EventDispatcher:AddEventListener(GameEvents.PVPOffUpdateInfo,self._update)
    
end

function AthleticsRewardView:Close()
    EventDispatcher:RemoveEventListener(GameEvents.PVPOffUpdateInfo,self._update)
end

function AthleticsRewardView:Update()
    local data  =  AthleticsData:GetRankRewards()
    self.gameObject:SetActive(true)    
    self._list:SetDataList(data)
end

function AthleticsRewardView:OnDestory()

end