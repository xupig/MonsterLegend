-- OnlinePvpPanel.lua
local OnlinePvpPanel = Class.OnlinePvpPanel(ClassTypes.BasePanel)
require "Modules.ModuleAthletics.ChildView.OnlinePvpRewardView"
local OnlinePvpRewardView = ClassTypes.OnlinePvpRewardView
require "Modules.ModuleAthletics.ChildView.OnlinePvpHelpView"
local OnlinePvpHelpView = ClassTypes.OnlinePvpHelpView

require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local TaskRewardItem = ClassTypes.TaskRewardItem
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local TaskRewardData = ClassTypes.TaskRewardData

local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local public_config = require("ServerConfig/public_config")
local UIProgressBar = ClassTypes.UIProgressBar
require "UIComponent.Extend.SimpleCurrencyCom"
local SimpleCurrencyCom = ClassTypes.SimpleCurrencyCom
local ButtonWrapper = UIExtension.ButtonWrapper
local Mathf = Mathf
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local DateTimeUtil = GameUtil.DateTimeUtil
local StringStyleUtil = GameUtil.StringStyleUtil

local OnlinePvpManager = PlayerManager.OnlinePvpManager
local AionTowerManager = PlayerManager.AionTowerManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local OnlinePvpReward3DataHelper = GameDataHelper.OnlinePvpReward3DataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIParticle = ClassTypes.UIParticle

function OnlinePvpPanel:Awake()
    self:InitViews()
end

function OnlinePvpPanel:InitViews()
    self._textCount = self:GetChildComponent("Container_Top/Text_Count", "TextMeshWrapper")
    self._btnBuyCount = self:FindChildGO("Container_Top/Button_BuyCount")
    self._csBH:AddClick(self._btnBuyCount, function()self:BuyCountBtnClick() end)
    self._btnHelp = self:FindChildGO("Container_Top/Button_Help")
    self._csBH:AddClick(self._btnHelp, function()self:HelpBtnClick() end)
    
    self._textVP = self:GetChildComponent("Container_Left/Text_VP", "TextMeshWrapper")
    -- self._textPoint = self:GetChildComponent("Container_Left/Text_Point", "TextMeshWrapper")
    self._textExp = self:GetChildComponent("Container_Left/Text_Exp", "TextMeshWrapper")
    self._textRewards = self:GetChildComponent("Container_Left/Text_Rewards", "TextMeshWrapper")
    
    self._containerFight1 = self:FindChildGO("Container_Left/Container_Fight1")
    self._rotationFight1 = self._containerFight1:AddComponent(typeof(XGameObjectTweenRotation))
    self._containerFight5 = self:FindChildGO("Container_Left/Container_Fight5")
    self._rotationFight5 = self._containerFight5:AddComponent(typeof(XGameObjectTweenRotation))
    self._containerFight10 = self:FindChildGO("Container_Left/Container_Fight10")
    self._rotationFight10 = self._containerFight10:AddComponent(typeof(XGameObjectTweenRotation))
    local containerFxFight1 = self:FindChildGO("Container_Left/Container_Fight1Fx")
    local containerFxFight5 = self:FindChildGO("Container_Left/Container_Fight5Fx")
    local containerFxFight10 = self:FindChildGO("Container_Left/Container_Fight10Fx")
    self._fxFight1 = UIParticle.AddParticle(containerFxFight1, "fx_ui_30011")
    self._fxFight5 = UIParticle.AddParticle(containerFxFight5, "fx_ui_30011")
    self._fxFight10 = UIParticle.AddParticle(containerFxFight10, "fx_ui_30011")
    self._btnFight1 = self:FindChildGO("Container_Left/Container_Fight1/Button_Fight1")
    self._csBH:AddClick(self._btnFight1, function()self:Fight1BtnClick() end)
    self._btnFight5 = self:FindChildGO("Container_Left/Container_Fight5/Button_Fight5")
    self._csBH:AddClick(self._btnFight5, function()self:Fight5BtnClick() end)
    self._btnFight10 = self:FindChildGO("Container_Left/Container_Fight10/Button_Fight10")
    self._csBH:AddClick(self._btnFight10, function()self:Fight10BtnClick() end)
    self._imageFight1Get = self:FindChildGO("Container_Left/Container_Fight1/Button_Fight1/Image")
    self._imageFight5Get = self:FindChildGO("Container_Left/Container_Fight5/Button_Fight5/Image")
    self._imageFight10Get = self:FindChildGO("Container_Left/Container_Fight10/Button_Fight10/Image")
    self._imageFight1Get:SetActive(false)
    self._imageFight5Get:SetActive(false)
    self._imageFight10Get:SetActive(false)
    
    self._fightCountProgress = UIProgressBar.AddProgressBar(self.gameObject, "Container_Left/ProgressBar_FightCount")
    self._fightCountProgress:SetProgress(0)
    self._fightCountProgress:ShowTweenAnimate(false)
    self._fightCountProgress:SetMutipleTween(false)
    self._fightCountProgress:SetIsResetToZero(false)
    -- self._fightCountProgress:SetTweenTime(0.5)
    self._containerStage = self:FindChildGO("Container_Middle/Container_Stage")
    self._textStage = self:GetChildComponent("Container_Middle/Text_Stage", "TextMeshWrapper")
    self._textTimeLeft = self:GetChildComponent("Container_Middle/Text_TimeLeft", "TextMeshWrapper")
    
    self._btnStartMatch = self:FindChildGO("Container_Middle/Button_StartMatch")
    self._csBH:AddClick(self._btnStartMatch, function()self:StartMatchBtnClick() end)
    
    self._expProgress = UIProgressBar.AddProgressBar(self.gameObject, "Container_Middle/ProgressBar_Exp")
    self._expProgress:SetProgress(0)
    self._expProgress:ShowTweenAnimate(false)
    self._expProgress:SetMutipleTween(false)
    self._expProgress:SetIsResetToZero(false)
    
    self._btnTotalRewards = self:FindChildGO("Container_Right/Button_TotalRewards")
    self._csBH:AddClick(self._btnTotalRewards, function()self:TotalRewardsBtnClick() end)
    self._btnStageRewards = self:FindChildGO("Container_Right/Button_StageRewards")
    self._csBH:AddClick(self._btnStageRewards, function()self:StageRewardsBtnClick() end)
    self._btnShop = self:FindChildGO("Container_Right/Button_Shop")
    self._csBH:AddClick(self._btnShop, function()self:ShopBtnClick() end)
    self._imageRedPointTotalRewards = self:FindChildGO("Container_Right/Button_TotalRewards/Image_RedPoint")
    self._imageRedPointStageRewards = self:FindChildGO("Container_Right/Button_StageRewards/Image_RedPoint")
    
    self._containerFightTips = self:FindChildGO("Container_FightTips")
    self._containerFightTipsContent = self:FindChildGO("Container_FightTips/Container_Content").transform
    self._btnCloseFightTips = self:FindChildGO("Container_FightTips/Image_BG")
    self._csBH:AddClick(self._btnCloseFightTips, function()self:CloseFightTipsBtnClick() end)
    self._textFightTipsInfo = self:GetChildComponent("Container_FightTips/Container_Content/Text_Info", "TextMeshWrapper")
    self._gridFightTipsLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_FightTips/Container_Content/Container_Items")
    self._gridFightTipsLauoutGroup:SetItemType(TaskRewardItem)
    self._containerFightTips:SetActive(false)
    
    self._btnCloseTips = self:FindChildGO("Container_Tips/Button_Close")
    self._btnTipsCancel = self:FindChildGO("Container_Tips/Button_Cancel")
    self._csBH:AddClick(self._btnCloseTips, function()self:CloseTipsBtnClick() end)
    self._csBH:AddClick(self._btnTipsCancel, function()self:CloseTipsBtnClick() end)
    self._containerTips = self:FindChildGO("Container_Tips")
    self._containerTips:SetActive(false)
    self._textCountDown = self:GetChildComponent("Container_Tips/Text_CountDown", "TextMeshWrapper")
    -- self._textTipsTitle = self:GetChildComponent("Container_Tips/Text_Title", "TextMeshWrapper")
    self._ringContainer = self:FindChildGO("Container_Tips/Container_Ring")
    self._ringRotation = self._ringContainer:AddComponent(typeof(XGameObjectTweenRotation))
    
    self._textGetRewardStage = self:GetChildComponent("Container_GetReward/Text_Stage", "TextMeshWrapper")
    self._btnCloseGetReward = self:FindChildGO("Container_GetReward/Button_Close")
    self._btnGetRewardWrapper = self:GetChildComponent("Container_GetReward/Button_Get", "ButtonWrapper")
    self._btnGetReward = self:FindChildGO("Container_GetReward/Button_Get")
    self._csBH:AddClick(self._btnCloseGetReward, function()self:CloseGetRewardBtnClick() end)
    self._csBH:AddClick(self._btnGetReward, function()self:GetRewardBtnClick() end)
    self._gridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_GetReward/Container_Items")
    self._gridLauoutGroup:SetItemType(TaskRewardItem)
    self._containerGetReward = self:FindChildGO("Container_GetReward")
    self._containerGetReward:SetActive(false)
    
    self._textBuyCountInfo = self:GetChildComponent("Container_BuyCount/Text_Info", "TextMeshWrapper")
    self._textBuyCountCanBuyTime = self:GetChildComponent("Container_BuyCount/Text_CanBuyTime", "TextMeshWrapper")
    self._textBuyCountBuyTime = self:GetChildComponent("Container_BuyCount/Text_BuyTime", "TextMeshWrapper")
    self._containerPrice = self:FindChildGO("Container_BuyCount/Container_Price")
    self._btnBuyCountTimeLower = self:FindChildGO("Container_BuyCount/Button_Low")
    self._csBH:AddClick(self._btnBuyCountTimeLower, function()self:BuyCountTimeLowerBtnClick() end)
    self._btnBuyCountTimeHigher = self:FindChildGO("Container_BuyCount/Button_High")
    self._csBH:AddClick(self._btnBuyCountTimeHigher, function()self:BuyCountTimeHigherBtnClick() end)
    self._btnCloseBuyCount = self:FindChildGO("Container_BuyCount/Button_Close")
    self._btnBuyCountCancel = self:FindChildGO("Container_BuyCount/Button_Cancel")
    self._btnBuyCountBuy = self:FindChildGO("Container_BuyCount/Button_Buy")
    self._csBH:AddClick(self._btnCloseBuyCount, function()self:CloseBuyCountBtnClick() end)
    self._csBH:AddClick(self._btnBuyCountCancel, function()self:CloseBuyCountBtnClick() end)
    self._csBH:AddClick(self._btnBuyCountBuy, function()self:BuyCountBuyBtnClick() end)
    self._diamondCom = self:AddChildLuaUIComponent("Container_BuyCount/Container_Price", SimpleCurrencyCom)
    self._diamondCom:InitComps(public_config.MONEY_TYPE_COUPONS)
    self._containerBuyCount = self:FindChildGO("Container_BuyCount")
    self._containerBuyCount:SetActive(false)
    
    self._containerPopupReward = self:AddChildLuaUIComponent('Container_PopupReward', OnlinePvpRewardView)
    self._containerPopupReward.gameObject:SetActive(false)
    self._containerHelp = self:AddChildLuaUIComponent('Container_Help', OnlinePvpHelpView)
    self._containerHelp.gameObject:SetActive(false)
    
    self._onOnlinePvpGetData = function()self:OnOnlinePvpGetData() end
    self._onBeginMatchSuccess = function()self:OnBeginMatchSuccess() end
    self._onOnlinePvpGetTotalReward = function(id)self:OnOnlinePvpGetTotalReward(id) end
end

function OnlinePvpPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.OnlinePvpGetData, self._onOnlinePvpGetData)
    EventDispatcher:AddEventListener(GameEvents.BeginMatchSuccess, self._onBeginMatchSuccess)
    EventDispatcher:AddEventListener(GameEvents.OnlinePvpGetTotalReward, self._onOnlinePvpGetTotalReward)
end

function OnlinePvpPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.OnlinePvpGetData, self._onOnlinePvpGetData)
    EventDispatcher:RemoveEventListener(GameEvents.BeginMatchSuccess, self._onBeginMatchSuccess)
    EventDispatcher:RemoveEventListener(GameEvents.OnlinePvpGetTotalReward, self._onOnlinePvpGetTotalReward)
end

function OnlinePvpPanel:OnDestroy()
    self._textVP = nil
    self._textCount = nil
    -- self._textPoint = nil
    self._textExp = nil
    self._textRewards = nil
    self._btnStartMatch = nil
end

function OnlinePvpPanel:OnShow()
    self:AddEventListeners()
    OnlinePvpManager:GetData()
end

function OnlinePvpPanel:OnClose()
    self:RemoveEventListeners()
end

function OnlinePvpPanel:OnOnlinePvpGetTotalReward(id)
    self._imageRedPointTotalRewards:SetActive(OnlinePvpManager:HasNotGetStageRewards())
end

function OnlinePvpPanel:OnBeginMatchSuccess()
    self:StopCountDown()
    self._containerTips:SetActive(false)
end

function OnlinePvpPanel:OnOnlinePvpGetData()
    -- LoggerHelper.Log("MONEY_TYPE_ATHLETICS " .. GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_ATHLETICS]])
    local args = OnlinePvpManager:GetServerData()
    local OnlinePvpDataHelper = OnlinePvpManager:GetOnlinePvpDataHelper()
    local totalWinCount = args[public_config.CROSS_VS11_KEY_DATA_TOTAL_WIN]
    local totalFightCount = args[public_config.CROSS_VS11_KEY_DATA_TOTAL_COUNT]
    local fightCount = args[public_config.CROSS_VS11_KEY_DATA_COUNT]
    self._textVP.text = (totalFightCount == 0 and 0 or Mathf.Floor(totalWinCount * 100 / totalFightCount)) .. "%"
    self._textCount.text = (OnlinePvpManager:GetTotalCount() - fightCount) .. "/" .. OnlinePvpManager:GetTotalCount()
    local isCrossServer = OnlinePvpManager:IsCrossServer()
    -- LoggerHelper.Log("isCrossServer " .. tostring(isCrossServer))
    self._btnBuyCount:SetActive(isCrossServer)
    -- self._textPoint.text = args[public_config.CROSS_VS11_KEY_DATA_POINT]
    self._textExp.text = StringStyleUtil.GetLongNumberString(args[public_config.CROSS_VS11_KEY_DATA_EXP])
    self._textRewards.text = args[public_config.CROSS_VS11_KEY_DATA_EXPLOIT]
    
    local curPoint = args[public_config.CROSS_VS11_KEY_DATA_TOTAL_POINT]
    local curStageId, nextStageId = OnlinePvpManager:GetCurStageAndNextStageId()
    local stageName = LanguageDataHelper.CreateContent(OnlinePvpDataHelper:GetName(curStageId))
    self._textStage.text = stageName
    self._textGetRewardStage.text = stageName
    GameWorld.AddIcon(self._containerStage, OnlinePvpDataHelper:GetIcon(curStageId))
    
    self._expProgress:SetProgressByValue(curPoint, OnlinePvpDataHelper:GetPoint(nextStageId), true)
    local fightTotalCount = GlobalParamsHelper.GetParamValue(576)--巅峰竞技实时1v1每日挑战次数
    self._fightCountProgress:SetProgressByValue(fightCount, fightTotalCount, true)
    
    self._btnGetRewardWrapper.interactable = OnlinePvpManager:HasNotGetDailyReward()
    self._imageRedPointStageRewards:SetActive(OnlinePvpManager:HasNotGetDailyReward())
    
    if isCrossServer then
        self._textTimeLeft.text = LanguageDataHelper.CreateContent(51162) .. OnlinePvpManager:GetBattleDateTime()
    else
        self._textTimeLeft.text = ""
    end
    
    self:UpdateCountReward(args)
    self._imageRedPointTotalRewards:SetActive(OnlinePvpManager:HasNotGetStageRewards())
end

function OnlinePvpPanel:UpdateCountReward(args)
    local countReward = args[public_config.CROSS_VS11_KEY_DATA_DAY_COUNT_RWD]
    local fightCount = args[public_config.CROSS_VS11_KEY_DATA_COUNT]
    -- LoggerHelper.Log("UpdateCountReward: countReward[1]: " .. tostring(countReward[1]))
    -- LoggerHelper.Log("UpdateCountReward: countReward[5]: " .. tostring(countReward[5]))
    -- LoggerHelper.Log("UpdateCountReward: countReward[10]: " .. tostring(countReward[10]))
    local fromRot = Vector3.New(0, 0, 15)
    local toRot = Vector3.New(0, 0, -15)
    local normalRot = Vector3.New(0, 0, 0)
    if countReward[1] ~= nil then
        self._imageFight1Get:SetActive(true)
        self._rotationFight1.IsTweening = false
        self._containerFight1.transform.eulerAngles = normalRot
        self._fxFight1:Stop()
    else
        self._imageFight1Get:SetActive(false)
        if fightCount >= 1 then
            self._rotationFight1:SetFromToRotation(fromRot, toRot, 0.5, 4,nil)
            self._fxFight1:Play(true)
        else
            self._rotationFight1.IsTweening = false
            self._containerFight1.transform.eulerAngles = normalRot
            self._fxFight1:Stop()
        end
    end
    if countReward[5] ~= nil then
        self._imageFight5Get:SetActive(true)
        self._rotationFight5.IsTweening = false
        self._containerFight5.transform.eulerAngles = normalRot
        self._fxFight5:Stop()
    else
        self._imageFight5Get:SetActive(false)
        if fightCount >= 5 then
            self._rotationFight5:SetFromToRotation(fromRot, toRot, 0.5, 4,nil)
            self._fxFight5:Play(true)
        else
            self._rotationFight5.IsTweening = false
            self._containerFight5.transform.eulerAngles = normalRot
            self._fxFight5:Stop()
        end
    end
    if countReward[10] ~= nil then
        self._imageFight10Get:SetActive(true)
        self._rotationFight10.IsTweening = false
        self._containerFight10.transform.eulerAngles = normalRot
        self._fxFight10:Stop()
    else
        self._imageFight10Get:SetActive(false)
        if fightCount >= 10 then
            self._rotationFight10:SetFromToRotation(fromRot, toRot, 0.5, 4,nil)
            self._fxFight10:Play(true)
        else
            self._rotationFight10.IsTweening = false
            self._containerFight10.transform.eulerAngles = normalRot
            self._fxFight10:Stop()
        end
    end
end

function OnlinePvpPanel:ShowBuyCount()
    local args = OnlinePvpManager:GetServerData()
    local buyCount = args[public_config.CROSS_VS11_KEY_DATA_BUY_CROSS_COUNT]
    local totalBuyCount = GlobalParamsHelper.GetParamValue(577)--巅峰竞技实时1v1每日可购买挑战次数
    local leftBuyCount = totalBuyCount - buyCount
    if leftBuyCount == 0 then
        local showText = LanguageDataHelper.GetContent(51208)
        GUIManager.ShowText(2, showText)
        return
    end
    self._containerBuyCount:SetActive(true)
    local countLeft = OnlinePvpManager:GetTotalCount() - args[public_config.CROSS_VS11_KEY_DATA_COUNT]
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = countLeft
    self._textBuyCountInfo.text = LanguageDataHelper.CreateContent(51141, argsTable)
    argsTable["0"] = leftBuyCount
    self._textBuyCountCanBuyTime.text = LanguageDataHelper.CreateContent(51142, argsTable)
    self._textBuyCountBuyTime.text = leftBuyCount
    local cost = GlobalParamsHelper.GetParamValue(579)--巅峰竞技实时购买挑战次数花费
    self._diamondCom:SetMoneyNum(leftBuyCount * cost)
end

function OnlinePvpPanel:CloseBuyCountBtnClick()
    self._containerBuyCount:SetActive(false)
end

function OnlinePvpPanel:ShowGetReward()
    self._containerGetReward:SetActive(true)
    local curStageId, nextStageId = OnlinePvpManager:GetCurStageAndNextStageId()
    local OnlinePvpDataHelper = OnlinePvpManager:GetOnlinePvpDataHelper()
    local rewards = {}
    table.insert(rewards, TaskRewardData(public_config.MONEY_TYPE_ATHLETICS, OnlinePvpDataHelper:GetCreditPointReward(curStageId)))
    self._gridLauoutGroup:SetDataList(rewards)

end

function OnlinePvpPanel:CloseGetRewardBtnClick()
    self._containerGetReward:SetActive(false)
end

function OnlinePvpPanel:ShowFightTips(count)
    self._containerFightTips:SetActive(true)
    if count == 1 then
        self._containerFightTipsContent.localPosition = Vector3.New(245, 357, 0)
    elseif count == 5 then
        self._containerFightTipsContent.localPosition = Vector3.New(245, 167, 0)
    else
        self._containerFightTipsContent.localPosition = Vector3.New(245, 7, 0)
    end
    local id = OnlinePvpReward3DataHelper:GetIdByLevelAndTime(AionTowerManager:GetAionInfo()[public_config.AION_NOW_LAYER], count)
    local rewards = OnlinePvpReward3DataHelper:GetReward(id)
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = count
    self._textFightTipsInfo.text = LanguageDataHelper.CreateContent(51218, argsTable)
    local result = {}
    for i, v in ipairs(rewards) do
        table.insert(result, TaskRewardData(v[1], v[2]))
    end
    self._gridFightTipsLauoutGroup:SetDataList(result)
end


function OnlinePvpPanel:CloseFightTipsBtnClick()
    self._containerFightTips:SetActive(false)
end

function OnlinePvpPanel:ShowTips(title)
    self._containerTips:SetActive(true)
    self._ringRotation:SetFromToRotation(Vector3.New(0, 0, 0), Vector3.New(0, 0, -360), 2, 2,nil)
-- self._textTipsTitle.text = text
end

function OnlinePvpPanel:CloseTipsBtnClick()
    self:OnCancelMatch()
end

function OnlinePvpPanel:BuyCountBtnClick()
    self:ShowBuyCount()
end

function OnlinePvpPanel:BuyCountTimeLowerBtnClick()
    local curCount = tonumber(self._textBuyCountBuyTime.text)
    curCount = curCount - 1
    if curCount <= 0 then
        curCount = 1
    end
    self._textBuyCountBuyTime.text = tostring(curCount)
    local cost = GlobalParamsHelper.GetParamValue(579)--巅峰竞技实时购买挑战次数花费
    self._diamondCom:SetMoneyNum(curCount * cost)
end

function OnlinePvpPanel:BuyCountTimeHigherBtnClick()
    local args = OnlinePvpManager:GetServerData()
    local buyCount = args[public_config.CROSS_VS11_KEY_DATA_BUY_CROSS_COUNT]
    local totalBuyCount = GlobalParamsHelper.GetParamValue(577)--巅峰竞技实时1v1每日可购买挑战次数
    local leftBuyCount = totalBuyCount - buyCount
    local curCount = tonumber(self._textBuyCountBuyTime.text)
    curCount = curCount + 1
    if curCount > leftBuyCount then
        curCount = leftBuyCount
    end
    self._textBuyCountBuyTime.text = tostring(curCount)
    local cost = GlobalParamsHelper.GetParamValue(579)--巅峰竞技实时购买挑战次数花费
    self._diamondCom:SetMoneyNum(curCount * cost)
end

function OnlinePvpPanel:BuyCountBuyBtnClick()
    local curCount = tonumber(self._textBuyCountBuyTime.text)
    if self._diamondCom:IsLackMoney() then
        GUIManager.ShowChargeMessageBox()
        return
    end
    OnlinePvpManager:BuyCount(curCount)
    self:CloseBuyCountBtnClick()
end

function OnlinePvpPanel:Fight1BtnClick()
    local args = OnlinePvpManager:GetServerData()
    local countReward = args[public_config.CROSS_VS11_KEY_DATA_DAY_COUNT_RWD]
    if countReward[1] ~= nil then
        return
    end
    local fightCount = args[public_config.CROSS_VS11_KEY_DATA_COUNT]
    if fightCount >= 1 then
        OnlinePvpManager:GetDailyCountReward(1)
    else
        self:ShowFightTips(1)
    end
end

function OnlinePvpPanel:Fight5BtnClick()
    local args = OnlinePvpManager:GetServerData()
    local countReward = args[public_config.CROSS_VS11_KEY_DATA_DAY_COUNT_RWD]
    if countReward[5] ~= nil then
        return
    end
    local fightCount = args[public_config.CROSS_VS11_KEY_DATA_COUNT]
    if fightCount >= 5 then
        OnlinePvpManager:GetDailyCountReward(5)
    else
        self:ShowFightTips(5)
    end
end

function OnlinePvpPanel:Fight10BtnClick()
    local args = OnlinePvpManager:GetServerData()
    local countReward = args[public_config.CROSS_VS11_KEY_DATA_DAY_COUNT_RWD]
    if countReward[10] ~= nil then
        return
    end
    local fightCount = args[public_config.CROSS_VS11_KEY_DATA_COUNT]
    if fightCount >= 10 then
        OnlinePvpManager:GetDailyCountReward(10)
    else
        self:ShowFightTips(10)
    end
end

function OnlinePvpPanel:GetRewardBtnClick()
    OnlinePvpManager:GetStageReward()
end

function OnlinePvpPanel:TotalRewardsBtnClick()
    self._containerPopupReward.gameObject:SetActive(true)
    local isCrossServer = OnlinePvpManager:IsCrossServer()
    if isCrossServer then
        self._containerPopupReward:OnTabBarClick(0)
    else
        self._containerPopupReward:OnTabBarClick(2)
    end
end

function OnlinePvpPanel:HelpBtnClick()
    self._containerHelp.gameObject:SetActive(true)
    self._containerHelp:ShowView()
end

function OnlinePvpPanel:StageRewardsBtnClick()
    self:ShowGetReward()
end

function OnlinePvpPanel:ShopBtnClick()
    local data = {["id"] = EnumType.ShopPageType.Honor}
    GUIManager.ShowPanel(PanelsConfig.Shop, data)
end

function OnlinePvpPanel:StartMatchBtnClick()
    local args = OnlinePvpManager:GetServerData()
    if args then
        local fightCount = args[public_config.CROSS_VS11_KEY_DATA_COUNT]
        if fightCount == OnlinePvpManager:GetTotalCount() then
            local totalBuyCount = GlobalParamsHelper.GetParamValue(577)--巅峰竞技实时1v1每日可购买挑战次数
            local curBuyCount = args[public_config.CROSS_VS11_KEY_DATA_BUY_CROSS_COUNT]
            if totalBuyCount == curBuyCount then
                local showText = LanguageDataHelper.GetContent(51211)
                GUIManager.ShowText(2, showText)
            else
                local showText = LanguageDataHelper.GetContent(51210)
                GUIManager.ShowText(2, showText)
            end
            return
        end
        
        OnlinePvpManager:BeginMatch()
        self._cancelTime = DateTimeUtil:GetServerTime() + GlobalParamsHelper.GetParamValue(584)--巅峰竞技实时1v1匹配倒计时（单位：秒）
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnCancelMatchCountDown() end)
        self:ShowTips("")
        self:UpdateTimeLeft()
    end
end

function OnlinePvpPanel:UpdateTimeLeft()
    local leftTime = self._cancelTime - DateTimeUtil:GetServerTime()
    --self._txtInfo.text = LanguageDataHelper.CreateContent(56032) .. leftTime .. LanguageDataHelper.CreateContent(56033)
    self._textCountDown.text = "(" .. tostring(leftTime) .. ")"
end

function OnlinePvpPanel:OnCancelMatchCountDown()
    if (self._cancelTime - DateTimeUtil:GetServerTime() > 0) then
        self:UpdateTimeLeft()
    else
        local showText = LanguageDataHelper.GetContent(51212)
        GUIManager.ShowText(2, showText)
        self:OnCancelMatch()
    end
end

function OnlinePvpPanel:StopCountDown()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function OnlinePvpPanel:OnCancelMatch()
    self:StopCountDown()
    self._containerTips:SetActive(false)
    OnlinePvpManager:CancelMatch()
end

function OnlinePvpPanel:CloseBtnClick()
-- GUIManager.ClosePanel(PanelsConfig.Athletics)
end

-- -- 0 没有  1 暗红底板  2 蓝色底板  3 遮罩底板
function OnlinePvpPanel:CommonBGType()
    return PanelBGType.NoBG
end

function OnlinePvpPanel:WinBGType()
    return PanelWinBGType.FullSceenBG
end
