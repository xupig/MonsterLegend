AthleticsModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function AthleticsModule.Init()
    GUIManager.AddPanel(PanelsConfig.AthleticsEnter,"Panel_AthleticsEnter",GUILayer.LayerUIPanel,"Modules.ModuleAthletics.AthleticsEnterPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.Athletics,"Panel_Athletics",GUILayer.LayerUIPanel,"Modules.ModuleAthletics.AthleticsPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.OnlinePvp,"Panel_OnlinePvp",GUILayer.LayerUIPanel,"Modules.ModuleAthletics.OnlinePvpPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.OffPVPHead,"Panel_OffPvpHead",GUILayer.LayerUICG,"Modules.ModuleAthletics.OffPvpHeadPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return AthleticsModule