LogsModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function LogsModule.Init()
    GUIManager.AddPanel(PanelsConfig.Logs,"Panel_Logs",GUILayer.LayerUIPanel,"Modules.ModuleLogs.LogsPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return LogsModule