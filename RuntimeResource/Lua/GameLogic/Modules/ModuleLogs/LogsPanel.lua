-- LogsPanel.lua
local LogsPanel = Class.LogsPanel(ClassTypes.BasePanel)
local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LogsManager = PlayerManager.LogsManager
local UIComponentUtil = GameUtil.UIComponentUtil
function LogsPanel:Awake()
    self:InitViews()
    self._updateLogs=function() self:UpdateLogs()end
end

function LogsPanel:InitViews()
    self._scroll = UIComponentUtil.FindChild(self.gameObject, "Container_Logs/ScrollView")
    self._scrollContent = self:FindChildGO("Container_Logs/ScrollView/Mask/Content")
    self._rectTransform = self._scrollContent:GetComponent(typeof(UnityEngine.RectTransform))
    self._txtDesc = self:GetChildComponent("Container_Logs/ScrollView/Mask/Content/Text", 'TextMeshWrapper')
    self._rectTransformScroll = self._scroll:GetComponent(typeof(UnityEngine.RectTransform))
    self._height = self._rectTransform.sizeDelta.y
    self._width = self._rectTransform.sizeDelta.x
    self._txtPage = self:GetChildComponent("Text_Page", 'TextMeshWrapper')

    self._btGo = self:FindChildGO("Button_Open")
    self._csBH:AddClick(self._btGo,function() self:OpenBtnClick() end)
    self._btLast = self:FindChildGO("Button_Last")
    self._csBH:AddClick(self._btLast,function() self:LastBtnClick() end)
    self._btNext = self:FindChildGO("Button_Next")
    self._csBH:AddClick(self._btNext,function() self:NextBtnClick() end)
end



function LogsPanel:UpdateLogs()
    local content = GameWorld.GetCurrPageContent()
    self._txtPage.text = "" .. GameWorld.GetCurrPage() .. "/" .. GameWorld.GetTotalPage()
    self._txtDesc.text = content
    if self._txtDesc.preferredHeight > self._height then
        self._rectTransform.sizeDelta = Vector2(self._width, self._txtDesc.preferredHeight)
    else
        self._rectTransform.sizeDelta = Vector2(self._width, self._height)
    end
end
    


function LogsPanel:OnDestroy()
end

function LogsPanel:OnShow()
    GameWorld.GetTodayLogText()
    self:UpdateLogs()
end

function LogsPanel:OnClose()
    GameWorld.ClearLog()
end


function LogsPanel:OpenBtnClick()
    GameWorld.OpenTodayLogFile()
end

function LogsPanel:LastBtnClick()
    GameWorld.LastPage()
    self:UpdateLogs()
end

function LogsPanel:NextBtnClick()
    GameWorld.NextPage()
    self:UpdateLogs()
end

-- -- 0 没有  1 暗红底板  2 蓝色底板  3 遮罩底板
function LogsPanel:CommonBGType()
    return PanelBGType.NoBG
end

function LogsPanel:WinBGType()
    return PanelWinBGType.NormalBG
end
