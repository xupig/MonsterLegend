local RoleFashionWeaponResolveItem = Class.RoleFashionWeaponResolveItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RoleFashionWeaponResolveItem.interface = GameConfig.ComponentsConfig.Component
RoleFashionWeaponResolveItem.classPath = "Modules.ModuleRole.ChildComponent.RoleFashionWeaponResolveItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local fashionData = PlayerManager.PlayerDataManager.fashionData
local FashionDataHelper = GameDataHelper.FashionDataHelper

function RoleFashionWeaponResolveItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self:InitView()
end

function RoleFashionWeaponResolveItem:OnDestroy()
    self.data = nil
end

--override
function RoleFashionWeaponResolveItem:OnRefreshData(data)
    self.data = data
    self:Reset()
    if self:CanSelected() then
        self._icon:SetItem(data.cfg_id)
    else
        self._icon:SetItem(data.cfg_id, nil, 12)
    end
end

function RoleFashionWeaponResolveItem:InitView()
    local parent = self:FindChildGO("Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._selectImage = self:FindChildGO("Image_Selected")
end

function RoleFashionWeaponResolveItem:ChangeState()
    if not self:CanSelected() then
        return
    end
    self._select = not self._select
    self._selectImage:SetActive(self._select)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Fashion_Selected_Weapon_Item, self.data.bagPos, self._select)
end

function RoleFashionWeaponResolveItem:Selected()
    if not self:CanSelected() then
        return
    end
    self._select = true
    self._selectImage:SetActive(self._select)
    if self.data ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Fashion_Selected_Weapon_Item, self.data.bagPos, self._select)
    end
end

function RoleFashionWeaponResolveItem:Reset()
    self._select = false
    self._selectImage:SetActive(self._select)
end

function RoleFashionWeaponResolveItem:CanSelected()
    if self.data.cfg_id == nil then
        return false
    end
    local star = fashionData:GetFashionActiveInfo(self.data.cfg_id)
    local maxStar = FashionDataHelper.GetFashionMaxStar(self.data.cfg_id)
    if (star < 0) or (star < maxStar) then
        return false
    end
    return true
end