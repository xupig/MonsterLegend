-- RoleTitleFirstLevelItem.lua
--一级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local RoleTitleFirstLevelItem = Class.RoleTitleFirstLevelItem(UINavigationMenuItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
RoleTitleFirstLevelItem.interface = GameConfig.ComponentsConfig.PointableComponent
RoleTitleFirstLevelItem.classPath = "Modules.ModuleRole.ChildComponent.RoleTitleFirstLevelItem"

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function RoleTitleFirstLevelItem:Awake()
    UINavigationMenuItem.Awake(self)
    self._isExpanded = false
    self:IninView()
end

function RoleTitleFirstLevelItem:OnDestroy()

end

function RoleTitleFirstLevelItem:IninView()
    self._titleText = self:FindChildGO("Text_Title"):GetComponent("TextMeshWrapper")
	self._imgContract = self:FindChildGO("Container_Arrow/Image_Contract")
    self._imgExpand = self:FindChildGO("Container_Arrow/Image_Expand")
    self._selectedImage = self:FindChildGO("Image_Selected")
end

function RoleTitleFirstLevelItem:OnPointerDown(pointerEventData)

end

function RoleTitleFirstLevelItem:ToggleSelected()
    if self._isExpanded then
		self._isExpanded = false
		self._imgContract:SetActive(true)
        self._imgExpand:SetActive(false)
        self._selectedImage:SetActive(false)
	else
		self._isExpanded = true
		self._imgContract:SetActive(false)
        self._imgExpand:SetActive(true)
        self._selectedImage:SetActive(true)
	end
end

function RoleTitleFirstLevelItem:Reset()
    if self._isExpanded then
		self._isExpanded = false
		self._imgContract:SetActive(true)
        self._imgExpand:SetActive(false)
        self._selectedImage:SetActive(false)
	end
end


--override {type}
function RoleTitleFirstLevelItem:OnRefreshData(data)
    self.data = data
    local type = data[1]
    local config = GlobalParamsHelper.GetParamValue(445)
    self._titleText.text = LanguageDataHelper.CreateContent(config[type])
end