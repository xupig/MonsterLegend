-- RolePKItem.lua
local RolePKItem = Class.RolePKItem(ClassTypes.BaseComponent)

RolePKItem.interface = GameConfig.ComponentsConfig.Component
RolePKItem.classPath = "Modules.ModuleRole.ChildComponent.RolePKItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local BagManager = PlayerManager.BagManager
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ShopDataHelper = GameDataHelper.ShopDataHelper
local ShopManager = PlayerManager.ShopManager
local GUIManager = GameManager.GUIManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local TipsManager = GameManager.TipsManager

function RolePKItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._icon:ActivateTipsById()

	self._btnUse = self:FindChildGO("Button_Use")
	self._csBH:AddClick(self._btnUse, function() self:OnUseClick() end )
end

function RolePKItem:UpdateData(itemId)
	self._itemId = itemId
	self._icon:SetItem(itemId)
end

function RolePKItem:UpdateCount(count)
	if count > 0 and self._btnBuy then
		self._btnBuy:SetActive(false)
		self._txtPrice.gameObject:SetActive(false)
		self.moneyIcon:SetActive(false)
	end
	self._icon:SetCountString(tostring(count))
	if count > 0 then
		self._btnUse:SetActive(true)
	end
end

function RolePKItem:OnUseClick()
	local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._itemId)
	local effectId = ItemDataHelper.GetItemEffect(self._itemId)
    if pos > 0 then
        BagManager:RequsetUse(pos,1,0)
    end
end

function RolePKItem:ShowBuy()
	if self._btnBuy == nil then
		self._txtPrice = self:GetChildComponent("Text_Price","TextMeshWrapper")
		self.moneyIcon = self:FindChildGO("Container_Money")
		self._btnBuy = self:FindChildGO("Button_Buy")
		self._csBH:AddClick(self._btnBuy, function() self:OnBuy() end )
	end
	self._txtPrice.gameObject:SetActive(true)
	self:ShowPrice()
	self.moneyIcon:SetActive(true)
	self._btnBuy:SetActive(true)
	self._btnUse:SetActive(false)
end

function RolePKItem:ShowPrice()
	local shopItemList = ShopDataHelper:GetShopDataByItemId(self._itemId)
    local type, price

	for i=1,#shopItemList do
        local data = shopItemList[i]
		type, price = ShopDataHelper:GetItemTypeAndOriPrice(data.__id)
	end

    self.moneyType = type
    self.price = price
	self._txtPrice.text = price
    GameWorld.AddIcon(self.moneyIcon,ItemDataHelper.GetIcon(self.moneyType))
end

function RolePKItem:OnBuy()
    self:BuyGuideTip(self._itemId)
end

function RolePKItem:BuyGuideTip(itemId)
    if ItemDataHelper.GetGuideBuyCost(itemId)~=0 then
        local btnArgs = {}
        btnArgs.guideBuy = {itemId}
        TipsManager:ShowItemTipsById(itemId,btnArgs)
    end
end