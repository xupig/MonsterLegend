local RoleFashionClothTotalAttriItem = Class.RoleFashionClothTotalAttriItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RoleFashionClothTotalAttriItem.interface = GameConfig.ComponentsConfig.Component
RoleFashionClothTotalAttriItem.classPath = "Modules.ModuleRole.ChildComponent.RoleFashionClothTotalAttriItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local AttriDataHelper = GameDataHelper.AttriDataHelper

function RoleFashionClothTotalAttriItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self._active = false
    self._putOn = false
    self:InitView()
end

function RoleFashionClothTotalAttriItem:OnDestroy()
    self.data = nil
end

--override data {attri, value}
function RoleFashionClothTotalAttriItem:OnRefreshData(data)
    self.data = data
    self:ShowView()
end

function RoleFashionClothTotalAttriItem:InitView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._valueText = self:GetChildComponent("Text_Value", "TextMeshWrapper")
end

function RoleFashionClothTotalAttriItem:ShowView()
    self._nameText.text = AttriDataHelper:GetName(self.data[1])
    self._valueText.text = self.data[2]
end