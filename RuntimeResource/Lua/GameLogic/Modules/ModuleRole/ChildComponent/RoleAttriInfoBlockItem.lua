local RoleAttriInfoBlockItem = Class.RoleAttriInfoBlockItem(ClassTypes.BaseLuaUIComponent)

RoleAttriInfoBlockItem.interface = GameConfig.ComponentsConfig.Component
RoleAttriInfoBlockItem.classPath = "Modules.ModuleRole.ChildComponent.RoleAttriInfoBlockItem"

local AttriDataHelper = GameDataHelper.AttriDataHelper

function RoleAttriInfoBlockItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function RoleAttriInfoBlockItem:OnDestroy() 

end

function RoleAttriInfoBlockItem:InitView()
    self._nameText = self:GetChildComponent("Text_Attri", "TextMeshWrapper")
    self._valueText = self:GetChildComponent("Text_Attri/Text_Value", "TextMeshWrapper")
end

function RoleAttriInfoBlockItem:SetData(data)
    self._nameText.text = AttriDataHelper:GetName(data.id)
    self._valueText.text = data.value
end
