UIListItem = ClassTypes.UIListItem
local StarEnterItem = Class.StarEnterItem(ClassTypes.UIListItem)
StarEnterItem.interface = GameConfig.ComponentsConfig.Component
StarEnterItem.classPath = "Modules.ModuleRole.ChildComponent.StarEnterItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function StarEnterItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function StarEnterItem:OnDestroy()
    self.data = nil
end

function StarEnterItem:InitView()
    local _btn = self:FindChildGO("Button")
    self._csBH:AddClick(_btn, function()self:OnBtnEnter() end)
    self._icon = self:FindChildGO("Button/Image_Icon")
    self._nameText = self:GetChildComponent("Button/Text", "TextMeshWrapper")
end

--data: {attriId, curValue, delta}
function StarEnterItem:OnRefreshData(data)
    self.data = data
    self._nameText.text = LanguageDataHelper.GetContent(data[2])
    GameWorld.AddIcon(self._icon,data[3])
end

function StarEnterItem:OnBtnEnter()
    if FunctionOpenDataHelper:CheckFunctionOpen(self.data[1]) then
        GUIManager.ShowPanelByFunctionId(self.data[1])
    else      
        local text = LanguageDataHelper.GetContent(FunctionOpenDataHelper:GetFunctionOpenName(self.data[1]))
        local showText = LanguageDataHelper.CreateContentWithArgs(80916, {["0"]=text})
        GUIManager.ShowText(2, showText)
    end
end