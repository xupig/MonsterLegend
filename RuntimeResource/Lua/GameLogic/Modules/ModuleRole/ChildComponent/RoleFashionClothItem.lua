local RoleFashionClothItem = Class.RoleFashionClothItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RoleFashionClothItem.interface = GameConfig.ComponentsConfig.Component
RoleFashionClothItem.classPath = "Modules.ModuleRole.ChildComponent.RoleFashionClothItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local FashionManager = PlayerManager.FashionManager
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local bagData = PlayerManager.PlayerDataManager.bagData
local FashionDataHelper = GameDataHelper.FashionDataHelper

local Unit_Width = 20
local setV2 = Vector2.New(0,0)

function RoleFashionClothItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self._active = false
    self._putOn = false
    self:InitView()
end

function RoleFashionClothItem:OnDestroy()
    self.data = nil
end

--override data {[id], [hasGot], [star]}
function RoleFashionClothItem:OnRefreshData(data)
    self.data = data
    self:ShowView()
end

function RoleFashionClothItem:InitView()
    local parent = self:FindChildGO("Container_content/Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._icon:SetValues({["noAnimFrame"] = true})

    self._nameText = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._selectImage = self:FindChildGO("Container_content/Image_Selected")
    self._notActiveGo = self:FindChildGO("Container_content/Text_NotActive")
    self._putOnGo = self:FindChildGO("Container_content/Image_PutOn")
    self._starImageGo = self:FindChildGO("Container_content/Image_Star")
    self._starImageRect = self:GetChildComponent("Container_content/Image_Star/Image_Glod", 'RectTransform')
    self._starImage = self:GetChildComponent("Container_content/Image_Star/Image_Glod", 'ImageWrapper')
    self._starImage:SetContinuousDimensionDirty(true)
    setV2.y = self._starImageRect.sizeDelta.y
    self._redPointImage = self:FindChildGO("Container_content/Image_RedPoint")
end

function RoleFashionClothItem:ShowView()
    local itemId = self.data.id
    self._icon:SetItem(itemId)
    local name = ItemDataHelper.GetItemName(itemId)
    if self.data.star > -1 then
        self._active = true
        self._nameText.text = name
    else
        self._active = false
        self._nameText.text = BaseUtil.GetColorString(name, ColorConfig.C)
    end
    self._putOn = FashionManager:IsFashionLoaded(itemId)
    self._notActiveGo:SetActive(not self._active)
    self._putOnGo:SetActive(self._putOn)
    self._starImageGo:SetActive(self._active)
    self:SetStarLevel()
    self:RefreshRedPoint()
end

function RoleFashionClothItem:ShowSelect(state)
    self._selectImage:SetActive(state)
end

function RoleFashionClothItem:SetStarLevel()
    setV2.x = Unit_Width * self.data.star
    self._starImageRect.sizeDelta = setV2
end

function RoleFashionClothItem:RefreshRedPoint()
    local show = false
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self.data.id)
    if self.data.star == -1 then
        if ownCount > 0 then
            show = true
        end
    else
        local starData = FashionDataHelper.GetStarDataByIdAndStar(self.data.id, self.data.star)
        if starData.star_lvup_cost ~= nil then
            local _, count = next(starData.star_lvup_cost)
            if ownCount >= count then
                show = true
            end
        end
    end
    self._redPointImage:SetActive(show)
end