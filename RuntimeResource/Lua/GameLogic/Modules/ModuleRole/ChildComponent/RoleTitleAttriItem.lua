local RoleTitleAttriItem = Class.RoleTitleAttriItem(ClassTypes.UIListItem)

RoleTitleAttriItem.interface = GameConfig.ComponentsConfig.Component
RoleTitleAttriItem.classPath = "Modules.ModuleRole.ChildComponent.RoleTitleAttriItem"

local AttriDataHelper = GameDataHelper.AttriDataHelper

function RoleTitleAttriItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function RoleTitleAttriItem:OnDestroy() 

end

function RoleTitleAttriItem:InitView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._valueText = self:GetChildComponent("Text_Value", "TextMeshWrapper")
end

-- {[attri], [value]}
function RoleTitleAttriItem:OnRefreshData(data)
    self.data = data
    self._nameText.text = AttriDataHelper:GetName(data.attri)
    self._valueText.text = data.value
end