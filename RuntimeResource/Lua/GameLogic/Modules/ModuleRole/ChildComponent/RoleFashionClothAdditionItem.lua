local RoleFashionClothAdditionItem = Class.RoleFashionClothAdditionItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RoleFashionClothAdditionItem.interface = GameConfig.ComponentsConfig.Component
RoleFashionClothAdditionItem.classPath = "Modules.ModuleRole.ChildComponent.RoleFashionClothAdditionItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local AttriDataHelper = GameDataHelper.AttriDataHelper

function RoleFashionClothAdditionItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self._active = false
    self._putOn = false
    self:InitView()
end

function RoleFashionClothAdditionItem:OnDestroy()
    self.data = nil
end

--override data {id, value}
function RoleFashionClothAdditionItem:OnRefreshData(data)
    self.data = data
    self:ShowView()
end

function RoleFashionClothAdditionItem:InitView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._valueText = self:GetChildComponent("Text_Value", "TextMeshWrapper")
end

function RoleFashionClothAdditionItem:ShowView()
    self._nameText.text = AttriDataHelper:GetName(self.data[1])
    self._valueText.text = (self.data[2] * 0.01).."%"
end