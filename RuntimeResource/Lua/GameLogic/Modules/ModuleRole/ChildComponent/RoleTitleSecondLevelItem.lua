-- RoleTitleSecondLevelItem.lua
--二级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local RoleTitleSecondLevelItem = Class.RoleTitleSecondLevelItem(UINavigationMenuItem)

RoleTitleSecondLevelItem.interface = GameConfig.ComponentsConfig.PointableComponent
RoleTitleSecondLevelItem.classPath = "Modules.ModuleRole.ChildComponent.RoleTitleSecondLevelItem"

local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TitleDataHelper = GameDataHelper.TitleDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local TitleManager = PlayerManager.TitleManager

function RoleTitleSecondLevelItem:Awake()
    UINavigationMenuItem.Awake(self)
    self:IninView()
end

function RoleTitleSecondLevelItem:OnDestroy()

end

function RoleTitleSecondLevelItem:IninView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._selectedImage = self:FindChildGO("Image_Selected")
end

function RoleTitleSecondLevelItem:OnPointerDown(pointerEventData)

end

--override {titleId}
function RoleTitleSecondLevelItem:OnRefreshData(data)
    self.data = data
    local text = TitleDataHelper.GetTitleName(data[1])
    local state = TitleManager:GetStateByTitleId(data[1])
    if state == 0 then
        self._nameText.text = BaseUtil.GetColorString(text, ColorConfig.C)
    else
        self._nameText.text = BaseUtil.GetColorString(text, ColorConfig.A)
    end
end

function RoleTitleSecondLevelItem:OnSelected(state)
    self._selectedImage:SetActive(state)
end