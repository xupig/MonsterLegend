UIListItem = ClassTypes.UIListItem
local StarAttrItem = Class.StarAttrItem(ClassTypes.UIListItem)
StarAttrItem.interface = GameConfig.ComponentsConfig.Component
StarAttrItem.classPath = "Modules.ModuleRole.ChildComponent.StarAttrItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local AttriDataHelper = GameDataHelper.AttriDataHelper

function StarAttrItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function StarAttrItem:OnDestroy()
    self.data = nil
end

function StarAttrItem:InitView()
    self._nameText = self:GetChildComponent("Text_AttrName", "TextMeshWrapper")
    self._valueText = self:GetChildComponent("Text_AttrValue", "TextMeshWrapper")
    self._addValueText = self:GetChildComponent("Text_AddAttrValue", "TextMeshWrapper")
end

--data: {attriId, curValue, delta}
function StarAttrItem:OnRefreshData(data)
    self.data = data
    self._nameText.text = AttriDataHelper:GetName(data['attri'])
    self._valueText.text = AttriDataHelper:ConvertAttriValue(data['attri'],data['value'])
    self._addValueText.text = data['madd']
end