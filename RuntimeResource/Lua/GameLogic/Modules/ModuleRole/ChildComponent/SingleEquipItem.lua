-- SingleEquipItem.lua
-- 单件装备Item
local SingleEquipItem = Class.SingleEquipItem(ClassTypes.BaseLuaUIComponent)
SingleEquipItem.interface = GameConfig.ComponentsConfig.Component
SingleEquipItem.classPath = "Modules.ModuleRole.ChildComponent.SingleEquipItem"
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local bagData = PlayerManager.PlayerDataManager.bagData
local EquipDataHelper = GameDataHelper.EquipDataHelper
local public_config = GameWorld.public_config

function SingleEquipItem:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._container = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)

	self._csBH:AddClick(self.gameObject, function() self:OnEquipClick() end)
end

function SingleEquipItem:OnEquipClick()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_PLAYER_EQUIP_CLICK,self._equipData,self._index)
end

function SingleEquipItem:UpdateData(equipData,index)
	self._equipData = equipData
	self._index = index
	self._container:SetItemData(equipData)
	--self._imgTypeIcon:SetActive(equipData == nil)
end

function SingleEquipItem:ActivateSuitIcon(suitData)
	self._container:ActivateSuitIcon(suitData)
end

-- function SingleEquipItem:ShowCanStrength(shouldShowCanStrength)
-- 	if shouldShowCanStrength then
-- 		self:UpdateCanStrength()
-- 	else
-- 		self._imgCanStrength:SetActive(false)
-- 	end
-- end

--更新是否能强化
-- function SingleEquipItem:UpdateCanStrength()
-- 	if self._equipData then
-- 		--强化配置
-- 		local eLv = self._equipData:EquipStrenLevel()
-- 		if eLv == nil then
-- 			eLv = 0
-- 		end
-- 		local equipStrengthCfg = EquipDataHelper.GetStrengthData(eLv)
-- 		local matId
-- 		local needNum
-- 		for k,v in pairs(equipStrengthCfg.equip_stren_cost) do
-- 			if k ~= public_config.MONEY_TYPE_GOLD then
-- 				matId = k
-- 				needNum = v
-- 			end
-- 		end

-- 		local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(matId)
-- 		--强化需求数量
-- 		if bagNum >= needNum then
-- 			self._imgCanStrength:SetActive(true)
-- 		else
-- 			self._imgCanStrength:SetActive(false)
-- 		end
-- 	else
-- 		self._imgCanStrength:SetActive(false)
-- 	end
-- end
