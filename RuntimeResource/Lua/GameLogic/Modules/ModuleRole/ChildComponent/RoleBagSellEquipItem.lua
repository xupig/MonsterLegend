local RoleBagSellEquipItem = Class.RoleBagSellEquipItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RoleBagSellEquipItem.interface = GameConfig.ComponentsConfig.Component
RoleBagSellEquipItem.classPath = "Modules.ModuleRole.ChildComponent.RoleBagSellEquipItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil

function RoleBagSellEquipItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self:InitView()
end

function RoleBagSellEquipItem:OnDestroy()
    self.data = nil
end

--override
function RoleBagSellEquipItem:OnRefreshData(data)
    self.data = data
    self._icon:SetItem(data.cfg_id)
    if self._select then
        self:Selected()
    end
end

function RoleBagSellEquipItem:InitView()
    local parent = self:FindChildGO("Container_content/Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._selectImage = self:FindChildGO("Container_content/Image_Selected")
end

function RoleBagSellEquipItem:ChangeState()
    if self.data.cfg_id == nil then
        return
    end
    self._select = not self._select
    self._selectImage:SetActive(self._select)
    EventDispatcher:TriggerEvent(GameEvents.REFRESH_BAG_SELL_SELECTED_EQUIP_ITEM, self.data, self._select)
end

function RoleBagSellEquipItem:Selected()
    self._select = true
    self._selectImage:SetActive(self._select)
end

function RoleBagSellEquipItem:Reset()
    self._select = false
    self._selectImage:SetActive(self._select)
end