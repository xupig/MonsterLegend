-- TitleTipsItem.lua
local TitleTipsItem = Class.TitleTipsItem(ClassTypes.BaseComponent)

TitleTipsItem.interface = GameConfig.ComponentsConfig.Component
TitleTipsItem.classPath = "Modules.ModuleRole.ChildComponent.TitleTipsItem"

local closeTime = 3
local TitleDataHelper = GameDataHelper.TitleDataHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

--override
function TitleTipsItem:Awake()
    self._button = self:FindChildGO("Button_Title")
    self._csBH:AddClick(self._button, function() self:OnAchievementButtonClick() end)

    self._IconContainer = self:FindChildGO("Button_Title/Container_Icon")

    self._initLocalPosition = self.transform.localPosition
    self._containerPos = self.gameObject:AddComponent(typeof(XGameObjectTweenPosition))
end

function TitleTipsItem:OnDestroy()
    self:ClearTimer()
    if  self.gameObject then 
        self.gameObject:SetActive(false)
    end
end

function TitleTipsItem:SetAchievementId(id)
    self._titleId = id

    local icon = TitleDataHelper.GetTitleShowName(id)
    GameWorld.AddIconAnim(self._IconContainer, icon)
    GameWorld.SetIconAnimSpeed(self._IconContainer, 5)
end

function TitleTipsItem:OnAchievementButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_SHOWTITLE_ITEM_CLICK, self._titleId)
end

function TitleTipsItem:ShowView()
    self._isMove = false
    self.gameObject:SetActive(true)
    self:AddTimer()
    self.transform.localPosition = Vector3.New(0,0,0)
end

function TitleTipsItem:Move()
    self._isMove = true
    self._containerPos:SetFromToPos(Vector3.New(0,0,0), Vector3(0, 160, 0), 0.5, 1, nil)
end

function TitleTipsItem:IsMove()
    return self._isMove
end

function TitleTipsItem:Hide()
    self:ClearTimer()
    self.gameObject:SetActive(false)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_TITLE_ITEM, self._titleId)
end

function TitleTipsItem:AddTimer()
    self:ClearTimer()
    self._timer = TimerHeap:AddSecTimer(closeTime, 0, 0, function()
        self:ClearTimer()
        self.gameObject:SetActive(false)
        EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_TITLE_ITEM, self._titleId)
    end)
end

function TitleTipsItem:ClearTimer()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    if self.transform then
        self._containerPos.IsTweening = false
        self.transform.localPosition = self._initLocalPosition
    end
end