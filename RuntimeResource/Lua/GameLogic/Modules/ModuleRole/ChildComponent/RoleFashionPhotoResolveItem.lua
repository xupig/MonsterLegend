local RoleFashionPhotoResolveItem = Class.RoleFashionPhotoResolveItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RoleFashionPhotoResolveItem.interface = GameConfig.ComponentsConfig.Component
RoleFashionPhotoResolveItem.classPath = "Modules.ModuleRole.ChildComponent.RoleFashionPhotoResolveItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local fashionData = PlayerManager.PlayerDataManager.fashionData
local FashionDataHelper = GameDataHelper.FashionDataHelper

function RoleFashionPhotoResolveItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self:InitView()
end

function RoleFashionPhotoResolveItem:OnDestroy()
    self.data = nil
end

--override
function RoleFashionPhotoResolveItem:OnRefreshData(data)
    self.data = data
    self:Reset()
    if self:CanSelected() then
        self._icon:SetItem(data.cfg_id)
    else
        self._icon:SetItem(data.cfg_id, nil, 12)
    end
end

function RoleFashionPhotoResolveItem:InitView()
    local parent = self:FindChildGO("Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._selectImage = self:FindChildGO("Image_Selected")
end

function RoleFashionPhotoResolveItem:ChangeState()
    if not self:CanSelected() then
        return
    end
    self._select = not self._select
    self._selectImage:SetActive(self._select)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Fashion_Selected_Photo_Item, self.data.bagPos, self._select)
end

function RoleFashionPhotoResolveItem:Selected()
    if not self:CanSelected() then
        return
    end
    self._select = true
    self._selectImage:SetActive(self._select)
    if self.data ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Fashion_Selected_Photo_Item, self.data.bagPos, self._select)
    end
end

function RoleFashionPhotoResolveItem:Reset()
    self._select = false
    self._selectImage:SetActive(self._select)
end

function RoleFashionPhotoResolveItem:CanSelected()
    if self.data.cfg_id == nil then
        return false
    end
    local star = fashionData:GetFashionActiveInfo(self.data.cfg_id)
    local maxStar = FashionDataHelper.GetFashionMaxStar(self.data.cfg_id)
    if (star < 0) or (star < maxStar) then
        return false
    end
    return true
end