-- BagListItem.lua
--背包列表Item类
local UIListItem = ClassTypes.UIListItem
local BagListItem = Class.BagListItem(UIListItem)
local public_config = require("ServerConfig/public_config")

BagListItem.interface = GameConfig.ComponentsConfig.PointableComponent
BagListItem.classPath = "Modules.ModuleRole.ChildComponent.BagListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local BaseUtil = GameUtil.BaseUtil

function BagListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_content/Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._imgLock = self:FindChildGO("Container_content/Image_Lock")
	
end

function BagListItem:OnDestroy()
	self._iconContainer = nil
end

function BagListItem:SetUseAndPowerCompare(showEquipUI,canNotUse,isUp)
	if self._imgCanNotUse == nil then
	 	self._imgCanNotUse = self:FindChildGO("Container_content/Image_CanNotUse")
	 	self._imgArrowUp = self:FindChildGO("Container_content/Image_ArrowUp")
	 	self._imgArrowDown = self:FindChildGO("Container_content/Image_ArrowDown")
	end
	if showEquipUI then
		if canNotUse then
			BaseUtil.SetObjectActive(self._imgCanNotUse,true)
			BaseUtil.SetObjectActive(self._imgArrowUp,false)
			BaseUtil.SetObjectActive(self._imgArrowDown,false)
		else
			BaseUtil.SetObjectActive(self._imgCanNotUse,false)
			if isUp then
				BaseUtil.SetObjectActive(self._imgArrowUp,true)
				BaseUtil.SetObjectActive(self._imgArrowDown,false)
			else
				BaseUtil.SetObjectActive(self._imgArrowUp,false)
				BaseUtil.SetObjectActive(self._imgArrowDown,true)
			end
		end
	else
		BaseUtil.SetObjectActive(self._imgCanNotUse,false)
		BaseUtil.SetObjectActive(self._imgArrowUp,false)
		BaseUtil.SetObjectActive(self._imgArrowDown,false)
	end
end

function BagListItem:ShowTipInfo(text)
	if text then
		if self._tipTextGo == nil then
			self._tipTextGo = self:FindChildGO("Container_content/Text_Tip")
			self._tipText = self:GetChildComponent("Container_content/Text_Tip", "TextMeshWrapper")
		end
		self._tipTextGo:SetActive(true)
		self._tipText.text = text
	else
		if self._tipTextGo then
			self._tipTextGo:SetActive(false)
		end
	end
end

function BagListItem:OnPointerDown(pointerEventData)
	--LoggerHelper.Error("BagListItem:OnPointerDown")
end

--override
function BagListItem:OnRefreshData(data)
	self._data = data
	if data == "nil" then
		self._iconContainer:SetItemData(nil)
		BaseUtil.SetObjectActive(self._imgLock,false)
		return
	end

	if data == "lock" then
		self._iconContainer:SetItemData(nil)
		BaseUtil.SetObjectActive(self._imgLock,true)
		return
	end

	self._iconContainer = self._itemManager:GetLuaUIComponent(data.cfg_id, self._itemContainer)
	self._iconContainer:SetItemData(data)
	BaseUtil.SetObjectActive(self._imgLock,false)
end