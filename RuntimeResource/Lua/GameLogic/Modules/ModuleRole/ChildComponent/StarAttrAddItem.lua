UIListItem = ClassTypes.UIListItem
local StarAttrAddItem = Class.StarAttrAddItem(ClassTypes.UIListItem)
StarAttrAddItem.interface = GameConfig.ComponentsConfig.Component
StarAttrAddItem.classPath = "Modules.ModuleRole.ChildComponent.StarAttrAddItem"
local StringStyleUtil = GameUtil.StringStyleUtil
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local AttriDataHelper = GameDataHelper.AttriDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
function StarAttrAddItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function StarAttrAddItem:OnDestroy()
    self.data = nil
end

function StarAttrAddItem:InitView()
    self._valueText = self:GetChildComponent("Text", "TextMeshWrapper")
end

--data: {attriId, curValue, delta}
function StarAttrAddItem:OnRefreshData(data)
    self.data = data
    if data["datatype"]==0 then     
        local c = StringStyleUtil.GetColorString(data["value"], StringStyleUtil.green)
        self._valueText.text = LanguageDataHelper.CreateContentWithArgs(data['attri'],{['0']=c})
    else
        local content = AttriDataHelper:ConvertAttriValue(data['attri'],data['value'])
        local c = StringStyleUtil.GetColorString(content, StringStyleUtil.green)
        self._valueText.text = AttriDataHelper:GetName(data['attri'])..":"..c
    end
end

-- 87017