local RoleFashionClothAttriItem = Class.RoleFashionClothAttriItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RoleFashionClothAttriItem.interface = GameConfig.ComponentsConfig.Component
RoleFashionClothAttriItem.classPath = "Modules.ModuleRole.ChildComponent.RoleFashionClothAttriItem"
require "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local AttriDataHelper = GameDataHelper.AttriDataHelper

function RoleFashionClothAttriItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self._select = false
    self._active = false
    self._putOn = false
    self:InitView()
end

function RoleFashionClothAttriItem:OnDestroy()
    self.data = nil
end

--override data {attri, value, delta}
function RoleFashionClothAttriItem:OnRefreshData(data)
    self.data = data
    self:ShowView()
end

function RoleFashionClothAttriItem:InitView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._valueText = self:GetChildComponent("Text_Value", "TextMeshWrapper")
    self._valueAddText = self:GetChildComponent("Text_AddValue", "TextMeshWrapper")
end

function RoleFashionClothAttriItem:ShowView()
    self._nameText.text = AttriDataHelper:GetName(self.data[1])
    self._valueText.text = self.data[2]
    self._valueAddText.text = self.data[3]
end