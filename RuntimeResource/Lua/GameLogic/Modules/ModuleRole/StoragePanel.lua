local StoragePanel = Class.StoragePanel(ClassTypes.BasePanel)
local GUIManager = GameManager.GUIManager
require "Modules.ModuleRole.ChildView.StorageSubView"
local StorageSubView = ClassTypes.StorageSubView
local public_config = GameWorld.public_config
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType

--override
function StoragePanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')

    self._containerBag = self:AddChildLuaUIComponent("Container_Bag", StorageSubView)
    self._containerStorage = self:AddChildLuaUIComponent("Container_Storage", StorageSubView)

    self._containerBag:SetBagType(public_config.PKG_TYPE_ITEM)
    self._containerStorage:SetBagType(public_config.PKG_TYPE_WAREHOUSE)
end

function StoragePanel:OnShow()
	self._containerBag:AddEventListener()
	self._containerStorage:AddEventListener()
	self._containerBag:UpdateData()
	self._containerStorage:UpdateData()
end

function StoragePanel:OnClose()
	self._containerBag:RemoveEventListener()
	self._containerStorage:RemoveEventListener()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_BAG_VIEW_SHOW_ROLE_MODEL)

end

function StoragePanel:WinBGType()
    return PanelWinBGType.NormalBG
end