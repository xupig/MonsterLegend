RoleModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function RoleModule.Init()
	GUIManager.AddPanel(PanelsConfig.Role,"Panel_Role",GUILayer.LayerUIPanel,"Modules.ModuleRole.RolePanel",true,false,nil,true)
	GUIManager.AddPanel(PanelsConfig.Storage,"Panel_Storage",GUILayer.LayerUIPanel,"Modules.ModuleRole.StoragePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
	GUIManager.AddPanel(PanelsConfig.TitleTips, "Panel_TitleTips", GUILayer.LayerUIFloat, "Modules.ModuleRole.TitleTipsPanel", true, false)
end

return RoleModule