require "Modules.ModuleRole.ChildView.RoleAttriTabView"
require "Modules.ModuleRole.ChildView.RoleBagTabView"
require "Modules.ModuleRole.ChildView.RoleFashionTabView"
require "Modules.ModuleRole.ChildView.RoleDukeTabView"
require "Modules.ModuleRole.ChildView.RoleTitleTabView"
require "Modules.ModuleRole.ChildView.StarView"

local RolePanel = Class.RolePanel(ClassTypes.BasePanel)

local UIToggleGroup = ClassTypes.UIToggleGroup
local GUIManager = GameManager.GUIManager
local RolePanelType = GameConfig.EnumType.RolePanelType
local RoleAttriTabView = ClassTypes.RoleAttriTabView
local RoleBagTabView = ClassTypes.RoleBagTabView
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local RoleFashionTabView = ClassTypes.RoleFashionTabView
local RoleTitleTabView = ClassTypes.RoleTitleTabView
local RoleDukeTabView = ClassTypes.RoleDukeTabView
local StarView = ClassTypes.StarView

--override
function RolePanel:Awake()
    self:InitViews()
end

--override
function RolePanel:OnDestroy()

end

function RolePanel:OnShow(data)
    self:AddEventListeners()
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
    GameWorld.ShowPlayer():ShowModel(1)
    self:SetSecTabContainerZ(-6000)
end

function RolePanel:OnClose()
    self:GetViewComponent("Container_Title"):CancelCountDown()

    GameWorld.ShowPlayer():ShowModel(3)
    self:RemoveEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_role_panel_close_button")
end

function RolePanel:AddEventListeners()

end

function RolePanel:RemoveEventListeners()

end

function RolePanel:InitViews()

    self:InitView("Container_Attri",    RoleAttriTabView,   RolePanelType.Attri)
    self:InitView("Container_Bag",      RoleBagTabView,     RolePanelType.Bag)
    self:InitView("Container_Fashion",  RoleFashionTabView, RolePanelType.Fashion,1)
    self:InitView("Container_Fashion",  RoleFashionTabView, RolePanelType.Fashion,2)
    self:InitView("Container_Fashion",  RoleFashionTabView, RolePanelType.Fashion,3)
    self:InitView("Container_Fashion",  RoleFashionTabView, RolePanelType.Fashion,4)
    self:InitView("Container_Fashion",  RoleFashionTabView, RolePanelType.Fashion,5)
    self:InitView("Container_Title",    RoleTitleTabView,   RolePanelType.Title)
    self:InitView("Container_Duke",     RoleDukeTabView,    RolePanelType.Duke)
    self:InitView("Container_Star",     StarView,    RolePanelType.Star)

    self._secTabCb = function (index)
		self:SelectSecTab(index)
	end
    self:SetSecondTabClickCallBack(self._secTabCb)
end

function RolePanel:SelectSecTab(secIndex)
    if self._selectedView then
        self._selectedView:SelectSecTab(secIndex)
    end
end

function RolePanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function RolePanel:StructingViewInit()
     return true
end

--关闭的时候重新开启二级主菜单
function RolePanel:ReopenSubMainPanel()
    return true
end

--基于结构化View初始化方式，隐藏View使用Scale的方式处理
function RolePanel:ScaleToHide()
    return true
end

function RolePanel:BaseShowModel()
    if self._selectedPath == "Container_Attri" or self._selectedPath == "Container_Bag" or self._selectedPath == "Container_Fashion" then
        self._selectedView:OnShowModel()
    end
end

function RolePanel:BaseHideModel()
    if self._selectedPath == "Container_Attri" or self._selectedPath == "Container_Bag" or self._selectedPath == "Container_Fashion" then
        self._selectedView:OnHideModel()
    end
end