local RoleBagToPreciousView = Class.RoleBagToPreciousView(ClassTypes.BaseLuaUIComponent)

RoleBagToPreciousView.interface = GameConfig.ComponentsConfig.Component
RoleBagToPreciousView.classPath = "Modules.ModuleRole.ChildView.RoleBagToPreciousView"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local EquipFindDataHelper = GameDataHelper.EquipFindDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper

function RoleBagToPreciousView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end

function RoleBagToPreciousView:InitView()
	self._btnToPrecious = self:FindChildGO("Button_Get")
    self._csBH:AddClick(self._btnToPrecious,function () self:ToPrecious() end)

    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose,function () self:OnCloseClick() end)

    self._txtDesc1 = self:GetChildComponent("Text_Desc1","TextMeshWrapper")
    self._txtDesc2 = self:GetChildComponent("Text_Desc2","TextMeshWrapper")
    self._txtItemName = self:GetChildComponent("Text_ItemName","TextMeshWrapper")

    local parent = self:FindChildGO("Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)

    self._rewardData = EquipFindDataHelper:GetEquipFindSpecialData(GameWorld.Player().level, GameWorld.Player():GetVocationGroup())
end

function RoleBagToPreciousView:ToPrecious()
	GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_EQUIP_TREASURE)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_CLOSE_TO_PRECIOUS)
end

function RoleBagToPreciousView:OnCloseClick()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_CLOSE_TO_PRECIOUS)
end

function RoleBagToPreciousView:RefreshView(index)
	local itemId
	local desc1 = 0
	local desc2 = 0
	if index == 9 then
		itemId = EquipFindDataHelper:GetEquipFindItemId(self._rewardData[1])
		desc1 = 58786
		desc2 = 58787
	else
		itemId = EquipFindDataHelper:GetEquipFindItemId(self._rewardData[2])
		desc1 = 58788
		desc2 = 58789
	end

	self._txtDesc1.text = LanguageDataHelper.CreateContent(desc1)
	self._txtDesc2.text = LanguageDataHelper.CreateContent(desc2)
	self._txtItemName.text = ItemDataHelper.GetItemNameWithColor(itemId)
	self._itemIcon:SetItem(itemId)
end