require "Modules.ModuleRole.ChildComponent.RoleFashionClothTotalAttriItem"
require "Modules.ModuleRole.ChildComponent.RoleFashionClothAdditionItem"
require "Modules.ModuleRole.ChildView.RolePhotoResolveDetailView"
require "Modules.ModuleRole.ChildView.RoleFashionPhotoAttriView"

local RoleFashionPhotoResolveView = Class.RoleFashionPhotoResolveView(ClassTypes.BaseLuaUIComponent)

RoleFashionPhotoResolveView.interface = GameConfig.ComponentsConfig.Component
RoleFashionPhotoResolveView.classPath = "Modules.ModuleRole.ChildView.RoleFashionPhotoResolveView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local FashionDataHelper = GameDataHelper.FashionDataHelper
local XArtNumber = GameMain.XArtNumber
local RoleFashionClothTotalAttriItem = ClassTypes.RoleFashionClothTotalAttriItem
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local LuaUIRawImage = GameMain.LuaUIRawImage
local fashionData = PlayerManager.PlayerDataManager.fashionData
local FashionManager = PlayerManager.FashionManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIProgressBar = ClassTypes.UIProgressBar
local RolePhotoResolveDetailView = ClassTypes.RolePhotoResolveDetailView
local RoleFashionClothAdditionItem = ClassTypes.RoleFashionClothAdditionItem
local RoleFashionPhotoAttriView = ClassTypes.RoleFashionPhotoAttriView

function RoleFashionPhotoResolveView:Awake()
    self:InitView()
end
--override
function RoleFashionPhotoResolveView:OnDestroy() 

end

function RoleFashionPhotoResolveView:OnEnable()
    self:AddEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.Show_Fashion_Photo_Fx, false)
end

function RoleFashionPhotoResolveView:OnDisable()
    self:RemoveEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.Show_Fashion_Photo_Fx, true)
end

--data结构
function RoleFashionPhotoResolveView:ShowView(data)
    self:RefreshView()
end

function RoleFashionPhotoResolveView:AddEventListeners()

end

function RoleFashionPhotoResolveView:RemoveEventListeners()

end

function RoleFashionPhotoResolveView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function () self:OnCloseButtonClick() end)
    self._upButton = self:FindChildGO("Button_Up")
    self._csBH:AddClick(self._upButton,function () self:OnUpButtonClick() end)
    self._resolveButton = self:FindChildGO("Button_Resolve")
    self._csBH:AddClick(self._resolveButton,function () self:OnResolveButtonClick() end)

    self._resolveText = self:GetChildComponent("Button_Resolve/Text", "TextMeshWrapper")
    self._titleText = self:GetChildComponent("Image_BG1/Text_Title", "TextMeshWrapper")

    self._levelText = self:GetChildComponent("Image_Level/Text_Level", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
    self._descText.text = LanguageDataHelper.GetContent(58606)
    self._resolveText.text = LanguageDataHelper.CreateContent(58674)
    self._titleText.text = LanguageDataHelper.CreateContent(58674)

    self:InitProgress()

    self._clothResolveDetailGo = self:FindChildGO("Container_ClothResolveDetail")
    self._clothResolveDetailView = self:AddChildLuaUIComponent("Container_ClothResolveDetail", RolePhotoResolveDetailView)
    self._clothResolveDetailGo:SetActive(false)

    self._clothAttriGo = self:FindChildGO("Container_CurAttriInfo")
    self._clothAttriView = self:AddChildLuaUIComponent("Container_CurAttriInfo", RoleFashionPhotoAttriView)
end

function RoleFashionPhotoResolveView:InitProgress()
    self._essenceComp = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar_Essence")
    self._essenceComp:SetProgress(0)
    self._essenceComp:ShowTweenAnimate(true)
    self._essenceComp:SetMutipleTween(false)
    self._essenceComp:SetIsResetToZero(false)
    self._essenceComp:SetTweenTime(0.2)

    self:OnEssenceChange()
    self._onEssenceChangeCB = function() self:OnEssenceChange() end
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_FASHION_PHOTO, self._onEssenceChangeCB)
end

function RoleFashionPhotoResolveView:OnEssenceChange()
    local entity = GameWorld.Player()
    local cur_exp = entity.money_fashion_photo
    local level = fashionData:GetCurLoadFashionLevel(public_config.FASHION_TYPE_PHOTO)
    local data = FashionDataHelper.GetLevelDataByTypeAndLevel(public_config.FASHION_TYPE_PHOTO, level)
    local max_exp = data.levelup_cost[public_config.MONEY_TYPE_FASHION_PHOTO]
    local value = (cur_exp > max_exp) and 1 or (cur_exp/max_exp)
    self._essenceComp:SetProgress(value)
    self._essenceComp:SetProgressText(tostring(cur_exp).."/"..tostring(max_exp))
end

function RoleFashionPhotoResolveView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function RoleFashionPhotoResolveView:OnUpButtonClick()
    FashionManager:FashionLevelUpReq(public_config.FASHION_TYPE_PHOTO)
end

function RoleFashionPhotoResolveView:OnResolveButtonClick()
    self._clothResolveDetailGo:SetActive(true)
    self._clothResolveDetailView:ShowView()
end

function RoleFashionPhotoResolveView:RefreshView()
    self:RefreshFashionLevel()
    self._clothAttriView:ShowView()
end

function RoleFashionPhotoResolveView:RefreshFashionLevel()
    self._levelText.text = "Lv."..fashionData:GetCurLoadFashionLevel(public_config.FASHION_TYPE_PHOTO)
end

function RoleFashionPhotoResolveView:RefreshFashionSelectedClothItem(pos, selectState)
    self._clothResolveDetailView:RefreshSelectedFashionItem(pos, selectState)
end