require "Modules.ModuleRole.ChildComponent.RoleBagSellEquipItem"

local RoleBagSellView = Class.RoleBagSellView(ClassTypes.BaseLuaUIComponent)

RoleBagSellView.interface = GameConfig.ComponentsConfig.Component
RoleBagSellView.classPath = "Modules.ModuleRole.ChildView.RoleBagSellView"


local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local BagManager = PlayerManager.BagManager
local UIComponentUtil = GameUtil.UIComponentUtil
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EntityConfig = GameConfig.EntityConfig
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RoleBagSellEquipItem = ClassTypes.RoleBagSellEquipItem
local UIList = ClassTypes.UIList
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemConfig = GameConfig.ItemConfig

local Min_Item_Count = 32

function RoleBagSellView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._typeShow = false
    self._gradeShow = false
    self._selectedItemList = {}
    self:InitCallBack()
    self:InitView()
    --self:AddEventListeners()
end

function RoleBagSellView:InitCallBack()
    self._onRefreshSelectedEquipItem = function(pos, selectState) self:RefreshSelectedEquipItem(pos, selectState) end
end
--override
function RoleBagSellView:OnDestroy() 
    --self:RemoveEventListeners()
end

function RoleBagSellView:AddEventListeners()
   
    EventDispatcher:AddEventListener(GameEvents.REFRESH_BAG_SELL_SELECTED_EQUIP_ITEM, self._onRefreshSelectedEquipItem)
end

function RoleBagSellView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.REFRESH_BAG_SELL_SELECTED_EQUIP_ITEM, self._onRefreshSelectedEquipItem)
end

function RoleBagSellView:InitView()
    self._btnSell = self:FindChildGO("Button_Sell")
    self._csBH:AddClick(self._btnSell,function () self:OnSellClick() end)

    self._btnSetting = self:FindChildGO("Button_Setting")
    self._csBH:AddClick(self._btnSetting,function () self:OnSettingClick() end)

    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose,function () self:OnCloseClick() end)

    self._txtPrice = self:GetChildComponent("Text_Price", "TextMeshWrapper")
    local moneyIcon = self:FindChildGO("Container_MoneyIcon")
    GameWorld.AddIcon(moneyIcon, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_GOLD))

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(RoleBagSellEquipItem)
    self._list:SetPadding(10, 0, 0, 10)
    self._list:SetGap(10, 12)
    self._list:SetDirection(UIList.DirectionLeftToRight, 8, 100)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
end

function RoleBagSellView:RefreshView()
    self._selectedItemList = {}
    local equips = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemListByItemType(public_config.ITEM_ITEMTYPE_EQUIP)
    local needData = {}
    local count = 0
    for k,v in pairs(equips) do
        if v.cfgData.quality <= ItemConfig.ItemQualityBlue and v.cfgData.allowSale > 0 then
            table.insert(needData, v)
            count = count + 1
        end
    end
    --填充空数据为了生成空格
    local addCount = math.max(Min_Item_Count, count) - count
	for i=1, addCount do
        table.insert(needData, {})
	end
    self._list:SetDataList(needData)

    self:AutoSelected()
end

--打开挂机设置
function RoleBagSellView:OnSettingClick()
    --这里出现两个同时打开的Panel，重开二级菜单特殊处理
    if GUIManager.reopenSubMainMenu > 0 then
        GUIManager.reopenSubMainMenu = GUIManager.reopenSubMainMenu + 1
    end
    GUIManager.ShowPanelByFunctionId(94)
end

function RoleBagSellView:OnSellClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_role_bag_sell_button")
    local posList = {}
    for k,_ in pairs(self._selectedItemList) do
        table.insert(posList, k)
    end
    if #posList <= 0 then
        self:OnCloseClick()
        return
    else
        local str = tostring(public_config.PKG_TYPE_ITEM)..","
        for i=1,#posList do
            str = str..posList[i]
            if i ~= #posList then
                str = str..","
            end
        end
        BagManager:RequestSell(str)
        self:OnCloseClick()
    end
end

function RoleBagSellView:OnCloseClick()
    EventDispatcher:TriggerEvent(GameEvents.UIEVENT_CLOSE_ROLE_BAG_SELL)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_role_bag_sell_close_button")
end

function RoleBagSellView:OnListItemClicked(idx)
    local item = self._list:GetItem(idx)
    item:ChangeState()
end

function RoleBagSellView:RefreshSelectedEquipItem(equipData, selectState)
    if selectState then
        self._selectedItemList[equipData.bagPos] = equipData
    else
        self._selectedItemList[equipData.bagPos] = nil
    end

    self:SetPrice()
end

function RoleBagSellView:SetPrice()
    local total = 0
    for k,v in pairs(self._selectedItemList) do
        total = total + v.cfgData.salePrice
    end
    self._txtPrice.text = tostring(total)
end

function RoleBagSellView:AutoSelected()
    self._selectedItemList = {}
    for i=0, self._list:GetLength()-1  do
        local equipData = self._list:GetDataByIndex(i)
        local item = self._list:GetItem(i)
        if equipData.cfgData ~= nil then
            self._selectedItemList[equipData.bagPos] = equipData
            item:Selected()
        else
            item:Reset()
        end
    end

    self:SetPrice()
end