require "Modules.ModuleRole.ChildComponent.RoleAttriInfoBlockItem"

local RoleAttriInfoBlockView = Class.RoleAttriInfoBlockView(ClassTypes.BaseLuaUIComponent)

RoleAttriInfoBlockView.interface = GameConfig.ComponentsConfig.Component
RoleAttriInfoBlockView.classPath = "Modules.ModuleRole.ChildView.RoleAttriInfoBlockView"

local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local RoleAttriInfoBlockItem = ClassTypes.RoleAttriInfoBlockItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function RoleAttriInfoBlockView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self._itemPool = {}
    self._items = {}
end
--override
function RoleAttriInfoBlockView:OnDestroy() 
    self._itemPool = nil
    self._items = nil
end

function RoleAttriInfoBlockView:InitView()
    self._itemTemp = self:FindChildGO("Container_AttriItemGroup/Container_AttriItem")
    self._itemTemp:SetActive(false)
    self._titleText = self:GetChildComponent("Text_Title", "TextMeshWrapper")
end

--data 
function RoleAttriInfoBlockView:SetData(data)
    self:RemoveAll()
    for i, v in pairs(data) do
        self:AddItem(v)
    end
end

function RoleAttriInfoBlockView:CreateItem()
    local itemPool = self._itemPool
    if #itemPool > 0 then
        return table.remove(itemPool, 1)
    end
    local go = self:CopyUIGameObject("Container_AttriItemGroup/Container_AttriItem","Container_AttriItemGroup")
    local item = UIComponentUtil.AddLuaUIComponent(go, RoleAttriInfoBlockItem)
    return item
end

function RoleAttriInfoBlockView:AddItem(data)
    local item = self:CreateItem()
    item.gameObject:SetActive(true)
    table.insert(self._items, item)
    item:SetData(data)
end

function RoleAttriInfoBlockView:RemoveAll()
    for i=#self._items,1,-1 do
        local item = self._items[i]
        item.gameObject:SetActive(false)
        self:ReleaseItem(item)
    end
    self._items = {}
end

function RoleAttriInfoBlockView:ReleaseItem(item)
    table.insert(self._itemPool, 1, item)
end

function RoleAttriInfoBlockView:SetTitle(id)
    local data = GlobalParamsHelper.GetParamValue(12)
    self._titleText.text = LanguageDataHelper.GetContent(data[id])
end