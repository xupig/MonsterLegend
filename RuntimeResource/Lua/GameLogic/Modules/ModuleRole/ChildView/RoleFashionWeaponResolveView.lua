require "Modules.ModuleRole.ChildComponent.RoleFashionClothTotalAttriItem"
require "Modules.ModuleRole.ChildComponent.RoleFashionClothAdditionItem"
require "Modules.ModuleRole.ChildView.RoleWeaponResolveDetailView"
require "Modules.ModuleRole.ChildView.RoleFashionWeaponAttriView"

local RoleFashionWeaponResolveView = Class.RoleFashionWeaponResolveView(ClassTypes.BaseLuaUIComponent)

RoleFashionWeaponResolveView.interface = GameConfig.ComponentsConfig.Component
RoleFashionWeaponResolveView.classPath = "Modules.ModuleRole.ChildView.RoleFashionWeaponResolveView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local FashionDataHelper = GameDataHelper.FashionDataHelper
local XArtNumber = GameMain.XArtNumber
local RoleFashionClothTotalAttriItem = ClassTypes.RoleFashionClothTotalAttriItem
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local LuaUIRawImage = GameMain.LuaUIRawImage
local fashionData = PlayerManager.PlayerDataManager.fashionData
local FashionManager = PlayerManager.FashionManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIProgressBar = ClassTypes.UIProgressBar
local RoleWeaponResolveDetailView = ClassTypes.RoleWeaponResolveDetailView
local RoleFashionClothAdditionItem = ClassTypes.RoleFashionClothAdditionItem
local RoleFashionWeaponAttriView = ClassTypes.RoleFashionWeaponAttriView

function RoleFashionWeaponResolveView:Awake()
    self:InitView()
end
--override
function RoleFashionWeaponResolveView:OnDestroy() 

end

function RoleFashionWeaponResolveView:OnEnable()
    self:AddEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.Hide_Player_Model)
end

function RoleFashionWeaponResolveView:OnDisable()
    self:RemoveEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.Show_Player_Model)
end

--data结构
function RoleFashionWeaponResolveView:ShowView(data)
    self:RefreshView()
end

function RoleFashionWeaponResolveView:AddEventListeners()

end

function RoleFashionWeaponResolveView:RemoveEventListeners()

end

function RoleFashionWeaponResolveView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function () self:OnCloseButtonClick() end)
    self._upButton = self:FindChildGO("Button_Up")
    self._csBH:AddClick(self._upButton,function () self:OnUpButtonClick() end)
    self._resolveButton = self:FindChildGO("Button_Resolve")
    self._csBH:AddClick(self._resolveButton,function () self:OnResolveButtonClick() end)

    self._resolveText = self:GetChildComponent("Button_Resolve/Text", "TextMeshWrapper")
    self._titleText = self:GetChildComponent("Image_BG1/Text_Title", "TextMeshWrapper")

    self._levelText = self:GetChildComponent("Image_Level/Text_Level", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
    self._descText.text = LanguageDataHelper.GetContent(58677)
    self._resolveText.text = LanguageDataHelper.CreateContent(58675)
    self._titleText.text = LanguageDataHelper.CreateContent(58675)


    self:InitProgress()

    self._clothResolveDetailGo = self:FindChildGO("Container_ClothResolveDetail")
    self._clothResolveDetailView = self:AddChildLuaUIComponent("Container_ClothResolveDetail", RoleWeaponResolveDetailView)
    self._clothResolveDetailGo:SetActive(false)

    self._clothAttriGo = self:FindChildGO("Container_CurAttriInfo")
    self._clothAttriView = self:AddChildLuaUIComponent("Container_CurAttriInfo", RoleFashionWeaponAttriView)
end

function RoleFashionWeaponResolveView:InitProgress()
    self._essenceComp = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar_Essence")
    self._essenceComp:SetProgress(0)
    self._essenceComp:ShowTweenAnimate(true)
    self._essenceComp:SetMutipleTween(false)
    self._essenceComp:SetIsResetToZero(false)
    self._essenceComp:SetTweenTime(0.2)

    self:OnEssenceChange()
    self._onEssenceChangeCB = function() self:OnEssenceChange() end
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_FASHION_WEAPON, self._onEssenceChangeCB)
end

function RoleFashionWeaponResolveView:OnEssenceChange()
    local entity = GameWorld.Player()
    local cur_exp = entity.money_fashion_weapon
    local level = fashionData:GetCurLoadFashionLevel(public_config.FASHION_TYPE_WEAPON)
    local data = FashionDataHelper.GetLevelDataByTypeAndLevel(public_config.FASHION_TYPE_WEAPON, level)
    local max_exp = data.levelup_cost[public_config.MONEY_TYPE_FASHION_WEAPON]
    local value = (cur_exp > max_exp) and 1 or (cur_exp/max_exp)
    self._essenceComp:SetProgress(value)
    self._essenceComp:SetProgressText(tostring(cur_exp).."/"..tostring(max_exp))
end

function RoleFashionWeaponResolveView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function RoleFashionWeaponResolveView:OnUpButtonClick()
    FashionManager:FashionLevelUpReq(public_config.FASHION_TYPE_WEAPON)
end

function RoleFashionWeaponResolveView:OnResolveButtonClick()
    self._clothResolveDetailGo:SetActive(true)
    self._clothResolveDetailView:ShowView()
end

function RoleFashionWeaponResolveView:RefreshView()
    self:RefreshFashionLevel()
    self._clothAttriView:ShowView()
end

function RoleFashionWeaponResolveView:RefreshFashionLevel()
    self._levelText.text = "Lv."..fashionData:GetCurLoadFashionLevel(public_config.FASHION_TYPE_WEAPON)
end

function RoleFashionWeaponResolveView:RefreshFashionSelectedClothItem(pos, selectState)
    self._clothResolveDetailView:RefreshSelectedFashionItem(pos, selectState)
end