require "Modules.ModuleRole.ChildComponent.StarAttrAddItem"
local StarAddView = Class.StarAddView(ClassTypes.BaseLuaUIComponent)
StarAddView.interface = GameConfig.ComponentsConfig.Component
StarAddView.classPath = "Modules.ModuleRole.ChildView.StarAddView"
local GUIManager = GameManager.GUIManager
local StarManager = PlayerManager.StarManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local StarAttrAddItem = ClassTypes.StarAttrAddItem
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local XArtNumber = GameMain.XArtNumber
local StarVeinDataHelper = GameDataHelper.StarVeinDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
function StarAddView:Awake()
    self:InitView()
end

--override
function StarAddView:OnDestroy() 

end

function StarAddView:OnEnable()
    self:ShowView()
end

function StarAddView:OnDisable()
    self:CloseView()    
end

--data结构
function StarAddView:ShowView()
    self:RefreshStarAttView()
end

function StarAddView:CloseView()

end



function StarAddView:RefreshStarAttView()
    local info = GameWorld.Player().zodiac_info
    local rank = info[public_config.ZODIAC_RANK] or 1
    local level = 1
    local curid = StarVeinDataHelper:GetDataByRankLevel(rank,level)
    local curatts = StarVeinDataHelper:GetAttriRank(curid)
    if not StarVeinDataHelper:IsFunRank(curid+1) then
        local nextatts = {}
        self._nextgo:SetActive(true)
        nextatts = StarVeinDataHelper:GetAttriRank(curid+1)
        local nextattriList = {}
        for k,v in pairs(nextatts) do
            table.insert(nextattriList, {["datatype"]=1,["attri"]=k, ["value"]=v})
        end
        local nextid = StarVeinDataHelper:GetDataByRankLevel(rank+1,level)
        local nextPre = StarVeinDataHelper:GetAttriPercent(nextid)
        table.insert(nextattriList, {["datatype"]=0,["attri"]=87017, ["value"]=nextPre})
        self._nextattlist:SetDataList(nextattriList)
        self._nextText.text = LanguageDataHelper.CreateContentWithArgs(87016,{['0']=rank+1,['1']=level})
    else
        self._nextgo:SetActive(false)
    end
    local curattriList = {}
    for k,v in pairs(curatts) do
        table.insert(curattriList, {["datatype"]=1,["attri"]=k, ["value"]=v})
    end
    local curPre = StarVeinDataHelper:GetAttriPercent(curid)
    table.insert(curattriList, {["datatype"]=0,["attri"]=87017, ["value"]=curPre})
    self._curattlist:SetDataList(curattriList)
    self._curText.text = LanguageDataHelper.CreateContentWithArgs(87015,{['0']=rank,['1']=level})
end



function StarAddView:InitView()
    self:InitScroll()
    self:InitCom()
end

function StarAddView:InitCom()
    self._curgo = self:FindChildGO("Image_Bg/Container_Cur")
    self._nextgo = self:FindChildGO("Image_Bg/Container_Next")

    self._curText = self:GetChildComponent("Image_Bg/Container_Cur/Text_Cur", "TextMeshWrapper")
    self._nextText = self:GetChildComponent("Image_Bg/Container_Next/Text_Next", "TextMeshWrapper")  
end

function StarAddView:InitScroll()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Image_Bg/Container_Cur/ScrollViewList_Current")
	self._curattlist = list
	self._curattscroll = scrollView 
    self._curattscroll:SetHorizontalMove(false)
    self._curattscroll:SetVerticalMove(true)
    self._curattlist:SetItemType(StarAttrAddItem)
    self._curattlist:SetPadding(0, 0, 0, 10)
    self._curattlist:SetGap(0, 0)
    self._curattlist:SetDirection(UIList.DirectionTopToDown, 1, -1)


    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Image_Bg/Container_Next/ScrollViewList_Next")
	self._nextattlist = list
	self._nextattscroll = scrollView 
    self._nextattscroll:SetHorizontalMove(false)
    self._nextattscroll:SetVerticalMove(true)
    self._nextattlist:SetItemType(StarAttrAddItem)
    self._nextattlist:SetPadding(0, 0, 0,5)
    self._nextattlist:SetGap(0, 0)
    self._nextattlist:SetDirection(UIList.DirectionTopToDown, 1, -1)

end



