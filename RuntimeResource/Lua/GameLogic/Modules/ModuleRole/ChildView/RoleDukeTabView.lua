local RoleDukeTabView = Class.RoleDukeTabView(ClassTypes.BaseLuaUIComponent)

RoleDukeTabView.interface = GameConfig.ComponentsConfig.Component
RoleDukeTabView.classPath = "Modules.ModuleRole.ChildView.RoleDukeTabView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local RoleManager = PlayerManager.RoleManager
local UIComponentUtil = GameUtil.UIComponentUtil
local NobilityDataHelper = GameDataHelper.NobilityDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local ConvertStyle = GameMain.LuaFacade.ConvertStyle
local ItemConfig = GameConfig.ItemConfig
require "UIComponent.Extend.AttriCom"
require "UIComponent.Extend.ItemGridEx"
local AttriCom = ClassTypes.AttriCom
local ItemGridEx = ClassTypes.ItemGridEx
local BaseUtil = GameUtil.BaseUtil
local UIParticle = ClassTypes.UIParticle

function RoleDukeTabView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitCallback()
    self:InitView()
    self:AddEventListeners()
end
--override
function RoleDukeTabView:OnDestroy() 
    self:RemoveEventListeners()
end

--data结构
function RoleDukeTabView:ShowView(data)
    self._iconBg = self:FindChildGO('Image_Bg')
    GameWorld.AddIcon(self._iconBg,3317)
    self:RefreshView()
    GameWorld.ShowPlayer():Hide()
end

function RoleDukeTabView:CloseView()
      self._iconBg = nil
      GameWorld.ShowPlayer():Show()
end

function RoleDukeTabView:InitCallback()
  	self._updateCb = function ()
        self._delayTimer = TimerHeap:AddSecTimer(1,0,1,function ()
            self:RefreshView()
            TimerHeap:DelTimer(self._delayTimer)
        end)
  		  
        self:PlayFx()
  	end
end

function RoleDukeTabView:AddEventListeners()
	EventDispatcher:AddEventListener(GameEvents.NOBILITY_UPDATE,self._updateCb)
end

function RoleDukeTabView:RemoveEventListeners()
	EventDispatcher:RemoveEventListener(GameEvents.NOBILITY_UPDATE,self._updateCb)
end

function RoleDukeTabView:InitView()
    self._fxUpgrade = UIParticle.AddParticle(self.gameObject,"fx_ui_30033")
    self._fxUpgrade:Stop()

    self._curNobilityIcon = self:FindChildGO("Container_CurNobilityIcon")
    self._txtNoneGO = self:FindChildGO("Text_None")
    self._containerCurNobility = self:FindChildGO("Container_CurNobility")
    self._curNobilityTitle = self:FindChildGO("Container_CurNobility/Container_CurNobilityTitle")

    self._txtCurFightPower = self:GetChildComponent("Container_CurNobility/Text_CurFightPower","TextMeshWrapper")
    self._curAttris = {}
    for i=1,2 do
    	local item = self:AddChildLuaUIComponent("Container_CurNobility/Container_CurAttri/Container_Attri"..i,AttriCom)
    	self._curAttris[i] = item
   	end

   	self._containerUpgrade = self:FindChildGO("Container_Upgrade")

   	self._nextNobilityIcon = self:FindChildGO("Container_Upgrade/Container_NextNobilityIcon")
    self._nextNobilityTitle = self:FindChildGO("Container_Upgrade/Container_NextNobilityTitle")

    self._txtNextFightPower = self:GetChildComponent("Container_Upgrade/Text_NextFightPower","TextMeshWrapper")
    self._nextAttris = {}
    for i=1,2 do
    	local item = self:AddChildLuaUIComponent("Container_Upgrade/Container_NextAttri/Container_Attri"..i,AttriCom)
   		self._nextAttris[i] = item
   	end

   	self._btnUpgrade = self:FindChildGO("Container_Upgrade/Button_Upgrade")
   	self._csBH:AddClick(self._btnUpgrade, function() self:OnUpgrade() end )
    self._imgUpgradeRedPoint = self:FindChildGO("Container_Upgrade/Button_Upgrade/Image_RedPoint")

   	self._txtCondition = self:GetChildComponent("Container_Upgrade/Text_UpgradeCondition","TextMeshWrapper")

   	self._matIcon = self:AddChildLuaUIComponent("Container_Upgrade/Container_Material",ItemGridEx)
    self._matIcon:ActivateTips()
end

function RoleDukeTabView:PlayFx()
    self._fxUpgrade:ForceReorderAll()
    self._fxUpgrade:Play(true,true)
end

function RoleDukeTabView:RefreshView()
	  local curNobility = GameWorld.Player().nobility_rank
    local curCfg = NobilityDataHelper.GetNobilityCfg(curNobility) 
    local nextCfg = NobilityDataHelper.GetNobilityCfg(curNobility+1) 
   
   	GameWorld.AddIcon(self._curNobilityIcon,curCfg.nobility_icon)

    if curNobility > 0 then
      self._containerCurNobility:SetActive(true)
      self._txtNoneGO:SetActive(false)
     	GameWorld.AddIcon(self._curNobilityTitle,curCfg.nobility_show)
      local ct = {}
      for i=1,2 do
      	local attriData = curCfg.nobility_atrri[i]
        local cak = attriData[1]
        local cav = attriData[2]
        table.insert(ct,{["attri"]=cak, ["value"]=cav})
      	self._curAttris[i]:UpdateItem(AttriDataHelper:GetName(cak),"+"..cav)
      end
      self._txtCurFightPower.text = "+"..BaseUtil.CalculateFightPower(ct,GameWorld.Player().vocation)
    else
      self._containerCurNobility:SetActive(false)
      self._txtNoneGO:SetActive(true)
    end

    if nextCfg then
    	GameWorld.AddIcon(self._nextNobilityIcon,nextCfg.nobility_icon)
   		GameWorld.AddIcon(self._nextNobilityTitle,nextCfg.nobility_show)

   		for itemId,count in pairs(curCfg.nobility_levelup_cost) do
   			self._matIcon:UpdateData(itemId,count)
   		end

    	local needFightPower = curCfg.nobility_levelup_need
    	local curFightPower = GameWorld.Player().fight_force
    	self._containerUpgrade:SetActive(true)
    	if needFightPower > curFightPower then
    		self._txtCondition.text = ConvertStyle(string.format("$(%d,%s)",ItemConfig.ItemForbidden,curFightPower)).."/"..needFightPower
    	else
    		self._txtCondition.text = curFightPower.."/"..needFightPower
    	end

      if needFightPower <= curFightPower and self._matIcon:IsEnough() then
        self._imgUpgradeRedPoint:SetActive(true)
      else
        self._imgUpgradeRedPoint:SetActive(false)
      end

      local nt = {}
    	for i=1,2 do
      	local nextAttriData = nextCfg.nobility_atrri[i]
        local nak = nextAttriData[1]
        local nav = nextAttriData[2]
      	self._nextAttris[i]:UpdateItem(AttriDataHelper:GetName(nak),"+"..nav)
        table.insert(nt,{["attri"]=nak, ["value"]=nav})
      end
      self._txtNextFightPower.text = "+"..BaseUtil.CalculateFightPower(nt,GameWorld.Player().vocation)
    else
    	self._containerUpgrade:SetActive(false)
    end
end

function RoleDukeTabView:OnUpgrade()
	RoleManager:UpgradeNobility()
end