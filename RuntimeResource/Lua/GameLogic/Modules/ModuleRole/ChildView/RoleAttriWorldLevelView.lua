local RoleAttriWorldLevelView = Class.RoleAttriWorldLevelView(ClassTypes.BaseLuaUIComponent)

RoleAttriWorldLevelView.interface = GameConfig.ComponentsConfig.Component
RoleAttriWorldLevelView.classPath = "Modules.ModuleRole.ChildView.RoleAttriWorldLevelView"

local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WorldLevelDataHelper = GameDataHelper.WorldLevelDataHelper

function RoleAttriWorldLevelView:Awake()
    self:InitView()
end
--override
function RoleAttriWorldLevelView:OnDestroy() 

end

function RoleAttriWorldLevelView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function () self:OnCloseButtonClick() end)
    self._okButton = self:FindChildGO("Button_OK")
    self._csBH:AddClick(self._okButton,function () self:OnCloseButtonClick() end)

    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
    self._descText.text = LanguageDataHelper.GetContent(58608)

    self._levelText = self:GetChildComponent("Text_Tips1/Text_Value", "TextMeshWrapper")
    self._additionText = self:GetChildComponent("Text_Tips2/Text_Value", "TextMeshWrapper")
end

function RoleAttriWorldLevelView:ShowView()
    self:RefreshView()
end

function RoleAttriWorldLevelView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function RoleAttriWorldLevelView:RefreshView()
    self._levelText.text = GameWorld.Player().world_level
    local deviationLevel = GameWorld.Player().world_level - GameWorld.Player().level
    self._additionText.text = WorldLevelDataHelper.GetWorldLevelCorrection(GameWorld.Player().world_level, deviationLevel) .. "%"
end