require "Modules.ModuleRole.ChildComponent.StarAttrItem"
require "Modules.ModuleRole.ChildComponent.StarEnterItem"
require "Modules.ModuleRole.ChildView.StarAddView"
local StarView = Class.StarView(ClassTypes.BaseLuaUIComponent)
StarView.interface = GameConfig.ComponentsConfig.Component
StarView.classPath = "Modules.ModuleRole.ChildView.StarView"
local GUIManager = GameManager.GUIManager
local StarManager = PlayerManager.StarManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local StarAttrItem = ClassTypes.StarAttrItem
local StarEnterItem = ClassTypes.StarEnterItem
local StarAddView = ClassTypes.StarAddView
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local XArtNumber = GameMain.XArtNumber
local StarVeinDataHelper = GameDataHelper.StarVeinDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
function StarView:Awake()
    self:InitCallBack()
    self:InitView()
end
--override
function StarView:OnDestroy() 

end

function StarView:OnEnable()
    
end

function StarView:OnDisable()
    
end

--data结构
function StarView:ShowView(data)
    self.data = data
    self:AddEventListeners()
    self:RefreshStarAttView()
    self:RefreshMater()   
    self:UpdatePosition()
    self:SetFxState(true)
end


function StarView:CloseView()
    self:RemoveEventListeners()
    self:SetFxState(false)
end

function StarView:RefreshFight(attriList)
    local fightPower = BaseUtil.CalculateFightPower(attriList, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)
end

function StarView:RefreshStarAttView()
    local info = GameWorld.Player().zodiac_info
    local rank = info[public_config.ZODIAC_RANK] or 1
    local level = info[public_config.ZODIAC_LEVEL] or 0
    local curid = StarVeinDataHelper:GetDataByRankLevel(rank,level)
    --icon
    GameWorld.AddIcon(self._bgGO,StarVeinDataHelper:GetBg(curid))
    GameWorld.AddIcon(self._iconGO,StarVeinDataHelper:GetConstellation(curid))

    --
    self._TextName.text = "ss"
    self._TextRank.text = "ss"
    --属性
    local ids = StarVeinDataHelper:GetPreRankIds(rank)
    table.insert(ids,curid)
    local atts = StarVeinDataHelper:GetAttByIds(ids)

    local curatt = {}
    if not StarVeinDataHelper:IsFunRank(curid+1) then
        if StarVeinDataHelper:IsFunLevel(rank,level) then
            local _curid = StarVeinDataHelper:GetDataByRankLevel(rank+1,1)
            curatt = StarVeinDataHelper:GetAttriLevel(_curid)        
        else
            curatt = StarVeinDataHelper:GetAttriLevel(curid+1)                
        end
    end
    local attriList = {}
    for k,v in pairs(atts) do
        local add = curatt[k] or 0
        table.insert(attriList, {["attri"]=k, ["value"]=v,["madd"] = add})
    end
    self._attlist:SetDataList(attriList)
    self:RefreshFight(attriList)

    self:UpdatePosition()
end


function StarView:SetEnters()
    local enters = GlobalParamsHelper.GetParamValue(981)
    self._enterlist:SetDataList(enters)
end

function StarView:RefreshMater()
    local info = GameWorld.Player().zodiac_info 
    local mater = GameWorld.Player().zodiac_material 
    local rank = info[public_config.ZODIAC_RANK] or 1
    local level = info[public_config.ZODIAC_LEVEL] or 0
    local curid = StarVeinDataHelper:GetDataByRankLevel(rank,level)
    local count = StarVeinDataHelper:GetCost(curid)
    local v =  math.floor(mater/count)
    self._barComp:SetProgress(v)
    self._TextAttach.text = mater..'/'..count
end

function StarView:InitCallBack()
    self._refreshAtt = function()RefreshStarAttView()end
    self._refreshMater = function()RefreshMater()end
end

function StarView:AddEventListeners()
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_ZODIAC_MATERIAL, self._refreshMater)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_ZODIAC_INFO, self._refreshAtt)
end

function StarView:RemoveEventListeners()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_ZODIAC_MATERIAL, self._refreshMater)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_ZODIAC_INFO, self._refreshAtt)	
end

function StarView:InitView()
    self._desActive = false
    self._addActive = false
    self:InitScroll()
    self:InitProgressBar()
    self:InitCom()    
end

function StarView:InitCom()
    self._bgGO = self:FindChildGO("Image_Bg")
    self._iconGO = self:FindChildGO("Image_Icon")
    self._btnlight = self:FindChildGO("Button_Light")
    self._btndes = self:FindChildGO("Button_Des")
    self._btnbg = self:FindChildGO("Container_Introduce/Button_Bg")
    self._btnaddbg = self:FindChildGO("Container_Tips/Button_Bg")
    self._btnadd = self:FindChildGO("Button_Add")
    self._materIcon = self:FindChildGO("Image_MaterIcon")
    self._csBH:AddClick(self._btnlight, function()self:OnBtnLight() end)
    self._csBH:AddClick(self._btndes, function()self:OnBtnDes() end)
    self._csBH:AddClick(self._btnbg, function()self:OnBtnDes() end)
    self._csBH:AddClick(self._btnadd, function()self:OnBtnAdd() end)
    self._csBH:AddClick(self._btnaddbg, function()self:OnBtnAdd() end)
    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)

    self._introGO = self:FindChildGO("Container_Introduce")
    self._introGO:SetActive(false)
    self._addGO = self:FindChildGO("Container_Tips")

    self._addView = self:AddChildLuaUIComponent('Container_Tips',StarAddView)  

    self._TextName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._TextRank = self:GetChildComponent("Text_Rank", "TextMeshWrapper")

    GameWorld.AddIcon(self._materIcon,GlobalParamsHelper.GetParamValue(993))

    self._btns={}
    self._btns[1] = self:FindChildGO("Button_1").transform
    self._btns[2] = self:FindChildGO("Button_2").transform
    self._btns[3] = self:FindChildGO("Button_3").transform
    self._btns[4] = self:FindChildGO("Button_4").transform
    self._btns[5] = self:FindChildGO("Button_5").transform
    self._btns[6] = self:FindChildGO("Button_6").transform
    self._btns[7] = self:FindChildGO("Button_7").transform
    self._btns[8] = self:FindChildGO("Button_8").transform

    self._btnsFx={}
    self._btnsFx[1] = self:FindChildGO("Button_1/fx_ui_30023")
    self._btnsFx[2] = self:FindChildGO("Button_2/fx_ui_30023")
    self._btnsFx[3] = self:FindChildGO("Button_3/fx_ui_30023")
    self._btnsFx[4] = self:FindChildGO("Button_4/fx_ui_30023")
    self._btnsFx[5] = self:FindChildGO("Button_5/fx_ui_30023")
    self._btnsFx[6] = self:FindChildGO("Button_6/fx_ui_30023")
    self._btnsFx[7] = self:FindChildGO("Button_7/fx_ui_30023")
    self._btnsFx[8] = self:FindChildGO("Button_8/fx_ui_30023")
end

function StarView:OnBtnLight()
    StarManager.RouletteRewardRpc()
end

function StarView:SetFxState(state)
    for k,v in ipairs(self._btnsFx) do
        v:SetActive(state)
    end
end

function StarView:OnBtnDes()
    self._desActive = not self._desActive
    self._introGO:SetActive(self._desActive)
    if self._desActive then
        self:SetEnters()
    end
end

function StarView:UpdatePosition()
    local info = GameWorld.Player().zodiac_info 
    local rank = info[public_config.ZODIAC_RANK] or 1
    -- local level = info[public_config.ZODIAC_LEVEL] or 0
    local poslist = StarVeinDataHelper:GetPositionByRank(rank)
    for k,v in ipairs(poslist) do
        self._btns[k].localPosition = Vector3(v[1],v[2],0)
    end
end

function StarView:OnBtnAdd()
    self._addActive = not self._addActive
    self._addGO:SetActive(self._addActive)
end

function StarView:InitScroll()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "List_Attr")
	self._attlist = list
	self._attscroll = scrollView 
    self._attscroll:SetHorizontalMove(false)
    self._attscroll:SetVerticalMove(true)
    self._attlist:SetItemType(StarAttrItem)
    self._attlist:SetPadding(0, 0, 0, 50)
    self._attlist:SetGap(0, 0)
    self._attlist:SetDirection(UIList.DirectionTopToDown, 1, -1)


    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Introduce/Image_Bg/ScrollViewList")
	self._enterlist = list
	self._enterscroll = scrollView 
    self._enterscroll:SetHorizontalMove(true)
    self._enterscroll:SetVerticalMove(false)
    self._enterlist:SetItemType(StarEnterItem)
    self._enterlist:SetPadding(0, 0, 0, 0)
    self._enterlist:SetGap(0, 0)
    self._enterlist:SetDirection(UIList.DirectionLeftToRight, -1, -1)

end

function StarView:InitProgressBar()  
    self._barComp = UIScaleProgressBar.AddProgressBar(self.gameObject,"ProgressBar")
    self._barComp:SetProgress(0)
    self._barComp:ShowTweenAnimate(true)
    self._barComp:SetMutipleTween(false)
    self._barComp:SetIsResetToZero(false)
    self._barComp:SetTweenTime(0.2)
    self._TextAttach = self:GetChildComponent("ProgressBar/Text_Attach", "TextMeshWrapper")
    self._TextAttach.text = ''
end


