require "Modules.ModuleRole.ChildView.RoleFashionClothView"
require "Modules.ModuleRole.ChildView.RoleFashionWeaponView"
require "Modules.ModuleRole.ChildView.RoleFashionEffectView"
require "Modules.ModuleRole.ChildView.RoleFashionPhotoFrameView"
require "Modules.ModuleRole.ChildView.RoleFashionChatFrameView"

local RoleFashionTabView = Class.RoleFashionTabView(ClassTypes.BaseLuaUIComponent)

RoleFashionTabView.interface = GameConfig.ComponentsConfig.Component
RoleFashionTabView.classPath = "Modules.ModuleRole.ChildView.RoleFashionTabView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIToggleGroup = ClassTypes.UIToggleGroup
local RoleFashionClothView = ClassTypes.RoleFashionClothView
local LuaUIRawImage = GameMain.LuaUIRawImage
local RoleFashionWeaponView = ClassTypes.RoleFashionWeaponView
local PlayerModelComponent = GameMain.PlayerModelComponent
local RoleFashionEffectView = ClassTypes.RoleFashionEffectView
local FashionDataHelper = GameDataHelper.FashionDataHelper
local RoleFashionPhotoFrameView = ClassTypes.RoleFashionPhotoFrameView
local RoleFashionChatFrameView = ClassTypes.RoleFashionChatFrameView

local Max_View_Count = 5

function RoleFashionTabView:Awake()
    self._selectTab = -1
    self._indexToGO = {}
    self._indexToView = {}
    self:InitCallBack()
    self:InitView()
end
--override
function RoleFashionTabView:OnDestroy() 

end

function RoleFashionTabView:OnEnable()
    self:AddEventListeners()
    GameWorld.Player():SetPreviewState(true)
end

function RoleFashionTabView:OnDisable()
    self:RemoveEventListeners()
    GameWorld.Player():SetPreviewState(false)
end

--data结构
function RoleFashionTabView:ShowView(data)
    self.data = data
    self:RefreshView()
    for i=1,Max_View_Count do
        self._indexToView[i]:SetScrollRectState(true)
    end
    self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
    self:OnEnable()
end

function RoleFashionTabView:CloseView()
    for i=1,#self._indexToView do
        self._indexToView[i]:Reset()
    end
    for i=1,Max_View_Count do
        self._indexToView[i]:SetScrollRectState(false)
    end
    GameWorld.ShowPlayer():SyncShowPlayerFacade()
    self:OnDisable()
end

function RoleFashionTabView:InitCallBack()
    self._showPlayerModel = function() self._modelComponent:Show() end
    self._hidePlayerModel = function() self._modelComponent:Hide() end
    self._resetPlayerModel = function() self:ResetPlayerModel() end
end

function RoleFashionTabView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Show_Player_Model, self._showPlayerModel)
    EventDispatcher:AddEventListener(GameEvents.Hide_Player_Model, self._hidePlayerModel)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_BAG_RETSET_PLAYER_MODEL, self._resetPlayerModel)
end

function RoleFashionTabView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Show_Player_Model, self._showPlayerModel)
    EventDispatcher:RemoveEventListener(GameEvents.Hide_Player_Model, self._hidePlayerModel)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_BAG_RETSET_PLAYER_MODEL, self._resetPlayerModel)
end

function RoleFashionTabView:InitView()
    self._indexToGO[1] = self:FindChildGO("Container_Cloth")
    self._indexToGO[2] = self:FindChildGO("Container_Weapon")
    self._indexToGO[3] = self:FindChildGO("Container_Effect")
    self._indexToGO[4] = self:FindChildGO("Container_PhotoFrame")
    self._indexToGO[5] = self:FindChildGO("Container_ChatFrame")

    for i=1,#self._indexToGO do
        self._indexToGO[i]:SetActive(false)
    end

    self._indexToView[1] = self:AddChildLuaUIComponent("Container_Cloth", RoleFashionClothView)
    self._indexToView[2] = self:AddChildLuaUIComponent("Container_Weapon", RoleFashionWeaponView)
    self._indexToView[3] = self:AddChildLuaUIComponent("Container_Effect", RoleFashionEffectView)
    self._indexToView[4] = self:AddChildLuaUIComponent("Container_PhotoFrame", RoleFashionPhotoFrameView)
    self._indexToView[5] = self:AddChildLuaUIComponent("Container_ChatFrame", RoleFashionChatFrameView)

    self._modelComponent = self:AddChildComponent('Container_Model', PlayerModelComponent)
end

function RoleFashionTabView:SelectSecTab(index)
    if self._selectTab ~= -1 then
        if self._indexToView[self._selectTab] ~= nil then
            self._indexToView[self._selectTab]:CloseView()
        end
        self._indexToGO[self._selectTab]:SetActive(false)
    end
    self._indexToGO[index]:SetActive(true)
    if self._indexToView[index] ~= nil then
        self._indexToView[index]:ShowView(self.data)
    end
    self._selectTab = index
end

function RoleFashionTabView:RefreshView()

end

function RoleFashionTabView:OnShowModel()
    self._modelComponent:Show()
    EventDispatcher:TriggerEvent(GameEvents.Show_Fashion_Chat_Fx, true)
    EventDispatcher:TriggerEvent(GameEvents.Show_Fashion_Photo_Fx, true)
end

function RoleFashionTabView:OnHideModel()
    self._modelComponent:Hide()
    EventDispatcher:TriggerEvent(GameEvents.Show_Fashion_Chat_Fx, false)
    EventDispatcher:TriggerEvent(GameEvents.Show_Fashion_Photo_Fx, true)
end

function RoleFashionTabView:ResetPlayerModel()
    self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
    if self._indexToView[self._selectTab] ~= nil then
        self._indexToView[self._selectTab]:ResetSelect()
    end
end