require "Modules.ModuleRole.ChildComponent.RoleFashionClothTotalAttriItem"
require "Modules.ModuleRole.ChildComponent.RoleFashionClothAdditionItem"
require "Modules.ModuleRole.ChildView.RoleEffectResolveDetailView"
require "Modules.ModuleRole.ChildView.RoleFashionEffectAttriView"

local RoleFashionEffectResolveView = Class.RoleFashionEffectResolveView(ClassTypes.BaseLuaUIComponent)

RoleFashionEffectResolveView.interface = GameConfig.ComponentsConfig.Component
RoleFashionEffectResolveView.classPath = "Modules.ModuleRole.ChildView.RoleFashionEffectResolveView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local FashionDataHelper = GameDataHelper.FashionDataHelper
local XArtNumber = GameMain.XArtNumber
local RoleFashionClothTotalAttriItem = ClassTypes.RoleFashionClothTotalAttriItem
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local LuaUIRawImage = GameMain.LuaUIRawImage
local fashionData = PlayerManager.PlayerDataManager.fashionData
local FashionManager = PlayerManager.FashionManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIProgressBar = ClassTypes.UIProgressBar
local RoleEffectResolveDetailView = ClassTypes.RoleEffectResolveDetailView
local RoleFashionClothAdditionItem = ClassTypes.RoleFashionClothAdditionItem
local RoleFashionEffectAttriView = ClassTypes.RoleFashionEffectAttriView

function RoleFashionEffectResolveView:Awake()
    self:InitView()
end
--override
function RoleFashionEffectResolveView:OnDestroy() 

end

--data结构
function RoleFashionEffectResolveView:ShowView(data)
    self:RefreshView()
end

function RoleFashionEffectResolveView:OnEnable()
    self:AddEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.Hide_Player_Model)
end

function RoleFashionEffectResolveView:OnDisable()
    self:RemoveEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.Show_Player_Model)
end

function RoleFashionEffectResolveView:AddEventListeners()

end

function RoleFashionEffectResolveView:RemoveEventListeners()

end

function RoleFashionEffectResolveView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function () self:OnCloseButtonClick() end)
    self._upButton = self:FindChildGO("Button_Up")
    self._csBH:AddClick(self._upButton,function () self:OnUpButtonClick() end)
    self._resolveButton = self:FindChildGO("Button_Resolve")
    self._csBH:AddClick(self._resolveButton,function () self:OnResolveButtonClick() end)

    self._resolveText = self:GetChildComponent("Button_Resolve/Text", "TextMeshWrapper")
    self._titleText = self:GetChildComponent("Image_BG1/Text_Title", "TextMeshWrapper")

    self._levelText = self:GetChildComponent("Image_Level/Text_Level", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")

    self._descText.text = LanguageDataHelper.GetContent(58678)
    self._resolveText.text = LanguageDataHelper.CreateContent(58676)
    self._titleText.text = LanguageDataHelper.CreateContent(58676)

    self:InitProgress()

    self._clothResolveDetailGo = self:FindChildGO("Container_ClothResolveDetail")
    self._clothResolveDetailView = self:AddChildLuaUIComponent("Container_ClothResolveDetail", RoleEffectResolveDetailView)
    self._clothResolveDetailGo:SetActive(false)

    self._clothAttriGo = self:FindChildGO("Container_CurAttriInfo")
    self._clothAttriView = self:AddChildLuaUIComponent("Container_CurAttriInfo", RoleFashionEffectAttriView)
end

function RoleFashionEffectResolveView:InitProgress()
    self._essenceComp = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar_Essence")
    self._essenceComp:SetProgress(0)
    self._essenceComp:ShowTweenAnimate(true)
    self._essenceComp:SetMutipleTween(false)
    self._essenceComp:SetIsResetToZero(false)
    self._essenceComp:SetTweenTime(0.2)

    self:OnEssenceChange()
    self._onEssenceChangeCB = function() self:OnEssenceChange() end
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_FASHION_SPECIAL, self._onEssenceChangeCB)
end

function RoleFashionEffectResolveView:OnEssenceChange()
    local entity = GameWorld.Player()
    local cur_exp = entity.money_fashion_special
    local level = fashionData:GetCurLoadFashionLevel(public_config.FASHION_TYPE_SPECIAL) or 0
    local data = FashionDataHelper.GetLevelDataByTypeAndLevel(public_config.FASHION_TYPE_SPECIAL, level)
    local max_exp = data.levelup_cost[public_config.MONEY_TYPE_FASHION_SPECIAL]
    local value = (cur_exp > max_exp) and 1 or (cur_exp/max_exp)
    self._essenceComp:SetProgress(value)
    self._essenceComp:SetProgressText(tostring(cur_exp).."/"..tostring(max_exp))
end

function RoleFashionEffectResolveView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function RoleFashionEffectResolveView:OnUpButtonClick()
    FashionManager:FashionLevelUpReq(public_config.FASHION_TYPE_SPECIAL)
end

function RoleFashionEffectResolveView:OnResolveButtonClick()
    self._clothResolveDetailGo:SetActive(true)
    self._clothResolveDetailView:ShowView()
end

function RoleFashionEffectResolveView:RefreshView()
    self:RefreshFashionLevel()
    self._clothAttriView:ShowView()
end

function RoleFashionEffectResolveView:RefreshFashionLevel()
    self._levelText.text = "Lv."..fashionData:GetCurLoadFashionLevel(public_config.FASHION_TYPE_SPECIAL)
end

function RoleFashionEffectResolveView:RefreshFashionSelectedClothItem(pos, selectState)
    self._clothResolveDetailView:RefreshSelectedFashionItem(pos, selectState)
end