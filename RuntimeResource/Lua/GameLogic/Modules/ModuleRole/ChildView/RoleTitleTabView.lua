require "Modules.ModuleRole.ChildComponent.RoleTitleAttriItem"
require "Modules.ModuleRole.ChildComponent.RoleTitleFirstLevelItem"
require "Modules.ModuleRole.ChildComponent.RoleTitleSecondLevelItem"

local RoleTitleTabView = Class.RoleTitleTabView(ClassTypes.BaseLuaUIComponent)

RoleTitleTabView.interface = GameConfig.ComponentsConfig.Component
RoleTitleTabView.classPath = "Modules.ModuleRole.ChildView.RoleTitleTabView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local XArtNumber = GameMain.XArtNumber
local LuaUIRawImage = GameMain.LuaUIRawImage
local UIList = ClassTypes.UIList
local RoleTitleAttriItem = ClassTypes.RoleTitleAttriItem
local UINavigationMenu = ClassTypes.UINavigationMenu
local RoleTitleFirstLevelItem = ClassTypes.RoleTitleFirstLevelItem
local RoleTitleSecondLevelItem = ClassTypes.RoleTitleSecondLevelItem
local TitleDataHelper = GameDataHelper.TitleDataHelper
local BaseUtil = GameUtil.BaseUtil
local TitleManager = PlayerManager.TitleManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local PlayerModelComponent = GameMain.PlayerModelComponent
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

local Move_Y = 20

function RoleTitleTabView:Awake()
    self._preF = -1
    self._preFI = -1
    self._preSI = -1
    self._selectedTitleId = -1
    self._state = 0 --0：未达成 1：达成未穿戴 2：达成且穿戴
    self._timer = -1
    self:InitView()
end
--override
function RoleTitleTabView:OnDestroy() 

end

function RoleTitleTabView:OnEnable()
    self:AddEventListeners()
end

function RoleTitleTabView:OnDisable()
    self:RemoveEventListeners()
end

--data结构
function RoleTitleTabView:ShowView(data)
    self:ShowFx(true)
    self:RefreshView()
    self._scrollView:SetScrollRectState(true)
    self._scrollPage:SetScrollRectState(true)
end

function RoleTitleTabView:CloseView()
    self:ShowFx(false)
    self:CancelCountDown()
    self._scrollView:SetScrollRectState(false)
    self._scrollPage:SetScrollRectState(false)
end

function RoleTitleTabView:AddEventListeners()
    self._refreshRoleTitleInfo = function() self:RefreshRoleTitleInfo() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Role_Title_Info, self._refreshRoleTitleInfo)
end

function RoleTitleTabView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Role_Title_Info, self._refreshRoleTitleInfo)
end

function RoleTitleTabView:InitView()
    self._putOnButton = self:FindChildGO("Button_PutOn")
    self._csBH:AddClick(self._putOnButton,function () self:OnPutOnButtonClick() end)
    self._buttonText = self:GetChildComponent("Button_PutOn/Text", "TextMeshWrapper")

    self._titleInfoText = self:GetChildComponent("Container_TitleInfo/Text_Info", "TextMeshWrapper")
    self._timeGo = self:FindChildGO("Text_Time")
    self._timeText = self:GetChildComponent("Text_Time/Text_Value", "TextMeshWrapper")

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self._notGetImage = self:FindChildGO("Image_NotGet")
    self._titleGo = self:FindChildGO("Text_Title")
    self._titleText = self:GetChildComponent("Text_Title", "TextMeshWrapper")

    self._IconContainer = self:FindChildGO("Container_Icon")
    local moveTo = self._IconContainer.transform.localPosition
    moveTo.y = moveTo.y + Move_Y
    self._tweenPos = self:AddChildComponent('Container_Icon', XGameObjectTweenPosition)
    self._tweenPos:SetToPosTween(moveTo, 2, 4, 4, nil)

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/FirstCharge")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Attri/ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(RoleTitleAttriItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 5)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 100)

    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "NavigationMenu")
	self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(RoleTitleFirstLevelItem)
	self._menu:SetSecondLevelItemType(RoleTitleSecondLevelItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(10, 5)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)

end

function RoleTitleTabView:OnMenuFItemClicked(idx)
    if self._preF ~= -1 and self._preF ~= idx then
        local item = self._menu:GetFItem(self._preF)
        item:Reset()
    end
    self._selectedFItem = self._menu:GetFItem(idx)
    self._selectedFItem:ToggleSelected()
    self._preF = idx
end

function RoleTitleTabView:OnMenuSItemClicked(fIndex, sIndex)
    if self._preFI ~= -1 and self._preSI ~= -1 then
        local item = self._menu:GetSItem(self._preFI, self._preSI)
        item:OnSelected(false)
    end
    self._selectedSItem = self._menu:GetSItem(fIndex,sIndex)
    self._selectedSItem:OnSelected(true)
    self._preFI = fIndex
    self._preSI = sIndex
    self._selectedTitleId = self._menu:GetSecondLevelDataByIndex(self._preFI, self._preSI)[1]
    self:RefreshSelectedItem()
    self:RefreshButtonState()
    self:ShowTitle()
    self:ShowTimeInfo()
end

function RoleTitleTabView:OnPutOnButtonClick()
    if self._selectedTitleId == -1 then
        return 
    end
    if self._state == 1 then
        TitleManager:TitleSetTitleReq(self._selectedTitleId)
    elseif self._state == 2 then
        TitleManager:TitleUnsetTitleReq()
    end
end

function RoleTitleTabView:RefreshView()
    local listData = TitleDataHelper.GetTitleShowData()
    self._menu:SetDataList(listData)
    self._menu:SelectFItem(0)
    self:OnMenuSItemClicked(0, 0)
end

function RoleTitleTabView:RefreshSelectedItem()
    local id = self._selectedTitleId
    local attriData = TitleDataHelper.GetTitleAttri(id)
    self._list:SetDataList(attriData)

    local fightPower = BaseUtil.CalculateFightPower(attriData, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)
    self._titleInfoText.text = TitleDataHelper.GetTitleDescribe(id)
end

function RoleTitleTabView:RefreshButtonState()
    self._state = TitleManager:GetStateByTitleId(self._selectedTitleId)
    if self._state == 0 then
        self._putOnButton:SetActive(false)
        self._notGetImage:SetActive(true)
    else
        self._putOnButton:SetActive(true)
        self._notGetImage:SetActive(false)
        if self._state == 1 then
            self._buttonText.text = LanguageDataHelper.GetContent(4502)
        else
            self._buttonText.text = LanguageDataHelper.GetContent(4503)
        end
    end
end

function RoleTitleTabView:ShowTitle()
    local result = TitleDataHelper.GetTitleShowName(self._selectedTitleId)
    if type(result) == "number" then
        self._IconContainer:SetActive(true)
        self._titleGo:SetActive(false)
        --GameWorld.AddIcon(self._IconContainer, result)
        GameWorld.AddIconAnim(self._IconContainer, result)
        GameWorld.SetIconAnimSpeed(self._IconContainer, 5)
    else
        self._IconContainer:SetActive(false)
        self._titleGo:SetActive(true)
        self._titleText.text = result
    end
end

function RoleTitleTabView:ShowTimeInfo()
    if self._state == 0 then
        self._timeGo:SetActive(false)
    else
        self._timeGo:SetActive(true)
        local time = TitleDataHelper.GetTitleTimeLimit(self._selectedTitleId)
        if time == 0 then
            self:CancelCountDown()
            self._timeText.text = LanguageDataHelper.GetContent(226)
        else
            local startTime = TitleManager:GetStartTimestamp(self._selectedTitleId)
            local now = DateTimeUtil.GetServerTime()
            local diff = (startTime + time) - now
            if diff <= 0 then
                self._timeGo:SetActive(false)
            else
                self.countValue = diff
                self:CancelCountDown()
                self._timer = TimerHeap:AddSecTimer(0, 1, 0, function() self:CountDown() end)
            end
        end
    end
end

function RoleTitleTabView:CountDown()
    self.countValue = self.countValue - 1
    self._timeText.text = DateTimeUtil.FormatParseTime(self.countValue)
    if self.countValue <= 0 then
        self:CancelCountDown()
    end
end

function RoleTitleTabView:CancelCountDown()
    if self._timer ~= -1 then
        TimerHeap:DelTimer(self._timer)
        self._timer = -1
    end
end

function RoleTitleTabView:RefreshRoleTitleInfo()
    local listData = TitleDataHelper.GetTitleShowData()
    self._menu:SetDataList(listData)
    local fIndex = 0
    local sIndex = 0
    if self._preFI ~= -1 then
        fIndex = self._preFI
    end
    if self._preSI ~= -1 then
        sIndex = self._preSI
    end
    self._menu:SelectFItem(fIndex)
    self:OnMenuSItemClicked(fIndex, sIndex)
    self:RefreshSelectedItem()
    self:RefreshButtonState()
    self:ShowTitle()
    self:ShowTimeInfo()
end

function RoleTitleTabView:ShowFx(state)
    self._fx.gameObject:SetActive(state)
end

function RoleTitleTabView:OnShowModel()

end

function RoleTitleTabView:OnHideModel()

end