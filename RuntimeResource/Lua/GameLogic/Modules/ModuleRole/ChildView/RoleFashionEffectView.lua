require "Modules.ModuleRole.ChildComponent.RoleFashionClothItem"
require "Modules.ModuleRole.ChildComponent.RoleFashionClothAttriItem"
require "UIComponent.Extend.ItemGrid"
require "Modules.ModuleRole.ChildView.RoleFashionEffectResolveView"
require "Modules.ModuleRole.ChildView.RoleFashionEffectAttriView"

local RoleFashionEffectView = Class.RoleFashionEffectView(ClassTypes.BaseLuaUIComponent)

RoleFashionEffectView.interface = GameConfig.ComponentsConfig.Component
RoleFashionEffectView.classPath = "Modules.ModuleRole.ChildView.RoleFashionEffectView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local RoleFashionClothItem = ClassTypes.RoleFashionClothItem
local FashionDataHelper = GameDataHelper.FashionDataHelper
local XArtNumber = GameMain.XArtNumber
local RoleFashionClothAttriItem = ClassTypes.RoleFashionClothAttriItem
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local LuaUIRawImage = GameMain.LuaUIRawImage
local fashionData = PlayerManager.PlayerDataManager.fashionData
local FashionManager = PlayerManager.FashionManager
local RoleFashionEffectResolveView = ClassTypes.RoleFashionEffectResolveView
local RoleFashionEffectAttriView = ClassTypes.RoleFashionEffectAttriView
local ColorConfig = GameConfig.ColorConfig
local TipsManager = GameManager.TipsManager
local SortOrderedRenderAgent = GameMain.SortOrderedRenderAgent

function RoleFashionEffectView:Awake()
    self._preSelected = -1
    self._selectedId = 0
    self._loaded = false
    self:InitView()
    self:InitListenerFunc()
end
--override
function RoleFashionEffectView:OnDestroy() 

end

function RoleFashionEffectView:InitListenerFunc()
    self._onRefreshFashionInfo = function() self:OnRefreshFashionInfo() end
    self._refreshFashionSelectedEffectItem = function(pos, selectState) self:RefreshFashionSelectedClothItem(pos, selectState) end
end

function RoleFashionEffectView:SetScrollRectState(state)
    if self._scrollView then
        self._scrollView:SetScrollRectState(state)
        self._scrollViewAttri:SetScrollRectState(state)
        self._clothAttriGo:SetActive(state)
    end
end

--data结构
function RoleFashionEffectView:ShowView(data)
    GameWorld.ShowPlayer():SetCurWing(0)
    self:RefreshView()
    self:AddEventListeners()
    local listData = FashionManager:GetFashionDataByType(public_config.FASHION_TYPE_SPECIAL)
    if data ~= nil and data[1] ~= nil then
        for k,v in ipairs(listData) do
            if v.id == data[1] then
                self._list:SetPositionByNum(k)
                self:OnListItemClicked(k-1)
                break
            end
        end
    end
end

function RoleFashionEffectView:CloseView()
    self:RemoveEventListeners()
    GameWorld.ShowPlayer():SyncShowPlayerFacade()
end

function RoleFashionEffectView:OnEnable()

end

function RoleFashionEffectView:OnDisable()
    
end

function RoleFashionEffectView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Fashion_Info, self._onRefreshFashionInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Pkg_Fashion, self._onRefreshFashionInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Fashion_Effect_Info, self._onRefreshFashionInfo)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Fashion_Selected_Effect_Item, self._refreshFashionSelectedEffectItem)
end

function RoleFashionEffectView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Fashion_Info, self._onRefreshFashionInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Pkg_Fashion, self._onRefreshFashionInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Fashion_Effect_Info, self._onRefreshFashionInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Fashion_Selected_Effect_Item, self._refreshFashionSelectedEffectItem)
end

function RoleFashionEffectView:InitView()
    self._putOnButton = self:FindChildGO("Button_PutOn")
    self._csBH:AddClick(self._putOnButton,function () self:OnPutOnButtonClick() end)
    self._putOnText = self:GetChildComponent("Button_PutOn/Text", "TextMeshWrapper")
    self._starButton = self:FindChildGO("Button_Star")
    self._csBH:AddClick(self._starButton,function () self:OnStarButtonClick() end)
    self._levelButton = self:FindChildGO("Button_Level")
    self._csBH:AddClick(self._levelButton,function () self:OnLevelButtonClick() end)
    self._levelText = self:GetChildComponent("Button_Level/Text_Level", "TextMeshWrapper")
    self._activeButton = self:FindChildGO("Button_Active")
    self._csBH:AddClick(self._activeButton,function () self:OnActiveButtonClick() end)
    self._activeButtonWrapper = self._activeButton:GetComponent("ButtonWrapper")

    self._fpNumber = self:AddChildComponent('Container_AttriInfo/Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self._starItemIcon = self:FindChildGO("Container_AttriInfo/Container_Star/Container_Icon")
    local itemGo = GUIManager.AddItem(self._starItemIcon, 1)
    self._starIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._starItemCountText = self:GetChildComponent("Container_AttriInfo/Container_Star/Text_Count", "TextMeshWrapper")

    local scrollViewAttri, listAttri = UIList.AddScrollViewList(self.gameObject, "Container_AttriInfo/Container_Attri/ScrollViewList")
    self._listAttri = listAttri
	self._scrollViewAttri = scrollViewAttri
    self._listAttri:SetItemType(RoleFashionClothAttriItem)
    self._listAttri:SetPadding(0, 0, 0, 0)
    self._listAttri:SetGap(0, 5)
    self._listAttri:SetDirection(UIList.DirectionTopToDown, 1, 100)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(RoleFashionClothItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 5)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 100)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._clothResolveGo = self:FindChildGO("Container_ClothResolve")
    self._clothResolveView = self:AddChildLuaUIComponent("Container_ClothResolve", RoleFashionEffectResolveView)
    self._clothResolveGo:SetActive(false)

    self._clothSelectedAttriGo = self:FindChildGO("Container_AttriInfo")

    self._clothAttriGo = self:FindChildGO("Container_CurAttriInfo")
    self._clothAttriView = self:AddChildLuaUIComponent("Container_CurAttriInfo", RoleFashionEffectAttriView)
end

function RoleFashionEffectView:OnPutOnButtonClick()
    if self._selectedId > 0 then
        if self._loaded then
            FashionManager:FashionUnloadReq(self._selectedId)
        else
            FashionManager:FashionLoadReq(self._selectedId)
        end
    end
end

function RoleFashionEffectView:OnStarButtonClick()
    if self._selectedId > 0 then
        FashionManager:FashionStarUpReq(self._selectedId)
    end
end

function RoleFashionEffectView:OnLevelButtonClick()
    self._clothResolveGo:SetActive(true)
    self._clothResolveView:ShowView()
end

function RoleFashionEffectView:OnActiveButtonClick()
    if self._selectedId > 0 then
        FashionManager:FashionActiveReq(self._selectedId)
    end
end

function RoleFashionEffectView:OnListItemClicked(idx)
    if self._preSelected == idx then
        return
    end
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelect(false)
    end
    self._list:GetItem(idx):ShowSelect(true)
    self._preSelected = idx
    local data = self._list:GetDataByIndex(idx)
    self._selectedId = data.id
    self:RefreshSelectedItemView()
    self:RefreshButtonState()
    self:RefreshModel()
end

function RoleFashionEffectView:ResetItemList()
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelect(false)
    end
    self._preSelected = -1
    self._selectedId = 0
    self._loaded = false
    self:RefreshButtonState()
end

function RoleFashionEffectView:RefreshView()
    self:ResetItemList()
    self._list:SetDataList(FashionManager:GetFashionDataByType(public_config.FASHION_TYPE_SPECIAL))
    self:RefreshFashionLevel()
    self:ShowSelectedAttriView(false)
end

function RoleFashionEffectView:ShowSelectedAttriView(state)
    if state then
        self._clothSelectedAttriGo:SetActive(true)
        self._clothAttriGo:SetActive(false)
    else
        self._clothSelectedAttriGo:SetActive(false)
        self._clothAttriGo:SetActive(true)
        self._clothAttriView:ShowView()
    end
end

function RoleFashionEffectView:RefreshStarItem(itemId)
    self._func = function() TipsManager:ShowItemTipsById(itemId) end
    self._starIcon:SetPointerDownCB(self._func)
    self._starIcon:SetItem(itemId)
    local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    local showText = bagNum.."/1"
    if bagNum >= 1 then
        showText = BaseUtil.GetColorString(showText, ColorConfig.J)
    else
        showText = BaseUtil.GetColorString(showText, ColorConfig.H)
    end
    self._starItemCountText.text = showText
end

function RoleFashionEffectView:RefreshSelectedItemView()
    if self._preSelected ~= -1 then
        self:ShowSelectedAttriView(true)
        local data = self._list:GetDataByIndex(self._preSelected)
        local star = fashionData:GetFashionActiveInfo(data.id)
        local attri = FashionDataHelper.GetStarAttriAndNextDelta(data.id, star)
        local attriData = {}
        for k,v in pairs(attri) do
            table.insert(attriData, {["attri"]=v[1], ["value"]=v[2]})
        end
        local fightPower = BaseUtil.CalculateFightPower(attriData, GameWorld.Player().vocation)
        self._fpNumber:SetNumber(fightPower)
        self._listAttri:SetDataList(attri)
        self:RefreshStarItem(data.id)
    else
        self:ShowSelectedAttriView(false)
    end
end

function RoleFashionEffectView:RefreshButtonState()
    if self._preSelected == -1 then
        self._putOnButton:SetActive(false)
        self._starButton:SetActive(false)
        self._activeButton:SetActive(false)
    else
        local data = self._list:GetDataByIndex(self._preSelected)
        local isLoad = FashionManager:IsFashionLoaded(data.id)
        if data.star == -1 then
            self._putOnButton:SetActive(false)
            self._starButton:SetActive(false)
            if data.hasGot then
                --可激活
                self._activeButton:SetActive(true)
            else
                self._activeButton:SetActive(false)
            end
        end
        if data.star > -1 then
            self._activeButton:SetActive(false)
            self._putOnButton:SetActive(true)
            self._starButton:SetActive(true)
            if isLoad then
                --可卸载
                self._putOnText.text = LanguageDataHelper.GetContent(4503)
                self._loaded = true
            else
                --可穿戴
                self._putOnText.text = LanguageDataHelper.GetContent(4502)
                self._loaded = false
            end
        end
    end
end

function RoleFashionEffectView:OnRefreshFashionInfo()
    self._list:SetDataList(FashionManager:GetFashionDataByType(public_config.FASHION_TYPE_SPECIAL))
    self:RefreshButtonState()
    self:RefreshSelectedItemView()
    self:RefreshFashionLevel()
    self:RefreshFashionClothResolve()
end

function RoleFashionEffectView:RefreshModel()
    local type = FashionDataHelper.GetFashionType(self._selectedId)
    local model = FashionDataHelper.GetFashionModel(self._selectedId)
    if type == public_config.FASHION_TYPE_SPECIAL then
        GameWorld.ShowPlayer():SetCurEffect(model)
        TimerHeap:AddTimer(200, 0, 0, function() SortOrderedRenderAgent.ReorderAll() end)
    end
end

function RoleFashionEffectView:RefreshFashionLevel()
    self._levelText.text = "Lv."..fashionData:GetCurLoadFashionLevel(public_config.FASHION_TYPE_SPECIAL)
end

function RoleFashionEffectView:RefreshFashionSelectedClothItem(pos, selectState)
    self._clothResolveView:RefreshFashionSelectedClothItem(pos, selectState)
end

function RoleFashionEffectView:RefreshFashionClothResolve()
    self._clothResolveView:RefreshView()
end

function RoleFashionEffectView:Reset()
    if self._preSelected == nil then
        return
    end
    self:ResetItemList()
    self:ShowSelectedAttriView(false)
end

function RoleFashionEffectView:ResetSelect()
    GameWorld.ShowPlayer():SetCurWing(0)
    self:RefreshModel()
end