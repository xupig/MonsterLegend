local StorageSubView = Class.StorageSubView(ClassTypes.BaseLuaUIComponent)

StorageSubView.interface = GameConfig.ComponentsConfig.Component
StorageSubView.classPath = "Modules.ModuleRole.ChildView.StorageSubView"
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

require "Modules.ModuleRole.ChildComponent.BagListItem"
local BagListItem = ClassTypes.BagListItem
local UIList = ClassTypes.UIList
local BagManager = PlayerManager.BagManager
local public_config = GameWorld.public_config
local bagData = PlayerManager.PlayerDataManager.bagData
local TipsManager = GameManager.TipsManager
local GUIManager = GameManager.GUIManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType

function StorageSubView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitCallBack()
    self:InitView()
end

function StorageSubView:InitCallBack()
	self._updateCB = function (pkgType)
		if pkgType == self._bagType then
			self:UpdateData()
		end
	end
	
	self._handleClickCB = function ()
		self:HandleClick()
	end
end

function StorageSubView:AddEventListener()
	EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCB)
	EventDispatcher:AddEventListener(GameEvents.BAG_VOLUMN_UPDATE,self._updateCB)
end

function StorageSubView:RemoveEventListener()
	EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCB)
	EventDispatcher:RemoveEventListener(GameEvents.BAG_VOLUMN_UPDATE,self._updateCB)
end

function StorageSubView:InitView()
	local scrollPage, listBag = UIList.AddScrollViewList(self.gameObject,"List_Bag")
	self._listBag = listBag
	self._scrollPage = scrollPage
	self._listBag:SetItemType(BagListItem)
	self._listBag:SetPadding(10, 5, 10, 5)
	self._listBag:SetGap(10, 6)
	self._listBag:SetDirection(UIList.DirectionLeftToRight, 5, 50);
	self._listBag:SetItemClickedCB(function(idx) self:OnItemClicked(idx) end)

	self._lastClickIndex = -1

	self._btnTiny = self:FindChildGO("Button_Tiny")
	self._csBH:AddClick(self._btnTiny, function() self:OnClickTiny() end )

	self._txtVolumn = self:GetChildComponent("Text_Volumn","TextMeshWrapper")
end

function StorageSubView:OnItemClicked(index)
	if self._lastClickIndex == index then
		self._isDoubleClick = true
	else
		self._isDoubleClick = false
	end
	self._lastClickIndex = index
	self._timeId = TimerHeap:AddSecTimer(0.25, 0, 0, self._handleClickCB)
end

function StorageSubView:HandleClick()
	local bagPos = self._lastClickIndex+1
	local data = self._bagData[bagPos]
	if data == nil or data == "nil" then
		return
	end
	--开背包格子
	if data == "lock" then
		local openNum = self._lastClickIndex + 1 - self._curVolumn

		local data = {["id"]=MessageBoxType.OpenBag,["value"]={["openCount"] = openNum,["pkgType"] = self._bagType}}
    	GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
		return
	end
	if self._isDoubleClick then
		if self._bagType == public_config.PKG_TYPE_ITEM then
			local emptyPos = bagData:GetPkg(public_config.PKG_TYPE_WAREHOUSE):GetOneEmtpyPos()
		    if emptyPos > 0 then
		        BagManager:RequestMoveItem(public_config.PKG_TYPE_ITEM,bagPos,public_config.PKG_TYPE_WAREHOUSE,emptyPos)
		    end
		elseif self._bagType == public_config.PKG_TYPE_WAREHOUSE then
			local emptyPos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetOneEmtpyPos()
		    if emptyPos > 0 then
		        BagManager:RequestMoveItem(public_config.PKG_TYPE_WAREHOUSE,bagPos,public_config.PKG_TYPE_ITEM,emptyPos)
		    end
		end
	else
		local btnArgs = {}
		if self._bagType == public_config.PKG_TYPE_ITEM then
			btnArgs.save = true
			TipsManager:ShowItemTips(data,btnArgs)
		elseif self._bagType == public_config.PKG_TYPE_WAREHOUSE then
			btnArgs.get = true
			TipsManager:ShowItemTips(data,btnArgs)
		end
	end
	TimerHeap:DelTimer(self._timeId)
	self._lastClickIndex = -1
end

function StorageSubView:OnClickTiny()
	BagManager:RequestTiny(self._bagType)
end

function StorageSubView:UpdateData()
	if self._bagType == public_config.PKG_TYPE_ITEM then
		self._curVolumn = GameWorld.Player().pkg_items_size
	elseif self._bagType == public_config.PKG_TYPE_WAREHOUSE then
		self._curVolumn = GameWorld.Player().pkg_warehouse_size
	end
	--整个背包的数据
	local t = bagData:GetPkg(self._bagType):GetItemInfos()
	self._bagData = {}
	--填充空数据为了生成空格
	for i=1,self._maxVolumn do
		if i <= self._curVolumn then
	 		if t[i] then
	 			self._bagData[i] = t[i]
	 		else
				self._bagData[i] = "nil"
			end
		else
			self._bagData[i] = "lock"
		end
	end
	self._listBag:SetDataList(self._bagData)
	self:UpdateVolumn()
end

function StorageSubView:UpdateVolumn()
	self._useCount = bagData:GetPkg(self._bagType):GetTotalCount()
	self._txtVolumn.text = self._useCount.."/"..self._curVolumn
end

function StorageSubView:SetBagType(bagType)
	self._bagType = bagType
	if self._bagType == public_config.PKG_TYPE_ITEM then
		--初始容量
		self._baseVolumn = GlobalParamsHelper.GetParamValue(22)
		--最大容量
		self._maxVolumn = GlobalParamsHelper.GetParamValue(24)
	elseif self._bagType == public_config.PKG_TYPE_WAREHOUSE then
		self._baseVolumn = GlobalParamsHelper.GetParamValue(25)
		self._maxVolumn = GlobalParamsHelper.GetParamValue(27)
	end
end

--设定双击回调
function StorageSubView:SetDoubleClickCB(doubleClickCB)
	self._doubleClickCB = doubleClickCB
end