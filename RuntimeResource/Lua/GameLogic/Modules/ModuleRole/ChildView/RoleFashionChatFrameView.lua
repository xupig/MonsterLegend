require "Modules.ModuleRole.ChildComponent.RoleFashionClothItem"
require "Modules.ModuleRole.ChildComponent.RoleFashionClothAttriItem"
require "UIComponent.Extend.ItemGrid"
require "Modules.ModuleRole.ChildView.RoleFashionChatResolveView"
require "Modules.ModuleRole.ChildView.RoleFashionChatAttriView"

local RoleFashionChatFrameView = Class.RoleFashionChatFrameView(ClassTypes.BaseLuaUIComponent)

RoleFashionChatFrameView.interface = GameConfig.ComponentsConfig.Component
RoleFashionChatFrameView.classPath = "Modules.ModuleRole.ChildView.RoleFashionChatFrameView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local RoleFashionClothItem = ClassTypes.RoleFashionClothItem
local FashionDataHelper = GameDataHelper.FashionDataHelper
local XArtNumber = GameMain.XArtNumber
local RoleFashionClothAttriItem = ClassTypes.RoleFashionClothAttriItem
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local LuaUIRawImage = GameMain.LuaUIRawImage
local fashionData = PlayerManager.PlayerDataManager.fashionData
local FashionManager = PlayerManager.FashionManager
local RoleFashionChatResolveView = ClassTypes.RoleFashionChatResolveView
local RoleFashionChatAttriView = ClassTypes.RoleFashionChatAttriView
local ColorConfig = GameConfig.ColorConfig
local TipsManager = GameManager.TipsManager
local SortOrderedRenderAgent = GameMain.SortOrderedRenderAgent
local UIParticle = ClassTypes.UIParticle
local CommonIconType = GameConfig.EnumType.CommonIconType
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

local Move_Y = 20

function RoleFashionChatFrameView:Awake()
    self._preSelected = -1
    self._selectedId = 0
    self._loaded = false
    self:InitCallBack()
    self:InitView()
    self:InitListenerFunc()
end
--override
function RoleFashionChatFrameView:OnDestroy() 

end

function RoleFashionChatFrameView:InitListenerFunc()
    self._onRefreshFashionInfo = function() self:OnRefreshFashionInfo() end
    self._refreshFashionSelectedClothItem = function(pos, selectState) self:RefreshFashionSelectedClothItem(pos, selectState) end
end

function RoleFashionChatFrameView:SetScrollRectState(state)
    if self._scrollView then
        self._scrollView:SetScrollRectState(state)
        self._scrollViewAttri:SetScrollRectState(state)
        self._clothAttriGo:SetActive(state)
    end
end

--data结构
function RoleFashionChatFrameView:ShowView(data)
    EventDispatcher:TriggerEvent(GameEvents.Hide_Player_Model)
    self._fx.gameObject:SetActive(true)
    self:RefreshView()
    self:AddEventListeners()
    local listData = FashionManager:GetFashionDataByType(public_config.FASHION_TYPE_CHAT)
    if data ~= nil and data[1] ~= nil then
        for k,v in ipairs(listData) do
            if v.id == data[1] then
                self._list:SetPositionByNum(k)
                self:OnListItemClicked(k-1)
                break
            end
        end
    end
end

function RoleFashionChatFrameView:CloseView()
    --GameWorld.ShowPlayer():SyncShowPlayerFacade()
    self:RemoveEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.Show_Player_Model)
    self._fx.gameObject:SetActive(false)
end

function RoleFashionChatFrameView:OnEnable()

end

function RoleFashionChatFrameView:OnDisable()

end

function RoleFashionChatFrameView:InitCallBack()
    self._showFashionChatFx = function(state) self:ShowFx(state) end
end

function RoleFashionChatFrameView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Fashion_Info, self._onRefreshFashionInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Pkg_Fashion, self._onRefreshFashionInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Fashion_Chat_Info, self._onRefreshFashionInfo)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Fashion_Selected_Chat_Item, self._refreshFashionSelectedClothItem)
    EventDispatcher:AddEventListener(GameEvents.Show_Fashion_Chat_Fx, self._showFashionChatFx)
end

function RoleFashionChatFrameView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Fashion_Info, self._onRefreshFashionInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Pkg_Fashion, self._onRefreshFashionInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Fashion_Chat_Info, self._onRefreshFashionInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Fashion_Selected_Chat_Item, self._refreshFashionSelectedClothItem)
    EventDispatcher:RemoveEventListener(GameEvents.Show_Fashion_Chat_Fx, self._showFashionChatFx)
end

function RoleFashionChatFrameView:InitView()
    self._putOnButton = self:FindChildGO("Button_PutOn")
    self._csBH:AddClick(self._putOnButton,function () self:OnPutOnButtonClick() end)
    self._putOnText = self:GetChildComponent("Button_PutOn/Text", "TextMeshWrapper")
    self._starButton = self:FindChildGO("Button_Star")
    self._csBH:AddClick(self._starButton,function () self:OnStarButtonClick() end)
    self._levelButton = self:FindChildGO("Button_Level")
    self._csBH:AddClick(self._levelButton,function () self:OnLevelButtonClick() end)
    self._levelText = self:GetChildComponent("Button_Level/Text_Level", "TextMeshWrapper")
    self._activeButton = self:FindChildGO("Button_Active")
    self._csBH:AddClick(self._activeButton,function () self:OnActiveButtonClick() end)
    self._activeButtonWrapper = self._activeButton:GetComponent("ButtonWrapper")

    self._fpNumber = self:AddChildComponent('Container_AttriInfo/Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self._starItemIcon = self:FindChildGO("Container_AttriInfo/Container_Star/Container_Icon")
    local itemGo = GUIManager.AddItem(self._starItemIcon, 1)
    self._starIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._starItemCountText = self:GetChildComponent("Container_AttriInfo/Container_Star/Text_Count", "TextMeshWrapper")

    local scrollViewAttri, listAttri = UIList.AddScrollViewList(self.gameObject, "Container_AttriInfo/Container_Attri/ScrollViewList")
    self._listAttri = listAttri
	self._scrollViewAttri = scrollViewAttri
    self._listAttri:SetItemType(RoleFashionClothAttriItem)
    self._listAttri:SetPadding(0, 0, 0, 0)
    self._listAttri:SetGap(0, 5)
    self._listAttri:SetDirection(UIList.DirectionTopToDown, 1, 100)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(RoleFashionClothItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 5)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 100)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._clothResolveGo = self:FindChildGO("Container_ClothResolve")
    self._clothResolveView = self:AddChildLuaUIComponent("Container_ClothResolve", RoleFashionChatResolveView)
    self._clothResolveGo:SetActive(false)

    self._clothSelectedAttriGo = self:FindChildGO("Container_AttriInfo")

    self._clothAttriGo = self:FindChildGO("Container_CurAttriInfo")
    self._clothAttriView = self:AddChildLuaUIComponent("Container_CurAttriInfo", RoleFashionChatAttriView)

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/FirstCharge")

    self._icon = self:FindChildGO("Container_Icon")
    local moveTo = self._icon.transform.localPosition
    moveTo.y = moveTo.y + Move_Y
    self._tweenPos = self:AddChildComponent('Container_Icon', XGameObjectTweenPosition)
    self._tweenPos:SetToPosTween(moveTo, 2, 4, 4, nil)
end

function RoleFashionChatFrameView:ShowFx(state)
    self._fx.gameObject:SetActive(state)
end

function RoleFashionChatFrameView:OnPutOnButtonClick()
    if self._selectedId > 0 then
        if self._loaded then
            FashionManager:FashionUnloadReq(self._selectedId)
        else
            FashionManager:FashionLoadReq(self._selectedId)
        end
    end
end

function RoleFashionChatFrameView:OnStarButtonClick()
    if self._selectedId > 0 then
        FashionManager:FashionStarUpReq(self._selectedId)
    end
end

function RoleFashionChatFrameView:OnLevelButtonClick()
    self._clothResolveGo:SetActive(true)
    self._clothResolveView:ShowView()
end

function RoleFashionChatFrameView:OnActiveButtonClick()
    if self._selectedId > 0 then
        FashionManager:FashionActiveReq(self._selectedId)
    end
end

function RoleFashionChatFrameView:OnListItemClicked(idx)
    if self._preSelected == idx then
        return
    end
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelect(false)
    end
    self._list:GetItem(idx):ShowSelect(true)
    self._preSelected = idx
    local data = self._list:GetDataByIndex(idx)
    self._selectedId = data.id
    self:RefreshSelectedItemView()
    self:RefreshButtonState()
    self:RefreshModel()
end

function RoleFashionChatFrameView:ResetItemList()
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelect(false)
    end
    self._preSelected = -1
    self._selectedId = 0
    self._loaded = false
    self:RefreshButtonState()
end

function RoleFashionChatFrameView:RefreshView()
    self._list:SetDataList(FashionManager:GetFashionDataByType(public_config.FASHION_TYPE_CHAT))
    self:ResetItemList()
    self:RefreshFashionLevel()
    self:ShowSelectedAttriView(false)
    self:ResetModel()
end

function RoleFashionChatFrameView:ShowSelectedAttriView(state)
    if state then
        self._clothSelectedAttriGo:SetActive(true)
        self._clothAttriGo:SetActive(false)
    else
        self._clothSelectedAttriGo:SetActive(false)
        self._clothAttriGo:SetActive(true)
        self._clothAttriView:ShowView()
    end
end

function RoleFashionChatFrameView:RefreshStarItem(itemId)
    self._func = function() TipsManager:ShowItemTipsById(itemId) end
    self._starIcon:SetPointerDownCB(self._func)
    self._starIcon:SetItem(itemId)
    local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    local showText = bagNum.."/1"
    if bagNum >= 1 then
        showText = BaseUtil.GetColorString(showText, ColorConfig.J)
    else
        showText = BaseUtil.GetColorString(showText, ColorConfig.H)
    end
    self._starItemCountText.text = showText
end

function RoleFashionChatFrameView:RefreshSelectedItemView()
    if self._preSelected ~= -1 then
        self:ShowSelectedAttriView(true)
        local data = self._list:GetDataByIndex(self._preSelected)
        local star = fashionData:GetFashionActiveInfo(data.id)
        local attri = FashionDataHelper.GetStarAttriAndNextDelta(data.id, star)
        local attriData = {}
        for k,v in pairs(attri) do
            table.insert(attriData, {["attri"]=v[1], ["value"]=v[2]})
        end
        local fightPower = BaseUtil.CalculateFightPower(attriData, GameWorld.Player().vocation)
        self._fpNumber:SetNumber(fightPower)
        self._listAttri:SetDataList(attri)
        self:RefreshStarItem(data.id)
    else
        self:ShowSelectedAttriView(false)
    end
end

function RoleFashionChatFrameView:RefreshButtonState()
    if self._preSelected == -1 then
        self._putOnButton:SetActive(false)
        self._starButton:SetActive(false)
        self._activeButton:SetActive(false)
    else
        local data = self._list:GetDataByIndex(self._preSelected)
        local isLoad = FashionManager:IsFashionLoaded(data.id)
        if data.star == -1 then
            self._putOnButton:SetActive(false)
            self._starButton:SetActive(false)
            if data.hasGot then
                --可激活
                self._activeButton:SetActive(true)
            else
                self._activeButton:SetActive(false)
            end
        end
        if data.star > -1 then
            self._activeButton:SetActive(false)
            self._putOnButton:SetActive(true)
            self._starButton:SetActive(true)
            if isLoad then
                --可卸载
                self._putOnText.text = LanguageDataHelper.GetContent(4503)
                self._loaded = true
            else
                --可穿戴
                self._putOnText.text = LanguageDataHelper.GetContent(4502)
                self._loaded = false
            end
        end
    end
end

function RoleFashionChatFrameView:OnRefreshFashionInfo()
    self._list:SetDataList(FashionManager:GetFashionDataByType(public_config.FASHION_TYPE_CHAT))
    self:RefreshButtonState()
    self:RefreshSelectedItemView()
    self:RefreshFashionLevel()
    self:RefreshFashionClothResolve()
end

function RoleFashionChatFrameView:ResetModel()
    local data = self._list:GetDataByIndex(0)
    local type = FashionDataHelper.GetFashionType(data.id)
    local model = FashionDataHelper.GetFashionModel(data.id)
    if type == public_config.FASHION_TYPE_CHAT then
        GUIManager.AddCommonIcon(self._icon, CommonIconType.Bubble, model)
    end
end

function RoleFashionChatFrameView:RefreshModel()
    local type = FashionDataHelper.GetFashionType(self._selectedId)
    local model = FashionDataHelper.GetFashionModel(self._selectedId)
    if type == public_config.FASHION_TYPE_CHAT then
        GUIManager.AddCommonIcon(self._icon, CommonIconType.Bubble, model)
    end
end

function RoleFashionChatFrameView:RefreshFashionLevel()
    self._levelText.text = "Lv."..fashionData:GetCurLoadFashionLevel(public_config.FASHION_TYPE_CHAT)
end

function RoleFashionChatFrameView:RefreshFashionSelectedClothItem(pos, selectState)
    self._clothResolveView:RefreshFashionSelectedClothItem(pos, selectState)
end

function RoleFashionChatFrameView:RefreshFashionClothResolve()
    self._clothResolveView:RefreshView()
end

function RoleFashionChatFrameView:Reset()
    if self._preSelected == nil then
        return
    end
    self:ResetItemList()
    self:ShowSelectedAttriView(false)
    self._fx.gameObject:SetActive(false)
end

function RoleFashionChatFrameView:ResetSelect()

end