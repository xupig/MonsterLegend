local RoleBagTabView = Class.RoleBagTabView(ClassTypes.BaseLuaUIComponent)

RoleBagTabView.interface = GameConfig.ComponentsConfig.Component
RoleBagTabView.classPath = "Modules.ModuleRole.ChildView.RoleBagTabView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local BagManager = PlayerManager.BagManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local TipsManager = GameManager.TipsManager
local LuaUIRawImage = GameMain.LuaUIRawImage
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local PlayerModelComponent = GameMain.PlayerModelComponent

require "Modules.ModuleRole.ChildComponent.SingleEquipItem"
require "Modules.ModuleRole.ChildComponent.BagListItem"
require "Modules.ModuleRole.ChildView.RoleBagSellView"
require "Modules.ModuleRole.ChildView.RoleBagToPreciousView"
local SingleEquipItem = ClassTypes.SingleEquipItem
local BagListItem = ClassTypes.BagListItem
local RoleBagSellView = ClassTypes.RoleBagSellView
local RoleBagToPreciousView = ClassTypes.RoleBagToPreciousView
local PanelsConfig = GameConfig.PanelsConfig
local VIPManager = PlayerManager.VIPManager

function RoleBagTabView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitCallBack()
    self:InitView()
end
--override
function RoleBagTabView:OnDestroy() 
    
end

--data结构
function RoleBagTabView:ShowView()
	self:AddEventListeners()
	self._listBag:SetListItemRenderingAgent(true)
	self._scrollPage:SetScrollRectState(true)
	self:RefreshView()
	self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
	EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleRole.ChildView.RoleBagTabView")
end

function RoleBagTabView:CloseView()
	self:RemoveEventListeners()
	self._listBag:SetListItemRenderingAgent(false)
	self._scrollPage:SetScrollRectState(false)
end

function RoleBagTabView:InitCallBack()
	self._equipClickCb = function (selectedEquipData,index)
		self:OnEquipClick(selectedEquipData,index)
	end

	self._updateCb = function (pkgType)
		self:UpdateData(pkgType)
	end

	self._closeSellCb = function ()
		self:CloseSell()
	end

	self._closeToPreciousCb = function ()
		self:CloseToPrecious()
	end

	self._showRoleModelCb = function ()
		self:ShowRoleModel()
	end

	self._onSellComplete = function ()
        self:OnSellComplete()
    end

    self._setPlayerModelCb = function ()
    	self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
    end
end

function RoleBagTabView:AddEventListeners()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_PLAYER_EQUIP_CLICK,self._equipClickCb)
	EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCb)
	EventDispatcher:AddEventListener(GameEvents.BAG_VOLUMN_UPDATE,self._updateCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_CLOSE_ROLE_BAG_SELL,self._closeSellCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_CLOSE_TO_PRECIOUS,self._closeToPreciousCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_BAG_VIEW_SHOW_ROLE_MODEL,self._showRoleModelCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_BAG_SELL_COMPLETE,self._onSellComplete)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_BAG_RETSET_PLAYER_MODEL,self._setPlayerModelCb)
end

function RoleBagTabView:RemoveEventListeners()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_PLAYER_EQUIP_CLICK,self._equipClickCb)
	EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCb)
	EventDispatcher:RemoveEventListener(GameEvents.BAG_VOLUMN_UPDATE,self._updateCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_CLOSE_ROLE_BAG_SELL,self._closeSellCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_CLOSE_TO_PRECIOUS,self._closeToPreciousCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_BAG_VIEW_SHOW_ROLE_MODEL,self._showRoleModelCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_BAG_SELL_COMPLETE,self._onSellComplete)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_BAG_RETSET_PLAYER_MODEL,self._setPlayerModelCb)
end

function RoleBagTabView:InitView()
	--人物模型
	self._containerModel = self:FindChildGO("Container_Model")
	self._modelComponent = self:AddChildComponent('Container_Model', PlayerModelComponent)

    --背包最大容量
	local bagBaseVolumn = GlobalParamsHelper.GetParamValue(22)
	--local bagExpandLevels = GlobalParamsHelper.GetParamValue(23)
	self._maxVolumn = GlobalParamsHelper.GetParamValue(24)

	local scrollPage, listBag = UIList.AddScrollViewList(self.gameObject,"List_Bag")
	self._listBag = listBag
	self._scrollPage = scrollPage
	self._listBag:SetItemType(BagListItem)
	self._listBag:SetPadding(10, 5, 10, 5)
	self._listBag:SetGap(10, 6)
	self._listBag:SetDirection(UIList.DirectionLeftToRight, 5, math.ceil(self._maxVolumn/5));
	self._listBag:SetItemClickedCB(function(idx) self:OnItemClicked(idx) end)

	self._txtVolumn = self:GetChildComponent("Text_Volumn","TextMeshWrapper")

	self._btnTiny = self:FindChildGO("Button_Tiny")
	self._btnStorage = self:FindChildGO("Button_Storage")
	self._btnOneClickSell = self:FindChildGO("Button_OneClickSell")

	self._csBH:AddClick(self._btnTiny, function() self:OnClickTinyBag() end )
	self._csBH:AddClick(self._btnStorage, function() self:OnClickOpenStorage() end )
	self._csBH:AddClick(self._btnOneClickSell, function() self:OnClickSell() end )

	--装备部分
	self._btnEquipStrength = self:FindChildGO("Container_RoleEquip/Button_EquipStrength")
	self._btnEquipStar = self:FindChildGO("Container_RoleEquip/Button_EquipStar")
	self._btnEquipGem = self:FindChildGO("Container_RoleEquip/Button_EquipGem")
	-- self._strengthIconObj = self:FindChildGO("Container_RoleEquip/Button_EquipStrength/Container_Icon")
	-- GameWorld.AddIcon(self._strengthIconObj, 1)
	-- self._starIconObj = self:FindChildGO("Container_RoleEquip/Button_EquipStar/Container_Icon")
	-- GameWorld.AddIcon(self._starIconObj, 2)
	-- self._gemIconObj = self:FindChildGO("Container_RoleEquip/Button_EquipGem/Container_Icon")
	-- GameWorld.AddIcon(self._gemIconObj, 3)

	self._csBH:AddClick(self._btnEquipStrength, function() self:OnEquipStrength() end )
	self._csBH:AddClick(self._btnEquipStar, function() self:OnEquipStar() end )
	self._csBH:AddClick(self._btnEquipGem, function() self:OnEquipGem() end )

	-- self._btnEquipStrength:SetActive(false)
	-- self._btnEquipStar:SetActive(false)
	-- self._btnEquipGem:SetActive(false)
	
	-- self._containerEquip = self:FindChildGO("Container_RoleEquip/Container_Equips")
	-- self._containerEquip:SetActive(true)
	self._slotToType ={1,2,3,4,5,6,7,8,9,10,11,12}
    self._slotCount = 12
	self._equipSlots = {}
	self._equipSlotsGO = {}
	self._typeIconGO = {}
	-- self._selectedFrame = self:FindChildGO("Container_RoleEquip/Container_Equips/Image_SelectedFrame")
	-- self._selectedFrame:SetActive(false)
    for i=1,self._slotCount do
    	self._equipSlotsGO[i] = self:FindChildGO("Container_RoleEquip/Container_Equips/Container_Equip"..i)
        local item = self:AddChildLuaUIComponent("Container_RoleEquip/Container_Equips/Container_Equip"..i.."/Button_Equip",SingleEquipItem)
		self._equipSlots[i] = item
		self._equipSlots[i]:ActivateSuitIcon(GameWorld.Player().equip_suit_data)
		self._typeIconGO[i] = self:FindChildGO("Container_RoleEquip/Container_Equips/Container_TypeIcon/Image_TypeIcon"..i)
    end

    --出售界面
    self._sellView = self:AddChildLuaUIComponent("Container_Sell", RoleBagSellView)
    self._sellView:SetActive(false)

    --跳转到装备寻宝
    self._toPreciousView = self:AddChildLuaUIComponent("Container_GetRareEquip", RoleBagToPreciousView)
    self._toPreciousView:SetActive(false)
end

function RoleBagTabView:OnEquipClick(selectedEquipData,index)
	if selectedEquipData then
		local btnArgs = {}
		btnArgs.renew = true
		TipsManager:ShowItemTips(selectedEquipData,btnArgs,false,false,nil)
	else
		if index == 9 or index == 10 then
			self:OnClickToPrecious(index)
		end
	end
end

function RoleBagTabView:OnItemClicked(index)
	local data = self._bagData[index+1]
	if data == "nil" then
		return
	end
	if data == "lock" then
		local openNum = index + 1 - self._curVolumn
		local data = {["id"]=MessageBoxType.OpenBag,["value"]={["openCount"] = openNum,["pkgType"] = public_config.PKG_TYPE_ITEM}}
    	GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
		return
	end

	--VIP体验卡特殊处理,开启体验窗口
	if data.cfgData.effectId and data.cfgData.effectId[1] == 13 and data.cfgData.effectId[2] > 100 then
		self:HideRoleModel()
		VIPManager:SetVipPopTypeExpire(false)
		GUIManager.ShowPanel(PanelsConfig.VIPPop,{["showType"] = 1,["cardId"] = data.cfg_id})
		return
	end
	
	local btnArgs = {}
	btnArgs.use = true
	btnArgs.sell = true
	btnArgs.renew = true
	btnArgs.useRing = true
	btnArgs.equip = true
	btnArgs.forSale = true
	btnArgs.combine = true
	btnArgs.split = true

	TipsManager:ShowItemTips(data,btnArgs,true,false,nil)
end

function RoleBagTabView:UpdateData(pkgType)
	if pkgType == public_config.PKG_TYPE_ITEM then
		self:UpdateBag()
	elseif pkgType == public_config.PKG_TYPE_LOADED_EQUIP then
		self:UpdateEquips()
		self:UpdatePowerCompare()
	end
end

function RoleBagTabView:RefreshView()
 	self:UpdateBag()
 	self:UpdateEquips()
end

function RoleBagTabView:UpdateBag()
	--整个背包的数据
	local t = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
	self._bagData = {}
	self._curVolumn = GameWorld.Player().pkg_items_size
	--填充空数据为了生成空格
	for i=1,self._maxVolumn do
		if i <= self._curVolumn then
	 		if t[i] then
	 			self._bagData[i] = t[i]
	 		else
				self._bagData[i] = "nil"
			end
		else
			self._bagData[i] = "lock"
		end
	end
	self._listBag:SetOnAllItemCreatedCB(function() self:OnContentListAllItemCreated() end)
	self._listBag:SetDataList(self._bagData, 5)--使用协程加载
	-- self._listBag:SetDataList(self._bagData)
	self:UpdateVolumn()
end
	
function RoleBagTabView:OnContentListAllItemCreated()
	self:UpdatePowerCompare()
end

function RoleBagTabView:UpdatePowerCompare()
	--检查装备是否能装备/战力是否更强
	for i=1,self._maxVolumn do
		local itemData = self._bagData[i]
		local listItem = self._listBag:GetItem(i-1)
		if itemData ~= "nil" and itemData ~= "lock" and itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP then
			if itemData:CheckVocationMatchGeneral() then
				if itemData:CheckBetterThanLoaded() then
					listItem:SetUseAndPowerCompare(true,false,true)
				else
					listItem:SetUseAndPowerCompare(true,false,false)
				end
			else
				listItem:SetUseAndPowerCompare(true,true)
			end
			if itemData:CheckGuardTimeOut() then
				listItem:ShowTipInfo(LanguageDataHelper.CreateContent(595))
			else
				listItem:ShowTipInfo(nil)
			end
		else
			listItem:ShowTipInfo(nil)
			listItem:SetUseAndPowerCompare(false)
		end
	end
end

function RoleBagTabView:UpdateVolumn()
	self._useCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetTotalCount()
	self._txtVolumn.text = self._useCount.."/"..self._curVolumn
end

function RoleBagTabView:UpdateEquips()
	local type,data
	local score = 0
    for i=1,self._slotCount do
        type = self._slotToType[i]
        data = self:GetEquipedDataBySlot(type)
        local equipData = data[1]
        self._equipSlots[i]:UpdateData(equipData,i)
        self._typeIconGO[i]:SetActive(equipData == nil)
        -- if equipData then 
        -- 	score = score + equipData:GetFightPower()
        -- end
    end
    --self._txtScore.text = tostring(score)
end

--获取装备背包上某个类型装备
function RoleBagTabView:GetEquipedDataBySlot(type)
    local equipdata = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetEquipListBySubType(type)
    return equipdata
end


function RoleBagTabView:OnClickTinyBag()
	BagManager:RequestTiny(public_config.PKG_TYPE_ITEM)
end

function RoleBagTabView:OnClickOpenStorage()
	self:HideRoleModel()
	GUIManager.ShowPanel(PanelsConfig.Storage)
end

function RoleBagTabView:HideRoleModel()
	self._containerModel:SetActive(false)
end

function RoleBagTabView:ShowRoleModel()
	self._containerModel:SetActive(true)
end

function RoleBagTabView:OnClickSell()
	self:HideRoleModel()
	self._sellView:SetActive(true)
	self._sellView:AddEventListeners()
	self._sellView:RefreshView()
	EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_bag_one_click_sell_button")
end

function RoleBagTabView:CloseSell()
	self:ShowRoleModel()
	self._sellView:SetActive(false)
	self._sellView:RemoveEventListeners()
end

--卖完整理背包
function RoleBagTabView:OnSellComplete()
    BagManager:RequestTiny(public_config.PKG_TYPE_ITEM)
end

-----------------------------------------------------------------------
function RoleBagTabView:OnClickToPrecious(index)
	self:HideRoleModel()
	self._toPreciousView:SetActive(true)
	self._toPreciousView:RefreshView(index)
end

function RoleBagTabView:CloseToPrecious()
	self:ShowRoleModel()
	self._toPreciousView:SetActive(false)
end

function RoleBagTabView:OnShowModel()
    self._modelComponent:Show()
end

function RoleBagTabView:OnHideModel()
    self._modelComponent:Hide()
end

-----------------------------------------------------------------------
function RoleBagTabView:OnEquipStrength()
	local data = {}
	data.tipsType = 3
	data.subType = 1
	-- data.dragonSoulItemData = dragonSoulItemData
	-- data.buttonArgs = buttonArgs
	-- data.isBag = isBag 
	-- data.slot = slot
	GUIManager.ShowPanel(PanelsConfig.Tips,data)
end

function RoleBagTabView:OnEquipStar()
	local data = {}
	data.tipsType = 3
	data.subType = 2
	GUIManager.ShowPanel(PanelsConfig.Tips,data)
end

function RoleBagTabView:OnEquipGem()
	local data = {}
	data.tipsType = 3
	data.subType = 3
	GUIManager.ShowPanel(PanelsConfig.Tips,data)
end