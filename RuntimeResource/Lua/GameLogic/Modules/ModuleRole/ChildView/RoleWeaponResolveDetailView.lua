require "Modules.ModuleRole.ChildComponent.RoleFashionClothAttriItem"
require "Modules.ModuleRole.ChildComponent.RoleFashionWeaponResolveItem"

local RoleWeaponResolveDetailView = Class.RoleWeaponResolveDetailView(ClassTypes.BaseLuaUIComponent)

RoleWeaponResolveDetailView.interface = GameConfig.ComponentsConfig.Component
RoleWeaponResolveDetailView.classPath = "Modules.ModuleRole.ChildView.RoleWeaponResolveDetailView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local FashionDataHelper = GameDataHelper.FashionDataHelper
local XArtNumber = GameMain.XArtNumber
local RoleFashionClothAttriItem = ClassTypes.RoleFashionClothAttriItem
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local LuaUIRawImage = GameMain.LuaUIRawImage
local fashionData = PlayerManager.PlayerDataManager.fashionData
local FashionManager = PlayerManager.FashionManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIProgressBar = ClassTypes.UIProgressBar
local UIToggle = ClassTypes.UIToggle
local RoleFashionWeaponResolveItem = ClassTypes.RoleFashionWeaponResolveItem

local Min_Item_Count = 24

function RoleWeaponResolveDetailView:Awake()
    self._selectedItemList = {}
    self:InitView()
end
--override
function RoleWeaponResolveDetailView:OnDestroy() 

end

--data结构
function RoleWeaponResolveDetailView:ShowView(data)
    self:RefreshView()
end

function RoleWeaponResolveDetailView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function () self:OnCloseButtonClick() end)
    self._resolveButton = self:FindChildGO("Button_Resolve")
    self._csBH:AddClick(self._resolveButton,function () self:OnResolveButtonClick() end)

    self._titleText = self:GetChildComponent("Image_BG1/Text_Title", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
    self._descText.text = LanguageDataHelper.GetContent(58607)
    self._titleText.text = LanguageDataHelper.CreateContent(58675)

    self._selectToggle = UIToggle.AddToggle(self.gameObject, "Toggle_Select")
    self._selectToggle:SetIsOn(false)
    self._selectToggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

    self._essenceText = self:GetChildComponent("Text_Essence/Text_Value", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(RoleFashionWeaponResolveItem)
    self._list:SetPadding(10, 0, 0, 30)
    self._list:SetGap(10, 5)
    self._list:SetDirection(UIList.DirectionLeftToRight, 8, 100)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

end

function RoleWeaponResolveDetailView:OnListItemClicked(idx)
    local item = self._list:GetItem(idx)
    item:ChangeState()
end

function RoleWeaponResolveDetailView:OnToggleValueChanged(state)
    self:SelectedAll(state)
end

function RoleWeaponResolveDetailView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function RoleWeaponResolveDetailView:OnResolveButtonClick()
    self:OnCloseButtonClick()
    local posList = {}
    for k,_ in pairs(self._selectedItemList) do
        table.insert(posList, k)
    end
    if #posList <= 0 then
        return
    end
    FashionManager:FashionResolveReq(posList)
end

function RoleWeaponResolveDetailView:RefreshView()
    local fashionData = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemListByItemType(public_config.ITEM_ITEMTYPE_FASHION)
    local needData = {}
    local count = 0
    for k,v in pairs(fashionData) do
        if v.cfgData.type == public_config.FASHION_TYPE_WEAPON then
            table.insert(needData, v)
            count = count + 1
        end
    end
    --填充空数据为了生成空格
    local addCount = math.max(Min_Item_Count, count) - count
	for i=1, addCount do
        table.insert(needData, {})
	end
    self._list:SetDataList(needData)
    self:ShowAllSelectEssence()
end

function RoleWeaponResolveDetailView:SelectedAll(state)
    self._selectedItemList = {}
    for i=0, self._list:GetLength()-1  do
        local data = self._list:GetDataByIndex(i)
        local item = self._list:GetItem(i)
        if state then
            item:Selected()
        else
            item:Reset()
        end
    end
    self:ShowAllSelectEssence()
end

function RoleWeaponResolveDetailView:ShowAllSelectEssence()
    local sum = 0
    for k,v in pairs(self._selectedItemList) do
        local itemData = bagData:GetItem(public_config.PKG_TYPE_ITEM, k)
        if itemData ~= nil then
            local id = itemData.cfg_id
            local essence = FashionDataHelper.GetFashionResolve(id)[public_config.MONEY_TYPE_FASHION_WEAPON] or 0
            sum = sum + essence
        end
    end
    self._essenceText.text = "+"..sum
end

function RoleWeaponResolveDetailView:RefreshSelectedFashionItem(pos, selectState)
    if selectState then
        self._selectedItemList[pos] = 1
    else
        self._selectedItemList[pos] = nil
    end
    self:ShowAllSelectEssence()
end