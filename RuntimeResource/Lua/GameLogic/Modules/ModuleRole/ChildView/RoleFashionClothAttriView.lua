require "Modules.ModuleRole.ChildComponent.RoleFashionClothTotalAttriItem"
require "Modules.ModuleRole.ChildComponent.RoleFashionClothAdditionItem"

local RoleFashionClothAttriView = Class.RoleFashionClothAttriView(ClassTypes.BaseLuaUIComponent)

RoleFashionClothAttriView.interface = GameConfig.ComponentsConfig.Component
RoleFashionClothAttriView.classPath = "Modules.ModuleRole.ChildView.RoleFashionClothAttriView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local FashionDataHelper = GameDataHelper.FashionDataHelper
local XArtNumber = GameMain.XArtNumber
local RoleFashionClothTotalAttriItem = ClassTypes.RoleFashionClothTotalAttriItem
local fashionData = PlayerManager.PlayerDataManager.fashionData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleFashionClothAdditionItem = ClassTypes.RoleFashionClothAdditionItem

function RoleFashionClothAttriView:Awake()
    self:InitView()
end
--override
function RoleFashionClothAttriView:OnDestroy() 

end

--data结构
function RoleFashionClothAttriView:ShowView(data)
    self:RefreshView()
end

function RoleFashionClothAttriView:InitView()
    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    local scrollViewOne, listOne = UIList.AddScrollViewList(self.gameObject, "Container_Attri1/ScrollViewList")
    self._listOne = listOne
	self._scrollViewOne = scrollViewOne
    self._listOne:SetItemType(RoleFashionClothTotalAttriItem)
    self._listOne:SetPadding(0, 0, 0, 0)
    self._listOne:SetGap(0, 5)
    self._listOne:SetDirection(UIList.DirectionTopToDown, 1, 100)

    local scrollViewTwo, listTwo = UIList.AddScrollViewList(self.gameObject, "Container_Attri2/ScrollViewList")
    self._listTwo = listTwo
	self._scrollViewTwo = scrollViewTwo
    self._listTwo:SetItemType(RoleFashionClothAdditionItem)
    self._listTwo:SetPadding(0, 0, 0, 0)
    self._listTwo:SetGap(0, 5)
    self._listTwo:SetDirection(UIList.DirectionTopToDown, 1, 100)
end

function RoleFashionClothAttriView:RefreshView()
    local attriData = fashionData:GetActiveFashionAttri(public_config.FASHION_TYPE_CLOTHES)
    self._listOne:SetDataList(attriData)
    local attri = {}
    for k,v in pairs(attriData) do
        table.insert(attri, {["attri"]=v[1], ["value"]=v[2]})
    end
    local fightPower = BaseUtil.CalculateFightPower(attri, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)

    local attriAdditionData = fashionData:GetCurLoadFashionAddition(public_config.FASHION_TYPE_CLOTHES)
    self._listTwo:SetDataList(attriAdditionData)
end