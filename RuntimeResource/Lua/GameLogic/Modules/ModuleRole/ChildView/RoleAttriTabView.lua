require "Modules.ModuleRole.ChildView.RoleAttriInfoBlockView"
require "Modules.ModuleRole.ChildView.RoleAttriWorldLevelView"
require "Modules.ModuleRole.ChildComponent.RolePKItem"

local RoleAttriTabView = Class.RoleAttriTabView(ClassTypes.BaseLuaUIComponent)

RoleAttriTabView.interface = GameConfig.ComponentsConfig.Component
RoleAttriTabView.classPath = "Modules.ModuleRole.ChildView.RoleAttriTabView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIProgressBar = ClassTypes.UIProgressBar
local XArtNumber = GameMain.XArtNumber
local EntityConfig = GameConfig.EntityConfig
local RoleDataHelper = GameDataHelper.RoleDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local RoleAttriInfoBlockView = ClassTypes.RoleAttriInfoBlockView
local combatData = PlayerManager.PlayerDataManager.combatData
local Rect = UnityEngine.Rect
local RoleAttriWorldLevelView = ClassTypes.RoleAttriWorldLevelView
local PlayerModelComponent = GameMain.PlayerModelComponent
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RolePKItem = ClassTypes.RolePKItem
local bagData = PlayerManager.PlayerDataManager.bagData

local Max_Attri_Category = 2 --总的子title个数，策划配表要保持连续，即1,2,3不能1,3,4
--根据UI搭建界面直接定死，不进行动态适应
local Title_Text_Height = 30
local Modified_Variable = 30
local Item_Height = 30 --GridLayoutGroup的cell_size的值

local Set_Height = Vector3.New(0,0,0)
local Max_PK_Item_Count = 1

function RoleAttriTabView:Awake()
    self._idToCount = {}
    self._idToHeight = {}
    self:InitView()
    self:InitListenerFunc()
end
--override
function RoleAttriTabView:OnDestroy() 

end

function RoleAttriTabView:InitListenerFunc()
    self._onGetWorldLevel = function() self:OnGetWorldLevel() end
    self._onAttrRefresh = function() self:OnAttrRefresh() end
    self._onCharmChangeCB = function() self:OnCharmChange() end
    self._onLevelChangeCB = function() self:OnLevelChange() end
    self._onFightForceChangeCB = function() self:OnFightForceChange() end
    self._onPKValueChangeCB = function() self:OnPKValueChange() end
    self._onPKItemChangeCB = function() self:OnPKItemChange() end
end

--data结构
function RoleAttriTabView:ShowView(data)
    self:AddEventListeners()
    self._attriScrollViewGO:SetActive(true)
    self:RefreshView()
    self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
end

function RoleAttriTabView:CloseView()
    self:RemoveEventListeners()
    self._attriScrollViewGO:SetActive(false)
end

function RoleAttriTabView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.On_Get_World_Level, self._onGetWorldLevel)
    EventDispatcher:AddEventListener(GameEvents.ON_ATTR_REFRESH, self._onAttrRefresh)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CHARM, self._onCharmChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_FIGHT_FORCE, self._onFightForceChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PK_VALUE, self._onPKValueChangeCB)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_PK_Item_Info, self._onPKItemChangeCB)
end

function RoleAttriTabView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.On_Get_World_Level, self._onGetWorldLevel)
    EventDispatcher:RemoveEventListener(GameEvents.ON_ATTR_REFRESH, self._onAttrRefresh)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CHARM, self._onCharmChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_FIGHT_FORCE, self._onFightForceChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PK_VALUE, self._onPKValueChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_PK_Item_Info, self._onPKItemChangeCB)
end

function RoleAttriTabView:InitView()
    self._intimacyButton = self:FindChildGO("Button_Intimacy")
    self._csBH:AddClick(self._intimacyButton,function () self:OnIntimacyButtonClick() end)
    self._worldLevelButton = self:FindChildGO("Container_AttriShow/Button_WorldLevel")
    self._csBH:AddClick(self._worldLevelButton,function () self:OnWorldLevelButtonClick() end)
    self._clearButton = self:FindChildGO("Container_AttriShow/Button_Clear")
    self._csBH:AddClick(self._clearButton,function () self:OnClearButtonClick() end)

    self._intimacyText = self:GetChildComponent("Button_Intimacy/Text_Value", "TextMeshWrapper")

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)
    
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._nameText.text = GameWorld.Player().name

    self._modelComponent = self:AddChildComponent('Container_Model', PlayerModelComponent)

    self._levelAttrText = self:GetChildComponent("Container_AttriShow/Text_Level/Text_Value", "TextMeshWrapper")
    self._guildText = self:GetChildComponent("Container_AttriShow/Text_Guild/Text_Value", "TextMeshWrapper")
    self._pkValueText = self:GetChildComponent("Container_AttriShow/Text_PK/Text_Value", "TextMeshWrapper")

    self._rectTransform = self:GetChildComponent("Container_AttriShow/ScrollView_Attri/Mask/Content", "RectTransform")
    self._attriInfoBlockGO = self:FindChildGO("Container_AttriShow/ScrollView_Attri/Mask/Content/Container_AttriInfo")
    self._attriScrollViewGO = self:FindChildGO("Container_AttriShow/ScrollView_Attri")
    self._attriGO = {}
    self._attriView = {}
    self._startPos = self._attriInfoBlockGO.transform.localPosition
    for i=1, Max_Attri_Category do
        self._attriGO[i] = self:CopyUIGameObject("Container_AttriShow/ScrollView_Attri/Mask/Content/Container_AttriInfo","Container_AttriShow/ScrollView_Attri/Mask/Content/")
        self._attriView[i] = UIComponentUtil.AddLuaUIComponent(self._attriGO[i], RoleAttriInfoBlockView)
    end
    self._attriInfoBlockGO:SetActive(false)

    self._worldLevelGo = self:FindChildGO("Container_WorldLevel")
    self._worldLevelGo:SetActive(true)
    self._worldLevelView = self:AddChildLuaUIComponent("Container_WorldLevel", RoleAttriWorldLevelView)
    self._worldLevelGo:SetActive(false)

    self._containerPK = self:FindChildGO("Container_AttriShow/Container_PKItem")
    self._containerPK:SetActive(true)
	local allPKItem = ItemDataHelper.GetAllItemByEffectId(31)
	self._pkItemIds = {}
	self._pkItems = {}
	for i=1,Max_PK_Item_Count do
		local itemCfg = allPKItem[i]
        table.insert(self._pkItemIds, itemCfg.id)
	end
	
	for i=1,Max_PK_Item_Count do
		self._pkItems[i] = self:AddChildLuaUIComponent("Container_AttriShow/Container_PKItem/Container_Items/Container_Item"..i, RolePKItem)
		self._pkItems[i]:UpdateData(self._pkItemIds[i])
	end

	self._btnClosePK = self:FindChildGO("Container_AttriShow/Container_PKItem/Button_Close")
    self._csBH:AddClick(self._btnClosePK, function() self:OnClosePK() end)
    self._containerPK:SetActive(false)

    self:SetGuildName()

    self:InitProgress()
end

function RoleAttriTabView:RefreshScrollView()
    self._idToHeight[1] = 0
    local sum = self._idToHeight[1]
    for i=2, Max_Attri_Category + 1 do
        local count = self._idToCount[i-1]
        local h = math.ceil(count/2) * Item_Height + Title_Text_Height + Modified_Variable
        sum = sum + h
        self._idToHeight[i] = sum
    end
    for i=1, Max_Attri_Category do
        Set_Height.y = self._idToHeight[i]*-1
        self._attriGO[i].transform.localPosition = self._startPos + Set_Height
    end
    self._rectTransform.sizeDelta = Vector2.New(self._rectTransform.sizeDelta.x, self._idToHeight[Max_Attri_Category + 1])
end

function RoleAttriTabView:InitProgress()
    self._lastLevel = GameWorld.Player().level

    self._expComp = UIProgressBar.AddProgressBar(self.gameObject,"Container_AttriShow/ProgressBar_Exp")
    self._expComp:SetProgress(0)
    self._expComp:ShowTweenAnimate(true)
    self._expComp:SetMutipleTween(true)
    self._expComp:SetIsResetToZero(false)
    self._expComp:SetTweenTime(0.2)

    self:OnExpChange()
    self._onExpChangeCB = function() self:OnExpChange() end
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_EXP, self._onExpChangeCB)
end

function RoleAttriTabView:OnExpChange()
    local diff = GameWorld.Player().level - self._lastLevel
    local curExp = GameWorld.Player().money_exp
    local maxExp = RoleDataHelper.GetCurLevelExp(GameWorld.Player().level)
    if PlayerManager.RoleManager:CanUpgradeLevel() then
        self._expComp:ShowTweenAnimate(true)
        self._expComp:SetMutipleTween(true)
        self._expComp:SetProgress(diff + curExp/maxExp)
    else
        self._expComp:ShowTweenAnimate(false)
        self._expComp:SetMutipleTween(false)
        self._expComp:SetProgress(1)
    end
    self._expComp:SetProgressText(StringStyleUtil.GetLongNumberString(curExp).."/"..StringStyleUtil.GetLongNumberString(tonumber(maxExp)))
    self._lastLevel = GameWorld.Player().level
end

function RoleAttriTabView:RefreshView()
    for i=1, Max_Attri_Category do
        local idToData = combatData:GetAttrDataById(i)
        self._attriView[i]:SetData(idToData)
        self._attriView[i]:SetTitle(i)
        self._idToCount[i] = #idToData
    end
    self:RefreshScrollView()
    self:OnCharmChange()
    self:OnLevelChange()
    self:OnFightForceChange()
    self:OnPKValueChange()
end

function RoleAttriTabView:OnIntimacyButtonClick()

end

function RoleAttriTabView:OnWorldLevelButtonClick()
    self._worldLevelGo:SetActive(true)
    self._worldLevelView:ShowView()
end

function RoleAttriTabView:OnClearButtonClick()
    self._modelComponent:HideFx()
    self._containerPK:SetActive(true)
    for i=1,#self._pkItemIds do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._pkItemIds[i])
        self._pkItems[i]:UpdateCount(bagNum)
        if bagNum > 0 then
            self._pkItems[i]:SetActive(true)
        else
            self._pkItems[i]:ShowBuy()
        end
    end
end

function RoleAttriTabView:OnClosePK()
    self._modelComponent:ShowFx()
    self._containerPK:SetActive(false)
end

function RoleAttriTabView:OnPKItemChange()
    for i=1,#self._pkItemIds do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._pkItemIds[i])
        self._pkItems[i]:UpdateCount(bagNum)
        if bagNum > 0 then
            self._pkItems[i]:SetActive(true)
        else
            self._pkItems[i]:ShowBuy()
        end
    end
end

function RoleAttriTabView:OnCharmChange()
    self._intimacyText.text = GameWorld.Player().charm
end

function RoleAttriTabView:OnLevelChange()
    self._levelAttrText.text = GameWorld.Player().level
end

function RoleAttriTabView:OnFightForceChange()
    self._fpNumber:SetNumber(GameWorld.Player().fight_force)
end

function RoleAttriTabView:OnPKValueChange()
    self._pkValueText.text = GameWorld.Player().pk_value
end

function RoleAttriTabView:SetGuildName()
    self._guildText.text = GameWorld.Player().guild_name
end

function RoleAttriTabView:OnGetWorldLevel()
    self._worldLevelView:RefreshView()
end

function RoleAttriTabView:OnAttrRefresh()
    self:RefreshView()
end

function RoleAttriTabView:OnShowModel()
    self._modelComponent:Show()
end

function RoleAttriTabView:OnHideModel()
    self._modelComponent:Hide()
end