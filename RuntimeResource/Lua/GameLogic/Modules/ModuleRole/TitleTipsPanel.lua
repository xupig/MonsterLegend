-- TitleTipsPanel.lua
require "Modules.ModuleRole.ChildComponent.TitleTipsItem"

local TitleTipsPanel = Class.TitleTipsPanel(ClassTypes.BasePanel)

local TitleTipsItem = ClassTypes.TitleTipsItem
local TitleDataHelper = GameDataHelper.TitleDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local closeTime = 3

function TitleTipsPanel:Awake()
	self:InitView()
end

function TitleTipsPanel:OnDestroy()
	self._csBH = nil
end

function TitleTipsPanel:OnShow(id)
    self:AddEventListeners()
    self:SetData(id)
end

function TitleTipsPanel:SetData(id)
    if id == nil then
        GUIManager.ClosePanel(PanelsConfig.TitleTips)
        return
    end
    self:ShowItem(id)
end

function TitleTipsPanel:OnClose()
    for i,v in ipairs(self._itemContainers) do
        v:ClearTimer()
    end

    for k,v in pairs(self._useViews) do
        v:ClearTimer()
        table.insert(self._itemContainers, v)
    end

    self._useNum = 0
    self._useViews = {}
    self._moveAchievementId = nil
    self.__showTitleId = nil
	self:RemoveEventListeners()
end

function TitleTipsPanel:AddEventListeners()  
    EventDispatcher:AddEventListener(GameEvents.ON_SHOWTITLE_ITEM_CLICK, self._onItemClick)
    EventDispatcher:AddEventListener(GameEvents.ON_CLOSE_TITLE_ITEM, self._closeItem)
end

function TitleTipsPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_SHOWTITLE_ITEM_CLICK, self._onItemClick)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLOSE_TITLE_ITEM, self._closeItem)
end

function TitleTipsPanel:InitView()
    self._onItemClick = function(id) self:OnItemClick(id) end
    self._closeItem = function(id) self:CloseItem(id) end

    self._useViews = {}
    self._itemContainers = {}
    self._itemNum = 0
    self._useNum = 0
end

function TitleTipsPanel:SetValue(id)
    self._titleText.text = AchievementDataHelper.GetTitle(id)
end

function TitleTipsPanel:OnItemClick(id)
    GUIManager.ClosePanel(PanelsConfig.TitleTips)
    GUIManager.ShowPanelByFunctionId(5)
end

function TitleTipsPanel:CloseItem(id)
    if self._moveAchievementId == id then
        self._moveAchievementId = nil
    end
    
    self._useNum = self._useNum - 1
    if self._useNum == 0 then
        GUIManager.ClosePanel(PanelsConfig.TitleTips)
    end
    table.insert(self._itemContainers, self._useViews[id])
    self._useViews[id] = nil
end

function TitleTipsPanel:CopyContainer()
    self._itemNum = self._itemNum + 1
    return self:CopyUIGameObject("Container_Tips/Container_Demo", "Container_Tips/Container_Appear")
end

function TitleTipsPanel:ShowItem(id)
    local view
    if table.isEmpty(self._itemContainers) then
        --需要创建新的container
        local item = self:CopyContainer()
        view = UIComponentUtil.AddLuaUIComponent(item, TitleTipsItem)
    else
        view = self._itemContainers[#self._itemContainers]
        self._itemContainers[#self._itemContainers] = nil
    end

    if self.__showTitleId ~= nil then        
        self._useViews[self.__showTitleId]:Move()
        if self._moveAchievementId ~= nil then
            self._useViews[self._moveAchievementId]:Hide()
        end
        self._moveAchievementId = self.__showTitleId  
    end

    view:SetAchievementId(id)
    view:ShowView()
    self.__showTitleId = id
    self._useNum = self._useNum + 1
    self._useViews[id] = view
end