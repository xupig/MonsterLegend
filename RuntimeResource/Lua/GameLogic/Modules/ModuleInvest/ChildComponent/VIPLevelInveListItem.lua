-- VIPLevelInveListItem.lua
require "UIComponent.Extend.ItemGrid"
local UIListItem = ClassTypes.UIListItem
local VIPLevelInveListItem = Class.VIPLevelInveListItem(UIListItem)
VIPLevelInveListItem.interface = GameConfig.ComponentsConfig.Component
VIPLevelInveListItem.classPath = "Modules.ModuleInvest.ChildComponent.VIPLevelInveListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local InvestManager = PlayerManager.InvestManager
local WelfareData = PlayerManager.PlayerDataManager.investData
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TitleDataHelper = GameDataHelper.TitleDataHelper
local XImageFlowLight = GameMain.XImageFlowLight

function VIPLevelInveListItem:Awake()
    self._base.Awake(self)
    self._weekText = self:GetChildComponent("Text_Week",'TextMeshWrapper')    
    self._dayText = self:GetChildComponent("Text_Day",'TextMeshWrapper')   
    self.getText = self:GetChildComponent("Button_Get/Text",'TextMeshWrapper')    
    self.getText.text = LanguageDataHelper.GetContent(123)

    self.containerItemgos = {}
    self.containerItemgos[1] = self:FindChildGO('Container_Item1')
    self.containerItems = {}
    local a = GUIManager.AddItem(self.containerItemgos[1], 1)
    self.containerItems[1] = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self.containerItems[1]:ActivateTipsById()
    self.btnget = self:FindChildGO('Button_Get')
    self.imageget = self:FindChildGO('Image_Get')
    self.imageicon = self:FindChildGO('Image_Icon')
    self.imagetitle = self:FindChildGO('Image_Title')
    self._btngetWrap = self:GetChildComponent("Button_Get",'ButtonWrapper') 
    self._csBH:AddClick(self.btnget,function() self:OnBtnGet()end)

    self._goButtonFlowLight = self:AddChildComponent("Button_Get", XImageFlowLight)
end

function VIPLevelInveListItem:OnDestroy()
    self._csBH = nil
    self.containers = nil
end
function VIPLevelInveListItem:OnBtnGet()
    InvestManager:RequestLevelIneGetReward(tostring(self._level))
end

--override
function VIPLevelInveListItem:OnRefreshData(data)
    self.containerItemgos[1]:SetActive(false)
    local le = data:GetLevel()
    self._level = le
    local rewards = data:GetRewards()
    local rewardid = 0
    local rewardnum = 0
    rewardid,rewardnum= next(rewards)
    if le==1 then
        if data:GetType()==1880 then
            self.imagetitle:SetActive(true)
            local i = GlobalParamsHelper.GetParamValue(862)
            GameWorld.AddIconAnim(self.imagetitle, TitleDataHelper.GetTitleShowName(i))
            GameWorld.SetIconAnimSpeed(self.imagetitle, 1)
            self._weekText.text = LanguageDataHelper.CreateContent(76996)
        else
            self.imagetitle:SetActive(false)
            self._weekText.text = LanguageDataHelper.CreateContent(80800)
        end  
        self._dayText.text = rewardnum..''
    else
        self.imagetitle:SetActive(false)
        local x = InvestLevelDataHelper:GetType(data._id)
        local arg=string.format("%.2f",rewardnum/x*100)..'%'
        self._weekText.text = LanguageDataHelper.CreateContent(76811,{['0']=le})..' '..LanguageDataHelper.CreateContent(76812,{['0']=arg})
        self._dayText.text = rewardnum..''
    end
    GameWorld.AddIcon(self.imageicon,ItemDataHelper.GetIcon(rewardid))
    self.containerItemgos[1]:SetActive(true)
    self.containerItems[1]:SetItem(rewardid)
    if data._flage then
        self.imageget:SetActive(true)
        self.btnget:SetActive(false)
    else
        self.btnget:SetActive(true)
        self.imageget:SetActive(false)
        if not data:IsEnoughLevel() then
            self._btngetWrap.interactable = false    
            self._goButtonFlowLight.enabled = false    
            self.getText.text = LanguageDataHelper.GetContent(76806)
        else
            local t = WelfareData:GetLevelInvesType()
            if t ==0 or data._type~=t then
                self._btngetWrap.interactable = false    
                self._goButtonFlowLight.enabled = false    
                self.getText.text = LanguageDataHelper.GetContent(123)
            else
                self._btngetWrap.interactable = true 
                self._goButtonFlowLight.enabled = true    
                self.getText.text = LanguageDataHelper.GetContent(123)
            end
        end
    end
end