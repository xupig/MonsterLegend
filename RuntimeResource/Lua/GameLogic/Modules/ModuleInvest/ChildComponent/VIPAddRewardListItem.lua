-- VIPAddRewardListItem.lua
require "UIComponent.Extend.ItemGrid"
local UIListItem = ClassTypes.UIListItem
local VIPAddRewardListItem = Class.VIPAddRewardListItem(UIListItem)
VIPAddRewardListItem.interface = GameConfig.ComponentsConfig.Component
VIPAddRewardListItem.classPath = "Modules.ModuleInvest.ChildComponent.VIPAddRewardListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local InvestManager = PlayerManager.InvestManager
local InvestData = PlayerManager.PlayerDataManager.investData
local public_config = GameWorld.public_config


function VIPAddRewardListItem:Awake()
    self._base.Awake(self)
    local a = self:FindChildGO('Container_Item')
    local i = GUIManager.AddItem(a,1)
    self._item = UIComponentUtil.AddLuaUIComponent(i,ItemGrid)
    self._item:ActivateTipsById()
end

function VIPAddRewardListItem:OnDestroy()
    self._csBH = nil
    -- self.containers = nil
end
function VIPAddRewardListItem:OnBtnGet()
    
end

--override
function VIPAddRewardListItem:OnRefreshData(data)
    self._item:SetItem(data[1],data[2])
end