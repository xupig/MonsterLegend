-- VIPMonthInveListItem.lua
require "UIComponent.Extend.ItemGrid"
local UIListItem = ClassTypes.UIListItem
local VIPMonthInveListItem = Class.VIPMonthInveListItem(UIListItem)
VIPMonthInveListItem.interface = GameConfig.ComponentsConfig.Component
VIPMonthInveListItem.classPath = "Modules.ModuleInvest.ChildComponent.VIPMonthInveListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local InvestManager = PlayerManager.InvestManager
local InvestData = PlayerManager.PlayerDataManager.investData
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local XImageFlowLight = GameMain.XImageFlowLight

function VIPMonthInveListItem:Awake()
    self._base.Awake(self)
    self._dayText = self:GetChildComponent("Container_Des/Text_Day",'TextMeshWrapper')    
    self._numText = self:GetChildComponent("Container_Des/Text_Num",'TextMeshWrapper')    
    self.getText = self:GetChildComponent("Button_Get/Text",'TextMeshWrapper')    
    self.getText.text = LanguageDataHelper.GetContent(123)

    self.tipsText = self:GetChildComponent("Text_Tips",'TextMeshWrapper')  

    self.containerItemgos = {}
    self.containerItemgos[1] = self:FindChildGO('Container_Item1')
    self.containerItemgos[2] = self:FindChildGO('Container_Item2')
    self.containerItemgos[3] = self:FindChildGO('Container_Item3')

    self.containerItems = {}
    local a = GUIManager.AddItem(self.containerItemgos[1], 1)
    self.containerItems[1] = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self.containerItems[1]:ActivateTipsById()
    local a = GUIManager.AddItem(self.containerItemgos[2], 1)
    self.containerItems[2] = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self.containerItems[2]:ActivateTipsById()
    local a = GUIManager.AddItem(self.containerItemgos[3], 1)
    self.containerItems[3] = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self.containerItems[3]:ActivateTipsById()

    self.btnget = self:FindChildGO('Button_Get')
    self.imageget = self:FindChildGO('Image_Get')
    self._btngetWrap = self:GetChildComponent("Button_Get",'ButtonWrapper') 
    self._csBH:AddClick(self.btnget,function() self:OnBtnGet()end)

    self._imgIcon = self:FindChildGO('Container_Des/Image_Icon')
    self._containerDes = self:FindChildGO("Container_Des")

    self._goButtonFlowLight = self:AddChildComponent("Button_Get", XImageFlowLight)
end

function VIPMonthInveListItem:OnDestroy()
    self._csBH = nil
    self.containers = nil
end
function VIPMonthInveListItem:OnBtnGet()
    InvestManager:RequestMonthIneGetReward()
end

--override
function VIPMonthInveListItem:OnRefreshData(data)
    self.containerItemgos[1]:SetActive(false)
    self.containerItemgos[2]:SetActive(false)
    self.containerItemgos[3]:SetActive(false)
    self._day = data:GetDay()
    local rewards = data:GetRewards()
    if self._day==1 then
        local r1 = rewards[1]
        local r2 = rewards[2]
        local id 
        local num1
        local num2
        id,num1 = next(r1)
        self.containerItemgos[1]:SetActive(true)
        self.containerItems[1]:SetItem(id)
        id,num2 = next(r2)
        self.containerItemgos[2]:SetActive(true)  
        self.containerItems[2]:SetItem(id)
        self._containerDes.transform.anchoredPosition = Vector3.New(202,0,0)
        self._numText.text = num1..'+'..num2
        self._dayText.text = LanguageDataHelper.GetContent(76997)
    else
        local arg = LanguageDataHelper.GetArgsTable()
        arg['0']=self._day
        self._dayText.text = LanguageDataHelper.CreateContent(52090,arg)
        local id 
        local num
        id,num = next(rewards)
        self.containerItemgos[1]:SetActive(true)
        self.containerItems[1]:SetItem(id)
        self._numText.text = num..''
        self._containerDes.transform.anchoredPosition = Vector3.New(118,0,0)
        
    end
    GameWorld.AddIcon(self._imgIcon,ItemDataHelper.GetIcon(4))
    if data._isget then
        self.btnget:SetActive(false)
        self.imageget:SetActive(true)
        if self._day==1 then
            self.tipsText.text = LanguageDataHelper.GetContent(76998)
        else
            self.tipsText.text=data:GetDes()
        end
    else
        self.tipsText.text=''
        self.btnget:SetActive(true)
        self.imageget:SetActive(false)
        if data:IsCanGet() then
            self._goButtonFlowLight.enabled=true
            self._btngetWrap.interactable = true        
        else
            self._goButtonFlowLight.enabled=false
            self._btngetWrap.interactable = false        
        end
    end

end