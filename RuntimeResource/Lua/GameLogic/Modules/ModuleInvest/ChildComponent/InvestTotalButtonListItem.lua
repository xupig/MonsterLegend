-- InvestTotalButtonListItem.lua
local InvestTotalButtonListItem = Class.InvestTotalButtonListItem(ClassTypes.UIListItem)
InvestTotalButtonListItem.interface = GameConfig.ComponentsConfig.Component
InvestTotalButtonListItem.classPath = "Modules.ModuleInvest.ChildComponent.InvestTotalButtonListItem"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InvestManager = PlayerManager.InvestManager
local public_config = GameWorld.public_config
local SevenDayLoginManager = PlayerManager.SevenDayLoginManager

function InvestTotalButtonListItem:__ctor__()
    self._state=nil
end

function InvestTotalButtonListItem:Awake()
    self:InitItem()
    self._refreshRed = function()self:UpdateRed() end
    EventDispatcher:AddEventListener(GameEvents.InvestRedUpdate, self._refreshRed)
    if self._state==nil then
        return
    end
    self:SetState(self._state)

end

function InvestTotalButtonListItem:OnDestroy()
    EventDispatcher:RemoveEventListener(GameEvents.InvestRedUpdate, self._refreshRed)
end

function InvestTotalButtonListItem:InitItem()

  self._imgRed = self:FindChildGO("Image_Red")
  self._unselected = self:FindChildGO("UnSelected")
  self._selected = self:FindChildGO("Selected")
  self._text1 = self:GetChildComponent("UnSelected/Text","TextMeshWrapper")
  self._text2 = self:GetChildComponent("Selected/Text","TextMeshWrapper")
  self._imgRed:SetActive(false)
end

function InvestTotalButtonListItem:OnRefreshData(data)
    if data then
        self._text1.text = data._title
        self._text2.text = data._title
        self._funId =  data._funId
        self:UpdateRed()
    end
end

function InvestTotalButtonListItem:UpdateRed()      
    if  self._funId == public_config.FUNCTION_ID_OPEN_CUMULATE then
        self._imgRed:SetActive(InvestManager:RefreshAddRed())
    elseif  self._funId == public_config.FUNCTION_ID_LEVEL_INVEST then
        self._imgRed:SetActive(InvestManager:RefreshLevelRed())
    elseif  self._funId == public_config.FUNCTION_ID_MOUTH_INVEST then
        self._imgRed:SetActive(InvestManager:RefreshMonthRed())
    elseif  self._funId == public_config.FUNCTION_ID_VIP_INVEST then
        self._imgRed:SetActive(InvestManager:RefreshVipRed())
    end	
end


	



function InvestTotalButtonListItem:SetState(state)
    -- self._state=state
    -- if state==0 then
    --     self._unselected:SetActive(true)
    --     self._selected:SetActive(false)
    -- else
    --     self._unselected:SetActive(false)
    --     self._selected:SetActive(true)
    -- end
end