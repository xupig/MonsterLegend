-- VIPInvertListItem.lua
require "UIComponent.Extend.ItemGrid"
local UIListItem = ClassTypes.UIListItem
local VIPInvertListItem = Class.VIPInvertListItem(UIListItem)
VIPInvertListItem.interface = GameConfig.ComponentsConfig.Component
VIPInvertListItem.classPath = "Modules.ModuleInvest.ChildComponent.VIPInvertListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local InvestManager = PlayerManager.InvestManager
local InvestData = PlayerManager.PlayerDataManager.investData
local XImageFlowLight = GameMain.XImageFlowLight

function VIPInvertListItem:Awake()
    self._base.Awake(self)
    self._weekText = self:GetChildComponent("Text_Week",'TextMeshWrapper')    
    self._dayText = self:GetChildComponent("Text_Day",'TextMeshWrapper')   
    local getText = self:GetChildComponent("Button_Get/Text",'TextMeshWrapper')    
    getText.text = LanguageDataHelper.GetContent(123)

    self.containerItemgos = {}
    self.containerItemgos[1] = self:FindChildGO('Container_Item1')
    self.containerItemgos[2] = self:FindChildGO('Container_Item2')
    self.containerItemgos[3] = self:FindChildGO('Container_Item3')

    self.containerItems = {}
    local a = GUIManager.AddItem(self.containerItemgos[1], 1)
    self.containerItems[1] = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self.containerItems[1]:ActivateTipsById()
    local a = GUIManager.AddItem(self.containerItemgos[2], 1)
    self.containerItems[2] = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self.containerItems[2]:ActivateTipsById()
    local a = GUIManager.AddItem(self.containerItemgos[3], 1)
    self.containerItems[3] = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self.containerItems[3]:ActivateTipsById()

    self.btnget = self:FindChildGO('Button_Get')
    self.imageget = self:FindChildGO('Image_Get')
    self._btngetWrap = self:GetChildComponent("Button_Get",'ButtonWrapper') 
    self._csBH:AddClick(self.btnget,function() self:OnBtnGet()end)

    self._goButtonFlowLight = self:AddChildComponent("Button_Get", XImageFlowLight)
end

function VIPInvertListItem:OnDestroy()
    self._csBH = nil
    self._weekText = nil
    self._dayText = nil
    self.containers = nil
end
function VIPInvertListItem:OnBtnGet()
    InvestManager:RequestInestGetReward(tostring(self._day))
end

--override
function VIPInvertListItem:OnRefreshData(data)
    self.containerItemgos[1]:SetActive(false)
    self.containerItemgos[2]:SetActive(false)
    self.containerItemgos[3]:SetActive(false)

    self._day = data:GetVipday()
    local arg = LanguageDataHelper.GetArgsTable()
    arg['0']=data:GetVipWeek()
    self._weekText.text = LanguageDataHelper.CreateContent(76800,arg)
    local arg = LanguageDataHelper.GetArgsTable()
    arg['0']=data:GetVipday()
    self._dayText.text = LanguageDataHelper.CreateContent(76801,arg)
    
    local rewards = data:GetRewards()
    local x = 1
    for k, v in pairs(rewards) do
        self.containerItemgos[x]:SetActive(true)
        self.containerItems[x]:SetItem(tonumber(k),v)
        x = x+1
    end
    if data._flage or not data._canget then
        self._goButtonFlowLight.enabled=false
        self._btngetWrap.interactable = false
    else
        self._goButtonFlowLight.enabled=true
        self._btngetWrap.interactable = true        
    end
    if data._flage then
        self.imageget:SetActive(true)
        self.btnget:SetActive(false)
    else
        self.btnget:SetActive(true)
        self.imageget:SetActive(false)
    end
end