-- VIPAddInveListItem.lua
require"Modules.ModuleInvest.ChildComponent.VIPAddRewardListItem"
local UIListItem = ClassTypes.UIListItem
local VIPAddInveListItem = Class.VIPAddInveListItem(UIListItem)
VIPAddInveListItem.interface = GameConfig.ComponentsConfig.Component
VIPAddInveListItem.classPath = "Modules.ModuleInvest.ChildComponent.VIPAddInveListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local InvestManager = PlayerManager.InvestManager
local InvestData = PlayerManager.PlayerDataManager.investData
local public_config = GameWorld.public_config
local VIPAddRewardListItem = ClassTypes.VIPAddRewardListItem
local UIList = ClassTypes.UIList
local BaseUtil = GameUtil.BaseUtil
local PanelsConfig = GameConfig.PanelsConfig
local XImageFlowLight = GameMain.XImageFlowLight

function VIPAddInveListItem:Awake()
    self._base.Awake(self)
    self._monText = self:GetChildComponent("Text_Mon",'TextMeshWrapper')   
    self.getText = self:GetChildComponent("Button_Get/Text",'TextMeshWrapper')    
    self.getText.text = LanguageDataHelper.GetContent(123)

    self.chargeText = self:GetChildComponent("Button_Charge/Text",'TextMeshWrapper')    
    self.chargeText.text = LanguageDataHelper.GetContent(268)

    self.btnget = self:FindChildGO('Button_Get')
    self.btncharge = self:FindChildGO('Button_Charge')
    self.imageget = self:FindChildGO('Image_Get')
    self._containerRay = self:FindChildGO("Container_Raycast")
    self._btngetWrap = self:GetChildComponent("Button_Get",'ButtonWrapper') 
    self._btnchargeWrap = self:GetChildComponent("Button_Charge",'ButtonWrapper') 
    self._csBH:AddClick(self.btnget,function() self:OnBtnGet()end)
    self._csBH:AddClick(self.btncharge,function() self:OnBtnCharge()end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(VIPAddRewardListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:UseObjectPool()
    self._list:SetDirection(UIList.DirectionLeftToRight, 10, -1)
    local itemClickedCB = 
	function(index)
		self:OnListItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)
    self._goButton1FlowLight = self:AddChildComponent("Button_Get", XImageFlowLight)
    self._goButton2FlowLight = self:AddChildComponent("Button_Charge", XImageFlowLight)
end

function VIPAddInveListItem:OnDestroy()
    self._csBH = nil
end

function VIPAddInveListItem:OnBtnGet()
    InvestManager:RequestAddIneGetReward(self.data._id)
end

function VIPAddInveListItem:OpenMsg(state)
    self._btnchargeWrap.interactable=state
    self._btngetWrap.interactable=state
end

function VIPAddInveListItem:OnBtnCharge()
    local data={}
    data.firstTabIndex=2
    GUIManager.ShowPanel(PanelsConfig.VIP,data)
end
--override
function VIPAddInveListItem:OnRefreshData(data)
    self.data = data
    local amount = GameWorld.Player().cumulative_charge_amount or 0
    local cost = data:GetCost()
    local t=LanguageDataHelper.GetContent(17)
    if data._isget then
        self._goButton1FlowLight.enabled = false
        self._goButton2FlowLight.enabled = false
        self.btnget:SetActive(false)
        self.imageget:SetActive(true)
        self.btncharge:SetActive(false)
        if amount>=cost then
            self._monText.text = BaseUtil.GetColorString(cost..'/'..cost..t,ColorConfig.J)..LanguageDataHelper.GetContent(76822)
        else
            self._monText.text = BaseUtil.GetColorString(amount..'/'..cost..t,ColorConfig.J)..LanguageDataHelper.GetContent(76822)
        end
    else
        if data._canget then
            self._goButton1FlowLight.enabled = true
            self._goButton2FlowLight.enabled = false
            self.btnget:SetActive(true)
            self.btncharge:SetActive(false)
            self.imageget:SetActive(false)
            if amount>=cost then
                self._monText.text = BaseUtil.GetColorString(cost..'/'..cost..t,ColorConfig.J)..LanguageDataHelper.GetContent(76822)
            end
        else
            self._goButton1FlowLight.enabled = false
            self._goButton2FlowLight.enabled = true
            self.btnget:SetActive(false)
            self.imageget:SetActive(false)
            self.btncharge:SetActive(true)
            self._monText.text = BaseUtil.GetColorString(amount..'/'..cost..t,ColorConfig.J)..LanguageDataHelper.GetContent(76822)
        end
    end
    local rewards = {}
    local d = data:GetRewards()
    local x = 1
    for k,v in pairs(d) do
        rewards[x] = {tonumber(k),v}
        x = x + 1
    end
    self._containerRay:SetActive(false)
    self._list:SetDataList(rewards)
end