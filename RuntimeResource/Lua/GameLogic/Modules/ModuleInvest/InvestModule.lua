--InvestModule.lua
InvestModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function InvestModule.Init()
	GUIManager.AddPanel(PanelsConfig.Invest,"Panel_Invest",GUILayer.LayerUIPanel,"Modules.ModuleInvest.InvestPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return InvestModule
