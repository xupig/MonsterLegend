﻿--InvestPanel.lua
require "Modules.ModuleInvest.ChildComponent.InvestTotalButtonListItem"
require "Modules.ModuleInvest.ChildView.LevelInvestmentView"
require "Modules.ModuleInvest.ChildView.VIPAddInvestmentView"
require "Modules.ModuleInvest.ChildView.VIPMonthCardView"
require "Modules.ModuleInvest.ChildView.VIPInvestmentView"
local ClassTypes = ClassTypes
local InvestPanel = Class.InvestPanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local UIToggleGroup = ClassTypes.UIToggleGroup
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local InvestTotalButtonListItem = ClassTypes.InvestTotalButtonListItem

local LevelInvestmentView = ClassTypes.LevelInvestmentView
local VIPAddInvestmentView = ClassTypes.VIPAddInvestmentView
local VIPMonthCardView = ClassTypes.VIPMonthCardView
local VIPInvestmentView = ClassTypes.VIPInvestmentView
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local public_config = GameWorld.public_config
local InvestData = PlayerManager.PlayerDataManager.investData
local InvestManager = PlayerManager.InvestManager
local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

function InvestPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self:InitCom()

end

function InvestPanel:InitCom()
    -- FUNCTION_ID_MOUTH_INVEST        = 632,  --月卡投资
    -- FUNCTION_ID_VIP_INVEST          = 633,  --VIP投资
    -- FUNCTION_ID_LEVEL_INVEST        = 634,  --等级投资
    -- FUNCTION_ID_OPEN_CUMULATE       = 635,  --开服累充
    self._welfareType = {}
    self._welfareViewType = {}

    self._welfareType[public_config.FUNCTION_ID_MOUTH_INVEST] = self:FindChildGO("Container_MonthCard")
    self._welfareViewType[public_config.FUNCTION_ID_MOUTH_INVEST] = self:AddChildLuaUIComponent("Container_MonthCard", VIPMonthCardView)

    self._welfareType[public_config.FUNCTION_ID_VIP_INVEST] = self:FindChildGO("Container_Investment")
    self._welfareViewType[public_config.FUNCTION_ID_VIP_INVEST] = self:AddChildLuaUIComponent("Container_Investment", VIPInvestmentView)

    self._welfareType[public_config.FUNCTION_ID_LEVEL_INVEST] = self:FindChildGO("Container_Level")
    self._welfareViewType[public_config.FUNCTION_ID_LEVEL_INVEST] = self:AddChildLuaUIComponent("Container_Level", LevelInvestmentView)

    self._welfareType[public_config.FUNCTION_ID_OPEN_CUMULATE] = self:FindChildGO("Container_Add")
    self._welfareViewType[public_config.FUNCTION_ID_OPEN_CUMULATE] = self:AddChildLuaUIComponent("Container_Add", VIPAddInvestmentView)

    self:InitView()
end



function InvestPanel:Start()
    self:OnItemBTClicked(0)
end

function InvestPanel:UpdateFunOpen()
    self.titleList = {}
    self.welfareGoList = {}
    self.welfareViewList = {}

    self:AddTitleByOpen(public_config.FUNCTION_ID_OPEN_CUMULATE,self:IsOpenCumulate()) --开服累冲
    self:AddTitleByOpen(public_config.FUNCTION_ID_LEVEL_INVEST,self:IsOpenLevel()) --等级投资
    self:AddTitleByOpen(public_config.FUNCTION_ID_MOUTH_INVEST,self:IsOpenMonth()) --月卡投资
    self:AddTitleByOpen(public_config.FUNCTION_ID_VIP_INVEST,self:IsOpenVip()) --vip投资


end

function InvestPanel:AddTitleByOpen(funId,isOpen)
    self:AddViewByTitle(funId,isOpen)
end
  --开服累冲
function InvestPanel:IsOpenCumulate()
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_OPEN_CUMULATE) and not InvestData:IsVipAddClosed()
    return isOpen
end

--等级投资
function InvestPanel:IsOpenLevel()
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_LEVEL_INVEST)
    return isOpen
end

--月卡投资
function InvestPanel:IsOpenMonth()
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_MOUTH_INVEST)
    return isOpen
end

--vip投资
function InvestPanel:IsOpenVip()
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_VIP_INVEST)
    return isOpen
end

function InvestPanel:UpdateFunView(funId,isOpen)
        self._welfareType[funId].transform.localScale = HideScale---:SetActive(false)
        if isOpen then
            self:SetViewByFuncId(funId,self._welfareType[funId],self._welfareViewType[funId])
        end
end


function InvestPanel:AddTitleByFuncId(funId)
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(funId)
    self:AddViewByTitle(funId,isOpen)
end

function InvestPanel:AddViewByTitle(funId,isOpen)
    if isOpen then
        local nameId = FunctionOpenDataHelper:GetFunctionOpenName(funId)
        local funItem = {_title = LanguageDataHelper.CreateContent(nameId),_funId = funId}
        table.insert(self.titleList, funItem)
    end
    self:UpdateFunView(funId,isOpen)
end




function InvestPanel:SetViewByFuncId(funId,viewGo,viewPage)
    --viewGo:SetActive(false)
    local goItem = {_funID = funId,_ViewGo=viewGo}
    table.insert(self.welfareGoList, goItem)

    local viewItem = {_funID = funId,_View=viewPage}
    table.insert(self.welfareViewList, viewItem)
end

function InvestPanel:SetTitleData()
    self:UpdateFunOpen()
    for i,v in ipairs(self.welfareGoList) do
        self.welfareGoList[i]._ViewGo.transform.localScale = HideScale--:SetActive(false)
    end
    self._welfareList:SetDataList(self.titleList)
end


-- function InvestPanel:ScaleToHide()
--     return true
-- end

function InvestPanel:InitView()
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_Total/ScrollViewList_Buttons")
    self._welfareList = list
    self._welfareView = scrollview
    self._welfareList:SetItemType(InvestTotalButtonListItem)
    self._welfareList:SetPadding(5, 0, 0, 0)
    self._welfareList:SetGap(5, 0)
    self._welfareList:SetDirection(UIList.DirectionTopToDown, 1, 100)
    local itemBTClickedCB =
        function(index)
            self:OnItemBTClicked(index)
        end
    
    self._welfareList:SetItemClickedCB(itemBTClickedCB)
    

    
    self._btnClose = self:FindChildGO("Container_Total/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:OnCloseWelfareClick() end)

end

function InvestPanel:OnItemBTClicked(index)
    self._welfareList:SetSelectedIndex(index)
    self._curIndex = index
    local index = index+1
    local value = 1
    self:ShowView(index, value)
end






--override
function InvestPanel:OnDestroy()
    self._welfareType = {}
    self._welfareViewType = {}
end

--override
function InvestPanel:OnShow(data)
    InvestManager:EnterFirstRed(false)
    self._iconBg = self:FindChildGO("Container_Total/Image")
    GameWorld.AddIcon(self._iconBg,13512)
    self.welfareGoList = {}
    self.welfareViewList = {}
    self._curShowView = 1
    self:SetTitleData()
    self:OnItemBTClicked(0)
    if data then
        local index = data.id or 0
        local value = data.value
        self:ShowView(index, value)
    end
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleInvest.InvestPanel")
end

function InvestPanel:SetData(data)
    local index = data.id or 0
    local value = data.value
    self:ShowView(index, value)

end

function InvestPanel:ShowView(index, data)
    InvestManager:EnterFirstRed(false)
    if self._curShowView ~= -1 and index ~= self._curShowView then
        self:CloseView()
    end
    if not self.welfareGoList[index]._ViewGo.activeSelf then
        self.welfareGoList[index]._ViewGo:SetActive(true)
    end
    self.welfareGoList[index]._ViewGo.transform.localScale = ShowScale---:SetActive(true)
    if self.welfareViewList[index]._View ~= nil then
        self.welfareViewList[index]._View:ShowView(data)
        self._curShowView = index
    end
end

function InvestPanel:CloseView()
    if self._curShowView ~= -1 then
        self.welfareViewList[self._curShowView]._View:CloseView()
        self.welfareGoList[self._curShowView]._ViewGo.transform.localScale = HideScale---:SetActive(false)
        self._curShowView = -1
    end
    InvestManager:FirstEnterRed()
end


function InvestPanel:WinBGType()
    return PanelWinBGType.NoBG
end

function InvestPanel:OnCloseWelfareClick()
    self:CloseView()
    self._welfareList:RemoveAll()
    GUIManager.ClosePanel(PanelsConfig.Invest)
end

