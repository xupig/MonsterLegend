-- VIPInvestmentView.lua
require"Modules.ModuleInvest.ChildComponent.VIPInvertListItem"

local VIPInvestmentView = Class.VIPInvestmentView(ClassTypes.BaseComponent)
VIPInvestmentView.interface = GameConfig.ComponentsConfig.Component
VIPInvestmentView.classPath = "Modules.ModuleInvest.ChildView.VIPInvestmentView"
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InvestData = PlayerManager.PlayerDataManager.investData
local VIPInvertListItem = ClassTypes.VIPInvertListItem
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local BaseUtil= GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local InvestManager = PlayerManager.InvestManager
local XImageFlowLight = GameMain.XImageFlowLight
function VIPInvestmentView:Awake()
    self._update = function()self:UpdateView()end
    self:InitCallBack()
    self:InitComps()
    self._listView:SetScrollRectState(false)        
end

function VIPInvestmentView:InitCallBack()

end

function VIPInvestmentView:ShowView()
    self._listView:SetScrollRectState(true)
    self._price = GlobalParamsHelper.GetParamValue(757)[public_config.MONEY_TYPE_COUPONS]
    self.btnInveText.text = LanguageDataHelper.CreateContent(76995,{['0']=self._price})
    self:UpdateView()
end

function VIPInvestmentView:CloseView()
    self._listView:SetScrollRectState(false)        
end
function VIPInvestmentView:InitComps()
    local btnInve = self:FindChildGO("Button_Inverst")
    self._bgtips1 = self:FindChildGO("Image_BgTips1")
    self._bgtips2 = self:FindChildGO("Image_BgTips2")
    self.btnInveText = self:GetChildComponent("Button_Inverst/Text",'TextMeshWrapper')
    self.btnInveWrap = self:GetChildComponent("Button_Inverst",'ButtonWrapper')
    self._csBH:AddClick(btnInve,function()self:OnClickInve()end)

    -- self.residueText = self:GetChildComponent("Text_Residue",'TextMeshWrapper')

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(VIPInvertListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 0)
    self._list:UseObjectPool()
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)
    local itemClickedCB = 
	function(index)
		self:OnListItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)
    self._goButtonFlowLight = self:AddChildComponent("Button_Inverst", XImageFlowLight)
end

function VIPInvestmentView:UpdateView()
    local d = InvestData:UpdateInveList()
    self._list:SetDataList(d)
    local arg = LanguageDataHelper.GetArgsTable()
    local f = GameWorld.Player().invest_vip_flag or 0
    local a = InvestData:GetInvestDay()
    -- local v =28-a
    if f==0 then
        -- v=0
        if GameWorld.Player().vip_level<4 then
            self._goButtonFlowLight.enabled = true
            self.btnInveWrap.interactable = true
            self.btnInveText.text = LanguageDataHelper.GetContent(81665)
            self._bgtips1:SetActive(false)
            self._bgtips2:SetActive(true)
        else
            self._goButtonFlowLight.enabled = true
            self.btnInveWrap.interactable = true
            self.btnInveText.text = LanguageDataHelper.CreateContent(76995,{['0']=self._price})
            self._bgtips1:SetActive(true)
            self._bgtips2:SetActive(false)
        end

    else
        self._goButtonFlowLight.enabled = false
        self.btnInveWrap.interactable = false
        self.btnInveText.text = LanguageDataHelper.GetContent(76805)
        self._bgtips1:SetActive(true)
        self._bgtips2:SetActive(false) 
    end
    -- arg['0']=BaseUtil.GetColorString(v,ColorConfig.J)
    -- self.residueText.text = LanguageDataHelper.CreateContent(76802,arg)
    
end

function VIPInvestmentView:OnListItemClicked(idx)

end

function VIPInvestmentView:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.VIP_INVEST_UPDATE, self._update)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_INVEST_VIP_DAYS, self._update)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_INVEST_VIP_GET_REWARD_DAYS, self._update)	
end

function VIPInvestmentView:OnClickInve()
    if GameWorld.Player().vip_level>=4 then
        local d = {}
        local data = {}
        d.id = MessageBoxType.Tip
        d.value =  data
        data.confirmCB=function()InvestManager.RequestVipInest() GUIManager.ClosePanel(PanelsConfig.MessageBox) end
        data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.MessageBox) end
        local arg = LanguageDataHelper.GetArgsTable()
        local v = self._price
        arg['0']=v
        arg['1']=v
        data.text = LanguageDataHelper.CreateContent(76804,arg)
        GUIManager.ShowPanel(PanelsConfig.MessageBox, d)
    else
        InvestManager.RequestVipInest()
    end
	
end

function VIPInvestmentView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.VIP_INVEST_UPDATE, self._update)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_INVEST_VIP_DAYS, self._update)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_INVEST_VIP_GET_REWARD_DAYS, self._update)	
end