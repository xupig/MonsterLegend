-- VIPAddInvestmentView.lua
require"Modules.ModuleInvest.ChildComponent.VIPAddInveListItem"
local VIPAddInvestmentView = Class.VIPAddInvestmentView(ClassTypes.BaseComponent)
VIPAddInvestmentView.interface = GameConfig.ComponentsConfig.Component
VIPAddInvestmentView.classPath = "Modules.ModuleInvest.ChildView.VIPAddInvestmentView"
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local InvestData = PlayerManager.PlayerDataManager.investData
local VIPAddInveListItem = ClassTypes.VIPAddInveListItem
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local BaseUtil= GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local InvestManager = PlayerManager.InvestManager
local AddChargeDays = 7
function VIPAddInvestmentView:Awake()
    self._flage = false
    self._timerCountDownId=-1
    self._update = function()self:UpdateView()end
    self:InitCallBack()
    self:InitComps()
    self._listView:SetScrollRectState(false)    
end

function VIPAddInvestmentView:InitCallBack()
    self.timrCountTimer = function()self:CountDownTimer()end
    self.openmsg = function()self:OpenMsg()end
end
function VIPAddInvestmentView:ShowView()
    self._listView:SetScrollRectState(true)    
    local nowDate = DateTimeUtil.SomeDay(DateTimeUtil.GetServerTime())
    self.nowTime = nowDate.hour * DateTimeUtil.OneHourTime + nowDate.min * DateTimeUtil.OneMinTime + nowDate.sec * DateTimeUtil.OneSecTime
    self:UpdateView()
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_EVENT_CLOSED, self.openmsg)
end

function VIPAddInvestmentView:OpenMsg()
    local state = true
    if InvestData:IsVipAddClosed() then
       state=false
    end
    local len = self._list:GetLength()
    for i=0,len-1 do
        self._list:GetItem(i):OpenMsg(false)
    end
end

function VIPAddInvestmentView:CloseView()
    self._listView:SetScrollRectState(false)    
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_EVENT_CLOSED, self.openmsg)
    
end
function VIPAddInvestmentView:InitComps()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(VIPAddInveListItem)
    self._list:SetPadding(0, 0, 0, 20)
    self._list:SetGap(0, 0)
    self._list:UseObjectPool()
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)
    local itemClickedCB = 
	function(index)
		self:OnListItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)
    self._timeText = self:GetChildComponent("Text_Time",'TextMeshWrapper')
end

function VIPAddInvestmentView:CountDownTimer()
    local a=self:LeftTimeSec()
    if a<=0 then
        if self._timerCountDownId~=-1 then
            TimerHeap:DelTimer(self._timerCountDownId)
            self._timerCountDownId=-1
        end
        self._timeText.text=LanguageDataHelper.GetContent(76951)
        return 
    end
    self:SetLefeTime(a)    
end


function VIPAddInvestmentView:InitCountDownTimer()
    local a=self:LeftTimeSec()
    if a>0 then
        if self._timerCountDownId==-1 then
            self._timerCountDownId=TimerHeap:AddSecTimer(0,1,0,self.timrCountTimer)
        end
    else
        if self._timerCountDownId~=-1 then
            TimerHeap:DelTimer(self._timerCountDownId)
            self._timerCountDownId=-1
        end
    end
    self:SetLefeTime(a)
end

function VIPAddInvestmentView:SetLefeTime(lefttime)
    self._timeText.text=LanguageDataHelper.GetContent(80820)..DateTimeUtil:FormatFullTime(lefttime)    
end

function VIPAddInvestmentView:LeftTimeSec()
    local createtime = GameWorld.Player().create_time
    local s = DateTimeUtil.MinusSec(createtime)
    local totalsec = AddChargeDays*DateTimeUtil.OneDayTime-self.nowTime
    local a = totalsec-s
    if a<0 then
        return 0
    else
        return a
    end
end

function VIPAddInvestmentView:UpdateView()
    local d = InvestData:GetVipAdd()
    self._list:SetDataList(d)
    self:InitCountDownTimer()
end

function VIPAddInvestmentView:OnListItemClicked(idx)

end

function VIPAddInvestmentView:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.INVEST_ADD_UPDATE, self._update)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_AMOUNT, self._update)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_REFUND_INFO, self._update)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_CURRENT_DAY, self._update)	
end

function VIPAddInvestmentView:OnClickInve()

end

function VIPAddInvestmentView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.INVEST_ADD_UPDATE, self._update)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_AMOUNT, self._update)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_REFUND_INFO, self._update)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_CURRENT_DAY, self._update)	
   
end