-- LevelInvestmentView.lua
require"Modules.ModuleInvest.ChildComponent.VIPLevelInveListItem"
local LevelInvestmentView = Class.LevelInvestmentView(ClassTypes.BaseComponent)
LevelInvestmentView.interface = GameConfig.ComponentsConfig.Component
LevelInvestmentView.classPath = "Modules.ModuleInvest.ChildView.LevelInvestmentView"
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local InvestData = PlayerManager.PlayerDataManager.investData
local VIPLevelInveListItem = ClassTypes.VIPLevelInveListItem
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local BaseUtil= GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local InvestManager = PlayerManager.InvestManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local XImageFlowLight = GameMain.XImageFlowLight

function LevelInvestmentView:Awake()
    self._flage = false
    self._update = function()self:UpdateView()end
    self:InitCallBack()
    self:InitComps()
    self._listView:SetScrollRectState(false)        
end

function LevelInvestmentView:InitCallBack()
    self._toggleSele = function(index)
        self:ToggleSeleFun(index)
    end
end

function LevelInvestmentView:ToggleSeleFun(index)
    local d = InvestLevelDataHelper:GetAllType()
    local data = d[index+1]
    self:SetViewToggle(data)
end

function LevelInvestmentView:SetViewToggle(type)
    self._seleType = type
    local a = InvestData:GetVipLevelByType(type)
    self._list:SetDataList(a)
    self.btnInveText.text = LanguageDataHelper.CreateContent(76995,{['0']=self._seleType})
    local t = InvestData:GetLevelInvesType()
    if t==self._seleType or t>self._seleType then
        self._goButtonFlowLight.enabled = false   
        self.btnInveWrap.interactable = false
    else
        self._goButtonFlowLight.enabled = true   
        self.btnInveWrap.interactable = true
    end
end

function LevelInvestmentView:ShowView()
    self._listView:SetScrollRectState(true)    
    self:UpdateView()
end

function LevelInvestmentView:CloseView()
    self._listView:SetScrollRectState(false)        
end
function LevelInvestmentView:InitComps()
    local btnInve = self:FindChildGO("Button_Inverst")
    self.btnInveText = self:GetChildComponent("Button_Inverst/Text",'TextMeshWrapper')
    self.btnInveWrap = self:GetChildComponent("Button_Inverst",'ButtonWrapper')
    self._csBH:AddClick(btnInve,function()self:OnClickInve()end)   
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(VIPLevelInveListItem)
    self._list:SetPadding(18, 0, 0, 0)
    self._list:SetGap(10, 0)
    self._list:UseObjectPool()
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)
    local itemClickedCB = 
	function(index)
		self:OnListItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)
    self._tipstext = self:GetChildComponent("Text_Tips",'TextMeshWrapper')
    self._toggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup")
    self._toggleGroup:SetOnSelectedIndexChangedCB(self._toggleSele)
    self._toggles = {}
    local d = InvestLevelDataHelper:GetAllType()
    for i=1,3 do 
        local t = self:GetChildComponent("ToggleGroup/Toggle"..i..'/Label','TextMeshWrapper')
        self._toggles[i]=self:GetChildComponent("ToggleGroup/Toggle"..i,'ToggleWrapper')
        t.text = d[i]..LanguageDataHelper.GetContent(17)
    end
    self._goButtonFlowLight = self:AddChildComponent("Button_Inverst", XImageFlowLight)
end



function LevelInvestmentView:UpdateView()
    local d = InvestData:UpdateLevelList()
    self._list:SetDataList(d)
    local t = InvestData:GetLevelInvesType()
    local types = InvestLevelDataHelper:GetAllType()
    if t==0 then
        self._toggles[3].isOn=true
        self:SetViewToggle(types[3])
    else
        local index
        for k,v in pairs(types) do
            if t==v then
                index=tonumber(k)
                break
            end
        end
        self._toggles[index].isOn=true
        self:SetViewToggle(t)
    end
    local l=types[#types]
    self._tipstext.text = LanguageDataHelper.CreateContentWithArgs(76894,{['0']=l})
end

function LevelInvestmentView:OnListItemClicked(idx)

end

function LevelInvestmentView:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.INVEST_LEVEL_UPDATE, self._update)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LV_INVEST_TYPE, self._update)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LV_INVEST_RECORD, self._update)	
end

function LevelInvestmentView:OnClickInve()
	local d = {}
	local data = {}
	d.id = MessageBoxType.Tip
	d.value =  data
	data.confirmCB=function()InvestManager:RequestLevelInest(tostring(self._seleType)) GUIManager.ClosePanel(PanelsConfig.MessageBox) end
    data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.MessageBox) end
    local arg = LanguageDataHelper.GetArgsTable()
    local v = InvestData:GetLevelInvesType()
    if v==0 then
        arg['0']=self._seleType
        data.text = LanguageDataHelper.CreateContent(76814,arg)
    else
        local a=self._seleType-v
        if a<=0 then           
            arg['0']=self._seleType
        else
            arg['0'] = a
        end
        data.text = LanguageDataHelper.CreateContent(76815,arg)
    end
	GUIManager.ShowPanel(PanelsConfig.MessageBox, d)
end

function LevelInvestmentView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.INVEST_LEVEL_UPDATE, self._update)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LV_INVEST_TYPE, self._update)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LV_INVEST_RECORD, self._update)	
end