-- VIPMonthCardView.lua
require"Modules.ModuleInvest.ChildComponent.VIPMonthInveListItem"

local VIPMonthCardView = Class.VIPMonthCardView(ClassTypes.BaseComponent)
VIPMonthCardView.interface = GameConfig.ComponentsConfig.Component
VIPMonthCardView.classPath = "Modules.ModuleInvest.ChildView.VIPMonthCardView"
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local InvestMonthDataHelper = GameDataHelper.InvestMonthDataHelper
local InvestData = PlayerManager.PlayerDataManager.investData
local VIPMonthInveListItem = ClassTypes.VIPMonthInveListItem
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local BaseUtil= GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local InvestManager = PlayerManager.InvestManager
local XImageFlowLight = GameMain.XImageFlowLight
function VIPMonthCardView:Awake()
    self._flage = false
    self._update = function()self:UpdateView()end
    self:InitCallBack()
    self:InitComps()
    self._listView:SetScrollRectState(false)
    
end
function VIPMonthCardView:ShowView()
    self._listView:SetScrollRectState(true)
    self:UpdateView()
end

function VIPMonthCardView:CloseView()
    self._listView:SetScrollRectState(false)    
end
function VIPMonthCardView:InitCallBack()

end

function VIPMonthCardView:InitComps()
    local btnInve = self:FindChildGO("Button_Inverst")
    self.btnInveText = self:GetChildComponent("Button_Inverst/Text",'TextMeshWrapper')
    self.btnInveWrap = self:GetChildComponent("Button_Inverst",'ButtonWrapper')
    self._csBH:AddClick(btnInve,function()self:OnClickInve()end)
    self.residueText = self:GetChildComponent("Text_Residue",'TextMeshWrapper')

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(VIPMonthInveListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 0)
    self._list:UseObjectPool()
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)
    local itemClickedCB = 
	function(index)
		self:OnListItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)


    self.MonnumText = self:GetChildComponent("Container_Money/Text_Num",'TextMeshWrapper')

    self.RenumText = self:GetChildComponent("Container_Remain/Text_Num",'TextMeshWrapper')

    local montext = self:GetChildComponent("Container_Money/Text_Name",'TextMeshWrapper')
    montext.text = LanguageDataHelper.GetContent(76808)

    local remaintext = self:GetChildComponent("Container_Remain/Text_Name",'TextMeshWrapper')
    remaintext.text = LanguageDataHelper.GetContent(52091)

    local reIcon = self:FindChildGO("Container_Remain/Image_Icon")
    local itemid = public_config.MONEY_TYPE_COUPONS_BIND
    GameWorld.AddIcon(reIcon,ItemDataHelper.GetIcon(itemid))

    local r=InvestMonthDataHelper:GetReward(1)
    local monIcon = self:FindChildGO("Container_Money/Image_Icon")
    local itemid = public_config.MONEY_TYPE_COUPONS_BIND
    self.price = r[public_config.MONEY_TYPE_COUPONS_BIND]
    self.MonnumText.text = self.price..''
    GameWorld.AddIcon(monIcon,ItemDataHelper.GetIcon(itemid))
    self.btnInveText.text = LanguageDataHelper.CreateContent(76995,{['0']=self.price})
    self._goButtonFlowLight = self:AddChildComponent("Button_Inverst", XImageFlowLight)
end


function VIPMonthCardView:UpdateView()
    local d = InvestData:UpdateMonth()
    self._list:SetDataList(d)
    local arg = LanguageDataHelper.GetArgsTable()
    local a = 30-InvestData:GetMonthStep()
    arg['0'] = BaseUtil.GetColorString(a,ColorConfig.J)
    self.residueText.text = LanguageDataHelper.CreateContent(76802,arg)
    local f = not InvestData:GetIsMonthMon()
    self.btnInveWrap.interactable = f
    self._goButtonFlowLight.enabled = f   
    self.RenumText.text = InvestData:UpdateRemainNum()..''
end

function VIPMonthCardView:OnListItemClicked(idx)

end

function VIPMonthCardView:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.INVEST_MONTH_UPDATE, self._update)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONTH_CARD_DAILY_REFUND_STEP, self._update)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONTH_CARD_DAILY_REFUND_FLAG, self._update)	
end


function VIPMonthCardView:OnClickInve()
	local d = {}
	local data = {}
	d.id = MessageBoxType.Tip
	d.value =  data
	data.confirmCB=function()InvestManager:RequestMonthInest() GUIManager.ClosePanel(PanelsConfig.MessageBox) end
    data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.MessageBox) end
    local arg = LanguageDataHelper.GetArgsTable()
    local v = InvestData:GetLevelInvesType()
    arg['0']=self.price
    data.text = LanguageDataHelper.CreateContent(76816,arg)
	GUIManager.ShowPanel(PanelsConfig.MessageBox, d)
end

function VIPMonthCardView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.INVEST_MONTH_UPDATE,self._update)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONTH_CARD_DAILY_REFUND_STEP, self._update)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONTH_CARD_DAILY_REFUND_FLAG, self._update)	
end