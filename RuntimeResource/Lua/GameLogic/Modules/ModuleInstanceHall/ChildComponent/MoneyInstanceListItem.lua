-- MoneyInstanceListItem.lua
local MoneyInstanceListItem = Class.MoneyInstanceListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
MoneyInstanceListItem.interface = GameConfig.ComponentsConfig.Component
MoneyInstanceListItem.classPath = "Modules.ModuleInstanceHall.ChildComponent.MoneyInstanceListItem"

local MoneyInstanceDataHelper = GameDataHelper.MoneyInstanceDataHelper
local public_config = GameWorld.public_config

function MoneyInstanceListItem:Awake()
    self._bgContainer = self:FindChildGO("Container_BG")
    self._difficultText = self:GetChildComponent("Image_Bg/Text_Difficult",'TextMeshWrapper')
    self._lockContainer = self:FindChildGO("Container_Lock")
    self._selectContainer = self:FindChildGO("Image_Select")
    self._startRect = self:GetChildComponent("Image_Start" , "RectTransform")
    self._startImageWrapper = self:GetChildComponent("Image_Start", "ImageWrapper")
    self._perStartSize = self._startRect.sizeDelta
end

function MoneyInstanceListItem:OnDestroy() 

end

--override
function MoneyInstanceListItem:OnRefreshData(id)
    self._difficultText.text = MoneyInstanceDataHelper:GetName(id)
    local nowLevel = GameWorld.Player().level
    self._lockContainer:SetActive(nowLevel < MoneyInstanceDataHelper:GetLevel(id))

    local petData = GameWorld.Player().instance_gold_info[public_config.INSTANCE_GOLD_KEY_PASS_INFO]
    if petData == nil or table.isEmpty(petData) or petData[id] == nil then
        self._startRect.sizeDelta = Vector2(0, self._perStartSize.y)
    else
        self._startRect.sizeDelta = Vector2(self._perStartSize.x * petData[id], self._perStartSize.y)     
    end    
    GameWorld.AddIcon(self._bgContainer, MoneyInstanceDataHelper:GetIcon(id),0, true)
    self._startImageWrapper:SetAllDirty()
end

function MoneyInstanceListItem:SetSelect(flag)
    self._selectContainer:SetActive(flag)
end