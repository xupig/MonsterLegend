-- MoneyInstanceDropListItem.lua
local MoneyInstanceDropListItem = Class.MoneyInstanceDropListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
MoneyInstanceDropListItem.interface = GameConfig.ComponentsConfig.Component
MoneyInstanceDropListItem.classPath = "Modules.ModuleInstanceHall.ChildComponent.MoneyInstanceDropListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function MoneyInstanceDropListItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function MoneyInstanceDropListItem:OnDestroy() 
    self._itemContainer = nil
    self._itemIcon = nil
end

--override
function MoneyInstanceDropListItem:OnRefreshData(itemId)
    self._itemIcon:ActivateTipsById()
    self._itemIcon:SetItem(itemId)
end