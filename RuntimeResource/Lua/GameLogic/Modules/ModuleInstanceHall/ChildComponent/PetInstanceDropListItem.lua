-- PetInstanceDropListItem.lua
local PetInstanceDropListItem = Class.PetInstanceDropListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
PetInstanceDropListItem.interface = GameConfig.ComponentsConfig.Component
PetInstanceDropListItem.classPath = "Modules.ModuleInstanceHall.ChildComponent.PetInstanceDropListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function PetInstanceDropListItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function PetInstanceDropListItem:OnDestroy() 

end

--override
function PetInstanceDropListItem:OnRefreshData(data)
    local itemId = data
    
    self._itemIcon:ActivateTipsById()
    self._itemIcon:SetItem(itemId)
end