-- EquipmentInstanceDropListItem.lua
local EquipmentInstanceDropListItem = Class.EquipmentInstanceDropListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
EquipmentInstanceDropListItem.interface = GameConfig.ComponentsConfig.Component
EquipmentInstanceDropListItem.classPath = "Modules.ModuleInstanceHall.ChildComponent.EquipmentInstanceDropListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

local ItemDataHelper = GameDataHelper.ItemDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local public_config = GameWorld.public_config

function EquipmentInstanceDropListItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
    -- self._qulityImage = self:FindChildGO("Image_Qulity_Bg")
    -- self._qulityText = self:GetChildComponent("Image_Qulity_Bg/Text_Qulity",'TextMeshWrapper')
end

function EquipmentInstanceDropListItem:OnDestroy() 
    self._itemContainer = nil
    self._itemIcon = nil
end

--override
function EquipmentInstanceDropListItem:OnRefreshData(itemId)
    local itemType = ItemDataHelper.GetItemType(itemId)
    local grade = ItemDataHelper.GetEquipGrade(itemId)

    -- self._qulityImage:SetActive(itemType == public_config.ITEM_ITEMTYPE_EQUIP)
    -- if itemType == public_config.ITEM_ITEMTYPE_EQUIP and grade then
    --     self._qulityText.text = StringStyleUtil.GetChineseNumber(grade)..LanguageDataHelper.CreateContent(305)
    -- end
    
    self._itemIcon:ActivateTipsById()
    self._itemIcon:SetItem(itemId)
end