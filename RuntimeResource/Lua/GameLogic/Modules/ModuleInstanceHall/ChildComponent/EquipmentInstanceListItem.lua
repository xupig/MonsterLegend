-- EquipmentInstanceListItem.lua
local EquipmentInstanceListItem = Class.EquipmentInstanceListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
EquipmentInstanceListItem.interface = GameConfig.ComponentsConfig.Component
EquipmentInstanceListItem.classPath = "Modules.ModuleInstanceHall.ChildComponent.EquipmentInstanceListItem"

require "Modules.ModuleInstanceHall.ChildComponent.EquipmentInstanceDropListItem"
local EquipmentInstanceDropListItem = ClassTypes.EquipmentInstanceDropListItem

local BloodDungeonDataHelper = GameDataHelper.BloodDungeonDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local StringStyleUtil = GameUtil.StringStyleUtil
local OperatingActivityManager = PlayerManager.OperatingActivityManager

function EquipmentInstanceListItem:Awake()
    self._bgContainer = self:FindChildGO("Container_content/Container_Bg/Container_Bg_Icon")
    self._titleText = self:GetChildComponent("Container_content/Container_Bg/Image_Title/Text_Title",'TextMeshWrapper')
    self._fightText = self:GetChildComponent("Container_content/Container_Text/Image_Bg/Text_Fight",'TextMeshWrapper')
    self._limitText = self:GetChildComponent("Container_content/Container_Text/Image_Bg/Text_Limit",'TextMeshWrapper')
    self._selectImage = self:FindChildGO("Container_content/Container_Bg/Image_Select")
    self._lockContainer = self:FindChildGO("Container_content/Container_Lock")
    -- self._frameContainer = self:FindChildGO("Container_content/Container_Bg/Container_Frame")
    -- GameWorld.AddIcon(self._frameContainer, 5431)
    self._imgDouble = self:FindChildGO("Container_content/Image_Double")
    self:InitRewardList()
end

function EquipmentInstanceListItem:InitRewardList()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_content/Container_Rewards/ScrollViewList")
	self._dropList = list
	self._dropListView = scrollView 
    self._dropListView:SetHorizontalMove(false)
    self._dropListView:SetVerticalMove(false)
    self._dropList:SetItemType(EquipmentInstanceDropListItem)
    self._dropList:SetPadding(0, 0, 0, 0)
    self._dropList:SetGap(0, 0)
    self._dropList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function EquipmentInstanceListItem:OnDestroy() 
    self._bgContainer = nil
    self._titleText = nil
    self._fightText = nil
    self._limitText = nil
    self._selectImage = nil
    self._lockContainer = nil
end

--override
function EquipmentInstanceListItem:OnRefreshData(id)
    local nowLevel = GameWorld.Player().level
    local limitLevel = BloodDungeonDataHelper:GetLevel(id)
    local vocation = GameWorld.Player():GetVocationGroup()
    local rewards = BloodDungeonDataHelper:GetAward(id) and BloodDungeonDataHelper:GetAward(id)[vocation]
    local suggestFight = BloodDungeonDataHelper:GetCommend(id)
    local nowFight = GameWorld.Player().fight_force
    if rewards then
        self._dropList:SetDataList(rewards)
    end

    GameWorld.AddIcon(self._bgContainer, BloodDungeonDataHelper:GetIcon(id),0,true)
    self._titleText.text = LanguageDataHelper.CreateContent(BloodDungeonDataHelper:GetDesc(id))
    
    if nowLevel >= limitLevel then
        self._limitText.gameObject:SetActive(false)
        self._fightText.gameObject:SetActive(true)
        self._lockContainer:SetActive(false)
        -- if suggestFight > nowFight then
            -- self._fightText.text = LanguageDataHelper.CreateContent(75009) ..StringStyleUtil.GetColorString(suggestFight, StringStyleUtil.red)
        -- else
        self._fightText.text = LanguageDataHelper.CreateContent(75009) .. StringStyleUtil.GetColorString(StringStyleUtil.GetLevelString(limitLevel), StringStyleUtil.green)
        -- end
    else
        self._limitText.gameObject:SetActive(true)
        self._fightText.gameObject:SetActive(false)
        self._lockContainer:SetActive(true)
        self._limitText.text = LanguageDataHelper.CreateContent(75010, {["0"] = StringStyleUtil.GetLevelString(limitLevel)}) 
    end

    self:CheckDouble()
end

function EquipmentInstanceListItem:CheckDouble()
    if OperatingActivityManager:GetInstanceDoubleState(public_config.FUNCTION_ID_EQUIPMENT_INSTANCE) then
        self._imgDouble:SetActive(true)
    else
        self._imgDouble:SetActive(false)
    end
end

function EquipmentInstanceListItem:SetSelect(flag)
    self._selectImage:SetActive(flag)
end