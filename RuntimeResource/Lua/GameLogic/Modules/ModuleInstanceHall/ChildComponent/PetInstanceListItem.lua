-- PetInstanceListItem.lua
local PetInstanceListItem = Class.PetInstanceListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
PetInstanceListItem.interface = GameConfig.ComponentsConfig.Component
PetInstanceListItem.classPath = "Modules.ModuleInstanceHall.ChildComponent.PetInstanceListItem"

local PetInstanceDataHelper = GameDataHelper.PetInstanceDataHelper
local public_config = GameWorld.public_config

function PetInstanceListItem:Awake()
    self._bgContainer = self:FindChildGO("Container_BG")
    self._difficultText = self:GetChildComponent("Image_Bg/Text_Difficult",'TextMeshWrapper')
    self._lockContainer = self:FindChildGO("Container_Lock")
    self._selectContainer = self:FindChildGO("Image_Select")
    self._startRect = self:GetChildComponent("Image_Start" , "RectTransform")
    self._startImageWrapper = self:GetChildComponent("Image_Start", "ImageWrapper")
    self._perStartSize = self._startRect.sizeDelta
end

function PetInstanceListItem:OnDestroy() 

end

--override
function PetInstanceListItem:OnRefreshData(id)
    self._difficultText.text = PetInstanceDataHelper:GetName(id)
    local nowLevel = GameWorld.Player().level
    self._lockContainer:SetActive(nowLevel < PetInstanceDataHelper:GetLevel(id))

    local petData = GameWorld.Player().holy_tower_info[public_config.HOLY_TOWER_KEY_PASS_INFO]
    if petData == nil or table.isEmpty(petData) or petData[id] == nil then
        self._startRect.sizeDelta = Vector2(0, self._perStartSize.y)
    else
        self._startRect.sizeDelta = Vector2(self._perStartSize.x * petData[id], self._perStartSize.y)     
    end    
    GameWorld.AddIcon(self._bgContainer, PetInstanceDataHelper:GetIcon(id),0, true)
    self._startImageWrapper:SetAllDirty()
end

function PetInstanceListItem:SetSelect(flag)
    self._selectContainer:SetActive(flag)
end