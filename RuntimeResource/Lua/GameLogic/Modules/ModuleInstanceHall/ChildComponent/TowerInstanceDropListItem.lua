-- TowerInstanceDropListItem.lua
local TowerInstanceDropListItem = Class.TowerInstanceDropListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
TowerInstanceDropListItem.interface = GameConfig.ComponentsConfig.Component
TowerInstanceDropListItem.classPath = "Modules.ModuleInstanceHall.ChildComponent.TowerInstanceDropListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function TowerInstanceDropListItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function TowerInstanceDropListItem:OnDestroy() 

end

--override
function TowerInstanceDropListItem:OnRefreshData(data)
    local itemId = data
    
    self._itemIcon:ActivateTipsById()
    self._itemIcon:SetItem(itemId)
end