local InstanceCommonPanel = Class.InstanceCommonPanel(ClassTypes.BasePanel)

local DateTimeUtil = GameUtil.DateTimeUtil
local InstanceCommonConfig = GameConfig.InstanceCommonConfig
local PetInstanceDataHelper = GameDataHelper.PetInstanceDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local RectTransform = UnityEngine.RectTransform
local GUIManager = GameManager.GUIManager
local InstanceDataHelper = GameDataHelper.InstanceDataHelper

function InstanceCommonPanel:Awake()
    self.disableCameraCulling = true
    self._instancecommonUpdata = function(data) self:SetData(data) end
	self:InitComps()
end

function InstanceCommonPanel:InitComps()
    -- self._initPosition = self.gameObject.transform.localPosition--self._rect.transform.localPosition
    -- self.gameObject.transform.localPosition = Vector3.New(self._initPosition.x - (GUIManager.canvasWidth - GUIManager.PANEL_WIDTH)/2, self._initPosition.y, self._initPosition.z)    
    -- self._deviation = (GUIManager.canvasWidth - GUIManager.PANEL_WIDTH)/2

    --副本结束倒计时
    self._instanceTimeContainer = self:FindChildGO("Container_InstanceTime")
    self._timeText = self:GetChildComponent("Container_InstanceTime/Container/Text_Time",'TextMeshWrapper')    

    --玩法开始倒计时
    self._beginCountDownContainer = self:FindChildGO("Container_CountDown")
    self._beginTimeText = self:GetChildComponent("Container_CountDown/Container/Image_Bg/Text_Time",'TextMeshWrapper') 
    self._image = self:GetChildComponent("Container_CountDown/Container/Image_Bg", 'ImageWrapper')
    self._image:SetContinuousDimensionDirty(true)   

    --左侧副本信息：通关目标、副本描述
    self._isOpen = true
    self._instanceContainer = self:FindChildGO("Container_Instance_Info")
    self._instanceTargetText = self:GetChildComponent("Container_Instance_Info/Container_Info/Text_Title",'TextMeshWrapper') 
    self._instanceDescText = self:GetChildComponent("Container_Instance_Info/Container_Info/Text_Desc",'TextMeshWrapper') 
    self._instanceNameText = self:GetChildComponent("Container_Instance_Info/Button_Expand/Text_Name",'TextMeshWrapper') 
    self._btnExpand = self:FindChildGO("Container_Instance_Info/Button_Expand")
    self._containerInfo = self:FindChildGO("Container_Instance_Info/Container_Info")
    self._containerInitPosition = self._containerInfo.transform.localPosition
    self._containerClosePosition = self._containerInitPosition + Vector3.New(-280, 0, 0)
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
	self._imgLeft = self:FindChildGO("Container_Instance_Info/Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Container_Instance_Info/Button_Expand/Image_Right")

    --进入副本界面
    self._enterCopy = self:FindChildGO("Container_Enter_Copy")
    self._copyIcon = self:FindChildGO("Container_Enter_Copy/Container_Tips/Container_Icon")
    self._txtEnterCopyTitle = self:GetChildComponent("Container_Enter_Copy/Container_Tips/Image_BG1/Image_Title/Text_Title",'TextMeshWrapper') 
    self._txtEnterCopyDesc = self:GetChildComponent("Container_Enter_Copy/Container_Tips/Text_Desc",'TextMeshWrapper') 
    self._btnEnter = self:FindChildGO("Container_Enter_Copy/Container_Tips/Button_Enter")
    self._csBH:AddClick(self._btnEnter, function() self:OnEnterCopy() end )

    --星级评价
    self._startText = self:GetChildComponent("Container_Start_Container/Container_Content/Text_Info",'TextMeshWrapper')    
    self._startContainer = self:FindChildGO("Container_Start_Container")
    self._startContentContiner = self:FindChildGO("Container_Start_Container/Container_Content")
    self._startRect = self._startContentContiner:GetComponent("RectTransform")
    self._startPos = self._startContentContiner.gameObject:AddComponent(typeof(XGameObjectTweenPosition))

    self._startInitPosition = self:FindChildGO("Container_Start_Pos_Begin").transform.localPosition
    self._endPosition = self:FindChildGO("Container_Start_Pos_End").transform.localPosition
    self._middlePosition = (self._startInitPosition + self._endPosition ) / 2 - Vector3.New(150,0,0)
    
    self._startFlyTime = 1
    self._waitTime1 = 1
    self._waitTime2 = 1
    self._startViews = {}
    for i=0,4 do
        self._startViews[i] = self:FindChildGO("Container_Start_Container/Container_Content/Container_" .. i)
    end

    --自动退出副本倒计时
    self._instanceLeaveContainer = self:FindChildGO("Container_InstanceLeaveTime")
    self._timeLeaveText = self:GetChildComponent("Container_InstanceLeaveTime/Container/Text_Time",'TextMeshWrapper')
end

function InstanceCommonPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.INSTANCECOMMON_UPDATE, self._instancecommonUpdata)
end

function InstanceCommonPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.INSTANCECOMMON_UPDATE, self._instancecommonUpdata)
end

function InstanceCommonPanel:OnShow(data)
    -- LoggerHelper.Log("sam InstanceCommonPanel:OnShow(data)", true)
    self:OnResize()
    self:SetData(data)
    self:AddEventListeners()
end

function InstanceCommonPanel:OnClose()
    -- LoggerHelper.Log("sam InstanceCommonPanel:OnClose()", true)
    self:RemoveEventListeners()
    self._instanceTimeContainer:SetActive(false)
    self._beginCountDownContainer:SetActive(false)
    self._startContainer:SetActive(false)
    self._instanceContainer:SetActive(false)
    self._instanceLeaveContainer:SetActive(false)
    self._start = nil
    self:ClearTimers()
    self._showBeginCountDownCB = nil
    self._showLeaveCountDownCB = nil

    if not self._isOpen then
        self:OnOpenPanelAnimation()
        self._isOpen = true
    end
end

function InstanceCommonPanel:OnResize()

end

function InstanceCommonPanel:SetData(data)
    -- LoggerHelper.Log("sam InstanceCommonPanel:SetData()", true)
    if data == nil or table.isEmpty(data) then return end
    local showType = data.type
    if showType == nil then 
        return 
    elseif showType == InstanceCommonConfig.COMMON_INSTANCE_SHOW_START then
        --显示评价星级  
        self:ShowStart(data)
    elseif showType == InstanceCommonConfig.COMMON_INSTANCE_BEGIN_COUNTDOWN then
        --副本开始倒计时,如：5秒后开始副本
        self:ShowBeginCountDown(data)
    elseif showType == InstanceCommonConfig.COMMON_INSTANCE_STOP_COUNTDOWN then
        --停止副本开始倒计时
        self:StopBeginCountDown()
    elseif showType == InstanceCommonConfig.COMMON_INSTANCE_COUNT_DOWN then
        --更新时间（1秒一次）
        self:UpdateTime(data)
    elseif showType == InstanceCommonConfig.COMMON_INSTANCE_SHOW_COUNTDOWN then
        --显示副本时间
        self:ShowInstanceCountDown(data)
    elseif showType == InstanceCommonConfig.COMMON_INSTANCE_SHOW_INFO then
        --显示左侧副本信息：通关目标、副本描述
        self:ShowInstanceInfo(data)
    elseif showType == InstanceCommonConfig.COMMON_INSTANCE_LEAVE_COUNTDOWN then
        --显示自动离开副本的倒计时
        self:ShowInstanceLeaveCountDown(data)
    elseif showType == InstanceCommonConfig.COMMON_INSTANCE_ENTER_INFO then
        --进入副本
        self:ShowInstanceEnterInfo(data)
    end        
end

function InstanceCommonPanel:ShowInstanceEnterInfo(data)
    self._enterCopy:SetActive(true)
    self._txtEnterCopyTitle.text = LanguageDataHelper.CreateContent(InstanceDataHelper.GetInstanceName(data.copyId))
    self._txtEnterCopyDesc.text = LanguageDataHelper.CreateContent(InstanceDataHelper.GetInstanceDescribe(data.copyId))
    GameWorld.AddIcon(self._copyIcon, InstanceDataHelper.GetInstanceIcon(data.copyId))
    self._confirmCB = data.confirmCB
end

--更新时间
--data:
--endTime:副本剩余时间
--startTime:星级评价到达时间
function InstanceCommonPanel:UpdateTime(data)
    if self._start ~= nil and self._start ~= 0 and (not self._startMoving) then
        local showStartTime = self._startTimeData[self._start - 1][1]
        if DateTimeUtil.GetServerTime() - self._startTime >= showStartTime then 
            local d = {}
            d.type = InstanceCommonConfig.COMMON_INSTANCE_SHOW_START
            d.data = {}
            d.start = self._start - 1
            d.startTime = self._startTime
            d.startTimeData = self._startTimeData
            d.dropMessages = self._dropMessages
            d.countDownMessages = self._countDownMessages
            self:ShowStart(d)
        end
    end
    if self._start ~= nil and (not self._startMoving) then  
        local leaveSeconds = self._startTimeData[self._start][2] - (DateTimeUtil.GetServerTime() - self._startTime)
        self._startText.text = LanguageDataHelper.CreateContent(self._countDownMessages[self._start], {["0"] = DateTimeUtil:ParseTime(leaveSeconds)})
    end
    self._timeText.text = DateTimeUtil:ParseTime(self._startTime + data.instanceTime - DateTimeUtil.GetServerTime())
end

--显示星级评级变化动画
function InstanceCommonPanel:ShowStart(data)
    --LoggerHelper.Error("sam starttimedata==== " .. PrintTable:TableToStr(data), true)
    self._startMoving = true
    self._startTimeData = data.startTimeData
    self:SetMaxStart()
    self._startTime = data.startTime
    self._start = self:GetNowStart()--data.start
    self._dropMessages = data.dropMessages
    self._countDownMessages = data.countDownMessages
    local cb = data.cb
    if self._start == 3 then        
        self:SetBeginCountDownTime(0)
        self:ClearBeginCountDownTimer()
    end
    self._startText.text = self._dropMessages[self._start]
    self._startRect.transform.localPosition = self._startInitPosition
    self._startContainer:SetActive(true)
    for i=0,4 do
        self._startViews[i]:SetActive(i == self._start)
    end
    self._startPos:SetFromToPos(self._startInitPosition, self._middlePosition, self._startFlyTime, 1, nil)

    -- self._timer1 = TimerHeap:AddTimer(0, (self._startFlyTime  + self._waitTime1) * 1000, 0, function()
    --     TimerHeap:DelTimer(self._timer1) 
    --     self._timer1 = nil
    --     -- self._startPos:SetFromToPos(self._middlePosition, self._endPosition, self._startFlyTime, 1, nil)
    -- end)

    self._timer2 = TimerHeap:AddTimer((self._startFlyTime  + self._waitTime1) * 1000, 0, 0, function()
        TimerHeap:DelTimer(self._timer2) 
        self._timer2 = nil
        self._startPos:SetFromToPos(self._middlePosition, self._endPosition, self._startFlyTime, 1, nil)
        -- if cb ~= nil then
        --     cb()
        -- end
    end)

    self._timer3 = TimerHeap:AddTimer((self._startFlyTime  + self._waitTime1 + self._waitTime2) * 1000, 0, 0, function()
        TimerHeap:DelTimer(self._timer3) 
        self._timer3 = nil
        self._startMoving = false
        if cb ~= nil then
            cb(self._start)
        end
        -- self._instanceTimeContainer:SetActive(true)
    end)
end

function InstanceCommonPanel:GetNowStart()
    --LoggerHelper.Error("DateTimeUtil.GetServerTime()====" .. tostring(DateTimeUtil.GetServerTime()))
    --LoggerHelper.Error("self._startTime === " .. tostring(self._startTime))
    local pass = DateTimeUtil.GetServerTime() - self._startTime
    --LoggerHelper.Error("pass === " .. tostring(pass))
    if pass < 0 then return self._maxStart or 3 end
    for k,v in pairs(self._startTimeData) do
        local beginTime = v[1]
        local endTime = v[2]
        if pass >= beginTime and pass <= endTime then
            return k
        end
    end
    return 0
end

function InstanceCommonPanel:SetMaxStart()
    self._maxStart = 0
    for k,v in pairs(self._startTimeData) do
        if k > self._maxStart then
            self._maxStart = k
        end
    end
end

--停止副本玩法倒计时
function InstanceCommonPanel:StopBeginCountDown(data)
    self._beginCountDownContainer:SetActive(false)
    self:ClearBeginCountDownTimer()
end

--开始副本玩法倒计时
function  InstanceCommonPanel:ShowBeginCountDown(data)
    self._showBeginCountDownCB = data.cb
    self._startTextId = data.textId
    self._beginEndTime = data.startTime + data.seconds
    self._beginStartTime = data.startTime
    self._beginSeconds = data.seconds
    self._beginCountDownContainer:SetActive(true)
    self._beginCountDownTimer = TimerHeap:AddSecTimer(0, 1, 0, function() self:OnUpdataBeginCountDownTime() end)
end

function InstanceCommonPanel:OnUpdataBeginCountDownTime()    
    local time = self._beginEndTime - DateTimeUtil.GetServerTime()
    if time > self._beginSeconds then
        time = self._beginSeconds
    end

    if time <= 0 then
        self:SetBeginCountDownTime(0)
        self:ClearBeginCountDownTimer()
        if self._showBeginCountDownCB ~= nil then
            self._showBeginCountDownCB()
            self._showBeginCountDownCB = nil
        end
    else        
        self:SetBeginCountDownTime(time)
    end
end

function InstanceCommonPanel:ClearBeginCountDownTimer()
    if self._beginCountDownTimer ~= nil then
        TimerHeap:DelTimer(self._beginCountDownTimer)
        self._beginCountDownTimer = nil
        self._beginCountDownContainer:SetActive(false)
    end
end

function InstanceCommonPanel:ClearTimers()
    self:ClearBeginCountDownTimer()
    self:ClearLeaveCountDownTimer()
    if self._timer2 ~= nil then
        TimerHeap:DelTimer(self._timer2)
        self._timer2 = nil
    end

    if self._timer3 ~= nil then
        TimerHeap:DelTimer(self._timer3)
        self._timer3 = nil
    end
end

function InstanceCommonPanel:SetBeginCountDownTime(seconds)
    if self._beginTimeText.gameObject.activeSelf and self._startTextId then
        self._beginTimeText.text = seconds .. LanguageDataHelper.CreateContent(self._startTextId)
    end    
end

--显示副本倒计时
function InstanceCommonPanel:ShowInstanceCountDown(data)
    self:SetBeginCountDownTime(0)
    self:ClearBeginCountDownTimer()
    self._startTime = data.startTime
    self._instanceTimeContainer:SetActive(true)
end

function InstanceCommonPanel:GetStartTime()
    return self._startTime or DateTimeUtil.GetServerTime()
end

--显示副本自动离开倒计时
function InstanceCommonPanel:ShowInstanceLeaveCountDown(data)
    self._showLeaveCountDownCB = data.cb
    self._leaveEndTime = data.startTime + data.seconds
    self._instanceTimeContainer:SetActive(false)
    self._startContainer:SetActive(false)
    self._instanceLeaveContainer:SetActive(true)
    self._leaveCountDownTimer = TimerHeap:AddSecTimer(0, 1, 0, function() self:OnUpdataLeaveCountDownTime() end)
end

function InstanceCommonPanel:OnUpdataLeaveCountDownTime()
    if self._leaveEndTime - DateTimeUtil.GetServerTime() <= 0 then
        self:SetLeaveCountDownTime(0)
        TimerHeap:AddSecTimer(0.5, 0, 0, function() 
            self:ClearLeaveCountDownTimer()
            if self._showLeaveCountDownCB ~= nil then
                self._showLeaveCountDownCB()
                self._showLeaveCountDownCB = nil
            end
        end)
    else        
        self:SetLeaveCountDownTime(self._leaveEndTime - DateTimeUtil.GetServerTime())
    end
end

function InstanceCommonPanel:ClearLeaveCountDownTimer()
    if self._leaveCountDownTimer ~= nil then
        TimerHeap:DelTimer(self._leaveCountDownTimer)
        self._leaveCountDownTimer = nil
        self._instanceLeaveContainer:SetActive(false)
    end
end

function InstanceCommonPanel:SetLeaveCountDownTime(seconds)
    self._timeLeaveText.text = LanguageDataHelper.CreateContentWithArgs(53202, {["0"]=seconds})
end

function InstanceCommonPanel:ShowInstanceInfo(data)
    self._instanceContainer:SetActive(true)
    self._instanceNameText.text = data.instanceName
    self._instanceTargetText.text = data.targetText
    self._instanceDescText.text = data.descText
end

function InstanceCommonPanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function InstanceCommonPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    self._containerInfo.transform.localPosition = self._containerClosePosition
    self._tweenPosition:SetToPosOnce(self._containerInitPosition, 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function InstanceCommonPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._containerInfo.transform.localPosition = self._containerInitPosition
    self._tweenPosition:SetToPosOnce(self._containerClosePosition, 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

function InstanceCommonPanel:OnEnterCopy()
    if self._confirmCB then
        self._confirmCB()
    end
end