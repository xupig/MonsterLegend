-- EquipmentInstanceView.lua
local EquipmentInstanceView = Class.EquipmentInstanceView(ClassTypes.BaseComponent)

EquipmentInstanceView.interface = GameConfig.ComponentsConfig.Component
EquipmentInstanceView.classPath = "Modules.ModuleInstanceHall.ChildView.EquipmentInstanceView"

require "Modules.ModuleInstanceHall.ChildComponent.EquipmentInstanceListItem"
local EquipmentInstanceListItem = ClassTypes.EquipmentInstanceListItem

local BloodDungeonDataHelper = GameDataHelper.BloodDungeonDataHelper
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TeamDataHelper = GameDataHelper.TeamDataHelper
local TeamManager = PlayerManager.TeamManager
local teamData = PlayerManager.PlayerDataManager.teamData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager

function EquipmentInstanceView:Awake()
	self:InitCallBack()
	self:InitComps()
end

function EquipmentInstanceView:ShowView()
	self:InitInstanceListData()
    self:AddEventListeners()
	self:UpdateTimes()
	self:UpdateMatchData()
	if self._instanceListView then
		self._instanceListView:SetScrollRectState(true)
	end
end

function EquipmentInstanceView:CloseView()
	self:RemoveEventListeners()
	if self._instanceList and self._selectIndex then
		self._instanceList:GetItem(self._selectIndex):SetSelect(false)
	end
	self._selectIndex = nil
	if self._instanceListView then
		self._instanceListView:SetScrollRectState(false)
	end
end

function EquipmentInstanceView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.MATCH_BEGIN, self._matchBegin)
    EventDispatcher:AddEventListener(GameEvents.MATCH_END, self._matchEnd)
end

function EquipmentInstanceView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.MATCH_BEGIN, self._matchBegin)
    EventDispatcher:RemoveEventListener(GameEvents.MATCH_END, self._matchEnd)
end

function EquipmentInstanceView:InitCallBack()
	self:InitInstanceList()
	self._matchBegin = function() self:UpdateMatchData() end
	self._matchEnd = function() self:UpdateMatchData() end
    
    self._helpFightContainer = self:FindChildGO("Container_Help_Fight")
    self._helpFightText = self:GetChildComponent("Container_Help_Fight/Text_Info",'TextMeshWrapper')
    self._timesText = self:GetChildComponent("Container_Times/Text_Num",'TextMeshWrapper')
    self._matchText = self:GetChildComponent("Button_Match/Text",'TextMeshWrapper')

	self._matchButton = self:FindChildGO("Button_Match")
    self._csBH:AddClick(self._matchButton, function() self:OnMatchButtonClick() end)
    
	self._teamButton = self:FindChildGO("Button_Team")
	self._csBH:AddClick(self._teamButton, function() self:OnTeamButtonClick() end)
end

function EquipmentInstanceView:InitInstanceList()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._instanceList = list
	self._instanceListView = scrollView 
    self._instanceListView:SetHorizontalMove(true)
    self._instanceListView:SetVerticalMove(false)
    self._instanceList:SetItemType(EquipmentInstanceListItem)
    self._instanceList:SetPadding(0, 0, 0, 0)
    self._instanceList:SetGap(0, 0)
	self._instanceList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
	
	self._instanceListData = BloodDungeonDataHelper:GetAllId()
	self._instanceList:SetDataList(self._instanceListData)

	local itemClickedCB = 
	function(idx)
		self:OnInstanceListItemClicked(idx)
	end
	self._instanceList:SetItemClickedCB(itemClickedCB)
	self._instanceListView:SetScrollRectState(false)
end

function EquipmentInstanceView:OnInstanceListItemClicked(idx)
	local id = self._instanceListData[idx + 1]
	local nowLevel = GameWorld.Player().level
	local openLevel = BloodDungeonDataHelper:GetLevel(id)
	if nowLevel < openLevel then
		-- GameManager.GUIManager.ShowText(2,LanguageDataHelper.CreateContent(176, {["0"] = openLevel}))
		return
	end
	self._id = id
	self._instanceList:GetItem(self._selectIndex):SetSelect(false)
	self._selectIndex = idx
	self._instanceList:GetItem(self._selectIndex):SetSelect(true)
	-- self:UpdateText()
end

function EquipmentInstanceView:InitComps()
end

function EquipmentInstanceView:UpdateMatchData()
	local match_state = GameWorld.Player().match_state
	if match_state == 0 then
		--未匹配
		self._matchText.text = LanguageDataHelper.CreateContent(10304)--"个人匹配"
	else
		--已经匹配
		self._matchText.text = LanguageDataHelper.CreateContent(10302)--"取消匹配"
	end
end

function EquipmentInstanceView:UpdateTimes()
    local equipementInfo = GameWorld.Player().equipment_instance_info
    local commonTimes = GlobalParamsHelper.GetParamValue(532)
    local fightNums = equipementInfo[public_config.INSTANCE_EQUIPMENT_KEY_DAY_PASS_NUM]
    self._timesText.text = (commonTimes - fightNums)  .. "/" .. commonTimes
    if commonTimes - fightNums <= 0 then
        local totalNum = GlobalParamsHelper.GetParamValue(533)
        local helpNum = equipementInfo[public_config.INSTANCE_EQUIPMENT_KEY_DAY_ASSIST_NUM]
        self._helpFightContainer:SetActive(true)
        self._helpFightText.text = LanguageDataHelper.CreateContent(75029,{
            ["0"] = GlobalParamsHelper.GetParamValue(535),
            ["1"] = totalNum - helpNum,
            ["2"] = totalNum
        })
    else
        self._helpFightContainer:SetActive(false)
    end
end

function EquipmentInstanceView:InitInstanceListData()
	if self._instanceListData == nil then return end
	for i,v in ipairs(self._instanceListData) do
		self._instanceList:GetItem(i - 1):OnRefreshData(v)
	end


	local nowLevel = GameWorld.Player().level
	local num = 1
	local max = #self._instanceListData
	self._id = self._instanceListData[1]
	for i,id in ipairs(self._instanceListData) do
		if nowLevel < BloodDungeonDataHelper:GetLevel(id) then
			break
		end
		self._id = id
		num = i
	end
	self._selectIndex = num - 1
	self._instanceList:GetItem(self._selectIndex):SetSelect(true)
	if  num == 1 then		
		self._instanceList:SetPositionByNum(1)
	else
		if num == max then
			self._instanceList:SetPositionByNum(self._selectIndex + 1)		
		end
		self._instanceList:SetPositionByNum(self._selectIndex)	
	end
	-- self._instanceList:SetPositionByNum(self._selectIndex + 1)
end

function EquipmentInstanceView:OnMatchButtonClick()
	local match_state = GameWorld.Player().match_state

	if match_state == 0 then
		--未匹配
		local mapId = BloodDungeonDataHelper:GetSenceId(self._id)
		--LoggerHelper.Error("mapId =====" .. tostring(mapId))
		local targetId = TeamDataHelper.GetTeamTargetByMap(mapId).id
		if GameWorld.Player().team_status == 2 then
			TeamManager:RequestChangeTargetId(targetId)
		end
	
		TeamManager:RequestTeamMatch(targetId)
	else
		--匹配中
		TeamManager:RequestTeamMatchCancel()
	end


	-- local equipementInfo = GameWorld.Player().equipment_instance_info
    -- local commonTimes = GlobalParamsHelper.GetParamValue(532)
    -- local fightNums = equipementInfo[public_config.INSTANCE_EQUIPMENT_KEY_DAY_PASS_NUM]
	-- if commonTimes - fightNums <= 0 then
	-- 	TeamManager:RequestTeamMatch(targetId)
	-- 	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TEAM_SHOW_MATCH)
	-- else
	-- 	TeamManager:RequestTeamMatch(targetId)
	-- 	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TEAM_SHOW_MATCH)
	-- end

end

function EquipmentInstanceView:OnTeamButtonClick()
	local mapId = BloodDungeonDataHelper:GetSenceId(self._id)
	local targetId = TeamDataHelper.GetTeamTargetByMap(mapId).id

	if GameWorld.Player().team_status > 0 then
		 TeamManager:RequestChangeTargetId(targetId)
		 self:RequestTeamEnter()
		GUIManager.ClosePanel(PanelsConfig.InstanceHall)
	else
		teamData:SetReadyTarget(targetId)
		local data = {}
		data.targetId = targetId
        TeamManager:OpenTeamPanel(data)
	end
end

function EquipmentInstanceView:RequestTeamEnter()
    local mapId = BloodDungeonDataHelper:GetSenceId(self._id)
	TeamManager:RequestTeamStart(mapId)
end

function EquipmentInstanceView:MatchBegin()
	self:UpdateMatchData()
end

function EquipmentInstanceView:MatchEnd()
	self:UpdateMatchData()
end