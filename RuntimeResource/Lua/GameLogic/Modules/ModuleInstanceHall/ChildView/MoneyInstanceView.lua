-- MoneyInstanceView.lua
local MoneyInstanceView = Class.MoneyInstanceView(ClassTypes.BaseComponent)

MoneyInstanceView.interface = GameConfig.ComponentsConfig.Component
MoneyInstanceView.classPath = "Modules.ModuleInstanceHall.ChildView.MoneyInstanceView"

require "Modules.ModuleInstanceHall.ChildComponent.MoneyInstanceListItem"
local MoneyInstanceListItem = ClassTypes.MoneyInstanceListItem
require "Modules.ModuleInstanceHall.ChildComponent.MoneyInstanceDropListItem"
local MoneyInstanceDropListItem = ClassTypes.MoneyInstanceDropListItem

local MoneyInstanceDataHelper = GameDataHelper.MoneyInstanceDataHelper
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MoneyInstanceManager = PlayerManager.MoneyInstanceManager
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager
local InstanceManager = PlayerManager.InstanceManager
local OperatingActivityManager = PlayerManager.OperatingActivityManager
local SystemInfoDataHelper = GameDataHelper.SystemInfoDataHelper

function MoneyInstanceView:Awake()
    self:InitCallBack()
    self:InitComps()
end

function MoneyInstanceView:ShowView()
    if self._instanceListView then
        self._instanceListView:SetScrollRectState(true)
    end
    self._IconBg = self:FindChildGO("Image_BG")
    GameWorld.AddIcon(self._IconBg,13508)
    self:InitInstanceListData()
    self:UpdateDropDatas()
    self:UpdateText()
    self:CheckDouble()
    self:AddEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleInstanceHall.ChildView.MoneyInstanceView")
end

function MoneyInstanceView:CloseView()
    if self._instanceListView then
        self._instanceListView:SetScrollRectState(false)
    end
    self._IconBg=nil
    self:RemoveEventListeners()
    if self._instanceList and self._selectIndex then
        self._instanceList:GetItem(self._selectIndex):SetSelect(false)
    end
    self._selectIndex = nil
end

function MoneyInstanceView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.MONEY_INSTANCE_INFO_CHANGE, self._moneyInstanceChange)
end

function MoneyInstanceView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.MONEY_INSTANCE_INFO_CHANGE, self._moneyInstanceChange)
end

function MoneyInstanceView:InitCallBack()
    self._enterButton = self:FindChildGO("Button_Enter")
    self._csBH:AddClick(self._enterButton, function()self:OnEnterButtonClick() end)
    
    self._sweepButton = self:FindChildGO("Button_Sweep")
    self._csBH:AddClick(self._sweepButton, function()self:OnSweepButtonClick() end)
    
    self._buyButton = self:FindChildGO("Container_Info/Container_Times/Button_Plus")
    self._csBH:AddClick(self._buyButton, function()self:OnBuyButtonClick() end)
end

function MoneyInstanceView:InitComps()
    self:InitDropsList()
    self:InitInstanceList()
    
    self._moneyInstanceChange = function()self:UpdateText() end
    
    self._levelText = self:GetChildComponent("Container_Info/Container_Instance_Info/Text_Level", 'TextMeshWrapper')
    self._descText = self:GetChildComponent("Container_Info/Container_Instance_Info/Text_Desc", 'TextMeshWrapper')
    self._timesText = self:GetChildComponent("Container_Info/Container_Times/Text_Num", 'TextMeshWrapper')

    self._imgDouble = self:FindChildGO("Image_Double")
end

function MoneyInstanceView:InitInstanceList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._instanceList = list
    self._instanceListView = scrollView
    self._instanceListView:SetHorizontalMove(false)
    self._instanceListView:SetVerticalMove(true)
    self._instanceList:SetItemType(MoneyInstanceListItem)
    self._instanceList:SetPadding(0, 0, 0, 0)
    self._instanceList:SetGap(10, 0)
    self._instanceList:SetDirection(UIList.DirectionTopToDown, 1, -1)
    
    self._instanceListData = MoneyInstanceDataHelper:GetAllId()
    self._instanceList:SetDataList(self._instanceListData)
    
    local itemClickedCB =
        function(idx)
            self:OnInstanceListItemClicked(idx)
        end
    self._instanceList:SetItemClickedCB(itemClickedCB)
	self._instanceListView:SetScrollRectState(false)
end

function MoneyInstanceView:OnInstanceListItemClicked(idx)
    local id = self._instanceListData[idx + 1]
    local nowLevel = GameWorld.Player().level
    local openLevel = MoneyInstanceDataHelper:GetLevel(id)
    if nowLevel < openLevel then
        GUIManager.ShowText(2, LanguageDataHelper.CreateContent(176, {["0"] = openLevel}))
        return
    end
    self._id = id
    self._instanceList:GetItem(self._selectIndex):SetSelect(false)
    self._selectIndex = idx
    self._instanceList:GetItem(self._selectIndex):SetSelect(true)
    self:UpdateText()
    self:UpdateDropDatas()
end

function MoneyInstanceView:InitDropsList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Drops/ScrollViewList")
    self._dropList = list
    self._dropListView = scrollView
    self._dropListView:SetHorizontalMove(false)
    self._dropListView:SetVerticalMove(false)
    self._dropList:SetItemType(MoneyInstanceDropListItem)
    self._dropList:SetPadding(0, 0, 0, 0)
    self._dropList:SetGap(0, 0)
    self._dropList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function MoneyInstanceView:CheckDouble()
    if OperatingActivityManager:GetInstanceDoubleState(public_config.FUNCTION_ID_GOLD_INSTANCE) then
        self._imgDouble:SetActive(true)
    else
        self._imgDouble:SetActive(false)
    end
end

function MoneyInstanceView:InitInstanceListData()
    if self._instanceListData == nil then return end
    for i, v in ipairs(self._instanceListData) do
        self._instanceList:GetItem(i - 1):OnRefreshData(v)
    end
    
    
    local nowLevel = GameWorld.Player().level
    local num = 1
    self._id = self._instanceListData[1]
    for i, id in ipairs(self._instanceListData) do
        if nowLevel < MoneyInstanceDataHelper:GetLevel(id) then
            break
        end
        self._id = id
        num = i
    end
    self._selectIndex = num - 1
    self._instanceList:GetItem(self._selectIndex):SetSelect(true)
-- self._instanceList:SetSelectIndex(num)
end

function MoneyInstanceView:UpdateDropDatas()
    self._dropListData = MoneyInstanceDataHelper:GetPreviewReward(self._instanceListData[self._selectIndex + 1])
    self._dropList:SetDataList(self._dropListData)
end

function MoneyInstanceView:UpdateText()
    -- self._id = self._instanceListData[1]
    self._levelText.text = LanguageDataHelper.CreateContent(74705, {["0"] = MoneyInstanceDataHelper:GetLevel(self._id)})
    self._descText.text = LanguageDataHelper.CreateContent(74706, {["0"] = LanguageDataHelper.CreateContent(MoneyInstanceDataHelper:GetDesc(self._id))})
    
    local petData = GameWorld.Player().instance_gold_info
    local fightNum = petData[public_config.INSTANCE_GOLD_KEY_LEFT_ENTER_NUM]
    local buyNum = petData[public_config.INSTANCE_GOLD_KEY_BUY_NUM]
    local baseNum = GlobalParamsHelper.GetParamValue(516)
    self._timesText.text = fightNum .. "/" .. (baseNum + buyNum)
    self._fightNum = fightNum
end

function MoneyInstanceView:OnEnterButtonClick()
	if self._fightNum <= 0 then
		GUIManager.ShowText(1, SystemInfoDataHelper.CreateContent(9502))
		return
	end
    
    MoneyInstanceManager:EnterMoneyInstance(self._id)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_money_instance_battle_button")
end

function MoneyInstanceView:OnSweepButtonClick()
    self._sweepFunc = function()self:SweepSureFunc() end
    self._getSweepFightFunc = function() return self:GetFightNum() end
    self._getSweepBuyFunc = function() return self:GetBuyNum() end
    self._showBuyVipFunc = function()self:OnBuyButtonClick() end
    local petData = GameWorld.Player().instance_gold_info
    local data = {}
    data.fightNumFunc = self._getSweepFightFunc --获取能打的次数的方法
    data.sweepLevel = GlobalParamsHelper.GetParamValue(513)--扫荡需要的等级
    data.sweepFunc = self._sweepFunc --扫荡方法
    data.sweepCostData = GlobalParamsHelper.GetParamValue(514)--扫荡消耗数据
    data.vipBuyType = public_config.MAP_TYPE_GOLD --VIP购买次数类型
    data.showBuyVipFunc = self._showBuyVipFunc --显示vip购买次数方法
    data.sweepType = 1 --扫荡类型
    data.start = petData[public_config.INSTANCE_GOLD_KEY_PASS_INFO][self._selectIndex + 1]--星星数
    local sweepData = InstanceManager:ResetSweepData(data)
    InstanceManager:OnEnterSweep(sweepData)
end

function MoneyInstanceView:OnBuyButtonClick()
    local data = {}
    data.type = public_config.MAP_TYPE_GOLD
    data.cb = function()MoneyInstanceManager:BuyMoneyInstanceEnterNum(1) end
    GUIManager.ShowPanel(PanelsConfig.VIPBuyInstancePanel, data)
end


function MoneyInstanceView:SweepSureFunc()
	MoneyInstanceManager:SweepMoneyInstance(self._id)
end

function MoneyInstanceView:GetFightNum()	
	local petData = GameWorld.Player().instance_gold_info	
	return petData[public_config.INSTANCE_GOLD_KEY_LEFT_ENTER_NUM]
end

function MoneyInstanceView:GetBuyNum()
	local petData = GameWorld.Player().instance_gold_info
	return petData[public_config.INSTANCE_GOLD_KEY_BUY_NUM]
end
