-- TowerInstanceView.lua
local TowerInstanceView = Class.TowerInstanceView(ClassTypes.BaseComponent)

TowerInstanceView.interface = GameConfig.ComponentsConfig.Component
TowerInstanceView.classPath = "Modules.ModuleInstanceHall.ChildView.TowerInstanceView"

require "Modules.ModuleInstanceHall.ChildComponent.TowerInstanceDropListItem"
local TowerInstanceDropListItem = ClassTypes.TowerInstanceDropListItem

local PetInstanceDataHelper = GameDataHelper.PetInstanceDataHelper
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TowerInstanceManager = PlayerManager.TowerInstanceManager
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager
local InstanceManager = PlayerManager.InstanceManager
local OperatingActivityManager = PlayerManager.OperatingActivityManager
local SystemInfoDataHelper = GameDataHelper.SystemInfoDataHelper

function TowerInstanceView:Awake()
	self:InitCallBack()
	self:InitComps()
end

function TowerInstanceView:ShowView()
	self._IconBg = self:FindChildGO("Image_BG")
	GameWorld.AddIcon(self._IconBg,13510)
	self:UpdateDropDatas()
	self:UpdateText()
	self:CheckDouble()
	self:AddEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleInstanceHall.ChildView.TowerInstanceView")
end

function TowerInstanceView:CloseView()
	self._IconBg=nil
	self:RemoveEventListeners()
end

function TowerInstanceView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.TOWER_INSTANCE_INFO_CHANGE, self._towerInstanceChange)
end

function TowerInstanceView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.TOWER_INSTANCE_INFO_CHANGE, self._towerInstanceChange)
end

function TowerInstanceView:InitCallBack()
	self._enterButton = self:FindChildGO("Button_Enter")
	self._csBH:AddClick(self._enterButton, function() self:OnEnterButtonClick() end)
	
	self._sweepButton = self:FindChildGO("Button_Sweep")
	self._csBH:AddClick(self._sweepButton, function() self:OnSweepButtonClick() end)
	
	self._buyButton = self:FindChildGO("Container_Info/Container_Times/Button_Plus")
    self._csBH:AddClick(self._buyButton, function() self:OnBuyButtonClick() end)
end

function TowerInstanceView:InitComps()
    self:InitDropsList()
    
    self._towerInstanceChange = function() self:UpdateText() end

    self._levelText = self:GetChildComponent("Container_Info/Container_Instance_Info/Text_Level",'TextMeshWrapper')
    self._descText = self:GetChildComponent("Container_Info/Container_Instance_Info/Text_Desc",'TextMeshWrapper')
    self._timesText = self:GetChildComponent("Container_Info/Container_Times/Text_Num",'TextMeshWrapper')
    self._imgDouble = self:FindChildGO("Image_Double")
end
	
function TowerInstanceView:InitDropsList()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Drops/ScrollViewList")
	self._dropList = list
	self._dropListView = scrollView 
    self._dropListView:SetHorizontalMove(false)
    self._dropListView:SetVerticalMove(false)
    self._dropList:SetItemType(TowerInstanceDropListItem)
    self._dropList:SetPadding(0, 0, 0, 0)
    self._dropList:SetGap(0, 0)
    self._dropList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function TowerInstanceView:CheckDouble()
    if OperatingActivityManager:GetInstanceDoubleState(public_config.FUNCTION_ID_TOWER_DEFENSE) then
        self._imgDouble:SetActive(true)
    else
        self._imgDouble:SetActive(false)
    end
end

function TowerInstanceView:UpdateDropDatas()
	self._dropListData = GlobalParamsHelper.GetParamValue(718)
	self._dropList:SetDataList(self._dropListData)
end

function TowerInstanceView:UpdateText()
	self._levelText.text = LanguageDataHelper.CreateContent(76300)--,{["0"] = PetInstanceDataHelper:GetLevel(self._id)})
	self._descText.text = LanguageDataHelper.CreateContent(76301)--,{["0"] = PetInstanceDataHelper:GetDescribe(self._id)})

	local towerData = GameWorld.Player().tower_defense_info
	local fightNum = towerData[public_config.TOWER_DEFENSE_KEY_ENTER_NUM]
	local buyNum = towerData[public_config.TOWER_DEFENSE_KEY_BUY_NUM]
	local baseNum = GlobalParamsHelper.GetParamValue(696)
	self._timesText.text = (baseNum + buyNum - fightNum) .. "/" .. (baseNum + buyNum)
	self._fightNum = baseNum + buyNum - fightNum
end

function TowerInstanceView:OnEnterButtonClick()
	if self._fightNum <= 0 then
		GUIManager.ShowText(1, SystemInfoDataHelper.CreateContent(10701))
		return
	end

	TowerInstanceManager:EnterTowerInstance()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_tower_instance_battle_button")
end

function TowerInstanceView:OnSweepButtonClick()
	self._sweepFunc = function(num)self:SweepSureFunc(num) end
	self._getSweepFightFunc = function() return self:GetFightNum() end
	self._getSweepBuyFunc = function() return self:GetBuyNum() end
	self._showBuyVipFunc = function() self:OnBuyButtonClick() end
	local petData = GameWorld.Player().tower_defense_info
	local data = {}
	data.fightNumFunc = self._getSweepFightFunc--获取能打的次数的方法
	data.sweepLevel = GlobalParamsHelper.GetParamValue(697)--扫荡需要的等级
	data.sweepFunc = self._sweepFunc--扫荡方法
	data.sweepCostData = GlobalParamsHelper.GetParamValue(698)--扫荡消耗数据
	data.vipBuyType = public_config.MAP_TYPE_TOWER_DEFENSE--VIP购买次数类型
	data.showBuyVipFunc = self._showBuyVipFunc--显示vip购买次数方法
	data.sweepType = 2--扫荡类型
	data.costNum =  GlobalParamsHelper.GetParamValue(849)
	data.start = 3--星星数
	local sweepData = InstanceManager:ResetSweepData(data)
	InstanceManager:OnEnterSweep(sweepData)
	-- PetInstanceManager:SweepPetInstance(self._id)
end

function TowerInstanceView:OnBuyButtonClick()
	local data = {}
	data.type = public_config.MAP_TYPE_TOWER_DEFENSE
	data.cb = function() TowerInstanceManager:BuyTowerInstanceEnterNum(1) end
	GUIManager.ShowPanel(PanelsConfig.VIPBuyInstancePanel, data)
end

function TowerInstanceView:GetFightNum()	
	local towerData = GameWorld.Player().tower_defense_info
	-- LoggerHelper.Log("TowerInstanceView:GetFightNum():" .. PrintTable:TableToStr(towerData))	

	local fightNum = towerData[public_config.TOWER_DEFENSE_KEY_ENTER_NUM]
	local buyNum = towerData[public_config.TOWER_DEFENSE_KEY_BUY_NUM]
	local baseNum = GlobalParamsHelper.GetParamValue(696)

	return baseNum + buyNum - fightNum
end

function TowerInstanceView:GetBuyNum()
	local towerData = GameWorld.Player().tower_defense_info
	return towerData[public_config.TOWER_DEFENSE_KEY_BUY_NUM]
end

function TowerInstanceView:SweepSureFunc(num)
	--LoggerHelper.Error("TowerInstanceView:SweepSureFunc():"..num)
	TowerInstanceManager:SweepPetInstance(num)
end