-- ExpInstanceView.lua
local ExpInstanceView = Class.ExpInstanceView(ClassTypes.BaseComponent)

ExpInstanceView.interface = GameConfig.ComponentsConfig.Component
ExpInstanceView.classPath = "Modules.ModuleInstanceHall.ChildView.ExpInstanceView"

local public_config = GameWorld.public_config
local ExpInstanceManager = PlayerManager.ExpInstanceManager
local GUIManager = GameManager.GUIManager
local TeamManager = PlayerManager.TeamManager
local teamData = PlayerManager.PlayerDataManager.teamData
local bagData = PlayerManager.PlayerDataManager.bagData
local ExpInstanceDataHelper = GameDataHelper.ExpInstanceDataHelper
local TeamDataHelper = GameDataHelper.TeamDataHelper
local public_config = GameWorld.public_config
local error_code = GameWorld.error_code
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig
local EnumType = GameConfig.EnumType
local SystemInfoManager = GameManager.SystemInfoManager
local TipsManager = GameManager.TipsManager
local SystemInfoDataHelper = GameDataHelper.SystemInfoDataHelper

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

function ExpInstanceView:Awake()
	self:InitCallBack()
	self:InitComps()
end

function ExpInstanceView:ShowView()
    self._IconBg = self:FindChildGO("Image_Bg")
	GameWorld.AddIcon(self._IconBg,13509)
	EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleInstanceHall.ChildView.ExpInstanceView")
	self:UpdateView()
	self:AddEventListeners()
end

function ExpInstanceView:CloseView()
    self._IconBg=nil
	self:RemoveEventListeners()
end

function ExpInstanceView:InitCallBack()
	self._expInstanceChange = function() self:UpdateView() end
	self._itemChange = function() self:UpdateItemCount() end
end

function ExpInstanceView:InitComps()
	self._addTimesButton = self:FindChildGO("Container_Info/Container_Times/Button_Plus")
    self._csBH:AddClick(self._addTimesButton, function()self:OnAddTimesClick() end)

    self._addItemButton = self:FindChildGO("Container_Info/Container_Item/Button_Plus")
    self._csBH:AddClick(self._addItemButton, function()self:OnAddItemClick() end)

	-- self._btnTeamEnter = self:FindChildGO("Button_TeamEnter")
 --    self._csBH:AddClick(self._btnTeamEnter, function()self:OnTeamEnter() end)

	self._btnSingleEnter = self:FindChildGO("Button_SingleEnter")
    self._csBH:AddClick(self._btnSingleEnter, function()self:OnSingleEnter() end)

    -- self._btnMatch = self:FindChildGO("Button_Match")
    -- self._csBH:AddClick(self._btnMatch, function()self:OnMatch() end)

    self._txtTimes = self:GetChildComponent("Container_Info/Container_Times/Text_Times","TextMeshWrapper")
    self._txtItemCount = self:GetChildComponent("Container_Info/Container_Item/Text_ItemCount","TextMeshWrapper")
    
   	self._itemIcon = self:FindChildGO("Container_Info/Container_Item/Container_Icon")

    self._cfgData = ExpInstanceDataHelper:GetExpInstanceCfg(1)

    for itemId,needNum in pairs(self._cfgData.cost) do
    	local icon = ItemDataHelper.GetIcon(itemId)
    	GameWorld.AddIcon(self._itemIcon,icon)
    end

    local parent2 = self:FindChildGO("Container_Info/Container_Instance_Info/Container_Exp")
   	local itemGo2 = GUIManager.AddItem(parent2, 1)
   	self._expIcon = UIComponentUtil.AddLuaUIComponent(itemGo2, ItemGrid)
   	self._expIcon:SetItem(public_config.MONEY_TYPE_EXP)

    local txtInfo = self:GetChildComponent("Container_Info/Container_Instance_Info/Text_Info","TextMeshWrapper")
    local txtInfo1 = self:GetChildComponent("Container_Info/Container_Instance_Info/Text_Info1","TextMeshWrapper")
    local txtInfo2 = self:GetChildComponent("Container_Info/Container_Instance_Info/Text_Info2","TextMeshWrapper")
    local txtInfo3 = self:GetChildComponent("Container_Info/Container_Instance_Info/Text_Info3","TextMeshWrapper")

    txtInfo.text = LanguageDataHelper.CreateContent(58419)
    txtInfo1.text = LanguageDataHelper.CreateContent(58420)
    txtInfo2.text = LanguageDataHelper.CreateContent(58421)
    txtInfo3.text = LanguageDataHelper.CreateContent(58422)
end

function ExpInstanceView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.EXP_INSTANCE_INFO_CHANGE, self._expInstanceChange)
    EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE, self._itemChange)
end

function ExpInstanceView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.EXP_INSTANCE_INFO_CHANGE, self._expInstanceChange)
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE, self._itemChange)
end

function ExpInstanceView:UpdateView()
	local info = GameWorld.Player().exp_instance_info
	local buyNum = info[public_config.EXP_INSTANCE_BUY_ENTER_NUM] or 0
	local itemAddNum = info[public_config.EXP_INSTANCE_ITEM_ADD_ENTER_NUM] or 0
	local totalNum = self._cfgData.enter_limit + buyNum + itemAddNum
	self._timesLeft = totalNum - (info[public_config.EXP_INSTANCE_ENTER_TIMES] or 0)

	if self._timesLeft > 0 then
		self._txtTimes.text = self._timesLeft.."/".. totalNum
	else
		self._txtTimes.text = StringStyleUtil.GetColorStringWithColorId(self._timesLeft.."/"..totalNum,ItemConfig.ItemForbidden)
	end

	self:UpdateItemCount()
end

function ExpInstanceView:UpdateItemCount()
	for itemId,needNum in pairs(self._cfgData.cost) do
		local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
		if bagNum >= needNum then
			self._itemEnough = true
			self._txtItemCount.text = needNum.."/"..bagNum
		else
			self._itemEnough = false
			self._txtItemCount.text = StringStyleUtil.GetColorStringWithColorId(needNum.."/"..bagNum,ItemConfig.ItemForbidden)
		end
	end
end

function ExpInstanceView:OnSingleEnter()
	if self._timesLeft <= 0 then
		GUIManager.ShowText(1, SystemInfoDataHelper.CreateContent(2814))
		return
	end

	EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_exp_instance_single_button_click")

	if self._itemEnough then
		ExpInstanceManager:RequestExpInstanceEnter()
	else
		for itemId,needNum in pairs(self._cfgData.cost) do
			self:BuyGuideTip(itemId)
		end
	end
end


function ExpInstanceView:BuyGuideTip(itemId)
    if ItemDataHelper.GetGuideBuyCost(itemId)~=0 then
        local btnArgs = {}
        btnArgs.guideBuy = {itemId}
        TipsManager:ShowItemTipsById(itemId,btnArgs)
    end
end

function ExpInstanceView:OnAddTimesClick()
	local data = {}
	data.type = public_config.MAP_TYPE_EXP
	data.cb = function() ExpInstanceManager:BuyExpInstanceEnterNum(1) end
	GUIManager.ShowPanel(PanelsConfig.VIPBuyInstancePanel, data)
end

function ExpInstanceView:OnAddItemClick()
	local data ={["id"]=EnumType.ShopPageType.Common}
	GUIManager.ShowPanel(PanelsConfig.Shop, data, nil)
end

---------------------------------------组队和匹配不用了----------------------------------------------
-- function ExpInstanceView:OnMatch()
-- 	--道具不足
-- 	if not self._itemEnough then
-- 		SystemInfoManager:ShowClientTip(error_code.ERR_TEAM_ITEM_NOT_ENOUGH)
-- 		return
-- 	end
-- 	--次数不足
-- 	if self._timesLeft <= 0 then
-- 		SystemInfoManager:ShowClientTip(error_code.ERR_TEAM_EXP_INSTANCE_TIMES_OVER)
-- 		return
-- 	end
-- 	local mapId = self._cfgData.map_id
-- 	local targetId = TeamDataHelper.GetTeamTargetByMap(mapId).id
-- 	if GameWorld.Player().team_status == 2 then
-- 		TeamManager:RequestChangeTargetId(targetId)
-- 	end
-- 	TeamManager:RequestTeamMatch(targetId)
-- 	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TEAM_SHOW_MATCH)
-- end

-- function ExpInstanceView:OnTeamEnter()
-- 	local mapId = self._cfgData.map_id
-- 	local targetId = TeamDataHelper.GetTeamTargetByMap(mapId).id
-- 	if GameWorld.Player().team_status > 0 then
-- 		-- local memberCount  = teamData:GetMyTeamMemberCount()
-- 		TeamManager:RequestChangeTargetId(targetId)
-- 		self:RequestTeamEnter()
-- 		GUIManager.ClosePanel(PanelsConfig.InstanceHall)
--     else
-- 		teamData:SetReadyTarget(targetId)
--         TeamManager:OpenTeamPanel()
-- 	end
-- end

-- function ExpInstanceView:RequestTeamEnter()
-- 	local mapId = self._cfgData.map_id
-- 	TeamManager:RequestTeamStart(mapId)
-- end