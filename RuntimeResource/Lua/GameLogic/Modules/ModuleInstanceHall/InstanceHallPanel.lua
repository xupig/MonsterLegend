local InstanceHallPanel = Class.InstanceHallPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager

require "Modules.ModuleInstanceHall.ChildView.ExpInstanceView"
local ExpInstanceView = ClassTypes.ExpInstanceView
require "Modules.ModuleAion.ChildView.AionTowerView"
local AionTowerView = ClassTypes.AionTowerView
require "Modules.ModuleInstanceHall.ChildView.PetInstanceView"
local PetInstanceView = ClassTypes.PetInstanceView
require "Modules.ModuleInstanceHall.ChildView.MoneyInstanceView"
local MoneyInstanceView = ClassTypes.MoneyInstanceView
require "Modules.ModuleInstanceHall.ChildView.EquipmentInstanceView"
local EquipmentInstanceView = ClassTypes.EquipmentInstanceView
require "Modules.ModuleInstanceHall.ChildView.TowerInstanceView"
local TowerInstanceView = ClassTypes.TowerInstanceView

local UIToggleGroup = ClassTypes.UIToggleGroup
local TeamManager = PlayerManager.TeamManager

local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

function InstanceHallPanel:Awake()
	self:InitCallBack()
	self:InitComps()
end

function InstanceHallPanel:InitCallBack()
    -- self._sTabClickCB = function(index)
    --     self:OnSTabClick(index)
    -- end

    -- self._showMatchCB = function()
    --     self:OnShowMatch()
    -- end
end

-- --二级标签点击处理
-- function InstanceHallPanel:OnSTabClick(index)
--     for i,sData in ipairs(self._views) do
--         for k, view in ipairs(sData) do
--             if self._selectedIndex == i and index == k then
--                 -- view.gameObject:SetActive(true)
--                 if not view.gameObject.activeSelf then
--                     view.gameObject:SetActive(true)
--                 end
--                 view:OnEnable()
--                 view.transform.localScale = ShowScale
--             else
--                 -- view.gameObject:SetActive(false)
--                 view.transform.localScale = HideScale
--                 view:OnDisable()
--             end
--         end
--     end
-- end

function InstanceHallPanel:InitComps()
    self:InitView("Container_AionTower", AionTowerView,  1,1)--永恒之塔
    self:InitView("Container_ExpInstance", ExpInstanceView,  1,2)--经验本
    self:InitView("Container_PetInstance", PetInstanceView,  1,3)--守护结界塔
    self:InitView("Container_MoneyInstance", MoneyInstanceView,  1,4)--众神宝库
    self:InitView("Container_TowerInstance", TowerInstanceView,  1,5)--百鬼夜行
    self:InitView("Container_EquitmentInstance", EquipmentInstanceView,  2,1)--圣灵血

    --------------------------匹配倒计时-----------------------------------
    -- self._containerMatch = self:FindChildGO("Container_Match")
    -- self._txtMatchTime = self:GetChildComponent("Container_Match/Text_Time", "TextMeshWrapper")
    -- self._btnCancelMatch = self:FindChildGO("Container_Match/Button_Cancel")
    -- self._csBH:AddClick(self._btnCancelMatch, function() self:OnCancelMatch() end)
end

function InstanceHallPanel:OnShow(data)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleInstanceHall.InstanceHallPanel")
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)

    --EventDispatcher:AddEventListener(GameEvents.UIEVENT_TEAM_SHOW_MATCH,self._showMatchCB)
end

function InstanceHallPanel:OnClose()
    -- self._expInstanceView:SetActive(false)
    -- self._petInstanceView:SetActive(false)
    -- self._aionTowerView:SetActive(false)

    -- self._expInstanceView.transform.localScale = HideScale
    -- self._petInstanceView.transform.localScale = HideScale
    -- self._aionTowerView.transform.localScale = HideScale
    -- self._aionTowerView:OnDisable()

    -- if self._inMatch then
    --     self:OnCancelMatch()
    -- end
    -- EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TEAM_SHOW_MATCH,self._showMatchCB)
end

-- function InstanceHallPanel:OnShowMatch()
--     -- self._containerMatch:SetActive(true)
--     if not self._containerMatch.gameObject.activeSelf then
--         self._containerMatch:SetActive(true)
--     end
--     self._containerMatch.transform.localScale = ShowScale
--     self._inMatch = true
--     self._matchTimeCount = 0

--     self._matchTimerId = TimerHeap:AddSecTimer(0,1,0,function ()
--         self._matchTimeCount = self._matchTimeCount + 1
--         self._txtMatchTime.text = tostring(self._matchTimeCount)
--     end)
-- end

-- function InstanceHallPanel:OnCancelMatch()
--     self._inMatch = false
--     TimerHeap:DelTimer(self._matchTimerId)
--     TeamManager:RequestTeamMatchCancel()
--     -- self._containerMatch:SetActive(false)
--     self._containerMatch.transform.localScale = HideScale
-- end


function InstanceHallPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function InstanceHallPanel:StructingViewInit()
     return true
end

function InstanceHallPanel:ScaleToHide()
    return true
end