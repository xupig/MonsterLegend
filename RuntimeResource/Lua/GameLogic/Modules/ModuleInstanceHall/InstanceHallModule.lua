InstanceHallModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function InstanceHallModule.Init()
    GUIManager.AddPanel(PanelsConfig.InstanceHall,"Panel_InstanceHall",GUILayer.LayerUIPanel,"Modules.ModuleInstanceHall.InstanceHallPanel",true,false, nil, true, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.InstanceCommon,"Panel_InstanceCommon",GUILayer.LayerUIMiddle,"Modules.ModuleInstanceHall.InstanceCommonPanel",true,PanelsConfig.EXTEND_TO_FIT)
    GUIManager.AddPanel(PanelsConfig.Sweep, "Panel_Sweep", GUILayer.LayerUIPanel, "Modules.ModuleSweep.SweepPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return InstanceHallModule