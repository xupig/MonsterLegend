-- PersonalBossListInfoItem.lua
local PersonalBossListInfoItem = Class.PersonalBossListInfoItem(ClassTypes.UIListItem)
PersonalBossListInfoItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
PersonalBossListInfoItem.classPath = "Modules.ModuleWorldBoss.ChildComponent.PersonalBossListInfoItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
local StringStyleUtil = GameUtil.StringStyleUtil
local ColorConfig = GameConfig.ColorConfig

function PersonalBossListInfoItem:Awake()
    self._base.Awake(self)
    
    self._textBossName = self:GetChildComponent("Container_content/Text_BossName", "TextMeshWrapper")
    self._containerIcon = self:FindChildGO("Container_content/Container_Icon")
    self._imageSelected = self:FindChildGO("Container_content/Image_Selected")
    self._gradeImage = self:FindChildGO("Container_content/Image_Grade")
    self._gradeText = self:GetChildComponent("Container_content/Image_Grade/Text", "TextMeshWrapper")
    self:SetSelected(false)
end

function PersonalBossListInfoItem:OnDestroy()
    self._textBossName = nil
    self._containerIcon = nil
    self._imageSelected = nil
    self._base.OnDestroy(self)
end

function PersonalBossListInfoItem:SetSelected(isSeleted)
    self._imageSelected:SetActive(isSeleted)
    if self._data then
        self._data:SetIsSelected(isSeleted)
    end
end

--override
function PersonalBossListInfoItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        local levelLimit = data:GetLevelLimit()
        local str = string.format("%s %s%s", LanguageDataHelper.GetContent(data:GetBossName()), data:GetBossLevel(), LanguageDataHelper.GetContent(69))
        if GameWorld.Player().level < levelLimit then
            self._textBossName.text = StringStyleUtil.GetColorString(str, ColorConfig.H)
        else
            self._textBossName.text = StringStyleUtil.GetColorString(str, ColorConfig.A)
        end
        GameWorld.AddIcon(self._containerIcon, data:GetBossIcon(), nil, true)
        self:SetSelected(data:GetIsSelected())
        self._gradeImage:SetActive(data:GetLevelShow() > 0)
        self._gradeText.text = LanguageDataHelper.CreateContentWithArgs(56084, {["0"]=data:GetLevelShow()})
    end
end
