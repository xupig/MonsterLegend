-- BossHomeListInfoItem.lua
local BossHomeListInfoItem = Class.BossHomeListInfoItem(ClassTypes.UIListItem)
BossHomeListInfoItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
BossHomeListInfoItem.classPath = "Modules.ModuleWorldBoss.ChildComponent.BossHomeListInfoItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
local UIToggle = ClassTypes.UIToggle
local WorldBossManager = PlayerManager.WorldBossManager
local DateTimeUtil = GameUtil.DateTimeUtil

function BossHomeListInfoItem:Awake()
    self._base.Awake(self)
    
    self._textBossName = self:GetChildComponent("Container_content/Text_BossName", "TextMeshWrapper")
    self._textRebornCountDown = self:GetChildComponent("Container_content/Text_RebornCountDown", "TextMeshWrapper")
    self._goRebornCountDown = self:FindChildGO("Container_content/Text_RebornCountDown")
    self._containerIcon = self:FindChildGO("Container_content/Container_Icon")
    self._imageSelected = self:FindChildGO("Container_content/Image_Selected")
    self._imageInstrest = self:FindChildGO("Container_content/Text_BossIntrest")
    self._gradeImage = self:FindChildGO("Container_content/Image_Grade")
    self._gradeText = self:GetChildComponent("Container_content/Image_Grade/Text", "TextMeshWrapper")
    self:SetSelected(false)
    self._onIsIntrestChanged = function(id) self:OnIsIntrestChanged(id) end
    EventDispatcher:AddEventListener(GameEvents.On_Boss_Home_Intrest_Changed, self._onIsIntrestChanged)
end

function BossHomeListInfoItem:OnDestroy()
    EventDispatcher:RemoveEventListener(GameEvents.On_Boss_Home_Intrest_Changed, self._onIsIntrestChanged)
    self._textBossName = nil
    self._textRebornCountDown = nil
    self._goRebornCountDown = nil
    self._containerIcon = nil
    self._imageSelected = nil
    self._base.OnDestroy(self)
end

function BossHomeListInfoItem:OnIsIntrestChanged(id)
    if self._data and self._data:GetId() == id then
        self:SetInstrest(self._data:GetIsIntrest())
    end
end

function BossHomeListInfoItem:SetSelected(isSeleted)
    self._imageSelected:SetActive(isSeleted)
    if self._data then
        self._data:SetIsSelected(isSeleted)
    end
end

function BossHomeListInfoItem:SetInstrest(isInstrest)
    self._imageInstrest:SetActive(isInstrest)
end

function BossHomeListInfoItem:UpdateCountDown()
    local data = self._data
    if data then
        local isDead = data:IsDead()
        if self._lastIsDead ~= isDead then
            self._goRebornCountDown:SetActive(isDead)
        end
        self._lastIsDead = isDead
        if isDead then
            local leftTime = data:GetRebornTime() - DateTimeUtil:GetServerTime()
            self._textRebornCountDown.text = DateTimeUtil:FormatFullTime(leftTime)
        end
    end
end

--override
function BossHomeListInfoItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        GameWorld.AddIcon(self._containerIcon, data:GetBossIcon(), nil, true)
        self:SetInstrest(data:GetIsIntrest())
        self:SetSelected(data:GetIsSelected())
        self:UpdateCountDown()
        self._textBossName.text = string.format("%s %s%s", LanguageDataHelper.GetContent(data:GetBossName()), data:GetBossLevel(), LanguageDataHelper.GetContent(69))
        self._gradeImage:SetActive(data:GetLevelShow() > 0)
        self._gradeText.text = LanguageDataHelper.CreateContentWithArgs(56084, {["0"]=data:GetLevelShow()})
    end
end
