-- PersonalBossDropRewardItem.lua
require "UIComponent.Extend.ItemManager"

local PersonalBossDropRewardItem = Class.PersonalBossDropRewardItem(ClassTypes.UIListItem)
PersonalBossDropRewardItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
PersonalBossDropRewardItem.classPath = "Modules.ModuleWorldBoss.ChildComponent.PersonalBossDropRewardItem"

local ItemManager = ClassTypes.ItemManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function PersonalBossDropRewardItem:Awake()
    self._tagImage = self:FindChildGO("Image_Tag")
    self._itemManager = ItemManager()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._icon:ActivateTipsById()
end

function PersonalBossDropRewardItem:OnDestroy()
    self._textCount = nil
    self._icon = nil
end

--override {itemId,count,must}
function PersonalBossDropRewardItem:OnRefreshData(data)
    self._data = data
    self._icon:SetItem(data[1], data[2])
    self._tagImage:SetActive(data[3]==1)
end
