-- PersonalBossContainerView.lua
require "Modules.ModuleWorldBoss.ChildComponent.PersonalBossListInfoItem"
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
require "PlayerManager/PlayerData/EventData"
require "UIComponent.Extend.ItemGrid"
require "Modules.ModuleWorldBoss.ChildComponent.PersonalBossDropRewardItem"

local PersonalBossContainerView = Class.PersonalBossContainerView(ClassTypes.BaseComponent)

PersonalBossContainerView.interface = GameConfig.ComponentsConfig.Component
PersonalBossContainerView.classPath = "Modules.ModuleWorldBoss.ChildView.PersonalBossContainerView"

local PersonalBossListInfoItem = ClassTypes.PersonalBossListInfoItem
local TaskRewardItem = ClassTypes.TaskRewardItem
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local PersonalBossManager = PlayerManager.PersonalBossManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local ActorModelComponent = GameMain.ActorModelComponent
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local BaseUtil = GameUtil.BaseUtil
local PersonalBossDropRewardItem = ClassTypes.PersonalBossDropRewardItem


function PersonalBossContainerView:Awake()
    self._textRemainCountValue = self:GetChildComponent("Container_BossInfo/Text_RemainCountValue", "TextMeshWrapper")
    self._containerBossName = self:FindChildGO("Container_BossInfo/Container_Name")
    self._btnHelp = self:FindChildGO("Container_BossInfo/Button_Help")
    self._btnFight = self:FindChildGO("Container_BossInfo/Button_Fight")

    self._btnFightText = self:GetChildComponent("Container_BossInfo/Button_Fight/Text", "TextMeshWrapper")
    self._btnFightText.text = LanguageDataHelper.CreateContent(56076)

    self._fightRedPointImage = self:FindChildGO("Container_BossInfo/Button_Fight/Image_RedPoint")

    self._csBH:AddClick(self._btnHelp, function()self:HelpBtnClick() end)
    self._csBH:AddClick(self._btnFight, function()self:FightBtnClick() end)
    
    self._btnCloseTips = self:FindChildGO("Container_Tips/Button_Close")
    self._btnCloseBG = self:FindChildGO("Container_Tips/Image_BG")
    self._csBH:AddClick(self._btnCloseTips, function()self:CloseTipsBtnClick() end)
    self._csBH:AddClick(self._btnCloseBG, function()self:CloseTipsBtnClick() end)
    self._containerTips = self:FindChildGO("Container_Tips")
    self._containerTips:SetActive(false)

    self._btnCloseVIPTips = self:FindChildGO("Container_VIPTips/Button_Close")
    self._csBH:AddClick(self._btnCloseVIPTips, function()self:CloseVIPTipsBtnClick() end)
    self._btnOKVIPTips = self:FindChildGO("Container_VIPTips/Button_OK")
    self._csBH:AddClick(self._btnOKVIPTips, function()self:OKVIPTipsBtnClick() end)
    self._containerVIPTips = self:FindChildGO("Container_VIPTips")
    self._containerVIPTips:SetActive(false)

    self._textTipsInfo = self:GetChildComponent("Container_Tips/Text_Info", "TextMeshWrapper")
    self._textTipsTitle = self:GetChildComponent("Container_Tips/Text_Title", "TextMeshWrapper")
    self._containerTooHighLevel = self:FindChildGO("Container_BossInfo/Container_TooHighLevel")
    self._containerTooHighLevel:SetActive(false)

    self._btnGuideTire = self:FindChildGO("Container_BossInfo/Button_GuideTire")
    self._csBH:AddClick(self._btnGuideTire, function()self:GuideTireBtnClick() end)
    
    self._bossModelComponent = self:AddChildComponent('Container_BossInfo/Container_Monster', ActorModelComponent)
    self._bossModelComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 1)
    self:InitBossList()
    self:InitRewardsList()

    self._ticketContainerGO = self:FindChildGO("Container_Ticket")
    self._ticketIconGO = self:FindChildGO("Container_Ticket/Container_Icon")
    local itemGo = GUIManager.AddItem(self._ticketIconGO, 1)
    self._ticketIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._ticketInfoText = self:GetChildComponent("Container_Ticket/Text_Info", "TextMeshWrapper")
    self._ticketTipsText = self:GetChildComponent("Container_Ticket/Text_Tips", "TextMeshWrapper")
    self._ticketCountText = self:GetChildComponent("Container_Ticket/Text_Count", "TextMeshWrapper")
    self._ticketOKButton = self:FindChildGO("Container_Ticket/Button_OK")
    self._csBH:AddClick(self._ticketOKButton, function()self:TicketOKBtnClick() end)
    self._ticketCloseButton = self:FindChildGO("Container_Ticket/Button_Close")
    self._csBH:AddClick(self._ticketCloseButton, function()self:TicketCloseBtnClick() end)
    self._ticketContainerGO:SetActive(false)

    self._onRefreshRedPoint = function() self:RefreshRedPoint() end
end

function PersonalBossContainerView:OnDestroy()

end

function PersonalBossContainerView:ShowView()
    EventDispatcher:AddEventListener(GameEvents.OnPersonalBossRefreshBossInfo, self._onRefreshRedPoint)
    self:ShowBossData()
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleWorldBoss.ChildView.PersonalBossContainerView")
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(true)
    end
    self:ShowModel()
    self:RefreshRedPoint()
end

function PersonalBossContainerView:CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.OnPersonalBossRefreshBossInfo, self._onRefreshRedPoint)
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(false)
    end
    self:HideModel()
end

function PersonalBossContainerView:RefreshRedPoint()
    --local minVipLevel = GlobalParamsHelper.GetParamValue(508)
    --self._fightRedPointImage:SetActive(PersonalBossManager:GetRemainCount()>0 and GameWorld.Player().vip_level >= minVipLevel)
end

function PersonalBossContainerView:CloseVIPTipsBtnClick()
    self._containerVIPTips:SetActive(false)
end

function PersonalBossContainerView:OKVIPTipsBtnClick()
    self._containerVIPTips:SetActive(false)
    GUIManager.ShowPanelByFunctionId(42)
end

function PersonalBossContainerView:TicketOKBtnClick()
    PersonalBossManager:GotoBoss(self._currentSelectedBoss)
    self._ticketContainerGO:SetActive(false)
end

function PersonalBossContainerView:TicketCloseBtnClick()
    self._ticketContainerGO:SetActive(false)
end

function PersonalBossContainerView:ShowTicketView()
    self._ticketContainerGO:SetActive(true)
    self._ticketTipsText.text = LanguageDataHelper.CreateContentWithArgs(56050, {["0"]=1})
    local itemId, needCount = next(GlobalParamsHelper.GetParamValue(504))
    local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    self._ticketIcon:SetItem(itemId)
    self._ticketCountText.text = BaseUtil.GetColorStringByCount(bagCount, needCount)
    self._ticketInfoText.text = LanguageDataHelper.CreateContentWithArgs(56052, {["0"]=needCount})
    local diff = needCount - bagCount
    if diff > 0 then
        local money = GlobalParamsHelper.GetParamValue(505)[itemId] * diff
        self._ticketTipsText.text = LanguageDataHelper.CreateContentWithArgs(56050, {["0"]=money})
    else
        self._ticketTipsText.text = ""
    end
end

function PersonalBossContainerView:ShowBossData()
    self._textRemainCountValue.text = PersonalBossManager:GetRemainCount() .. "/" .. PersonalBossManager:GetMaxCount()
    local bossInfos = PersonalBossManager:GetAllBossInfo()
    local level = GameWorld.Player().level
    local selectedIndex = nil
    for i, v in ipairs(bossInfos) do
        v:SetIsSelected(false)
        if level >= v:GetBossLevel() then
            selectedIndex = i
        end
    end
    for i, v in ipairs(bossInfos) do
        if level < v:GetBossLevel() and level >= v:GetLevelLimit() then
            selectedIndex = i
            break
        end
    end

    selectedIndex = selectedIndex or 1
    local selectedBoss = bossInfos[selectedIndex]
    selectedBoss:SetIsSelected(true)
    self._currentSelectedBoss = selectedBoss
    self:ShowCurrentBoss()
    self._listBoss:SetDataList(bossInfos)
    self._listBoss:SetPositionByNum(selectedIndex)
end

function PersonalBossContainerView:ShowCurrentBoss()
    self:UpdateModel(self._currentSelectedBoss:GetBossModel())

    GameWorld.AddIcon(self._containerBossName, self._currentSelectedBoss:GetBossNameIcon(), nil, nil, nil, nil, true)
    
    self._listRewards:SetDataList(self._currentSelectedBoss:GetBossAllDrops())
end

function PersonalBossContainerView:HelpBtnClick()
    self:ShowTips(LanguageDataHelper.CreateContent(56041), LanguageDataHelper.CreateContent(56040))
end

function PersonalBossContainerView:GuideTireBtnClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_personal_boss_guide_tire_button")
end

function PersonalBossContainerView:FightBtnClick()
    if PersonalBossManager:GetLastEnterTime() == 0 then
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_personal_boss_fight_button")
        PersonalBossManager:GotoBoss(self._currentSelectedBoss)
        GUIManager.ClosePanel(PanelsConfig.WorldBoss)
        return
    end
    if self._currentSelectedBoss then
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_personal_boss_fight_button")
        local levelLimit = self._currentSelectedBoss:GetLevelLimit()
        local minVipLevel = GlobalParamsHelper.GetParamValue(508)
        if GameWorld.Player().vip_level < minVipLevel then
            self._containerVIPTips:SetActive(true)
        else
            if levelLimit > GameWorld.Player().level then
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(56016) .. levelLimit .. LanguageDataHelper.CreateContent(56017) .. self._currentSelectedBoss:GetSenceName(), 1)
            elseif PersonalBossManager:GetRemainCount() <= 0 then
                GUIManager.ShowText(2, LanguageDataHelper.CreateContent(56053))
            else
                self:ShowTicketView()
            end
        end
    end
end

function PersonalBossContainerView:ShowTips(title, text)
    self._containerTips:SetActive(true)
    self._textTipsInfo.text = text
    self._textTipsTitle.text = title
end

function PersonalBossContainerView:CloseTipsBtnClick()
    self._containerTips:SetActive(false)
end

function PersonalBossContainerView:InitBossList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_BossList/ScrollViewList_BossList")
    self._listBoss = list
    self._scrollViewBoss = scrollView
    self._scrollViewBoss:SetHorizontalMove(false)
    self._scrollViewBoss:SetVerticalMove(true)
    self._listBoss:SetItemType(PersonalBossListInfoItem)
    self._listBoss:SetPadding(0, 0, 0, 0)
    self._listBoss:SetGap(0, 0)
    self._listBoss:SetDirection(UIList.DirectionTopToDown, 1, 100)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._listBoss:SetItemClickedCB(itemClickedCB)
end

function PersonalBossContainerView:InitRewardsList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_BossInfo/Container_Rewards/ScrollViewList")
    self._listRewards = list
    self._scrollViewRewards = scrollView
    self._scrollViewRewards:SetHorizontalMove(true)
    self._scrollViewRewards:SetVerticalMove(false)
    self._listRewards:SetItemType(PersonalBossDropRewardItem)
    self._listRewards:SetPadding(0, 0, 0, 0)
    self._listRewards:SetGap(0, 6)
    self._listRewards:SetDirection(UIList.DirectionLeftToRight, 100, 1)
end

function PersonalBossContainerView:OnListItemClicked(idx)
    self._currentSelectedBoss = self._listBoss:GetDataByIndex(idx)
    self:SetListItemSelected(idx)
    self:ShowCurrentBoss()
end

function PersonalBossContainerView:SetListItemSelected(index)
    local length = self._listBoss:GetLength() - 1
    for i = 0, length do
        local item = self._listBoss:GetItem(i)
        item:SetSelected(i == index)
    end
end

function PersonalBossContainerView:UpdateModel(modelId)
    self._bossModelComponent:LoadModel(modelId)
end

function PersonalBossContainerView:ShowModel()
    self._bossModelComponent.gameObject:SetActive(true)
end

function PersonalBossContainerView:HideModel()
    self._bossModelComponent.gameObject:SetActive(false)
end
