-- BackWoodsContainerView.lua
-- 黑暗禁地游戏UI模块
require "Modules.ModuleWorldBoss.ChildComponent.BossHomeListInfoItem"
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
require "PlayerManager/PlayerData/EventData"
require "UIComponent.Extend.ItemGrid"

local BackWoodsContainerView = Class.BackWoodsContainerView(ClassTypes.BaseComponent)

BackWoodsContainerView.interface = GameConfig.ComponentsConfig.Component
BackWoodsContainerView.classPath = "Modules.ModuleWorldBoss.ChildView.BackWoodsContainerView"

local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local BackWoodsManager = PlayerManager.BackWoodsManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local ActorModelComponent = GameMain.ActorModelComponent
local BossHomeListInfoItem = ClassTypes.BossHomeListInfoItem
local TaskRewardItem = ClassTypes.TaskRewardItem
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local BaseUtil = GameUtil.BaseUtil

local MAX_TOGGLE_COUNT = 7

function BackWoodsContainerView:Awake()
    self._selectTab = -1
    self._selectedFloor = 1
    self:InitView()
end

function BackWoodsContainerView:OnDestroy()

end

function BackWoodsContainerView:ShowView(data)
    EventDispatcher:AddEventListener(GameEvents.On_Backwoods_Get_Info, self._onBackwoodsGetInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Backwoods_Get_Killer_Records, self._onBackwoodsGetKillerRecords)
    self:ShowRemainCountInfo()
    self._selectedBossId = data
    if self._selectedBossId ~= nil then
        local floor = BackWoodsManager:GetFloorBossId(self._selectedBossId)
        if floor ~= 0 then
            self._selectedFloor = floor
        end
    end
    BackWoodsManager:OnGetData(self._selectedFloor)
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(true)
    end
    self:ShowModel()
end

function BackWoodsContainerView:CloseView()
    self._selectedBossId = nil
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    EventDispatcher:RemoveEventListener(GameEvents.On_Backwoods_Get_Info, self._onBackwoodsGetInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Backwoods_Get_Killer_Records, self._onBackwoodsGetKillerRecords)
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(false)
    end
    self:HideModel()
end

function BackWoodsContainerView:InitView()
    self._containerBossName = self:FindChildGO("Container_BossInfo/Container_Name")
    self._btnFight = self:FindChildGO("Container_BossInfo/Button_Fight")
    self._btnKillRecord = self:FindChildGO("Container_BossInfo/Button_KillRecord")
    self._btnHelp = self:FindChildGO("Container_BossInfo/Button_Help")
    self._csBH:AddClick(self._btnHelp, function()self:HelpBtnClick() end)
    self._csBH:AddClick(self._btnFight, function()self:FightBtnClick() end)
    self._csBH:AddClick(self._btnKillRecord, function()self:KillRecordBtnClick() end)
    self._toggleIntrest = UIToggle.AddToggle(self.gameObject, "Container_BossInfo/Toggle_Intrest")
    self._toggleIntrest:SetOnValueChangedCB(function(state)self:OnClickIntrest(state) end)
    
    self._btnCloseTips = self:FindChildGO("Container_Tips/Button_Close")
    self._btnCloseBG = self:FindChildGO("Container_Tips/Image_BG")
    self._csBH:AddClick(self._btnCloseTips, function()self:CloseTipsBtnClick() end)
    self._csBH:AddClick(self._btnCloseBG, function()self:CloseTipsBtnClick() end)
    self._containerTips = self:FindChildGO("Container_Tips")
    self._containerTips:SetActive(false)
    self._btnCloseInstrestTips = self:FindChildGO("Container_InstrestTips/Button_OK")
    self._btnCloseInstrestBG = self:FindChildGO("Container_InstrestTips/Image_BG")
    self._csBH:AddClick(self._btnCloseInstrestTips, function()self:CloseInstrestTipsBtnClick() end)
    self._csBH:AddClick(self._btnCloseInstrestBG, function()self:CloseInstrestTipsBtnClick() end)
    self._toggleIntrestTips = UIToggle.AddToggle(self.gameObject, "Container_InstrestTips/Toggle_Intrest")
    self._toggleIntrestTips:SetOnValueChangedCB(function(state)self:OnClickIntrestTips(state) end)
    self._containerInstrestTips = self:FindChildGO("Container_InstrestTips")
    self._containerInstrestTips:SetActive(false)
    self._textTipsInfo = self:GetChildComponent("Container_Tips/Text_Info", "TextMeshWrapper")
    self._textTipsTitle = self:GetChildComponent("Container_Tips/Text_Title", "TextMeshWrapper")
    self._containerTooHighLevel = self:FindChildGO("Container_BossInfo/Container_TooHighLevel")
    self._containerTooHighLevel:SetActive(false)
    
    self._bossModelComponent = self:AddChildComponent('Container_BossInfo/Container_Monster', ActorModelComponent)
    self._bossModelComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 1)
    self:InitBossList()
    self:InitRewardsList()
    self._onBackwoodsGetInfo = function() self:OnBackwoodsGetInfo() end
    self._onBackwoodsGetKillerRecords = function(args) self:OnBackwoodsGetKillerRecords(args) end

    self._remainCountText = self:GetChildComponent("Container_BossInfo/Text_RemainCount", "TextMeshWrapper")

    self._playerCountText = self:GetChildComponent("Text_PlayerCount", "TextMeshWrapper")

    self._ticketContainerGO = self:FindChildGO("Container_Ticket")
    self._ticketIconGO = self:FindChildGO("Container_Ticket/Container_Icon")
    local itemGo = GUIManager.AddItem(self._ticketIconGO, 1)
    self._ticketIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._ticketInfoText = self:GetChildComponent("Container_Ticket/Text_Info", "TextMeshWrapper")
    self._ticketTipsText = self:GetChildComponent("Container_Ticket/Text_Tips", "TextMeshWrapper")
    self._ticketCountText = self:GetChildComponent("Container_Ticket/Text_Count", "TextMeshWrapper")
    self._ticketOKButton = self:FindChildGO("Container_Ticket/Button_OK")
    self._csBH:AddClick(self._ticketOKButton, function()self:TicketOKBtnClick() end)
    self._ticketCloseButton = self:FindChildGO("Container_Ticket/Button_Close")
    self._csBH:AddClick(self._ticketCloseButton, function()self:TicketCloseBtnClick() end)
    self._ticketContainerGO:SetActive(false)

    self:InitToggleGroup()
end

function BackWoodsContainerView:TicketOKBtnClick()
    BackWoodsManager:GotoBoss(self._currentSelectedBoss)
    self._ticketContainerGO:SetActive(false)
end

function BackWoodsContainerView:TicketCloseBtnClick()
    self._ticketContainerGO:SetActive(false)
end

function BackWoodsContainerView:ShowTicketView()
    self._ticketContainerGO:SetActive(true)
    self._ticketTipsText.text = LanguageDataHelper.CreateContentWithArgs(56050, {["0"]=1})
    local count = BackWoodsManager:GetMaxCount() - BackWoodsManager:GetRemainCount()
    local data = GlobalParamsHelper.GetParamValue(558)[count]
    local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(data[2])
    self._ticketIcon:SetItem(data[2])
    self._ticketCountText.text = BaseUtil.GetColorStringByCount(bagCount, data[1])
    self._ticketInfoText.text = LanguageDataHelper.CreateContentWithArgs(56049, {["0"]=data[1]})
    local diff = data[1] - bagCount
    if diff > 0 then
        local money = GlobalParamsHelper.GetParamValue(548)[data[2]] * diff
        self._ticketTipsText.text = LanguageDataHelper.CreateContentWithArgs(56050, {["0"]=money})
    else
        self._ticketTipsText.text = ""
    end
end

function BackWoodsContainerView:InitToggleGroup()
    self.toggleGOList = {}
    for i=1,MAX_TOGGLE_COUNT do
        self.toggleGOList[i] = self:FindChildGO("ToggleGroup_Function/Toggle"..i)
    end
    local showCount = GlobalParamsHelper.GetParamValue(625)
    for i=1,showCount do
        self.toggleGOList[i]:SetActive(true)
    end

    self.toggle = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup_Function")
	self.toggle:SetOnSelectedIndexChangedCB(function(index) self:OnToggleGroupClick(index) end)
    self.toggle:SetSelectIndex(0)
    self:OnToggleGroupClick(0)
end

function BackWoodsContainerView:OnToggleGroupClick(index)
    if self._selectTab == index then
        return
    end
    self._selectTab = index
    self._selectedFloor = index + 1
    BackWoodsManager:OnGetData(self._selectedFloor)
    self:ShowPlayerCount()
end

function BackWoodsContainerView:ShowPlayerCount()
    self._playerCountText.text = LanguageDataHelper.CreateContentWithArgs(56082, {["0"]=BackWoodsManager:GetPlayerCountByFloor(self._selectedFloor)})
end

--黑暗禁地接收到响应数据
function BackWoodsContainerView:OnBackwoodsGetInfo()
    self:ShowBossData()
    self:ShowPlayerCount()
end

function BackWoodsContainerView:OnBackwoodsGetKillerRecords(args)
    local text = ""
    for i, v in ipairs(args) do
        text = text .. DateTimeUtil.GetDateTimeStr(v[1]) .. LanguageDataHelper.CreateContent(56022) .. v[2] .. LanguageDataHelper.CreateContent(56023)
    end
    self:ShowTips(LanguageDataHelper.CreateContent(56024), text)
end

function BackWoodsContainerView:ShowRemainCountInfo()
    local countStr = string.format("%d/%d",BackWoodsManager:GetRemainCount(), BackWoodsManager:GetMaxCount())
    self._remainCountText.text = LanguageDataHelper.CreateContentWithArgs(56051, {["0"]=countStr})
end

--请求boss列表数据
function BackWoodsContainerView:ShowBossData()
    self:ShowRemainCountInfo()
    --根据层级，获取boss数据列表
    local bossInfos = BackWoodsManager:GetAllBossInfoByFloor(self._selectedFloor)
    LoggerHelper.Log("bossInfos:"..bossInfos);
    local level = GameWorld.Player().level
    --设置当前选择boss，显示高亮
    local selectedIndex = nil
    for i, v in ipairs(bossInfos) do
        v:SetIsSelected(false)
        if self._selectedBossId then
            if v:GetId() == self._selectedBossId then
                selectedIndex = i
            end
        else
            if level >= v:GetBossLevel() then
                selectedIndex = i
            end
        end
    end
    selectedIndex = selectedIndex or 1
    local selectedBoss = bossInfos[selectedIndex]
    selectedBoss:SetIsSelected(true)
    self._currentSelectedBoss = selectedBoss
    self:ShowCurrentBoss()
    --把boss数据列表设置给List列表的每条显示
    self._listBoss:SetDataList(bossInfos)
    self._listBoss:SetPositionByNum(selectedIndex)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateBossCountDown() end)
end

function BackWoodsContainerView:ShowCurrentBoss()
    self:UpdateModel(self._currentSelectedBoss:GetBossModel())
    self._toggleIntrest:GetToggleWrapper().isOn = self._currentSelectedBoss:GetIsIntrest()
    GameWorld.AddIcon(self._containerBossName, self._currentSelectedBoss:GetBossNameIcon(), nil, nil, nil, nil, true)
    if GameWorld.Player().level > self._currentSelectedBoss:GetBossLevel() + 100 then
        self._containerTooHighLevel:SetActive(true)
    else
        self._containerTooHighLevel:SetActive(false)
    end
    
    self._listRewards:SetDataList(self._currentSelectedBoss:GetBossDrops())
end

function BackWoodsContainerView:OnClickIntrest(state)
    if self._currentSelectedBoss and self._currentSelectedBoss:GetIsIntrest() ~= state then
        BackWoodsManager:OnBossIntrest(self._currentSelectedBoss:GetId())
        if state and not self._notShowTip then
            self._containerInstrestTips:SetActive(true)
        end
    end
end

function BackWoodsContainerView:OnClickIntrestTips(state)
    self._notShowTip = state
end

function BackWoodsContainerView:FightBtnClick()
    if self._currentSelectedBoss then
        local levelLimit = self._currentSelectedBoss:GetLevelLimit()
        if levelLimit > GameWorld.Player().level then
            GUIManager.ShowText(1, LanguageDataHelper.CreateContent(56016) .. levelLimit .. LanguageDataHelper.CreateContent(56017) .. self._currentSelectedBoss:GetSenceName(), 1)
        elseif BackWoodsManager:GetRemainCount() <= 0 then
            GUIManager.ShowText(2, LanguageDataHelper.CreateContent(56053))
        else
            self:ShowTicketView()
        end
    end
end

function BackWoodsContainerView:KillRecordBtnClick()
    if self._currentSelectedBoss then
        BackWoodsManager:OnBossGetKillerRecords(self._currentSelectedBoss:GetId())
    end
end

function BackWoodsContainerView:ShowTips(title, text)
    self._containerTips:SetActive(true)
    self._textTipsInfo.text = text
    self._textTipsTitle.text = title
end

function BackWoodsContainerView:HelpBtnClick()
    self:ShowTips(LanguageDataHelper.CreateContent(56078), LanguageDataHelper.CreateContent(56075))
end

function BackWoodsContainerView:CloseTipsBtnClick()
    self._containerTips:SetActive(false)
end

function BackWoodsContainerView:CloseInstrestTipsBtnClick()
    self._containerInstrestTips:SetActive(false)
end

--初始化BossList的UI列表
function BackWoodsContainerView:InitBossList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_BossList/ScrollViewList_BossList")
    self._listBoss = list
    self._scrollViewBoss = scrollView
    self._scrollViewBoss:SetHorizontalMove(false)
    self._scrollViewBoss:SetVerticalMove(true)
    self._listBoss:SetItemType(BossHomeListInfoItem)
    self._listBoss:SetPadding(0, 0, 0, 0)
    self._listBoss:SetGap(0, 0)
    self._listBoss:SetDirection(UIList.DirectionTopToDown, 1, 100)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._listBoss:SetItemClickedCB(itemClickedCB)
end

function BackWoodsContainerView:InitRewardsList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_BossInfo/Container_Rewards/ScrollViewList")
    self._listRewards = list
    self._scrollViewRewards = scrollView
    self._scrollViewRewards:SetHorizontalMove(true)
    self._scrollViewRewards:SetVerticalMove(false)
    self._listRewards:SetItemType(TaskRewardItem)
    self._listRewards:SetPadding(0, 0, 0, 0)
    self._listRewards:SetGap(0, 6)
    self._listRewards:SetDirection(UIList.DirectionLeftToRight, 100, 1)
end

function BackWoodsContainerView:OnListItemClicked(idx)
    self._currentSelectedBoss = self._listBoss:GetDataByIndex(idx)
    self:SetListItemSelected(idx)
    self:ShowCurrentBoss()
end

function BackWoodsContainerView:UpdateBossCountDown()
    local length = self._listBoss:GetLength() - 1
    for i = 0, length do
        -- local data = self._listBoss:GetDataByIndex(i)
        local item = self._listBoss:GetItem(i)
        item:UpdateCountDown()
    end
end

function BackWoodsContainerView:SetListItemSelected(index)
    local length = self._listBoss:GetLength() - 1
    for i = 0, length do
        local item = self._listBoss:GetItem(i)
        item:SetSelected(i == index)
    end
end

function BackWoodsContainerView:UpdateModel(modelId)
    self._bossModelComponent:LoadModel(modelId)
end

function BackWoodsContainerView:ShowModel()
    self._bossModelComponent.gameObject:SetActive(true)
end

function BackWoodsContainerView:HideModel()
    self._bossModelComponent.gameObject:SetActive(false)
end