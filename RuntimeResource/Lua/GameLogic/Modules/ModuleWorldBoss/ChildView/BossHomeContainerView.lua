-- BossHomeContainerView.lua

require "Modules.ModuleWorldBoss.ChildComponent.BossHomeListInfoItem"
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
require "PlayerManager/PlayerData/EventData"

local BossHomeContainerView = Class.BossHomeContainerView(ClassTypes.BaseComponent)

BossHomeContainerView.interface = GameConfig.ComponentsConfig.Component
BossHomeContainerView.classPath = "Modules.ModuleWorldBoss.ChildView.BossHomeContainerView"

local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local BossHomeManager = PlayerManager.BossHomeManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local ActorModelComponent = GameMain.ActorModelComponent
local BossHomeListInfoItem = ClassTypes.BossHomeListInfoItem
local TaskRewardItem = ClassTypes.TaskRewardItem

local MAX_TOGGLE_COUNT = 7

function BossHomeContainerView:Awake()
    self._selectTab = -1
    self._selectedFloor = 1
    self:InitView()
end

function BossHomeContainerView:OnDestroy()

end

function BossHomeContainerView:ShowView(data)
    EventDispatcher:AddEventListener(GameEvents.On_Boss_Home_Get_Info, self._onBossHomeGetInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Boss_Home_Get_Killer_Records, self._onBossHomeGetKillerRecords)
    self._selectedBossId = data
    if self._selectedBossId ~= nil then
        local floor = BossHomeManager:GetFloorBossId(self._selectedBossId)
        if floor ~= 0 then
            self._selectedFloor = floor
        end
    end
    BossHomeManager:OnGetData(self._selectedFloor)
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(true)
    end
    self:ShowModel()
end

function BossHomeContainerView:CloseView()
    self._selectedBossId = nil
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    EventDispatcher:RemoveEventListener(GameEvents.On_Boss_Home_Get_Info, self._onBossHomeGetInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Boss_Home_Get_Killer_Records, self._onBossHomeGetKillerRecords)
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(false)
    end
    self:HideModel()
end

function BossHomeContainerView:InitView()
    self._containerBossName = self:FindChildGO("Container_BossInfo/Container_Name")
    self._btnFight = self:FindChildGO("Container_BossInfo/Button_Fight")
    self._btnKillRecord = self:FindChildGO("Container_BossInfo/Button_KillRecord")
    self._csBH:AddClick(self._btnFight, function()self:FightBtnClick() end)
    self._csBH:AddClick(self._btnKillRecord, function()self:KillRecordBtnClick() end)
    self._toggleIntrest = UIToggle.AddToggle(self.gameObject, "Container_BossInfo/Toggle_Intrest")
    self._toggleIntrest:SetOnValueChangedCB(function(state)self:OnClickIntrest(state) end)
    self._btnFightText = self:GetChildComponent("Container_BossInfo/Button_Fight/Text", "TextMeshWrapper")

    self._btnHelp = self:FindChildGO("Container_BossInfo/Button_Help")
    self._csBH:AddClick(self._btnHelp, function()self:HelpBtnClick() end)

    self._btnCloseVIPTips = self:FindChildGO("Container_VIPTips/Button_Close")
    self._csBH:AddClick(self._btnCloseVIPTips, function()self:CloseVIPTipsBtnClick() end)
    self._btnOKVIPTips = self:FindChildGO("Container_VIPTips/Button_OK")
    self._csBH:AddClick(self._btnOKVIPTips, function()self:OKVIPTipsBtnClick() end)
    self._containerVIPTips = self:FindChildGO("Container_VIPTips")
    self._containerVIPTips:SetActive(false)
    
    self._btnCloseTips = self:FindChildGO("Container_Tips/Button_Close")
    self._btnCloseBG = self:FindChildGO("Container_Tips/Image_BG")
    self._csBH:AddClick(self._btnCloseTips, function()self:CloseTipsBtnClick() end)
    self._csBH:AddClick(self._btnCloseBG, function()self:CloseTipsBtnClick() end)
    self._containerTips = self:FindChildGO("Container_Tips")
    self._containerTips:SetActive(false)
    self._btnCloseInstrestTips = self:FindChildGO("Container_InstrestTips/Button_OK")
    self._btnCloseInstrestBG = self:FindChildGO("Container_InstrestTips/Image_BG")
    self._csBH:AddClick(self._btnCloseInstrestTips, function()self:CloseInstrestTipsBtnClick() end)
    self._csBH:AddClick(self._btnCloseInstrestBG, function()self:CloseInstrestTipsBtnClick() end)
    self._toggleIntrestTips = UIToggle.AddToggle(self.gameObject, "Container_InstrestTips/Toggle_Intrest")
    self._toggleIntrestTips:SetOnValueChangedCB(function(state)self:OnClickIntrestTips(state) end)
    self._containerInstrestTips = self:FindChildGO("Container_InstrestTips")
    self._containerInstrestTips:SetActive(false)
    self._textTipsInfo = self:GetChildComponent("Container_Tips/Text_Info", "TextMeshWrapper")
    self._textTipsTitle = self:GetChildComponent("Container_Tips/Text_Title", "TextMeshWrapper")
    self._containerTooHighLevel = self:FindChildGO("Container_BossInfo/Container_TooHighLevel")
    self._containerTooHighLevel:SetActive(false)
    
    self._bossModelComponent = self:AddChildComponent('Container_BossInfo/Container_Monster', ActorModelComponent)
    self._bossModelComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 1)
    self:InitBossList()
    self:InitRewardsList()
    self._onBossHomeGetInfo = function() self:OnBossHomeGetInfo() end
    self._onBossHomeGetKillerRecords = function(args) self:OnBossHomeGetKillerRecords(args) end

    self:InitToggleGroup()
end

function BossHomeContainerView:InitToggleGroup()
    self.toggleGOList = {}
    for i=1,MAX_TOGGLE_COUNT do
        self.toggleGOList[i] = self:FindChildGO("ToggleGroup_Function/Toggle"..i)
    end
    local showCount = GlobalParamsHelper.GetParamValue(624)
    for i=1,showCount do
        self.toggleGOList[i]:SetActive(true)
    end

    self.toggle = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup_Function")
	self.toggle:SetOnSelectedIndexChangedCB(function(index) self:OnToggleGroupClick(index) end)
    self.toggle:SetSelectIndex(0)
    self:OnToggleGroupClick(0)
end

function BossHomeContainerView:OnToggleGroupClick(index)
    if self._selectTab == index then
        return
    end
    self._selectTab = index
    self._selectedFloor = index + 1
    BossHomeManager:OnGetData(self._selectedFloor)
    self:RefreshFightButtonText()
end

function BossHomeContainerView:RefreshFightButtonText()
    if self._selectedFloor == 1 then
        self._btnFightText.text = LanguageDataHelper.CreateContent(56076)
    else 
        self._btnFightText.text = LanguageDataHelper.CreateContent(56077)
    end
end

function BossHomeContainerView:OnBossHomeGetInfo()
    self:ShowBossData()
end

function BossHomeContainerView:OnBossHomeGetKillerRecords(args)
    local text = ""
    for i, v in ipairs(args) do
        text = text .. DateTimeUtil.GetDateTimeStr(v[1]) .. LanguageDataHelper.CreateContent(56022) .. v[2] .. LanguageDataHelper.CreateContent(56023)
    end
    self:ShowTips(LanguageDataHelper.CreateContent(56024), text)
end

function BossHomeContainerView:ShowBossData()
    local bossInfos = BossHomeManager:GetAllBossInfoByFloor(self._selectedFloor)
    local level = GameWorld.Player().level
    local selectedIndex = nil
    for i, v in ipairs(bossInfos) do
        v:SetIsSelected(false)
        if self._selectedBossId then
            if v:GetId() == self._selectedBossId then
                selectedIndex = i
            end
        else
            if level >= v:GetBossLevel() then
                selectedIndex = i
            end
        end
    end
    
    selectedIndex = selectedIndex or 1
    local selectedBoss = bossInfos[selectedIndex]
    selectedBoss:SetIsSelected(true)
    self._currentSelectedBoss = selectedBoss
    self:ShowCurrentBoss()
    self._listBoss:SetDataList(bossInfos)
    self._listBoss:SetPositionByNum(selectedIndex)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateBossCountDown() end)
end

function BossHomeContainerView:ShowCurrentBoss()
    self:UpdateModel(self._currentSelectedBoss:GetBossModel())
    self._toggleIntrest:GetToggleWrapper().isOn = self._currentSelectedBoss:GetIsIntrest()
    GameWorld.AddIcon(self._containerBossName, self._currentSelectedBoss:GetBossNameIcon(), nil, nil, nil, nil, true)
    if GameWorld.Player().level > self._currentSelectedBoss:GetBossLevel() + 100 then
        self._containerTooHighLevel:SetActive(true)
    else
        self._containerTooHighLevel:SetActive(false)
    end
    
    self._listRewards:SetDataList(self._currentSelectedBoss:GetBossDrops())
end

function BossHomeContainerView:CloseVIPTipsBtnClick()
    self._containerVIPTips:SetActive(false)
end

function BossHomeContainerView:OKVIPTipsBtnClick()
    self._containerVIPTips:SetActive(false)
    GUIManager.ShowPanelByFunctionId(42)
end

function BossHomeContainerView:OnClickIntrest(state)
    if self._currentSelectedBoss and self._currentSelectedBoss:GetIsIntrest() ~= state then
        BossHomeManager:OnBossIntrest(self._currentSelectedBoss:GetId())
        if state and not self._notShowTip then
            self._containerInstrestTips:SetActive(true)
        end
    end
end

function BossHomeContainerView:OnClickIntrestTips(state)
    self._notShowTip = state
end

function BossHomeContainerView:HelpBtnClick()
    self:ShowTips(LanguageDataHelper.CreateContent(56086), LanguageDataHelper.CreateContent(56085))
end

function BossHomeContainerView:FightBtnClick()
    if self._currentSelectedBoss then
        local levelLimit = self._currentSelectedBoss:GetLevelLimit()
        local vipLevelLimit = self._currentSelectedBoss:GetVIPLevelLimit()
        local minVipLevel = GlobalParamsHelper.GetParamValue(494)
        if GameWorld.Player().vip_level < vipLevelLimit then
            if GameWorld.Player().vip_level < minVipLevel then
                self._containerVIPTips:SetActive(true)
            else
                GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(56038, {["0"] = self._currentSelectedBoss:GetVIPLevelMoney()}), 
                function() BossHomeManager:GotoBoss(self._currentSelectedBoss) end, nil)
            end
        elseif levelLimit > GameWorld.Player().level then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(56016) .. levelLimit .. LanguageDataHelper.CreateContent(56017) .. self._currentSelectedBoss:GetSenceName(), 1)
        else
            BossHomeManager:GotoBoss(self._currentSelectedBoss)
        end
    end
end

function BossHomeContainerView:KillRecordBtnClick()
    if self._currentSelectedBoss then
        BossHomeManager:OnBossGetKillerRecords(self._currentSelectedBoss:GetId())
    end
end

function BossHomeContainerView:ShowTips(title, text)
    self._containerTips:SetActive(true)
    self._textTipsInfo.text = text
    self._textTipsTitle.text = title
end

function BossHomeContainerView:CloseTipsBtnClick()
    self._containerTips:SetActive(false)
end

function BossHomeContainerView:CloseInstrestTipsBtnClick()
    self._containerInstrestTips:SetActive(false)
end

function BossHomeContainerView:InitBossList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_BossList/ScrollViewList_BossList")
    self._listBoss = list
    self._scrollViewBoss = scrollView
    self._scrollViewBoss:SetHorizontalMove(false)
    self._scrollViewBoss:SetVerticalMove(true)
    self._listBoss:SetItemType(BossHomeListInfoItem)
    self._listBoss:SetPadding(0, 0, 0, 0)
    self._listBoss:SetGap(0, 0)
    self._listBoss:SetDirection(UIList.DirectionTopToDown, 1, 100)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._listBoss:SetItemClickedCB(itemClickedCB)
end

function BossHomeContainerView:InitRewardsList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_BossInfo/Container_Rewards/ScrollViewList")
    self._listRewards = list
    self._scrollViewRewards = scrollView
    self._scrollViewRewards:SetHorizontalMove(true)
    self._scrollViewRewards:SetVerticalMove(false)
    self._listRewards:SetItemType(TaskRewardItem)
    self._listRewards:SetPadding(0, 0, 0, 0)
    self._listRewards:SetGap(0, 6)
    self._listRewards:SetDirection(UIList.DirectionLeftToRight, 100, 1)
end

function BossHomeContainerView:OnListItemClicked(idx)
    self._currentSelectedBoss = self._listBoss:GetDataByIndex(idx)
    self:SetListItemSelected(idx)
    self:ShowCurrentBoss()
end

function BossHomeContainerView:UpdateBossCountDown()
    local length = self._listBoss:GetLength() - 1
    for i = 0, length do
        -- local data = self._listBoss:GetDataByIndex(i)
        local item = self._listBoss:GetItem(i)
        item:UpdateCountDown()
    end
end

function BossHomeContainerView:SetListItemSelected(index)
    local length = self._listBoss:GetLength() - 1
    for i = 0, length do
        local item = self._listBoss:GetItem(i)
        item:SetSelected(i == index)
    end
end

function BossHomeContainerView:UpdateModel(modelId)
    self._bossModelComponent:LoadModel(modelId)
end

function BossHomeContainerView:ShowModel()
    self._bossModelComponent.gameObject:SetActive(true)
end

function BossHomeContainerView:HideModel()
    self._bossModelComponent.gameObject:SetActive(false)
end