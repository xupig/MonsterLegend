-- WorldBossDropRecordContainerView.lua
local WorldBossDropRecordContainerView = Class.WorldBossDropRecordContainerView(ClassTypes.BaseComponent)
require "Modules.ModuleWorldBoss.ChildComponent.WorldBossDropRecordListInfoItem"
local WorldBossDropRecordListInfoItem = ClassTypes.WorldBossDropRecordListInfoItem
WorldBossDropRecordContainerView.interface = GameConfig.ComponentsConfig.Component
WorldBossDropRecordContainerView.classPath = "Modules.ModuleWorldBoss.ChildView.WorldBossDropRecordContainerView"
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local DropRecordManager = PlayerManager.DropRecordManager

function WorldBossDropRecordContainerView:Awake()
    self:InitList()
    self._onDropRecordGetLocal = function(args)self:OnDropRecordGetLocal(args) end
    self._containerNone = self:FindChildGO("Container_None")
end

function WorldBossDropRecordContainerView:OnDestroy()

end

function WorldBossDropRecordContainerView:ShowView()
    EventDispatcher:AddEventListener(GameEvents.OnDropRecordGetLocal, self._onDropRecordGetLocal)
    DropRecordManager:GetLocal()
    if self._scrollView then
        self._scrollView:SetScrollRectState(true)
    end
end

function WorldBossDropRecordContainerView:CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.OnDropRecordGetLocal, self._onDropRecordGetLocal)
    if self._scrollView then
        self._scrollView:SetScrollRectState(false)
    end
end

function WorldBossDropRecordContainerView:OnDropRecordGetLocal(args)
    self._list:SetDataList(args)
    self._containerNone:SetActive(#args == 0)
end

function WorldBossDropRecordContainerView:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._scrollView:SetHorizontalMove(false)
    self._scrollView:SetVerticalMove(true)
    self._list:SetItemType(WorldBossDropRecordListInfoItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 100)
end

function WorldBossDropRecordContainerView:ShowModel()
end

function WorldBossDropRecordContainerView:HideModel()
end
