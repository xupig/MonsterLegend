-- WorldBossContainerView.lua
local WorldBossContainerView = Class.WorldBossContainerView(ClassTypes.BaseComponent)
require "Modules.ModuleWorldBoss.ChildComponent.WorldBossListInfoItem"
local WorldBossListInfoItem = ClassTypes.WorldBossListInfoItem
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local TaskRewardItem = ClassTypes.TaskRewardItem
require "PlayerManager/PlayerData/EventData"
WorldBossContainerView.interface = GameConfig.ComponentsConfig.Component
WorldBossContainerView.classPath = "Modules.ModuleWorldBoss.ChildView.WorldBossContainerView"
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local WorldBossManager = PlayerManager.WorldBossManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local ActorModelComponent = GameMain.ActorModelComponent
local StringStyleUtil = GameUtil.StringStyleUtil

function WorldBossContainerView:Awake()
    self._textBossTireValue = self:GetChildComponent("Container_BossInfo/Text_BossTireValue", "TextMeshWrapper")
    self._containerBossName = self:FindChildGO("Container_BossInfo/Container_Name")
    self._btnHelp = self:FindChildGO("Container_BossInfo/Button_Help")
    self._btnFight = self:FindChildGO("Container_BossInfo/Button_Fight")
    self._btnKillRecord = self:FindChildGO("Container_BossInfo/Button_KillRecord")
    self._csBH:AddClick(self._btnHelp, function()self:HelpBtnClick() end)
    self._csBH:AddClick(self._btnFight, function()self:FightBtnClick() end)
    self._csBH:AddClick(self._btnKillRecord, function()self:KillRecordBtnClick() end)
    self._toggleIntrest = UIToggle.AddToggle(self.gameObject, "Container_BossInfo/Toggle_Intrest")
    self._toggleIntrest:SetOnValueChangedCB(function(state)self:OnClickIntrest(state) end)

    self._fightRedPointImage = self:FindChildGO("Container_BossInfo/Button_Fight/Image_RedPoint")
    
    self._btnGuideTire = self:FindChildGO("Container_BossInfo/Button_GuideTire")
    self._csBH:AddClick(self._btnGuideTire, function()self:GuideTireBtnClick() end)
    
    self._btnCloseTips = self:FindChildGO("Container_Tips/Button_Close")
    self._btnCloseBG = self:FindChildGO("Container_Tips/Image_BG")
    self._csBH:AddClick(self._btnCloseTips, function()self:CloseTipsBtnClick() end)
    self._csBH:AddClick(self._btnCloseBG, function()self:CloseTipsBtnClick() end)
    self._containerTips = self:FindChildGO("Container_Tips")
    self._containerTips:SetActive(false)
    self._btnCloseInstrestTips = self:FindChildGO("Container_InstrestTips/Button_OK")
    self._btnCloseInstrestBG = self:FindChildGO("Container_InstrestTips/Image_BG")
    self._csBH:AddClick(self._btnCloseInstrestTips, function()self:CloseInstrestTipsBtnClick() end)
    self._csBH:AddClick(self._btnCloseInstrestBG, function()self:CloseInstrestTipsBtnClick() end)
    self._toggleIntrestTips = UIToggle.AddToggle(self.gameObject, "Container_InstrestTips/Toggle_Intrest")
    self._toggleIntrestTips:SetOnValueChangedCB(function(state)self:OnClickIntrestTips(state) end)
    self._containerInstrestTips = self:FindChildGO("Container_InstrestTips")
    self._containerInstrestTips:SetActive(false)
    self._textTipsInfo = self:GetChildComponent("Container_Tips/Text_Info", "TextMeshWrapper")
    self._textTipsTitle = self:GetChildComponent("Container_Tips/Text_Title", "TextMeshWrapper")
    self._containerTooHighLevel = self:FindChildGO("Container_BossInfo/Container_TooHighLevel")
    self._containerTooHighLevel:SetActive(false)
    
    self._bossModelComponent = self:AddChildComponent('Container_BossInfo/Container_Monster', ActorModelComponent)
    self._bossModelComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 1)
    self:InitBossList()
    self:InitRewardsList()
    self._onWorldBossGetInfo = function()self:OnWorldBossGetInfo() end
    self._onWorldBossGetKillerRecords = function(args)self:OnWorldBossGetKillerRecords(args) end
    self._onRefreshRedPoint = function() self:RefreshRedPoint() end
end

function WorldBossContainerView:OnDestroy()

end

function WorldBossContainerView:ShowView(args)
    EventDispatcher:AddEventListener(GameEvents.OnWorldBossGetInfo, self._onWorldBossGetInfo)
    EventDispatcher:AddEventListener(GameEvents.OnWorldBossGetKillerRecords, self._onWorldBossGetKillerRecords)
    EventDispatcher:AddEventListener(GameEvents.OnWorldBossRefreshTireFlag, self._onRefreshRedPoint)
    -- self:ShowBossData()
    self._selectedBossId = args
    WorldBossManager:OnGetData()
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(true)
    end
    self:ShowModel()
    self:RefreshRedPoint()
end

function WorldBossContainerView:CloseView()
    self._selectedBossId = nil
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossGetInfo, self._onWorldBossGetInfo)
    EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossGetKillerRecords, self._onWorldBossGetKillerRecords)
    EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossRefreshTireFlag, self._onRefreshRedPoint)
    if self._scrollViewBoss then
        self._scrollViewBoss:SetScrollRectState(false)
    end
    self:HideModel()
end

function WorldBossContainerView:ShowModel()
    self._bossModelComponent.gameObject:SetActive(true)
end

function WorldBossContainerView:HideModel()
    self._bossModelComponent.gameObject:SetActive(false)
end

function WorldBossContainerView:RefreshRedPoint()
    --self._fightRedPointImage:SetActive(GameWorld.Player().tire_flag == 0)
end

function WorldBossContainerView:OnWorldBossGetInfo()
    self:ShowBossData()
end

function WorldBossContainerView:OnWorldBossGetKillerRecords(args)
    local text = ""
    for i, v in ipairs(args) do
        text = text .. DateTimeUtil.GetDateTimeStr(v[1]) .. LanguageDataHelper.CreateContent(56022) .. v[2] .. LanguageDataHelper.CreateContent(56023)
    end
    self:ShowTips(LanguageDataHelper.CreateContent(56024), text)
end

function WorldBossContainerView:ShowBossData()
    self._textBossTireValue.text = WorldBossManager:GetBossTire() .. "/" .. GlobalParamsHelper.GetParamValue(439)--become_tire_flag_max_value_i
    local bossInfos = WorldBossManager:GetAllBossInfo()
    local level = GameWorld.Player().level
    local selectedIndex = nil
    for i, v in ipairs(bossInfos) do
        v:SetIsSelected(false)
        if self._selectedBossId then
            if v:GetId() == self._selectedBossId then
                selectedIndex = i
            end
        else
            if level >= v:GetBossLevel() then
                selectedIndex = i
            end
        end
    end
    
    selectedIndex = selectedIndex or 1
    local selectedBoss = bossInfos[selectedIndex]
    selectedBoss:SetIsSelected(true)
    self._currentSelectedBoss = selectedBoss
    -- self:OnListItemClicked(selectedIndex)
    self:ShowCurrentBoss()
    self._listBoss:SetDataList(bossInfos)
    self._listBoss:SetPositionByNum(selectedIndex)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateBossCountDown() end)
end

function WorldBossContainerView:ShowCurrentBoss()
    self:UpdateModel(self._currentSelectedBoss:GetBossModel())
    self._toggleIntrest:GetToggleWrapper().isOn = self._currentSelectedBoss:GetIsIntrest()
    GameWorld.AddIcon(self._containerBossName, self._currentSelectedBoss:GetBossNameIcon(), nil, nil, nil, nil, true)
    if GameWorld.Player().level > self._currentSelectedBoss:GetBossLevel() + 100 then
        self._containerTooHighLevel:SetActive(true)
    else
        self._containerTooHighLevel:SetActive(false)
    end
    
    self._listRewards:SetDataList(self._currentSelectedBoss:GetBossDrops())
end

function WorldBossContainerView:OnClickIntrest(state)
    -- LoggerHelper.Log("Ash: OnClickIntrest: " .. tostring(state))
    if self._currentSelectedBoss and self._currentSelectedBoss:GetIsIntrest() ~= state then
        WorldBossManager:OnBossIntrest(self._currentSelectedBoss:GetId())
        if state and not self._notShowTip then
            self._containerInstrestTips:SetActive(true)
        end
    end
end

function WorldBossContainerView:OnClickIntrestTips(state)
    -- LoggerHelper.Log("Ash: OnClickIntrestTips: " .. tostring(state))
    self._notShowTip = state
end

function WorldBossContainerView:HelpBtnClick()
    self:ShowTips(LanguageDataHelper.CreateContent(56014), LanguageDataHelper.CreateContent(56015))
end

function WorldBossContainerView:GuideTireBtnClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_world_boss_guide_tire_button")
end

function WorldBossContainerView:FightBtnClick()
    --世界boss指引特殊处理
    if GameManager.DramaManager:IsRunning("drama_guide", "__WorldBoss") then
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_world_boss_fight_button")
        PlayerManager.TaskCommonManager:RealStop()
        PlayerManager.InstanceManager:OnInstanceEnter(28)
        GUIManager.ClosePanel(PanelsConfig.WorldBoss)
        return
    end
    
    if self._currentSelectedBoss then
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_world_boss_fight_button")
        local levelLimit = self._currentSelectedBoss:GetLevelLimit()
        if levelLimit > GameWorld.Player().level then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(56016) .. StringStyleUtil.GetLevelString(levelLimit)
                .. LanguageDataHelper.CreateContent(56017) .. self._currentSelectedBoss:GetSenceName(), 1)
        else
            WorldBossManager:GotoBoss(self._currentSelectedBoss)
        end
    end
end

function WorldBossContainerView:KillRecordBtnClick()
    if self._currentSelectedBoss then
        WorldBossManager:OnBossGetKillerRecords(self._currentSelectedBoss:GetId())
    end
end

function WorldBossContainerView:ShowTips(title, text)
    self._containerTips:SetActive(true)
    self._textTipsInfo.text = text
    self._textTipsTitle.text = title
end

function WorldBossContainerView:CloseTipsBtnClick()
    self._containerTips:SetActive(false)
end

function WorldBossContainerView:CloseInstrestTipsBtnClick()
    self._containerInstrestTips:SetActive(false)
end

function WorldBossContainerView:InitBossList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_BossList/ScrollViewList_BossList")
    self._listBoss = list
    self._scrollViewBoss = scrollView
    self._scrollViewBoss:SetHorizontalMove(false)
    self._scrollViewBoss:SetVerticalMove(true)
    self._listBoss:SetItemType(WorldBossListInfoItem)
    self._listBoss:SetPadding(0, 0, 0, 0)
    self._listBoss:SetGap(0, 0)
    self._listBoss:SetDirection(UIList.DirectionTopToDown, 1, 100)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._listBoss:SetItemClickedCB(itemClickedCB)
end

function WorldBossContainerView:InitRewardsList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_BossInfo/Container_Rewards/ScrollViewList")
    self._listRewards = list
    self._scrollViewRewards = scrollView
    self._scrollViewRewards:SetHorizontalMove(true)
    self._scrollViewRewards:SetVerticalMove(false)
    self._listRewards:SetItemType(TaskRewardItem)
    self._listRewards:SetPadding(0, 0, 0, 0)
    self._listRewards:SetGap(0, 6)
    self._listRewards:SetDirection(UIList.DirectionLeftToRight, 100, 1)
end

function WorldBossContainerView:OnListItemClicked(idx)
    -- local _text = self._listData[idx + 1]
    -- LoggerHelper.Log("OnListItemClicked:" .. idx .. PrintTable:TableToStr(self._listBoss:GetDataByIndex(idx)))
    self._currentSelectedBoss = self._listBoss:GetDataByIndex(idx)
    self:SetListItemSelected(idx)
    self:ShowCurrentBoss()
end

function WorldBossContainerView:UpdateBossCountDown()
    local length = self._listBoss:GetLength() - 1
    for i = 0, length do
        -- local data = self._listBoss:GetDataByIndex(i)
        local item = self._listBoss:GetItem(i)
        item:UpdateCountDown()
    end
end

function WorldBossContainerView:SetListItemSelected(index)
    local length = self._listBoss:GetLength() - 1
    for i = 0, length do
        local item = self._listBoss:GetItem(i)
        item:SetSelected(i == index)
    end
end

function WorldBossContainerView:UpdateModel(modelId)
    self._bossModelComponent:LoadModel(modelId)
end
