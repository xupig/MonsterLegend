-- WorldBossPanel.lua
local WorldBossPanel = Class.WorldBossPanel(ClassTypes.BasePanel)

require "Modules.ModuleWorldBoss.ChildView.WorldBossContainerView"
require "Modules.ModuleWorldBoss.ChildView.BossHomeContainerView"
require "Modules.ModuleWorldBoss.ChildView.PersonalBossContainerView"
require "Modules.ModuleWorldBoss.ChildView.BackWoodsContainerView"
require "Modules.ModuleWorldBoss.ChildView.WorldBossDropRecordContainerView"

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local BossPanelType = GameConfig.EnumType.BossPanelType
local WorldBossManager = PlayerManager.WorldBossManager
local WorldBossContainerView = ClassTypes.WorldBossContainerView
local BossHomeContainerView = ClassTypes.BossHomeContainerView
local PersonalBossContainerView = ClassTypes.PersonalBossContainerView
local BackWoodsContainerView = ClassTypes.BackWoodsContainerView
local WorldBossDropRecordContainerView = ClassTypes.WorldBossDropRecordContainerView

function WorldBossPanel:Awake()
    self:InitComps()
end

function WorldBossPanel:OnShow(data)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleWorldBoss.WorldBossPanel")
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
end

function WorldBossPanel:OnClose()
end

function WorldBossPanel:OnDestroy()

end

function WorldBossPanel:InitComps()
    self:InitView("Container_WorldBoss", WorldBossContainerView, BossPanelType.WorldBoss)
    self:InitView("Container_BossHome", BossHomeContainerView, BossPanelType.BossHome)
    self:InitView("Container_PersonalBoss", PersonalBossContainerView, BossPanelType.PersonalBoss)
    self:InitView("Container_BackWoods", BackWoodsContainerView, BossPanelType.BackWoods)
    self:InitView("Container_DropRecord", WorldBossDropRecordContainerView, BossPanelType.DropRecord)

    self._bgIconObj = self:FindChildGO("Container_BgIcon")
    GameWorld.AddIcon(self._bgIconObj,13547)
end

function WorldBossPanel:StructingViewInit()
    return true
end

function WorldBossPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--基于结构化View初始化方式，隐藏View使用Scale的方式处理
-- function WorldBossPanel:ScaleToHide()
--     return true
-- end

function WorldBossPanel:BaseShowModel()
    self._selectedView:ShowModel()
end

function WorldBossPanel:BaseHideModel()
    self._selectedView:HideModel()
end