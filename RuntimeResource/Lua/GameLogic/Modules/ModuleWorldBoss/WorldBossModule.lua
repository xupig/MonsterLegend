-- WorldBossModule.lua
WorldBossModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function WorldBossModule.Init()
    GUIManager.AddPanel(PanelsConfig.WorldBoss, "Panel_WorldBoss", GUILayer.LayerUIPanel, "Modules.ModuleWorldBoss.WorldBossPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return WorldBossModule
