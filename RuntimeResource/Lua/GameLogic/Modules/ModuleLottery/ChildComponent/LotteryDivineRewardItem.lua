local LotteryDivineRewardItem = Class.LotteryDivineRewardItem(ClassTypes.BaseLuaUIComponent)

LotteryDivineRewardItem.interface = GameConfig.ComponentsConfig.Component
LotteryDivineRewardItem.classPath = "Modules.ModuleLottery.ChildComponent.LotteryDivineRewardItem"

local ColorDataHelper = GameDataHelper.ColorDataHelper
local DivineDataHelper = GameDataHelper.DivineDataHelper
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function LotteryDivineRewardItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function LotteryDivineRewardItem:OnDestroy() 

end

function LotteryDivineRewardItem:InitView()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._countText = self:GetChildComponent("Text_Count", "TextMeshWrapper")
    self._typeIcon = self:FindChildGO("Image_TypeIcon")
    self._itemManager = ItemManager()
    self._icon = self._itemManager:GetLuaUIComponent(nil, self._iconContainer)
end

--type 1:保底奖励 2:普通奖励
function LotteryDivineRewardItem:SetData(data, type)
    --LoggerHelper.Log(data)
    self.data = data
    if self.data.type == 1 then
        self._typeIcon:SetActive(true)
    else
        self._typeIcon:SetActive(false)
    end

    self._countText.text = self.data.count
    self._icon:SetItem(self.data.item)
end

function LotteryDivineRewardItem:OnSelected(state)

end