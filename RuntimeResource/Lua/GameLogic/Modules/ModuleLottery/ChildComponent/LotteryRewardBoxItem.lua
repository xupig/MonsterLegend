require "UIComponent.Extend.PointableButton"
require "Modules.ModuleLottery.ChildComponent.LotteryWeekCountRewardItem"

local LotteryRewardBoxItem = Class.LotteryRewardBoxItem(ClassTypes.UIListItem)

LotteryRewardBoxItem.interface = GameConfig.ComponentsConfig.Component
LotteryRewardBoxItem.classPath = "Modules.ModuleLottery.ChildComponent.LotteryRewardBoxItem"

local ColorDataHelper = GameDataHelper.ColorDataHelper
local DivineDataHelper = GameDataHelper.DivineDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DivineManager = PlayerManager.DivineManager
local RewardBoxStateType = GameConfig.EnumType.RewardBoxStateType
local PointableButton = ClassTypes.PointableButton
local LotteryWeekCountRewardItem = ClassTypes.LotteryWeekCountRewardItem
local UIComponentUtil = GameUtil.UIComponentUtil

function LotteryRewardBoxItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self.state = 0
    self:InitView()
end
--override
function LotteryRewardBoxItem:OnDestroy() 

end

function LotteryRewardBoxItem:InitView()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._countText = self:GetChildComponent("Text_Count", "TextMeshWrapper")

    self._pointableButton = self:AddChildLuaUIComponent("Container_Icon", PointableButton)
    self._pointableButton:SetOnPointerDownFunc(function() self:OnPointerDown() end)
    self._pointableButton:SetOnPointerUpFunc(function() self:OnPointerUp() end)

    self._notOpenImage = self:FindChildGO("Container_Icon/Image_NotOpen")
    self._canOpenImage = self:FindChildGO("Container_Icon/Image_CanOpen")
    self._openedImage = self:FindChildGO("Container_Icon/Image_Opened")

    self._typeToOpenGo = {[1]=self._notOpenImage, [2]=self._canOpenImage, [3]=self._openedImage}

    self._rewardPreViewPos = self:FindChildGO("Container_RewardPos")
    self._rectTransform = self._rewardPreViewPos:GetComponent(typeof(UnityEngine.RectTransform))

end

function LotteryRewardBoxItem:OnRefreshData(data)
    --LoggerHelper.Log(data)
    self.data = data
    self._countText.text = string.format("%d%s", self.data.order, LanguageDataHelper.GetContent(916))
    self:RefreshState()
end

function LotteryRewardBoxItem:RefreshState()
    local state = DivineManager:GetRewardState(self.data.order)
    if state ~= self.state then
        if self._typeToOpenGo[self.state] ~= nil then
            self._typeToOpenGo[self.state]:SetActive(false)
        end
        if self._typeToOpenGo[state] ~= nil then
            self._typeToOpenGo[state]:SetActive(true)
        end
        self.state = state
    end
end

function LotteryRewardBoxItem:OnPointerDown()
    self:RefreshState()
    if self.state == RewardBoxStateType.NotGet then
        local data = {}
        data.isShow = true
        data.pos = self._rectTransform.position
        data.itemData = {[self.data.itemId] = self.data.count}
        EventDispatcher:TriggerEvent(GameEvents.Show_Divine_Reward_Preview, data)
    elseif self.state == RewardBoxStateType.CanGet then
        --点击领奖请求
        DivineManager:DivineGetRewardReq(self.data.order)
    end
end

function LotteryRewardBoxItem:OnPointerUp()
    if self.state == RewardBoxStateType.NotGet then
        local data = {}
        data.isShow = false
        EventDispatcher:TriggerEvent(GameEvents.Show_Divine_Reward_Preview, data)
    end
end