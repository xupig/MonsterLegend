local LotteryWeekCountRewardItem = Class.LotteryWeekCountRewardItem(ClassTypes.BaseLuaUIComponent)

LotteryWeekCountRewardItem.interface = GameConfig.ComponentsConfig.Component
LotteryWeekCountRewardItem.classPath = "Modules.ModuleLottery.ChildComponent.LotteryWeekCountRewardItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function LotteryWeekCountRewardItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function LotteryWeekCountRewardItem:OnDestroy() 

end

function LotteryWeekCountRewardItem:InitView()
    self._textCount = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemManager = ItemManager()
    self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
end

function LotteryWeekCountRewardItem:SetData(itemId, count)
    self._textCount.text = LanguageDataHelper.CreateContent(901) .. count
    self._icon:SetItem(itemId)
end
