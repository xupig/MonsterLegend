local LotteryRewardResultItem = Class.LotteryRewardResultItem(ClassTypes.BaseLuaUIComponent)

LotteryRewardResultItem.interface = GameConfig.ComponentsConfig.Component
LotteryRewardResultItem.classPath = "Modules.ModuleLottery.ChildComponent.LotteryRewardResultItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local DivineDataHelper = GameDataHelper.DivineDataHelper

function LotteryRewardResultItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function LotteryRewardResultItem:OnDestroy() 

end

function LotteryRewardResultItem:InitView()
    self._textCount = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemManager = ItemManager()
    self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
end

function LotteryRewardResultItem:SetData(id)
    local data = DivineDataHelper:GetDivineRewardCfgData(id)
    self._textCount.text = LanguageDataHelper.CreateContent(901) .. data.count
    self._icon:SetItem(data.item)
end
