require "UIComponent.Extend.CurrencyCom"
require "Modules.ModuleLottery.ChildComponent.LotteryRewardBoxItem"
require "Modules.ModuleLottery.ChildComponent.LotteryDivineRewardItem"
require "Modules.ModuleLottery.ChildView.LotteryRewardResultView"
require "Modules.ModuleLottery.ChildView.LotteryRewardPreviewView"

local LotteryPanel = Class.LotteryPanel(ClassTypes.BasePanel)

local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local CurrencyCom = ClassTypes.CurrencyCom
local LotteryRewardBoxItem = ClassTypes.LotteryRewardBoxItem
local UIList = ClassTypes.UIList
local LotteryDivineRewardItem = ClassTypes.LotteryDivineRewardItem
local DivineDataHelper = GameDataHelper.DivineDataHelper
local DivineManager = PlayerManager.DivineManager
local divineData = PlayerManager.PlayerDataManager.divineData
local LotteryRewardResultView = ClassTypes.LotteryRewardResultView
local LotteryRewardPreviewView = ClassTypes.LotteryRewardPreviewView
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local UIDial = ClassTypes.UIDial

local Divine_Item_Count = 8

--override
function LotteryPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self:InitView()
    self:AddEventListeners()
    self:InitMoney()
end

--override
function LotteryPanel:OnDestroy()
	self._csBH = nil
    self:RemoveEventListeners()
end

function LotteryPanel:AddEventListeners()
    self._refreshDivineStarPower = function() self:RefreshDivineStarPower() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Divine_Star_Power, self._refreshDivineStarPower)
    self._refreshDivineWeekCount = function() self:RefreshDivineWeekCount() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Divine_Week_Count, self._refreshDivineWeekCount)
    self._refreshDivineWeekRewardData = function() self:RefreshDivineWeekRewardData() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Divine_Week_Reward_Data, self._refreshDivineWeekRewardData)
    self._showDivineRewardResult = function(data) self:ShowDivineRewardResult(data) end
    EventDispatcher:AddEventListener(GameEvents.Show_Divine_Reward_Result, self._showDivineRewardResult)
    self._showDivineRewardPreview = function(data) self:ShowDivineRewardPreview(data) end
    EventDispatcher:AddEventListener(GameEvents.Show_Divine_Reward_Preview, self._showDivineRewardPreview)
end

function LotteryPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Divine_Star_Power, self._refreshDivineStarPower)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Divine_Week_Count, self._refreshDivineWeekCount)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Divine_Week_Reward_Data, self._refreshDivineWeekRewardData)
    EventDispatcher:RemoveEventListener(GameEvents.Show_Divine_Reward_Result, self._showDivineRewardResult)
    EventDispatcher:RemoveEventListener(GameEvents.Show_Divine_Reward_Preview, self._showDivineRewardPreview)
end

function LotteryPanel:InitView()
    self._backButton = self:FindChildGO("Button_Back")
    self._csBH:AddClick(self._backButton,function () self:OnBackButtonClick() end)
    self._infoButton = self:FindChildGO("Button_Info")
    self._csBH:AddClick(self._infoButton,function () self:OnInfoButtonClick() end)
    self._normalLotteryButton = self:FindChildGO("Button_NormalLottery")
    self._csBH:AddClick(self._normalLotteryButton,function () self:OnNormalLotteryButtonClick() end)
    self._advancedLotteryButton = self:FindChildGO("Button_AdvancedLottery")
    self._csBH:AddClick(self._advancedLotteryButton,function () self:OnAdvancedLotteryButtonClick() end)
    self._advancedTimesText = self:FindChildGO("Button_AdvancedLottery/Text"):GetComponent("TextMeshWrapper")

    self._normalCostIcon = self:FindChildGO("Container_NormalCost/Container_Icon")
    self._normalCostText = self:FindChildGO("Container_NormalCost/Text_Count"):GetComponent("TextMeshWrapper")
    self._advancedCostIcon = self:FindChildGO("Container_AdvancedCost/Container_Icon")
    self._advancedCostText = self:FindChildGO("Container_AdvancedCost/Text_Count"):GetComponent("TextMeshWrapper")

    for itemId,count in pairs(DivineDataHelper:DivineCost(1)) do
        local iconId = ItemDataHelper.GetIcon(itemId)
        GameWorld.AddIcon(self._normalCostIcon, iconId)
        self._normalCostText.text = count
    end
    for itemId,count in pairs(DivineDataHelper:DivineWholesale(1)) do
        local iconId = ItemDataHelper.GetIcon(itemId)
        GameWorld.AddIcon(self._advancedCostIcon, iconId)
        self._advancedCostText.text = count
    end

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_RewardBoxs/ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(LotteryRewardBoxItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self._lotteryRewardPreviewGo = self:FindChildGO("Container_RewardBoxs/Container_RewardInfo")
    self._lotteryRewardPreviewView = self:AddChildLuaUIComponent("Container_RewardBoxs/Container_RewardInfo", LotteryRewardPreviewView)
    self._rectTransform = self._lotteryRewardPreviewGo:GetComponent(typeof(UnityEngine.RectTransform))
    self._rectTransform.localPosition = self.HIDE_POSITION

    local divineRewardListData = DivineDataHelper:GetDivineRewardListData()
    self._divineRewardItems = {}
    for i=1,Divine_Item_Count do
        self._divineRewardItems[i] = self:AddChildLuaUIComponent("Container_Dial/Container_Items/item"..i, LotteryDivineRewardItem)
        local type = 1
        if i == 1 then
            type = 2
        end
        self._divineRewardItems[i]:SetData(divineRewardListData[i], type)
    end

    self._sandglassCountText = self:GetChildComponent("Container_Sandglass/Text_Title/Text_Count", "TextMeshWrapper")
    self._LotteryInfoCountText = self:GetChildComponent("Text_LotteryInfo/Text_Count", "TextMeshWrapper")
    self._LotteryResetInfoText = self:GetChildComponent("Text_ResetInfo", "TextMeshWrapper")

    self._lotteryRewardResultGO = self:FindChildGO("Container_RewardResult")
    self._lotteryRewardResultView = self:AddChildLuaUIComponent("Container_RewardResult", LotteryRewardResultView)

    self._dialComp = UIDial.AddDial(self.gameObject,"Container_Dial")
    local dialEndCB = function() self:OnEndRoundTurnCB() end
    self._dialComp:SetEndRoundTurnCB(dialEndCB)
end

function LotteryPanel:InitMoney()
	self._currency = self:AddChildLuaUIComponent("Container_Currency", CurrencyCom)
end

function LotteryPanel:OnShow()
    self._currency:OnShow()
    self:RefreshDivineStarPower()
    self:RefreshDivineWeekCount()
    self:RefreshDivineResetInfoText()
end

function LotteryPanel:OnBackButtonClick()
    GUIManager.ClosePanel(GameConfig.PanelsConfig.Lottery)
end

function LotteryPanel:OnInfoButtonClick()
    
end

function LotteryPanel:OnNormalLotteryButtonClick()
    DivineManager:DivineOneReq()
end

function LotteryPanel:OnAdvancedLotteryButtonClick()
    DivineManager:DivineFiveReq()
end

function LotteryPanel:RefreshDivineStarPower()
    self._sandglassCountText.text = divineData.divineStarPower
end

function LotteryPanel:RefreshDivineWeekCount()
    self._LotteryInfoCountText.text = divineData.divineWeekCount..LanguageDataHelper.GetContent(916)
    local data = DivineDataHelper:DivineGrand(1)
    self._list:SetDataList(data)
end

function LotteryPanel:RefreshDivineWeekRewardData()
    local data = DivineDataHelper:DivineGrand(1)
    self._list:SetDataList(data)
end

function LotteryPanel:ShowDivineRewardResult(data)
    --self._lotteryRewardResultGO:SetActive(true)
    --self._lotteryRewardResultView:SetData(data)
    self._rewardResultData = data
    self._dialComp:StartTurn(data)
end

function LotteryPanel:ShowDivineRewardPreview(data)
    if data.isShow then
        self._rectTransform.position = data.pos
        self._lotteryRewardPreviewView:SetData(data.itemData)
    else
        self._rectTransform.localPosition = self.HIDE_POSITION
    end
end

function LotteryPanel:RefreshDivineResetInfoText()
    local leaveSeconds = DateTimeUtil.GetToNextMondayFiveOclockSec()
    local date = DateTimeUtil.FormatParseTime(leaveSeconds)
    self._LotteryResetInfoText.text = date..LanguageDataHelper.GetContent(52034)
end

function LotteryPanel:OnEndRoundTurnCB()
    self._lotteryRewardResultGO:SetActive(true)
    self._lotteryRewardResultView:SetData(self._rewardResultData)
end