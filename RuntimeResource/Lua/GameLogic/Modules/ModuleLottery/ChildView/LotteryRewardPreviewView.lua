require "Modules.ModuleLottery.ChildComponent.LotteryWeekCountRewardItem"

local LotteryRewardPreviewView = Class.LotteryRewardPreviewView(ClassTypes.BaseLuaUIComponent)

LotteryRewardPreviewView.interface = GameConfig.ComponentsConfig.Component
LotteryRewardPreviewView.classPath = "Modules.ModuleLottery.ChildView.LotteryRewardPreviewView"

local UIList = ClassTypes.UIList
local LotteryWeekCountRewardItem = ClassTypes.LotteryWeekCountRewardItem
local UIComponentUtil = GameUtil.UIComponentUtil
local DivineManager = PlayerManager.DivineManager
local DivineDataHelper = GameDataHelper.DivineDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function LotteryRewardPreviewView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self._itemPool = {}
    self._items = {}
end

function LotteryRewardPreviewView:OnDestroy()
    self._itemPool = nil
    self._items = nil
end

function LotteryRewardPreviewView:InitView()
    self._rewardTemp = self:FindChildGO("Container_Reward")
    self._rewardTemp:SetActive(false)
end

function LotteryRewardPreviewView:SetData(data)
    self:RemoveAll()
    for i, v in pairs(data) do
        self:AddItem(i,v)
    end
end

function LotteryRewardPreviewView:CreateItem()
    local itemPool = self._itemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Reward")
    local item = UIComponentUtil.AddLuaUIComponent(go, LotteryWeekCountRewardItem)
    return item
end

function LotteryRewardPreviewView:AddItem(itemId, count)
    local item = self:CreateItem()
    item.gameObject:SetActive(true)
    table.insert(self._items, item)
    item:SetData(itemId, count)
end

function LotteryRewardPreviewView:RemoveAll()
    for k, item in pairs(self._items) do
        item.gameObject:SetActive(false)
        self:ReleaseItem(item)
    end
    self._items = {}
end

function LotteryRewardPreviewView:ReleaseItem(item)
    table.insert(self._itemPool, item)
end