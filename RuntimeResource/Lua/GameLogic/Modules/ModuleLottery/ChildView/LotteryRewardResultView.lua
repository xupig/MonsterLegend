require "Modules.ModuleLottery.ChildComponent.LotteryRewardResultItem"

local LotteryRewardResultView = Class.LotteryRewardResultView(ClassTypes.BaseLuaUIComponent)

LotteryRewardResultView.interface = GameConfig.ComponentsConfig.Component
LotteryRewardResultView.classPath = "Modules.ModuleLottery.ChildView.LotteryRewardResultView"

local UIList = ClassTypes.UIList
local LotteryRewardResultItem = ClassTypes.LotteryRewardResultItem
local UIComponentUtil = GameUtil.UIComponentUtil
local DivineManager = PlayerManager.DivineManager
local DivineDataHelper = GameDataHelper.DivineDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function LotteryRewardResultView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self._itemPool = {}
    self._items = {}
end

function LotteryRewardResultView:OnDestroy()
    self._itemPool = nil
    self._items = nil
end

function LotteryRewardResultView:InitView()
    self._rewardTemp = self:FindChildGO("Container_RewardInfo/Container_Reward")
    self._rewardTemp:SetActive(false)

    self._costIcon = self:FindChildGO("Container_Cost/Container_Icon")
    self._costText = self:FindChildGO("Container_Cost/Text_Count"):GetComponent("TextMeshWrapper")
    self._lotteryButton = self:FindChildGO("Button_Lottery")
    self._csBH:AddClick(self._lotteryButton,function () self:OnLotteryButtonClick() end)
    self._lotteryButtonText = self:FindChildGO("Button_Lottery/Text"):GetComponent("TextMeshWrapper")
    self._okButton = self:FindChildGO("Button_OK")
    self._csBH:AddClick(self._okButton,function () self:OnOKButtonClick() end)
end

function LotteryRewardResultView:OnLotteryButtonClick(item)
    self.gameObject:SetActive(false)
    if self.type == 1 then
        DivineManager:DivineFiveReq()
    else
        DivineManager:DivineOneReq()
    end
end

function LotteryRewardResultView:OnOKButtonClick(item)
    self.gameObject:SetActive(false)
end

function LotteryRewardResultView:SetData(data)
    if #data > 1 then
        self.type = 1
        self._lotteryButtonText.text = LanguageDataHelper.GetContent(52033)
        for itemId,count in pairs(DivineDataHelper:DivineWholesale(1)) do
            local iconId = ItemDataHelper.GetIcon(itemId)
            GameWorld.AddIcon(self._costIcon, iconId)
            self._costText.text = count
        end
    else
        self.type = 2
        self._lotteryButtonText.text = LanguageDataHelper.GetContent(52032)
        for itemId,count in pairs(DivineDataHelper:DivineCost(1)) do
            local iconId = ItemDataHelper.GetIcon(itemId)
            GameWorld.AddIcon(self._costIcon, iconId)
            self._costText.text = count
        end
    end
    self:RemoveAll()
    for i, v in pairs(data) do
        self:AddItem(v)
    end
end

function LotteryRewardResultView:CreateItem()
    local itemPool = self._itemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_RewardInfo/Container_Reward", "Container_RewardInfo")
    local item = UIComponentUtil.AddLuaUIComponent(go, LotteryRewardResultItem)
    return item
end

function LotteryRewardResultView:AddItem(id)
    local item = self:CreateItem()
    item.gameObject:SetActive(true)
    table.insert(self._items, item)
    item:SetData(id)
end

function LotteryRewardResultView:RemoveAll()
    for k, item in pairs(self._items) do
        item.gameObject:SetActive(false)
        self:ReleaseItem(item)
    end
    self._items = {}
end

function LotteryRewardResultView:ReleaseItem(item)
    table.insert(self._itemPool, item)
end