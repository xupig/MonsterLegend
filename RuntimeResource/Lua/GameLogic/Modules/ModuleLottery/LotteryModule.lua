LotteryModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function LotteryModule.Init()
	GUIManager.AddPanel(PanelsConfig.Lottery,"Panel_Lottery",GUILayer.LayerUIPanel,"Modules.ModuleLottery.LotteryPanel",true,false)
end

return LotteryModule