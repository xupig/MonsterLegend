-- FactionMemberInfoListItem.lua
--据点PVP列表排名Item基类
local UIListItem = ClassTypes.UIListItem
local FactionMemberInfoListItem = Class.FactionMemberInfoListItem(UIListItem)

FactionMemberInfoListItem.interface = GameConfig.ComponentsConfig.PointableComponent
FactionMemberInfoListItem.classPath = "Modules.ModuleActivity.ChildComponent.FactionMemberInfoListItem"

function FactionMemberInfoListItem:Awake()
	UIListItem.Awake(self)

	self._txtPlayerName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
	self._txtPlayerLevel = self:GetChildComponent("Text_Level", "TextMeshWrapper")

	self._containerHead = self:FindChildGO("Container_Head")
	self._imgLeaderIcon = self:FindChildGO("Image_LeaderIcon")

	self._hpComp = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar")
    self._hpComp:SetProgress(1)
    self._hpComp:ShowTweenAnimate(true)
    self._hpComp:SetMutipleTween(false)
    self._hpComp:SetIsResetToZero(false)
end

--override
function FactionMemberInfoListItem:OnRefreshData(data)
	self._data = data
	if data then
		self:SetActive(true)
		self._txtPlayerName.text = data.name
		self._txtPlayerLevel.text = data.level
	else
		self:SetActive(false)
	end
end


function FactionMemberInfoListItem:UpdateHP()
	if self._data then
		self._hpComp:SetProgressByValue(self._data:GetHP(),self._data)
	end
end

function FactionMemberInfoListItem:UpdateLevel()
	if self._data then
		self._txtPlayerLevel.text = tostring(self._data.level)
	end
end