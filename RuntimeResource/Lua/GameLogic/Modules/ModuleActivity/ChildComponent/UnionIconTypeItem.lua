-- UnionIconTypeItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

local public_config = GameWorld.public_config
local UnionIconTypeItem = Class.UnionIconTypeItem(ClassTypes.UIListItem)
UnionIconTypeItem.interface = GameConfig.ComponentsConfig.Component
UnionIconTypeItem.classPath = "Modules.ModuleActivity.ChildComponent.UnionIconTypeItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local EventDataHelper = GameDataHelper.EventDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local UIParticle = ClassTypes.UIParticle
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local UIList = ClassTypes.UIList
local TipsManager = GameManager.TipsManager

function UnionIconTypeItem:Awake()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._icon = ItemManager():GetLuaUIComponent(nil, self._iconContainer)

    self._onClick = function(pointerEventData)self:OnPointerUp(pointerEventData) end
    self._icon:SetPointerUpCB(self._onClick)
end

function UnionIconTypeItem:OnDestroy()

end

--override
function UnionIconTypeItem:OnPointerDown(pointerEventData)
end

--override
function UnionIconTypeItem:OnPointerUp(pointerEventData)
    -- LoggerHelper.Log("CollectIconTypeItem:OnPointerUp========== ..")
    if self._itemId then
        TipsManager:ShowItemTipsById(self._itemId)
    end 
end

function UnionIconTypeItem:SetPointerUpCB(func)
end


function UnionIconTypeItem:InitItem()


end

function UnionIconTypeItem:AddFriendClick()

end

function UnionIconTypeItem:InitView()

end



function UnionIconTypeItem:OnRefreshData(data)
    if data then
        self._itemId = data[1]
        self._icon:SetItem(data[1], data[2], 0, true)
    end
end


