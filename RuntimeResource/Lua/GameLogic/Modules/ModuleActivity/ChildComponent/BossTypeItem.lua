-- BossTypeItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

local public_config = GameWorld.public_config
local BossTypeItem = Class.BossTypeItem(ClassTypes.UIListItem)
BossTypeItem.interface = GameConfig.ComponentsConfig.Component
BossTypeItem.classPath = "Modules.ModuleActivity.ChildComponent.BossTypeItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local EventDataHelper = GameDataHelper.EventDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper


function BossTypeItem:Awake()
    self._bossIcon = self:FindChildGO("Container_Icon")
    self._txtLevel = self:GetChildComponent("Text_Level", 'TextMeshWrapper')
    self._alreadyGo = self:FindChildGO("Container_Already_Get")
    self._alreadyGo:SetActive(false)
end

function BossTypeItem:OnDestroy()

end

function BossTypeItem:InitItem()


    
 


end

function BossTypeItem:AddFriendClick()

end

function BossTypeItem:InitView()

end



function BossTypeItem:OnRefreshData(data)
    if data then
        -- local itemId = data[1]
        -- local num = data[2]

         local monsterId = EventDataHelper:GetConds(data[1],public_config.EVENT_PARAM_MONSTER_ID)   
         GameWorld.AddIcon(self._bossIcon,MonsterDataHelper.GetIcon(monsterId))
         local level = MonsterDataHelper.GetLv(monsterId)
         self._txtLevel.text = level.."级"
        --  LoggerHelper.Log("BossTypeItem:OnRefreshData(data)"..MonsterDataHelper.GetIcon(monsterId))

        if data[2] == 0 then
            self._alreadyGo:SetActive(false)
        elseif data[2] == 1 then
            self._alreadyGo:SetActive(true)
        end

    end
end


