-- StrongholdRankListItem.lua
--据点PVP列表排名Item基类
local UIListItem = ClassTypes.UIListItem
local StrongholdRankListItem = Class.StrongholdRankListItem(UIListItem)
local public_config = require("ServerConfig/public_config")

StrongholdRankListItem.interface = GameConfig.ComponentsConfig.PointableComponent
StrongholdRankListItem.classPath = "Modules.ModuleActivity.ChildComponent.StrongholdRankListItem"
local RoleDataHelper =  GameDataHelper.RoleDataHelper

function StrongholdRankListItem:Awake()
	UIListItem.Awake(self)
	self._txtRank = self:GetChildComponent("Text_Rank", "TextMeshWrapper")
	self._txtVocation = self:GetChildComponent("Text_Vocation", "TextMeshWrapper")
	self._txtName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
	self._txtLevel = self:GetChildComponent("Text_Level", "TextMeshWrapper")
	self._txtKill = self:GetChildComponent("Text_Kill", "TextMeshWrapper")
	self._txtDead = self:GetChildComponent("Text_Dead", "TextMeshWrapper")
	self._txtContribute = self:GetChildComponent("Text_Contribute", "TextMeshWrapper")
end

--override
function StrongholdRankListItem:OnRefreshData(data)
	self._data = data
	if data then
		self._txtVocation.text = RoleDataHelper.GetRoleNameTextByVocation(data:GetVocation())
		self._txtName.text = data:GetName()
		self._txtLevel.text = "Lv"..data:GetLevel()
		self._txtKill.text = data:GetKill()
		self._txtDead.text = data:GetDeath()
		self._txtContribute.text = data:GetContribution()
	end
end