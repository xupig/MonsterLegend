-- PerfectLoveItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local PerfectLoveItem = Class.PerfectLoveItem(ClassTypes.UIListItem)
PerfectLoveItem.interface = GameConfig.ComponentsConfig.Component
PerfectLoveItem.classPath = "Modules.ModuleActivity.ChildComponent.PerfectLoveItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil


function PerfectLoveItem:Awake()
    self._imgOne = self:FindChildGO("Image_Rank_One")
    self._imgTwo = self:FindChildGO("Image_Rank_Two")
    self._imgThree = self:FindChildGO("Image_Rank_Three")
    self._imgOther = self:FindChildGO("Image_Rank_Other")
    
    self._nameText = self:GetChildComponent("Text_Name", 'TextMeshWrapper')
    self._rankText = self:GetChildComponent("Text_Index", 'TextMeshWrapper')
end

function PerfectLoveItem:OnDestroy()

end



function PerfectLoveItem:OnRefreshData(data)
    if data then

        self._nameText.text = data
        --self._data = data
        local rank = self:GetIndex() + 1
        self._rankText.text = rank
        self._rankText.gameObject:SetActive(false)
        if rank == 1 then
            self._imgOne:SetActive(true)
            self._imgTwo:SetActive(false)
            self._imgThree:SetActive(false)
            self._imgOther:SetActive(false)
        elseif  rank  == 2 then
         self._imgOne:SetActive(false)
         self._imgTwo:SetActive(true)
         self._imgThree:SetActive(false)
         self._imgOther:SetActive(false)
        elseif  rank  == 3 then
        self._imgOne:SetActive(false)
        self._imgTwo:SetActive(false)
        self._imgThree:SetActive(true)
        self._imgOther:SetActive(false)
        elseif  rank  >=3 then
        self._rankText.gameObject:SetActive(true)
        self._imgOne:SetActive(false)
        self._imgTwo:SetActive(false)
        self._imgThree:SetActive(false)
        self._imgOther:SetActive(true)
        end
        
    end
end
