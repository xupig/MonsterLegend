-- CollectIconTypeItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

local public_config = GameWorld.public_config
local CollectIconTypeItem = Class.CollectIconTypeItem(ClassTypes.UIListItem)
CollectIconTypeItem.interface = GameConfig.ComponentsConfig.Component
CollectIconTypeItem.classPath = "Modules.ModuleActivity.ChildComponent.CollectIconTypeItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local EventDataHelper = GameDataHelper.EventDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local UIParticle = ClassTypes.UIParticle
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local UIList = ClassTypes.UIList
local TipsManager = GameManager.TipsManager
local BagData = PlayerManager.PlayerDataManager.bagData
local StringStyleUtil = GameUtil.StringStyleUtil

function CollectIconTypeItem:Awake()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._imgSuo = self:FindChildGO("Image_Suo")
    self._imgBg = self:FindChildGO("itembg")    
    self._txtNum = self:GetChildComponent("Text_Num", 'TextMeshWrapper')
    self._icon = ItemManager():GetLuaUIComponent(nil, self._iconContainer)
    self._imgEquql = self:FindChildGO("Container_Equal")
    self._imgAdd = self:FindChildGO("Container_Add")
    self._imgAdd:SetActive(false)
    self._onClick = function(pointerEventData)self:OnPointerUp(pointerEventData) end
    self._icon:SetPointerUpCB(self._onClick)
end

function CollectIconTypeItem:OnDestroy()

end

function CollectIconTypeItem:InitItem()


end

function CollectIconTypeItem:AddFriendClick()

end

function CollectIconTypeItem:InitView()

end


--override
function CollectIconTypeItem:OnPointerDown(pointerEventData)
end

--override
function CollectIconTypeItem:OnPointerUp(pointerEventData)
    -- LoggerHelper.Log("CollectIconTypeItem:OnPointerUp========== ..")
    if self._itemId and self._itemId~=-1 then
        if self._itemType == 2 then
        TipsManager:ShowItemTipsById(self._itemId)
        end
    end 
end

function CollectIconTypeItem:SetPointerUpCB(func)
end


function CollectIconTypeItem:OnRefreshData(data)
    if data then
        -- LoggerHelper.Log("CollectIconTypeItem:OnRefreshData========== .." ..PrintTable:TableToStr(data))
        self._itemId = data._id 
        self._itemType = data._type 
        if data._id == -1 then
            self._imgEquql:SetActive(true)
            self._txtNum.text = ""
            self._imgSuo:SetActive(false)
            self._imgBg:SetActive(false)
        else
            self._imgEquql:SetActive(false)
            local bagCount = BagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._itemId)
            self._icon:SetItem(data._id, data._num, 0, true)
            if data._type == 1 then
                if data._num > bagCount then
                    self._txtNum.text =StringStyleUtil.GetColorStringWithTextMesh(bagCount.."/"..data._num,StringStyleUtil.red)
                else
                    self._txtNum.text =StringStyleUtil.GetColorStringWithTextMesh(bagCount.."/"..data._num,StringStyleUtil.green)
                end
                
                self._imgBg:SetActive(true)
                self._imgSuo:SetActive(false)
            elseif data._type == 2 then
                self._txtNum.text = ""
                self._imgBg:SetActive(false)
                self._imgSuo:SetActive(true)
            end
        end

    end
end


