-- StrongholdStateItem.lua
-- 据点状态item
local StrongholdStateItem = Class.StrongholdStateItem(ClassTypes.BaseLuaUIComponent)
StrongholdStateItem.interface = GameConfig.ComponentsConfig.Component
StrongholdStateItem.classPath = "Modules.ModuleActivity.ChildComponent.StrongholdStateItem"

local UIProgressBar = ClassTypes.UIProgressBar
local Color = UnityEngine.Color
local red = Color(240/255, 39/255 , 76/255 ,1)
local blue = Color(0,0,1,1)

function StrongholdStateItem:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self._iconContainer = self:FindChildGO("Container_Icon")
	self._pb = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar")
	self._pb:SetProgress(0)
    self._pb:ShowTweenAnimate(false)
    self._pb:SetMutipleTween(false)
    self._pbBarImg = self:GetChildComponent("ProgressBar/Image_Bar", "ImageWrapper")
    self._txtFaction = self:GetChildComponent("Text_Faction","TextMeshWrapper")
end

function StrongholdStateItem:UpdateData(progress,faction)
	self._pb:SetProgress(progress)

	self._txtFaction.text = faction
end

function StrongholdStateItem:UpdateProcess(processFaction)
	if processFaction == 1 then
		self._pbBarImg.color = red
	else
		self._pbBarImg.color = blue
	end
end

