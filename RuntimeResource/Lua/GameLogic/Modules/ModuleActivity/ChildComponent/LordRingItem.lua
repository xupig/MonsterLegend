-- LordRingItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local LordRingItem = Class.LordRingItem(ClassTypes.UIListItem)
LordRingItem.interface = GameConfig.ComponentsConfig.Component
LordRingItem.classPath = "Modules.ModuleActivity.ChildComponent.LordRingItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local LordRingTotalDataHelper = GameDataHelper.LordRingTotalDataHelper
local LordRingDetailDataHelper = GameDataHelper.LordRingDetailDataHelper
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local UIParticle = ClassTypes.UIParticle
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local StringStyleUtil = GameUtil.StringStyleUtil
local TipsManager = GameManager.TipsManager
local XImageFlowLight = GameMain.XImageFlowLight
local TimerHeap = GameWorld.TimerHeap

function LordRingItem:Awake()
    self:InitItem()
end

function LordRingItem:OnDestroy()

end

function LordRingItem:InitItem()
    self._txtWearDesc = self:GetChildComponent("Text_Wear_Desc", 'TextMeshWrapper')

    self._goButtonFlowLight = self:AddChildComponent("Container_State/Button_Get_Reward", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Container_State/Button_Get_Reward")
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)



    self._iconRewardContainer = self:FindChildGO("Container_Icon_Reward")
    self._iconReward = ItemManager():GetLuaUIComponent(nil, self._iconRewardContainer)

    self._iconContainer = self:FindChildGO("Container_Icon_Wear")
    self._imgRed = self:FindChildGO("Container_State/Button_Get_Reward/Image_Red")
    
    --self._iconWear = ItemManager():GetLuaUIComponent(nil, self._iconContainer)

    self._notCompleteImgGo = self:FindChildGO("Container_State/Image_No_Complete")
    self._notCompleteImgGo:SetActive(false)
    self._alreadyGetImgGo = self:FindChildGO("Container_State/Image_Already_Get")
    self._alreadyGetImgGo:SetActive(false)
    self:SetButtonActive(false)
    --self._getRewardButton:SetActive(false)

    self._onClick = function(pointerEventData)self:OnPointerUp(pointerEventData) end
    self._iconReward:SetPointerUpCB(self._onClick)
end

function LordRingItem:AddFriendClick()

end

function LordRingItem:SetButtonActive(flag)
    self._getRewardButton:SetActive(flag)    
    self._goButtonFlowLight.enabled = flag
end

--override
function LordRingItem:OnPointerDown(pointerEventData)
end

--override
function LordRingItem:OnPointerUp(pointerEventData)
    if self._id then
        local reward = LordRingDetailDataHelper:GetReward(self._id)
        TipsManager:ShowItemTipsById(reward[1][1])
    end
end

function LordRingItem:SetPointerUpCB(func)
end



function LordRingItem:OnDestroy()
    self._iconContainer = nil
    self._iconWear = nil

    self._iconRewardContainer = nil
    self._iconReward = nil

    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function LordRingItem:OnGetRewardClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_service_activity_reward_button")
    if self._id then
        ServiceActivityManager:SendGetTargetRewardReq(self._id)
    end
end


function LordRingItem:OnRefreshData(id)
    if id then
        self._id = id
        self._notCompleteImgGo:SetActive(true)
        self._alreadyGetImgGo:SetActive(false)
        --self._getRewardButton:SetActive(false)
        self:SetButtonActive(false)
        self._imgRed:SetActive(false)
        local targets = LordRingDetailDataHelper:GetTargets(id)
        local argsTable = LanguageDataHelper.GetArgsTable()
        if ServiceActivityData:GetCurOpenType()==1 then
            argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(targets[1][2], StringStyleUtil.green)
        else
            argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh("0/1", StringStyleUtil.red)
        end
        self._txtWearDesc.text = LanguageDataHelper.CreateContent(LordRingDetailDataHelper:GetName(id), argsTable)
        --self._txtWearDesc.text = LanguageDataHelper.CreateContent(LordRingDetailDataHelper:GetName(id))
        local iconList = LordRingDetailDataHelper:GetIcon(id)
        --self._iconWear:SetItem(, nil)     
        GameWorld.AddIcon( self._iconContainer,iconList[1])
        local reward = LordRingDetailDataHelper:GetReward(id)
        self._iconReward:SetItem(reward[1][1], reward[1][2], 0, true)

        local progressTb = ServiceActivityData:GetProgressTable()
        --LoggerHelper.Log("LordRingItem:GetProgressTable[1]"..PrintTable:TableToStr(progressTb))
        for k,v in pairs(progressTb) do
            if k == id then
                if v[2] == 1 then
                    self._notCompleteImgGo:SetActive(true)
                    self._alreadyGetImgGo:SetActive(false)
                    self:SetButtonActive(false)
                    --self._getRewardButton:SetActive(false)
                    self._imgRed:SetActive(false)
                elseif  v[2] == 2 then
                    self._notCompleteImgGo:SetActive(false)
                    self._alreadyGetImgGo:SetActive(false)
                    --self._getRewardButton:SetActive(true)
                    self:SetButtonActive(true)
                    --EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleActivity.ChildView.LordRingView")
                    self._imgRed:SetActive(true)
                    local targets = LordRingDetailDataHelper:GetTargets(id)
                    local argsTable = LanguageDataHelper.GetArgsTable()
                    if ServiceActivityData:GetCurOpenType()==1 then
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(targets[1][2], StringStyleUtil.green)
                    else
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh("1/1", StringStyleUtil.green)
                    end
                    self._txtWearDesc.text = LanguageDataHelper.CreateContent(LordRingDetailDataHelper:GetName(id), argsTable)

                elseif  v[2] == 3 then     
                    self._notCompleteImgGo:SetActive(false)
                    self._alreadyGetImgGo:SetActive(true)
                    self:SetButtonActive(false)
                    --self._getRewardButton:SetActive(false)
                    self._imgRed:SetActive(false)
                    local targets = LordRingDetailDataHelper:GetTargets(id)
                    local argsTable = LanguageDataHelper.GetArgsTable()
                    if ServiceActivityData:GetCurOpenType()==1 then
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh(targets[1][2], StringStyleUtil.green)
                    else
                        argsTable["0"] = StringStyleUtil.GetColorStringWithTextMesh("1/1", StringStyleUtil.green)
                    end
                    self._txtWearDesc.text = LanguageDataHelper.CreateContent(LordRingDetailDataHelper:GetName(id), argsTable)
                end

                self._timer = TimerHeap:AddSecTimer(1, 0, 0, function()self:ScrollViewCanClick() end)

            
            end
        end
      
    end   
end

function LordRingItem:ScrollViewCanClick() 
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleActivity.ChildView.LordRingView")
end
