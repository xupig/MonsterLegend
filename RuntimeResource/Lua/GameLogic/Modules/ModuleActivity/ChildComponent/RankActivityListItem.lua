-- RankActivityListItem.lua
local RankActivityListItem = Class.RankActivityListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RankActivityListItem.interface = GameConfig.ComponentsConfig.Component
RankActivityListItem.classPath = "Modules.ModuleActivity.ChildComponent.RankActivityListItem"

local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIParticle = ClassTypes.UIParticle

function RankActivityListItem:Awake()
    self._imgNormal = self:FindChildGO("Container_Rank/Image_Normal")
    self._imgHigh = self:FindChildGO("Container_Rank/Image_High")
    self._nameText = self:GetChildComponent("Text_Name", 'TextMeshWrapper')
    self._stageText = self:GetChildComponent("Text_Stage", 'TextMeshWrapper')
    self._rankText = self:GetChildComponent("Container_Rank/Text_Rank", 'TextMeshWrapper')
end

function RankActivityListItem:OnDestroy()

end

--override
function RankActivityListItem:OnRefreshData(data)
    if data then
        self._nameText.text = data:GetName()
        self._data = data
        local rank = self:GetIndex() + 1
        self._rankText.text = rank
        if rank <= 3 then
            self._imgHigh:SetActive(true)
            self._imgNormal:SetActive(false)
        else
            self._imgHigh:SetActive(false)
            self._imgNormal:SetActive(true)
        end
        
        if ServiceActivityData:GetCurOpenType() == 4 then
            self._stageText.text = LanguageDataHelper.CreateContent(76981)
        else
            self._stageText.text = data:GetSecondValue()
        end
       
        -- LoggerHelper.Log("RankActivityListItem:OnRefreshData====================" .. PrintTable:TableToStr(data))
    
    end
end

function RankActivityListItem:SetSelect(flag)
    self._selectContainer:SetActive(flag)
    self._unSelectContainer:SetActive(not flag)
end
