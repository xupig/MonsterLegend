-- UnionHegemonyItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local UnionHegemonyItem = Class.UnionHegemonyItem(ClassTypes.UIListItem)
UnionHegemonyItem.interface = GameConfig.ComponentsConfig.Component
UnionHegemonyItem.classPath = "Modules.ModuleActivity.ChildComponent.UnionHegemonyItem"
require "Modules.ModuleActivity.ChildComponent.UnionIconTypeItem"
local UnionIconTypeItem = ClassTypes.UnionIconTypeItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local UIList = ClassTypes.UIList
local GuildConquestOpenServerDataHelper = GameDataHelper.GuildConquestOpenServerDataHelper


function UnionHegemonyItem:Awake()
    self._txtDesc = self:GetChildComponent("Text_Wear_Desc", 'TextMeshWrapper')
    self._containerState = self:FindChildGO("Container_State")
    self._containerState:SetActive(false)
    --self._txtDesc = self:GetChildComponent("Text_Wear_Desc", 'TextMeshWrapper')
    self:InitCom()
end

function UnionHegemonyItem:OnDestroy()

end

function UnionHegemonyItem:InitCom()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._iconList = list
    self._iconView = scrollView
    self._iconView:SetHorizontalMove(false)
    self._iconView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._iconList:SetItemType(UnionIconTypeItem)
    self._iconList:SetPadding(0, 0, 0, 0)
    self._iconList:SetGap(10, 10)
    self._iconList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function UnionHegemonyItem:AddFriendClick()

end



function UnionHegemonyItem:OnRefreshData(id)
    if id then
        --LoggerHelper.Log("UnionHegemonyItem:OnRefreshData" .. id)
        --LoggerHelper.Log("UnionHegemonyItem:OnRefreshDataQ" ..PrintTable:TableToStr())
        self._txtDesc.text = LanguageDataHelper.CreateContent(GuildConquestOpenServerDataHelper:GetDes(id))
        local rewardData = GuildConquestOpenServerDataHelper:GetReward(id)
        self._iconList:SetDataList(rewardData)
    end
end
