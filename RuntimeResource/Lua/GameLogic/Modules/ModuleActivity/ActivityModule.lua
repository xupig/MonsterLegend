-- ActivityModule.lua
ActivityModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function ActivityModule.Init()
    GUIManager.AddPanel(PanelsConfig.SevenDayLogin, "Panel_SevenDayLogin", GUILayer.LayerUIPanel, "Modules.ModuleActivity.SevenDayLoginPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.Activity, "Panel_Activity", GUILayer.LayerUIPanel, "Modules.ModuleActivity.ActivityPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)    
end

return ActivityModule