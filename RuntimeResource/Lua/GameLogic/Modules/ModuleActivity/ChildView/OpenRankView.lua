local OpenRankView = Class.OpenRankView(ClassTypes.BaseComponent)

OpenRankView.interface = GameConfig.ComponentsConfig.Component
OpenRankView.classPath = "Modules.ModuleActivity.ChildView.OpenRankView"
require "Modules.ModuleActivity.ChildComponent.OpenRankItem"

local public_config = GameWorld.public_config
local OpenRankItem = ClassTypes.OpenRankItem
local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local WelfareRankWayDataHelper = GameDataHelper.WelfareRankWayDataHelper
local WelfareRankRewardDataHelper = GameDataHelper.WelfareRankRewardDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local TimerHeap = GameWorld.TimerHeap
local DateTimeUtil = GameUtil.DateTimeUtil
local RankExManager = PlayerManager.RankExManager
local ServiceActivityConfig = GameConfig.ServiceActivityConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local OpenRankType = GameConfig.EnumType.OpenRankType 

require "Modules.ModuleActivity.ChildComponent.RankActivityListItem"
local RankActivityListItem = ClassTypes.RankActivityListItem

require "UIComponent.Extend.TipsCom"
local TipsCom = ClassTypes.TipsCom

function OpenRankView:Awake()
    self._selectTab = -1
    self._selectedFloor = 1
    self._btnList = {}
    self._txtList = {}
    self._iconList = {}

    self._csBH = self:GetComponent('LuaUIComponent')
    self._txtCount = self:GetChildComponent("Container_Bottom/Text_Count", 'TextMeshWrapper')
    self._txtRankDesc = self:GetChildComponent("Container_left/Image_Rank_Desc/Text_Rank_Desc", 'TextMeshWrapper')
    self._txtLevelDesc = self:GetChildComponent("Container_left/Image_Level_Desc/Text_Level_Desc", 'TextMeshWrapper')
    
    self._txtToLevel = self:GetChildComponent("Container_left/Image_ToLevel/Text_ToLevel", 'TextMeshWrapper')
    self._txtRank = self:GetChildComponent("Container_left/Image_Rank/Text_Rank", 'TextMeshWrapper')
    self._txtLevel = self:GetChildComponent("Container_left/Image_Level/Text_Level", 'TextMeshWrapper')
    
    self._button = self:FindChildGO("Container_left/Button_Rank_Info")
    self._csBH:AddClick(self._button, function()self:OnButtonClick() end)
    
    self._btn1 = self:FindChildGO("Container_left/Container_Tolevel_Activity/Button_1")
    self._txt1 = self:GetChildComponent("Container_left/Container_Tolevel_Activity/Button_1/Text_1", 'TextMeshWrapper')
    self._icon1 = self:FindChildGO("Container_left/Container_Tolevel_Activity/Container_1")
    
    self._btn2 = self:FindChildGO("Container_left/Container_Tolevel_Activity/Button_2")  
    self._txt2 = self:GetChildComponent("Container_left/Container_Tolevel_Activity/Button_2/Text_2", 'TextMeshWrapper')
    self._icon2 = self:FindChildGO("Container_left/Container_Tolevel_Activity/Container_2")

    self._btn3 = self:FindChildGO("Container_left/Container_Tolevel_Activity/Button_3")
    self._txt3 = self:GetChildComponent("Container_left/Container_Tolevel_Activity/Button_3/Text_3", 'TextMeshWrapper')
    self._icon3 = self:FindChildGO("Container_left/Container_Tolevel_Activity/Container_3")

    self._btn4 = self:FindChildGO("Container_left/Container_Tolevel_Activity/Button_4")
    self._txt4 = self:GetChildComponent("Container_left/Container_Tolevel_Activity/Button_4/Text_4", 'TextMeshWrapper')
    self._icon4 = self:FindChildGO("Container_left/Container_Tolevel_Activity/Container_4")




    self._rankView = self:FindChildGO("Container_Leaderboard")
    self._btnCloseRank = self:FindChildGO("Container_Leaderboard/Button_Close")
    self._csBH:AddClick(self._btnCloseRank, function()self:OnCloseRankClick() end)
    self:InitRankView()


    self._rankView.gameObject:SetActive(false)

    self._btnHelp = self:FindChildGO("Container_Bottom/Button_Help")
    self._csBH:AddClick(self._btnHelp, function()self:OnPlayDescClick() end)

    self._levelBg = self:FindChildGO("Image_OpenRank_level")
    GameWorld.AddIcon(self._levelBg,13535)
    self._petBg = self:FindChildGO("Image_OpenRank_Pet")
    GameWorld.AddIcon(self._petBg,13532)
    self._gemBg = self:FindChildGO("Image_OpenRank_Gem")
    GameWorld.AddIcon(self._gemBg,13536)
    self._fightBg = self:FindChildGO("Image_OpenRank_Fight")
    GameWorld.AddIcon(self._fightBg,13537)
    self._rideBg = self:FindChildGO("Image_OpenRank_Ride")
    GameWorld.AddIcon(self._rideBg,13533)
    self._chargeBg = self:FindChildGO("Image_OpenRank_Charge")
    GameWorld.AddIcon(self._chargeBg,13534)





    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(76846))
    self._tipsGO:SetActive(false)
    
    self._btnList[1] = self._btn1
    self._btnList[2] = self._btn2
    self._btnList[3] = self._btn3
    self._btnList[4] = self._btn4
    for i=1,4 do
        self._csBH:AddClick(self._btnList[i], function()self:OnGoButtonClick(i) end)
    end
   
    self._txtList[1] = self._txt1
    self._txtList[2] = self._txt2
    self._txtList[3] = self._txt3
    self._txtList[4] = self._txt4

    self._iconList[1] = self._icon1
    self._iconList[2] = self._icon2
    self._iconList[3] = self._icon3
    self._iconList[4] = self._icon4
    --进入其他
    

    self:InitView()
    self._refreshView = function(type)self:RefreshViewData(type) end
    self._getRankListResp = function()self:GetRankListResp() end
end


function OpenRankView:InitRankView()
    self._typeText = self:GetChildComponent("Container_Leaderboard/Container_LocalRank/Container_Title/Container_3/Text",'TextMeshWrapper')

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Leaderboard/Container_LocalRank/ScrollViewList")
    self._rankSmalllist = list
    self._rankSmalllistlView = scrollView
    self._rankSmalllistlView:SetHorizontalMove(false)
    self._rankSmalllistlView:SetVerticalMove(true)
    self._rankSmalllist:SetItemType(RankActivityListItem)
    self._rankSmalllist:SetPadding(0, 0, 0, 0)
    self._rankSmalllist:SetGap(0, 0)
    self._rankSmalllist:SetDirection(UIList.DirectionTopToDown, 1, -1)
    
    self._listData = RankExManager:GetRankList()
    if self._listData then
        self._rankSmalllist:SetDataList(self._listData)
    end
  
    
end

function OpenRankView:ShowView()
    --LoggerHelper.Error(" OpenRankView:ShowView()")
    self:AddEventListeners()

    if self._rankSmalllistlView then
        self._rankSmalllistlView:SetScrollRectState(true)
    end

end

function OpenRankView:CloseView()
    --LoggerHelper.Error(" OpenRankView:CloseView()")
    self:RemoveEventListeners()

    if self._rankSmalllistlView then
        self._rankSmalllistlView:SetScrollRectState(false)
    end

    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end



end



function OpenRankView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.GetOpenSeverGetInfoResp, self._refreshView)
    EventDispatcher:AddEventListener(GameEvents.RANK_LIST_GET_RESP, self._getRankListResp)
end

function OpenRankView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.GetOpenSeverGetInfoResp, self._refreshView)
    EventDispatcher:RemoveEventListener(GameEvents.RANK_LIST_GET_RESP, self._getRankListResp)
end

function OpenRankView:OnPlayDescClick()
    self._tipsGO:SetActive(true)
end


function OpenRankView:OnButtonClick()
    local type = self._index
    local rankType = 0
    if type == public_config.OPEN_SERVER_RANK_LEVEL then
        rankType = public_config.RANK_TYPE_LEVEL
    elseif type == public_config.OPEN_SERVER_RANK_PET then
        rankType = public_config.RANK_TYPE_PET
    elseif type == public_config.OPEN_SERVER_RANK_HORSE then
        rankType = public_config.RANK_TYPE_HORSE
    elseif type == public_config.OPEN_SERVER_RANK_CHARGE then
        rankType = public_config.RANK_TYPE_DAY_CHARGE
    elseif type == public_config.OPEN_SERVER_RANK_GEM then
        rankType = public_config.RANK_TYPE_GEM
    elseif type == public_config.OPEN_SERVER_RANK_COMBAT_VALUE then
        rankType = public_config.RANK_TYPE_COMBAT_VALUE
    end
    ServiceActivityManager:GetRank(type)

end

function OpenRankView:OnGoButtonClick(index)
    --GameManager.GUIManager.ClosePanel(PanelsConfig.Activity)
    local follow = WelfareRankWayDataHelper:GetFollow(self._index)
    EventDispatcher:TriggerEvent(GameEvents.On_OpenSvr_Click, follow[index])
end



function OpenRankView:OnCloseRankClick()
    self._rankView.gameObject:SetActive(false)
end

function OpenRankView:GetRankListResp()
    self._rankView.gameObject:SetActive(true)
    self._typeText.text = LanguageDataHelper.CreateContent(WelfareRankWayDataHelper:GetDesc3(ServiceActivityData:GetCurOpenType()))
    local rankContentListData = RankExManager:GetRankContentListData()
    if rankContentListData then 
        self._rankSmalllist:SetDataList(rankContentListData)
    end
end



function OpenRankView:RefreshViewData(type)
    --LoggerHelper.Error("OpenRankView:RefreshViewData()".."Type:"..type.."self._index:"..self._index)
    if type == self._index then
        if self._leftTime > 0 then
            self:SetValue(type)
        else
  
            if ServiceActivityData:GetIsGetReward() == 1 or ServiceActivityData:GetIsGetReward() == 0 then
                --LoggerHelper.Error("OpenRankView:SetValue==1")
                self:SetValue(type)
            elseif  ServiceActivityData:GetIsGetReward() == 2 then
                --LoggerHelper.Error("OpenRankView:SetValue==2")
                self._txtRankDesc.text = LanguageDataHelper.CreateContent(76974)
                self._txtLevelDesc.text = LanguageDataHelper.CreateContent(76974)
            end
        end
        self:RefreshListData(type)
    end
end

function OpenRankView:SetTypeBg(type)
    self._levelBg:SetActive(type == public_config.OPEN_SERVER_RANK_LEVEL)
    self._petBg:SetActive(type == public_config.OPEN_SERVER_RANK_PET)
    self._gemBg:SetActive(type == public_config.OPEN_SERVER_RANK_GEM)
    self._fightBg:SetActive(type == public_config.OPEN_SERVER_RANK_COMBAT_VALUE)
    self._rideBg:SetActive(type == public_config.OPEN_SERVER_RANK_HORSE)
    self._chargeBg:SetActive(type == public_config.OPEN_SERVER_RANK_CHARGE)
end


function OpenRankView:SetValue(type)
    local myInfo = ServiceActivityData:GetMyInfoData()
    --LoggerHelper.Error("ServiceActivityData:GetMyInfoData()"..PrintTable:TableToStr(myInfo))
    
    if myInfo:GetRankValue() then
        self._txtRankDesc.text = myInfo:GetRankValue()
    else    
        self._txtRankDesc.text = LanguageDataHelper.CreateContent(76952)
    end

    if type == public_config.OPEN_SERVER_RANK_LEVEL then
        if ServiceActivityManager:GetOpenRank(type)>0 then
            self._txtLevelDesc.text = GameWorld.Player().level
        else
            if myInfo:GetLevel() then
                --LoggerHelper.Error("myInfo:GetLevel()"..myInfo:GetLevel())
                self._txtLevelDesc.text = myInfo:GetLevel()
            else
                self._txtLevelDesc.text = LanguageDataHelper.CreateContent(76974)
            end
        end


    elseif type == public_config.OPEN_SERVER_RANK_PET then
    
            local argsTable = LanguageDataHelper.GetArgsTable()
            if ServiceActivityManager:GetOpenRank(type)>0 then
                argsTable["0"] = GameWorld.Player().pet_grade
                argsTable["1"] = GameWorld.Player().pet_star
            else
                if myInfo:GetPetGrade() then
                    argsTable["0"] = myInfo:GetPetGrade()
                    argsTable["1"] = myInfo:GetPetStar()
                else
                    self._txtLevelDesc.text = LanguageDataHelper.CreateContent(76974)
                    return
                end
            end
            self._txtLevelDesc.text = LanguageDataHelper.CreateContent(76975, argsTable)
    
    elseif type == public_config.OPEN_SERVER_RANK_HORSE then
        local argsTable = LanguageDataHelper.GetArgsTable()
        if ServiceActivityManager:GetOpenRank(type)>0 then
            argsTable["0"] = GameWorld.Player().horse_grade
            argsTable["1"] = GameWorld.Player().horse_star
        else
            if myInfo:GetHorseGrade() then
                argsTable["0"] = myInfo:GetHorseGrade()
                argsTable["1"] = myInfo:GetHorseStar()
            else
                self._txtLevelDesc.text = LanguageDataHelper.CreateContent(76974)
                return
            end
        end
        self._txtLevelDesc.text = LanguageDataHelper.CreateContent(76975, argsTable)
        
    elseif type == public_config.OPEN_SERVER_RANK_CHARGE then
        --LoggerHelper.Log("OPEN_SERVER_RANK_CHARGE:" .. PrintTable:TableToStr(myInfo))
        --LoggerHelper.Log("OPEN_SERVER_RANK_CHARGE:" .. myInfo:GetDayCharge()

        if ServiceActivityManager:GetOpenRank(type)>0 then
            self._txtLevelDesc.text = myInfo:GetDayCharge() or 0
        else
            if myInfo:GetDayCharge() then
                self._txtLevelDesc.text = myInfo:GetDayCharge()
            else
                self._txtLevelDesc.text = LanguageDataHelper.CreateContent(76974)
            end
        end
    elseif type == public_config.OPEN_SERVER_RANK_GEM then
        if ServiceActivityManager:GetOpenRank(type)>0 then
            self._txtLevelDesc.text = GameWorld.Player().total_gem_level
        else
            if myInfo:GetGemLevel() then
                self._txtLevelDesc.text = myInfo:GetGemLevel()
            else
                self._txtLevelDesc.text = LanguageDataHelper.CreateContent(76974)
            end
        end
    elseif type == public_config.OPEN_SERVER_RANK_COMBAT_VALUE then
        if ServiceActivityManager:GetOpenRank(type)>0 then
            self._txtLevelDesc.text = GameWorld.Player().fight_force
        else
            if myInfo:GetComBatValue() then
                self._txtLevelDesc.text = myInfo:GetComBatValue()
            else
                self._txtLevelDesc.text = LanguageDataHelper.CreateContent(76974)
            end
        end
    end   
end

function OpenRankView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._rankList = list
    self._openRanklistlView = scrollView
    self._openRanklistlView:SetHorizontalMove(false)
    self._openRanklistlView:SetVerticalMove(true)
    self._rankList:SetItemType(OpenRankItem)
    self._rankList:SetPadding(0, 0, 0, 0)
    self._rankList:SetGap(10, 0)
    self._rankList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function OpenRankView:InitData(secIndex)
    --LoggerHelper.Error("OpenRankView:RefreshViewData()".."self._index:"..self._index)
    ServiceActivityData:SetCurOpenType(secIndex)
    ServiceActivityManager:SendOpenSeverGetInfoReq(secIndex)
    ServiceActivityManager:UpdateOpenRank()
    self._leftTime = ServiceActivityManager:GetOpenRank(secIndex)
    self._index = secIndex
    --LoggerHelper.Error("OpenRankView:RefreshViewData()".."self._index:"..self._index)

    if ServiceActivityManager:GetOpenRank(secIndex) > 0 then
        if self._timer then
            TimerHeap:DelTimer(self._timer)
            self._timer = nil
        end
        self._button.gameObject:SetActive(true) 
        self:UpdateTimeLeft(secIndex)
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnBackToActivityTime() end)
    else
        --self._button.gameObject:SetActive(false)     
        self._txtCount.text = LanguageDataHelper.CreateContent(76951)
    end
    self._txtLevel.text = LanguageDataHelper.CreateContent(WelfareRankWayDataHelper:GetDesc1(secIndex))
    self._txtToLevel.text = LanguageDataHelper.CreateContent(WelfareRankWayDataHelper:GetDesc2(secIndex))
    
    self:RefreshListData(secIndex)


    local followTb = WelfareRankWayDataHelper:GetFollow(secIndex)
    for i = 1, 4 do
        if followTb[i] then
            self._btnList[i]:SetActive(true)
            self._iconList[i]:SetActive(true)
            GameWorld.AddIcon(self._iconList[i], followTb[i][2][3])
            self._txtList[i].text = LanguageDataHelper.CreateContent(followTb[i][2][2])
        else
            self._btnList[i]:SetActive(false)
            self._iconList[i]:SetActive(false)
        end
    end
    
    --LoggerHelper.Log("self._serTimeSetting" .. PrintTable:TableToStr(followTb))

end


function OpenRankView:RefreshListData(secIndex)
    local needIds = {}
    local ids = WelfareRankRewardDataHelper:GetAllId()
    for i, v in ipairs(ids) do
        local type = WelfareRankRewardDataHelper:GetType(v)
        if type == secIndex then
            table.insert(needIds, v)
        end
    end
    self._needIds = needIds
    self._rankList:SetDataList(needIds)
end

function OpenRankView:UpdateTimeLeft(secIndex)
    local index = self._index
    ServiceActivityManager:UpdateOpenRank()
    self._leftTime = ServiceActivityManager:GetOpenRank(index)
    local argsTableRank = LanguageDataHelper.GetArgsTable()
    argsTableRank["0"] = DateTimeUtil.FormatParseTimeStr(ServiceActivityManager:GetOpenRank(index))
    self._txtCount.text = LanguageDataHelper.CreateContent(76844, argsTableRank)
end

function OpenRankView:OnBackToActivityTime()
    if (self._leftTime > 0) then
        -- self._reviveTime = self._reviveTime - 1
        self:UpdateTimeLeft()
        self._button.gameObject:SetActive(true) 
    else
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
        --self._isTire = false
        --self._button.gameObject:SetActive(false)
        self._txtCount.text = LanguageDataHelper.CreateContent(76951)
    end
end


function OpenRankView:SetSuitType(secIndex)
    self:InitData(secIndex)
    self:SetTypeBg(secIndex)
end



function OpenRankView:OnToggleGroupClick()

end

function OpenRankView:OnDestroy()

end
