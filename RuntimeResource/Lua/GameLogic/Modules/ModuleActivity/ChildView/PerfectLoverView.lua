local PerfectLoverView = Class.PerfectLoverView(ClassTypes.BaseComponent)

PerfectLoverView.interface = GameConfig.ComponentsConfig.Component
PerfectLoverView.classPath = "Modules.ModuleActivity.ChildView.PerfectLoverView"
require "Modules.ModuleActivity.ChildComponent.PerfectLoveItem"
local UIList = ClassTypes.UIList
local PerfectLoveItem = ClassTypes.PerfectLoveItem
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local DateTimeUtil = GameUtil.DateTimeUtil
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local MarriageManager = PlayerManager.MarriageManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PlayerActionManager = GameManager.PlayerActionManager

function PerfectLoverView:Awake()
    self._txtTime = self:GetChildComponent("Text_Activity_Time", 'TextMeshWrapper')
    self._getGoMarry = self:FindChildGO("Container_Right/Button_ToMarry")
    self._iconContainer = self:FindChildGO("Container_Right/Container")
    self._csBH:AddClick(self._getGoMarry, function()self:OnGoMarryClick() end)
    self:InitCom()
    self._onRefreshList = function()self:RefreshLoveList() end
end


function PerfectLoverView:InitCom()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._rankList = list
    self._openRanklistlView = scrollView
    self._openRanklistlView:SetHorizontalMove(false)
    self._openRanklistlView:SetVerticalMove(true)
    self._rankList:SetItemType(PerfectLoveItem)
    self._rankList:SetPadding(0, 0, 0, 0)
    self._rankList:SetGap(0, 0)
    self._rankList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function PerfectLoverView:OnGoMarryClick()


    GameManager.GUIManager.ClosePanel(PanelsConfig.Activity)

    MarriageManager:GotoMarriage()
    --TaskCommonManager:GoToNpc(GlobalParamsHelper.GetParamValue(714))--结婚唯一指定NPC
end


function PerfectLoverView:OnDestroy()

end

function PerfectLoverView:ShowView()
    if self._openRanklistlView then
        self._openRanklistlView:SetScrollRectState(true)
    end

    self._lovego = self:FindChildGO("Container_Right/Image_Love")
    GameWorld.AddIcon(self._lovego,13540)
    GameWorld.AddIcon(self._iconContainer,13636)
    ServiceActivityManager:SendMarryRankReq()

    
    local closeTimeSec = DateTimeUtil.GetOpenServerTime()+7*24*60*60
    local serTime = DateTimeUtil.GetOpenServerTime()

    -- LoggerHelper.Log("PerfectLoverView:ShowView()" ..DateTimeUtil.GetDateMonthDayStr(serTime))
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = DateTimeUtil.GetDateMonthDayStr(serTime)
    argsTable["1"] = DateTimeUtil.GetDateHourMinStr(serTime)
    argsTable["2"] = DateTimeUtil.GetDateMonthDayStr(closeTimeSec)
    argsTable["3"] = DateTimeUtil.GetDateHourMinStr(closeTimeSec)
    self._txtTime.text = LanguageDataHelper.CreateContent(76976, argsTable)
    
    EventDispatcher:AddEventListener(GameEvents.GetMarryRankGetInfoResp, self._onRefreshList)

end

function PerfectLoverView:CloseView()
    if self._openRanklistlView then
        self._openRanklistlView:SetScrollRectState(false)
    end
    self._lovego=nil
    EventDispatcher:RemoveEventListener(GameEvents.GetMarryRankGetInfoResp, self._onRefreshList)
end


function PerfectLoverView:RefreshLoveList()
    local loveList = ServiceActivityData:GetLoveList()
	self._rankList:SetDataList(loveList)
    -- LoggerHelper.Log("UnionHegemonyItem:OnRefreshDataQ" .. PrintTable:TableToStr(loveList))
end
