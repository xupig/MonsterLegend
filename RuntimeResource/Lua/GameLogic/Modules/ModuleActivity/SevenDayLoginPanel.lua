--SevenDayLoginPanel.lua
local SevenDayLoginPanel = Class.SevenDayLoginPanel(ClassTypes.BasePanel)

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local Welfare7dayRewardDataHelper = GameDataHelper.Welfare7dayRewardDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local RectTransform = UnityEngine.RectTransform
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local SevenDayLoginManager = PlayerManager.SevenDayLoginManager
local XArtNumber = GameMain.XArtNumber
local GUIManager =  GameManager.GUIManager
local XImageFlowLight = GameMain.XImageFlowLight
local ActorModelComponent = GameMain.ActorModelComponent
local EquipModelComponent = GameMain.EquipModelComponent
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local UIParticle = ClassTypes.UIParticle
local PlayerModelComponent = GameMain.PlayerModelComponent
local SortOrderedRenderAgent = GameMain.SortOrderedRenderAgent


require "Modules.ModuleActivity.ChildComponent.SevenDayRewardItem"
local SevenDayRewardItem = ClassTypes.SevenDayRewardItem

require "Modules.ModuleActivity.ChildComponent.DayRewardItem"
local DayRewardItem = ClassTypes.DayRewardItem

function SevenDayLoginPanel:Awake()
    self:InitCallback()
    self:InitViews()
end

function SevenDayLoginPanel:InitCallback()
    self._updateData = function() self:UpdateData() end
    self._getRewardSuccess = function(day) self:HandleGetRewardSuccess(day) end
end

function SevenDayLoginPanel:InitViews()
    self._weekDay = 7
    self._fx1 = UIParticle.AddParticle(self.gameObject, "Container_Reward_Content/Container_Fx/FirstCharge")

    self._dayArtNumber = self:AddChildComponent('Container_Info/Container_SevenDay', XArtNumber)
    self._infoContainer = self:FindChildGO("Container_Info/Container_Info1")
    self._rewardInfoContainer = self:FindChildGO("Container_Reward_Content/Container_Reward_Info")
    self._itemContainer = self:FindChildGO("Container_Reward_Content/Container_Item")
    self._itemContainerPos = self._itemContainer:AddComponent(typeof(XGameObjectTweenPosition))
    self._itemContainerInitPosition = self._itemContainer.transform.localPosition
    self._rewardText = self:GetChildComponent("Button_Get_Reward/Text",'TextMeshWrapper')

    self._containerModel = self:FindChildGO("Container_Reward_Content/Container_Model")
    self._modelComponent = self:AddChildComponent('Container_Reward_Content/Container_Model', ActorModelComponent)
    self._modelComponent:SetStartSetting(Vector3(0, 0, -500), Vector3(0, 180, 0), 1)

    self._equipModelComponent = self:AddChildComponent('Container_Reward_Content/Container_EquipModel', EquipModelComponent)
    self._equipModelComponent:SetStartSetting(Vector3(0, 0, -500), Vector3(0, 180, 0), 1)

    self._playerActorComponent = self:AddChildComponent('Container_Reward_Content/Container_PlayerModel', PlayerModelComponent)
    self._playerActorComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)

    self._redPointGo = self:FindChildGO("Button_Get_Reward/Image_RedPoint")
    self._updateRedPointFunc = function(flag) self:SetRedPoint(flag) end    

    self._getRewardButton = self:FindChildGO("Button_Get_Reward")
    self._csBH:AddClick(self._getRewardButton, function() self:OnGetRewardButtonClick() end)
    self._getRewardButtonFlowLight = self:AddChildComponent("Button_Get_Reward", XImageFlowLight)

    self._closeButton = self:FindChildGO("Container_Bg/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnCloseButtonClick() end)

    self:InitProgressBar()
    self:InitSevenDayRewardView()
    self:InitRewardGroup()
end

function SevenDayLoginPanel:InitRewardGroup()
    self._rewardGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_RewardList")
    self._rewardGroup:SetItemType(DayRewardItem)

    -- self._rewardGroupItemClick = function(idx) self:RewardGroupItemClick(idx) end
    -- self._rewardGroup:SetItemClickedCB(self._rewardGroupItemClick)
end

function SevenDayLoginPanel:SevenDayRewardItemClick(idx)
    self._selectDay = idx
    self._dayArtNumber:SetNumberDontChangeY(self._selectDay)
    local icons = Welfare7dayRewardDataHelper:GetIcons(self._selectDay)
    GameWorld.AddIcon(self._infoContainer, icons[2])
    GameWorld.AddIcon(self._rewardInfoContainer, icons[3])
    -- GameWorld.AddIcon(self._itemContainer, icons[4])
    local bigPic = Welfare7dayRewardDataHelper:GetBigPic(self._selectDay)
    local iconType = bigPic[1]
    self._iconType = iconType
    if iconType == 1 then--宠物坐骑模型
        self._modelComponent:Show()
        self._equipModelComponent:Hide()
        self._playerActorComponent:Hide()
        self._itemContainer:SetActive(false)
        local modelId = bigPic[2]
        self._modelComponent:SetStartSetting(Vector3(bigPic[5], bigPic[6], bigPic[7]), Vector3(0, bigPic[3], 0), bigPic[4])
        self._modelComponent:LoadModel(modelId)
    elseif iconType == 2 then--武器模型
        self._equipModelComponent:Show()
        self._modelComponent:Hide()
        self._playerActorComponent:Hide()
        self._itemContainer:SetActive(false)
        local modelId = bigPic[2]
        self._equipModelComponent:SetStartSetting(Vector3(bigPic[7], bigPic[8], bigPic[9]), Vector3(bigPic[3], bigPic[4], bigPic[5]), bigPic[6])
        self._equipModelComponent:LoadEquipModel(modelId, "")
        if bigPic[10] == 1 then
            self._equipModelComponent:SetSelfRotate(true)
        else
            self._equipModelComponent:SetSelfRotate(false)
        end
    elseif iconType == 3 then--特效
        -- LoggerHelper.Error("bigPic[2] =====" .. tostring(bigPic[2]))
        self._playerActorComponent:Show()
        self._modelComponent:Hide()
        self._equipModelComponent:Hide()
        self._itemContainer:SetActive(false)

        self._effect = bigPic[2]
        GameWorld.ShowPlayer():SetCurEffect(self._effect)
        GameWorld.ShowPlayer():SetCurWing(0)
        TimerHeap:AddTimer(200, 0, 0, function() SortOrderedRenderAgent.ReorderAll() end)
    elseif iconType == 0 then--图片
        self._itemContainer:SetActive(true)
        self._modelComponent:Hide()
        self._equipModelComponent:Hide()
        self._playerActorComponent:Hide()
        GameWorld.AddIcon(self._itemContainer, bigPic[2])

        self._itemContainer.transform.localPosition = Vector3(bigPic[3],bigPic[4],bigPic[5])
        self._itemContainerPos:SetFromToPos(self._itemContainer.transform.localPosition, self._itemContainer.transform.localPosition + Vector3(0, 10, 0), 0.5, 4, nil)
    end

    local group = GameWorld.Player():GetVocationGroup()
    self._rewardGroupDatas = Welfare7dayRewardDataHelper:GetRewardByVocationGroup(group, self._selectDay)
    self._rewardGroup:SetDataList(self._rewardGroupDatas)

    self:UpdateGetRewardButtonState(idx)
end

--day 选中第几第几天
function SevenDayLoginPanel:UpdateGetRewardButtonState(day)
    local get_reward_day = GameWorld.Player().get_reward_day 
    local canGetNum = SevenDayLoginManager:GetCanGetRewardNum()
    local get_reward_num = SevenDayLoginManager:GetRewardDays()

    if get_reward_day[day] then 
        --已领取
        self._getRewardButton:SetActive(true)
        self._getRewardButton:GetComponent("ButtonWrapper"):SetInteractable(false)
        self._getRewardButtonFlowLight.enabled = false
        --LoggerHelper.Error("已领取")
        self._rewardText.text = LanguageDataHelper.CreateContent(76982)
    else
        if canGetNum > 0 then
            --可以领取
            self._getRewardButton:SetActive(true)
            self._getRewardButton:GetComponent("ButtonWrapper"):SetInteractable(true)
            self._getRewardButtonFlowLight.enabled = true
            self._rewardText.text = LanguageDataHelper.CreateContent(76914)
        else
            --未达到领取条件            
            self._getRewardButton:SetActive(false)
            self._getRewardButtonFlowLight.enabled = false
        end
    end
end

function SevenDayLoginPanel:InitSevenDayRewardView()
    local ids = Welfare7dayRewardDataHelper:GetAllId()
    self._rewardViews = {}
    self._sevenDayRewardItemClick = function(idx) self:SevenDayRewardItemClick(idx) end
    for i=1,self._weekDay do
        local v = ids[i]
        local go = self:CopyRewardContainer()
        table.insert(self._rewardViews, UIComponentUtil.AddLuaUIComponent(go, SevenDayRewardItem))
        self._rewardViews[i]:InitData(v, self._progressBarLength)
        self._rewardViews[i]:SetPointerUpCB(self._sevenDayRewardItemClick)
    end
    self._rewardDemoContainer:SetActive(false)
end

function SevenDayLoginPanel:CopyRewardContainer()
    return self:CopyUIGameObject("Container_Progress/Container_Rewards/Container_Reward", "Container_Progress/Container_Rewards")
end

function SevenDayLoginPanel:InitProgressBar()
    self._rewardDemoContainer = self:FindChildGO("Container_Progress/Container_Rewards/Container_Reward")
    self._progressBarLength = self:FindChildGO("Container_Progress/ProgressBar/Image_Background"):GetComponent(typeof(RectTransform)).sizeDelta.x

    self._barComp = UIScaleProgressBar.AddProgressBar(self.gameObject,"Container_Progress/ProgressBar")
    self._barComp:SetProgress(0)
    self._barComp:ShowTweenAnimate(true)
    self._barComp:SetMutipleTween(false)
    self._barComp:SetIsResetToZero(false)
    self._barComp:SetTweenTime(0.2)
end

function SevenDayLoginPanel:OnShow(data)
    -- SevenDayLoginManager:RegistUpdateFunc(self._updateRedPointFunc)
    --LoggerHelper.Error("ShowView")
    GameWorld.ShowPlayer():ShowModel(1)
    self._playerActorComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
    self:AddEventListeners()
    self:UpdateData()
end

function SevenDayLoginPanel:SetData(data)
end

function SevenDayLoginPanel:OnClose()
    -- SevenDayLoginManager:RegistUpdateFunc(nil)
    GameWorld.ShowPlayer():SyncShowPlayerFacade()
    GameWorld.ShowPlayer():ShowModel(3)
    self._iconType = nil
    self:RemoveEventListeners()
end

function SevenDayLoginPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.SEVEN_DAY_REFRESH, self._updateData)
    EventDispatcher:AddEventListener(GameEvents.SEVEN_DAY_GET_REWQARD_SUCCESS, self._getRewardSuccess)
end

function SevenDayLoginPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.SEVEN_DAY_REFRESH, self._updateData)
    EventDispatcher:RemoveEventListener(GameEvents.SEVEN_DAY_GET_REWQARD_SUCCESS, self._getRewardSuccess)
end

function SevenDayLoginPanel:UpdateData()
    local canGetNum = SevenDayLoginManager:GetCanGetRewardNum()    
    local getRewardDay = SevenDayLoginManager:GetRewardDays()--已领取天数
    local index = 0
    local login_days = GameWorld.Player().login_days
    local count = 0
    local progress = 0.01
    local week = math.ceil(getRewardDay / self._weekDay) - 1
    if getRewardDay ~=0 and getRewardDay % self._weekDay == 0 and getRewardDay ~= #Welfare7dayRewardDataHelper:GetAllId() then
        week = week + 1
    end
    if week > 0 then
        index = index + self._weekDay * week
    end

    for i=1,self._weekDay do
        local v = i + index
        if login_days >= v then
            count = count + 1
        end
        self._rewardViews[i]:InitData(v, self._progressBarLength)
    end

    if count ~= 0 then
        progress = (count - 1) / (self._weekDay - 1)
    end

    local day = SevenDayLoginManager:GetRewardDays() + 1
    local selectDay = SevenDayLoginManager:GetSelectDay()
    

    if self._progress ~= progress then
        self._progress = progress
        self._barComp:SetProgress(self._progress)
    end

    for i,v in ipairs(self._rewardViews) do
        self._rewardViews[i]:UpdateData()
    end
    self:SevenDayRewardItemClick(selectDay)

    -- self._dayArtNumber:SetNumber(day)
    -- local icons = Welfare7dayRewardDataHelper:GetIcons(day)
    -- GameWorld.AddIcon(self._infoContainer, icons[2])
    -- GameWorld.AddIcon(self._rewardInfoContainer, icons[3])
    -- GameWorld.AddIcon(self._itemContainer, icons[4])
end

function SevenDayLoginPanel:OnGetRewardButtonClick()
    SevenDayLoginManager:GetReward(self._selectDay)
end

function SevenDayLoginPanel:OnCloseButtonClick()
    GUIManager.ClosePanel(PanelsConfig.SevenDayLogin)
end

function SevenDayLoginPanel:SetRedPoint(flag)
    self._redPointGo:SetActive(flag)
    self._getRewardButton:GetComponent("ButtonWrapper"):SetInteractable(flag)
    self._getRewardButtonFlowLight.enabled = flag
end

function SevenDayLoginPanel:HandleGetRewardSuccess(day)
    self:UpdateData()

    -- while day > 7 do
    --     day = day - 7
    -- end
    -- self._rewardViews[day]:UpdateData()
    -- self:SevenDayRewardItemClick(SevenDayLoginManager:GetSelectDay())
end

function SevenDayLoginPanel:BaseShowModel()
    if self._iconType == 1 then
        self._modelComponent:Show()
    elseif self._iconType == 2 then
        self._equipModelComponent:Show()
    elseif self._iconType == 3 then
        self._playerActorComponent:Show()
        GameWorld.ShowPlayer():SetCurEffect(self._effect)
        GameWorld.ShowPlayer():SetCurWing(0)
    end
end

function SevenDayLoginPanel:BaseHideModel()
    if self._iconType == 1 then
        self._modelComponent:Hide()
    elseif self._iconType == 2 then
        self._equipModelComponent:Hide()
    elseif self._iconType == 3 then
        self._playerActorComponent:Hide()
        GameWorld.ShowPlayer():SyncShowPlayerFacade()
    end
end