-- ActivityPanel.lua
local ActivityPanel = Class.ActivityPanel(ClassTypes.BasePanel)

require "Modules.ModuleActivity.ChildView.LordRingView"
require "Modules.ModuleActivity.ChildView.OpenRankView"
require "Modules.ModuleActivity.ChildView.PerfectLoverView"
require "Modules.ModuleActivity.ChildView.UnionHegemonyView"
require "Modules.ModuleActivity.ChildView.CollectWordView"

local LordRingView = ClassTypes.LordRingView
local OpenRankView = ClassTypes.OpenRankView
local PerfectLoverView = ClassTypes.PerfectLoverView
local UnionHegemonyView = ClassTypes.UnionHegemonyView
local CollectWordView = ClassTypes.CollectWordView
local DateTimeUtil = GameUtil.DateTimeUtil
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local ServiceActivityType = GameConfig.EnumType.ServiceActivityType
local WelfareRankWayDataHelper = GameDataHelper.WelfareRankWayDataHelper
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local OpenRankType = GameConfig.EnumType.OpenRankType
local public_config = GameWorld.public_config


function ActivityPanel:Awake()
    self:InitComps()
    self:InitCallBack()
    self._onRefreshTab = function()self:RefreshTab() end
end


function ActivityPanel:InitText()
end

--初始化回调
function ActivityPanel:InitCallBack()
    self:SetTabClickAfterUpdateSecTabCB(function(index)
        self:SelectTab(index)
    end)
    self:SetSecondTabClickCallBack(function(index)
        self:SelectSecTab(index)
    end)

    self:SetCustomizeFunctionOpenTextCB(function(index,secIndex)
        self:OpenTips(index,secIndex)
    end)
end

function ActivityPanel:SelectTab(index)
    ServiceActivityManager:UpdateOpenRank()
    if ServiceActivityManager:GetIsOpenRanking() == true then

    else
        self:SetFirstTabActive(2, false)
    end
    if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_DEMON_RING) then
    else
        self:SetFirstTabActive(1, false)
    end





    if ServiceActivityManager:GetIsOpenActivity() then
    else
        self:SetFirstTabActive(3, false)   
        self:SetFirstTabActive(4, false)  
        self:SetFirstTabActive(5, false)  
    end 

    
   
    if self._selectedIndex == 2 then
        ServiceActivityManager:UpdateOpenRank()
        for i = OpenRankType.Level, OpenRankType.Combat do
            if ServiceActivityManager:GetOpenRank(i) == -1 then
                --LoggerHelper.Log("ActivityPanel:SetSecTabFunctionActive========== .." .. i)
                self:SetSecTabFunctionActive(i, false)
            else
                self:SetSecTabFunctionActive(i, true)
            end
        end
        local index = ServiceActivityManager:GetCurOpenRankIndex()
        if tonumber(index)~=0 then
            self:OnSecTabClick(index)
        end
    end

end




function ActivityPanel:SelectSecTab(secIndex)
    local typeRing = secIndex
    if self._selectedIndex == 1 then
        if secIndex == 3 or secIndex == 4 then
            typeRing = secIndex+1
        end
    end

    ServiceActivityData:SetCurOpenType(typeRing)
    if self._selectedIndex == 1 then
        self._selectedView:SetSuitType(typeRing)
        for i = 1, 4 do
            if i>ServiceActivityManager:GetRingUnlockStep() and i~=1 and i~=4 then  
                --self._secTabs[i]:SetName("???")
                self:SetSecTabFunctionActive(i, false)
            else
                self:SetSecTabFunctionActive(i, true)
            end
        end
        
    end
    
    if self._selectedIndex == 2 then
        if ServiceActivityManager:GetIsOpenRanking() == true then
            self._selectedView:SetSuitType(secIndex)
            ServiceActivityManager:UpdateOpenRank()
        end
    end

end

function ActivityPanel:OpenTips(index,secIndex)
    if index == 1 then     
        if secIndex == 2 then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76977))
        end

        if secIndex == 3 then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76978))
        end

        if secIndex == 4 then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76979))
        end
    end

    self._serTimeSetting = GlobalParamsHelper.GetParamValue(758)
    self._openRankTitle = GlobalParamsHelper.GetParamValue(813)
    if index == 2 then
            -- LoggerHelper.Log("ActivityPanel:OpenTips"..index..","..secIndex)
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(self._openRankTitle[secIndex])
            argsTable["1"] = self._serTimeSetting[secIndex][1]
            argsTable["2"] = self._serTimeSetting[secIndex][2] 
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76980,argsTable))
    end
end

function ActivityPanel:InitComps()
    
    self:InitView("Container_LordRings", LordRingView, 1, 1)
    self:InitView("Container_LordRings", LordRingView, 1, 2)
    self:InitView("Container_LordRings", LordRingView, 1, 3)
    self:InitView("Container_LordRings", LordRingView, 1, 4)
    self:InitView("Container_LordRings", LordRingView, 1, 5)
    

    self:InitView("Container_OpenRank", OpenRankView, 2, 1)
    self:InitView("Container_OpenRank", OpenRankView, 2, 2)
    self:InitView("Container_OpenRank", OpenRankView, 2, 3)
    self:InitView("Container_OpenRank", OpenRankView, 2, 4)
    self:InitView("Container_OpenRank", OpenRankView, 2, 5)
    self:InitView("Container_OpenRank", OpenRankView, 2, 6)
    
    
    
    
    self:InitView("Container_CollectWord", CollectWordView, 3)
    self:InitView("Container_UnionHegemony", UnionHegemonyView, 4)
    self:InitView("Container_PerfectLover", PerfectLoverView, 5)




--self:SetSecondTabClickCallBack(self._secTabCb)
end



function ActivityPanel:OnShow(data)


    self:UpdateFunctionOpen()
    --self.secondTabIndex = 2
    self:OnShowSelectTab(data)


    EventDispatcher:AddEventListener(GameEvents.GetRingUnlockResp, self._onRefreshTab)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RING_UNLOCK_STEP, self._onRefreshTab) 
end

function ActivityPanel:OnClose()
    EventDispatcher:RemoveEventListener(GameEvents.GetRingUnlockResp, self._onRefreshTab)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RING_UNLOCK_STEP, self._onRefreshTab)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_service_activity_close_button")
--EventDispatcher:RemoveEventListener(GameEvents.VIPCHARGE,self._seleView)
end

function ActivityPanel:RefreshTab()
    --LoggerHelper.Log("ServiceActivityManager:GetRingUnlockStep========== .." .. ServiceActivityManager:GetRingUnlockStep())
    if self._selectedIndex == 1 then
        for i = 1, 4 do
            if i>ServiceActivityManager:GetRingUnlockStep() and i~=1 and i~=4 then  
                --self._secTabs[i]:SetName("???")
                self:SetSecTabFunctionActive(i, false)
                -- LoggerHelper.Log("ServiceActivityManager:GetRingUnlockStep==false======== .." ..i)
            else
                self._ringTitle = GlobalParamsHelper.GetParamValue(823)
                -- LoggerHelper.Log("ServiceActivityManager:GetRingUnlockStep==_ringTitle======== .." ..PrintTable:TableToStr(self._ringTitle))
                if  i == 3 or i == 4 then
                    self._secTabs[i]:SetName(LanguageDataHelper.CreateContent(self._ringTitle[i+1]))
                else
                    self._secTabs[i]:SetName(LanguageDataHelper.CreateContent(self._ringTitle[i]))
                end
                self._secTabs[i]:SetName(LanguageDataHelper.CreateContent(self._ringTitle[i]))
                self:SetSecTabFunctionActive(i, true)
                -- LoggerHelper.Log("ServiceActivityManager:GetRingUnlockStep==true======== .." ..i)
            end
        end
    end
end



function ActivityPanel:StructingViewInit()
    return true
end



function ActivityPanel:WinBGType()
    return PanelWinBGType.NormalBG
end
