--EscortPeriSettleModule.lua
EscortPeriSettleModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function EscortPeriSettleModule.Init()
     GUIManager.AddPanel(PanelsConfig.EscortPeriSettle,"Panel_EscortPeriSettle",GUILayer.LayerUIPanel,"Modules.ModuleEscortPeriSettle.EscortPeriSettlePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return EscortPeriSettleModule