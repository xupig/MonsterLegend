require "Modules.ModuleTask.ChildComponent.TaskRewardItem"

local EscortPeriSettlePanel = Class.EscortPeriSettlePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TaskRewardItem = ClassTypes.TaskRewardItem
local TaskRewardData = ClassTypes.TaskRewardData
local EscortPeriManager = PlayerManager.EscortPeriManager
local public_config = GameWorld.public_config
local GuardGoddessDataHelper = GameDataHelper.GuardGoddessDataHelper
local UIParticle = ClassTypes.UIParticle

local Unit_Item_Width = 68
--override
function EscortPeriSettlePanel:Awake()
    self:InitView()
end

--override
function EscortPeriSettlePanel:OnDestroy()

end

function EscortPeriSettlePanel:OnShow(data)
    self._iconBg1 = self:FindChildGO("Container_BG/Image_BG1")
    self._iconBg2 = self:FindChildGO("Container_BG/Image_BG2")
    GameWorld.AddIcon(self._iconBg1,13523)
    GameWorld.AddIcon(self._iconBg2,13523)
    self:AddEventListeners()
    self.data = data
    self:RefreshView()
end

function EscortPeriSettlePanel:OnClose()
    self._iconBg1=nil
    self._iconBg2=nil
    self:RemoveEventListeners()
end

function EscortPeriSettlePanel:AddEventListeners()

end

function EscortPeriSettlePanel:RemoveEventListeners()

end

function EscortPeriSettlePanel:InitView()
    self._okButton = self:FindChildGO("Container_TypeOne/Button_Ok")
    self._csBH:AddClick(self._okButton,function () self:OnOneOkButtonClick() end)
    self._cancelButton = self:FindChildGO("Container_TypeOne/Button_Cancel")
    self._csBH:AddClick(self._cancelButton,function () self:OnOneCancelButtonClick() end)
    self._okTwoButton = self:FindChildGO("Container_TypeTwo/Button_Ok")
    self._csBH:AddClick(self._okTwoButton,function () self:OnTwoOkButtonClick() end)

    self._fx = UIParticle.AddParticle(self.gameObject,"Container_Fx/fx_ui_30009")

    self._tpyeOne = self:FindChildGO("Container_TypeOne")
    self._tpyeTwo = self:FindChildGO("Container_TypeTwo")
    
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._tipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Reward/ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(TaskRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, 10, 1)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)
end

function EscortPeriSettlePanel:OnOneOkButtonClick()
    GUIManager.ClosePanel(PanelsConfig.EscortPeriSettle)
    EscortPeriManager:GotoAcceptEscort()
end

function EscortPeriSettlePanel:OnOneCancelButtonClick()
    GUIManager.ClosePanel(PanelsConfig.EscortPeriSettle)
end

function EscortPeriSettlePanel:OnTwoOkButtonClick()
    GUIManager.ClosePanel(PanelsConfig.EscortPeriSettle)
end

function EscortPeriSettlePanel:RefreshView()
    local count = EscortPeriManager:GetRemainCount()
    if count > 0 then
        self._tipsText.text = LanguageDataHelper.CreateContentWithArgs(74906, {["0"]=count})
        self._tpyeOne:SetActive(true)
        self._tpyeTwo:SetActive(false)
    else
        self._tipsText.text = LanguageDataHelper.GetContent(74905)
        self._tpyeOne:SetActive(false)
        self._tpyeTwo:SetActive(true)
    end
    self:ShowReward()
end

function EscortPeriSettlePanel:ShowReward()
    local isDouble = self.data[public_config.GUARD_GODDESS_IS_DOUBLE] == 1
    local rewards = {}
    local rewardsConfig = GuardGoddessDataHelper:GetRewardConfig(self.data[public_config.GUARD_GODDESS_KEY_ID], GameWorld.Player().level)
    for k,v in ipairs(rewardsConfig) do
        table.insert(rewards, TaskRewardData(v[1], v[2]))
    end
    if isDouble then
        for k,v in ipairs(rewardsConfig) do
            table.insert(rewards, TaskRewardData(v[1], v[2]))
        end
    end
    --居中处理
    if #rewards == 2 then
        self._list:SetPadding(0, 0, 0, Unit_Item_Width + 10)
    else
        self._list:SetPadding(0, 0, 0, 0)
    end
    self._list:SetDataList(rewards)
    if isDouble then
        self._infoText.text = LanguageDataHelper.CreateContentWithArgs(74904, {["0"]="200%"})
    else
        self._infoText.text = LanguageDataHelper.CreateContentWithArgs(74904, {["0"]="100%"})
    end
end

function EscortPeriSettlePanel:WinBGType()
    return PanelWinBGType.NoBG
end