-- SuitMatItem.lua
--套装打造材料Item类
local SuitMatItem = Class.SuitMatItem(ClassTypes.BaseComponent)
local public_config = GameWorld.public_config

SuitMatItem.interface = GameConfig.ComponentsConfig.Component
SuitMatItem.classPath = "Modules.ModuleEquip.ChildComponent.SuitMatItem"

require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

function SuitMatItem:Awake()
	self._mat = self:AddChildLuaUIComponent("Container_Mat",ItemGridEx)
	self._mat:ActivateTips()
	self._mat:SetShowName(false)
	self._imgLock = self:FindChildGO("Image_Lock")
end

function SuitMatItem:UpdateData(itemId,count)
	if itemId then
		self._imgLock:SetActive(false)
		self._mat:UpdateData(itemId,count)
	else
		self._imgLock:SetActive(true)
		self._mat:SetEmpty()
	end
end