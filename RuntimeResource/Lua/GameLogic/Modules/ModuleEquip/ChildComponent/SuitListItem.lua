-- SuitListItem.lua
--查看所有套装列表Item类
local SuitListItem = Class.SuitListItem(ClassTypes.BaseComponent)
local public_config = GameWorld.public_config

SuitListItem.interface = GameConfig.ComponentsConfig.Component
SuitListItem.classPath = "Modules.ModuleEquip.ChildComponent.SuitListItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper

require "Modules.ModuleTips.ChildComponent.EquipTipsSuitCom"
local EquipTipsSuitCom = ClassTypes.EquipTipsSuitCom

function SuitListItem:Awake()
	self._btnExpand = self:FindChildGO("Button_Expand")
	self._csBH:AddClick(self._btnExpand,function() self:OnExpand() end)
	self._imgContract = self:FindChildGO("Button_Expand/Image_Contract")
	self._imgExpand = self:FindChildGO("Button_Expand/Image_Expand")

	self._containerSuit = self:AddChildLuaUIComponent("Container_Suit",EquipTipsSuitCom)
	self._containerEquipGroup = self:FindChildGO("Container_Suit/Container_SuitEquipGroup")
	self._containerAttriGroup = self:FindChildGO("Container_Suit/Container_SuitAttriGroup")
end

function SuitListItem:SetIndex(index)
	self._index = index
end

function SuitListItem:SetExpand(isExpand)
	self._isExpand = isExpand
	if isExpand then
		self._height = self._containerSuit:GetHeight()
		self._containerEquipGroup:SetActive(true)
		self._containerAttriGroup:SetActive(true)
		self._imgContract:SetActive(false)
		self._imgExpand:SetActive(true)
	else
		self._height = 60
		self._containerEquipGroup:SetActive(false)
		self._containerAttriGroup:SetActive(false)
		self._imgContract:SetActive(true)
		self._imgExpand:SetActive(false)
	end
end

function SuitListItem:OnExpand()
	self:SetExpand(not self._isExpand)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_SUIT_LIST,self._index)
end

function SuitListItem:UpdateData(itemCfg,suitData,suitLevel)
	self._containerSuit:UpdateCom(itemCfg,suitData,suitLevel)
	self:SetExpand(false)
end

function SuitListItem:Hide()
	self._height = 0
end

function SuitListItem:GetHeight()
	return self._height
end