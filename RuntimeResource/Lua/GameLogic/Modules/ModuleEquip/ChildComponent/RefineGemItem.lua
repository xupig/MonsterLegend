-- RefineGemItem.lua
--宝石精炼界面宝石Item类
local RefineGemItem = Class.RefineGemItem(ClassTypes.BaseComponent)
local public_config = GameWorld.public_config

RefineGemItem.interface = GameConfig.ComponentsConfig.PointableComponent
RefineGemItem.classPath = "Modules.ModuleEquip.ChildComponent.RefineGemItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper

function RefineGemItem:Awake()
	self._iconContainer = self:FindChildGO("Container_Icon")

	self._imgLock = self:FindChildGO("Image_Lock")
end

function RefineGemItem:UpdateData(gemId)
	if gemId then
		self._iconContainer:SetActive(true)
		local icon = ItemDataHelper.GetIcon(gemId)
		GameWorld.AddIcon(self._iconContainer,icon)
	else
		self._iconContainer:SetActive(false)
	end
end

function RefineGemItem:SetLock(isLock)
	self._imgLock:SetActive(isLock)
	if isLock then
		self._iconContainer:SetActive(false)
	end
end