-- EquipListItem.lua
--装备界面的装备列表Item类
local UIListItem = ClassTypes.UIListItem
local EquipListItem = Class.EquipListItem(UIListItem)
local public_config = GameWorld.public_config

EquipListItem.interface = GameConfig.ComponentsConfig.PointableComponent
EquipListItem.classPath = "Modules.ModuleEquip.ChildComponent.EquipListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
require "Modules.ModuleEquip.ChildComponent.SmallGemItem"
local SmallGemItem = ClassTypes.SmallGemItem
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig
local public_config = GameWorld.public_config
local EquipManager = GameManager.EquipManager
local EquipDataHelper = GameDataHelper.EquipDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function EquipListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._iconContainer:ActivateSuitIcon(GameWorld.Player().equip_suit_data)
	
	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtName1 = self:GetChildComponent("Text_Name1",'TextMeshWrapper')
	self._txtNotActive = self:GetChildComponent("Text_NotActive",'TextMeshWrapper')

	self._imgMax = self:FindChildGO("Image_Max")
	self._imgRedPoint = self:FindChildGO("Image_RedPoint")
	
	self._containerGem = self:FindChildGO("Container_Gems")
	self._gems = {}
	local gemItemGOs = {}
	gemItemGOs[1] = self:FindChildGO("Container_Gems/Container_Gem")
    for i = 2, 6 do
        local go = self:CopyUIGameObject("Container_Gems/Container_Gem", "Container_Gems")
    	gemItemGOs[i] = go
    end

    for i=1,6 do
    	local go = gemItemGOs[i]
    	local item = UIComponentUtil.AddLuaUIComponent(go, SmallGemItem)
    	self._gems[i] = item
    end
    self._vipSlotLevel = GlobalParamsHelper.GetParamValue(469)

    self._imgSelected = self:FindChildGO("Image_Selected")
end

function EquipListItem:OnDestroy()
	self._iconContainer = nil
	self._txtName = nil
	self._txtName1 = nil
end

function EquipListItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end

--override
function EquipListItem:OnRefreshData(equipdata)
	self._type = equipdata[1]
	self._equipdata = equipdata[2]

	--LoggerHelper.Error("self._type"..self._type)
	self:UpdateData()
end

function EquipListItem:UpdateData()
	--list底部的item可能执行过OnRefreshData，_equipdata为空
	if self._equipdata == nil then
		return
	end
	local bagPos = self._equipdata.bagPos
	local washInfo = GameWorld.Player().equip_wash_info[bagPos]
	local strengthInfo = GameWorld.Player().equip_stren_info[bagPos]
	--local washSlotCount = washInfo[public_config.EQUIP_WASH_INFO_SLOT_COUNT] or 0
	local strengthLevel = 0
	local showRedPoint = false
	--强化
	if self._type == 1 then
		if strengthInfo then
			strengthLevel = strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL] or 0
			if strengthLevel >= self._equipdata.cfgData.strengthHighLv then
				self._imgMax:SetActive(true)
			else
				self._imgMax:SetActive(false)
			end
		else
			self._imgMax:SetActive(false)
		end
		showRedPoint = EquipManager:CheckSingleEquipStrength(self._equipdata)
	else
		self._imgMax:SetActive(false)
	end

	--洗炼
	if self._type == 2 then
		self._txtName1.text = self._equipdata:GetItemName()
		self._txtName.text = ""
		if washInfo then
			self._txtNotActive.text = ""
		else
			local args = LanguageDataHelper.GetArgsTable()
			args["0"] = EquipDataHelper:GetWashOpenLevel(bagPos)
			self._txtNotActive.text = LanguageDataHelper.CreateContentWithArgs(58782, args)
		end
	else
		if strengthLevel > 0 then
			local strengthStr = StringStyleUtil.GetColorStringWithColorId("+"..strengthLevel,ItemConfig.ItemAttriValue)
			self._txtName.text = self._equipdata:GetItemName()..strengthStr
		else
			self._txtName.text = self._equipdata:GetItemName()
		end
		
		self._txtName1.text = ""
		self._txtNotActive.text = ""
	end
	self._iconContainer:SetItemData(self._equipdata)
	
	--宝石
	if self._type == 3 or self._type == 4 then
		local gemInfo = GameWorld.Player().equip_gem_info[bagPos]
		self._containerGem:SetActive(true)
		local gemSlotCount = gemInfo[public_config.EQUIP_GEM_INFO_HOLE_COUNT]
		local vipLevel = GameWorld.Player().vip_level or 0
		local vipSlotOpen = false
		if vipLevel >= self._vipSlotLevel then
			vipSlotOpen = true
			gemSlotCount = gemSlotCount + 1
		end
		for i=1,6 do
			if i<= gemSlotCount then
				self._gems[i]:SetActive(true)
				if vipSlotOpen then
					if i == 1 then
						self._gems[i]:UpdateData(gemInfo[public_config.EQUIP_GEM_INFO_GEM_MAX1])
					else
						self._gems[i]:UpdateData(gemInfo[gemSlotCount + 1 -i])
					end
				else
					self._gems[i]:UpdateData(gemInfo[gemSlotCount + 1 -i])
				end
			else
				self._gems[i]:SetActive(false)
			end
		end
		if self._type == 3 then
			showRedPoint = EquipManager:GetGemInlayStateByType(self._equipdata.bagPos)
		end
	else
		self._containerGem:SetActive(false)
	end

	self._imgRedPoint:SetActive(showRedPoint)
end