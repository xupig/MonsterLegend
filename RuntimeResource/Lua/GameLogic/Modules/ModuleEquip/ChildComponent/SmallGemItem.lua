-- SmallGemItem.lua
--装备界面的装备列表Item里的小宝石图标类
local SmallGemItem = Class.SmallGemItem(ClassTypes.BaseComponent)
local public_config = GameWorld.public_config

SmallGemItem.interface = GameConfig.ComponentsConfig.PointableComponent
SmallGemItem.classPath = "Modules.ModuleEquip.ChildComponent.SmallGemItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper

function SmallGemItem:Awake()
	self._iconContainer = self:FindChildGO("Container_Icon")
end

function SmallGemItem:UpdateData(gemId)
	if gemId then
		self._iconContainer:SetActive(true)
		local icon = ItemDataHelper.GetIcon(gemId)
		GameWorld.AddIcon(self._iconContainer,icon)
	else
		self._iconContainer:SetActive(false)
	end
end
