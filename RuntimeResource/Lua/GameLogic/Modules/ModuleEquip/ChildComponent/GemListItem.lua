-- GemListItem.lua
--宝石镶嵌列表Item类
local UIListItem = ClassTypes.UIListItem
local GemListItem = Class.GemListItem(UIListItem)
local public_config = GameWorld.public_config

GemListItem.interface = GameConfig.ComponentsConfig.PointableComponent
GemListItem.classPath = "Modules.ModuleEquip.ChildComponent.GemListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

local ItemDataHelper = GameDataHelper.ItemDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper

function GemListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')

	self._txtAttris = {}
	self._txtAttris[1] = self:GetChildComponent("Text_Attri1",'TextMeshWrapper')
	self._txtAttris[2] = self:GetChildComponent("Text_Attri2",'TextMeshWrapper')
end

function GemListItem:OnRefreshData(gemData)
	local gemId = gemData.gemId
	local gemCount = gemData.gemNum
	local gemCfg = ItemDataHelper.GetItem(gemId)
	self._iconContainer:SetItem(gemId,gemCount)
	self._txtName.text = ItemDataHelper.GetItemNameWithColor(gemId)
	for i=1,2 do
		if gemCfg.gem_attribute[i] then
			local attriKey = gemCfg.gem_attribute[i][1]
			local attriValue = gemCfg.gem_attribute[i][2]
			self._txtAttris[i].text = AttriDataHelper:GetName(attriKey).."+"..AttriDataHelper:ConvertAttriValue(attriKey,attriValue)
		else
			self._txtAttris[i].text = ""
		end
	end
end