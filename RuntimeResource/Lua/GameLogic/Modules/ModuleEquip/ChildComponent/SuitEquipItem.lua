-- SuitEquipItem.lua
--套装装备列表Item类
local SuitEquipItem = Class.SuitEquipItem(ClassTypes.BaseComponent)

SuitEquipItem.interface = GameConfig.ComponentsConfig.PointableComponent
SuitEquipItem.classPath = "Modules.ModuleEquip.ChildComponent.SuitEquipItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local EquipManager = GameManager.EquipManager
--local bagData = PlayerManager.PlayerDataManager.bagData

function SuitEquipItem:Awake()
	local parent = self:FindChildGO("Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._equipIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)

   	self._imgTextBg = self:FindChildGO("Image_TextBg")
	self._txtCanMakeGO = self:FindChildGO("Image_TextBg/Text_CanMake")
	self._txtCanNotMakeGO = self:FindChildGO("Image_TextBg/Text_CanNotMake")

	self._imgLegend = self:FindChildGO("Image_Legend")
	self._imgWarLord = self:FindChildGO("Image_WarLord")

	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected:SetActive(false)

	self._imgTypeIcon = self:FindChildGO("Image_TypeIcon")
	self._imgRedPoint = self:FindChildGO("Image_RedPoint")
end

--suitLevel:当前部位
--pageLevel:当前页面，1传奇2战神
function SuitEquipItem:UpdateData(equipData,suitLevel,pageLevel)
	self._equipData = equipData
	if equipData then
		local suitId = equipData:GetSuitId()
		local suitCost = equipData:GetSuitCost()
		self._equipIcon:SetItemData(equipData)
		if suitLevel == 1 then
			self._imgLegend:SetActive(true)
			self._imgWarLord:SetActive(false)
			if pageLevel == 1 then
				self:SetCanMake(false,true,false)
			end

			if pageLevel == 2 then
				if suitId[2] then
					local matEnough = false
					local cost = equipData:GetSuitCost()[suitId[2]]
					if EquipManager:CheckSuitCost(cost) then
						matEnough = true
					end
					self:SetCanMake(true,false,matEnough)
				else
					self:SetCanMake(false,false,false)
				end
			end
		elseif suitLevel == 2 then
			self._imgLegend:SetActive(false)
			self._imgWarLord:SetActive(true)
			self:SetCanMake(false,true,false)
		else
			self._imgLegend:SetActive(false)
			self._imgWarLord:SetActive(false)

			if pageLevel == 1 then
				if suitId then
					local matEnough = false
					local cost = equipData:GetSuitCost()[suitId[1]]
					if EquipManager:CheckSuitCost(cost) then
						matEnough = true
					end
					self:SetCanMake(true,false,matEnough)
				else
					self:SetCanMake(false,false,false)
				end
			else
				self:SetCanMake(false,false,false)
			end
		end
		self._imgTypeIcon:SetActive(false)
	else
		self._equipIcon:SetItem(nil)
		self._imgRedPoint:SetActive(false)
		self._imgLegend:SetActive(false)
		self._imgWarLord:SetActive(false)
		self:SetCanMake(false,true)
		self._imgTypeIcon:SetActive(true)
	end
end

--显示是否可以打造
function SuitEquipItem:SetCanMake(canMake,showNone,showRedPoint)
	if showNone then
		self._imgTextBg:SetActive(false)
	else
		self._imgTextBg:SetActive(true)
		self._txtCanNotMakeGO:SetActive(not canMake)
		self._txtCanMakeGO:SetActive(canMake)
	end
	self._imgRedPoint:SetActive(showRedPoint)
end

function SuitEquipItem:SetIndex(index)
	self._index = index
end

function SuitEquipItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end

function SuitEquipItem:OnPointerDown()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SELECT_SUIT_EQUIP,self._index)
end

function SuitEquipItem:OnPointerUp()
	
end