-- SingleGemItem.lua
--宝石镶嵌界面单个宝石Item类
local SingleGemItem = Class.SingleGemItem(ClassTypes.BaseComponent)
local public_config = GameWorld.public_config

SingleGemItem.interface = GameConfig.ComponentsConfig.PointableComponent
SingleGemItem.classPath = "Modules.ModuleEquip.ChildComponent.SingleGemItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function SingleGemItem:Awake()
	self._iconContainer = self:FindChildGO("Container_Icon")
	self._imgAdd1 = self:FindChildGO("Image_Add1")
	self._imgAdd2 = self:FindChildGO("Image_Add2")

	self._txtLockTip = self:GetChildComponent("Text_LockTip","TextMeshWrapper")
	self._imgRed = self:FindChildGO("Image_Red")
	self._imgLock = self:FindChildGO("Image_Lock")
	self._txtAttris = {}
	self._txtAttris[1] = self:GetChildComponent("Text_Attri1",'TextMeshWrapper')
	self._txtAttris[2] = self:GetChildComponent("Text_Attri2",'TextMeshWrapper')
end

function SingleGemItem:UpdateData(gemId,slotType)
	if gemId then
		self._iconContainer:SetActive(true)
		local icon = ItemDataHelper.GetIcon(gemId)
		local gemCfg = ItemDataHelper.GetItem(gemId)
		GameWorld.AddIcon(self._iconContainer,icon)
		self._imgAdd1:SetActive(false)
		self._imgAdd2:SetActive(false)
		for i=1,2 do
			if gemCfg.gem_attribute[i] then
				local attriKey = gemCfg.gem_attribute[i][1]
				local attriValue = gemCfg.gem_attribute[i][2]
				self._txtAttris[i].text = AttriDataHelper:GetName(attriKey).."+"..AttriDataHelper:ConvertAttriValue(attriKey,attriValue)
			else
				self._txtAttris[i].text = ""
			end
		end
	else
		self._iconContainer:SetActive(false)
		if slotType == 1 then
			self._imgAdd1:SetActive(true)
			self._imgAdd2:SetActive(false)
		else
			self._imgAdd1:SetActive(false)
			self._imgAdd2:SetActive(true)
		end
		for i=1,2 do
			self._txtAttris[i].text = ""
		end
	end
end

function SingleGemItem:SetLock(isLock)
	self._isLock = isLock
	self._imgLock:SetActive(isLock)
	if isLock then
		self._iconContainer:SetActive(false)
		self._imgRed:SetActive(false)
		self._imgAdd1:SetActive(false)
		self._imgAdd2:SetActive(false)
		for i=1,2 do
			self._txtAttris[i].text = ""
		end
		if self._index ~= public_config.EQUIP_GEM_INFO_GEM_MAX1 then
			self._txtLockTip.text = LanguageDataHelper.CreateContent(58615,{["0"] = self._openGrade})
		else
			self._txtLockTip.text = ""
		end
	else
		self._txtLockTip.text = ""
	end
end

--是否有可装宝石，红点处理
function SingleGemItem:SetRed(isRed)
	self._imgRed:SetActive(isRed)
end

function SingleGemItem:SetIndex(index)
	self._index = index
	--开启的
	self._openGrade = GlobalParamsHelper.GetParamValue(468)[index]
end

function SingleGemItem:OnPointerDown()
	if not self._isLock then
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_OPEN_GEM_SELECT,self._index)
	end
end

function SingleGemItem:OnPointerUp()
	
end