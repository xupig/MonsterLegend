-- EquipWashAttriItem.lua
--洗炼属性Item类
local EquipWashAttriItem = Class.EquipWashAttriItem(ClassTypes.BaseComponent)
local public_config = GameWorld.public_config

EquipWashAttriItem.interface = GameConfig.ComponentsConfig.Component
EquipWashAttriItem.classPath = "Modules.ModuleEquip.ChildComponent.EquipWashAttriItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local EquipManager = GameManager.EquipManager
local AttriDataHelper = GameDataHelper.AttriDataHelper
local UIToggle = ClassTypes.UIToggle
local ItemConfig = GameConfig.ItemConfig
local EquipDataHelper = GameDataHelper.EquipDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function EquipWashAttriItem:Awake()
	self._btnOpenLock = self:FindChildGO("Button_OpenLock")
	self._csBH:AddClick(self._btnOpenLock, function()self:OnClickOpen() end)

	self._txtAttriName = self:GetChildComponent("Container_Attri/Text_AttriName","TextMeshWrapper")

	self._containerAttri = self:FindChildGO("Container_Attri")
	self._toggleLock = UIToggle.AddToggle(self.gameObject, "Container_Attri/Toggle")
	self._toggleCb = function(state) self:OnToggleValueChanged(state) end
    
    self._toggleLockWrapper = self._toggleLock:GetToggleWrapper()
    self._isLock = 0
    self._openCost = GlobalParamsHelper.GetParamValue(452)
end

function EquipWashAttriItem:UpdateData(equipPos,attriKey,attriValue,attriQulity)
	self._equipPos = equipPos
	--LoggerHelper.Error("EquipWashAttriItem:UpdateData"..equipPos..","..attriKey..","..attriValue..","..attriQulity)
	if attriKey then
		local washCfg = EquipDataHelper.GetWashData(equipPos)
		self._btnOpenLock:SetActive(false)
		self._containerAttri:SetActive(true)
		local colorId = ItemConfig.QualityTextMap[attriQulity]
		local result = AttriDataHelper:GetName(attriKey)
		local minValue = washCfg.attri[attriKey][1]
		local maxValue = washCfg.attri[attriKey][2]
		attriValue = AttriDataHelper:ConvertAttriValue(attriKey,attriValue)
		minValue = AttriDataHelper:ConvertAttriValue(attriKey,minValue)
		maxValue = AttriDataHelper:ConvertAttriValue(attriKey,maxValue)
		result = result.."+"..attriValue.." ["..minValue.."-"..maxValue.."]"
 		result = StringStyleUtil.GetColorStringWithColorId(result,colorId)
 		self._txtAttriName.text = result
	else
		if self._containerVIP then
			if GameWorld.Player().vip_level >= 6 then
				self._btnOpenLock:SetActive(true)
				self._containerVIP:SetActive(false)
			else
				self._btnOpenLock:SetActive(false)
				self._containerVIP:SetActive(true)
			end
		else
			self._btnOpenLock:SetActive(true)
		end
		
		self._containerAttri:SetActive(false)
	end
end

--设定锁定toggle是否能点击
function EquipWashAttriItem:SetToggleActive(isActive)
	if isActive then
		self._toggleLockWrapper.interactable = true
	else
		if self._isLock == 0 then
			self._toggleLockWrapper.interactable = false
		end
	end
end

function EquipWashAttriItem:SetVIP()
	if self._containerVIP == nil then
		self._containerVIP = self:FindChildGO("Container_VIP")
	end
end

--设定锁定状态
function EquipWashAttriItem:SetLock(value)
	self._toggleLock:SetOnValueChangedCB(nil)
	self._toggleLock:SetIsOn(value == 1)
    self._isLock = value
    self._toggleLock:SetOnValueChangedCB(self._toggleCb)
end

function EquipWashAttriItem:OnToggleValueChanged(state)
	if state then
		self._isLock = 1
	else
		self._isLock = 0
	end
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_WASH_LOCK_CHANGE)
end

function EquipWashAttriItem:GetLock()
	return self._isLock
end

function EquipWashAttriItem:OnClickOpen()
	local washInfo = GameWorld.Player().equip_wash_info[self._equipPos]
	local slotCount = washInfo[public_config.EQUIP_WASH_INFO_SLOT_COUNT]
	local cost = self._openCost[slotCount+1]
	local str = LanguageDataHelper.CreateContentWithArgs(58617,{["0"] = cost})
	GameManager.GUIManager.ShowMessageBox(2,str,function() self:SendRequest() end,nil)
end

function EquipWashAttriItem:SendRequest()
	EquipManager:RequestWashOpenSlot(self._equipPos)
end