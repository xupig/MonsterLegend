-- RefineMatItem.lua
--宝石精炼界面材料Item类
local RefineMatItem = Class.RefineMatItem(ClassTypes.BaseComponent)
local public_config = GameWorld.public_config

RefineMatItem.interface = GameConfig.ComponentsConfig.PointableComponent
RefineMatItem.classPath = "Modules.ModuleEquip.ChildComponent.RefineMatItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper

require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

function RefineMatItem:Awake()
	self._matItem = self:AddChildLuaUIComponent("Container_ItemGrid", ItemGridEx)
	self._matItem:SetShowName(false)
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
end

function RefineMatItem:UpdateData(matId,index)
	self._index = index
	self._matItem:UpdateData(matId,1)
	self._itemName = ItemDataHelper.GetItemNameWithColor(matId)
end

function RefineMatItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
	if isSelected then
		self._txtName.text = self._itemName
	else
		self._txtName.text = ""
	end
end

function RefineMatItem:OnPointerDown()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SELECT_GEM_REFINE_MAT,self._index)
end

function RefineMatItem:OnPointerUp()
	
end