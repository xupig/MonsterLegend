-- EquipStrengthView.lua
local EquipStrengthView = Class.EquipStrengthView(ClassTypes.EquipBaseView)

EquipStrengthView.interface = GameConfig.ComponentsConfig.Component
EquipStrengthView.classPath = "Modules.ModuleEquip.ChildView.EquipStrengthView"

require "UIComponent.Extend.AttriCom"
local AttriCom = ClassTypes.AttriCom
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require"Modules.ModuleEquip.ChildView.EquipExpTipsView"
local EquipExpTipsView = ClassTypes.EquipExpTipsView

local EquipManager = GameManager.EquipManager
local EquipDataHelper = GameDataHelper.EquipDataHelper
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local GUIManager = GameManager.GUIManager
local AttriDataHelper = GameDataHelper.AttriDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

function EquipStrengthView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function EquipStrengthView:InitCallBack()
	self._updateInfoCb = function ()
        self:UpdateEquipInfo()
    end

    self._expChangeCb = function (args)
    	self:OnExpChange(args)
    end

    self._strengthFail = function ()
    	if self._inAuto then
    		self:StopAuto()
    	end
    end
end

function EquipStrengthView:ShowModel()
	--LoggerHelper.Error("EquipStrengthView:ShowModel()")
	self._containerRune:SetActive(true)
end

function EquipStrengthView:HideModel()
	--LoggerHelper.Error("EquipStrengthView:HideModel()")
	self._containerRune:SetActive(false)
end

function EquipStrengthView:InitComps()
	self._containerRune = self:FindChildGO("Container_Rune")
	self._fxRune = UIParticle.AddParticle(self.gameObject,"Container_Rune/fx_ui_30005")

	self._fxStrength = UIParticle.AddParticle(self.gameObject,"Container_StrengthSuccess/fx_ui_qianghua")
	self._fxStrength:SetLifeTime(2)
	self._fxStrength:Stop()

	self._expProgress = UIScaleProgressBar.AddProgressBar(self.gameObject,"ProgressBar_Exp")
    self._expProgress:SetProgress(0)
    self._expProgress:ShowTweenAnimate(true)
    self._expProgress:SetMutipleTween(true)
    self._expProgress:SetIsResetToZero(false)
    self._expProgress:SetTweenTime(0.1)

    local parent = self:FindChildGO("Container_CurEquip")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._curEquipIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._curEquipIcon:ActivateTipsByItemData()

   	local parent1 = self:FindChildGO("Container_NextEquip")
   	local itemGo1 = GUIManager.AddItem(parent1, 1)
   	self._nextEquipIcon = UIComponentUtil.AddLuaUIComponent(itemGo1, ItemGrid)
	self._nextEquipIcon:ActivateTipsByItemData()

   	local parent2 = self:FindChildGO("Container_Gold")
   	local itemGo2 = GUIManager.AddItem(parent2, 1)
   	self._moneyIcon = UIComponentUtil.AddLuaUIComponent(itemGo2, ItemGrid)
   	
    self._txtCurLevel = self:GetChildComponent("Text_CurLevel","TextMeshWrapper")
    self._txtNextLevel = self:GetChildComponent("Text_NextLevel","TextMeshWrapper")
    self._txtNeedGold = self:GetChildComponent("Text_NeedGold","TextMeshWrapper")

    self._levelUpTip = self:FindChildGO("Image_LevelUp").transform
	self._levelUpTip.localScale = Vector3.zero
	self._tweenScaleLevelUp = self:AddChildComponent('Image_LevelUp', XGameObjectTweenScale)

	self._btnStrength = self:FindChildGO("Button_Strength")
    self._csBH:AddClick(self._btnStrength, function()self:Strength() end)

    self._btnAutoStrength = self:FindChildGO("Button_AutoStrength")
    self._csBH:AddClick(self._btnAutoStrength, function()self:AutoStrength() end)
    self._inAuto = false

    self._btnStopAuto = self:FindChildGO("Button_Stop")
    self._csBH:AddClick(self._btnStopAuto, function()self:StopAuto() end)
    self._btnStopAuto:SetActive(false)

    self._attriItems = {}
	local attriItemGOs = {}
	attriItemGOs[1] = self:FindChildGO("Container_AttriPreview/Container_AttriItem")
    attriItemGOs[2] = self:CopyUIGameObject("Container_AttriPreview/Container_AttriItem", "Container_AttriPreview")
    
    for i=1,2 do
    	local go = attriItemGOs[i]
    	local item = UIComponentUtil.AddLuaUIComponent(go, AttriCom)
    	self._attriItems[i] = item
    end

    self._tipsShowView = self:AddChildLuaUIComponent("Container_TipsShow", EquipExpTipsView)
    self._tipsShowView:InitCrit()
end

function EquipStrengthView:StopAuto()
	if self._inAuto then
        self._inAuto = false
        TimerHeap:DelTimer(self._autoTimer)
        self._btnStopAuto:SetActive(false)
        --能继续强化才显示
        if self._canStrength then
        	self._btnAutoStrength:SetActive(true)
        end
    end
end

function EquipStrengthView:AutoStrength()
	EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_equip_auto_strength_button")
	self._inAuto = true
	self._autoTimer = TimerHeap:AddSecTimer(0,0.5,0,function ()
		self._btnStopAuto:SetActive(true)
    	self._btnAutoStrength:SetActive(false)
        self:Strength()
    end)
end

function EquipStrengthView:Strength()
	--不够钱了
	if self._inAuto and 
		(self._needMoney > GameWorld.Player().money_gold or not self._canStrength) then
		self:StopAuto()
    end
	EquipManager:RequestStrength(self._equipPos)
end

function EquipStrengthView:OnEquipClick(selectedEquipData)
	self._lastLevel = nil
	self._dLevel = 0
	self._expProgress:ShowTweenAnimate(false)
	self._selectedEquip = selectedEquipData
	self:StopAuto()
	self:UpdateEquipInfo()
end

function EquipStrengthView:UpdateEquipInfo()
	self._equipPos = self._selectedEquip.bagPos
	local strengthInfo = GameWorld.Player().equip_stren_info[self._equipPos]
	local level = 0
	local exp = 0

	if strengthInfo then
		level = strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL] or 0
		exp = strengthInfo[public_config.EQUIP_STREN_INFO_EXP] or 0
	end
	local maxLevel = self._selectedEquip.cfgData.strengthHighLv
	level = math.min(level,maxLevel)
	local curCfg = EquipDataHelper.GetStrengthData(level)
	local attriInfo = curCfg.attris[self._equipPos]
	local nextCfg = EquipDataHelper.GetStrengthData(level+1)
	self._curEquipIcon:SetItemData(self._selectedEquip)
	self._txtCurLevel.text = "Lv."..level

	--还能强化
	if nextCfg and level < maxLevel then
		self._canStrength = true
		self._nextEquipIcon:SetItemData(self._selectedEquip)
		self._needMoney = curCfg.cost_gold
		self._goldCount = GameWorld.Player().money_gold
		self._moneyIcon:SetItem(public_config.MONEY_TYPE_GOLD)
		local moneyStr = StringStyleUtil.GetLongNumberStringEx(self._goldCount).."/"..StringStyleUtil.GetLongNumberStringEx(self._needMoney)
		if self._needMoney > self._goldCount then
			moneyStr = StringStyleUtil.GetColorStringWithColorId(moneyStr,ItemConfig.ItemForbidden)
		end
		self._txtNeedGold.text = moneyStr

		local nextAttriInfo = nextCfg.attris[self._equipPos]
		self._btnStrength:SetActive(true)
		if self._inAuto == false then
			self._btnAutoStrength:SetActive(true)
		end

		--计算经验变化
		--发生了等级变化
		if self._lastLevel and level > self._lastLevel then
			self._dLevel = level - self._lastLevel
			if self._dLevel > 0 then
				self._tweenScaleLevelUp:OnceOutQuad(1, 0.5, function()  
					self._levelUpTip.localScale = Vector3.zero
				end)
				
				self._fxStrength:ForceReorderAll()
				self._fxStrength:Play(true,true)
			end
		--发生了经验变化
		else
			self._expProgress:SetProgress(exp/curCfg.next_exp + self._dLevel)
			self._expProgress:SetProgressText(exp.."/"..curCfg.next_exp)
			self._expProgress:ShowTweenAnimate(true)
			self._dLevel = 0
		end
		
		--LoggerHelper.Error("SetProgress"..(exp/curCfg.next_exp + dLevel))
		
		self._lastLevel = level

		self._txtNextLevel.text = "Lv."..(level+1)
		for i=1,2 do
			local attriKey
			local attriValue
			local nextValue = nextAttriInfo[(i-1)*2+2]
			if level == 0 then
				attriKey = nextAttriInfo[(i-1)*2+1]
				attriValue = 0
			else
				attriKey = attriInfo[(i-1)*2+1]
				attriValue = attriInfo[(i-1)*2+2]
			end
			local dValue = nextValue - attriValue
			self._attriItems[i]:UpdateItem(AttriDataHelper:GetName(attriKey),
				AttriDataHelper:ConvertAttriValue(attriKey,attriValue),false,"+"..dValue)
		end

	--满级了
	else
		self._canStrength = false
		self._btnStrength:SetActive(false)
		self._btnAutoStrength:SetActive(false)
		self._expProgress:SetProgress(1)
		self._expProgress:SetProgressText("MAX")
		self._nextEquipIcon:SetItem(nil)
		self._moneyIcon:SetItem(nil)
		self._txtNeedGold.text = ""
		
		self._txtNextLevel.text = ""
		for i=1,2 do
			local attriKey = attriInfo[(i-1)*2+1]
			local attriValue = attriInfo[(i-1)*2+2]
			self._attriItems[i]:UpdateItem(AttriDataHelper:GetName(attriKey),AttriDataHelper:ConvertAttriValue(attriKey,attriValue),false,"")
		end
	end
end

function EquipStrengthView:OnExpChange(args)
	local add = args[1]
	if add > 0 then
        self._tipsShowView:ShowView(args)
    end
end

function EquipStrengthView:ShowView()
	self:ShowModel()
	self._fxStrength:Stop()
	EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleEquip.ChildView.EquipStrengthView")
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_STRENGTH_UPDATE,self._updateInfoCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_STRENGTH_EXP_CHANGE,self._expChangeCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_STRENGTH_FAIL,self._strengthFail)
end

function EquipStrengthView:CloseView()
	self:HideModel()
	self:StopAuto()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_STRENGTH_UPDATE,self._updateInfoCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_STRENGTH_EXP_CHANGE,self._expChangeCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_STRENGTH_FAIL,self._strengthFail)
end