-- EquipGemInlayView.lua
local EquipGemInlayView = Class.EquipGemInlayView(ClassTypes.EquipBaseView)

EquipGemInlayView.interface = GameConfig.ComponentsConfig.Component
EquipGemInlayView.classPath = "Modules.ModuleEquip.ChildView.EquipGemInlayView"

require "Modules.ModuleEquip.ChildComponent.GemListItem"
local GemListItem = ClassTypes.GemListItem

require "Modules.ModuleEquip.ChildComponent.SingleGemItem"
local SingleGemItem = ClassTypes.SingleGemItem

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

local UIList = ClassTypes.UIList
local EquipManager = GameManager.EquipManager
local public_config = GameWorld.public_config
local bagData = PlayerManager.PlayerDataManager.bagData
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local UIParticle = ClassTypes.UIParticle

function EquipGemInlayView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function EquipGemInlayView:InitCallBack()
	self._openGemSelectCB = function (selectedSlot)
		self:OnOpenSelect(selectedSlot)
	end

	self._updateInfoCB = function ()
		self:UpdateEquipInfo()
	end

	self._updateGemInBagCB = function ()
		self:UpdateGemInBag()
	end
end

function EquipGemInlayView:ShowModel()
	self._containerRune:SetActive(true)
end

function EquipGemInlayView:HideModel()
	self._containerRune:SetActive(false)
end

function EquipGemInlayView:InitComps()
	self._containerRune = self:FindChildGO("Container_Rune")
	self._fxRune = UIParticle.AddParticle(self.gameObject,"Container_Rune/fx_ui_30005")

	local parent = self:FindChildGO("Container_EquipIcon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._equipIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)

	self._btnGemCombine = self:FindChildGO("Button_GemCombine")
    self._csBH:AddClick(self._btnGemCombine, function()self:OnClickToCombine() end)
	
	self._gemItems = {}
    for i=1,6 do
    	local gemItem = self:AddChildLuaUIComponent("Container_GemItems/Container_GemItem"..i, SingleGemItem)
    	gemItem:SetIndex(i)
    	self._gemItems[i] = gemItem
    end
    self._slotTypes = GlobalParamsHelper.GetParamValue(760)
    self._vipSlotLevel = GlobalParamsHelper.GetParamValue(469)

	--选择宝石窗口
	self._containerSelectGem = self:FindChildGO("Container_SelectGem")

	self._btnCloseSelect = self:FindChildGO("Container_SelectGem/Button_Close")
    self._csBH:AddClick(self._btnCloseSelect, function()self:OnClickCloseSelect() end)

    self._btnUnload = self:FindChildGO("Container_SelectGem/Button_Unload")
    self._csBH:AddClick(self._btnUnload, function()self:OnUnload() end)

	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_SelectGem/List_Gem")
    self._listGem = list
    self._scrollView = scrollView
    self._listGem:SetItemType(GemListItem)
    self._listGem:SetPadding(0, 0, 0, 0)
    self._listGem:SetGap(0, 0)
    self._listGem:SetDirection(UIList.DirectionTopToDown, 1, 1)
    self._listGem:SetItemClickedCB(function (idx)
    	self:OnGemClickInlay(idx)
    end)

    self._containerSelectGem:SetActive(false)
end

function EquipGemInlayView:OnClickToCombine()
	local funcOpen = FunctionOpenDataHelper:CheckFunctionOpen(36)
	if funcOpen then
		--这里出现两个同时打开的Panel，重开二级菜单特殊处理
	    if GUIManager.reopenSubMainMenu > 0 then
	        GUIManager.reopenSubMainMenu = GUIManager.reopenSubMainMenu + 1
	    end
		GUIManager.ShowPanelByFunctionId(36)

	else
		local funcName = LanguageDataHelper.CreateContentWithArgs(FunctionOpenDataHelper:GetFunctionOpenName(funcId)) 
		local str = funcName..LanguageDataHelper.CreateContent(52072)
		GameManager.GUIManager.ShowText(1, str)
	end
end

function EquipGemInlayView:OnEquipClick(selectedEquipData)
	self._selectedEquip = selectedEquipData
	self:UpdateEquipInfo()
end

function EquipGemInlayView:UpdateGemInBag()
	self._gemListInBag = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetGemListByEquipPos(self._equipPos)
	self._listGem:SetDataList(self._gemListInBag)
end

function EquipGemInlayView:UpdateEquipInfo()
	self._equipIcon:SetItemData(self._selectedEquip)
	self._equipPos = self._selectedEquip.bagPos
	self:UpdateGemInBag()

	self._gemInfo = GameWorld.Player().equip_gem_info[self._equipPos]
	local gemSlotCount = self._gemInfo[public_config.EQUIP_GEM_INFO_HOLE_COUNT]

	local slotType = self._slotTypes[self._equipPos]

	for i=1,5 do
		if i <= gemSlotCount then
			self._gemItems[i]:SetLock(false)
			self._gemItems[i]:UpdateData(self._gemInfo[i],slotType)

			-- if #self._gemListInBag > 0 and self._gemInfo[i] == nil then
			-- 	self._gemItems[i]:SetRed(true)
			-- else
			-- 	self._gemItems[i]:SetRed(false)
			-- end
			self._gemItems[i]:SetRed(self:CheckRedPoint(i))
		else
			self._gemItems[i]:SetRed(false)
			self._gemItems[i]:SetLock(true)
		end
	end

	--vip宝石孔
	local vipLevel = GameWorld.Player().vip_level or 0
	if vipLevel >= self._vipSlotLevel then
		self._gemItems[6]:SetLock(false)
		self._gemItems[6]:UpdateData(self._gemInfo[6],slotType)
		-- if #self._gemListInBag > 0 and self._gemInfo[6] == nil then
		-- 	self._gemItems[6]:SetRed(true)
		-- else
		-- 	self._gemItems[6]:SetRed(false)
		-- end
		self._gemItems[6]:SetRed(self:CheckRedPoint(6))
	else
		self._gemItems[6]:SetRed(false)
		self._gemItems[6]:SetLock(true)
	end
end

function EquipGemInlayView:CheckRedPoint(slotIndex)
	local result = false
	local gemId = self._gemInfo[slotIndex]
	if #self._gemListInBag > 0 and gemId == nil then
		result = true
	end

	if gemId then
		for i=1,#self._gemListInBag do
			if self._gemListInBag[i].gemId > gemId then
				result = true
				break
			end
		end
	end
	return result
end

function EquipGemInlayView:OnOpenSelect(selectedSlot)
	if selectedSlot == 1 then
		EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_equip_gem_slot_button")
	end
	self._selectedSlot = selectedSlot
	self._containerSelectGem:SetActive(true)
	self._fxRune:Stop()
	--有宝石
	if self._gemInfo[self._selectedSlot] then
		self._btnUnload:SetActive(true)
	else
		self._btnUnload:SetActive(false)
	end
end

function EquipGemInlayView:OnClickCloseSelect()
	self._containerSelectGem:SetActive(false)
	self._fxRune:Play(true,true)
end

function EquipGemInlayView:OnGemClickInlay(idx)
	if idx == 0 then
		EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_equip_gem_inlay_list_button")
	end
	local gemTypeData = self._gemListInBag[idx + 1]
	local bagPos = gemTypeData.bagPosList[1]
	EquipManager:RequestGemLoad(self._equipPos, self._selectedSlot,bagPos)
	self:OnClickCloseSelect()
end

function EquipGemInlayView:OnUnload()
	EquipManager:RequestGemUnload(self._equipPos, self._selectedSlot)
	self:OnClickCloseSelect()
end

function EquipGemInlayView:ShowView()
	self:ShowModel()
	EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleEquip.ChildView.EquipGemInlayView")
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_OPEN_GEM_SELECT,self._openGemSelectCB)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_GEM_UPDATE,self._updateInfoCB)
	EventDispatcher:AddEventListener(GameEvents.Refresh_Gem_Inlay_Red_Point_State,self._updateGemInBagCB)
end

function EquipGemInlayView:CloseView()
	self:HideModel()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_OPEN_GEM_SELECT,self._openGemSelectCB)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_GEM_UPDATE,self._updateInfoCB)
	EventDispatcher:RemoveEventListener(GameEvents.Refresh_Gem_Inlay_Red_Point_State,self._updateGemInBagCB)
end