--装备升级小飘字组件
local EquipExpTipsView = Class.EquipExpTipsView(ClassTypes.BaseLuaUIComponent)

EquipExpTipsView.interface = GameConfig.ComponentsConfig.Component
EquipExpTipsView.classPath = "Modules.ModuleEquip.ChildView.EquipExpTipsView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local ItemConfig = GameConfig.ItemConfig
local ConvertStyle = GameMain.LuaFacade.ConvertStyle
local Max_Tips_Count = 3

function EquipExpTipsView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._queue = {}
    self:InitView()
end
--override
function EquipExpTipsView:OnDestroy() 

end
--data结构
function EquipExpTipsView:ShowView(data)
    self:RefreshView(data)
end

function EquipExpTipsView:InitView()
    self._tipsList = {}
    self._tipsTextList = {}
    self._showState = {}
    self._pos = {}
    self._tweenPosTipsList = {}
    for i=1, Max_Tips_Count do
        self._tipsList[i] = self:FindChildGO("Text_Tip"..i)
        self._tipsTextList[i] = self:GetChildComponent("Text_Tip"..i, "TextMeshWrapper")
        self._pos[i] = self._tipsList[i].transform.localPosition
        self._tweenPosTipsList[i] = self:AddChildComponent("Text_Tip"..i, XGameObjectTweenPosition)
        self:ResetItem(i)
    end
end

function EquipExpTipsView:InitCrit()
    self._critList = {}
    for i=1, Max_Tips_Count do
        self._critList[i] = self:FindChildGO("Text_Tip"..i.."/Image_Crit")
        self._critList[i]:SetActive(false)
    end
end

function EquipExpTipsView:GetItemId()
    for i=1, Max_Tips_Count do
        if self._showState[i] == 0 then
            self._showState[i] = 1
            return i
        end
    end
    return 0
end

function EquipExpTipsView:ResetItem(id)
    self._showState[id] = 0
    self._tipsList[id].transform.localPosition = self.HIDE_POSITION
end

function EquipExpTipsView:RefreshView(data)
    table.insert(self._queue, data)
    self:CheckShow()
end

function EquipExpTipsView:CheckShow()
    if #self._queue > 0 then
        local id = self:GetItemId()
        if id == 0 then
            return
        end
        local data = table.remove(self._queue, 1)
        local add = data[1]
        local crit = data[2]
        
        if self._critList then
            if crit == 1 then
                self._critList[id]:SetActive(true)
                self._tipsTextList[id].text = ConvertStyle(string.format("$(%d,%s)",ItemConfig.ItemForbidden,add))
            else
                self._critList[id]:SetActive(false)
                self._tipsTextList[id].text = "+"..add
            end
        else
            self._tipsTextList[id].text = "+"..add
        end
        self._tipsList[id].transform.localPosition = self._pos[id]
        local moveTo = Vector3.New(0, 50, 0) + self._pos[id]
        self._tweenPosTipsList[id]:OnceOutQuad(moveTo, 1, function()
            self:ResetItem(id)
            self:CheckShow()
        end)
    end
end