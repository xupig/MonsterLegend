-- EquipGemRefineView.lua
local EquipGemRefineView = Class.EquipGemRefineView(ClassTypes.EquipBaseView)

EquipGemRefineView.interface = GameConfig.ComponentsConfig.Component
EquipGemRefineView.classPath = "Modules.ModuleEquip.ChildView.EquipGemRefineView"

local EquipManager = GameManager.EquipManager
local EquipDataHelper = GameDataHelper.EquipDataHelper
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local GUIManager = GameManager.GUIManager
local bagData = PlayerManager.PlayerDataManager.bagData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local TipsManager = GameManager.TipsManager
local UIParticle = ClassTypes.UIParticle

require "UIComponent.Extend.AttriCom"
local AttriCom = ClassTypes.AttriCom

require "Modules.ModuleEquip.ChildComponent.RefineGemItem"
local RefineGemItem = ClassTypes.RefineGemItem

require "Modules.ModuleEquip.ChildComponent.RefineMatItem"
local RefineMatItem = ClassTypes.RefineMatItem

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

function EquipGemRefineView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function EquipGemRefineView:InitCallBack()
	self._updateInfoCB = function ()
		self:UpdateEquipInfo()
	end

	self._selectMatCB = function (index)
		self:SelectMat(index)
	end
end

function EquipGemRefineView:ShowModel()
	self._containerRune:SetActive(true)
end

function EquipGemRefineView:HideModel()
	self._containerRune:SetActive(false)
end

function EquipGemRefineView:InitComps()
	self._containerRune = self:FindChildGO("Container_Rune")
	self._fxRune = UIParticle.AddParticle(self.gameObject,"Container_Rune/fx_ui_30005")
	self._fxSuccess = UIParticle.AddParticle(self.gameObject,"Container_RefineSuccess/fx_ui_30032")
	self._fxSuccess:Stop()

	local parent = self:FindChildGO("Container_EquipIcon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._equipIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)

   	self._txtRefineLevel = self:GetChildComponent("Text_Level","TextMeshWrapper")
   	self._dLevel = 0

	self._btnRefine = self:FindChildGO("Button_Refine")
    self._csBH:AddClick(self._btnRefine, function()self:Refine() end)

    self._btnAutoRefine = self:FindChildGO("Button_OneClickRefine")
    self._csBH:AddClick(self._btnAutoRefine, function()self:AutoRefine() end)
    self._inAuto = false

    self._btnStopAuto = self:FindChildGO("Button_Stop")
    self._csBH:AddClick(self._btnStopAuto, function()self:StopAutoRefine() end)
    self._btnStopAuto:SetActive(false)

    self._expProgress = UIScaleProgressBar.AddProgressBar(self.gameObject,"ProgressBar_Exp")
    self._expProgress:SetProgress(0)
    self._expProgress:ShowTweenAnimate(true)
    self._expProgress:SetMutipleTween(true)
    self._expProgress:SetIsResetToZero(false)
    self._expProgress:SetTweenTime(0.3)

    --材料
    self._matIds = GlobalParamsHelper.GetParamValue(470)
	self._slotTypes = GlobalParamsHelper.GetParamValue(760)
    self._matItems = {}
	local matItemGOs = {}
	matItemGOs[1] = self:FindChildGO("Container_Materials/Container_MatItem")
    for i = 2, 3 do
        local go = self:CopyUIGameObject("Container_Materials/Container_MatItem", "Container_Materials")
    	matItemGOs[i] = go
    end

    for i=1,3 do
    	local go = matItemGOs[i]
    	local item = UIComponentUtil.AddLuaUIComponent(go, RefineMatItem)
    	--item:UpdateData(self._matIds[i],i)
    	self._matItems[i] = item
    end

    --身上宝石
    self._gemItems = {}
    for i=1,6 do
    	local gemItem = self:AddChildLuaUIComponent("Container_GemItems/Container_GemItem"..i, RefineGemItem)
    	self._gemItems[i] = gemItem
    end
    self._vipSlotLevel = GlobalParamsHelper.GetParamValue(469)

    --属性显示
    self._gemAttris = {}
    for i=1,2 do
    	local gemAttri = self:AddChildLuaUIComponent("Container_Attris/Container_AttriItem"..i, AttriCom)
    	self._gemAttris[i] = gemAttri
    end
    
    self._gemRefineAttris = {}
    for i=1,2 do
    	local gemRefineAttri = self:AddChildLuaUIComponent("Container_AttrisRefine/Container_AttriItem"..i, AttriCom)
    	self._gemRefineAttris[i] = gemRefineAttri
    end

    self._txtGemCurRate = self:GetChildComponent("Container_GemAttriRate/Text_AttriValue","TextMeshWrapper")
    self._txtGemDiffRate = self:GetChildComponent("Container_GemAttriRate/Text_AddValue","TextMeshWrapper")
end

function EquipGemRefineView:Refine()
	if self._selectedMatId then
		local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._selectedMatId)
		if pos > 0 then
			EquipManager:RequestGemRefine(self._equipPos, pos)
		--不够道具了
		elseif self._inAuto then
			self:StopAutoRefine()
		end
	end
end

function EquipGemRefineView:AutoRefine()
	self._inAuto = true
	self._autoTimer = TimerHeap:AddSecTimer(0,0.5,0,function ()
		self._btnStopAuto:SetActive(true)
    	self._btnAutoRefine:SetActive(false)
        self:Refine()
    end)
end

function EquipGemRefineView:StopAutoRefine()
	if self._inAuto then
        self._inAuto = false
        TimerHeap:DelTimer(self._autoTimer)
        self._btnStopAuto:SetActive(false)
        self._btnAutoRefine:SetActive(true)
    end
end

function EquipGemRefineView:OnEquipClick(selectedEquipData)
	self._lastLevel = nil
	self._dLevel = 0
	self._selectedMatId = nil
	self._selectedEquip = selectedEquipData

	self:UpdateMaterial()
	self:UpdateEquipInfo()
end

function EquipGemRefineView:UpdateMaterial()
	self._refineMatType = self._slotTypes[self._selectedEquip:GetType()]
	for i=1,3 do
    	local matId = self._matIds[self._refineMatType][(i-1)*2+1]
    	self._matItems[i]:UpdateData(matId,i)
    end
    self:SelectMat(1)
end

function EquipGemRefineView:UpdateEquipInfo()
	self._equipIcon:SetItemData(self._selectedEquip)
	self._equipPos = self._selectedEquip.bagPos
	self._gemInfo = GameWorld.Player().equip_gem_info[self._equipPos]
	local gemSlotCount = self._gemInfo[public_config.EQUIP_GEM_INFO_HOLE_COUNT]
	for i=1,5 do
		if i <= gemSlotCount then
			self._gemItems[i]:SetLock(false)
			self._gemItems[i]:UpdateData(self._gemInfo[i])
		else
			self._gemItems[i]:SetLock(true)
		end
	end
	--vip宝石孔
	local vipLevel = GameWorld.Player().vip_level or 0
	if vipLevel >= self._vipSlotLevel then
		self._gemItems[6]:SetLock(false)
		self._gemItems[6]:UpdateData(self._gemInfo[6])
	else
		self._gemItems[6]:SetLock(true)
	end

	--精炼信息
	local refineLevel = self._gemInfo[public_config.EQUIP_GEM_INFO_REFINE_LEVEL] or 0
	local refineExp = self._gemInfo[public_config.EQUIP_GEM_INFO_REFINE_EXP] or 0
	local refineCfg = EquipDataHelper.GetGemRefineData(refineLevel)
	local nextCfg = EquipDataHelper.GetGemRefineData(refineLevel+1)
	local curRate = refineCfg.attri_add
	local dRate = nextCfg.attri_add - curRate

	--计算经验变化
	if self._lastLevel and refineLevel > self._lastLevel then
		self._dLevel = refineLevel - self._lastLevel
		if self._dLevel > 0 then
			self:PlayFx()
		end
	else
		self._expProgress:SetProgress(refineExp/refineCfg.next_exp + self._dLevel)
		self._expProgress:SetProgressText(refineExp.."/"..refineCfg.next_exp)
		self._dLevel = 0
	end
	--LoggerHelper.Error("refineExp"..refineExp.."/"..refineCfg.next_exp.."/"..self._dLevel)
	
	self._lastLevel = refineLevel

	self._txtRefineLevel.text = tostring(refineLevel)

	local attriMap = {}
	local h = 1
	for i=1,6 do
		local gemId = self._gemInfo[i]
		if gemId  then
			local gemCfg = ItemDataHelper.GetItem(gemId)
			local gemAttribute = gemCfg.gem_attribute
			for i=1,#gemAttribute do
				if attriMap[i] == nil then
					attriMap[i] = {}
				end
				attriMap[i][1] = gemAttribute[i][1]
				if attriMap[i][2] then
					attriMap[i][2] = attriMap[i][2] + gemAttribute[i][2]
				else
					attriMap[i][2] = gemAttribute[i][2]
				end
			end
		end
	end

	for i=1,#attriMap do
		self._gemAttris[h]:SetActive(true)
		self._gemAttris[h]:UpdateItem(AttriDataHelper:GetName(attriMap[i][1]),
			AttriDataHelper:ConvertAttriValue(attriMap[i][1],attriMap[i][2]))
		h = h+1
	end
	
	for i=h,2 do
		self._gemAttris[i]:SetActive(false)
	end

	h = 1
	for i=1,#attriMap do
		self._gemRefineAttris[h]:SetActive(true)
		local addition = math.ceil(dRate/100 * attriMap[i][2]) 
		local v1 = math.ceil(curRate/100 * attriMap[i][2])
		self._gemRefineAttris[h]:UpdateItem(AttriDataHelper:GetName(attriMap[i][1]),
			AttriDataHelper:ConvertAttriValue(attriMap[i][1],v1),false,addition)
		h = h+1
	end

	for i=h,2 do
		self._gemRefineAttris[i]:SetActive(false)
	end

	self._txtGemCurRate.text = "+"..curRate.."%"
	self._txtGemDiffRate.text = "+"..dRate.."%"
end

function EquipGemRefineView:SelectMat(index)
	local matId = self._matIds[self._refineMatType][(index-1)*2+1]
	if matId == self._selectedMatId then
		TipsManager:ShowItemTipsById(matId)
		return
	end
	if self._selectedMatItem then
		self._selectedMatItem:SetSelected(false)
	end
	self._selectedMatId = matId
	self._selectedMatItem = self._matItems[index]
	self._selectedMatItem:SetSelected(true)
end

function EquipGemRefineView:PlayFx()
	self._fxSuccess:Play(true,true)
	--洗炼声效
	GameWorld.PlaySound(24)
end

function EquipGemRefineView:ShowView()
	self:ShowModel()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_GEM_UPDATE,self._updateInfoCB)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_SELECT_GEM_REFINE_MAT,self._selectMatCB)
end

function EquipGemRefineView:CloseView()
	self:HideModel()
	self:StopAutoRefine()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_GEM_UPDATE,self._updateInfoCB)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_SELECT_GEM_REFINE_MAT,self._selectMatCB)
end