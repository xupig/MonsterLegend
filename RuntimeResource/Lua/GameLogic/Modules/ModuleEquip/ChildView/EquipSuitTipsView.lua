-- EquipSuitTipsView.lua
local EquipSuitTipsView = Class.EquipSuitTipsView(ClassTypes.EquipBaseView)

EquipSuitTipsView.interface = GameConfig.ComponentsConfig.Component
EquipSuitTipsView.classPath = "Modules.ModuleEquip.ChildView.EquipSuitTipsView"

function EquipSuitTipsView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function EquipSuitTipsView:InitCallBack()
end

function EquipSuitTipsView:InitComps()
end

