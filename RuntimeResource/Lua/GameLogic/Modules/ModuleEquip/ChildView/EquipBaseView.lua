-- EquipBaseView.lua
--装备模块View的基类
local EquipBaseView = Class.EquipBaseView(ClassTypes.BaseLuaUIComponent)

EquipBaseView.interface = GameConfig.ComponentsConfig.Component
EquipBaseView.classPath = "Modules.ModuleEquip.ChildView.EquipBaseView"

--for override
function EquipBaseView:ShowView()
end

--for override
function EquipBaseView:CloseView()
end

--for override
function EquipBaseView:OnEquipClick(selectedEquipData)
end

function EquipBaseView:ShowModel()
end

function EquipBaseView:HideModel()
end