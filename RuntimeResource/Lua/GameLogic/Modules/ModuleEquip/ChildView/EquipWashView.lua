-- EquipWashView.lua
local EquipWashView = Class.EquipWashView(ClassTypes.EquipBaseView)

EquipWashView.interface = GameConfig.ComponentsConfig.Component
EquipWashView.classPath = "Modules.ModuleEquip.ChildView.EquipWashView"

require "Modules.ModuleEquip.ChildComponent.EquipWashAttriItem"
local EquipWashAttriItem = ClassTypes.EquipWashAttriItem

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

require "UIComponent.Extend.SimpleCurrencyCom"
local SimpleCurrencyCom = ClassTypes.SimpleCurrencyCom

local EquipManager = GameManager.EquipManager
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local XArtNumber = GameMain.XArtNumber
local UIToggle = ClassTypes.UIToggle
local BaseUtil = GameUtil.BaseUtil
local UIParticle = ClassTypes.UIParticle
local qualityMap = {1,3,4,5,6}
local bagData = PlayerManager.PlayerDataManager.bagData

function EquipWashView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function EquipWashView:InitCallBack()
	self._onLockChangeCB = function ()
		self:OnLockChange()
	end

	self._onUpdateWashCB = function ()
		self:PlayFx()
		self:UpdateEquipInfo()
	end
end

function EquipWashView:ShowModel()
	self._containerRune:SetActive(true)
end

function EquipWashView:HideModel()
	self._containerRune:SetActive(false)
end

function EquipWashView:InitComps()
	self._containerRune = self:FindChildGO("Container_Rune")
	self._fxRune = UIParticle.AddParticle(self.gameObject,"Container_Rune/fx_ui_30005")
	self._fxWash = UIParticle.AddParticle(self.gameObject,"Container_WashSuccess/fx_ui_30032")
	self._fxWash:Stop()

	self._containerNotActive = self:FindChildGO("Container_NotActive")
	self._containerWash = self:FindChildGO("Container_Wash")

	self._btnWash = self:FindChildGO("Container_Wash/Button_Wash")
    self._csBH:AddClick(self._btnWash, function()self:OnClickWash() end)

    self._btnAddMat = self:FindChildGO("Container_Wash/Button_AddMat")
    self._csBH:AddClick(self._btnAddMat, function()self:AddMat() end)

    self._btnCancelMat = self:FindChildGO("Container_Wash/Button_CancelMat")
    self._csBH:AddClick(self._btnCancelMat, function()self:CancelMat() end)

    --当前装备icon
    local parent = self:FindChildGO("Container_Wash/Container_EquipIcon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._equipIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._equipIcon:ActivateTipsByItemData()

   	self._freeTimeMax = GlobalParamsHelper.GetParamValue(453)
   	self._txtTimes = self:GetChildComponent("Container_Wash/Text_Times","TextMeshWrapper")
   	self._leftFreeTimesCount = 0 --剩余免费次数

   	-----------------------属性部分------------------------------------------
   	self._fpNumber = self:AddChildComponent('Container_Wash/Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self._attriItems = {}

    for i=1,5 do
    	local item = self:AddChildLuaUIComponent("Container_Wash/Container_Attris/Container_AttriItem"..i, EquipWashAttriItem)
    	self._attriItems[i] = item
    end

   	------------------------选择材料---------------------------------
   	self._containerSelectMat = self:FindChildGO("Container_SelectMat")

   	self._btnCloseSelect = self:FindChildGO("Container_SelectMat/Button_Close")
   	self._csBH:AddClick(self._btnCloseSelect, function()self:OnCloseSelectMat() end)

   	self._mat1Id = GlobalParamsHelper.GetParamValue(454)
   	self._btnMat1 = self:FindChildGO("Container_SelectMat/Item1/Button_Bg")
   	self._csBH:AddClick(self._btnMat1, function()self:SelectMat(self._mat1Id) end)
   	self._mat1Icon = self:AddChildLuaUIComponent("Container_SelectMat/Item1/Container_MatIcon",ItemGridEx)
   	self._mat1Icon:ActivateTips()
   	self._mat1Icon:UpdateData(self._mat1Id,0)
   	self._mat1Desc = self:GetChildComponent("Container_SelectMat/Item1/Text_Desc","TextMeshWrapper")
   	self._mat1Desc.text = LanguageDataHelper.CreateContent(58604)

   	self._mat2Id = GlobalParamsHelper.GetParamValue(455)
   	self._btnMat2 = self:FindChildGO("Container_SelectMat/Item2/Button_Bg")
   	self._csBH:AddClick(self._btnMat2, function()self:SelectMat(self._mat2Id) end)
   	self._mat2Icon = self:AddChildLuaUIComponent("Container_SelectMat/Item2/Container_MatIcon",ItemGridEx)
   	self._mat2Icon:ActivateTips()
   	self._mat2Icon:UpdateData(self._mat2Id,0)
   	self._mat2Desc = self:GetChildComponent("Container_SelectMat/Item2/Text_Desc","TextMeshWrapper")
   	self._mat2Desc.text = LanguageDataHelper.CreateContent(58605)

   	------------------------消耗材料---------------------------------
	self._toggleUseDiamond = UIToggle.AddToggle(self.gameObject, "Container_Wash/Toggle_UseDiamond")
	self._toggleUseDiamond:SetIsOn(false)
    self._toggleUseDiamond:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)
    self._useDiamond = 0

    self._txtMatTip = self:GetChildComponent("Container_Wash/Text_MatTips","TextMeshWrapper")
    
    --必出紫色消耗
    self._toggleDiamondCount = GlobalParamsHelper.GetParamValue(451)
    self._txtToggleDiamond = self:GetChildComponent("Container_Wash/Toggle_UseDiamond/Label","TextMeshWrapper")
    self._toggleDiamondIcon = self:FindChildGO("Container_Wash/Toggle_UseDiamond/Container_DiamondIcon")
    local diamonIcon = ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS)
    GameWorld.AddIcon(self._toggleDiamondIcon,diamonIcon)

    self._baseCost = GlobalParamsHelper.GetParamValue(447)
    self._baseMatIcon = self:AddChildLuaUIComponent("Container_Wash/Container_Mat",ItemGridEx)
   	self._baseMatIcon:ActivateTips()
   	self._baseMatIcon:SetShowName(false)
   	for k,v in pairs(self._baseCost) do
   		self._baseMatId = k
   	end
   	self._baseMatIcon:UpdateData(self._baseMatId,0)

   	self._additionMatIcon = self:AddChildLuaUIComponent("Container_Wash/Container_AdditionMat",ItemGridEx)
   	self._additionMatIcon:ActivateTips()
   	self._additionMatIcon:SetShowName(false)

   	self._diamondCom = self:AddChildLuaUIComponent("Container_Wash/Container_Diamond", SimpleCurrencyCom)
   	self._diamondCom:InitComps(public_config.MONEY_TYPE_COUPONS)
   	self._diamondCom:SetMoneyNum(0)
   	
   	self._baseLockExCost = {}
	self._baseLockExCost[1] = GlobalParamsHelper.GetParamValue(448)
	self._baseLockExCost[2] = GlobalParamsHelper.GetParamValue(449)
	self._baseLockExCost[3] = GlobalParamsHelper.GetParamValue(450)
	self._baseLockExCost[4] = GlobalParamsHelper.GetParamValue(978)

	self._mat1LockExCost = {}
	self._mat1LockExCost[1] = GlobalParamsHelper.GetParamValue(461)[self._mat1Id]
	self._mat1LockExCost[2] = GlobalParamsHelper.GetParamValue(462)[self._mat1Id]
	self._mat1LockExCost[3] = GlobalParamsHelper.GetParamValue(463)[self._mat1Id]
	self._mat1LockExCost[4] = GlobalParamsHelper.GetParamValue(979)[self._mat1Id]

	self._mat2LockExCost = {}
	self._mat2LockExCost[1] = GlobalParamsHelper.GetParamValue(464)[self._mat2Id]
	self._mat2LockExCost[2] = GlobalParamsHelper.GetParamValue(465)[self._mat2Id]
	self._mat2LockExCost[3] = GlobalParamsHelper.GetParamValue(466)[self._mat2Id]
	self._mat2LockExCost[4] = GlobalParamsHelper.GetParamValue(980)[self._mat2Id]
end

function EquipWashView:OnClickWash()
	local matPos = 0
	if self._selectedMatId > 0 then
		matPos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._selectedMatId)
	end
	local str = self._equipPos..","..matPos..","..self._useDiamond
	for i=1,5 do
		str = str..","..self._attriItems[i]:GetLock()
	end
	EquipManager:RequestWash(str)
end

function EquipWashView:OnCloseSelectMat()
	self._containerSelectMat:SetActive(false)
end

function EquipWashView:AddMat()
	self._containerSelectMat:SetActive(true)
	if self._lockCount > 0 then
		local exCost1 = self._mat1LockExCost[self._lockCount]
		local exCost2 = self._mat2LockExCost[self._lockCount]
		self._mat1Icon:UpdateCount(1+exCost1)
		self._mat2Icon:UpdateCount(1+exCost2)
	else
		self._mat1Icon:UpdateCount(1)
		self._mat2Icon:UpdateCount(1)
	end
end

function EquipWashView:CancelMat()
	self._containerSelectMat:SetActive(false)
	self._btnAddMat:SetActive(true)
	self._btnCancelMat:SetActive(false)
	self._selectedMatId = 0
	self._txtMatTip.text = ""
	self._toggleUseDiamond:SetActive(true)
	self:UpdateCost()
end

function EquipWashView:SelectMat(selectedMatId)
	self._containerSelectMat:SetActive(false)
	self._btnAddMat:SetActive(false)
	self._btnCancelMat:SetActive(true)
	if selectedMatId == self._mat1Id then
		self._txtMatTip.text = LanguageDataHelper.CreateContent(58604)
	else
		self._txtMatTip.text = LanguageDataHelper.CreateContent(58605)
	end
	
	self._toggleUseDiamond:SetActive(false)
	self._toggleUseDiamond:SetIsOn(false)
	self._selectedMatId = selectedMatId
	self:UpdateCost()
end

function EquipWashView:OnToggleValueChanged(state)
	if state then
		self._useDiamond = 1
	else
		self._useDiamond = 0
	end
	self:UpdateDiamondCost()
end

function EquipWashView:OnEquipClick(selectedEquipData)
	self._selectedEquip = selectedEquipData
	self._lockState = {0,0,0,0,0}
	self:UpdateEquipInfo()
end

function EquipWashView:UpdateEquipInfo()
	self._equipPos = self._selectedEquip.bagPos
	local washInfo = GameWorld.Player().equip_wash_info[self._equipPos]

	--未激活
	if washInfo == nil then
		self._containerNotActive:SetActive(true)
		self._containerWash:SetActive(false)
		return
	else
		self._containerNotActive:SetActive(false)
		self._containerWash:SetActive(true)
	end

	self._equipIcon:SetItemData(self._selectedEquip)
	self._slotCount = washInfo[public_config.EQUIP_WASH_INFO_SLOT_COUNT] or 0

	for i=1,5 do
		local item = self._attriItems[i]
		if i <= self._slotCount then
			local baseIndex = (i-1)*3
			local attriKey = washInfo[baseIndex+1]
			local attriValue = washInfo[baseIndex+2]
			local attriQuality = qualityMap[washInfo[baseIndex+3]]

			item:UpdateData(self._equipPos,attriKey,attriValue,attriQuality)
			item:SetLock(self._lockState[i])
		else
			--VIP6槽位
			if i == 5 then
				item:SetVIP()
			end
			item:UpdateData(self._equipPos)
		end
	end

	local fightPower = self._selectedEquip:GetWashScore()
	self._fpNumber:SetNumber(fightPower)

	self:UpdateTimes()
	self:UpdateCost()
	self:OnLockChange()
end

function EquipWashView:UpdateTimes()
	local dayCount = GameWorld.Player().wash_cur_day_count or 0
	self._leftFreeTimesCount = math.max(0,self._freeTimeMax - dayCount)
	self._txtTimes.text = self._leftFreeTimesCount.."/"..self._freeTimeMax
end

function EquipWashView:OnLockChange()
	local lockCount = 0
	for i=1,4 do
		local ls = self._attriItems[i]:GetLock()
		lockCount = ls + lockCount
		self._lockState[i] = ls
	end
	for i=1,4 do
		self._attriItems[i]:SetToggleActive(lockCount < self._slotCount - 1)
	end
	self._lockCount = lockCount
	self:UpdateCost()
end

function EquipWashView:UpdateCost()
	--额外材料的消耗
	if self._selectedMatId == 0 then
		self._additionMatIcon:SetActive(false)
	else
		self._additionMatIcon:SetActive(true)
		if self._selectedMatId == self._mat1Id then
			if self._lockCount > 0 then
				local exCost = self._mat1LockExCost[self._lockCount]
				self._additionMatIcon:UpdateData(self._selectedMatId,1+exCost)
			else
				self._additionMatIcon:UpdateData(self._selectedMatId,1)
			end
		end

		if self._selectedMatId == self._mat2Id then
			if self._lockCount > 0 then
				local exCost = self._mat2LockExCost[self._lockCount]
				self._additionMatIcon:UpdateData(self._selectedMatId,1+exCost)
			else
				self._additionMatIcon:UpdateData(self._selectedMatId,1)
			end
		end
	end

	--基础材料消耗
	local baseMatCount = self._baseCost[self._baseMatId]
	if self._lockCount > 0  then
		baseMatCount = self._baseLockExCost[self._lockCount][self._baseMatId] + baseMatCount
	end

	self._baseMatIcon:UpdateCount(baseMatCount)

	--钻石消耗
	self:UpdateDiamondCost()
end

function EquipWashView:UpdateDiamondCost()
	local diamonCount = 0
	-- if self._lockCount > 0 then
	-- 	diamonCount = diamonCount + self._baseLockExCost[self._lockCount][public_config.MONEY_TYPE_COUPONS]
	-- end

	if self._useDiamond > 0 then
		diamonCount = diamonCount + self._toggleDiamondCount[self._lockCount]
	end

	self._txtToggleDiamond.text = LanguageDataHelper.CreateContent(300)..self._toggleDiamondCount[self._lockCount]

	self._diamondCom:SetMoneyNum(diamonCount)
end

function EquipWashView:PlayFx()
	self._fxWash:Play(true,true)
	--洗炼声效
	GameWorld.PlaySound(24)
end

function EquipWashView:ShowView()
	self._lockCount = 0 --锁定的总数
	self._equipPos = 0
	self:CancelMat()
	self:ShowModel()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_WASH_LOCK_CHANGE,self._onLockChangeCB)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_WASH_UPDATE,self._onUpdateWashCB)
end

function EquipWashView:CloseView()
	self:HideModel()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_WASH_LOCK_CHANGE,self._onLockChangeCB)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_WASH_UPDATE,self._onUpdateWashCB)
end