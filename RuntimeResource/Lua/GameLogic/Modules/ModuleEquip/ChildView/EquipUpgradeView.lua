-- EquipUpgradeView.lua
local EquipUpgradeView = Class.EquipUpgradeView(ClassTypes.EquipBaseView)

EquipUpgradeView.interface = GameConfig.ComponentsConfig.Component
EquipUpgradeView.classPath = "Modules.ModuleEquip.ChildView.EquipUpgradeView"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

local EquipManager = GameManager.EquipManager
local EquipDataHelper = GameDataHelper.EquipDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ConvertStyle = GameMain.LuaFacade.ConvertStyle
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager

function EquipUpgradeView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function EquipUpgradeView:ShowView()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_ADVANCE_UPDATE,self._onUpdateUpgradeCB)
end

function EquipUpgradeView:CloseView()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_ADVANCE_UPDATE,self._onUpdateUpgradeCB)
end

function EquipUpgradeView:InitCallBack()
	self._onUpdateUpgradeCB = function ()
		self:UpdateEquipInfo()
	end
end

function EquipUpgradeView:InitComps()
	 --当前装备icon
	self._containerCurEquip = self:FindChildGO("Container_CurEquip")
    local parent = self:FindChildGO("Container_CurEquip/Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._curEquipIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	--self._equipIcon:ActivateTipsByItemData()
   	self._txtCurName = self:GetChildComponent("Container_CurEquip/Text_CurName","TextMeshWrapper")
   	self._txtCurGrade = self:GetChildComponent("Container_CurEquip/Text_CurGrade","TextMeshWrapper")
   	

   	 --升阶装备icon
   	self._containerUpgradeEquip = self:FindChildGO("Container_UpgradeEquip")
    local parent1 = self:FindChildGO("Container_UpgradeEquip/Container_Icon")
   	local itemGo1 = GUIManager.AddItem(parent1, 1)
   	self._upgradeEquipIcon = UIComponentUtil.AddLuaUIComponent(itemGo1, ItemGrid)
   	self._txtUpgradeName = self:GetChildComponent("Container_UpgradeEquip/Text_UpgradeName","TextMeshWrapper")
   	self._txtNextGrade = self:GetChildComponent("Container_UpgradeEquip/Text_NextGrade","TextMeshWrapper")

   	self._matIcon = self:AddChildLuaUIComponent("Container_Mat",ItemGridEx)
   	self._matIcon:ActivateTips()

	self._btnUpgrade = self:FindChildGO("Button_Upgrade")
    self._csBH:AddClick(self._btnUpgrade, function()self:OnClickUpgrade() end)

    self._containerCurInfoText = self:FindChildGO("Container_CurInfo/Container_Text")
    self._txtCurInfoGrade = self:GetChildComponent("Container_CurInfo/Container_Text/Text_Grade","TextMeshWrapper")
    self._txtCurInfoQuality = self:GetChildComponent("Container_CurInfo/Container_Text/Text_Quality","TextMeshWrapper")
    self._txtCurInfoInfo = self:GetChildComponent("Container_CurInfo/Container_Text/Text_Info","TextMeshWrapper")

    self._containerNextInfoText = self:FindChildGO("Container_NextInfo/Container_Text")
    self._txtNextInfoGrade = self:GetChildComponent("Container_NextInfo/Container_Text/Text_Grade","TextMeshWrapper")
    self._txtNextInfoQuality = self:GetChildComponent("Container_NextInfo/Container_Text/Text_Quality","TextMeshWrapper")
    self._txtNextInfoInfo = self:GetChildComponent("Container_NextInfo/Container_Text/Text_Info","TextMeshWrapper")

    self._gradeSlot = GlobalParamsHelper.GetParamValue(468)
end

function EquipUpgradeView:OnEquipClick(selectedEquipData)
	self._selectedEquip = selectedEquipData
	self:UpdateEquipInfo()
end

function EquipUpgradeView:OnClickUpgrade()
	if self._selectedEquip then
		EquipManager:RequestUpgrade(self._selectedEquip.bagPos)
	end
end

function EquipUpgradeView:UpdateEquipInfo()
	local curEquipId = self._selectedEquip.cfg_id
	local nextGradeCfg = EquipDataHelper.GetEquipAdvanceData(curEquipId)
	if nextGradeCfg then
		--原装备
		self._containerCurEquip:SetActive(true)
		self._containerUpgradeEquip:SetActive(true)
		self._matIcon:SetActive(true)
		self._containerCurInfoText:SetActive(true)
		self._containerNextInfoText:SetActive(true)
		self._btnUpgrade:SetActive(true)

		local nextEquipId = nextGradeCfg.advanced_equip
		local curGrade = ItemDataHelper.GetEquipGrade(curEquipId)
		local nextGrade = ItemDataHelper.GetEquipGrade(nextEquipId)

		self._curEquipIcon:SetItem(curEquipId)
		self._txtCurName.text = ItemDataHelper.GetItemNameWithColor(curEquipId)
		self._txtCurGrade.text = StringStyleUtil.GetChineseNumber(curGrade)..LanguageDataHelper.CreateContent(305)
		self._txtCurInfoGrade.text = curGrade..LanguageDataHelper.CreateContent(305)
		local quality = ItemDataHelper.GetQuality(curEquipId)
		local colorId = ItemConfig.QualityTextMap[quality]
		local cnId = ItemConfig.QualityCN[quality]
		self._txtCurInfoQuality.text = ConvertStyle(string.format("$(%d,%s)",colorId,LanguageDataHelper.GetContent(cnId)))
		local str = ""
		local itemBaseAttris = EquipDataHelper:GetBaseAttris(curEquipId)

		colorId = ItemConfig.ItemNormal
		for i=1,#itemBaseAttris do
			local attriKey = itemBaseAttris[i].attri
			local baseValue = itemBaseAttris[i].value
			str = str..AttriDataHelper:GetName(attriKey)
			str = str..":"..ConvertStyle(string.format("$(%d,%s)",colorId,baseValue)).."\n"
		end
		str = str..LanguageDataHelper.CreateContent(58658)..self:GetGemSlot(curGrade)
		self._txtCurInfoInfo.text = str

		--材料
		for k,v in pairs(nextGradeCfg.cost) do
			self._matIcon:UpdateData(k,v)
		end
		--进阶产物
		self._upgradeEquipIcon:SetItem(nextEquipId)
		self._txtUpgradeName.text = ItemDataHelper.GetItemNameWithColor(nextEquipId)
		self._txtNextGrade.text = StringStyleUtil.GetChineseNumber(nextGrade)..LanguageDataHelper.CreateContent(305)
		self._txtNextInfoGrade.text = nextGrade..LanguageDataHelper.CreateContent(305)
		quality = ItemDataHelper.GetQuality(nextEquipId)
		colorId = ItemConfig.QualityTextMap[quality]
		cnId = ItemConfig.QualityCN[quality]
		self._txtNextInfoQuality.text = ConvertStyle(string.format("$(%d,%s)",colorId,LanguageDataHelper.GetContent(cnId)))

		str = ""
		itemBaseAttris = EquipDataHelper:GetBaseAttris(nextEquipId)

		colorId = ItemConfig.ItemAttriValue
		for i=1,#itemBaseAttris do
			local attriKey = itemBaseAttris[i].attri
			local baseValue = itemBaseAttris[i].value
			str = str..AttriDataHelper:GetName(attriKey)
			str = str..":"..ConvertStyle(string.format("$(%d,%s)",colorId,baseValue)).."\n"
		end
		str = str..LanguageDataHelper.CreateContent(58658)..self:GetGemSlot(nextGrade)
		self._txtNextInfoInfo.text = str
	else
		self._containerCurEquip:SetActive(false)
		self._containerUpgradeEquip:SetActive(false)
		self._matIcon:SetActive(false)
		self._containerCurInfoText:SetActive(false)
		self._containerNextInfoText:SetActive(false)
		self._btnUpgrade:SetActive(false)
	end
end

function EquipUpgradeView:GetGemSlot(grade)
	for i=1,5 do
		if grade < self._gradeSlot[i] then
			return i
		end
	end
	return 6
end