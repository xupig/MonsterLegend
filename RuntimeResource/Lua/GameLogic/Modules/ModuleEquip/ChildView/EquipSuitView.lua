-- EquipSuitView.lua
local EquipSuitView = Class.EquipSuitView(ClassTypes.EquipBaseView)

EquipSuitView.interface = GameConfig.ComponentsConfig.Component
EquipSuitView.classPath = "Modules.ModuleEquip.ChildView.EquipSuitView"

require "Modules.ModuleEquip.ChildComponent.SuitEquipItem"
require "Modules.ModuleEquip.ChildComponent.SuitMatItem"
require "Modules.ModuleEquip.ChildComponent.SuitListItem"
require "Modules.ModuleTips.ChildComponent.EquipTipsSuitCom"
require "UIComponent.Extend.ItemGrid"

local ItemGrid = ClassTypes.ItemGrid
local SuitEquipItem = ClassTypes.SuitEquipItem
local SuitMatItem = ClassTypes.SuitMatItem
local SuitListItem = ClassTypes.SuitListItem
local EquipTipsSuitCom = ClassTypes.EquipTipsSuitCom
local UIComponentUtil = GameUtil.UIComponentUtil
local EquipManager = GameManager.EquipManager
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local UIComponentUtil = GameUtil.UIComponentUtil
local UIScrollView = ClassTypes.UIScrollView
local ItemDataHelper = GameDataHelper.ItemDataHelper
local TipsManager = GameManager.TipsManager
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

function EquipSuitView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self:InitCallBack()
	self:InitComps()
end

function EquipSuitView:InitCallBack()
	self._selectEquipCb = function (index)
		self:SelectEquip(index)
	end

	self._updateSuitListCb = function (index)
		self:UpdateSuitListLayout()
	end

	self._updateSuitCb = function (change)
		self:UpdateEquipSuit(change)
	end
end

function EquipSuitView:InitComps()
	self._equips = {}
    for i=1,10 do
    	local item = self:AddChildLuaUIComponent("Container_EquipList/Container_Equip"..i,SuitEquipItem)
    	item:SetIndex(i)
    	self._equips[i] = item
    end
    self._selectEquipIndex = 0

    self._containerReadyMake = self:FindChildGO("Container_ReadyMake")
    --当前装备
    self._txtCurEquipName = self:GetChildComponent("Container_ReadyMake/Container_CurEquip/Text_Name",'TextMeshWrapper')
    local parent1 = self:FindChildGO("Container_ReadyMake/Container_CurEquip/Container_Icon")
   	local itemGo1 = GUIManager.AddItem(parent1, 1)
   	self._curEquipIcon = UIComponentUtil.AddLuaUIComponent(itemGo1, ItemGrid)
   	self._curEquipIcon:ActivateTipsByItemData()
   	--打造成品
   	self._txtReadyMakeEquipName = self:GetChildComponent("Container_ReadyMake/Container_SuitEquip/Text_Name",'TextMeshWrapper')
    local parent2 = self:FindChildGO("Container_ReadyMake/Container_SuitEquip/Container_Icon")
   	local itemGo2 = GUIManager.AddItem(parent2, 1)
   	self._readyMakeIcon = UIComponentUtil.AddLuaUIComponent(itemGo2, ItemGrid)
   	self._readyMakeIcon:SetPointerUpCB(function ()
   		self:ShowPreviewEquipTips()
   	end)
   	self._imgReadyMakeLegend = self:FindChildGO("Container_ReadyMake/Container_SuitEquip/Image_Legend")
   	self._imgReadyMakeWarLord = self:FindChildGO("Container_ReadyMake/Container_SuitEquip/Image_WarLord")
   
    self._containerCanNotMake = self:FindChildGO("Container_CanNotMake")
    --已打造过的装备
    self._txtMakeCompleteEquipName = self:GetChildComponent("Container_CanNotMake/Container_SuitEquip/Text_Name",'TextMeshWrapper')
    local parent = self:FindChildGO("Container_CanNotMake/Container_SuitEquip/Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._makeCompleteIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._imgMakeCompleteLegend = self:FindChildGO("Container_CanNotMake/Container_SuitEquip/Image_Legend")
   	self._imgMakeCompleteWarLord = self:FindChildGO("Container_CanNotMake/Container_SuitEquip/Image_WarLord")
   	self._makeCompleteIcon:ActivateTipsByItemData()

    self._txtCompleteGO1 = self:FindChildGO("Container_CanNotMake/Text_MakeComplete1")
    self._txtCompleteGO2 = self:FindChildGO("Container_CanNotMake/Text_MakeComplete2")
    self._txtCanNotMake = self:GetChildComponent("Container_CanNotMake/Text_CanNotMake",'TextMeshWrapper')

    --打造材料
    self._matList = {}
    for i=1,3 do
    	local matItem = self:AddChildLuaUIComponent("Container_Mats/Container_Item"..i,SuitMatItem)
		self._matList[i] = matItem
    end

    self._btnTips = self:FindChildGO("Button_Tips")
    self._csBH:AddClick(self._btnTips, function()self:OnClickOpenTips() end)

    self._containerTips = self:FindChildGO("Container_Tips")
    self._btnCloseTips = self:FindChildGO("Container_Tips/Button_Bg")
    self._csBH:AddClick(self._btnCloseTips, function()self:OnClickCloseTips() end)
    local txtTips = self:GetChildComponent("Container_Tips/Image_Bg/Text_Desc",'TextMeshWrapper')
    txtTips.text = LanguageDataHelper.CreateContent(58662)
    self._containerTips:SetActive(false)
    
    self._btnMakeSuit = self:FindChildGO("Button_MakeSuit")
    self._btnMakeSuitWrapper = self._btnMakeSuit:GetComponent("ButtonWrapper")
    self._csBH:AddClick(self._btnMakeSuit, function()self:OnClickMakeSuit() end)


    self._levelUpTip = self:FindChildGO("Image_LevelUp").transform
	self._levelUpTip.localScale = Vector3.zero
	self._tweenScaleLevelUp = self:AddChildComponent('Image_LevelUp', XGameObjectTweenScale)

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30034")
    self._fx:SetLifeTime(2)
    self._fx:Stop()
    ---------------------------------套装属性---------------------------------------------
    self._containerSuit = self:AddChildLuaUIComponent("Container_Suit",EquipTipsSuitCom)

    --------------------------------查看全部套装--------------------------------------------
    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Text_CheckAll')
    self._linkTextMesh:SetOnClickCB(function(linkIndex) self:OnClickLink(linkIndex) end)

    self._containerCheckAll = self:FindChildGO("Container_CheckAll")

    self._btnCloseCheckAll = self:FindChildGO("Container_CheckAll/Button_Close")
    self._csBH:AddClick(self._btnCloseCheckAll, function() self:OnClickCloseCheckAll() end)

    self._scroll = UIComponentUtil.FindChild(self.gameObject, "Container_CheckAll/ScrollView_Suit")
	self._scrollViewAttri = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
	self._scrollContent = self:FindChildGO("Container_CheckAll/ScrollView_Suit/Mask/Content")
	self._rectTransform = self._scrollContent:GetComponent(typeof(UnityEngine.RectTransform))
	
	self._suitItemBase = self:FindChildGO("Container_CheckAll/ScrollView_Suit/Mask/Content/Container_Item")
	--local suitItem = UIComponentUtil.AddLuaUIComponent(self._suitItemBase, SuitListItem)
	self._suitItemBase:SetActive(false)
	self._suitItemList = {}
	--self._selectedSuitIndex = 0

	self._containerCheckAll:SetActive(false)
end

function EquipSuitView:SetSuitType(suitType)
	--LoggerHelper.Error("EquipSuitView:SetSuitType"..suitType)
	self._suitType = suitType
end

function EquipSuitView:OnClickOpenTips()
	self._containerTips:SetActive(true)
end

function EquipSuitView:OnClickCloseTips()
	self._containerTips:SetActive(false)
end

function EquipSuitView:OnClickMakeSuit()
	if self._selectEquipData then
		EquipManager:RequestMakeSuit(self._selectEquipData.bagPos)
	end
end

function EquipSuitView:ShowPreviewEquipTips()
	if self._selectEquipData then
		TipsManager:ShowItemTips(self._selectEquipData,nil,false,true)
	end
end

function EquipSuitView:SelectEquip(index)
	local loadedEquips = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
	if loadedEquips[index] == nil then
		return
	end

	if self._selectEquipIndex > 0 then
		self._equips[self._selectEquipIndex]:SetSelected(false)
	end
	self._equips[index]:SetSelected(true)
	self._selectEquipIndex = index

	
	local suitData = GameWorld.Player().equip_suit_data or {}
	local suitLevel = suitData[index] or 0
	self._selectEquipData = loadedEquips[index]
	local suitId = self._selectEquipData:GetSuitId()

	--可打造
	if (suitLevel == 0 and self._suitType == 1 and suitId) or (suitLevel == 1 and self._suitType == 2 and suitId[2]) then
		self._containerReadyMake:SetActive(true)
		self._containerCanNotMake:SetActive(false)
		self._curEquipIcon:SetItemData(self._selectEquipData)
		self._txtCurEquipName.text = self._selectEquipData:GetEquipName()
		self._readyMakeIcon:SetItemData(self._selectEquipData)
		self._txtReadyMakeEquipName.text = self._selectEquipData:GetEquipName(true)
		if self._suitType == 1 then
			self._imgReadyMakeLegend:SetActive(true)
			self._imgReadyMakeWarLord:SetActive(false)
		else
			self._imgReadyMakeLegend:SetActive(false)
			self._imgReadyMakeWarLord:SetActive(true)
		end

		local cost = self._selectEquipData:GetSuitCost()[suitId[self._suitType]]
		local h = 1
		for i=1,#cost,2 do
			local itemId = cost[i]
			local needCount = cost[i+1]
			self._matList[h]:UpdateData(itemId,needCount)
			h = h+1
		end

		for j=h,#self._matList do
			self._matList[j]:UpdateData(nil)
		end

		self._btnMakeSuitWrapper.interactable = true
		--显示预览
		self:UpdateSuitInfo(true,suitData,suitLevel+1)
	--不可打造的
	else
		self._containerReadyMake:SetActive(false)
		self._containerCanNotMake:SetActive(true)
		self._makeCompleteIcon:SetItemData(self._selectEquipData)
		self._txtMakeCompleteEquipName.text = self._selectEquipData:GetEquipName()
		if suitLevel == 1 then
			self._txtCompleteGO2:SetActive(false)
			if self._suitType == 1 then
				self._txtCompleteGO1:SetActive(true)
				self._txtCanNotMake.text = ""
			else
				self._txtCompleteGO1:SetActive(false)
				self._txtCanNotMake.text = LanguageDataHelper.CreateContent(58665)
			end
			self._imgMakeCompleteLegend:SetActive(true)
			self._imgMakeCompleteWarLord:SetActive(false)
			self:UpdateSuitInfo(true,suitData,suitLevel)
		elseif suitLevel == 2 then
			self._txtCompleteGO1:SetActive(false)
			self._txtCompleteGO2:SetActive(true)
			self._txtCanNotMake.text = ""
			self._imgMakeCompleteLegend:SetActive(false)
			self._imgMakeCompleteWarLord:SetActive(true)
			self:UpdateSuitInfo(true,suitData,suitLevel)
		else
			self._txtCompleteGO1:SetActive(false)
			self._txtCompleteGO2:SetActive(false)
			if self._suitType == 1 then
				if self._selectEquipData.bagPos <= 6 then
					self._txtCanNotMake.text = LanguageDataHelper.CreateContent(58663)
				else
					self._txtCanNotMake.text = LanguageDataHelper.CreateContent(58664)
				end
			else
				self._txtCanNotMake.text = LanguageDataHelper.CreateContent(58665)
			end
			
			self._imgMakeCompleteLegend:SetActive(false)
			self._imgMakeCompleteWarLord:SetActive(false)
			self:UpdateSuitInfo(false,suitData,suitLevel)
		end
		 
		for i=1,3 do
			self._matList[i]:UpdateData(nil)
		end

		self._btnMakeSuitWrapper.interactable = false
	end

end

function EquipSuitView:UpdateSuitInfo(showSuitInfo,suitData,viewLevel)
	if showSuitInfo then
		self._containerSuit:SetActive(true)
		self._containerSuit:UpdateCom(self._selectEquipData.cfgData,suitData,viewLevel)
	else
		self._containerSuit:SetActive(false)
	end
end

--更新装备列表
function EquipSuitView:UpdateEquips()
	local loadedEquips = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
	local suitData = GameWorld.Player().equip_suit_data
	for i=1,10 do
		self._equips[i]:UpdateData(loadedEquips[i],suitData[i],self._suitType)
	end
end

--打造后刷界面
function EquipSuitView:UpdateEquipSuit(change)
	self:UpdateEquips()
	if self._selectEquipIndex > 0 then
		self:SelectEquip(self._selectEquipIndex)
	end
	self._tweenScaleLevelUp:OnceOutQuad(1, 1, function()  
		self._levelUpTip.localScale = Vector3.zero
	end)
	self._fx:Play()
end


function EquipSuitView:OnClickCloseCheckAll()
	self._containerCheckAll:SetActive(false)
	--self._selectedSuitIndex = 0
end

function EquipSuitView:OnClickLink(linkId)
    self:OpenCheckAllSuit()
end

--查看全部套装
function EquipSuitView:OpenCheckAllSuit()
	self._containerCheckAll:SetActive(true)

	local suitListData = {}
	local tempList = {}
	local loadedEquips = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
	local suitData = GameWorld.Player().equip_suit_data
	for i=1,10 do
		local equipData = loadedEquips[i]
		local suitLevel = suitData[i]
		if equipData and suitLevel and suitLevel > 0 then
			local suitId = equipData:GetSuitId()[suitLevel]
			if tempList[suitId] == nil then
				tempList[suitId] = equipData
			end
		end
	end
	for k,v in pairs(tempList) do
		table.insert(suitListData,v)
	end

	local h = 1
	self._suitItemBase:SetActive(true)
	for i=1,#suitListData do
		if self._suitItemList[i] == nil then
			local item = self:CreateSuitItem()
			self._suitItemList[i] = item
		end
		self._suitItemList[i]:SetActive(true)
		local equipData = suitListData[i]
		local suitLevel = suitData[equipData.bagPos]
		self._suitItemList[i]:UpdateData(equipData.cfgData,suitData,suitLevel)
		h = h + 1
	end
	self._suitItemBase:SetActive(false)

	for j=h,#self._suitItemList do
		self._suitItemList[j]:Hide()
		self._suitItemList[j]:SetActive(false)
	end

	self:UpdateSuitListLayout()
end

-- function EquipSuitView:UpdateSuitList(index)
-- 	if self._selectedSuitIndex > 0 then
-- 		self._suitItemList[index]:SetExpand(false)
-- 	end
-- 	self._selectedSuitIndex = index
-- 	self:UpdateSuitListLayout()
-- end

function EquipSuitView:UpdateSuitListLayout()
	local curY = 0
	for i=1,#self._suitItemList do
		local h = self._suitItemList[i]:GetHeight()
		self:SetPosition(self._suitItemList[i],curY)
		curY = curY - h
	end
	local height = -curY
	self._rectTransform.sizeDelta = Vector2(356,height)
end

function EquipSuitView:SetPosition(com,y)
	com.transform.anchoredPosition = Vector3(com.transform.anchoredPosition.x,y,0)
end

function EquipSuitView:CreateSuitItem()
	local go = self:CopyUIGameObject("Container_CheckAll/ScrollView_Suit/Mask/Content/Container_Item","Container_CheckAll/ScrollView_Suit/Mask/Content/")
    local item = UIComponentUtil.AddLuaUIComponent(go, SuitListItem)
    return item
end

function EquipSuitView:ShowView()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_SELECT_SUIT_EQUIP,self._selectEquipCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_SUIT_LIST,self._updateSuitListCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_SUIT_UPDATE,self._updateSuitCb)
end

function EquipSuitView:CloseView()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_SELECT_SUIT_EQUIP,self._selectEquipCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_SUIT_LIST,self._updateSuitListCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_SUIT_UPDATE,self._updateSuitCb)
end

function EquipSuitView:ShowEquips()
	self:UpdateEquips()
	self:SelectEquip(1)
end