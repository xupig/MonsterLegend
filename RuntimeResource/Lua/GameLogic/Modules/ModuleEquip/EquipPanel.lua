-- EquipPanel.lua
local EquipPanel = Class.EquipPanel(ClassTypes.BasePanel)
require "Modules.ModuleEquip.ChildView.EquipBaseView"
require "Modules.ModuleEquip.ChildView.EquipStrengthView"
require "Modules.ModuleEquip.ChildView.EquipWashView"
require "Modules.ModuleEquip.ChildView.EquipUpgradeView"
require "Modules.ModuleEquip.ChildView.EquipGemInlayView"
require "Modules.ModuleEquip.ChildView.EquipGemRefineView"
require "Modules.ModuleEquip.ChildView.EquipSuitView"

require "Modules.ModuleEquip.ChildComponent.EquipListItem"

local public_config = GameWorld.public_config

local EquipStrengthView 	= ClassTypes.EquipStrengthView
local EquipWashView 		= ClassTypes.EquipWashView
local EquipUpgradeView 		= ClassTypes.EquipUpgradeView
local EquipGemInlayView 	= ClassTypes.EquipGemInlayView
local EquipGemRefineView 	= ClassTypes.EquipGemRefineView
local EquipSuitView 		= ClassTypes.EquipSuitView

local bagData = PlayerManager.PlayerDataManager.bagData
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local EquipListItem = ClassTypes.EquipListItem
local UIList = ClassTypes.UIList
local EquipDataHelper = GameDataHelper.EquipDataHelper

function EquipPanel:Awake()
	self._csBH = self:GetComponent("LuaUIPanel")
	self:InitCallBack()
	self:InitComps()
	self:InitEquips()
end

--初始化回调
function EquipPanel:InitCallBack()
	self._secTabCb = function (index)
		self:SelectSecTab(index)
	end

	self._updateEquipCb = function (change)
		if self._selectedPath == "Container_Strength" then
			self:SortEquipList()
		end
		if self._selectedPath == "Container_GemInlay" then
			self:SortEquipList()
			return
		end
		self:OnEquipChange(change)
	end

	self._itemChangeCb = function (pkgType, pos1, pos2, flag)
		self:OnEquipUpdate(pkgType, pos1, pos2, flag)
	end
end

function EquipPanel:InitComps()
	self._imgBg = self:FindChildGO("Image_Bg")

	self:InitView("Container_Strength", EquipStrengthView,	1,1)
	self:InitView("Container_Wash", 	EquipWashView,		1,2)
	self:InitView("Container_Wash", 	EquipWashView,		1,3)
	self:InitView("Container_Upgrade", 	EquipUpgradeView,	1,4)
	self:InitView("Container_GemInlay", EquipGemInlayView,	2,1)
	self:InitView("Container_GemRefine",EquipGemRefineView,	2,2)
	self:InitView("Container_Suit", 	EquipSuitView,		3,1)
	self:InitView("Container_Suit", 	EquipSuitView,		3,2)

	self:SetSecondTabClickCallBack(self._secTabCb)
end

function EquipPanel:InitEquips()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "List_Equip")
    self._listEquip = list
    self._scrollView = scrollView
    self._listEquip:SetItemType(EquipListItem)
    self._listEquip:SetPadding(0, 0, 0, 0)
    self._listEquip:SetGap(0, 0)
    self._listEquip:SetDirection(UIList.DirectionTopToDown, 1, 1)
    self._listEquip:SetItemClickedCB(function (idx)
    	self:OnEquipClick(idx)
    end)
end

--选中装备
function EquipPanel:OnEquipClick(idx)
	if self._selectedEquip then
		if self._selectedEquip:GetIndex() == idx then
			return
		end
		self._selectedEquip:SetSelected(false)
	end
	self._selectedEquip = self._listEquip:GetItem(idx)
	self._selectedEquip:SetSelected(true)
	self:UpdateViewSelectEquip(idx)
end

--调用View的OnEquipClick刷新页面
function EquipPanel:UpdateViewSelectEquip(idx)
	if self._selectedView then
		local selectedEquipData = self._listEquip:GetDataByIndex(idx)[2]
		self._selectedListIndex = idx
		self._selectedEquipType = selectedEquipData:GetType()
		self._selectedView:OnEquipClick(selectedEquipData)
	end
end

--显示某个装备系统
function EquipPanel:SelectSecTab(secIndex)
	self._listEquip:SetActive(self._selectedIndex ~= 3)
	if self._selectedIndex == 3 or (self._selectedIndex ==1 and secIndex == 3) then
		self._imgBg:SetActive(false)
	else
		self._imgBg:SetActive(true)
	end

	if self._selectedIndex == 3 then
		self._selectedView:SetSuitType(secIndex)
		self._selectedView:ShowEquips()
	else
		self:ClearSelectedEquip()
		--LoggerHelper.Error("EquipPanel:SelectSecTab"..self._selectedIndex)
		self:UpdateEquips()
	end
	
end

local function SortEquipListStrength(aa, bb) 
	local a = aa[2]
	local b = bb[2]
	local strengthInfo = GameWorld.Player().equip_stren_info
	local aType= a:GetType()
	local bType= b:GetType()
	local aStrengthLv = strengthInfo[aType] and strengthInfo[aType][public_config.EQUIP_STREN_INFO_LEVEL] or 0
	local bStrengthLv = strengthInfo[bType] and strengthInfo[bType][public_config.EQUIP_STREN_INFO_LEVEL] or 0
	local aCanStrength = aStrengthLv < a.cfgData.strengthHighLv
	local bCanStrength = bStrengthLv < b.cfgData.strengthHighLv
	if aCanStrength ~= bCanStrength then
		return aCanStrength
	else
		return aType < bType
	end 
end

local function SortEquipListNormal(a, b)
	return a[2]:GetType() < b[2]:GetType()
end

local function SortEquipListWash(aa, bb)
	local a = aa[2]
	local b = bb[2]
	local aType= a:GetType()
	local bType= b:GetType()
	local washInfo = GameWorld.Player().equip_wash_info
	local aCanWash = washInfo[aType] ~= nil 
	local bCanWash = washInfo[bType] ~= nil 
	if aCanWash ~= bCanWash then
		return aCanWash
	elseif aCanWash then
		return aType < bType
	else
		local aOpenLevel = EquipDataHelper:GetWashOpenLevel(aType)
		local bOpenLevel = EquipDataHelper:GetWashOpenLevel(bType)
		return aOpenLevel < bOpenLevel
	end 
end

function EquipPanel:UpdateEquips()
	self._selectedListIndex = nil
	self._selectedEquipType = nil
	self._selectedEquip = nil
	self._equipDataList = {}
	local t = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
	for i=1,public_config.PKG_LOADED_EQUIP_INDEX_MAX do
		if t[i] then
			local showType = 0
			local shouldAddData = true
			--强化
			if self._selectedPath == "Container_Strength" then
				showType = 1
			end

			--洗炼
			if self._selectedPath == "Container_Wash" then
				showType = 2
			end

			--进阶
			if self._selectedPath == "Container_Upgrade" then
				local equipType = t[i]:GetType()
				--只显示戒指和手镯部位
				if equipType ~= 9 and equipType ~= 10 then
					shouldAddData = false
				end
			end
			--宝石镶嵌
			if self._selectedPath == "Container_GemInlay" then
				showType = 3
			end

			--宝石精炼
			if self._selectedPath == "Container_GemRefine" then
				showType = 4
			end
			
			if shouldAddData then
				table.insert(self._equipDataList, {[1] = showType,[2] = t[i]} )
			end
		end
	end

	self:SortEquipList()
	--默认选中第一个装备
	if #self._equipDataList > 0 then
		self:OnEquipClick(0)
	end
end

function EquipPanel:SortEquipList()
	if self._selectedPath == "Container_Strength" then
		table.sort(self._equipDataList,SortEquipListStrength)
		self._listEquip:SetDataList(self._equipDataList)
		if self._selectedListIndex then
			local newType = self._equipDataList[self._selectedListIndex+1][2]:GetType()
			--LoggerHelper.Error("newType"..newType)
			if self._selectedEquipType ~= newType then
				self:UpdateViewSelectEquip(self._selectedListIndex)
			end
		end
	elseif self._selectedPath == "Container_Wash" then
		table.sort(self._equipDataList,SortEquipListWash)
		self._listEquip:SetDataList(self._equipDataList)
	else
		table.sort(self._equipDataList,SortEquipListNormal)
		self._listEquip:SetDataList(self._equipDataList)
	end
end

--装备数据变化更新左侧列表
function EquipPanel:OnEquipChange(change)
	if change then
		for i=1,#self._equipDataList do
			local item = self._listEquip:GetItem(i-1)
			local equipData = self._equipDataList[i][2]
			for pos,v in pairs(change) do
				--pos<0的时候是删除，只能全部刷新
				if equipData.bagPos == pos or tonumber(pos) < 0 then
					item:UpdateData()
				end
			end
		end
	end
end

--装备数据变化更新左侧列表
function EquipPanel:OnEquipUpdate(pkgType, pos1, pos2, flag)
	if pkgType == public_config.PKG_TYPE_LOADED_EQUIP then
		for i=1,#self._equipDataList do
			local item = self._listEquip:GetItem(i-1)
			local equipData = self._equipDataList[i][2]
			if equipData.bagPos == pos1 then
				item:UpdateData()
			end
		end
	end
end

function EquipPanel:ClearSelectedEquip()
	if self._selectedEquip then
		self._selectedEquip:SetSelected(false)
		self._selectedEquip = nil
	end
end

--重新打开面板处理更新
function EquipPanel:OnShow(data)
	self:UpdateFunctionOpen()
	self:OnShowSelectTab(data)
	self._scrollView:SetScrollRectState(true)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_STRENGTH_UPDATE,self._updateEquipCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_WASH_UPDATE,self._updateEquipCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_EQUIP_GEM_UPDATE,self._updateEquipCb)
	EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_UPDATE,self._itemChangeCb)
end

function EquipPanel:OnClose()
	self._scrollView:SetScrollRectState(false)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_STRENGTH_UPDATE,self._updateEquipCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_WASH_UPDATE,self._updateEquipCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_EQUIP_GEM_UPDATE,self._updateEquipCb)
	EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_UPDATE,self._itemChangeCb)
end

function EquipPanel:BaseShowModel()
	if self._selectedView then
		self._selectedView:ShowModel()
	end
end

function EquipPanel:BaseHideModel()
	if self._selectedView then
		self._selectedView:HideModel()
	end
end

-- -- 0 没有  1 普通底板
function EquipPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function EquipPanel:StructingViewInit()
     return true
end

--关闭的时候重新开启二级主菜单
function EquipPanel:ReopenSubMainPanel()
    return true
end

-- function EquipPanel:ScaleToHide()
--     return true
-- end
