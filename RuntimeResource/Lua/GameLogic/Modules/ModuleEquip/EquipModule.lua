-- EquipUIModule.lua
EquipUIModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function EquipUIModule.Init()
    GUIManager.AddPanel(PanelsConfig.Equip, "Panel_Equip", GUILayer.LayerUIPanel, "Modules.ModuleEquip.EquipPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return EquipUIModule
