local CheckOtherPlayerPanel = Class.CheckOtherPlayerPanel(ClassTypes.BasePanel)

require "Modules.ModuleRole.ChildView.RoleAttriInfoBlockView"
require "Modules.ModuleRole.ChildComponent.SingleEquipItem"
require "PlayerManager/PlayerData/CombatData"
require "PlayerManager/PlayerData/CommonItemData"

local CombatData = ClassTypes.CombatData
local CommonItemData = ClassTypes.CommonItemData
local GUIManager = GameManager.GUIManager
local TipsManager = GameManager.TipsManager
local public_config = GameWorld.public_config
local UIProgressBar = ClassTypes.UIProgressBar
local XArtNumber = GameMain.XArtNumber
local EntityConfig = GameConfig.EntityConfig
local RoleDataHelper = GameDataHelper.RoleDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local RoleAttriInfoBlockView = ClassTypes.RoleAttriInfoBlockView
local SingleEquipItem = ClassTypes.SingleEquipItem
local Rect = UnityEngine.Rect
local ActorModelComponent = GameMain.ActorModelComponent
local StringStyleUtil = GameUtil.StringStyleUtil
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local Max_Attri_Category = 2 --总的子title个数，策划配表要保持连续，即1,2,3不能1,3,4
--根据UI搭建界面直接定死，不进行动态适应
local Title_Text_Height = 30
local Modified_Variable = 30
local Item_Height = 30 --GridLayoutGroup的cell_size的值

local Set_Height = Vector3.New(0,0,0)

function CheckOtherPlayerPanel:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._itemPool = {}
    self._items = {}
    self:InitCallBack()
    self:InitView()
    self:InitEquip()
end

function CheckOtherPlayerPanel:InitCallBack()
	self._equipClickCb = function (selectedEquipData,index)
		self:OnEquipClick(selectedEquipData,index)
	end
end

function CheckOtherPlayerPanel:OnDestroy() 

end

function CheckOtherPlayerPanel:OnClose()
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_PLAYER_EQUIP_CLICK,self._equipClickCb)
end

function CheckOtherPlayerPanel:InitEquip()

	self._slotToType ={1,2,3,4,5,6,7,8,9,10,11,12}
    self._slotCount = 12
	self._equipSlots = {}
	self._equipSlotsGO = {}
	self._typeIconGO = {}

    for i=1,self._slotCount do
    	self._equipSlotsGO[i] = self:FindChildGO("Container_RoleEquip/Container_Equips/Container_Equip"..i)
        local item = self:AddChildLuaUIComponent("Container_RoleEquip/Container_Equips/Container_Equip"..i.."/Button_Equip",SingleEquipItem)
		self._equipSlots[i] = item
        self._typeIconGO[i] = self:FindChildGO("Container_RoleEquip/Container_Equips/Container_TypeIcon/Image_TypeIcon"..i)
    end
end

function CheckOtherPlayerPanel:OnEquipClick(selectedEquipData,index)
	TipsManager:ShowItemTips(selectedEquipData,nil,false,false,self._equipArg)
end

function CheckOtherPlayerPanel:OnShow(data)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_PLAYER_EQUIP_CLICK,self._equipClickCb)
    self.data = data
    self._equipArg = {}
    self._equipArg.vipLevel     = data[public_config.PEEP_KEY_VIP]
    self._equipArg.strengthInfo = data[public_config.PEEP_KEY_STREN]
    self._equipArg.washInfo 	= data[public_config.PEEP_KEY_WASH]
    self._equipArg.gemInfo 		= data[public_config.PEEP_KEY_GEM]
    self._equipArg.suitData 	= data[public_config.PEEP_KEY_SUIT]
    
    for i=1,self._slotCount do
        self._equipSlots[i]:ActivateSuitIcon(self._equipArg.suitData)
    end

    self.combatData = CombatData()
    self.combatData:RefreshData(data[public_config.PEEP_KEY_COMBAT_ATTRI])
    self:RefreshView()
end

function CheckOtherPlayerPanel:AddEventListeners()
    -- self._onGetWorldLevel = function() self:OnGetWorldLevel() end
    -- EventDispatcher:AddEventListener(GameEvents.On_Get_World_Level, self._onGetWorldLevel)
    -- self._onAttrRefresh = function() self:OnAttrRefresh() end
    -- EventDispatcher:AddEventListener(GameEvents.ON_ATTR_REFRESH, self._onAttrRefresh)
end

function CheckOtherPlayerPanel:RemoveEventListeners()
    -- EventDispatcher:RemoveEventListener(GameEvents.On_Get_World_Level, self._onGetWorldLevel)
    -- EventDispatcher:RemoveEventListener(GameEvents.ON_ATTR_REFRESH, self._onAttrRefresh)
end

function CheckOtherPlayerPanel:InitView()

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)
    
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._spouseNameText = self:GetChildComponent("Text_SpouseName", "TextMeshWrapper")

    self._playerModelComponent = self:AddChildComponent('Container_Model', ActorModelComponent)
    self._playerModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)

    self._levelAttrText = self:GetChildComponent("Container_AttriShow/Text_Level/Text_Value", "TextMeshWrapper")
    self._guildText = self:GetChildComponent("Container_AttriShow/Text_Guild/Text_Value", "TextMeshWrapper")
    self._pkValueText = self:GetChildComponent("Container_AttriShow/Text_PK/Text_Value", "TextMeshWrapper")

    self._rectTransform = self:GetChildComponent("Container_AttriShow/ScrollView_Attri/Mask/Content", "RectTransform")
    self._attriInfoBlockGO = self:FindChildGO("Container_AttriShow/ScrollView_Attri/Mask/Content/Container_AttriInfo")

    self._idToCount = {}
    self._idToHeight = {}
    self._attriGO = {}
    self._attriView = {}
    self._startPos = self._attriInfoBlockGO.transform.localPosition
    for i=1, Max_Attri_Category do
        self._attriGO[i] = self:CopyUIGameObject("Container_AttriShow/ScrollView_Attri/Mask/Content/Container_AttriInfo","Container_AttriShow/ScrollView_Attri/Mask/Content/")
        self._attriView[i] = UIComponentUtil.AddLuaUIComponent(self._attriGO[i], RoleAttriInfoBlockView)
    end
    self._attriInfoBlockGO:SetActive(false)

    self:InitProgress()
end

function CheckOtherPlayerPanel:RefreshScrollView()
    self._idToHeight[1] = 0
    local sum = self._idToHeight[1]
    for i=2, Max_Attri_Category + 1 do
        local count = self._idToCount[i-1]
        local h = math.ceil(count/2) * Item_Height + Title_Text_Height + Modified_Variable
        sum = sum + h
        self._idToHeight[i] = sum
    end
    for i=1, Max_Attri_Category do
        Set_Height.y = self._idToHeight[i]*-1
        self._attriGO[i].transform.localPosition = self._startPos + Set_Height
    end
    self._rectTransform.sizeDelta = Vector2.New(self._rectTransform.sizeDelta.x, self._idToHeight[Max_Attri_Category + 1])
end

function CheckOtherPlayerPanel:InitProgress()
    self._expComp = UIProgressBar.AddProgressBar(self.gameObject,"Container_AttriShow/ProgressBar_Exp")
    self._expComp:SetProgress(0)
    self._expComp:ShowTweenAnimate(false)
    self._expComp:SetMutipleTween(false)
    self._expComp:SetIsResetToZero(false)
end

function CheckOtherPlayerPanel:OnExpChange()
    local curExp = self.data[public_config.PEEP_KEY_EXP] 
    local maxExp = RoleDataHelper.GetCurLevelExp(self.data[public_config.PEEP_KEY_LEVEL])
    self._expComp:SetProgress(curExp/maxExp)
    self._expComp:SetProgressText(StringStyleUtil.GetLongNumberString(curExp).."/"..StringStyleUtil.GetLongNumberString(tonumber(maxExp)))
end

function CheckOtherPlayerPanel:RefreshView()
	self:UpdatePlayerName()
	self:OnLevelChange()
	self:OnExpChange()
	self:OnFightForceChange()
	self:OnPKValueChange()
    self:SetGuildName()
    self:UpatePlayerModel()
    self:UpateEquip()

    for i=1, Max_Attri_Category do
        local idToData = self.combatData:GetAttrDataById(i)
        self._attriView[i]:SetData(idToData)
        self._attriView[i]:SetTitle(i)
        self._idToCount[i] = #idToData
    end
    self:RefreshScrollView()
end

function CheckOtherPlayerPanel:OnClearButtonClick()

end

function CheckOtherPlayerPanel:UpateEquip()
	local type
    self._equipArg.allEquipIds = {}
    for i=1,self._slotCount do
        type = self._slotToType[i]
        local equipData = self.data[public_config.PEEP_KEY_EQUIP][type]
        if equipData then
        	equipData = CommonItemData.CreateItemData(equipData,public_config.PKG_TYPE_LOADED_EQUIP,type)
            self._equipArg.allEquipIds[type] = equipData.cfg_id
        end
        self._equipSlots[i]:UpdateData(equipData,i)
        self._typeIconGO[i]:SetActive(equipData == nil)
        if self._selectedPanelIndex == 2 then
        	self._equipSlots[i]:UpdateCanStrength()
        end
    end
end

function CheckOtherPlayerPanel:UpatePlayerModel()
	local vocation = self.data[public_config.PEEP_KEY_VOCATION]
	local facade = self.data[public_config.PEEP_KEY_FACADE]
	self._playerModelComponent:LoadAvatarModel(vocation, facade, false)
end

function CheckOtherPlayerPanel:UpdatePlayerName()
    local nameStr = self.data[public_config.PEEP_KEY_NAME]
    nameStr = BaseUtil.GetColorString("VIP"..self.data[public_config.PEEP_KEY_VIP], ColorConfig.E).."  "..nameStr
	self._nameText.text = nameStr

    local spouseName = self.data[public_config.PEEP_KEY_MATE_NAME]
    if spouseName and spouseName ~= "" then
        self._spouseNameText.text = LanguageDataHelper.CreateContent(626)..spouseName
    else
        self._spouseNameText.text = ""
    end
end

function CheckOtherPlayerPanel:OnLevelChange()
    self._levelAttrText.text = self.data[public_config.PEEP_KEY_LEVEL]
end

function CheckOtherPlayerPanel:OnFightForceChange()
    self._fpNumber:SetNumber(self.data[public_config.PEEP_KEY_COMBAT_VALUE])
end

function CheckOtherPlayerPanel:OnPKValueChange()
    self._pkValueText.text = self.data[public_config.PEEP_KEY_PK]
end

function CheckOtherPlayerPanel:SetGuildName()
    self._guildText.text = self.data[public_config.PEEP_KEY_GUILD]
end

-- -- 0 没有  1 普通底板
function CheckOtherPlayerPanel:WinBGType()
    return PanelWinBGType.NormalBG
end