CheckOtherPlayerModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function CheckOtherPlayerModule.Init()
    GUIManager.AddPanel(PanelsConfig.CheckOtherPlayer,"Panel_CheckOtherPlayer",GUILayer.LayerUIPanel,"Modules.ModuleCheckOtherPlayer.CheckOtherPlayerPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return CheckOtherPlayerModule