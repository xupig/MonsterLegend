--TextFloatPanel.lua
local TextFloatPanel = Class.TextFloatPanel(ClassTypes.BasePanel)

local XDamageHandler = GameMain.XDamageHandler

-- local DamageManager = GameManager.DamageManager

function TextFloatPanel:Awake()
    self.panelBH = self:GetComponent('LuaUIPanel')
    self:AddChildComponent("Container_BattleTextFloat", XDamageHandler)

    -- local numberGo = self:FindChildGO("Container_BattleTextFloat/Container_number")
    -- local wordGo = self:FindChildGO("Container_BattleTextFloat/Container_word")
    -- local damageGo = self:FindChildGO("Container_BattleTextFloat/Container_damage")
    -- local underTransform = self:FindChildGO("Container_BattleTextFloat/Container_layer1").transform
    -- local topTransform = self:FindChildGO("Container_BattleTextFloat/Container_layer2").transform
    -- DamageManager:SetPanelInfo(numberGo,damageGo,wordGo,underTransform,topTransform)
end

function TextFloatPanel:OnDestroy() 
    self.panelBH = nil
end