--TextFloatModule.lua
TextFloatModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function TextFloatModule.Init()
    GUIManager.AddPanel(PanelsConfig.TextFloat, "Panel_TextFloat", GUILayer.LayerUIMain, "Modules.ModuleTextFloat.TextFloatPanel", true, false)
end

return TextFloatModule