--TextFloatGrid.lua
local DamageGrid = Class.DamageGrid(ClassTypes.BaseLuaUIComponent)

local Vector3 = Vector3
local Vector2 = Vector2
local Camera = UnityEngine.Camera
local Screen = UnityEngine.Screen
local math = math

function DamageGrid:Awake()
    self._damageItemList = {}
    self._size = Vector3.zero   
    self._floatPosition = Vector3.zero
    self._height = 1
    self._call = function () self:OnEnd() end
    self._isUse = false
    self._randOffX = 0
end

function DamageGrid:AddNumberItem(damageItem)    
    table.insert(self._damageItemList,damageItem)
    self._size.x = self._size.x + damageItem._rectTransform.sizeDelta.x
    self._size.y = math.max(damageItem._rectTransform.sizeDelta.y,self._size.y)
end

function DamageGrid:Show(position,type)
    self._floatPosition = position
    self._isUse = true
    self:InitValue()
    local item
    for i=1,#self._damageItemList do
        item = self._damageItemList[i]
        item:Show(item:GetPosition(),type,self._call)
    end
end

function DamageGrid:InitValue()
    local damagePosition = self._floatPosition + Vector3.New(0,self._height,0)
    local screenPosition = Camera.main:WorldToScreenPoint(damagePosition)
    local localPosition = screenPosition
    localPosition.x = screenPosition.x/Global.CanvasScaleX
    localPosition.y = screenPosition.y/Global.CanvasScaleY

    self:SetStartPosition(localPosition)
    self:SetStartScale()
    self:SetStartAlpha()
end

function DamageGrid:OnEnd()
    if self._isUse then
        self._isUse = false
    else
        return
    end
    DamageManager:RemoveItemFormList(self._damageItemList)
    self:RemoveAllItem()
    DamageManager:RemoveDamageGrid(self)
end

function DamageGrid:SetStartScale()
    local sizeX = self._size.x
    local item = nil
    local size
    for i=1,#self._damageItemList do
        item = self._damageItemList[i]
        item:SetScale(Vector3.one)
        size = item:GetSize()
        item:SetPivot(Vector2.New(sizeX/size.x * 0.5,0.5))
        sizeX = sizeX - size.x * 2
        local pos = item:GetPosition() + Vector3.New(size.x * item:GetPivot().x,0,0)
        item:SetPosition(pos)
    end
end

function DamageGrid:SetStartPosition(localPosition)
    self._randOffX = math.random(-35,35)    
    --localPosition.y = localPosition.y - Screen.height/DamageManager.Scale
    localPosition.x = localPosition.x + self._randOffX
    localPosition.z = 0
    local startX = localPosition.x -self._size.x/2
    local item = nil
    local pos = Vector3.zero
    local povit = Vector2.New(0,0.5)
    for i=1,#self._damageItemList do
        item = self._damageItemList[i]
        item:SetPivot(povit)
        pos.x = startX
        pos.y = localPosition.y
        item:SetPosition(pos)
        startX = startX + item:GetSize().x;
    end
end

function DamageGrid:SetStartAlpha()
    local item = nil
    for i=1,#self._damageItemList do
        item = self._damageItemList[i]
        item:SetAlpha(1)
    end
end

function DamageGrid:Hide()
    self._floatPosition = Vector3.zero
end

function DamageGrid:RemoveAllItem()
    self._damageItemList = {}
    self._size = Vector3.zero
end

