--TextFloatItem.lua
local DamageItem = Class.DamageItem(ClassTypes.BaseLuaUIComponent)

local ImageWrapper = UIExtension.ImageWrapper
local XImageFloat = GameMain.XImageFloat
local XImageChanger = GameMain.XImageChanger
local Color = UnityEngine.Color

DamageItem.interface = GameConfig.ComponentsConfig.Component
DamageItem.classPath = "Modules.ModuleTextFloat.ChildComponent.DamageItem"

function DamageItem:Awake()
    self._image = self.gameObject:GetComponentsInChildren(typeof(ImageWrapper))
    self._ximageChanger = self.gameObject:AddComponent(typeof(XImageChanger))
    self._rectTransform = self.transform
    self.Name = ""
    self.TypeName = ""
    self._imageFloat = self.gameObject:AddComponent(typeof(XImageFloat))
end

function DamageItem:Show(position,type,callBack)
    self._imageFloat:StartFloat(position,type,callBack)
end

function DamageItem:Hide()
    self._rectTransform.localPosition = DamageManager.HIDE_POSITION
end

function DamageItem:SetScale(scale)
    self._rectTransform.localScale = scale
end

function DamageItem:GetScale()
    return self._rectTransform.localScale
end

function DamageItem:SetPivot(pivot)
    self._rectTransform.pivot = pivot
end

function DamageItem:GetPivot()
    return self._rectTransform.pivot
end

function DamageItem:SetPosition(position)
    self._rectTransform.localPosition = position
end

function DamageItem:GetPosition()
    return self._rectTransform.localPosition
end

function DamageItem:GetSize()
    return self._rectTransform.sizeDelta
end

function DamageItem:SetAlpha(alpha)
    self._imageFloat:SetAlpha(alpha)
end

function DamageItem:ChangeImage(name, gameObject)
    if name == self.Name then
        return
    end
    self._ximageChanger:ChangeImageByGO(gameObject)
    self.Name = name
end