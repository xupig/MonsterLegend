-- FunctionPreviewPanel.lua
local ClassTypes = ClassTypes
local FunctionPreviewPanel = Class.FunctionPreviewPanel(ClassTypes.BasePanel)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local EquipModelComponent = GameMain.EquipModelComponent
local ActorModelComponent = GameMain.ActorModelComponent
local public_config = GameWorld.public_config
local PartnerDataHelper = PartnerDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TransformDataHelper = GameDataHelper.TransformDataHelper
local UIParticle = ClassTypes.UIParticle
local StringStyleUtil = GameUtil.StringStyleUtil

function FunctionPreviewPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self:InitView()

    self._modelFuncMap = {
        [public_config.FUNCTION_ID_WING]      = public_config.TREASURE_TYPE_WING,
        [public_config.FUNCTION_ID_TALISMAN]  = public_config.TREASURE_TYPE_TALISMAN,
        --[public_config.FUNCTION_ID_WEAPON]    = public_config.TREASURE_TYPE_WEAPON,
        [public_config.FUNCTION_ID_CLOAK]     = public_config.TREASURE_TYPE_CLOAK,
        [public_config.FUNCTION_ID_PET]       = public_config.TREASURE_TYPE_PET,
        [public_config.FUNCTION_ID_HORSE]     = public_config.TREASURE_TYPE_HORSE
    }
end

function FunctionPreviewPanel:OnDestroy()
    self._csBH = nil
end

function FunctionPreviewPanel:OnShow(data)
    self.data = data
    self:RefreshView()
end

function FunctionPreviewPanel:SetData(data)
    self.data = data
    self:RefreshView()
end

function FunctionPreviewPanel:OnClose()

end

function FunctionPreviewPanel:InitView()
    self._partPreview = self:FindChildGO("Container_PartPreview")

    self._bgButtonPart = self:FindChildGO("Container_PartPreview/Button_BG")
    self._csBH:AddClick(self._bgButtonPart, function()self:OnBGButtonPartClick() end)
    self._iconContainerPart = self:FindChildGO("Container_PartPreview/Container_Icon")
    self._titleTextPart = self:GetChildComponent("Container_PartPreview/Text_Title", "TextMeshWrapper")
    self._infoTextPart = self:GetChildComponent("Container_PartPreview/Text_Info", "TextMeshWrapper")

    
    self._modelPreview = self:FindChildGO("Container_ModelPreview")

     self._bgButtonModel = self:FindChildGO("Container_ModelPreview/Button_BG")
    self._csBH:AddClick(self._bgButtonModel, function()self:OnBGButtonPartClick() end)

    self._modelTitleText = self:GetChildComponent("Container_ModelPreview/Text_Title", "TextMeshWrapper")
    self._modelInfoText = self:GetChildComponent("Container_ModelPreview/Text_Info", "TextMeshWrapper")

    self._containerActorModel = self:FindChildGO("Container_ModelPreview/Container_ActorModel")
    self._actorModelComponent = self:AddChildComponent('Container_ModelPreview/Container_ActorModel', ActorModelComponent)
    self._actorModelComponent:SetStartSetting(Vector3(0, 0, -150), Vector3(0, 180, 0), 1)

    self._containerTreasureModel = self:FindChildGO("Container_ModelPreview/Container_TreasureModel")
    self._treaureModelComponent = self:AddChildComponent('Container_ModelPreview/Container_TreasureModel', EquipModelComponent)
    self._fx1 = UIParticle.AddParticle(self.gameObject, "Container_ModelPreview/Container_Fx/FirstCharge")
   
   self._partPreview:SetActive(false)
   self._modelPreview:SetActive(false)
end

function FunctionPreviewPanel:OnBGButtonPartClick()
    GUIManager.ShowPanel(PanelsConfig.FunctionOpenTips, {2,self.data.id})
end

function FunctionPreviewPanel:OnBGButtonTipsClick()
    self._openTips:SetActive(false)
end

function FunctionPreviewPanel:RefreshView()
    local funcId = self.data.id
    if self._modelFuncMap[funcId] then
        self._partPreview:SetActive(false)
        self._modelPreview:SetActive(true)
        local modelType = self._modelFuncMap[funcId]

        if modelType < 5 then
            self._containerActorModel:SetActive(false)
            self._containerTreasureModel:SetActive(true)

            local vocationGroup = math.floor(GameWorld.Player().vocation/100)
            local uiModelCfgId = GlobalParamsHelper.GetParamValue(703+modelType)[vocationGroup]
            local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
            
            local modelId = modelInfo.model_ui
            local posx = modelInfo.pos[1]
            local posy = modelInfo.pos[2]
            local posz = modelInfo.pos[3]
            local rotationx = modelInfo.rotation[1]
            local rotationy = modelInfo.rotation[2]
            local rotationz = modelInfo.rotation[3]
            local scale = modelInfo.scale
            self._treaureModelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
            
            --神兵自转
            -- if modelType == public_config.TREASURE_TYPE_WEAPON then
            --     self._treaureModelComponent:SetSelfRotate(true)
            -- end

            if modelType == public_config.TREASURE_TYPE_TALISMAN then
                --法宝替换controller
                local controllerPath = GlobalParamsHelper.GetParamValue(692)
                self._treaureModelComponent:LoadEquipModel(modelId, controllerPath)
            else
                self._treaureModelComponent:LoadEquipModel(modelId, "")
            end
        else
            self._containerActorModel:SetActive(true)
            self._containerTreasureModel:SetActive(false)
            self._selectedGrade = 1
            if modelType == public_config.TREASURE_TYPE_PET then
                local scale = PartnerDataHelper.GetPetScale(self._selectedGrade)
                local y = PartnerDataHelper.GetPetRotate(self._selectedGrade)
                self._actorModelComponent:SetStartSetting(Vector3(0, 0, -150), Vector3(0, y, 0), scale)
                local modelId = PartnerDataHelper.GetPetModel(self._selectedGrade)
                self._actorModelComponent:LoadModel(modelId)
            else
                local scale = PartnerDataHelper.GetRideScale(self._selectedGrade)
                self._actorModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 90, 0), scale)
                local modelId = PartnerDataHelper.GetRideModel(self._selectedGrade)
                self._actorModelComponent:LoadModel(modelId)
            end
        end

        self._modelTitleText.text = LanguageDataHelper.GetContent(self.data.name)
        self._modelInfoText.text = LanguageDataHelper.CreateContentWithArgs(179, {["0"] = self.data.preview_level[1]})
    else
        self._partPreview:SetActive(true)
        self._modelPreview:SetActive(false)
        local iconId = self.data.icon
        GameWorld.AddIcon(self._iconContainerPart, iconId)
        self._titleTextPart.text = LanguageDataHelper.GetContent(self.data.name)
        local levelStr = StringStyleUtil.GetLevelString(self.data.preview_level[1],nil,true)
        self._infoTextPart.text = LanguageDataHelper.CreateContentWithArgs(179, {["0"] = levelStr})
    end
end