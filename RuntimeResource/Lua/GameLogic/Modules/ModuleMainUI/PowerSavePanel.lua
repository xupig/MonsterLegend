-- PowerSavePanel.lua
local PowerSavePanel = Class.PowerSavePanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function PowerSavePanel:Awake()
	self:InitView()
end

function PowerSavePanel:InitView()
    local btnClose = self:FindChildGO("Container_Base/Container_Block/Button_Close")
    self._csBH:AddClick(btnClose, function() self:OnCloseClick() end)
end

function PowerSavePanel:OnCloseClick()
	PlayerManager.PowerSaveManager:StopPowerSave()
    GUIManager.ClosePanel(PanelsConfig.PowerSave)
end

