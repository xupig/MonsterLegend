-- LeftMainUIPanel.lua
--主界面左侧部分
local LeftMainUIPanel = Class.LeftMainUIPanel(ClassTypes.BasePanel)

require "Modules.ModuleMainUI.ChildComponent.CollectButton"

require "Modules.ModuleMainUI.ChildView.TeamQuickView"
require "Modules.ModuleMainUI.ChildView.TaskView"
require "Modules.ModuleMainUI.ChildView.BossView"
-- require "Modules.ModuleMainUI.ChildView.WildEventView"
-- require "Modules.ModuleMainUI.ChildView.WildEventDetailsView"
local TeamQuickView = ClassTypes.TeamQuickView
local TaskView = ClassTypes.TaskView
local BossView = ClassTypes.BossView
-- local WildEventView = ClassTypes.WildEventView
-- local WildEventDetailsView = ClassTypes.WildEventDetailsView
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local GUIManager = GameManager.GUIManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TeamDataHelper = GameDataHelper.TeamDataHelper
local SceneConfig = GameConfig.SceneConfig
local TaskCommonManager = PlayerManager.TaskCommonManager
local TeamManager = PlayerManager.TeamManager
local XAutoCombat = GameMain.XAutoCombat

function LeftMainUIPanel:Awake()
    -- self._csBH = self:GetComponent("LuaUIPanel")
    self._sceneType = SceneConfig.SCENE_TYPE_MAIN
    self._initComplete = false
    self:InitComp()
    self:InitAnimation()
end

function LeftMainUIPanel:OnShow(sceneType)
    self._taskClickCount = 0
    self._teamClickCount = 0
    self._functionToggleGroup:SetSelectIndex(0)
    -- LoggerHelper.Error("self._initComplete ========" .. tostring(self._initComplete))
    EventDispatcher:AddEventListener(GameEvents.CIRCLE_TASK_REFRESH, self._onCircleTaskChanged)
    -- EventDispatcher:AddEventListener(GameEvents.OnTaskChanged, self._onTaskChanged)
    EventDispatcher:AddEventListener(GameEvents.ON_TASK_MENU_CHANGE, self._onTaskMenuChanged)
    -- EventDispatcher:AddEventListener(GameEvents.ON_GUARD_GODDESS_CHANGE, self._onGuardGoddessChanged)
    EventDispatcher:AddEventListener(GameEvents.OnWorldBossSpaceBossInfo, self._onWorldBossSpaceBossInfo)
    EventDispatcher:AddEventListener(GameEvents.MATCH_BEGIN, self._onMatchBegin)
    EventDispatcher:AddEventListener(GameEvents.MATCH_END, self._onMatchEnd)
    self:SetData(sceneType)
    if TaskCommonManager:IsInitComplete() then
        --任务列表初始化完成
        self:OnCompleteTaskListInit()
    else
        --任务列表未初始化完成
        EventDispatcher:AddEventListener(GameEvents.TASK_LIST_INIT_COMPLETE, self._onCompleteTaskListInit)
    end
    self:RefreshTaskData()
end

function LeftMainUIPanel:SetData(sceneType)
    self._sceneType = sceneType
    if SceneConfig.IsBossScene(sceneType) then
        self:ShowBoss()
    else
        self:HideBoss()
    end
end

function LeftMainUIPanel:OnClose()
    EventDispatcher:RemoveEventListener(GameEvents.CIRCLE_TASK_REFRESH, self._onCircleTaskChanged)
    -- EventDispatcher:RemoveEventListener(GameEvents.OnTaskChanged, self._onTaskChanged)
    EventDispatcher:RemoveEventListener(GameEvents.ON_TASK_MENU_CHANGE, self._onTaskMenuChanged)
    -- EventDispatcher:RemoveEventListener(GameEvents.ON_GUARD_GODDESS_CHANGE, self._onGuardGoddessChanged)
    EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossSpaceBossInfo, self._onWorldBossSpaceBossInfo)
    EventDispatcher:RemoveEventListener(GameEvents.MATCH_BEGIN, self._onMatchBegin)
    EventDispatcher:RemoveEventListener(GameEvents.MATCH_END, self._onMatchEnd)
    
    self._xAutoCombat.IsTweening = false
end

function LeftMainUIPanel:InitComp()
    self._functionToggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "Container_TaskAndTeam/ToggleGroup_Function")
    self._functionToggleGroup:SetOnSelectedIndexChangedCB(function(index)self:OnFunctionBarClick(index) end)
    self._functionToggleGroup:SetRepeatClick(true)
    
    --记录每个标签连续点击的次数
    self._taskClickCount = 0
    self._teamClickCount = 0
    
    self._btnContract = self:FindChildGO("Button_Contract")
    self._csBH:AddClick(self._btnContract, function()self:OnClickContract() end)
    self._imgLeft = self:FindChildGO("Button_Contract/Image_Left")
    self._imgRight = self:FindChildGO("Button_Contract/Image_Right")
    
    self._labelCheckmarkBoss = self:FindChildGO("Container_TaskAndTeam/ToggleGroup_Function/Toggle_TaskAndBoss/Checkmark/Label_Boss")
    self._labelBackgroundBoss = self:FindChildGO("Container_TaskAndTeam/ToggleGroup_Function/Toggle_TaskAndBoss/Background/Label_Boss")
    self._labelCheckmarkTask = self:FindChildGO("Container_TaskAndTeam/ToggleGroup_Function/Toggle_TaskAndBoss/Checkmark/Label_Task")
    self._labelBackgroundTask = self:FindChildGO("Container_TaskAndTeam/ToggleGroup_Function/Toggle_TaskAndBoss/Background/Label_Task")
    -- self._textToggleTaskCheckmark = self:GetChildComponent("Container_TaskAndTeam/ToggleGroup_Function/Toggle_TaskAndBoss/Checkmark/Label", "TextMeshWrapper")
    -- self._textToggleTaskBackground = self:GetChildComponent("Container_TaskAndTeam/ToggleGroup_Function/Toggle_TaskAndBoss/Background/Label", "TextMeshWrapper")
    self._teamQuickView = self:AddChildLuaUIComponent("Container_TaskAndTeam/Container_TeamQuick", TeamQuickView)
    self._taskView = self:AddChildLuaUIComponent("Container_TaskAndTeam/Container_Task", TaskView)
    self._bossView = self:AddChildLuaUIComponent("Container_TaskAndTeam/Container_Boss", BossView)
    self._bossView.gameObject:SetActive(false)
    
    self._matchContainer = self:FindChildGO("Container_TaskAndTeam/Container_Match")
    self._cancelMatchButton = self:FindChildGO("Container_TaskAndTeam/Container_Match/Button_Cancel")
    self._csBH:AddClick(self._cancelMatchButton, function()self:OnCancelMatchButtonClick() end)
    self._matchInstanceNameText = self:GetChildComponent("Container_TaskAndTeam/Container_Match/Text_Instance_Name", "TextMeshWrapper")
    
    -- self._wildEventView = self:AddChildLuaUIComponent("Container_WildEvent", WildEventView)
    -- self._wildEventDetailsView = self:AddChildLuaUIComponent("Container_WildEventDetails", WildEventDetailsView)
    -- self._wildEventDetailsView.gameObject:SetActive(false)
    -- self._onTaskChanged = function()self:RefreshTaskData() end
    self._onTaskMenuChanged = function()self:RefreshTaskData() end
    -- self._onGuardGoddessChanged = function()self:RefreshTaskData() end
    -- self._onCircleTaskChanged = function() self:RefreshTaskData() end
    self._onWorldBossSpaceBossInfo = function()self:OnWorldBossSpaceBossInfo() end
    self._onCompleteTaskListInit = function()self:OnCompleteTaskListInit() end
    self._onMatchBegin = function()self:OnMatchBegin() end
    self._onMatchEnd = function()self:OnMatchEnd() end
    -- self._wildEventView:SetShowWildEventDetailsCallback(function()self:ShowWildEventDetails() end)
    -- self._wildEventView:SetHideWildEventDetailsCallback(function()self:HideWildEventDetails() end)
    self._leftTabView = self._taskView
    
    local obj = self:FindChildGO("Container_TaskAndTeam/Container_Match/Container_Images")
    self._xAutoCombat = obj:AddComponent(typeof(XAutoCombat))
    
    
    self:SetIsBossOrTask(false)
end

function LeftMainUIPanel:OnFunctionBarClick(index)
    self._selectedIndex = index
    if index == 0 then
        self._taskClickCount = self._taskClickCount + 1
        self._teamClickCount = 0
        if self._taskClickCount > 1 then
            if self._isTask then
                GUIManager.ShowPanel(PanelsConfig.Task)
            end
        else
            self._teamQuickView:SetActive(false)
            self._leftTabView:SetActive(true)
            self._matchContainer:SetActive(false)
            self._xAutoCombat.IsTweening = false
        end
    else
        self._teamClickCount = self._teamClickCount + 1
        self._taskClickCount = 0
        if self._teamClickCount > 1 then
            --有队伍打开队伍
            if GameWorld.Player().team_status > 0 then
                GUIManager.ShowPanel(PanelsConfig.Team, {firstTabIndex = 2})
            --没有队伍打开队伍列表
            else
                GUIManager.ShowPanel(PanelsConfig.Team, {firstTabIndex = 1})
            end
        else
            local match_state = GameWorld.Player().match_state
            if match_state == 0 then
                self._leftTabView:SetActive(false)
                self._teamQuickView:SetActive(true)
                self._matchContainer:SetActive(false)
                self._xAutoCombat.IsTweening = false
            else
                --匹配中
                self._leftTabView:SetActive(false)
                self._teamQuickView:SetActive(false)
                self._matchContainer:SetActive(true)
                self._xAutoCombat:StartAnimation()
                self._matchInstanceNameText.text = TeamDataHelper.GetNameByMapId(match_state)
            end
        -- self._teamQuickView:OnShow()
        end
    end
end

function LeftMainUIPanel:RefreshTaskData()
    if TaskCommonManager:IsInitComplete() then
        self._taskView:RefreshTaskData()
    end
end

function LeftMainUIPanel:OnWorldBossSpaceBossInfo()
    -- LoggerHelper.Log("Ash: OnWorldBossSpaceBossInfo")
    self._bossView:RefreshBossData()
end

function LeftMainUIPanel:OnCompleteTaskListInit()
    EventDispatcher:RemoveEventListener(GameEvents.TASK_LIST_INIT_COMPLETE, self._onCompleteTaskListInit)

-- self._taskView:InitTaskData()
end

function LeftMainUIPanel:ShowBoss()
    if self._timerId then
        TimerHeap:DelTimer(self._timerId)
    end
    
    self._bossView:SetActive(true)
    self:SetIsBossOrTask(true)
    self._bossView:InitType(self._sceneType)
    self._bossView:RefreshBossData()
    --local activeSelf = self._leftTabView.gameObject.activeSelf
    -- LoggerHelper.Log("Ash: ShowBoss: " .. self._selectedIndex)
    if self._selectedIndex == 0 then
        if self._leftTabView ~= self._bossView then
            self._leftTabView:SetActive(false)
        end
    else
        self._bossView:SetActive(false)
    end
    self._leftTabView = self._bossView
-- self._textToggleTaskCheckmark.text = LanguageDataHelper.CreateContent(913)-- 事件
-- self._textToggleTaskBackground.text = LanguageDataHelper.CreateContent(913)-- 事件
end

function LeftMainUIPanel:HideBoss()
    -- LoggerHelper.Log("Ash: HideBoss")
    --local activeSelf = self._leftTabView.gameObject.activeSelf
    if self._selectedIndex == 0 then
        self._leftTabView:SetActive(false)
        self._taskView:SetActive(true)
    end
    self._leftTabView = self._taskView
    
    self:SetIsBossOrTask(false)
-- self._textToggleTaskCheckmark.text = LanguageDataHelper.CreateContent(65)-- 任务
-- self._textToggleTaskBackground.text = LanguageDataHelper.CreateContent(65)-- 任务
end

-- function LeftMainUIPanel:DoHideWildEventDetails()
-- end
function LeftMainUIPanel:SetIsBossOrTask(isBoss)
    self._isTask = not isBoss
    self._labelCheckmarkBoss:SetActive(isBoss)
    self._labelBackgroundBoss:SetActive(isBoss)
    self._labelCheckmarkTask:SetActive(not isBoss)
    self._labelBackgroundTask:SetActive(not isBoss)
end

function LeftMainUIPanel:OnClickContract()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function LeftMainUIPanel:InitAnimation()
    self._isOpen = true
    self._containerTaskAndTeam = self:FindChildGO("Container_TaskAndTeam")
    self._tweenPosition = self._containerTaskAndTeam:AddComponent(typeof(XGameObjectTweenPosition))
end

function LeftMainUIPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    self._containerTaskAndTeam.transform.localPosition = Vector3.New(-370, 360, 0)
    self._tweenPosition:SetToPosOnce(Vector3.New(0, 360, 0), 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function LeftMainUIPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._containerTaskAndTeam.transform.localPosition = Vector3.New(0, 360, 0)
    self._tweenPosition:SetToPosOnce(Vector3.New(-370, 360, 0), 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

function LeftMainUIPanel:OnCancelMatchButtonClick()
    TeamManager:RequestTeamMatchCancel()
end

function LeftMainUIPanel:OnMatchBegin()
    self._taskClickCount = 0
    self._teamClickCount = 0
    self._functionToggleGroup:SetSelectIndex(1)
end

function LeftMainUIPanel:OnMatchEnd()
    if self._selectedIndex ~= 0 then
        self._teamClickCount = 0
        self._taskClickCount = 0
        self._functionToggleGroup:SetSelectIndex(1)
    end
end
