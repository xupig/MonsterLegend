-- NewEquipTipPanel.lua
--有新装备/新道具/守护续费提示玩家要装备的小信息面板
local NewEquipTipPanel = Class.NewEquipTipPanel(ClassTypes.BasePanel)
local GUIManager = GameManager.GUIManager
local EquipManager = GameManager.EquipManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local BagManager = PlayerManager.BagManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local public_config = GameWorld.public_config
local NewEquipType = GameConfig.EnumType.NewEquipType
local TimerHeap = GameWorld.TimerHeap

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

function NewEquipTipPanel:Awake()
	self.disableCameraCulling = true
	self._txtName = self:GetChildComponent("Text_Name", "TextMeshWrapper")

	self._btnUse = self:FindChildGO("Button_Use")
	self._csBH:AddClick(self._btnUse, function()self:OnClickUse() end)
	self._txtBtnName = self:GetChildComponent("Button_Use/Text", "TextMeshWrapper")
	GameWorld.AddHitArea(self._btnUse, 138, 68, 52, 22)

    self._btnClose = self:FindChildGO("Button_Close")
	self._csBH:AddClick(self._btnClose, function()self:NextItem() end)
	GameWorld.AddHitArea(self._btnClose, 76, 76, 21, 21)

    self._imgArrow = self:FindChildGO("Image_Arrow")

    local parent = self:FindChildGO("Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
	self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)

    self._itemDatas = {}
    --self._powerUps = {}

    self:InitCallback()
end

function NewEquipTipPanel:InitCallback()
	self._equipRemoveCb = function (pkgType,pkgPos)
    	self:CheckEquipRemove(pkgType,pkgPos)
    end
end

function NewEquipTipPanel:OnShow()
	-- self._cdTime = 5
	-- self._timer = TimerHeap:AddSecTimer(0,1,0,function ()
	-- 	self:Cooldown()
	-- end)
	EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_REMOVE,self._equipRemoveCb)
	--EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_UPDATE,self._equipRemoveCb)
end

function NewEquipTipPanel:OnClose()
	self._itemDatas = {}
	self:RemoveEventListeners()
end

function NewEquipTipPanel:RemoveEventListeners()
	EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_REMOVE,self._equipRemoveCb)
	--EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_UPDATE,self._equipRemoveCb)
end

function NewEquipTipPanel:OnDestroy()
	self:ClearTimer()
	self:RemoveEventListeners()
end

function NewEquipTipPanel:ClearTimer()
	if self._timer then
		TimerHeap:DelTimer(self._timer)
		self._timer = nil
	end
end

-- function NewEquipTipPanel:Cooldown()
-- 	if self._cdTime <= 0 then
-- 		self:NextItem()
-- 		self._cdTime = 5
-- 	end
-- 	self._txtCD.text = "("..self._cdTime..")"
-- 	self._cdTime = self._cdTime - 1
-- end

function NewEquipTipPanel:CheckEquipRemove(pkgType,pkgPos)
	--LoggerHelper.Error("NewEquipTipPanel:CheckEquipRemove")
	local showType = self._itemDatas[1][2]
	if showType == NewEquipType.Equip or showType == NewEquipType.Use then
		if self._curItemData.bagPos == pkgPos and self._curItemData.bagType == pkgType then
			self:NextItem()
			--LoggerHelper.Error("NewEquipTipPanel:CheckEquipRemove11")
		else
			for i=#self._itemDatas,2,-1 do
				-- LoggerHelper.Error("#self._itemDatas:"..PrintTable:TableToStr(self._itemDatas[i]))
				-- LoggerHelper.Error("#self._itemDataswerl:"..self._itemDatas[i].bagPos..","..pkgPos..","..self._itemDatas[i].bagType..","..pkgType)
				if self._itemDatas[i][1].bagPos == pkgPos and self._itemDatas[i][1].bagType == pkgType then
					--LoggerHelper.Error("NewEquipTipPanel:CheckEquipRemove33")
					table.remove(self._itemDatas,i)
					--LoggerHelper.Error("#self._itemDatas:2222")
				end
			end
		end
	end
end

function NewEquipTipPanel:NextItem()
	--LoggerHelper.Error("#self._itemDatas:3333")
	table.remove(self._itemDatas,1)
	--结束
	if #self._itemDatas == 0 then
		-- self:ClearTimer()
		self._curItemData = nil
		GUIManager.ClosePanel(PanelsConfig.NewEquipTip)
		self:ClearTimer()
	else
		self:UpdateInfo()
	end


end

function NewEquipTipPanel:AddEquipData(itemData)--powerUp
	table.insert(self._itemDatas,{itemData,NewEquipType.Equip})
	self:UpdateInfo()
end

--离线挂机卡
function NewEquipTipPanel:AddOffineData(ItemId,func)--powerUp
	table.insert(self._itemDatas,{ItemId,NewEquipType.OffineRemind,func})
	self:UpdateInfo()
end

function NewEquipTipPanel:AddItemUseData(itemData)--powerUp
	local shouldAddData = true
	for i=1,#self._itemDatas do
		local showType = self._itemDatas[i][2]
		if showType ~= NewEquipType.OffineRemind and itemData.bagPos == self._itemDatas[i][1].bagPos then
			shouldAddData = false
		end
	end
	if shouldAddData then
		table.insert(self._itemDatas,{itemData,NewEquipType.Use})
	end
	self:UpdateInfo()
end

function NewEquipTipPanel:AddGuardData(itemData)--powerUp
	table.insert(self._itemDatas,{itemData,NewEquipType.Renew})
	self:UpdateInfo()
end

function NewEquipTipPanel:UpdateInfo()
	self._curItemData = self._itemDatas[1][1]
	local showType = self._itemDatas[1][2]
	if showType == NewEquipType.Equip then
		self:ClearTimer()
		self._leftTime = 6
		self._txtBtnName.text = LanguageDataHelper.CreateContent(54).."("..self._leftTime..")" 
		self._timer = TimerHeap:AddSecTimer(0, 1, 0, function() self:OnWillEquip() end)
		self._imgArrow:SetActive(true)
	elseif showType == NewEquipType.Use then
		self._txtBtnName.text = LanguageDataHelper.CreateContent(568)
		self._imgArrow:SetActive(false)
	elseif showType == NewEquipType.OffineRemind then
		self._txtBtnName.text = LanguageDataHelper.CreateContent(565)
		self._txtName.text = LanguageDataHelper.CreateContent(567)
		self._callFunc = self._itemDatas[1][3]
		self._icon:SetItem(self._curItemData)
		return
	elseif showType == NewEquipType.Renew then 
		self._txtBtnName.text = LanguageDataHelper.CreateContent(566)
		self._imgArrow:SetActive(false)
	end

	self._txtName.text = ItemDataHelper.GetItemNameWithColor(self._curItemData.cfg_id)
	self._icon:SetItemData(self._curItemData)
end


--装备
function NewEquipTipPanel:OnWillEquip()
	if self._curItemData then
		local showType = self._itemDatas[1][2]
		if showType == NewEquipType.Equip then
		    if (self._leftTime > 1) then
				self._leftTime = self._leftTime - 1
				self._txtBtnName.text = LanguageDataHelper.CreateContent(54).."("..self._leftTime..")" 
			else
				self._txtBtnName.text = LanguageDataHelper.CreateContent(54)
				self:ClearTimer()
				self:OnEquipGo()	
			end
		end
	end
end

--使用
function NewEquipTipPanel:OnClickUse()
	if self._curItemData then
		local showType = self._itemDatas[1][2]
		if showType == NewEquipType.Equip then
			self:OnEquipGo()	
		elseif showType == NewEquipType.Use then
			self:OnUseGo()
		elseif showType == NewEquipType.Renew then 
			self:OnRenewGo()
		elseif showType == NewEquipType.OffineRemind then
			self:OnViewGo()
		end
	end
end

--装装备
function NewEquipTipPanel:OnEquipGo()
	self:ClearTimer()
	EquipManager:PutOnEquip(self._curItemData.bagPos)
	self:NextItem()	
end

--查看
function NewEquipTipPanel:OnViewGo()
	if self._callFunc then
		self._callFunc()
	end
	self:NextItem()
end




function NewEquipTipPanel:OnUseGo()
	local func = function() self:NextItem() end
	BagManager:OnUseClick(self._curItemData,func)
	--self:NextItem() 
end




function NewEquipTipPanel:OpenPanelFunction(funcId)
    local funcOpen = FunctionOpenDataHelper:CheckFunctionOpen(funcId)
    if funcOpen then
        GUIManager.ClosePanel(PanelsConfig.Role)
        GUIManager.ShowPanelByFunctionId(funcId)
        return true
    else
        local funcName = LanguageDataHelper.CreateContentWithArgs(FunctionOpenDataHelper:GetFunctionOpenName(funcId))
        local str = funcName .. LanguageDataHelper.CreateContent(52072)
        GameManager.GUIManager.ShowText(1, str)
        return false
    end
end

--守护续费
function NewEquipTipPanel:OnRenewGo()
	local costCfg = GlobalParamsHelper.GetParamValue(542)
	local cost = costCfg[self._curItemData.cfg_id]
	local str = LanguageDataHelper.CreateContentWithArgs(58708,{["0"] = cost})
	GUIManager.ShowMessageBox(2,str,function ()
		self:RequsetRenew()
	end)
end

function NewEquipTipPanel:RequsetRenew()
	EquipManager:RequestGuardBuy(self._curItemData.bagType,self._curItemData.bagPos)
	self:NextItem()
end
