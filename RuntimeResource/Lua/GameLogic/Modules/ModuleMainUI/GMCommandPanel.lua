-- GMCommandPanel.lua
local GMCommandPanel = Class.GMCommandPanel(ClassTypes.BasePanel)

local GameObject = UnityEngine.GameObject
local GameSceneManager = GameManager.GameSceneManager
local SystemInfoManager = GameManager.SystemInfoManager
local ReloadUtil = GameUtil.ReloadUtil

function GMCommandPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel');
    self.Parent = self:FindChildGO("Container_Buttons");
    self.Button = self:FindChildGO("Container_Buttons/Button");
    self.Content = self:FindChildGO("Container_Content")

    self._InputText = self:GetChildComponent("InputField", 'InputFieldMeshWrapper')
    self._InputText.text = "Abyss"

    self:InitButtonLayout()
end

function GMCommandPanel:OnDestroy() 
    self.Button = nil
    self.Content = nil
    self._csBH = nil
end

function GMCommandPanel:InitButtonLayout() 
    
    self:InitButton(Vector3.New(0,0,0),'关闭',function()
        -- GameSceneManager:ComeHome()
        GameManager.GUIManager.ClosePanel(PanelsConfig.GMCommand)
    end)

    self:InitButton(Vector3.New(250,0,0),'聊天测试',function()
            -- print('test GM2 ')
            GameWorld.TestUIComponent(self.Content,1);
        end)

    self:InitButton(Vector3.New(500,0,0),'demo',function()
            -- print('test GM2 ')
            GameManager.GUIManager.ShowPanel(PanelsConfig.Demo)
        end)

    self:InitButton(Vector3.New(750,0,0),'飘字',function()
            SystemInfoManager:ShowClientTip(1)
        end)

    self:InitButton(Vector3.New(280,-630,0),'Reload',function()
            ReloadUtil.ReloadModule(self._InputText.text)
        end)
end

function GMCommandPanel:InitButton(pos,des,func)
    local go = GameObject.Instantiate(self.Button)
    go.transform:SetParent(self.Parent.transform);
    go.transform.localPosition = pos
    go.transform.localScale = Vector3.New(1,1,1)
    go.gameObject:SetActive(true)
    self._csBH:AddClick(go, function() func()   end);
    local _textGo = go.transform:Find("Text");
    _textGo:GetComponent("TextMeshWrapper").text = des
    go:GetComponent("ImageWrapper").raycastTarget = true;
end