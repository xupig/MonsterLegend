-- BuffMainUIPanel.lua

local BuffMainUIPanel = Class.BuffMainUIPanel(ClassTypes.BasePanel)

require "Modules.ModuleMainUI.ChildComponent.BuffItem"
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AddRateDataHelper = GameDataHelper.AddRateDataHelper
local UIToggle = ClassTypes.UIToggle
local BuffItem = ClassTypes.BuffItem
local BuffDataHelper = GameDataHelper.BuffDataHelper
local GUIManager = GameManager.GUIManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DateTimeUtil = GameUtil.DateTimeUtil
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local TipsManager = GameManager.TipsManager

local MainUIManager = PlayerManager.MainUIManager
local UIList = ClassTypes.UIList


function BuffMainUIPanel:Awake()

	self._btnClose = self:FindChildGO("Container_Tips/Button_Close")
	self._csBH:AddClick(self._btnClose, function()self:OnCloseClick() end)
	

	self:InitView()
	self:InitListenerFunc()
end

function BuffMainUIPanel:InitListenerFunc()
	self._onBuffListChange = function () self:RefreshData() end
end

function BuffMainUIPanel:OnCloseClick()
	GUIManager.ClosePanel(PanelsConfig.Buff)
end

function BuffMainUIPanel:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Tips/ScrollViewList")
    self._buffList = list
    self._bufflistlView = scrollView
    self._bufflistlView:SetHorizontalMove(false)
    self._bufflistlView:SetVerticalMove(true)
    self._buffList:SetItemType(BuffItem)
    self._buffList:SetPadding(0, 0, 0, 0)
    self._buffList:SetGap(10, 0)
    self._buffList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function BuffMainUIPanel:RefreshData()
	local bufflist = MainUIManager:GetCurBuffList()
	self._buffList:SetDataList(bufflist)
end


function BuffMainUIPanel:OnShow(data)
	self:AddListener()
	self:RefreshData()
end

function BuffMainUIPanel:AddListener()

	EventDispatcher:AddEventListener(GameEvents.BUFF_LIST_CHANGE, self._onBuffListChange)
end

function BuffMainUIPanel:RemoveListener()
	EventDispatcher:RemoveEventListener(GameEvents.BUFF_LIST_CHANGE, self._onBuffListChange)
end

function BuffMainUIPanel:OnClose()
	self:RemoveListener()

end

function BuffMainUIPanel:InitViews(index)

end
