-- ActivityOpenTipsPanel.lua
local ActivityOpenTipsPanel = Class.ActivityOpenTipsPanel(ClassTypes.BasePanel)
local WorldBossManager = PlayerManager.WorldBossManager
local BossHomeManager = PlayerManager.BossHomeManager
local BackWoodsManager = PlayerManager.BackWoodsManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local GuildDataHelper = GameDataHelper.GuildDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local DailyActivityExManager = PlayerManager.DailyActivityExManager

function ActivityOpenTipsPanel:Awake()
    self.disableCameraCulling = true
    self:InitPanel()
end

function ActivityOpenTipsPanel:InitPanel()
    self._textTitle = self:GetChildComponent("Container_Right/Text_Title", "TextMeshWrapper")
    self._btnGo = self:FindChildGO("Container_Right/Button_Go")
    self._csBH:AddClick(self._btnGo, function()self:GoBtnClick() end)
    self._btnClose = self:FindChildGO("Container_Right/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    self._containerIcon = self:FindChildGO("Container_Right/Container_Icon")

    self._textTitle.text = LanguageDataHelper.CreateContent(53463)
    local monsterId = GuildDataHelper:GetGuildMonsterData(1).monster_id
    local icon = MonsterDataHelper.GetIcon(monsterId)
    GameWorld.AddIcon(self._containerIcon, icon)
end

function ActivityOpenTipsPanel:OnShow(id)
    self:SetData(id)
end

function ActivityOpenTipsPanel:GoBtnClick()
    -- GUIManager.ShowPanelByFunctionId(415)
    local follow = ActivityDailyDataHelper:GetFollow(self._id)
    DailyActivityExManager:DoFollowEvent(follow)
    GUIManager.ClosePanel(PanelsConfig.ActivityOpenTips)
end

function ActivityOpenTipsPanel:CloseBtnClick()
    GUIManager.ClosePanel(PanelsConfig.ActivityOpenTips)
end

function ActivityOpenTipsPanel:SetData(id)
    self._id = id
    local icon = ActivityDailyDataHelper:GetIcon(id)
    local name = LanguageDataHelper.CreateContent(ActivityDailyDataHelper:GetName(id))
    self._textTitle.text = LanguageDataHelper.CreateContent(52120, {["0"] = name})
    GameWorld.AddIcon(self._containerIcon, icon)

    self:ClearTimer()
    self._countdownTimer = GameWorld.TimerHeap:AddSecTimer(60, 0, 0, function()self:OnCountdownTick() end)
end

function ActivityOpenTipsPanel:OnCountdownTick()
    GUIManager.ClosePanel(PanelsConfig.ActivityOpenTips)
end

function ActivityOpenTipsPanel:OnClose()
    self:ClearTimer()
end

function ActivityOpenTipsPanel:ClearTimer()
    if self._countdownTimer ~= nil then
        GameWorld.TimerHeap:DelTimer(self._countdownTimer)
        self._countdownTimer = nil
    end
end

function ActivityOpenTipsPanel:OnDestroy()
    self._textName = nil
    self._btnGo = nil
    self._btnClose = nil
    self._containerIcon = nil
end
