-- CollectPanel.lua
--客户端采集/采集/拾取/复活/传送倒计时面板
local CollectPanel = Class.CollectPanel(ClassTypes.BasePanel)

require "Modules.ModuleMainUI.ChildComponent.CollectButton"

local CollectButton = ClassTypes.CollectButton
local CollectManager = GameManager.CollectManager
local MapManager = PlayerManager.MapManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CollectDataHelper = GameDataHelper.CollectDataHelper

-- CollectPanel.MODE_TELEPORT = 5
CollectPanel.MODE_CLIENT_ENTITY_COLLECT = 2
-- CollectPanel.MODE_RESCUE = 3
CollectPanel.MODE_COMMON_COLLECT = 3
CollectPanel.MODE_COLLECT = 1

function CollectPanel:Awake()
	self.disableCameraCulling = true
	self:InitCallback()
	self:InitComps()
	self._collectList = {}

	-----------------------模式 5传送 4客户端采集，3.复活，2宝箱探索，1采集
	-------------优先级：传送客＞户端采集＞采集复活＞宝箱探索＞采集
	self._mode = 0 
	self._modeState = {}
end

function CollectPanel:InitCallback()
	self._addModeCb = function (mode) self:AddMode(mode) end
	self._removeModeCb = function (mode) self:RemoveMode(mode) end
end

function CollectPanel:OnShow()
	self:HideTips()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_COLLECT_ADD_MODE,self._addModeCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_COLLECT_REMOVE_MODE,self._removeModeCb)
	EventDispatcher:AddEventListener(GameEvents.START_COLLECT_CLIENT_RESOURCE,self._startCollectClientResource)
	EventDispatcher:AddEventListener(GameEvents.START_COLLECT_ITEM,self._startColectItemCB)
end

function CollectPanel:OnClose()
	--这个面板的消息不移除，有可能在active false状态下提前执行collect button显示
	-- self:RemoveEventListeners()
end

function CollectPanel:OnDestroy()
	self:RemoveEventListeners()
end

function CollectPanel:RemoveEventListeners()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_COLLECT_ADD_MODE,self._addModeCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_COLLECT_REMOVE_MODE,self._removeModeCb)
	EventDispatcher:RemoveEventListener(GameEvents.START_COLLECT_CLIENT_RESOURCE,self._startCollectClientResource)
	EventDispatcher:RemoveEventListener(GameEvents.START_COLLECT_ITEM,self._startColectItemCB)
end


function CollectPanel:InitComps()
	--self._btnPick = self:AddChildLuaUIComponent("Container_Pick", CollectButton)
	self._btnCollect = self:AddChildLuaUIComponent("Container_Collect", CollectButton)
	--self._btnRescue = self:AddChildLuaUIComponent("Container_Rescue", CollectButton)
	self._btnCommonCollect = self:AddChildLuaUIComponent("Container_CommonCollect", CollectButton)
	self._btnClientEntityCollect = self:AddChildLuaUIComponent("Container_ClientEntityCollect", CollectButton)
	--self._btnTeleport = self:AddChildLuaUIComponent("Container_Teleport", CollectButton)

	self._btns = {}
	--self._btns[CollectPanel.MODE_RESCUE] = self._btnRescue
	self._btns[CollectPanel.MODE_COMMON_COLLECT] = self._btnCommonCollect
	--self._btns[CollectPanel.MODE_PICK] = self._btnPickGO
	self._btns[CollectPanel.MODE_COLLECT] = self._btnCollect
	self._btns[CollectPanel.MODE_CLIENT_ENTITY_COLLECT] = self._btnClientEntityCollect
	--self._btns[CollectPanel.MODE_TELEPORT] = self._btnTeleport

	self._collectBtnInfo = GlobalParamsHelper.GetParamValue(53)
	self._mineBtnInfo = GlobalParamsHelper.GetParamValue(54)
	-- self._CommonCollectBtnInfo = GlobalParamsHelper.GetParamValue(55)
	-- self._teleportBtnInfo = GlobalParamsHelper.GetParamValue(56)
	-- self._rescueBtnInfo = GlobalParamsHelper.GetParamValue(57)

    --采集回调
    self._startColectItemCB = function () self:StartCollect() end

    -- self._endColectItemCB = 
    -- function ()
    --    self:StopCollect()
    -- end

	-- self._collectCompleteCB = function ()
	-- 	self:CompleteCollect()
	-- end

	--救赎回调
    --self._startRescueCB = function () self:StartRescue() end

	-- self._rescueCompleteCB = function ()
	-- 	self:CompleteRescue()
	-- end

	--通用采集回调
    self._startCommonCollectCB = function () self:StartCommonCollect() end

    --客户端采集回调
    self._startCollectClientResource = function () self:StartCollectClientResource() end

	self._btnCollect:SetPointerDownCB(self._startColectItemCB)
	self._btnCollect:InitCD()
	--self._btnCollect:SetCoolDownCompleteCB(self._collectCompleteCB)

	-- self._btnRescue:SetPointerUpCB(self._startRescueCB)
	-- self._btnRescue:InitCD()
	-- self._btnRescue:UpdateInfo(self._rescueBtnInfo[2],self._rescueBtnInfo[1])
	-- --self._btnRescue:SetCoolDownCompleteCB(self._rescueCompleteCB)

	self._btnCommonCollect:SetPointerUpCB(self._startCommonCollectCB)
	self._btnCommonCollect:InitCD()
	
	
	self._btnClientEntityCollect:SetPointerUpCB(self._startCollectClientResource)
	self._btnClientEntityCollect:InitCD()

	-- self._btnTeleport:InitCD()
	-- self._btnTeleport:UpdateInfo(self._teleportBtnInfo[2],self._teleportBtnInfo[1])

	self._btnCommonCollect:SetActive(false)
	self._btnCollect:SetActive(false)
	--self._btnRescue:SetActive(false)
	self._btnClientEntityCollect:SetActive(false)
	--self._btnTeleport:SetActive(false)

	self._contanerTips = self:FindChildGO("Container_Tips")
	self._txtTips = self:GetChildComponent("Container_Tips/Text_Tips", "TextMeshWrapper")
end

function CollectPanel:AddMode(mode)
	--LoggerHelper.Log("CollectPanel:AddMode"..mode)
	self._modeState[mode] = true
	self:HandleAllMode()
end

function CollectPanel:RemoveMode(mode)
	self._modeState[mode] = false
	self:HandleAllMode()
end

function CollectPanel:HandleAllMode()
	local firstBtnCheck = false --第一个检查出来的按钮才处理
	for i=#self._btns,1,-1 do
		if self._modeState[i] and not firstBtnCheck then
			self._btns[i]:SetActive(true)
			firstBtnCheck = true

			if i==1 then
				self:OnCollectShow()
			end
			--客户端采集点特殊处理
			if i == 2 then
				self:OnClientEntityCollectShow()
			end

			if i == 3 then
				self:OnCommonCollectShow()
			end
		else
			self._btns[i]:SetActive(false)
		end
	end
	
	if not self._inShowTipsState then
		self:HideTips()
	end
end

function CollectPanel:ShowTips(text)
	self._contanerTips:SetActive(true)
	self._txtTips.text = text
end

function CollectPanel:HideTips()
	self._inShowTipsState = false
	self._contanerTips:SetActive(false)
end

--设定当前按钮

---------------------采集处理-----------------------------
--采集按钮显示处理
function CollectPanel:OnCollectShow()
	local cid = CollectManager:GetCurCollectItemData()
	self._btnCollect:UpdateInfo(self._collectBtnInfo[2],self._collectBtnInfo[1])
	-- if cid then
	-- 	if cid.type == public_config.RESOURCE_WOOD or cid.type == public_config.RESOURCE_PLANT then
	-- 		self._btnCollect:UpdateInfo(self._collectBtnInfo[2],self._collectBtnInfo[1])
	-- 	else
	-- 		self._btnCollect:UpdateInfo(self._mineBtnInfo[2],self._mineBtnInfo[1])
	-- 	end
	-- end
end

--开始采集
function CollectPanel:StartCollect()
	--LoggerHelper.Error("StartCollect")
	CollectManager:StartCollectItem()
end

--后端确认可以开始采集
function CollectPanel:ConfirmStartCollect()
	local cid = CollectManager:GetCurCollectItemData()
	if cid then
		local cd = cid.collectCfg.collect_time*1000
		self._btnCollect:SetCoolDown(cd)
		EventDispatcher:TriggerEvent(GameEvents.PlayerCommandCollectItem, cid.itemEid,cd)
		local tips = cid.collectCfg.collecting_tips
		if tips > 0 then
			local str = LanguageDataHelper.CreateContent(tips)
			self:ShowTips(str)
		end
	end
end

--中断采集
function CollectPanel:StopCollect()
	--LoggerHelper.Error("EndCollect")
	self._btnCollect:Interrupt()
end


function CollectPanel:CompleteCollect(collectId,itemLeft)
	local collectCfg = CollectDataHelper.GetCollectCfg(collectId)
	if collectCfg and collectCfg.collected_tips > 0 then
		local collectName = LanguageDataHelper.CreateContent(collectCfg.name)
		local str = LanguageDataHelper.CreateContentWithArgs(collectCfg.collected_tips,{["0"] = collectName,["1"] = itemLeft})
		self:ShowTips(str)
		self._inShowTipsState = true
		self._tipsTimer = TimerHeap:AddSecTimer(2, 0, 0, function() self:CompleteCollectHidTips() end)
	else
		self:HideTips()
	end
end

function CollectPanel:CompleteCollectHidTips()
	TimerHeap:DelTimer(self._tipsTimer)
	self:HideTips()
end

-----------------------救赎处理---------------------------
-- --设置救赎cd
-- function CollectPanel:SetRescueCD(curRescueCD)
-- 	self._curRescueCD = curRescueCD
-- end

-- --开始救赎
-- function CollectPanel:StartRescue()
-- 	if self._curRescueCD then
-- 		--LoggerHelper.Error("CollectPanel:StartRescue")
-- 		self._btnRescue:SetCoolDown(self._curRescueCD)
-- 		ReviveRescueManager:StartRescue()
-- 	end
-- end

-- --救赎中断
-- function CollectPanel:StopRescue()
-- 	self._btnRescue:Interrupt()
-- end

-- ---------------------通用采集处理-----------------------------
--采集按钮显示处理
function CollectPanel:OnCommonCollectShow()
	local ccd = CollectManager:GetCurCommonCollectData()
	self._btnCommonCollect:UpdateInfo(ccd.collectName,ccd.collectIcon)
end

--开始通用采集吟唱
function CollectPanel:StartCommonCollect()
	local ccd = CollectManager:GetCurCommonCollectData()
	if ccd then
		--LoggerHelper.Log("StartOpenCommonCollect1")
		local cd = ccd.cdTime*1000
		self._btnCommonCollect:SetCoolDown(cd)
		EventDispatcher:TriggerEvent(GameEvents.PlayerCommandCommonCollect,cd)
		CollectManager:StartCommonCollect()
	end
end

--中断通用采集
function CollectPanel:StopOpenCommonCollect()
	--LoggerHelper.Error("EndCollect")
	self._btnCommonCollect:Interrupt()
end

---------------------客户端采集处理-----------------------------
--客户端采集按钮出现，更新数据
function CollectPanel:OnClientEntityCollectShow()
	local ccid = CollectManager:GetCurClientCollectEntityData()
	if ccid then
		self._btnClientEntityCollect:UpdateInfo(ccid.collectName,ccid.collectIcon)
		CollectManager:CollectClientPrepareAction()
	end
end

--开始客户端采集
function CollectPanel:StartCollectClientResource()
	local ccid = CollectManager:GetCurClientCollectEntityData()
	if ccid then
		--LoggerHelper.Error("StartCollectClientResourc1"..ccid.cdTime*1000)
		local cd = ccid.cdTime*1000
		self._btnClientEntityCollect:SetCoolDown(cd)
		EventDispatcher:TriggerEvent(GameEvents.PlayerCommandCollectClientResource, ccid.entityCfgId,cd)
		CollectManager:CollectClientStartAction()
	end
end

--中断客户端采集
function CollectPanel:StopCollectClientEntity()
	self._btnClientEntityCollect:Interrupt()
end

---------------------传送处理-----------------------------
--开始客户端传送
-- function CollectPanel:StartTeleport()
-- 	local cd = tonumber(GlobalParamsHelper.GetParamValue(32))*1000
-- 	local teleportRegionId = MapManager:GetTeleportRegionId()
-- 	-- if ccid then
-- 	-- 	--LoggerHelper.Error("StartCollectClientResourc1"..ccid.cdTime*1000)
-- 	if teleportRegionId then
-- 		self._btnTeleport:SetCoolDown(cd)
-- 		EventDispatcher:TriggerEvent(GameEvents.PlayerCommandTeleport, teleportRegionId)
-- 	end
-- end

-- function CollectPanel:OnTeleportShow()
-- 	local cd = tonumber(GlobalParamsHelper.GetParamValue(32))*1000
-- 	self._btnTeleport:SetCoolDown(cd)
-- end

-- --中断客户端传送
-- function CollectPanel:StopTeleport()
-- 	self._btnTeleport:Interrupt()
-- end

