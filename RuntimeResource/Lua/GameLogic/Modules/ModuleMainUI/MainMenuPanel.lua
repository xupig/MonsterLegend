-- MainMenuPanel.lua
local ClassTypes = ClassTypes
local MainMenuPanel = Class.MainMenuPanel(ClassTypes.BasePanel)
-- require "Modules.ModuleMainUI.ChildView.TaskView"
require "Modules.ModuleMainUI.ChildComponent.MenuItem"
local public_config = GameWorld.public_config

-- local TaskView = ClassTypes.TaskView
local PanelsConfig = GameConfig.PanelsConfig
-- local PanelBGType = PanelsConfig.PanelBGType
-- local ButtonWrapper = UIExtension.ButtonWrapper
-- local UIToggle = ClassTypes.UIToggle
-- local UIList = ClassTypes.UIList
local MenuItem = ClassTypes.MenuItem
local GUIManager = GameManager.GUIManager
local SceneConfig = GameConfig.SceneConfig
local MenuConfig = GameConfig.MenuConfig
-- local TimerHeap = GameWorld.TimerHeap
local GameSceneManager = GameManager.GameSceneManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local RedPointManager = GameManager.RedPointManager
local FunctionTimingDataHelper = GameDataHelper.FunctionTimingDataHelper
local StrengthenManager = PlayerManager.StrengthenManager
local SevenDayLoginManager = PlayerManager.SevenDayLoginManager
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local FunctionOpenManager = PlayerManager.FunctionOpenManager

function MainMenuPanel:Awake()
    self._hideStickFly = false
    self._csCallControlUIState = true
    self._containerRowGO = self:FindChildGO("Container_Row")
    self._containerRow = self._containerRowGO.transform

    self._containerRowBigGO = self:FindChildGO("Container_RowBig")
    self._containerRowBig = self._containerRowBigGO.transform
    
    self._containerRow1GO = self:FindChildGO("Container_Row1")
    self._containerRow2GO = self:FindChildGO("Container_Row2")
    self._containerRow3GO = self:FindChildGO("Container_Row3")

    self._containerRow1 = self._containerRow1GO.transform
    self._containerRow2 = self._containerRow2GO.transform
    self._containerRow3 = self._containerRow3GO.transform


    self._containerRow1StableGO = self:FindChildGO("Container_Row1/Container_Stable")
    self._containerRow1Stable = self._containerRow1StableGO.transform
    self._containerRow2StableGO = self:FindChildGO("Container_Row2/Container_Stable")
    self._containerRow2Stable = self._containerRow2StableGO.transform
    self._containerRow3StableGO = self:FindChildGO("Container_Row3/Container_Stable")
    self._containerRow3Stable = self._containerRow3StableGO.transform

    self._containerRow1MoveGO = self:FindChildGO("Container_Row1/Container_Move")
    self._containerRow1Move = self._containerRow1MoveGO.transform
    self._containerRow2MoveGO = self:FindChildGO("Container_Row2/Container_Move")
    self._containerRow2Move = self._containerRow2MoveGO.transform
    self._containerRow3MoveGO = self:FindChildGO("Container_Row3/Container_Move")
    self._containerRow3Move = self._containerRow3MoveGO.transform

    self._containerStableRows = {}
    self._containerStableRows[1] = self._containerRow1Stable
    self._containerStableRows[2] = self._containerRow2Stable
    self._containerStableRows[3] = self._containerRow3Stable

    self._stableCounts = {}

    self._containerMoveRows = {}
    self._containerMoveRows[1] = self._containerRow1Move
    self._containerMoveRows[2] = self._containerRow2Move
    self._containerMoveRows[3] = self._containerRow3Move

    self._containerFxs = {}
    for i=1,3 do
        self._containerFxs[i] = self:FindChildGO("Container_Fx/Container_Fx"..i).transform
    end
    self._needFxPanelIds = {}
    self._needFxPanelIds[5] = 1 --开服活动
    self._needFxPanelIds[3] = 2 --首充
    self._needFxPanelIds[124] = 2 --每日首充
    self._needFxPanelIds[9] = 3 --寻宝

    self._fxPath = {}
    self._fxPath[1] = "Container_Fx1/fx_ui_menu2_activity"
    self._fxPath[2] = "Container_Fx2/fx_ui_menu2_diamond"
    self._fxPath[3] = "Container_Fx3/fx_ui_menu2_treasure"


    GUIManager.PreloadPanel(PanelsConfig.WaitingLoadScene)
    --self._menuItemPool = {}
    self._lightPool = {}
    self._menuItems = {}
    --self._dynamicItems = {}
    
    self._btnExpand = self:FindChildGO("Button_Expand")
    GameWorld.AddHitArea(self._btnExpand,60,60,24,7)
    self._csBH:AddClick(self._btnExpand, function()self:OnClickChangeExpand() end)
    self._imgExpandRedPoint = self:FindChildGO("Button_Expand/Image_RedPoint")
    
    self._btnRetract = self:FindChildGO("Button_Retract")
    GameWorld.AddHitArea(self._btnRetract,60,60,24,7)
    self._csBH:AddClick(self._btnRetract, function()self:OnClickChangeExpand() end)
    
    -- self._onClientTeleportBegin = function()self:ClientTeleportBegin() end
    -- self._onClientTeleportEnd = function()self:ClientTeleportEnd() end
    -- self._onTriggerSceneWaitingArea = function(sceneName)self:TriggerSceneWaitingArea(sceneName) end
    -- self._onRefreshStick = function(state)self:OnRefreshStick(state) end
    self._onRefreshMainMenu = function()self:ShowMainMenu() end
    self._onExpandByOther = function(state)self:OnExpandByOther(state) end
    self._onRefreshDynamicMenu = function (id) self:RefreshDynamicMenu(id) end
    -- self._onAddMenuIcon = function(cfgId, clickFunc)self:OnAddMenuIcon(cfgId, clickFunc) end
    -- self._onRemoveMenuIcon = function(cfgId)self:OnRemoveMenuIcon(cfgId) end
-- self._onCsCallControlUI = function(state)self:OnCsCallControlUI(state) end
    self._hideWelfare = GameLoader.SystemConfig.GetValueInCfg('HideWelfare')
    --MJ包主菜单位置修改
    if GameWorld.SpecialExamine then
        self._rowInitX = -697
        self._containerRowBig.anchoredPosition = Vector3.New(1052,568)
        self._btnExpand.transform.anchoredPosition = Vector3.New(1256,702)
        self._btnRetract.transform.anchoredPosition = Vector3.New(1256,702)
    else
        self._rowInitX = -837
    end
end

function MainMenuPanel:OnDestroy()
    --self._menuItemPool = nil
    self._lightPool = nil
    self._menuItems = nil
    --self._dynamicItems = nil
end

function MainMenuPanel:OnShow(isExpand)
    
    -- LoggerHelper.Log("Ash: MainMenuPanel OnShow:" .. 123, true)
    -- self:UpdateSkillPanelActive()
    -- self:UpdateStickPanelActive()
    self:AddEventListeners()
    self:ShowMainMenu()
    -- local sceneType = GameManager.GameSceneManager:GetCurSceneType()
    -- if sceneType ~= SceneConfig.SCENE_TYPE_AION_TOWER then
    --     self:ShowMainMenu()
    -- else
    --     self:RemoveAllMenu()
    -- end
    
    self:UpdateStrengthenPanelActive()
    --GUIManager.ShowPanel(PanelsConfig.Strengthen)
    SevenDayLoginManager:CheckStatus()
    ServiceActivityManager:CheckStatus()
    self:SetData(isExpand)
    EventDispatcher:TriggerEvent(GameEvents.MAIN_MENU_ICON_UPDATE)
end

function MainMenuPanel:SetData(isExpand)
    --一般情况
    if isExpand == true or isExpand == nil then
        self._inSpecialMode = false
        self:SetExpandState(true)
    --特殊情况收缩方式改变
    else
        self._inSpecialMode = true
        self:SetExpandState(false)
    end
end

function MainMenuPanel:OnClose()
    -- self:UpdateSkillPanelActive()
    -- self:UpdateStickPanelActive()
    self:RemoveEventListeners()
end

function MainMenuPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ON_REFRESH_MAIN_MENU, self._onRefreshMainMenu)
    EventDispatcher:AddEventListener(GameEvents.ON_SET_EXPAND_STATE, self._onExpandByOther)

    EventDispatcher:AddEventListener(GameEvents.ON_REFRESH_MAIN_MENU_DYNAMIC_ICON,self._onRefreshDynamicMenu)
    -- EventDispatcher:AddEventListener(GameEvents.ON_CSCALL_CONTROL_UI, self._onCsCallControlUI)
end

function MainMenuPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_REFRESH_MAIN_MENU, self._onRefreshMainMenu)
    EventDispatcher:RemoveEventListener(GameEvents.ON_SET_EXPAND_STATE, self._onExpandByOther)

    EventDispatcher:RemoveEventListener(GameEvents.ON_REFRESH_MAIN_MENU_DYNAMIC_ICON,self._onRefreshDynamicMenu)
    -- EventDispatcher:RemoveEventListener(GameEvents.ON_CSCALL_CONTROL_UI, self._onCsCallControlUI)
end

-- function MainMenuPanel:OnCsCallControlUI(state)
--     self._csCallControlUIState = state
--     self:UpdateSkillPanelActive()
-- end

--

function MainMenuPanel:OnExpandByOther(state)
    self._inSpecialMode = true
    self:SetExpandState(state)
end

function MainMenuPanel:OnClickChangeExpand()
    local b = not self._isExpand
    self:SetExpandState(b)
end

function MainMenuPanel:SetExpandState(isExpand)
    self._isExpand = isExpand
    self._btnRetract:SetActive(isExpand)
    self._btnExpand:SetActive(not isExpand)
    local bossStateShow = GUIManager.PanelIsActive(PanelsConfig.BossState)
    --特殊的时候
    if self._inSpecialMode then
        self._containerRow1GO:SetActive(isExpand)
        self._containerRow2GO:SetActive(isExpand)
        self._containerRow3GO:SetActive(isExpand)
        self._showRows = false
        self._containerRowBigGO:SetActive(isExpand)
        if isExpand then
            self._containerRow1MoveGO:SetActive(true)
            self._containerRow2MoveGO:SetActive(true)
            self._containerRow3MoveGO:SetActive(true)
            GUIManager.ClosePanel(PanelsConfig.InstQuit)
        else
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        end
        
        local b = not isExpand
        EventDispatcher:TriggerEvent(GameEvents.SHOW_BUTTON_BY_MAIN_MENU,b)
        if bossStateShow then
            EventDispatcher:TriggerEvent(GameEvents.Set_Boss_State_Panel_Show,b)
        end
    --一般情况
    else
        if self._showRows == false then
            self._containerRow1GO:SetActive(true)
            self._containerRow2GO:SetActive(true)
            self._containerRow3GO:SetActive(true)
            self._containerRowBigGO:SetActive(true)
            self._showRows = true
        end
        self._containerRow1MoveGO:SetActive(isExpand)
        self._containerRow2MoveGO:SetActive(isExpand)
        self._containerRow3MoveGO:SetActive(isExpand)
    end

    if not isExpand then
        local redPointState = false
        for k,v in pairs(self._menuItems) do
            if v:GetRedPointState() then
                redPointState = true
                break
            end
        end
        self._imgExpandRedPoint:SetActive(redPointState)
    end
end

--动态设置菜单的icon/名字
function MainMenuPanel:RefreshDynamicMenu(id)
    if self._menuItems[id] then
        self._menuItems[id]:SetIcon(id)
        self._menuItems[id]:SetName(id)
    end
end

function MainMenuPanel:ShowMainMenu()
    
    self:RemoveAllMenu()
    local fo = FunctionOpenDataHelper:GetAllMainMenuCfg()
    local dynamicFuncMap = FunctionOpenManager:GetDynamicMenu()
    local stableCounts = {}
    local moveCounts = {}
    local bigMenuCount = 0
    for i, cfg in ipairs(fo) do
        local shouldShow = false
        local id = cfg.id
        local pos = cfg.position
        local isDynamic = false
        if cfg.button_type == MenuConfig.MAIN_MENU then
            if FunctionOpenDataHelper:CheckPanelOpen(id) and pos then
                shouldShow = true
            end
        elseif cfg.button_type == MenuConfig.DYNAMIC_MAIN_MENU then
            if dynamicFuncMap[id] then
                isDynamic = true
                shouldShow = true
            end
        end
        --特殊屏蔽福利入口
        if id == 10 and self._hideWelfare == "1" then
            shouldShow = false
        end

        if shouldShow then
            local parent = nil
            local isBig = false
            
            local line = math.floor(pos[1] / 100)
            local isStable = pos[2]
            --普通按钮
            if line > 0  then
                if isStable == 1 then
                    parent = self._containerStableRows[line]
                    if stableCounts[line] then
                        stableCounts[line] = stableCounts[line] + 1
                    else
                        stableCounts[line] = 1
                    end
                else
                    if moveCounts[line] then
                        moveCounts[line] = moveCounts[line] + 1
                    else
                        moveCounts[line] = 1
                    end
                    parent = self._containerMoveRows[line]
                end
            --大按钮
            else
                bigMenuCount = bigMenuCount + 1
                isBig = true
                parent = self._containerRowBig
            end

            if self._menuItems[id] == nil then
                self:AddMenu(id, cfg, isDynamic,isBig)
            end
            local item = self._menuItems[id]
            local trans = item.transform
            trans:SetParent(parent)
            item.gameObject:SetActive(true)
            item:SetLightAnimateActive(true)
            if isBig then
                trans.anchoredPosition = Vector3(-40-(bigMenuCount-1)*90,-72,0)
            else
                local posIndex
                if isStable == 1 then
                    posIndex = stableCounts[line]
                else
                    posIndex = moveCounts[line]
                end
                trans.anchoredPosition = Vector3(-32-(posIndex-1)*74,-32,0)
            end
            --动态图标
            if dynamicFuncMap[id] then
                local endTime = FunctionOpenManager:GetDynamicMenuEndTime(id)
                if endTime then
                    local timeLeft = endTime - DateTimeUtil.GetServerTime()
                    local getReady = FunctionOpenManager:GetDynamicMenuGetReady(id)
                    item:UpdateTimeLeft(timeLeft,getReady)
                end
            end
        end
    end

    self._containerRow1.localPosition = Vector3.New(self._rowInitX - 90*bigMenuCount,-70,0)
    self._containerRow2.localPosition = Vector3.New(self._rowInitX - 90*bigMenuCount,-152,0)

    for i=1,3 do
        local a = stableCounts[i] or 0
        self._containerMoveRows[i].localPosition = Vector3.New(- 74*a,0,0)
    end
    --LoggerHelper.Error("MainMenuPanel:ShowMainMenu()eeeeeeeeeeeeeeeeee",true)
end

function MainMenuPanel:UpdateStrengthenPanelActive()
    if table.isEmpty(StrengthenManager:GetStrengthenData()) then
        GUIManager.ClosePanel(PanelsConfig.Strengthen)
    else
        GUIManager.ShowPanel(PanelsConfig.Strengthen)
    end
end

-- 创建菜单按钮
function MainMenuPanel:CreateMenuItem()
    -- local itemPool = self._menuItemPool
    -- if #itemPool > 0 then
    --     return table.remove(itemPool)
    -- end
    local go = self:CopyUIGameObject("Button_Func", "Container_Row")
    local item = UIComponentUtil.AddLuaUIComponent(go, MenuItem)
    item:SetActive(true)
    return item
end

-- 创建菜单按钮
function MainMenuPanel:CreateMenuBigItem()
    local go = self:CopyUIGameObject("Button_Big", "Container_Row")
    local item = UIComponentUtil.AddLuaUIComponent(go, MenuItem)
    item:SetActive(true)
    return item
end

-- function MainMenuPanel:ReleaseMenuItem(item)
--     local itemPool = self._menuItemPool
--     table.insert(itemPool, item)
-- end

-- 菜单光效对象池
function MainMenuPanel:CreateLightItem()
    -- local ligthPool = self._lightPool
    -- if #ligthPool > 0 then
    --     return table.remove(ligthPool)
    -- end
    local go = self:CopyUIGameObject("Container_Light", "Container_Row")
    go:SetActive(true)
    return go
--local item = UIComponentUtil.AddLuaUIComponent(go, MenuItem)
--return item
end

function MainMenuPanel:ReleaseLightItem(item)
    local ligthPool = self._lightPool
    table.insert(ligthPool, item)
end


function MainMenuPanel:AddMenu(id, cfg, isDynamic, isBig)
    if id==125 then return end
    local panelNames
    local cfgBtnFunc = cfg.button_func
    -- LoggerHelper.Log("Ash: ShowMainMenu: " .. PrintTable:TableToStr(btnFunc))
    if cfgBtnFunc ~= nil and cfgBtnFunc[MenuConfig.SHOW_PANEL] ~= nil then
        panelNames = cfgBtnFunc[MenuConfig.SHOW_PANEL]
    end

    local item
    if isBig then
        item = self:CreateMenuBigItem()
    else
        item = self:CreateMenuItem()
    end
    
    if panelNames and panelNames[1] then
        local node = RedPointManager:GetRedPointDataByPanelName(panelNames[1])
        if node then
            item:SetRedPointData(node)
        end
    end
    self._menuItems[id] = item
    item:SetData(id,cfg,panelNames,isDynamic)
    --加光效
    if cfg.circle > 0 then
        local lightItem = self:CreateLightItem()
        item:SetLightAnimate(lightItem)
        item:SetOnceClickLight(cfg.circle == 2)
    end

    local needFxIndex = self._needFxPanelIds[id]
    if needFxIndex then
        local fxItem = self._containerFxs[needFxIndex]
        item:SetIconFx(fxItem,self._fxPath[needFxIndex])
    end
    return item
end

function MainMenuPanel:RemoveAllMenu()
    for k, item in pairs(self._menuItems) do
        item.gameObject:SetActive(false)
        item.transform:SetParent(self._containerRow)
        item:SetLightAnimateActive(false)
        -- local lightItem = item:GetLightAnimate()
        -- if lightItem then
        --     lightItem.transform:SetParent(self._containerRow)
        --     self:ReleaseLightItem(lightItem)
        -- end
        -- item:RemoveRedPointListener()
        --self:ReleaseMenuItem(item)
    end
    
    --self._menuItems = {}
end