--SubMainMenuPanel.lua
--打开二级菜单的菜单按钮
local SubMainMenuPanel = Class.SubMainMenuPanel(ClassTypes.BasePanel)
local GUIManager = GameManager.GUIManager
local RedPointManager = GameManager.RedPointManager

function SubMainMenuPanel:Awake()
    self.disableCameraCulling = true
    self:InitCallback()
    self:InitViews()
end

function SubMainMenuPanel:InitCallback()
    self._updateRedpointFunc = function(flag)self:SetRedPoint(flag) end 
end

function SubMainMenuPanel:InitViews()
    self._menuButton = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self._menuButton, function() self:OnMenuButtonClick() end)
    self._imgRedPoint = self:FindChildGO("Button_BG/Image_RedPoint")
end

function SubMainMenuPanel:OnMenuButtonClick()
    GUIManager.ShowPanel(PanelsConfig.SubMainUI)
end

function SubMainMenuPanel:OnShow(data)
    RedPointManager:RegisterHeadUpdateFunc2(self._updateRedpointFunc)
end

function SubMainMenuPanel:OnClose()
    RedPointManager:RegisterHeadUpdateFunc2(nil)
end

function SubMainMenuPanel:SetRedPoint(isActive)
    self._imgRedPoint:SetActive(isActive)
end
