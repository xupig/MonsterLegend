--QualityChangePanel.lua
local QualityChangePanel = Class.QualityChangePanel(ClassTypes.BasePanel)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local SystemSettingManager = PlayerManager.SystemSettingManager
local SystemSettingConfig = GameConfig.SystemSettingConfig
local WaitTimeManager = PlayerManager.WaitTimeManager

local seconds = 10

function QualityChangePanel:Awake()
    self.disableCameraCulling = true
    self:InitCallback()
    self:InitViews()
end

function QualityChangePanel:InitCallback()
end

function QualityChangePanel:InitViews()
    self._confirmText = self:GetChildComponent("Container_QualityChange/Button_Confirm/Text", "TextMeshWrapper")

    self._confirmButton = self:FindChildGO("Container_QualityChange/Button_Confirm")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Container_QualityChange/Button_Cancel")
    self._csBH:AddClick(self._cancelButton,function() self:OnCancelButtonClick() end)
end

function QualityChangePanel:OnShow(data)
    self._seconds = seconds
    self:AddEventListeners()
    self:AddTimer()
end

function QualityChangePanel:SetData(data)
end

function QualityChangePanel:OnClose()
    self:ClearTimer()
    self:RemoveEventListeners()
end

function QualityChangePanel:AddEventListeners()
end

function QualityChangePanel:RemoveEventListeners()
end

function QualityChangePanel:AddTimer()
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()
        if self._seconds < 0 then
            self:ClearTimer()
            self:OnConfirmButtonClick()
        end
        self._confirmText.text = LanguageDataHelper.CreateContent(13) .. "(" .. self._seconds .. ")"
        self._seconds = self._seconds - 1
    end)
end

function QualityChangePanel:ClearTimer()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function QualityChangePanel:OnConfirmButtonClick()
    local nowQuality = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.RENDER_QUALITY)
    local quality = SystemSettingConfig.QUALITY_LOW
    if nowQuality == SystemSettingConfig.QUALITY_HIGH then
        quality = SystemSettingConfig.QUALITY_MIDDLE
    end

    SystemSettingManager:SetPlayerSetting(SystemSettingConfig.RENDER_QUALITY, quality)
    EventDispatcher:TriggerEvent(GameEvents.OnQualityChanged)
    GameWorld.SetLowFps(false)

    GUIManager.ClosePanel(PanelsConfig.QualityChange)
end

function QualityChangePanel:OnCancelButtonClick()
    WaitTimeManager:ClaerFpsTimer()
    GUIManager.ClosePanel(PanelsConfig.QualityChange)
end