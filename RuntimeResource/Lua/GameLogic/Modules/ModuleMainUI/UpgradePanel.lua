--UpgradePanel.lua
local UpgradePanel = Class.UpgradePanel(ClassTypes.BasePanel)
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local UpgradeManager = PlayerManager.UpgradeManager
local UIList = ClassTypes.UIList
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper


require "Modules.ModuleMainUI.ChildComponent.UpgradeListItem"
local UpgradeListItem = ClassTypes.UpgradeListItem

function UpgradePanel:Awake()
    self.disableCameraCulling = true
    self:InitViews()
end



function UpgradePanel:InitViews()
    self._upgradeListGo = self:FindChildGO("Container_List")
    self._shadeButton = self:FindChildGO("Container_List/Button_Shade")
    self._csBH:AddClick(self._shadeButton, function() self:OnCloseList() end)
    self._closeButton = self:FindChildGO("Container_List/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnCloseList() end)
    --self._upgradeListGo:SetActive(false)
    self:InitUpgradeList()
end

function UpgradePanel:InitUpgradeList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_List/ScrollViewList")
	self._upgradeList = list
	self._upgradeListlView = scrollView 
    self._upgradeListlView:SetHorizontalMove(false)
    self._upgradeListlView:SetVerticalMove(true)
    self._upgradeList:SetItemType(UpgradeListItem)
    self._upgradeList:SetPadding(0, 0, 0, 8)
    self._upgradeList:SetGap(8, 0)
    self._upgradeList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function UpgradePanel:OnShow(data)
    local followNew = UpgradeManager:GetUpgradeData()

    if #followNew > 0 then
        --self._upgradeListGo:SetActive(true)
    end

    if #followNew > 3 then        
        self._upgradeListlView:SetVerticalMove(true)
    else
        self._upgradeListlView:SetVerticalMove(false)
    end
    --LoggerHelper.Error("GlobalParamsHelper.GetParamValue(870):"..PrintTable:TableToStr(followData))
    self._upgradeList:SetDataList(followNew)
   
end

function UpgradePanel:SetData(data)
end

function UpgradePanel:OnClose()
    
end


function UpgradePanel:OnCloseList()
    GameManager.GUIManager.ClosePanel(PanelsConfig.Upgrade)
end