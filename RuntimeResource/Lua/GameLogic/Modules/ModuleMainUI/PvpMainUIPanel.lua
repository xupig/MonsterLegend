-- PvpMainUIPanel.lua
local PvpMainUIPanel = Class.PvpMainUIPanel(ClassTypes.BasePanel)
require "Modules.ModuleMainUI.ChildView.PvpHeadView"
local PvpHeadView = ClassTypes.PvpHeadView

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local DateTimeUtil = GameUtil.DateTimeUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local EntityType = GameConfig.EnumType.EntityType
local PlayerActionManager = GameManager.PlayerActionManager

function PvpMainUIPanel:Awake()
    self.disableCameraCulling = true
    self._selfHeadView = self:AddChildLuaUIComponent("Container_Self", PvpHeadView)
    self._enemyHeadView = self:AddChildLuaUIComponent("Container_Enemy", PvpHeadView)
    self._instanceTimeContainer = self:FindChildGO("Container_InstanceTime")
    self._timeText = self:GetChildComponent("Container_InstanceTime/Text_Time",'TextMeshWrapper')    
    self._instanceTimeContainer:SetActive(false)

    self._containerWaiting = self:FindChildGO("Container_Waiting")
    self._textLoading = self:FindChildGO("Container_Waiting/Text_Loading")
    self._imageFight1 = self:FindChildGO("Container_Waiting/Image_Fight_1")
    self._imageFight2 = self:FindChildGO("Container_Waiting/Image_Fight_2")
    self._imageFight3 = self:FindChildGO("Container_Waiting/Image_Fight_3")
    self._imageFightText = self:FindChildGO("Container_Waiting/Image_Fight_Text")
    self._textLoading:SetActive(false)
    self._imageFight1:SetActive(false)
    self._imageFight2:SetActive(false)
    self._imageFight3:SetActive(false)
    self._imageFightText:SetActive(false)
    self._containerWaiting:SetActive(false)
    self._onAvatarEnterWorld = function(entityId)self:OnAvatarEnterWorld(entityId) end
    self._onAvatarLeaveWorld = function(entityId)self:OnAvatarLeaveWorld(entityId) end
    self._onUpdateFightForce = function()self:OnUpdateFightForce() end
    self._onEnterMap = function()self:OnEnterMap() end
    self._checkCounter = 0
end

function PvpMainUIPanel:OnEnterMap()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel OnEnterMap")
    self:CheckBeginCountDown()
end

function PvpMainUIPanel:OnUpdateFightForce()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel OnUpdateFightForce")
    self:CheckBeginCountDown()
end

function PvpMainUIPanel:CheckBeginCountDown()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel CheckBeginCountDown: " .. self._checkCounter)
    self._checkCounter = self._checkCounter + 1
    if self._checkCounter == 2 then
        self:BeginCountDown()
    end
end

function PvpMainUIPanel:ShowLoading()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel ShowLoading")
    self:StopCountDown()
    self._containerWaiting:SetActive(true)
    self._textLoading:SetActive(true)
    self._instanceTimeContainer:SetActive(false)
end

function PvpMainUIPanel:BeginCountDown()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel BeginCountDown")
    self._containerWaiting:SetActive(true)
    self._imageFight1:SetActive(false)
    self._imageFight2:SetActive(false)
    self._imageFight3:SetActive(false)
    self._imageFightText:SetActive(false)
    self._counter = 3
    self._timer = TimerHeap:AddSecTimer(2, 1, 0, function()self:OnCountDownTick() end)
end

function PvpMainUIPanel:OnCountDownTick()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel OnCountDownTick: " .. self._counter)
    if self._counter == 3 then
        self._textLoading:SetActive(false)
        self._imageFight1:SetActive(false)
        self._imageFight2:SetActive(false)
        self._imageFight3:SetActive(true)
        self._imageFightText:SetActive(false)
    elseif self._counter == 2 then
        self._imageFight1:SetActive(false)
        self._imageFight2:SetActive(true)
        self._imageFight3:SetActive(false)
        self._imageFightText:SetActive(false)
    elseif self._counter == 1 then
        self._imageFight1:SetActive(true)
        self._imageFight2:SetActive(false)
        self._imageFight3:SetActive(false)
        self._imageFightText:SetActive(false)
    elseif self._counter == 0 then
        self._imageFight1:SetActive(false)
        self._imageFight2:SetActive(false)
        self._imageFight3:SetActive(false)
        self._imageFightText:SetActive(true)
    else
        self:HideWaiting()
        self:BeginCountDownTimeLeft()
        GameWorld.Player():StartAutoFight()
        end
    
    self._counter = self._counter - 1
end

function PvpMainUIPanel:StopCountDown()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel StopCountDown")
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function PvpMainUIPanel:BeginCountDownTimeLeft()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel BeginCountDown")
    self._instanceTimeContainer:SetActive(true)
    self._reviveTime = DateTimeUtil:GetServerTime() + GlobalParamsHelper.GetParamValue(660)--巅峰竞技巅峰竞技实时1v1战斗总时长（单位：秒）
    self._timerTimeLeft = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnCountDownTimeLeftTick() end)
end

function PvpMainUIPanel:OnCountDownTimeLeftTick()
    if (self._reviveTime - DateTimeUtil:GetServerTime() >= 0) then
        -- self._reviveTime = self._reviveTime - 1
        self:UpdateTimeLeft()
    else
        self:StopCountDownTimeLeft()
    end
end

function PvpMainUIPanel:UpdateTimeLeft()
    local leftTime = self._reviveTime - DateTimeUtil:GetServerTime()
    self._timeText.text = DateTimeUtil:FormatFullTime(leftTime, false, true)
end

function PvpMainUIPanel:StopCountDownTimeLeft()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel StopCountDown")
    if self._timerTimeLeft then
        TimerHeap:DelTimer(self._timerTimeLeft)
        self._timerTimeLeft = nil
    end
end

function PvpMainUIPanel:HideWaiting()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel HideWaiting")
    self._containerWaiting:SetActive(false)
    self._textLoading:SetActive(false)
    self._imageFight1:SetActive(false)
    self._imageFight2:SetActive(false)
    self._imageFight3:SetActive(false)
    self._imageFightText:SetActive(false)
    self:StopCountDown()
    self:StopCountDownTimeLeft()
end

function PvpMainUIPanel:OnShow(data)
    -- LoggerHelper.Log("Ash: PvpMainUIPanel OnShow")
    EventDispatcher:AddEventListener(GameEvents.OnlinePvpUpdateFightForce, self._onUpdateFightForce)
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    self:ShowLoading()
    self._checkCounter = 0
    local player = GameWorld.Player()
    player:StopAutoFight()
    -- player:StopMoveWithoutCallback()
    PlayerActionManager:StopAllAction()
    self._selfHeadView:SetEntity(player)
    local avatars = GameWorld.GetEntitiesByType(EntityType.Avatar)
    if #avatars == 0 then
        -- LoggerHelper.Log("PvpMainUIPanel: #avatars == 0")
        self._enemyHeadView:Init()
        EventDispatcher:AddEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
        EventDispatcher:AddEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
        return
    end
    for k, v in ipairs(avatars) do
        if v ~= player then
            self._enemyHeadView:SetEntity(v)
        end
    -- LoggerHelper.Log("PvpMainUIPanel: ", v.name)
    end
end

function PvpMainUIPanel:OnClose()
    -- LoggerHelper.Log("Ash: PvpMainUIPanel OnClose")
    self._checkCounter = 0
    self._selfHeadView:RemoveEntity()
    self._enemyHeadView:RemoveEntity()
    EventDispatcher:RemoveEventListener(GameEvents.OnlinePvpUpdateFightForce, self._onUpdateFightForce)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
    self:HideWaiting()
end

function PvpMainUIPanel:OnAvatarEnterWorld(entityId)
    -- LoggerHelper.Log("Ash: PvpMainUIPanel OnAvatarEnterWorld:" .. tostring(entityId))
    local ety = GameWorld.GetEntity(entityId)
    self._enemyHeadView:SetEntity(ety)
end

function PvpMainUIPanel:OnAvatarLeaveWorld(entityId)
    -- LoggerHelper.Log("Ash: PvpMainUIPanel OnAvatarLeaveWorld:" .. tostring(entityId))
    self._enemyHeadView:RemoveEntity()
end

function PvpMainUIPanel:OnDestroy()
    self._selfHeadView = nil
    self._enemyHeadView = nil
end
