-- MarryMainUIPanel.lua
local MarryMainUIPanel = Class.MarryMainUIPanel(ClassTypes.BasePanel)
require "Modules.ModuleMainUI.ChildComponent.MarryFireworkItem"
local MarryFireworkItem = ClassTypes.MarryFireworkItem

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local DateTimeUtil = GameUtil.DateTimeUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local EntityType = GameConfig.EnumType.EntityType
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local StringStyleUtil = GameUtil.StringStyleUtil
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local MarriageManager = PlayerManager.MarriageManager
local InstanceCommonManager = PlayerManager.InstanceCommonManager

function MarryMainUIPanel:Awake()
    self._textState = self:GetChildComponent("Container_Left/Text_State", 'TextMeshWrapper')
    self._textFoodValue = self:GetChildComponent("Container_Left/Text_FoodValue", 'TextMeshWrapper')
    self._textExp = self:GetChildComponent("Container_Left/Text_Exp", 'TextMeshWrapper')
    self._textGetBlessing = self:GetChildComponent("Container_Left/Text_GetBlessing", 'TextMeshWrapper')
    self._textFireworks = self:GetChildComponent("Container_Left/Text_Fireworks", 'TextMeshWrapper')
    
    self._btnStartWedding = self:FindChildGO("Container_Center/Button_StartWedding")
    self._btnBlessing = self:FindChildGO("Container_Center/Button_Blessing")
    self._btnGiftShop = self:FindChildGO("Container_Center/Button_GiftShop")
    self._btnGuestManager = self:FindChildGO("Container_Center/Button_GuestManager")
    self._imageGuestManagerRedPoint = self:FindChildGO("Container_Center/Button_GuestManager/Image_RedPoint")
    self._csBH:AddClick(self._btnStartWedding, function()self:StartWeddingBtnClick() end)
    self._csBH:AddClick(self._btnBlessing, function()self:BlessingBtnClick() end)
    self._csBH:AddClick(self._btnGiftShop, function()self:GiftShopBtnClick() end)
    self._csBH:AddClick(self._btnGuestManager, function()self:GuestManagerBtnClick() end)
    self._btnStartWeddingWrapper = self:GetChildComponent("Container_Center/Button_StartWedding", "ButtonWrapper")
    self._btnGuestManagerWrapper = self:GetChildComponent("Container_Center/Button_GuestManager", "ButtonWrapper")
    self:InitProgress()
    
    self._gridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Right/Container_Items")
    self._gridLauoutGroup:SetItemType(MarryFireworkItem)
    
    -- self._guestManagerView = self:AddChildLuaUIComponent("Container_GuestManager", GuestManagerMainUIView)
    self._onMarryNotifyBasicInfo = function(args)self:OnMarryNotifyBasicInfo(args) end
    self._onMarryNotifyExp = function(args)self:OnMarryNotifyExp(args) end
    self._onMarryNotifyFood = function(args)self:OnMarryNotifyFood(args) end
    self._onMarryNotifyGift = function(args)self:OnMarryNotifyGift(args) end
    self._onMarriageQueryInvite = function()self:OnMarriageQueryInvite() end
    self._onGuestManagerUpdate = function(flag)self:OnGuestManagerUpdate(flag) end
end

function MarryMainUIPanel:OnShow(data)
    -- LoggerHelper.Log("Ash: MarryMainUIPanel OnShow")
    -- self._guestManagerView.gameObject:SetActive(false)
    -- self._pbHeat:SetProgressByValue(curAngry , maxAngry)
    EventDispatcher:AddEventListener(GameEvents.MarryNotifyBasicInfo, self._onMarryNotifyBasicInfo)
    EventDispatcher:AddEventListener(GameEvents.MarryNotifyExp, self._onMarryNotifyExp)
    EventDispatcher:AddEventListener(GameEvents.MarryNotifyFood, self._onMarryNotifyFood)
    EventDispatcher:AddEventListener(GameEvents.MarryNotifyGift, self._onMarryNotifyGift)
    EventDispatcher:AddEventListener(GameEvents.MarriageQueryInvite, self._onMarriageQueryInvite)
    local hitProps = GlobalParamsHelper.GetParamValue(785) or {}-- 婚礼用于加热度的道具ID（烟花）
    self._gridLauoutGroup:SetDataList(hitProps)
    self:OnMarriageQueryInvite()
    local now = DateTimeUtil.Now()
    local targetTime = DateTimeUtil.NewTime(now.year, now.month, now.day, now.hour, 0, 0)
    InstanceCommonManager:StartInstanceCountDown(public_config.MARRY_STAGE_4_WAIT_TIME, targetTime)
    MarriageManager:RegistGuestManagerUpdateFunc(self._onGuestManagerUpdate)

    local text = LanguageDataHelper.GetContent(81690)
    GUIManager.ShowNewsTicke(text,1,function()end,8)


end

function MarryMainUIPanel:OnClose()
    -- LoggerHelper.Log("Ash: MarryMainUIPanel OnClose")
    EventDispatcher:RemoveEventListener(GameEvents.MarryNotifyBasicInfo, self._onMarryNotifyBasicInfo)
    EventDispatcher:RemoveEventListener(GameEvents.MarryNotifyExp, self._onMarryNotifyExp)
    EventDispatcher:RemoveEventListener(GameEvents.MarryNotifyFood, self._onMarryNotifyFood)
    EventDispatcher:RemoveEventListener(GameEvents.MarryNotifyGift, self._onMarryNotifyGift)
    EventDispatcher:RemoveEventListener(GameEvents.MarriageQueryInvite, self._onMarriageQueryInvite)
    MarriageManager:RegistGuestManagerUpdateFunc(nil)
    InstanceCommonManager:StopInstanceCountDown()
end

function MarryMainUIPanel:OnMarriageQueryInvite()
    local name1, name2, vocation1, vocation2, dbid1, dbid2 = MarriageManager:GetCurWeddingHourData()
    local selfDbid = GameWorld.Player().dbid
    -- LoggerHelper.Log("Ash: MarryMainUIPanel OnShow: " .. selfDbid .. " " .. tostring(dbid1) .. " " .. tostring(dbid2))
    if selfDbid == dbid1 or selfDbid == dbid2 then
        self._btnStartWeddingWrapper.interactable = true
        self._btnGuestManagerWrapper.interactable = true
    else
        self._btnStartWeddingWrapper.interactable = false
        self._btnGuestManagerWrapper.interactable = false
    end
end

function MarryMainUIPanel:OnMarryNotifyBasicInfo(args)
    self._notStarted = (args[1] or 0) == 0
    self._textState.text = LanguageDataHelper.CreateContent(self._notStarted and 59081 or 59082)
    
    local hitInfo = GlobalParamsHelper.GetParamValue(772) or {}-- 婚礼-达到多少热度刷礼物
    local topHitValue = hitInfo[#hitInfo] or 1
    local firstHitValue = hitInfo[1] or 1
    local curHitValue = args[2] or 0
    local percent = curHitValue >= topHitValue and 1 or curHitValue / topHitValue
    self._pbHeat:SetProgress(percent)
    self._pbHeat:SetProgressText(tostring(curHitValue) .. "/" .. tostring(topHitValue))
    self._textGetBlessing.text = LanguageDataHelper.CreateContent(curHitValue < firstHitValue and 59078 or 59079)
end

function MarryMainUIPanel:OnMarryNotifyExp(args)
    self._textExp.text = StringStyleUtil.GetLongNumberString(args[1])
end

function MarryMainUIPanel:OnMarryNotifyFood(args)
    local topFoodCount = GlobalParamsHelper.GetParamValue(768)-- 婚礼-品尝美食最大次数
    self._textFoodValue.text = args[1] .. "/" .. topFoodCount
end

function MarryMainUIPanel:OnMarryNotifyGift(args)
end

function MarryMainUIPanel:OnGuestManagerUpdate(flag)
    self._imageGuestManagerRedPoint:SetActive(flag)
end

function MarryMainUIPanel:OnDestroy()
end

function MarryMainUIPanel:InitProgress()
    self._pbHeat = UIScaleProgressBar.AddProgressBar(self.gameObject, "Container_Center/ProgressBar_Heat")
    self._pbHeat:SetProgress(0)
    self._pbHeat:ShowTweenAnimate(false)
    self._pbHeat:SetMutipleTween(false)
    self._pbHeat:SetIsResetToZero(false)
end

function MarryMainUIPanel:StartWeddingBtnClick()
    if self._notStarted then
        local content = LanguageDataHelper.CreateContent(58702)
        GUIManager.ShowMessageBox(2, content,
            function()MarriageManager:MarryCeremony() end)
    else
        local content = LanguageDataHelper.CreateContent(59080)
        GUIManager.ShowText(2, content)
    end
end

function MarryMainUIPanel:BlessingBtnClick()
    -- GUIManager.ShowText(2, "内测期间不开放")
    GUIManager.ShowPanel(PanelsConfig.GetMarry, {"ShowSendBlessings"})
end

function MarryMainUIPanel:GiftShopBtnClick()
    local data = {["id"] = EnumType.ShopPageType.Marriage}
    GUIManager.ShowPanel(PanelsConfig.Shop, data)
end

function MarryMainUIPanel:GuestManagerBtnClick()
    GUIManager.ShowPanel(PanelsConfig.MarriageGuestManager)
    -- self._guestManagerView.gameObject:SetActive(true)
    -- self._guestManagerView:ShowView()
end
