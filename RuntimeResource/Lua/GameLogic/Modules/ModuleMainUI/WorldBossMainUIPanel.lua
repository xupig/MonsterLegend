-- WorldBossMainUIPanel.lua
local WorldBossMainUIPanel = Class.WorldBossMainUIPanel(ClassTypes.BasePanel)
local WorldBossManager = PlayerManager.WorldBossManager
local BossHomeManager = PlayerManager.BossHomeManager
local BackWoodsManager = PlayerManager.BackWoodsManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local MessageBoxType = EnumType.MessageBoxType

function WorldBossMainUIPanel:Awake()
    self.disableCameraCulling = true
    self._textName = self:GetChildComponent("Container_Right/Text_Name", "TextMeshWrapper")
    self._btnGo = self:FindChildGO("Container_Right/Button_Go")
    self._csBH:AddClick(self._btnGo, function()self:GoBtnClick() end)
    self._btnClose = self:FindChildGO("Container_Right/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    self._containerIcon = self:FindChildGO("Container_Right/Container_Icon")
end
function WorldBossMainUIPanel:OnShow(data)
    self:SetData(data)
end

function WorldBossMainUIPanel:GoBtnClick()
    if self._bossInfo then
        if self._bossInfo:GetType() == 1 then           
            WorldBossManager:GotoBoss(self._bossInfo)
        elseif self._bossInfo:GetType() == 2 then
            local levelLimit = self._bossInfo:GetLevelLimit()
            local vipLevelLimit = self._bossInfo:GetVIPLevelLimit()
            local minVipLevel = GlobalParamsHelper.GetParamValue(494)
            if GameWorld.Player().vip_level < vipLevelLimit then
                if GameWorld.Player().vip_level < minVipLevel then
                    GUIManager.ShowText(1, LanguageDataHelper.CreateContent(56039))
                else
                    GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(56038, {["0"] = self._bossInfo:GetVIPLevelMoney()}), 
                    function() BossHomeManager:GotoBoss(self._bossInfo) end, nil)
                end
            elseif levelLimit > GameWorld.Player().level then
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(56016) .. levelLimit .. LanguageDataHelper.CreateContent(56017) .. self._bossInfo:GetSenceName(), 1)
            else
                BossHomeManager:GotoBoss(self._bossInfo)
            end
            --BossHomeManager:GotoBoss(self._bossInfo)
        elseif self._bossInfo:GetType() == 3 then
            --BackWoodsManager:GotoBoss(self._bossInfo)
            local data = {}
            data.id = MessageBoxType.BackWoodsTicket
            data.value = {self._bossInfo}
            GameManager.GUIManager.ShowPanel(PanelsConfig.MessageBox, data)
        end
    end
end

function WorldBossMainUIPanel:CloseBtnClick()
    --LoggerHelper.Log("Ash: CloseBtnClick ")
    GUIManager.ClosePanel(PanelsConfig.WorldBossMainUI)
end

function WorldBossMainUIPanel:SetData(data)
    self._bossInfo = data
    self._textName.text = tostring(LanguageDataHelper.GetContent(data:GetBossName())) .. "Lv." .. tostring(data:GetBossLevel())
    GameWorld.AddIcon(self._containerIcon, data:GetBossHeadIcon())
    if self._countdownTimer ~= nil then
        GameWorld.TimerHeap:DelTimer(self._countdownTimer)
    end
    self._countdownTimer = GameWorld.TimerHeap:AddSecTimer(60, 0, 0, function()self:OnCountdownTick() end)
end

function WorldBossMainUIPanel:OnCountdownTick()
    --LoggerHelper.Log("Ash: OnCountdownTick ")
    GUIManager.ClosePanel(PanelsConfig.WorldBossMainUI)
end

function WorldBossMainUIPanel:OnClose()
    GameWorld.TimerHeap:DelTimer(self._countdownTimer)
    self._countdownTimer = nil
end

function WorldBossMainUIPanel:OnDestroy()
    self._textName = nil
    self._btnGo = nil
    self._btnClose = nil
    self._containerIcon = nil
end
