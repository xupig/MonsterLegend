-- PlayerStatePanel.lua
local ClassTypes = ClassTypes
local PlayerStatePanel = Class.PlayerStatePanel(ClassTypes.BasePanel)

local ComponentsConfig = GameConfig.ComponentsConfig
local RoleDataHelper = GameDataHelper.RoleDataHelper
local UIProgressBar = ClassTypes.UIProgressBar
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
require "Modules.ModuleMainUI.ChildComponent.BuffIcon"
local BuffIcon = ClassTypes.BuffIcon
local LuaUIRawImage = GameMain.LuaUIRawImage
local GUIManager = GameManager.GUIManager
local SkillDataHelper = GameDataHelper.SkillDataHelper
local XArtNumber = GameMain.XArtNumber
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local StringStyleUtil = GameUtil.StringStyleUtil
local BaseUtil = GameUtil.BaseUtil
local RedPointManager = GameManager.RedPointManager
local MainUIManager = PlayerManager.MainUIManager
local VIPManager = PlayerManager.VIPManager
local SystemSettingManager = PlayerManager.SystemSettingManager
local SystemSettingConfig = GameConfig.SystemSettingConfig
local GameWorld = GameWorld
local UIParticle = ClassTypes.UIParticle
local public_config = GameWorld.public_config
local FashionManager = PlayerManager.FashionManager
local FashionDataHelper = GameDataHelper.FashionDataHelper

require "UIComponent.Extend.PlayerHead"
local PlayerHead = ClassTypes.PlayerHead

local Heart_Beat_Time_Out = 10
local Heart_Beat_Timer_Tick = 3
local Battery_Timer_Tick = 60
local Exp_Update_Timer_Tick = 1

function PlayerStatePanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel');
    
    self._lastLevel = 0 --上一次等级
    self._levelChange = 0 --等级变化量
    
    self:InitCallback()
    self:InitComps()
    self:InitHPProgress()
    self:InitEXPProgress()
    self:InitPhoneInfo()
    self:ShowHeadImage()
    self:InitOtherPlayer()
    self:OnResize()
end

function PlayerStatePanel:InitComps()
    self._level = self:GetChildComponent("Text_Level", "TextMeshWrapper")
    self._txtVocation = self:GetChildComponent("Text_Vocation", "TextMeshWrapper")
    
    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)
    
    self._imgHead = self:FindChildGO("Image_Head")
    self._containerHeadFrame = self:FindChildGO("Container_HeadFrame")
    self._btnHead = self:FindChildGO("Button_Head")
    self._csBH:AddClick(self._btnHead, function()self:OnHeadClick() end)
    
    --头像红点
    self._imgRedPoint = self:FindChildGO("Image_RedPoint")
    self._imgRedPoint:SetActive(false)
    
    --VIP部分
    self._vipRedPoint = self:FindChildGO("Button_VIP/Image_RedPoint")
    self._vipRedPoint:SetActive(false)
    self._btnVip = self:FindChildGO("Button_VIP")
    self._csBH:AddClick(self._btnVip, function()self:OnVIPClick() end)
    self._vipNumber = self:AddChildComponent('Button_VIP/Container_VIP', XArtNumber)
    self._fxVIP = UIParticle.AddParticle(self.gameObject,"Button_VIP/fx_ui_menu2_vip")
    self._fxVIP:Play(true,true)
    
    self._containerVIPExperienceTips = self:FindChildGO("Container_VIPExperienceTips")
    self._txtVIPExperienceTips = self:GetChildComponent("Container_VIPExperienceTips/Text_Tips", "TextMeshWrapper")
    
    self._btnFightMode = self:FindChildGO("Button_FightMode")
    self._csBH:AddClick(self._btnFightMode, function()self:OnFightModeClick() end)
    self._fightModeImgs = {}
    for i = 1, 3 do
        local img = self:FindChildGO("Button_FightMode/Image_Mode" .. i)
        self._fightModeImgs[i] = img
    end
    self._txtMode = self:GetChildComponent("Button_FightMode/Text_Mode", "TextMeshWrapper")
    
    self._btnBuff = self:FindChildGO("Button_Buff")
    self._csBH:AddClick(self._btnBuff, function()self:OnBuffClick() end)
    self._buffNumber = self:AddChildComponent('Button_Buff/Container_Buff', XArtNumber)
    --self._buffNumber2 = self:AddChildComponent('Button_Buff/Container_Buff', XArtNumber)
    self._buffNumber:SetNumber(0)
    -- GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PK_VALUE, function()
    --     --self:UpdatePK()
    -- end)
    self._txtDiamond = self:GetChildComponent("Text_Diamond", "TextMeshWrapper")
    self._txtBindDiamond = self:GetChildComponent("Text_BindDiamond", "TextMeshWrapper")
    self._txtGold = self:GetChildComponent("Text_Gold", "TextMeshWrapper")

--self._refreshHeadImage = function(mode) self:RefreshHeadImage(mode) end
--EventDispatcher:AddEventListener(GameEvents.Refresh_Head_Image, self._refreshHeadImage)
end

function PlayerStatePanel:InitCallback()
    self._onLevelChange = function()self:OnLevelChange() end
    self._onVocationChange = function()self:OnVocationChange() end
    self._onHPChangeCB = function()self:OnHPChange() end
    self._onVIPChange = function()self:OnVIPChange() end
    self._onFightPowerChange = function()self:OnFightPowerChange() end
    
    self._onFightModeChange = function()self:OnFightModeChange() end
    self._onDiamondChange = function()self:UpdateDiamond() end
    self._onBindDiamondChange = function()self:UpdateBindDiamond() end
    self._onGoldChange = function()self:UpdateGold() end
    
    self._onLevelExpChange = function(diffLevel)self:OnEXPChange(diffLevel) end
    self._onBuffListChange = function()self:RefreshBuffNum() end
    self._refreshHeadImage = function(mode)self:RefreshHeadImage(mode) end
    self._showVIPButton = function(state)self:ShowVIPButton(state) end
    
    --查看其它玩家
    self._showOtherPlayerCb = function(avatar, shouldShow)
        self:ShowOtherPlayer(avatar, shouldShow)
    end
    
    self._updateOtherPlayerHPCb = function()
        self:UpdateOtherPlayerHp()
    end
    
    self._updateRedpointFunc = function(flag)self:SetRedPoint(flag) end
    self._updateVIPRedpointFunc = function(flag)self:SetVIPRedPoint(flag) end
    self._updateHeartBeat = function()self:UpdateHeartBeat() end
    self._onQualityChangeCb = function(value) self:OnQualityChange(value) end
    self._updateHeadFrameCb = function() self:UpdateHeadFrame() end
end

function PlayerStatePanel:AddEventListeners()
    local player = GameWorld.Player()
    
    player:AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChange)
    player:AddPropertyListener(EntityConfig.PROPERTY_VOCATION, self._onVocationChange)
    player:AddPropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._onVIPChange)
    player:AddPropertyListener(EntityConfig.PROPERTY_PK_MODE, self._onFightModeChange)
    player:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
    player:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
    player:AddPropertyListener(EntityConfig.PROPERTY_FIGHT_FORCE, self._onFightPowerChange)
    
    player:AddPropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS, self._onDiamondChange)
    player:AddPropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS_BIND, self._onBindDiamondChange)
    player:AddPropertyListener(EntityConfig.PROPERTY_MONEY_GOLD, self._onGoldChange)
    
    
    EventDispatcher:AddEventListener(GameEvents.ON_LEVEL_EXP_CHANGE, self._onLevelExpChange)
    
    EventDispatcher:AddEventListener(GameEvents.BUFF_LIST_CHANGE, self._onBuffListChange)
    
    
    EventDispatcher:AddEventListener(GameEvents.Refresh_Head_Image, self._refreshHeadImage)
    
    EventDispatcher:AddEventListener(GameEvents.Show_VIP_Button, self._showVIPButton)
    
    EventDispatcher:AddEventListener(GameEvents.SHOW_OTHER_PLAYER_HEAD, self._showOtherPlayerCb)
    
    EventDispatcher:AddEventListener(GameEvents.Update_Heart_Beat, self._updateHeartBeat)
    
    EventDispatcher:AddEventListener(GameEvents.OnQualityChanged, self._onQualityChangeCb)

    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Fashion_Info,self._updateHeadFrameCb)

end

function PlayerStatePanel:RemoveEventListeners()
    local player = GameWorld.Player()
    if player then
        player:RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChange)
        player:RemovePropertyListener(EntityConfig.PROPERTY_VOCATION, self._onVocationChange)
        player:RemovePropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._onVIPChange)
        player:RemovePropertyListener(EntityConfig.PROPERTY_PK_MODE, self._onFightModeChange)
        player:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
        player:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
        player:RemovePropertyListener(EntityConfig.PROPERTY_FIGHT_FORCE, self._onFightPowerChange)
        
        player:RemovePropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS, self._onDiamondChange)
        player:RemovePropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS_BIND, self._onBindDiamondChange)
        player:RemovePropertyListener(EntityConfig.PROPERTY_MONEY_GOLD, self._onGoldChange)
    end
    
    EventDispatcher:RemoveEventListener(GameEvents.ON_LEVEL_EXP_CHANGE, self._onLevelExpChange)
    
    EventDispatcher:RemoveEventListener(GameEvents.BUFF_LIST_CHANGE, self._onBuffListChange)
    
    
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Head_Image, self._refreshHeadImage)
    
    EventDispatcher:RemoveEventListener(GameEvents.Show_VIP_Button, self._showVIPButton)
    
    EventDispatcher:RemoveEventListener(GameEvents.SHOW_OTHER_PLAYER_HEAD, self._showOtherPlayerCb)
    
    EventDispatcher:RemoveEventListener(GameEvents.Update_Heart_Beat, self._updateHeartBeat)
    
    EventDispatcher:RemoveEventListener(GameEvents.OnQualityChanged, self._onQualityChangeCb)

    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Fashion_Info,self._updateHeadFrameCb)
end

----------------------------其他玩家头像----------------------------
function PlayerStatePanel:InitOtherPlayer()
    self._containerOtherPlayer = self:FindChildGO("Container_OtherPlayerHead")
    
    self._btnCheckOtherPlayer = self:FindChildGO("Container_OtherPlayerHead/Button_CheckInfo")
    self._csBH:AddClick(self._btnCheckOtherPlayer, function()self:OnOtherPlayerHeadClick() end)
    
    self._otherHpComp = UIProgressBar.AddProgressBar(self.gameObject, "Container_OtherPlayerHead/ProgressBar_HP")
    self._otherHpComp:SetProgress(1)
    self._otherHpComp:ShowTweenAnimate(false)
    self._otherHpComp:SetMutipleTween(false)
    self._otherHpComp:SetIsResetToZero(false)
    
    self._otherPlayerHead = self:AddChildLuaUIComponent("Container_OtherPlayerHead/Container_Head", PlayerHead)
    self._containerOtherPlayer:SetActive(false)
end

local posVec = Vector2.New(400, 450)
function PlayerStatePanel:OnOtherPlayerHeadClick()
    local d = {}
    d.playerName = self._otherAvatarName
    d.dbid = self._otherAvatarDBId
    d.rawListPos = posVec
    GUIManager.ShowPanel(PanelsConfig.PlayerInfoMenu, d)
end

function PlayerStatePanel:CheckOtherPlayerLeave()
    if self._curShowOtherPlayer then
        if GameWorld.GetEntity(self._curShowOtherPlayer.id) == nil then
            self:ShowOtherPlayer(nil,false)
        end
    else
        self:ShowOtherPlayer(nil,false)
    end
end

function PlayerStatePanel:ShowOtherPlayer(avatar, shouldShow)
    if shouldShow then
        if self._curShowOtherPlayer then
            self._curShowOtherPlayer:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._updateOtherPlayerHPCb)
            self._curShowOtherPlayer:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._updateOtherPlayerHPCb)
        end
        self._curShowOtherPlayer = avatar
        avatar:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._updateOtherPlayerHPCb)
        avatar:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._updateOtherPlayerHPCb)
        self._containerOtherPlayer:SetActive(true)
        self._otherAvatarName = avatar.name
        self._otherAvatarDBId = BaseUtil.GetDBIdFromCrossUUId(avatar.cross_uuid)
        local levelstr = StringStyleUtil.GetLevelString(avatar.level)
        local headIcon = RoleDataHelper.GetHeadIconByVocation(avatar.vocation)
        
        self._otherPlayerHead:SetPlayerHeadData(self._otherAvatarName, levelstr, headIcon)
        self:UpdateOtherPlayerHp()

        self._checkOhterPlayerLeaveTimer = TimerHeap:AddSecTimer(0, 1, 0, function ()
            self:CheckOtherPlayerLeave()
        end)
    else
        if self._curShowOtherPlayer then
            self._curShowOtherPlayer:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._updateOtherPlayerHPCb)
            self._curShowOtherPlayer:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._updateOtherPlayerHPCb)
            self._curShowOtherPlayer = nil
        end
        if self._checkOhterPlayerLeaveTimer then
            TimerHeap:DelTimer(self._checkOhterPlayerLeaveTimer)
            self._checkOhterPlayerLeaveTimer = nil
        end
        self._containerOtherPlayer:SetActive(false)
    end
end

function PlayerStatePanel:UpdateOtherPlayerHp(curHp, maxHp)
    local avatar = self._curShowOtherPlayer
    if avatar == nil then
        return
    end
    local otherCurHp = avatar.cur_hp
    local otherMaxHp = avatar.max_hp
    self._otherHpComp:SetProgress(otherCurHp / otherMaxHp)
end

--头像 mode 0:动态 1:图片
function PlayerStatePanel:RefreshHeadImage(mode)
    local gameQuality = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.RENDER_QUALITY)
    
    --低品质不使用动态头像
    if gameQuality ~= SystemSettingConfig.QUALITY_LOW then
        mode = mode or 0
        if mode == 1 then
            self.rawImage.texture = GameWorld.ShowPlayer().textureHead
        else
            self.rawImage.texture = GameWorld.ShowPlayer().RenderTextureHead
        end
    end
end

function PlayerStatePanel:OnQualityChange(value)
    local gameQuality = value or SystemSettingManager:GetPlayerSetting(SystemSettingConfig.RENDER_QUALITY)
    if gameQuality == SystemSettingConfig.QUALITY_LOW then
        self._containerHeadShow:SetActive(false)
        self._vocationHead:SetActive(true)
    else
        self._containerHeadShow:SetActive(true)
        self._vocationHead:SetActive(false)
    end
end
--显示动态头像
function PlayerStatePanel:ShowHeadImage()
    self._containerHeadShow = self:FindChildGO("Container_HeadShow")
    self._rawImageObj = self:FindChildGO("Container_HeadShow/RawImage")
    self.rawImage = self._rawImageObj:AddComponent(typeof(LuaUIRawImage))
    self.rawImage.texture = GameWorld.ShowPlayer().RenderTextureHead
    
    self._vocationHead = self:FindChildGO("Container_VocationHead")
end

function PlayerStatePanel:UpdateVocationHead()
    local headId = RoleDataHelper.GetHeadIconByVocation(GameWorld.Player().vocation)
    GameWorld.AddIcon(self._vocationHead, headId)
end

function PlayerStatePanel:UpdateHeadFrame()
    local frameId = FashionManager:GetCurLoadFashionId(public_config.FASHION_TYPE_PHOTO)
    if frameId == self._frameId then
        return
    else
        self._frameId = frameId
    end
    if frameId and frameId > 0 then
        local modelId = FashionDataHelper.GetFashionModel(frameId)
        self._containerHeadFrame:SetActive(true)
        self._imgHead:SetActive(false)
        GameWorld.AddIcon(self._containerHeadFrame,modelId,0,false,1,true,false)
    else
        self._containerHeadFrame:SetActive(false)
        self._imgHead:SetActive(true)
    end
end

function PlayerStatePanel:InitHPProgress()
    self._hpComp = UIProgressBar.AddProgressBar(self.gameObject, "ProgressBar_HP")
    self._hpComp:SetProgress(0)
    self._hpComp:ShowTweenAnimate(true)
    self._hpComp:SetMutipleTween(true)
    self._hpComp:SetIsResetToZero(false)
    self._hpComp:SetTweenTime(0.2)
    local tweenCompleteCB =
        function()
            self:ProgressComplete()
        end
    self._hpComp:SetTweenCompleteCB(tweenCompleteCB)

-- local nextTweenCB =
-- function ()
--     self:NextTween()
-- end
-- self._hpComp:SetNextTweenCB(nextTweenCB)
end

function PlayerStatePanel:InitEXPProgress()
    self._expComp = UIScaleProgressBar.AddProgressBar(self.gameObject, "ProgressBar_Exp")
    self._expComp:SetProgress(0)
    self._expComp:ShowTweenAnimate(true)
    self._expComp:SetMutipleTween(true)
    self._expComp:SetIsResetToZero(false)
end

function PlayerStatePanel:InitPhoneInfo()
    self._txtTime = self:GetChildComponent("Container_PhoneInfo/Text_Time", "TextMeshWrapper")
    self._timerId = TimerHeap:AddSecTimer(0, 1, 0, function()
        self:OnTimeChange()
    end)
    
    self._batteryComp = UIProgressBar.AddProgressBar(self.gameObject, "Container_PhoneInfo/ProgressBar_Battery")
    self._batteryComp:SetProgress(0)
    self._batteryComp:ShowTweenAnimate(false)
    self._batteryComp:SetMutipleTween(false)
    self._batteryComp:SetIsResetToZero(false)
    
    self._networkPoorGO = self:FindChildGO("Container_PhoneInfo/Container_NetworkPoor")
    self._networkPoorGO:SetActive(false)
    self._networkPoorOn = false
end

function PlayerStatePanel:OnResize()
    local canvasWidth = GUIManager.GetCutoutScreenPanelWidth()-- GUIManager.canvasWidth --GUIManager._rootRectTransform.sizeDelta.x;
    local canvasHeight = GUIManager.canvasHeight
    local lineSpace = canvasWidth / 10
    for i = 1, 9 do
        local imgline = self:FindChildGO("ProgressBar_Exp/Container_Line/Image_Line" .. i)
        imgline.transform.anchoredPosition = Vector2.New(lineSpace * i, 0)
    end
    
    local expCompTrans = self:FindChildGO("ProgressBar_Exp").transform
    expCompTrans.sizeDelta = Vector2.New(canvasWidth, 6)
    expCompTrans.anchoredPosition = Vector2.New(0, 720 - canvasHeight)
    
    local containerPhoneInfo = self:FindChildGO("Container_PhoneInfo")
    containerPhoneInfo.transform.anchoredPosition = Vector2(0, 726 - canvasHeight)
end

function PlayerStatePanel:OnShow()
    self.rawImage.texture = GameWorld.ShowPlayer().RenderTextureHead
    self:AddEventListeners()
    self:OnLevelChange()
    self:OnVocationChange()
    self:OnFightPowerChange()
    self:OnFightModeChange()
    self:OnVIPChange()
    self:OnHPChange()
    self:OnEXPChange()
    self:OnTimeChange()
    self:RefreshBuffNum()
    self:UpdateVocationHead()
    self:OnQualityChange()
    self:UpdateDiamond()
    self:UpdateBindDiamond()
    self:UpdateGold()
    self:UpdateHeadFrame()
    
    RedPointManager:RegisterHeadUpdateFunc(self._updateRedpointFunc)
    RedPointManager:RegisterNodeUpdateFunc(RedPointManager:GetRedPointDataByPanelName(PanelsConfig.VIP), self._updateVIPRedpointFunc)
    
    self._lastPower = 0
    self._lastHeartBeatTicker = 0
    self._heartBeattimer = TimerHeap:AddSecTimer(0, Heart_Beat_Timer_Tick, 0, function()
        self._lastHeartBeatTicker = self._lastHeartBeatTicker + Heart_Beat_Timer_Tick
        self:HeartBeatCheck()
    end)
    
    self._batteryTimer = TimerHeap:AddSecTimer(0, Battery_Timer_Tick, 0, function()
        self:UpdateBattery()
    end)
    self:CleanExpUpdateTimer()
    self._expUpdateTimer = TimerHeap:AddSecTimer(0, Exp_Update_Timer_Tick, 0, function()self:UpdateExp() end)
end

function PlayerStatePanel:OnClose()
    RedPointManager:RegisterHeadUpdateFunc(nil)
    RedPointManager:RegisterNodeUpdateFunc(RedPointManager:GetRedPointDataByPanelName(PanelsConfig.VIP), nil)
    self:RemoveEventListeners()
    
    if self._heartBeattimer then
        TimerHeap:DelTimer(self._heartBeattimer)
    end
    
    if self._batteryTimer then
        TimerHeap:DelTimer(self._batteryTimer)
    end
    self:CleanExpUpdateTimer()
end

function PlayerStatePanel:OnDestroy()
    self._csBH = nil
    TimerHeap:DelTimer(self._timerId)
    self._txtTime = nil
    self._hpComp = nil
    self._expComp = nil
end

function PlayerStatePanel:CleanExpUpdateTimer()
    if self._expUpdateTimer then
        TimerHeap:DelTimer(self._expUpdateTimer)
        self._expUpdateTimer = nil
    end
end

function PlayerStatePanel:UpdateExp()
    if not self._isExpChange then return end
    self._isExpChange = false
    
    if PlayerManager.RoleManager:CanUpgradeLevel() then
        local diffLevel = self._lastDiffLevel or 0
        self._lastDiffLevel = nil
        
        local curExp = GameWorld.Player().money_exp
        local level = GameWorld.Player().level
        local maxExp = RoleDataHelper.GetCurLevelExp(level)
        local progress = curExp / maxExp + diffLevel
        self._expComp:ShowTweenAnimate(true)
        self._expComp:SetMutipleTween(true)
        self._expComp:SetProgress(progress)
    else
        self._expComp:ShowTweenAnimate(false)
        self._expComp:SetMutipleTween(false)
        self._expComp:SetProgress(1)
    end
    self._levelChange = 0
end

function PlayerStatePanel:OnHPChange()
    local entity = GameWorld.Player()
    if entity ~= nil then
        local curHp = entity.cur_hp or 0
        local maxHp = entity.max_hp or 0
        self._hpComp:SetProgressByValue(curHp, maxHp)
        self._hpComp:SetProgressText(self:GetNumString(curHp) .. "/" .. self:GetNumString(maxHp))
    end
end

function PlayerStatePanel:GetNumString(number)
    local str
    if number >= 100000 then
        str = tostring(math.floor(number / 100) / 100) .. LanguageDataHelper.GetContent(143)
    else
        str = tostring(number)
    end
    return str
end

function PlayerStatePanel:OnFightPowerChange()
    local entity = GameWorld.Player()
    if entity ~= nil then
        self._fpNumber:SetNumber(entity.fight_force)
    end
end



function PlayerStatePanel:OnLevelChange()
    local level = GameWorld.Player().level or {}
    self._level.text = StringStyleUtil.GetLevelString(level)
end

function PlayerStatePanel:OnVocationChange()
    local vocation = GameWorld.Player().vocation
    self._txtVocation.text = RoleDataHelper.GetRoleNameTextByVocation(vocation)
end

function PlayerStatePanel:OnEXPChange(diffLevel)
    self._lastDiffLevel = diffLevel
    self._isExpChange = true
end

--点击头像
function PlayerStatePanel:OnHeadClick()
    --self._hpComp:SetProgress(3.7)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_player_state_head_button")
    GUIManager.ShowPanel(PanelsConfig.SubMainUI)
end

function PlayerStatePanel:SetRedPoint(isActive)
    self._imgRedPoint:SetActive(isActive)
end

function PlayerStatePanel:SetVIPRedPoint(isActive)
    self._vipRedPoint:SetActive(isActive)
end

--开VIP
function PlayerStatePanel:OnVIPClick()
    GUIManager.ShowPanel(PanelsConfig.VIP)
end

function PlayerStatePanel:OnVIPChange()
    local playerVipLevel = GameWorld.Player().vip_level or 0
    local vipReal = GameWorld.Player().vip_real or 0
    self._vipNumber:SetNumber(playerVipLevel)
    --体验中
    if playerVipLevel > 0 and vipReal == 0 then
        self._containerVIPExperienceTips:SetActive(true)
        if self._vipExperienceTimer == nil then
            local vipEndtime = GameWorld.Player().vip_endtime
            self._expericeLeftTime = math.max(0, vipEndtime - DateTimeUtil.GetServerTime())
            self._vipExperienceTimer = TimerHeap:AddSecTimer(0, 1, 0, function()
                self:VIPExperinceCD()
            end)
        end
    --一般时候
    else
        self._containerVIPExperienceTips:SetActive(false)
    end
end

function PlayerStatePanel:VIPExperinceCD()
    if self._expericeLeftTime == 0 and self._vipExperienceTimer then
        TimerHeap:DelTimer(self._vipExperienceTimer)
        self._vipExperienceTimer = nil
        self._containerVIPExperienceTips:SetActive(false)
    end
    local timeStr = DateTimeUtil:FormatFullTime(self._expericeLeftTime, false, true)
    self._txtVIPExperienceTips.text = LanguageDataHelper.CreateContentWithArgs(511, {["0"] = timeStr})
    self._expericeLeftTime = self._expericeLeftTime - 1
end

--模式切换
function PlayerStatePanel:OnFightModeClick()
    GUIManager.ShowPanel(PanelsConfig.PKMode)
end

function PlayerStatePanel:OnFightModeChange()
    local mode = GameWorld.Player().pk_mode + 1
    for i = 1, 3 do
        if i == mode then
            self._fightModeImgs[i]:SetActive(true)
        else
            self._fightModeImgs[i]:SetActive(false)
        end
    end
    
    self._txtMode.text = LanguageDataHelper.CreateContent(70002 + mode)
end

function PlayerStatePanel:OnBuffClick()
    GUIManager.ShowPanel(PanelsConfig.Buff)
end

function PlayerStatePanel:OnTimeChange()
    local t = DateTimeUtil.Now()
    if self._lastHour == t.hour and self._lastMin == t.min then
        if GameWorld.showFps then
            self._txtTime.text = self._lastText .. string.format(" fps: %0.0f avgFps: %0.0f", GameWorld.GetFps(), GameWorld.GetAverageFps())
        end
        return
    end
    self._lastHour = t.hour
    self._lastMin = t.min
    --LoggerHelper.Error("DateTimeUtil.serverTimeStamp"..DateTimeUtil.serverTimeStamp)
    self._lastText = DateTimeUtil:FormatNumber(t.hour) .. ":" .. DateTimeUtil:FormatNumber(t.min)--..":"..DateTimeUtil:FormatNumber(t.sec)
    self._txtTime.text = self._lastText
end

function PlayerStatePanel:UpdateDiamond()
    local money = GameWorld.Player().money_coupons
    self._txtDiamond.text = self:GetDiamondStr(money)
end

function PlayerStatePanel:UpdateBindDiamond()
    local money = GameWorld.Player().money_coupons_bind
    self._txtBindDiamond.text = self:GetDiamondStr(money)
end

function PlayerStatePanel:GetDiamondStr(money)
    if money < 10000000 then
        return tostring(money)
    elseif money < 100000000 then
        return tostring(math.floor(money / 1000) / 10) .. LanguageDataHelper.GetContent(143)
    else
        return tostring(math.floor(money / 10000)) .. LanguageDataHelper.GetContent(143)
    end
end

function PlayerStatePanel:UpdateGold()
    local money = GameWorld.Player().money_gold
    self._txtGold.text = StringStyleUtil.GetLongNumberStringEx(money)
end

function PlayerStatePanel:ProgressComplete()
--LoggerHelper.Error("ProgressComplete")
end

function PlayerStatePanel:NextTween()
--LoggerHelper.Error("NextTween")
end

--设定buff
function PlayerStatePanel:SetBuff()
end


function PlayerStatePanel:UpdatePK()
    local pkValue = GameWorld.Player().pk_value
--LoggerHelper.Error("PlayerStatePanel:UpdatePK()"..pkValue)
end

function PlayerStatePanel:RefreshBuffNum()
    local buffList = MainUIManager:GetCurBuffList()
    if buffList then
        self._buffNumber:SetNumber(#buffList)
    else
        self._buffNumber:SetNumber(0)
    end

end

function PlayerStatePanel:ShowVIPButton(state)
    self._btnVip:SetActive(state)
end

function PlayerStatePanel:ShowNetworkPoor(state)
    if self._networkPoorOn == state then
        return
    end
    self._networkPoorOn = state
    self._networkPoorGO:SetActive(state)
end

--更新电量
function PlayerStatePanel:UpdateBattery()
    local power = GameLoader.PlatformSdk.PlatformSdkMgr.Instance:GetBattery() or 0
    if self._lastPower ~= power then
        self._batteryComp:SetProgress(power / 100)
        self._lastPower = power
    end
end

function PlayerStatePanel:UpdateHeartBeat(state)
    self._lastHeartBeatTicker = 0
    self:HeartBeatCheck()
end

function PlayerStatePanel:HeartBeatCheck()
    if self._lastHeartBeatTicker > Heart_Beat_Time_Out then
        self:ShowNetworkPoor(true)
    else
        self:ShowNetworkPoor(false)
    end
end
