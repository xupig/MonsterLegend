-- SkillControlPanel.lua
local ClassTypes = ClassTypes
local SkillControlPanel = Class.SkillControlPanel(ClassTypes.BasePanel)

local ComponentsConfig = GameConfig.ComponentsConfig
local RoleDataHelper = GameDataHelper.RoleDataHelper
local SkillDataHelper = GameDataHelper.SkillDataHelper

require "Modules.ModuleMainUI.ChildComponent.SkillButton"
require "Modules.ModuleMainUI.ChildComponent.LockButton"
require "Modules.ModuleMainUI.ChildComponent.FlyButton"
require "Modules.ModuleMainUI.ChildComponent.JumpButton"
require "Modules.ModuleMainUI.ChildComponent.RushButton"

local SkillButton = ClassTypes.SkillButton
local LockButton = ClassTypes.LockButton
local FlyButton = ClassTypes.FlyButton
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local Locater = GameMain.Locater
local XButtonHitArea = GameMain.XButtonHitArea
local StateConfig = GameConfig.StateConfig
local skillData = PlayerManager.PlayerDataManager.skillData
local SKILL_BUTTON_SUM = 9
local bit = bit
local SkillManager = PlayerManager.SkillManager
local JumpButton = ClassTypes.JumpButton
local RushButton = ClassTypes.RushButton
local XSkillDragContainer = GameMain.XSkillDragContainer

function SkillControlPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self._groundPosToSlot = GameWorld.Player().groundPosToSlot
    self._groundPosTransformToSlot = GameWorld.Player().groundPosTransformToSlot
    self:InitSkillButtons()
    self:InitCallbackFunc()
    self.playerInAir = false
    self:RefreshButtonsState()
    --UI加载完刷新下状态
    --GameWorld.Player():CallRefreshSkillButtons()
end

function SkillControlPanel:OnShow()
    GameWorld.Player():CallRefreshSkillButtons()
    self:SetSkillPage(1)
    self:RefreshIcons()
end

function SkillControlPanel:InitSkillButtons()
    self._skillButtons = {}
    self._skillButtonsPos = {} 
    self._skillButtonsWorldPos = {}
    for i = 1, SKILL_BUTTON_SUM do
        local skillButton = self:AddChildLuaUIComponent("Container_SkillButtons/Container_Skills/Container_Skill"..i, SkillButton)
        skillButton.skillPos = i
        self._skillButtons[i] = skillButton
        self._skillButtonsPos[i] = skillButton.transform.localPosition
        self._skillButtonsWorldPos[i] = skillButton.transform.position
    end
    SkillManager:SetSkillButtonPosInfo(self._skillButtonsWorldPos)
    self._lockButton = self:AddChildLuaUIComponent("Container_LockButton", LockButton)
    self._lockButtonPos = self._lockButton.transform.localPosition

    self.skillDragContainer = self:AddChildComponent('Container_SkillButtons', XSkillDragContainer)
end

function SkillControlPanel:ShowFixFuncButtons(state)
    if state then
        self._lockButton.transform.localPosition = self._lockButtonPos
    else
        self._lockButton.transform.localPosition = self.HIDE_POSITION
    end 
end

function SkillControlPanel:InitSkillStick()

	self._stickContainerRect = self:GetChildComponent("Container_SkillButtons/Container_SkillStick/Container_Stick", 'RectTransform')
    self._stickRect = self:GetChildComponent("Container_SkillButtons/Container_SkillStick/Container_Stick/Image_ControlStick", 'RectTransform')
    self._stickLocater = self._stickContainerRect.gameObject:AddComponent(typeof(Locater))
    self._rectTransform = self:GetChildComponent("Container_SkillButtons/Container_SkillStick",'RectTransform')
    self._rectTransform.gameObject:AddComponent(typeof(XButtonHitArea))
    self._originalPosition = self._stickLocater.Position
    self._controlStickImage = self:GetChildComponent("Container_SkillButtons/Container_SkillStick/Container_Stick/Image_ControlStick", 'ImageWrapper')
    self._stickBGImage = self:GetChildComponent("Container_SkillButtons/Container_SkillStick/Container_Stick/Image_StickBG", 'ImageWrapper')

	self._ControlStickSourcePosition = Vector2.zero
    self._ControlStickSourcePosition.x = self._stickRect.localPosition.x
    self._ControlStickSourcePosition.y = self._stickRect.localPosition.y
    self._ControlStickCenterOffset = Vector2.zero
    self._ControlStickCenterOffset.x = self._stickRect.rect.width * 0.5
    self._ControlStickCenterOffset.y = -self._stickRect.rect.height * 0.5	

    local center = self._ControlStickSourcePosition + self._ControlStickCenterOffset
    for i = 1, SKILL_BUTTON_SUM do
         self._skillButtons[i]:RefreshSkillStickData(self._stickContainerRect,center)
    end
end

function SkillControlPanel:SetCurSkillSlot(slot)
    self._curSkillSlot = slot
end

function SkillControlPanel:ResetState(state)

end

function SkillControlPanel:InitCallbackFunc()
    self._onCDChange = slot(self.OnSkillCDChange, self)
    EventDispatcher:AddEventListener(GameEvents.SET_SKILL_CD, self._onCDChange)
    self._onIconChange = slot(self.OnSkillIconChange, self)
    EventDispatcher:AddEventListener(GameEvents.SET_SKILL_ICON, self._onIconChange)
    self._refreshButtonsState = slot(self.RefreshButtonsState, self)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Buttons_State, self._refreshButtonsState)
    self._showFixFuncButtons = function(state) self:ShowFixFuncButtons(state) end
    EventDispatcher:AddEventListener(GameEvents.Show_Fix_Func_Buttons, self._showFixFuncButtons)
    self._setCurSkillSlot = function (slot) self:SetCurSkillSlot(slot) end
    EventDispatcher:AddEventListener(GameEvents.SkillControlPanel_SetCurSkillSlot, self._setCurSkillSlot)
    self._resetState = function () self:ResetState() end
    EventDispatcher:AddEventListener(GameEvents.SkillControlPanel_ResetState, self._resetState)
    self._setSkillPage = function(page) self:SetSkillPage(page) end
    EventDispatcher:AddEventListener(GameEvents.SkillControlPanel_SetSkillPage, self._setSkillPage)
end

function SkillControlPanel:OnDestroy() 
    self._csBH = nil
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onMapChange)
    EventDispatcher:RemoveEventListener(GameEvents.SET_SKILL_CD, self._onCDChange)
    EventDispatcher:RemoveEventListener(GameEvents.SET_SKILL_ICON, self._onIconChange)   
    EventDispatcher:RemoveEventListener(GameEvents.SkillControlPanel_SetCurSkillSlot, self._setCurSkillSlot)
    EventDispatcher:RemoveEventListener(GameEvents.SkillControlPanel_ResetState, self._resetState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Buttons_State, self._refreshButtonsState)
    EventDispatcher:RemoveEventListener(GameEvents.SkillControlPanel_SetSkillPage, self._setSkillPage)
end

function SkillControlPanel:SetSkillPage(page)
    self.skillDragContainer:SetPage(page)
end

function SkillControlPanel:OnSkillCDChange(pos, cd, totalCD, roundCD)
    if pos > SKILL_BUTTON_SUM then
        return
    end
    self._skillButtons[pos]:SetCoolDown(cd,totalCD,roundCD)
end

function SkillControlPanel:OnSkillIconChange(pos, iconId)
    if pos > SKILL_BUTTON_SUM then
        return
    end
    self._skillButtons[pos]:SetIcon(iconId)
end

function SkillControlPanel:RefreshIcons()
    for i = 1, SKILL_BUTTON_SUM do
         local button = self._skillButtons[i]
         if button.iconId ~= 0 then
            button:SetIcon(button.iconId)
         end
    end
end

function SkillControlPanel:RefreshButtonsState()
    local player = GameWorld.Player()
    local isInState = (player.isInWorld and player:InState(StateConfig.AVATAR_STATE_TRANSFORM))
    self.groundSlot = skillData:GetGroundSkillSlotState()
    local slot = 0
    for i = 1, SKILL_BUTTON_SUM do
        if isInState then
            --self._skillButtons[i].transform.localPosition = self.HIDE_POSITION
            self._skillButtons[i]:ShowLockIcon(true, slot)
        else
            slot = self._groundPosToSlot[i]
            if self.groundSlot[slot] ~= nil then
                self._skillButtons[i]:ShowLockIcon(false, slot)
                if self.groundSlot[slot] == 1 then
                    self._skillButtons[i]:ShowLockIcon(false, slot)
                else
                    self._skillButtons[i]:ShowLockIcon(true, slot)
                end
            else
                self._skillButtons[i]:ShowLockIcon(true, slot)
            end
        end
    end
end

