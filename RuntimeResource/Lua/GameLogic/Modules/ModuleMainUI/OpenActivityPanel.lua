--OpenActivityPanel.lua
local OpenActivityPanel = Class.OpenActivityPanel(ClassTypes.BasePanel)
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local OpenActivityManager = PlayerManager.OpenActivityManager
local UIList = ClassTypes.UIList
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local OpenActivityData = PlayerManager.PlayerDataManager.openActivityData


require "Modules.ModuleMainUI.ChildComponent.OpenActivityListItem"
local OpenActivityListItem = ClassTypes.OpenActivityListItem
local UIComponentUtil = GameUtil.UIComponentUtil

require "UIComponent.Base.UIScrollView"
local UIScrollView = ClassTypes.UIScrollView

function OpenActivityPanel:Awake()
    self.disableCameraCulling = true
    self:InitViews()
    self._refreshList = function()self:RefreshActivityList() end
end



function OpenActivityPanel:InitViews()
    self._upgradeListGo = self:FindChildGO("Container_List")
    self._shadeButton = self:FindChildGO("Container_List/Button_Shade")
    self._csBH:AddClick(self._shadeButton, function() self:OnCloseList() end)
    self._closeButton = self:FindChildGO("Container_List/Container_Act/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnCloseList() end)
    --self._upgradeListGo:SetActive(false)
    self:InitUpgradeList()
end

function OpenActivityPanel:InitUpgradeList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_List/Container_Act/ScrollViewList")
	self._upgradeList = list
	self._upgradeListlView = scrollView 
    self._upgradeListlView:SetHorizontalMove(false)
    self._upgradeListlView:SetVerticalMove(true)
    self._upgradeList:SetItemType(OpenActivityListItem)
    self._upgradeList:SetPadding(0, 0, 0, 8)
    self._upgradeList:SetGap(8, 0)
    self._upgradeList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._actionListTrans = self:FindChildGO("Container_List/Container_Act").transform
    self._listBg = self:FindChildGO("Container_List/Container_Act/Image_Di")
    self._listKuang = self:FindChildGO("Container_List/Container_Act/Image_Bg")
    self._rectTransformBg = self._listBg:GetComponent(typeof(UnityEngine.RectTransform))
    self._rectTransformKuang = self._listKuang:GetComponent(typeof(UnityEngine.RectTransform))
end

function OpenActivityPanel:OnShow(data)
    --LoggerHelper.Error("OpenActivityPanel:OnShow"..PrintTable:TableToStr(data))
    self._actionListTrans.anchoredPosition = Vector2.New(data.x, self._actionListTrans.anchoredPosition.y)
    --self._actionListTrans.anchoredPosition = Vector2.New(data.x, data.y)
    self:AddEventListeners()
    local followNew = {}
    local followData =  GlobalParamsHelper.GetNSortValue(934)
    for i,followItemData in ipairs(followData) do
        if followItemData[2][1] ~= 0 then
            if OpenActivityData:GetRemindByType(followItemData[2][1]) then
                table.insert(followNew,followItemData)
            else 
                OpenActivityData:SetChangeList(followItemData[2][1],false)
            end
        end

        if followItemData[2][2] ~= 0 then
            if OpenActivityData:GetRemindByType(followItemData[2][2]) then
                table.insert(followNew,followItemData)
            else 
                OpenActivityData:SetChangeList(followItemData[2][2],false)
            end
        end
    end
    
    if #followNew > 0 then
        --self._upgradeListGo:SetActive(true)
    end

    if #followNew > 4 then        
        self._upgradeListlView:SetVerticalMove(false)
    else
        self._upgradeListlView:SetVerticalMove(false)
    end
    --LoggerHelper.Error("GlobalParamsHelper.GetParamValue(870):"..PrintTable:TableToStr(followData))
    self._upgradeList:SetDataList(followNew)

    self._rectTransformBg.sizeDelta = Vector2(self._rectTransformBg.sizeDelta.x, #followNew*40+65)
    self._rectTransformKuang.sizeDelta = Vector2(self._rectTransformKuang.sizeDelta.x, #followNew*40+70)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleMainUI.OpenActivityPanel")
end

function OpenActivityPanel:RefreshActivityList()
    local followNew = {}
    local followData =  GlobalParamsHelper.GetNSortValue(934)
    for i,followItemData in ipairs(followData) do
        if followItemData[2][1] ~= 0 then
            if OpenActivityData:GetRemindByType(followItemData[2][1]) then
                table.insert(followNew,followItemData)
            else 
                OpenActivityData:SetChangeList(followItemData[2][1],false)
            end
        end

        if followItemData[2][2] ~= 0 then
            if OpenActivityData:GetRemindByType(followItemData[2][2]) then
                table.insert(followNew,followItemData)
            else 
                OpenActivityData:SetChangeList(followItemData[2][2],false)
            end
        end
    end
    
    if #followNew > 0 then
        --self._upgradeListGo:SetActive(true)
    end

    if #followNew > 4 then        
        self._upgradeListlView:SetVerticalMove(false)
    else
        self._upgradeListlView:SetVerticalMove(false)
    end
    --LoggerHelper.Error("GlobalParamsHelper.GetParamValue(870):"..PrintTable:TableToStr(followData))
    self._upgradeList:SetDataList(followNew)

    self._rectTransformBg.sizeDelta = Vector2(self._rectTransformBg.sizeDelta.x, #followNew*40+65)
    self._rectTransformKuang.sizeDelta = Vector2(self._rectTransformKuang.sizeDelta.x, #followNew*40+70)
end


function OpenActivityPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.OpenActivityListChange, self._refreshList)
end


function OpenActivityPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.OpenActivityListChange, self._refreshList)
end

function OpenActivityPanel:RemindChange(remindType)
    if OpenActivityData:GetRemindByType(remindType) then

    else 

    end
end

function OpenActivityPanel:SetData(data)
end

function OpenActivityPanel:OnClose()
    self:RemoveEventListeners()
end


function OpenActivityPanel:OnCloseList()
    GameManager.GUIManager.ClosePanel(PanelsConfig.OpenActivity)
end