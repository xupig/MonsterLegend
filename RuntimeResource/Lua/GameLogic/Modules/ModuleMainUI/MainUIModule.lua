-- MainUIModule.lua
MainUIModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function MainUIModule.Init()
    GUIManager.AddPanel(PanelsConfig.Stick, "Panel_Stick", GUILayer.LayerUIMain, "Modules.ModuleMainUI.StickPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.SkillControl, "Panel_SkillControl", GUILayer.LayerUIMain, "Modules.ModuleMainUI.SkillControlPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.Strengthen, "Panel_Strengthen", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.StrengthenPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.SubMainMenu, "Panel_SubMainMenu", GUILayer.LayerUIMain, "Modules.ModuleMainUI.SubMainMenuPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.Upgrade, "Panel_Upgrade", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.UpgradePanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.OpenActivity, "Panel_OpenActivity", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.OpenActivityPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.PlayerState, "Panel_PlayerState", GUILayer.LayerUIMain, "Modules.ModuleMainUI.PlayerStatePanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.Community, "Panel_Community", GUILayer.LayerUIMain, "Modules.ModuleMainUI.CommunityPanel", true, false)
    -- GUIManager.AddPanel(PanelsConfig.GMCommand, "Panel_FunctionTest", GUILayer.LayerUIPanel, "Modules.ModuleMainUI.GMCommandPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.MainMenu, "Panel_MainMenu", GUILayer.LayerUIMain, "Modules.ModuleMainUI.MainMenuPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.BossState, "Panel_BossState", GUILayer.LayerUIMain, "Modules.ModuleMainUI.BossStatePanel", true, false)
    GUIManager.AddPanel(PanelsConfig.Collect, "Panel_Collect", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.CollectPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.WaitingLoadScene, "Panel_WaitingLoadScene", GUILayer.LayerUIMain, "Modules.ModuleMainUI.WaitingLoadScenePanel", true, false)
    GUIManager.AddPanel(PanelsConfig.SpecialDialog, "Panel_SpecialDialog", GUILayer.LayerUIMain, "Modules.ModuleMainUI.SpecialDialogPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.MiniMap, "Panel_MiniMap", GUILayer.LayerUIMain, "Modules.ModuleMainUI.MiniMapPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.LeftMainUI, "Panel_LeftMainUI", GUILayer.LayerUIMain, "Modules.ModuleMainUI.LeftMainUIPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.AionTowerMainUI, "Panel_AionTowerMainUI", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.AionTowerMainUIPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.FunctionPreview, "Panel_FunctionPreview", GUILayer.LayerUIMain, "Modules.ModuleMainUI.FunctionPreviewPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.QualityChange, "Panel_QualityChangePanel", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.QualityChangePanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.FunctionOpenTips, "Panel_FunctionOpenTips", GUILayer.LayerUIFloat, "Modules.ModuleMainUI.FunctionOpenTipsPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.NewEquipTip, "Panel_NewEquipTip", GUILayer.LayerUIFloat, "Modules.ModuleMainUI.NewEquipTipPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST, nil, true)
    GUIManager.AddPanel(PanelsConfig.InstQuit, "Panel_InstQuit", GUILayer.LayerUIMain, "Modules.ModuleMainUI.InstQuitPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.WorldBossMainUI, "Panel_WorldBossMainUI", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.WorldBossMainUIPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.PvpMainUI, "Panel_PvpMainUI", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.PvpMainUIPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.MarryMainUI, "Panel_MarryMainUI", GUILayer.LayerUIMain, "Modules.ModuleMainUI.MarryMainUIPanel", true, PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.Buff, "Panel_Buff", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.BuffMainUIPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.Remind, "Panel_Remind", GUILayer.LayerUIMain, "Modules.ModuleMainUI.RemindMainUIPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.GuildMonsterOpenTips, "Panel_GuildMonsterOpenTips", GUILayer.LayerUIMiddle, "Modules.ModuleMainUI.GuildMonsterOpenTipsPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.ActivityOpenTips, "Panel_GuildMonsterOpenTips", GUILayer.LayerUIFloat, "Modules.ModuleMainUI.ActivityOpenTipsPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)

    GUIManager.AddPanel(PanelsConfig.CutoutScreenBorder,"Panel_CutoutScreenBorder",GUILayer.LayerUITop,"Modules.ModuleMainUI.CutoutScreenBorderPanel",false,PanelsConfig.EXTEND_TO_FIT)
    GUIManager.AddPanel(PanelsConfig.PowerSave, "Panel_PowerSave", GUILayer.LayerUITop, "Modules.ModuleMainUI.PowerSavePanel", false, PanelsConfig.IS_CENTER_ADJUST)
end

return MainUIModule
