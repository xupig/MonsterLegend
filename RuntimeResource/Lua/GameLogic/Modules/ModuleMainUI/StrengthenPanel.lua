--StrengthenPanel.lua
local StrengthenPanel = Class.StrengthenPanel(ClassTypes.BasePanel)

local StrengthenManager = PlayerManager.StrengthenManager
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local stop = Vector3.New(0, 0, 360)
local start = Vector3.New(0, 0, 0)

require "Modules.ModuleMainUI.ChildComponent.StrengthenListItem"
local StrengthenListItem = ClassTypes.StrengthenListItem

function StrengthenPanel:Awake()
    self.disableCameraCulling = true
    self:InitCallback()
    self:InitViews()
end

function StrengthenPanel:InitCallback()
    self._updateData = function() self:UpdateData() end
    -- self._showStrength = function (shouldShow)
    --     self:ShowStrength(shouldShow)
    -- end
    self._strengthenListItemClick = function() self:OnCloseList() end   
end

function StrengthenPanel:InitViews()
    self._autoFightLight = self:FindChildGO("Container_Strengthen/Image_Guangxiao")
    self._autoFightLightRotation = self._autoFightLight:AddComponent(typeof(XGameObjectTweenRotation))
   
    self._strengthenListGo = self:FindChildGO("Container_List")

    self._strengthenButton = self:FindChildGO("Container_Strengthen/Button_BG")
    self._csBH:AddClick(self._strengthenButton, function() self:OnStrengthenButtonClick() end)

    self._shadeButton = self:FindChildGO("Container_List/Button_Shade")
    self._csBH:AddClick(self._shadeButton, function() self:OnCloseList() end)

    self._closeButton = self:FindChildGO("Container_List/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnCloseList() end)

    self:InitStrengthenList()
    self._strengthenListGo:SetActive(false)
end

function StrengthenPanel:InitStrengthenList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_List/ScrollViewList")
	self._strengthenList = list
	self._strengthenListlView = scrollView 
    self._strengthenListlView:SetHorizontalMove(false)
    self._strengthenListlView:SetVerticalMove(true)
    self._strengthenList:SetItemType(StrengthenListItem)
    self._strengthenList:SetPadding(0, 0, 0, 8)
    self._strengthenList:SetGap(8, 0)
    self._strengthenList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function StrengthenPanel:OnShow(data)
    self:UpdateData()
    self._autoFightLightRotation:SetFromToRotation(start, stop, 2, 2,nil)
end

function StrengthenPanel:SetData(data)
end

function StrengthenPanel:OnClose()
    self:RemoveEventListeners()
end

function StrengthenPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.STRENGTHEN_CHANGE, self._updateData)
    EventDispatcher:AddEventListener(GameEvents.STRENGTHEN_LIST_ITEM_CLICK, self._strengthenListItemClick)
end

function StrengthenPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.STRENGTHEN_CHANGE, self._updateData)
    EventDispatcher:RemoveEventListener(GameEvents.STRENGTHEN_LIST_ITEM_CLICK, self._strengthenListItemClick)
end

function StrengthenPanel:OnStrengthenButtonClick()
    -- StrengthenManager:PrintData() 
    self._strengthenListData = StrengthenManager:GetStrengthenData()
    if not table.isEmpty(self._strengthenListData) then
        self:AddEventListeners()
        self._strengthenListGo:SetActive(true)
        self:UpdateData()
    end
end

function StrengthenPanel:UpdateData()
    self._strengthenListData = StrengthenManager:GetStrengthenData()
    self._strengthenList:SetDataList(self._strengthenListData)
    self._strengthenList:SetPositionByNum(1)
    if #self._strengthenListData > 3 then        
        self._strengthenListlView:SetVerticalMove(true)
    else
        self._strengthenListlView:SetVerticalMove(false)
    end
end

function StrengthenPanel:OnCloseList()
    self:RemoveEventListeners()
    self._strengthenListGo:SetActive(false)
end