-- RemindMainUIPanel.lua
local RemindMainUIPanel = Class.RemindMainUIPanel(ClassTypes.BasePanel)


local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local SpecialRemindDataHelper = GameDataHelper.SpecialRemindDataHelper
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local BagData = PlayerManager.PlayerDataManager.bagData
local MainUIManager = PlayerManager.MainUIManager
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
require "Modules.ModuleMainUI.ChildComponent.RemindItem"
local RemindItem = ClassTypes.RemindItem

require "Modules.ModuleMainUI.ChildComponent.LimitRemindItem"
local LimitRemindItem = ClassTypes.LimitRemindItem

local RemindData = PlayerManager.PlayerDataManager.remindData
local RemindType = GameConfig.EnumType.RemindType

function RemindMainUIPanel:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    
    self._itemGo1 = self:FindChildGO("Container_item")
    self._txtTitle = self:GetChildComponent("Container_item/Text_Title", 'TextMeshWrapper')
    self._txtLeft = self:GetChildComponent("Container_item/Text_left", 'TextMeshWrapper')
    self._txtGo = self:GetChildComponent("Container_item/Text_Go", 'TextMeshWrapper')
    
    -- self._itemGo2 = self:FindChildGO("Container_Time/Container_item_2")
    -- self._txtTitleTwo = self:GetChildComponent("Container_Time/Container_item_1/Text_Title", 'TextMeshWrapper')
    -- self._txtLeftTwo = self:GetChildComponent("Container_Time/Container_item_1/Text_left", 'TextMeshWrapper')
    -- self._txtGoTwo = self:GetChildComponent("Container_Time/Container_item_1/Text_Go", 'TextMeshWrapper')
    
    self._btnItem1 = self:FindChildGO("Container_item/Button")
    self._csBH:AddClick(self._btnItem1, function()self:OnCloseClick() end)
    
    -- self._btnItem2 = self:FindChildGO("Container_Time/Container_item_2/Button")
    -- self._csBH:AddClick(self._btnItem2, function()self:OnCloseTwoClick() end)
    self:InitView()
    -- self._itemGo1:SetActive(false)
    -- self._itemGo2:SetActive(false)
    self:InitListenerFunc()
    self._menuItems = {}  
    self._limitMenuItems = {}
    self._fxItemPool={}

    -- for k,v in pairs(self._limitMenuItems) do
    --     self:ReleaseRemindItem(v)
    --     self._limitMenuItems[k]=nil
    -- end
end

function RemindMainUIPanel:InitListenerFunc()
    self._refreshList = function()self:RefreshRemindList() end
    self._refreshLimitList = function()self:RefreshLimitRemindList() end
end

function RemindMainUIPanel:OnCloseClick()
    self._itemGo1:SetActive(false)
end

function RemindMainUIPanel:OnCloseTwoClick()
    self._itemGo2:SetActive(false)
end

function RemindMainUIPanel:InitView()

end

function RemindMainUIPanel:RefreshData()
    self:RefreshRemindList()
end

function RemindMainUIPanel:ReleaseRemindItem(item)
    local itemPool = self._fxItemPool
    table.insert(itemPool, item)
end


--提醒列表 按钮对象池
function RemindMainUIPanel:CreateRemindItem()
    local itemPool = self._fxItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Button_Func", "Container_System")
    local item = UIComponentUtil.AddLuaUIComponent(go, RemindItem)
    return item
end

--限时提醒列表 按钮对象池
function RemindMainUIPanel:CreateLimitRemindItem()
    local go = self:CopyUIGameObject("Container_item", "Container_Time")
    local item = UIComponentUtil.AddLuaUIComponent(go, LimitRemindItem)
    return item
end



function RemindMainUIPanel:OnShow()
    self:AddEventListeners()
    self:RefreshData()
end


function RemindMainUIPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.RemindListChange, self._refreshList)
    EventDispatcher:AddEventListener(GameEvents.LimitRemindListChange, self._refreshLimitList)
end


function RemindMainUIPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.RemindListChange, self._refreshList)
    EventDispatcher:RemoveEventListener(GameEvents.LimitRemindListChange, self._refreshLimitList)
end



function RemindMainUIPanel:RefreshRemindList()
    local ids = ActivityDailyDataHelper:GetAllId()
    for i,id in ipairs(ids) do
        self:RemindChange(id)
    end
end


function RemindMainUIPanel:RefreshLimitRemindList()
    --LoggerHelper.Error("RefreshLimitRemindList()")
    local ids = ActivityDailyDataHelper:GetAllId()
    for i,id in ipairs(ids) do
       --动态图标
       local limitMap = RemindData:CheckLimitRemindOpen(id)
       if limitMap then
        if self._limitMenuItems[id] == nil then
            local item = self:CreateLimitRemindItem()
            self._limitMenuItems[id] = item
            local limitRemind = RemindData:GetLimitRemindList()
            item:SetData(id,ActivityDailyDataHelper:GetName(id),limitRemind[id],RemindData:GetLimitRemindEndTime(id))
            local endTime = RemindData:GetLimitRemindEndTime(id)
            --LoggerHelper.Error("RemindData:GetLimitRemindEndTime"..endTime)

            if endTime then
                local timeLeft = endTime - DateTimeUtil.GetServerTime()
                item:UpdateTimeLeft(timeLeft)
            end
        else
            local item = self._limitMenuItems[id]
            local endTime = RemindData:GetLimitRemindEndTime(id)
            if endTime then
                local timeLeft = endTime - DateTimeUtil.GetServerTime()
                item:UpdateTimeLeft(timeLeft)
            end
        end
       end
    end
end


function RemindMainUIPanel:RemindChange(remindType)
    
    if RemindData:GetRemindByType(remindType) then
        if not self._menuItems[remindType] then
            local item = self:CreateRemindItem()
            item:SetData(remindType, SpecialRemindDataHelper:GetIcon(remindType), LanguageDataHelper.CreateContent(SpecialRemindDataHelper:GetName(remindType)),RemindData:GetRemindPathByType(remindType))
            
            if SpecialRemindDataHelper:GetCircle(remindType) == 0 then
                item:CleanLight()
                item:StartScale()
            elseif SpecialRemindDataHelper:GetCircle(remindType) == 1 then
                local lightItem = self:CreateLightItem()
                item:SetLightAnimate(lightItem)
            elseif SpecialRemindDataHelper:GetCircle(remindType) == 2 then
                item:CleanLight()
                item:StartScale()
            end

            self._menuItems[remindType] = item
        else  
            self._menuItems[remindType]:SetActive(true)
            if SpecialRemindDataHelper:GetCircle(remindType) == 2 then
                self._menuItems[remindType]:StopScale()
                self._menuItems[remindType]:StartScale()
            end
        end
    else 
        if self._menuItems[remindType] then
            self._menuItems[remindType]:SetActive(false)
        end
    end

end

-- 菜单光效对象池
function RemindMainUIPanel:CreateLightItem()
    local go = self:CopyUIGameObject("Container_Light", "Container_Row")
    return go
end



function RemindMainUIPanel:OnClose()
    --LoggerHelper.Error("RemindMainUIPanel:OnClose()")
    self:RemoveEventListeners()
    for k,v in pairs(self._limitMenuItems) do
        self:ReleaseRemindItem(v)
        self._limitMenuItems[k]=nil
    end
end

function RemindMainUIPanel:ClearData()
    -- for k,v in pairs(self._limitMenuItems) do
    --     self:ReleaseRemindItem(v)
    --     self._limitMenuItems[k]=nil
    -- end
end




function RemindMainUIPanel:InitViews()

end
