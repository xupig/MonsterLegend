-- FunctionOpenTipsPanel.lua
local ClassTypes = ClassTypes
local FunctionOpenTipsPanel = Class.FunctionOpenTipsPanel(ClassTypes.BasePanel)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local GUIManager = GameManager.GUIManager
local FunctionOpenManager = PlayerManager.FunctionOpenManager
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local MenuConfig = GameConfig.MenuConfig
local EquipModelComponent = GameMain.EquipModelComponent
local ActorModelComponent = GameMain.ActorModelComponent
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TransformDataHelper = GameDataHelper.TransformDataHelper
local UIParticle = ClassTypes.UIParticle

function FunctionOpenTipsPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self._bgImg = self:FindChildGO("Image_BG")
    self._bgButtonTips = self:FindChildGO("Button_Touch")
    self._csBH:AddClick(self._bgButtonTips, function()self:OnBGButtonTipsClick() end)

    self:InitOpenTips()
    self:InitOpenModel()
    --self:InitAnimation()

    self._modelFuncMap = {
        [public_config.FUNCTION_ID_WING]      = public_config.TREASURE_TYPE_WING,
        [public_config.FUNCTION_ID_TALISMAN]  = public_config.TREASURE_TYPE_TALISMAN,
        [public_config.FUNCTION_ID_WEAPON]    = public_config.TREASURE_TYPE_WEAPON,
        [public_config.FUNCTION_ID_CLOAK]     = public_config.TREASURE_TYPE_CLOAK,
        [public_config.FUNCTION_ID_PET]       = public_config.TREASURE_TYPE_PET,
        [public_config.FUNCTION_ID_HORSE]     = public_config.TREASURE_TYPE_HORSE
    }

    self._txtModelNameMap = {
        [public_config.TREASURE_TYPE_WING]      = 4623,
        [public_config.TREASURE_TYPE_TALISMAN]  = 4624,
        [public_config.TREASURE_TYPE_WEAPON]    = 4625,
        [public_config.TREASURE_TYPE_CLOAK]     = 4626
    }
end

function FunctionOpenTipsPanel:OnDestroy()
    self._csBH = nil
end

function FunctionOpenTipsPanel:OnShow(data)
    self.showType = data[1]
    self.funId = data[2]
    self:RefreshView()
    if self.showType == 1 then
        GameWorld.PlaySound(12)
        EventDispatcher:TriggerEvent(GameEvents.FUNCITON_OPEN_START)
    end
end

function FunctionOpenTipsPanel:OnClose()
    if self._timerId then
        TimerHeap:DelTimer(self._timerId)
        self._timerId = nil
    end
end

function FunctionOpenTipsPanel:InitOpenTips()
    self._containerOpenTips = self:FindChildGO("Container_OpenTips")
    
    --开启UI
    self._containerOpenTipsOpen = self:FindChildGO("Container_OpenTips/Container_Open")
    self._infoText = self:GetChildComponent("Container_OpenTips/Container_Open/Container_Info/Text_Info", "TextMeshWrapper")

    self._iconContainerTips = self:FindChildGO("Container_OpenTips/Container_Open/Image_IconBg/Container_Icon")

    self._fxTips = UIParticle.AddParticle(self.gameObject,"Container_OpenTips/Container_Open/Image_IconBg/Container_Fx/fx_ui_30038")

    --预览UI
    self._containerOpenTipsPreview = self:FindChildGO("Container_OpenTips/Container_Preview")
    self._containerOpenTipsPreviewIcon = self:FindChildGO("Container_OpenTips/Container_Preview/Container_Icon")
    self._txtTipsPreviewName = self:GetChildComponent("Container_OpenTips/Container_Preview/Text_Name", "TextMeshWrapper")
    self._txtTipsPreviewOpen = self:GetChildComponent("Container_OpenTips/Container_Preview/Text_Open", "TextMeshWrapper")
    self._txtTipsPreviewInfo = self:GetChildComponent("Container_OpenTips/Container_Preview/Text_Info", "TextMeshWrapper")
end

function FunctionOpenTipsPanel:InitOpenModel()
   self._containerOpenModel = self:FindChildGO("Container_OpenModel")
   
   self._containerActorModel = self:FindChildGO("Container_OpenModel/Container_ActorModel")
   self._actorModelComponent = self:AddChildComponent('Container_OpenModel/Container_ActorModel', ActorModelComponent)
   self._actorModelComponent:SetStartSetting(Vector3(0, 0, -150), Vector3(0, 180, 0), 1)

   self._containerTreasureModel = self:FindChildGO("Container_OpenModel/Container_TreasureModel")
   self._treaureModelComponent = self:AddChildComponent('Container_OpenModel/Container_TreasureModel', EquipModelComponent)
   self._modelInfoText = self:GetChildComponent("Container_OpenModel/Text_Info", "TextMeshWrapper")

   --开启UI
   self._containerOpenModelOpen = self:FindChildGO("Container_OpenModel/Container_Open")

   self._imgTitles = {}
   self._imgTitles[public_config.TREASURE_TYPE_WING] = self:FindChildGO("Container_OpenModel/Container_Open/Container_TitleImage/Image_Title1")
   self._imgTitles[public_config.TREASURE_TYPE_TALISMAN] = self:FindChildGO("Container_OpenModel/Container_Open/Container_TitleImage/Image_Title2")
   self._imgTitles[public_config.TREASURE_TYPE_WEAPON] = self:FindChildGO("Container_OpenModel/Container_Open/Container_TitleImage/Image_Title3")
   self._imgTitles[public_config.TREASURE_TYPE_PET] = self:FindChildGO("Container_OpenModel/Container_Open/Container_TitleImage/Image_Title5")
   self._imgTitles[public_config.TREASURE_TYPE_HORSE] = self:FindChildGO("Container_OpenModel/Container_Open/Container_TitleImage/Image_Title6")

   self._txtModelOpenName = self:GetChildComponent("Container_OpenModel/Container_Open/Text_Name", "TextMeshWrapper")
   
   self._fxModelTips = UIParticle.AddParticle(self.gameObject,"Container_OpenModel/Container_Open/Container_Fx/fx_ui_30035")

   --预览UI
   self._containerOpenModelPreview = self:FindChildGO("Container_OpenModel/Container_Preview")
   self._txtModelPreviewName = self:GetChildComponent("Container_OpenModel/Container_Preview/Text_Name", "TextMeshWrapper")
   self._txtModelPreviewOpen = self:GetChildComponent("Container_OpenModel/Container_Preview/Text_Open", "TextMeshWrapper")
end

-- function FunctionOpenTipsPanel:InitAnimation()
--     self._isOpen = true
--     self._containerAnim = self:FindChildGO("Container_OpenTips/Container_Anim")
--     self._tweenPosition = self._containerAnim:AddComponent(typeof(XGameObjectTweenPosition))

--     local canvasWidth = GUIManager.canvasWidth
--     local canvasHeight = GUIManager.canvasHeight
--     self._offsetX = (canvasWidth - 1280)/2
--     self._offsetY = (canvasHeight - 720)/2
-- end

function FunctionOpenTipsPanel:OnBGButtonTipsClick()
    if self.showType == 1 then
        self._hasClick = true
        
        if self._isShowModel then
            GUIManager.ClosePanel(PanelsConfig.FunctionOpenTips) 
        else
            GUIManager.ClosePanel(PanelsConfig.FunctionOpenTips) 
            --self:StartAnimate()
        end
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_function_open_animate_complete")
        EventDispatcher:TriggerEvent(GameEvents.FUNCITON_OPEN_END)
        FunctionOpenManager:CheckShowOpenTips()
    else
        GUIManager.ClosePanel(PanelsConfig.FunctionOpenTips) 
    end
end

--按普通提示显示
function FunctionOpenTipsPanel:UpdateOpenTip()
    --self._containerAnim.transform.localPosition = Vector3.New(640, 414, 0)
    if self.showType == 1 then
        self._bgImg:SetActive(false)
        self._containerOpenTipsPreview:SetActive(false)
        self._containerOpenTipsOpen:SetActive(true)
        self._infoText.text = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetPreviewText(self.funId))
        local iconId = FunctionOpenDataHelper:GetFunctionOpenIcon(self.funId)
        GameWorld.AddIcon(self._iconContainerTips, iconId)
        self._fxTips:Play(true,true)
    else
        self._bgImg:SetActive(true)
        self._containerOpenTipsPreview:SetActive(true)
        self._containerOpenTipsOpen:SetActive(false)
        self._txtTipsPreviewInfo.text = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetPreviewText(self.funId))
        self._txtTipsPreviewName.text = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetFunctionOpenName(self.funId))
        local preview_level = FunctionOpenDataHelper:GetPreviewLevel(self.funId)
        self._txtTipsPreviewOpen.text = LanguageDataHelper.CreateContentWithArgs(179, {["0"] = preview_level[1]})
        local iconShowId = FunctionOpenDataHelper:GetFunctionOpenIconShow(self.funId)
        if iconShowId then
            GameWorld.AddIcon(self._containerOpenTipsPreviewIcon, iconShowId)
        end
        self._fxTips:Stop()
    end
end

--按模型显示
function FunctionOpenTipsPanel:UpdateOpenModel()
    local modelType = self._modelFuncMap[self.funId]
    
    self._bgImg:SetActive(true)
    if self.showType == 1 then
        self._containerOpenModelPreview:SetActive(false)
        self._containerOpenModelOpen:SetActive(true)
        self._fxModelTips:Play(true,true)
    else
        self._containerOpenModelPreview:SetActive(true)
        self._containerOpenModelOpen:SetActive(false)
        self._fxModelTips:Stop()
    end

    local name
    if modelType < 5 then
        self._containerActorModel:SetActive(false)
        self._containerTreasureModel:SetActive(true)

        local vocationGroup = math.floor(GameWorld.Player().vocation/100)
        local uiModelCfgId = GlobalParamsHelper.GetParamValue(703+modelType)[vocationGroup]
        local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
        
        local modelId = modelInfo.model_ui
        local posx = modelInfo.pos[1]
        local posy = modelInfo.pos[2]
        local posz = modelInfo.pos[3]
        local rotationx = modelInfo.rotation[1]
        local rotationy = modelInfo.rotation[2]
        local rotationz = modelInfo.rotation[3]
        local scale = modelInfo.scale
        self._treaureModelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
        
        --神兵自转
        if modelType == public_config.TREASURE_TYPE_WEAPON then
            --self._modelComponent:SetSelfRotate(true)
        end

        if modelType == public_config.TREASURE_TYPE_TALISMAN then
            --法宝替换controller
            local controllerPath = GlobalParamsHelper.GetParamValue(692)
            self._treaureModelComponent:LoadEquipModel(modelId, controllerPath)
        else
            self._treaureModelComponent:LoadEquipModel(modelId, "")
        end

        name = self._txtModelNameMap[modelType]
    else
        self._containerActorModel:SetActive(true)
        self._containerTreasureModel:SetActive(false)
        self._selectedGrade = 1
        if modelType == public_config.TREASURE_TYPE_PET then
            local scale = PartnerDataHelper.GetPetScale(self._selectedGrade)
            local y = PartnerDataHelper.GetPetRotate(self._selectedGrade)
            self._actorModelComponent:SetStartSetting(Vector3(0, 0, -150), Vector3(0, y, 0), scale)
            local modelId = PartnerDataHelper.GetPetModel(self._selectedGrade)
            self._actorModelComponent:LoadModel(modelId)

            name = PartnerDataHelper.GetPetName(self._selectedGrade)
        else
            local scale = PartnerDataHelper.GetRideScale(self._selectedGrade)
            self._actorModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 90, 0), scale)
            local modelId = PartnerDataHelper.GetRideModel(self._selectedGrade)
            self._actorModelComponent:LoadModel(modelId)

            name = PartnerDataHelper.GetHorseName(self._selectedGrade)
        end
    end

    self._modelInfoText.text = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetPreviewText(self.funId))
    if self.showType == 1 then
        for k,v in pairs(self._imgTitles) do
            if k == modelType then
                v:SetActive(true)
            else
                v:SetActive(false)
            end
        end
        self._txtModelOpenName.text = LanguageDataHelper.CreateContent(name) 
    else
        self._txtModelPreviewName.text = LanguageDataHelper.CreateContent(name)
        local preview_level = FunctionOpenDataHelper:GetPreviewLevel(self.funId)
        self._txtModelPreviewOpen.text = LanguageDataHelper.CreateContentWithArgs(179, {["0"] = preview_level[1]})
    end
end

function FunctionOpenTipsPanel:RefreshView()
    if self._modelFuncMap[self.funId] then
        self._isShowModel = true
        self._containerOpenTips:SetActive(false)
        self._containerOpenModel:SetActive(true)
        self:UpdateOpenModel()
    else
        self._isShowModel = false
        self._containerOpenTips:SetActive(true)
        self._containerOpenModel:SetActive(false)
        self:UpdateOpenTip()
    end

    --功能开启5秒后自动关闭
    if self.showType == 1 then
        self._hasClick = false
        self._timerId = TimerHeap:AddSecTimer(5,0,5 ,function ()
            TimerHeap:DelTimer(self._timerId)
            self._timerId = nil
            if self._hasClick == false then
                self:OnBGButtonTipsClick()
            end
        end)
    end
end

-------------------------------------------------动画(暂时不用)-------------------------------------------------------------

local HeadVec3 = Vector3.New(64, 666, 0)
local RowBigVec = Vector3.New(1042, 685, 0)
local RowVecs = {Vector3.New(1042, 685, 0),Vector3.New(1042, 613, 0),Vector3.New(1235, 523, 0)}
local RowDX = 74
local RowBigX = 90
function FunctionOpenTipsPanel:StartAnimate()
    self._containerInfo:SetActive(false)
    self._bgButtonTips:SetActive(false)
    self._bgImg:SetActive(false)
    --LoggerHelper.Error("FunctionOpenTipsPanel:StartAnimate()")
    --LoggerHelper.Log(self.funId)
    local menuType,row,index,bigCount = FunctionOpenDataHelper:GetFunctionPanelIconPosition(self.funId)
    local resultVec = Vector3.New(0, 0, 0)  
    local direction = 1
    --次级菜单
    if menuType == MenuConfig.SUB_MENU then
        resultVec = HeadVec3
        direction = -1
    --主菜单
    elseif menuType == MenuConfig.MAIN_MENU then
        --小图标
        if row > 0 then
            local vec = RowVecs[row]
            resultVec.x = vec.x - RowDX*index - bigCount*RowBigX
            resultVec.y = vec.y
        --大图标
        else
            local vec = RowBigVec
            resultVec.x = vec.x - RowBigX*index
            resultVec.y = vec.y
        end
    else
        --其他的情况直接结束
        self:EndAnimate()
        return
    end

    resultVec.x = resultVec.x + self._offsetX *direction
    resultVec.y = resultVec.y + self._offsetY

    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetToPosOnce(resultVec, 1.5, nil)
    self._animTimer = TimerHeap:AddSecTimer(1.6,0,0,function ()
        self:EndAnimate()
    end)
end

function FunctionOpenTipsPanel:EndAnimate()
    TimerHeap:DelTimer(self._animTimer)
    GUIManager.ClosePanel(PanelsConfig.FunctionOpenTips)
    self._containerInfo:SetActive(true)
    self._bgButtonTips:SetActive(true)
    self._bgImg:SetActive(true)
    self._containerAnim.transform.localPosition = Vector3.New(640, 418, 0)
end