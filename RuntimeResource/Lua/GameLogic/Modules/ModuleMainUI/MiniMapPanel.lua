-- StickPanel.lua
local MiniMapPanel = Class.MiniMapPanel(ClassTypes.BasePanel)
require "Modules.ModuleWorldMap.ChildView.DistristMapView"
local DistristMapView = ClassTypes.DistristMapView

local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local MapManager = PlayerManager.MapManager
local GameSceneManager = GameManager.GameSceneManager
local MapDataHelper = GameDataHelper.MapDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local Vector2 = Vector2
local Vector3 = Vector3
local SceneConfig = GameConfig.SceneConfig

function MiniMapPanel:Awake()
    self._textMapName = self:GetChildComponent("Container_Title/Container_Name/Text_Name", "TextMeshWrapper")
    self._btnMap = self:FindChildGO("Container_Map")
    self._containerDistrictMap = self:FindChildGO("Container_Map/Mask/Container_DistrictMap").transform
    self._districtMapView = self:AddChildLuaUIComponent("Container_Map/Mask/Container_DistrictMap", DistristMapView)
    self._csBH:AddClick(self._btnMap, function()self:BtnMapClick() end)
    -- self._btnWildEvent = self:FindChildGO("Container_Title/Container_Weather/Button_WildEvent")
    -- self._csBH:AddClick(self._btnWildEvent, function()self:BtnWildEventClick() end)
    self._mask = self:FindChildGO("Container_Map/Mask")
    local tranMask = self._mask:GetComponent(typeof(UnityEngine.RectTransform))
    self._paddingX = tranMask.sizeDelta.x / 2
    self._paddingY = tranMask.sizeDelta.y / 2
    self._screenWidth = tranMask.sizeDelta.x
    self._screenHeight = tranMask.sizeDelta.y
    self:InitCallbackFunc()
    self._districtMapView:SetUpdatePlayerInDistristCallback(self._onUpdateMapPos)
    --LoggerHelper.Log("Ash: MiniMapPanel:" .. self._paddingX .. " " .. self._paddingY)
    
    -- self._btnToDamageStatistics = self:FindChildGO("Button_ToDamageStatistics")
    -- self._csBH:AddClick(self._btnToDamageStatistics, function() self:OnClickToDamageStatistics() end )
end

function MiniMapPanel:InitCallbackFunc()
    self._onUpdateMapPos = function()self:UpdateMapPos() end
    self._onMyTeamUpdate = function()self:UpdateTeamList() end
    self._onTaskChanged = function()self:UpdateTask() end
    self._onMonsterEnterWorld = function(monsterId, entityId)self:OnMonsterEnterWorld(monsterId, entityId) end
    self._onMonsterLeaveWorld = function(monsterId, entityId)self:OnMonsterLeaveWorld(monsterId, entityId) end
    self._onAvatarEnterWorld = function(entityId)self:OnAvatarEnterWorld(entityId) end
    self._onAvatarLeaveWorld = function(entityId)self:OnAvatarLeaveWorld(entityId) end
    -- self._onClickFlagEnterWorld = function(entityId)self:OnClickFlagEnterWorld(entityId) end
    -- self._onClickFlagLeaveWorld = function(entityId)self:OnClickFlagLeaveWorld(entityId) end
end

function MiniMapPanel:OnShow(data)
    local mapId = GameSceneManager:GetCurrMapID()
    -- LoggerHelper.Log("Ash: MiniMapPanel OnShow: " .. tostring(mapId))
    self._textMapName.text = LanguageDataHelper.GetLanguageData(MapDataHelper.GetChinese(mapId) or 0)
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_PVP6
        or sceneType == SceneConfig.SCENE_TYPE_PVP_NEW
        or sceneType == SceneConfig.SCENE_TYPE_FIGHT_FRIEND
        or sceneType == SceneConfig.SCENE_TYPE_LADDER_1V1
    then
        self:ShowPVPMapInfo(mapId)
    elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then
        self:ShowGuildClickFlagMap(mapId)
    else
        self:ShowNormalMap(mapId)
    end
end

function MiniMapPanel:OnClose()
    if self._isNormalMap then
        self:CloseNormalMap()
    else
        self:ClosePVPMap()
    end
end

function MiniMapPanel:OnDestroy()
    self._btnMap = nil
    self._districtMapView = nil
end


function MiniMapPanel:ShowNormalMap(mapId)
    self._isNormalMap = true
    self:ShowCurrMap(mapId)
    self:UpdateMapPos()
    EventDispatcher:AddEventListener(GameEvents.TEAM_MY_UPDATE, self._onMyTeamUpdate)
    EventDispatcher:AddEventListener(GameEvents.TEAM_MEMBER_MAPINFO_UPDATE, self._onMyTeamUpdate)
    -- EventDispatcher:AddEventListener(GameEvents.OnTaskChanged, self._onTaskChanged)
    EventDispatcher:AddEventListener(GameEvents.OnMonsterEnterWorld, self._onMonsterEnterWorld)
    EventDispatcher:AddEventListener(GameEvents.OnMonsterLeaveWorld, self._onMonsterLeaveWorld)
end

function MiniMapPanel:CloseNormalMap()
    EventDispatcher:RemoveEventListener(GameEvents.TEAM_MY_UPDATE, self._onMyTeamUpdate)
    EventDispatcher:RemoveEventListener(GameEvents.TEAM_MEMBER_MAPINFO_UPDATE, self._onMyTeamUpdate)
    -- EventDispatcher:RemoveEventListener(GameEvents.OnTaskChanged, self._onTaskChanged)
    EventDispatcher:RemoveEventListener(GameEvents.OnMonsterEnterWorld, self._onMonsterEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnMonsterLeaveWorld, self._onMonsterLeaveWorld)
    self._districtMapView:StopUpdateDistrist()
end

function MiniMapPanel:ShowPVPMapInfo(mapId)
    -- LoggerHelper.Log("Ash: MiniMapPanel ShowPVPMapInfo: " .. tostring(mapId))
    self._isNormalMap = false
    self:ShowPVPMap(mapId)
    self:UpdateMapPos()
    EventDispatcher:AddEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
    EventDispatcher:AddEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
end

function MiniMapPanel:ClosePVPMap()
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
    self._districtMapView:StopUpdatePVPDistrist()
end

function MiniMapPanel:ShowGuildClickFlagMap(mapId)
    -- LoggerHelper.Log("Ash: MiniMapPanel ShowPVPMap: ")
    self._isNormalMap = false
    self:ShowGuildClickFlag(mapId)
    EventDispatcher:AddEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
    EventDispatcher:AddEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
    -- EventDispatcher:AddEventListener(GameEvents.OnClickFlagEnterWorld, self._onClickFlagEnterWorld)
    -- EventDispatcher:AddEventListener(GameEvents.OnClickFlagLeaveWorld, self._onClickFlagLeaveWorld)
end

function MiniMapPanel:CloseGuildClickFlagMap()
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
    -- EventDispatcher:RemoveEventListener(GameEvents.OnClickFlagEnterWorld, self._onClickFlagEnterWorld)
    -- EventDispatcher:RemoveEventListener(GameEvents.OnClickFlagLeaveWorld, self._onClickFlagLeaveWorld)
    self._districtMapView:StopUpdatePVPDistrist()
end

function MiniMapPanel:UpdateTeamList()
    -- LoggerHelper.Log("Ash: MiniMapPanel UpdateTeamList: " .. tostring(self._isWorldMap))
    self._districtMapView:ShowPlayer(GameSceneManager:GetCurrMapID())
end

function MiniMapPanel:UpdateTask()
    -- self._districtMapView:ShowTask(GameSceneManager:GetCurrMapID())
end

function MiniMapPanel:OnMonsterEnterWorld(monsterId, entityId)
    local ety = GameWorld.GetEntity(entityId)
    self._districtMapView:AddMonsterByEty(ety, GameWorld.Player())
end

function MiniMapPanel:OnMonsterLeaveWorld(monsterId, entityId)
    self._districtMapView:RemoveMonsterByEtyId(entityId)
end

function MiniMapPanel:OnAvatarEnterWorld(entityId)
    -- LoggerHelper.Log("Ash: OnAvatarEnterWorld:" .. tostring(entityId))
    local ety = GameWorld.GetEntity(entityId)
    self._districtMapView:AddAvatarByEty(ety)
end

function MiniMapPanel:OnAvatarLeaveWorld(entityId)
    -- LoggerHelper.Log("Ash: OnAvatarLeaveWorld:" .. tostring(entityId))
    self._districtMapView:RemoveAvatarByEtyId(entityId)
end

-- function MiniMapPanel:OnClickFlagEnterWorld(entityId)
--     local ety = GameWorld.GetEntity(entityId)
--     self._districtMapView:AddGuildClickFlagByEty(ety)
-- end

-- function MiniMapPanel:OnClickFlagLeaveWorld(entityId)
--     self._districtMapView:RemoveGuildClickFlagByEtyId(entityId)
-- end

function MiniMapPanel:ShowCurrMap(mapId)
    -- LoggerHelper.Log("Ash: DistrictBtnClick:" .. tostring(mapId))
    local worldMapId, curMapRegInfo = MapManager:GetRegionalInfoByMapId(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    local isWild = false
    if sceneType == SceneConfig.SCENE_TYPE_MAIN or sceneType == SceneConfig.SCENE_TYPE_WILD or SceneConfig.IsBossScene(sceneType) then
        isWild = true
    end
   self._districtMapView:ShowDistrictInfo(mapId, worldMapId, curMapRegInfo, false, isWild)
end

function MiniMapPanel:ShowPVPMap(mapId)
    -- LoggerHelper.Log("Ash: DistrictBtnClick:" .. tostring(mapId))
    local worldMapId, curMapRegInfo = MapManager:GetRegionalInfoByMapId(mapId)
    self._districtMapView:ShowPVPInfo(mapId, worldMapId, curMapRegInfo, false)
end

function MiniMapPanel:ShowGuildClickFlag(mapId)
    -- LoggerHelper.Log("Ash: DistrictBtnClick ShowGuildClickFlag:" .. tostring(mapId))
    local worldMapId, curMapRegInfo = MapManager:GetRegionalInfoByMapId(mapId)
    self._districtMapView:ShowGuildClickFlagInfo(mapId, worldMapId, curMapRegInfo, false)
end

function MiniMapPanel:UpdateMapPos()
    if self._isNormalMap then
        self._districtMapView:UpdateMonster()
    end
    if self._districtMapView:HasLoadScale() ~= true then
        LoggerHelper.Error("Ash: UpdateMapPos has not load scale.")
        return
    end
    local playerPos = self._districtMapView:GetPlayerPos()
    if playerPos == nil or (self._lastPos ~= nil and Vector2.Distance(self._lastPos, playerPos) < 1) then
        return
    end
    -- LoggerHelper.Log("Ash: playerPos:" .. PrintTable:TableToStr(self.playerPos))
    local screenPoint = Vector2(playerPos.x - self._paddingX, playerPos.y - self._paddingY)
    local innerScreenPoint = Vector2(screenPoint.x + 12.5, screenPoint.y + 12.5)
    local innerScreenWidth = self._screenWidth - 25
    local innerScreenHeight = self._screenHeight - 25
    local rectPoint = {}
    table.insert(rectPoint, innerScreenPoint)
    table.insert(rectPoint, Vector2(innerScreenPoint.x + innerScreenWidth, innerScreenPoint.y))
    table.insert(rectPoint, Vector2(innerScreenPoint.x + innerScreenWidth, innerScreenPoint.y + innerScreenHeight))
    table.insert(rectPoint, Vector2(innerScreenPoint.x, innerScreenPoint.y + innerScreenHeight))
    table.insert(rectPoint, innerScreenPoint)
    
    if self._isNormalMap then
        self._districtMapView:UpdateAllItemsInMiniMap(innerScreenPoint.x, innerScreenPoint.y, innerScreenWidth, innerScreenHeight, rectPoint)
    end
    local mapPos = Vector3(-screenPoint.x, -screenPoint.y, 0)
    self._containerDistrictMap.localPosition = mapPos
    -- LoggerHelper.Log("Ash: _containerDistrictMap:" .. PrintTable:TableToStr(temp))
    self._lastPos = playerPos
end

function MiniMapPanel:BtnMapClick()
    GUIManager.ShowPanel(PanelsConfig.WorldMap, nil, nil)
end
