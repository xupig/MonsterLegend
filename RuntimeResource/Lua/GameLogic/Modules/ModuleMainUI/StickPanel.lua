-- StickPanel.lua
local ClassTypes = ClassTypes
local StickPanel = Class.StickPanel(ClassTypes.BasePanel)

local ComponentsConfig = GameConfig.ComponentsConfig

require "Modules.ModuleMainUI.ChildView.StickView"
local StickView = ClassTypes.StickView

local PartnerManager = PlayerManager.PartnerManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local public_config = GameWorld.public_config
local MapDataHelper = GameDataHelper.MapDataHelper
local GameSceneManager = GameManager.GameSceneManager
local CollectManager = GameManager.CollectManager

function StickPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel');
    self._stickView = self:AddChildLuaUIComponent("Container_wheel", StickView)
    --self:PositionFrom(self.transform.localPosition+Vector3.New(-300,0,0),5):Play(true)

    --坐骑上下马
    self._btnOnRide = self:FindChildGO("Button_OnRide")
    self._csBH:AddClick(self._btnOnRide, function() self:OnRideClick() end)

    self._btnOffRide = self:FindChildGO("Button_OffRide")
    self._csBH:AddClick(self._btnOffRide, function() self:OffRideClick() end)

    --坐骑功能开启
    self._updateRideCb = function ()
        self:UpdateRideState()
    end
    
    self._checkHorseOpenCb = function (id)
    	if id == public_config.FUNCTION_ID_HORSE then
    		self:UpdateRideState()
    	end
    end
end

function StickPanel:OnDestroy() 
    self._csBH = nil
end

function StickPanel:OnShow()
    self._stickView:ResetControlStick()
    self:InitRideState()
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_HORSE_RIDE_MODEL, self._updateRideCb)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkHorseOpenCb)
end

function StickPanel:OnClose()
    self._stickView:ResetControlStick()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_HORSE_RIDE_MODEL, self._updateRideCb)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkHorseOpenCb)
end

--坐骑上下马
function StickPanel:OnRideClick()
    if GameSceneManager:CanRide() then
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_on_ride_button")
        PartnerManager:HorseRideReq()
        EventDispatcher:TriggerEvent(GameEvents.ON_BREAK_COLLECT)
    end
end

function StickPanel:OffRideClick()
    PartnerManager:RideHideReq()
end

function StickPanel:InitRideState()
	if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_HORSE) then
		self:UpdateRideState()
	else
		self._btnOffRide:SetActive(false)
        self._btnOnRide:SetActive(false)
	end
end

function StickPanel:UpdateRideState()
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_HORSE) then
        return
    end
    local rideModel = GameWorld.Player().horse_ride_model
    if rideModel and rideModel > 0 then
        self._btnOffRide:SetActive(true)
        self._btnOnRide:SetActive(false)
    else
        self._btnOffRide:SetActive(false)
        self._btnOnRide:SetActive(true)
    end
end