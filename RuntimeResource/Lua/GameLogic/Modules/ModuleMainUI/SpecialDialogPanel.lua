-- SpecialDialogPanel.lua
local SpecialDialogPanel = Class.SpecialDialogPanel(ClassTypes.BasePanel)

local RectTransform = UnityEngine.RectTransform
local UIComponentUtil = GameUtil.UIComponentUtil
local SpecialDialogManager = PlayerManager.SpecialDialogManager
local SpecialDialogDataHelper = GameDataHelper.SpecialDialogDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ChineseVoiceDataHelper = GameDataHelper.ChineseVoiceDataHelper
local Vector3 = Vector3
local ActorModelComponent = GameMain.ActorModelComponent
local LuaUIRawImage = GameMain.LuaUIRawImage

function SpecialDialogPanel:Awake()
    self._textContent = self:GetChildComponent("Container_Dialog/Container_Content/Text_Content", "TextMeshWrapper")
    self._imageBG = self:FindChildGO("Container_Dialog/Image_BG")
    self._headGO = self:FindChildGO("Container_Dialog/Container_Head/RawImage")
    self._containerDialog = self:FindChildGO("Container_Dialog")
    self._containerMini = self:FindChildGO("Container_Mini")
    self._containerName = self:FindChildGO("Container_Dialog/Container_Name")
    self._imageVoice = self:FindChildGO("Container_Dialog/Container_Name/Image_Voice")
    self._textName = self:GetChildComponent("Container_Dialog/Container_Name/Text_Name", "TextMeshWrapper")
    self._csBH:AddClick(self._imageBG, function()self:OnClickImageBG() end)
    self:CreateHeadRttCamera()
    self:InitAvatar()
end

function SpecialDialogPanel:OnDestroy()
    self._textName = nil
    self._textContent = nil
    self._imageBG = nil
    self._headGO = nil
    self._imageVoice = nil
    self._containerDialog = nil
    self._containerMini = nil
    self._containerName = nil
end

function SpecialDialogPanel:OnShow(data)
    local npcId = SpecialDialogDataHelper:GetNpcId(data)
    local npcName = NPCDataHelper.GetNPCName(npcId)
    local npcModel = NPCDataHelper.GetModel(npcId)
    local dialogId = SpecialDialogDataHelper:GetDialog(data)
    local dialog = LanguageDataHelper.CreateContent(dialogId, LanguageDataHelper.GetArgsTable())
    self._textName.text = npcName
    self._imageVoice.transform.localPosition = Vector3.New(self._textName.preferredWidth + 3, 0, 0)
    self._textContent.text = dialog
    self._containerDialog:SetActive(true)
    self._containerName.transform:SetParent(self._containerDialog.transform, true)
    self._containerName.transform.localPosition = Vector3.New(120, 70, 0)
    self:UpdateModel(npcModel)
    local voice = ChineseVoiceDataHelper:GetVoice(dialogId)
    if voice ~= nil and voice ~= 0 then
        GameWorld.PlaySound(voice)
    end
end

function SpecialDialogPanel:OnClose()
    self._modelComponent:ClearModelGameObject()
end

function SpecialDialogPanel:OnClickImageBG()
    -- LoggerHelper.Log("Ash: OnClickImageBG: ")
    self._containerName.transform:SetParent(self._containerMini.transform, true)
    self._containerName.transform.localPosition = Vector3.New(0, 0, 0)
    self._containerDialog:SetActive(false)
end

local Camera = UnityEngine.Camera
local CameraClearFlags = UnityEngine.CameraClearFlags
local GameObject = UnityEngine.GameObject
local RenderTexture = UnityEngine.RenderTexture
local RenderingPath = UnityEngine.RenderingPath

function SpecialDialogPanel:CreateHeadRttCamera()
    self.rttForHead = GameObject.New("RttForEudemonHead")
    self.rttForHead.transform:SetParent(self.transform)
    self.RenderCameraHead = self.rttForHead:AddComponent(typeof(Camera))
    -- self.RenderCameraHead.renderingPath = RenderingPath.Forward--DeferredLighting --.DeferredShading
    self.RenderTextureHead = RenderTexture.New(100, 100, 16)
    self.RenderTextureHead.name = "SpecialDialog"
    self.RenderCameraHead.targetTexture = self.RenderTextureHead
    self.RenderCameraHead.clearFlags = CameraClearFlags.SolidColor;
    self.RenderCameraHead.backgroundColor = UnityEngine.Color.New(46 / 255, 64 / 255, 83 / 255, 0 / 255)
    self.RenderCameraHead.fieldOfView = 20
    self.RenderCameraHead.cullingMask = math.pow(2, LayerMask.NameToLayer("UI"))
    self.RenderCameraHead.transform.localPosition = Vector3.New(-1590, -1275, -112)
    self.rawImage = self._headGO:AddComponent(typeof(LuaUIRawImage))
    self.rawImage.texture = self.RenderTextureHead
end


function SpecialDialogPanel:InitAvatar()
    self._modelComponent = self:AddChildComponent('Container_Model', ActorModelComponent)
    self._modelComponent:SetStartSetting(Vector3(-1000, -1000, 0), Vector3(0, 180, 0), 1)
end

function SpecialDialogPanel:UpdateModel(modelId)
    self._modelComponent:LoadModel(modelId)
end
