local TeamQuickView = Class.TeamQuickView(ClassTypes.BaseLuaUIComponent)

TeamQuickView.interface = GameConfig.ComponentsConfig.Component
TeamQuickView.classPath = "Modules.ModuleMainUI.ChildView.TeamQuickView"

local TeamData = PlayerManager.PlayerDataManager.teamData
local TeamManager = PlayerManager.TeamManager

require "Modules.ModuleMainUI.ChildComponent.TeamQuickMemberItem"
local TeamQuickMemberItem = ClassTypes.TeamQuickMemberItem
local GUIManager = GameManager.GUIManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function TeamQuickView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	--self.shouldShowTeamPanel = false --是否应该弹出组队面板
	self:InitComps()
	self:InitCallback()
end

function TeamQuickView:OnDestroy()
	self._csBH = nil
	self._btnCreateTeam = nil
	self._btnFindTeam = nil
end

--更新成员列表
function TeamQuickView:InitComps()
	self._containerMyTeam = self:FindChildGO("Container_MyTeam")

	self._list = {}
	for i=1,3 do
		self._list[i] = self:AddChildLuaUIComponent("Container_MyTeam/Container_MemberList/item"..i, TeamQuickMemberItem)
	end

	self._rectMemberCountBG = self:FindChildGO("Container_MyTeam/Container_Exp/Image_MemberCount"):GetComponent(typeof(UnityEngine.RectTransform))
	self._rectMemberCountBGImage = self:FindChildGO("Container_MyTeam/Container_Exp/Image_MemberCount"):GetComponent("ImageWrapper")

    self._txtExpRate = self:GetChildComponent("Container_MyTeam/Container_Exp/Text_Exp","TextMeshWrapper")

	self._containerButtons = self:FindChildGO("Container_Buttons")
	self._btnCreateTeam = self:FindChildGO("Container_Buttons/Button_CreateTeam")
	self._btnFindTeam = self:FindChildGO("Container_Buttons/Button_FindTeam")

	self._csBH:AddClick(self._btnCreateTeam, function() self:OnClickCreateTeam() end )
	self._csBH:AddClick(self._btnFindTeam, function() self:OnClickFindTeam() end )
end

--点击成员处理
-- function TeamQuickView:OnListItemClicked()
-- 	--打开邀请玩家
-- 	GUIManager.ShowPanel(PanelsConfig.TeamInvite)
-- end


--更新面板状态team_status:0无队伍/1有队伍/2队长等
function TeamQuickView:UpdateStatus()
	--LoggerHelper.Error("UpdateStatus"..GameWorld.Player().team_status)
	if GameWorld.Player().team_status > 0 then
		self._containerButtons:SetActive(false)
		self._containerMyTeam:SetActive(true)
	else
		self._containerButtons:SetActive(true)
		self._containerMyTeam:SetActive(false)
	end
end

-------------------二级菜单---------------------
--集合队友
-- function TeamQuickView:OnClickCallTeammate()
-- 	GameWorld.Player().server.team_action_req(action_config.TEAM_CALL,"0")
-- 	LoggerHelper.Log("TeamQuickView:Click")
-- end

--跟随队长
-- function TeamQuickView:OnClickFollowLeader()
-- 	-- GameWorld.Player().server.team_action_req(action_config.TEAM_CREATE,"0");
-- end


--建队
function TeamQuickView:OnClickCreateTeam()
	TeamManager:TeamCreate(1)
	--self.shouldShowTeamPanel = true
end

--找队
function TeamQuickView:OnClickFindTeam()
	--开启队伍面板
	GUIManager.ShowPanel(PanelsConfig.Team,nil,
		function ()
			GUIManager.ShowPanel(PanelsConfig.Team,  {firstTabIndex = 1})
		end)
end

function TeamQuickView:InitCallback()
	self._myTeamUpdate = function() self:RefreshData() end
	self._updateHp = function() self:UpdateHp() end
	self._updateLevel = function() self:UpdateLevel() end
	self._updateStatus = function() self:UpdateStatus() end
end

--初始化监听
function TeamQuickView:AddEventListeners()
	EventDispatcher:AddEventListener(GameEvents.TEAM_MY_UPDATE, self._myTeamUpdate)
	EventDispatcher:AddEventListener(GameEvents.TEAM_REFRESH_HP, self._updateHp)
	EventDispatcher:AddEventListener(GameEvents.TEAM_REFRESH_LEVEL, self._updateLevel)
	EventDispatcher:AddEventListener(GameEvents.TEAM_QUIT, self._updateStatus)
	EventDispatcher:AddEventListener(GameEvents.TEAM_MEMBER_MAPINFO_UPDATE, self._myTeamUpdate)
	--EventDispatcher:AddEventListener(GameEvents.UIEVENT_TEAM_QUICK_MEMBER_CLICK,function () self:OnListItemClicked() end)
end

--移除监听
function TeamQuickView:RemoveEventListeners()
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_MY_UPDATE, self._myTeamUpdate)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_REFRESH_HP, self._updateHp)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_REFRESH_LEVEL, self._updateLevel)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_QUIT, self._updateStatus)
	EventDispatcher:RemoveEventListener(GameEvents.TEAM_MEMBER_MAPINFO_UPDATE, self._myTeamUpdate)
	--EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TEAM_QUICK_MEMBER_CLICK,function () self:OnListItemClicked() end)
end

function TeamQuickView:OnEnable()
	self:AddEventListeners()
	self:RefreshData()
end

function TeamQuickView:OnDisable()
	self:RemoveEventListeners()
end

function TeamQuickView:RefreshData()
	self:UpdateTeamList()
	self:UpdateExpInfo()
end

--更新队伍信息
function TeamQuickView:UpdateTeamList()
	self:UpdateStatus()
	local memberInfos = TeamData:GetMyTeamMemberInfos()
	if memberInfos == nil then
		do return end
	end 
	self._memberInfosWithOutMe = memberInfos
	for i=1,#self._list do
		self._list[i]:OnRefreshData(self._memberInfosWithOutMe[i], GameWorld.Player().team_status == 2)
	end
	
	-- if self.shouldShowTeamPanel then
	-- 	GameManager.GUIManager.ShowPanel(PanelsConfig.Team,nil,
	-- 	function ()
	-- 		local p = GameManager.GUIManager.GetPanel(PanelsConfig.Team)
	-- 		p:ShowMyTeam()
	-- 	end)
	-- 	self.shouldShowTeamPanel = false
	-- end
end

function TeamQuickView:UpdateExpInfo()
	local count = TeamData:GetNearbyMemberCount()
	self._rectMemberCountBG.sizeDelta = Vector2.New(count*32,32)
	self._rectMemberCountBGImage:SetAllDirty()

	if count > 1 then
		local rate = GlobalParamsHelper.GetParamValue(10)[count]
		self._txtExpRate.text = "+"..tostring(rate*100).."%"
	else
		self._txtExpRate.text = "+0%"
	end
end

function TeamQuickView:UpdateHp()
	if self._memberInfosWithOutMe then
		for i=1,#self._memberInfosWithOutMe do
			if i <= 3 then
				self._list[i]:UpdateHP()
			end
		end
	end
end


function TeamQuickView:UpdateLevel()
	if self._memberInfosWithOutMe then
		for i=1,#self._memberInfosWithOutMe do
			if i <= 3 then
				self._list[i]:UpdateLevel()
			end
		end
	end
end