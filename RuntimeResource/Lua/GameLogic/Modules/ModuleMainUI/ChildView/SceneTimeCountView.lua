local SceneTimeCountView = Class.SceneTimeCountView(ClassTypes.BaseLuaUIComponent)

SceneTimeCountView.interface = GameConfig.ComponentsConfig.Component
SceneTimeCountView.classPath = "Modules.ModuleMainUI.ChildView.SceneTimeCountView"

local DateTimeUtil = GameUtil.DateTimeUtil
local Color = Color
local red = Color(240/255, 39/255 , 76/255 ,1)
local lastTime = 10
local XArtNumber = GameMain.XArtNumber

function SceneTimeCountView:Awake()
    self:InitViews()
    self:InitListenerFunc()
end

function SceneTimeCountView:InitListenerFunc()
    self._ShowTimeUIFun = function () self:UpdateTimeCount() end
    self._ShowAddTimeUIFun = function (data) self:ShowAddTime(data) end
end

function SceneTimeCountView:OnDestroy()
end

function SceneTimeCountView:OnEnable()
    --self._timeText.color = Color(1,1,1,1)
    EventDispatcher:AddEventListener(GameEvents.TimeCount, self._ShowTimeUIFun)
    EventDispatcher:AddEventListener(GameEvents.ActionTimerAddTime, self._ShowAddTimeUIFun)
end

function SceneTimeCountView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.TimeCount, self._ShowTimeUIFun)
    EventDispatcher:RemoveEventListener(GameEvents.ActionTimerAddTime, self._ShowAddTimeUIFun)
    TimerHeap:DelTimer(self._timer)
end

function SceneTimeCountView:InitViews()
    self._type1Obj = self:FindChildGO("Container_Type1")
    self._artNumberRight = self:AddChildComponent('Container_Type1/Container_ArtNumber', XArtNumber)
    self._type1Obj:SetActive(false)

    -- self._type2Obj = self:FindChildGO("Container_Type2")
    -- self._timeText = self:GetChildComponent("Container_Type2/Text_Time", "TextMeshWrapper")
    -- self._addTimeText = self:GetChildComponent("Container_Type2/Text_AddTime", "TextMeshWrapper")
    -- self._addTimeObj = self:FindChildGO("Container_Type2/Text_AddTime")
    -- self._addTimeObj:SetActive(false)
end

function SceneTimeCountView:SetData(cfgdata)
    self._cfg = cfgdata
    if self._cfg then
        -- for type, value in pairs(self._cfg.ui_type) do
        --     if type == 1 then
        --         self._type1Obj:SetActive(true)
        --         --self._type2Obj:SetActive(false)
        --     else 
        --         self._type1Obj:SetActive(false)
        --         --self._type2Obj:SetActive(true)
        --     end
        -- end
        --LoggerHelper.Error("SceneTimeCountView:SetData")
        self._type1Obj:SetActive(true)
        self:UpdateTimeCount()
    end
end

function SceneTimeCountView:UpdateTimeCount()
    --可能数据还没设定
    if self._cfg == nil then
        return
    end
    --计时方式:1 倒计时 2 正常计时
    if self._cfg.time_type == 1 then
        self._time = self._cfg.time_second - self._cfg.second_counter
    else
        self._time = self._cfg.second_counter
    end

    -- for type, value in pairs(self._cfg.ui_type) do
    --     if type == 1 then
    --         --self:UIType1(value[1],value[2])
    --         self:UIType2()
    --     else 
    --         self:UIType2()
    --     end
    -- end
    self:UIType1(1,1)
end

--类型1：每隔X秒显示一次，最后A秒的时候每秒都显示直到计时结束，当计时间隔大于1秒的情况下，UI是否隐藏
function SceneTimeCountView:UIType1(x,a)
    if self._time <= a then
        self._artNumberRight:SetNumber(self._time)
        self._type1Obj:SetActive(true)
    else
        local t1,t2 = math.modf(self._time/x)
        if t2 == 0 then
            self._artNumberRight:SetNumber(self._time)
            self._type1Obj:SetActive(true)
            self._timer = TimerHeap:AddSecTimer(1.5, 0, 0, function() self._type1Obj:SetActive(false) end)
        else
            --让第一秒显示久点
            if self._time == self._cfg.time_second -1 then
                return
            else
                self._type1Obj:SetActive(false)
            end
        end
    end
    --GameWorld.LoggerHelper.Log("时间:"..self._time)
end

--类型2：时间格式：1 HH:MM:SS  2 MM:SS 3 SS
-- function SceneTimeCountView:UIType2()
--     --最后剩余时间多少的时候进行闪红显示
--     if self._time == lastTime then
--         self._timeText.color = red
--     end

--     -- if self._cfg.time_format == 1 then
--     --     self._timeText.text = DateTimeUtil:FormatFullTime(self._time)
--     -- elseif self._cfg.time_format == 2 then
--     --     self._timeText.text = DateTimeUtil:ParseTime(self._time)
--     -- else
--     --     self._timeText.text = self._time
--     -- end
--     self._timeText.text = self._time
-- end

function SceneTimeCountView:ShowAddTime(data)
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
    end
    self._addTimeObj:SetActive(true)
    for id, time in pairs(data) do
        self._addTimeText.text = string.format("+%ds,", time)
    end

    self._timer = TimerHeap:AddSecTimer(1, 0, 0, function() self._addTimeObj:SetActive(false) end)
end

---------------------------------------PK后端本---------------------------------------------------------------------------------

function SceneTimeCountView:SetType()
    self._type1Obj:SetActive(true)
    --self._type2Obj:SetActive(false)
end