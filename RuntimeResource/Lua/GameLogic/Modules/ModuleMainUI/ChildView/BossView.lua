-- BossView.lua
local BossView = Class.BossView(ClassTypes.BaseComponent)
require "Modules.ModuleMainUI.ChildComponent.MainUIBossInfoItem"
require "Modules.ModuleMainUI.ChildComponent.MainUIEliteInfoItem"

BossView.interface = GameConfig.ComponentsConfig.Component
BossView.classPath = "Modules.ModuleMainUI.ChildView.BossView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil

local UIList = ClassTypes.UIList
local WorldBossManager = PlayerManager.WorldBossManager
-- local MapManager = PlayerManager.MapManager
local MainUIBossInfoItem = ClassTypes.MainUIBossInfoItem
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local SceneConfig = GameConfig.SceneConfig
local BossHomeManager = PlayerManager.BossHomeManager
local BackWoodsManager = PlayerManager.BackWoodsManager
local GodIslandManager = PlayerManager.GodIslandManager
local GameSceneManager = GameManager.GameSceneManager
-- local RegionalMapDataHelper = GameDataHelper.RegionalMapDataHelper
local MainUIEliteInfoItem = ClassTypes.MainUIEliteInfoItem
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

function BossView:Awake()
    self.state = false
    self._sceneType = SceneConfig.SCENE_TYPE_WORLD_BOSS
    self:InitList()
end

function BossView:OnEnable()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateBossCountDown() end)
end

function BossView:OnDisable()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function BossView:OnDestroy()
end

function BossView:InitList()
    self._scrollViewListeGO = self:FindChildGO("ScrollViewList")
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(MainUIBossInfoItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(2, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1);
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._list:SetItemClickedCB(itemClickedCB)
    
    self._scrollViewListEliteGO = self:FindChildGO("ScrollViewList_Elite")
    local scrollViewElite, listElite = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Elite")
    self._listElite = listElite
    self._scrollViewElite = scrollViewElite
    self._listElite:SetItemType(MainUIEliteInfoItem)
    self._listElite:SetPadding(0, 0, 0, 0)
    self._listElite:SetGap(2, 0)
    self._listElite:SetDirection(UIList.DirectionTopToDown, 1, 1);
    local itemClickedCB =
        function(idx)
            self:OnListItemClickedElite(idx)
        end
    self._listElite:SetItemClickedCB(itemClickedCB)
    self._scrollViewListEliteGO:SetActive(false)
    
    self._switchButton = self:FindChildGO("Button_Switch")
    self._csBH:AddClick(self._switchButton, function()self:OnSwitchButtonClick() end)

    self._guideContainer = self:FindChildGO("Button_Switch/Container_Guide")
    self._guideContainerPos = self._guideContainer:AddComponent(typeof(XGameObjectTweenPosition))
    self._guideContainerInitPosition = self._guideContainer.transform.localPosition
    self._image = self:GetChildComponent("Button_Switch/Container_Guide/Container_Arrow/Image_Tail", 'ImageWrapper')
    self._image:SetContinuousDimensionDirty(true)
    self._textGuide = self:GetChildComponent("Button_Switch/Container_Guide/Container_Arrow/Image_Tail/Text_Des", "TextMeshWrapper")
end

function BossView:OnListItemClicked(idx)
    -- TaskCommonManager:OnClickTrackCurTask(self._list:GetDataByIndex(idx))
    if self._lastClickedItemIndex ~= nil then
        -- LoggerHelper.Log("OnListItemClicked:" .. self._lastClickedItemIndex .. " " .. self._list:GetLength())
        if self._lastClickedItemIndex < self._list:GetLength() then
            local item = self._list:GetItem(self._lastClickedItemIndex)
            if item ~= nil then
                item:SetItemActive(false)
            end
        end
    end
    self._lastClickedItemIndex = idx
    local curItem = self._list:GetItem(idx)
    curItem:SetItemActive(true)

    if GameSceneManager:GetCurrMapID() == public_config.FAKE_WORLD_BOSS_MAP_ID then
        local player = GameWorld.Player()
        if player.autoFight then
            player:StopAutoFight()
            player:StartAutoFight()
        else
            player:StartAutoFight()
        end
        return
    end

    if self._sceneType == SceneConfig.SCENE_TYPE_WORLD_BOSS then
        WorldBossManager:GotoBoss(self._list:GetDataByIndex(idx))
    elseif self._sceneType == SceneConfig.SCENE_TYPE_BOSS_HOME then
        BossHomeManager:GotoBoss(self._list:GetDataByIndex(idx))
    elseif self._sceneType == SceneConfig.SCENE_TYPE_FORBIDDEN then
        BackWoodsManager:GotoBoss(self._list:GetDataByIndex(idx))
    elseif self._sceneType == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
        GodIslandManager:GotoBoss(self._list:GetDataByIndex(idx))
    end

    GameWorld.Player():StopAutoFight()
end

function BossView:OnListItemClickedElite(idx)
    if self._lastClickedItemEliteIndex ~= nil then
        local item = self._listElite:GetItem(self._lastClickedItemEliteIndex)
        if item ~= nil then
            item:SetItemActive(false)
        end
    end
    self._lastClickedItemEliteIndex = idx
    local curItem = self._listElite:GetItem(idx)
    curItem:SetItemActive(true)
    if self._sceneType == SceneConfig.SCENE_TYPE_FORBIDDEN then
        BackWoodsManager:GotoElite(self._listElite:GetDataByIndex(idx))
    end
end

function BossView:UpdateBossCountDown()
    local length = self._list:GetLength() - 1
    for i = 0, length do
        -- local data = self._listBoss:GetDataByIndex(i)
        local item = self._list:GetItem(i)
        item:UpdateCountDown()
    end
end

function BossView:RefreshBossData()
    if self._switchButton then
        self.clicked = false
        self._switchButton:SetActive(false)
        self:HideGuideText()
    end
    local data = {}
    if self._sceneType == SceneConfig.SCENE_TYPE_WORLD_BOSS then
        data = WorldBossManager:GetAllSpaceBossInfo()
    elseif self._sceneType == SceneConfig.SCENE_TYPE_BOSS_HOME then
        data = BossHomeManager:GetAllSpaceBossInfo()
    elseif self._sceneType == SceneConfig.SCENE_TYPE_FORBIDDEN then
        data = BackWoodsManager:GetAllSpaceBossInfo()
        --显示精英信息
        local eliteData = BackWoodsManager:GetAllSpaceEliteInfo()
        self._listElite:SetDataList(eliteData)
        if self._switchButton then
            self._switchButton:SetActive(true)
            self:ShowGuideText()
        end
    elseif self._sceneType == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
        data = GodIslandManager:GetAllSpaceInfo()
    end
    --LoggerHelper.Log("RefreshBossData: " .. PrintTable:TableToStr(data))
    if self._list then
        self._list:SetDataList(data)
    end
-- if #tasks < 4 then
--     self._scrollView:SetScrollRectEnable(false)
-- else
--     self._scrollView:SetScrollRectEnable(true)
-- end
end

function BossView:InitType(sceneType)
    self._sceneType = sceneType
    self:ResetSwitchState()
end

function BossView:OnSwitchButtonClick()
    if not self.clicked then
        self.clicked = true
        self:HideGuideText()
    end
    self.state = not self.state
    self._scrollViewListeGO:SetActive(not self.state)
    self._scrollViewListEliteGO:SetActive(self.state)
end

function BossView:ResetSwitchState()
    self.clicked = false
    self.state = false
    self._scrollViewListeGO:SetActive(not self.state)
    self._scrollViewListEliteGO:SetActive(self.state)
end

function BossView:ShowGuideText()
    self._guideContainer:SetActive(true)
    self._textGuide.text = LanguageDataHelper.CreateContent(56093)
    self._guideContainerPos:SetFromToPos(self._guideContainerInitPosition, self._guideContainerInitPosition + Vector3(20, 0, 0), 0.3, 4, nil)
end

function BossView:HideGuideText()
    self._guideContainer:SetActive(false)
    self._guideContainerPos.IsTweening = false
end