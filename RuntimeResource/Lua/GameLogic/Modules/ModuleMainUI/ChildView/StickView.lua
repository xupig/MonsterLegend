-- LoginPanel.lua
local StickView = Class.StickView(ClassTypes.BaseLuaUIComponent)

local Color = require "UnityEngine.Color"

StickView.interface = GameConfig.ComponentsConfig.PointableDragableComponent
StickView.classPath = "Modules.ModuleMainUI.ChildView.StickView"

local PointerInputModule = UnityEngine.EventSystems.PointerInputModule
local EventSystem = UnityEngine.EventSystems.EventSystem
local RectTransformUtility = UnityEngine.RectTransformUtility
local Locater = GameMain.Locater
local XButtonHitArea = GameMain.XButtonHitArea
local LuaGameState = GameWorld.LuaGameState
local ControlStickState = GameConfig.ControlStickState
local GameSceneManager = GameManager.GameSceneManager

local MAX_DIS = 56
local color = Color(1, 1, 1, 1)
local Euler_Angles = Vector3.New(0,0,0)
local Original_Direction = Vector2.New(0,1)

function StickView:Awake()
    self._isDraging = false
    self._fingerId = -1
    self._stickContainerRect = self:GetChildComponent("Container_Stick", 'RectTransform')
    self._stickRect = self:GetChildComponent("Container_Stick/Image_ControlStick", 'RectTransform')
    self._directionRect = self:GetChildComponent("Container_Stick/Image_Direction", 'RectTransform')
    self._stickLocater = self._stickContainerRect.gameObject:AddComponent(typeof(Locater))
    self._rectTransform = self:GetComponent('RectTransform')
    self:AddComponent(typeof(XButtonHitArea))
    self._originalPosition = self._stickLocater.Position
    self._controlStickImage = self:GetChildComponent("Container_Stick/Image_ControlStick", 'ImageWrapper')
    self._stickBGImage = self:GetChildComponent("Container_Stick/Image_StickBG", 'ImageWrapper')
    self._directionImage = self:GetChildComponent("Container_Stick/Image_Direction", 'ImageWrapper')
    self:SetAlpha(0.5, 0.5, 0)
    GameWorld.LateUpdate:Add(self.LateUpdate, self)
    
    self._directionPosition = self._directionRect.localPosition
    self._ControlStickSourcePosition = Vector2.zero
    self._ControlStickSourcePosition.x = self._stickRect.localPosition.x
    self._ControlStickSourcePosition.y = self._stickRect.localPosition.y
    self._ControlStickCenterOffset = Vector2.zero
    self._ControlStickCenterOffset.x = self._stickRect.rect.width * 0.5
    self._ControlStickCenterOffset.y = -self._stickRect.rect.height * 0.5
    self._LoadSceneStart = function(isResetAllScenes)self:OnLoadGameSceneStartOrFinish(isResetAllScenes) end
    self._LoadSceneFinish = function(isResetAllScenes)self:OnLoadGameSceneStartOrFinish(isResetAllScenes) end
    self._onTriggerSceneWaitingArea = function(sceneName)self:TriggerSceneWaitingArea(sceneName) end
    self._resetControlStick = function() self:ResetControlStick() end
    EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._LoadSceneStart)
    EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._LoadSceneFinish)
    EventDispatcher:AddEventListener(GameEvents.ON_TRIGGER_SCENE_WAITING_AREA, self._onTriggerSceneWaitingArea)
    EventDispatcher:AddEventListener(GameEvents.ResetControlStick, self._resetControlStick)
end

function StickView:SetAlpha(alpha1, alpha2, alpha3)
    color.a = alpha1
    self._stickBGImage.color = color
    color.a = alpha2
    self._controlStickImage.color = color
    color.a = alpha3
    self._directionImage.color = color
end

function StickView:OnDestroy()
    GameWorld.LateUpdate:Remove(self.LateUpdate, self)
    self._stickContainerRect = nil
    self._stickRect = nil
    self._stickLocater = nil
    self._rectTransform = nil
    self._originalPosition = nil
    EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._LoadSceneStart)
    EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._LoadSceneFinish)
    EventDispatcher:RemoveEventListener(GameEvents.ON_TRIGGER_SCENE_WAITING_AREA, self._onTriggerSceneWaitingArea)
    EventDispatcher:RemoveEventListener(GameEvents.ResetControlStick, self._resetControlStick)
end

function StickView:LateUpdate()
    --由于EventSystem的响应顺序在Update之前，所以需要在LateUpdate的时候处理
    ControlStickState:SetControlStickIsDragging(self._isDraging)
    ControlStickState:SetControlStickFingerId(self._fingerId)
end

function StickView:OnPointerDown(pointerEventData)
    if self._isDraging then return end
    self._isDraging = true
    self._fingerId = pointerEventData.pointerId
    if self._fingerId == nil then self._fingerId = -1 end
    ControlStickState:SetControlStickIsDragging(self._isDraging)
    ControlStickState:SetControlStickFingerId(self._fingerId)
    if self._fingerId >= 0 or self._fingerId == PointerInputModule.kMouseLeftId then
        local result = false
        local position = Vector2.zero
        result, position = RectTransformUtility.ScreenPointToLocalPointInRectangle(self._rectTransform,
            pointerEventData.position, pointerEventData.pressEventCamera, position)
        if result then
            position.x = position.x - self._stickLocater.Width * 0.5;
            position.y = position.y + self._stickLocater.Height * 0.5;
            self._stickLocater.Position = position
        end
        self._directionRect.localPosition = self._directionPosition
    end
end

function StickView:OnPointerUp(pointerEventData)
    self:ResetControlStick()
end

function StickView:OnBeginDrag(pointerEventData)
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandMoveByStick)
    self:SetAlpha(1, 1, 1)
    if pointerEventData.pointerId >= 0 or pointerEventData.pointerId == PointerInputModule.kMouseLeftId then
        --EventSystem.current.SetSelectedGameObject(self.gameObject, pointerEventData);
        ControlStickState:SetControlStickStrength(0.01)  --提前做一个状态设置
        EventDispatcher:TriggerEvent(GameEvents.Stick_Begin_Drag)
    end
end

function StickView:OnDrag(pointerEventData)
    if self._isDraging ~= true then
        do return end
    end
    if pointerEventData.pointerId >= 0 or pointerEventData.pointerId == PointerInputModule.kMouseLeftId then
        self:OnDragStick(pointerEventData)
    end
end



function StickView:OnEndDrag(pointerEventData)
    self:SetAlpha(0.5, 0.5, 0)
    self._stickRect.localPosition = Vector3(self._ControlStickSourcePosition.x, self._ControlStickSourcePosition.y, 0);
    ControlStickState:SetControlStickDirection(Vector2.zero)
    ControlStickState:SetControlStickStrength(0)
    EventDispatcher:TriggerEvent(GameEvents.Stick_End_Drag)
end

function StickView:OnLoadGameSceneStartOrFinish(isResetAllScenes)
    if isResetAllScenes then
        self:ResetControlStick()
    end
end

function StickView:TriggerSceneWaitingArea(sceneName)
    local curSceneName = GameSceneManager:GetSceneName()
    local sceneInLoading = GameSceneManager:GetInLoading()
    if curSceneName == sceneName and sceneInLoading then
        self:ResetControlStick()
    end
end

function StickView:ResetControlStick()
    EventDispatcher:TriggerEvent(GameEvents.Stick_Reset)
    self:SetAlpha(0.5, 0.5, 0)
    self._isDraging = false;
    self._fingerId = -1;
    self._stickLocater.Position = self._originalPosition
    self._directionRect.localPosition = self.HIDE_POSITION
    
    self._stickRect.localPosition = Vector3(self._ControlStickSourcePosition.x, self._ControlStickSourcePosition.y, 0);
    ControlStickState:SetControlStickDirection(Vector2.zero)
    ControlStickState:SetControlStickStrength(0)
end

function StickView:OnDragStick(pointerEventData)
    local result = false
    local fingerPos = Vector2.zero
    result, fingerPos = RectTransformUtility.ScreenPointToLocalPointInRectangle(self._stickContainerRect,
        pointerEventData.position, pointerEventData.pressEventCamera, fingerPos)
    if result
    then
        local center = self._ControlStickSourcePosition + self._ControlStickCenterOffset
        local direction = (fingerPos - center)
        if (direction.magnitude < MAX_DIS) then
            self._stickRect.localPosition =
                Vector3(fingerPos.x - self._ControlStickCenterOffset.x, fingerPos.y - self._ControlStickCenterOffset.y, 0);
        else
            direction = direction.normalized;
            direction = direction * MAX_DIS;
            local stickPos = center + direction;
            self._stickRect.localPosition =
                Vector3(stickPos.x - self._ControlStickCenterOffset.x, stickPos.y - self._ControlStickCenterOffset.y, 0);
        end
        local strength = direction.magnitude / MAX_DIS
        ControlStickState:SetControlStickDirection(direction)
        ControlStickState:SetControlStickStrength(strength)
        self:ShowDirectionImage(direction)
    end
end

function StickView:ShowDirectionImage(direction)
    local angle = Vector2.Angle(direction, Original_Direction)
    if direction.x > 0 then
        angle = 360 - angle
    end
    Euler_Angles.z = angle
    self._directionRect.localEulerAngles = Euler_Angles
end
