-- PvpHeadView.lua
local PvpHeadView = Class.PvpHeadView(ClassTypes.BaseComponent)

PvpHeadView.interface = GameConfig.ComponentsConfig.Component
PvpHeadView.classPath = "Modules.ModuleMainUI.ChildView.PvpHeadView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil

local public_config = GameWorld.public_config
local RoleDataHelper = GameDataHelper.RoleDataHelper
local GameSceneManager = GameManager.GameSceneManager
local XArtNumber = GameMain.XArtNumber
local UIProgressBar = ClassTypes.UIProgressBar
local OnlinePvpManager = PlayerManager.OnlinePvpManager

function PvpHeadView:Awake()
    self._fpNumber = self:AddChildComponent('Container_FightPower/Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)
    self._containerHead = self:FindChildGO("Container_Head")
    self._onUpdateFightForce = function()self:OnUpdateFightForce() end
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self:InitHPProgress()
end

function PvpHeadView:OnEnable()
    -- LoggerHelper.Log("Ash: PvpHeadView OnEnable:", true)
    EventDispatcher:AddEventListener(GameEvents.OnlinePvpUpdateFightForce, self._onUpdateFightForce)
end

function PvpHeadView:OnDisable()
    -- LoggerHelper.Log("Ash: PvpHeadView OnDisable:", true)
    EventDispatcher:RemoveEventListener(GameEvents.OnlinePvpUpdateFightForce, self._onUpdateFightForce)
end

function PvpHeadView:OnDestroy()
end

function PvpHeadView:OnUpdateFightForce()
    if self._entity then
        local fightForce = OnlinePvpManager:GetFightForce(self._entity.id)
        if fightForce then
            self._fpNumber:SetNumber(fightForce)
        end
    end
end

function PvpHeadView:InitHPProgress()
    self._hpComp = UIProgressBar.AddProgressBar(self.gameObject, "ProgressBar_Hp")
    self._hpComp:SetProgress(0)
    self._hpComp:ShowTweenAnimate(true)
    self._hpComp:SetMutipleTween(true)
    self._hpComp:SetIsResetToZero(false)
    self._hpComp:SetTweenTime(0.2)
    self._onHPChangeCB = function()self:OnHPChange() end
end

function PvpHeadView:Init()
    self._textName.text = ""
    self._containerHead:SetActive(false)
    self._fpNumber.gameObject:SetActive(false)
    self._hpComp:SetProgressByValue(1, 1)
    self._hpComp:SetProgressText(self:GetNumString(1) .. "/" .. self:GetNumString(1))
end

function PvpHeadView:SetEntity(entity)
    self._entity = entity
    self._textName.text = entity.name
    local icon = RoleDataHelper.GetHeadIconByVocation(entity.vocation)
    self._containerHead:SetActive(true)
    GameWorld.AddIcon(self._containerHead, icon)
    self:OnHPChange()
    self._fpNumber.gameObject:SetActive(true)
    self:OnUpdateFightForce()
    entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
    entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
end

function PvpHeadView:RemoveEntity()
    if self._entity ~= nil then
        self._entity:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
        self._entity:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
    end
end


function PvpHeadView:OnHPChange()
    if self._entity ~= nil then
        local curHp = self._entity.cur_hp or 0
        local maxHp = self._entity.max_hp or 0
        self._hpComp:SetProgressByValue(curHp, maxHp)
        self._hpComp:SetProgressText(self:GetNumString(curHp) .. "/" .. self:GetNumString(maxHp))
    end
end

function PvpHeadView:GetNumString(number)
    local str
    if number >= 100000 then
        str = tostring(math.floor(number / 100) / 100) .. LanguageDataHelper.GetContent(143)
    else
        str = tostring(number)
    end
    return str
end
