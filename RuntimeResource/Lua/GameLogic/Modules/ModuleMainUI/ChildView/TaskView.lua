-- TaskView.lua
local TaskView = Class.TaskView(ClassTypes.BaseComponent)
require "Modules.ModuleMainUI.ChildComponent.TrackedTaskInfoItem"

TaskView.interface = GameConfig.ComponentsConfig.Component
TaskView.classPath = "Modules.ModuleMainUI.ChildView.TaskView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil

local UIList = ClassTypes.UIList
local TaskManager = PlayerManager.TaskManager
local MapManager = PlayerManager.MapManager
local TrackedTaskInfoItem = ClassTypes.TrackedTaskInfoItem
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GameSceneManager = GameManager.GameSceneManager
local RegionalMapDataHelper = GameDataHelper.RegionalMapDataHelper
local InstanceManager = PlayerManager.InstanceManager
local TaskCommonManager = PlayerManager.TaskCommonManager

function TaskView:Awake()
    self._onHasLittleShoesChanged = function()self:OnHasLittleShoesChanged() end
    self._onDoMainTaskAgain = function()self:OnDoMainTaskAgain() end
    self:InitList()
end

function TaskView:OnEnable()
    -- LoggerHelper.Log("Ash: TaskView OnEnable:", true)
    EventDispatcher:AddEventListener(GameEvents.OnHasLittleShoesChanged, self._onHasLittleShoesChanged)
    EventDispatcher:AddEventListener(GameEvents.CAN_GO_MAIN_TASK_AGAIN, self._onDoMainTaskAgain)--弹出指引指向主线任务，取消指引指向赏金任务
end

function TaskView:OnDisable()
    -- LoggerHelper.Log("Ash: TaskView OnDisable:", true)
    self._list:SetAllItemDirty()
    EventDispatcher:RemoveEventListener(GameEvents.OnHasLittleShoesChanged, self._onHasLittleShoesChanged)
    EventDispatcher:RemoveEventListener(GameEvents.CAN_GO_MAIN_TASK_AGAIN, self._onDoMainTaskAgain)
end

function TaskView:OnDestroy()
end

function TaskView:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(TrackedTaskInfoItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(2, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1);
    -- local itemClickedCB =
    --     function(idx)
    --         self:OnListItemClicked(idx)
    --     end
    -- self._list:SetItemClickedCB(itemClickedCB)
end

function TaskView:OnHasLittleShoesChanged()
    local hasLittleShoes = InstanceManager:GetHasLittleShoes()
    -- LoggerHelper.Log("Ash: OnHasLittleShoesChanged:" .. tostring(hasLittleShoes))
    local length = self._list:GetLength() - 1
    for i = 0, length do
        -- local data = self._list:GetDataByIndex(i)
        local item = self._list:GetItem(i)
        item:ChangeLittleShoes(hasLittleShoes)
    end
end

--弹出指引指向主线任务，取消指引指向赏金任务
function TaskView:OnDoMainTaskAgain()
    -- LoggerHelper.Error("TaskView:OnDoMainTaskAgain()")
    for i,v in ipairs(self._listData) do
        local item = self._list:GetItem(i-1)
        item:DoMainTaskAgain()
    end
end

-- function TaskView:OnListItemClicked(idx)
--     -- LoggerHelper.Log("OnListItemClicked:" .. idx .. PrintTable:TableToStr(self._list:GetDataByIndex(idx)))
--     TaskCommonManager:OnManualHandleTask(self._list:GetDataByIndex(idx))
--     -- if self._lastClickedItemIndex ~= nil then
--     --     local item = self._list:GetItem(self._lastClickedItemIndex)
--         -- if item ~= nil then
--         --     item:SetItemActive(false)
--         -- end
--     -- end
--     self._lastClickedItemIndex = idx
--     local curItem = self._list:GetItem(idx)
--     --curItem:SetItemActive(true)
--     EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_main_task_ui_button")
-- end

function TaskView:RefreshTaskData()
    local tasks = TaskManager:GetUpdateTasksForMainMenu()
    if tasks == nil then
        return
    end
    self._listData = tasks
    self._list:SetDataList(self._listData)    
    if #tasks < 4 then
        self._list:SetPositionByNum(1)
        self._scrollView:SetScrollRectEnable(false)
    else
        self._scrollView:SetScrollRectEnable(true)
    end
end

function TaskView:InitTaskData()
    local tasks = TaskManager:GetTasksForMainMenu()
    self._list:SetDataList(tasks)
    if #tasks < 4 then
        self._list:SetPositionByNum(1)
        self._scrollView:SetScrollRectEnable(false)
    else
        self._scrollView:SetScrollRectEnable(true)
    end
end