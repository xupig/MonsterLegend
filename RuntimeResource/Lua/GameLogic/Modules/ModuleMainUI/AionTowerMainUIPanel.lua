-- AionTowerMainUIPanel.lua
local AionTowerMainUIPanel = Class.AionTowerMainUIPanel(ClassTypes.BasePanel)

local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local AionDataHelper = GameDataHelper.AionDataHelper
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local AionTowerManager = PlayerManager.AionTowerManager
local public_config = GameWorld.public_config

require"Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem

function AionTowerMainUIPanel:Awake()
    self.disableCameraCulling = true
    self:InitViews()
end

function AionTowerMainUIPanel:InitViews()
    self._isOpen = true
	self._btnExpand = self:FindChildGO("Container_Content/Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
    
    self._containerInfo = self:FindChildGO("Container_Content/Container_Info")
    self._initPosition = self._containerInfo.transform.localPosition
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
	self._imgLeft = self:FindChildGO("Container_Content/Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Container_Content/Button_Expand/Image_Right")

    self._targetText = self:GetChildComponent("Container_Content/Container_Info/Text_Title","TextMeshWrapper")
    self._posText = self:GetChildComponent("Container_Content/Container_Info/Text_Pos","TextMeshWrapper")
    self:InitRewardsList()
end

function AionTowerMainUIPanel:OnShow()
    self:SetText()
end

function AionTowerMainUIPanel:OnClose()
end

function AionTowerMainUIPanel:InitRewardsList()
    self._rewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Content/Container_Info/Container_Reward")
    self._rewardGridLauoutGroup:SetItemType(RewardItem)
end

function AionTowerMainUIPanel:OnDestroy()
    self._isOpen = nil
	self._btnExpand = nil
	self._containerInfo = nil
    self._tweenPosition = nil
	self._imgLeft = nil
    self._imgRight = nil
    self._targetText = nil
    self._rewardGridLauoutGroup = nil
end

function AionTowerMainUIPanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function AionTowerMainUIPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    -- self._containerInfo.transform.localPosition = Vector3.New(-280, 0, 0)
    self._tweenPosition:SetFromToPos(self._containerInfo.transform.localPosition, self._initPosition, 0.4, 1, nil)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(0, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function AionTowerMainUIPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetFromToPos(self._initPosition, Vector3.New(self._initPosition.x - 280, self._initPosition.y, self._initPosition.z), 0.4, 1, nil)
    -- self._containerInfo.transform.localPosition = Vector3.New(0, 0, 0)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(-280, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

function AionTowerMainUIPanel:SetText()
    local pos = AionTowerManager:GetNowPos()
    local monsterId = AionDataHelper:GetMonsterId(pos)[1]
    local rewardData = AionDataHelper:GetRewardShow(pos)
    
    self._rewardGridLauoutGroup:SetDataList(rewardData)
    self._targetText.text = LanguageDataHelper.CreateContent(55009, {
        ["0"] = MonsterDataHelper.GetMonsterName(monsterId)
    })
    self._posText.text = LanguageDataHelper.CreateContent(55004, {
        ["0"] = pos
    })
end