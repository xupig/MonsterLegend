
-- BuffItem.lua
local BuffItem = Class.BuffItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local UIToggle = ClassTypes.UIToggle
local public_config = GameWorld.public_config
local TimerHeap = GameWorld.TimerHeap
BuffItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
BuffItem.classPath = "Modules.ModuleMainUI.ChildComponent.BuffItem"

function BuffItem:Awake()
    self._base.Awake(self)
    self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper') 
    self._txtSpecial = self:GetChildComponent("Text_Special",'TextMeshWrapper') 
    self._txtDesc = self:GetChildComponent("Text_Desc",'TextMeshWrapper') 
    self._txtTime = self:GetChildComponent("Text_Time",'TextMeshWrapper') 
    self._iconContainer = self:FindChildGO("Container")
end

function BuffItem:OnDestroy()

    self._base.OnDestroy(self)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end

end

--override
function BuffItem:OnRefreshData(data)
    if data then
        -- LoggerHelper.Log("buffItem====================" .. PrintTable:TableToStr(data))
        self._data = data
        self._txtName.text = data._name 
        self._txtDesc.text = data._desc
        if data._icon then
            GameWorld.AddIcon(self._iconContainer,data._icon)
        end  
        --local buffTime = 

        if  data._time ~= -1 or data._day then
            self._txtTime.gameObject:SetActive(true)
            if data._time ~= -1 then
                self._leftTime = data._time
                if self._timer then
                    TimerHeap:DelTimer(self._timer)
                    self._timer = nil
                end
                self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnBackTime() end)
                self._txtTime.text = DateTimeUtil:FormatFullTime(self._leftTime, false, false, true)
            end

            if data._day then
                self._txtTime.text = data._day..LanguageDataHelper.CreateContent(147)
            end
        else
            self._txtTime.gameObject:SetActive(false)
        end

       




        if data._special ~= -1 then
            self._txtSpecial.gameObject:SetActive(true)
            self._txtSpecial.text =  data._special
        else
            self._txtSpecial.gameObject:SetActive(false)
        end 

    end
end

function BuffItem:OnBackTime()
    self._leftTime = self._leftTime - 1
    if (self._leftTime > 0) then
        -- self._reviveTime = self._reviveTime - 1    
        self._txtTime.text = DateTimeUtil:FormatFullTime(self._leftTime, false, false, true)
    else
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
        self._txtTime.text = "0"
    end
end