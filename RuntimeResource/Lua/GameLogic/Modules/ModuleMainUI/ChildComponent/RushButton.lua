local ClassTypes = ClassTypes
local RushButton = Class.RushButton(ClassTypes.SkillButton)
local StateConfig = GameConfig.StateConfig
local EntityConfig = GameConfig.EntityConfig
local ImageWrapper = UIExtension.ImageWrapper
local UIProgressBar = ClassTypes.UIProgressBar
local XImageFilling = GameMain.XImageFilling

RushButton.interface = GameConfig.ComponentsConfig.PointableComponent
RushButton.classPath = "Modules.ModuleMainUI.ChildComponent.RushButton"

local Up_Scale = Vector3.New(1,1,1)
local Down_Scale = Vector3.New(0.9,0.9,1)

function RushButton:Awake()
    self.skillPos = 6
    self:InitView()
end

function RushButton:InitView()
    self._UseTimeNum = self:FindChildGO("Container_UseTime/Text_UseTimeNum")
    self._UseTimeNumPos = self._UseTimeNum.transform.localPosition
    self._UseTimeNumText = self._UseTimeNum:GetComponent("TextMeshWrapper")
    self._UseTimeNum.transform.localPosition = self.HIDE_POSITION
end

function RushButton:OnDestroy()
    self._imageFilling = nil
end

function RushButton:OnPointerDown(pointerEventData)
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandPlaySkill, self.skillPos)
	self.transform.localScale = Down_Scale
end

function RushButton:OnPointerUp(pointerEventData)
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandReleaseSkill, self.skillPos)
    self.transform.localScale = Up_Scale
end

function RushButton:SetUseTimeActivite(state)
	if state then
		self._UseTimeNum.transform.localPosition = self._UseTimeNumPos
	else
		self._UseTimeNum.transform.localPosition = self.HIDE_POSITION
	end
end
