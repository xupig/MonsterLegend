--主界面子菜单的Item
-- MenuItem.lua
local MenuItem = Class.MenuItem(ClassTypes.BaseComponent)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
MenuItem.interface = GameConfig.ComponentsConfig.Component
MenuItem.classPath = "Modules.ModuleMainUI.ChildComponent.MenuItem"

-- local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local UIComponentUtil = GameUtil.UIComponentUtil
local FunctionOpenManager = PlayerManager.FunctionOpenManager
-- local stop = Vector3.New(0, 0, 360)
-- local start = Vector3.New(0, 0, 0)
local UIParticle = ClassTypes.UIParticle
local GUILayerManager = GameManager.GUILayerManager
local RectTransformUtility = UnityEngine.RectTransformUtility

function MenuItem:Awake()
    self._base.Awake(self)
    self._imageIcon = self:FindChildGO("Image_Icon")
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._csBH:AddClick(self.gameObject, function()self:OnBtnClick() end)

    self._imgRedPoint = self:FindChildGO("Image_RedPoint")
    self._imgRedPoint:SetActive(false)
    self._updateRedPointFunc = function(flag) self:SetRedPoint(flag) end
end

function MenuItem:OnEnable()		
	if self._redpointData then
        self._redpointData:SetUpdateRedPointFunc(self._updateRedPointFunc)
	end
end

function MenuItem:OnDisable()
	if self._redpointData then 
        self._redpointData:SetUpdateRedPointFunc(nil)
	end
end

function MenuItem:OnDestroy()
    self._imageIcon = nil
    self._textName = nil
    self._base.OnDestroy(self)
end

function MenuItem:SetIconFx(fxItem,path)
    self._fxItem = fxItem
    self._fxItem.transform:SetParent(self.transform)
    self._fxItem.transform.anchoredPosition = Vector3(32,32,0)
    self._fxParticle = UIParticle.AddParticle(self.gameObject,path)
    --self._fxLightGo = self:FindChildGO("Container_Light(Clone)/fx_ui_BigCircleButton")
    self._fxParticle:Play(true,true)
end

function MenuItem:SetLightAnimate(lightItem)
    self._lightItem = lightItem
    self._lightItem.transform:SetParent(self.transform)
    self._lightItem.transform.anchoredPosition = Vector3(0,0,0)
    self._fxLight = UIParticle.AddParticle(self.gameObject,"Container_Light(Clone)/fx_ui_BigCircleButton")
    self._fxLightGo = self:FindChildGO("Container_Light(Clone)/fx_ui_BigCircleButton")
    -- fxLightGo.transform.localScale = Vector3.New(72,72,72)
    self._fxLight:Play(true,true)
    -- local light = UIComponentUtil.FindChild(self._lightItem,"Image_Light")
    -- self._lightRotation = light:GetComponent(typeof(XGameObjectTweenRotation))
    -- if self._lightRotation == nil then
    --     self._lightRotation = light:AddComponent(typeof(XGameObjectTweenRotation))
    -- end
    -- self._lightRotation.IsTweening = true
    -- self._lightRotation:SetFromToRotation(start, stop, 2, 2,nil)
end

function MenuItem:SetLightAnimateActive(state)
    --已经点击过的
    if self._isAlreadyClick and self._isOnce then
        return
    end
    if self._fxLight then
        if state then
            self._fxLight:Play(true,true)
        else
            self._fxLight:Stop()
        end
        --self._lightRotation.IsTweening = state
    end
end

function MenuItem:SetOnceClickLight(isOnce)
    self._isOnce = isOnce
end
-- function MenuItem:CleanLight()
--     self._lightItem = nil
--     self._lightRotation = nil
-- end

--获取屏幕坐标
function MenuItem:TransformToCanvasLocalPosition()
    local layerUIMain = GUILayerManager.GetUILayerTransform(GUILayer.LayerUIMain)
    local uicanvas = layerUIMain.transform:GetComponent('Canvas')
    --得出屏幕坐标
    local screenPos = uicanvas.worldCamera:WorldToScreenPoint(self.transform.position)
    local localPos = nil 
    local result = nil
    --UGUI屏幕坐标转UI坐标
    result, localPos = RectTransformUtility.ScreenPointToLocalPointInRectangle(uicanvas.transform, screenPos, uicanvas.worldCamera, localPos)
    localPos = localPos + Vector2.New(GameWorld.CANVAS_WIDTH / 2, GameWorld.CANVAS_HEIGHT / 2)
    --LoggerHelper.Error((localPos.x+640)..","..(localPos.y+360))
    return localPos
end

function MenuItem:OnBtnClick()
    --self:TransformToCanvasLocalPosition()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_main_menu_button_"..self._cfgId)
    -- LoggerHelper.Log("Ash: OnBtnClick: " .. PrintTable:TableToStr(self._panelNames))
    if self._isDynamic then
        local clickFunc = FunctionOpenManager:GetDynamicMenuClickFun(self._cfgId)
        if clickFunc then
            if self._cfgId == 5 then
                clickFunc(self:TransformToCanvasLocalPosition())
            else
                clickFunc()
            end
     
            self:CheckOnceClickLight()
        end
        return
    end
    if self._panelNames then
        for i, v in ipairs(self._panelNames) do
            --副本指引
            if self._panelNames == "InstanceHall" then
                EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_main_ui_instance_hall")
            end
            GUIManager.ShowPanel(v, nil, nil)
            self:CheckOnceClickLight()
            --GameWorld.PlaySound(3)
        end
    end
end

function MenuItem:CheckOnceClickLight()
    if self._isOnce and not self._isAlreadyClick and self._fxLightGo then
        self._fxLight:Stop()
        self._fxLightGo:SetActive(false)
        self._isAlreadyClick = true
    end
end

function MenuItem:SetRedPointData(redpointData)
    if redpointData ~= nil then
        self._redpointData = redpointData
		self._redpointData:SetUpdateRedPointFunc(self._updateRedPointFunc)
	end
end

function MenuItem:RemoveRedPointListener()
    if self._redpointData ~= nil then
        self._redpointData:SetUpdateRedPointFunc(nil)
        self._redpointData = nil
	end
end

function MenuItem:SetData(id,cfg,panelNames, isDynamic)
    self._cfgId = id
    local name = LanguageDataHelper.CreateContent(cfg.name)
    local iconId = cfg.icon
    local iconAnimId = cfg.icon_anim

    self.gameObject.name = "Button_"..id
    self._panelNames = panelNames
    self._isDynamic = isDynamic

    if iconAnimId and iconAnimId > 0 then
        self:SetIconAnim(id,iconAnimId)
    else
        self:SetIcon(id,iconId)
    end
    -- LoggerHelper.Log("Ash: SetData: " .. iconId .. " " .. text .. " " .. PrintTable:TableToStr(panelNames))
    self:SetName(id,name)
end

function MenuItem:SetIcon(id,iconId)
    local menuIcon = FunctionOpenManager:GetDynamicMenuIcon(id)
    if menuIcon then
        GameWorld.AddIcon(self._imageIcon, menuIcon, nil, true)
    elseif iconId then
        GameWorld.AddIcon(self._imageIcon, iconId, nil, true)
    end
end

function MenuItem:SetIconAnim(id,iconAnimId)
    -- local menuIcon = FunctionOpenManager:GetDynamicMenuIcon(id)
    -- if menuIcon then
    --     GameWorld.AddIcon(self._imageIcon, menuIcon, nil, true)
    -- elseif iconId then
    --     GameWorld.AddIcon(self._imageIcon, iconId, nil, true)
    -- end
    GameWorld.AddIconAnim(self._imageIcon, iconAnimId)
end

function MenuItem:SetName(id,name)
    local menuName = FunctionOpenManager:GetDynamicMenuName(id)
    if menuName then
        self._textName.text = menuName
    elseif name then
        self._textName.text = name
    end
end

function MenuItem:SetRedPoint(isActive)
    self._redPointState = isActive
    self._imgRedPoint:SetActive(isActive)
end

function MenuItem:GetRedPointState()
    return self._redPointState
end

function MenuItem:UpdateTimeLeft(timeLeft,getReady)
    if self._textTime == nil then
        self._textTime = self:GetChildComponent("Text_Time", "TextMeshWrapper")
        self._strGetReady = LanguageDataHelper.CreateContent(52105)
    end
    self._getReady = getReady
    if timeLeft > 0 then
        self._timeLeft = timeLeft
        if self._timerId == nil then
            self._timerId = TimerHeap:AddSecTimer(0,1,0,function ()
                if self._timeLeft > DateTimeUtil.OneDayTime then
                    local dayCount = math.ceil(self._timeLeft/DateTimeUtil.OneDayTime)
                    if dayCount ~= self._dayCount then
                        self._textTime.text = dayCount..LanguageDataHelper.CreateContent(147)
                        self._dayCount = dayCount
                    end
                else
                    local noHour = self._timeLeft < DateTimeUtil.OneHourTime
                    local str = DateTimeUtil:FormatFullTime(self._timeLeft,false,noHour)
                    if self._getReady then
                        str = str .. "\n" .. self._strGetReady
                    end
                    self._textTime.text = str
                end
                
                self._timeLeft = self._timeLeft - 1
                if self._timeLeft <= 0  then
                    self:ClearTimer()
                end
            end)
        end
    else
        self:ClearTimer()
    end
end

function MenuItem:ClearTimer()
    if self._timerId then
        TimerHeap:DelTimer(self._timerId)
        self._timerId = nil
        self._dayCount = nil
    end
    self._textTime.text = ""
end