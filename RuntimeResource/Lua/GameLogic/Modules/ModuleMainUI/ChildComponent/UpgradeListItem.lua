-- UpgradeListItem.lua
local UpgradeListItem = Class.UpgradeListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
UpgradeListItem.interface = GameConfig.ComponentsConfig.Component
UpgradeListItem.classPath = "Modules.ModuleMainUI.ChildComponent.UpgradeListItem"
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function UpgradeListItem:Awake()
    self._nameText = self:GetChildComponent("Image_Bg/Text_Name",'TextMeshWrapper') 
    self._goButton = self:FindChildGO("Image_Bg")
    self._csBH:AddClick(self._goButton, function() self:OnGoButtonClick() end)
end

function UpgradeListItem:OnDestroy() 

end


function UpgradeListItem:OnRefreshData(data)    
    if data then
        self._data = data
        self._nameText.text = LanguageDataHelper.CreateContent(data[2][2])
    end
end

function UpgradeListItem:OnGoButtonClick()
    GameManager.GUIManager.ClosePanel(PanelsConfig.Upgrade)
    local follow = self._data
    EventDispatcher:TriggerEvent(GameEvents.ON_UPGRADE_CLICK, follow)
end