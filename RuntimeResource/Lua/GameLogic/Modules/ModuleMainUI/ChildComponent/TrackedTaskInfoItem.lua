--主界面已跟踪任务上任务的ListItem
-- TrackedTaskInfoItem.lua
require "Modules.ModuleTask.ChildView.TaskStepView"
local TrackedTaskInfoItem = Class.TrackedTaskInfoItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local UIToggle = ClassTypes.UIToggle
local TaskStepView = ClassTypes.TaskStepView
local public_config = GameWorld.public_config
local FontStyleHelper = GameDataHelper.FontStyleHelper
local TaskDataHelper = GameDataHelper.TaskDataHelper
local TaskManager = PlayerManager.TaskManager
local InstanceManager = PlayerManager.InstanceManager
local TaskCommonManager = PlayerManager.TaskCommonManager
local TaskConfig = GameConfig.TaskConfig
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local XImageFlowLight = GameMain.XImageFlowLight
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local PlayerActionManager = GameManager.PlayerActionManager
local GUIManager = GameManager.GUIManager
local VocationChangeManager = PlayerManager.VocationChangeManager
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale
local SystemInfoDataHelper = GameDataHelper.SystemInfoDataHelper
local BagManager = PlayerManager.BagManager
local ChangeVocationDataHelper = GameDataHelper.ChangeVocationDataHelper

TrackedTaskInfoItem.interface = GameConfig.ComponentsConfig.Component
TrackedTaskInfoItem.classPath = "Modules.ModuleMainUI.ChildComponent.TrackedTaskInfoItem"

function TrackedTaskInfoItem:Awake()
    self._base.Awake(self)
    self._imageBG = self:FindChildGO("Container_content/Background")
    self._imageBGActive = self:FindChildGO("Container_content/BackgroundActive")
    self._imageBGActiveFlowLight = self:AddChildComponent("Container_content/BackgroundActive", XImageFlowLight)
    -- self._clickContainer = self:FindChildGO("Container_content/Container_Click")
    self._textHead1 = self:GetChildComponent("Container_content/Background/Container_Text/Text_Head", "TextMeshWrapper")
    self._textStep1 = self:GetChildComponent("Container_content/Background/Container_Text/Text_Step", "TextMeshWrapper")
    -- self._imageHeadIcon1 = self:FindChildGO("Container_content/Background/Image_HeadIcon")
    self._textHead2 = self:GetChildComponent("Container_content/BackgroundActive/Container_Text/Text_Head", "TextMeshWrapper")
    self._textStep2 = self:GetChildComponent("Container_content/BackgroundActive/Container_Text/Text_Step", "TextMeshWrapper")
    -- self._imageHeadIcon2 = self:FindChildGO("Container_content/BackgroundActive/Image_HeadIcon")

    self._levelGuideCoantaienr = self:FindChildGO("Container_content/Container_Level_Guide")
    self._levelGuideClickCoantaienr = self:FindChildGO("Container_content/Container_Level_Guide/Container_Level_Click")
    self._csBH:AddClick(self._levelGuideClickCoantaienr , function() self:LevelGuideClick() end)

    self._vocationGuideCoantaienr = self:FindChildGO("Container_content/Container_3_Vocation_Guide")
    self._vocationGuideClickButton = self:FindChildGO("Container_content/Container_3_Vocation_Guide/Button_Vocation")
    self._csBH:AddClick(self._vocationGuideClickButton , function() self:VocationQuickClick() end)
    --LoggerHelper.Error("初始化结束")

    self._imageHeadIcon = self:FindChildGO("Container_content/Image_HeadIcon")
    self._csBH:AddClick(self._imageHeadIcon, function()self:OnClickLittleShoes() end)
    self._imageHeadIconScale = self._imageHeadIcon:AddComponent(typeof(XGameObjectTweenScale))

    -- self._imageHeadIcon:AddComponent(typeof(ButtonWrapper))
    -- self._csBH:AddClick(self._imageHeadIcon1, function()self:OnClickLittleShoes() end)
    -- self._csBH:AddClick(self._imageHeadIcon2, function()self:OnClickLittleShoes() end)
    
    self._csBH:AddClick(self._imageBG, function()self:BackgroundClick() end)
    self._csBH:AddClick(self._imageBGActive, function()self:BackgroundClick() end)
    -- self._csBH:AddClick(self._clickContainer , function() self:BackgroundClick() end)
    self._guideContainer = self:FindChildGO("Container_content/Container_Guide")
    self._guideContainerPos = self._guideContainer:AddComponent(typeof(XGameObjectTweenPosition))
    self._guideContainerInitPosition = self._guideContainer.transform.localPosition
    self._image = self:GetChildComponent("Container_content/Container_Guide/Container_Arrow/Image_Tail", 'ImageWrapper')
    self._image:SetContinuousDimensionDirty(true)
    self._textGuide = self:GetChildComponent("Container_content/Container_Guide/Container_Arrow/Image_Tail/Text_Des", "TextMeshWrapper")

    self._mainTextContainer = self:FindChildGO("Container_content/BackgroundActive/Container_Text")
    self._initPosition = self._mainTextContainer.transform.localPosition
    self._otherTextContainer = self:FindChildGO("Container_content/Background/Container_Text")

    self._tipsContaienr = self:FindChildGO("Container_content/Container_Tips")
    self._kuangImage = self:FindChildGO("Container_content/Container_Tips/Image_Kuang")
    self._kuangImageScale = self._kuangImage:AddComponent(typeof(XGameObjectTweenScale))
    self._jianTouImage = self:FindChildGO("Container_content/Container_Tips/Image_Jiantou")
    self._jianTouImagePos = self._jianTouImage:AddComponent(typeof(XGameObjectTweenPosition))
    self._jianTouInitPosition = self._jianTouImage.transform.localPosition
    
-- GameWorld.AddIcon(self._imageHeadIcon, InstanceManager:GetLittleShoesIcon(), nil, true)
-- self._toggleTrackMark = UIToggle.AddToggle(self.gameObject, "Toggle_TrackMark")
-- self._toggleTrackMark:SetOnValueChangedCB(function(state)self:OnClickTrackMark(state) end)
end

function TrackedTaskInfoItem:OnDestroy()
    self._imageBG = nil
    self._imageBGActive = nil
    self._textHead = nil
    self._textStep = nil
    self._imageHeadIcon = nil
    self._toggleTrackMark = nil
    self._base.OnDestroy(self)
end

function TrackedTaskInfoItem:VocationQuickClick()
    VocationChangeManager:FastCompleteVocation3()
end

function TrackedTaskInfoItem:LevelGuideClick()
    GUIManager.ShowPanel(PanelsConfig.Upgrade)
end

function TrackedTaskInfoItem:OnClickLittleShoes()
    if GameWorld.Player().vip_level <= 0 then
        if BagManager:GetCommonItemCount(133) <= 0 then--小飞鞋数量不足
            GUIManager.ShowText(1, SystemInfoDataHelper.CreateContent(1015))
            return
        end
    end
    self:HideGuideText()

    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_little_shoes_button")
    if self:CheckNeedFly() then
        PlayerActionManager:StopAllAction()
        GameWorld.Player():StopMoveWithoutCallback()
    end
    -- TaskCommonManager:RealStop()
    -- TimerHeap:AddTimer(2000,0,0,function()
    -- LoggerHelper.Error("id =====" .. tostring(self._data:GetTask():GetId()))
    TaskCommonManager:OnManualHandleTask(self._data, true, TaskConfig.IS_HAND_CLICK)
-- end)
end

function TrackedTaskInfoItem:CheckNeedFly()
    if self._data == nil then return false end
    local task = self._data:GetTask()
    local target = self._data:GetEventItemData()
    if task == nil then return false end
    local taskItemType = task:GetTaskItemType()
    local state = task:GetState()
    if target == nil then return false end
    local eventID = target:GetEventID()

    if taskItemType == TaskConfig.TASK_TYPE_COMMON_TASK and task:GetTaskType() == public_config.TASK_TYPE_VOCATION and
        --做四转任务时
        GameWorld.Player():GetVocationChange() == 3 then
        return false
    end
    
    if taskItemType == TaskConfig.TASK_TYPE_COMMON_TASK and (not task:IsOpen()) then
        --一般任务，主线、支线、转职,未达到开启等级
        return false
    end

    if state == public_config.TASK_STATE_ACCEPTABLE then
        local acceptNPC = task:GetAcceptNPC()
        if acceptNPC == nil or acceptNPC == 0 then
            return false
        end
    end
    
    if state == public_config.TASK_STATE_COMPLETED then
        local commitNPC = task:GetCommitNPC()
        if commitNPC == nil or commitNPC == 0 then
            return false
        elseif commitNPC == 666 then
            return false
        end
    end

    --加入一个公会
    if eventID == public_config.EVENT_ID_IN_GUILD then
        return false
    end

    --加好友
    if eventID == public_config.EVENT_ID_MOB_WITH then
        return false
    end

    --世界发言
    if eventID == public_config.EVENT_ID_GOSSIP then
        return false
    end

    if eventID == public_config.EVENT_ID_GIVE_ITEM_TO_NPC then
        local follow = target:GetFollow()
        if follow and follow[1] ~= nil then --公会任务上交
            return false
        end
    end

    if eventID == public_config.EVENT_ID_PASS_INST then
        local follow = target:GetFollow()
        if follow and follow[2] ~= nil then
            --打开某功能id的窗口
            return false
        end
    end

    if eventID == public_config.EVENT_ID_GET_ITEM then
        local follow = target:GetFollow()
        if follow and follow[2] ~= nil then
            return false
        end
    end

    if eventID == public_config.EVENT_ID_KILL_MONSTER then
        local follow = target:GetFollow()
        if follow and follow[0] ~= nil then
            --打开某功能id的窗口
            return false
        end
    end

    local funcId = TaskCommonManager:GetOpenFuncId(target)--打开窗口
    if funcId ~= nil then
        return false
    end
    return true
end

function TrackedTaskInfoItem:ChangeLittleShoes(hasLittleShoes)
    -- LoggerHelper.Log("Ash: OnHasLittleShoesChanged:" .. tostring(hasLittleShoes))
    if self._data == nil then return end
    local data = self._data:GetTask()
    local flag = data:GetLittleShoesOpen()
    self:SetLittleShopsActive(flag)
end

--override
function TrackedTaskInfoItem:OnRefreshData(curMapTrackTask)
    --LoggerHelper.Error("刷新")
    self._data = curMapTrackTask
    if curMapTrackTask ~= nil then
        local data = curMapTrackTask:GetTask()
        local taskId = data:GetId()
        local taskItemType = data:GetTaskItemType()
        local taskType = data:GetTaskType()
        if taskType == nil then
            self:SetBackgroundActive(true)
            self._levelGuideCoantaienr:SetActive(false)
        else
            self:SetBackgroundActive(not (taskType == public_config.TASK_TYPE_MAIN))
        end

        local hasLittleShoes = InstanceManager:GetHasLittleShoes()
        self:ChangeLittleShoes(hasLittleShoes)

        if taskType == public_config.TASK_TYPE_MAIN and (not data:IsOpen()) then
            self._levelGuideCoantaienr:SetActive(true)
            self._imageHeadIcon:SetActive(false)
            self:SetLittleShopsActive(false)
        else
            self._levelGuideCoantaienr:SetActive(false)
        end

        if taskItemType == TaskConfig.TASK_TYPE_COMMON_TASK and taskType == public_config.TASK_TYPE_VOCATION then
            if GameWorld.Player():GetVocationChange() == 2 then
                --在做3转任务
                self._vocationGuideCoantaienr:SetActive(true)
                self._imageHeadIcon:SetActive(false)
                self:SetLittleShopsActive(false)
            else
                self._vocationGuideCoantaienr:SetActive(false)
            end
        else
            self._vocationGuideCoantaienr:SetActive(false)
        end

        if data:GetCommitNPC() == 666 then
            self:SetLittleShopsActive(false)
        end

        if taskItemType == TaskConfig.TASK_TYPE_COMMON_TASK and TaskDataHelper:GetTaskArrowGuide(taskId) == 1 then
            self._tipsContaienr:SetActive(true)
            self._kuangImageScale:SetFromToScale(1, 0.98, 0.3, 4, nil)
            self._jianTouImagePos:SetFromToPos(self._jianTouInitPosition - Vector3.New(10,0,0), self._jianTouInitPosition, 0.3, 4, nil)
        else
            self._tipsContaienr:SetActive(false)
            self._kuangImageScale.IsTweening = false
            self._jianTouImagePos.IsTweening = false
        end

        if taskItemType == TaskConfig.TASK_TYPE_CIRCLETASK then
            local name, color = data:GetTitleNameAndColor()
            self._textHead.text = name .. "(" .. (GameWorld.Player().circle_task_cur_day_count + 1) .. "/" .. GlobalParamsHelper.GetParamValue(487) .. ")"
            self._textHead.color = color
            if  TaskManager:GetNoMainTask() then
                if TaskManager:GetAgainMainTask() then
                    self:HideGuideText()
                else
                    self:ShowGuideText(LanguageDataHelper.CreateContent(77218))
                end
            else
                self._guideContainer:SetActive(false)
            end
        elseif taskItemType == TaskConfig.TASK_TYPE_GUARD_GODDESS then
            self._textHead.text = data:GetName()
            local colorId = data:GetColorId()
            self._textHead.color = FontStyleHelper:GetFontColor(colorId)
            self._guideContainer:SetActive(false)
        elseif taskItemType == TaskConfig.TASK_TYPE_WEEK_TASK then
            local ringCount = GlobalParamsHelper.GetParamValue(637)
            local num = (GameWorld.Player().week_task_cur_week_count + 1) % ringCount
            if num == 0 then
                num = 10
            end
            local name, color = data:GetTitleNameAndColor()
            self._textHead.text = name .. "(" .. (num) .. "/" .. ringCount .. ")"
            self._textHead.color = color
            self._guideContainer:SetActive(false)
        else
            if TaskDataHelper:GetTaskGuide(taskId) ~= 0 and TaskManager:GetHasGuidTask(taskId) == nil and TaskManager:GetAddTask(taskId) and data:IsOpen() then
                self:ShowGuideText(LanguageDataHelper.CreateContent(TaskDataHelper:GetTaskGuide(taskId)))
            else
                self._guideContainer:SetActive(false)
            end
            
            if taskType == public_config.TASK_TYPE_MAIN then
                if TaskManager:GetAgainMainTask() then
                    self:ShowGuideText(LanguageDataHelper.CreateContent(77219))
                end
                self._textHead.text = LanguageDataHelper.CreateContent(134) .. LanguageDataHelper.CreateContent(data:GetName())
                self._textHead.color = FontStyleHelper:GetFontColor(25)
            elseif taskType == public_config.TASK_TYPE_BRANCH then
                self._textHead.text = LanguageDataHelper.CreateContent(135) .. LanguageDataHelper.CreateContent(data:GetName())
                self._textHead.color = FontStyleHelper:GetFontColor(26)
            elseif taskType == public_config.TASK_TYPE_VOCATION then
                self._textHead.text = LanguageDataHelper.CreateContent(302) .. LanguageDataHelper.CreateContent(data:GetName())
                self._textHead.color = FontStyleHelper:GetFontColor(29)
            else
                self._textHead.text = LanguageDataHelper.CreateContent(146) .. LanguageDataHelper.CreateContent(data:GetName())
                self._textHead.color = FontStyleHelper:GetFontColor(27)
                LoggerHelper.Error("Task type not handle:" .. data:GetId() .. " " .. taskType)
            end
        end
        -- if data:GetState() == public_config.TASK_STATE_ACCEPTABLE then
        --     self._toggleTrackMark.gameObject:SetActive(false)
        -- else
        --     self._toggleTrackMark.gameObject:SetActive(true)
        --     self._toggleTrackMark:GetToggleWrapper().isOn = data:GetIsTrack()
        -- end
        self._textStep.text = TaskCommonManager:GetTaskCurStepText(data)
        self._imageBGActiveFlowLight.enabled = false
        -- TimerHeap:AddTimer(500, 0, 0, function()
        if taskType then
            self._imageBGActiveFlowLight.enabled = taskType == public_config.TASK_TYPE_MAIN
        end
        -- end)
    -- self:SetTargetsInfo(data:GetTargetsInfo(), data:GetTargetsDesc(), data:GetTargetsCurTimes(), data:GetState(), curMapTrackTask)
    else
        self._textHead.text = ""
        self._textStep.text = ""
    end
-- self:SetItemActive(false)
end

function TrackedTaskInfoItem:ShowGuideText(text)
    self._guideContainer:SetActive(true)
    self._textGuide.text = text
    self._guideContainerPos:SetFromToPos(self._guideContainerInitPosition, self._guideContainerInitPosition + Vector3(20, 0, 0), 0.3, 4, nil)
end

function TrackedTaskInfoItem:SetBackgroundActive(flag)
    self._imageBG:SetActive(flag)
    self._imageBGActive:SetActive(not flag)
    if flag then
        self._textHead = self._textHead1
        self._textStep = self._textStep1
        -- self._imageHeadIcon = self._imageHeadIcon1
    else
        self._textHead = self._textHead2
        self._textStep = self._textStep2
        -- self._imageHeadIcon = self._imageHeadIcon2
    end
end

function TrackedTaskInfoItem:BackgroundClick()
    self:HideGuideText()
    if GameWorld.Player().vip_level > 0 then
        self:OnClickLittleShoes()
    else
        TaskCommonManager:OnManualHandleTask(self._data, false, TaskConfig.IS_HAND_CLICK)
    end
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_main_task_ui_button")
end

function TrackedTaskInfoItem:HideGuideText()
    if self._data ~= nil then
        local data = self._data:GetTask()
        local taskId = data:GetId()
        local taskType = data:GetTaskType()
        local taskItemType = data:GetTaskItemType()
        TaskManager:SetHasGuidTask(taskId)
        self._guideContainer:SetActive(false)
        self._guideContainerPos.IsTweening = false
        if taskItemType == TaskConfig.TASK_TYPE_CIRCLETASK then--赏金任务
            TaskManager:SetNoMainTask(false)
        elseif taskType == public_config.TASK_TYPE_MAIN then--主线任务
            TaskManager:SetAgainMainTask(false)
        end
    end
end

-- function TrackedTaskInfoItem:SetItemActive(isActive)
--     self._imageBG:SetActive(isActive == false)
--     self._imageBGActive:SetActive(isActive)
--     self._imageBGActiveFlowLight.enabled = isActive
-- end
function TrackedTaskInfoItem:OnClickTrackMark(flag)
    EventDispatcher:TriggerEvent(GameEvents.OnTaskTrackMarkChanged, self._data:GetTask():GetId(), flag)
-- LoggerHelper.Log("Ash: OnClickTrackMark:" .. tostring(flag))
end

function TrackedTaskInfoItem:SetLittleShopsActive(flag)
    self._imageHeadIcon:SetActive(flag)
    if flag then
        self._mainTextContainer.transform.localPosition = self._initPosition
        self._otherTextContainer.transform.localPosition = self._initPosition
        self._imageHeadIconScale:SetFromToScale(1, 1.1, 0.5, 4, nil)
    else
        self._mainTextContainer.transform.localPosition = Vector3.zero
        self._otherTextContainer.transform.localPosition = Vector3.zero
        self._imageHeadIconScale.IsTweening = false
    end
end

--弹出指引指向主线任务，取消指引指向赏金任务
function TrackedTaskInfoItem:DoMainTaskAgain()
    if self._data == nil then return end
    local task = self._data:GetTask()
    local taskItemType = task:GetTaskItemType()
    local taskType = task:GetTaskType()
    if taskItemType == TaskConfig.TASK_TYPE_CIRCLETASK then--赏金任务
        -- LoggerHelper.Error("隐藏赏金任务")
        self:HideGuideText()
    elseif taskType == public_config.TASK_TYPE_MAIN then--主线任务
        -- LoggerHelper.Error("显示主线任务")
        self:ShowGuideText(LanguageDataHelper.CreateContent(77219))
    end
end
