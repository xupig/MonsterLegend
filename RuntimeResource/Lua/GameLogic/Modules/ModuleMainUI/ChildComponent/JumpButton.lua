local ClassTypes = ClassTypes
local JumpButton = Class.JumpButton(ClassTypes.SkillButton)
local StateConfig = GameConfig.StateConfig
local EntityConfig = GameConfig.EntityConfig
local ImageWrapper = UIExtension.ImageWrapper
local UIProgressBar = ClassTypes.UIProgressBar
local XImageFilling = GameMain.XImageFilling

JumpButton.interface = GameConfig.ComponentsConfig.PointableComponent
JumpButton.classPath = "Modules.ModuleMainUI.ChildComponent.JumpButton"

local Up_Scale = Vector3.New(1,1,1)
local Down_Scale = Vector3.New(0.9,0.9,1)

function JumpButton:Awake()
    self.skillPos = 7
    self:InitView()
end

function JumpButton:InitView()
    self._UseTimeNum = self:FindChildGO("Container_UseTime/Text_UseTimeNum")
    self._UseTimeNumPos = self._UseTimeNum.transform.localPosition
    self._UseTimeNumText = self._UseTimeNum:GetComponent("TextMeshWrapper")
    self._UseTimeNum.transform.localPosition = self.HIDE_POSITION
end

function JumpButton:OnDestroy()
    self._imageFilling = nil
end

function JumpButton:OnPointerDown(pointerEventData)
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandPlaySkill, self.skillPos)
    self.transform.localScale = Down_Scale
end

function JumpButton:OnPointerUp(pointerEventData)
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandReleaseSkill, self.skillPos)
    self.transform.localScale = Up_Scale
end

function JumpButton:SetUseTimeActivite(state)
	if state then
		self._UseTimeNum.transform.localPosition = self._UseTimeNumPos
	else
		self._UseTimeNum.transform.localPosition = self.HIDE_POSITION
	end
end
