--主界面子菜单的Item
-- LimitRemindItem.lua
local LimitRemindItem = Class.LimitRemindItem(ClassTypes.BaseComponent)
local GUIManager = GameManager.GUIManager
LimitRemindItem.interface = GameConfig.ComponentsConfig.Component
LimitRemindItem.classPath = "Modules.ModuleMainUI.ChildComponent.LimitRemindItem"

local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale
local TimerHeap = GameWorld.TimerHeap
local StringStyleUtil = GameUtil.StringStyleUtil
local UIComponentUtil = GameUtil.UIComponentUtil
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local stop = Vector3.New(0, 0, 360)
local start = Vector3.New(0, 0, 0)



function LimitRemindItem:Awake()
    self._base.Awake(self)

    self._textTitle = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    self._textLeft = self:GetChildComponent("Text_left", "TextMeshWrapper")
    self._textGo = self:GetChildComponent("Text_Go", "TextMeshWrapper")

    self._getTo = self:FindChildGO("Text_Go")
    self._textGo = self:GetChildComponent("Text_Go", "TextMeshWrapper")
    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject, "Text_Go")
    self._linkTextMesh:SetOnClickCB(function(linkIndex, postion)self:OnGoKillBoss(linkIndex, postion) end)
    local killDesc = '<u><link="CLICK_LINK">' .. LanguageDataHelper.CreateContent(76937) .. '</link></u>'
    self._textGo.text =  StringStyleUtil.GetColorStringWithTextMesh(killDesc,StringStyleUtil.green)
end

function LimitRemindItem:OnGoKillBoss()
    GameWorld.PlaySound(3)
    if self._clickFunc then
        self._clickFunc()
        return
    end
end

function LimitRemindItem:OnEnable()		

end

function LimitRemindItem:OnDisable()

end


function LimitRemindItem:OnDestroy()

    self._base.OnDestroy(self)
end


function LimitRemindItem:SetData(id, text, clickFunc)
    self.gameObject.name = "Button_"..id
    self._clickFunc = clickFunc
    self._textTitle.text = LanguageDataHelper.CreateContent(tonumber(text))
end



function LimitRemindItem:UpdateTimeLeft(timeLeft)
    if self._textLeft == nil then
        self._textLeft = self:GetChildComponent("Text_Time", "TextMeshWrapper")
    end
    
    --LoggerHelper.Error("timeLeft" ..timeLeft)
    if timeLeft > 0 then
        self._timeLeft = timeLeft
        if self._timerId == nil then
            self._timerId = TimerHeap:AddSecTimer(0,1,0,function ()
                local noHour = self._timeLeft < DateTimeUtil.OneHourTime
                self._textTime.text = DateTimeUtil:FormatFullTime(self._timeLeft,false,noHour)
                self._timeLeft = self._timeLeft - 1
                if self._timeLeft <= 0  then
                    self:ClearTimer()
                end
            end)
        end
    else
        self:ClearTimer()
    end
end

function LimitRemindItem:ClearTimer()
    if self._timerId then
        TimerHeap:DelTimer(self._timerId)
        self._timerId = nil
    end
    self._textTime.text = ""
end
