-- FlyButton.lua
local ClassTypes = ClassTypes
--local FlyButton = Class.FlyButton(ClassTypes.BaseLuaUIComponent)
local FlyButton = Class.FlyButton(ClassTypes.SkillButton)
local StateConfig = GameConfig.StateConfig
local EntityConfig = GameConfig.EntityConfig
local ImageWrapper = UIExtension.ImageWrapper
local UIProgressBar = ClassTypes.UIProgressBar
local XImageFilling = GameMain.XImageFilling

local Up_Scale = Vector3.New(1,1,1)
local Down_Scale = Vector3.New(0.9,0.9,1)

FlyButton.interface = GameConfig.ComponentsConfig.PointableComponent
FlyButton.classPath = "Modules.ModuleMainUI.ChildComponent.FlyButton"

function FlyButton:Awake()
    self.flyContainer = self:FindChildGO("Container_Fly")
    self.timeContainer = self:FindChildGO("Container_Time")
    self.timeImage = self:FindChildGO("Container_Time/Image_Bar")
    self._pos = self.transform.localPosition
    self.skillPos = 8
    self.stateCnt = 1
    self.canClicked = false
    self.needShow = false
    self.showState = true
    self:InitStateButton()
    self._imageFilling = self:FindChildGO("Container_Time/Image_Bar"):AddComponent(typeof(XImageFilling))
    self._imageFilling._Damping = 1
    self._onFlyEnergyChange = function() self:OnFlyEnergyChange() end
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_FLY_ENERGY, self._onFlyEnergyChange)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_FLY_MAX_ENERGY, self._onFlyEnergyChange)
    self:OnFlyEnergyChange()
    self._setState = function(state) self:SetState(state) end
    EventDispatcher:AddEventListener(GameEvents.FlyButton_Set_State, self._setState)
    self._onFlySpeedChange = function() self:OnFlySpeedChange() end
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_FLY_SPEED, self._onFlySpeedChange)
    self:OnFlySpeedChange()
end

function FlyButton:InitStateButton()
    self.bgs = {}
    self.bgsPos = {}
    for i = 1,self.stateCnt do
        self.bgs[i] = self:FindChildGO("Container_Fly/Image_Fly"..i)
        self.bgsPos[i] = self.bgs[i].transform.localPosition
        self.bgs[i].transform.localPosition = self.HIDE_POSITION
    end
end

function FlyButton:OnShow()
    local fly_speed = GameWorld.Player().fly_speed or 0
    if fly_speed > 0 then
        self.canClicked = true
    else
        self.canClicked = false
    end
    self:RefreshClickedState()
end

function FlyButton:OnDestroy()
    local entity = GameWorld.Player()
    self._imageFilling = nil
    if entity ~= nil then
        entity:RemovePropertyListener(EntityConfig.PROPERTY_FLY_ENERGY, self._onFlyEnergyChange)
        entity:RemovePropertyListener(EntityConfig.PROPERTY_FLY_MAX_ENERGY, self._onFlyEnergyChange)
        entity:RemovePropertyListener(EntityConfig.PROPERTY_FLY_SPEED, self._onFlySpeedChange)
    end
end

function FlyButton:OnPointerDown(pointerEventData)
    if self.canClicked then
        self:StartFly()
        self.transform.localScale = Down_Scale
    end
end

function FlyButton:OnPointerUp(pointerEventData)
    if self.canClicked then
        self:EndFly()
        self.transform.localScale = Up_Scale
    end
end

function FlyButton:StartFly()
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandPlaySkill, self.skillPos)
end

function FlyButton:EndFly()
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandReleaseSkill, self.skillPos)
end

function FlyButton:OnFlyEnergyChange()
    local entity = GameWorld.Player()
    local fly_max_energy = entity.fly_max_energy or 1
    local fly_energy = entity.fly_energy or 0
    if entity.fly_max_energy == 0 then
        fly_max_energy = 1
    end
    local value = fly_energy / fly_max_energy
    self._imageFilling:SetValue(value)
end

function FlyButton:RefreshClickedState()
    if not self.canClicked then
        self:SetState(1)
    else
        self:SetState(2)
    end
end

function FlyButton:SetState(state)
    do return end
    for i = 1, self.stateCnt do
        self.bgs[i].transform.localPosition = self.HIDE_POSITION
        if state == i then
            self.bgs[i].transform.localPosition = self.bgsPos[i]
        end
    end
end

function FlyButton:OnFlySpeedChange()
    local fly_speed = GameWorld.Player().fly_speed or 0
    if fly_speed > 0 then
        self.needShow = true
    else
        self.needShow = false
    end
    self:RefreshShowState(self.showState)
end

function FlyButton:RefreshShowState(state)
    local fly_speed = GameWorld.Player().fly_speed or 0
    if fly_speed > 0 then
        self.canClicked = true
    else
        self.canClicked = false
    end
    self:RefreshClickedState()
    
    self.showState = state
    if self.needShow == false then
        self.transform.localPosition = self.HIDE_POSITION
    else
        if state then
            self.transform.localPosition = self._pos
        else
            self.transform.localPosition = self.HIDE_POSITION
        end
    end
end