-- CollectButton.lua
local ClassTypes = ClassTypes
local CollectButton = Class.CollectButton(ClassTypes.BaseLuaUIComponent)

CollectButton.interface = GameConfig.ComponentsConfig.PointableDragableComponent
CollectButton.classPath = "Modules.ModuleMainUI.ChildComponent.CollectButton"
local UIProgressBar = ClassTypes.UIProgressBar
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CD_INTERVAL = 50

function CollectButton:Awake()
end

function CollectButton:OnBeginDrag(pointerEventData)
end

function CollectButton:OnDrag(pointerEventData)
end

function CollectButton:OnEndDrag(pointerEventData)
end

function CollectButton:OnPointerUp(pointerEventData)
    --正在cd中就不能交互
    if self._cdPassTime > 0 or self._interruptCd > 0 then
        return
    end
	if self._puFunc ~= nil then
        self._puFunc(pointerEventData)
    end
end

function CollectButton:OnPointerDown(pointerEventData)	
    --正在cd中就不能交互
    if self._cdPassTime > 0 or self._interruptCd > 0 then
        -- LoggerHelper.Error("self._interruptCd"..self._interruptCd)
        -- LoggerHelper.Error("self._cdPassTime"..self._cdPassTime)
        return
    end
	if self._pdFunc ~= nil then
        self._pdFunc(pointerEventData)
    end
end

function CollectButton:SetPointerDownCB(func)
    self._pdFunc = func
end

function CollectButton:SetPointerUpCB(func)
    self._puFunc = func
end

function CollectButton:InitCD()
    self._cdPassTime = 0
    self._cdTotalTime = 0
    self._interruptCd = 0

    self._Cooldown = self:FindChildGO("Image_CoolDown")
    --self._CoolDownPoint = self:FindChildGO("Image_CoolDownPoint")
    --self._CDMask = self:FindChildGO("Image_Up")
    self._CooldownImage = self._Cooldown:GetComponent("ImageWrapper")
    self._CooldownImage.fillAmount = 0
    --self._CDMaskPos = self._CDMask.transform.localPosition
    self._CooldownPos = self._Cooldown.transform.localPosition
    --self._CoolDownPointPos = self._CoolDownPoint.transform.localPosition
    self._TmpPos = Vector3.New(0,0,0)
    
    --self._CDMask:SetActive(true)
    self._Cooldown:SetActive(true)
    --self._CoolDownPoint:SetActive(true)
    --self._CDMask.transform.localPosition = self.HIDE_POSITION
    self._Cooldown.transform.localPosition = self.HIDE_POSITION
    --self._CoolDownPoint.transform.localPosition = self.HIDE_POSITION
    self._txtName = self:GetChildComponent("Text_Name","TextMeshWrapper")
    self._containerIcon = self:FindChildGO("Container_Icon")

    self._containerInterrupt = self:FindChildGO("Container_Interrupt")
    local interruptCooldownImage = self:GetChildComponent("Container_Interrupt/Image_Interrupt","ImageWrapper")
    interruptCooldownImage.fillAmount = 1

    self._containerInterrupt:SetActive(false)

    self._intervalCb = function()
        if self._cdPassTime >= self._cdTotalTime then
            self:CoolDownComplete()
        else
            self:ShowSelfCD()
            self._cdPassTime = self._cdPassTime + CD_INTERVAL
        end
    end

    self._interruptCb = function()
        self:InterruptEnd()
        -- if self._interruptCd >= 1000 then
        --     --LoggerHelper.Error("self._interruptCd"..self._interruptTimerID)
        --     TimerHeap:DelTimer(self._interruptTimerID)
        --     self._containerInterrupt:SetActive(false)
        --     self._interruptCd = 0
        -- else
        --     --LoggerHelper.Error("self._interruptCd111"..self._interruptCd)
        --      local rate = self._interruptCd / 1000
        --      self._interruptCooldownImage.fillAmount = rate
        --      self._interruptCd = self._interruptCd + CD_INTERVAL
        -- end
    end


end

function CollectButton:UpdateInfo(name,icon)
    if name > 0 then
        self._txtName.text = LanguageDataHelper.CreateContent(name)
    else
        self._txtName.text = ""
    end
    if icon > 0 then
        GameWorld.AddIcon(self._containerIcon,icon)
    end
end

--暂时不用
function CollectButton:CDMaskActivite(visiable)
    -- if visiable then
    --     self._CDMask.transform.localPosition = self._CDMaskPos
    -- else
    --     self._CDMask.transform.localPosition = self.HIDE_POSITION
    -- end
end

function CollectButton:SetCoolDownActivite(visiable)
    if visiable then
        --self._CoolDownPoint.transform.localPosition = self._CDMaskPos
        self._Cooldown.transform.localPosition = self._CooldownPos
    else
        --self._CoolDownPoint.transform.localPosition = self.HIDE_POSITION
        self._Cooldown.transform.localPosition = self.HIDE_POSITION
    end
end


function CollectButton:SetCoolDown(totalTime)
    --LoggerHelper.Error("CollectButton:SetCoolDown")
    self:CDMaskActivite(true)
    self:SetCoolDownActivite(true)
    self._cdPassTime = 0
    self._cdTotalTime = totalTime
    self._intervalTimerID = TimerHeap:AddTimer(0, CD_INTERVAL, 0, self._intervalCb)
end

function CollectButton:SetCoolDownCompleteCB(func)
    self._cdcFunc = func
end

function CollectButton:CoolDownComplete()
   --LoggerHelper.Error("CollectButton:CoolDownComplete")
   self:CDReset()
   if self._cdcFunc ~= nil then
        self._cdcFunc()
    end
end

---重置CD
function CollectButton:CDReset()
    if self._intervalTimerID ~= 0 then
        TimerHeap:DelTimer(self._intervalTimerID)
        self._intervalTimerID = 0
    end
    self._cdPassTime = 0
    self._cdTotalTime = 0
    self._CooldownImage.fillAmount = 0
    self:CDMaskActivite(false)
    self:SetCoolDownActivite(false)
end

function CollectButton:ShowSelfCD()
    local rate = self._cdPassTime / self._cdTotalTime
    self._CooldownImage.fillAmount = rate
    -- rate = 1 - rate
    -- local x = self._CoolDownPointPos.x - 40 * math.sin(math.rad(360 * rate))
    -- local y = self._CoolDownPointPos.y - 40 * ( 1 - math.cos(math.rad(360 * rate)))
    -- self._TmpPos.x = x
    -- self._TmpPos.y = y
    -- self._TmpPos.z = self._CoolDownPointPos.z
    --self._CoolDownPoint.transform.localPosition = self._TmpPos
end

--被打断后
function CollectButton:Interrupt()
    --LoggerHelper.Error("CollectButton:Interrupt()",true)
    self:CDReset()
    self._containerInterrupt:SetActive(true)
    self._interruptTimerID = TimerHeap:AddSecTimer(0.5,0,0, self._interruptCb)
    self._interruptCd = 1
end

function CollectButton:InterruptEnd()
    TimerHeap:DelTimer(self._interruptTimerID)
    self._containerInterrupt:SetActive(false)
    self._interruptCd = 0
end