-- BuffIcon.lua
--主界面头像上的buff图标
local BuffIcon = Class.BuffIcon(ClassTypes.BaseComponent)
BuffIcon.interface = GameConfig.ComponentsConfig.Component
BuffIcon.classPath = "Modules.ModuleMainUI.ChildComponent.BuffIcon"

function BuffIcon:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self._btn = self:FindChildGO("Button")
end

function BuffIcon:UpdateItem(iconId)
	GameWorld.AddIcon(self.gameObject, iconId)
end