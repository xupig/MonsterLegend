-- LockButton.lua
local ClassTypes = ClassTypes
local LockButton = Class.LockButton(ClassTypes.BaseLuaUIComponent)

LockButton.interface = GameConfig.ComponentsConfig.PointableComponent
LockButton.classPath = "Modules.ModuleMainUI.ChildComponent.LockButton"

local Up_Scale = Vector3.New(1,1,1)
local Down_Scale = Vector3.New(0.9,0.9,1)

function LockButton:Awake()
    self._iconImage = self:FindChildGO("Image_Icon")
    self._clickedImage = self:FindChildGO("Image_Icon/Image_Clicked")
    self._clickedImage:SetActive(false)
end

function LockButton:OnDestroy()
end

function LockButton:OnPointerDown(pointerEventData)
    self._clickedImage:SetActive(true)
    self._iconImage.transform.localScale = Down_Scale
    GameWorld.SwitchLockTarget()
end

function LockButton:OnPointerUp(pointerEventData)
    self._clickedImage:SetActive(false)
    self._iconImage.transform.localScale = Up_Scale
end