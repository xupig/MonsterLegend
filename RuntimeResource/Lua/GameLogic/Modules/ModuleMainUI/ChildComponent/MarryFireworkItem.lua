-- MarryFireworkItem.lua
local MarryFireworkItem = Class.MarryFireworkItem(ClassTypes.UIListItem)

require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

MarryFireworkItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
MarryFireworkItem.classPath = "Modules.ModuleMainUI.ChildComponent.MarryFireworkItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local bagData = PlayerManager.PlayerDataManager.bagData
local BagManager = PlayerManager.BagManager
local PanelsConfig = GameConfig.PanelsConfig
local public_config = GameWorld.public_config

function MarryFireworkItem:Awake()
    self._base.Awake(self)
    self._iconContainer = self:AddChildLuaUIComponent("Container_Icon/Container_ItemIcon", ItemGridEx)
    self._iconContainer:ActivateTips()
    self._iconContainer:SetUpdateCountCB(function()self:UpdateConfirmText() end)
    self._confirmBtn = self:FindChildGO("Container_Icon/Button_Confirm")
    self._textConfirmBtn = self:GetChildComponent("Container_Icon/Button_Confirm/Text", 'TextMeshWrapper')
    self._csBH:AddClick(self._confirmBtn, function()self:ConfirmBtnClick() end)
end

function MarryFireworkItem:OnDestroy()
    self._itemManage = nil
    self._icon = nil
    self._iconContainer = nil
    self._confirmBtn = nil
    self._base.OnDestroy(self)
end

function MarryFireworkItem:UpdateConfirmText()
    local count, haveCount = self._iconContainer:GetCount()
    -- LoggerHelper.Log("Ash: MarryFireworkItem UpdateConfirmText:" .. haveCount)
    if haveCount == 0 then
        self._textConfirmBtn.text = LanguageDataHelper.CreateContent(523)
    else
        self._textConfirmBtn.text = LanguageDataHelper.CreateContent(516)
    end
end

function MarryFireworkItem:ConfirmBtnClick()
    local count, haveCount = self._iconContainer:GetCount()
    if haveCount == 0 then
        local data = {["id"] = EnumType.ShopPageType.Marriage}
        GUIManager.ShowPanel(PanelsConfig.Shop, data)
    else
        local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._itemId)
        BagManager:RequsetUse(pos, 1)
    end
end

--override
function MarryFireworkItem:OnRefreshData(data)
    if data then
        local itemId = data
        self._itemId = itemId
        self._iconContainer:UpdateData(itemId, 1)
        self:UpdateConfirmText()
    end
end
