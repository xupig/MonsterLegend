-- RewardItem.lua
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local RewardItem = Class.RewardItem(ClassTypes.UIListItem)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
RewardItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
RewardItem.classPath = "Modules.ModuleMainUI.ChildComponent.RewardItem"

function RewardItem:Awake()
    self._base.Awake(self)
    self._itemManager = ItemManager()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._icon:ActivateTipsById()
end

function RewardItem:OnDestroy()
    self._itemManage = nil 
    self._icon = nil
    self._itemContainer = nil
    self._base.OnDestroy(self)
end

--override
function RewardItem:OnRefreshData(data)
    local iconId = data[1]
    local num = data[2]
    self._icon:SetItem(iconId, num)
end
