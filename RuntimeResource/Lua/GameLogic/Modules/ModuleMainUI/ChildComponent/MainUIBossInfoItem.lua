--主界面已跟踪任务上任务的ListItem
-- MainUIBossInfoItem.lua
local MainUIBossInfoItem = Class.MainUIBossInfoItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local UIToggle = ClassTypes.UIToggle
local public_config = GameWorld.public_config
local FontStyleHelper = GameDataHelper.FontStyleHelper
MainUIBossInfoItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
MainUIBossInfoItem.classPath = "Modules.ModuleMainUI.ChildComponent.MainUIBossInfoItem"

function MainUIBossInfoItem:Awake()
    self._base.Awake(self)
    self._imageBG = self:FindChildGO("Background")
    self._imageBGActive = self:FindChildGO("BackgroundActive")
    self._textHead = self:GetChildComponent("Text_Head", "TextMeshWrapper")
    self._textDead = self:GetChildComponent("Text_Dead", "TextMeshWrapper")
    self._textDeadGO = self:FindChildGO("Text_Dead")
    self._textAliveGO = self:FindChildGO("Text_Alive")
    self._peaceImage = self:FindChildGO("Image_Peace")
    self._peaceImage:SetActive(false)
-- self._toggleTrackMark = UIToggle.AddToggle(self.gameObject, "Toggle_TrackMark")
-- self._toggleTrackMark:SetOnValueChangedCB(function(state)self:OnClickTrackMark(state) end)
end

function MainUIBossInfoItem:OnDestroy()
    self._imageBG = nil
    self._imageBGActive = nil
    self._textHead = nil
    self._textDead = nil
    self._textDeadGO = nil
    self._textAliveGO = nil
    self._base.OnDestroy(self)
end

function MainUIBossInfoItem:UpdateCountDown()
    -- LoggerHelper.Error("OnClickBoss")
    local data = self._data -- WorldBossManager:GetBossInfo(data)
    if data then
        local isDead = data:IsDead()
        if self._lastIsDead ~= isDead then
            self:SetDeadOrAlive(isDead)
        -- self._textDead.text = "已刷新"
        end
        self._lastIsDead = isDead
        if isDead then
            local leftTime = data:GetRebornTime() - DateTimeUtil:GetServerTime()
            self._textDead.text = DateTimeUtil:FormatFullTime(leftTime)
        end
    end
end

function MainUIBossInfoItem:SetDeadOrAlive(isDead)
    self._textDeadGO:SetActive(isDead)
    self._textAliveGO:SetActive(not isDead)
end

--override
function MainUIBossInfoItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        local bossInfo = self._data -- WorldBossManager:GetBossInfo(data)
        if bossInfo.IsGodIslandData then
            local monsterType = bossInfo:GetMonsterType()
            if monsterType == 1 then
                self._textHead.text = bossInfo:GetNameChinese() .. "Lv." .. tostring(bossInfo:GetLevel())
            else
                self._textHead.text = bossInfo:GetNameChinese() .. ": " .. tostring(bossInfo:GetLeftCount())
            end
        else
            self._textHead.text = tostring(LanguageDataHelper.GetContent(bossInfo:GetBossName())) .. "Lv." .. tostring(bossInfo:GetBossLevel())
        end
        self:SetItemActive(data:GetIsSelected())
        self:UpdateCountDown()
        self._peaceImage:SetActive(bossInfo:GetPeaceShow()==1)
    else
        self._textHead.text = ""
        self._textDead.text = ""
    end
    self:SetItemActive(false)
end

function MainUIBossInfoItem:SetItemActive(isActive)
    self._imageBG:SetActive(isActive == false)
    self._imageBGActive:SetActive(isActive)
end
