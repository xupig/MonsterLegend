-- OpenActivityListItem.lua
local OpenActivityListItem = Class.OpenActivityListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
OpenActivityListItem.interface = GameConfig.ComponentsConfig.Component
OpenActivityListItem.classPath = "Modules.ModuleMainUI.ChildComponent.OpenActivityListItem"
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RedPointManager = GameManager.RedPointManager

function OpenActivityListItem:Awake()
    self._nameText = self:GetChildComponent("Image_Bg/Text_Name",'TextMeshWrapper') 
    self._imgRed = self:FindChildGO("Image_Bg/Image_Red")
    self._imgRed:SetActive(false)
    self._goButton = self:FindChildGO("Image_Bg")
    self._csBH:AddClick(self._goButton, function() self:OnGoButtonClick() end)
end

function OpenActivityListItem:OnDestroy() 

end


function OpenActivityListItem:OnRefreshData(data)    
    if data then
        --LoggerHelper.Error("OpenRankView:SetValue==1"..PrintTable:TableToStr(data))
        self._data = data
        local follow = self._data
        local isShowRed = false
        if follow[2][1] ~= 0 then
            if follow[2][1] == GameConfig.EnumType.OpenActivityType.Ring then
                EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_service_activity_ring_button")
            end
            isShowRed = RedPointManager:GetRedPointDataByFirstId(follow[2][1]):GetRedPoint()
        end
    
        if follow[2][2] ~= 0 then
            isShowRed = RedPointManager:GetRedPointDataByFuncId(follow[2][2]):GetRedPoint()
        end
        self._imgRed:SetActive(isShowRed)
        self._nameText.text = LanguageDataHelper.CreateContent(data[2][3])
    end
end

function OpenActivityListItem:OnGoButtonClick()
    GameManager.GUIManager.ClosePanel(PanelsConfig.OpenActivity)
    local follow = self._data
    if follow[2][1] ~= 0 then
        GUIManager.ShowPanelByPanelId(follow[2][1])
    end

    if follow[2][2] ~= 0 then
        GUIManager.ShowPanelByFunctionId(follow[2][2])
    end
    --EventDispatcher:TriggerEvent(GameEvents.ON_UPGRADE_CLICK, follow)
end