--主界面子菜单的ListItem
-- SubMenuItem.lua
local SubMenuItem = Class.SubMenuItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local UIToggle = ClassTypes.UIToggle
local public_config = GameWorld.public_config
SubMenuItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
SubMenuItem.classPath = "Modules.ModuleMainUI.ChildComponent.SubMenuItem"

function SubMenuItem:Awake()
    self._base.Awake(self)
    self._btnMenu = self:FindChildGO("StateButton_Menu")
    self._btnMenuUp = self:FindChildGO("StateButton_Menu/Up/Container_Up")
    self._btnMenuDown = self:FindChildGO("StateButton_Menu/Down/Container_Down")
    -- self._textButtonMenu = self:GetChildComponent("Button_Menu/Text", "TextMeshWrapper")
end

function SubMenuItem:OnDestroy()
    self._btnMenuUp = nil
    self._btnMenuDown = nil
    self._btnMenu = nil
    self._base.OnDestroy(self)
end

--override
function SubMenuItem:OnRefreshData(data)
    self._data = data
    self._csBH:RemoveClick(self._btnMenu)
    if data ~= nil then
        GameWorld.AddIcon(self._btnMenuUp, data["up"])
        GameWorld.AddIcon(self._btnMenuDown, data["down"])
        -- self._textButtonMenu.text = LanguageDataHelper.CreateContent(data["name"])
        self._csBH:AddClick(self._btnMenu, data["cb"])
    else
        self._textButtonMenu.text = ""
    end
end
