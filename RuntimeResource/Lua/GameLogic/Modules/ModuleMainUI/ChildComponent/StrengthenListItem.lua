-- StrengthenListItem.lua
local StrengthenListItem = Class.StrengthenListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
StrengthenListItem.interface = GameConfig.ComponentsConfig.Component
StrengthenListItem.classPath = "Modules.ModuleMainUI.ChildComponent.StrengthenListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function StrengthenListItem:Awake()
    self._nameText = self:GetChildComponent("Image_Bg/Text_Name",'TextMeshWrapper') 
    self._goButton = self:FindChildGO("Image_Bg")
    self._csBH:AddClick(self._goButton, function() self:OnGoButtonClick() end)
end

function StrengthenListItem:OnDestroy() 

end

--override
function StrengthenListItem:OnRefreshData(data)  
    self._data = data  
    self._cb = data:GetStrengthCallBack()
    self._nameText.text = LanguageDataHelper.CreateContent(data:GetStrengthTextId())
end

function StrengthenListItem:OnGoButtonClick()
    self._cb(self._data:GetStrengthData())
    EventDispatcher:TriggerEvent(GameEvents.STRENGTHEN_LIST_ITEM_CLICK)
end