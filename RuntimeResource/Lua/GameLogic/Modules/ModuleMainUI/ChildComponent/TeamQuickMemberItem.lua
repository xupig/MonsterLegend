-- TeamQuickMemberItem.lua
local TeamQuickMemberItem = Class.TeamQuickMemberItem(ClassTypes.BaseComponent)

TeamQuickMemberItem.interface = GameConfig.ComponentsConfig.PointableComponent
TeamQuickMemberItem.classPath = "Modules.ModuleMainUI.ChildComponent.TeamQuickMemberItem"
local UIProgressBar = ClassTypes.UIProgressBar

require 'UIComponent.Extend.PlayerHead'
local PlayerHead = ClassTypes.PlayerHead
local RoleDataHelper = GameDataHelper.RoleDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local TeamManager = PlayerManager.TeamManager
local GUIManager = GameManager.GUIManager
local StringStyleUtil = GameUtil.StringStyleUtil
local SceneConfig = GameConfig.SceneConfig

function TeamQuickMemberItem:Awake()
	self._base.Awake(self)
	self:Init()
end

function TeamQuickMemberItem:Init()
	self._playerHead = self:AddChildLuaUIComponent('Container_Head', PlayerHead)
	--self._imgLeaderIcon = self:FindChildGO("Image_LeaderIcon")

	self._hpComp = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar_HP")
    self._hpComp:SetProgress(1)
    self._hpComp:ShowTweenAnimate(true)
    self._hpComp:SetMutipleTween(false)
	self._hpComp:SetIsResetToZero(false)
	
	self._leaderImage = self:FindChildGO("Container_Head/Image_Leader")
    self._tagText = self:GetChildComponent("Container_Head/Text_Tag","TextMeshWrapper")

    self._containerInvite = self:FindChildGO("Container_Invite")

	self._txtPlayerLevel = self:GetChildComponent("Container_Head/Text_Level","TextMeshWrapper")
	
	self._locationContainer = self:FindChildGO("Container_Location")
	self._locationText = self:GetChildComponent("Container_Location/Container_Arrow/Image_Tail/Text_Des","TextMeshWrapper")

    -- self._btnFollow = self:FindChildGO("Button_Follow")
    -- self._csBH:AddClick(self._btnFollow,function() self:OnFollow() end)
    self._headClickCb = function(posVec)self:OnViewInfoClick(posVec) end
end

function TeamQuickMemberItem:OnDestroy()
end

function TeamQuickMemberItem:OnPointerDown(pointerEventData)
	if self._data == nil and self._showInvite then
		GUIManager.ShowPanel(PanelsConfig.Team,  {firstTabIndex = 2, invite = true})
		--EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TEAM_QUICK_MEMBER_CLICK)
	end
end

function TeamQuickMemberItem:OnPointerUp(pointerEventData)
end

--override
function TeamQuickMemberItem:OnRefreshData(data,showInvite)
	self._data = data
	self._showInvite = showInvite
	if data == nil then
		self._containerInvite:SetActive(self._showInvite)
		self._hpComp:SetActive(false)
		self._playerHead:SetActive(false)
		self._leaderImage:SetActive(false)
		self._tagText.textrt = ""
		self._locationContainer:SetActive(false)
	else
		self._containerInvite:SetActive(false)
		self._hpComp:SetActive(true)
		self._playerHead:SetActive(true)

		self._leaderImage:SetActive(data:GetIsCaptain())
		if data:GetOnline() == 0 then--离线
			self._tagText.text = LanguageDataHelper.CreateContent(616)
			self._locationContainer:SetActive(false)
		else
			local myMapId = TeamManager:GetMyMapId()
			local mapId = data:GetMapId()
			local sceneType = MapDataHelper.GetSceneType(mapId) 
			if myMapId == mapId then--附近
				if sceneType == SceneConfig.SCENE_TYPE_MAIN or sceneType == SceneConfig.SCENE_TYPE_WILD then
					self._locationContainer:SetActive(false)
				else
					self._locationContainer:SetActive(true)
					self._locationText.text = LanguageDataHelper.CreateContent(MapDataHelper.GetChinese(mapId))
				end
				self._tagText.text = LanguageDataHelper.CreateContent(614)
			else--远离
				if data:GetDBId() ~= GameWorld.Player().dbid then
					--队员
					if sceneType ~= SceneConfig.SCENE_TYPE_MAIN and sceneType ~= SceneConfig.SCENE_TYPE_WILD then
						self._locationContainer:SetActive(true)
						self._locationText.text = LanguageDataHelper.CreateContent(MapDataHelper.GetChinese(mapId))
					else
						self._locationContainer:SetActive(false)
					end
				else
					--自己
					self._locationContainer:SetActive(false)
				end
				self._tagText.text = LanguageDataHelper.CreateContent(615)
			end
		end

		local headIcon = RoleDataHelper.GetHeadIconByVocation(data:GetVocation())
		local lvStr = StringStyleUtil.GetLevelString(data:GetLevel())
		self._playerHead:SetPlayerHeadData(data:GetName(), lvStr,headIcon,self._headClickCb,data:GetOnline())
		-- if data.isCaptain == true then
		-- 	self._imgLeaderIcon:SetActive(true)
		-- 	if GameWorld.Player().team_status < 2 then
		-- 		self._btnFollow:SetActive(true)
		-- 	else
		-- 		self._btnFollow:SetActive(false)
		-- 	end
		-- else
		-- 	self._imgLeaderIcon:SetActive(false)
		-- 	self._btnFollow:SetActive(false)
		-- end
		self:UpdateHP()
	end
end

function TeamQuickMemberItem:UpdateHP()
	if self._data then
		self._hpComp:SetProgressByValue(self._data:GetHP(),self._data:GetMaxHP())
	end
end

function TeamQuickMemberItem:UpdateLevel()
	if self._data then
		self._txtPlayerLevel.text = StringStyleUtil.GetLevelString(self._data:GetLevel())
	end
end

function TeamQuickMemberItem:OnViewInfoClick(posVec)
    --LoggerHelper.Log("TeamQuickMemberItem:OnViewInfoClick"..PrintTable:TableToStr(posVec))

    -- local pos = Vector2.New(posVec["x"]+50, posVec["y"]-600)
	--LoggerHelper.Log("FriendChatSecItem:OnViewInfoClickpos"..PrintTable:TableToStr(pos))
	
	--显示玩家TIPS信息
    -- local d = {}
    -- d.playerName = self._data:GetName()
    -- d.dbid = self._data:GetDBId()
    -- d.teamInfo = true
    -- d.listPos = posVec
	-- GUIManager.ShowPanel(PanelsConfig.PlayerInfoMenu, d)
	GUIManager.ShowPanel(PanelsConfig.Team,{firstTabIndex = 2})
	
end

-- function TeamQuickMemberItem:OnFollow()
-- 	TeamManager:RequestAcceptCall()
-- end