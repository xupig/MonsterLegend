-- SkillButton.lua
local ClassTypes = ClassTypes
local SkillButton = Class.SkillButton(ClassTypes.BaseLuaUIComponent)

SkillButton.interface = GameConfig.ComponentsConfig.PointableComponent
SkillButton.classPath = "Modules.ModuleMainUI.ChildComponent.SkillButton"

local GUIManager = GameManager.GUIManager
local TimerHeap = GameWorld.TimerHeap
local Color = require "UnityEngine.Color"
local CD_INTERVAL = 50
local math = math
local Time = Time
local PointerInputModule = UnityEngine.EventSystems.PointerInputModule
local EventSystem = UnityEngine.EventSystems.EventSystem
local RectTransformUtility = UnityEngine.RectTransformUtility
local Locater = GameMain.Locater
local XButtonHitArea = GameMain.XButtonHitArea
local SkillDataHelper = GameDataHelper.SkillDataHelper
local LuaGameState = GameWorld.LuaGameState

local MAX_DIS = 100
local color = Color(1,1,1,1)
local Up_Scale = Vector3.New(1,1,1)
local Down_Scale = Vector3.New(0.9,0.9,1)

function SkillButton:Awake()
    self.skillPos = 0	
    self._skillIcon = self:FindChildGO("Container_Icon")
    self._CDTime = self:FindChildGO("Text_CDTime")
	self._CDMask = self:FindChildGO("Image_Up")
	self._CDMaskImage = self._CDMask:GetComponent("ImageWrapper")
	self._LockIcon = self:FindChildGO("Image_Lock")
	self._CooldownText = self._CDTime:GetComponent("TextMeshWrapper")
	self._lockTipsText = self:GetChildComponent("Image_Lock/Text_Tips", "TextMeshWrapper")
	self._cdRemainTime = 0
	self._cdTotalTime = 0
	self._commonCD = 0
	self._intervalTimerID = 0
	self._commonCDTimerID = 0
	self.iconId = 0
	self._isHide = false
	self._isLocked = true

	self._CDMaskPos = self._CDMask.transform.localPosition
	self._CDTimePos = self._CDTime.transform.localPosition
	self._LockIconPos = self._LockIcon.transform.localPosition
	self._skillIconPos = self._skillIcon.transform.localPosition
	
	self._rectTransform = self:GetComponent('RectTransform')
	self._width = self._rectTransform.sizeDelta.x
	self._height = self._rectTransform.sizeDelta.y

	self:Init()
end

function SkillButton:OnDestroy() 
    self._skillIcon = nil
	GameWorld.LateUpdate:Remove(self.LateUpdate,self)
	self:RemoveTimer()
end

function SkillButton:LateUpdate()
    --由于EventSystem的响应顺序在Update之前，所以需要在LateUpdate的时候处理
end

function SkillButton:Init()
	self._CDMask:SetActive(true)
	self._CDTime:SetActive(true)
	self._CDMask.transform.localPosition = self.HIDE_POSITION
	self._CDTime.transform.localPosition = self.HIDE_POSITION
	self._LockIcon.transform.localPosition = self.HIDE_POSITION
end
--判断是否可点击
function SkillButton:IsCanClicked()
	if self._isLocked then
		return false
	end
	if self._commonCD ~= 0 then
		return false
	end
	if self._cdTotalTime ~= 0 then
		local showText = LanguageDataHelper.GetContent(24998)
        GUIManager.ShowText(2, showText)
		return false
	end
	return true
end

function SkillButton:RefreshSkillStickData(rect,center)
	self._stickContainerRect = rect
	self._center = center
end

function SkillButton:InArea(fingerPos)
	if fingerPos.x >= self._width*-0.5 and fingerPos.x <= self._width*0.5 and fingerPos.y >= self._height*-0.5 and fingerPos.y <= self._height*0.5 then
		return true
	end
	return false
end

function SkillButton:OnPointerDown(pointerEventData)
	EventDispatcher:TriggerEvent(GameEvents.SkillControlPanel_SetCurSkillSlot, self.skillPos)
	if self:IsCanClicked() then
		self.transform.localScale = Down_Scale
		--EventDispatcher:TriggerEvent(GameEvents.PlayerCommandPlaySkill, self.skillPos)
	end
end

function SkillButton:OnPointerUp(pointerEventData)
	if LuaGameState.skillDragState == 1 then
		--拖动时不响应点击
		return
	end
	if self:IsCanClicked() then
		EventDispatcher:TriggerEvent(GameEvents.PlayerCommandPlaySkill, self.skillPos)
	end
	EventDispatcher:TriggerEvent(GameEvents.PlayerCommandReleaseSkill, self.skillPos)
	self.transform.localScale = Up_Scale
end

function SkillButton:OnBeginDrag(pointerEventData)

end

function SkillButton:OnDrag(pointerEventData)
	if self._isDraging ~= true then 
        return
    end 
    if pointerEventData.pointerId >= 0 or pointerEventData.pointerId == PointerInputModule.kMouseLeftId
    then
        self:OnDragStick(pointerEventData)
    end
end

function SkillButton:OnEndDrag(pointerEventData)
	if self._isDraging ~= true then 
        return
    end 
end

function SkillButton:OnDragStick(pointerEventData)

end

function SkillButton:ShowLockIcon(show, slot)
	if show then
		self._LockIcon.transform.localPosition = self._LockIconPos
	else
		self._LockIcon.transform.localPosition = self.HIDE_POSITION
	end
	self._isLocked = show
end

function SkillButton:SetIcon(iconID)
	self.iconId = iconID
    if iconID == 0 then
		self:HideIcon(true)
	else
		self:HideIcon(false)
		GameWorld.AddIcon(self._skillIcon, iconID, nil)
	end
end

function SkillButton:HideIcon(state)
	if state then
		self._skillIcon.transform.localPosition = self.HIDE_POSITION
	else
		self._skillIcon.transform.localPosition = self._skillIconPos
	end
end

--amount是填充数0-1,text是cd时间（整数）
function SkillButton:CoolDown(amount,text)
    if amount == 0 or text == 0 then
        self:CoolDownActivite(false)
    end
	self._CDMaskImage.fillAmount = amount
end

function SkillButton:CDMaskActivite(visiable)
	if visiable then
		self._CDMask.transform.localPosition = self._CDMaskPos
	else
		self._CDMask.transform.localPosition = self.HIDE_POSITION
	end
end

function SkillButton:SetCoolDownActivite(visiable)
	if visiable then
		self._Cooldown.transform.localPosition = self._CooldownPos
	else
		self._Cooldown.transform.localPosition = self.HIDE_POSITION
	end
end

function SkillButton:SetCDTimeActivite(visiable)
	if visiable then
		self._CDTime.transform.localPosition = self._CDTimePos
	else
		self._CDTime.transform.localPosition = self.HIDE_POSITION
	end
end

function SkillButton:SetCoolDown(time,totalTime,roundTime)
	if time < 0 then 
		self:SetCommonCd(-time)
	elseif time == 0 then
		if roundTime ~= 0 then
			self:RefreshCD(time,totalTime,roundTime)
		else
			self:CDReset()
		end
	elseif time > 1000 then
		self:RefreshCD(time,totalTime,roundTime)
	end
end

function SkillButton:SetLifeTime(time,totalTime,roundTime)
	if time == 0 then
		self:LifeTimeReset()
	elseif time > 1000 then
		self:RefreshLifeTime(time,totalTime,roundTime)
	end
end

function SkillButton:LifeTimeReset()
	if self._intervalTimerIDLifeTime ~= 0 then
		TimerHeap:DelTimer(self._intervalTimerIDLifeTime)
		self._intervalTimerIDLifeTime = 0
	end
	self._lifeTimeRemainTime = 0
	self._lifeTimeTotalTime = 0
	self._CooldownImage.fillAmount = 0
	self:SetCoolDownActivite(false)
end

function SkillButton:RefreshLifeTime(time,totalTime)
	self:RemoveLifeTimer()
	self._lifeTimeRemainTime = time
	self._lifeTimeTotalTime = totalTime
	self:RefreshNormalLifeTime()
end

function SkillButton:RefreshNormalLifeTime()
	self:SetCoolDownActivite(true)
	self:ShowSelfLifeTime()
	self:RemoveLifeTimer()
	self._intervalTimerIDLifeTime = TimerHeap:AddTimer(0, CD_INTERVAL, 0, 
		function()
			self._lifeTimeRemainTime = self._lifeTimeRemainTime - CD_INTERVAL
			if self._lifeTimeRemainTime <= 0 then
				self:LifeTimeReset()
			else
				self:ShowSelfLifeTime()
			end
        end
	)
end

function SkillButton:ShowSelfLifeTime()
	local cdText = math.ceil(self._lifeTimeRemainTime * 0.001)
	local rate = self._lifeTimeRemainTime / self._lifeTimeTotalTime
	self._CooldownImage.fillAmount = rate
end

function SkillButton:CDReset()
	if self._intervalTimerID ~= 0 then
		TimerHeap:DelTimer(self._intervalTimerID)
		self._intervalTimerID = 0
	end
	self._cdRemainTime = 0
	self._cdTotalTime = 0
	self._commonCD = 0
	self._CDMaskImage.fillAmount = 0
	self:CDMaskActivite(false)
	self:SetCDTimeActivite(false)
end

function SkillButton:SetCommonCd(time)
	self._commonCD = time
	self:RemoveCDTimer()
	self._CDMaskImage.fillAmount = 1
	self:CDMaskActivite(true)
	self:SetCDTimeActivite(false)
	self._intervalTimerID = TimerHeap:AddTimer(time, 0, 0, slot(self.CDReset, self))
end

function SkillButton:RefreshCD(time,totalTime,roundTime)
	self._round = roundTime
	self:RemoveCDTimer()
	self._cdRemainTime = time
	self._cdTotalTime = totalTime
	self:RefreshNormalCD()
end

function SkillButton:RefreshNormalCD()
	self:CDMaskActivite(true)
	self:SetCDTimeActivite(true)
	self:ShowSelfCD()
	self._intervalTimerID = TimerHeap:AddTimer(0, CD_INTERVAL, 0, 
		function()
			self._cdRemainTime = self._cdRemainTime - CD_INTERVAL
			if self._cdRemainTime <= 0 then
				self:CDReset()
			else
				self:ShowSelfCD()
			end
        end
	)
end

function SkillButton:ShowSelfCD()
	local cdText = math.ceil(self._cdRemainTime * 0.001)
	local rate = self._cdRemainTime / self._cdTotalTime
	self._CooldownText.text = cdText
	self._CDMaskImage.fillAmount = rate
end

function SkillButton:RemoveCDTimer()
	if self._intervalTimerID ~= 0 then		
		TimerHeap:DelTimer(self._intervalTimerID)
		self._intervalTimerID = 0
	end
end

function SkillButton:RemoveLifeTimer()
	if self._intervalTimerIDLifeTime ~= 0 then		
		TimerHeap:DelTimer(self._intervalTimerIDLifeTime)
		self._intervalTimerIDLifeTime = 0
	end
end

function SkillButton:RemoveTimer()
	self:RemoveLifeTimer()
	self:RemoveCDTimer()
end