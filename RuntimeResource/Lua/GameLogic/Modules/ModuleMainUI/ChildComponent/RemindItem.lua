--主界面子菜单的Item
-- RemindItem.lua
local RemindItem = Class.RemindItem(ClassTypes.BaseComponent)
-- local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
RemindItem.interface = GameConfig.ComponentsConfig.Component
RemindItem.classPath = "Modules.ModuleMainUI.ChildComponent.RemindItem"

local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

local UIComponentUtil = GameUtil.UIComponentUtil
local stop = Vector3.New(0, 0, 360)
local start = Vector3.New(0, 0, 0)


function RemindItem:Awake()
    self._base.Awake(self)
    self._imageIcon = self:FindChildGO("Image_Icon")
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._csBH:AddClick(self.gameObject, function()self:OnBtnClick() end)

    self._imgRedPoint = self:FindChildGO("Image_RedPoint")
    self._imgRedPoint:SetActive(false)
    self._updateRedPointFunc = function(flag) self:SetRedPoint(flag) end

    self._An = self.gameObject:AddComponent(typeof(XGameObjectTweenScale))
end

function RemindItem:OnEnable()		
	if self._redpointData then
        self._redpointData:SetUpdateRedPointFunc(self._updateRedPointFunc)
	end
end

function RemindItem:OnDisable()
	if self._redpointData then 
        self._redpointData:SetUpdateRedPointFunc(nil)
	end
end

function RemindItem:StartScale()
    self._An.IsTweening = false
    self._An:SetFromToScale(1,0.92,0.35,4,nil)
end

function RemindItem:StopScale()
    self._An.IsTweening = true
end

function RemindItem:OnDestroy()
    self._imageIcon = nil
    self._textName = nil
    self._base.OnDestroy(self)
end

function RemindItem:SetLightAnimate(lightItem)
    self._lightItem = lightItem
    self._lightItem.transform:SetParent(self.gameObject.transform)
    self._lightItem.transform.anchoredPosition = Vector3(0,0,0)
    local light = UIComponentUtil.FindChild(self._lightItem,"Image_Light")
    self._lightRotation = light:GetComponent(typeof(XGameObjectTweenRotation))
    if self._lightRotation == nil then
        self._lightRotation = light:AddComponent(typeof(XGameObjectTweenRotation))
    end
    self._lightRotation.IsTweening = true
    self._lightRotation:SetFromToRotation(start, stop, 2, 2,nil)
end

function RemindItem:GetLightAnimate()
    if self._lightRotation then
        self._lightRotation.IsTweening = false
    end
    return self._lightItem
end

function RemindItem:CleanLight()
    self._lightItem = nil
    self._lightRotation = nil
end

function RemindItem:OnBtnClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_main_menu_button_"..self._cfgId)
    -- LoggerHelper.Log("Ash: OnBtnClick: " .. PrintTable:TableToStr(self._panelNames))
    GameWorld.PlaySound(3)
    if self._clickFunc then
        self._clickFunc()
        return
    end
end

function RemindItem:SetRedPointData(redpointData)
    if redpointData ~= nil then
        self._redpointData = redpointData
		self._redpointData:SetUpdateRedPointFunc(self._updateRedPointFunc)
	end
end

function RemindItem:RemoveRedPointListener()
    if self._redpointData ~= nil then
        self._redpointData:SetUpdateRedPointFunc(nil)
        self._redpointData = nil
	end
end

function RemindItem:SetData(id, iconId, text, clickFunc)
    self._cfgId = id
    self.gameObject.name = "Button_"..id
    self._clickFunc = clickFunc
    GameWorld.AddIcon(self._imageIcon, iconId, nil, true)
    -- LoggerHelper.Log("Ash: SetData: " .. iconId .. " " .. text .. " " .. PrintTable:TableToStr(panelNames))
    self._textName.text = text
end

function RemindItem:SetRedPoint(isActive)
    self._imgRedPoint:SetActive(isActive)
end