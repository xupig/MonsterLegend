--主界面已跟踪任务上任务的ListItem
-- MainUIEliteInfoItem.lua
local MainUIEliteInfoItem = Class.MainUIEliteInfoItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local UIToggle = ClassTypes.UIToggle
local public_config = GameWorld.public_config
local FontStyleHelper = GameDataHelper.FontStyleHelper
MainUIEliteInfoItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
MainUIEliteInfoItem.classPath = "Modules.ModuleMainUI.ChildComponent.MainUIEliteInfoItem"

function MainUIEliteInfoItem:Awake()
    self._base.Awake(self)
    self._imageBG = self:FindChildGO("Background")
    self._imageBGActive = self:FindChildGO("BackgroundActive")
    self._textHead = self:GetChildComponent("Text_Head", "TextMeshWrapper")
end

function MainUIEliteInfoItem:OnDestroy()
    self._imageBG = nil
    self._imageBGActive = nil
    self._textHead = nil
    self._base.OnDestroy(self)
end

--override
function MainUIEliteInfoItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        local eliteInfo = self._data
        self._textHead.text = eliteInfo:GetEliteName() .. "    Lv." .. tostring(eliteInfo:GetEliteLevel())
    else
        self._textHead.text = ""
    end
    self:SetItemActive(false)
end

function MainUIEliteInfoItem:SetItemActive(isActive)
    self._imageBG:SetActive(isActive == false)
    self._imageBGActive:SetActive(isActive)
end
