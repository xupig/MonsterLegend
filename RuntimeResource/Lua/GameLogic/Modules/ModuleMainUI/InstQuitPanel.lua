-- InstQuitPanel.lua
local InstQuitPanel = Class.InstQuitPanel(ClassTypes.BasePanel)
local InstanceManager = PlayerManager.InstanceManager
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local PlayerActionManager = GameManager.PlayerActionManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TaskCommonManager = PlayerManager.TaskCommonManager

function InstQuitPanel:Awake()
    self._btnQuit = self:FindChildGO("Button_Quit")
    self._csBH:AddClick(self._btnQuit, function()self:QuitBtnClick() end)
    self._defaultContentId = 58412
    self._contentId = 58412
end

function InstQuitPanel:OnShow(data)
    if data ~= nil then
        self._contentId = data
    else
        self._contentId = self._defaultContentId
    end
end

function InstQuitPanel:QuitBtnClick()
    if GameWorld.Player():CanExitMapByHit() then
        GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContent(self._contentId), function()
            PlayerActionManager:StopAllAction()
            -- GameWorld.Player():StopMoveWithoutCallback()
            -- LoggerHelper.Log("Ash: InstQuitPanel:QuitBtnClick()")
            TaskCommonManager:SetCurAutoTask(nil, nil)
            InstanceManager:OnInstanceExit()
        end, nil)
    end
end
