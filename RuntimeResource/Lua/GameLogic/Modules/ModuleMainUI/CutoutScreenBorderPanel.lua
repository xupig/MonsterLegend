-- CutoutScreenBorderPanel.lua
local CutoutScreenBorderPanel = Class.CutoutScreenBorderPanel(ClassTypes.BasePanel)
local RectTransform = UnityEngine.RectTransform

function CutoutScreenBorderPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self._leftborder = self:FindChildGO("Container_Border/Image_Left")
    self._leftborderRect = self._leftborder:GetComponent(typeof(RectTransform))
    self._leftborderSize = self._leftborderRect.sizeDelta

    self._rightborer = self:FindChildGO("Container_Border/Image_Right")
    self._rightborerRect = self._rightborer:GetComponent(typeof(RectTransform))
    self._rightborerSize = self._rightborerRect.sizeDelta
end

function CutoutScreenBorderPanel:OnShow()
    self:SetData()
end

function CutoutScreenBorderPanel:SetData()
    self:ResizeBorder()
end

function CutoutScreenBorderPanel:ResizeBorder()
    local cutoutHeight = GameManager.GUIManager.GetCutoutScreenHeight()
    self._leftborderRect.sizeDelta = Vector2.New(cutoutHeight, self._leftborderSize.y)
    self._rightborerRect.sizeDelta = Vector2.New(cutoutHeight, self._rightborerSize.y)
end

