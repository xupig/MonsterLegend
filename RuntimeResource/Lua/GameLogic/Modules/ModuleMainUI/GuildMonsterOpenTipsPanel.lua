-- GuildMonsterOpenTipsPanel.lua
local GuildMonsterOpenTipsPanel = Class.GuildMonsterOpenTipsPanel(ClassTypes.BasePanel)
local WorldBossManager = PlayerManager.WorldBossManager
local BossHomeManager = PlayerManager.BossHomeManager
local BackWoodsManager = PlayerManager.BackWoodsManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local GuildDataHelper = GameDataHelper.GuildDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper

function GuildMonsterOpenTipsPanel:Awake()
    self.disableCameraCulling = true
    self:InitPanel()
end

function GuildMonsterOpenTipsPanel:InitPanel()
    self._textTitle = self:GetChildComponent("Container_Right/Text_Title", "TextMeshWrapper")
    self._btnGo = self:FindChildGO("Container_Right/Button_Go")
    self._csBH:AddClick(self._btnGo, function()self:GoBtnClick() end)
    self._btnClose = self:FindChildGO("Container_Right/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    self._containerIcon = self:FindChildGO("Container_Right/Container_Icon")

    self._textTitle.text = LanguageDataHelper.CreateContent(53463)
    local monsterId = GuildDataHelper:GetGuildMonsterData(1).monster_id
    local icon = MonsterDataHelper.GetIcon(monsterId)
    GameWorld.AddIcon(self._containerIcon, icon)
end

function GuildMonsterOpenTipsPanel:OnShow(data)
    self:SetData(data)
end

function GuildMonsterOpenTipsPanel:GoBtnClick()
    GUIManager.ShowPanelByFunctionId(415)
    GUIManager.ClosePanel(PanelsConfig.GuildMonsterOpenTips)
end

function GuildMonsterOpenTipsPanel:CloseBtnClick()
    GUIManager.ClosePanel(PanelsConfig.GuildMonsterOpenTips)
end

function GuildMonsterOpenTipsPanel:SetData(data)
    if self._countdownTimer ~= nil then
        GameWorld.TimerHeap:DelTimer(self._countdownTimer)
    end
    self._countdownTimer = GameWorld.TimerHeap:AddSecTimer(60, 0, 0, function()self:OnCountdownTick() end)
end

function GuildMonsterOpenTipsPanel:OnCountdownTick()
    GUIManager.ClosePanel(PanelsConfig.GuildMonsterOpenTips)
end

function GuildMonsterOpenTipsPanel:OnClose()
    if self._countdownTimer ~= nil then
        GameWorld.TimerHeap:DelTimer(self._countdownTimer)
    end
    self._countdownTimer = nil
end

function GuildMonsterOpenTipsPanel:OnDestroy()
    self._textName = nil
    self._btnGo = nil
    self._btnClose = nil
    self._containerIcon = nil
end
