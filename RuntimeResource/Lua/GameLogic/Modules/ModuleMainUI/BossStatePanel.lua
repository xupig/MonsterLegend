-- BossStatePanel.lua
local ClassTypes = ClassTypes
local BossStatePanel = Class.BossStatePanel(ClassTypes.BasePanel)

local ComponentsConfig = GameConfig.ComponentsConfig
local UIProgressBar = ClassTypes.UIProgressBar
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local UIMultipleProgressBar = ClassTypes.UIMultipleProgressBar
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function BossStatePanel:Awake()
    self.hpNum = 0

    self._btnHead = self:FindChildGO("Container_BossInfo/Button_Head")
    self._csBH:AddClick(self._btnHead, function() self:OnHeadButton() end)

    self._name = self:GetChildComponent("Container_BossInfo/Text_Name","TextMeshWrapper")
    self._countText = self:GetChildComponent("Container_BossInfo/Text_Count","TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Container_BossInfo/Container_Icon")

    self._angerTextGO = self:FindChildGO("Container_BossInfo/Text_Anger")
    self._angerText = self:GetChildComponent("Container_BossInfo/Text_Anger","TextMeshWrapper")
    self._angerTextGO:SetActive(false)

    self._dropImage = self:FindChildGO("Container_BossInfo/Image_Drop")
    self._dropText = self:GetChildComponent("Container_BossInfo/Image_Drop/Text_Drop","TextMeshWrapper")

    self._hpComp = UIMultipleProgressBar.AddProgressBar(self.gameObject,"ProgressBar_HP")
    self._hpComp:SetProgress(0)
    self._hpComp:ShowTweenAnimate(true)
    self._hpComp:SetMutipleTween(true)
    self._hpComp:SetIsResetToZero(false)
    self._hpComp:SetTweenTime(0.5)
    local tweenCompleteCB = 
    function ()
       self:ProgressComplete()
    end
    self._hpComp:SetTweenCompleteCB(tweenCompleteCB)

    local nextTweenCB = 
    function ()
        self:NextTween()
    end
    self._hpComp:SetNextTweenCB(nextTweenCB)

    self:AddEventListeners()
end

function BossStatePanel:Init()
    EventDispatcher:TriggerEvent(GameEvents.ON_SET_EXPAND_STATE, false)
    self:SetRoleInfo()
    self:OnHPChange()
    self:OnDropOwnerChange()
    self._onHPChangeCB = function() self:OnHPChange() end
    self._onDropOwnerChangeCB = function() self:OnDropOwnerChange() end
    self.entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
    self.entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
    self.entity:AddPropertyListener(EntityConfig.PROPERTY_DROP_OWNER, self._onDropOwnerChangeCB)
    self._setBossStatePanelShow = function(state) self:Show(state) end
    EventDispatcher:AddEventListener(GameEvents.Set_Boss_State_Panel_Show, self._setBossStatePanelShow)
end

function BossStatePanel:OnHeadButton()
    if self.entity ~= nil then
        GameWorld.Player():SetTargetId(self.entity.id)
    end
end

function BossStatePanel:SetEntity(eid)
    self.entity = GameWorld.GetEntity(eid)
    if self.entity ~= nil then
        self:Init()
    end
end

function BossStatePanel:AddEventListeners()
    self._onSetEntity = function(eid) self:SetEntity(eid) end
    EventDispatcher:AddEventListener(GameEvents.BossStatePanel_SetEntity, self._onSetEntity)
end

function BossStatePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.BossStatePanel_SetEntity, self._onSetEntity)
end

function BossStatePanel:SetRoleInfo()
    --self._name.text = self.entity.name
    self._name.text = string.format("Lv.%s %s", self.entity.level, self.entity.name)
    local icon = MonsterDataHelper.GetIcon(self.entity.monster_id)
    GameWorld.AddIcon(self._iconContainer, icon)
    self.hpNum = math.max(MonsterDataHelper.GetMonsterHPNum(self.entity.monster_id), 1)
    if self.hpNum > 1 then
        self._showMultiple = true
    else
        self._showMultiple = false
    end
    self.maxHp = math.ceil(self.entity.max_hp/self.hpNum)
    self:ShowAnger(self.entity.monster_id)
end

function BossStatePanel:ShowAnger(monster_id)
    local anger = MonsterDataHelper.GetMonsterAngry(monster_id)
    if anger > 0 then
        self._angerTextGO:SetActive(true)
        self._angerText.text = anger
    else
        self._angerTextGO:SetActive(false)
    end
end

function BossStatePanel:RefreshCount(count)
    if self._showMultiple then
        self._countText.text = string.format("×%d",count)
    else
        self._countText.text = ""
    end
end

function BossStatePanel:OnDestroy() 
    self._csBH = nil
    self:RemoveEventListeners()
    if self.entity ~= nil then
        self.entity:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
        self.entity:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
        self.entity:RemovePropertyListener(EntityConfig.PROPERTY_DROP_OWNER, self._onDropOwnerChangeCB)
    end
end

--for override
function BossStatePanel:OnClose()
    if self.entity ~= nil then
        self.entity:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
        self.entity:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
        self.entity:RemovePropertyListener(EntityConfig.PROPERTY_DROP_OWNER, self._onDropOwnerChangeCB)
        EventDispatcher:RemoveEventListener(GameEvents.Set_Boss_State_Panel_Show, self._setBossStatePanelShow)
    end
end

function BossStatePanel:OnHPChange()
    if self.entity ~= nil then
        local cnt = math.max(math.floor(self.entity.cur_hp/self.maxHp),0)
        self:RefreshCount(math.min(self.hpNum, cnt+1))
        self._hpComp:SetProgressByValue(self.entity.cur_hp , self.maxHp)
    end
end

function BossStatePanel:OnDropOwnerChange()
    if self.entity ~= nil and not string.IsNullOrEmpty(self.entity.drop_owner) then
        self._dropImage:SetActive(true)
        self._dropText.text = self.entity.drop_owner
    else
        self._dropImage:SetActive(false)
        self._dropText.text = ""
    end
end

function BossStatePanel:Show(state)
    self.gameObject:SetActive(state)
end

function BossStatePanel:NextTween()
end

function BossStatePanel:ProgressComplete()
end
