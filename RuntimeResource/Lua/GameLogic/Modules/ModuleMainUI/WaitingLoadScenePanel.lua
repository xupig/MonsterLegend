-- WaitingLoadScenePanel.lua
local WaitingLoadScenePanel = Class.WaitingLoadScenePanel(ClassTypes.BasePanel)

local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local GameSceneManager = GameManager.GameSceneManager
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig

function WaitingLoadScenePanel:Awake()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
end

function WaitingLoadScenePanel:OnShow(data)
    -- LoggerHelper.Log("Ash: WaitingLoadScenePanel " .. data)
    self._sceneName = data
    if self._countdownTimer ~= nil then
        GameWorld.TimerHeap:DelTimer(self._countdownTimer)
    end
    self._countdownTimer = GameWorld.TimerHeap:AddSecTimer(0, 1, 0, function()self:OnCountdownTick() end)
end

function WaitingLoadScenePanel:OnClose()
    GameWorld.TimerHeap:DelTimer(self._countdownTimer)
    self._countdownTimer = nil
end

function WaitingLoadScenePanel:OnCountdownTick()
    local sceneInLoading = GameSceneManager:GetInLoading()
    if sceneInLoading == false then
        GUIManager.ClosePanel(PanelsConfig.WaitingLoadScene)
    end
end

function WaitingLoadScenePanel:OnEnterMap(mapId)
    local sceneName = GameSceneManager:GetSceneName()
    -- LoggerHelper.Log("Ash: OnEnterMap " .. sceneName .. ", " .. self._sceneName)
    if self._sceneName == sceneName then
        GUIManager.ClosePanel(PanelsConfig.WaitingLoadScene)
    end
end

function WaitingLoadScenePanel:OnDestroy()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
end

-- -- 是否有背景 0 没有  1 清晰底板  2 模糊底板  3 二级面板遮罩
function WaitingLoadScenePanel:CommonBGType()
    return PanelBGType.Mask
end
