local SystemTipsPanel = Class.SystemTipsPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local Time_Out_Sec = 5
--override
function SystemTipsPanel:Awake()
    self._confirmButtonCB = nil
    self._cancelButtonCB = nil
    self:InitPanel()
    self._countDownFunc = function() self:CountDown() end
end

--override
function SystemTipsPanel:OnDestroy()

end

--override
function SystemTipsPanel:OnShow(data)
    if data == nil then
        GUIManager.ClosePanel(PanelsConfig.SystemTips)
        return
    end
    self:AddEventListeners()
    self:SetData(data)
end

function SystemTipsPanel:SetData(data)
    self._confirmButtonCB = data.confirmCB
    self._cancelButtonCB = data.cancelCB
    self._autoConfirm = data.autoConfirm
    self:ShowConfirmText(data.confirmText or LanguageDataHelper.CreateContent(13))
    self:ShowDesc(data.text)
    if self._autoConfirm then
        self._endTime = Time_Out_Sec
        if self.timer then
            TimerHeap:DelTimer(self.timer)
        end
        self.timer = TimerHeap:AddSecTimer(0, 1, 0 , self._countDownFunc)
    else
        self.countDownText.text = ""
    end
end

--override
function SystemTipsPanel:OnClose()
    self:RemoveEventListeners()
    if self.timer then
        TimerHeap:DelTimer(self.timer)
    end
end

function SystemTipsPanel:AddEventListeners()

end

function SystemTipsPanel:RemoveEventListeners()

end

function SystemTipsPanel:InitPanel()
    self._confirmButton = self:FindChildGO("Container_Tip/Button_Confirm")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Container_Tip/Button_Cancel")
    self._csBH:AddClick(self._cancelButton,function() self:OnCancelButtonClick() end)
    self.descText = self:GetChildComponent("Container_Tip/Container_Desc/Text_Desc", "TextMeshWrapper")
    self.descText.text = ""

    self.countDownText = self:GetChildComponent("Container_Tip/Text_CountDown", "TextMeshWrapper")
    
    self._confirmText = self:GetChildComponent("Container_Tip/Button_Confirm/Text", "TextMeshWrapper")
end

function SystemTipsPanel:OnConfirmButtonClick()
    GUIManager.ClosePanel(PanelsConfig.SystemTips)
    if self._confirmButtonCB ~= nil then
        self._confirmButtonCB()
    end
end

function SystemTipsPanel:OnCancelButtonClick()
    GUIManager.ClosePanel(PanelsConfig.SystemTips)
    if self._cancelButtonCB ~= nil then
        self._cancelButtonCB()
    end
end

function SystemTipsPanel:ShowDesc(text)
    self.descText.text = text
end

function SystemTipsPanel:CountDown()
    self._endTime = math.max(self._endTime - 1, 0)
    if self._endTime == 0 then
        if self.timer then
            TimerHeap:DelTimer(self.timer)
        end
        self:OnConfirmButtonClick()
    end
    self.countDownText.text = LanguageDataHelper.CreateContentWithArgs(594, {["0"]=self._endTime})
end

function SystemTipsPanel:ShowConfirmText(text)
    self._confirmText.text = text
end 