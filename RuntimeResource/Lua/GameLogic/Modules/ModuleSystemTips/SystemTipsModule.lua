SystemTipsModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function SystemTipsModule.Init()
    GUIManager.AddPanel(PanelsConfig.SystemTips,"Panel_SystemTips",GUILayer.LayerUICG,"Modules.ModuleSystemTips.SystemTipsPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return SystemTipsModule