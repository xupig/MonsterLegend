-- LimitedShopItem.lua
local LimitedShopItem = Class.LimitedShopItem(ClassTypes.UIListItem)
local PanelsConfig = GameConfig.PanelsConfig

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

LimitedShopItem.interface = GameConfig.ComponentsConfig.Component
LimitedShopItem.classPath = "Modules.ModuleLimitedShopping.ChildComponent.LimitedShopItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local LimitedShoppingManager = PlayerManager.LimitedShoppingManager
local FlashSale2DataHelper = GameDataHelper.FlashSale2DataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local hideScale = Vector3.New(0, 1, 1)
local LimitStr = LanguageDataHelper.CreateContent(58803)

function LimitedShopItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function LimitedShopItem:InitView()
    self._discount = self:GetChildComponent("Text_Discount", "TextMeshWrapper")
    self._itemName = self:GetChildComponent("Text_ItemName", "TextMeshWrapper")
    self._itemGO = self:FindChildGO("Container_Item")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemGO)
    -- self._itemIcon:ActivateTipsById()
    -- self._itemIcon:SetItem(tonumber(positionList[self._posId][1]), tonumber(positionList[self._posId][2]))

    self._oldCostItem = self:FindChildGO("Container_OldPrice/Container_CostItem")
    self._oldPrice = self:GetChildComponent("Container_OldPrice/Text_Num", "TextMeshWrapper")
    
    self._nowCostItem = self:FindChildGO("Container_NewPrice/Container_CostItem")
    self._nowPrice = self:GetChildComponent("Container_NewPrice/Text_Num", "TextMeshWrapper")

    self._limitTime = self:GetChildComponent("Text_LimitTime", "TextMeshWrapper")

    self._limitCondition = self:GetChildComponent("Text_LimitCondition", "TextMeshWrapper")
    self._limitConditionGO = self:FindChildGO("Text_LimitCondition")
    self._conditionTransform = self._limitConditionGO.transform

    self._buy = self:FindChildGO("Button_Buy")
    self._btnBuyTransform = self._buy.transform
    self._btnBuyTransform.localScale = Vector3.one

    self._alreadyBuy = self:FindChildGO("Image_AlreadyBuy")
    self._alreadyBuyTransform = self._alreadyBuy.transform

    self._csBH:AddClick(self._buy, function() self:OnBuy() end)

    self._conditionScale = Vector3.one
    self._btnBuyScale = true
    self._alreadyBuyScale = Vector3.one
end

function LimitedShopItem:OnDestroy()
    self._init = nil
end

function LimitedShopItem:OnDisable()
    self._inited = false
end
-- [shopItemId = id,upLevelTime = upTime]
function LimitedShopItem:OnRefreshData(data)
    self._data = data
    local shopItemId = data.shopItemId
    local upLevelTime = data.upLevelTime
    local shopDatas = FlashSale2DataHelper.GetFlashSale2(shopItemId)
    self._shopDatas = shopDatas

    local ItemId, ItemNum = self:GetMData(shopDatas.item)
    self._itemIcon:ActivateTipsById()
    self._itemIcon:SetItem(ItemId, ItemNum)

    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable['0'] = (shopDatas.discount / 10)
    self._discount.text = LanguageDataHelper.CreateContent(58805, argsTable) --(shopDatas.discount / 10) .. "折"
    self._itemName.text = ItemDataHelper.GetItemNameWithColor(ItemId)
    
    local costId, costNum = self:GetMData(shopDatas.cost)
    GameWorld.AddIcon(self._oldCostItem, ItemDataHelper.GetIcon(costId))
    GameWorld.AddIcon(self._nowCostItem, ItemDataHelper.GetIcon(costId))
    self._oldPrice.text = costNum
    self._nowPrice.text = math.floor(costNum*shopDatas.discount/100)

    -- argsTable['0'] = shopDatas.level
    -- self._limitCondition.text = LanguageDataHelper.CreateContent(58804, argsTable)  --"限购等级:" .. tostring(shopDatas.level)

    self:TickTime()
    self._init = true

    self:ShowCondition()

end

function LimitedShopItem:ShowCondition()
    if self._init == true then
        -- LimitedShoppingManager:IsBuyed(id) 已经购买
        -- upLevelTime == nil 未开启
        -- 可以购买
        local shopItemId = self._data.shopItemId
        local upLevelTime = self._data.upLevelTime
        local level = self._shopDatas.level
        if LimitedShoppingManager:IsBuyed(shopItemId) then --已经买过了
            self._limitCondition.text = ""
            self._conditionScale = hideScale
            self._btnBuyScale = false
            self._alreadyBuyScale = Vector3.one
        else--可以购买
            self._limitCondition.text = ""
            self._conditionScale = hideScale
            self._btnBuyScale = true
            self._alreadyBuyScale = hideScale
        end

        if upLevelTime == nil then
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable['0'] = level
            self._limitCondition.text = LanguageDataHelper.CreateContent(58804, argsTable)--"达到等级" .. tostring(level) .. "可以购买"
            self._conditionScale = Vector3.one
            self._btnBuyScale = false
            self._alreadyBuyScale = hideScale
        end

        self._conditionTransform.localScale = self._conditionScale
        self._buy:SetActive(self._btnBuyScale)
        self._alreadyBuyTransform.localScale = self._alreadyBuyScale
        -- self._btnBuyTransform.localScale = self._btnBuyScale
    end
end

function LimitedShopItem:TickTime()
    if self._init == true then
        if self._data.upLevelTime == nil then
            self._limitTime.text = ""
            return
        end
        local outOfDateTime = self._data.upLevelTime + self._shopDatas.time
        local result, timeStr = LimitedShoppingManager:CalcTime(outOfDateTime)
        if result == true then
            self._limitTime.text = string.format( "%s%s", LimitStr, timeStr )
        else
            EventDispatcher:TriggerEvent(GameEvents.PackNewLimitedShoppingDatas)
        end
    end
end

function LimitedShopItem:OnBuy()
    if self._data ~= nil then
        LimitedShoppingManager:BuyLimitedItem(self._data.shopItemId)
    end
end

function LimitedShopItem:GetMData(data)
    local key = 0
    local value = 0
    for k, v in pairs(data) do
        key = k
        value = v
    end
    return key, value
end
