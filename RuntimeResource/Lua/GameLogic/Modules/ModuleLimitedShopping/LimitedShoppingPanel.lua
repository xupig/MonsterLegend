-- LimitedShoppingPanel.lua
require "Modules.ModuleLimitedShopping.ChildComponent.LimitedShopItem"
local LimitedShopItem = ClassTypes.LimitedShopItem

local LimitedShoppingPanel = Class.LimitedShoppingPanel(ClassTypes.BasePanel)
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local UIList = ClassTypes.UIList
local LimitedShoppingManager = PlayerManager.LimitedShoppingManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local okText = LanguageDataHelper.CreateContent(58808)
local cancelText = LanguageDataHelper.CreateContent(58809)

function LimitedShoppingPanel:Awake()
    self:InitView()
end

function LimitedShoppingPanel:InitView()
    -- self._btnClose = self:FindChildGO("Button_Close")
    -- self._csBH:AddClick(self._btnClose, function() self:OnPanelClose() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/ScrollViewList")
	self._list = list
	self._listlView = scrollView 
    self._listlView:SetHorizontalMove(true)
    self._listlView:SetVerticalMove(false)
    -- scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(LimitedShopItem)
    self._list:SetPadding(10, 10, 0, 10)
    self._list:SetGap(10, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end


function LimitedShoppingPanel:OnCloseClick()
    -- GUIManager.ClosePanel(PanelsConfig.LimitedShopping)
    GUIManager.ShowDailyTips(SettingType.CARD_LUCK_NEED, function() self:OnShoppingPanelClose()  end,nil, LanguageDataHelper.CreateContent(58806), true , okText, cancelText)
end

function LimitedShoppingPanel:OnShoppingPanelClose()
    GUIManager.ClosePanel(PanelsConfig.LimitedShopping)
    LimitedShoppingManager:CheckPanelOutOfDate()
    LimitedShoppingManager:CheckNoItemCanBuy()
end

-- GameManager.GUIManager.ShowPanel(GameConfig.PanelsConfig.LimitedShopping)
function LimitedShoppingPanel:Start()
end

function LimitedShoppingPanel:OnDestroy()
    self._btnClose = nil
    self._list = nil
    self._listlView = nil
    self._listData = nil
    self:EndTickTime()
end

function LimitedShoppingPanel:SetData(data)
    self:PackNewDatas()
    self:StartTickTime()
end

function LimitedShoppingPanel:OnShow(data)
    self:RightBgIsActive(false)
    self:SetData()
end

function LimitedShoppingPanel:OnEnable()
    self._onLimitedShoppingSucceed = function() self:LimitedBuyAction() end
    EventDispatcher:AddEventListener(GameEvents.OnRefreshLimitedShoppingPanel, self._onLimitedShoppingSucceed)
    self._onPackNewDatas = function() self:PackNewDatas() end
    EventDispatcher:AddEventListener(GameEvents.PackNewLimitedShoppingDatas, self._onPackNewDatas)
end

function LimitedShoppingPanel:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.OnRefreshLimitedShoppingPanel, self._onLimitedShoppingSucceed)
    EventDispatcher:RemoveEventListener(GameEvents.PackNewLimitedShoppingDatas, self._onPackNewDatas)
    self:EndTickTime()
end

function LimitedShoppingPanel:TickTimeAction()
    if self._listData ~= nil and #self._listData > 0 then
        for index=1, #self._listData do
            local item = self._list:GetItem(index-1)
            if item ~= nil then
                item:TickTime()
            end
        end
    end
end

function LimitedShoppingPanel:LimitedBuyAction()
    if self._listData ~= nil and #self._listData > 0 then
        for index=0, #self._listData-1 do
            local item = self._list:GetItem(index)
            if item ~= nil then
                item:ShowCondition()
            end
        end
    end
end

function LimitedShoppingPanel:PackNewDatas()
    self._listData = LimitedShoppingManager:PackShopItemData()
    self._list:SetDataList(self._listData)
end


function LimitedShoppingPanel:WinBGType()
    return  PanelsConfig.PanelWinBGType.RedNewBG
end

function LimitedShoppingPanel:StartTickTime()
    if self._tickTimer ~= nil then return end --防止重复开启
    self._tickTimer = TimerHeap:AddSecTimer(0, 1, 0, function() self:TickTimeAction() end)
end

function LimitedShoppingPanel:EndTickTime()
    TimerHeap:DelTimer(self._tickTimer)
    self._tickTimer = nil
end

