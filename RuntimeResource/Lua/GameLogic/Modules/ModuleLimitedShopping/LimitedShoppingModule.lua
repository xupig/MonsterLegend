-- LimitedShoppingModule.lua
LimitedShoppingModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function LimitedShoppingModule.Init()
    GUIManager.AddPanel(PanelsConfig.LimitedShopping, "Panel_LimitedShopping", GUILayer.LayerUIPanel, "Modules.ModuleLimitedShopping.LimitedShoppingPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return LimitedShoppingModule



