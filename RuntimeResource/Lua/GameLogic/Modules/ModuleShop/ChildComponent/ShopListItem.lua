local ShopListItem = Class.ShopListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
ShopListItem.interface = GameConfig.ComponentsConfig.Component
ShopListItem.classPath = "Modules.ModuleShop.ChildComponent.ShopListItem"

local ShopDataHelper = GameDataHelper.ShopDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local QualityType = GameConfig.EnumType.QualityType
local ShopManager = PlayerManager.ShopManager
local ShopPageType = GameConfig.EnumType.ShopPageType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local TipsManager = GameManager.TipsManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemConfig = GameConfig.ItemConfig
local StringStyleUtil = GameUtil.StringStyleUtil

local MAX_DIAMOND_SHOP_ID = 19999 --最大钻石商店流水id
local MIN_DIAMOND_SHOP_ID = 10000 --最小钻石商店流水id
local MAX_GOLD_SHOP_ID    = 29999 --最大金币商店流水id
local MIN_GOLD_SHOP_ID    = 20000 --最小金币商店流水id
local MAX_SILVER_SHOP_ID  = 39999 --最大银币商店流水id
local MIN_SILVER_SHOP_ID  = 30000 --最小银币商店流水id

local MAX_TAG_COUNT = 4
local QUALITY_TO_ICON = {[QualityType.Green]=4120, [QualityType.Blue]=4121, [QualityType.Purple]=4122, [QualityType.Orange]=4123,
        [QualityType.Red]=4124, [QualityType.Pink]=4125}

local function _gey_money_type_by_id(id)
    local type = -1
    if id >= MIN_DIAMOND_SHOP_ID and id <= MAX_DIAMOND_SHOP_ID then
        type = public_config.MONEY_TYPE_COUPONS
    elseif id >= MIN_GOLD_SHOP_ID and id <= MAX_GOLD_SHOP_ID then
        type = public_config.MONEY_TYPE_COUPONS_BIND
    elseif id >= MIN_SILVER_SHOP_ID and id <= MAX_SILVER_SHOP_ID then
        type = public_config.MONEY_TYPE_GOLD
    end
    return type
end

function ShopListItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function ShopListItem:OnDestroy()
    self.data = nil
end

--override
function ShopListItem:OnRefreshData(data)
    self.data = data
    local id = data.id

    self:ShowItemIcon(id)
    self:ShowItemName(id)
    self:ShowDiscount(id)
    self:ShowItemPrice(id)
    self:ShowItemInfo(id)
    self:ShowVIPLimit(id)
end

function ShopListItem:InitView()
    self._lightIcon = self:FindChildGO("Container_Icon")
    self._iconContainer = self:FindChildGO("Container_Item")

    self.nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")

    self.priceContainer = self:FindChildGO("Container_ItemPrice")
    self.moneyIcon = self:FindChildGO("Container_ItemPrice/Container_Money")
    self.priceText = self:GetChildComponent("Container_ItemPrice/Text_Price", "TextMeshWrapper")

    self.countInfoText = self:GetChildComponent("Container_ItemCount/Text_Count", "TextMeshWrapper")
    
    self.tagContainer = self:FindChildGO("Container_Tag")
    self.discountText = self:GetChildComponent("Container_Tag/Text_Value", "TextMeshWrapper")

    self.vipLimitContainer = self:FindChildGO("Container_VIPLimit")
    self.vipText = self:GetChildComponent("Container_VIPLimit/Text_VIP", "TextMeshWrapper")
end

function ShopListItem:ShowSelected(state)
    if state then
        --显示tips
        local btnArgs = {}
        btnArgs.shopBuy = {self.data.id, self.price, self.buyLimitCount, self.moneyType} --20单价,10限购数量
        local itemId = ShopDataHelper:GetItemId(self.data.id)
        TipsManager:ShowItemTipsById(itemId,btnArgs)
    end
end

function ShopListItem:ShowDiscount(id)
    local isDiscount = ShopDataHelper:IsInDiscount(id)
    local discount = ShopDataHelper:GetDiscount(id)
    if isDiscount then
        self:ShowItemDiscount(true)
        local discount = discount * 10
        self.discountText.text = discount
    else
        self:ShowItemDiscount(false)
    end
end

function ShopListItem:ShowItemDiscount(state)
    self.tagContainer:SetActive(state)
end

function ShopListItem:ShowItemPrice(id)
    local isDiscount = ShopDataHelper:IsInDiscount(id)
    local moneyType, price = ShopDataHelper:GetItemTypeAndOriPrice(id)
    local _, disPrice = ShopDataHelper:GetItemTypeAndUnitPrice(id)
    local realPrice = price
    if isDiscount then
        realPrice = disPrice
    end
    self.price = math.floor(realPrice)
    self.moneyType = moneyType
    if BaseUtil.IsEnoughMoney(realPrice, moneyType) then
        self.priceText.text = BaseUtil.GetColorString(tostring(realPrice), ColorConfig.A)
    else
        self.priceText.text = BaseUtil.GetColorString(tostring(realPrice), ColorConfig.H)
    end
    GameWorld.AddIcon(self.moneyIcon,ItemDataHelper.GetIcon(moneyType))
end

function ShopListItem:ShowItemIcon(id)
    local itemId = ShopDataHelper:GetItemId(id)
    GameWorld.AddIcon(self._iconContainer, ItemDataHelper.GetIcon(itemId))
    local qualityIcon = QUALITY_TO_ICON[ItemDataHelper.GetQuality(itemId)]
    GameWorld.AddIcon(self._lightIcon, qualityIcon)
end

function ShopListItem:ShowItemName(id)
    local itemId = ShopDataHelper:GetItemId(id)
    local cfg = GlobalParamsHelper.GetParamValue(541)
    local name = ShopDataHelper:GetItemName(id)
    if cfg[itemId] ~= nil then
        local day = math.floor(cfg[itemId]/86400)
        local quality = ItemDataHelper.GetQuality(itemId)
        local colorId = ItemConfig.QualityTextMap[quality]
        local dayStr = StringStyleUtil.GetColorStringWithColorId(LanguageDataHelper.CreateContent(58780, {["0"]=day}), colorId)
        name = name .. dayStr
    end
    self.nameText.text = name
end

function ShopListItem:ShowItemInfo(id)
    local type = ShopDataHelper:GetPageType(id)
    self.buyLimitCount = -1
    if type == ShopPageType.WeekSell and ShopDataHelper:GetWeekLimit(id) ~= 0 then
        local data = GameWorld.Player().shop_week_limit
        local buyNum = data[id] or 0
        local weekLimit = ShopDataHelper:GetWeekLimit(id)
        local num = weekLimit - buyNum
        self.buyLimitCount = num
        self.countInfoText.text = LanguageDataHelper.CreateContentWithArgs(58666, {["0"]=num})
    elseif type == ShopPageType.Honor and ShopDataHelper:GetDayLimit(id) ~= 0 then
        local data = GameWorld.Player().shop_day_limit
        local buyNum = data[id] or 0
        local dayLimit = ShopDataHelper:GetDayLimit(id)
        local num = dayLimit - buyNum
        self.buyLimitCount = num
        self.countInfoText.text = LanguageDataHelper.CreateContentWithArgs(58666, {["0"]=num})
    else
        self.countInfoText.text = ""
    end
end

function ShopListItem:ShowVIPLimit(id)
    local limit = ShopDataHelper:GetVIPLevelLimit(id)
    if GameWorld.Player().vip_level >= limit then
        self.vipLimitContainer:SetActive(false)
        self.priceContainer:SetActive(true)
        self.vipText.text = ""
    else
        self.vipLimitContainer:SetActive(true)
        self.priceContainer:SetActive(false)
        self.vipText.text = LanguageDataHelper.CreateContentWithArgs(58667, {["0"]=limit})
    end
end