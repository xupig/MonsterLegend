local ShopTabItem = Class.ShopTabItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
ShopTabItem.interface = GameConfig.ComponentsConfig.Component
ShopTabItem.classPath = "Modules.ModuleShop.ChildComponent.ShopTabItem"

local ShopDataHelper = GameDataHelper.ShopDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local UIParticle = ClassTypes.UIParticle

function ShopTabItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function ShopTabItem:OnDestroy()
    self.data = nil
end

function ShopTabItem:InitView()
    self.selectContainer = self:FindChildGO("Container_Select")
    self.unSelectContainer = self:FindChildGO("Container_UnSelect")
    self.selectNameText = self:GetChildComponent("Container_Select/Label", "TextMeshWrapper")
    self.unSelectNameText = self:GetChildComponent("Container_UnSelect/Label", "TextMeshWrapper")
    local containerFx = self:FindChildGO("Container_Select/Container_Fx")
    UIParticle.AddParticle(containerFx, "fx_ui_30019")
    self:OnSelected(false)
end

--override
function ShopTabItem:OnRefreshData(data)
    self.data = data
    self.selectNameText.text = ShopDataHelper:GetShopTagName(data.id)
    self.unSelectNameText.text = ShopDataHelper:GetShopTagName(data.id)
end

function ShopTabItem:OnSelected(state)
    if state then
        self.selectContainer:SetActive(true)
        self.unSelectContainer:SetActive(false)
    else
        self.selectContainer:SetActive(false)
        self.unSelectContainer:SetActive(true)
    end
end