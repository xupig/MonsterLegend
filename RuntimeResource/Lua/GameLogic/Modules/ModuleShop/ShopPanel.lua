require "Modules.ModuleShop.ChildView.ItemListView"
require"UIComponent.Extend.CurrencyCom"
require "Modules.ModuleShop.ChildComponent.ShopTabItem"

local ShopPanel = Class.ShopPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local ItemListView = ClassTypes.ItemListView
local ShopDataHelper = GameDataHelper.ShopDataHelper
local CurrencyCom = ClassTypes.CurrencyCom
local ShopManager = PlayerManager.ShopManager
local UIList = ClassTypes.UIList
local ShopTabItem = ClassTypes.ShopTabItem
local ShopType = GameConfig.EnumType.ShopType
local ShopPageType = GameConfig.EnumType.ShopPageType
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local FIXED_SHOW_MAX_ITEM_COUNT = 6

--override
function ShopPanel:Awake()
    self.curPageType = 0
    self._preSelect = -1
    self:InitPanel()
    self:InitMoney()
end

--override
function ShopPanel:OnDestroy()
end

--override data结构 id:商城页签
function ShopPanel:OnShow(data)
    self:FindChildGO('Container_WinBg').transform.localPosition = Vector3(-640,-360,0)
    self:AddEventListeners()
    self:SetData(data)
end
--override
function ShopPanel:SetData(data)
    if #ShopDataHelper:GetShopPages() <= FIXED_SHOW_MAX_ITEM_COUNT then
        self._scrollView:SetScrollRectEnable(false)
    end
    self._list:SetDataList(ShopDataHelper:GetShopPages())
    self._currency:OnShow()
    if data ~= nil then
        local tab = data.id
        if tab ~= nil then
            if tab == ShopPageType.Marriage then
                --结婚商城特殊处理
                self._list:SetDataList({{id=ShopPageType.Marriage}})
                self._scrollView:SetScrollRectEnable(false)
                self:OnListItemClicked(0)
            else
                local index = ShopDataHelper:GetShopIndexByTag(tab)
                self:OnListItemClicked(index)
            end
        else
            self:Reset()
        end
    else
        self:Reset()
    end
end

--override
function ShopPanel:OnClose()
    self:Clear()
    self:RemoveEventListeners()
end

function ShopPanel:AddEventListeners()
    self._refreshWeekLimit = function () self:RefreshShopView() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Week_Limit, self._refreshWeekLimit)
    self._refreshDayLimit = function () self:RefreshShopView() end
	EventDispatcher:AddEventListener(GameEvents.Refresh_Day_Limit, self._refreshDayLimit)
end

function ShopPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Week_Limit, self._refreshWeekLimit)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Day_Limit, self._refreshDayLimit)
end

function ShopPanel:InitMoney()
    self._currency = self:AddChildLuaUIComponent("Container_Currency", CurrencyCom)
    self._currency:SetCurrencyTypes({public_config.MONEY_TYPE_COUPONS,public_config.MONEY_TYPE_COUPONS_BIND,public_config.MONEY_TYPE_ATHLETICS})
end

function ShopPanel:InitPanel()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Tab")
    self._list = list
    self._scrollView = scrollView
    scrollView:SetHorizontalMove(false)
    scrollView:SetVerticalMove(true)
    self._list:SetItemType(ShopTabItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)

	local itemClickedCB =
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    if #ShopDataHelper:GetShopPages() <= FIXED_SHOW_MAX_ITEM_COUNT then
        scrollView:SetScrollRectEnable(false)
    end
    self._list:SetDataList(ShopDataHelper:GetShopPages())

    self._itemListView = self:AddChildLuaUIComponent("Container_ShopItemList", ItemListView)

    self._tipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")
end

function ShopPanel:OnListItemClicked(idx)
    if self._preSelect == idx then
        return
    end
    if self._preSelect ~= -1 then
        self._list:GetItem(self._preSelect):OnSelected(false)
        GameWorld.PlaySound(2)
    end
    self._list:GetItem(idx):OnSelected(true)
    local data = self._list:GetDataByIndex(idx)
    self._preSelect = idx
    self.curPageType = data.id
    self:RefreshShopView()
    self._itemListView:Reset()
    self:RefreshTips()
end

function ShopPanel:Reset()
    self:Clear()
    self:OnListItemClicked(0)
    self:RefreshShopView()
end

function ShopPanel:Clear()
    if self._preSelect ~= -1 then
        self._list:GetItem(self._preSelect):OnSelected(false)
    end
    self._preSelect = -1
end

function ShopPanel:RefreshShopView()
    local data = ShopDataHelper:GetShopDataByTagAndLevel(self.curPageType, GameWorld.Player().level, GameWorld.Player():GetVocationGroup()) 
    self._itemListView:RefreshData(data)
end

function ShopPanel:RefreshTips()
    if self.curPageType == ShopPageType.WeekSell then
        self._tipsText.text = LanguageDataHelper.CreateContent(58670)
    elseif self.curPageType == ShopPageType.Honor then
        self._tipsText.text = LanguageDataHelper.CreateContent(58671)
    else
        self._tipsText.text = ""
    end
end

function ShopPanel:WinBGType()
    return PanelWinBGType.FullSceenBG
end