require "Modules.ModuleShop.ChildComponent.ShopListItem"

local ItemListView = Class.ItemListView(ClassTypes.BaseLuaUIComponent)

ItemListView.interface = GameConfig.ComponentsConfig.Component
ItemListView.classPath = "Modules.ModuleShop.ChildView.ItemListView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local ShopListItem = ClassTypes.ShopListItem

function ItemListView:Awake()
    self:InitScrollViewList()
    self._preSelected = -1
end
--override
function ItemListView:OnDestroy() 

end

function ItemListView:RefreshData(data)
    self:Clear()
    self._listData = data
    self._list:SetDataList(self._listData)
end

function ItemListView:InitScrollViewList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(ShopListItem)
    self._list:SetPadding(10, 0, 0, 5)
    self._list:SetGap(10, 15)
    self._list:SetDirection(UIList.DirectionTopToDown, 4, 1)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
end

function ItemListView:OnListItemClicked(idx)
    self._list:GetItem(idx):ShowSelected(true)
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelected(false)
    end
    self._preSelected = idx
end

function ItemListView:Clear()
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelected(false)
    end
    self._preSelected = -1
end

function ItemListView:Reset()
    self:Clear()
end