ShopModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function ShopModule.Init()
    GUIManager.AddPanel(PanelsConfig.Shop,"Panel_Shop",GUILayer.LayerUIPanel,"Modules.ModuleShop.ShopPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return ShopModule