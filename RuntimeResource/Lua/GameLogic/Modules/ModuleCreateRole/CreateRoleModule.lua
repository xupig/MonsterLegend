--CreateRoleModule.lua
CreateRoleModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function CreateRoleModule.Init()
	GUIManager.AddPanel(PanelsConfig.CreateRole,"Panel_CreateRole",GUILayer.LayerUIPanel,"Modules.ModuleCreateRole.CreateRolePanel",true,PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return CreateRoleModule