require "Modules.ModuleCreateRole.ChildComponent.CreateRoleVocationItem"

local CreateRoleSelectView = Class.CreateRoleSelectView(ClassTypes.BaseLuaUIComponent)

CreateRoleSelectView.interface = GameConfig.ComponentsConfig.Component
CreateRoleSelectView.classPath = "Modules.ModuleCreateRole.ChildView.CreateRoleSelectView"

local UIList = ClassTypes.UIList
local UIComponentUtil = GameUtil.UIComponentUtil
local CreateRoleVocationItem = ClassTypes.CreateRoleVocationItem
local GameObject = UnityEngine.GameObject
local SkillDataHelper = GameDataHelper.SkillDataHelper
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local SkillDataHelper = GameDataHelper.SkillDataHelper
local LuaUIRotateModel = GameMain.LuaUIRotateModel
local EquipModelType = GameConfig.EnumType.EquipModelType
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

local Move_Y = 20
local Time_Out_Sec = 20
local Init_Show_Pos = Vector3(0, 0, 0)
local Show_Vocation_List = {{100, 1, 1251}, {200, 1, 1252}}--显示职业设置{职业id，开启状态(1为开启), 职业文字icon}

local function GetRandomIndex()
    math.randomseed(os.time())
    local idx = math.random(10000)
    if idx <= 5000 then
        return 0
    else
        return 1
    end
end

function CreateRoleSelectView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._preVocation = -1
    self:InitView()
end

function CreateRoleSelectView:OnDestroy()

end

function CreateRoleSelectView:OnShow()
    self:ShowCreateRoleWaiting(false)
    if self._preVocation == -1 then
        local idx = GetRandomIndex()
        self:OnListItemClicked(idx)
    end
end

function CreateRoleSelectView:OnClose()
    if self._preVocation ~= -1 then
        self._list:GetItem(self._preVocation):ShowSelected(false)
    end
    self._preVocation = -1
    if self._timer then
        TimerHeap:DelTimer(self._timer)
    end
end

function CreateRoleSelectView:InitView()
    self._maskContainer = self:FindChildGO("Container_Mask")
    
    self._leftContainer = self:FindChildGO("Container_Left")
    self._rightContainer = self:FindChildGO("Container_Right")
    
    self._backButton = self:FindChildGO("Button_Back")
    self._csBH:AddClick(self._backButton, function()self:OnBackButtonClick() end)
    self._nextButton = self:FindChildGO("Container_Right/Button_Next")
    self._csBH:AddClick(self._nextButton, function()self:OnCreateButtonClick() end)
    
    self._vocationDescText = self:GetChildComponent("Container_Right/Container_Desc/Text_Info", "TextMeshWrapper")
    self._vocationIcon = self:FindChildGO("Container_Right/Container_Desc/Container_Icon")
    self._nameIcon = self:FindChildGO("Container_Right/Container_Desc/Container_Name")
    
    local moveTo = self._vocationIcon.transform.localPosition
    moveTo.y = moveTo.y + Move_Y
    self._tweenPosVocation = self:AddChildComponent('Container_Right/Container_Desc/Container_Icon', XGameObjectTweenPosition)
    self._tweenPosVocation:SetToPosTween(moveTo, 2, 4, 4, nil)
    
    self._nameContainer = self:FindChildGO("Container_Name")
    self._randomNameButton = self:FindChildGO("Container_Name/Button_RandomName")
    self._inputUserName = self:GetChildComponent("Container_Name/Input_Name", "InputFieldMeshWrapper")
    self._csBH:AddClick(self._randomNameButton, function()self:OnRandomNameClick() end)
    
    local rotateObj = self:FindChildGO("Container_ChangeRole")
    self.rotateModel = rotateObj:AddComponent(typeof(LuaUIRotateModel))
    
    self._waitingImage = self:FindChildGO("Image_Waiting")
    self._waitingText = self:GetChildComponent("Image_Waiting/Text_Desc", 'TextMeshWrapper')
    self._waitingImage:SetActive(false)
    
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Left/ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    scrollView:SetHorizontalMove(false)
    scrollView:SetVerticalMove(true)
    self._list:SetItemType(CreateRoleVocationItem)
    self._list:SetPadding(6, 0, 0, 4)
    self._list:SetGap(16, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._list:SetItemClickedCB(itemClickedCB)
    
    self._list:SetDataList(Show_Vocation_List)
end

function CreateRoleSelectView:OnListItemClicked(idx)
    if self._preVocation == idx then
        return
    end
    if self._preVocation ~= -1 then
        self._list:GetItem(self._preVocation):ShowSelected(false)
    end
    self._list:GetItem(idx):ShowSelected(true)
    self._preVocation = idx
    
    local data = self._list:GetDataByIndex(idx)
    self.selectedVocation = data[1]
    self:SetDefaultFacade(self.selectedVocation)
    EventDispatcher:TriggerEvent(GameEvents.On_Role_Refresh_Selected_Vocation, self.selectedVocation)
    self:RefreshView(self.selectedVocation)
    self:OnRandomNameClick()
end

function CreateRoleSelectView:ShowViewPos()
    CreateRoleManager:ChangeCameraForRole()
    self.rotateModel:SetTragetTransform(CreateRoleManager.Entity.cs:GetTransform())
end

function CreateRoleSelectView:OnListItemClickedVocation(idx)
    local vocationData = Show_Vocation_List[idx]
    local openState = vocationData[2] == 1
    if not openState then
        GUIManager.ShowText(1, LanguageDataHelper.GetContent(180))
        return
    end
    if self._preVocation == idx then
        return
    end
    if self._preVocation ~= -1 then
        self._vocationList[self._preVocation]:ShowSelected(false)
        self._lightList[self._preVocation]:SetActive(false)
    end
    self._vocationList[idx]:ShowSelected(true)
    self._lightList[idx]:SetActive(true)
    self._preVocation = idx
    
    local vocation = vocationData[1]
    self:SetDefaultFacade(vocation)
    EventDispatcher:TriggerEvent(GameEvents.On_Role_Refresh_Selected_Vocation, vocation)
    self:RefreshView(vocation)
end

function CreateRoleSelectView:OnBackButtonClick()
    GUIManager.ClosePanel(PanelsConfig.CreateRole)
    if GameWorld.Player().roleState == 2 then
        --没有过角色，回退到登录界面
        GameManager.GameStateManager.ReturnLogin(true)
        GUIManager.ShowPanel(PanelsConfig.Login)
    else
        local data = {GameWorld.Player().avatar_tbl, GameWorld.Player().avatar_dbid}
        GUIManager.ShowPanel(PanelsConfig.SelectRole, data)
    end
end

function CreateRoleSelectView:RefreshView(vocation)
    CreateRoleManager:DestroyEntity()
    CreateRoleManager.vocation = vocation
    CreateRoleManager.ActorId = RoleDataHelper.GetModelByVocation(vocation)
    CreateRoleManager.facade = ""
    --  LoggerHelper.Log("Ash: voc: " .. tostring(voc) .. " vocation: " .. vocation)
    CreateRoleManager.gender = CreateRoleManager:GetGenderByVocation(vocation)
    CreateRoleManager:BuildRole(CreateRoleManager.vocation, CreateRoleManager.ActorId, CreateRoleManager.facade, 0,
        function()
        --self.rotateModel:SetTragetTransform(CreateRoleManager.Entity.cs:GetTransform())
        end)
    
    self._vocationDescText.text = RoleDataHelper.GetRoleDescTextByVocation(vocation)
    local vocationIcon = RoleDataHelper.GetIconId2ByVocation(vocation)
    local nameIcon = RoleDataHelper.GetNameIconIdByVocation(vocation)
    GameWorld.AddIcon(self._vocationIcon, vocationIcon)
    GameWorld.AddIcon(self._nameIcon, nameIcon)
end

function CreateRoleSelectView:SetDefaultFacade(vocation)
    local selected = CreateRoleManager.CurrentSelectedCharacter
    CreateRoleManager.CurrentSelectedVocation = vocation
    
    -- 衣服、武器、头部、翅膀
    local equipData = RoleDataHelper.GetCreateRoleEquipByVocation(vocation)
    local headCfg = equipData[EquipModelType.Head]
    CreateRoleManager.HeadId[selected] = {headCfg[1], headCfg[2], headCfg[3], 0, 0, 0}
    local clothCfg = equipData[EquipModelType.Cloth]
    CreateRoleManager.ClothId[selected] = {clothCfg[1], clothCfg[2], clothCfg[3], 0, 0, 0}
    local weaponCfg = equipData[EquipModelType.Weapon]
    CreateRoleManager.WeaponId[selected] = {weaponCfg[1], weaponCfg[2], weaponCfg[3], 0, 0, 0}
    local wingCfg = equipData[EquipModelType.Wing]
    CreateRoleManager.WingId[selected] = {wingCfg[1], wingCfg[2], wingCfg[3], 0, 0, 0}
end

function CreateRoleSelectView:OnRandomNameClick()
    CreateRoleManager:GetRandomName(self.selectedVocation)
end

function CreateRoleSelectView:OnGetRandomName(name)
    self._inputUserName.text = name
end

function CreateRoleSelectView:OnCreateButtonClick()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_15)
    if GameWorld.IsClient then
        CreateRoleManager:CreateRoleEnterGame()
    else
        CreateRoleManager.roleName = self._inputUserName.text
        if not string.IsNullOrEmpty(CreateRoleManager.roleName) then
            self:ShowCreateRoleWaiting(true)
            CreateRoleManager:CreateAvatarReq()
        else
            GUIManager.ShowText(1, LanguageDataHelper.GetContent(582))
        end
    end
end

function CreateRoleSelectView:OnCreateRoleCGEnd()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_16)
    self:ShowViewPos()
end

function CreateRoleSelectView:OnCreateRoleCGBegin()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_17)
    local cameraTran = CreateRoleManager.Entity:GetBoneByName("bone_camera")
    CreateRoleManager:SetCamera(cameraTran)
end


function CreateRoleSelectView:ShowCreateRoleWaiting(state)
    self._waitingImage:SetActive(state)
    if state then
        self._endTime = Time_Out_Sec
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnWaitingUpdate() end)
    else
        if self._timer then
            TimerHeap:DelTimer(self._timer)
        end
    end
end

function CreateRoleSelectView:OnWaitingUpdate()
    self._endTime = self._endTime - 1
    self._waitingText.text = LanguageDataHelper.CreateContent(585, self._endTime)
    if self._endTime < 0 then
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_18)
        self:ShowCreateRoleWaiting(false)
    end
end
