local CreateRoleVocationItem = Class.CreateRoleVocationItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
CreateRoleVocationItem.interface = GameConfig.ComponentsConfig.PointableComponent
CreateRoleVocationItem.classPath = "Modules.ModuleCreateRole.ChildComponent.CreateRoleVocationItem"

local public_config = GameWorld.public_config
local RoleDataHelper = GameDataHelper.RoleDataHelper

function CreateRoleVocationItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function CreateRoleVocationItem:OnDestroy()
    self.data = nil
end

function CreateRoleVocationItem:InitView()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._nameIcon = self:FindChildGO("Container_Name")
    self._selectedImage = self:FindChildGO("Image_Selected")
end

--override {职业id，开启状态(1为开启)}
function CreateRoleVocationItem:OnRefreshData(data)
    self.data = data
    self.openState = data[2] == 1
    local iconId = RoleDataHelper.GetIconIdByVocation(data[1])
    if self.openState then
        GameWorld.AddIcon(self._iconContainer, iconId)
        GameWorld.AddIcon(self._nameIcon, data[3])
    else
        GameWorld.AddIcon(self._iconContainer, iconId, 12)
        GameWorld.AddIcon(self._nameIcon, data[3], 12)
    end
end

function CreateRoleVocationItem:ShowSelected(state)
    self._selectedImage:SetActive(state)
end