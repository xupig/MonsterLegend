--CreateRolePanel.lua
require "Modules.ModuleCreateRole.ChildView.CreateRoleSelectView"

local CreateRolePanel = Class.CreateRolePanel(ClassTypes.BasePanel)
local LocalSetting = GameConfig.LocalSetting
local UIToggleGroup = ClassTypes.UIToggleGroup
local RoleDataHelper = GameDataHelper.RoleDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GameObject = UnityEngine.GameObject

local CreateRoleSelectView = ClassTypes.CreateRoleSelectView

function CreateRolePanel:Awake()
    self._selectedVocation = 1
    self:InitView()
    self:InitListenerFunc()
end

function CreateRolePanel:InitListenerFunc()
    self._onGetRandomNameResp = function(name) self:GetCreateRoleRandomName(name) end
    self._onRefreshSelectedVocation = function(vocation) self:OnRefreshSelectedVocation(vocation) end
    self._onRefreshSelectedVocationItem = function(index) self:OnRefreshSelectedVocationItem(index) end
    self._onCreateRoleCGEnd = function() self:OnCreateRoleCGEnd() end
    self._onCreateRoleWaiting = function(state) self:ShowCreateRoleWaiting(state) end
    self._onCreateRoleCGBegin = function() self:OnCreateRoleCGBegin() end
end

function CreateRolePanel:OnDestroy()
end

function CreateRolePanel:OnShow()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_19)
    self:AddEventListeners()
    self:OnCreateRoleViewSwitch()
end

--override
function CreateRolePanel:OnClose()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_20)
    self._createRoleSelectView:OnClose()
    self:RemoveEventListeners()
end

function CreateRolePanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.OnGetRandomNameResp, self._onGetRandomNameResp)
    EventDispatcher:AddEventListener(GameEvents.On_Role_Refresh_Selected_Vocation, self._onRefreshSelectedVocation)
    EventDispatcher:AddEventListener(GameEvents.On_Role_Refresh_Selected_Vocation_Item, self._onRefreshSelectedVocationItem)
    EventDispatcher:AddEventListener(GameEvents.On_Create_Role_CG_End, self._onCreateRoleCGEnd)
    EventDispatcher:AddEventListener(GameEvents.ON_CREATE_ROLE_WAITING, self._onCreateRoleWaiting)
    EventDispatcher:AddEventListener(GameEvents.On_Create_Role_CG_Begin, self._onCreateRoleCGBegin)
end

function CreateRolePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.OnGetRandomNameResp, self._onGetRandomNameResp)
    EventDispatcher:RemoveEventListener(GameEvents.On_Role_Refresh_Selected_Vocation, self._onRefreshSelectedVocation)
    EventDispatcher:RemoveEventListener(GameEvents.On_Role_Refresh_Selected_Vocation_Item, self._onRefreshSelectedVocationItem)
    EventDispatcher:RemoveEventListener(GameEvents.On_Create_Role_CG_End, self._onCreateRoleCGEnd)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CREATE_ROLE_WAITING, self._onCreateRoleWaiting)
    EventDispatcher:RemoveEventListener(GameEvents.On_Create_Role_CG_Begin, self._onCreateRoleCGBegin)
end

function CreateRolePanel:InitView()
	self._createRoleSelectView = self:AddChildLuaUIComponent("Container_Select", CreateRoleSelectView)
    self._createRoleSelectGO = self:FindChildGO("Container_Select")

    self._createRoleSelectGO:SetActive(false)
end

function CreateRolePanel:OnCreateRoleViewSwitch(state)
    self._createRoleSelectGO:SetActive(true)
    self._createRoleSelectView:OnShow()
end

function CreateRolePanel:GetCreateRoleRandomName(name)
    self._createRoleSelectView:OnGetRandomName(name)
end

function CreateRolePanel:OnRefreshSelectedVocation(vocation)
    self._selectedVocation = vocation
end

function CreateRolePanel:OnRefreshSelectedVocationItem(index)
    self._createRoleSelectView:OnListItemClickedVocation(index)
end

function CreateRolePanel:OnCreateRoleCGEnd()
    self._createRoleSelectView:OnCreateRoleCGEnd()
end

function CreateRolePanel:ShowCreateRoleWaiting(state)
    self._createRoleSelectView:ShowCreateRoleWaiting(state)
end

function CreateRolePanel:OnCreateRoleCGBegin()
    self._createRoleSelectView:OnCreateRoleCGBegin()
end