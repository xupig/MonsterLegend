require "Modules.ModuleTask.ChildComponent.TaskRewardItem"

local EscortPeriView = Class.EscortPeriView(ClassTypes.UIListItem)

EscortPeriView.interface = GameConfig.ComponentsConfig.Component
EscortPeriView.classPath = "Modules.ModuleEscortPeri.ChildView.EscortPeriView"

local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskRewardItem = ClassTypes.TaskRewardItem
local GuardGoddessDataHelper = GameDataHelper.GuardGoddessDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local EscortPeriManager = PlayerManager.EscortPeriManager
local ShopType = GameConfig.EnumType.ShopType

function EscortPeriView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self._isFree = false
    self._ownCount = 0
    self._needCount = 0
end
--override
function EscortPeriView:OnDestroy() 

end

function EscortPeriView:InitView()
    self._escortButton = self:FindChildGO("Button_Escort")
    self._csBH:AddClick(self._escortButton,function () self:OnEscortButtonClick() end)

    self._icon = self:FindChildGO("Container_Icon")
    self._doubleImage = self:FindChildGO("Image_Double")
    self._countText = self:GetChildComponent("Image_Info/Text_Count", "TextMeshWrapper")
    self._moneyIcon = self:FindChildGO("Image_Info/Container_MoneyIcon")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(TaskRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 5)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)
end

--data {itemData}
function EscortPeriView:OnRefreshData(data)
    self.data = data

    GameWorld.AddIcon(self._icon, self.data:GetIcon())
    self:ShowCost()
    self:ShowDouble()
    self._list:SetDataList(self.data:GetReward())
end

function EscortPeriView:OnEscortButtonClick()
    if EscortPeriManager:GetRemainCount() <= 0 then
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(74919))
        return
    end
    if self._isFree then
        self:OnEscortReq()
        return
    end
    if self._ownCount < self._needCount then
        local diff = self._needCount - self._ownCount
        local needMoney = GlobalParamsHelper.GetParamValue(526)[self._needItem] * diff
        GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(74903, {["0"] = needMoney}), 
        function() self:OnCheckMoney(needMoney) end, nil)
    else
        self:OnEscortReq()
    end
end

function EscortPeriView:OnCheckMoney(needMoney)
    local player = GameWorld.Player()
    local ownMoney = player.money_coupons + player.money_coupons_bind
    if needMoney > ownMoney then
        GUIManager.ShowMessageBox(2, LanguageDataHelper.GetContent(58651), 
        function() self:GotoCharge() end, nil)
    else
        self:OnEscortReq()
    end
end

function EscortPeriView:OnEscortReq()
    EscortPeriManager:OnEscortReq(self.data:GetId())
    GUIManager.ClosePanel(PanelsConfig.EscortPeri)
end

function EscortPeriView:GotoCharge()
    --todo 跳转到充值界面
    GUIManager.ClosePanel(PanelsConfig.EscortPeri)
end

function EscortPeriView:ShowCost()
    local cost = self.data:GetCost()
    if cost == nil then
        self._isFree = true
        self._moneyIcon:SetActive(false)
        self._countText.text = LanguageDataHelper.GetContent(74901)
    else
        self._isFree = false
        self._moneyIcon:SetActive(true)
        for k,v in pairs(cost) do
            GameWorld.AddIcon(self._moneyIcon, ItemDataHelper.GetIcon(k))
            local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(k)
            self._countText.text = string.format("%d/%d", ownCount, v)
            self._ownCount = ownCount
            self._needCount = v
            self._needItem = k
        end
    end
end

function EscortPeriView:ShowDouble()
    self._doubleImage:SetActive(EscortPeriManager:IsInDoubleTime())
end
