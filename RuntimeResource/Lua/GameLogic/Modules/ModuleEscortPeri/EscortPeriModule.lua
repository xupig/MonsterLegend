--EscortPeriModule.lua
EscortPeriModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function EscortPeriModule.Init()
     GUIManager.AddPanel(PanelsConfig.EscortPeri,"Panel_EscortPeri",GUILayer.LayerUIPanel,"Modules.ModuleEscortPeri.EscortPeriPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return EscortPeriModule