require "Modules.ModuleEscortPeri.ChildView.EscortPeriView"

local EscortPeriPanel = Class.EscortPeriPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local EscortPeriView = ClassTypes.EscortPeriView
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local EscortPeriManager = PlayerManager.EscortPeriManager

--override
function EscortPeriPanel:Awake()
    self:InitView()
end

--override
function EscortPeriPanel:OnDestroy()

end

function EscortPeriPanel:OnShow(data)
    self:AddEventListeners()
    local listData = EscortPeriManager:GetAllData()
    self._list:SetDataList(listData)
    self:RefreshView()
end

function EscortPeriPanel:OnClose()
    self:RemoveEventListeners()
end

function EscortPeriPanel:AddEventListeners()

end

function EscortPeriPanel:RemoveEventListeners()

end

function EscortPeriPanel:InitView()
    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(EscortPeriView)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, 10, 1)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)
end

function EscortPeriPanel:RefreshView()
    self._descText.text = EscortPeriManager:GetShowTips()
end

function EscortPeriPanel:WinBGType()
    return PanelWinBGType.NormalBG
end