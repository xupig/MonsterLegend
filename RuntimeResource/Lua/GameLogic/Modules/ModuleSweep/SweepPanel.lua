-- SweepPanel.lua

local SweepPanel = Class.SweepPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIToggle = ClassTypes.UIToggle
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local TipsManager = GameManager.TipsManager
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local ItemDataHelper = GameDataHelper.ItemDataHelper

function SweepPanel:Awake()
    self._txtDesc = self:GetChildComponent("Text_Desc", 'TextMeshWrapper')
	self._txtCost = self:GetChildComponent("Text_Cost", 'TextMeshWrapper')
	


	
	self._matIcon = self:AddChildLuaUIComponent("Container_ItemGridEx",ItemGridEx)
	self._matIcon:SetShowName(false)

	--self._onClick = function(pointerEventData)self:OnPointerUp(pointerEventData) end
	--self._matIcon:SetPointerUpCB(self._onClick)

    self._bottom = self:FindChildGO("Container_Bottom")
    self._toggleCost = UIToggle.AddToggle(self.gameObject, "Container_Bottom/Toggle_Cost")
    
    self._btnCancel = self:FindChildGO("Button_Cancel")
	self._csBH:AddClick(self._btnCancel, function()self:OnCancelClick() end)
	
	self._btnSure = self:FindChildGO("Button_Sure")
	self._csBH:AddClick(self._btnSure, function()self:OnSureClick() end)

	self._btnClose = self:FindChildGO("Button_Close")
	self._csBH:AddClick(self._btnClose, function()self:OnCancelClick() end)
	
	self._btnAdd = self:FindChildGO("Container_Bottom/Button_Add")
	self._csBH:AddClick(self._btnAdd,function () self:OnAdd() end)

	self._btnDe = self:FindChildGO("Container_Bottom/Button_De")
	self._csBH:AddClick(self._btnDe,function () self:OnDe() end)

	self._txtCostNum = self:GetChildComponent("Container_Bottom/Text_CostNum", 'TextMeshWrapper')
	self._txtCostDesc = self:GetChildComponent("Container_Bottom/Text_CostDesc", 'TextMeshWrapper')

	self._sweepEndFunc = function(result) self:SweepEnd(result) end
end

function SweepPanel:OnCancelClick()
	GameManager.GUIManager.ClosePanel(PanelsConfig.Sweep)
end

function SweepPanel:OnAdd()
	self._curcount = self._curcount+1
    if self._curcount > self._maxCount then
		self._curcount = 10
	end

	local argsTable = LanguageDataHelper.GetArgsTable()
	argsTable["0"] = self._curcount*GlobalParamsHelper.GetParamValue(700)
	local str = LanguageDataHelper.CreateContent(76332, argsTable)
	self._toggleCost:SetText(str)
	self._txtCostNum.text = tostring(self._curcount)
end

function SweepPanel:OnDe()
	self._curcount = self._curcount-1
	if self._curcount <= 0 then
		self._curcount=1
	end	

	local argsTable = LanguageDataHelper.GetArgsTable()
	argsTable["0"] = self._curcount*GlobalParamsHelper.GetParamValue(700)
	local str = LanguageDataHelper.CreateContent(76332, argsTable)
	self._toggleCost:SetText(str)
	self._txtCostNum.text = tostring(self._curcount)
end

function SweepPanel:OnSureClick()
	self._data:SetSweepEndListener(self._sweepEndFunc)
	--LoggerHelper.Error("SweepPanel:OnSureClick")
	if self._sweepType == 1 then
		if self._matIcon:IsEnough() then
			self._data:SweepInstance()
		else
			self:BuyGuideTip(self._itemId)
		end
		-- self._sureFun()
	end

	if self._sweepType == 2 then
		local num = 0
		if self._toggleCost:GetToggleWrapper().isOn then
			num = self._curcount
		end

		if self._matIcon:IsEnough() then
			self._data:SweepInstance(num,self._toggleCost:GetToggleWrapper().isOn)
		else
			self:BuyGuideTip(self._itemId)
		end
		
		-- self._sureFun(self._curcount,self._toggleCost:GetToggleWrapper().isOn)
	end
end

function SweepPanel:SweepEnd(result)
	if result then
		GameManager.GUIManager.ClosePanel(PanelsConfig.Sweep)
	end
end

function SweepPanel:OnShow(data)
	self._data = data
	local itemId = data:GetItemId()
	local itemNum = data:GetItemNum()
	local sweepFunc = data:GetSweepFunc()
	local sweepType = data:GetSweepType()
	local start = data:GetStart()

	self._txtDesc.text = LanguageDataHelper.CreateContent(58710)
	self._matIcon:UpdateData(itemId,itemNum)
	self._itemId = itemId
	self._sureFun = sweepFunc
    self._sweepType = sweepType
	if self._sweepType == 1 then
		self._txtCost.gameObject:SetActive(true)
		self._bottom:SetActive(false)
		local argsTable = LanguageDataHelper.GetArgsTable()
		argsTable["0"] = start
		argsTable["1"] = start
		self._txtCost.text = LanguageDataHelper.CreateContent(58713, argsTable)
	end

	if self._sweepType == 2 then
		self._txtCost.gameObject:SetActive(false)
		self._bottom:SetActive(true)

		self._txtCostDesc.text = LanguageDataHelper.CreateContent(76326)
		self._curcount = data:GetCostNum()
		self._maxCount = data:GetCostNum()

		local argsTable = LanguageDataHelper.GetArgsTable()
		argsTable["0"] = self._curcount*GlobalParamsHelper.GetParamValue(700)
		local str = LanguageDataHelper.CreateContent(76332, argsTable)
		self._toggleCost:SetText(str)

		self._txtCostNum.text = data:GetCostNum()
	end
end


--override
function SweepPanel:OnPointerDown(pointerEventData)
end

--override
function SweepPanel:OnPointerUp(pointerEventData)
    if self._itemId then
        TipsManager:ShowItemTipsById(self._itemId)    
    end 
end

function SweepPanel:SetPointerUpCB(func)
end

function SweepPanel:OnClose()
	
end

function SweepPanel:InitViews(index)

end

function SweepPanel:BuyGuideTip(itemId)
    if ItemDataHelper.GetGuideBuyCost(itemId)~=0 then
        local btnArgs = {}
        btnArgs.guideBuy = {itemId}
        TipsManager:ShowItemTipsById(itemId,btnArgs)
    end
end