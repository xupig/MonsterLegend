local SubSystemButton = Class.SubSystemButton(ClassTypes.BaseLuaUIComponent)

SubSystemButton.interface = GameConfig.ComponentsConfig.PointableComponent
SubSystemButton.classPath = "Modules.ModuleSubMainUI.ChildComponent.SubSystemButton"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local public_config = GameWorld.public_config
local GuildManager = PlayerManager.GuildManager
local RedPointManager = GameManager.RedPointManager

function SubSystemButton:Awake()
    self._base.Awake(self)
    self._clickFunc = nil
    self:InitView()    
end

function SubSystemButton:InitView()
    self._containerIcon = self:FindChildGO("Container_Icon")
    -- self._containerText = self:FindChildGO("Container_Text")
    -- self._tween = self._containerText:AddComponent(typeof(XGameObjectTweenPosition))
    self._nameMText = self:GetChildComponent("Container_Text/Text_NameM", "TextMeshWrapper")
    self._nameSText = self:GetChildComponent("Container_Text/Text_NameS", "TextMeshWrapper")
    self._imgLock = self:FindChildGO("Image_Lock")
    self._imgRedPoint = self:FindChildGO("Image_RedPoint")
    self._imgRedPoint:SetActive(false)
    self._updateRedpointFunc = function(isActive) self:SetRedPoint(isActive) end
end

function SubSystemButton:ShowInfo(data)
    self.data = data
    local name = FunctionOpenDataHelper:GetPanelOpenName(data.id)
    local desc = FunctionOpenDataHelper:GetDescribe(data.id)
    local icon = FunctionOpenDataHelper:GetPanelOpenIcon(data.id)
    local node = RedPointManager:GetRedPointDataByFirstId(data.id)
    self._id = data.id

    self._nameMText.text = name
    self._nameSText.text = desc
    --判断开启条件
    if FunctionOpenDataHelper:CheckPanelOpen(data.id) then
        self._imgLock:SetActive(false)
        self._isOpen = true
        self._containerIcon:SetActive(true)
        if node then
            node:SetUpdateRedPointFunc(self._updateRedpointFunc)
        end
    else
        self._imgLock:SetActive(true)
        self._isOpen = false
        self._containerIcon:SetActive(false)
        if node then
            node:SetUpdateRedPointFunc(nil)
        end
    end
    GameWorld.AddIcon(self._containerIcon, icon, nil)
end

function SubSystemButton:SetRedPoint(isActive)
    self._imgRedPoint:SetActive(isActive)
end

-- function SubSystemButton:SetXPosition(startX,endX)
--     self._startX = Vector3.New(startX,15,0)
--     self._endX = Vector3.New(endX,15,0)
-- end

-- function SubSystemButton:ShowText()
--     self._containerText:SetActive(true)
--     self._tween:SetFromToPosOnce(self._startX,self._endX,0.2,nil)
-- end

-- function SubSystemButton:HideText()
--     self._containerText:SetActive(false)
-- end

function SubSystemButton:OnPointerDown(pointerEventData)
    --功能未开启
    if self._isOpen == false then
        return
    end

    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_sub_main_ui_button_"..self.data.id)
    GUIManager.ShowPanelByPanelFuncId(self.data.id)
    GameWorld.PlaySound(3)
    GUIManager.reopenSubMainMenu = GUIManager.reopenSubMainMenu + 1
    GUIManager.ClosePanel(PanelsConfig.SubMainUI)
end

function SubSystemButton:OnPointerUp(pointerEventData)

end
