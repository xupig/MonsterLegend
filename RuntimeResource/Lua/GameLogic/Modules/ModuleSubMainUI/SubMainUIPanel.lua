require "Modules.ModuleSubMainUI.ChildComponent.SubSystemButton"

local SubMainUIPanel = Class.SubMainUIPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local SubSystemButton = ClassTypes.SubSystemButton
local RoleDataHelper = GameDataHelper.RoleDataHelper
local RawImage = UnityEngine.UI.RawImage
local LuaUIRawImage = GameMain.LuaUIRawImage
local UIProgressBar = ClassTypes.UIProgressBar
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local FunctionOpenManager = PlayerManager.FunctionOpenManager
local attri_config = require("ServerConfig/attri_config")
local AttriDataHelper = GameDataHelper.AttriDataHelper
local combatData = PlayerManager.PlayerDataManager.combatData
local ItemDataHelper = GameDataHelper.ItemDataHelper
local guildData = PlayerManager.PlayerDataManager.guildData
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local GUILayerManager = GameManager.GUILayerManager
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local SIDE_Count = 7
--override
function SubMainUIPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self._showAttr = {attri_config.ATTRI_ID_TOTAL_PHY_ATK,attri_config.ATTRI_ID_TOTAL_MAG_ATK,
                      attri_config.ATTRI_ID_TOTAL_PHY_DEF,attri_config.ATTRI_ID_TOTAL_MAG_DEF}
    
    self:InitPanel()
    self._bgColor = UnityEngine.Color.New(220/255,220/255,220/255,0/255)
    self.disableCameraCulling = true
end

--override
function SubMainUIPanel:OnDestroy()
	self._csBH = nil
    
end

--override 
function SubMainUIPanel:OnShow(data)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleSubMainUI.SubMainUIPanel")
    --GameWorld.Player().server.attri_get_req()
    --GameWorld.Player().server.server_world_level_req()
    GUILayerManager.HideLayer(GUILayer.LayerUIMain)
    GUILayerManager.HideLayer(GUILayer.LayerUIMiddle)
    self:ShowSystemButton()
    self:ButtonAnimate()
    self:AddEventListeners()
end

local s3Left = Vector3.New(0, 0, 0)
local s2Left = Vector3.New(0, 0, 10)
local s1Left = Vector3.New(0, 0, -90)
local s3Right = Vector3.New(0, 0, 0)
local s2Right = Vector3.New(0, 0, -10)
local s1Right = Vector3.New(0, 0, 90)

local f1Left = Vector3.New(-200, 0, 0)
local f2Left = Vector3.New(0, 0, 0)
local f1Right = Vector3.New(1480, 0, 0)
local f2Right = Vector3.New(1280, 0, 0)

function SubMainUIPanel:ButtonAnimate()
    self._tweenFrameLeft:SetFromToPosOnce(f1Left,f2Left,0.2,nil)
    self._tweenFrameRight:SetFromToPosOnce(f1Right,f2Right,0.2,nil)
    self._leftButtonsRotation:SetFromToRotation(s1Left, s2Left, 0.15, 1,nil)
    self._rightButtonsRotation:SetFromToRotation(s1Right, s2Right, 0.15, 1,nil)
    self._secondStepTimer = TimerHeap:AddSecTimer(0.15,0,0,function ()
        self._leftButtonsRotation:SetFromToRotation(s2Left,s3Left, 0.1, 1,nil)
        self._rightButtonsRotation:SetFromToRotation(s2Right,s3Right, 0.1, 1,nil)
        TimerHeap:DelTimer(self._secondStepTimer)
    end)
end

--override
function SubMainUIPanel:SetData(data)

end

--override
function SubMainUIPanel:OnClose()
    GUILayerManager.ShowLayer(GUILayer.LayerUIMain)
    GUILayerManager.ShowLayer(GUILayer.LayerUIMiddle)
    self:RemoveEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_VIEW, "Modules.ModuleSubMainUI.SubMainUIPanel")
end

function SubMainUIPanel:AddEventListeners()
end

function SubMainUIPanel:RemoveEventListeners()
end

function SubMainUIPanel:InitPanel()
    self._backButton = self:FindChildGO("Button_Back")
    self._csBH:AddClick(self._backButton,function () self:OnBackButtonClick() end)

    self._leftButtons = {}
    self._leftButtonGOs = {}
    for i=1,SIDE_Count do
        self._leftButtonGOs[i] = self:FindChildGO("Container_SystemLeft/Container_System"..i)
        self._leftButtons[i] = self:AddChildLuaUIComponent("Container_SystemLeft/Container_System"..i, SubSystemButton)
        --self._leftButtons[i]:SetXPosition(-320,-120)
        self._leftButtonGOs[i]:SetActive(false)
    end
    self._leftButtonsContainer = self:FindChildGO("Container_SystemLeft")
    self._leftButtonsRotation = self._leftButtonsContainer:AddComponent(typeof(XGameObjectTweenRotation))

    self._rightButtons = {}
    self._rightButtonGOs = {}
    for i=1,SIDE_Count do
        self._rightButtonGOs[i] = self:FindChildGO("Container_SystemRight/Container_System"..i)
        self._rightButtons[i] = self:AddChildLuaUIComponent("Container_SystemRight/Container_System"..i, SubSystemButton)
        --self._rightButtons[i]:SetXPosition(200,80)
        self._rightButtonGOs[i]:SetActive(false)
    end
    self._rightButtonsContainer = self:FindChildGO("Container_SystemRight")
    self._rightButtonsRotation = self._rightButtonsContainer:AddComponent(typeof(XGameObjectTweenRotation))

    self._leftFrame = self:FindChildGO("Container_Frame/Image_FrameLeft")
    self._tweenFrameLeft = self._leftFrame:AddComponent(typeof(XGameObjectTweenPosition))
    self._rightFrame = self:FindChildGO("Container_Frame/Image_FrameRight")
    self._tweenFrameRight = self._rightFrame:AddComponent(typeof(XGameObjectTweenPosition))

    local canvasWidth = GUIManager.canvasWidth
    local dx = (canvasWidth - 1280)/2
    local cutoutHeight = GUIManager.GetCutoutScreenHeight()
    self._leftButtonsContainer.transform.anchoredPosition = Vector2(640-dx+cutoutHeight,360)
    self._rightButtonsContainer.transform.anchoredPosition = Vector2(640+dx-cutoutHeight,360)
    f1Left.x = -200-dx+cutoutHeight
    f2Left.x = -dx+cutoutHeight
    f1Right.x = 1480+dx-cutoutHeight
    f2Right.x = 1280+dx-cutoutHeight
end

function SubMainUIPanel:OnBackButtonClick()
    GUIManager.ClosePanel(PanelsConfig.SubMainUI)
end

function SubMainUIPanel:ShowSystemButton()
    local data = FunctionOpenManager:GetSubMainFuncDatas()
    for k,v in pairs(data) do
        local position = v.position[1]
        local index = position
        if position > SIDE_Count then
            index = position - SIDE_Count
            self._rightButtons[index]:ShowInfo(v)
            self._rightButtonGOs[index]:SetActive(true)
        else
            self._leftButtons[index]:ShowInfo(v)
            self._leftButtonGOs[index]:SetActive(true)
        end
    end
end