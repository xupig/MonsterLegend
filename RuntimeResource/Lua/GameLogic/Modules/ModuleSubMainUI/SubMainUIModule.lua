SubMainUIModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function SubMainUIModule.Init()
    GUIManager.AddPanel(PanelsConfig.SubMainUI,"Panel_SubMainUI",GUILayer.LayerUIPanel,"Modules.ModuleSubMainUI.SubMainUIPanel",true,false)
end

return SubMainUIModule