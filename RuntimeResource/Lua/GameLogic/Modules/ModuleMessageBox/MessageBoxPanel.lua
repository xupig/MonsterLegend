
require "Modules.ModuleMessageBox.ChildView.ExchangeMBView"
require "Modules.ModuleMessageBox.ChildView.BuySuccMBView"
require "Modules.ModuleMessageBox.ChildView.BulkUseMBView"
require "Modules.ModuleMessageBox.ChildView.PropSplitMBView"
require "Modules.ModuleMessageBox.ChildView.TipMBView"
require "Modules.ModuleMessageBox.ChildView.InviteJoinGuildMBView"
require "Modules.ModuleMessageBox.ChildView.RedEnvelopeMBView"
require "Modules.ModuleMessageBox.ChildView.RedEnvelopeRecordMBView"
-- require "Modules.ModuleMessageBox.ChildView.DescriptionTextMBView"
-- require "Modules.ModuleMessageBox.ChildView.ExchangeSilverMBView"
require "Modules.ModuleMessageBox.ChildView.OpenBagView"
require "Modules.ModuleMessageBox.ChildView.ToggleTipsView"
require "Modules.ModuleMessageBox.ChildView.RewardCountDowenTipView"
require "Modules.ModuleMessageBox.ChildView.BuyCountView"
require "Modules.ModuleMessageBox.ChildView.TipMBView1"
require "Modules.ModuleMessageBox.ChildView.TradeSellMBView"
require "Modules.ModuleMessageBox.ChildView.SelectUseMBView"
require "Modules.ModuleMessageBox.ChildView.TicketMBView"
require "Modules.ModuleMessageBox.ChildView.GiftSuccMBView"
require "Modules.ModuleMessageBox.ChildView.FirstChargeMBView"

local MessageBoxPanel = Class.MessageBoxPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local ExchangeMBView = ClassTypes.ExchangeMBView
local BuySuccMBView = ClassTypes.BuySuccMBView
local BulkUseMBView = ClassTypes.BulkUseMBView
local PropSplitMBView = ClassTypes.PropSplitMBView
local TipMBView = ClassTypes.TipMBView
local InviteJoinGuildMBView = ClassTypes.InviteJoinGuildMBView
local RedEnvelopeMBView = ClassTypes.RedEnvelopeMBView
local RedEnvelopeRecordMBView = ClassTypes.RedEnvelopeRecordMBView
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local OpenBagView = ClassTypes.OpenBagView
local ToggleTipsView = ClassTypes.ToggleTipsView
local RewardCountDowenTipView = ClassTypes.RewardCountDowenTipView
local BuyCountView = ClassTypes.BuyCountView
local TipMBView1 = ClassTypes.TipMBView1
local TradeSellMBView = ClassTypes.TradeSellMBView
local SelectUseMBView = ClassTypes.SelectUseMBView
local TicketMBView = ClassTypes.TicketMBView
local GiftSuccMBView = ClassTypes.GiftSuccMBView
local FirstChargeMBView = ClassTypes.FirstChargeMBView
--override
function MessageBoxPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self._curShowView = 0
    self._indexToView = {}
    self._messageboxView = {}
    self:AddEventListeners()
    self:InitPanel()
end

--override
function MessageBoxPanel:OnDestroy()
	self._csBH = nil
    self:RemoveEventListeners()
end

--override
function MessageBoxPanel:OnShow(data)
    local index = data.id or 0
    local value = data.value

    self:ShowView(index, value)
end

function MessageBoxPanel:SetData(data)
    local index = data.id or 0
    local value = data.value
    self:ShowView(index, value)
end

--override
function MessageBoxPanel:OnClose()
    self:CloseView()
end

function MessageBoxPanel:AddEventListeners()

end

function MessageBoxPanel:RemoveEventListeners()

end

function MessageBoxPanel:InitPanel()
    self._indexToView[MessageBoxType.Exchange] = self:FindChildGO("Container_Exchange")
    self._indexToView[MessageBoxType.BuySucc] = self:FindChildGO("Container_BuySucc")   
    self._indexToView[MessageBoxType.Tip] = self:FindChildGO("Container_Tip")
    self._indexToView[MessageBoxType.InviteJoinGuild] = self:FindChildGO("Container_InviteJoinGuild")
    self._indexToView[MessageBoxType.RedEnvelope] = self:FindChildGO("Container_RedEnvelope")
    self._indexToView[MessageBoxType.RedEnvelopeInfo] = self:FindChildGO("Container_RedEnvelopeInfo")
    self._indexToView[MessageBoxType.OpenBag] = self:FindChildGO("Container_OpenBag")
    self._indexToView[MessageBoxType.ToggleTips] = self:FindChildGO("Container_ToggleTips")
    self._indexToView[MessageBoxType.RewardCountDowenTip] = self:FindChildGO("Container_RewardCountDowenTip")
    self._indexToView[MessageBoxType.BuyCountView] = self:FindChildGO("Container_Buy")
    self._indexToView[MessageBoxType.Tip1] = self:FindChildGO("Container_Tip1")
    self._indexToView[MessageBoxType.TradeSell] = self:FindChildGO("Container_TradeSell")
    self._indexToView[MessageBoxType.BulkUse] = self:FindChildGO("Container_BulkUse")
    self._indexToView[MessageBoxType.PropSplit] = self:FindChildGO("Container_PropSplit")
    self._indexToView[MessageBoxType.SelectUse] = self:FindChildGO("Container_SelectUse")
    self._indexToView[MessageBoxType.BackWoodsTicket] = self:FindChildGO("Container_Ticket")
    self._indexToView[MessageBoxType.GiftSucc] = self:FindChildGO("Container_GiftSucc")
    self._indexToView[MessageBoxType.FirstCharge] = self:FindChildGO("Container_FirstCharge")


    self._messageboxView[MessageBoxType.Exchange] = self:AddChildLuaUIComponent("Container_Exchange", ExchangeMBView)
    self._messageboxView[MessageBoxType.BuySucc] = self:AddChildLuaUIComponent("Container_BuySucc", BuySuccMBView)
    self._messageboxView[MessageBoxType.Tip] = self:AddChildLuaUIComponent("Container_Tip", TipMBView)
    self._messageboxView[MessageBoxType.InviteJoinGuild] = self:AddChildLuaUIComponent("Container_InviteJoinGuild", InviteJoinGuildMBView)
    self._messageboxView[MessageBoxType.RedEnvelope] = self:AddChildLuaUIComponent("Container_RedEnvelope", RedEnvelopeMBView)
    self._messageboxView[MessageBoxType.RedEnvelopeInfo] = self:AddChildLuaUIComponent("Container_RedEnvelopeInfo", RedEnvelopeRecordMBView)
    self._messageboxView[MessageBoxType.OpenBag] = self:AddChildLuaUIComponent("Container_OpenBag", OpenBagView)
    self._messageboxView[MessageBoxType.ToggleTips] = self:AddChildLuaUIComponent("Container_ToggleTips", ToggleTipsView)
    self._messageboxView[MessageBoxType.RewardCountDowenTip] = self:AddChildLuaUIComponent("Container_RewardCountDowenTip", RewardCountDowenTipView)
    self._messageboxView[MessageBoxType.BuyCountView] = self:AddChildLuaUIComponent("Container_Buy", BuyCountView)
    self._messageboxView[MessageBoxType.Tip1] = self:AddChildLuaUIComponent("Container_Tip1", TipMBView1)
    self._messageboxView[MessageBoxType.TradeSell] = self:AddChildLuaUIComponent("Container_TradeSell", TradeSellMBView)
    self._messageboxView[MessageBoxType.BulkUse] = self:AddChildLuaUIComponent("Container_BulkUse", BulkUseMBView)
    self._messageboxView[MessageBoxType.PropSplit] = self:AddChildLuaUIComponent("Container_PropSplit", PropSplitMBView)
    self._messageboxView[MessageBoxType.SelectUse] = self:AddChildLuaUIComponent("Container_SelectUse", SelectUseMBView)
    self._messageboxView[MessageBoxType.BackWoodsTicket] = self:AddChildLuaUIComponent("Container_Ticket", TicketMBView)
    self._messageboxView[MessageBoxType.GiftSucc] = self:AddChildLuaUIComponent("Container_GiftSucc", GiftSuccMBView)
    self._messageboxView[MessageBoxType.FirstCharge] = self:AddChildLuaUIComponent("Container_FirstCharge", FirstChargeMBView)
    for k,v in pairs(self._indexToView) do
        v:SetActive(false)
    end
end

function MessageBoxPanel:ShowView(index, data)
    if self._curShowView ~= 0 and index ~= self._curShowView then
        self:CloseView()
    end
    self._indexToView[index]:SetActive(true)
    self._messageboxView[index]:ShowView(data)
    self._curShowView = index
end

function MessageBoxPanel:CloseView()
    if self._curShowView ~= 0 then
        self._indexToView[self._curShowView]:SetActive(false)
        self._curShowView = 0
    end
end

