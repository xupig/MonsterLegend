MessageBoxModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function MessageBoxModule.Init()
    GUIManager.AddPanel(PanelsConfig.MessageBox,"Panel_MessageBox",GUILayer.LayerUICommon,"Modules.ModuleMessageBox.MessageBoxPanel",true,false)
end

return MessageBoxModule