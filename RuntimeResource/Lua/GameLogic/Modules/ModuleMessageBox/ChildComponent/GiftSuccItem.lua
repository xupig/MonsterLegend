-- GiftSuccItem.lua
require "UIComponent.Extend.ItemGrid"
local GiftSuccItem = Class.GiftSuccItem(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
GiftSuccItem.interface = GameConfig.ComponentsConfig.Component
GiftSuccItem.classPath = "Modules.ModuleMessageBox.ChildComponent.GiftSuccItem"
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
function GiftSuccItem:Awake()
    self._ContainerItem = self:FindChildGO("Container_Icon")
    local a = GUIManager.AddItem(self._ContainerItem, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._icon:ActivateTipsById()
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
end

function GiftSuccItem:OnDestroy() 

end

--override
function GiftSuccItem:OnRefreshData(data)
    --LoggerHelper.Error("GiftSuccItem:OnRefreshData"..PrintTable:TableToStr(data))
    if data==nil then
        self._ContainerItem:SetActive(false)
        self._textName.text = ""
        return
    else
        self._ContainerItem:SetActive(true)
        self._icon:SetItem(data._id,data._num)
        self._textName.text = ItemDataHelper.GetItemNameWithColor(data._id)
    end

end
