local RedEnvelopeRecordMBItem = Class.RedEnvelopeRecordMBItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RedEnvelopeRecordMBItem.interface = GameConfig.ComponentsConfig.Component
RedEnvelopeRecordMBItem.classPath = "Modules.ModuleMessageBox.ChildComponent.RedEnvelopeRecordMBItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager

function RedEnvelopeRecordMBItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function RedEnvelopeRecordMBItem:OnDestroy()
    self.data = nil
end

function RedEnvelopeRecordMBItem:InitView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._countText = self:GetChildComponent("Text_Value", "TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Text_Value/Container_Icon")
end

--override
function RedEnvelopeRecordMBItem:OnRefreshData(data)
    self.data = data
    local name = self.data[2]
    local getCount = self.data[3]
    local moneyType = self.data.type
    self._nameText.text = name
    self._countText.text = getCount
    local icon = ItemDataHelper.GetIcon(moneyType)
    GameWorld.AddIcon(self._iconContainer, icon, nil)
end