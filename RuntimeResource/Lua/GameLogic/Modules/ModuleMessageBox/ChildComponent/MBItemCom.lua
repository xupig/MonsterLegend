-- MBItemCom.lua
--道具列表Item_显示物品名称
--local action_config = require("ServerConfig/action_config")
local MBItemCom = Class.MBItemCom(ClassTypes.BaseLuaUIComponent)

MBItemCom.interface = GameConfig.ComponentsConfig.Component
MBItemCom.classPath = "Modules.ModuleMessageBox.ChildComponent.MBItemCom"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

function MBItemCom:Awake()
	self._base.Awake(self)
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")

	self._selectedItem = self:FindChildGO("Container_Already_Get")
	self._itemIcon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)

	self._txtItemName = self:GetChildComponent("Text_ItemName","TextMeshWrapper")
end

function MBItemCom:OnRefreshData(itemId,itemCount)
	self._itemIcon:SetItem(itemId,itemCount)
	self._txtItemName.text = ItemDataHelper.GetItemNameWithColor(itemId)
end

function MBItemCom:SetSelected(isSelected)
	if self._imgSelected == nil then
		self._imgSelected = self:FindChildGO("Image_Selected")
	end

	if self._selectedItem == nil then
		self._selectedItem = self:FindChildGO("Container_Already_Get")
	end

	self._imgSelected:SetActive(isSelected)
	self._selectedItem:SetActive(isSelected)
end

function MBItemCom:SetShowTips()
	self._itemIcon:ActivateTipsById()
end

function MBItemCom:SetPointerDownCB(func)
    self._itemIcon:SetPointerDownCB(func)
end

function MBItemCom:SetPointerUpCB(func)
    self._itemIcon:SetPointerUpCB(func)
end