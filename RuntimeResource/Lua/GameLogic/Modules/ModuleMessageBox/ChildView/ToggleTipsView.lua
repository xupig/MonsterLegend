--带两个按钮的一般显示信息消息框
local ToggleTipsView = Class.ToggleTipsView(ClassTypes.BaseLuaUIComponent)

ToggleTipsView.interface = GameConfig.ComponentsConfig.Component
ToggleTipsView.classPath = "Modules.ModuleMessageBox.ChildView.ToggleTipsView"

local GUIManager = GameManager.GUIManager
local UIToggle = ClassTypes.UIToggle
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RectTransform = UnityEngine.RectTransform

function ToggleTipsView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._confirmButtonCB = nil
    self._cancelButtonCB = nil
    self._notShowTip = false
    self:InitView()
end

function ToggleTipsView:OnDisable()
    self._notShowTip = false
    self._toggleIntrestTips:SetIsOn(self._notShowTip)
    self._toggleContainer:SetActive(true)
end

--override
function ToggleTipsView:OnDestroy() 

end
--data结构 text confirmCB cancelCB confirmButton cancleButton type
function ToggleTipsView:ShowView(data)
    if data.confirmButton == nil then
        data.confirmButton = true
    end

    if data.cancleButton == nil then
        data.cancleButton = true
    end

    self._confirmButtonCB = data.confirmCB
    self._cancelButtonCB = data.cancelCB
    self._confirmButton:SetActive(data.confirmButton)
    self._cancelButton:SetActive(data.cancleButton)
    self._dataType = data.tipType

    self:SetText(data)
end


function ToggleTipsView:SetText(data)
    self._infoText.text = data.text
    self._titleText.text = data.title or LanguageDataHelper.CreateContent(303)
    self._toogleText.text = data.toogleText or ""
    self._toggleIntrestTips:SetIsOn(data.toogleFlag or false)
    self._okText.text = data.okText or LanguageDataHelper.CreateContent(13)
    self._cancelText.text = data.cancelText or LanguageDataHelper.CreateContent(14)
    self._toggleContainer:SetActive(data.toogleActive)

    if data.toogleActive then
        self._infoRect.sizeDelta = self._initSizeDelta
    else
        self._infoRect.sizeDelta = Vector2.New(self._initSizeDelta.x, self._initSizeDelta.y + 80)
    end
end

function ToggleTipsView:InitView()
    self._confirmButton = self:FindChildGO("Container_Buttons/Button_OK")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Container_Buttons/Button_Cancel")
    self._csBH:AddClick(self._cancelButton,function() self:OnCancelButtonClick() end)
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._infoRect = self._infoText.gameObject:GetComponent("RectTransform")
    self._initSizeDelta = self._infoRect.sizeDelta
    self._infoText.text = ""
    self._titleText = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    self._toogleText = self:GetChildComponent("Toggle_Intrest/Label", "TextMeshWrapper")
    self._okText = self:GetChildComponent("Container_Buttons/Button_OK/Text_OK", "TextMeshWrapper")
    self._cancelText = self:GetChildComponent("Container_Buttons/Button_Cancel/Text", "TextMeshWrapper")

    self._toggleContainer = self:FindChildGO("Toggle_Intrest")
    self._toggleIntrestTips = UIToggle.AddToggle(self.gameObject, "Toggle_Intrest")
    self._toggleIntrestTips:SetOnValueChangedCB(function(state)self:OnClickIntrestTips(state) end)
end

function ToggleTipsView:OnConfirmButtonClick()
    if self._toggleIntrestTips:GetToggleWrapper().isOn and self._dataType then
        local settingData = PlayerManager.PlayerDataManager.playerSettingData
        settingData:SetData(self._dataType, DateTimeUtil.GetServerTime())
        settingData:SaveData()
    end

    if self._confirmButtonCB ~= nil then
        self._confirmButtonCB(self._toggleIntrestTips:GetToggleWrapper().isOn)
    end

    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function ToggleTipsView:OnCancelButtonClick()    
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
    if self._cancelButtonCB ~= nil then
        self._cancelButtonCB()
    end
end

function ToggleTipsView:OnClickIntrestTips(state)
    self._notShowTip = state
end