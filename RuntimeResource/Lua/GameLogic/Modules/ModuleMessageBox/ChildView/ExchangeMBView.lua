--钻石兑换金币消息框
local ExchangeMBView = Class.ExchangeMBView(ClassTypes.BaseLuaUIComponent)

ExchangeMBView.interface = GameConfig.ComponentsConfig.Component
ExchangeMBView.classPath = "Modules.ModuleMessageBox.ChildView.ExchangeMBView"

local GUIManager = GameManager.GUIManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ShopManager = PlayerManager.ShopManager
local public_config = GameWorld.public_config

function ExchangeMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self.moneyTypeToIcon = {[2]=public_config.MONEY_TYPE_COUPONS, [4]=public_config.MONEY_TYPE_COUPONS_BIND, [5]=public_config.MONEY_TYPE_GOLD}
    self:InitView()
end
--override
function ExchangeMBView:OnDestroy() 

end
--
function ExchangeMBView:ShowView(data)

end

function ExchangeMBView:InitView()
    self.bg = self:FindChildGO("Image_BG")
    self._csBH:AddClick(self.bg,function () self:OnBGButtonClick() end)
    self.costIcon = self:FindChildGO("Container_Desc/Container_Cost")
    self.getIcon = self:FindChildGO("Container_Desc/Container_Get")
    local costId = self.moneyTypeToIcon[2]
    GameWorld.AddIcon(self.costIcon,ItemDataHelper.GetIcon(costId))
    local getId = self.moneyTypeToIcon[4]
    GameWorld.AddIcon(self.getIcon,ItemDataHelper.GetIcon(getId))

    local diamondToGold = GlobalParamsHelper.GetParamValue(330)
    local diamondCount = 100
    local goldCount = 100
    for k,v in pairs(diamondToGold) do
        diamondToGold = k
        goldCount = v
    end
    self.costCountText = self:GetChildComponent("Container_Desc/Text_Cost", "TextMeshWrapper")
    self.costCountText.text = diamondCount
    self.getCountText = self:GetChildComponent("Container_Desc/Text_Get", "TextMeshWrapper")
    self.getCountText.text = goldCount

    self._confirmButton = self:FindChildGO("Button_ExchangeOne")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Button_ExchangeTen")
    self._csBH:AddClick(self._cancelButton,function() self:OnCancelButtonClick() end)
end

function ExchangeMBView:OnBGButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function ExchangeMBView:OnConfirmButtonClick()
    ShopManager:DiamondToGold(1)
end

function ExchangeMBView:OnCancelButtonClick()
    ShopManager:DiamondToGold(2)
end