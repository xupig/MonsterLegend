require "UIComponent.Extend.ItemGrid"

--使用物品弹窗
local PropSplitMBView = Class.PropSplitMBView(ClassTypes.BaseLuaUIComponent)

PropSplitMBView.interface = GameConfig.ComponentsConfig.Component

PropSplitMBView.classPath = "Modules.ModuleMessageBox.ChildView.PropSplitMBView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UISlider = ClassTypes.UISlider
local UIComponentUtil = GameUtil.UIComponentUtil
local BagManager = PlayerManager.BagManager
--local ShopDataHelper = GameDataHelper.ShopDataHelper

function PropSplitMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function PropSplitMBView:OnDestroy() 

end




--data结构 shopId count
function PropSplitMBView:ShowView(itemData)
    
    self._selectedItemData = itemData
	self._maxNum = itemData:GetCount()
	self._numSlider:SetMaxValue(self._maxNum)
    self._numSlider:SetMinValue(0.99)
	self._numSlider:SetValue(1)

    self._itemIcon:SetItemData(itemData)
    self._txtItemName.text = ItemDataHelper.GetItemNameWithColor(itemData.cfg_id)
    
    --self._txtTaxTips.text = LanguageDataHelper.CreateContentWithArgs(73507,{["0"] = vipLevel,["1"] = rate})
    --self._inputTextPw.interactable = vipLevel>=4
    --self:SetTotalPrice()
end

function PropSplitMBView:InitView()
	
	self._btnClosePopup = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClosePopup, function() self:OnCloseClick() end)


   	self._btnPlus = self:FindChildGO("Container_Num/Button_Plus_BG")
    self._csBH:AddClick(self._btnPlus, function() self:OnPlusClick() end)

    self._btnReduce = self:FindChildGO("Container_Num/Button_Reduce_BG")
    self._csBH:AddClick(self._btnReduce, function() self:OnReduceClick() end)

    self._btnConfirm = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(self._btnConfirm, function() self:OnConfirmClick() end)

    self._btnCancel = self:FindChildGO("Button_Cancel")
    self._csBH:AddClick(self._btnCancel, function() self:OnCloseClick() end)

    local parent = self:FindChildGO("Container_Item")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._itemIcon:ActivateTipsByItemData()

   	self._txtItemName = self:GetChildComponent("Text_Name",'TextMeshWrapper')

   	self._numSlider = UISlider.AddSlider(self.gameObject, "Container_Num/Slider")
    self._numSlider:SetOnValueChangedCB(function() self:SellNumChange() end)
end

function PropSplitMBView:OnCloseClick()
	GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function PropSplitMBView:SellNumChange()
    
end

function PropSplitMBView:OnPlusClick()
	local num = self._numSlider:GetValue()
    if num == self._maxNum then return end
    self._numSlider:SetValue(num + 1)
end

function PropSplitMBView:OnReduceClick()
	local num = self._numSlider:GetValue()
    if num == 1 then return end
    self._numSlider:SetValue(num -1)
end

function PropSplitMBView:OnConfirmClick()
    BagManager:RequestSplit(self._selectedItemData.bagPos,self._numSlider:GetValue())
    GUIManager.ClosePanel(PanelsConfig.MessageBox)   
end