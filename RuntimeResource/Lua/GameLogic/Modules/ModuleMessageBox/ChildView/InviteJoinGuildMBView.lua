--邀请加入公会弹窗
local InviteJoinGuildMBView = Class.InviteJoinGuildMBView(ClassTypes.BaseLuaUIComponent)

InviteJoinGuildMBView.interface = GameConfig.ComponentsConfig.Component
InviteJoinGuildMBView.classPath = "Modules.ModuleMessageBox.ChildView.InviteJoinGuildMBView"

local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local GuildManager = PlayerManager.GuildManager

function InviteJoinGuildMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function InviteJoinGuildMBView:OnDestroy() 

end
--data结构 text confirmCB cancelCB
function InviteJoinGuildMBView:ShowView(data)
    self.data = data
    self:ShowDesc(data)
end

function InviteJoinGuildMBView:InitView()
    self.bg = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self.bg,function() self:OnBGButtonClick() end)
    self._confirmButton = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Button_Cancel")
    self._csBH:AddClick(self._cancelButton,function() self:OnCancelButtonClick() end)
    self.descText = self:GetChildComponent("Text_Notice", "TextMeshWrapper")
    self.descText.text = ""
end

function InviteJoinGuildMBView:OnBGButtonClick()
    self:OnClose()
end

function InviteJoinGuildMBView:OnConfirmButtonClick()
    --同意邀请
    local dbid = self.data[1]
    GuildManager:GuildReplyInviteReq(dbid, 0)
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function InviteJoinGuildMBView:OnCancelButtonClick()
    self:OnClose()
end

function InviteJoinGuildMBView:OnClose()
    local dbid = self.data[1]
    GuildManager:GuildReplyInviteReq(dbid, 1)
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function InviteJoinGuildMBView:ShowDesc(data)
    local guildName = data[2]
    local inviteName = data[3]
    self.descText.text = string.format("%s 邀请你加入公会 %s", inviteName, guildName) ---待中文配置
end