--邀请加入公会弹窗
local RedEnvelopeMBView = Class.RedEnvelopeMBView(ClassTypes.BaseLuaUIComponent)

RedEnvelopeMBView.interface = GameConfig.ComponentsConfig.Component
RedEnvelopeMBView.classPath = "Modules.ModuleMessageBox.ChildView.RedEnvelopeMBView"

local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local GuildManager = PlayerManager.GuildManager
local RoleDataHelper = GameDataHelper.RoleDataHelper

function RedEnvelopeMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function RedEnvelopeMBView:OnDestroy() 

end

function RedEnvelopeMBView:ShowView(data)
    self.data = data
    self:ShowDesc(data)
end

function RedEnvelopeMBView:InitView()
    self.bg = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self.bg,function() self:OnBGButtonClick() end)
    self._snatchButton = self:FindChildGO("Container_Snatch/Button_Snatch")
    self._csBH:AddClick(self._snatchButton,function() self:OnSnatchButtonClick() end)

    self._iconContainer = self:FindChildGO("Container_Snatch/Container_Icon")
    self._nameText = self:GetChildComponent("Container_Snatch/Text_Name", "TextMeshWrapper")
    self._redEnvelopeNameText = self:GetChildComponent("Container_Snatch/Text_Info", "TextMeshWrapper")
end

function RedEnvelopeMBView:OnBGButtonClick()
    self:OnClose()
end

function RedEnvelopeMBView:OnSnatchButtonClick()
    local id = self.data[public_config.GUILD_RED_ENVELOPE_INFO_ID]
    GuildManager:GuildGetRedEnvelopeReq(id)
    self:OnClose()
end

function RedEnvelopeMBView:OnClose()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function RedEnvelopeMBView:ShowDesc(data)
    local nameId = self.data[public_config.GUILD_RED_ENVELOPE_INFO_CFG_ID] or 1
    local name = LanguageDataHelper.GetContent(nameId)
    local playerName = self.data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_NAME]
    local vocation = self.data[public_config.GUILD_RED_ENVELOPE_INFO_VOCATION]
    local headIcon = RoleDataHelper.GetHeadIconByVocation(vocation)
    GameWorld.AddIcon(self._iconContainer, headIcon, nil)
    self._nameText.text = playerName
    self._redEnvelopeNameText.text = name
end