require "UIComponent.Extend.ItemGrid"

--购买成功消息框
local FirstChargeMBView = Class.FirstChargeMBView(ClassTypes.BaseLuaUIComponent)

FirstChargeMBView.interface = GameConfig.ComponentsConfig.Component
FirstChargeMBView.classPath = "Modules.ModuleMessageBox.ChildView.FirstChargeMBView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local UIParticle = ClassTypes.UIParticle
local ItemDataHelper = GameDataHelper.ItemDataHelper
require"Modules.ModuleMessageBox.ChildComponent.GiftSuccItem"
local GiftSuccItem = ClassTypes.GiftSuccItem

function FirstChargeMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function FirstChargeMBView:OnDestroy() 

end
function FirstChargeMBView:OnEnable() 
    -- self._iconBg1 = self:FindChildGO("Image_BG10")
    -- self._iconBg2 = self:FindChildGO("Image_BG11")
    -- GameWorld.AddIcon(self._iconBg1,13523)
    -- GameWorld.AddIcon(self._iconBg2,13523)
end


function FirstChargeMBView:OnDisable() 
    -- self._iconBg1=nil
    -- self._iconBg2=nil
end

--data结构 shopId count
function FirstChargeMBView:ShowView(data)
    self._itemData = data._itemData
    local itemData = self._itemData
    self._requestUseCb = data._func
    self._txtItemName.text = itemData:GetItemName()
    local itemInfo = ItemDataHelper.GetItemEffect(itemData.cfgData.id)
     
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = itemInfo[3]
    self._txtDesc.text = LanguageDataHelper.CreateContent(80827, argsTable)
    
    self._txtItemOrigin.text = '<s>' .. itemInfo[4] .. '</s>'
    self._txtItemCurrent.text = itemInfo[3]
    local _iconList = {}
    local vocationGroup = GameWorld.Player():GetVocationGroup()
    for i,v in ipairs(itemInfo) do
        if i>=5 and (i-5)%3 == 0 then
            if itemInfo[i] == vocationGroup then
                local iconId =  itemInfo[i+1]
                local num = itemInfo[i+2]
                local _iconItem = {_id = iconId, _num = num}
                table.insert( _iconList, _iconItem)  
            end
        end
    end

   

    GameWorld.AddIcon(self._iconOrigin, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    GameWorld.AddIcon(self._iconCurrent, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))

    --LoggerHelper.Error("itemInfo"..PrintTable:TableToStr(itemInfo))
    -- GameWorld.PlaySound(15)
    -- self._itemList = data[2]

    -- local _iconList = {}
    -- for k,v in pairs(self._itemList) do
    --     local itemId =  k
    --     local num = v
    --     local _iconItem = {_id = itemId, _num = num}
    --     table.insert( _iconList, _iconItem)
    -- end





    if #_iconList == 1 then
        self._cardList:SetPadding(0, 10, 0, 220-(#_iconList)*20)
    elseif #_iconList == 2 then
        self._cardList:SetPadding(0, 10, 0, 260-(#_iconList)*45)
    elseif  #_iconList == 3 then
        self._cardList:SetPadding(0, 10, 0, 250-(#_iconList)*45)
    elseif  #_iconList == 4 then
        self._cardList:SetPadding(0, 10, 0, 250-(#_iconList)*45)
    else
        self._cardList:SetPadding(0, 10, 0, 10)
    end 

    self._cardList:SetDataList(_iconList)
end

function FirstChargeMBView:InitView()
    -- self.bg = self:FindChildGO("Button_BG")
    -- self._csBH:AddClick(self.bg,function () self:OnBGButtonClick() end)

    -- self._fx = UIParticle.AddParticle(self.gameObject,"Container_Fx/fx_ui_30009")

    self._txtItemName = self:GetChildComponent("Text_Title",'TextMeshWrapper')
    self._txtItemOrigin = self:GetChildComponent("Text_Origin_Num",'TextMeshWrapper')
    self._txtItemCurrent = self:GetChildComponent("Text_Current_Num",'TextMeshWrapper')
    self._txtDesc = self:GetChildComponent("Text_Desc",'TextMeshWrapper')

    self._iconOrigin = self:FindChildGO("Container_Icon_Ori")
    self._iconCurrent = self:FindChildGO("Container_Icon_Cur")


    self._btnCancel = self:FindChildGO("Button_Cancel")
    self._csBH:AddClick(self._btnCancel, function() self:OnCancelClick() end)

    self._btnUse = self:FindChildGO("Button_Use")
    self._csBH:AddClick(self._btnUse, function() self:OnUseClick() end)

    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function() self:OnCancelClick() end)

    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._cardList = list
    self._cardView = scrollview
    self._cardList:SetItemType(GiftSuccItem)
    self._cardView:SetHorizontalMove(true)
    self._cardView:SetVerticalMove(false)
    self._cardList:SetPadding(0, 0, 0, 0)
    self._cardList:SetGap(15, 30)
    self._cardList:SetDirection(UIList.DirectionLeftToRight, 100, 1)
    local itemBTClickedCB =
        function(index)
            self:OnCardItemBTClicked(index)
        end
    self._cardList:SetItemClickedCB(itemBTClickedCB)
end



function FirstChargeMBView:OnCancelClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end


function FirstChargeMBView:OnUseClick()
    if self._requestUseCb then
        self._requestUseCb()
    end
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function FirstChargeMBView:ShowItem(itemId)
    self._matIcon:SetItem(itemId)
end

function FirstChargeMBView:ShowItemName(name)
    self.nameText.text = name
end

function FirstChargeMBView:ShowItemCount(count)
    self.countText.text = count
end