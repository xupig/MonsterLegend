require "UIComponent.Extend.ItemGrid"

--使用物品弹窗
local BulkUseMBView = Class.BulkUseMBView(ClassTypes.BaseLuaUIComponent)

BulkUseMBView.interface = GameConfig.ComponentsConfig.Component

BulkUseMBView.classPath = "Modules.ModuleMessageBox.ChildView.BulkUseMBView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UISlider = ClassTypes.UISlider
local UIComponentUtil = GameUtil.UIComponentUtil
local BagManager = PlayerManager.BagManager

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
--local ShopDataHelper = GameDataHelper.ShopDataHelper

function BulkUseMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function BulkUseMBView:OnDestroy() 

end




--data结构 shopId count
function BulkUseMBView:ShowView(itemData)
    
    self._selectedItemData = itemData
	self._maxNum = itemData:GetCount()
	self._numSlider:SetMaxValue(self._maxNum)
    self._numSlider:SetMinValue(0.99)
	self._numSlider:SetValue(self._maxNum)
    self:CheckOffineTime()
    self._itemIcon:SetItemData(itemData)
    self._txtItemName.text = ItemDataHelper.GetItemNameWithColor(itemData.cfg_id)
    
    --self._txtTaxTips.text = LanguageDataHelper.CreateContentWithArgs(73507,{["0"] = vipLevel,["1"] = rate})
    --self._inputTextPw.interactable = vipLevel>=4
    --self:SetTotalPrice()
end

function BulkUseMBView:InitView()
	
	self._btnClosePopup = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClosePopup, function() self:OnCloseClick() end)


   	self._btnPlus = self:FindChildGO("Container_Num/Button_Plus_BG")
    self._csBH:AddClick(self._btnPlus, function() self:OnPlusClick() end)

    self._btnReduce = self:FindChildGO("Container_Num/Button_Reduce_BG")
    self._csBH:AddClick(self._btnReduce, function() self:OnReduceClick() end)

    self._btnBulkUse = self:FindChildGO("Button_Use")
    self._csBH:AddClick(self._btnBulkUse, function() self:OnBulkUseClick() end)

    local parent = self:FindChildGO("Container_Item")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._itemIcon:ActivateTipsByItemData()

   	self._txtItemName = self:GetChildComponent("Text_Name",'TextMeshWrapper')

   	self._numSlider = UISlider.AddSlider(self.gameObject, "Container_Num/Slider")
    self._numSlider:SetOnValueChangedCB(function() self:SellNumChange() end)
end

function BulkUseMBView:OnCloseClick()
	GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function BulkUseMBView:SellNumChange()
    
end

function BulkUseMBView:OnPlusClick()
	local num = self._numSlider:GetValue()
    if num == self._maxNum then return end
    self._numSlider:SetValue(num + 1)
end

function BulkUseMBView:OnReduceClick()
	local num = self._numSlider:GetValue()
    if num == 1 then return end
    self._numSlider:SetValue(num -1)
end

function BulkUseMBView:OnBulkUseClick()
    local offineList = GlobalParamsHelper.GetParamValue(855)
    for i,v in ipairs(offineList) do
        if tonumber(v[2]) == self._selectedItemData.cfg_id then
            local str = LanguageDataHelper.CreateContent(52092)
            if GameWorld.Player().left_hang_up_time+self._numSlider:GetValue()*v[1] > GlobalParamsHelper.GetParamValue(859) then
                GUIManager.ShowMessageBox(2, str,
                function() self:OnBulkUseSure() end)
                return
            else
                self:OnBulkUseSure()
                return
            end
            break
        end
    end

    self:OnBulkUseSure()
end

function BulkUseMBView:OnBulkUseSure()
    local offineList = GlobalParamsHelper.GetParamValue(855)
    for i,v in ipairs(offineList) do
        if tonumber(v[2]) == self._selectedItemData.cfg_id then
            local value = 0
            local str = LanguageDataHelper.CreateContent(52092)
            local sliderValue = math.ceil(((GlobalParamsHelper.GetParamValue(859) - GameWorld.Player().left_hang_up_time)/v[1]))
            if sliderValue > 0 then
                if self._numSlider:GetValue() > sliderValue then
                    BagManager:RequestUse(self._selectedItemData.bagPos,sliderValue,0)
                    GUIManager.ClosePanel(PanelsConfig.MessageBox) 
                    return
                end
            else
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(52119))
                GUIManager.ClosePanel(PanelsConfig.MessageBox) 
                return
            end
        end
    end


    BagManager:RequestUse(self._selectedItemData.bagPos,self._numSlider:GetValue(),0)
    GUIManager.ClosePanel(PanelsConfig.MessageBox)   
end

--检测挂机是否小于某个值
function BulkUseMBView:CheckOffineTime()
    local offineList = GlobalParamsHelper.GetParamValue(855)
    for i,v in ipairs(offineList) do
        if tonumber(v[2]) == self._selectedItemData.cfg_id then
            local str = LanguageDataHelper.CreateContent(52092)
            local sliderValue = math.ceil(((GlobalParamsHelper.GetParamValue(859) - GameWorld.Player().left_hang_up_time)/v[1]))
            if sliderValue > 0 then
                if sliderValue > self._maxNum then
                    self._numSlider:SetValue(self._maxNum)
                else
                    self._numSlider:SetValue(sliderValue)
                end   
            else
                self._numSlider:SetValue(1)
            end
        end
    end
end

