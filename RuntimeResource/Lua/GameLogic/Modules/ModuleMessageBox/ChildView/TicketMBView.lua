--黑暗禁地二次确认框
local TicketMBView = Class.TicketMBView(ClassTypes.BaseLuaUIComponent)

TicketMBView.interface = GameConfig.ComponentsConfig.Component
TicketMBView.classPath = "Modules.ModuleMessageBox.ChildView.TicketMBView"

local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local BackWoodsManager = PlayerManager.BackWoodsManager
local bagData = PlayerManager.PlayerDataManager.bagData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemGrid = ClassTypes.ItemGrid
local public_config = GameWorld.public_config
local BaseUtil = GameUtil.BaseUtil

function TicketMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._confirmButtonCB = nil
    self._cancelButtonCB = nil
    self:InitView()
end
--override
function TicketMBView:OnDestroy() 

end
--data结构 
function TicketMBView:ShowView(data)
    self.data = data
    self:ShowDesc()
end

function TicketMBView:InitView()
    self._ticketIconGO = self:FindChildGO("Container_Icon")
    local itemGo = GUIManager.AddItem(self._ticketIconGO, 1)
    self._ticketIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._ticketInfoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._ticketTipsText = self:GetChildComponent("Text_Tips", "TextMeshWrapper")
    self._ticketCountText = self:GetChildComponent("Text_Count", "TextMeshWrapper")
    self._ticketOKButton = self:FindChildGO("Button_OK")
    self._csBH:AddClick(self._ticketOKButton, function()self:TicketOKBtnClick() end)
    self._ticketCloseButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._ticketCloseButton, function()self:TicketCloseBtnClick() end)
end

function TicketMBView:TicketOKBtnClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
    if self.data ~= nil then
        BackWoodsManager:GotoBoss(self.data[1], self.data[2])
    end
end

function TicketMBView:TicketCloseBtnClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function TicketMBView:ShowDesc()
    self._ticketTipsText.text = LanguageDataHelper.CreateContentWithArgs(56050, {["0"]=1})
    local count = BackWoodsManager:GetMaxCount() - BackWoodsManager:GetRemainCount()
    local data = GlobalParamsHelper.GetParamValue(558)[count]
    local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(data[2])
    self._ticketIcon:SetItem(data[2])
    self._ticketCountText.text = BaseUtil.GetColorStringByCount(bagCount, data[1])
    self._ticketInfoText.text = LanguageDataHelper.CreateContentWithArgs(56049, {["0"]=data[1]})
    local diff = data[1] - bagCount
    if diff > 0 then
        local money = GlobalParamsHelper.GetParamValue(548)[data[2]] * diff
        self._ticketTipsText.text = LanguageDataHelper.CreateContentWithArgs(56050, {["0"]=money})
    else
        self._ticketTipsText.text = ""
    end
end