require "UIComponent.Extend.ItemGrid"

--购买成功消息框
local BuySuccMBView = Class.BuySuccMBView(ClassTypes.BaseLuaUIComponent)

BuySuccMBView.interface = GameConfig.ComponentsConfig.Component
BuySuccMBView.classPath = "Modules.ModuleMessageBox.ChildView.BuySuccMBView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local ShopDataHelper = GameDataHelper.ShopDataHelper
local UIParticle = ClassTypes.UIParticle
local ItemDataHelper = GameDataHelper.ItemDataHelper

function BuySuccMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function BuySuccMBView:OnDestroy() 

end
function BuySuccMBView:OnEnable() 
    self._iconBg1 = self:FindChildGO("Image_BG10")
    self._iconBg2 = self:FindChildGO("Image_BG11")
    GameWorld.AddIcon(self._iconBg1,13523)
    GameWorld.AddIcon(self._iconBg2,13523)
end


function BuySuccMBView:OnDisable() 
    self._iconBg1=nil
    self._iconBg2=nil
end

--data结构 shopId count
function BuySuccMBView:ShowView(data)

    GameWorld.PlaySound(15)
    local shopId = data.shopId  --商城购买
    local composeItemId = data.composeItemId --合成获得
    local name
    local itemId
    if shopId then
        name = ShopDataHelper:GetItemName(shopId)
        itemId = ShopDataHelper:GetItemId(shopId)
    end
    if composeItemId then
        itemId = composeItemId
        name = ItemDataHelper.GetItemNameWithColor(itemId)
    end
    self:ShowItem(itemId)
    self:ShowItemCount(data.count)
    self:ShowItemName(name)
end

function BuySuccMBView:InitView()
    self.bg = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self.bg,function () self:OnBGButtonClick() end)
    self._iconContainer = self:FindChildGO("Container_Icon")
    local itemGo1 = GUIManager.AddItem(self._iconContainer, 1)
	self._matIcon = UIComponentUtil.AddLuaUIComponent(itemGo1, ItemGrid)
    self.nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self.countText = self:GetChildComponent("Text_Count", "TextMeshWrapper")

    self._fx = UIParticle.AddParticle(self.gameObject,"Container_Fx/fx_ui_30009")
end

function BuySuccMBView:OnBGButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function BuySuccMBView:OnConfirmButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function BuySuccMBView:ShowItem(itemId)
    self._matIcon:SetItem(itemId)
end

function BuySuccMBView:ShowItemName(name)
    self.nameText.text = name
end

function BuySuccMBView:ShowItemCount(count)
    self.countText.text = count
end