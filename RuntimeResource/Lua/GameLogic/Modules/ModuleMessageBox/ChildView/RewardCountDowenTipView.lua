--带两个按钮的一般显示信息消息框
local RewardCountDowenTipView = Class.RewardCountDowenTipView(ClassTypes.BaseLuaUIComponent)

RewardCountDowenTipView.interface = GameConfig.ComponentsConfig.Component
RewardCountDowenTipView.classPath = "Modules.ModuleMessageBox.ChildView.RewardCountDowenTipView"

local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

require"Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem

function RewardCountDowenTipView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._confirmButtonCB = nil
    self._cancelButtonCB = nil
    self:InitView()
end

function RewardCountDowenTipView:OnDisable()
    self:ClearTimer()
end

--override
function RewardCountDowenTipView:OnDestroy() 

end
--data结构 text confirmCB cancelCB confirmButton cancleButton seconds rewards
function RewardCountDowenTipView:ShowView(data)
    if data.confirmButton == nil then
        data.confirmButton = true
    end

    if data.cancleButton == nil then
        data.cancleButton = false
    end

    self._seconds = data.seconds or 5
    self._confirmButtonCB = data.confirmCB
    self._cancelButtonCB = data.cancelCB
    self._rewards = data.rewards or {}
    self._rewardGridLauoutGroup:SetDataList(self._rewards)
    self._descText.text = data.text
    self:AddTimer()
end

function RewardCountDowenTipView:AddTimer()
    local s = self._seconds
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()
        if self._seconds < 0 then
            self:ClearTimer()
            self:OnConfirmButtonClick()
        end
        self._confirmText.text = LanguageDataHelper.CreateContent(13) .. "(" .. self._seconds .. ")"
        self._seconds = self._seconds - 1
    end)
end

function RewardCountDowenTipView:ClearTimer()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function RewardCountDowenTipView:InitView()
    self._confirmButton = self:FindChildGO("Container_Buttons/Button_OK")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Container_Buttons/Button_Cancel")
    self._csBH:AddClick(self._cancelButton,function() self:OnCancelButtonClick() end)
    self._descText = self:GetChildComponent("Container_Desc/Text_Desc", "TextMeshWrapper")
    self._descText.text = ""
    self._confirmText = self:GetChildComponent("Container_Buttons/Button_OK/Text_OK", "TextMeshWrapper")
    
    
    self:InitRewardGridLayoutGroup()
end

function RewardCountDowenTipView:InitRewardGridLayoutGroup()
    self._rewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Rewards")
    self._rewardGridLauoutGroup:SetItemType(RewardItem)
end

function RewardCountDowenTipView:OnConfirmButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
    if self._confirmButtonCB ~= nil then
        self._confirmButtonCB()
    end
end

function RewardCountDowenTipView:OnCancelButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
    if self._cancelButtonCB ~= nil then
        self._cancelButtonCB()
    end
end