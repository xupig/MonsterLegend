require "UIComponent.Extend.ItemGrid"

--购买成功消息框
local GiftSuccMBView = Class.GiftSuccMBView(ClassTypes.BaseLuaUIComponent)

GiftSuccMBView.interface = GameConfig.ComponentsConfig.Component
GiftSuccMBView.classPath = "Modules.ModuleMessageBox.ChildView.GiftSuccMBView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local UIParticle = ClassTypes.UIParticle
local ItemDataHelper = GameDataHelper.ItemDataHelper
require"Modules.ModuleMessageBox.ChildComponent.GiftSuccItem"
local GiftSuccItem = ClassTypes.GiftSuccItem

function GiftSuccMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function GiftSuccMBView:OnDestroy() 

end
function GiftSuccMBView:OnEnable() 
    self._iconBg1 = self:FindChildGO("Image_BG10")
    self._iconBg2 = self:FindChildGO("Image_BG11")
    GameWorld.AddIcon(self._iconBg1,13523)
    GameWorld.AddIcon(self._iconBg2,13523)
end


function GiftSuccMBView:OnDisable() 
    self._iconBg1=nil
    self._iconBg2=nil
end

--data结构 shopId count
function GiftSuccMBView:ShowView(data)
    GameWorld.PlaySound(15)
    self._itemContain = data[1]
    self._itemList = data[2]


    local _iconList = {}
    if data[1] ~= -1 then
        for k,v in pairs(self._itemList) do
            local itemId =  k
            local num = v
            local _iconItem = {_id = itemId, _num = num}
            table.insert( _iconList, _iconItem)
        end
    else
        for k,v in pairs(self._itemList) do
            local itemId =  v
            local num = 1
            local _iconItem = {_id = itemId, _num = num}
            table.insert( _iconList, _iconItem)
        end
    end






    if #_iconList == 1 then
        self._cardList:SetPadding(50, 10, 0, 220-(#_iconList)*20)
    elseif #_iconList == 2 then
        self._cardList:SetPadding(50, 10, 0, 260-(#_iconList)*50)
    elseif  #_iconList == 3 then
        self._cardList:SetPadding(50, 10, 0, 250-(#_iconList)*50)
    elseif  #_iconList == 4 then
        self._cardList:SetPadding(50, 10, 0, 250-(#_iconList)*50)
    else
        self._cardList:SetPadding(20, 10, 5, 10)
    end 



    self._cardList:SetDataList(_iconList)
end

function GiftSuccMBView:InitView()
    self.bg = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self.bg,function () self:OnBGButtonClick() end)

    self._fx = UIParticle.AddParticle(self.gameObject,"Container_Fx/fx_ui_30009")

    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    self._cardList = list
    self._cardView = scrollview
    self._cardList:SetItemType(GiftSuccItem)
    self._cardList:SetPadding(5, 0, 0, 0)
    self._cardList:SetGap(15, 30)
    self._cardList:SetDirection(UIList.DirectionLeftToRight, 5, 100)
    local itemBTClickedCB =
        function(index)
            self:OnCardItemBTClicked(index)
        end
    self._cardList:SetItemClickedCB(itemBTClickedCB)
end

function GiftSuccMBView:OnBGButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function GiftSuccMBView:OnConfirmButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function GiftSuccMBView:ShowItem(itemId)
    self._matIcon:SetItem(itemId)
end

function GiftSuccMBView:ShowItemName(name)
    self.nameText.text = name
end

function GiftSuccMBView:ShowItemCount(count)
    self.countText.text = count
end