--邀请加入公会弹窗
require "Modules.ModuleMessageBox.ChildComponent.RedEnvelopeRecordMBItem"

local RedEnvelopeRecordMBView = Class.RedEnvelopeRecordMBView(ClassTypes.BaseLuaUIComponent)

RedEnvelopeRecordMBView.interface = GameConfig.ComponentsConfig.Component
RedEnvelopeRecordMBView.classPath = "Modules.ModuleMessageBox.ChildView.RedEnvelopeRecordMBView"

local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local GuildManager = PlayerManager.GuildManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RedEnvelopeRecordMBItem = ClassTypes.RedEnvelopeRecordMBItem
local UIList = ClassTypes.UIList
local RoleDataHelper = GameDataHelper.RoleDataHelper

function RedEnvelopeRecordMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function RedEnvelopeRecordMBView:OnDestroy() 

end

function RedEnvelopeRecordMBView:ShowView(data)
    self.data = data
    self:ShowDesc(data)
end

function RedEnvelopeRecordMBView:InitView()
    self.bg = self:FindChildGO("Button_BG")
    self._csBH:AddClick(self.bg,function() self:OnBGButtonClick() end)

    self._iconContainer = self:FindChildGO("Container_Open/Container_Head/Container_Icon")
    self._moneyIconContainer = self:FindChildGO("Container_Open/Container_Head/Container_MoneyIcon")
    self._playerNameText = self:GetChildComponent("Container_Open/Container_Head/Text_PlayerName", "TextMeshWrapper")
    self._redEnvelopeNameText = self:GetChildComponent("Container_Open/Container_Head/Text_Info", "TextMeshWrapper")
    self._countText = self:GetChildComponent("Container_Open/Container_Head/Text_Count", "TextMeshWrapper")

    self._moneyIcon = self:FindChildGO("Container_Open/Container_Middle/Container_MoneyIcon")
    self._getCountInfoText = self:GetChildComponent("Container_Open/Container_Middle/Text_Count", "TextMeshWrapper")
    self._valueCountInfoText = self:GetChildComponent("Container_Open/Container_Middle/Text_Value", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Open/ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(RedEnvelopeRecordMBItem)
    self._list:SetPadding(0, 0, 0, 10)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
	self._list:SetItemClickedCB(itemClickedCB)
end

function RedEnvelopeRecordMBView:OnListItemClicked(idx)
    
end

function RedEnvelopeRecordMBView:OnBGButtonClick()
    self:OnClose()
end

function RedEnvelopeRecordMBView:OnClose()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function RedEnvelopeRecordMBView:ShowDesc(data)
    local nameId = self.data[public_config.GUILD_RED_ENVELOPE_INFO_CFG_ID] or 1
    local name = LanguageDataHelper.GetContent(nameId)
    local form = self.data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_NAME]
    local getCount = self.data[public_config.GUILD_RED_ENVELOPE_INFO_GET_PACKET]
    local sum = self.data[public_config.GUILD_RED_ENVELOPE_INFO_PACKET]
    local moneyCount = self.data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_COUNT]
    local moneyType = self.data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_TYPE]
    local getInfo = self.data[public_config.GUILD_RED_ENVELOPE_INFO_SET]
    local icon = ItemDataHelper.GetIcon(moneyType)
    GameWorld.AddIcon(self._moneyIconContainer, icon, nil)
    GameWorld.AddIcon(self._moneyIcon, icon, nil)
    local vocation = self.data[public_config.GUILD_RED_ENVELOPE_INFO_VOCATION]
    local headIcon = RoleDataHelper.GetHeadIconByVocation(vocation)
    GameWorld.AddIcon(self._iconContainer, headIcon, nil)
    self._redEnvelopeNameText.text = name
    self._playerNameText.text = form
    self._getCountInfoText.text = string.format("已领取 %d/%d 个红包", getCount, sum) --待中文配置

    local selfCount = 0
    local getSumValue = 0
    for k,v in pairs(getInfo) do
        v.type = moneyType
        getSumValue = getSumValue + v[3]
        if v[2] == GameWorld.Player().name then
            selfCount = v[3]
        end
    end
    self._valueCountInfoText.text = string.format("总额%d/%d", moneyCount-getSumValue, moneyCount) --待中文配置
    self._countText.text = selfCount
    self._list:SetDataList(getInfo)
end