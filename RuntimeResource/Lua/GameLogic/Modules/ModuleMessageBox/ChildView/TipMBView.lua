--带两个按钮的一般显示信息消息框
local TipMBView = Class.TipMBView(ClassTypes.BaseLuaUIComponent)

TipMBView.interface = GameConfig.ComponentsConfig.Component
TipMBView.classPath = "Modules.ModuleMessageBox.ChildView.TipMBView"

local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function TipMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._confirmButtonCB = nil
    self._cancelButtonCB = nil
    self:InitView()
end
--override
function TipMBView:OnDestroy() 

end
--data结构 text confirmCB cancelCB
function TipMBView:ShowView(data)
    self._confirmButtonCB = data.confirmCB
    self._cancelButtonCB = data.cancelCB
    self:ShowConfirmText(data.confirmText or LanguageDataHelper.CreateContent(13))
    self:ShowDesc(data.text)
end

function TipMBView:InitView()
    self._confirmButton = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Button_Cancel")
    self._csBH:AddClick(self._cancelButton,function() self:OnCancelButtonClick() end)
    self.descText = self:GetChildComponent("Container_Desc/Text_Desc", "TextMeshWrapper")
    self.descText.text = ""
    
    self._confirmText = self:GetChildComponent("Button_Confirm/Text", "TextMeshWrapper")
end

function TipMBView:OnConfirmButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
    if self._confirmButtonCB ~= nil then
        self._confirmButtonCB()
    end
end

function TipMBView:OnCancelButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
    if self._cancelButtonCB ~= nil then
        self._cancelButtonCB()
    end
end

function TipMBView:ShowDesc(text)
    self.descText.text = text
end

function TipMBView:ShowConfirmText(text)
    self._confirmText.text = text
end 