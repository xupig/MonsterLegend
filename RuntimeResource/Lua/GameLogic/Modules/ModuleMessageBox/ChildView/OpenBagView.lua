--购买成功消息框
local OpenBagView = Class.OpenBagView(ClassTypes.BaseLuaUIComponent)

OpenBagView.interface = GameConfig.ComponentsConfig.Component
OpenBagView.classPath = "Modules.ModuleMessageBox.ChildView.OpenBagView"
local GUIManager = GameManager.GUIManager
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local BagManager = PlayerManager.BagManager
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function OpenBagView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end

function OpenBagView:InitView()
	self._btnClose = self:FindChildGO("Button_Close")
	self._csBH:AddClick(self._btnClose,function () self:OnCloseButtonClick() end)

	self._btnConfirm = self:FindChildGO("Button_Confirm")
	self._csBH:AddClick(self._btnConfirm,function () self:OnConfirmOpen() end)

	self._matIcon = self:AddChildLuaUIComponent("Container_ItemGridEx",ItemGridEx)
	self._matIcon:SetShowName(false)

	self._txtCount = self:GetChildComponent("Text_Add","TextMeshWrapper")
	self._txtTitle = self:GetChildComponent("Text_Title","TextMeshWrapper")

	self._bagCost = GlobalParamsHelper.GetParamValue(23)
	self._storageCost = GlobalParamsHelper.GetParamValue(26)
	local perItemCostMap = GlobalParamsHelper.GetParamValue(28)
	for k,v in pairs(perItemCostMap) do
		self._perItemCost = v
	end
end

function OpenBagView:ShowView(data)
	self._pkgType = data.pkgType
	self._openCount = data.openCount

	self._txtCount.text = tostring(self._openCount)

	if self._pkgType == public_config.PKG_TYPE_ITEM then
		self._txtTitle.text = LanguageDataHelper.CreateContent(58622)
		for itemId,v in pairs(self._bagCost) do
			self._costNum = v*self._openCount
			self._matIcon:UpdateData(itemId,self._costNum)
		end
	elseif self._pkgType == public_config.PKG_TYPE_WAREHOUSE then
		self._txtTitle.text = LanguageDataHelper.CreateContent(58623)
		for itemId,v in pairs(self._storageCost) do
			self._costNum = v*self._openCount
			self._matIcon:UpdateData(itemId,self._costNum)
		end
	end
end

function OpenBagView:OnCloseButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function OpenBagView:OnConfirmOpen()
	if self._openCount then
		local count,bagCount = self._matIcon:GetCount()
		if count <= bagCount then
			self:RequestOpen()
		else
			local dCount = count - bagCount
			local diamondNeed = self._perItemCost*dCount
			local str = LanguageDataHelper.CreateContentWithArgs(58600,{["0"]=diamondNeed })
			GUIManager.ShowMessageBox(2,str,function() self:RequestOpen() end,
				function() self:OnCloseButtonClick() end)
		end
	end
end

function OpenBagView:RequestOpen()
	if self._pkgType == public_config.PKG_TYPE_ITEM then
		BagManager:RequestBuyBagCount(self._openCount) 
	elseif self._pkgType == public_config.PKG_TYPE_WAREHOUSE then
		BagManager:RequestBuyWarehouseCount(self._openCount) 
	end
	GUIManager.ClosePanel(PanelsConfig.MessageBox)
end