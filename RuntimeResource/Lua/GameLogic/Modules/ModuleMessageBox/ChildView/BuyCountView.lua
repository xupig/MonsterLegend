--购买次数消息框
local BuyCountView = Class.BuyCountView(ClassTypes.BaseLuaUIComponent)
BuyCountView.interface = GameConfig.ComponentsConfig.Component
BuyCountView.classPath = "Modules.ModuleMessageBox.ChildView.BuyCountView"
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig

function BuyCountView:Awake()
	self._data = {}
	self._initcount = 1
	self._confirmcb = nil
	self._curcount = 1
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end

function BuyCountView:InitView()
	self._btnClose = self:FindChildGO("Button_Close")
	self._csBH:AddClick(self._btnClose,function () self:OnCloseButtonClick() end)

	self._btnConfirm = self:FindChildGO("Button_Confirm")
	self._csBH:AddClick(self._btnConfirm,function () self:OnConfirmOpen() end)

	self._btnAdd = self:FindChildGO("Button_Add")
	self._csBH:AddClick(self._btnAdd,function () self:OnAdd() end)

	self._btnDe = self:FindChildGO("Button_De")
	self._csBH:AddClick(self._btnDe,function () self:OnDe() end)

	self._btnCancel = self:FindChildGO("Button_Cancel")
	self._csBH:AddClick(self._btnCancel,function () self:OnCloseButtonClick() end)

	self._textBuyNum = self:GetChildComponent('Text_BuyNum',"TextMeshWrapper")
	self._textAddTitle = self:GetChildComponent('Text_AddTitle',"TextMeshWrapper")
	self._textMaNum = self:GetChildComponent("Text_MaNum","TextMeshWrapper")
	-- self._textBuyCanNum = self:GetChildComponent('Text_BuyCanNum',"TextMeshWrapper")
	self._textBuy0 = self:GetChildComponent('Text_Buy0',"TextMeshWrapper")
	self._textBuy1 = self:GetChildComponent('Text_Buy1',"TextMeshWrapper")
	self._textToPrice = self:GetChildComponent('Text_ToPrice',"TextMeshWrapper")

	self._textBuy1.text = LanguageDataHelper.GetContent(51143)
	self._textToPrice.text = LanguageDataHelper.GetContent(51144)
end

function BuyCountView:OnAdd()
	self._curcount = self._curcount+1
	if self._curcount > self._data['buyCanNum'] then
		self._curcount = self._data['buyCanNum']
	end
	self._textBuyNum.text = tostring(self._curcount)
	self._textMaNum.text = tostring(self._data['price']*self._curcount)	
end

function BuyCountView:OnDe()
	self._curcount = self._curcount-1
	if self._curcount <= 0 then
		self._curcount=1
	end	
	self._textBuyNum.text = tostring(self._curcount)
	self._textMaNum.text = tostring(self._data['price']*self._curcount)
end
-- 
function BuyCountView:ShowView(data)
	self._data = data
	self._textBuyNum.text = tostring(data['count'])
	self._curcount = data['count']
	self._textMaNum.text = tostring(data['price']*data['count'])
	self._confirmcb = data['confirmCB']
	local argsTable = LanguageDataHelper.GetArgsTable()
	if data['title']==0 then
		argsTable["0"] =  BaseUtil.GetColorString(data['title'],ColorConfig.H)
	else
		argsTable["0"] =  BaseUtil.GetColorString(data['title'],ColorConfig.J)
	end
	self._textAddTitle.text = LanguageDataHelper.CreateContent(51141, argsTable)

	local argsTable0 = LanguageDataHelper.GetArgsTable()
	argsTable0["0"] =  BaseUtil.GetColorString(data['buyCanNum'],ColorConfig.J)
	self._textBuy0.text = LanguageDataHelper.CreateContent(51142, argsTable0)
end

function BuyCountView:OnCloseButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function BuyCountView:OnConfirmOpen()
	if self._confirmcb ~=nil then
		self._confirmcb(self._curcount)
		self:OnCloseButtonClick()
	end
end