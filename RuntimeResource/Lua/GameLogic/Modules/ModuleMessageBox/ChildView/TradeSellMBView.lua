--确认上架小窗口
local TradeSellMBView = Class.TradeSellMBView(ClassTypes.BaseLuaUIComponent)

TradeSellMBView.interface = GameConfig.ComponentsConfig.Component
TradeSellMBView.classPath = "Modules.ModuleMessageBox.ChildView.TradeSellMBView"

local GUIManager = GameManager.GUIManager
local UISlider = ClassTypes.UISlider
local VIPDataHelper = GameDataHelper.VIPDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local TradeManager = PlayerManager.TradeManager
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local public_config = GameWorld.public_config

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil

function TradeSellMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end

--override
function TradeSellMBView:OnDestroy() 

end

function TradeSellMBView:InitView()
	--确认出售小窗口
	self._btnClosePopup = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClosePopup, function() self:OnCloseClick() end)

	local iconDiamond1 = self:FindChildGO("Container_Diamond1")
	local iconDiamond2 = self:FindChildGO("Container_Diamond2")
	GameWorld.AddIcon(iconDiamond1, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
	GameWorld.AddIcon(iconDiamond2, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))

   	self._btnPlus = self:FindChildGO("Container_Num/Button_Plus_BG")
    self._csBH:AddClick(self._btnPlus, function() self:OnPlusClick() end)

    self._btnReduce = self:FindChildGO("Container_Num/Button_Reduce_BG")
    self._csBH:AddClick(self._btnReduce, function() self:OnReduceClick() end)

    self._btnSell = self:FindChildGO("Button_Sell")
    self._csBH:AddClick(self._btnSell, function() self:OnSellClick() end)

    self._inputFieldMeshPw = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputFieldMesh_Password")
    self._inputFieldMeshPw:SetOnEndMeshEditCB(function(text) self:OnEndEditPw(text) end)
    self._inputTextPw = self:GetChildComponent("InputFieldMesh_Password", 'InputFieldMeshWrapper')

    self._inputFieldMeshPrice = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputFieldMesh_Price")
    self._inputFieldMeshPrice:SetOnEndMeshEditCB(function(text) self:OnEndEditPrice(text) end)
    self._inputTextPrice = self:GetChildComponent("InputFieldMesh_Price", 'InputFieldMeshWrapper')

    local parent = self:FindChildGO("Container_Item")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._itemIcon:ActivateTipsByItemData()

   	self._txtItemName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
   	self._txtTaxTips = self:GetChildComponent("Text_Tips",'TextMeshWrapper')
   	self._txtTotalPrice = self:GetChildComponent("Text_TotalPrice",'TextMeshWrapper')

   	self._numSlider = UISlider.AddSlider(self.gameObject, "Container_Num/Slider")
    self._numSlider:SetOnValueChangedCB(function() self:SellNumChange() end)
end

function TradeSellMBView:OnEndEditPrice(text)
	local singlePrice = self._inputTextPrice.text
  local num = self._numSlider:GetValue()
	if singlePrice ~= "" and tonumber(singlePrice)*num < 2 then
		self._inputTextPrice.text = 2
	end
	self:SetTotalPrice()
end

function TradeSellMBView:OnEndEditPw(text)

end

function TradeSellMBView:OnCloseClick()
	GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function TradeSellMBView:OnPlusClick()
	local num = self._numSlider:GetValue()
    if num == self._maxNum then return end
    self._numSlider:SetValue(num + 1)
end

function TradeSellMBView:OnReduceClick()
	local num = self._numSlider:GetValue()
    if num == 1 then return end
    self._numSlider:SetValue(num -1)
end

function TradeSellMBView:OnSellClick()
  	local singlePrice = self._inputTextPrice.text
  	if singlePrice ~= "" and tonumber(singlePrice)> 0 then
  		local pw = -1
  		if self._inputTextPw.text ~= "" and tonumber(self._inputTextPw.text)> 0 then
  			pw = self._inputTextPw.text
  		end
      local bagType = self._selectedItemData.bagType
  		local bagPos = self._selectedItemData.bagPos
  		local count = self._numSlider:GetValue()
      local totalPrice = singlePrice*count
  		TradeManager:RequestAuctionSell(bagType,bagPos,count,totalPrice,pw)
  		GUIManager.ClosePanel(PanelsConfig.MessageBox)
  	end
end

function TradeSellMBView:SellNumChange()
	 self:SetTotalPrice()
end

function TradeSellMBView:SetTotalPrice()
  	local count = self._numSlider:GetValue()
  	local singlePrice = self._inputTextPrice.text
  	if count and singlePrice ~= "" and tonumber(singlePrice)> 0 then
      	self._txtTotalPrice.text = count*tonumber(singlePrice)
    end
end

function TradeSellMBView:ShowView(itemData)
	self._selectedItemData = itemData
	self._maxNum = itemData:GetCount()
	self._numSlider:SetMaxValue(self._maxNum)
    self._numSlider:SetMinValue(0.99)
	self._numSlider:SetValue(self._maxNum)

    self._itemIcon:SetItemData(itemData)
    self._txtItemName.text = ItemDataHelper.GetItemNameWithColor(itemData.cfg_id)
    self._inputTextPrice.text = 2
    
    local vipLevel = GameWorld.Player().vip_level or 0
    local rate = tostring(VIPDataHelper:GetVipRevenue(vipLevel)/100).."%"
    self._txtTaxTips.text = LanguageDataHelper.CreateContentWithArgs(73507,{["0"] = vipLevel,["1"] = rate})

    self._inputTextPw.interactable = vipLevel>=4

    self:SetTotalPrice()
end