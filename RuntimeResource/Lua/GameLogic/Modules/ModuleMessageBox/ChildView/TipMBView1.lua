--带两个按钮的一般显示信息消息框
local TipMBView1 = Class.TipMBView1(ClassTypes.BaseLuaUIComponent)

TipMBView1.interface = GameConfig.ComponentsConfig.Component
TipMBView1.classPath = "Modules.ModuleMessageBox.ChildView.TipMBView1"

local GUIManager = GameManager.GUIManager

function TipMBView1:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._confirmButtonCB = nil
    self._cancelButtonCB = nil
    self._time = 0
    self._timer = -1
    self._timerfun = function() self:timer()end
    self:InitView()
end
--override
function TipMBView1:OnDestroy() 

end
--data结构 text confirmCB cancelCB
function TipMBView1:ShowView(data)
    self._confirmButtonCB = data.confirmCB
    self._cancelButtonCB = data.cancelCB
    self._time = data.time
    self:ShowDesc(data.text0,data.text1)
    if self._time>0 then
        local arg = LanguageDataHelper.GetArgsTable()
        arg["0"] = self._time
        self._timer=TimerHeap:AddSecTimer(0,1,self._time,self._timerfun)
        self._timetext.text = LanguageDataHelper.CreateContent(51147,arg)
    else
        self._timetext.text = LanguageDataHelper.GetContent(14)
    end
end
function TipMBView1:timer()
    local arg = LanguageDataHelper.GetArgsTable()
    self._time = self._time-1
    arg["0"] = self._time
    self._timetext.text = LanguageDataHelper.CreateContent(51147,arg)
    if self._time==0 then
        TimerHeap:DelTimer(self._timer)
        self:OnCancelButtonClick()
    end
end
function TipMBView1:InitView()
    self._confirmButton = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(self._confirmButton,function() self:OnConfirmButtonClick() end)
    self._cancelButton = self:FindChildGO("Button_Cancel")
    self._csBH:AddClick(self._cancelButton,function() self:OnCancelButtonClick() end)
    self.descText0 = self:GetChildComponent("Container_Desc0/Text_Desc0", "TextMeshWrapper")
    self.descText1 = self:GetChildComponent("Container_Desc1/Text_Desc1", "TextMeshWrapper")
    self._timetext = self:GetChildComponent("Button_Cancel/Text","TextMeshWrapper")
    
    self.descText0.text = ""
    self.descText1.text = ""
end

function TipMBView1:OnConfirmButtonClick()
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
    if self._confirmButtonCB ~= nil then
        self._confirmButtonCB()
    end
end

function TipMBView1:OnCancelButtonClick()
    TimerHeap:DelTimer(self._timer)    
    GUIManager.ClosePanel(PanelsConfig.MessageBox)
    if self._cancelButtonCB ~= nil then
        self._cancelButtonCB()
    end
end

function TipMBView1:ShowDesc(text0,text1)
    self.descText0.text = text0
    self.descText1.text = text1
end