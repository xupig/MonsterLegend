require "UIComponent.Extend.ItemGrid"

--多选一使用物品弹窗
local SelectUseMBView = Class.SelectUseMBView(ClassTypes.BaseLuaUIComponent)

SelectUseMBView.interface = GameConfig.ComponentsConfig.Component

SelectUseMBView.classPath = "Modules.ModuleMessageBox.ChildView.SelectUseMBView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UISlider = ClassTypes.UISlider
local UIComponentUtil = GameUtil.UIComponentUtil
local BagManager = PlayerManager.BagManager
local ItemConfig = GameConfig.ItemConfig
local FontStyleHelper = GameDataHelper.FontStyleHelper

require"Modules.ModuleMessageBox.ChildComponent.MBItemCom"
local MBItemCom = ClassTypes.MBItemCom

function SelectUseMBView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function SelectUseMBView:OnDestroy() 

end

--data结构 shopId count
function SelectUseMBView:ShowView(itemData)
  self._selectedItemData = itemData
	self._maxNum = itemData:GetCount()
	self._numSlider:SetMaxValue(self._maxNum)
  self._numSlider:SetMinValue(0.99)
  if itemData.cfgData.no_batch_use > 0  then
      self._containerNum:SetActive(false)
      self._txtBatchTitleGo:SetActive(false)
      self._containerSelectTrans.anchoredPosition = Vector2.New(0,27)
      self._numSlider:SetValue(1)
  else
      self._containerNum:SetActive(true)
      self._txtBatchTitleGo:SetActive(true)
      self._containerSelectTrans.anchoredPosition = Vector2.New(0,56)
      self._numSlider:SetValue(self._maxNum)
  end
    
  local h = 1
  for i=2,#itemData.cfgData.effectId,2 do
      local itemId = itemData.cfgData.effectId[i]
      local itemCount = itemData.cfgData.effectId[i+1]
      local item = self._items[i/2]
      item:SetActive(true)
      item:OnRefreshData(itemId,itemCount)
      h = h+1
  end
  for i=h,5 do
      self._items[i]:SetActive(false)
  end
  self:SelectItem(1)
  self._txtItemName.text = itemData:GetItemName()
end

function SelectUseMBView:InitView()
	
	  self._btnClosePopup = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClosePopup, function() self:OnCloseClick() end)

    self._containerNum = self:FindChildGO("Container_Num")
   	self._btnPlus = self:FindChildGO("Container_Num/Button_Plus_BG")
    self._csBH:AddClick(self._btnPlus, function() self:OnPlusClick() end)

    self._btnReduce = self:FindChildGO("Container_Num/Button_Reduce_BG")
    self._csBH:AddClick(self._btnReduce, function() self:OnReduceClick() end)

    self._btnUse = self:FindChildGO("Button_Use")
    self._csBH:AddClick(self._btnUse, function() self:OnUseClick() end)

    self._containerSelectTrans = self:FindChildGO("Container_Select").transform
    self._items = {}
    for i=1,5 do
      local item = self:AddChildLuaUIComponent("Container_Select/Container_Items/Container_Item"..i,MBItemCom)
      item:SetSelected(false)
      item:SetPointerDownCB(function ()
          self:SelectItem(i)
      end)
      self._items[i] = item
    end

   	self._txtItemName = self:GetChildComponent("Text_Title",'TextMeshWrapper')

    self._txtBatchTitleGo = self:FindChildGO("Text_BatchTitle")
   	self._numSlider = UISlider.AddSlider(self.gameObject, "Container_Num/Slider")
    self._numSlider:SetOnValueChangedCB(function() self:SellNumChange() end)
end

function SelectUseMBView:SelectItem(index)
    if self._selectedItem then
      self._selectedItem:SetSelected(false)
    end
    self._selectedIndex = index
    self._selectedItem = self._items[index]
    self._selectedItem:SetSelected(true)
end

function SelectUseMBView:OnCloseClick()
	GUIManager.ClosePanel(PanelsConfig.MessageBox)
end

function SelectUseMBView:SellNumChange()
    
end

function SelectUseMBView:OnPlusClick()
	local num = self._numSlider:GetValue()
  if num == self._maxNum then return end
  self._numSlider:SetValue(num + 1)
end

function SelectUseMBView:OnReduceClick()
	local num = self._numSlider:GetValue()
  if num == 1 then return end
  self._numSlider:SetValue(num -1)
end

function SelectUseMBView:OnUseClick()
    BagManager:RequestUse(self._selectedItemData.bagPos,self._numSlider:GetValue(),self._selectedIndex)
    GUIManager.ClosePanel(PanelsConfig.MessageBox)   
end