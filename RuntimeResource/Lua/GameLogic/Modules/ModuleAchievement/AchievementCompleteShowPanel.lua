-- AchievementCompleteShowPanel.lua
require "Modules.ModuleAchievement.ChildComponent.AchievementShowItem"

local AchievementCompleteShowPanel = Class.AchievementCompleteShowPanel(ClassTypes.BasePanel)

local AchievementShowItem = ClassTypes.AchievementShowItem
local AchievementDataHelper = GameDataHelper.AchievementDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local closeTime = 3

function AchievementCompleteShowPanel:Awake()
	self:InitView()
end

function AchievementCompleteShowPanel:OnDestroy()
	self._csBH = nil
end

function AchievementCompleteShowPanel:OnShow(achievementId)
    self:AddEventListeners()
    self._showList = {}
    self._showList[achievementId] = achievementId
    self:SetData(achievementId)
end

function AchievementCompleteShowPanel:SetData(achievementId)
    self:ShowItem(achievementId)
    GameWorld.PlaySound(14)
end

function AchievementCompleteShowPanel:OnClose()
    for i,v in ipairs(self._itemContainers) do
        v:ClearTimer()
    end

    for k,v in pairs(self._useViews) do
        v:ClearTimer()
        table.insert(self._itemContainers, v)
    end

    self._useNum = 0
    self._useViews = {}
    self._moveAchievementId = nil
    self._showAchievementId = nil
	self:RemoveEventListeners()
end

function AchievementCompleteShowPanel:AddEventListeners()  
    EventDispatcher:AddEventListener(GameEvents.ON_SHOWACHIEVEMENT_ITEM_CLICK, self._onItemClick)
    EventDispatcher:AddEventListener(GameEvents.ON_CLOSE_ACHIEVEMENT_ITEM, self._closeItem)
end

function AchievementCompleteShowPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_SHOWACHIEVEMENT_ITEM_CLICK, self._onItemClick)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLOSE_ACHIEVEMENT_ITEM, self._closeItem)
end

function AchievementCompleteShowPanel:InitView()
    self._onItemClick = function(achievementId) self:OnItemClick(achievementId) end
    self._closeItem = function(achievementId) self:CloseItem(achievementId) end

    self._useViews = {}
    self._itemContainers = {}
    self._itemNum = 0
    self._useNum = 0
end

function AchievementCompleteShowPanel:SetValue(achievementId)
    self._titleText.text = AchievementDataHelper.GetTitle(achievementId)
end

function AchievementCompleteShowPanel:OnItemClick(achievementId)
    GUIManager.ClosePanel(PanelsConfig.AchievementCompleteShow)

    if GUIManager.reopenSubMainMenu > 0 then
        GUIManager.reopenSubMainMenu = GUIManager.reopenSubMainMenu + 1
    end
    GUIManager.ShowPanel(PanelsConfig.Achievement, achievementId)
end

function AchievementCompleteShowPanel:CloseItem(achievementId)
    if self._moveAchievementId == achievementId then
        self._moveAchievementId = nil
    end
    
    self._useNum = self._useNum - 1
    if self._useNum == 0 then
        GUIManager.ClosePanel(PanelsConfig.AchievementCompleteShow)
    end
    table.insert(self._itemContainers, self._useViews[achievementId])
    self._useViews[achievementId] = nil
end

function AchievementCompleteShowPanel:CopyContainer()
    self._itemNum = self._itemNum + 1
    return self:CopyUIGameObject("Container_AchievementCompleteShow/Container_Demo", "Container_AchievementCompleteShow/Container_Appear")
end

function AchievementCompleteShowPanel:ShowItem(achievementId)
    local view
    if table.isEmpty(self._itemContainers) then
        --需要创建新的container
        local item = self:CopyContainer()
        view = UIComponentUtil.AddLuaUIComponent(item, AchievementShowItem)
        -- self._useViews[achievementId] = view
    else
        view = self._itemContainers[#self._itemContainers]
        self._itemContainers[#self._itemContainers] = nil
    end

    if self._showAchievementId ~= nil then        
        self._useViews[self._showAchievementId]:Move()
        if self._moveAchievementId ~= nil then
            self._useViews[self._moveAchievementId]:Hide()
            -- self._moveView:Hide()
        end
        self._moveAchievementId = self._showAchievementId
        -- self._moveView = self._useViews[self._showAchievementId]        
    end

    view:SetAchievementId(achievementId)
    view:ShowView()
    self._showAchievementId = achievementId
    self._useNum = self._useNum + 1
    self._useViews[achievementId] = view
end