-- AchievementPanel.lua
require "Modules.ModuleAchievement.ChildView.AchievementView"

local AchievementPanel = Class.AchievementPanel(ClassTypes.BasePanel)

local AchievementView = ClassTypes.AchievementView
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local AchievementDataHelper = GameDataHelper.AchievementDataHelper

function AchievementPanel:Awake()
	self:InitView()
end

function AchievementPanel:OnDestroy()
	self._csBH = nil
end

function AchievementPanel:OnShow(achievementId)
	self._achievementView:SetAchievementId(achievementId)
	self._achievementView:SetActive(true)
	self:AddEventListeners()
end

function AchievementPanel:OnClose()
	self._achievementView:SetAchievementId(nil)
	self._achievementView:SetActive(false)
	self:RemoveEventListeners()
end

function AchievementPanel:AddEventListeners()
end

function AchievementPanel:RemoveEventListeners()
end

function AchievementPanel:InitView()
    self._achievementView = self:AddChildLuaUIComponent("Container_Achievement", AchievementView)
end

function AchievementPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--关闭的时候重新开启二级主菜单
function AchievementPanel:ReopenSubMainPanel()
    return true
end