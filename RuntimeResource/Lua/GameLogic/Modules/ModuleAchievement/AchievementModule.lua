--AchievementModule.lua
AchievementModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function AchievementModule.Init()
    GUIManager.AddPanel(PanelsConfig.Achievement, "Panel_Achievement", GUILayer.LayerUIPanel, "Modules.ModuleAchievement.AchievementPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.AchievementCompleteShow, "Panel_AchievementCompleteShow", GUILayer.LayerUIFloat, "Modules.ModuleAchievement.AchievementCompleteShowPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
 end

return AchievementModule