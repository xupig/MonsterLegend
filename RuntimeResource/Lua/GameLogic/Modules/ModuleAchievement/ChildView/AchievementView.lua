require "Modules.ModuleAchievement.ChildComponent.AchievementFirstItem"
require "Modules.ModuleAchievement.ChildComponent.AchievementSecondItem"
require "Modules.ModuleAchievement.ChildComponent.AchievementPointsItem"
require "Modules.ModuleAchievement.ChildComponent.AchievementItem"

local AchievementView = Class.AchievementView(ClassTypes.BaseLuaUIComponent)

AchievementView.interface = GameConfig.ComponentsConfig.Component
AchievementView.classPath = "Modules.ModuleAchievement.ChildView.AchievementView"

local UINavigationMenu = ClassTypes.UINavigationMenu
local AchievementFirstItem = ClassTypes.AchievementFirstItem
local AchievementSecondItem = ClassTypes.AchievementSecondItem
local AchievementPointsItem = ClassTypes.AchievementPointsItem
local AchievementItem = ClassTypes.AchievementItem
local AchievementDataHelper = GameDataHelper.AchievementDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local AchievementManager = PlayerManager.AchievementManager
local UIList = ClassTypes.UIList
local AchievementConfig = GameConfig.AchievementConfig

function AchievementView:Awake()
    self:InitView()
end
--override
function AchievementView:OnDestroy() 
    self:RemoveEventListeners()
end

function AchievementView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ON_GET_ACHIEVEMENT_REWARD_COMPLETE, self._getRewardComplete)
    EventDispatcher:AddEventListener(GameEvents.ON_GET_ACHIEVEMENT_List_DATA, self._getAchievementListHandle)
    EventDispatcher:AddEventListener(GameEvents.ACHIEVEMENT_REFRESH, self._refreshAchievement)
    -- EventDispatcher:AddEventListener(GameEvents.ON_GET_ACHIEVEMENT_REWARD_CLICK, self._onGetAchievementRewardClick)
end

function AchievementView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_GET_ACHIEVEMENT_REWARD_COMPLETE, self._getRewardComplete)
    EventDispatcher:RemoveEventListener(GameEvents.ON_GET_ACHIEVEMENT_List_DATA, self._getAchievementListHandle)
    EventDispatcher:RemoveEventListener(GameEvents.ACHIEVEMENT_REFRESH, self._refreshAchievement)
    -- EventDispatcher:RemoveEventListener(GameEvents.ON_GET_ACHIEVEMENT_REWARD_CLICK, self._onGetAchievementRewardClick)    
end

function AchievementView:InitView()
    self._getRewardComplete = function(achievementId) self:GetRewardComplete(achievementId) end
    self._getAchievementListHandle = function() self:GetAchievementListHandle() end
    self._refreshAchievement = function() self:RefreshAchievement() end
    -- self._onGetAchievementRewardClick = function(id) self:OnGetAchievementRewardClick(id) end

    self:InitOtherAchievement()
    self:InitTotalList()
    self:InitOtherAchievementList()

    self._selectRecord = {}

    self._totalContainer = self:FindChildGO("Container_Content_Total")
    self._totalPointsContainer = self:FindChildGO("Container_Content_Total/Container_Text/Container_Total")
    self._achievementContentContainer = self:FindChildGO("Container_Content_Achievement")    
    self._redImage = self:FindChildGO("Container_List/Button_Total/Image_Red")

    self._totalPoints = UIComponentUtil.AddLuaUIComponent(self._totalPointsContainer, AchievementPointsItem)
    
    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "Container_List/NavigationMenu")
    self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(AchievementFirstItem)
	self._menu:SetSecondLevelItemType(AchievementSecondItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(5, 0)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)

    self._totalButton = self:FindChildGO("Container_List/Button_Total")
    self._csBH:AddClick(self._totalButton, function() self:OnTotalButtonClick() end)
end

function AchievementView:InitOtherAchievementList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content_Achievement/Container_List/ScrollViewList")
	self._achievementList = list
	self._achievementlistlView = scrollView 
    self._achievementlistlView:SetHorizontalMove(false)
    self._achievementlistlView:SetVerticalMove(true)
    self._achievementList:SetItemType(AchievementItem)
    self._achievementList:SetPadding(0, 0, 0, 0)
    self._achievementList:SetGap(10, 0)
    self._achievementList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end

function AchievementView:InitTotalList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content_Total/Container_List/ScrollViewList")
	self._list = list
	self._listlView = scrollView 
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(true)
    self._list:SetItemType(AchievementItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)

    -- self:InitAchievementTotalListData()
    -- self._listData = AchievementManager:GetAchievementListByType(AchievementConfig.TOTAL_ACHIEVEMENT, AchievementConfig.NONE_ACHIEVEMENT_GROUP)
    -- self._list:SetDataList(self._listData)
end

function AchievementView:InitOtherAchievement()
    self._bigTypes = AchievementDataHelper.GetBigTypes() 
    self._otherPoints = {}
    self._copyContainer = self:FindChildGO("Container_Content_Total/Container_Text/Container_Other/Container")
    for i,v in ipairs(self._bigTypes) do
        local container = self:CopyUIGameObject("Container_Content_Total/Container_Text/Container_Other/Container", "Container_Content_Total/Container_Text/Container_Other")
        self._otherPoints[i] = UIComponentUtil.AddLuaUIComponent(container, AchievementPointsItem)
        self._otherPoints[i]:SetName(v.typeName)
        self._otherPoints[i]:SetType(v.type)
    end
    self._copyContainer:SetActive(false)
end

function AchievementView:SetAchievementId(achievementId)
    self._achievementId = achievementId
end

function AchievementView:OnEnable()
    self:AddEventListeners()
    AchievementManager:GetAchievementListReq()
end

function AchievementView:GetAchievementListHandle()
    self:InitShowView()
    self:SetAchievementPoints()
    self:RefreshView()
    self:InitAchievementTotalListData()
    self:RefreshTotalRedDot()
end

function AchievementView:OnDisable()
    self._fIndex = nil
    self._sIndex = nil
    self._otherId = nil
    self._isTotalContainer = true
    self._achievementId = nil
    self._selectItem = nil
    self:RemoveEventListeners()
end

function AchievementView:InitShowView()
    self:SetAchievementContainerActive(true)   
end

function AchievementView:SetAchievementPoints()
    -- LoggerHelper.Error("GameWorld.Player().achievement_point ====" .. tostring(GameWorld.Player().achievement_point))
    self._totalPoints:SetProgress(GameWorld.Player().achievement_point, AchievementManager:GetMaxTotalPoints())
    for i,v in ipairs(self._otherPoints) do
        local point = AchievementManager:GetAlreadyGetAchievementPointByBigType(self._otherPoints[i]:GetType())
        self._otherPoints[i]:SetProgress(point, AchievementManager:GetAchievementPointByBigType(self._bigTypes[i].type))
    end
end

function AchievementView:OnMenuFItemClicked(idx)
    if self._fIndex and self._fIndex ~= idx then
        self._selectedFItem = self._menu:GetFItem(self._fIndex)
        self._selectedFItem:SetSelectImage(false)
    end

    self._fIndex = idx
    self._selectedFItem = self._menu:GetFItem(idx)
    self._selectedFItem:ToggleSetSelect()
end

function AchievementView:OnMenuSItemClicked(fIndex, sIndex)
    -- LoggerHelper.Error("AchievementView:OnMenuSItemClicked(fIndex, sIndex) ====" .. tostring(fIndex) .. "sIndex == " .. tostring(sIndex))
    -- LoggerHelper.Error("self._menuData ====== " .. PrintTable:TableToStr(self._menuData))
    self._fIndex = fIndex
    self._sIndex = sIndex
    self._otherId = self._menuData[fIndex+1][sIndex+2].id
    self:SetAchievementContainerActive(false)
    
    self:InitOtherAchievementListData(self._otherId)

    -- self._achievementListData = AchievementManager:GetAchievementListByType(typeId, group)
    -- self._achievementList:SetDataList(self._achievementListData)
    if self._selectItem ~= nil then 
        self._selectItem:SetSelect(false)    
    end
    self._selectItem = self._menu:GetSItem(fIndex, sIndex)
    self._selectItem:SetSelect(true)
    self._achievementList:SetPositionByNum(1)
end

function AchievementView:RefreshView()
    self._menuData = AchievementDataHelper.GetTitleList()
    LoggerHelper.Log(self._menuData)
    self._menu:SetDataList(self._menuData)

    --LoggerHelper.Error("self._achievementId ==" .. tostring(self._achievementId))
    if self._achievementId ~= nil then
        self:SelectByAchievementId(self._achievementId)
    else
        local achievementId, type = AchievementManager:GetCanGetRewardAchievementId(self._menuData)
        if achievementId ~= 0 and type ~= 1 then
            self._achievementId = achievementId
            self:SelectByAchievementId(self._achievementId)
        end
    end
    -- self._menu:SelectFItem(0)
    -- self:OnMenuSItemClicked(0, 0)
end

function AchievementView:SelectByAchievementId(achievementId)
    local typeId = AchievementDataHelper.GetAchievementTypeId(achievementId)
    local type, group = AchievementDataHelper.GetTypeAndGroupByAchievementTypeId(typeId)

    if type ~= AchievementConfig.TOTAL_ACHIEVEMENT then
        local fId = 0
        local sId = 0
        for i,v in ipairs(self._menuData) do
            local vType, vGroup = AchievementDataHelper.GetTypeAndGroupByAchievementTypeId(v[1].id)
            if vType == type then
                fId = i
                for n, data in ipairs(v) do
                    if n ~= 1 then
                        if data.id == typeId and fId ~= 0 then
                            sId = n
                            break
                        end
                    end
                end
                break
            end                        
        end
        if fId ~= 0 and sId ~= 0 then
            if self._fIndex == nil or self._fIndex ~= fId - 1 then
                self._menu:SelectFItem(fId - 1)
            end
            self:OnMenuSItemClicked(fId - 1, sId - 2)
            self._menu:SetPositionByNum(fId)
        end
    end
end

function AchievementView:OnTotalButtonClick()    
    self:SetAchievementContainerActive(true)
end

--设置成就列表container
function AchievementView:SetAchievementContainerActive(flag)
    self._isTotalContainer = flag
    self._totalContainer:SetActive(flag)
    self._achievementContentContainer:SetActive(not flag)
    self._list:SetPositionByNum(1)    
end

--刷新成就完成数据
function AchievementView:RefreshAchievement()
    if self._isTotalContainer then
        self:InitAchievementTotalListData()
    else
        self:InitOtherAchievementListData(self._otherId)
    end
end

function AchievementView:GetRewardComplete(achievementId)
    local typeId = AchievementDataHelper.GetAchievementTypeId(achievementId)
    local type, group = AchievementDataHelper.GetTypeAndGroupByAchievementTypeId(typeId)
    if type == AchievementConfig.TOTAL_ACHIEVEMENT then
        --获取成就总览里的成就奖励
        self:RefreshTotalRedDot()
    else
        --获取其他的成就奖励
        local typeId = AchievementDataHelper.GetAchievementTypeId(achievementId)
        local type, group = AchievementDataHelper.GetTypeAndGroupByAchievementTypeId(typeId)
        -- LoggerHelper.Error("achievementId ==" .. tostring(achievementId))
        -- LoggerHelper.Error("self._menuData ==" .. PrintTable:TableToStr(self._menuData))
        local fId = 0
        for i,v in ipairs(self._menuData) do
            local vType, vGroup = AchievementDataHelper.GetTypeAndGroupByAchievementTypeId(v[1].id)
            if vType == type then
                fId = i
                self._menu:GetFItem(i -1):OnRefreshData(v[1])

                for n, data in ipairs(v) do
                    if n ~= 1 then
                        if data.id == typeId and fId ~= 0 then
                            self._menu:GetSItem(fId - 1, n-2):OnRefreshData(data)
                            break
                        end
                    end
                end
                break
            end                        
        end
    end

    local achievementId, achievementType = AchievementManager:GetCanGetRewardAchievementId(self._menuData)

    if achievementId ~= 0 and achievementId ~= nil then
        if achievementType == 1 then
            self:OnTotalButtonClick()--成就总览的奖励
        else
            self._achievementId = achievementId
            self:SelectByAchievementId(self._achievementId)
        end
    end
end

--点击获取成就奖励按钮事件
function AchievementView:OnGetAchievementRewardClick(id)
    -- LoggerHelper.Error("AchievementView:OnGetAchievementRewardClick(id) ====" .. tostring(id))
    AchievementManager:GetAchievementReward(id)
end

function AchievementView:InitAchievementTotalListData()
    self._listData = AchievementManager:GetAchievementListByType(AchievementConfig.TOTAL_ACHIEVEMENT, AchievementConfig.NONE_ACHIEVEMENT_GROUP)
    local getList = {}              --已经领奖的列表
    local completeList = {}         --已经完成但未领奖列表
    local list = {}                 --未完成列表
    for i, id in ipairs(self._listData) do
        if AchievementManager:GetAchievementStateById(id) == AchievementConfig.ACHIEVEMENT_COMPLETE then
            table.insert(completeList, id)
        elseif AchievementManager:GetAchievementStateById(id) == AchievementConfig.ACHIEVEMENT_GET_REWARD then
            table.insert(getList, id)
        else
            table.insert(list, id)
        end
    end

    self._listData = {}
    for i,v in ipairs(completeList) do
        table.insert(self._listData, v)
    end

    for i,v in ipairs(list) do
        table.insert(self._listData, v)
    end

    for i,v in ipairs(getList) do
        table.insert(self._listData, v)
    end

    self._list:SetDataList(self._listData)
end

function AchievementView:InitOtherAchievementListData(id)
    local typeId, group = AchievementDataHelper.GetTypeAndGroupByAchievementTypeId(id)
    self._achievementListData = AchievementManager:GetAchievementListByType(typeId, group)
    -- LoggerHelper.Error("其他的列表信息 ====" .. PrintTable:TableToStr(self._achievementListData))
    local getList = {}              --已经领奖的列表
    local completeList = {}         --已经完成但未领奖列表
    local list = {}                 --未完成列表
    for i, id in ipairs(self._achievementListData) do
        if AchievementManager:GetAchievementStateById(id) == AchievementConfig.ACHIEVEMENT_COMPLETE then
            table.insert(completeList, id)
        elseif AchievementManager:GetAchievementStateById(id) == AchievementConfig.ACHIEVEMENT_GET_REWARD then
            table.insert(getList, id)
        else
            table.insert(list, id)
        end
    end

    self._achievementListData = {}
    for i,v in ipairs(completeList) do
        table.insert(self._achievementListData, v)
    end

    for i,v in ipairs(list) do
        table.insert(self._achievementListData, v)
    end

    for i,v in ipairs(getList) do
        table.insert(self._achievementListData, v)
    end

    self._achievementList:SetDataList(self._achievementListData)
end

function AchievementView:RefreshTotalRedDot()
    local achievementIds = AchievementDataHelper.GetAchievementIdsByType(AchievementConfig.TOTAL_ACHIEVEMENT)
    local flag = false
    for _, achievementId in ipairs(achievementIds) do
        if AchievementManager:GetAchievementStateById(achievementId) == AchievementConfig.ACHIEVEMENT_COMPLETE then
            flag = true       
            break
        end
    end
    self._redImage:SetActive(flag)
end

function AchievementView:CheckTotalRedPoint()
    for i,v in ipairs(self._listData) do
        if AchievementManager:GetAchievementStateById(v) == AchievementConfig.ACHIEVEMENT_COMPLETE then
            return true
        end
    end
    return false
end

--检查一级菜单是否存在红点，顺序检查，返回第一个有红点的一级菜单，没有则返回0
function AchievementView:CheckFirstMenuRedpoint()
    for i,value in ipairs(self._menuData) do
        local id = value[1].id
        if AchievementManager:IsCanGetByAchievementTypeId(id) then
            return i
        end
    end
    return 0
end

function AchievementView:GetAchievementIdByFirstMenuIndex(index)
    for i,value in ipairs(self._menuData[index]) do
        if i ~= 1 then
            
        end
    end
end