-- AchievementSecondItem.lua
--二级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local AchievementSecondItem = Class.AchievementSecondItem(UINavigationMenuItem)

AchievementSecondItem.interface = GameConfig.ComponentsConfig.PointableComponent
AchievementSecondItem.classPath = "Modules.ModuleAchievement.ChildComponent.AchievementSecondItem"

local AchievementDataHelper = GameDataHelper.AchievementDataHelper
local AchievementManager = PlayerManager.AchievementManager
local AchievementConfig = GameConfig.AchievementConfig

function AchievementSecondItem:Awake()
    UINavigationMenuItem.Awake(self)
    self._isExpanded = false
    self:IninView()
end

function AchievementSecondItem:OnDestroy()

end

function AchievementSecondItem:IninView()
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper')
    self._selectImage = self:FindChildGO("Image_Select")
    self._redImage = self:FindChildGO("Image_Red")
end

function AchievementSecondItem:OnPointerDown(pointerEventData)

end

--override {type}
function AchievementSecondItem:OnRefreshData(data)
    -- LoggerHelper.Error("AchievementSecondItem:OnRefreshData(data) ==" .. PrintTable:TableToStr(data))
    local id = data.id--成就类型主键id
    local achievementIds = AchievementDataHelper.GetAchievementIdsByType(id)
    local totalNum = #achievementIds
    local flag = false
    local completeNum = 0
    for i,v in ipairs(achievementIds) do
        local state = AchievementManager:GetAchievementStateById(v)
        if state == AchievementConfig.ACHIEVEMENT_COMPLETE then
            completeNum = completeNum + 1
            flag = true            
        end

        if state == AchievementConfig.ACHIEVEMENT_GET_REWARD then
            completeNum = completeNum + 1
        end
    end
    self._redImage:SetActive(flag)
    self._nameText.text = AchievementDataHelper.GetGroupName(id) .. "(" .. completeNum .."/" .. totalNum .. ")"
end

function AchievementSecondItem:SetSelect(flag)
    self._selectImage:SetActive(flag)
end