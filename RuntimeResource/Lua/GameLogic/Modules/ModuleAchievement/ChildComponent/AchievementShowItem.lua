-- AchievementShowItem.lua
local AchievementShowItem = Class.AchievementShowItem(ClassTypes.BaseComponent)

AchievementShowItem.interface = GameConfig.ComponentsConfig.Component
AchievementShowItem.classPath = "Modules.ModuleAchievement.ChildComponent.AchievementShowItem"

local closeTime = 3
local AchievementDataHelper = GameDataHelper.AchievementDataHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

--override
function AchievementShowItem:Awake()
    self._titleText = self:GetChildComponent("Button_Achievement/Text_Title",'TextMeshWrapper')
    self._achievementButton = self:FindChildGO("Button_Achievement")
    self._csBH:AddClick(self._achievementButton, function() self:OnAchievementButtonClick() end)

    self._initLocalPosition = self.transform.localPosition
    self._containerPos = self.gameObject:AddComponent(typeof(XGameObjectTweenPosition))
end

function AchievementShowItem:OnDestroy()
    self:ClearTimer()
    if  self.gameObject then 
        self.gameObject:SetActive(false)
    end
end

function AchievementShowItem:SetAchievementId(achievementId)
    self._achievementId = achievementId
    self._titleText.text = AchievementDataHelper.GetTitle(achievementId)
    -- self._titleText.text = achievementId--AchievementDataHelper.GetTitle(achievementId)
end

function AchievementShowItem:OnAchievementButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_SHOWACHIEVEMENT_ITEM_CLICK, self._achievementId)
end

function AchievementShowItem:ShowView()
    self._isMove = false
    self.gameObject:SetActive(true)
    self:AddTimer()
    self.transform.localPosition = Vector3.New(0,0,0)
end

function AchievementShowItem:Move()
    self._isMove = true
    self._containerPos:SetFromToPos(Vector3.New(0,0,0), Vector3(0, 160, 0), 0.5, 1, nil)
end

function AchievementShowItem:IsMove()
    return self._isMove
end

function AchievementShowItem:Hide()
    self:ClearTimer()
    self.gameObject:SetActive(false)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_ACHIEVEMENT_ITEM, self._achievementId)
end

function AchievementShowItem:AddTimer()
    self:ClearTimer()
    self._timer = TimerHeap:AddSecTimer(closeTime, 0, 0, function()
        self:ClearTimer()
        self.gameObject:SetActive(false)
        EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_ACHIEVEMENT_ITEM, self._achievementId)
    end)
end

function AchievementShowItem:ClearTimer()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    if self.transform then
        self._containerPos.IsTweening = false
        self.transform.localPosition = self._initLocalPosition
    end
end