-- AchievementRewardItem.lua
local AchievementRewardItem = Class.AchievementRewardItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
AchievementRewardItem.interface = GameConfig.ComponentsConfig.Component
AchievementRewardItem.classPath = "Modules.ModuleAchievement.ChildComponent.AchievementRewardItem"

local UIList = ClassTypes.UIList
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function AchievementRewardItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function AchievementRewardItem:OnDestroy() 

end

--override
function AchievementRewardItem:OnRefreshData(data)
    local itemId = data[1]
    local num = data[2]
    
    self._itemIcon:ActivateTipsById()
    self._itemIcon:SetItem(itemId, num)
end