-- AchievementFirstItem.lua
--一级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local AchievementFirstItem = Class.AchievementFirstItem(UINavigationMenuItem)

AchievementFirstItem.interface = GameConfig.ComponentsConfig.PointableComponent
AchievementFirstItem.classPath = "Modules.ModuleAchievement.ChildComponent.AchievementFirstItem"

local AchievementDataHelper = GameDataHelper.AchievementDataHelper
local AchievementConfig = GameConfig.AchievementConfig
local AchievementManager = PlayerManager.AchievementManager

function AchievementFirstItem:Awake()
    UINavigationMenuItem.Awake(self)
    self._isExpanded = false
    self._isGetSelect = false
    self:IninView()
end

function AchievementFirstItem:OnDestroy()

end

function AchievementFirstItem:IninView()
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper')
    self._selectImage = self:FindChildGO("Image_Select")
    self._redImage = self:FindChildGO("Image_Red")
    self._jianSelectImage = self:FindChildGO("Image_Jian_Select")
    self._jianUnSelectImage = self:FindChildGO("Image_Jian_UnSelect")
    self._jianSelectImage:SetActive(true)
    self._jianUnSelectImage:SetActive(false)
end

function AchievementFirstItem:OnPointerDown(pointerEventData)

end

--override {type}
function AchievementFirstItem:OnRefreshData(data)
    -- LoggerHelper.Error("AchievementFirstItem:OnRefreshData(data) ==" .. PrintTable:TableToStr(data))
    local id = data.id--成就类型主键id
    local flag = AchievementManager:IsCanGetByAchievementTypeId(id)
    
    self._redImage:SetActive(flag)
    self._nameText.text = AchievementDataHelper.GetTypeName(id)

    self:SetSelectImage(false)
end

function AchievementFirstItem:SetSelect(flag)
    self._isExpanded = flag
    self._selectImage:SetActive(flag)
end

function AchievementFirstItem:SetSelectImage(flag)
    self._jianSelectImage:SetActive(not flag)
    self._jianUnSelectImage:SetActive(flag)
    self._isGetSelect = flag
end

function AchievementFirstItem:ToggleSetSelect()
    if self._isGetSelect then
        self._jianSelectImage:SetActive(true)
        self._jianUnSelectImage:SetActive(false)
        self._isGetSelect = false
    else
        self._jianSelectImage:SetActive(false)
        self._jianUnSelectImage:SetActive(true)
        self._isGetSelect = true
    end
end