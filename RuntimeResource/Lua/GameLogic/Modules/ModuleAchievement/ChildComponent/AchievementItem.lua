-- AchievementItem.lua
local AchievementItem = Class.AchievementItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
AchievementItem.interface = GameConfig.ComponentsConfig.Component
AchievementItem.classPath = "Modules.ModuleAchievement.ChildComponent.AchievementItem"

require "Modules.ModuleAchievement.ChildComponent.AchievementRewardItem"
local AchievementRewardItem = ClassTypes.AchievementRewardItem
local StringStyleUtil = GameUtil.StringStyleUtil
local AchievementDataHelper = GameDataHelper.AchievementDataHelper
local UIList = ClassTypes.UIList
local AchievementManager = PlayerManager.AchievementManager
local AchievementConfig = GameConfig.AchievementConfig
local XImageFlowLight = GameMain.XImageFlowLight

function AchievementItem:Awake()
    self._icon = self:FindChildGO("Container_content/Image_Icon")

    self._titleNameText = self:GetChildComponent("Container_content/Text_Name",'TextMeshWrapper')
    self._desText = self:GetChildComponent("Container_content/Text_Des",'TextMeshWrapper')
    self._numText = self:GetChildComponent("Container_content/Text_Num",'TextMeshWrapper')

    self._notCompleteTextGo = self:FindChildGO("Container_content/Container_State/Text_Not_Complete")
    self._alreadyGetTextGo = self:FindChildGO("Container_content/Container_State/Text_Already_Get")
    self._getRewardButton = self:FindChildGO("Container_content/Container_State/Button_Get_Reward")
    self._goButtonFlowLight = self:AddChildComponent("Container_content/Container_State/Button_Get_Reward", XImageFlowLight)
    self._csBH:AddClick(self._getRewardButton, function() self:OnGetRewardClick() end)

    self._getRewardComplete = function(id) self:GetRewardComplete(id) end

    self:InitRewardList()
end

function AchievementItem:SetButtonActive(flag)
    self._getRewardButton:SetActive(flag)    
    self._goButtonFlowLight.enabled = flag
end

function AchievementItem:InitRewardList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_content/ScrollViewList")
	self._list = list
	self._listlView = scrollView 
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(AchievementRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function AchievementItem:OnDestroy() 
    --LoggerHelper.Error("AchievementItem:OnDestroy()")
    EventDispatcher:RemoveEventListener(GameEvents.ON_GET_ACHIEVEMENT_REWARD_COMPLETE, self._getRewardComplete)
end

--override
function AchievementItem:OnRefreshData(data)
    self._id = data--成就id
    local achievementData = AchievementDataHelper.GetAchievementData(self._id)
    if achievementData.type ~= AchievementConfig.TOTAL_ACHIEVEMENT and achievementData.type ~= 0  then
        self._isTotal = false
        self._icon:SetActive(true)
        self._numText.gameObject:SetActive(true)
    else
        self._isTotal = true
        self._icon:SetActive(false)
        self._numText.gameObject:SetActive(false)
    end

    local needProgress = AchievementDataHelper.GetNeedProgress(self._id)
    local num = AchievementDataHelper.GetCompleteNum(self._id)
    local nowCompleteNum = AchievementManager:GetCompleteNum(self._id)
    
    if needProgress and num ~= 0 and nowCompleteNum ~= nil then
        local desc = "(" ..StringStyleUtil.GetLongNumberString(nowCompleteNum).. "/" .. StringStyleUtil.GetLongNumberString(num).. ")"
        if nowCompleteNum == num then
            self._desText.text = AchievementDataHelper.GetDesc(self._id) ..StringStyleUtil.GetColorStringWithTextMesh(desc, StringStyleUtil.green)
        else
            self._desText.text = AchievementDataHelper.GetDesc(self._id) ..StringStyleUtil.GetColorStringWithTextMesh(desc, StringStyleUtil.red)
        end   
    else
        self._desText.text = AchievementDataHelper.GetDesc(self._id)
    end

    self._numText.text = AchievementDataHelper.GetPoints(self._id)
    self._titleNameText.text = AchievementDataHelper.GetTitle(self._id)
    

    self._listData = AchievementDataHelper.GetReward(self._id)
    self._list:SetDataList(self._listData)

    local state = AchievementManager:GetAchievementStateById(self._id)
    if state == AchievementConfig.ACHIEVEMENT_NO_PROGRESS or state == AchievementConfig.ACHIEVEMENT_HAS_PROGRESS then
        self._notCompleteTextGo:SetActive(true)
        self._alreadyGetTextGo:SetActive(false)
        --self._getRewardButton:SetActive(false)
        self:SetButtonActive(false)
    elseif state == AchievementConfig.ACHIEVEMENT_COMPLETE then
        self._notCompleteTextGo:SetActive(false)
        self._alreadyGetTextGo:SetActive(false)
        --self._getRewardButton:SetActive(true)
        self:SetButtonActive(true)
    elseif state == AchievementConfig.ACHIEVEMENT_GET_REWARD then
        self._notCompleteTextGo:SetActive(false)
        self._alreadyGetTextGo:SetActive(true)
        --self._getRewardButton:SetActive(false)
        self:SetButtonActive(false)
    end
end

function AchievementItem:OnGetRewardClick()
    EventDispatcher:AddEventListener(GameEvents.ON_GET_ACHIEVEMENT_REWARD_COMPLETE, self._getRewardComplete)
    -- EventDispatcher:TriggerEvent(GameEvents.ON_GET_ACHIEVEMENT_REWARD_CLICK, self._id)
    AchievementManager:GetAchievementReward(self._id)
end

function AchievementItem:GetRewardComplete(id)
    if self._id == id then
        self._notCompleteTextGo:SetActive(false)
        self._alreadyGetTextGo:SetActive(true)
        --self._getRewardButton:SetActive(false)
        self:SetButtonActive(false)
        EventDispatcher:RemoveEventListener(GameEvents.ON_GET_ACHIEVEMENT_REWARD_COMPLETE, self._getRewardComplete)
    end
end