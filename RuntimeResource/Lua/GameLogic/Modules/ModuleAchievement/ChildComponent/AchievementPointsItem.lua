-- AchievementPointsItem.lua
local AchievementPointsItem = Class.AchievementPointsItem(ClassTypes.BaseComponent)

AchievementPointsItem.interface = GameConfig.ComponentsConfig.Component
AchievementPointsItem.classPath = "Modules.ModuleAchievement.ChildComponent.AchievementPointsItem"

local UIProgressBar = ClassTypes.UIProgressBar

--override
function AchievementPointsItem:Awake()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")

    self._barComp = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar")
    self._barComp:SetProgress(0)
    self._barComp:ShowTweenAnimate(true)
    self._barComp:SetMutipleTween(true)
    self._barComp:SetIsResetToZero(false)
    self._barComp:SetTweenTime(0.2)
end

function AchievementPointsItem:OnDestroy()
end

function AchievementPointsItem:SetName(name)
    self._nameText.text = name
end

function AchievementPointsItem:SetType(type)
    self._type = type
end

function AchievementPointsItem:GetType()
    return self._type
end

function AchievementPointsItem:SetProgress(cur, max)
    self._barComp:SetProgress(cur/max)
    self._barComp:SetProgressText(tostring(cur).."/"..tostring(max))
end