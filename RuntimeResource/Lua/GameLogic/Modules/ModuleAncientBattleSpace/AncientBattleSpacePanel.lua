require "Modules.ModuleAncientBattleSpace.ChildView.AncientBattleSpaceScoreRankView"
require "UIComponent.Extend.TipsCom"
require"Modules.ModuleMainUI.ChildComponent.RewardItem"
require "Modules.ModuleAncientBattle.ChildView.AncientBattleRankRewardView"

local AncientBattleSpacePanel = Class.AncientBattleSpacePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InstanceManager = PlayerManager.InstanceManager
local UIProgressBar = ClassTypes.UIProgressBar
local StringStyleUtil = GameUtil.StringStyleUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper
local public_config = require("ServerConfig/public_config")
local action_config = require("ServerConfig/action_config")
local AncientBattleManager = PlayerManager.AncientBattleManager
local GameSceneManager = GameManager.GameSceneManager
local DateTimeUtil = GameUtil.DateTimeUtil
local math = math
local AncientBattleSpaceScoreRankView = ClassTypes.AncientBattleSpaceScoreRankView
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local TipsCom = ClassTypes.TipsCom
local RewardItem = ClassTypes.RewardItem
local AncientBattleRankRewardView = ClassTypes.AncientBattleRankRewardView
local AncientBattleDataHelper = GameDataHelper.AncientBattleDataHelper

local Unit_COUNT = 4
local Grade_Lang = {[1]=53423, [2]=53424, [3]=53425, [4]=53426, [5]=53427}
local Max_Point_Count = 5
local Self_Point_Icon = 13414
local Enemy_Point_Icon = 13415
local Empty_Point_Icon = 13416

local Warnning_Point = 1500
local Start_Count_Down_Sec = 60

local function SortByKey1(a,b)
    return tonumber(a[1]) < tonumber(b[1])
end

--override
function AncientBattleSpacePanel:Awake()
    self.disableCameraCulling = true
    self._eids = {}
    self.firstGetData = false
    self.monsterCountEnd = false
    self.beginIndex = 1
    self:InitView()
    self:InitListenerFunc()
end

--override
function AncientBattleSpacePanel:OnDestroy()

end

function AncientBattleSpacePanel:OnShow(data)
    self:SetData(data)
    self:ShowPersonalInfo()
    self:ShowSpaceInfo()
end

function AncientBattleSpacePanel:SetData(data)
    self:RefreshView(data)
end

function AncientBattleSpacePanel:OnClose()
    self:Reset()
end

function AncientBattleSpacePanel:OnEnable()
    self:AddEventListeners()
    self._startTimeShow = false
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateCountDown() end)
end

function AncientBattleSpacePanel:OnDisable()
    self:RemoveEventListeners()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    if self._timerTips then
        TimerHeap:DelTimer(self._timerTips)
        self._timerTips = nil
    end
end

function AncientBattleSpacePanel:InitListenerFunc()
    self._refreshSelfInfo = function() self:ShowPersonalInfo() end
    self._refreshFactionInfo = function() self:ShowFactionInfo() end
    self._refreshSettleInfo = function() self:ShowSettleInfo() end
    self._refreshBuffInfo = function() self:ShowBuffInfo() end

    self._onSelfHPChangeCB = function()self:OnSelfHPChange() end
    self._onEnemyHPChangeCB = function()self:OnEnemyHPChange() end
end

function AncientBattleSpacePanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ancient_Battle_Space_Self_Info, self._refreshSelfInfo)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ancient_Battle_Space_Faction_Info, self._refreshFactionInfo)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ancient_Battle_Space_Settle_Info, self._refreshSettleInfo)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ancient_Battle_Space_Buff_Info, self._refreshBuffInfo)
end

function AncientBattleSpacePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ancient_Battle_Space_Self_Info, self._refreshSelfInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ancient_Battle_Space_Faction_Info, self._refreshFactionInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ancient_Battle_Space_Settle_Info, self._refreshSettleInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ancient_Battle_Space_Buff_Info, self._refreshBuffInfo)
end

function AncientBattleSpacePanel:InitView()
    self._rankButton = self:FindChildGO("Container_SpaceInfo/Button_Rank")
    self._csBH:AddClick(self._rankButton, function() self:OnRankButton() end)
    self._scoreButton = self:FindChildGO("Container_SpaceInfo/Button_Score")
    self._csBH:AddClick(self._scoreButton, function() self:OnScoreButton() end)
    self._tipsButton = self:FindChildGO("Container_SpaceInfo/Button_Tips")
    self._csBH:AddClick(self._tipsButton, function() self:OnTipsButton() end)

    self._titleText = self:GetChildComponent("Container_SpaceInfo/Text_Title", "TextMeshWrapper")
    self._spaceInfoText = self:GetChildComponent("Container_SpaceInfo/Text_Info", "TextMeshWrapper")
    self._spaceInfoText.text = LanguageDataHelper.CreateContent(51232)
    self._factionBGIcon = self:FindChildGO("Container_SpaceInfo/Container_FactionBGIcon")
    self._factionIcon = self:FindChildGO("Container_SpaceInfo/Container_FactionIcon")

    self._scoreInfoText = self:GetChildComponent("Container_SpaceInfo/Text_ScoreInfo", "TextMeshWrapper")

    self._killText = self:GetChildComponent("Container_SpaceInfo/Text_Kill/Text_Value", "TextMeshWrapper")
    self._deadText = self:GetChildComponent("Container_SpaceInfo/Text_Dead/Text_Value", "TextMeshWrapper")
    self._rankText = self:GetChildComponent("Container_SpaceInfo/Text_Rank/Text_Value", "TextMeshWrapper")
    self._scoreText = self:GetChildComponent("Container_SpaceInfo/Text_Score/Text_Value", "TextMeshWrapper")

    self._nameText = self:GetChildComponent("Container_Self/Text_Name", "TextMeshWrapper")
    self._countText = self:GetChildComponent("Container_Self/Text_Count", "TextMeshWrapper")
    self._icon = self:FindChildGO("Container_Self/Container_Icon")
    self._infoText = self:GetChildComponent("Container_Self/Text_Info", "TextMeshWrapper")

    self._nameEnemyText = self:GetChildComponent("Container_Enemy/Text_Name", "TextMeshWrapper")
    self._countEnemyText = self:GetChildComponent("Container_Enemy/Text_Count", "TextMeshWrapper")
    self._iconEnemy = self:FindChildGO("Container_Enemy/Container_Icon")
    self._infoEnemyText = self:GetChildComponent("Container_Enemy/Text_Info", "TextMeshWrapper")

    self:InitProgress()

    self._tipsContainer = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(51245))
    self._tipsContainer:SetActive(false)

    self._showInfoContainer = self:FindChildGO("Container_ShowInfo")
    self._showInfoTipsText = self:GetChildComponent("Container_ShowInfo/Text_Tips", "TextMeshWrapper")
    self._showInfoContainer:SetActive(false)

    self._scoreRankGO = self:FindChildGO("Container_ScoreRank")
    self._scoreRankView = self:AddChildLuaUIComponent("Container_ScoreRank", AncientBattleSpaceScoreRankView)
    self._scoreRankGO:SetActive(false)

    self._rankRewardGO = self:FindChildGO("Container_RankReward")
    self._ranRewardView = self:AddChildLuaUIComponent("Container_RankReward", AncientBattleRankRewardView)
    self._rankRewardGO:SetActive(false)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_SpaceInfo/ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(true)
    self._listView:SetVerticalMove(false)
    self._list:SetItemType(RewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 5)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)

    self._finishImageGO = self:FindChildGO("Container_SpaceInfo/Image_Finish")
end

function AncientBattleSpacePanel:InitProgress()
    self._comp = UIProgressBar.AddProgressBar(self.gameObject, "Container_Self/ProgressBar")
    self._comp:SetProgress(0)
    self._comp:ShowTweenAnimate(true)
    self._comp:SetMutipleTween(true)
    self._comp:SetIsResetToZero(false)
    self._comp:SetTweenTime(0.2)

    self._compEnemy = UIProgressBar.AddProgressBar(self.gameObject, "Container_Enemy/ProgressBar")
    self._compEnemy:SetProgress(0)
    self._compEnemy:ShowTweenAnimate(true)
    self._compEnemy:SetMutipleTween(true)
    self._compEnemy:SetIsResetToZero(false)
    self._compEnemy:SetTweenTime(0.2)

    self:OnSelfHPChange(0)
    self:OnEnemyHPChange(0)
end

function AncientBattleSpacePanel:ShowSpaceInfo()
    if GameWorld.Player().faction_id == 1 then
        GameWorld.AddIcon(self._factionBGIcon, 13638)
        GameWorld.AddIcon(self._factionIcon, 13639)
    else
        GameWorld.AddIcon(self._factionBGIcon, 13640)
        GameWorld.AddIcon(self._factionIcon, 13641)
    end
end

--{faction,kill_num,die_num,rank,score}
function AncientBattleSpacePanel:ShowPersonalInfo()
    local data = AncientBattleManager:GetActionData(action_config.CROSS_ANCIENT_BATTLE_SELF_INFO) or {}
    local faction = data[1] or 1
    local killCount = data[2] or 0
    local deathCount = data[3] or 0
    local rank = data[4] or 0
    local score = data[5] or 0
    self._killText.text = killCount
    self._deadText.text = deathCount
    self._rankText.text = rank
    self._scoreText.text = score

    local maxPoint = AncientBattleDataHelper:GetAncientBattleFieldMaxPoint()
    if score >= maxPoint then
        self._finishImageGO:SetActive(true)
    else
        self._finishImageGO:SetActive(false)
    end

    local id = AncientBattleDataHelper:GetAncientBattleFieldPointByPoint(score)
    local maxScore = AncientBattleDataHelper:GetAncientBattleFieldPoint(id)
    self._scoreInfoText.text = LanguageDataHelper.CreateContentWithArgs(51246, {["0"]=maxScore})
    local rewardData = AncientBattleDataHelper:GetAncientBattleFieldAward(id, GameWorld.Player().level)
    local listData = {}
    for i=3,#rewardData,2 do
        table.insert(listData, {rewardData[i], rewardData[i+1]})
    end
    self._list:SetDataList(listData)
end

function AncientBattleSpacePanel:UpdateCountDown()
    if self.firstGetData == true then
        if self._eids[1] == nil then
            self:InitSelfMonster()
        end
        if self._eids[2] == nil then
            self:InitEnemyMonster()
        end
    end

    self:ShowMonsterCountDown()
    self:ShowAnnounce()
end

function AncientBattleSpacePanel:ShowMonsterCountDown()
    local data = AncientBattleManager:GetActionData(action_config.CROSS_ANCIENT_BATTLE_NOTIFY_ROOM_STAGE) or {}
    self.startTime = data[2] or 0
    if self.monsterCountEnd or data[2] == nil then
        return
    end
    local now = DateTimeUtil.GetServerTime()
    local left = math.max(data[2] + GlobalParamsHelper.GetParamValue(955) - now, 0)
    if left > 0 then
        self._infoText.text = LanguageDataHelper.CreateContentWithArgs(51240, {["0"]=left})
        self._infoEnemyText.text = LanguageDataHelper.CreateContentWithArgs(51240, {["0"]=left})
    else
        self._infoText.text = ""
        self._infoEnemyText.text = ""
        self.monsterCountEnd = true
    end
end

function AncientBattleSpacePanel:ShowAnnounce()
    local data = GlobalParamsHelper.GetParamValue(964)
    local now = DateTimeUtil.GetServerTime()
    for i=self.beginIndex,#data,2 do
        if self.startTime + data[i] == now then
            self.beginIndex = i
            self._showInfoTipsText.text = LanguageDataHelper.CreateContent(data[i+1])
            self._showInfoContainer:SetActive(true)
            if self._timerTips then
                TimerHeap:DelTimer(self._timerTips)
                self._timerTips = nil
            end
            self._timerTips = TimerHeap:AddSecTimer(GlobalParamsHelper.GetParamValue(972), 0, 0, function() self._showInfoContainer:SetActive(false) end)
            break
        end
    end
end

--{win_faction,killer_name}
function AncientBattleSpacePanel:ShowSettleInfo()
    local data = AncientBattleManager:GetActionData(action_config.CROSS_ANCIENT_BATTLE_NOTIFY_SETTLE) or {}
    if data[1] == 1 then
        self._showInfoTipsText.text = LanguageDataHelper.CreateContentWithArgs(51248, {["0"]=data[2]})
    else
        self._showInfoTipsText.text = LanguageDataHelper.CreateContentWithArgs(51247, {["0"]=data[2]})
    end
    self._showInfoContainer:SetActive(true)
    if self._timerTips then
        TimerHeap:DelTimer(self._timerTips)
        self._timerTips = nil
    end
    self._timerTips = TimerHeap:AddSecTimer(GlobalParamsHelper.GetParamValue(972), 0, 0, function() self._showInfoContainer:SetActive(false) end)
end

--{faction,player_name}
function AncientBattleSpacePanel:ShowBuffInfo()
    local data = AncientBattleManager:GetActionData(action_config.CROSS_ANCIENT_BATTLE_PICK_BUFF) or {}
    if data[1] == 1 then
        self._showInfoTipsText.text = LanguageDataHelper.CreateContentWithArgs(51249, {["0"]=data[2]})
    else
        self._showInfoTipsText.text = LanguageDataHelper.CreateContentWithArgs(51250, {["0"]=data[2]})
    end
    self._showInfoContainer:SetActive(true)
    if self._timerTips then
        TimerHeap:DelTimer(self._timerTips)
        self._timerTips = nil
    end
    self._timerTips = TimerHeap:AddSecTimer(GlobalParamsHelper.GetParamValue(972), 0, 0, function() self._showInfoContainer:SetActive(false) end)
end

function AncientBattleSpacePanel:OnSelfHPChange(curHP)
    local entity = GameWorld.GetEntity(self._eids[1] or 0)
    if curHP ~= nil then
        curHP = curHP or 0
        self._comp:SetProgressByValue(curHP, 1)
    elseif entity ~= nil then
        local entity = GameWorld.GetEntity(self._eids[1])
        self._comp:SetProgressByValue(entity.cur_hp , entity.max_hp)
    end
end

function AncientBattleSpacePanel:OnEnemyHPChange(curHP)
    local entity = GameWorld.GetEntity(self._eids[2] or 0)
    if curHP ~= nil then
        curHP = curHP or 0
        self._compEnemy:SetProgressByValue(curHP, 1)
    elseif entity ~= nil then
        self._compEnemy:SetProgressByValue(entity.cur_hp , entity.max_hp)
    end
end

function AncientBattleSpacePanel:ShowFactionInfo()
    self:ShowSelfInfo()
    self:ShowEnemyInfo()
    self.firstGetData = true
end

--[faction_1]={player_count,boss_monster_id,boss_monster_eid}
function AncientBattleSpacePanel:ShowSelfInfo()
    local data = AncientBattleManager:GetActionData(action_config.CROSS_ANCIENT_BATTLE_NOTIFY_FACTION_INFO) or {}
    local monsterId = data[1][2]
    if GameWorld.Player().faction_id == 1 then
        self._nameText.text = string.format("%s %s",LanguageDataHelper.CreateContent(51251), MonsterDataHelper.GetMonsterName(monsterId))
    else
        self._nameText.text = string.format("%s %s",LanguageDataHelper.CreateContent(51252), MonsterDataHelper.GetMonsterName(monsterId))
    end
    LanguageDataHelper.CreateContent(51242)
    local count = data[1][1] or 0
    self._countText.text = LanguageDataHelper.CreateContentWithArgs(53421,{["0"]=count})
    GameWorld.AddIcon(self._icon, MonsterDataHelper.GetIcon(monsterId))

    self:InitSelfMonster()
end

function AncientBattleSpacePanel:InitSelfMonster()
    local data = AncientBattleManager:GetActionData(action_config.CROSS_ANCIENT_BATTLE_NOTIFY_FACTION_INFO) or {}
    local entity = GameWorld.GetEntity(data[1][3])
    if entity ~= nil and self._eids[1] == nil then
        self._eids[1] = data[1][3]
        entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onSelfHPChangeCB)
        entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onSelfHPChangeCB)
        self:OnSelfHPChange()
    end
end

--[faction_2]={player_count,boss_monster_id,boss_monster_eid}
function AncientBattleSpacePanel:ShowEnemyInfo()
    local data = AncientBattleManager:GetActionData(action_config.CROSS_ANCIENT_BATTLE_NOTIFY_FACTION_INFO) or {}
    local monsterId = data[2][2]
    if GameWorld.Player().faction_id == 2 then
        self._nameEnemyText.text = string.format("%s %s",LanguageDataHelper.CreateContent(51251), MonsterDataHelper.GetMonsterName(monsterId))
    else
        self._nameEnemyText.text = string.format("%s %s",LanguageDataHelper.CreateContent(51252), MonsterDataHelper.GetMonsterName(monsterId))
    end
    local count = data[2][1] or 0
    self._countEnemyText.text = LanguageDataHelper.CreateContentWithArgs(53421,{["0"]=count})
    GameWorld.AddIcon(self._iconEnemy, MonsterDataHelper.GetIcon(monsterId))

    self:InitEnemyMonster()
end

function AncientBattleSpacePanel:InitEnemyMonster()
    local data = AncientBattleManager:GetActionData(action_config.CROSS_ANCIENT_BATTLE_NOTIFY_FACTION_INFO) or {}
    local entity = GameWorld.GetEntity(data[2][3])
    if entity ~= nil and self._eids[2] == nil then
        self._eids[2] = data[2][3]
        entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onEnemyHPChangeCB)
        entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onEnemyHPChangeCB)
        self:OnEnemyHPChange()
    end
end

function AncientBattleSpacePanel:OnRankButton()
    self._rankRewardGO:SetActive(true)
    self._ranRewardView:ShowView()
end

function AncientBattleSpacePanel:OnScoreButton()
    self._scoreRankGO:SetActive(true)
    self._scoreRankView:ShowView()
end

function AncientBattleSpacePanel:OnTipsButton()
    self._tipsContainer:SetActive(true)
end

function AncientBattleSpacePanel:RefreshView(data)
    
end

function AncientBattleSpacePanel:Reset()
    self._eids = {}
    self.firstGetData = false
    self.monsterCountEnd = false
    self._infoText.text = ""
    self._infoEnemyText.text = ""
    self.beginIndex = 1
end

function AncientBattleSpacePanel:WinBGType()
    return PanelWinBGType.NoBG
end