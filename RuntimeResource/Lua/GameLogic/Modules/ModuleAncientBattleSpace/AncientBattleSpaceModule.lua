AncientBattleSpaceModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function AncientBattleSpaceModule.Init()
    GUIManager.AddPanel(PanelsConfig.AncientBattleSpace,"Panel_AncientBattleSpace",GUILayer.LayerUIMiddle,"Modules.ModuleAncientBattleSpace.AncientBattleSpacePanel",true,PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return AncientBattleSpaceModule