local AncientBattleSpaceScoreRankItem = Class.AncientBattleSpaceScoreRankItem(ClassTypes.UIListItem)

AncientBattleSpaceScoreRankItem.interface = GameConfig.ComponentsConfig.Component
AncientBattleSpaceScoreRankItem.classPath = "Modules.ModuleAncientBattleSpace.ChildComponent.AncientBattleSpaceScoreRankItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local GuildDataHelper = GameDataHelper.GuildDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig

local Faction_To_Lang = {[1]=51242, [2]=51243}

function AncientBattleSpaceScoreRankItem:Awake()
    self:InitView()
end

--override
function AncientBattleSpaceScoreRankItem:OnDestroy() 

end

function AncientBattleSpaceScoreRankItem:InitView()
    self._rankText = self:GetChildComponent("Container_content/Text_Rank", "TextMeshWrapper")
    self._factionText = self:GetChildComponent("Container_content/Text_Faction", "TextMeshWrapper")
    self._nameText = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._levelText = self:GetChildComponent("Container_content/Text_Level", "TextMeshWrapper")
    self._scoreText = self:GetChildComponent("Container_content/Text_Score", "TextMeshWrapper")
    self._fightPowerText = self:GetChildComponent("Container_content/Text_FightPower", "TextMeshWrapper")
end

--data {faction,name,level,score,fight_force, rank}
function AncientBattleSpaceScoreRankItem:OnRefreshData(data)
    self.data = data
    if data[1] == 1 then
        self._factionText.text = BaseUtil.GetColorString(string.format("[%s]", LanguageDataHelper.CreateContent(Faction_To_Lang[data[1]])), ColorConfig.K)
    else
        self._factionText.text = BaseUtil.GetColorString(string.format("[%s]", LanguageDataHelper.CreateContent(Faction_To_Lang[data[1]])), ColorConfig.H)
    end
    self._nameText.text = data[2]
    self._levelText.text = data[3]
    self._scoreText.text = data[4]
    self._fightPowerText.text = StringStyleUtil.GetLongNumberString(data[5])
    self._rankText.text =  data[6]
end