require "Modules.ModuleAncientBattleSpace.ChildComponent.AncientBattleSpaceScoreRankItem"

local AncientBattleSpaceScoreRankView = Class.AncientBattleSpaceScoreRankView(ClassTypes.BaseLuaUIComponent)

AncientBattleSpaceScoreRankView.interface = GameConfig.ComponentsConfig.Component
AncientBattleSpaceScoreRankView.classPath = "Modules.ModuleAncientBattleSpace.ChildView.AncientBattleSpaceScoreRankView"

local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local GuildDataHelper = GameDataHelper.GuildDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local AncientBattleSpaceScoreRankItem = ClassTypes.AncientBattleSpaceScoreRankItem
local UIList = ClassTypes.UIList
local AncientBattleDataHelper = GameDataHelper.AncientBattleDataHelper
local AncientBattleManager = PlayerManager.AncientBattleManager
local action_config = GameWorld.action_config
local UIToggleGroup = ClassTypes.UIToggleGroup

local Max_Toggle_Count = 3

function AncientBattleSpaceScoreRankView:Awake()
    self:InitView()
    self:InitCallback()
end
--override
function AncientBattleSpaceScoreRankView:OnDestroy() 

end

function AncientBattleSpaceScoreRankView:ShowView()
    self:AddEventListeners()
    AncientBattleManager:OnGetScoreRankReq()
    self.toggle:SetSelectIndex(0)
    self:OnToggleGroupClick(0)
end

function AncientBattleSpaceScoreRankView:CloseView()
    self:RemoveEventListeners()
end

function AncientBattleSpaceScoreRankView:InitCallback()
    self._RefreshAncientBattleSpaceScoreRank = function() self:RefreshView() end
end

function AncientBattleSpaceScoreRankView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ancient_Battle_Space_Score_Rank, self._RefreshAncientBattleSpaceScoreRank)
end

function AncientBattleSpaceScoreRankView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ancient_Battle_Space_Score_Rank, self._RefreshAncientBattleSpaceScoreRank)
end


function AncientBattleSpaceScoreRankView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function() self:OnCloseButtonClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(AncientBattleSpaceScoreRankItem)
    self._list:SetPadding(10, 0, 0, 0)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self:InitToggleGroup()
end

function AncientBattleSpaceScoreRankView:InitToggleGroup()
    self.toggle = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup")
	self.toggle:SetOnSelectedIndexChangedCB(function(index) self:OnToggleGroupClick(index) end)
    self.toggle:SetSelectIndex(0)
    self:OnToggleGroupClick(0)

    self.toggleIconGO = {}
    for i=1,Max_Toggle_Count do
        self.toggleIconGO[i] = self:FindChildGO("ToggleGroup/Toggle"..i)
    end
end

function AncientBattleSpaceScoreRankView:OnToggleGroupClick(index)
    if self._selectTab == index then
        return
    end
    self._selectTab = index
    self:RefreshView()
end

function AncientBattleSpaceScoreRankView:RefreshView()
    local data = AncientBattleManager:GetActionData(action_config.CROSS_ANCIENT_BATTLE_GET_SCORE_RANK) or {}
    local listData = {}
    if self._selectTab == 0 then
        listData = data
    elseif self._selectTab == 1 then
        for k,v in ipairs(data) do
            if v[1]== 1 then
                table.insert(listData, v)
            end
        end
    elseif self._selectTab == 2 then
        for k,v in ipairs(data) do
            if v[1]== 2 then
                table.insert(listData, v)
            end
        end
    end
    self._list:SetDataList(listData)
end

function AncientBattleSpaceScoreRankView:OnCloseButtonClick()
    self:CloseView()
    self.gameObject:SetActive(false)
end