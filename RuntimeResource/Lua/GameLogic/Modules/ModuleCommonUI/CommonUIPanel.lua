--CommonUIPanel.lua
--常用UI界面，预备支持的接口：
--1.ShowNext 弹出文字（阻挡操作，点击继续,主要用于cg、指引等）
--2.ShowDialog 弹出对话框(确定/取消、确定，通用对话框)
--3.ShowText 弹出漂浮文字，从下到上，不阻挡操作，战斗数值
--4.ShowNotice 通知，从上到下弹出，类似手机通知，定时消失
--5.Showbarrage 弹幕，从右到左
--6.ShowUIFx 显示特效
--7.其他（ShowToolTip提示、）
--[[
例子代码：
-- GameManager.GUIManager.ShowNext("我是Nextaaa",function()
-- LoggerHelper.Error("Nextbbb")
-- end)

-- GameManager.GUIManager.ShowDialog(1,"我是对话框Ok",function()
-- 	LoggerHelper.Error("对话框ok bbb")
-- end)

-- 	GameManager.GUIManager.ShowDialog(2,"我是对话框OkCancel",
-- 		function()
-- 			LoggerHelper.Error("对话框okcancel OK")
-- 		end ,
-- 		function()
-- 			LoggerHelper.Error("对话框okcancel cancel")
-- 		end)

-- GameManager.GUIManager.ShowText(1,"静态文字静态文字111111",10)
-- GameManager.GUIManager.ShowText(1,"静态文字静态文字2222222",10)
-- GameManager.GUIManager.ShowText(1,"静态文字静态文字3333333",10)

-- GameManager.GUIManager.ShowText(2,"从下到上文字11111",10,function()
-- 	LoggerHelper.Error("从下到上文字 finish")
-- end)

-- GameManager.GUIManager.ShowText(3,"从右到左文字11111",20)
-- GameManager.GUIManager.ShowText(3,"从右到左文字22222",20)
-- GameManager.GUIManager.ShowText(3,"从右到左文字33333",20)

--GameManager.GUIManager.ShowNotice("这是通知通知",5,5,function()
]]
--CommonUIModule.lua
require "Modules.ModuleCommonUI.ChildView.StaticTextTweenItem"
--require "Modules.ModuleCommonUI.ChildView.NewsTickerView"
require "Modules.ModuleCommonUI.ChildView.NewsTickerUpView"
require "Modules.ModuleCommonUI.ChildView.NewsTickerBottomView"
require "Modules.ModuleCommonUI.ChildView.NewsTickerHornView"

local StaticTextTweenItem = ClassTypes.StaticTextTweenItem
--local NewsTickerView = ClassTypes.NewsTickerView
local NewsTickerUpView = ClassTypes.NewsTickerUpView
local NewsTickerBottomView = ClassTypes.NewsTickerBottomView
local NewsTickerHornView = ClassTypes.NewsTickerHornView

local CommonUIPanel = Class.CommonUIPanel(ClassTypes.BasePanel)
local LocalSetting = GameConfig.LocalSetting
local GUIManager = GameManager.GUIManager
local GameObject = UnityEngine.GameObject
local RectTransform = UnityEngine.RectTransform
local UIComponentUtil = GameUtil.UIComponentUtil
local FloatTextType = GameConfig.EnumType.FloatTextType
local XArtNumber = GameMain.XArtNumber
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

local Vector3_ONE = Vector3.New(1, 1, 1)
local Max_Float_Type = 6
local Float_Gap = 30 --同时显示两个floatText时相差间隔

function CommonUIPanel:Awake()
    -- self._csBH = self:GetComponent('LuaUIPanel')
    self._checkTimerIds = {}
	self:InitComps()
end

function CommonUIPanel:InitComps()
	self._ContainerNext = self:FindChildGO("Container_Next")
    self._NextGuide = self:FindChildGO("Container_Next/Type_Guide")
    self._NextText = self:FindChildGO("Container_Next/Type_Guide/Guide_Context")
    self._NextButton = self:FindChildGO("Container_Next/Type_Guide/Button_Next")
    
    self._ContainerDialog = self:FindChildGO("Container_Dialog")
    self._DialogOK = self:FindChildGO("Container_Dialog/Type_OK")
    self._DialogOk_Text = self:FindChildGO("Container_Dialog/Type_OK/OK_Image/OK_Text")
    self._DialogOk_Button = self:FindChildGO("Container_Dialog/Type_OK/OK_Image/OK_Button")
    self._DialogOKCancel = self:FindChildGO("Container_Dialog/Type_OKCancel")
    self._DialogOKCancel_Text = self:FindChildGO("Container_Dialog/Type_OKCancel/OK_Image/OK_Text")
    self._DialogOKCancel_BtnOk = self:FindChildGO("Container_Dialog/Type_OKCancel/OK_Image/OK_Button")
    self._DialogOKCancel_BtnCancel = self:FindChildGO("Container_Dialog/Type_OKCancel/OK_Image/Cancel_Button")

    self._ContainerMapName = self:FindChildGO("Container_MapName")
    self._textMapName = self:GetChildComponent("Container_MapName/Container_Content/Text_Name", "TextMeshWrapper")

    self._ConatainerFloatText = self:FindChildGO("Container_FloatText")
    self._StaticFloatText = self:FindChildGO("Container_FloatText/FloatText_Static")
    self._StaticTemplate = self:FindChildGO("Container_FloatText/FloatText_Static/Container_StaticText")
    self._Down2UpFloatText = self:FindChildGO("Container_FloatText/FloatText_Down2Up")
    self._Down2UpTemplate = self:FindChildGO("Container_FloatText/FloatText_Down2Up/Container_Down2Up")
    self._Right2LeftFloatText = self:FindChildGO("Container_FloatText/FloatText_Right2Left")
    self._Right2LeftTemplate = self:FindChildGO("Container_FloatText/FloatText_Right2Left/Container_Right2Left")
    -- self._ConatainerNotice = self:FindChildGO("Container_Notice")
    -- self._NoticeText = self:FindChildGO("Container_Notice/Image_Notice/Text_Notice")
    self._SubtitleFloatText = self:FindChildGO("Container_FloatText/FloatText_Subtitle")
    self._SubtitleTemplate = self:FindChildGO("Container_FloatText/FloatText_Subtitle/Container_Subtitle")
    self._ExpFloatText = self:FindChildGO("Container_FloatText/FloatText_ExpAdd")
    self._ExpTemplate = self:FindChildGO("Container_FloatText/FloatText_ExpAdd/Container_ExpAdd")
    UIComponentUtil.AddChildComponent(self._ExpTemplate, "Container_Exp", XArtNumber)
    self._PowerFloatText = self:FindChildGO("Container_FloatText/FloatText_PowerAdd")
    self._PowerTemplate = self:FindChildGO("Container_FloatText/FloatText_PowerAdd/Container_PowerAdd")
    UIComponentUtil.AddChildComponent(self._PowerTemplate, "Container_Power", XArtNumber)

    self._NewsTickeHornView = self:AddChildLuaUIComponent("Container_FloatText/FloatText_NewsTicker/FloatText_NewsTicker_4", NewsTickerHornView)
    self._NewsTickerUpView = self:AddChildLuaUIComponent("Container_FloatText/FloatText_NewsTicker/FloatText_NewsTicker_2", NewsTickerUpView)
    self._NewsTickerBottomView = self:AddChildLuaUIComponent("Container_FloatText/FloatText_NewsTicker/FloatText_NewsTicker_3", NewsTickerBottomView)

    
    self:InitFloatText()

    GUIManager.ShowNext = function(text, duration, callback)
        self:ShowNext(text, duration, callback)
    end

    --废弃（改用MessageBox）
    -- GUIManager.ShowDialog = function(type, text, duration, okCallback, cancelCallback)
    --     self:ShowDialog(type, text, duration, okCallback, cancelCallback)
    -- end

    GUIManager.ShowText = function(type, text, duration, callback)
        self:ShowText(type, text, duration, callback)
    end
    -- GUIManager.ShowNotice = function(text, duration, callback)
    --     self:ShowNotice(text, duration, callback)
    -- end
    GUIManager.ShowNewsTicke = function(text, count, callback,channelType)
        self:ShowNewsTicke(text, count, callback,channelType)
    end
    GUIManager.ShowMapName = function(text, duration)
        self:ShowMapName(text, duration)
    end
end

function CommonUIPanel:ShowNext(text, duration, callback)
    self._ContainerNext:SetActive(true)
    self._NextGuide:SetActive(true)
    self._NextText:GetComponent("TextMeshWrapper").text = text;
    
    self._csBH:AddClick(self._NextButton,
        function()
            self._ContainerNext:SetActive(false)
            self._csBH:RemoveClick(self._NextButton)
            if callback ~= nil then
                callback()
            end
        end)
    
    if duration > 0 then
        local timerId
        timerId = GameWorld.TimerHeap:AddSecTimer(duration, 0, 1, function()
            self._checkTimerIds[timerId] = nil 
            self._ContainerNext:SetActive(false)
            self._csBH:RemoveClick(self._NextButton)
            if callback ~= nil then
                callback()
            end
        end)
        self._checkTimerIds[timerId] = true
    end
end

--type=1是Ok，2是okcancel
function CommonUIPanel:ShowDialog(dlgType, text, duration, okCallback, cancelCallback)
    self._ContainerDialog:SetActive(true)
    if dlgType == 1 then
        self._DialogOK:SetActive(true)
        self._DialogOk_Text:GetComponent("TextMeshWrapper").text = text
        self._csBH:AddClick(self._DialogOk_Button,
            function()
                self._csBH:RemoveClick(self._DialogOk_Button)
                self._DialogOK:SetActive(false)
                if okCallback ~= nil then
                    okCallback()
                end
            end)
    elseif dlgType == 2 then
        self._DialogOKCancel:SetActive(true)
        self._DialogOKCancel_Text:GetComponent("TextMeshWrapper").text = text
        self._csBH:AddClick(self._DialogOKCancel_BtnOk,
            function()
                self._csBH:RemoveClick(self._DialogOKCancel_BtnOk)
                self._csBH:RemoveClick(self._DialogOKCancel_BtnCancel)
                self._DialogOKCancel:SetActive(false)
                if okCallback ~= nil then
                    okCallback()
                end
            end)
        self._csBH:AddClick(self._DialogOKCancel_BtnCancel,
            function()
                self._csBH:RemoveClick(self._DialogOKCancel_BtnOk)
                self._csBH:RemoveClick(self._DialogOKCancel_BtnCancel)
                self._DialogOKCancel:SetActive(false)
                if cancelCallback ~= nil then
                    cancelCallback()
                end
            end)
    end
end

--跑马灯公告
function CommonUIPanel:ShowNewsTicke(text, count, callback,channelType)
    count = count or 1
    if text == nil or text == "" then
        LoggerHelper.Error("errcode:文本不能为空")
        return
    end

    if channelType == 25 or channelType == 26 then
        -- local data = {["text"]=text, ["count"]=count, ["callback"]=callback,["channelType"]=channelType}
        -- self._NewsTickeView:OnShow(data)
        local data = {["text"]=text, ["count"]=count, ["callback"]=callback,["channelType"]=channelType}
        self._NewsTickerUpView:OnShow(data)
    elseif  channelType == 27 then    
        local data = {["text"]=text, ["count"]=count, ["callback"]=callback,["channelType"]=channelType}
        self._NewsTickerBottomView:OnShow(data)
    elseif channelType == 8 then
        local data = {["text"]=text, ["count"]=count, ["callback"]=callback,["channelType"]=channelType}
        self._NewsTickeHornView:OnShow(data)
    elseif channelType == 23 then
        local data = {["text"]=text, ["count"]=count, ["callback"]=callback,["channelType"]=channelType}
        self._NewsTickerUpView:OnShow(data)
    elseif channelType == 31 then
        local data = {["text"]=text, ["count"]=count, ["callback"]=callback,["channelType"]=channelType}
        self._NewsTickeHornView:OnShow(data)
    else
        LoggerHelper.Error("errcode:跑马灯没有定义该频道")
    end

end

-- --喇叭公告
-- function CommonUIPanel:ShowHornNewsTicke(text, count, callback,channelType)
--     count = count or 1
--     if channelType == 25 or channelType == 26 then
--         -- local data = {["text"]=text, ["count"]=count, ["callback"]=callback,["channelType"]=channelType}
--         -- self._NewsTickeView:OnShow(data)
--         local data = {["text"]=text, ["count"]=count, ["callback"]=callback,["channelType"]=channelType}
--         self._NewsTickerUpView:OnShow(data)
--     elseif  channelType == 27 then    
--         local data = {["text"]=text, ["count"]=count, ["callback"]=callback,["channelType"]=channelType}
--         self._NewsTickerBottomView:OnShow(data)
--     end

-- end

--显示场景名字
function CommonUIPanel:ShowMapName(text, duration)
    self._textMapName.text = text
    self:ClearDelayMapTimer()
    self:ClearDurationMapTimer()
    self._timerDelayShowMapName = TimerHeap:AddSecTimer(1, 0, 0, function() self._ContainerMapName:SetActive(true) end)
    self._timerDurationShowMapName = TimerHeap:AddTimer((duration or 5000) + 1000, 0, 0, function() self._ContainerMapName:SetActive(false) end)
end

--type:1是static,2是down2Up,3是right2Left,4是字幕
function CommonUIPanel:ShowText(textType, text, duration, callback)
    local defaultDuration = 1
    if textType == FloatTextType.ExpAdd then
        defaultDuration = 2
    end
    duration = duration or defaultDuration
    local showTextItem = {["type"]=textType, ["text"]=text, ["duration"]=duration, ["callback"]=callback}
    if textType == FloatTextType.PowerAdd then
        --FloatTextType.PowerAdd只显示最新的
        if #self._showTextQueue[textType] == 0 then
            table.insert(self._showTextQueue[textType], showTextItem)
        else
            table.remove(self._showTextQueue[textType])
            table.insert(self._showTextQueue[textType], showTextItem)
        end
    elseif textType == FloatTextType.ExpAdd then
        --FloatTextType.ExpAdd只显示10条
        if #self._showTextQueue[textType] < 10 then
            table.insert(self._showTextQueue[textType], showTextItem)
        else
            table.remove(self._showTextQueue[textType])
            table.insert(self._showTextQueue[textType], showTextItem)
        end
    else
        table.insert(self._showTextQueue[textType], showTextItem)
    end
    self:CheckShowText()
end

function CommonUIPanel:CheckShowText()
    for i =1,Max_Float_Type do
        self:CheckShowTextByType(i)
    end
end

function CommonUIPanel:CheckShowTextByType(type)
    local showTextQueue = self._showTextQueue[type]
    while #showTextQueue > 0 do
        local canShowCount = self._maxShowCount[type] - self:GetCurShowCount(type)
        local showCount = math.min(canShowCount, #showTextQueue)
        if showCount <= 0 then
            break
        end
        --ExpAdd Down2Up 逐条显示
        if type == FloatTextType.ExpAdd or type == FloatTextType.Down2Up then
            local pos = self._textTypePos[type]
            if self._lastShowTextTypeDict[type] ~= nil and self._lastShowTextTypeDict[type].show == 1 
                and self._lastShowTextTypeDict[type].go.transform.localPosition.y < self._textTypePos[type].y + Float_Gap then

                local timerId
                timerId = GameWorld.TimerHeap:AddSecTimer(0.3, 0, 1, function() 
                    self._checkTimerIds[timerId] = nil 
                    self:CheckShowTextByType(type) 
                end)
                self._checkTimerIds[timerId] = true
                break
            end
        end

        local showTextItem = showTextQueue[1]
        local text = showTextItem.text
        local duration = showTextItem.duration
        local callback = showTextItem.callback
        self:RealShowText(type, text, duration, callback)
        table.remove(showTextQueue, 1)
    end
end

function CommonUIPanel:GetCurShowCount(type)
    local count = 0
    local floatTextQueue = self._floatTextQueueDict[type]
    for i=1,self._maxShowCount[type] do
        local item = floatTextQueue[i]
        if item.show == 1 then
            count = count + 1
        end
    end
    return count
end

function CommonUIPanel:RealShowText(textType, text, duration, callback)
    if textType == FloatTextType.Static then
        self:ShowStaticTextPro(text, duration, callback)
    elseif textType == FloatTextType.Down2Up then
        self:ShowDown2UpText(text, duration, callback)
    elseif textType == FloatTextType.Right2Left then
        self:ShowRight2Left(text, duration, callback)
    elseif textType == FloatTextType.Subtitle then
        self:ShowSubtitle(text, duration, callback)
    elseif textType == FloatTextType.ExpAdd then
        self:ShowExpAdd(text, duration, callback)
    elseif textType == FloatTextType.PowerAdd then
        self:ShowPowerAdd(text, duration, callback)
    end
end

function CommonUIPanel:InitFloatText()
    self._textTypeTemplate = {[FloatTextType.Static]=self._StaticTemplate, [FloatTextType.Down2Up]=self._Down2UpTemplate, 
                                [FloatTextType.Right2Left]=self._Right2LeftTemplate, [FloatTextType.Subtitle]=self._SubtitleTemplate, 
                                [FloatTextType.ExpAdd]=self._ExpTemplate, [FloatTextType.PowerAdd]=self._PowerTemplate}
    self._templateParent = {[FloatTextType.Static]=self._StaticFloatText, [FloatTextType.Down2Up]=self._Down2UpFloatText, 
                                [FloatTextType.Right2Left]=self._Right2LeftFloatText, [FloatTextType.Subtitle]=self._SubtitleFloatText, 
                                [FloatTextType.ExpAdd]=self._ExpFloatText, [FloatTextType.PowerAdd]=self._PowerFloatText}
    self._textTypePos = {[FloatTextType.Static]=self._StaticTemplate.transform.localPosition, [FloatTextType.Down2Up]=self._Down2UpTemplate.transform.localPosition, 
                            [FloatTextType.Right2Left]=self._Right2LeftTemplate.transform.localPosition, [FloatTextType.Subtitle]=self._SubtitleTemplate.transform.localPosition,
                            [FloatTextType.ExpAdd]=self._ExpTemplate.transform.localPosition, [FloatTextType.PowerAdd]=self._PowerTemplate.transform.localPosition}
    --隐藏模板
    for i=1,Max_Float_Type do
        self._textTypeTemplate[i].transform.localPosition = self.HIDE_POSITION
    end
    self._floatTextQueueDict = {{}, {}, {}, {}, {}, {}} --类型对应的floatType资源队列
    self._lastShowTextTypeDict = {} --类型对应的上一个显示的item
    self._maxShowCount = {5, 5, 5, 1, 5, 1} --设置每个类型同时最大显示数目
    --类型对应待显示text的队列
    self._showTextQueue = {[1]={}, [2]={}, [3]={}, [4]={}, [5]={}, [6]={}}
    for i=1,Max_Float_Type do
        self:InitFloatTextGOByType(i)
    end
end

function CommonUIPanel:InitFloatTextGOByType(type)
     local floatTextQueue = self._floatTextQueueDict[type]
     for i=1,self._maxShowCount[type] do
        local template = self._textTypeTemplate[type]
        local newGo = GameObject.Instantiate(template)
        newGo.transform:SetParent(self._templateParent[type].transform, false)
        newGo.name = tostring(i)
        local newItem = {["go"]=newGo, ["show"]=0, ["index"]=i-1}
        table.insert(floatTextQueue, newItem)
    end
end

function CommonUIPanel:GetFloatText(type)
    local floatItem
    local floatTextQueue = self._floatTextQueueDict[type]
    for i=1,self._maxShowCount[type] do
        local item = floatTextQueue[i]
        if item.show == 0 then
            item.show = 1
            floatItem = item
            self._lastShowTextTypeDict[type] = item
            break
        end
    end
    return floatItem
end
--回收重置
function CommonUIPanel:ResetFloatText(item)
    item.show = 0
    item.go.transform.localPosition = self.HIDE_POSITION
    item.go.transform.localScale = Vector3_ONE
    self:CheckShowText()
end

function CommonUIPanel:TextLocalMove(item, text, moveTo, duration, type, callback)
    local moveTextCurrent = item.go
    if item.setComponent == nil then
        item.setComponent = UIComponentUtil.GetChildComponent(moveTextCurrent, "Text", "TextMeshWrapper")
    end
    if item.tweenComponent == nil then
        item.tweenComponent = moveTextCurrent:AddComponent(typeof(XGameObjectTweenPosition))
    end

    item.setComponent.text = text
    item.tweenComponent:OnceOutQuad(moveTo, duration, function()
        self:ResetFloatText(item)
        if callback ~= nil then
            callback()
        end
    end)
end

function CommonUIPanel:TextLocalMoveMode(item, text, moveTo, duration, type, callback, mode)
    local moveTextCurrent = item.go
    if item.setComponent == nil then
        item.setComponent = UIComponentUtil.GetChildComponent(moveTextCurrent, "Text", "TextMeshWrapper")
    end
    if item.tweenComponent == nil then
        item.tweenComponent = moveTextCurrent:AddComponent(typeof(XGameObjectTweenPosition))
    end
    item.setComponent.text = text
    item.tweenComponent:SetToPosTween(moveTo, duration, 1, mode, function()
        self:ResetFloatText(item)
        if callback ~= nil then
            callback()
        end
    end)
end

function CommonUIPanel:ImageTextLocalMoveMode(item, text, moveTo, duration, type, callback, mode)
    mode = mode or 0
    local moveTextCurrent = item.go
    if item.setComponent == nil then
        item.setComponent = UIComponentUtil.GetChildComponent(moveTextCurrent, "Container_Exp", "XArtNumber")
    end
    if item.tweenComponent == nil then
        item.tweenComponent = moveTextCurrent:AddComponent(typeof(XGameObjectTweenPosition))
    end

    item.setComponent:SetString(text)
    item.tweenComponent:SetToPosTween(moveTo, duration, 1, mode, function()
        self:ResetFloatText(item)
        if callback ~= nil then
            callback()
        end
    end)
end

function CommonUIPanel:TextLocalScale(item, text, moveTo, duration, type, callback)
    local moveTextCurrent = item.go
    if item.setComponent == nil then
        item.setComponent = UIComponentUtil.GetChildComponent(moveTextCurrent, "Text", "TextMeshWrapper")
    end
    if item.tweenComponent == nil then
        item.tweenComponent = moveTextCurrent:AddComponent(typeof(XGameObjectTweenScale))
    end

    item.setComponent.text = text
    item.tweenComponent:OnceOutQuad(moveTo, duration, function()
        self:ResetFloatText(item)
        if callback ~= nil then
            callback()
        end
    end)
end

function CommonUIPanel:PowerTextShow(item, text, duration, callback)
    local moveTextCurrent = item.go
    if item.setComponent == nil then
        item.setComponent = UIComponentUtil.GetChildComponent(moveTextCurrent, "Container_Power", "XArtNumber")
    end
    if item.fx == nil then
        item.fx = UIParticle.AddParticle(moveTextCurrent, "Container_Power/Container_Fx/fx_ui_fightpower_fire")
        item.fx.transform.localScale = Vector3.New(72,72,72)
    end
    local len = string.len(text)
    local interval = 50
    local powerTime = 0
    local count = 0
    local delta = math.floor(text/5)
    local powerDuration = duration * 1000
    local powerTimeId = -1
    local showText = 0
    local func = function()
        if powerTime > powerDuration then
            if powerTimeId ~= -1 then
                self._checkTimerIds[powerTimeId] = nil 
                TimerHeap:DelTimer(powerTimeId)
                self:ResetFloatText(item)
                if callback ~= nil then
                    callback()
                end
            end
        else
            powerTime = powerTime + interval
            if count > 6 then
                showText = text
            else
                count = count + 1
                showText = BaseUtil.RandomNumber(len)
            end
            item.setComponent:SetString("+"..showText)
        end
    end
    powerTimeId = TimerHeap:AddTimer(0, interval, 0, func)
    self._checkTimerIds[powerTimeId] = true
end

-- 显示statictext
function CommonUIPanel:ShowStaticTextPro(text, duration, callback)
    local type = FloatTextType.Static
    local item = self:GetFloatText(type)
    local staticTextCurrent = item.go
    staticTextCurrent.transform.localPosition = self._textTypePos[type] + Vector3.New(0, Float_Gap*item.index, 0)
    local endScale = 1.2
    self:TextLocalScale(item, text, endScale, duration, type, callback)
end

function CommonUIPanel:ShowDown2UpText(text, duration, callback)
    local type = FloatTextType.Down2Up
    local item = self:GetFloatText(type)
    local down2UpTextCurrent = item.go
    down2UpTextCurrent.transform.localPosition = self._textTypePos[type]
    local moveTo = Vector3.New(0, 100, 0) + down2UpTextCurrent.transform.localPosition
    self:TextLocalMoveMode(item, text, moveTo, duration, type, callback, 1)
end

function CommonUIPanel:ShowRight2Left(text, duration, callback)
    local type = FloatTextType.Right2Left
    local item = self:GetFloatText(type)
    local right2leftTextCurrent = item.go
    right2leftTextCurrent.transform.localPosition = self._textTypePos[type]
    local moveTo = Vector3.New(-2560, 0, 0) + self._textTypePos[type]
    self:TextLocalMove(item, text, moveTo, duration, type, callback)
end

function CommonUIPanel:ShowSubtitle(text, duration, callback)
    local type = FloatTextType.Subtitle
    local item = self:GetFloatText(type)
    local subtitleTextCurrent = item.go
    subtitleTextCurrent.transform.localPosition = self._textTypePos[type]
    local moveTo = subtitleTextCurrent.transform.localPosition
    self:TextLocalMove(item, text, moveTo, duration, type, callback)
end

function CommonUIPanel:ShowExpAdd(text, duration, callback)
    local type = FloatTextType.ExpAdd
    local item = self:GetFloatText(type)
    local expAddTextCurrent = item.go
    expAddTextCurrent.transform.localPosition = self._textTypePos[type]
    local moveTo = Vector3.New(0, 150, 0) + expAddTextCurrent.transform.localPosition
    self:ImageTextLocalMoveMode(item, text, moveTo, duration, type, callback, 1)
end

function CommonUIPanel:ShowPowerAdd(text, duration, callback)
    local type = FloatTextType.PowerAdd
    local item = self:GetFloatText(type)
    local powerAddTextCurrent = item.go
    powerAddTextCurrent.transform.localPosition = self._textTypePos[type]
    self:PowerTextShow(item, text, duration, callback)
end

function CommonUIPanel:ShowUIFx(id)

end

function CommonUIPanel:OnDestroy()
    self._csBH = nil
    self:ClearData()
end

function CommonUIPanel:ClearData()
    if next(self._checkTimerIds) then
        for timerId, _ in pairs(self._checkTimerIds) do
            TimerHeap:DelTimer(timerId)
        end
        self._checkTimerIds = {}
    end
    self._showTextQueue = {}
    self:ClearDelayMapTimer()
    self:ClearDurationMapTimer()
end


function CommonUIPanel:ClearDelayMapTimer()
    if self._timerDelayShowMapName then
        TimerHeap:DelTimer(self._timerDelayShowMapName)
        self._timerDelayShowMapName = nil
    end
end



function CommonUIPanel:ClearDurationMapTimer()
    if self._timerDurationShowMapName then
        TimerHeap:DelTimer(self._timerDurationShowMapName)
        self._timerDurationShowMapName = nil
    end
end

