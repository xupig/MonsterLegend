--跑马灯界面
local NewsTickerView = Class.NewsTickerView(ClassTypes.BaseLuaUIComponent)

NewsTickerView.interface = GameConfig.ComponentsConfig.Component
NewsTickerView.classPath = "Modules.ModuleCommonUI.ChildView.NewsTickerView"

local GUIManager = GameManager.GUIManager
local LuaTween = GameMain.LuaTween
local ceil = math.ceil

local Width_Per_Char = 22 --每个字符的设定宽度
local Text_Move_Speed = 150 --每秒移动的宽度
local Max_Count = 10

function NewsTickerView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._showQueue = {}
    self._isShow = true
    self:InitView()
end

function NewsTickerView:OnShow(data)
    self:AddItem(data)
    self:CheckShow()
end

function NewsTickerView:InitView()
    self._newsTickerTextGO = self:FindChildGO("Container_NewsTicker/Text")
    self._newsTickerText = self:GetChildComponent("Container_NewsTicker/Text", "TextMeshWrapper")
    self._newsTickerContainer = self:FindChildGO("Container_NewsTicker")
    self._newsTickerTextGOPos = self._newsTickerTextGO.transform.localPosition
    self._transformPos = self.transform.localPosition
    self._showWidth = self._newsTickerContainer.transform.sizeDelta.x
    self:Reset()
end

function NewsTickerView:CheckShow()
    while #self._showQueue > 0 do
        if self._isShow then
            break
        end
        local showItem = self._showQueue[1]
        if showItem.count == 0 then
            table.remove(self._showQueue, 1)
        else
            self:Show(showItem)
            self._showQueue[1].count = showItem.count - 1
        end
    end
end

function NewsTickerView:AddItem(data)
    if #self._showQueue >= Max_Count then
        table.remove(self._showQueue, #self._showQueue)
    end
    table.insert(self._showQueue, data)
end

function NewsTickerView:Show(data)
    self:SetShowState(true)
    local text = data.text
    local callback = data.callback
    local len = string.utf8lenwithoutcolor(text)
    local xMov = (len * Width_Per_Char + self._showWidth)
    local moveTo = Vector3.New(-1*xMov, 0, 0) + self._newsTickerTextGOPos
    local duration = xMov / Text_Move_Speed
    --LoggerHelper.Error("duration:"..duration.."len:"..len)
    self:TextLocalMove(text, moveTo, duration, callback)
end

function NewsTickerView:TextLocalMove(text, moveTo, duration, callback)
    self._newsTickerText.text = text
    --匀速(NewsTickerView已经废弃，不用修改)
    LuaTween.Transform_DoLocalMoveMode(self._newsTickerTextGO, moveTo, duration, 1).OnComplete(
        function()
            self:Reset()
            if callback ~= nil then
                callback()
            end
        end)
end

function NewsTickerView:Reset()
    self:SetShowState(false)
    self:CheckShow()
end

function NewsTickerView:SetShowState(state)
    if self._isShow ~= state then
        if state then
            self._newsTickerTextGO.transform.localPosition = self._newsTickerTextGOPos
            self.transform.localPosition = self._transformPos
        else
            self.transform.localPosition = self.HIDE_POSITION
            self._newsTickerTextGO.transform.localPosition = self.HIDE_POSITION
        end
        self._isShow = state
    end
end