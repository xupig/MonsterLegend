--跑马灯界面
local NewsTickerHornView = Class.NewsTickerHornView(ClassTypes.BaseLuaUIComponent)

NewsTickerHornView.interface = GameConfig.ComponentsConfig.Component
NewsTickerHornView.classPath = "Modules.ModuleCommonUI.ChildView.NewsTickerHornView"

local GUIManager = GameManager.GUIManager
local ceil = math.ceil
local Screen = UnityEngine.Screen
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

local Width_Per_Char = 22 --每个字符的设定宽度
local Text_Move_Speed = 200 --每秒移动的宽度
local Max_Count = 10

function NewsTickerHornView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._showQueue = {}
    self._isShow = true
    self:InitView()
end

function NewsTickerHornView:OnShow(data)
    self:AddItem(data)
    self:CheckShow()
end

function NewsTickerHornView:InitView()
    self._newsTickerTextGO = self:FindChildGO("Container_NewsTicker_Horn/Container_1")
    self._newsTickerText = self:GetChildComponent("Container_NewsTicker_Horn/Container_1/Text_Message", "TextMeshWrapper")
    self._newsTickerContainer = self:FindChildGO("Container_NewsTicker_Horn")
    self._newsTickerContainerRect = self:GetChildComponent("Container_NewsTicker_Horn", "RectTransform")
    self._MessageBGRect = self:GetChildComponent("Container_NewsTicker_Horn/Container_1/Image_bg", "RectTransform")
    self._starState1Image = self:GetChildComponent("Container_NewsTicker_Horn/Container_1/Image_bg", "ImageWrapper")
    self._newsTickerTextGOPos = self._newsTickerTextGO.transform.localPosition
    self._transformPos = self.transform.localPosition
    self._tweenPosNTH = self:AddChildComponent('Container_NewsTicker_Horn/Container_1', XGameObjectTweenPosition)
    
    self._newsTickerContainerRect.sizeDelta = Vector2.New(Screen.width, self._newsTickerContainerRect.sizeDelta.y)

    self._showWidth = self._newsTickerContainer.transform.sizeDelta.x
    self:Reset()
end

function NewsTickerHornView:CheckShow()
    while #self._showQueue > 0 do
        if self._isShow then
            break
        end
        local showItem = self._showQueue[1]
        if showItem.count == 0 then
            table.remove(self._showQueue, 1)
        else
            self:Show(showItem)
            self._showQueue[1].count = showItem.count - 1
        end
    end
end

function NewsTickerHornView:AddItem(data)
    if #self._showQueue >= Max_Count then
        table.remove(self._showQueue, #self._showQueue)
    end
    table.insert(self._showQueue, data)
end

function NewsTickerHornView:Show(data)
    self:SetShowState(true)
    local text = data.text
    local callback = data.callback
    --local len = string.utf8lenwithoutcolor(text)
    self._newsTickerText.text = text

    self._MessageBGRect.sizeDelta = Vector2.New(self._newsTickerText.preferredWidth+100, self._MessageBGRect.sizeDelta.y)
    self._starState1Image:SetAllDirty()
    local xMov = (self._newsTickerText.preferredWidth + self._showWidth+100)
    local moveTo = Vector3.New(-1*xMov, 0, 0) + self._newsTickerTextGOPos
    local duration = xMov / Text_Move_Speed
    self:TextLocalMove(text, moveTo, duration, callback)
end

function NewsTickerHornView:TextLocalMove(text, moveTo, duration, callback)
    self._tweenPosNTH:SetToPosOnce(moveTo, duration, function()
        self:Reset()
        if callback ~= nil then
            callback()
        end
    end)
end

function NewsTickerHornView:Reset()
    self:SetShowState(false)
    self:CheckShow()
end

function NewsTickerHornView:SetShowState(state)
    if self._isShow ~= state then
        if state then
            self._newsTickerTextGO.transform.localPosition = self._newsTickerTextGOPos
            self.transform.localPosition = self._transformPos
        else
            self.transform.localPosition = self.HIDE_POSITION
            self._newsTickerTextGO.transform.localPosition = self.HIDE_POSITION
        end
        self._isShow = state
    end
end