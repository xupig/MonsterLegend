--跑马灯界面
local NewsTickerUpView = Class.NewsTickerUpView(ClassTypes.BaseLuaUIComponent)

NewsTickerUpView.interface = GameConfig.ComponentsConfig.Component
NewsTickerUpView.classPath = "Modules.ModuleCommonUI.ChildView.NewsTickerUpView"

local GUIManager = GameManager.GUIManager
local ceil = math.ceil
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

local Width_Per_Char = 22 --每个字符的设定宽度
local Text_Move_Speed = 150 --每秒移动的宽度
local Max_Count = 10

function NewsTickerUpView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._showQueue = {}
    self._isShow = true
    self:InitView()
end

function NewsTickerUpView:OnShow(data)
    self:AddItem(data)
    self:CheckShow()
end

function NewsTickerUpView:InitView()
    self._newsTickerTextGO = self:FindChildGO("Container_NewsTicker_Up/Text")
    self._newsTickerText = self:GetChildComponent("Container_NewsTicker_Up/Text", "TextMeshWrapper")
    self._newsTickerContainer = self:FindChildGO("Container_NewsTicker_Up")
    self._newsTickerTextGOPos = self._newsTickerTextGO.transform.localPosition
    self._transformPos = self.transform.localPosition
    self._showWidth = self._newsTickerContainer.transform.sizeDelta.x
    self:Reset()
    self._tweenPosNTT = self:AddChildComponent('Container_NewsTicker_Up/Text', XGameObjectTweenPosition)
end

function NewsTickerUpView:CheckShow()
    while #self._showQueue > 0 do
        if self._isShow then
            break
        end
        local showItem = self._showQueue[1]
        if showItem.count == 0 then
            table.remove(self._showQueue, 1)
        else
            self:Show(showItem)
            self._showQueue[1].count = showItem.count - 1
        end
    end
end

function NewsTickerUpView:AddItem(data)
    if #self._showQueue >= Max_Count then
        table.remove(self._showQueue, #self._showQueue)
    end
    table.insert(self._showQueue, data)
end

function NewsTickerUpView:Show(data)
    self:SetShowState(true)
    local text = data.text
    local callback = data.callback
    --local len = string.utf8lenwithoutcolor(text)
    self._newsTickerText.text = text
    local xMov = (self._newsTickerText.preferredWidth + self._showWidth)
    local moveTo = Vector3.New(-1*xMov, 0, 0) + self._newsTickerTextGOPos
    local duration = xMov / Text_Move_Speed
    self:TextLocalMove(text, moveTo, duration, callback)
end

function NewsTickerUpView:TextLocalMove(text, moveTo, duration, callback)
    self._tweenPosNTT:SetToPosOnce(moveTo, duration,  function()
        self:Reset()
        if callback ~= nil then
            callback()
        end
    end)

end

function NewsTickerUpView:Reset()
    self:SetShowState(false)
    self:CheckShow()
end

function NewsTickerUpView:SetShowState(state)
    if self._isShow ~= state then
        if state then
            self._newsTickerTextGO.transform.localPosition = self._newsTickerTextGOPos
            self.transform.localPosition = self._transformPos
        else
            self.transform.localPosition = self.HIDE_POSITION
            self._newsTickerTextGO.transform.localPosition = self.HIDE_POSITION
        end
        self._isShow = state
    end
end