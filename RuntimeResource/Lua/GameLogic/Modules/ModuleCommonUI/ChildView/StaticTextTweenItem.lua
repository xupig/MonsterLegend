--StaticTextTweenItem
local StaticTextTweenItem = Class.StaticTextTweenItem(ClassTypes.BaseLuaUIComponent)

StaticTextTweenItem.interface = GameConfig.ComponentsConfig.Component
StaticTextTweenItem.classPath = "Modules.ModuleCommonUI.ChildView.StaticTextTweenItem"

local RectTransform = UnityEngine.RectTransform
local TimerHeap = GameWorld.TimerHeap
local wordWidth = 12
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale
local XImageTweenAlpha = GameMain.XImageTweenAlpha

function StaticTextTweenItem:Awake()
    self:InitVar()
    self:InitComp()
end

function StaticTextTweenItem:InitVar()
    -- self._index = -1
    self._groupTween = nil
    self._endPos = Vector3.New(0, 0, 0)
    self._timer = nil
end

function StaticTextTweenItem:OnDestroy()

end


function StaticTextTweenItem:InitComp()
    self._tweenPosition = self.gameObject:AddComponent(typeof(XGameObjectTweenPosition))
    self._tweenScale = self.gameObject:AddComponent(typeof(XGameObjectTweenScale))
    self._staticText = self:GetChildComponent("StaticText1",'TextMeshWrapper')
    self._bgGO = self.transform:Find('Image_bg')
    self._bgRectTransform = self._bgGO:GetComponent(typeof(RectTransform))
-- self._tweenAlpha = self.gameObject:AddComponent(typeof(XImageTweenAlpha))
end

function StaticTextTweenItem:SetIndex(value, text)
    self._index = value
    self:StopTween()
    self:InitText(text)
    self:PlayTween()
end

function StaticTextTweenItem:GetIndex()
    return self._index or -1
end

function StaticTextTweenItem:StopTween()
    self:StopMoveTween()
    self:StopGroupTween()
    self:StopScaleTween()
    self:StopTime()
end

function StaticTextTweenItem:InitText(text)
    self.gameObject:SetActive(true)
    self.transform.localPosition = Vector3.New(0, 0, 0)
    self._endPos = Vector3.New(0, 0, 0)
    self._staticText.text = text --tostring(self._index) .. text .. self.gameObject.name
    
    self:UpdateBg(text)
end

function StaticTextTweenItem:UpdateBg(text)
    --文本背景
    local sizeDelta = self._bgRectTransform.sizeDelta
    self._bgRectTransform.sizeDelta = Vector3.New(#text * wordWidth, sizeDelta.y, sizeDelta.z)
    local bgPos = self._bgGO.transform.localPosition
    self._bgGO.transform.localPosition= Vector3.New(-(#text * wordWidth / 2), bgPos.y, bgPos.z)
end

function StaticTextTweenItem:PlayTween()
    if self._tweenScale == nil then
        return
    end
    self._tweenScale:SetToScaleOnce(1.0, 0.3, nil)
    self._timer = GameWorld.TimerHeap:AddSecTimer(0.8, 0, 1, function()self:HideText() end)
end



function StaticTextTweenItem:StopTime()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function StaticTextTweenItem:StopScaleTween()
    -- 大小改变中
    if self._tweenScale == nil then
        return
    end
    self._tweenScale.IsTweening = false
    self.transform.localScale = Vector3.New(0.8, 0.8, 0.8)
end

function StaticTextTweenItem:StopGroupTween()
-- 透明度改变中
end

function StaticTextTweenItem:StopMoveTween()
    -- 在移动中
    if self._tweenPosition == nil then
        return
    end
    self._tweenPosition.IsTweening = false
end

function StaticTextTweenItem:MovePos()
    -- if not (self.gameObject.activeSelf) then
    --     return
    -- end
    -- self:StopTween()
    
    self._endPos = self._endPos + Vector3.New(0, 40, 0)
    self._tweenPosition:SetToPosOnce(self._endPos, 0.4, nil)
    self:StopTime()
    self._timer = GameWorld.TimerHeap:AddSecTimer(0.7, 0, 1, function()self:HideText() end)
end

function StaticTextTweenItem:HideText()
    self.gameObject:SetActive(false)
    self._staticText.text = ""
end
