--跑马灯界面
local NewsTickerBottomView = Class.NewsTickerBottomView(ClassTypes.BaseLuaUIComponent)

NewsTickerBottomView.interface = GameConfig.ComponentsConfig.Component
NewsTickerBottomView.classPath = "Modules.ModuleCommonUI.ChildView.NewsTickerBottomView"

local GUIManager = GameManager.GUIManager
local TimerHeap = GameWorld.TimerHeap
local ceil = math.ceil
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

local Width_Per_Char = 22 --每个字符的设定宽度
local Text_Move_Speed = 150 --每秒移动的宽度
local Max_Count = 10

function NewsTickerBottomView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._showQueue = {}
    self._isShow = true
    self:InitView()
end

function NewsTickerBottomView:OnShow(data)
    self:AddItem(data)
    self:CheckShow()
end

function NewsTickerBottomView:InitView()
    self._newsTickerTextGO = self:FindChildGO("Container_NewsTicker_Bottom/Text")
    self._newsTickerText = self:GetChildComponent("Container_NewsTicker_Bottom/Text", "TextMeshWrapper")
    self._newsTickerContainer = self:FindChildGO("Container_NewsTicker_Bottom")
    self._newsTickerBottomBg = self:FindChildGO("Container_NewsTicker_Bottom/Container")
    self._newsTickerBottomBg2 = self:FindChildGO("Container_NewsTicker_Bottom/Container_2")
    self._newsTickerBottomBg3 = self:FindChildGO("Container_NewsTicker_Bottom/Container_3")
    self._rectTransform = self._newsTickerBottomBg:GetComponent(typeof(UnityEngine.RectTransform))

    self._tweenScaleNTC = self._newsTickerContainer:AddComponent(typeof(XGameObjectTweenScale))

    self._newsTickerTextGOPos = self._newsTickerTextGO.transform.localPosition
    self._transformPos = self.transform.localPosition
    self._showWidth = self._newsTickerContainer.transform.sizeDelta.x
    self:Reset()
end

function NewsTickerBottomView:CheckShow()
    while #self._showQueue > 0 do
        if self._isShow then
            break
        end
        local showItem = self._showQueue[1]
        if showItem.count == 0 then
            table.remove(self._showQueue, 1)
        else
            self:Show(showItem)
            self._showQueue[1].count = showItem.count - 1
        end
    end
end

function NewsTickerBottomView:AddItem(data)
    if #self._showQueue >= Max_Count then
        table.remove(self._showQueue, #self._showQueue)
    end
    table.insert(self._showQueue, data)
end

function NewsTickerBottomView:Show(data)
    self:SetShowState(true)
    local text = data.text
    local callback = data.callback
    local len = string.utf8lenwithoutcolor(text)
    local xMov = (len * Width_Per_Char + self._showWidth)
    local moveTo = Vector3.New(-1*xMov, 0, 0) + self._newsTickerTextGOPos
    local duration = xMov / Text_Move_Speed
    self:TextLocalMove(text, moveTo, duration, callback)
end

function NewsTickerBottomView:TextLocalMove(text, moveTo, duration, callback)
    self._newsTickerText.text = text
    self._newsTickerBottomBg2:SetActive(self._newsTickerText.preferredWidth > 880)
    self._newsTickerBottomBg:SetActive(self._newsTickerText.preferredWidth <= 600)
    self._newsTickerBottomBg3:SetActive(self._newsTickerText.preferredWidth > 600 and self._newsTickerText.preferredWidth <= 880)
    
    self._tweenScaleNTC:OnceOutQuad(1, 0.5,function()  
        self:ResetScale()
        if callback ~= nil then
            callback()
        end
    end)
end

function NewsTickerBottomView:ResetScale()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
    end
    self._timer = TimerHeap:AddSecTimer(1, 0, 0, function()self:Reset() end)
end



function NewsTickerBottomView:Reset()


    self:SetShowState(false)
    self:CheckShow()
end

function NewsTickerBottomView:SetShowState(state)
    if self._isShow ~= state then
        if state then
            self._newsTickerTextGO.transform.localPosition = self._newsTickerTextGOPos
            self.transform.localPosition = self._transformPos
            self._newsTickerContainer.transform.localScale = Vector2(0,0)
        else
            self.transform.localPosition = self.HIDE_POSITION
            self._newsTickerTextGO.transform.localPosition = self.HIDE_POSITION
            self._newsTickerContainer.transform.localScale = Vector2(0,0)
        end
        self._isShow = state
    end
end