CommonUIModule = {}
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function CommonUIModule.Init()
	--LayerUIFloat
	GUIManager.AddPanel(PanelsConfig.CommonUI, "Panel_CommonUI", GUILayer.LayerUITop, "Modules.ModuleCommonUI.CommonUIPanel", true, false)
end
return CommonUIModule