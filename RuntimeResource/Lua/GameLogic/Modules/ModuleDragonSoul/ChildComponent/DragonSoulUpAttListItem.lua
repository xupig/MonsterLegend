-- DragonSoulUpAttListItem.lua
local DragonSoulUpAttListItem = Class.DragonSoulUpAttListItem(ClassTypes.UIListItem)
DragonSoulUpAttListItem.interface = GameConfig.ComponentsConfig.Component
DragonSoulUpAttListItem.classPath = "Modules.ModuleDragonSoul.ChildComponent.DragonSoulUpAttListItem"

function DragonSoulUpAttListItem:Awake()
    self._full = self:FindChildGO("Container_content/Container_Full")
    self._item = self:FindChildGO("Container_content/Container_Item")
    self._textatt = self:GetChildComponent("Container_content/Container_Full/Text_Att","TextMeshWrapper")
    self._textname = self:GetChildComponent("Container_content/Container_Item/Text_Name","TextMeshWrapper")
    self._textcur = self:GetChildComponent("Container_content/Container_Item/Text_CurrAtt","TextMeshWrapper")
    self._textnext = self:GetChildComponent("Container_content/Container_Item/Text_NextAtt","TextMeshWrapper")
end

function DragonSoulUpAttListItem:OnDestroy()

end
--override
function DragonSoulUpAttListItem:OnRefreshData(data)
    local type=data[1]
    local d=data[2]
    if type==1 then
        self._full:SetActive(false)
        self._item:SetActive(true)
        self._textname.text=d[1]
        if d[4]==1 then
            self._textcur.text=d[2]
            self._textnext.text=d[3]
        else
            self._textcur.text=string.format("%.2f",d[2]/100).."%"
            self._textnext.text="+"..string.format("%.2f",d[3]/100).."%"
        end
    else
        self._full:SetActive(true)
        self._item:SetActive(false)
        if d[3]==1 then
            self._textatt.text=d[1]..":  "..d[2]
        else
            self._textatt.text=d1[1]..":  "..string.format("%.2f",d[2]/100).."%"
        end
    end
end
