-- DragonSouResBagListItem.lua
require "UIComponent.Extend.ItemGrid"
local DragonSouResBagListItem = Class.DragonSouResBagListItem(ClassTypes.UIListItem)
DragonSouResBagListItem.interface = GameConfig.ComponentsConfig.Component
DragonSouResBagListItem.classPath = "Modules.ModuleDragonSoul.ChildComponent.DragonSouResBagListItem"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local DragonSoulManager = PlayerManager.DragonSoulManager

function DragonSouResBagListItem:Awake()
    self:OnInit()
    self._imgsele = self:FindChildGO("Container_content/Image_Sele")
    self._imggou= self:FindChildGO("Container_content/Image_Gou")
    self._containerItem = self:FindChildGO("Container_content/Container_Item")
    self._textlv = self:GetChildComponent("Container_content/Text_Lv","TextMeshWrapper")
    self._textname = self:GetChildComponent("Container_content/Text_Name","TextMeshWrapper")
    local a = GUIManager.AddItem(self._containerItem, 1)
    self._item = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self:Setstate(false)
end

function DragonSouResBagListItem:OnDestroy()
   
end

function DragonSouResBagListItem:OnInit()
    self._prestate = false
    self._first = true
end

function DragonSouResBagListItem:Resovel()
    self._prestate=false
end

function DragonSouResBagListItem:GetRes()
    if self._prestate and self._data then
       return self._data[2]:GetResolve()
    end
end

function DragonSouResBagListItem:OnSele()
    self._prestate=not self._prestate
    self:Setstate(self._prestate)
end

function DragonSouResBagListItem:GetStatus()
    return self._prestate
end

function DragonSouResBagListItem:Setstate(state)
    self._first = false
    self._prestate = state
    -- self._imgsele:SetActive(state)
    self._imggou:SetActive(state)
end
--override
function DragonSouResBagListItem:OnRefreshData(data)
    self._item:SetValues({['noFrame']=true,['noAnimFrame']=true})
    if data==-1 then
        self._item:SetItem(nil)    
        self:Setstate(false)
        self._textlv.text = ""
        self._textname.text = ""
        return
    end
    self._data = data
    self._item:SetItem(data[2]._id)
    self._textlv.text = "Lv."..data[2]:GetLevel()
    self._textname.text = data[2]:GetName()
    if self._first then
        if data[2]:GetQuality()==1 or data[2]:GetQuality()==3 or data[2]:GetTypes()[1]==99 then
            self:Setstate(true)    
        else
            self:Setstate(false)
        end
    else
        self:Setstate(self._prestate)
    end
end


