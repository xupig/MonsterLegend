-- DragonSouBagListItem.lua
require "UIComponent.Extend.ItemGrid"
local DragonSouBagListItem = Class.DragonSouBagListItem(ClassTypes.UIListItem)
DragonSouBagListItem.interface = GameConfig.ComponentsConfig.PointableComponent
DragonSouBagListItem.classPath = "Modules.ModuleDragonSoul.ChildComponent.DragonSouBagListItem"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager

function DragonSouBagListItem:Awake()
    self._containerItem = self:FindChildGO("Container_content/Container_Item")
    local a = GUIManager.AddItem(self._containerItem, 1)
    self._item = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._textlv = self:GetChildComponent("Container_content/Text_Lv","TextMeshWrapper")
    self._textname = self:GetChildComponent("Container_content/Text_Name","TextMeshWrapper")
    self._lvTextGo = self:FindChildGO("Container_content/Text_Lv")
    self._nameTextGo = self:FindChildGO("Container_content/Text_Name")
end

function DragonSouBagListItem:OnDestroy()
   
end
--override
function DragonSouBagListItem:OnRefreshData(data)
    if data==-1 then
        self._item:SetValues({['noFrame']=true,['noAnimFrame']=true})
        self._item:SetItem()
        self._lvTextGo:SetActive(false)
        self._nameTextGo:SetActive(false)
        return
    end
    
    self._item:SetValues({['noFrame']=true,['noAnimFrame']=true})
    self._item:SetItem(data[2]._id)
    self._lvTextGo:SetActive(true)
    self._nameTextGo:SetActive(true)
    self._textlv.text = "Lv."..data[2]:GetLevel()
    self._textname.text = data[2]:GetName()
end


