-- DragonSoulSlotItem.lua
require "UIComponent.Extend.ItemGrid"
local DragonSoulSlotItem = Class.DragonSoulSlotItem(ClassTypes.BaseComponent)
DragonSoulSlotItem.interface = GameConfig.ComponentsConfig.PointableComponent
DragonSoulSlotItem.classPath = "Modules.ModuleDragonSoul.ChildComponent.DragonSoulSlotItem"
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local  DragonSoulManager =  PlayerManager.DragonSoulManager
local TipsManager = GameManager.TipsManager
local XGameObjectComplexIcon = GameMain.XGameObjectComplexIcon

function DragonSoulSlotItem:Awake()
    self._imdigos = {}
    self._imdigos[1]=self:FindChildGO("Image_Di1")
    self._imdigos[3]=self:FindChildGO("Image_Di3")
    self._imdigos[4]=self:FindChildGO("Image_Di4")
    self._imdigos[5]=self:FindChildGO("Image_Di5")
    self._imdigos[6]=self:FindChildGO("Image_Di6")
    self._redgo=self:FindChildGO("Image_Red")
    self._havego=self:FindChildGO("Container_Have")
    self._lockgo=self:FindChildGO("Container_Lock")
    self._slotName = self:GetChildComponent('Text_Name',"TextMeshWrapper")
    self._itemgo=self:FindChildGO("Container_Have/Container_Icon")
    self._complexIcon = self._itemgo:AddComponent(typeof(XGameObjectComplexIcon))
    self._textname = self:GetChildComponent("Container_Have/Text_Name","TextMeshWrapper")
    self._textlock = self:GetChildComponent("Container_Lock/Text_Lock","TextMeshWrapper")
    self._timerid = -1
    self._timer = function()self:OnTimer() end
end
function  DragonSoulSlotItem:OnTimer()
    TimerHeap:DelTimer(self._timerid)
    self._timerid=-1
    local buttonArgs = {}
    buttonArgs.promote = true --晋升
    buttonArgs.compose = true -- 合成
    buttonArgs.upgrade = true -- 升级
    buttonArgs.disassembly = true -- 拆解
    buttonArgs.unload = true -- 卸下
    buttonArgs.equip = false --佩戴
    buttonArgs.decompose = false -- 分解
    TipsManager:ShowDragonSoulTips(self._dragonsoulitemdata,buttonArgs,false,self._itemdata._slot)
end
function DragonSoulSlotItem:OnDestroy()

end

function DragonSoulSlotItem:OnSeleQ(q)
    for i,v in pairs(self._imdigos) do
        if tonumber(i)==q then
            v:SetActive(true)
        else
            v:SetActive(false)
        end
    end
end

function DragonSoulSlotItem:OnSeleInative()
    for i,v in pairs(self._imdigos) do
        v:SetActive(false)
    end
end

--override
function DragonSoulSlotItem:OnRefreshData(itemdata)
    self._itemdata = itemdata
    if itemdata:IsHave() then
        self._lockgo:SetActive(false)
        self._havego:SetActive(true)
        local item=itemdata:GetItem()
        self:OnSeleQ(item:GetQuality())
        self._dragonsoulitemdata = item
        self._textname.text=item:GetName()..'Lv.'..item:GetLevel()       
        if self._complexIcon then
            self._complexIcon:AddComplexIcon(ItemDataHelper.GetIcon(itemdata._id))
            self._complexIcon:StartPlay()
        end
        self._slotName.text = ''
        self._redgo:SetActive(DragonSoulManager:UpRedPoint(itemdata._id,item:GetLevel()))
    elseif itemdata:IsLock() then
        self._lockgo:SetActive(true)
        self._havego:SetActive(false)
        self:OnSeleInative()
        self._textlock.text = LanguageDataHelper.CreateContentWithArgs(632,{['0']=itemdata:GetOpenlevel()})
        self._slotName.text = ''
        self._redgo:SetActive(false)
    else
        if itemdata._slot==7 then
            self._redgo:SetActive(DragonSoulManager:DragonSoulBagType(2))
        else           
            self._redgo:SetActive(DragonSoulManager:DragonSoulBagType(1))
        end
        self:OnSeleInative()
        self._lockgo:SetActive(false)
        self._havego:SetActive(false)
        self._slotName.text = itemdata:GetSlotName()
    end
end

function DragonSoulSlotItem:OnPointerDown()
    if self._itemdata:IsHave() then
        if self._timerid ~= -1 then
            TimerHeap:DelTimer(self._timerid)
            self._timerid=-1
            DragonSoulManager.PutOffRpc(self._itemdata._slot)
        else
            self._timerid = TimerHeap:AddSecTimer(0.5,0,0,self._timer)
        end
    else
        -- EventDispatcher:TriggerEvent(GameEvents.DragonSoulUpView,{self._itemdata._slot,self._itemdata._id,self._itemdata._level})
    end
end

function DragonSoulSlotItem:OnPointerUp()
	-- EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SELECT_RUNE_HOLE,self.itemdata)
end