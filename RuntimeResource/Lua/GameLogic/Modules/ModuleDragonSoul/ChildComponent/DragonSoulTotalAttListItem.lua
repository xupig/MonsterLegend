-- DragonSoulTotalAttListItem.lua
local DragonSoulTotalAttListItem = Class.DragonSoulTotalAttListItem(ClassTypes.UIListItem)
DragonSoulTotalAttListItem.interface = GameConfig.ComponentsConfig.Component
DragonSoulTotalAttListItem.classPath = "Modules.ModuleDragonSoul.ChildComponent.DragonSoulTotalAttListItem"
local DragonSoulManager = PlayerManager.DragonSoulManager

function DragonSoulTotalAttListItem:Awake()
    self._textdesName = self:GetChildComponent("Container_content/Text_DesName","TextMeshWrapper")
    self._textdesNum = self:GetChildComponent("Container_content/Text_DesNum","TextMeshWrapper")
end

function DragonSoulTotalAttListItem:OnDestroy()
   
end

--override
function DragonSoulTotalAttListItem:OnRefreshData(data)
    local content,type = DragonSoulManager:GetAttType(data[1])    
    self._textdesName.text = content
    if type==1 then
        self._textdesNum.text = data[2]
    else
        self._textdesNum.text = string.format("%.2f",data[2]/100)..'%'
    end
end


