-- DragonSoulResListItem.lua
local DragonSoulResListItem = Class.DragonSoulResListItem(ClassTypes.UIListItem)
DragonSoulResListItem.interface = GameConfig.ComponentsConfig.Component
DragonSoulResListItem.classPath = "Modules.ModuleDragonSoul.ChildComponent.DragonSoulResListItem"
local ItemDataHelper = GameDataHelper.ItemDataHelper

function DragonSoulResListItem:Awake()
    self._textatt = self:GetChildComponent("Text_Att","TextMeshWrapper")
end

function DragonSoulResListItem:OnDestroy()

end
--override
function DragonSoulResListItem:OnRefreshData(data)
   self._textatt.text = ItemDataHelper.GetItemNameWithColor(data[1])..' X'..data[2]
end
