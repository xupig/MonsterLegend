--DragonSoulAttView.lua
require"Modules.ModuleDragonSoul.ChildComponent.DragonSoulTotalAttListItem"
local DragonSoulAttView = Class.DragonSoulAttView(ClassTypes.BaseComponent)
DragonSoulAttView.interface = GameConfig.ComponentsConfig.Component
DragonSoulAttView.classPath = "Modules.ModuleDragonSoul.ChildView.DragonSoulAttView"
local UIList = ClassTypes.UIList
local DragonSoulTotalAttListItem = ClassTypes.DragonSoulTotalAttListItem
local LanguageHelper = GameDataHelper.LanguageDataHelper
local DragonSoulData = PlayerManager.PlayerDataManager.dragonSoulData
local DragonSoulManager = PlayerManager.DragonSoulManager
local HidePosition = Vector3.New(0,4000,0)
function DragonSoulAttView:Awake()
    self._base.Awake(self)
    self._items={}
    self:InitView()
end

function DragonSoulAttView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(DragonSoulTotalAttListItem)
    self._list:SetPadding(10, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)
    self._btCloseGo = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btCloseGo,function() self:BtnCloseClick() end)
    self._title = self:FindChildGO("Text_Title")
end


function DragonSoulAttView:BtnCloseClick()
    self.gameObject.transform.anchoredPosition=HidePosition
end
function DragonSoulAttView:OnShow()
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulFx,false)    
   self:Update()
end

function DragonSoulAttView:OnClose()
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulFx,true)    
end

function DragonSoulAttView:Update()   
    local rawdata = DragonSoulData:UpdateDragonSoulAtts()
    local atts = DragonSoulManager:GetTotalAtts(rawdata)   
    self._list:RemoveAll()
    if table.isEmpty(atts) then
        self._title.text = LanguageDataHelper.GetContent(998)
        return
    end
    self._title.text = LanguageDataHelper.GetContent(998)
    local listItems = {}
    for i,v in pairs(atts) do
        table.insert(listItems,{tonumber(i),v})
    end
    self._list:SetDataList(listItems)
end

function DragonSoulAttView:OnDestory()
    self._csBH = nil
end