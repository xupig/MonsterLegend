-- DragonSoulUpView.lua
require "UIComponent.Extend.ItemGrid"
require"Modules.ModuleDragonSoul.ChildComponent.DragonSoulUpAttListItem"
local DragonSoulUpView = Class.DragonSoulUpView(ClassTypes.BaseComponent)
DragonSoulUpView.interface = GameConfig.ComponentsConfig.Component
DragonSoulUpView.classPath = "Modules.ModuleDragonSoul.ChildView.DragonSoulUpView"
local DragonSoulUpAttListItem = ClassTypes.DragonSoulUpAttListItem
local UIList = ClassTypes.UIList
local DragonSoulManager=PlayerManager.DragonSoulManager
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local DragonSoulData = PlayerManager.PlayerDataManager.dragonSoulData
local HidePosition = Vector3.New(0,4000,0)
local BaseUtil = GameUtil.BaseUtil
function DragonSoulUpView:Awake()
    self._update = function(errorid) self:Update(errorid)end
    self:InitView()
end

function DragonSoulUpView:InitView()
    self._btnUp=self:FindChildGO("Button_Up")
    local btnClose=self:FindChildGO("Button_Close")
    self._containerIcon=self:FindChildGO("Container_Icon")
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(DragonSoulUpAttListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:UseObjectPool()
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._csBH:AddClick(self._btnUp,function() self:BtnUpClick()end)
    self._csBH:AddClick(btnClose,function() self:BtnCloseClick()end)
   
    local a = GUIManager.AddItem(self._containerIcon, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._icon:ActivateTipsById()


    self._textUp = self:GetChildComponent("Text_Up", "TextMeshWrapper")
    self._textUp.text = LanguageDataHelper.GetContent(61011)

    local texttitle = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    texttitle.text = LanguageDataHelper.GetContent(61007)

    local textup = self:GetChildComponent("Button_Up/Text", "TextMeshWrapper")
    textup.text = LanguageDataHelper.GetContent(25231)

    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._textIconName = self:GetChildComponent("Text_IconName", "TextMeshWrapper")
    self._textleve = self:GetChildComponent("Text_Level", "TextMeshWrapper")

    self._containerCost=self:FindChildGO("Container_Cost/Container_Icon")
    self._textCost = self:GetChildComponent("Container_Cost/Text_Cost", "TextMeshWrapper")
    local texttips = self:GetChildComponent("Container_Cost/Text_Tips", "TextMeshWrapper")
    texttips.text = LanguageDataHelper.GetContent(548)
end

function DragonSoulUpView:BtnUpClick()
    DragonSoulManager.UPLevelRpc(self._slot)
end

function DragonSoulUpView:BtnCloseClick()
    self:OnClose()
    self.gameObject.transform.anchoredPosition=HidePosition
end
function DragonSoulUpView:OnShow(slot,id,level)
    self:AddEventListeners()
    self:UpDateView(slot,id,level)
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulFx,false)  
end

function DragonSoulUpView:CloseView()
end

function DragonSoulUpView:ShowView()
 
end
function DragonSoulUpView:UpDateView(slot,id,level)
    self._slot = slot
    self._id = id
    self._level = level
    local itemcur = DragonSoulData:GetDragonSoulUpItem(slot,id,level)
    local i=itemcur._item
    local attscur = i:GetAtt()
    local isfull=i:IsFulllevel()
    local itemnext={}
    local items = {}
    if not isfull then
        itemnext = DragonSoulData:GetDragonSoulUpItem(slot,id,level+1)
        local attsnext = itemnext._item:GetAtt()
        local des = DragonSoulManager:GetAttDes(attscur,attsnext)
        for k,v in ipairs(des) do
            items[k]={1,v}
        end
        self._textUp.text = LanguageDataHelper.GetContent(61011)
        local c = i:GetUpcost()
        local id,num = next(c)
        local icon = ItemDataHelper.GetIcon(id)
        GameWorld.AddIcon(self._containerCost,icon)
        local have = GameWorld.Player().money_dragon_piece or 0
        if num<=have then
            self._textCost.text = BaseUtil.GetColorString(num,ColorConfig.J).."/"..have
        else
            self._textCost.text = BaseUtil.GetColorString(num,ColorConfig.H).."/"..have
        end
    else
        local des = DragonSoulManager:GetAttInfo(attscur)
        for k,v in ipairs(des) do
            items[k]={2,v}
        end
        self._textUp.text = LanguageDataHelper.GetContent(75527)
    end
    self._textName.text = i:GetName()..' LV'..i:GetLevel()
    self._list:SetDataList(items)
    self._icon:SetItem(itemcur._id)
    self._textIconName.text = i:GetName()
    self._textleve.text = ' LV'..i:GetLevel()
end
function DragonSoulUpView:OnClose()
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulFx,true)    
    self:RemoveEventListeners()
end

function DragonSoulUpView:Update(errorid)
    if errorid==0 then
        self:UpDateView(self._slot,self._id,self._level+1)
    end
end

function DragonSoulUpView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.DragonSoulUp,self._update)
end

function DragonSoulUpView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.DragonSoulUp,self._update)   
end
function DragonSoulUpView:OnDestory()
    self._csBH = nil
end