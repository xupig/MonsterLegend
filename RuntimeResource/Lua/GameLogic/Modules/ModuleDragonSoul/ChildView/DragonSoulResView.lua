--DragonSoulResView.lua
require "Modules.ModuleDragonSoul.ChildComponent.DragonSouResBagListItem"
local DragonSoulResView = Class.DragonSoulResView(ClassTypes.BaseComponent)
local DragonSouResBagListItem = ClassTypes.DragonSouResBagListItem
DragonSoulResView.interface = GameConfig.ComponentsConfig.Component
DragonSoulResView.classPath = "Modules.ModuleDragonSoul.ChildView.DragonSoulResView"
local DragonSoulData =  PlayerManager.PlayerDataManager.dragonSoulData
local UIList = ClassTypes.UIList
local UIToggle = ClassTypes.UIToggle
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local HidePosition = Vector3.New(0,4000,0)
local DragonSoulManager = PlayerManager.DragonSoulManager
function DragonSoulResView:Awake()
    self:InitView()
end

function DragonSoulResView:InitView()  
    self._countRes = 0
    self._countAddRes = 0
    local btnClose = self:FindChildGO("Button_Close")
    local btnOneRes = self:FindChildGO("Button_OneResolve")
    self._iconGet = self:FindChildGO("Container_Get/Image_Icon")
    self._csBH:AddClick(btnClose,function() self:BtnCloseClick()end)
    self._csBH:AddClick(btnOneRes,function() self:BtnOneResClick()end)

    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'ScrollViewList')
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(DragonSouResBagListItem)
    self._list:SetPadding(10,0,0,30)
    self._list:SetGap(10,20)
    self._list:SetDirection(UIList.DirectionLeftToRight,7,-1)
    local itemClickedCB = 
	function(index)
		self:OnListItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)
    self._list:SetOnAllItemCreatedCB(function() self:OnContentListAllItemCreated() end)

    local textTitle = self:GetChildComponent("Text_Title","TextMeshWrapper")
    textTitle.text=LanguageDataHelper.GetContent(61008)

    local textGet = self:GetChildComponent("Container_Get/Text_Get","TextMeshWrapper")
    textGet.text=LanguageDataHelper.GetContent(61009)

    self._textNum = self:GetChildComponent("Container_Get/Text_Num","TextMeshWrapper")

    local textRes = self:GetChildComponent("Button_OneResolve/Text","TextMeshWrapper")
    textRes.text=LanguageDataHelper.GetContent(25238)

    self._toggleR = UIToggle.AddToggle(self.gameObject,"Toggle5")
    self._toggleOr = UIToggle.AddToggle(self.gameObject,"Toggle4")
    self._toggleZi = UIToggle.AddToggle(self.gameObject,"Toggle3")
    self._toggleB = UIToggle.AddToggle(self.gameObject,"Toggle2")
    self._toggleW = UIToggle.AddToggle(self.gameObject,"Toggle1")
    self._toggleOr:SetOnValueChangedCB(function(stage) self:OnToggleClick4(stage)end)
    self._toggleZi:SetOnValueChangedCB(function(stage) self:OnToggleClick3(stage)end)    
    self._toggleB:SetOnValueChangedCB(function(stage) self:OnToggleClick2(stage)end)
    self._toggleW:SetOnValueChangedCB(function(stage) self:OnToggleClick1(stage)end) 
    self._toggleR:SetOnValueChangedCB(function(stage) self:OnToggleClick5(stage)end) 
    self._toggleOr:SetIsOn(false) 
    self._toggleZi:SetIsOn(false) 
    self._toggleB:SetIsOn(true) 
    self._toggleW:SetIsOn(true) 
    self._toggleR:SetIsOn(false) 

    self._dragonBagUpdate = function() self:UpdateByBag()end
    self._dragonResUpdate = function(errorid) self:UpdateByRes(errorid)end
    GameWorld.AddIcon(self._iconGet,ItemDataHelper.GetIcon(public_config.MONTY_TYPE_DRAGON_PICEC))
    self._scrollview:SetScrollRectState(false)
end

function DragonSoulResView:OnToggleClick1(stage)
    self:SetListItemstate(1,stage)
    self:UpdateResGet()   
end

function DragonSoulResView:OnToggleClick2(stage)
    self:SetListItemstate(3,stage)
    self:UpdateResGet()   
end

function DragonSoulResView:OnToggleClick3(stage)
    self:SetListItemstate(4,stage)
    self:UpdateResGet()   
end

function DragonSoulResView:OnToggleClick4(stage)
    self:SetListItemstate(5,stage)    
    self:UpdateResGet()   
end

function DragonSoulResView:OnToggleClick5(stage)
    self:SetListItemstate(6,stage) 
    self:UpdateResGet()   
end

function DragonSoulResView:UpdateResGet()
    local getid = 0
    local getnum = 0
    local get = {}
    for idx=0,self._list:GetLength()-1 do
        local item = self._list:GetItem(idx)
        if item then
            local issele = item:GetStatus()
            if issele then
                local d = self._list:GetDataByIndex(idx)
                if d and d~=-1 and d[2] then
                    local res = d[2]:GetResolve()
                    if res then
                        for k,v in pairs(res) do
                            getid = k
                            getnum = v
                        end
                    end
                    if not get[getid] then
                        get[getid]=getnum
                    else
                        get[getid]=get[getid]+getnum
                    end
                end     
            end       
        end
    end
    if table.isEmpty(get) then
        self._textNum.text = 'x0'
        return
    end
    self._textNum.text = 'x'..get[getid]
end
function DragonSoulResView:SetListItemstate(quanlity,stage)
    for idx=0,self._list:GetLength()-1 do
        local data = self._list:GetDataByIndex(idx)
        local item = self._list:GetItem(idx)
        if data ~= -1 and item and data[2]:GetQuality()==quanlity then
            item:Setstate(stage)
        end
    end

end
function DragonSoulResView:OnListItemClicked(idx)
    local data = self._list:GetDataByIndex(idx)
    if data==-1 then
        return
    end
    local item = self._list:GetItem(idx)
    item:OnSele()
    self:UpdateResGet()
end

function DragonSoulResView:BtnCloseClick()
    self:OnClose()
    self.transform.anchoredPosition=HidePosition
end

function DragonSoulResView:BtnOneResClick()
    self._countRes = 0
    DragonSoulManager:RefrenDragonpiece()
    for idx=0,self._list:GetLength()-1 do
        local item = self._list:GetItem(idx)
        if item and item:GetStatus() then
            self._countRes = self._countRes+1
            local itemdata = self._list:GetDataByIndex(idx) 
            if itemdata and itemdata~=-1 then
                DragonSoulManager:IsResolveNow(true)
                DragonSoulManager.PutResolveRpc(itemdata[1])
                item:Resovel()
            end          
        end
    end
end
function DragonSoulResView:OnShow()
    self._scrollview:SetScrollRectState(true)
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulFx,false)  
    self:AddEventListeners()
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT_BAG, self._dragonBagUpdate)
    local d = DragonSoulData:UpdateDragonSoulResBag()
    -- self._list:SetDataList(d)
    self._list:SetDataList(d, 4)--使用协程加载
    self._toggleW:SetIsOn(true) 
    self._toggleB:SetIsOn(true) 

    local  num = DragonSoulData:GetResNum()
    self._textNum.text = 'x'..num
end

--全部list加载完回调
function DragonSoulResView:OnContentListAllItemCreated()
    self:OnSelect()
end

function DragonSoulResView:OnClose()
    self._scrollview:SetScrollRectState(false)
    for idx=0,self._list:GetLength()-1 do
        local item = self._list:GetItem(idx)
        if item then
            item:OnInit()
        end
    end
    DragonSoulManager:IsResolveNow(false)
    self:RemoveEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulFx,true)    
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT_BAG, self._dragonBagUpdate)	
end

function DragonSoulResView:OnSelect()
    for idx=0,self._list:GetLength()-1 do
        local item = self._list:GetItem(idx)
        if item then
            local itemdata = self._list:GetDataByIndex(idx) 
            if itemdata and itemdata~=-1 and itemdata[2] then
                local q = itemdata[2]:GetQuality()
                if q==3 or q==1 or itemdata[2]:GetTypes()[1]==99 then
                    item:Setstate(true)
                end
            end           
        end
    end	
end

function DragonSoulResView:UpdateByBag()
    if self._countRes==0 then
        return
    end
    self:Updata()
    self:UpdateResGet()
end

function DragonSoulResView:UpdateByRes(errorid)
    if errorid==0 then
        self._countAddRes = self._countAddRes+1
        if self._countAddRes>= self._countRes then
            self._countRes=0
            self._countAddRes=0
            self:Updata()
            DragonSoulManager:UpdataPieceFloat()
        end
    else
        self._countRes=0
        self._countAddRes=0
        self:Updata()
        DragonSoulManager:UpdataPieceFloat()
    end
end

function DragonSoulResView:Updata()
    local d = DragonSoulData:UpdateDragonSoulResBag()
    self._list:SetDataList(d)
end
function DragonSoulResView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.DragonSoulRes,self._dragonResUpdate)
end

function DragonSoulResView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.DragonSoulRes,self._dragonResUpdate)      
end

function DragonSoulResView:OnDestory()
end