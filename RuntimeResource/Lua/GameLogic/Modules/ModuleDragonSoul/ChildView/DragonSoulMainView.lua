--DragonSoulMainView.lua
require "UIComponent.Extend.ItemGrid"
require"Modules.ModuleDragonSoul.ChildComponent.DragonSouBagListItem"
require"Modules.ModuleDragonSoul.ChildComponent.DragonSoulSlotItem"
local DragonSoulMainView = Class.DragonSoulMainView(ClassTypes.BaseComponent)
DragonSoulMainView.interface = GameConfig.ComponentsConfig.Component
DragonSoulMainView.classPath = "Modules.ModuleDragonSoul.ChildView.DragonSoulMainView"
local DragonSouBagListItem = ClassTypes.DragonSouBagListItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local UIList = ClassTypes.UIList
local CardManager=PlayerManager.CardManager
local ItemGrid = ClassTypes.ItemGrid
local DragonSoulSlotItem = ClassTypes.DragonSoulSlotItem
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local DragonSoulData = PlayerManager.PlayerDataManager.dragonSoulData
local  DragonSoulManager = PlayerManager.DragonSoulManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TipsManager = GameManager.TipsManager
local DragonsoulDataHelper = GameDataHelper.DragonsoulDataHelper
function DragonSoulMainView:Awake()
    self._items={}
    -- self._update = function() self:UpdateView()end
    self._dragonPieceUpdate = function() self:UpdatePiece()end
    self._updateStoneCrystal = function() self:UpdateStoneCrystal()end
    self._dragonBagUpdate = function () self:UpdateBag() end
    self._dragonSlotUpdate = function () self:UpdateSlot() end
    self:InitView()
end

function DragonSoulMainView:InitView()
    self._btnAtt=self:FindChildGO("Button_Att")
    self._btnCom=self:FindChildGO("Button_Component")
    self._btnRes=self:FindChildGO("Button_Resovle")
    self._containers={}
    self._containers[1]=self:FindChildGO("Container1")
    self._containers[2]=self:FindChildGO("Container2")
    self._containers[3]=self:FindChildGO("Container3")
    self._containers[4]=self:FindChildGO("Container4")
    self._containers[5]=self:FindChildGO("Container5")
    self._containers[6]=self:FindChildGO("Container6")
    self._containers[7]=self:FindChildGO("Container7")

    self._containerCost1=self:FindChildGO("Container_Cost1/Container_Icon")
    self._containerCost2=self:FindChildGO("Container_Cost2/Container_Icon")
    self._containerCost3=self:FindChildGO("Container_Cost3/Container_Icon")
    self._textCost1 = self:GetChildComponent("Container_Cost1/Text_Cost", "TextMeshWrapper")
    self._textCost2 = self:GetChildComponent("Container_Cost2/Text_Cost", "TextMeshWrapper")
    self._textCost3 = self:GetChildComponent("Container_Cost3/Text_Cost", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(DragonSouBagListItem)
    self._list:SetPadding(15, 0, 0, 20)
    self._list:SetGap(0, 5)
    self._list:SetDirection(UIList.DirectionLeftToRight, 5, -1)
    local itemClickedCB = 
	function(index)
		self:OnListItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._items[1]=self:AddChildLuaUIComponent("Container1",DragonSoulSlotItem)
    self._items[2]=self:AddChildLuaUIComponent("Container2",DragonSoulSlotItem)
    self._items[3]=self:AddChildLuaUIComponent("Container3",DragonSoulSlotItem)
    self._items[4]=self:AddChildLuaUIComponent("Container4",DragonSoulSlotItem)
    self._items[5]=self:AddChildLuaUIComponent("Container5",DragonSoulSlotItem)
    self._items[6]=self:AddChildLuaUIComponent("Container6",DragonSoulSlotItem)
    self._items[7]=self:AddChildLuaUIComponent("Container7",DragonSoulSlotItem)

    self._csBH:AddClick(self._btnAtt,function() self:BtnAttClick()end)
    self._csBH:AddClick(self._btnCom,function() self:BtnComClick()end)
    self._csBH:AddClick(self._btnRes,function() self:BtnResClick()end)
   

    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._textName.text = LanguageDataHelper.GetContent(61001)
    self._textTips = self:GetChildComponent("Text_Tips", "TextMeshWrapper")
    self._textTips.text = LanguageDataHelper.GetContent(61003)

    -- local _textres = self:GetChildComponent("Button_Resovle/Text", "TextMeshWrapper")
    -- _textres.text = LanguageDataHelper.GetContent(25238)

    -- local _textcom = self:GetChildComponent("Button_Component/Text", "TextMeshWrapper")
    -- _textcom.text = LanguageDataHelper.GetContent(61004)

    -- local _textatt = self:GetChildComponent("Button_Att/Text", "TextMeshWrapper")
    -- _textatt.text = LanguageDataHelper.GetContent(61002)

    self._timerid = -1
    self._preslot = -1
    self._timer = function()self:OnTimer() end

    self._redResRed=self:FindChildGO("Button_Resovle/Image_Red")
    self._redComonent=self:FindChildGO("Button_Component/Image_Red")
    self._redComonent:SetActive(false)
    self:ActiveScro(false)
end

function DragonSoulMainView:BtnAttClick()      
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulAtt)     
end

function DragonSoulMainView:BtnComClick()
    local data={}
    data.firstTabIndex=3
    GUIManager.ClosePanel(PanelsConfig.DragonSoul)
    GUIManager.ShowPanel(PanelsConfig.Compose,data)
end

function DragonSoulMainView:OnListItemClicked(idx)
    local data = self._list:GetDataByIndex(idx)
    self._seledata = data
    if data==-1 then return end
    self._dragonsoulitemdata = data[2]
    if self._preslot~=-1 and data[1]~=self._preslot then
        if self._timerid==-1 then
            self._preslot = data[1]
            self._timerid = TimerHeap:AddSecTimer(0.25,0,0,self._timer)
        else
            TimerHeap:DelTimer(self._timerid)
            self._timerid=-1
            self._preslot = -1
        end
    elseif self._preslot==-1 then
        self._preslot = data[1]
        self._timerid = TimerHeap:AddSecTimer(0.25,0,0,self._timer)
    else
        if self._timerid==-1 then
            self._preslot = -1
        else
            if data[1]==self._preslot then
                if self._timerid~=-1 then
                    TimerHeap:DelTimer(self._timerid)
                    self._timerid=-1
                    self._preslot = -1
                    DragonSoulManager.PutOnRpc(data[1])
                    return
                end      
            end
            self._timerid = TimerHeap:AddSecTimer(0.25,0,0,self._timer)
            self._preslot = data[1]                
        end
    end
end

function  DragonSoulMainView:OnTimer()
    TimerHeap:DelTimer(self._timerid)
    local buttonArgs = {}
    buttonArgs.promote = true --晋升
    buttonArgs.compose = true -- 合成
    buttonArgs.upgrade = false -- 升级
    buttonArgs.disassembly = true -- 拆解
    buttonArgs.unload = false -- 卸下
    buttonArgs.equip = true --佩戴
    buttonArgs.decompose = true -- 分解
    TipsManager:ShowDragonSoulTips(self._dragonsoulitemdata,buttonArgs,true,self._seledata[1])
    self._timerid=-1
    self._preslot = -1

end

function DragonSoulMainView:BtnResClick()
    if DragonSoulManager.IsOnlyElite() then
        local bag = GameWorld.Player().dragon_spirit_bag or {}
        for k, v in pairs(bag) do
            local t=DragonsoulDataHelper:GetType(v[1])
            if t[1]==99 then
                DragonSoulManager.PutResolveRpc(k)
            end
        end
        return 
    end
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulView,3)    
end

-- function DragonSoulMainView:BtnCloseClick()
--     self:Close()
--     self.gameObject:SetActive(false)
-- end

function DragonSoulMainView:OnShow()
    self:ActiveScro(true)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_UPDATE, self._updateStoneCrystal)
    EventDispatcher:AddEventListener(GameEvents.DrgonSoulSlotUpdate,self._dragonSlotUpdate)  
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEG_DRAGON_PIECE, self._dragonPieceUpdate)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT, self._dragonSlotUpdate)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT_BAG, self._dragonBagUpdate)

    self:UpdateBag()
    self:UpdateSlot()
    self:UpdatePiece()

    self:UpdateStoneCrystal()
end
-- function DragonSoulMainView:UpdateView(data)

-- end
function DragonSoulMainView:ActiveScro(state)
    if self._listView then
        self._list:SetListItemRenderingAgent(state)
        self._listView:SetScrollRectState(state)
    end
end

function DragonSoulMainView:UpdateStoneCrystal()
    local c=DragonSoulData:GetCost2()
    local d1 = c[1]
    local d2 = c[2]
    self:UpdateStone(d1)
    self:UpdateCrystal(d2)
end
function DragonSoulMainView:UpdatePiece()
    local c=DragonSoulData:GetCost1()
    local icon = ItemDataHelper.GetIcon(c[2])
    GameWorld.AddIcon(self._containerCost1,icon)
    self._textCost1.text = c[1]
end

function DragonSoulMainView:UpdateStone(d)
    local icon = ItemDataHelper.GetIcon(d[2])
    GameWorld.AddIcon(self._containerCost2,icon)
    self._textCost2.text = d[1]
end

function DragonSoulMainView:UpdateCrystal(d)
    local icon = ItemDataHelper.GetIcon(d[2])
    GameWorld.AddIcon(self._containerCost3,icon)
    self._textCost3.text = d[1]
end

function DragonSoulMainView:UpdateSlot()
    local d = DragonSoulData:UpdateDragonSoulSlot()
    for k,v in ipairs(d) do
        self._items[k]:OnRefreshData(v)
    end
end

function DragonSoulMainView:UpdateBag()
    local bag = GameWorld.Player().dragon_spirit_bag
    self._textName.text = LanguageDataHelper.GetContent(61001)..'('..self:baglen(bag)..'/'..GlobalParamsHelper.GetParamValue(724)..")"
    
    local d = DragonSoulData:UpdateDragonSoulBag()
    if self._list then
        --self._list:SetDataList(d)
        self._list:SetOnAllItemCreatedCB(function() self:OnContentListAllItemCreated() end)
        self._list:SetDataList(d, 5)--使用协程加载
    end
end

function DragonSoulMainView:OnContentListAllItemCreated()
    self._redResRed:SetActive(DragonSoulManager:RefreshResolvetRedPoint())   
end

function DragonSoulMainView:baglen(bag)
    local x = 0
    for i,v in pairs(bag) do
        x=x+1
    end
    return x 
 end

function DragonSoulMainView:OnClose()
    self:ActiveScro(false)
    EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_UPDATE, self._updateStoneCrystal)
    EventDispatcher:RemoveEventListener(GameEvents.DrgonSoulSlotUpdate,self._dragonSlotUpdate)  
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEG_DRAGON_PIECE, self._dragonPieceUpdate)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT, self._dragonSlotUpdate)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT_BAG, self._dragonBagUpdate)
end


function DragonSoulMainView:OnDestory()
    self._csBH = nil
end