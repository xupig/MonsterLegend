--DragonSoulDisassembleView.lua
require "Modules.ModuleDragonSoul.ChildComponent.DragonSoulResListItem"
require "UIComponent.Extend.ItemGrid"
local DragonSoulDisassembleView = Class.DragonSoulDisassembleView(ClassTypes.BaseComponent)
local DragonSoulResListItem = ClassTypes.DragonSoulResListItem
DragonSoulDisassembleView.interface = GameConfig.ComponentsConfig.Component
DragonSoulDisassembleView.classPath = "Modules.ModuleDragonSoul.ChildView.DragonSoulDisassembleView"
local UIList = ClassTypes.UIList
local DragonSoulData = PlayerManager.PlayerDataManager.dragonSoulData
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemGrid = ClassTypes.ItemGrid
local DragonSoulManager = PlayerManager.DragonSoulManager
local HidePosition = Vector3.New(0,4000,0)
function DragonSoulDisassembleView:Awake()
    self._update = function() self:Update()end
    self:InitView()

end
function DragonSoulDisassembleView:InitView() 
    local btnClose=self:FindChildGO("Button_Close") 
    self._csBH:AddClick(btnClose,function() self:BtnCloseClick()end)

    local btnDis=self:FindChildGO("Button_Disassemble") 
    self._csBH:AddClick(btnDis,function() self:BtnDisClick()end)

    self._containerIcon=self:FindChildGO("Container_Icon")

    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'ScrollViewList')
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(DragonSoulResListItem)
    self._list:SetPadding(0,0,0,0)
    self._list:SetGap(0,0)
    self._list:SetDirection(UIList.DirectionTopToDown,1,-1)


    local a = GUIManager.AddItem(self._containerIcon, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._icon:ActivateTipsById()


    local textUp = self:GetChildComponent("Text_Up", "TextMeshWrapper")
    textUp.text = LanguageDataHelper.GetContent(61006)

    local texttitle = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    texttitle.text = LanguageDataHelper.GetContent(61005)

    local textBtnDis = self:GetChildComponent("Button_Disassemble/Text", "TextMeshWrapper")
    textBtnDis.text = LanguageDataHelper.GetContent(61010)

    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._textIconName = self:GetChildComponent("Text_IconName", "TextMeshWrapper")
    self._textleve = self:GetChildComponent("Text_Level", "TextMeshWrapper")
end

function DragonSoulDisassembleView:BtnCloseClick()
    self:OnClose()
    self.gameObject.transform.anchoredPosition=HidePosition
end

function DragonSoulDisassembleView:BtnDisClick()
    DragonSoulManager.PutRestoreRpc(self._slot,self._distype)
end

function DragonSoulDisassembleView:OnShow(slot,id,level,distype)
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulFx,false)  
    self:AddEventListeners()
    self._slot = slot
    self._distype = distype
    local item = DragonSoulData:GetDragonSoulItem(id,level)
    local listitems = {}
    local get = item:IsDissGet()
    local cost = get[1]
    local dragons = get[2]
    if cost then
        for k,v in pairs(cost) do
            table.insert(listitems,{tonumber(k),v})
        end
    end
    if dragons then
        for k,v in pairs(dragons) do
            table.insert(listitems,{tonumber(k),v})
        end
    end
    self._list:SetDataList(listitems)
    self._textName.text = item:GetName()..' LV'..item:GetLevel()
    self._icon:SetItem(item._id)
    self._textIconName.text = item:GetName()
    self._textleve.text = ' LV'..item:GetLevel()
end

function DragonSoulDisassembleView:OnClose()
    EventDispatcher:TriggerEvent(GameEvents.DragonSoulFx,true)    
    self:RemoveEventListeners()
end

function DragonSoulDisassembleView:Update()
    self.gameObject.transform.anchoredPosition=HidePosition
end

function DragonSoulDisassembleView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.DragonSoulDis,self._update)
end

function DragonSoulDisassembleView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.DragonSoulDis,self._update)      
end

function DragonSoulDisassembleView:OnDestory()
    self._csBH = nil
end