DragonSoulModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function DragonSoulModule.Init()
    GUIManager.AddPanel(PanelsConfig.DragonSoul,"Panel_DragonSoul",GUILayer.LayerUIPanel,"Modules.ModuleDragonSoul.DragonSoulPanel",true,false,nil,true)
end

return DragonSoulModule