-- DragonSoulPanel.lua
require "Modules.ModuleDragonSoul.ChildView.DragonSoulUpView"
require "Modules.ModuleDragonSoul.ChildView.DragonSoulMainView"
require "Modules.ModuleDragonSoul.ChildView.DragonSoulDisassembleView"
require "Modules.ModuleDragonSoul.ChildView.DragonSoulResView"
require "Modules.ModuleDragonSoul.ChildView.DragonSoulAttView"

local DragonSoulPanel = Class.DragonSoulPanel(ClassTypes.BasePanel)
local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local DragonSoulUpView = ClassTypes.DragonSoulUpView
local DragonSoulDisassembleView = ClassTypes.DragonSoulDisassembleView
local DragonSoulResView = ClassTypes.DragonSoulResView
local DragonSoulAttView = ClassTypes.DragonSoulAttView
local DragonSoulMainView = ClassTypes.DragonSoulMainView
local HidePosition = Vector3.New(0,4000,0)
local ShowPosition = Vector3.New(0,0,0)
local ShowAttPosition = Vector3.New(422,84,0)
local UIParticle = ClassTypes.UIParticle
function DragonSoulPanel:Awake()
    self:InitViews()
    self._viewsele=function(index) self:ViewSele(index)end
    self._Upviewsele = function(data) self:UpViewSele(data)end
    self._Disviewsele = function(data) self:DisViewSele(data)end
    self._attclick=function() self:onAttClick()end
    self._dragonFx=function(isplay) self:OnPlayFx(isplay)end
end

function DragonSoulPanel:OnPlayFx(isplay)
    if isplay then
        self.fxtarget:Play(true,true)
    else
        self.fxtarget:Stop()
    end
end

function DragonSoulPanel:InitViews()
    self._views={}
    self._viewsgo={}
    self._containerMainGo = self:FindChildGO('Container_Main')
    self._containerMain = self:AddChildLuaUIComponent('Container_Main',DragonSoulMainView)
    self._containerUpGo = self:FindChildGO('Container_Up')
    self._containerUp = self:AddChildLuaUIComponent('Container_Up',DragonSoulUpView)
    self._viewsgo[1] = self._containerUpGo
    self._views[1] = self._containerUp

    self._containerDisGo = self:FindChildGO('Container_Disassemble')
    self._containerDis = self:AddChildLuaUIComponent('Container_Disassemble',DragonSoulDisassembleView)
    self._viewsgo[2] = self._containerDisGo
    self._views[2] = self._containerDis
    
    self._containerResGo = self:FindChildGO('Container_Res')
    self._containerRes = self:AddChildLuaUIComponent('Container_Res',DragonSoulResView)
    self._viewsgo[3] = self._containerResGo
    self._views[3] = self._containerRes

    self._containerAttGo = self:FindChildGO('Container_AttAll')
    self._containerAtt = self:AddChildLuaUIComponent('Container_AttAll',DragonSoulAttView)

    self._containerUpGo.transform.anchoredPosition=HidePosition
    self._containerDisGo.transform.anchoredPosition=HidePosition
    self._containerResGo.transform.anchoredPosition=HidePosition
    self._containerAttGo.transform.anchoredPosition=HidePosition
    self.fxtarget = UIParticle.AddParticle(self.gameObject,"Container_Fx/fx_ui_30025")
    self.fxtarget:Play(true,true)
end

function DragonSoulPanel:ViewSele(index)
    for k in ipairs(self._views) do
        if tonumber(k)==index then
            if not self._viewsgo[index].activeSelf then
                self._viewsgo[index]:SetActive(true)
            end
            self._viewsgo[index].transform.anchoredPosition=ShowPosition
            self._views[index]:OnShow()
        else
            self._viewsgo[k].transform.anchoredPosition=HidePosition
            self._views[k]:OnClose()
        end
    end
end

function DragonSoulPanel:UpViewSele(data)
    if not self._viewsgo[1].activeSelf then
        self._viewsgo[1]:SetActive(true)
    end
    self._viewsgo[1].transform.anchoredPosition=ShowPosition
    self._views[1]:OnShow(data[1],data[2],data[3])
end

function DragonSoulPanel:DisViewSele(data)
    if not self._viewsgo[2].activeSelf then
        self._viewsgo[2]:SetActive(true)
    end
    self._viewsgo[2].transform.anchoredPosition=ShowPosition
    self._views[2]:OnShow(data[1],data[2],data[3],data[4])
end

function DragonSoulPanel:onAttClick()
    if not self._containerAttGo.activeSelf then
        self._containerAttGo:SetActive(true)
    end
    self._containerAttGo.transform.anchoredPosition=ShowAttPosition
    self._containerAtt:OnShow()
end

function DragonSoulPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.DragonSoulView, self._viewsele)
    EventDispatcher:AddEventListener(GameEvents.DragonSoulDisView, self._Disviewsele)
    EventDispatcher:AddEventListener(GameEvents.DragonSoulUpView, self._Upviewsele)
    EventDispatcher:AddEventListener(GameEvents.DragonSoulAtt, self._attclick)
    EventDispatcher:AddEventListener(GameEvents.DragonSoulFx, self._dragonFx)
end

function DragonSoulPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.DragonSoulDisView, self._Disviewsele)
    EventDispatcher:RemoveEventListener(GameEvents.DragonSoulView, self._viewsele)
    EventDispatcher:RemoveEventListener(GameEvents.DragonSoulAtt, self._attclick)
    EventDispatcher:RemoveEventListener(GameEvents.DragonSoulUpView, self._Upviewsele)
    EventDispatcher:RemoveEventListener(GameEvents.DragonSoulFx, self._dragonFx)

end
    
function DragonSoulPanel:OnDestroy()
end

function DragonSoulPanel:OnShow(data)
    self.fxtarget:Play(true,true)
    self._containerMain:OnShow()
    self:AddEventListeners()
end

function DragonSoulPanel:OnClose()
    self.fxtarget:Stop()
    self._containerMain:OnClose()
    self:RemoveEventListeners()
end

-- -- 0 没有  1 暗红底板  2 蓝色底板  3 遮罩底板
function DragonSoulPanel:CommonBGType()
    return PanelBGType.NoBG
end

function DragonSoulPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--关闭的时候重新开启二级主菜单
function DragonSoulPanel:ReopenSubMainPanel()
    return true
end