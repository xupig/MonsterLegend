local SelectRoleItem = Class.SelectRoleItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
SelectRoleItem.interface = GameConfig.ComponentsConfig.PointableComponent
SelectRoleItem.classPath = "Modules.ModuleSelectRole.ChildComponent.SelectRoleItem"

local public_config = GameWorld.public_config
local RoleDataHelper = GameDataHelper.RoleDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local StringStyleUtil = GameUtil.StringStyleUtil

local Empty_Icon = 1250

function SelectRoleItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function SelectRoleItem:OnDestroy()
    self.data = nil
end

function SelectRoleItem:InitView()
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._selectedImage = self:FindChildGO("Image_Selected")
    self._levelText = self:GetChildComponent("Text_Level","TextMeshWrapper")
end

--override  角色数据{dbid，....}   dbid == 0 表示未有角色数据
function SelectRoleItem:OnRefreshData(data)
    self.data = data
    if data[public_config.CHARACTER_KEY_DBID] ~= 0 then
        local iconId = RoleDataHelper.GetIconIdByVocation(data[public_config.CHARACTER_KEY_VOCATION])
        GameWorld.AddIcon(self._iconContainer, iconId)
        local levelStr = StringStyleUtil.GetLevelString(data[public_config.CHARACTER_KEY_LEVEL],nil,true).. LanguageDataHelper.GetContent(69)
        self._levelText.text = levelStr
    else
        local iconId = Empty_Icon
        GameWorld.AddIcon(self._iconContainer, iconId)
        self._levelText.text = ""
    end
end

function SelectRoleItem:ShowSelected(state)
    self._selectedImage:SetActive(state)
    if self.data ~= nil and state then
        if self.data[public_config.CHARACTER_KEY_DBID] == 0 then
            self:OnAddRole()
        else
            self:OnSelectRole()
        end
    end
end

function SelectRoleItem:OnAddRole()
    local selected = CreateRoleManager.CurrentSelectedCharacter
    CreateRoleManager.HeadId[selected] = nil
    CreateRoleManager.ClothId[selected] = nil
    CreateRoleManager.CurrentSelectedCharacter = self:GetIndex() + 1
    CreateRoleManager:DestroyEntity()

    GUIManager.ShowPanel(PanelsConfig.CreateRole)
    GUIManager.ClosePanel(PanelsConfig.SelectRole)
end

function SelectRoleItem:OnSelectRole()
    CreateRoleManager.CurrentSelectedCharacter = self:GetIndex() + 1
end