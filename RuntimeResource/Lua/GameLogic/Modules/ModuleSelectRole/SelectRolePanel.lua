require "Modules.ModuleSelectRole.ChildComponent.SelectRoleItem"

local SelectRolePanel = Class.SelectRolePanel(ClassTypes.BasePanel)

local LuaUIRotateModel = GameMain.LuaUIRotateModel
local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local MaxButtonCount = 4
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ProgressBar = GameWorld.ProgressBar
local XArtNumber = GameMain.XArtNumber
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local LoadingBarManager = GameManager.LoadingBarManager
local StringStyleUtil = GameUtil.StringStyleUtil
local UIList = ClassTypes.UIList
local SelectRoleItem = ClassTypes.SelectRoleItem
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

local Move_Y = 20
local Time_Out_Sec = 20

--override
function SelectRolePanel:Awake()
    self._roleInfos = {}
    self._selectDBID = 0
    self._preSelectDBID = -1
    self._preSelect = -1
    self:InitView()
    self:InitCallBack()
end

--override
function SelectRolePanel:OnDestroy()
end

function SelectRolePanel:split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}
    local i = 1
    for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function SelectRolePanel:OnShow(data)
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_24)
    self:ShowSelectRoleWaiting(false)
    self.data = {}
    local roleTable = data[1]
    self._selectDBID = data[2]
    --保证上次登录角色信息在数据表的第一组
    local needId = 1
    for i = 1, #roleTable do
        table.insert(self.data, roleTable[i])
        if roleTable[i][public_config.CHARACTER_KEY_DBID] == data[2] then
            needId = i
        end
    end
    CreateRoleManager.CurrentSelectedCharacter = needId
    for j = 1, #self.data do
        self:InitEquip(self.data[j][public_config.CHARACTER_KEY_FACADE], j)
    end
    
    for i = #roleTable + 1, MaxButtonCount do
        table.insert(self.data, {0})
    end
    self:RefreshData(self.data, needId)
end

--override
function SelectRolePanel:OnClose()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_25)
    self._preSelectDBID = -1
    if self._timer then
        TimerHeap:DelTimer(self._timer)
    end
end

function SelectRolePanel:OnEnable()
    self:AddEventListeners()
end

function SelectRolePanel:OnDisable()
    self:RemoveEventListeners()
end

function SelectRolePanel:InitCallBack()
    self._onSelectChange = function(id)self:OnSelectChange(id) end
    self._onSelectRoleSucc = function()self:OnSelectRoleSucc() end
    self._oSelectRoleWaiting = function(state) self:ShowSelectRoleWaiting(state) end
end

function SelectRolePanel:InitEquip(equipstr, j)
    --LoggerHelper.Error("facadeLua:"..equipstr)
    --格式 1:1060102,0,0,0,0,0;11:1010101,0,0,0,0,0
    --
    if equipstr == nil then
        return
    end
    
    local equiptable = self:split(equipstr, ";")
    for k, v in pairs(equiptable) do
        local equipitem = self:split(v, ":")
        if equipitem[1] == "1" then --cloth
            CreateRoleManager.ClothId[j] = {}
            CreateRoleManager.ClothId[j][1] = tonumber(self:split(equipitem[2], ",")[1])
            CreateRoleManager.ClothId[j][2] = tonumber(self:split(equipitem[2], ",")[2])
            CreateRoleManager.ClothId[j][3] = tonumber(self:split(equipitem[2], ",")[3])
            CreateRoleManager.ClothId[j][4] = tonumber(self:split(equipitem[2], ",")[4])
            CreateRoleManager.ClothId[j][5] = tonumber(self:split(equipitem[2], ",")[5])
            CreateRoleManager.ClothId[j][6] = tonumber(self:split(equipitem[2], ",")[6])
        elseif equipitem[1] == "11" then --weapon
            CreateRoleManager.WeaponId[j] = {}
            CreateRoleManager.WeaponId[j][1] = tonumber(self:split(equipitem[2], ",")[1])
            CreateRoleManager.WeaponId[j][2] = tonumber(self:split(equipitem[2], ",")[2])
            CreateRoleManager.WeaponId[j][3] = tonumber(self:split(equipitem[2], ",")[3])
            CreateRoleManager.WeaponId[j][4] = tonumber(self:split(equipitem[2], ",")[4])
            CreateRoleManager.WeaponId[j][5] = tonumber(self:split(equipitem[2], ",")[5])
            CreateRoleManager.WeaponId[j][6] = tonumber(self:split(equipitem[2], ",")[6])
        elseif equipitem[1] == "21" then --wing
            CreateRoleManager.WingId[j] = {}
            CreateRoleManager.WingId[j][1] = tonumber(self:split(equipitem[2], ",")[1])
            CreateRoleManager.WingId[j][2] = tonumber(self:split(equipitem[2], ",")[2])
            CreateRoleManager.WingId[j][3] = tonumber(self:split(equipitem[2], ",")[3])
            CreateRoleManager.WingId[j][4] = tonumber(self:split(equipitem[2], ",")[4])
            CreateRoleManager.WingId[j][5] = tonumber(self:split(equipitem[2], ",")[5])
            CreateRoleManager.WingId[j][6] = tonumber(self:split(equipitem[2], ",")[6])
        elseif equipitem[1] == "31" then --head
            CreateRoleManager.HeadId[j] = {}
            CreateRoleManager.HeadId[j][1] = tonumber(self:split(equipitem[2], ",")[1])
            CreateRoleManager.HeadId[j][2] = tonumber(self:split(equipitem[2], ",")[2])
            CreateRoleManager.HeadId[j][3] = tonumber(self:split(equipitem[2], ",")[3])
            CreateRoleManager.HeadId[j][4] = tonumber(self:split(equipitem[2], ",")[4])
            CreateRoleManager.HeadId[j][5] = tonumber(self:split(equipitem[2], ",")[5])
            CreateRoleManager.HeadId[j][6] = tonumber(self:split(equipitem[2], ",")[6])
        end
    end
end

function SelectRolePanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ON_ROLE_INFO_SELECT, self._onSelectChange)
    EventDispatcher:AddEventListener(GameEvents.ON_SELECT_ROLE_SUCC, self._onSelectRoleSucc)
    EventDispatcher:AddEventListener(GameEvents.ON_SELECT_ROLE_WAITING, self._oSelectRoleWaiting)
end

function SelectRolePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_ROLE_INFO_SELECT, self._onSelectChange)
    EventDispatcher:RemoveEventListener(GameEvents.ON_SELECT_ROLE_SUCC, self._onSelectRoleSucc)
    EventDispatcher:RemoveEventListener(GameEvents.ON_SELECT_ROLE_WAITING, self._oSelectRoleWaiting)
end

function SelectRolePanel:InitView()
    self._backButton = self:FindChildGO("Button_Back")
    self._enterButton = self:FindChildGO("Button_Enter")
    self._csBH:AddClick(self._backButton, function()self:OnBackButtonClick() end)
    self._csBH:AddClick(self._enterButton, function()self:OnEnterGame() end)
    
    local rotateObj = self:FindChildGO("Container_ChangeRole")
    self.rotateModel = rotateObj:AddComponent(typeof(LuaUIRotateModel))
    
    self._vocationIcon = self:FindChildGO("Container_Right/Container_Desc/Container_Icon")
    self._nameIcon = self:FindChildGO("Container_Right/Container_Desc/Container_Name")
    self._fpNumber = self:AddChildComponent('Container_Right/Container_Desc/Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)
    
    local moveTo = self._vocationIcon.transform.localPosition
    moveTo.y = moveTo.y + Move_Y
    self._tweenPosVocation = self:AddChildComponent('Container_Right/Container_Desc/Container_Icon', XGameObjectTweenPosition)
    self._tweenPosVocation:SetToPosTween(moveTo, 2, 4, 4 ,nil)
    
    self._roleInfoText = self:GetChildComponent("Container_Right/Container_Desc/Text_Info", "TextMeshWrapper")
    
    self._waitingImage = self:FindChildGO("Image_Waiting")
    self._waitingText = self:GetChildComponent("Image_Waiting/Text_Desc", 'TextMeshWrapper')
    self._waitingImage:SetActive(false)
    
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Left/ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    scrollView:SetHorizontalMove(false)
    scrollView:SetVerticalMove(true)
    self._list:SetItemType(SelectRoleItem)
    self._list:SetPadding(6, 0, 0, 4)
    self._list:SetGap(16, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._list:SetItemClickedCB(itemClickedCB)
end

function SelectRolePanel:OnListItemClicked(idx)
    if self._preSelect ~= -1 then
        self._list:GetItem(self._preSelect):ShowSelected(false)
    end
    self._list:GetItem(idx):ShowSelected(true)
    self._preSelect = idx
    
    local data = self._list:GetDataByIndex(idx)
    self:RefreshSelectInfo(data)
end

function SelectRolePanel:RefreshData(data, selectId)
    self._list:RemoveAll()
    self._list:SetDataList(data)
    self:OnListItemClicked(selectId - 1)
end

function SelectRolePanel:OnBackButtonClick()
    GameManager.GameStateManager.ReturnLogin(true)
    GUIManager.ClosePanel(PanelsConfig.SelectRole)
    GUIManager.ShowPanel(PanelsConfig.Login)
end

function SelectRolePanel:OnEnterGame()
    --LoggerHelper.Error("-----------EnterGame----------"..tostring(self._selectDBID))
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_26)
    self:ShowSelectRoleWaiting(true)
    GameWorld.Player():choose_avatar_req(self._selectDBID)
end

function SelectRolePanel:OnSelectRoleSucc()
    GUIManager.ClosePanel(PanelsConfig.SelectRole)
    LoadingBarManager:ShowProgressBar(0.1)
    CreateRoleManager:DestroyRttCamera()
    CreateRoleManager:DestroyEntity()
end

function SelectRolePanel:RefreshSelectInfo(data)
    if data[public_config.CHARACTER_KEY_DBID] == 0 then
        return
    end
    self._selectDBID = data[public_config.CHARACTER_KEY_DBID]
    if self._preSelectDBID == self._selectDBID then
        return
    else
        self._preSelectDBID = self._selectDBID
    end
    
    local vocation = data[public_config.CHARACTER_KEY_VOCATION]
    local facade = data[public_config.CHARACTER_KEY_FACADE]
    local model = RoleDataHelper.GetModelByVocation(vocation)
    CreateRoleManager:DestroyEntity()
    CreateRoleManager:BuildRole(vocation, model, facade, 1, function()
        self.rotateModel:SetTragetTransform(CreateRoleManager.Entity.cs:GetTransform())
    end)
    
    local vocationIcon = RoleDataHelper.GetIconId2ByVocation(vocation)
    local nameIcon = RoleDataHelper.GetNameIconIdByVocation(vocation)
    GameWorld.AddIcon(self._vocationIcon, vocationIcon)
    GameWorld.AddIcon(self._nameIcon, nameIcon)
    
    local power = data[public_config.CHARACTER_KEY_FIGHT_FORCE] or 0
    self._fpNumber:SetNumber(power)
    
    local levelStr = StringStyleUtil.GetLevelString(data[public_config.CHARACTER_KEY_LEVEL], nil, true) .. LanguageDataHelper.GetContent(69)
    self._roleInfoText.text = string.format("%s %s", data[public_config.CHARACTER_KEY_NAME], levelStr)
end

function SelectRolePanel:ShowSelectRoleWaiting(state)
    self._waitingImage:SetActive(state)
    if state then
        self._endTime = Time_Out_Sec
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnWaitingUpdate() end)
    else
        if self._timer then
            TimerHeap:DelTimer(self._timer)
        end
    end
end

function SelectRolePanel:OnWaitingUpdate()
    self._endTime = self._endTime - 1
    self._waitingText.text = LanguageDataHelper.CreateContent(585, self._endTime)
    if self._endTime < 0 then
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_27)
        self:ShowSelectRoleWaiting(false)
    end
end

-- -- 是否有背景 0 没有  1 清晰底板  2 模糊底板  3 二级面板遮罩
function SelectRolePanel:CommonBGType()
    return PanelBGType.NoBG
end
