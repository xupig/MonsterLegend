SelectRoleModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function SelectRoleModule.Init()
	GUIManager.AddPanel(PanelsConfig.SelectRole,"Panel_SelectRole",GUILayer.LayerUIPanel,"Modules.ModuleSelectRole.SelectRolePanel",true,PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return SelectRoleModule