-- WorldMapModule.lua
WorldMapModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function WorldMapModule.Init()
    GUIManager.AddPanel(PanelsConfig.WorldMap, "Panel_WorldMap", GUILayer.LayerUIPanel, "Modules.ModuleWorldMap.WorldMapPanel", true, false)
end

return WorldMapModule
