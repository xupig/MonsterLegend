-- WorldMapPanel.lua
local WorldMapPanel = Class.WorldMapPanel(ClassTypes.BasePanel)

require "Modules.ModuleWorldMap.ChildView.DistristView"
require "Modules.ModuleWorldMap.ChildView.TelePointView"
require "Modules.ModuleWorldMap.ChildView.NPCView"
require "Modules.ModuleWorldMap.ChildView.PlayerView"
require "Modules.ModuleWorldMap.ChildView.TaskTrackView"
require "Modules.ModuleWorldMap.ChildView.DistristMapView"
require "Modules.ModuleWorldMap.ChildView.PopupMonsterInfoView"
require "Modules.ModuleWorldMap.ChildView.MapLineView"
require "Modules.ModuleWorldMap.ChildComponent.DistristMapTypeItem"
require "Modules.ModuleWorldMap.ChildComponent.DistristMapSubtypeItem"
local DistristView = ClassTypes.DistristView
local TelePointView = ClassTypes.TelePointView
local NPCView = ClassTypes.NPCView
local PlayerView = ClassTypes.PlayerView
local TaskTrackView = ClassTypes.TaskTrackView
local DistristMapView = ClassTypes.DistristMapView
local PopupMonsterInfoView = ClassTypes.PopupMonsterInfoView
local MapLineView = ClassTypes.MapLineView
local DistristMapTypeItem = ClassTypes.DistristMapTypeItem
local DistristMapSubtypeItem = ClassTypes.DistristMapSubtypeItem

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local RectTransform = UnityEngine.RectTransform
local UIComponentUtil = GameUtil.UIComponentUtil
local MapManager = PlayerManager.MapManager
local TaskManager = PlayerManager.TaskManager
local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local GUIManager = GameManager.GUIManager
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ButtonWrapper = UIExtension.ButtonWrapper
local UINavigationMenu = ClassTypes.UINavigationMenu
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MonsterFamilyDataHelper = GameDataHelper.MonsterFamilyDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local InstanceManager = PlayerManager.InstanceManager
local SystemInfoManager = GameManager.SystemInfoManager
local error_code = GameWorld.error_code
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local MapConfig = GameConfig.MapConfig
local MonsterType = GameConfig.EnumType.MonsterType
local UIParticle = ClassTypes.UIParticle
local RoleDataHelper = GameDataHelper.RoleDataHelper

-- local myselfIcon = 500
-- local playerIcon = 501
function WorldMapPanel:Awake()
    self._isWorldMap = false
    self._containerWorldMap = self:FindChildGO("Container_WorldMap")
    self._containerDistrictMap = self:FindChildGO("Container_DistrictMap")
    self._districtMapView = self:AddChildLuaUIComponent("Container_DistrictMap", DistristMapView)
    self._containerMonsterInfo = self:FindChildGO("Container_MonsterInfo")
    self._popupMonsterInfoView = self:AddChildLuaUIComponent("Container_MonsterInfo/Container_MonsterInfo", PopupMonsterInfoView)
    self._imageMonsterInfoMask = self:FindChildGO("Container_MonsterInfo/Image_Mask")
    self._csBH:AddClick(self._imageMonsterInfoMask, function()self:OnClickMonsterInfoMask() end)
    self._mapLineView = self:AddChildLuaUIComponent("Container_Buttons/Container_MapLine", MapLineView)
    self._containerMonsterInfo:SetActive(false)
    local monsterInfoRect = self._popupMonsterInfoView.gameObject:GetComponent(typeof(UnityEngine.RectTransform))
    self._monsterInfoWidth = monsterInfoRect.sizeDelta.x
    self._monsterInfoHeight = monsterInfoRect.sizeDelta.y
    
    -- self._btnWorldMap = self:FindChildGO("Container_Buttons/Button_WorldMap")
    self._btnWorldMapClose = self:FindChildGO("Container_Buttons/Button_Close")
    -- self._btnCurDistMap = self:FindChildGO("Container_Buttons/Button_CurDistMap")
    self._toggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "Container_Buttons/ToggleGroup_Map")
    self._toggleGroup:SetOnSelectedIndexChangedCB(function(index)self:OnTabMapClick(index) end)
    local containerFxCurDistMap = self:FindChildGO("Container_Buttons/ToggleGroup_Map/Toggle_CurDistMap/Checkmark/Container_Fx")
    local containerFxWorldMap = self:FindChildGO("Container_Buttons/ToggleGroup_Map/Toggle_WorldMap/Checkmark/Container_Fx")
    UIParticle.AddParticle(containerFxCurDistMap, "fx_ui_30019")
    UIParticle.AddParticle(containerFxWorldMap, "fx_ui_30019")
    -- self._toggleGroup:SetSelectIndex(0)
    -- self._btnDistristMapClose = self:FindChildGO("Container_Buttons/Button_Close")
    -- self._csBH:AddClick(self._btnWorldMap, function()self:ShowWorldMap(); self:ShowMapInfo(); end)
    -- self._csBH:AddClick(self._btnCurDistMap, function()self:ShowDistrictMapAndSetData(); end)
    self._csBH:AddClick(self._btnWorldMapClose, function()self:CloseBtnClick() end)
    -- self._csBH:AddClick(self._btnDistristMapClose, function()self:CloseBtnClick() end)
    self:InitCallbackFunc()
    self:InitFunction()
    self._districtMapView:InitForWorldMap()
    self._districtMapView:SetUpdatePlayerInDistristCallback(self._onUpdateMapPos)
    
    self._containerNavigationMenu = self:FindChildGO("Container_Function/NavigationMenu_TypeList")
    self._containerfunction = self:FindChildGO("Container_Function")
    self._containerDistrictInfo = self:FindChildGO("Container_Function/Container_DistrictInfo")
    self._textDistrictName = self:GetChildComponent("Container_Function/Container_DistrictInfo/Text_Name", "TextMeshWrapper")
    self._containerDistrictImage = self:FindChildGO("Container_Function/Container_DistrictInfo/Container_Image")
    self._toggleShowMonsterInfo = UIToggle.AddToggle(self.gameObject, "Container_Function/Container_ShowMonsterInfo/Toggle_ShowMonsterInfo")
    
    self._containerGuildClickFlagInfo = self:FindChildGO("Container_GuildClickFlagInfo")
    self._containerGuildClickFlagSelfIcon = self:FindChildGO("Container_GuildClickFlagInfo/Container_Self/Container_Icon")
    self._containerGuildClickFlagEnemyIcon = self:FindChildGO("Container_GuildClickFlagInfo/Container_Enemy/Container_Icon")
    self._containerGuildClickFlagEmptyIcon = self:FindChildGO("Container_GuildClickFlagInfo/Container_Empty/Container_Icon")
end

function WorldMapPanel:OnClickMonsterInfoMask()
    self._containerMonsterInfo:SetActive(false)
end

function WorldMapPanel:OnTabMapClick(index)
    if index == 1 then
        self:ShowWorldMap()
        self:ShowMapInfo()
    else
        self:ShowDistrictMapAndSetData()
    end
end

function WorldMapPanel:CloseBtnClick()
    GUIManager.ClosePanel(GameConfig.PanelsConfig.WorldMap)
end

-- function WorldMapPanel:FilterChanged(index)
--     self._currentIndex = index
--     self._districtMapView:SetFilter(index)
-- -- GUIManager.ClosePanel(GameConfig.PanelsConfig.WorldMap)
-- end
function WorldMapPanel:InitCallbackFunc()
    self._onMyTeamUpdate = function()self:UpdateTeamList() end
    self._onTaskChanged = function()self:UpdateTask() end
    self._onShowPopupMonsterInfo = function(pointPos, instanceId)self:ShowPopupMonsterInfo(pointPos, instanceId) end
    self._onHidePopupMonsterInfo = function()self:HidePopupMonsterInfo() end
    self._onShowMapFromPrestige = function(mapId, eventId)self:ShowMapFromPrestige(mapId, eventId) end
    -- self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onUpdateMapPos = function()self:UpdateMapPos() end
    self._onHasLittleShoesChanged = function()self:OnHasLittleShoesChanged() end
    
    self._onAvatarEnterWorld = function(entityId)self:OnAvatarEnterWorld(entityId) end
    self._onAvatarLeaveWorld = function(entityId)self:OnAvatarLeaveWorld(entityId) end
    -- self._onClickFlagEnterWorld = function(entityId)self:OnClickFlagEnterWorld(entityId) end
    -- self._onClickFlagLeaveWorld = function(entityId)self:OnClickFlagLeaveWorld(entityId) end
    self._onPlayerMove = function()self:OnPlayerMove() end
end

function WorldMapPanel:OnDestroy()
    self._containerWorldMap = nil
    self._containerDistrictMap = nil
    self._districtMapView = nil
    -- self._btnWorldMap = nil
    self._btnWorldMapClose = nil
    -- self._btnDistristMapClose = nil
    self._iconBg = nil
end

function WorldMapPanel:OnShow(data)
    self._iconBg = self:FindChildGO("Container_WorldMap/Image_Map")
    GameWorld.AddIcon(self._iconBg, 13514)
    if data == "ShowRecommend" then
        local worldMapIds = WorldMapDataHelper:GetAllId()
        local playerLevel = GameWorld.Player().level
        local topFitLevel = 0
        local topFitMapId = 0
        for i, v in ipairs(worldMapIds) do
            if WorldMapDataHelper:GetButtonName(v) ~= 0 then
                local mapId = WorldMapDataHelper:GetSceneId(v)
                if mapId ~= 10000 then
                    local limitLevel = MapDataHelper.GetLimitLevel(mapId)
                    if playerLevel >= limitLevel then
                        if limitLevel >= topFitLevel then
                            topFitLevel = limitLevel
                            topFitMapId = mapId
                        end
                    end
                end
            end
        end
        self:ShowMap(topFitMapId, true)
    else
        self:ShowMap(GameSceneManager:GetCurrMapID())
    end
    -- EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.OnHasLittleShoesChanged, self._onHasLittleShoesChanged)
-- self:ShowWorldMap(); self:ShowMapInfo();
-- EventDispatcher:AddEventListener(GameEvents.OnShowMapFromPrestige, self._onShowMapFromPrestige)
end

function WorldMapPanel:OnClose()
    self._iconBg = nil
    -- EventDispatcher:RemoveEventListener(GameEvents.OnShowMapFromPrestige, self._onShowMapFromPrestige)
    self:CloseMap()
    -- EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.OnHasLittleShoesChanged, self._onHasLittleShoesChanged)
-- self._selectedFItem = nil
end

function WorldMapPanel:OnPlayerMove()
    self._districtMapView:ShowPath()
end

function WorldMapPanel:OnHasLittleShoesChanged()
    local hasLittleShoes = InstanceManager:GetHasLittleShoes()
    -- LoggerHelper.Log("Ash: OnHasLittleShoesChanged:" .. tostring(self._selectedFIndex) .. " " .. tostring(hasLittleShoes))
    local length = self._menu:GetSecondLevelLength(self._selectedFIndex) - 1
    -- LoggerHelper.Log("Ash: GetSecondLevelLength:" .. tostring(length))
    for i = 0, length do
        -- local data = self._list:GetDataByIndex(i)
        local item = self._menu:GetSItem(self._selectedFIndex, i)
        item:ChangeLittleShoes(hasLittleShoes)
    end
end

function WorldMapPanel:OnEnterMap(mapId)
    self:CloseMap()
    self:ShowMap(mapId)
end
-- function WorldMapPanel:ShowMapFromPrestige(mapId, eventId)
--     if self._isWorldMap then
--         self:ShowDistrictMap()
--     end
--     if mapId ~= self._districtMapView:GetCurMapId() then
--         self:CloseMap()
--         self:ShowMap(mapId)
--     end
--     self._districtMapView:TriggerEventActivityClick(eventId)
-- end
function WorldMapPanel:UpdateMapPos()
    if self._isNormalMap then
        self._districtMapView:UpdateMonster()
    end
end

function WorldMapPanel:ShowDistrictInfo(worldMapId)
    local buttonName = WorldMapDataHelper:GetButtonName(worldMapId)
    if buttonName == 0 then
        self._containerDistrictInfo:SetActive(false)
    else
        self._containerDistrictInfo:SetActive(true)
        self._textDistrictName.text = LanguageDataHelper.CreateContent(buttonName)
        GameWorld.AddIcon(self._containerDistrictImage, WorldMapDataHelper:GetRegionalIcon(worldMapId))
    end
end

function WorldMapPanel:HideDistrictInfo()
    self._containerDistrictInfo:SetActive(false)
end

function WorldMapPanel:ShowMap(mapId, showRecommend)
    self._curMapId = mapId
    self._cacheNpcsByMapId = {}
    -- self:ShowWorldMap(); self:ShowMapInfo();
    self:HidePopupMonsterInfo()
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_MAIN or sceneType == SceneConfig.SCENE_TYPE_WILD then
        self._mapLineView.gameObject:SetActive(true)
    else
        self._mapLineView.gameObject:SetActive(false)
    end
    if sceneType == SceneConfig.SCENE_TYPE_PVP6
        or sceneType == SceneConfig.SCENE_TYPE_PVP_NEW
        or sceneType == SceneConfig.SCENE_TYPE_FIGHT_FRIEND
        or sceneType == SceneConfig.SCENE_TYPE_LADDER_1V1
    then
        self:ShowPVPMap(mapId)
    elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then
        self:ShowGuildClickFlagMap(mapId)
    else
        self:ShowNormalMap(mapId, showRecommend)
    end
end

function WorldMapPanel:CloseMap()
    self._cacheNpcsByMapId = {}--清理缓存数据
    -- LoggerHelper.Log("Ash: WorldMapPanel OnClose")
    if self._isNormalMap then
        self:CloseNormalMap()
    else
        self:ClosePVPMap()
    end
end

function WorldMapPanel:ShowNormalMap(mapId, showRecommend)
    self._toggleGroup.gameObject:SetActive(true)
    self._toggleGroup:SetSelectIndex(0)
    -- self._btnWorldMap:SetActive(true)
    -- self._btnCurDistMap:SetActive(true)
    self._containerfunction:SetActive(true)
    self._containerGuildClickFlagInfo:SetActive(false)
    self._isNormalMap = true
    self:ShowDistrictMapAndSetData(mapId, showRecommend)
    EventDispatcher:AddEventListener(GameEvents.TEAM_MY_UPDATE, self._onMyTeamUpdate)
    EventDispatcher:AddEventListener(GameEvents.TEAM_MEMBER_MAPINFO_UPDATE, self._onMyTeamUpdate)
    -- EventDispatcher:AddEventListener(GameEvents.OnTaskChanged, self._onTaskChanged)
    EventDispatcher:AddEventListener(GameEvents.OnShowPopupMonsterInfo, self._onShowPopupMonsterInfo)
    EventDispatcher:AddEventListener(GameEvents.OnHidePopupMonsterInfo, self._onHidePopupMonsterInfo)
    EventDispatcher:AddEventListener(GameEvents.OnPlayerMove, self._onPlayerMove)
    self._mapLineView:Reset()
end

function WorldMapPanel:CloseNormalMap()
    EventDispatcher:RemoveEventListener(GameEvents.TEAM_MY_UPDATE, self._onMyTeamUpdate)
    EventDispatcher:RemoveEventListener(GameEvents.TEAM_MEMBER_MAPINFO_UPDATE, self._onMyTeamUpdate)
    -- EventDispatcher:RemoveEventListener(GameEvents.OnTaskChanged, self._onTaskChanged)
    EventDispatcher:RemoveEventListener(GameEvents.OnShowPopupMonsterInfo, self._onShowPopupMonsterInfo)
    EventDispatcher:RemoveEventListener(GameEvents.OnHidePopupMonsterInfo, self._onHidePopupMonsterInfo)
    EventDispatcher:RemoveEventListener(GameEvents.OnPlayerMove, self._onPlayerMove)
    self._districtMapView:StopUpdatePlayerInDistrist()
end

function WorldMapPanel:ShowPVPMap(mapId)
    -- LoggerHelper.Log("Ash: WorldMapPanel ShowPVPMap: ")
    self._toggleGroup.gameObject:SetActive(false)
    -- self._btnWorldMap:SetActive(false)
    -- self._btnCurDistMap:SetActive(false)
    self._containerfunction:SetActive(false)
    self._containerGuildClickFlagInfo:SetActive(false)
    self._isNormalMap = false
    self:ShowPVPDistrictMapAndSetData(mapId)
    EventDispatcher:AddEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
    EventDispatcher:AddEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
end

function WorldMapPanel:ClosePVPMap()
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
    self._districtMapView:StopUpdatePVPDistrist()
end


function WorldMapPanel:ShowGuildClickFlagMap(mapId)
    -- LoggerHelper.Log("Ash: WorldMapPanel ShowPVPMap: ")
    self._toggleGroup.gameObject:SetActive(false)
    -- self._btnWorldMap:SetActive(false)
    -- self._btnCurDistMap:SetActive(false)
    self._containerfunction:SetActive(false)
    self._containerGuildClickFlagInfo:SetActive(true)
    self._isNormalMap = false
    GameWorld.AddIcon(self._containerGuildClickFlagSelfIcon, 13414)
    GameWorld.AddIcon(self._containerGuildClickFlagEnemyIcon, 13415)
    GameWorld.AddIcon(self._containerGuildClickFlagEmptyIcon, 13416)
    
    self:ShowGuildClickFlagDistrictMapAndSetData(mapId)
    EventDispatcher:AddEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
    EventDispatcher:AddEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
-- EventDispatcher:AddEventListener(GameEvents.OnClickFlagEnterWorld, self._onClickFlagEnterWorld)
-- EventDispatcher:AddEventListener(GameEvents.OnClickFlagLeaveWorld, self._onClickFlagLeaveWorld)
end

function WorldMapPanel:CloseGuildClickFlagMap()
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarEnterWorld, self._onAvatarEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnAvatarLeaveWorld, self._onAvatarLeaveWorld)
    -- EventDispatcher:RemoveEventListener(GameEvents.OnClickFlagEnterWorld, self._onClickFlagEnterWorld)
    -- EventDispatcher:RemoveEventListener(GameEvents.OnClickFlagLeaveWorld, self._onClickFlagLeaveWorld)
    self._districtMapView:StopUpdatePVPDistrist()
end

function WorldMapPanel:OnAvatarEnterWorld(entityId)
    local ety = GameWorld.GetEntity(entityId)
    self._districtMapView:AddAvatarByEty(ety)
end

function WorldMapPanel:OnAvatarLeaveWorld(entityId)
    self._districtMapView:RemoveAvatarByEtyId(entityId)
end

-- function WorldMapPanel:OnClickFlagEnterWorld(entityId)
--     local ety = GameWorld.GetEntity(entityId)
--     self._districtMapView:AddGuildClickFlagByEty(ety)
-- end
-- function WorldMapPanel:OnClickFlagLeaveWorld(entityId)
--     self._districtMapView:RemoveGuildClickFlagByEtyId(entityId)
-- end
function WorldMapPanel:ShowGuildClickFlagDistrictMapAndSetData(mapId)
    local mapId, worldMapId, curMapRegInfo = MapManager:GetMapRegionalInfo(mapId)
    self:ShowDistrictMap(worldMapId)
    -- LoggerHelper.Log("Ash: WorldMapPanel ShowGuildClickFlagDistrictMapAndSetData OnShow: " .. tostring(worldMapId) .. " " .. PrintTable:TableToStr(curMapRegInfo))
    self._districtMapView:ShowGuildClickFlagInfo(mapId, worldMapId, curMapRegInfo, true)
end

function WorldMapPanel:ShowPVPDistrictMapAndSetData(mapId)
    local mapId, worldMapId, curMapRegInfo = MapManager:GetMapRegionalInfo(mapId)
    self:ShowDistrictMap(worldMapId)
    -- LoggerHelper.Log("Ash: WorldMapPanel OnShow: " .. tostring(worldMapId) .. " " .. PrintTable:TableToStr(curMapRegInfo))
    self._districtMapView:ShowPVPInfo(mapId, worldMapId, curMapRegInfo, true)
end

function WorldMapPanel:ShowDistrictMapAndSetData(distMapId, showRecommend)
    local mapId, worldMapId, curMapRegInfo = MapManager:GetMapRegionalInfo(distMapId)
    self:ShowDistrictMap(worldMapId)
    -- LoggerHelper.Log("Ash: WorldMapPanel OnShow: " .. tostring(worldMapId) .. " " .. PrintTable:TableToStr(curMapRegInfo))
    local sceneType = MapDataHelper.GetSceneType(mapId)
    local isWild = false
    if sceneType == SceneConfig.SCENE_TYPE_MAIN or sceneType == SceneConfig.SCENE_TYPE_WILD or SceneConfig.IsBossScene(sceneType) then
        isWild = true
    end
    -- LoggerHelper.Log("Ash: WorldMapPanel ShowDistrictMapAndSetData: " .. tostring(sceneType) .. " " .. tostring(isWild))
    self._districtMapView:ShowDistrictInfo(mapId, worldMapId, curMapRegInfo, true, isWild)
    self:SetTypeNavData(mapId, showRecommend, isWild)
end

function WorldMapPanel:ShowDistrictMap(worldMapId)
    self._isWorldMap = false
    self._containerWorldMap:SetActive(false)
    self._containerDistrictMap:SetActive(true)
    self:ShowDistrictInfo(worldMapId)
end

function WorldMapPanel:ShowWorldMap()
    self._isWorldMap = true
    self._containerWorldMap:SetActive(true)
    self._containerDistrictMap:SetActive(false)
    self._districtMapView:StopUpdatePlayerInDistrist()
    self:HideDistrictInfo()
    self:HidePopupMonsterInfo()
end



function WorldMapPanel:UpdateTeamList()
    -- LoggerHelper.Log("Ash: WorldMapPanel UpdateTeamList: " .. tostring(self._isWorldMap))
    if self._isWorldMap then
        -- self:ShowPlayerInWorldMap()
        else
        if self._curMapId == GameSceneManager:GetCurrMapID() then
            self._districtMapView:ShowPlayer(self._curMapId)
        end
    end
end

function WorldMapPanel:UpdateTask()
-- if self._isWorldMap then
--     -- self:ShowTaskTrackInWorldMap()
--     else
--     self._districtMapView:ShowTask(self._curMapId)
-- end
end

function WorldMapPanel:ShowPopupMonsterInfo(pointPos, triggerId)
    if self._toggleShowMonsterInfo:GetToggleWrapper().isOn then
        local id = MonsterFamilyDataHelper:GetIdByMapAndTrigger(self._curMapId, triggerId)
        -- LoggerHelper.Log("Ash: WorldMapPanel ShowPopupMonsterInfo: " .. tostring(self._curMapId) .. " " .. tostring(triggerId))
        -- LoggerHelper.Log("Ash: WorldMapPanel ShowPopupMonsterInfo: " .. tostring(self._monsterInfoWidth) .. " " .. tostring(self._monsterInfoHeight) .. " " .. PrintTable:TableToStr(pointPos))
        if id then
            local x = pointPos.x
            local y = pointPos.y
            if pointPos.x > self._monsterInfoWidth then
                x = pointPos.x - self._monsterInfoWidth
            end
            if pointPos.y > self._monsterInfoHeight then
                y = pointPos.y - self._monsterInfoHeight
            end
            local instancePos = Vector2(x, y)
            -- LoggerHelper.Log("Ash: WorldMapPanel ShowPopupMonsterInfo: " .. tostring(self._monsterInfoWidth) .. " " .. tostring(self._monsterInfoHeight) .. " " .. PrintTable:TableToStr(pointPos) .. " " .. PrintTable:TableToStr(instancePos))
            self._containerMonsterInfo:SetActive(true)
            self._popupMonsterInfoView:SetMonsterFamilyId(id)
            self._popupMonsterInfoView.transform.localPosition = instancePos
        end
    end
end

function WorldMapPanel:HidePopupMonsterInfo()
    self._containerMonsterInfo:SetActive(false)
end



-- 世界地图逻辑
function WorldMapPanel:ShowMapInfo()
    if self._districtViews == nil then
        self._districtViews = {}
    end
    local worldMapIds = WorldMapDataHelper:GetAllId()
    local playerLevel = GameWorld.Player().level
    for i, v in ipairs(worldMapIds) do
        if WorldMapDataHelper:GetButtonName(v) ~= 0 then
            local mapId = WorldMapDataHelper:GetSceneId(v)
            local distView = nil
            if self._districtViews[mapId] == nil then
                local district = self:CopyUIGameObject("Container_WorldMap/Container_District", "Container_WorldMap")
                local trans = district:GetComponent(typeof(RectTransform))
                trans.localPosition = WorldMapDataHelper:GetButton(v)
                district.name = tostring(mapId)
                distView = UIComponentUtil.AddLuaUIComponent(district, DistristView)
                distView:SetBtnName(WorldMapDataHelper:GetButtonName(v))
                distView:SetMapId(mapId)
                distView:SetBtnCB(function(mapId)self:DistrictBtnClick(mapId) end)
            else
                distView = self._districtViews[mapId]
            end
            local limitLevel = MapDataHelper.GetLimitLevel(mapId)
            distView:SetLockState(playerLevel < limitLevel)
            distView:SetBtnImage(WorldMapDataHelper:GetRegionalIcon(v))
            distView:SetLimitLevel(limitLevel)
            self._districtViews[mapId] = distView
        end
    end
    self:ShowPlayerInWorldMap()
end

-- 世界地图显示玩家信息
function WorldMapPanel:ShowPlayerInWorldMap()
    local mapId = GameSceneManager:GetCurrMapID()
    local hasSetActiveTrue = false
    if self._playerInWorldMap == nil then
        self._playerInWorldMap = {}
    end
    for k, v in pairs(self._playerInWorldMap) do
        if k == mapId then
            v:SetActive(true)
            hasSetActiveTrue = true
        else
            v:SetActive(false)
        end
    end
    if not hasSetActiveTrue then
        local entity = GameWorld.Player()
        self:AddPlayerInWorldMap(mapId, entity, entity.name)
    end
-- local tm = MapManager:GetTeamMember()
-- if tm ~= nil then
--     for k, teamData in pairs(tm) do
--         -- if teamData ~= "nil" and entity.id ~= teamData:GetEntityId() then
--         -- LoggerHelper.Log("Ash: ShowPlayerInWorldMap:" .. tostring(teamData:GetMapId()) .. "___" .. teamData:GetName())
--         self:AddPlayerInWorldMap(teamData:GetMapId(), false, teamData:GetName())
--     -- end
--     end
-- end
end

function WorldMapPanel:AddPlayerInWorldMap(mapId, entity, name)
    if self._districtViews[mapId] ~= nil then
        local goPlayers = self:FindChildGO("Container_WorldMap/" .. tostring(mapId) .. "/Container_Players")
        goPlayers:SetActive(true)
        -- LoggerHelper.Log("Ash: AddPlayerInWorldMap:" .. tostring(mapId) .. "___" .. tostring(isMyself) .. "___" .. tostring(name))
        local goHead = self:FindChildGO("Container_WorldMap/" .. tostring(mapId) .. "/Container_Players/Container_Head")
        local icon = RoleDataHelper.GetHeadIconByVocation(entity.vocation)
        GameWorld.AddIcon(goHead, icon)
        self._playerInWorldMap[mapId] = goPlayers
    -- local go = self:CopyUIGameObject("Container_DistrictMap/Container_Player", "Container_WorldMap/" .. tostring(mapId) .. "/Container_Players")
    -- local item = UIComponentUtil.AddLuaUIComponent(go, PlayerView)
    -- if isMyself then
    --     item:SetIcon(myselfIcon)
    -- else
    --     item:SetIcon(playerIcon)
    -- end
    -- item:SetName(name)
    -- table.insert(self._playerInWorldMap, go)
    end
end


-- 世界地图显示任务追踪信息
function WorldMapPanel:ShowTaskTrackInWorldMap()
    if self._taskTrackInWorldMap ~= nil then
        for k, v in pairs(self._taskTrackInWorldMap) do
            GameWorld.Destroy(v)
        end
    end
    self._taskTrackInWorldMap = {}
    local mapTaskFilter = {}
    local worldMapIds = WorldMapDataHelper:GetAllId()
    for i, v in ipairs(worldMapIds) do
        local mapId = WorldMapDataHelper:GetSceneId(v)
        mapTaskFilter[mapId] = {}
        if self._cacheNpcsByMapId[mapId] == nil then
            self._cacheNpcsByMapId[mapId] = MapManager:GetNPCByMapId(mapId)-- 获取当前地图所有NPC
        end
        local npcs = self._cacheNpcsByMapId[mapId]
        for k, position in pairs(npcs) do
            local task, eventItemData = TaskManager:GetNPCTask(k)-- 判断NPC身上是否有任务
            -- LoggerHelper.Log("Ash: ShowTask: " .. PrintTable:TableToStr(task))
            if task ~= nil and task:GetIsTrack() then
                if mapTaskFilter[mapId][task:GetId()] == nil then
                    mapTaskFilter[mapId][task:GetId()] = 1
                    self:AddTaskTrackInWorldMap(mapId, task:GetIcon())
                end
            end
        end
        local tracingMapPoint = TaskManager:GetTracingMapPoints(mapId)-- 获取当前地图所有追踪任务点
        for k, v in pairs(tracingMapPoint) do
            local task = v:GetTask()
            local eventItemData = v:GetEventItemData()
            if mapTaskFilter[mapId][task:GetId()] == nil then
                mapTaskFilter[mapId][task:GetId()] = 1
                self:AddTaskTrackInWorldMap(mapId, task:GetIcon())
            end
        end
    end
end

function WorldMapPanel:AddTaskTrackInWorldMap(mapId, iconId)
-- if self._districtViews[mapId] ~= nil then
--     -- LoggerHelper.Log("Ash: AddPlayerInWorldMap:" .. tostring(mapId) .. "___" .. tostring(isMyself) .. "___" .. tostring(name))
--     local go = self:CopyUIGameObject("Container_DistrictMap/Container_Task", "Container_WorldMap/" .. tostring(mapId) .. "/Container_Tasks")
--     local item = UIComponentUtil.AddLuaUIComponent(go, TaskTrackView)
--     item:SetIcon(iconId)
--     table.insert(self._taskTrackInWorldMap, go)
-- end
end

function WorldMapPanel:DistrictBtnClick(mapId)
    -- LoggerHelper.Log("Ash: DistrictBtnClick:" .. tostring(mapId))
    if mapId == GameSceneManager:GetCurrMapID() then
        GUIManager.ShowText(1, LanguageDataHelper.CreateContent(58679), 5)
    else
        if GameWorld.Player():CanExitMapByHit() then
            -- if GameWorld.Player().guard_goddess_id > 0 then
            --     SystemInfoManager:ShowClientTip(error_code.ERR_GUARD_GODDESS_HAS)
            -- else
            --前端不限制切换，全由后端限制
            GUIManager.ClosePanel(GameConfig.PanelsConfig.WorldMap)
            InstanceManager:CommonChangeMap(mapId)
            -- end
        end
    end
end


function WorldMapPanel:InitFunction()
    self:InitTypeNav()
end

function WorldMapPanel:InitTypeNav()
    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "Container_Function/NavigationMenu_TypeList")
    self._menu = menu
    self._scrollPage = scroll
    self._menu:SetFirstLevelItemType(DistristMapTypeItem)
    self._menu:SetSecondLevelItemType(DistristMapSubtypeItem)
    self._menu:SetPadding(0, 0, 0, 0)
    self._menu:SetGap(0, 0)
    self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx)self:OnTypeMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fidx, sidx)self:OnTypeMenuSItemClicked(fidx, sidx) end)
    self._menu:UseObjectPool(true)
end

function WorldMapPanel:SetTypeNavData(mapId, showRecommend, isWild)
    if isWild then
        self._containerNavigationMenu:SetActive(true)
        local monsters = self:GetMonsterNavData(mapId)
        local teleportInfos = self:GetTeleportNavData(mapId)
        local npcs = self:GetNPCNavData(mapId)
        local listData = {}
        if #monsters ~= 0 then
            table.insert(listData, {type1 = {name = LanguageDataHelper.CreateContent(58680), icon_bg = MapConfig.MENU_MONSTER_ICON_BG, icon_fg = MapConfig.MENU_MONSTER_ICON_FG}, type2 = monsters})
        end
        if #npcs ~= 0 then
            table.insert(listData, {type1 = {name = LanguageDataHelper.CreateContent(58681), icon_bg = MapConfig.MENU_NPC_ICON_BG, icon_fg = MapConfig.MENU_NPC_ICON_FG}, type2 = npcs})
        end
        if #teleportInfos ~= 0 then
            table.insert(listData, {type1 = {name = LanguageDataHelper.CreateContent(104), icon_bg = MapConfig.MENU_TELEPORT_ICON_BG, icon_fg = MapConfig.MENU_TELEPORT_ICON_FG}, type2 = teleportInfos})
        end
        local d = {}
        for i = 1, #listData do
            local cfgData = listData[i]
            d[i] = {}
            d[i][1] = cfgData.type1
            for j = 1, #cfgData.type2 do
                d[i][j + 1] = cfgData.type2[j]
            end
        end
        self._menu:SetDataList(d)
        self._selectedFIndex = -1
        if self._selectedFItem then
            self._selectedFItem:SetSelected(false)
        end
        self._selectedFItem = nil
        if self._selectedSItem then
            self._selectedSItem:ToggleHightlight()
        end
        self._selectedSItem = nil
        self._menu:SelectFItem(0)
        if showRecommend then
            local playerLevel = GameWorld.Player().level
            local topFitIndex = 1
            for i, monster in ipairs(monsters) do
                if playerLevel >= monster.level then
                    topFitIndex = i
                end
            end
            self._menu:OnSecondLevelMenuItemClicked(self._menu:GetSItem(0, topFitIndex - 1))
            self:ShowPopupMonsterInfo(monsters[topFitIndex].pointPos, monsters[topFitIndex].monsterId)
            if monsters[topFitIndex].callback then
                monsters[topFitIndex].callback()
            end
        end
    else
        self._containerNavigationMenu:SetActive(false)
    end
-- self:OnTypeMenuFItemClicked(0)
end

function WorldMapPanel:GetMonsterNavData(mapId)
    local rawData = MapManager:GetMonsterByMapId(mapId)
    local list = {}
    for k, v in pairs(rawData) do
        local id = v.monster_id
        local name = MonsterDataHelper.GetMonsterName(id)
        local type = MonsterDataHelper.GetMonsterType(id)
        local level = MonsterDataHelper.GetLv(id)
        local callback = function(useLittleShoe)EventDispatcher:TriggerEvent(GameEvents.PlayerCommandKillMonsterPoint, mapId, v.truePos, 4, 10, useLittleShoe, true) end
        local pointPos = self._districtMapView:GetMapPos(v.truePos, true)
        -- LoggerHelper.Log("Ash: WorldMapPanel v.truePos: " .. PrintTable:TableToStr(v.truePos) .. " pointPos: " .. PrintTable:TableToStr(pointPos))
        local item = {monsterId = k, name = name, subName = "Lv." .. level, level = level, icon = MonsterType.IsBoss(type) and MapConfig.BOSS_ICON or nil, pointPos = pointPos, callback = callback}
        table.insert(list, item)
        table.sort(list, function(a, b) return a.level < b.level end)
    end
    return list
end

function WorldMapPanel:GetTeleportNavData(mapId)
    local rawData = MapManager:GetTeleportInfo(mapId)
    local list = {}
    for k, v in pairs(rawData) do
        -- LoggerHelper.Log("Ash: DistristMapView ShowInstance: entity.target_scene " .. tostring(entity.target_scene))
        local name = LanguageDataHelper.GetContent(MapDataHelper.GetChinese(v.target_scene) or 0)
        local callback = function(useLittleShoe)EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindPosition, mapId, v.pos, useLittleShoe, true) end
        local item = {middleName = name, callback = callback}
        table.insert(list, item)
    end
    return list
end

function WorldMapPanel:GetNPCNavData(mapId)
    local rawData = MapManager:GetNPCByMapId(mapId)
    local list = {}
    for npcId, pos in pairs(rawData) do
        -- LoggerHelper.Log("Ash: DistristMapView ShowInstance: entity.target_scene " .. tostring(entity.target_scene))
        local name = NPCDataHelper.GetNPCName(npcId)
        local honor = NPCDataHelper.GetNPCFunctionHonor(npcId)
        name = name .. honor
        local callback = function(useLittleShoe)EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindNPC, mapId, npcId, pos, tonumber(GlobalParamsHelper.GetParamValue(30)), useLittleShoe, nil, true) end
        local item = {middleName = name, callback = callback}
        table.insert(list, item)
    end
    return list
end

function WorldMapPanel:OnTypeMenuFItemClicked(idx)
    if self._selectedFItem then
        if self._selectedFIndex ~= idx then
            self._selectedFItem:SetSelected(false)
        end
    end
    
    if self._selectedFIndex ~= idx then
        local data = self._menu:GetFirstLevelDataByIndex(idx)
        local icon = data.icon_bg
        if icon == MapConfig.MENU_MONSTER_ICON_BG then
            self._districtMapView:SetAllMonsterPointNameVisiable(true)
            self._districtMapView:SetAllNpcNameVisiable(false)
            self._districtMapView:SetAllInstanceNameVisiable(false)
        elseif icon == MapConfig.MENU_NPC_ICON_BG then
            self:HidePopupMonsterInfo()
            self._districtMapView:SetAllMonsterPointNameVisiable(false)
            self._districtMapView:SetAllNpcNameVisiable(true)
            self._districtMapView:SetAllInstanceNameVisiable(false)
        else
            self:HidePopupMonsterInfo()
            self._districtMapView:SetAllMonsterPointNameVisiable(false)
            self._districtMapView:SetAllNpcNameVisiable(false)
            self._districtMapView:SetAllInstanceNameVisiable(true)
        end
    end
    
    self._selectedFIndex = idx
    self._selectedFItem = self._menu:GetFItem(idx)
    self._selectedFItem:ToggleHightlight()
    self:OnHasLittleShoesChanged()
end

function WorldMapPanel:OnTypeMenuSItemClicked(fidx, sidx)
    local curItem = self._menu:GetSItem(fidx, sidx)
    if self._selectedSItem == curItem then
        self._selectedSItem:SecondClick()
        return
    end
    if self._selectedSItem then
        self._selectedSItem:ToggleHightlight()
    end
    self._selectedSItem = curItem
    self._selectedGemTypeData = self._menu:GetSecondLevelDataByIndex(fidx, sidx)
    self._selectedSItem:ToggleHightlight()
    self._selectedSItem:FirstClick()
    
    self._curSubType = sidx + 1
    
    --重置当前页
    self._curPage = 1
-- tradeData:ResetConditionList()
-- self:SearchByCondition()
end

function WorldMapPanel:WinBGType()
    return PanelWinBGType.FullSceenBG
end
