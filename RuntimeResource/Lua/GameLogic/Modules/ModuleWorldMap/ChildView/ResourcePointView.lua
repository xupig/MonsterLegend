-- ResourcePointView.lua
local ResourcePointView = Class.ResourcePointView(ClassTypes.BaseComponent)

ResourcePointView.interface = GameConfig.ComponentsConfig.Component
ResourcePointView.classPath = "Modules.ModuleWorldMap.ChildView.ResourcePointView"
local UIComponentUtil = GameUtil.UIComponentUtil

function ResourcePointView:Awake()
    self._imageCollectPoint = self:FindChildGO("Image_CollectPoint")
end

function ResourcePointView:SetPosition(position)
    if self._position ~= nil and Vector2.Distance(self._position, position) < 1 then
        return
    end
    self.transform.localPosition = position
    self._position = position
end

function ResourcePointView:GetPosition()
    return self._position
end

function ResourcePointView:SetIcon(iconId)
    GameWorld.AddIcon(self._imageCollectPoint, iconId, nil, true)
end

function ResourcePointView:SetName(name)
end

function ResourcePointView:OnDestroy()
    self._imageCollectPoint = nil
    self._position = nil
end
