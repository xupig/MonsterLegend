-- DistristMapView.lua
local DistristMapView = Class.DistristMapView(ClassTypes.BaseComponent)

DistristMapView.interface = GameConfig.ComponentsConfig.Component
DistristMapView.classPath = "Modules.ModuleWorldMap.ChildView.DistristMapView"
require "Modules.ModuleWorldMap.ChildView.DistristView"
require "Modules.ModuleWorldMap.ChildView.TelePointView"
require "Modules.ModuleWorldMap.ChildView.NPCView"
require "Modules.ModuleWorldMap.ChildView.MonsterView"
require "Modules.ModuleWorldMap.ChildView.PlayerView"
require "Modules.ModuleWorldMap.ChildView.TaskTrackView"
require "Modules.ModuleWorldMap.ChildView.EventActivityView"
require "Modules.ModuleWorldMap.ChildView.InstanceView"
require "Modules.ModuleWorldMap.ChildView.ResourcePointView"
require "Modules.ModuleWorldMap.ChildView.RevivePointView"
require "Modules.ModuleWorldMap.ChildView.RegionLockView"
require "Modules.ModuleWorldMap.ChildView.PVPView"
require "Modules.ModuleWorldMap.ChildView.PathPointView"
require "Modules.ModuleWorldMap.ChildView.GuildClickFlagItemView"
local DistristView = ClassTypes.DistristView
local TelePointView = ClassTypes.TelePointView
local NPCView = ClassTypes.NPCView
local MonsterView = ClassTypes.MonsterView
local PlayerView = ClassTypes.PlayerView
local TaskTrackView = ClassTypes.TaskTrackView
local EventActivityView = ClassTypes.EventActivityView
local InstanceView = ClassTypes.InstanceView
local ResourcePointView = ClassTypes.ResourcePointView
local RevivePointView = ClassTypes.RevivePointView
local RegionLockView = ClassTypes.RegionLockView
local PVPView = ClassTypes.PVPView
local PathPointView = ClassTypes.PathPointView
local GuildClickFlagItemView = ClassTypes.GuildClickFlagItemView

local UIComponentUtil = GameUtil.UIComponentUtil
local MapManager = PlayerManager.MapManager
local TaskManager = PlayerManager.TaskManager
local GuildConquestManager = PlayerManager.GuildConquestManager
local EventActivityManager = PlayerManager.EventActivityManager
local MapDataHelper = GameDataHelper.MapDataHelper
local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper
local RegionalMapDataHelper = GameDataHelper.RegionalMapDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local EventActivityDataHelper = GameDataHelper.EventActivityDataHelper
local GUIManager = GameManager.GUIManager
local TimerHeap = GameWorld.TimerHeap
local GameSceneManager = GameManager.GameSceneManager
local GeometryUtil = GameUtil.GeometryUtil
local InstanceDataHelper = GameDataHelper.InstanceDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local MapConfig = GameConfig.MapConfig
local EntityType = GameConfig.EnumType.EntityType
local StateConfig = GameConfig.StateConfig
local XMapPathPoint = GameMain.XMapPathPoint
local MonsterType = GameConfig.EnumType.MonsterType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local SceneConfig = GameConfig.SceneConfig

local HIDE_POSITION = Vector2(-1000, -1000)
local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)
-- 调整Container_DistrictMap位置得改这里
local canvasWidth = 720
local canvasHeight = 720
local canvasX = 250
local canvasY = 0

function DistristMapView:Awake()
    self._imageDistrict = self:FindChildGO("Image_Map")
    self:InitPool()
    self._onClickFlagFactionChanged = function()self:OnClickFlagFactionChanged() end
end

function DistristMapView:OnDestroy()
    self._imageDistrict = nil
    self:RemoveAllTelPoint()
    self:InitPool()
end

function DistristMapView:InitForWorldMap()
    -- LoggerHelper.Log("InitForWorldMap")
    local go = self:FindChildGO("Container_PathPoints")
    self._mapPathPoint = go:AddComponent(typeof(XMapPathPoint))
    self._pathPointView = self:AddChildLuaUIComponent("Container_PathPoints", PathPointView)
    
    self._pathPointView:SetDeviation(canvasX, canvasY)
    self._pathPointView:SetMapPathPoint(self._mapPathPoint)
    self._pathPointView:SetPointDownCB(function(x, y)self:FindPath(x, y) end)
end

function DistristMapView:OnEnable()
    self._iconBg = self:FindChildGO("Image_BG")
    GameWorld.AddIcon(self._iconBg, 13513)
end

function DistristMapView:OnDisable()
    self._iconBg = nil
    if self._mapPathPoint then
        self._mapPathPoint:RemoveAllPathPoint()
    end
end

function DistristMapView:ShowPath()
    self._mapPathPoint:ShowPath()
end

function DistristMapView:FindPath(screenX, screenY)
    if SceneConfig.SCENE_TYPE_GUILD_CONQUEST == GameSceneManager:GetCurSceneType() and not GuildConquestManager:GetCanMoveByPath() then
        return
    end
    local targetX = screenX / self._xScale + self._downPosition.x
    local targetZ = screenY / self._yScale + self._downPosition.y
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindPosition, self._curMapId, Vector3.New(targetX, 0, targetZ), false, true)
end

function DistristMapView:InitPool()
    self._telPointItemPool = {}
    self._telPointItems = {}
    self._npcItemPool = {}
    self._npcItems = {}
    self._monsterPointItemPool = {}
    self._monsterPointItems = {}
    self._taskItemPool = {}
    self._taskItems = {}
    self._playerItemPool = {}
    self._playerItems = {}
    self._pvpItemPool = {}
    self._pvpItems = {}
    self._monsterItemPool = {}
    self._monsterItems = {}
    self._eventActivityItemPool = {}
    self._eventActivityItems = {}
    self._instanceItemPool = {}
    self._instanceItems = {}
    self._resourcePointItemPool = {}
    self._resourcePointItems = {}
    self._revivePointItemPool = {}
    self._revivePointItems = {}
    self._regionLockItemPool = {}
    self._regionLockItems = {}
    self._guildClickFlagItemPool = {}
    self._guildClickFlagItems = {}
-- self._pathPointItemPool = {}
-- self._pathPointItems = {}
end

function DistristMapView:SetUpdatePlayerInDistristCallback(callback)
    self._onUpdatePlayerInDistrist = callback
end

function DistristMapView:UpdatePlayerInDistrist()
    local entity = GameWorld.Player()
    self._playerPos = self:GetMapPos(entity:GetPosition())
    self._playerRot = entity:GetFace().y
    self:UpdatePlayerPos(entity.id, self._playerPos)
    self:UpdatePlayerRot(entity.id, self._playerRot)
    local tm = MapManager:GetTeamMember()
    if tm ~= nil then
        for k, teamData in pairs(tm) do
            -- if teamData ~= "nil" and entity.id ~= teamData:GetEntityId() then
            -- LoggerHelper.Log("Ash: DistristMapView ShowPlayer: " .. tostring(k) .. " GetEntityId: " .. tostring(teamData:GetEntityId()))
            local ety = GameWorld.GetEntity(teamData:GetEntityId())
            if ety ~= nil then
                local pos = self:GetMapPos(ety:GetPosition())
                -- LoggerHelper.Log("Ash: DistristMapView ShowNPC: " .. tostring(k) .. " " .. tostring(position) .. " " .. tostring(pos))
                self:UpdatePlayerPos(ety.id, pos)
            -- end
            end
        end
    end
    if self._onUpdatePlayerInDistrist ~= nil then
        self._onUpdatePlayerInDistrist()
    end
end

function DistristMapView:UpdatePVPInDistrist()
    local entity = GameWorld.Player()
    self._playerPos = self:GetMapPos(entity:GetPosition())
    self._playerRot = entity:GetFace().y
    self:UpdatePlayerPos(entity.id, self._playerPos)
    self:UpdatePlayerRot(entity.id, self._playerRot)
    local entities = GameWorld.Entities()
    for key, avatar in pairs(entities) do
        if avatar.entityType == EntityType.Avatar then
            local fly = avatar:InState(StateConfig.AVATAR_STATE_FLY)
            local glide = avatar:InState(StateConfig.AVATAR_STATE_GLIDE)
            local pos = self:GetMapPos(avatar:GetPosition())
            self:UpdatePVPPos(avatar.id, pos, fly or glide)
        end
    end
    if self._onUpdatePlayerInDistrist ~= nil then
        self._onUpdatePlayerInDistrist()
    end
end


-- 区域地图逻辑
function DistristMapView:ShowDistrictInfo(mapId, worldMapId, curMapRegInfo, isWorldMap, isWildMap)
    self._curMapId = mapId
    self._curMapRegInfo = curMapRegInfo
    self._isWorldMap = isWorldMap
    -- LoggerHelper.Error("_isWorldMap: " .. tostring(self._isWorldMap))
    if worldMapId ~= nil and curMapRegInfo ~= nil then
        -- 换底图
        -- LoggerHelper.Error("worldMapId: " .. worldMapId .. " " .. WorldMapDataHelper:GetTypeMapIcon(worldMapId))
        GameWorld.AddIcon(self._imageDistrict, WorldMapDataHelper:GetTypeMapIcon(worldMapId), nil)
        --加载地图比例转换参数
        self:LoadCurMapScale(worldMapId)
        -- 加载复活点
        -- self:ShowRevivePoint(mapId)
        -- 加载采集点
        -- self:ShowResourcePoint(mapId)
        -- 加载副本入口
        self:ShowInstance(mapId)
        -- 加载NPC
        self:ShowNPC(mapId)
        if isWildMap then
            -- 加载刷怪点
            self:ShowMonsterPoint(mapId)
        else
            self:RemoveAllMonsterPoint()
        end
        -- 加载任务目标，必须在加载NPC之后
        -- self:ShowTask(mapId)
        -- 加载野外事件
        -- self:ShowEventActivities(mapId, self._isWorldMap)
        if self._isWorldMap then
            -- 加载迷雾
            -- self:ShowRegionLock(mapId)
            -- 加载传送点
            self:ShowTelPoint(curMapRegInfo)
        end
        -- 加载Player，只加载当前区域的玩家信息
        if mapId == GameSceneManager:GetCurrMapID() then
            self:ShowPlayer(mapId)
            self:BeginUpdatePlayerInDistrist()
        else
            self:StopUpdateDistrist()
        end
        self:ShowMonster()
        self:SetAllMonsterPointNameVisiable(true)
        self:SetAllNpcNameVisiable(false)
        self:SetAllInstanceNameVisiable(false)
    end
end

function DistristMapView:StopUpdateDistrist()
    self:RemoveAllPlayer()
    self:RemoveAllPVP()
    self:StopUpdatePlayerInDistrist()
    self:StopUpdatePVPInDistrist()
    self:RemoveAllGuildClickFlag()
end

function DistristMapView:StopUpdatePVPDistrist()
    self:RemoveAllPlayer()
    self:RemoveAllPVP()
    self:StopUpdatePVPInDistrist()
    self:RemoveAllGuildClickFlag()
end

-- PVP地图逻辑
function DistristMapView:ShowPVPInfo(mapId, worldMapId, curMapRegInfo, isWorldMap)
    self._curMapId = mapId
    self._curMapRegInfo = curMapRegInfo
    self._isWorldMap = isWorldMap
    if worldMapId ~= nil and curMapRegInfo ~= nil then
        -- 换底图
        GameWorld.AddIcon(self._imageDistrict, WorldMapDataHelper:GetTypeMapIcon(worldMapId), nil)
        --加载地图比例转换参数
        self:LoadCurMapScale(worldMapId)
        self:RemoveAllInstance()
        self:RemoveAllNPC()
        self:RemoveAllMonsterPoint()
        self:RemoveAllTelPoint()
        if self._isWorldMap then
            -- 加载传送点
            self:ShowTelPoint(curMapRegInfo)
        end
        -- 加载Player，只加载当前区域的玩家信息
        if mapId == GameSceneManager:GetCurrMapID() then
            self:ShowPVP(mapId, true)
            self:BeginUpdatePVPInDistrist()
        else
            self:StopUpdatePVPDistrist()
        end
    end
end

-- 公会争霸地图逻辑
function DistristMapView:ShowGuildClickFlagInfo(mapId, worldMapId, curMapRegInfo, isWorldMap)
    self._curMapId = mapId
    self._curMapRegInfo = curMapRegInfo
    self._isWorldMap = isWorldMap
    if worldMapId ~= nil and curMapRegInfo ~= nil then
        -- 换底图
        GameWorld.AddIcon(self._imageDistrict, WorldMapDataHelper:GetTypeMapIcon(worldMapId), nil)
        --加载地图比例转换参数
        self:LoadCurMapScale(worldMapId)
        self:RemoveAllInstance()
        self:RemoveAllNPC()
        self:RemoveAllMonsterPoint()
        self:RemoveAllTelPoint()
        -- 加载Player，只加载当前区域的玩家信息
        if mapId == GameSceneManager:GetCurrMapID() then
            self:ShowPVP(mapId, false)
            self:BeginUpdatePVPInDistrist()
            self:ShowGuildClickFlag()
        else
            self:StopUpdatePVPDistrist()
            self:RemoveAllGuildClickFlag()
        end
    end
end

function DistristMapView:GetCurMapId()
    return self._curMapId
end

-- 加载野外事件
function DistristMapView:ShowEventActivities(mapId, isWorldMap)
    self:RemoveAllEventActivity()
    if self._isHideTask == true then
        return
    end
    local currEventActivities = EventActivityManager:GetCurrEventActivitiesByMapId(mapId)
    for i, v in ipairs(currEventActivities) do
        local position = EventActivityDataHelper:GetPos(v)
        local pos = self:GetMapPos(position)
        local iconId = EventActivityDataHelper:GetIcon(v)
        local name = EventActivityDataHelper:GetName(v)
        self:AddEventActivity(v, pos, iconId, name, isWorldMap)
    end
-- LoggerHelper.Log("Ash: ShowEventActivities: " .. PrintTable:TableToStr(currEventActivities))
end

-- 加载任务目标
function DistristMapView:ShowTask(mapId)
    self:RemoveAllTask()
    -- if self._isHideTask == true then
    --     return
    -- end
    local tracingMapPoint = TaskManager:GetTracingMapPoints(mapId, true)
    for k, v in pairs(tracingMapPoint) do
        local task = v:GetTask()
        local eventItemData = v:GetEventItemData()
        self:AddTask(k, task:GetIcon(), self:GetMapPos(v:GetLocation()))
    end
    
    for k, v in pairs(self._npcItems) do
        local task, eventItemData = TaskManager:GetNPCTask(k)
        -- LoggerHelper.Log("Ash: ShowTask: " .. PrintTable:TableToStr(task))
        if task ~= nil and task:GetIsTrack() then
            self:AddTask(k, task:GetIcon(), v:GetPosition())
        end
    end
end

-- 加载传送点
function DistristMapView:ShowTelPoint(curMapRegInfo)
    self:RemoveAllTelPoint()
    -- if self._isHideFunc == true then
    --     return
    -- end
    for i, v in ipairs(curMapRegInfo) do
        self:AddTelPoint(v)
    end
end

-- 加载复活点
function DistristMapView:ShowRevivePoint(mapId)
    self:RemoveAllRevivePoint()
    -- if self._isHideFunc == true then
    --     return
    -- end
    local revivePoints = MapManager:GetRevivePoint(mapId)
    for k, v in pairs(revivePoints) do
        local pos = self:GetMapPos(v)
        self:AddRevivePoint(k, pos)
    end
end

-- 加载区域迷雾
function DistristMapView:ShowRegionLock(mapId)
    self:RemoveAllRegionLock()
    local regionLock = MapManager:GetRegionLock(mapId)
    for k, v in pairs(regionLock) do
        local item = self:AddRegionLock(k, v)
        item:ResetFadeOut()
    end
    local regionUnlock = MapManager:GetFadeOutRegionLock(mapId)
    for k, v in pairs(regionUnlock) do
        local item = self:AddRegionLock(k, v)
        item:AddFadeOut()
    end
end

-- 加载NPC
function DistristMapView:ShowNPC(mapId)
    self:RemoveAllNPC()
    -- if self._isHideFunc == true then
    --     return
    -- end
    local npcs = MapManager:GetNPCByMapId(mapId)
    for k, position in pairs(npcs) do
        local pos = self:GetMapPos(position)
        -- LoggerHelper.Log("Ash: DistristMapView ShowNPC: " .. tostring(k) .. " " .. tostring(position) .. " " .. tostring(pos))
        self:AddNPC(k, pos, MapConfig.NPC_ICON, NPCDataHelper.GetNPCName(k))
    end
end

-- 加载刷怪点
function DistristMapView:ShowMonsterPoint(mapId)
    self:RemoveAllMonsterPoint()
    local monsters = MapManager:GetMonsterByMapId(mapId)
    -- LoggerHelper.Log("Ash: DistristMapView ShowMonsterPoint: " .. tostring(mapId) .. " " .. PrintTable:TableToStr(monsters))
    for k, v in pairs(monsters) do
        local monsterId = v.monster_id
        local type = MonsterDataHelper.GetMonsterType(monsterId)
        local name = MonsterDataHelper.GetMonsterName(monsterId)
        local pos = self:GetMapPos(v.truePos)
        -- LoggerHelper.Log("Ash: DistristMapView ShowMonsterPoint: " .. tostring(k) .. " " .. name .. " " .. tostring(v.truePos) .. " " .. PrintTable:TableToStr(pos))
        self:AddMonsterPoint(k, pos, MonsterType.IsBoss(type) and MapConfig.BOSS_ICON or MapConfig.MONSTER_ICON, name)
    end
end

-- 加载副本入口
function DistristMapView:ShowInstance(mapId)
    self:RemoveAllInstance()
    local teleportInfos = MapManager:GetTeleportInfo(mapId)
    for k, entity in pairs(teleportInfos) do
        if entity.target_scene ~= 0 then
            local pos = self:GetMapPos(entity.pos)
            -- LoggerHelper.Log("Ash: DistristMapView ShowInstance: entity.target_scene " .. tostring(entity.target_scene))
            local name = LanguageDataHelper.GetContent(MapDataHelper.GetChinese(entity.target_scene) or 0)
            self:AddInstance(k, pos, MapConfig.TELEPORT_ICON, name)
        end
    end
end

-- 加载采集点
function DistristMapView:ShowResourcePoint(mapId)
    self:RemoveAllResourcePoint()
    local resourcePointInfos = MapManager:GetCollectPointInfo(mapId)
    for k, entity in pairs(resourcePointInfos) do
        local iconId = 0
        if entity.res_type == 1 then
            iconId = MapConfig.RESOURCE_WOOD_ICON
        elseif entity.res_type == 2 then
            iconId = MapConfig.RESOURCE_PLANT_ICON
        elseif entity.res_type == 3 then
            iconId = MapConfig.RESOURCE_MINE_ICON
        elseif entity.res_type == 4 then
            iconId = MapConfig.RESOURCE_GEM_ICON
        end
        local pos = self:GetMapPos(entity.pos)
        self:AddResourcePoint(k, pos, iconId)
    -- LoggerHelper.Log("Ash: DistristMapView ShowResourcePoint: " .. tostring(k) .. " " .. tostring(entity.res_type))
    end
end

-- 加载Player
function DistristMapView:ShowPlayer(mapId)
    self:RemoveAllPlayer()
    local entity = GameWorld.Player()
    self._playerPos = self:GetMapPos(entity:GetPosition())
    self._playerRot = entity:GetFace().y
    local item = self:AddPlayer(entity.id, true, self._playerPos, entity.name)
    item:SetRotation(self._playerRot)
    -- LoggerHelper.Log("Ash: DistristMapView ShowPlayer: " .. tostring(entity.id) .. PrintTable:TableToStr(entity:GetPosition()) .. PrintTable:TableToStr(self._playerPos))
    local tm = MapManager:GetTeamMember()
    if tm ~= nil then
        for k, teamData in pairs(tm) do
            -- if teamData ~= "nil" and entity.id ~= teamData:GetEntityId() and mapId == teamData:GetMapId() then
            -- LoggerHelper.Log("Ash: DistristMapView ShowPlayer: " .. tostring(k) .. " GetEntityId: " .. tostring(teamData:GetEntityId()))
            local ety = GameWorld.GetEntity(teamData:GetEntityId())
            if ety ~= nil then
                local pos = self:GetMapPos(ety:GetPosition())
                -- LoggerHelper.Log("Ash: DistristMapView ShowNPC: " .. tostring(k) .. " " .. tostring(position) .. " " .. tostring(pos))
                self:AddPlayer(teamData:GetEntityId(), false, pos, ety.name)
            -- end
            end
        end
    end
end

-- 加载PVP
function DistristMapView:ShowPVP(mapId, isShowEnemy)
    self:RemoveAllPlayer()
    self:RemoveAllPVP()
    local entity = GameWorld.Player()
    local playerFaction = entity.faction_id
    self._playerPos = self:GetMapPos(entity:GetPosition())
    self._playerRot = entity:GetFace().y
    local item = self:AddPlayer(entity.id, true, self._playerPos, entity.name)
    item:SetRotation(self._playerRot)
    -- LoggerHelper.Log("Ash: DistristMapView ShowPlayer: " .. tostring(entity.id))
    local entities = GameWorld.Entities()
    for key, avatar in pairs(entities) do
        if avatar.entityType == EntityType.Avatar and entity.id ~= avatar.id then
            if isShowEnemy then
                self:AddAvatarByEty(avatar, playerFaction)
            else
                if avatar.faction_id == playerFaction then
                    self:AddAvatarByEty(avatar, playerFaction)
                end
            end
        end
    end
end

function DistristMapView:AddAvatarByEty(avatar, playerFaction)
    if avatar ~= nil then
        if playerFaction == nil then
            playerFaction = GameWorld.Player().faction_id
        end
        local fly = avatar:InState(StateConfig.AVATAR_STATE_FLY)
        local glide = avatar:InState(StateConfig.AVATAR_STATE_GLIDE)
        local isMember = false
        if avatar.faction_id == playerFaction then
            isMember = true
        end
        local pos = self:GetMapPos(avatar:GetPosition())
        self:AddPVP(avatar.id, isMember, pos, avatar.name, fly or glide)
    end
end

function DistristMapView:RemoveAvatarByEtyId(id)
    -- LoggerHelper.Log("Ash: DistristMapView RemoveMonsterByEtyId: " .. tostring(id))
    self:RemovePVP(id)
end
-- 加载怪物
function DistristMapView:ShowMonster()
    self:RemoveAllMonster()
    local monster = GameWorld.GetEntitiesByType(EntityType.Monster)
    local player = GameWorld.Player()
    -- LoggerHelper.Log("Ash: DistristMapView ShowMonster: " .. tostring(#monster))
    for i, v in pairs(monster) do
        self:AddMonsterByEty(v, player)
    end
end

function DistristMapView:AddMonsterByEty(ety, player)
    if ety ~= nil then
        local pos = self:GetMapPos(ety:GetPosition())
        -- LoggerHelper.Log("Ash: DistristMapView AddMonsterByEtyId: " .. tostring(ety.id) .. " " .. tostring(pos) .. " " .. ety.faction_id .. " " .. player.faction_id)
        self:AddMonster(ety.id, pos, ety.faction_id == player.faction_id)
    -- end
    end
end

function DistristMapView:RemoveMonsterByEtyId(id)
    -- LoggerHelper.Log("Ash: DistristMapView RemoveMonsterByEtyId: " .. tostring(id))
    self:RemoveMonster(id)
end

function DistristMapView:UpdateMonster()
    local monster = GameWorld.GetEntitiesByType(EntityType.Monster)
    local playerFaction = GameWorld.Player().faction_id
    -- LoggerHelper.Log("Ash: DistristMapView UpdateMonster: " .. tostring(#monster))
    for i, ety in pairs(monster) do
        if ety ~= nil then
            local pos = self:GetMapPos(ety:GetPosition())
            -- LoggerHelper.Log("Ash: DistristMapView UpdateMonsterPos: ".. tostring(ety.id) .. " " .. tostring(pos))
            self:UpdateMonsterPos(ety.id, pos, ety.faction_id == playerFaction)
        -- end
        end
    end
end

-- 加载公会争霸水晶
function DistristMapView:ShowGuildClickFlag()
    self:RemoveAllGuildClickFlag()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Conquest_Click_Flag_Info, self._onClickFlagFactionChanged)
    local clickFlags = GameSceneManager:GetClickFlagInfo(self._curMapId)
    local clickFlagInfo = GuildConquestManager:GetClickFlagInfo()
    -- local clickFlag = GameWorld.GetEntitiesByType(EntityType.ClickFlag)
    -- LoggerHelper.Log("Ash: DistristMapView ShowGuildClickFlag: " .. tostring(#clickFlag) .. " " .. EntityType.ClickFlag)
    for id, pos in pairs(clickFlags) do
        self:AddGuildClickFlagByEty(id, pos, clickFlagInfo[id] or 0)
    end
end

function DistristMapView:AddGuildClickFlagByEty(id, position, factionId)
    local pos = self:GetMapPos(position)
    local iconId = 0
    if factionId == 0 then
        iconId = 13416 --未抢资源点
    elseif factionId == GameWorld.Player().faction_id then
        iconId = 13414 --我方资源点
    else
        iconId = 13415 --敌方资源点
    end
    -- LoggerHelper.Log("Ash: DistristMapView AddGuildClickFlagByEty: " .. tostring(ety.id) .. " " .. tostring(pos))
    local flagNames = GlobalParamsHelper.GetMStringValue(921)
    self:AddGuildClickFlag(id, iconId, pos, flagNames[id] or "")
-- ety:AddPropertyListener(EntityConfig.PROPERTY_FACTION_ID, self._onClickFlagFactionChanged)
-- end
end

function DistristMapView:OnClickFlagFactionChanged()
    local playerFactionId = GameWorld.Player().faction_id
    local clickFlagInfo = GuildConquestManager:GetClickFlagInfo()
    for id, factionId in pairs(clickFlagInfo) do
        local iconId = 0
        if factionId == 0 then
            iconId = 13416 --未抢资源点
        elseif factionId == playerFactionId then
            iconId = 13414 --我方资源点
        else
            iconId = 13415 --敌方资源点
        end
        self:UpdateGuildClickFlagIcon(id, iconId)
    end
end

-- function DistristMapView:RemoveGuildClickFlagByEtyId(entityId)
--     local ety = GameWorld.GetEntity(entityId)
--     if ety ~= nil then
--         ety:RemovePropertyListener(EntityConfig.PROPERTY_FACTION_ID, self._onClickFlagFactionChanged)
--     end
--     -- LoggerHelper.Log("Ash: DistristMapView RemoveMonsterByEtyId: " .. tostring(id))
--     self:RemoveGuildClickFlag(entityId)
-- end
function DistristMapView:GetPlayerPos()
    return self._playerPos
end

function DistristMapView:GetPlayerRot()
    return self._playerRot
end

function DistristMapView:BeginUpdatePlayerInDistrist()
    if self._timer == nil then
        self._timer = TimerHeap:AddSecTimer(1, 1, 0, function()self:UpdatePlayerInDistrist() end)
    end
end

function DistristMapView:StopUpdatePlayerInDistrist()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function DistristMapView:BeginUpdatePVPInDistrist()
    if self._pvpTimer == nil then
        self._pvpTimer = TimerHeap:AddSecTimer(1, 1, 0, function()self:UpdatePVPInDistrist() end)
    end
end

function DistristMapView:StopUpdatePVPInDistrist()
    if self._pvpTimer ~= nil then
        TimerHeap:DelTimer(self._pvpTimer)
        self._pvpTimer = nil
    end
end

function DistristMapView:LoadCurMapScale(worldMapId)
    self._downPosition = WorldMapDataHelper:GetDownPosition(worldMapId)
    self._upPosition = WorldMapDataHelper:GetUpPosition(worldMapId)
    self._xScale = canvasWidth / (self._upPosition.x - self._downPosition.x)
    self._yScale = canvasHeight / (self._upPosition.y - self._downPosition.y)
    self._hasLoadScale = true
    if self._mapPathPoint then
        self._mapPathPoint:SetBasicPara(self._upPosition.x, self._upPosition.y,
            self._downPosition.x, self._downPosition.y, canvasWidth, canvasHeight)
    end
-- LoggerHelper.Log("Ash: LoadCurMapScale: " .. PrintTable:TableToStr(self._upPosition) .. " " .. PrintTable:TableToStr(self._downPosition))
end

function DistristMapView:HasLoadScale()
    return self._hasLoadScale
end

function DistristMapView:GetMapPos(realPos, needDeviation)
    if self._hasLoadScale ~= true then
        return Vector2(0, 0)
    end
    local x = (realPos.x - self._downPosition.x) * self._xScale
    local y = (realPos.z - self._downPosition.y) * self._yScale
    if needDeviation then
        return Vector2(x + canvasX, y + canvasY)
    else
        return Vector2(x, y)
    end
end

function DistristMapView:UpdateAllItemsInMiniMap(startPointX, startPointY, screenWidth, screenHeight, rectPoint)
    -- LoggerHelper.Log("Ash: UpdateAllItemsInMiniMap: " .. startPointX .. " " .. startPointY
    -- .. " " .. screenWidth .. " " .. screenHeight .. " " .. PrintTable:TableToStr(self:GetPlayerPos()) .. " " .. PrintTable:TableToStr(rectPoint))
    for k, v in pairs(self._taskItems) do
        local position = v:GetPosition()
        self:UpdateItemInMiniMap(v, position, startPointX, startPointY, screenWidth, screenHeight, rectPoint)
    end
    for k, v in pairs(self._eventActivityItems) do
        local position = v:GetPosition()
        self:UpdateItemInMiniMap(v, position, startPointX, startPointY, screenWidth, screenHeight, rectPoint)
    end
end

function DistristMapView:UpdateItemInMiniMap(item, position, startPointX, startPointY, screenWidth, screenHeight, rectPoint)
    if GeometryUtil.CheckPositionIsInScreen(position, startPointX, startPointY, screenWidth, screenHeight) then --在屏幕范围内
        item.transform.localPosition = position
        return
    end
    -- 在屏幕范围外
    local centerPosition = self:GetPlayerPos()
    for i = 1, 4 do
        local intersection = GeometryUtil.TryGetIntersection(centerPosition, position, rectPoint[i], rectPoint[i + 1])
        if intersection ~= nil then
            item.transform.localPosition = intersection
            return
        end
    end
end

-- 传送点对象池
function DistristMapView:CreateTelPointItem()
    local itemPool = self._telPointItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_TelPoint", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, TelePointView)
    return item
end

function DistristMapView:ReleaseTelPointItem(item)
    local itemPool = self._telPointItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddTelPoint(id)
    local item = self:CreateTelPointItem()
    item.transform.localScale = ShowScale
    self._telPointItems[id] = item
    local trueLocation = RegionalMapDataHelper:GetTrueLocation(id)
    item.transform.localPosition = self:GetMapPos(trueLocation)
    -- local triggerUnlock = MapManager:IsTeleportTriggerUnlock(id)
    -- if triggerUnlock == false then
    --     item:SetLockState(true)
    --     item:SetLockResultText(LanguageDataHelper.CreateContent(10589))-- 请先前往该传送点解锁
    -- else
    --     local levelUnlock = MapManager:IsTeleportLevelUnlock(id)
    --     if levelUnlock == false then
    --         item:SetLockState(true)
    --         item:SetLockResultText(LanguageDataHelper.CreateContent(10588) .. RegionalMapDataHelper:GetTransferLv(id))-- 该传送点解锁等级：
    --     else
    --         item:SetLockState(false)
    --     end
    -- end
    item:SetRegionalMapId(id)
    item:SetBtnName(RegionalMapDataHelper:GetSubtypeButtonName(id))
-- item:SetBtnCB(function(id)self:TelPointBtnClick(id) end)
end

function DistristMapView:RemoveAllTelPoint()
    for k, item in pairs(self._telPointItems) do
        item.transform.localPosition = HIDE_POSITION
        self:ReleaseTelPointItem(item)
    end
    self._telPointItems = {}
end

function DistristMapView:TelPointBtnClick(id)
    -- LoggerHelper.Log("Ash: TelPointBtnClick:" .. tostring(id))
    MapManager:BeginTeleport(id)
end



-- NPC对象池
function DistristMapView:CreateNPCItem()
    local itemPool = self._npcItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_NPC", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, NPCView)
    return item
end

function DistristMapView:ReleaseNPCItem(item)
    local itemPool = self._npcItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddNPC(id, position, iconId, name)
    local item = self:CreateNPCItem()
    item.transform.localScale = ShowScale
    self._npcItems[id] = item
    item:SetName(name)
    item:SetIcon(iconId)
    item:SetPosition(position)
end

function DistristMapView:RemoveAllNPC()
    for k, item in pairs(self._npcItems) do
        item.transform.localScale = HideScale
        self:ReleaseNPCItem(item)
    end
    self._npcItems = {}
end

function DistristMapView:SetAllNpcNameVisiable(flag)
    for k, item in pairs(self._npcItems) do
        item:SetNameVisiable(flag)
    end
end

-- Monster对象池
function DistristMapView:CreateMonsterPointItem()
    local itemPool = self._monsterPointItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Monster", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, MonsterView)
    return item
end

function DistristMapView:ReleaseMonsterPointItem(item)
    local itemPool = self._monsterPointItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddMonsterPoint(id, position, iconId, name)
    local item = self:CreateMonsterPointItem()
    item.transform.localScale = ShowScale
    self._monsterPointItems[id] = item
    item:SetName(name)
    item:SetIcon(iconId)
    item:SetPosition(position)
end

function DistristMapView:RemoveAllMonsterPoint()
    for k, item in pairs(self._monsterPointItems) do
        item.transform.localScale = HideScale
        self:ReleaseMonsterPointItem(item)
    end
    self._monsterPointItems = {}
end

function DistristMapView:SetAllMonsterPointNameVisiable(flag)
    for k, item in pairs(self._monsterPointItems) do
        item:SetNameVisiable(flag)
    end
end

-- Task对象池
function DistristMapView:CreateTaskItem()
    local itemPool = self._taskItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Task", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, TaskTrackView)
    return item
end

function DistristMapView:ReleaseTaskItem(item)
    local itemPool = self._taskItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddTask(id, iconId, position)
    local item = self:CreateTaskItem()
    item.transform.localScale = ShowScale
    self._taskItems[id] = item
    item.transform.localPosition = position
    item:SetPosition(position)
    item:SetIcon(iconId)
end

function DistristMapView:GetTask(id)
    return self._taskItems[id]
end

function DistristMapView:RemoveAllTask()
    for k, item in pairs(self._taskItems) do
        item.transform.localPosition = HIDE_POSITION
        self:ReleaseTaskItem(item)
    end
    self._taskItems = {}
end



-- Player对象池
function DistristMapView:CreatePlayerItem()
    local itemPool = self._playerItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Player", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, PlayerView)
    return item
end

function DistristMapView:ReleasePlayerItem(item)
    local itemPool = self._playerItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddPlayer(id, isMyself, position, name)
    local item = self:CreatePlayerItem()
    item.transform.localScale = ShowScale
    self._playerItems[id] = item
    if isMyself then
        item:SetIsMyself(true)
    else
        item:SetIsMyself(false)
        item:SetIcon(MapConfig.PLAYER_ICON)
    end
    item:SetPosition(position)
    item:SetName(name)
    return item
end

function DistristMapView:UpdatePlayerPos(id, position)
    if self._playerItems[id] ~= nil then
        self._playerItems[id]:SetPosition(position)
    end
end

function DistristMapView:UpdatePlayerRot(id, rotation)
    if self._playerItems[id] ~= nil then
        self._playerItems[id]:SetRotation(rotation)
    end
end

function DistristMapView:RemoveAllPlayer()
    for k, item in pairs(self._playerItems) do
        item.transform.localScale = HideScale
        self:ReleasePlayerItem(item)
    end
    self._playerItems = {}
end

-- PVP对象池
function DistristMapView:CreatePVPItem()
    local itemPool = self._pvpItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_PVP", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, PVPView)
    return item
end

function DistristMapView:ReleasePVPItem(item)
    local itemPool = self._pvpItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddPVP(id, isMember, position, name, isFly)
    -- LoggerHelper.Log("Ash: DistristMapView AddPVP: " .. id .. " " .. tostring(isMember) .. " " .. name)
    local item = self:CreatePVPItem()
    item.transform.localScale = ShowScale
    self._pvpItems[id] = item
    -- LoggerHelper.Log("Ash: DistristMapView AddPVP: " .. PrintTable:TableToStr(item))
    item:SetIsMember(isMember)
    item:SetPosition(position)
    item:SetName(name)
    item:SetIsFly(isFly)
    return item
end

function DistristMapView:UpdatePVPPos(id, position, isFly)
    if self._pvpItems[id] ~= nil then
        self._pvpItems[id]:SetPosition(position)
        self._pvpItems[id]:SetIsFly(isFly)
    end
end

function DistristMapView:UpdatePVPRot(id, rotation)
    if self._pvpItems[id] ~= nil then
        self._pvpItems[id]:SetRotation(rotation)
    end
end

function DistristMapView:RemovePVP(id)
    if self._pvpItems[id] ~= nil then
        self._pvpItems[id].transform.localScale = HideScale
        self:ReleasePVPItem(self._pvpItems[id])
        self._pvpItems[id] = nil
    end
end

function DistristMapView:RemoveAllPVP()
    for k, item in pairs(self._pvpItems) do
        item.transform.localScale = HideScale
        self:ReleasePVPItem(item)
    end
    self._pvpItems = {}
end


-- 怪物对象池
function DistristMapView:CreateMonsterItem()
    local itemPool = self._monsterItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Player", "Container_MonsterRoot")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, PlayerView)
    return item
end

function DistristMapView:ReleaseMonsterItem(item)
    local itemPool = self._monsterItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddMonster(id, position, isMember)
    local item = self:CreateMonsterItem()
    item.transform.localScale = ShowScale
    self._monsterItems[id] = item
    item:SetIsMyself(false)
    item:SetIcon(isMember and MapConfig.PLAYER_ICON or MapConfig.MONSTER_ICON)
    item:SetPosition(position)
    item:SetNameActive(false)
end

function DistristMapView:UpdateMonsterPos(id, position, isMember)
    if self._monsterItems[id] ~= nil then
        self._monsterItems[id]:SetPosition(position)
        self._monsterItems[id]:SetIcon(isMember and MapConfig.PLAYER_ICON or MapConfig.MONSTER_ICON)
    end
end

function DistristMapView:RemoveMonster(id)
    if self._monsterItems[id] ~= nil then
        self._monsterItems[id].transform.localScale = HideScale
        self:ReleaseMonsterItem(self._monsterItems[id])
        self._monsterItems[id] = nil
    end
end

function DistristMapView:RemoveAllMonster()
    for k, item in pairs(self._monsterItems) do
        item.transform.localScale = HideScale
        self:ReleaseMonsterItem(item)
    end
    self._monsterItems = {}
end

-- EventActivity对象池
function DistristMapView:CreateEventActivityItem()
    local itemPool = self._eventActivityItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_EventActivity", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, EventActivityView)
    return item
end

function DistristMapView:ReleaseEventActivityItem(item)
    local itemPool = self._eventActivityItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddEventActivity(id, position, iconId, name, isWorldMap)
    local item = self:CreateEventActivityItem()
    item.transform.localScale = ShowScale
    self._eventActivityItems[id] = item
    if isWorldMap then
        item:SetClick()
    end
    item:SetId(id)
    item:SetIcon(iconId)
    item:SetPosition(position)
    item.transform.localPosition = position
    item:SetName(name)
end

function DistristMapView:GetEventActivity(id)
    return self._eventActivityItems[id]
end

function DistristMapView:TriggerEventActivityClick(id)
    if self._eventActivityItems[id] ~= nil then
        self._eventActivityItems[id]:CurrentClick()
    end
end

function DistristMapView:RemoveAllEventActivity()
    for k, item in pairs(self._eventActivityItems) do
        item.transform.localPosition = HIDE_POSITION
        self:ReleaseEventActivityItem(item)
    end
    self._eventActivityItems = {}
end


-- 副本对象池
function DistristMapView:CreateInstanceItem()
    local itemPool = self._instanceItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Instance", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, InstanceView)
    return item
end

function DistristMapView:ReleaseInstanceItem(item)
    local itemPool = self._instanceItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddInstance(id, position, iconId, name)
    local item = self:CreateInstanceItem()
    item.transform.localScale = ShowScale
    self._instanceItems[id] = item
    item:SetIcon(iconId)
    item:SetName(name)
    item:SetPosition(position)
end

function DistristMapView:RemoveAllInstance()
    for k, item in pairs(self._instanceItems) do
        item.transform.localScale = HideScale
        self:ReleaseInstanceItem(item)
    end
    self._instanceItems = {}
end


function DistristMapView:SetAllInstanceNameVisiable(flag)
    for k, item in pairs(self._instanceItems) do
        item:SetNameVisiable(flag)
    end
end

-- 采集点对象池
function DistristMapView:CreateResourcePointItem()
    local itemPool = self._resourcePointItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_ResourcePoint", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, ResourcePointView)
    return item
end

function DistristMapView:ReleaseResourcePointItem(item)
    local itemPool = self._resourcePointItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddResourcePoint(id, position, iconId)
    local item = self:CreateResourcePointItem()
    item.transform.localScale = ShowScale
    self._resourcePointItems[id] = item
    item:SetIcon(iconId)
    item:SetPosition(position)
end

function DistristMapView:RemoveAllResourcePoint()
    for k, item in pairs(self._resourcePointItems) do
        item.transform.localScale = HideScale
        self:ReleaseResourcePointItem(item)
    end
    self._resourcePointItems = {}
end


-- 复活点对象池
function DistristMapView:CreateRevivePointItem()
    local itemPool = self._revivePointItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_RevivePoint", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, RevivePointView)
    return item
end

function DistristMapView:ReleaseRevivePointItem(item)
    local itemPool = self._revivePointItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddRevivePoint(id, position)
    local item = self:CreateRevivePointItem()
    item.transform.localScale = ShowScale
    self._revivePointItems[id] = item
    item:SetIcon(MapConfig.REVIVE_POINT_ICON)
    item:SetPosition(position)
end

function DistristMapView:RemoveAllRevivePoint()
    for k, item in pairs(self._revivePointItems) do
        item.transform.localScale = HideScale
        self:ReleaseRevivePointItem(item)
    end
    self._revivePointItems = {}
end


-- 区域迷雾对象池
function DistristMapView:CreateRegionLockItem()
    local itemPool = self._regionLockItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_RegionLock", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, RegionLockView)
    return item
end

function DistristMapView:ReleaseRegionLockItem(item)
    local itemPool = self._regionLockItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddRegionLock(id, position)
    local item = self:CreateRegionLockItem()
    item.transform.localScale = ShowScale
    self._regionLockItems[id] = item
    -- item:SetIcon(MapConfig.REVIVE_POINT_ICON)
    item:SetPosition(position)
    return item
end

function DistristMapView:RemoveAllRegionLock()
    for k, item in pairs(self._regionLockItems) do
        item.transform.localScale = HideScale
        self:ReleaseRegionLockItem(item)
    end
    self._regionLockItems = {}
end

-- 公会争霸水晶对象池
function DistristMapView:CreateGuildClickFlagItem()
    local itemPool = self._guildClickFlagItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_GuildClickFlag", "")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, GuildClickFlagItemView)
    return item
end

function DistristMapView:ReleaseGuildClickFlagItem(item)
    local itemPool = self._guildClickFlagItemPool
    table.insert(itemPool, item)
end

function DistristMapView:AddGuildClickFlag(id, iconId, position, name)
    local item = self:CreateGuildClickFlagItem()
    item.transform.localScale = ShowScale
    self._guildClickFlagItems[id] = item
    item:SetIcon(iconId)
    item:SetPosition(position)
    item:SetName(name)
end

function DistristMapView:UpdateGuildClickFlagIcon(id, iconId)
    if self._guildClickFlagItems[id] ~= nil then
        self._guildClickFlagItems[id]:SetIcon(iconId)
    end
end

function DistristMapView:RemoveGuildClickFlag(id)
    if self._guildClickFlagItems[id] ~= nil then
        self._guildClickFlagItems[id].transform.localScale = HideScale
        self:ReleaseGuildClickFlagItem(self._guildClickFlagItems[id])
        self._guildClickFlagItems[id] = nil
    end
end

function DistristMapView:RemoveAllGuildClickFlag()
    for k, item in pairs(self._guildClickFlagItems) do
        item.transform.localScale = HideScale
        self:ReleaseGuildClickFlagItem(item)
    end
    self._guildClickFlagItems = {}
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Conquest_Click_Flag_Info, self._onClickFlagFactionChanged)
end
