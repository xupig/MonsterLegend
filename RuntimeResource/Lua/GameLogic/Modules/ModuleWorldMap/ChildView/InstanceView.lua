-- InstanceView.lua
local InstanceView = Class.InstanceView(ClassTypes.BaseComponent)

InstanceView.interface = GameConfig.ComponentsConfig.Component
InstanceView.classPath = "Modules.ModuleWorldMap.ChildView.InstanceView"
local UIComponentUtil = GameUtil.UIComponentUtil

function InstanceView:Awake()
    self._textInstanceName = self:GetChildComponent("Text_InstanceName", "TextMeshWrapper")
    self._goInstanceName = self:FindChildGO("Text_InstanceName")
    self._imageInstance = self:FindChildGO("Image_Instance")
end

function InstanceView:SetPosition(position)
    -- LoggerHelper.Log("Ash: InstanceView SetPosition:" .. position.x .. " " .. position.y)
    if self._position ~= nil and Vector2.Distance(self._position, position) < 1 then
        return
    end
    self.transform.localPosition = position
    self._position = position
end

function InstanceView:GetPosition()
    return self._position
end

function InstanceView:SetIcon(iconId)
    GameWorld.AddIcon(self._imageInstance, iconId, nil, true)
end

function InstanceView:SetName(name)
    self._textInstanceName.text = name
end

function InstanceView:SetNameVisiable(flag)
    self._goInstanceName:SetActive(flag)
end

function InstanceView:OnDestroy()
    self._imageInstance = nil
    self._textInstanceName = nil
    self._goInstanceName = nil
    self._position = nil
end
