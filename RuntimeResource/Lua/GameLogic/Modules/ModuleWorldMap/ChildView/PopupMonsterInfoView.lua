-- PopupMonsterInfoView.lua
local PopupMonsterInfoView = Class.PopupMonsterInfoView(ClassTypes.BaseComponent)

PopupMonsterInfoView.interface = GameConfig.ComponentsConfig.Component
PopupMonsterInfoView.classPath = "Modules.ModuleWorldMap.ChildView.PopupMonsterInfoView"
local UIComponentUtil = GameUtil.UIComponentUtil
local MonsterFamilyDataHelper = GameDataHelper.MonsterFamilyDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local SceneConfig = GameConfig.SceneConfig
local ConvertStyle = GameMain.LuaFacade.ConvertStyle
local ItemConfig = GameConfig.ItemConfig
local StringStyleUtil = GameUtil.StringStyleUtil
local combatData = PlayerManager.PlayerDataManager.combatData
local DefenceId = 4

local QualityLanguageMap = {
    [ItemConfig.ItemQualityWhite] = 10551, --白
    [ItemConfig.ItemQualityGreen] = 10552, --蓝
    [ItemConfig.ItemQualityBlue] = 10552, --蓝
    [ItemConfig.ItemQualityPurple] = 10553, --紫
    [ItemConfig.ItemQualityOrange] = 10554, --橙
    [ItemConfig.ItemQualityGold] = 10555 --红
}

function PopupMonsterInfoView:Awake()
    self._textRecomandLevelValue = self:GetChildComponent("Text_RecomandLevelValue", "TextMeshWrapper")
    self._textRecomandDefenseValue = self:GetChildComponent("Text_RecomandDefenseValue", "TextMeshWrapper")
    self._textBlue = self:GetChildComponent("Text_Blue", "TextMeshWrapper")
    self._textPurple = self:GetChildComponent("Text_Purple", "TextMeshWrapper")
    self._textBlueValue = self:GetChildComponent("Text_BlueValue", "TextMeshWrapper")
    self._textPurpleValue = self:GetChildComponent("Text_PurpleValue", "TextMeshWrapper")
    self._textStanderExpValue = self:GetChildComponent("Text_StanderExpValue", "TextMeshWrapper")
end

function PopupMonsterInfoView:SetMonsterFamilyId(id)
    local playerAttrData = combatData:GetServerAttrData()
    local playerDefence = playerAttrData and playerAttrData[4] or 0
    self._textRecomandLevelValue.text = MonsterFamilyDataHelper:GetMinLevel(id)
    local monsterDenfence = MonsterFamilyDataHelper:GetBestProtection(id)
    self._textRecomandDefenseValue.text = StringStyleUtil.GetColorStringWithTextMesh(monsterDenfence, playerDefence >= monsterDenfence and StringStyleUtil.green or StringStyleUtil.red)
    local equipmentRate = MonsterFamilyDataHelper:GetEquipmentRate(id)
    if equipmentRate[1] then
        local level = equipmentRate[1][1]
        local quality = equipmentRate[1][2]
        local colorId = ItemConfig.QualityTextMap[quality]
        local text = QualityLanguageMap[quality]
        self._textBlue.text = ConvertStyle(string.format("$(%d,%s)", colorId, level .. LanguageDataHelper.GetContent(10550) .. LanguageDataHelper.GetContent(text) .. "："))
        self._textBlueValue.text = equipmentRate[1][3] .. LanguageDataHelper.CreateContent(58684)
    end
    if equipmentRate[2] then
        local level = equipmentRate[2][1]
        local quality = equipmentRate[2][2]
        local colorId = ItemConfig.QualityTextMap[quality]
        local text = QualityLanguageMap[quality]
        self._textPurple.text = ConvertStyle(string.format("$(%d,%s)", colorId, level .. LanguageDataHelper.GetContent(10550) .. LanguageDataHelper.GetContent(text) .. "："))
        self._textPurpleValue.text = equipmentRate[2][3] .. LanguageDataHelper.CreateContent(58684)
    end
    self._textStanderExpValue.text = StringStyleUtil.GetLongNumberString(MonsterFamilyDataHelper:GetExpExp(id)) .. LanguageDataHelper.CreateContent(58685)
end

function PopupMonsterInfoView:OnDestroy()
    self._textRecomandLevelValue = nil
    self._textRecomandDefenseValue = nil
    self._textBlue = nil
    self._textPurple = nil
    self._textBlueValue = nil
    self._textPurpleValue = nil
    self._textStanderExpValue = nil
end
