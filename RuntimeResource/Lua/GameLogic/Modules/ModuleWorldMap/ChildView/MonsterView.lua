-- MonsterView.lua
local MonsterView = Class.MonsterView(ClassTypes.BaseComponent)

MonsterView.interface = GameConfig.ComponentsConfig.Component
MonsterView.classPath = "Modules.ModuleWorldMap.ChildView.MonsterView"
local UIComponentUtil = GameUtil.UIComponentUtil

function MonsterView:Awake()
    self._textMonsterName = self:GetChildComponent("Text_MonsterName", "TextMeshWrapper")
    self._goMonsterName = self:FindChildGO("Text_MonsterName")
    self._imageMonster = self:FindChildGO("Image_Monster")
end

function MonsterView:SetPosition(position)
    if self._position ~= nil and Vector2.Distance(self._position, position) < 1 then
        return
    end
    self.transform.localPosition = position
    self._position = position
end

function MonsterView:GetPosition()
    return self._position
end

function MonsterView:SetIcon(iconId)
    if iconId ~= nil and iconId ~= 0 then
        self._imageMonster:SetActive(true)
        GameWorld.AddIcon(self._imageMonster, iconId, nil)
    else
        self._imageMonster:SetActive(false)
    end
end

function MonsterView:SetName(name)
    self._textMonsterName.text = name
end

function MonsterView:SetNameVisiable(flag)
    self._goMonsterName:SetActive(flag)
end

function MonsterView:OnDestroy()
    self._textMonsterName = nil
    self._goMonsterName = nil
    self._imageMonster = nil
    self._position = nil
end
