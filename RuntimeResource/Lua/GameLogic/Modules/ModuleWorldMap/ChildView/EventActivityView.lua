-- EventActivityView.lua
local EventActivityView = Class.EventActivityView(ClassTypes.BaseComponent)

EventActivityView.interface = GameConfig.ComponentsConfig.Component
EventActivityView.classPath = "Modules.ModuleWorldMap.ChildView.EventActivityView"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper
local ButtonWrapper = UIExtension.ButtonWrapper
local EventActivityConfig = GameConfig.EventActivityConfig
local EventActivityManager = PlayerManager.EventActivityManager

function EventActivityView:Awake()
    self._textEventActivity = self:GetChildComponent("Text_EventActivity", "TextMeshWrapper")
    self._imageEventActivity = self:FindChildGO("Image_EventActivity")
end

function EventActivityView:SetClick()
    if self._hasSetClick ~= true then
        self.gameObject:AddComponent(typeof(ButtonWrapper))
        self._csBH:AddClick(self.gameObject, function()self:CurrentClick() end)
        self._hasSetClick = true
    end
end

function EventActivityView:SetIcon(iconId)
    GameWorld.AddIcon(self._imageEventActivity, iconId, nil, true)
end

function EventActivityView:SetPosition(position)
    self._localPosition = position
end

function EventActivityView:GetPosition()
    return self._localPosition
end

function EventActivityView:CurrentClick()
    GameManager.GUIManager.ShowPanel(GameConfig.PanelsConfig.PopupEventActivity,
        {
            id = self._id,
            panelType = EventActivityConfig.INFO,
            goCallback = function()self:EventActivityPanelGoClick() end,
        })
end

function EventActivityView:EventActivityPanelGoClick()
    EventActivityManager:GoToEventActivityPos(self._id)
    GameManager.GUIManager.ClosePanel(GameConfig.PanelsConfig.PopupEventActivity)
    GameManager.GUIManager.ClosePanel(GameConfig.PanelsConfig.WorldMap)
end

function EventActivityView:SetId(id)
    self._id = id
end

function EventActivityView:SetName(name)
    self._textEventActivity.text = LanguageDataHelper.CreateContent(name)
end

function EventActivityView:OnDestroy()
    self._textEventActivity = nil
    self._imageEventActivity = nil
end
