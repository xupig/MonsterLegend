-- MapLineItemView.lua
local MapLineItemView = Class.MapLineItemView(ClassTypes.BaseComponent)

MapLineItemView.interface = GameConfig.ComponentsConfig.Component
MapLineItemView.classPath = "Modules.ModuleWorldMap.ChildView.MapLineItemView"
local UIComponentUtil = GameUtil.UIComponentUtil

function MapLineItemView:Awake()
    self._buttonLine = self:FindChildGO("Button_Line")
    self._textName = self:GetChildComponent("Button_Line/Text", "TextMeshWrapper")
end

function MapLineItemView:SetNameAndCallback(name, callback)
    self._textName.text = name
    self._csBH:RemoveClick(self._buttonLine)
    self._csBH:AddClick(self._buttonLine, callback)
end

function MapLineItemView:OnDestroy()
    self._buttonLine = nil
    self._textName = nil
end