-- PopupInstanceInfoView.lua
local PopupInstanceInfoView = Class.PopupInstanceInfoView(ClassTypes.BaseComponent)

PopupInstanceInfoView.interface = GameConfig.ComponentsConfig.Component
PopupInstanceInfoView.classPath = "Modules.ModuleWorldMap.ChildView.PopupInstanceInfoView"
local UIComponentUtil = GameUtil.UIComponentUtil
local InstanceGroupDataHelper = GameDataHelper.InstanceGroupDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local SceneConfig = GameConfig.SceneConfig
local InstanceDataHelper = GameDataHelper.InstanceDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local FontStyleHelper = GameDataHelper.FontStyleHelper

function PopupInstanceInfoView:Awake()
    self._imageMainBG = self:FindChildGO("Container_Banner/Image_MainBG")
    self._textRecomand = self:FindChildGO("Container_Content/Container_Recomand/Text_Recomand")
    self._textRecomand:SetActive(false)
    self._textHead = self:GetChildComponent("Container_Banner/Text_Head", "TextMeshWrapper")
    self._textDetails = self:GetChildComponent("Container_Content/Text_Details", "TextMeshWrapper")
    self._textRecommandPool = {}
    self._textRecommands = {}
end

function PopupInstanceInfoView:SetInstanceId(instanceId)
    local groupId = InstanceGroupDataHelper:GetIdByInstanceId(instanceId)
    if groupId ~= nil then
        GameWorld.AddIcon(self._imageMainBG, InstanceGroupDataHelper:GetGroupPic(groupId))
        self._textHead.text = LanguageDataHelper.CreateContent(InstanceGroupDataHelper:GetName(groupId))
        self._textDetails.text = LanguageDataHelper.CreateContent(InstanceGroupDataHelper:GetDescribe(groupId))
        local instanceList = InstanceGroupDataHelper:GetInstanceId(groupId)
        local fightForce = GameWorld.Player().fight_force
        self:RemoveAllTextRecommand()
        for i, v in ipairs(instanceList) do
            local mapId = InstanceDataHelper.GetInstanceMapId(v)
            -- local recomCe = InstanceDataHelper.GetInstanceRecomCe(v)
            local sceneType = MapDataHelper.GetSceneType(mapId)
            local text = ""
            if sceneType == SceneConfig.SCENE_TYPE_COMMON_BATTLE then
                text = LanguageDataHelper.CreateContent(10578) -- 普通
            elseif sceneType == SceneConfig.SCENE_TYPE_HERO_BATTLE then
                text = LanguageDataHelper.CreateContent(10579) -- 英雄
            elseif sceneType == SceneConfig.SCENE_TYPE_HELL_BATTLE then
                text = LanguageDataHelper.CreateContent(10580) -- 噩梦
            end
            -- text = text .. LanguageDataHelper.CreateContent(10581) .. ": " .. recomCe
            -- local color = nil
            -- if fightForce >= recomCe then
            --     color = FontStyleHelper:GetFontColor(48) -- 白色
            -- else
            --     color = FontStyleHelper:GetFontColor(6) -- 红色
            -- end
            -- self:AddTextRecommand(i, text, color, i)
        end
    end
end

function PopupInstanceInfoView:OnDestroy()
    self._imageCollectPoint = nil
    self._position = nil
    self._textRecommandPool = {}
    self._textRecommands = {}
end

-- 对象池
function PopupInstanceInfoView:CreateTextRecommandItem()
    local itemPool = self._textRecommandPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Content/Container_Recomand/Text_Recomand", "Container_Content/Container_Recomand")
    local item = go:GetComponent("TextMeshWrapper")
    return item
end

function PopupInstanceInfoView:ReleaseTextRecommandItem(item)
    local itemPool = self._textRecommandPool
    table.insert(itemPool, item)
end

function PopupInstanceInfoView:AddTextRecommand(id, text, color, index)
    local item = self:CreateTextRecommandItem()
    self._textRecommands[id] = item
    item.gameObject:SetActive(true)
    item.text = text
    item.color = color
    item.transform:SetSiblingIndex(index)
end

function PopupInstanceInfoView:RemoveAllTextRecommand()
    for k, item in pairs(self._textRecommands) do
        item.gameObject:SetActive(false)
        self:ReleaseTextRecommandItem(item)
    end
    self._textRecommands = {}
end
