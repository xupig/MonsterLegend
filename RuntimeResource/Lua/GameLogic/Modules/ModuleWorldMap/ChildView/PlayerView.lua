-- PlayerView.lua
local PlayerView = Class.PlayerView(ClassTypes.BaseComponent)

PlayerView.interface = GameConfig.ComponentsConfig.Component
PlayerView.classPath = "Modules.ModuleWorldMap.ChildView.PlayerView"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper
local Vector2 = Vector2
local Vector3 = Vector3
local abs = math.abs
local Scale_Hide = Vector3.New(0, 1, 1)
local Scale_Display = Vector3.one

function PlayerView:Awake()
    self._textPlayerName = self:GetChildComponent("Text_PlayerName", "TextMeshWrapper")
    self._textPlayerNameTransform = self._textPlayerName.gameObject.transform
    self._imagePlayer = self:FindChildGO("Image_Player")
    self._imagePlayerTransform = self._imagePlayer.transform
    self._imageMyself = self:FindChildGO("Image_Myself").transform
end

function PlayerView:SetIsMyself(isMyself)
    if isMyself then
        self._imageMyself.localScale = Scale_Display
        self._imagePlayerTransform.localScale = Scale_Hide
        self:SetNameActive(false)
    else
        self._imageMyself.localScale = Scale_Hide
        self._imagePlayerTransform.localScale = Scale_Display
        self:SetNameActive(true)
    end
end

function PlayerView:SetIcon(iconId)
    if self._lastIconId == iconId then
        return
    end
    GameWorld.AddIcon(self._imagePlayer, iconId, nil)
    self._lastIconId = iconId
end

function PlayerView:SetRotation(rotation)
    if self._lastRot ~= nil and abs(self._lastRot - rotation) < 5 then
        return
    end
    self._imageMyself.transform.localEulerAngles = Vector3(0, 0, -rotation * 2)
    self._lastRot = rotation
end

function PlayerView:SetPosition(position)
    if self._lastPos ~= nil and Vector2.Distance(self._lastPos, position) < 2 then
        return
    end
    self.transform.localPosition = position
    self._lastPos = position
end

function PlayerView:SetNameActive(isActive)
    if self._nameActive == isActive then
        return
    end
    self._textPlayerNameTransform.localScale = isActive and Scale_Display or Scale_Hide
    self._nameActive = isActive
end

function PlayerView:SetName(name)
    self._textPlayerName.text = name
end

function PlayerView:OnDestroy()
    self._textPlayerName = nil
    self._textPlayerNameTransform = nil
    self._imagePlayer = nil
    self._imagePlayerTransform = nil
    self._imageMyself = nil
end
