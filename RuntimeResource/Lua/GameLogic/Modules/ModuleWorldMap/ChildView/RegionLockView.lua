-- RegionLockView.lua
local RegionLockView = Class.RegionLockView(ClassTypes.BaseComponent)

RegionLockView.interface = GameConfig.ComponentsConfig.Component
RegionLockView.classPath = "Modules.ModuleWorldMap.ChildView.RegionLockView"
local UIComponentUtil = GameUtil.UIComponentUtil
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local XImageTweenAlpha = GameMain.XImageTweenAlpha

function RegionLockView:Awake()
    self._imageRegionLock = self:FindChildGO("Image_RegionLock")
end

function RegionLockView:SetPosition(position)
    if self._position ~= nil and Vector2.Distance(self._position, position) < 1 then
        return
    end
    self.transform.localPosition = position
    self._position = position
end

function RegionLockView:GetPosition()
    return self._position
end

function RegionLockView:ResetFadeOut()
    if self._hasFadeOut then
        self._imageRegionLock.transform.localPosition = Vector3.New(0, 0, 0)
        self._tweenAlpha:ResetAlpha(1)
        self._hasFadeOut = false
    end
end

function RegionLockView:AddFadeOut()
    self._tweenPosition = self._imageRegionLock:AddComponent(typeof(XGameObjectTweenPosition))
    self._tweenAlpha = self._imageRegionLock:AddComponent(typeof(XImageTweenAlpha))
    self._tweenPosition:SetToPosOnce(Vector3.New(150, 0, 0), 0.4, nil)
    self._tweenAlpha:SetToAlphaOnce(0, 0.4)
    self._hasFadeOut = true
end

-- function RegionLockView:SetIcon(iconId)
--     GameWorld.AddIcon(self._imageRegionLock, iconId, nil)
-- end
-- function RegionLockView:SetName(name)
-- end
function RegionLockView:OnDestroy()
    -- self._imageRegionLock = nil
    self._position = nil
end
