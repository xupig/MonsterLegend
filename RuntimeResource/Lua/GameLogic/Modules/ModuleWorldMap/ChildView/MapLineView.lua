-- MapLineView.lua
local MapLineView = Class.MapLineView(ClassTypes.BaseComponent)

MapLineView.interface = GameConfig.ComponentsConfig.Component
MapLineView.classPath = "Modules.ModuleWorldMap.ChildView.MapLineView"
local UIComponentUtil = GameUtil.UIComponentUtil
require "Modules.ModuleWorldMap.ChildView.MapLineItemView"
local MapLineItemView = ClassTypes.MapLineItemView
local UIToggle = ClassTypes.UIToggle
local MapManager = PlayerManager.MapManager
local GameSceneManager = GameManager.GameSceneManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function MapLineView:Awake()
    self._toggleChangeLine = UIToggle.AddToggle(self.gameObject, "Toggle_ChangeLine")
    self._textChangeLine = self:GetChildComponent("Toggle_ChangeLine/Label", "TextMeshWrapper")
    self._toggleChangeLine:SetOnValueChangedCB(function(state)self:OnClickChangeLine(state) end)
    self._containerLineList = self:FindChildGO("Container_LineList")
    self._mapLineItemPool = {}
    self._mapLineItems = {}
    self._onSpaceGetLineInfo = function(args)self:SpaceGetLineInfo(args) end
    -- self._onSpaceGotoLine = function(args)self:SpaceGotoLine(args) end
    local containerLineListItem = self:FindChildGO("Container_LineList/Container_Item")
    containerLineListItem:SetActive(false)
end

function MapLineView:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.OnSpaceGetLineInfo, self._onSpaceGetLineInfo)
-- EventDispatcher:AddEventListener(GameEvents.OnSpaceGotoLine, self._onSpaceGetLineInfo)
end

function MapLineView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.OnSpaceGetLineInfo, self._onSpaceGetLineInfo)
-- EventDispatcher:RemoveEventListener(GameEvents.OnSpaceGotoLine, self._onSpaceGetLineInfo)
end

function MapLineView:Reset()
    if self._textChangeLine then
        self._textChangeLine.text = GameWorld.Player().map_line .. LanguageDataHelper.GetContent(58686)
    end
    if self._toggleChangeLine then
        self._toggleChangeLine:GetToggleWrapper().isOn = false
    end
    if self._containerLineList then
        self._containerLineList:SetActive(false)
    end
end

-- function MapLineView:SpaceGotoLine(args)
--     LoggerHelper.Log("Ash: SpaceGotoLine:" .. PrintTable:TableToStr(args))
--     self:Reset()
-- end
function MapLineView:SpaceGetLineInfo(args)
    -- LoggerHelper.Log("Ash: SpaceGetLineInfo:" .. PrintTable:TableToStr(args))
    self._containerLineList:SetActive(true)
    self:RemoveAllMapLine()
    local mapId = GameSceneManager:GetCurrMapID()
    local lines = args[mapId]
    if lines then
        for k, v in pairs(lines) do
            self:AddMapLine(k, k .. LanguageDataHelper.GetContent(58686), function()self:ChangeLine(k) end)
        end
    end
end

function MapLineView:ChangeLine(lineId)
    if GameWorld.Player().map_line ~= lineId then
        MapManager:SpaceGoToLine(lineId)
    end
end

function MapLineView:OnClickChangeLine(state)
    if state then
        MapManager:SpaceGetLineInfo()
    else
        self._containerLineList:SetActive(false)
    end
end

function MapLineView:OnDestroy()
    self._toggleChangeLine = nil
    self._containerLineList = nil
    self._mapLineItemPool = nil
    self._mapLineItems = nil
end

-- 分线对象池
function MapLineView:CreateMapLineItem()
    local itemPool = self._mapLineItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_LineList/Container_Item", "Container_LineList")
    go:SetActive(true)
    local item = UIComponentUtil.AddLuaUIComponent(go, MapLineItemView)
    return item
end

function MapLineView:ReleaseMapLineItem(item)
    local itemPool = self._mapLineItemPool
    table.insert(itemPool, item)
end

function MapLineView:AddMapLine(id, name, callback)
    local item = self:CreateMapLineItem()
    self._mapLineItems[id] = item
    item.gameObject:SetActive(true)
    item.transform:SetSiblingIndex(0)
    item:SetNameAndCallback(name, callback)
end

function MapLineView:RemoveAllMapLine()
    for k, item in pairs(self._mapLineItems) do
        item:SetActive(false)
        self:ReleaseMapLineItem(item)
    end
    self._mapLineItems = {}
end
