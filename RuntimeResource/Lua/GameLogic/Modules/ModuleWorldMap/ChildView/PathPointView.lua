-- PathPointView.lua
local PathPointView = Class.PathPointView(ClassTypes.BaseComponent)

PathPointView.interface = GameConfig.ComponentsConfig.PointableComponent
PathPointView.classPath = "Modules.ModuleWorldMap.ChildView.PathPointView"
local UIComponentUtil = GameUtil.UIComponentUtil
local Global = Global

function PathPointView:Awake()

end

function PathPointView:SetMapPathPoint(mapPathPoint)
    self._mapPathPoint = mapPathPoint
end

function PathPointView:SetDeviation(x, y)
    -- LoggerHelper.Log("Ash: PathPointView SetDeviation:" .. x .. " " .. y)
    self._x, self._y = x, y
end

function PathPointView:OnPointerDown(pointerEventData)
    -- GameWorld.Player():StopAutoFight()
    -- LoggerHelper.Log("Ash: PathPointView OnPointerDown:" .. pointerEventData.position.x - self._x .. " " .. pointerEventData.position.y - self._y)
    -- self._mapPathPoint:FindPath
    if self._pointDownCB then
        self._pointDownCB(pointerEventData.position.x / Global.CanvasScaleX - self._x, pointerEventData.position.y / Global.CanvasScaleY - self._y)
    end
end

function PathPointView:OnPointerUp(pointerEventData)

end

function PathPointView:SetPointDownCB(callback)
    self._pointDownCB = callback
end

function PathPointView:OnDestroy()
    self._mapPathPoint = nil
end
