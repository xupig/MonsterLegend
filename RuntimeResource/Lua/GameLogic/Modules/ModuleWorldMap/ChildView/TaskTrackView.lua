-- TaskTrackView.lua
local TaskTrackView = Class.TaskTrackView(ClassTypes.BaseComponent)

TaskTrackView.interface = GameConfig.ComponentsConfig.Component
TaskTrackView.classPath = "Modules.ModuleWorldMap.ChildView.TaskTrackView"
local UIComponentUtil = GameUtil.UIComponentUtil

function TaskTrackView:Awake()
    self._imageTask = self:FindChildGO("Image_Task")
end

function TaskTrackView:SetPosition(position)
    self._localPosition = position
end

function TaskTrackView:GetPosition()
    return self._localPosition
end

function TaskTrackView:SetIcon(iconId)
    GameWorld.AddIcon(self._imageTask, iconId, nil)
end

function TaskTrackView:OnDestroy()
    self._imageTask = nil
end
