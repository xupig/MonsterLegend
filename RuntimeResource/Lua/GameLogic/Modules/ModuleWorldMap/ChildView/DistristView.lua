-- DistristView.lua
local DistristView = Class.DistristView(ClassTypes.BaseComponent)

DistristView.interface = GameConfig.ComponentsConfig.Component
DistristView.classPath = "Modules.ModuleWorldMap.ChildView.DistristView"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
-- local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper
-- local XImageTweenColor = GameMain.XImageTweenColor
local StringStyleUtil = GameUtil.StringStyleUtil
local TaskCommonManager = PlayerManager.TaskCommonManager
local PlayerActionManager = GameManager.PlayerActionManager

function DistristView:Awake()
    self._textDistrict = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._textLevel = self:GetChildComponent("Text_Level", "TextMeshWrapper")
    -- self._btnDistrict = self:FindChildGO("Button_Icon")
    self._containerIcon = self:FindChildGO("Container_Icon")
    self._csBH:AddClick(self.gameObject, function()self:InvokeDistrictCB() end)
end

function DistristView:InvokeDistrictCB()

    if self._isLock == true then
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(58672), 1)-- 该区域尚未解锁
        return
    end
    
    GameWorld.Player():StopAutoFight()
    GameWorld.Player():StopMoveWithoutCallback() 
    PlayerActionManager:StopAllAction()
    TaskCommonManager:SetCurAutoTask(nil, nil)

    if self._callback ~= nil then
        self._callback(self._mapId)
    end
end

-- function DistristView:SetGoName(goName)
--     self._goName = goName
-- end
function DistristView:SetMapId(mapId)
    self._mapId = mapId
end

-- function DistristView:SetRegionalIcon(iconId)
--     GameWorld.AddIcon(self._btnDistrict, iconId)
-- end
-- function DistristView:SetBtnGO(btnGO)
--     -- self._btnDistrict = btnGO
--     self._csBH:AddClick(self._btnDistrict, function()self:InvokeDistrictCB() end)
-- end
function DistristView:SetBtnImage(iconId)
    GameWorld.AddIcon(self._containerIcon, iconId, self._isLock and 7 or 15)
end

-- function DistristView:SetTextWrapper(textWrapper)
--     self._textDistrict = textWrapper
-- end
function DistristView:SetBtnCB(callback)
    self._callback = callback
end

function DistristView:SetBtnName(nameId)
    self._textDistrict.text = LanguageDataHelper.CreateContent(nameId)
end

function DistristView:SetLimitLevel(limitLevel)
    local content = "[" .. limitLevel .. "]" .. LanguageDataHelper.CreateContent(69)
    self._textLevel.text = StringStyleUtil.GetColorStringWithTextMesh(content, self._isLock and "#FF3232" or "#EAE4C2")
end

function DistristView:SetLockState(isLock)
    self._isLock = isLock
-- if isLock then
--     local red = 128 / 255
--     self._tweenColor:ResetColor(gray, gray, gray, 1)
-- else
--     self._tweenColor:ResetColor(1, 1, 1, 1)
-- end
end

-- function DistristView:PlayUnlockAnim()
--     self._isLock = false
--     local gray = 128 / 255
--     self._tweenColor:SetFromToColorOnce(gray, gray, gray, 1, 1, 1, 1, 1, 0.4)
-- end
function DistristView:OnDestroy()
    self._textDistrict = nil
-- self._btnDistrict = nil
end
