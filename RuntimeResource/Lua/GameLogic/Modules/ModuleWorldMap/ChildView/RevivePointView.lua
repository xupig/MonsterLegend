-- RevivePointView.lua
local RevivePointView = Class.RevivePointView(ClassTypes.BaseComponent)

RevivePointView.interface = GameConfig.ComponentsConfig.Component
RevivePointView.classPath = "Modules.ModuleWorldMap.ChildView.RevivePointView"
local UIComponentUtil = GameUtil.UIComponentUtil

function RevivePointView:Awake()
    self._imageRevivePoint = self:FindChildGO("Image_RevivePoint")
end

function RevivePointView:SetPosition(position)
    if self._position ~= nil and Vector2.Distance(self._position, position) < 1 then
        return
    end
    self.transform.localPosition = position
    self._position = position
end

function RevivePointView:GetPosition()
    return self._position
end

function RevivePointView:SetIcon(iconId)
    GameWorld.AddIcon(self._imageRevivePoint, iconId, nil)
end

function RevivePointView:SetName(name)
end

function RevivePointView:OnDestroy()
    self._imageRevivePoint = nil
    self._position = nil
end
