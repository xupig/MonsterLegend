-- PVPView.lua
local PVPView = Class.PVPView(ClassTypes.BaseComponent)

PVPView.interface = GameConfig.ComponentsConfig.Component
PVPView.classPath = "Modules.ModuleWorldMap.ChildView.PVPView"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper
local Vector2 = Vector2
local Vector3 = Vector3
local abs = math.abs
local MapConfig = GameConfig.MapConfig

function PVPView:Awake()
    self._textPlayerName = self:GetChildComponent("Text_PlayerName", "TextMeshWrapper")
    self._imagePlayer = self:FindChildGO("Image_Player")
    self._imageFly = self:FindChildGO("Image_Fly")
    self._imageMember = self:FindChildGO("Image_Member")
    self._imageEmeny = self:FindChildGO("Image_Emeny")
    GameWorld.AddIcon(self._imageMember, MapConfig.PLAYER_ICON)
    GameWorld.AddIcon(self._imageEmeny, MapConfig.MONSTER_ICON)
end

function PVPView:SetIsMember(isMember)
    if isMember then
        self._imageMember:SetActive(true)
        self._imageEmeny:SetActive(false)
    else
        self._imageMember:SetActive(false)
        self._imageEmeny:SetActive(true)
    end
end

function PVPView:SetIsFly(isFly)
        self._imageFly:SetActive(isFly)
end

function PVPView:SetIcon(iconId)
    GameWorld.AddIcon(self._imagePlayer, iconId)
end

function PVPView:SetRotation(rotation)
    if self._lastRot ~= nil and abs(self._lastRot - rotation) < 5 then
        return
    end
    self._imageMyself.transform.localEulerAngles = Vector3(0, 0, -rotation * 2)
    self._lastRot = rotation
end

function PVPView:SetPosition(position)
    if self._lastPos ~= nil and Vector2.Distance(self._lastPos, position) < 2 then
        return
    end
    self.transform.localPosition = position
    self._lastPos = position
end

function PVPView:SetNameActive(isActive)
    self._textPlayerName.gameObject:SetActive(isActive)
end

function PVPView:SetName(name)
    self._textPlayerName.text = name
end

function PVPView:OnDestroy()
    self._textPlayerName = nil
    self._imagePlayer = nil
    self._imageMyself = nil
end
