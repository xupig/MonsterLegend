-- GuildClickFlagItemView.lua
local GuildClickFlagItemView = Class.GuildClickFlagItemView(ClassTypes.BaseComponent)

GuildClickFlagItemView.interface = GameConfig.ComponentsConfig.Component
GuildClickFlagItemView.classPath = "Modules.ModuleWorldMap.ChildView.GuildClickFlagItemView"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function GuildClickFlagItemView:Awake()
    self._imageIcon = self:FindChildGO("Image_GuildClickFlag")
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
end

function GuildClickFlagItemView:SetPosition(position)
    if self._lastPos ~= nil and Vector2.Distance(self._lastPos, position) < 2 then
        return
    end
    self.transform.localPosition = position
    self._lastPos = position
end

function GuildClickFlagItemView:SetIcon(iconId)
    if self._lastIcon == iconId then
        return
    end
    self._lastIcon = iconId
    GameWorld.AddIcon(self._imageIcon, iconId, nil)
end

function GuildClickFlagItemView:SetName(name)
    self._textName.text = name
end

function GuildClickFlagItemView:OnDestroy()
    self._imageIcon = nil
end
