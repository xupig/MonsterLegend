-- TelePointView.lua
local TelePointView = Class.TelePointView(ClassTypes.BaseComponent)

TelePointView.interface = GameConfig.ComponentsConfig.Component
TelePointView.classPath = "Modules.ModuleWorldMap.ChildView.TelePointView"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper

function TelePointView:Awake()
    self._textTelPoint = self:GetChildComponent("Button_TelPoint/Text", "TextMeshWrapper")
    self._btnTelPoint = self:FindChildGO("Button_TelPoint")
    self._imageLock = self:FindChildGO("Image_Lock")
    self._imageUnlock = self:FindChildGO("Image_Unlock")
    self._csBH:AddClick(self._btnTelPoint, function()self:InvokeTelPointCB() end)
end

function TelePointView:InvokeTelPointCB()
    if self._isLock then
        GameManager.GUIManager.ShowText(1, self._lockResultText, 1)
        return
    end
    if self._callback ~= nil then
        self._callback(self._id)
    end
end

function TelePointView:SetRegionalMapId(id)
    self._id = id
end

function TelePointView:SetBtnCB(callback)
    self._callback = callback
end

function TelePointView:SetLockState(isLock)
    self._isLock = isLock
    self._imageLock:SetActive(isLock)
    self._imageUnlock:SetActive(isLock == false)
end

function TelePointView:SetLockResultText(text)
    self._lockResultText = text
end

function TelePointView:SetBtnName(nameId)
    self._textTelPoint.text = LanguageDataHelper.CreateContent(nameId)
end

function TelePointView:OnDestroy()
    self._textTelPoint = nil
    self._btnTelPoint = nil
end
