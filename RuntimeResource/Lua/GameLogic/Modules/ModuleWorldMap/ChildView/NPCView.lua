-- NPCView.lua
local NPCView = Class.NPCView(ClassTypes.BaseComponent)

NPCView.interface = GameConfig.ComponentsConfig.Component
NPCView.classPath = "Modules.ModuleWorldMap.ChildView.NPCView"
local UIComponentUtil = GameUtil.UIComponentUtil

function NPCView:Awake()
    self._textNPCName = self:GetChildComponent("Text_NPCName", "TextMeshWrapper")
    self._goNPCName = self:FindChildGO("Text_NPCName")
    self._imageNPC = self:FindChildGO("Image_NPC")
end

function NPCView:SetPosition(position)
    if self._position ~= nil and Vector2.Distance(self._position, position) < 1 then
        return
    end
    self.transform.localPosition = position
    self._position = position
end

function NPCView:GetPosition()
    return self._position
end

function NPCView:SetIcon(iconId)
    GameWorld.AddIcon(self._imageNPC, iconId, nil)
end

function NPCView:SetName(name)
    self._textNPCName.text = name
end

function NPCView:SetNameVisiable(flag)
    self._goNPCName:SetActive(flag)
end

function NPCView:OnDestroy()
    self._textNPCName = nil
    self._goNPCName = nil
    self._imageNPC = nil
    self._position = nil
end
