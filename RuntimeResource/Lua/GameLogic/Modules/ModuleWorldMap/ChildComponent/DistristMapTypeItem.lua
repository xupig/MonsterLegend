-- DistristMapTypeItem.lua
--交易所购买类型一级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local DistristMapTypeItem = Class.DistristMapTypeItem(UINavigationMenuItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
DistristMapTypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
DistristMapTypeItem.classPath = "Modules.ModuleWorldMap.ChildComponent.DistristMapTypeItem"

function DistristMapTypeItem:Awake()
    UINavigationMenuItem.Awake(self)
    self._textType = self:FindChildGO("Text_TypeName"):GetComponent("TextMeshWrapper")
    self._imgContract = self:FindChildGO("Container_Arrow/Image_Contract")
    self._imgExpand = self:FindChildGO("Container_Arrow/Image_Expand")
    self._imgContractIcon = self:FindChildGO("Container_Arrow/Image_Contract/Container_Icon")
    self._imgExpandIcon = self:FindChildGO("Container_Arrow/Image_Expand/Container_Icon")
    -- self._highlightBg = self:FindChildGO("Image_HighLightBg")
    self._isExpanded = false;
end

function DistristMapTypeItem:OnDestroy()

end

function DistristMapTypeItem:OnPointerDown(pointerEventData)

end

function DistristMapTypeItem:ToggleHightlight()
    if self._isExpanded then
        self._isExpanded = false
        self._imgContract:SetActive(true)
        self._imgExpand:SetActive(false)
    else
        self:SetSelected(not self._isExpanded)
    end
end

function DistristMapTypeItem:SetSelected(b)
    self._isExpanded = b
    -- self._highlightBg:SetActive(b)
    self._imgContract:SetActive(not b)
    self._imgExpand:SetActive(b)
end

--override
function DistristMapTypeItem:OnRefreshData(data)
    if data then
        self._textType.text = data.name --LanguageDataHelper.CreateContent(data)
        GameWorld.AddIcon(self._imgContractIcon, data.icon_bg)
        GameWorld.AddIcon(self._imgExpandIcon, data.icon_fg)
    end
end
