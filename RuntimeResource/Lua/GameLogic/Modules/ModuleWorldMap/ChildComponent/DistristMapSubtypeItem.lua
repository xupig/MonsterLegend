-- DistristMapSubtypeItem.lua
--交易所购买类型二级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local DistristMapSubtypeItem = Class.DistristMapSubtypeItem(UINavigationMenuItem)
local InstanceManager = PlayerManager.InstanceManager
DistristMapSubtypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
DistristMapSubtypeItem.classPath = "Modules.ModuleWorldMap.ChildComponent.DistristMapSubtypeItem"
-- require "UIComponent.Extend.ItemManager"
-- local ItemManager = ClassTypes.ItemManager
function DistristMapSubtypeItem:Awake()
    UINavigationMenuItem.Awake(self)
    
    self._txtSubtypeName = self:GetChildComponent("Text_SubTypeName", "TextMeshWrapper")
    self._txtMiddleName = self:GetChildComponent("Text_MiddleName", "TextMeshWrapper")
    self._txtSubtypeSubName = self:GetChildComponent("Text_SubTypeSubName", "TextMeshWrapper")
    self._highlightBg = self:FindChildGO("Image_Highlight")
    self._containerIcon = self:FindChildGO("Container_Icon")
    self._imageHeadIcon = self:FindChildGO("Image_HeadIcon")
    -- self._imageHeadIcon:AddComponent(typeof(ButtonWrapper))
    self._csBH:AddClick(self._imageHeadIcon, function()self:OnClickLittleShoes() end)
    -- GameWorld.AddIcon(self._imageHeadIcon, InstanceManager:GetLittleShoesIcon(), nil, true)
    self._isHighlight = false
end

function DistristMapSubtypeItem:OnDestroy()

end

function DistristMapSubtypeItem:OnClickLittleShoes()
    -- LoggerHelper.Log("Ash: OnClickLittleShoes:")
    if self._callback then
        self._callback(true)
    end
-- TaskCommonManager:OnClickTrackCurTask(self._data, true)
end

function DistristMapSubtypeItem:ChangeLittleShoes(hasLittleShoes)
    self._hasLittleShoes = hasLittleShoes
    -- LoggerHelper.Log("Ash: OnHasLittleShoesChanged:" .. tostring(hasLittleShoes))
    self:UpdateLittleShoes()
end

function DistristMapSubtypeItem:UpdateLittleShoes()
    if self._isHighlight then
        self._imageHeadIcon:SetActive(true)
    else
        self._imageHeadIcon:SetActive(false)
    end
end

function DistristMapSubtypeItem:FirstClick()
    if self._monsterId then
        EventDispatcher:TriggerEvent(GameEvents.OnShowPopupMonsterInfo, self._pointPos, self._monsterId)
    else
        if self._callback then
            self._callback()
        end
    end
end

function DistristMapSubtypeItem:SecondClick()
    if self._monsterId then
        if self._callback then
            self._callback()
        end
    end
end

-- function DistristMapSubtypeItem:OnPointerDown(pointerEventData)
--     LoggerHelper.Log("Ash: DistristMapSubtypeItem OnPointerDown: self._monsterId " .. tostring(self._monsterId))
-- end
function DistristMapSubtypeItem:ToggleHightlight()
    self._isHighlight = not self._isHighlight;
    self._highlightBg:SetActive(self._isHighlight)
    self:UpdateLittleShoes()
end

--override
function DistristMapSubtypeItem:OnRefreshData(subTypeData)
    --self._subTypeData = subTypeData
    if subTypeData then
        self._monsterId = subTypeData.monsterId
        if subTypeData.middleName then
            self._txtMiddleName.text = subTypeData.middleName or ""
            self._txtSubtypeName.text = ""
            self._txtSubtypeSubName.text = ""
        else
            self._txtMiddleName.text = ""
            self._txtSubtypeName.text = subTypeData.name or ""
            self._txtSubtypeSubName.text = subTypeData.subName or ""
        end
        if subTypeData.icon then
            GameWorld.AddIcon(self._containerIcon, subTypeData.icon)
        end
        self._pointPos = subTypeData.pointPos
        self._callback = subTypeData.callback
        local hasLittleShoes = InstanceManager:GetHasLittleShoes()
        self:ChangeLittleShoes(hasLittleShoes)
    end
end
