ApplyForGuildModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function ApplyForGuildModule.Init()
    GUIManager.AddPanel(PanelsConfig.ApplyForGuild,"Panel_ApplyForGuild",GUILayer.LayerUIPanel,"Modules.ModuleApplyForGuild.ApplyForGuildPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return ApplyForGuildModule