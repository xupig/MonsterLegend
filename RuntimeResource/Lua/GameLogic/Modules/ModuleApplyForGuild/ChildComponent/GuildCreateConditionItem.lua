require "UIComponent.Extend.ItemGrid"

local GuildCreateConditionItem = Class.GuildCreateConditionItem(ClassTypes.BaseLuaUIComponent)

GuildCreateConditionItem.interface = GameConfig.ComponentsConfig.Component
GuildCreateConditionItem.classPath = "Modules.ModuleApplyForGuild.ChildComponent.GuildCreateConditionItem"

local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local GuildManager = PlayerManager.GuildManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GuildDataHelper = GameDataHelper.GuildDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local GameSceneManager = GameManager.GameSceneManager
local guildData = PlayerManager.PlayerDataManager.guildData
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local UIComponentUtil = GameUtil.UIComponentUtil
local bagData = PlayerManager.PlayerDataManager.bagData
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemConfig = GameConfig.ItemConfig

function GuildCreateConditionItem:Awake()
    self:InitView()
end

function GuildCreateConditionItem:InitView()
    self._notReachGo= self:FindChildGO("Container_NotReach")
    self._getButton = self:FindChildGO("Container_NotReach/Button_Get")
    self._csBH:AddClick(self._getButton,function() self:OnGetTwoButtonClick() end)
    self._buyButton = self:FindChildGO("Container_NotReach/Button_Buy")
    self._csBH:AddClick(self._buyButton,function() self:OnBuyTwoButtonClick() end)
    self._conditionInfoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._tokenIconGo = self:FindChildGO("Container_Icon")
    local itemGo = GUIManager.AddItem(self._tokenIconGo, 1)
    self._tokenIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function()
        TipsManager:ShowItemTipsById(self.itemId)
    end
    self._tokenIcon:SetPointerDownCB(self._func)
end

--override data:{type,id,count} type 0:令牌 1：绑钻
function GuildCreateConditionItem:RefreshData(data)
    self.data = data
    self.itemId = data[2]
    self._tokenIcon:SetItem(data[2])
    self._nameText.text = ItemDataHelper.GetItemNameWithColor(self.itemId)
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self.itemId)
    if data[1] == 0 then
        local condition = (ownCount >= data[3])
        self._notReachGo:SetActive(not condition)
    else
        self._notReachGo:SetActive(false)
        ownCount = GameWorld.Player()[ItemConfig.MoneyTypeMap[self.itemId]]
    end
    self._conditionInfoText.text = BaseUtil.GetColorStringByCount(ownCount, data[3])
end

function GuildCreateConditionItem:OnBuyTwoButtonClick()
    self:BuyGuideTip(125)
end

function GuildCreateConditionItem:BuyGuideTip(itemId)
    if ItemDataHelper.GetGuideBuyCost(itemId)~=0 then
        local btnArgs = {}
        btnArgs.guideBuy = {itemId}
        TipsManager:ShowItemTipsById(itemId,btnArgs)
    end
end

function GuildCreateConditionItem:OnGetTwoButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.On_Get_Two_Button_Click)
end