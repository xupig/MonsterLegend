
local GuildGetTokenItem = Class.GuildGetTokenItem(ClassTypes.UIListItem)

GuildGetTokenItem.interface = GameConfig.ComponentsConfig.Component
GuildGetTokenItem.classPath = "Modules.ModuleApplyForGuild.ChildComponent.GuildGetTokenItem"

local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
UIListItem = ClassTypes.UIListItem
local GuildManager = PlayerManager.GuildManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GuildDataHelper = GameDataHelper.GuildDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig
local GameSceneManager = GameManager.GameSceneManager
local guildData = PlayerManager.PlayerDataManager.guildData

function GuildGetTokenItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end

function GuildGetTokenItem:InitView()
    self._actionButton = self:FindChildGO("Container_content/Button_Action")
    self._csBH:AddClick(self._actionButton, function() self:OnActionButtonClick() end)

    self._nameText = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._mapText = self:GetChildComponent("Container_content/Text_Map", "TextMeshWrapper")
    self._timeText = self:GetChildComponent("Container_content/Text_Time", "TextMeshWrapper")
end

--override data:{id}
function GuildGetTokenItem:OnRefreshData(data)
    self.data = data
    self:ShowInfo()
end

function GuildGetTokenItem:OnActionButtonClick()
    GuildManager:GotoTokenMonster(self.data[1])
    GUIManager.ClosePanel(PanelsConfig.ApplyForGuild)
end

function GuildGetTokenItem:ShowInfo()
    local id = self.data[1]
    self._timeText.text = ""
    local mapId = GuildDataHelper:GetTokenMapId(id)
    local regionId = GuildDataHelper:GetTokenRegionId(id)
    local mapDsecId = GuildDataHelper:GetTokenMapDesc(id)
    local pos = GameSceneManager:GetMonsterPos(mapId, regionId)
    local posStr = string.format("%d,%d", pos.x, pos.z)
    self._mapText.text = LanguageDataHelper.CreateContentWithArgs(mapDsecId, {["0"]=posStr})
    self._nameText.text = GuildDataHelper:GetTokenMonsterName(id)
end

function GuildGetTokenItem:UpdateCountDown()
    local id = self.data[1]
    local rebornTime = guildData.guildTokenMonsterData[id] or 0
    local leftTime = rebornTime - DateTimeUtil:GetServerTime()
    if leftTime < 0 then
        local str = LanguageDataHelper.GetContent(53190)
        self._timeText.text = BaseUtil.GetColorString(str, ColorConfig.J)
    else
        self._timeText.text = BaseUtil.GetColorString(DateTimeUtil:FormatFullTime(leftTime), ColorConfig.H)
    end
end
