
local ApplyForGuildInfoItem = Class.ApplyForGuildInfoItem(ClassTypes.UIListItem)

ApplyForGuildInfoItem.interface = GameConfig.ComponentsConfig.Component
ApplyForGuildInfoItem.classPath = "Modules.ModuleApplyForGuild.ChildComponent.ApplyForGuildInfoItem"

local GUIManager = GameManager.GUIManager
local public_config = require("ServerConfig/public_config")
local guildData = PlayerManager.PlayerDataManager.guildData
UIListItem = ClassTypes.UIListItem
local GuildManager = PlayerManager.GuildManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function ApplyForGuildInfoItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end

function ApplyForGuildInfoItem:InitView()
    self._actionButton = self:FindChildGO("Container_content/Button_Action")
    self._csBH:AddClick(self._actionButton, function() self:OnActionButtonClick() end)
    self._actionWrapper = self._actionButton:GetComponent("ButtonWrapper")
    self._actionButtonText = self:GetChildComponent("Container_content/Button_Action/Text", "TextMeshWrapper")

    self._rankText = self:GetChildComponent("Container_content/Text_Rank", "TextMeshWrapper")
    self._nameText = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._levelText = self:GetChildComponent("Container_content/Text_Level", "TextMeshWrapper")
    self._numText = self:GetChildComponent("Container_content/Text_Num", "TextMeshWrapper")
    self._leaderText = self:GetChildComponent("Container_content/Text_Leader", "TextMeshWrapper")
    self._iconContainer = self:FindChildGO("Container_content/Container_Icon")
    self._iconBGContainer = self:FindChildGO("Container_content/Container_IconBG")
end

--override
function ApplyForGuildInfoItem:OnRefreshData(data)
    self.data = data
    self:ShowInfo(data)
end

function ApplyForGuildInfoItem:ShowSelected(state)

end

function ApplyForGuildInfoItem:OnActionButtonClick()
    local needVerify = self.data[public_config.GUILD_DETAIL_INFO_NEED_VERIFY]
    if needVerify == public_config.GUILD_NEED_VERIFY_NO then
        --加入请求
        GuildManager:ApplyGuildReq(self.guildDbid)
    else
        --申请请求
        --EventDispatcher:TriggerEvent(GameEvents.Show_Guild_Recruit_View, self.data)
        GuildManager:ApplyGuildReq(self.guildDbid)
    end
end

function ApplyForGuildInfoItem:ShowInfo(data)
    self._actionWrapper.interactable = false
    self.guildDbid = data[public_config.GUILD_DETAIL_INFO_DBID]
    self._nameText.text = data[public_config.GUILD_DETAIL_INFO_NAME]
    self._levelText.text = data[public_config.GUILD_DETAIL_INFO_LEVEL]
    self._rankText.text = data[public_config.GUILD_DETAIL_INFO_RANK]
    local curNum = data[public_config.GUILD_DETAIL_INFO_COUNT]
    local maxNum = data[public_config.GUILD_DETAIL_INFO_MAX_COUNT]
    local mark = LanguageDataHelper.GetContent(900)
    self._numText.text = string.format("%d%s%d", curNum, mark, maxNum)
    self._leaderText.text = data[public_config.GUILD_DETAIL_INFO_PRESIDENT_NAME]
    local needVerify = data[public_config.GUILD_DETAIL_INFO_NEED_VERIFY]
    if GuildManager:HasApprove(self.guildDbid) then
        self._actionButtonText.text = LanguageDataHelper.GetContent(53007)
        self._actionWrapper.interactable = false
    else
        if needVerify == public_config.GUILD_NEED_VERIFY_NO then
            self._actionButtonText.text = LanguageDataHelper.GetContent(53006)
            self._actionWrapper.interactable = true
        else
            self._actionButtonText.text = LanguageDataHelper.GetContent(53006)
            self._actionWrapper.interactable = true
        end
    end
    local badge = data[public_config.GUILD_DETAIL_INFO_ICON_ID]
    local flag = data[public_config.GUILD_DETAIL_INFO_FLAG_ID]
    GameWorld.AddIcon(self._iconContainer, badge, nil)
    GameWorld.AddIcon(self._iconBGContainer, flag, nil)
end