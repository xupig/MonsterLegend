require "Modules.ModuleApplyForGuild.ChildComponent.ApplyForGuildInfoItem"
require "Modules.ModuleApplyForGuild.ChildView.CreateGuildView"

local ApplyForGuildPanel = Class.ApplyForGuildPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GuildManager = PlayerManager.GuildManager
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local ApplyForGuildInfoItem = ClassTypes.ApplyForGuildInfoItem
local guildData = PlayerManager.PlayerDataManager.guildData
local GuildManager = PlayerManager.GuildManager
local CreateGuildView = ClassTypes.CreateGuildView

--override
function ApplyForGuildPanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    self:InitCallBack()
    self:InitView()
end

--override
function ApplyForGuildPanel:OnDestroy()
	self._csBH = nil
    self:RemoveEventListeners()
end

function ApplyForGuildPanel:OnShow(data)
    self:AddEventListeners()
    GuildManager:GetGuildListReq(1)
end

function ApplyForGuildPanel:OnClose()
    if self._createGuildGO ~= nil then
        self._createGuildGO:SetActive(false)
        self._createGuildView:OnClose()
    end
    self:RemoveEventListeners()
end

function ApplyForGuildPanel:InitCallBack()
    self._refreshGuildListInfo = function() self:RefreshGuildListInfo() end
    self._onGuildEliteMonsterGetInfo = function()self:OnGuildEliteMonsterGetInfo() end
end

function ApplyForGuildPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_GuildList_Info, self._refreshGuildListInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Guild_Elite_Monster_Get_Info, self._onGuildEliteMonsterGetInfo)
end

function ApplyForGuildPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_GuildList_Info, self._refreshGuildListInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Guild_Elite_Monster_Get_Info, self._onGuildEliteMonsterGetInfo)
end

function ApplyForGuildPanel:InitView()
    self._createButton = self:FindChildGO("Button_Create")
    self._csBH:AddClick(self._createButton,function() self:OnCreateButtonClick() end)
    self._searchButton = self:FindChildGO("Button_Search")
    self._csBH:AddClick(self._searchButton,function() self:OnSearchButtonClick() end)
    self._joinButton = self:FindChildGO("Button_Join")
    self._csBH:AddClick(self._joinButton,function() self:OnJoinButtonClick() end)

    self._createRedPoint = self:FindChildGO("Button_Create/Image_RedPoint")
    self._joinRedPoint = self:FindChildGO("Button_Join/Image_RedPoint")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(ApplyForGuildInfoItem)
    self._list:SetPadding(5, 0, 0, 15)
    self._list:SetGap(5, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 100)

    self._nextCB = function() self:OnRequestNext() end
    self._scrollView:SetOnRequestNextCB(self._nextCB)

    self._inputFieldMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputField_GuildName")
    self._inputFieldMesh:SetOnEndMeshEditCB(function(text) self:OnEndEdit(text) end)
    self._inputText = self:GetChildComponent("InputField_GuildName", 'InputFieldMeshWrapper')

    self._createGuildGO = self:FindChildGO("Container_CreateGuild")
    self._createGuildView = self:AddChildLuaUIComponent("Container_CreateGuild", CreateGuildView)
    self._createGuildGO:SetActive(false)

    self._noneGuildImage = self:FindChildGO("Container_None")
    self._noneGuildImage:SetActive(false)
end

function ApplyForGuildPanel:OnRequestNext()
    local page = guildData.curGuildListPage or 0
    GuildManager:GetGuildListReq(page+1)
end

function ApplyForGuildPanel:OnEndEdit(text)
    local length = string.utf8len(text)
    if length < 4 or length > 8 then
        text = ""
        local showText = LanguageDataHelper.GetContent(53115)
        GUIManager.ShowText(2, showText)
        self._inputText.text = text
    end
    self._searchName = text
end

function ApplyForGuildPanel:OnCreateButtonClick()
    self._createGuildGO:SetActive(true)
    self._createGuildView:OnShow()
end

function ApplyForGuildPanel:OnJoinButtonClick()
    GuildManager:QuickJoinGuildReq()
end

function ApplyForGuildPanel:OnSearchButtonClick()
    local length = string.utf8len(self._searchName)
    if length < 4 or length > 8 then
        local showText = LanguageDataHelper.GetContent(53115)
        GUIManager.ShowText(2, showText)
        return
    end
    --搜索请求
    GuildManager:GuildResearchByNameReq(self._searchName)
end

function ApplyForGuildPanel:RefreshView()
    self._list:SetDataList(guildData.guildListInfo)
    if guildData.guildListInfo == nil or #guildData.guildListInfo == 0 then
        self._noneGuildImage:SetActive(true)
        self._createRedPoint:SetActive(true)
        self._joinRedPoint:SetActive(false)
    else
        self._noneGuildImage:SetActive(false)
        self._createRedPoint:SetActive(false)
        self._joinRedPoint:SetActive(true)
    end
end

function ApplyForGuildPanel:RefreshGuildListInfo()
    self:RefreshView()
end

function ApplyForGuildPanel:OnGuildEliteMonsterGetInfo()
    self._createGuildView:OnGuildEliteMonsterGetInfo()
end

function ApplyForGuildPanel:WinBGType()
    return PanelWinBGType.NormalBG
end