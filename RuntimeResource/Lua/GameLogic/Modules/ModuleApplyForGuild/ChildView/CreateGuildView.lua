require "Modules.ModuleApplyForGuild.ChildView.GuildGetTokenView"
require "UIComponent.Extend.ItemGrid"
require "Modules.ModuleApplyForGuild.ChildComponent.GuildCreateConditionItem"

local CreateGuildView = Class.CreateGuildView(ClassTypes.BaseLuaUIComponent)

CreateGuildView.interface = GameConfig.ComponentsConfig.Component
CreateGuildView.classPath = "Modules.ModuleApplyForGuild.ChildView.CreateGuildView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local GuildManager = PlayerManager.GuildManager
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local GuildDataHelper = GameDataHelper.GuildDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local ShopType = GameConfig.EnumType.ShopType
local GuildGetTokenView = ClassTypes.GuildGetTokenView
local ShopPageType = GameConfig.EnumType.ShopPageType
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local TipsManager = GameManager.TipsManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local GuildCreateConditionItem = ClassTypes.GuildCreateConditionItem
local ItemConfig = GameConfig.ItemConfig

local Index_To_Type = {[0]=0, [1]=1}

function CreateGuildView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._totemId = 1
    self._flagId = 1
    self._condition = false
    self._guildName = ""
    self._flagIconId = 0
    self._totemIconId = 0
    self._selectTab = -1
    self:InitCallBack()
    self:InitView()
end

function CreateGuildView:OnDestroy()

end

function CreateGuildView:OnShow(data)
    self.toggle:SetSelectIndex(1)
    self:OnToggleGroupClick(1)
    self:RefreshView()
end

function CreateGuildView:OnClose()
    if self._getTokenGO ~= nil then
        self._getTokenGO:SetActive(false)
    end
end

function CreateGuildView:OnEnable()
    self:AddEventListeners()
end

function CreateGuildView:OnDisable()
    self:RemoveEventListeners()
end

function CreateGuildView:InitCallBack()
    self._refreshGuildTokenInfo = function() self:RefreshCondition() end
    self._onGetTwoButtonClick = function() self:OnGetTwoButtonClick() end
end

function CreateGuildView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Guild_Token_Info, self._refreshGuildTokenInfo)
    EventDispatcher:AddEventListener(GameEvents.On_Get_Two_Button_Click, self._onGetTwoButtonClick)
end

function CreateGuildView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Guild_Token_Info, self._refreshGuildTokenInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_Get_Two_Button_Click, self._onGetTwoButtonClick)
end

function CreateGuildView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function() self:OnCloseButtonClick() end)
    self._createButton = self:FindChildGO("Button_Create")
    self._csBH:AddClick(self._createButton,function() self:OnCreateButtonClick() end)

    self._preTotemButton = self:FindChildGO("Container_Badge/Container_Totem/Button_Pre")
    self._csBH:AddClick(self._preTotemButton,function() self:OnPreTotemButtonClick() end)
    self._nextTotemButton = self:FindChildGO("Container_Badge/Container_Totem/Button_Next")
    self._csBH:AddClick(self._nextTotemButton,function() self:OnNextTotemButtonClick() end)
    self._preFlagButton = self:FindChildGO("Container_Badge/Container_Flag/Button_Pre")
    self._csBH:AddClick(self._preFlagButton,function() self:OnPreFlagButtonClick() end)
    self._nextFlagButton = self:FindChildGO("Container_Badge/Container_Flag/Button_Next")
    self._csBH:AddClick(self._nextFlagButton,function() self:OnNextFlagButtonClick() end)
    self._totemText = self:GetChildComponent("Container_Badge/Container_Totem/Text_Info", "TextMeshWrapper")
    self._flagText = self:GetChildComponent("Container_Badge/Container_Flag/Text_Info", "TextMeshWrapper")
    self._flagIcon = self:FindChildGO("Container_Badge/Container_Icon")
    self._totemIcon = self:FindChildGO("Container_Badge/Container_BadgeIcon")

    self._inputFieldMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputField_GuildName")
    self._inputFieldMesh:SetOnEndMeshEditCB(function(text) self:OnEndEdit(text) end)
    self._inputText = self:GetChildComponent("InputField_GuildName", 'InputFieldMeshWrapper')

    self._getTokenGO = self:FindChildGO("Container_GetToken")
    self._getTokenView = self:AddChildLuaUIComponent("Container_GetToken", GuildGetTokenView)
    self._getTokenGO:SetActive(false)

    self._conditionGOOne = self:FindChildGO("Container_Condition/Container_Condition1")
    self._conditionItemOne = self:AddChildLuaUIComponent("Container_Condition/Container_Condition1", GuildCreateConditionItem)

    self._conditionGOTwo = self:FindChildGO("Container_Condition/Container_Condition2")
    self._conditionItemTwo = self:AddChildLuaUIComponent("Container_Condition/Container_Condition2", GuildCreateConditionItem)

    self:InitToggleGroup()
end

function CreateGuildView:InitToggleGroup()
    self.toggle = UIToggleGroup.AddToggleGroup(self.gameObject, "Container_Condition/ToggleGroup")
	self.toggle:SetOnSelectedIndexChangedCB(function(index) self:OnToggleGroupClick(index) end)
    self.toggle:SetSelectIndex(1)
    self:OnToggleGroupClick(1)
end

function CreateGuildView:OnToggleGroupClick(index)
    if self._selectTab == index then
        return
    end
    self._selectTab = index
    self.type = Index_To_Type[index]
    self:RefreshCondition()
end

function CreateGuildView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function CreateGuildView:OnCreateButtonClick()
    --创建公会请求
    local length = string.utf8len(self._guildName)
    if length < 4 or length > 8 then
        local showText = LanguageDataHelper.GetContent(53115)
        GUIManager.ShowText(2, showText)
        return
    end
    if not self._condition then
        local showText = LanguageDataHelper.GetContent(53116)
        GUIManager.ShowText(2, showText)
        return
    end
    if self._totemIconId == 0 or self._flagIconId == 0 then
        --icon设置有误
        return
    end
    GuildManager:BuildGuild(self._guildName, self._totemIconId, self._flagIconId, self.type)
end

function CreateGuildView:OnEndEdit(text)
    local length = string.utf8len(text)
    if length < 4 or length > 8 then
        local showText = LanguageDataHelper.GetContent(53115)
        GUIManager.ShowText(2, showText)
        self._inputText.text = text
    end
    self._guildName = text
end

function CreateGuildView:OnPreTotemButtonClick()
    local maxCount = #GlobalParamsHelper.GetParamValue(555)
    self._totemId = (self._totemId - 2 + maxCount) % maxCount + 1
    self:RefreshBadge()
end

function CreateGuildView:OnNextTotemButtonClick()
    local maxCount = #GlobalParamsHelper.GetParamValue(555)
    self._totemId = (self._totemId + maxCount) % maxCount + 1
    self:RefreshBadge()
end

function CreateGuildView:OnPreFlagButtonClick()
    local maxCount = #GlobalParamsHelper.GetParamValue(554)
    self._flagId = (self._flagId - 2 + maxCount) % maxCount + 1
    self:RefreshBadge()
end

function CreateGuildView:OnNextFlagButtonClick()
    local maxCount = #GlobalParamsHelper.GetParamValue(554)
    self._flagId = (self._flagId + maxCount) % maxCount + 1
    self:RefreshBadge()
end

function CreateGuildView:OnGetTwoButtonClick()
    self._getTokenGO:SetActive(true)
    self._getTokenView:ShowView()
end

function CreateGuildView:OnBuyTwoButtonClick()
    --self:OnCloseButtonClick()
    --跳转到商城界面
    -- local data ={["id"]=ShopPageType.Common}
    -- GUIManager.ShowPanel(PanelsConfig.Shop, data)
    self:BuyGuideTip(125)
end


function CreateGuildView:BuyGuideTip(itemId)
    if ItemDataHelper.GetGuideBuyCost(itemId)~=0 then
        local btnArgs = {}
        btnArgs.guideBuy = {itemId}
        TipsManager:ShowItemTipsById(itemId,btnArgs)
    end
end


function CreateGuildView:RefreshView()
    self:RandomFlagAndTotem()
    self:RefreshBadge()
    self:RefreshCondition()
end

function CreateGuildView:RandomFlagAndTotem()
    local maxFlagCount = #GlobalParamsHelper.GetParamValue(554)
    local maxTotemCount = #GlobalParamsHelper.GetParamValue(555)
    self._totemId = math.random(1,maxTotemCount)
    self._flagId = math.random(1,maxFlagCount)
end

function CreateGuildView:RefreshBadge()
    self._flagIconId = GlobalParamsHelper.GetParamValue(554)[self._flagId]
    self._totemIconId = GlobalParamsHelper.GetParamValue(555)[self._totemId]
    GameWorld.AddIcon(self._flagIcon, self._flagIconId)
    GameWorld.AddIcon(self._totemIcon, self._totemIconId)
    self._totemText.text = LanguageDataHelper.CreateContentWithArgs(53011, {["0"]=self._totemId})
    self._flagText.text = LanguageDataHelper.CreateContentWithArgs(53012, {["0"]=self._flagId})
end

function CreateGuildView:RefreshCondition()
    local itemId, count = next(GlobalParamsHelper.GetParamValue(556))
    self._conditionItemOne:RefreshData({0, itemId, count})
    local itemId1, count1 = next(GlobalParamsHelper.GetParamValue(942))
    self._conditionItemTwo:RefreshData({1, itemId1, count1})

    if self.type == 0 then
        local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
        self._condition = ownCount >= count
    elseif self.type == 1 then
        local ownCount = GameWorld.Player()[ItemConfig.MoneyTypeMap[itemId1]]
        self._condition = ownCount >= count1
    end
end

function CreateGuildView:OnGuildEliteMonsterGetInfo()
    self._getTokenView:OnGuildEliteMonsterGetInfo()
end