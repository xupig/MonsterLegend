require "Modules.ModuleApplyForGuild.ChildComponent.GuildGetTokenItem"

local GuildGetTokenView = Class.GuildGetTokenView(ClassTypes.BaseLuaUIComponent)

GuildGetTokenView.interface = GameConfig.ComponentsConfig.Component
GuildGetTokenView.classPath = "Modules.ModuleApplyForGuild.ChildView.GuildGetTokenView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local GuildManager = PlayerManager.GuildManager
local guildData = PlayerManager.PlayerDataManager.guildData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GuildGetTokenItem = ClassTypes.GuildGetTokenItem
local BaseUtil = GameUtil.BaseUtil
local bagData = PlayerManager.PlayerDataManager.bagData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function GuildGetTokenView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end

function GuildGetTokenView:OnDestroy()
    self._csBH = nil
end

function GuildGetTokenView:CloseView()

end

function GuildGetTokenView:ShowView(data)
    GuildManager:GetGuildEliteMonsterListReq()
    self:RefreshView()
end

function GuildGetTokenView:OnEnable()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateCountDown() end)
end

function GuildGetTokenView:OnDisable()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function GuildGetTokenView:InitView()
    self._closeButton = self:FindChildGO("Container_PopupBig/Button_Close")
    self._csBH:AddClick(self._closeButton,function() self:OnCloseButtonClick() end)

    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")

    self._titleText = self:GetChildComponent("Container_PopupBig/Text_Title", "TextMeshWrapper")
    self._titleText.text = LanguageDataHelper.CreateContent(53473)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(GuildGetTokenItem)
    self._list:SetPadding(10, 0, 0, 0)
    self._list:SetGap(4, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function GuildGetTokenView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function GuildGetTokenView:RefreshView()
    local itemId, count = next(GlobalParamsHelper.GetParamValue(627))
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    local ownCount = ownCount
    local needCount = count
    local countStr =  BaseUtil.GetColorStringByCount(ownCount, needCount)
    self._infoText.text = LanguageDataHelper.CreateContentWithArgs(53188, {["0"]=countStr})
end

function GuildGetTokenView:UpdateCountDown()
    local length = self._list:GetLength() - 1
    for i = 0, length do
        local item = self._list:GetItem(i)
        item:UpdateCountDown()
    end
end

function GuildGetTokenView:OnGuildEliteMonsterGetInfo()
    local data = GuildDataHelper:GetAllTokenId()
    self._list:SetDataList(data)
end