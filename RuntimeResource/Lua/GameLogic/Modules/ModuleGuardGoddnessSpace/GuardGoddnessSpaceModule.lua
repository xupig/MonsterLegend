GuardGoddnessSpaceModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function GuardGoddnessSpaceModule.Init()
    GUIManager.AddPanel(PanelsConfig.GuardGoddnessSpace,"Panel_GuardGoddnessSpace",GUILayer.LayerUIMiddle,"Modules.ModuleGuardGoddnessSpace.GuardGoddnessSpacePanel",true,PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return GuardGoddnessSpaceModule