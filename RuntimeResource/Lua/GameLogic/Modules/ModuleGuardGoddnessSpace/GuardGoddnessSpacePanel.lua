require "Modules.ModuleGuardGoddnessSpace.ChildComponent.DamageInfoItem"

local GuardGoddnessSpacePanel = Class.GuardGoddnessSpacePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InstanceManager = PlayerManager.InstanceManager
local UIProgressBar = ClassTypes.UIProgressBar
local UIToggleGroup = ClassTypes.UIToggleGroup
local DamageInfoItem = ClassTypes.DamageInfoItem
local StringStyleUtil = GameUtil.StringStyleUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper

local MAX_COUNT = 3
local MAX_STAR_COUNT = 3

--override
function GuardGoddnessSpacePanel:Awake()
    self.disableCameraCulling = true
    self._startTime = 0
    self._death = {}
    self._waveTimeShow = false
    self._instanceTimeShow = false
    self._startTimeShow = false
    self._hitState = {}
    self._initEntity = {}
    self._entitys = {}
    self:ResetInitState()
    self:InitView()
    self:InitListenerFunc()
end


function GuardGoddnessSpacePanel:InitListenerFunc()
    self._setGuardGoddnessEntityInfo = function(data) self:SetEntity(data) end
    self._setGuardGoddnessDamageInfo = function(data) self:SetGuardGoddnessDamageInfo(data) end
    self._setGuardGoddnessKillInfo = function(data) self:SetGuardGoddnessKillInfo(data) end
    self._setGuardGoddnessStartTime = function(data) self:SetGuardGoddnessStartTime(data) end

    self._onHPOneChangeCB = function()self:OnHPOneChange() end
    self._onHPTwoChangeCB = function()self:OnHPTwoChange() end
    self._onHPThreeChangeCB = function()self:OnHPThreeChange() end
end


--override
function GuardGoddnessSpacePanel:OnDestroy()

end

function GuardGoddnessSpacePanel:OnShow(data)
    self:SetData(data)
end

function GuardGoddnessSpacePanel:SetData(data)
    self:RefreshView(data)
end

function GuardGoddnessSpacePanel:OnClose()
    self:Reset()
end

function GuardGoddnessSpacePanel:OnEnable()
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateCountDown() end)
    self:AddEventListeners()
end

function GuardGoddnessSpacePanel:OnDisable()
    self:RemoveEventListeners()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function GuardGoddnessSpacePanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Set_Guard_Goddness_Entity_Info, self._setGuardGoddnessEntityInfo)
    EventDispatcher:AddEventListener(GameEvents.Set_Guard_Goddness_Damage_Info, self._setGuardGoddnessDamageInfo)
    EventDispatcher:AddEventListener(GameEvents.Set_Guard_Goddness_Kill_Info, self._setGuardGoddnessKillInfo)
    EventDispatcher:AddEventListener(GameEvents.Set_Guard_Goddness_Start_Time, self._setGuardGoddnessStartTime)
end

function GuardGoddnessSpacePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guard_Goddness_Entity_Info, self._setGuardGoddnessEntityInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guard_Goddness_Damage_Info, self._setGuardGoddnessDamageInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guard_Goddness_Kill_Info, self._setGuardGoddnessKillInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guard_Goddness_Start_Time, self._setGuardGoddnessStartTime)
end

function GuardGoddnessSpacePanel:InitView()
    self._infoContainer = self:FindChildGO("Container_Info")
    self._damageContainer = self:FindChildGO("Container_Damage")
    self._monsterInfoContainer = self:FindChildGO("Container_MonsterInfo")
    self._instanceTimeContainer = self:FindChildGO("Container_InstanceTime")
    self._instanceWaveTimeContainer = self:FindChildGO("Container_InstanceWaveTime")
    self._instanceStartTimeContainer = self:FindChildGO("Container_InstanceStartTime")

    self._instanceTimeContainer:SetActive(false)
    self._instanceWaveTimeContainer:SetActive(false)
    self._instanceStartTimeContainer:SetActive(false)

    self._waveTimeText = self:GetChildComponent("Container_InstanceWaveTime/Text_Time", "TextMeshWrapper")
    self._instanceTimeText = self:GetChildComponent("Container_InstanceTime/Text_Time", "TextMeshWrapper")
    self._starTimeText = self:GetChildComponent("Container_InstanceStartTime/Text_Time", "TextMeshWrapper")

    self._warningText = self:GetChildComponent("Container_Info/Text_Warning", "TextMeshWrapper")
    self._waveText = self:GetChildComponent("Container_Info/Text_Wave", "TextMeshWrapper")
    self._killMonsterText = self:GetChildComponent("Container_Info/Text_KillMonster", "TextMeshWrapper")
    self._expText = self:GetChildComponent("Container_Info/Text_Exp", "TextMeshWrapper")
    self._targetText = self:GetChildComponent("Container_Info/Text_Target", "TextMeshWrapper")

    self._warningDamageText = self:GetChildComponent("Container_Damage/Text_Warning", "TextMeshWrapper")
    self._rankSelfText = self:GetChildComponent("Container_Damage/Container_SelfRank/Text_Rank", "TextMeshWrapper")
    self._nameSelfText = self:GetChildComponent("Container_Damage/Container_SelfRank/Text_Name", "TextMeshWrapper")
    self._damageSelfText = self:GetChildComponent("Container_Damage/Container_SelfRank/Text_Damage", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Damage/ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(DamageInfoItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self.toggle = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup_Function")
    self.toggle:SetOnSelectedIndexChangedCB(function(index)self:OnToggleGroupClick(index) end)
    self.toggle:SetSelectIndex(0)
    self:OnToggleGroupClick(0)

    self._starInfoText = self:GetChildComponent("Container_Start_Container/Container_Content/Text_Info", "TextMeshWrapper")
    self._starInfoText.text = LanguageDataHelper.CreateContentWithArgs(53229)

    self._starContainer = {}
    for i=0, MAX_STAR_COUNT do
        self._starContainer[i] = self:FindChildGO("Container_Start_Container/Container_Content/Container_"..i)
    end

    self:InitProgress()
    self._monsterInfoContainer:SetActive(false)
end

function GuardGoddnessSpacePanel:InitProgress()
    self._Comp = {}
    self._monsterNameText = {}
    self._monsterIcon = {}
    self._monsterExpText = {}
    self._deadText = {}
    self._actionButton = {}
    for i=1,MAX_COUNT do
        self._Comp[i] = UIProgressBar.AddProgressBar(self.gameObject, string.format("Container_MonsterInfo/Container_Monster%d/ProgressBar", i))
        self._Comp[i]:SetProgress(0)
        self._Comp[i]:ShowTweenAnimate(true)
        self._Comp[i]:SetMutipleTween(true)
        self._Comp[i]:SetIsResetToZero(false)

        self._deadText[i] = self:FindChildGO(string.format("Container_MonsterInfo/Container_Monster%d/Text_Dead", i))
        self._monsterNameText[i] = self:GetChildComponent(string.format("Container_MonsterInfo/Container_Monster%d/Text_Name", i), "TextMeshWrapper")
        self._monsterIcon[i] = self:FindChildGO(string.format("Container_MonsterInfo/Container_Monster%d/Container_Icon", i))
        self._monsterExpText[i] = self:GetChildComponent(string.format("Container_MonsterInfo/Container_Monster%d/Text_Exp", i), "TextMeshWrapper")

        self._actionButton[i] = self:FindChildGO(string.format("Container_MonsterInfo/Container_Monster%d/Button_Action", i))
        if i == 1 then
            self._csBH:AddClick(self._actionButton[i], function() self:OnActionButtonClickOne() end)
        elseif i == 2 then
            self._csBH:AddClick(self._actionButton[i], function() self:OnActionButtonClickTwo() end)
        elseif i == 3 then
            self._csBH:AddClick(self._actionButton[i], function() self:OnActionButtonClickThree() end)
        end
    end
end

function GuardGoddnessSpacePanel:OnActionButtonClickOne()
    if self._eids == nil or self._eids[1] == nil then
        return
    end
    local entity = GameWorld.GetEntity(self._eids[1][1])
    if entity ~= nil then
        local pos = entity:GetPosition()
        GameWorld.Player():Move(pos, -1)
    end
end

function GuardGoddnessSpacePanel:OnActionButtonClickTwo()
    if self._eids == nil or self._eids[2]== nil then
        return
    end
    local entity = GameWorld.GetEntity(self._eids[2][1])
    if entity ~= nil then
        local pos = entity:GetPosition()
        GameWorld.Player():Move(pos, -1)
    end
end

function GuardGoddnessSpacePanel:OnActionButtonClickThree()
    if self._eids == nil or self._eids[3] == nil then
        return
    end
    local entity = GameWorld.GetEntity(self._eids[3][1])
    if entity ~= nil then
        local pos = entity:GetPosition()
        GameWorld.Player():Move(pos, -1)
    end
end

--{{eid, monsterId, alive}}
function GuardGoddnessSpacePanel:SetEntity(eids)
    self._eids = eids
end

function GuardGoddnessSpacePanel:CheckInitEntity(index)
    if self._initEntity[index] then
        return
    end

    if self._eids ~= nil and self._eids[index][1] ~= nil then
        local entity = GameWorld.GetEntity(self._eids[index][1])
        local death = self._eids[index][3] == 0
        if entity ~= nil then
            self._entitys[index] = entity
            self._initEntity[index] = true
            if index == 1 then
                entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPOneChangeCB)
                entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPOneChangeCB)
            elseif index == 2 then
                entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPTwoChangeCB)
                entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPTwoChangeCB)
            elseif index == 3 then
                entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPThreeChangeCB)
                entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPThreeChangeCB)
            end
            self:OnHPChange(index)
            self:SetMonsterInfo(index)
        elseif death then
            --已死亡
            self:OnHPChange(index, 0)
            self:SetMonsterInfo(index)
            self._initEntity[index] = true
        end
    end

    self:RefreshStar()
    self._monsterInfoContainer:SetActive(true)
end

function GuardGoddnessSpacePanel:OnHPChange(index, curHP)
    if index == 1 then
        self:OnHPOneChange(curHP)
    elseif index == 2 then
        self:OnHPTwoChange(curHP)
    elseif index == 3 then
        self:OnHPThreeChange(curHP)
    end
end

function GuardGoddnessSpacePanel:CheckDeath()
    for i=1, MAX_COUNT do
        if self._initEntity[i] then
            if self._death[i] == nil and self._eids ~= nil and self._eids[i][3] == 0 then
                self._death[i] = 1
                self._deadText[i]:SetActive(true)
                local monsterId = self._eids[i][2]
                GameWorld.AddIcon(self._monsterIcon[i], MonsterDataHelper.GetIcon(monsterId), 12)
                self:RefreshStar()
            end
        end
    end
end

function GuardGoddnessSpacePanel:RefreshStar()
    local count = 0
    for i=1,MAX_COUNT do
        self._starContainer[i]:SetActive(false)
        if self._death[i] ~= nil then
            count = count + 1
        end
    end
    count = MAX_COUNT - count
    
    self._starContainer[count]:SetActive(true)
end

function GuardGoddnessSpacePanel:SetGuardGoddnessStartTime(data)
    self._startTime = data
end

function GuardGoddnessSpacePanel:SetMonsterInfo(index)
    local monsterId = self._eids[index][2]
    self._monsterNameText[index].text = MonsterDataHelper.GetMonsterName(monsterId)
    local expAdd = GlobalParamsHelper.GetParamValue(657)[monsterId]
    if expAdd ~= nil then
        self._monsterExpText[index].text = LanguageDataHelper.CreateContentWithArgs(53233, {["0"]=expAdd})
    else
        self._monsterExpText[index].text = ""
    end

    local entity = GameWorld.GetEntity(self._eids[index][1])
    if entity ~= nil then
        GameWorld.AddIcon(self._monsterIcon[index], MonsterDataHelper.GetIcon(monsterId))
        self._deadText[index]:SetActive(false)
    else
        GameWorld.AddIcon(self._monsterIcon[index], MonsterDataHelper.GetIcon(monsterId), 12)
        self._deadText[index]:SetActive(true)
    end
end

function GuardGoddnessSpacePanel:OnHPOneChange(curHP)
    local entity = GameWorld.GetEntity(self._eids[1][1])
    if curHP ~= nil then
        curHP = curHP or 0
        self._Comp[1]:SetProgressByValue(curHP, 1)
    elseif entity ~= nil then
        --local entity = GameWorld.GetEntity(self._eids[1][1])
        self._Comp[1]:SetProgressByValue(entity.cur_hp , entity.max_hp)
        if self._hitState[1] == nil then
            self._hitState[1] = 0
        end
    end
    if curHP == 0 then
        self._death[1] = 1
        self:RefreshStar()
    end
end

function GuardGoddnessSpacePanel:OnHPTwoChange(curHP)
    local entity = GameWorld.GetEntity(self._eids[2][1])
    if curHP ~= nil then
        curHP = curHP or 0
        self._Comp[2]:SetProgressByValue(curHP, 1)
    elseif entity ~= nil then
        --local entity = GameWorld.GetEntity(self._eids[2][1])
        self._Comp[2]:SetProgressByValue(entity.cur_hp, entity.max_hp)
        if self._hitState[2] == nil then
            self._hitState[2] = 0
        end
    end
    if curHP == 0 then
        self._death[2] = 1
        self:RefreshStar()
    end
end

function GuardGoddnessSpacePanel:OnHPThreeChange(curHP)
    local entity = GameWorld.GetEntity(self._eids[3][1])
    if curHP ~= nil then
        self._Comp[3]:SetProgressByValue(curHP, 1)
    elseif entity ~= nil then
        --local entity = GameWorld.GetEntity(self._eids[3][1])
        self._Comp[3]:SetProgressByValue(entity.cur_hp , entity.max_hp)
        if self._hitState[3] == nil then
            self._hitState[3] = 0
        end
    end
    if curHP == 0 then
        self._death[3] = 1
        self:RefreshStar()
    end
end

function GuardGoddnessSpacePanel:OnToggleGroupClick(index)
    if index == 0 then
        self._infoContainer:SetActive(true)
        self._damageContainer:SetActive(false)
    else
        self._infoContainer:SetActive(false)
        self._damageContainer:SetActive(true)
    end
end

function GuardGoddnessSpacePanel:RefreshView(data)

end

--data 伤害排行 {rank_1, rank_2,rank_3，self_info} rank_1 = {rank,name,damage} self_info 放在最后一个
function GuardGoddnessSpacePanel:SetGuardGoddnessDamageInfo(data)
    self._rankSelfText.text = data[#data][1]
    self._nameSelfText.text = data[#data][2]
    self._damageSelfText.text = StringStyleUtil.GetLongNumberString(data[#data][3])
    local listData = {}
    for i=1,#data-1 do
        table.insert(listData, data[i])
    end
    self._list:SetDataList(listData)
end

--data 波数杀怪信息 ｛总波数，当前波数，杀怪数，获得经验｝
function GuardGoddnessSpacePanel:SetGuardGoddnessKillInfo(data)
    self._waveText.text = LanguageDataHelper.CreateContentWithArgs(53234, {["0"]=data[2].."/"..data[1]})
    self._killMonsterText.text = LanguageDataHelper.CreateContentWithArgs(53235, {["0"]=data[3]})
    self._expText.text = LanguageDataHelper.CreateContentWithArgs(53236, {["0"]=StringStyleUtil.GetLongNumberString(data[4])})
    self._targetText.text = LanguageDataHelper.CreateContentWithArgs(53237)
end

function GuardGoddnessSpacePanel:CheckShowHit()
    if self._eids ~= nil then
        for i=1,MAX_COUNT do
            if self._hitState[i] ~= nil and self._death[i] == nil then
                self._hitState[i] = self._hitState[i] + 1
                local monsterId = self._eids[i][2]
                if self._hitState[i] == 2 then
                    self._hitState[i] = nil
                    GameWorld.AddIcon(self._monsterIcon[i], MonsterDataHelper.GetIcon(monsterId))
                elseif self._hitState[i] == 1 then
                    GameWorld.AddIcon(self._monsterIcon[i], MonsterDataHelper.GetIcon(monsterId), 7)
                end
            end
        end
    end
end

function GuardGoddnessSpacePanel:UpdateCountDown()
    for i=1,MAX_COUNT do
        self:CheckInitEntity(i)
    end

    self:ShowInstanceStartTime()
    self:ShowInstanceTime()
    self:ShowInstanceWaveTime()
    self:ShowWarningInfo()
    self:CheckDeath()
    self:CheckShowHit()
    self:RefreshStar()
end

function GuardGoddnessSpacePanel:ShowInstanceTime()
    local state = false
    local duration = GuildDataHelper:GetGuildGuardGoddnessTime(GameWorld.Player().world_level)
    local endCountDown = self._startTime + GlobalParamsHelper.GetParamValue(643) + duration - DateTimeUtil.GetServerTime()
    if endCountDown >= 0 and endCountDown <= duration then
        state = true
    end

    if self._instanceTimeShow ~= state then
        self._instanceTimeShow = state
        self._instanceTimeContainer:SetActive(state)
    end
    if self._instanceTimeShow then
        self._instanceTimeText.text = DateTimeUtil:ParseTime(endCountDown)
    end
end

function GuardGoddnessSpacePanel:ShowInstanceStartTime()
    local startCountDown = self._startTime + GlobalParamsHelper.GetParamValue(643) - DateTimeUtil.GetServerTime()
    local state = false
    if startCountDown >= 0 then
        state = true 
    end
    if self._startTimeShow ~= state then
        self._startTimeShow = state
        self._instanceStartTimeContainer:SetActive(state)
    end
    if self._startTimeShow then
        self._starTimeText.text = LanguageDataHelper.CreateContentWithArgs(53250, {["0"]=startCountDown})
    end
end

function GuardGoddnessSpacePanel:ShowInstanceWaveTime()
    local showDura = 15
    local curTime = DateTimeUtil.GetServerTime() - self._startTime - GlobalParamsHelper.GetParamValue(643)
    local waveData = GlobalParamsHelper.GetParamValue(638)
    local showTime = 0
    local state = false
    for i=2,#waveData do
        local data = waveData[i]
        showTime = data[1] - curTime
        if showTime <= showDura and showTime > 0 then
            state = true
            break
        end
    end
    if self._waveTimeShow ~= state then
        self._waveTimeShow = state
        self._instanceWaveTimeContainer:SetActive(state)
    end
    if self._waveTimeShow then
        self._waveTimeText.text = LanguageDataHelper.CreateContentWithArgs(53251, {["0"]=DateTimeUtil:ParseTime(showTime)})
    end
end

function GuardGoddnessSpacePanel:ShowWarningInfo()
    local curTime = math.max(DateTimeUtil.GetServerTime() - self._startTime, 0) -- GlobalParamsHelper.GetParamValue(643)
    curTime = GlobalParamsHelper.GetParamValue(641) - (curTime % GlobalParamsHelper.GetParamValue(641))
    self._warningDamageText.text = LanguageDataHelper.CreateContentWithArgs(53224, {["0"]=curTime})
    self._warningText.text = LanguageDataHelper.CreateContentWithArgs(53224, {["0"]=curTime})
end

function GuardGoddnessSpacePanel:Reset()
    self:ResetInitState()
    self.endTimestamp = 0
    for i=0,MAX_STAR_COUNT do
        self._starContainer[i]:SetActive(false)
    end
    self._startTime = 0
    self._death = {}
    self._instanceTimeShow = false
    self._waveTimeShow = false
    self._startTimeShow = false
    self._eids = nil
    self._monsterInfoContainer:SetActive(false)
    self._hitState = {}
    self._entitys = {}
end

function GuardGoddnessSpacePanel:ResetInitState()
    for i=1,MAX_COUNT do
        self._initEntity[i] = false
        local entity = self._entitys[i]
        if entity ~= nil then
            if i == 1 then
                entity:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPOneChangeCB)
                entity:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPOneChangeCB)
            elseif i == 2 then
                entity:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPTwoChangeCB)
                entity:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPTwoChangeCB)
            elseif i == 3 then
                entity:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPThreeChangeCB)
                entity:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPThreeChangeCB)
            end
        end
    end
end

function GuardGoddnessSpacePanel:WinBGType()
    return PanelWinBGType.NoBG
end