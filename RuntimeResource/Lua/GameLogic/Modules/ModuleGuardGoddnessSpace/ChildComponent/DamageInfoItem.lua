-- DamageInfoItem.lua
local DamageInfoItem = Class.DamageInfoItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local StringStyleUtil = GameUtil.StringStyleUtil

DamageInfoItem.interface = GameConfig.ComponentsConfig.Component
DamageInfoItem.classPath = "Modules.ModuleGuardGoddnessSpace.ChildComponent.DamageInfoItem"

function DamageInfoItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function DamageInfoItem:OnDestroy()
end

function DamageInfoItem:InitView()
    self._rankText = self:GetChildComponent("Text_Rank", "TextMeshWrapper")
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._damageText = self:GetChildComponent("Text_Damage", "TextMeshWrapper")
end

--override {rank,name,damage}
function DamageInfoItem:OnRefreshData(data)
    self._rankText.text = data[1]
    self._nameText.text = data[2]
    self._damageText.text = StringStyleUtil.GetLongNumberString(data[3])
end
