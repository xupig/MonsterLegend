-- TaskPanel.lua
local TaskPanel = Class.TaskPanel(ClassTypes.BasePanel)

require "Modules.ModuleTask.ChildView.MainTaskView"
local MainTaskView = ClassTypes.MainTaskView
require "Modules.ModuleTask.ChildView.CircleTaskView"
local CircleTaskView = ClassTypes.CircleTaskView
require "Modules.ModuleTask.ChildView.WeekTaskView"
local WeekTaskView = ClassTypes.WeekTaskView

local public_config = GameWorld.public_config
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local ButtonWrapper = UIExtension.ButtonWrapper
local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager

function TaskPanel:Awake()
    self:Init()
end

function TaskPanel:OnDestroy()
    self._priorityView = nil
    self._taskId = nil
    self._onTaskChanged = nil
end

function TaskPanel:Init()
	self:InitCallBack()
    self:InitView()
end

function TaskPanel:InitCallBack()
    self._tabClickCB = function (index)
        self:OnToggleGroupClick(index)
    end
end

--tab点击处理
function TaskPanel:OnToggleGroupClick(index)
    if self._firstTabIndex == index then do return end end
    if self._views[index] == nil then do return end end
    if self._firstTabIndex ~= nil then
        self._views[self._firstTabIndex]:SetActive(false)    
    end
    self._firstTabIndex = index
    self._views[index]:SetActive(true)
end


--初始化
function TaskPanel:InitView()
    self._priorityView = self:AddChildLuaUIComponent("Container_Task", MainTaskView)
    self._circleTaskView = self:AddChildLuaUIComponent("Container_Circle_Task", CircleTaskView)
    self._weekTaskView = self:AddChildLuaUIComponent("Container_Week_Task", WeekTaskView)
    self:SetTabClickCallBack(self._tabClickCB)

    self._views = {}
    self._views[1] = self._priorityView
    self._views[2] = self._circleTaskView
    self._views[3] = self._weekTaskView
end

function TaskPanel:CloseBtnClick()
    GUIManager.ClosePanel(PanelsConfig.Task)
end

function TaskPanel:OnShow(data)
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
end

function TaskPanel:OnClose()
    self._firstTabIndex = nil
    for i,v in ipairs(self._views) do
        v:SetActive(false)
    end
end

function TaskPanel:WinBGType()
    return PanelWinBGType.NormalBG
end
