-- MainTaskView.lua
local MainTaskView = Class.MainTaskView(ClassTypes.BaseComponent)

MainTaskView.interface = GameConfig.ComponentsConfig.Component
MainTaskView.classPath = "Modules.ModuleTask.ChildView.MainTaskView"
local UIProgressBar = ClassTypes.UIProgressBar
local public_config = GameWorld.public_config


require "Modules.ModuleTask.ChildView.TaskDetailsView"
local TaskDetailsView = ClassTypes.TaskDetailsView
require "Modules.ModuleTask.ChildComponent.TaskListFirstItem"
local TaskListFirstItem = ClassTypes.TaskListFirstItem
require "Modules.ModuleTask.ChildComponent.TaskListSecondItem"
local TaskListSecondItem = ClassTypes.TaskListSecondItem

local TaskManager = PlayerManager.TaskManager
local UIList = ClassTypes.UIList
local UINavigationMenu = ClassTypes.UINavigationMenu

function MainTaskView:Awake()
    self._preF = -1
    self._preFI = -1
    self._preSI = -1
    self:Init()
end

function MainTaskView:OnEnable()
    -- LoggerHelper.Log("MainTaskView:OnShow" .. PrintTable:TableToStr(data))
    -- self._priorityView:SetTaskInfo(data)
    self:RefreshTaskListData()
    self._menu:SelectFItem(0)
    self:OnMenuSItemClicked(0, 0)
    -- EventDispatcher:AddEventListener(GameEvents.OnTaskChanged, self._onTaskChanged)
end

function MainTaskView:RefreshTaskListData()
    self._menuData = TaskManager:GetNormalTaskMenuData()
    self._menu:SetDataList(self._menuData)
end

function MainTaskView:OnDisable()
    -- EventDispatcher:RemoveEventListener(GameEvents.OnTaskChanged, self._onTaskChanged)
end

function MainTaskView:OnDestroy()
    self._priorityView = nil
    self._taskId = nil
    self._onTaskChanged = nil
end

function MainTaskView:Init()
    self:InitView()
end

--初始化
function MainTaskView:InitView()
    self._priorityView = self:AddChildLuaUIComponent("Container_Content", TaskDetailsView)
    self._onTaskChanged = function()self:RefreshTaskListData() end
    self:InitTaskMenu()
end

function MainTaskView:InitTaskMenu()
    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "Container_Left/NavigationMenu")
	self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(TaskListFirstItem)
	self._menu:SetSecondLevelItemType(TaskListSecondItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(5, 5)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)
end

function MainTaskView:OnMenuFItemClicked(idx)
    if self._preF ~= -1 and self._preF ~= idx then
        local item = self._menu:GetFItem(self._preF)
        item:Reset()
    end
    self._selectedFItem = self._menu:GetFItem(idx)
    self._selectedFItem:ToggleSelected()
    self._preF = idx
end

function MainTaskView:OnMenuSItemClicked(fIndex, sIndex)
    if self._preFI ~= -1 and self._preSI ~= -1 then
        local item = self._menu:GetSItem(self._preFI, self._preSI)
        item:OnSelected(false)
    end
    self._selectedSItem = self._menu:GetSItem(fIndex,sIndex)
    self._selectedSItem:OnSelected(true)
    self._preFI = fIndex
    self._preSI = sIndex
    self:RefreshTaskData()
end

function MainTaskView:RefreshTaskData()
    -- LoggerHelper.Error("menuData = " .. PrintTable:TableToStr(self._menuData))
    -- LoggerHelper.Error("self._preFI ===" .. self._preFI)
    -- LoggerHelper.Error("self._preSI ===" .. self._preSI)
    local task = self._menuData[self._preFI + 1][1 + self._preSI + 1]:GetTask()
    -- local data = TaskManager:GetTaskById(self._taskId)
    self._priorityView:SetTaskInfo(task)
end
