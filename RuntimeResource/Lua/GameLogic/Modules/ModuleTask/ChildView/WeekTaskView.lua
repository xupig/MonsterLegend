-- WeekTaskView.lua
local WeekTaskView = Class.WeekTaskView(ClassTypes.BaseComponent)

WeekTaskView.interface = GameConfig.ComponentsConfig.Component
WeekTaskView.classPath = "Modules.ModuleTask.ChildView.WeekTaskView"

local WeekTaskManager = PlayerManager.WeekTaskManager
local TaskCommonManager = PlayerManager.TaskCommonManager
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local TaskConfig = GameConfig.TaskConfig
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GUIManager = GameManager.GUIManager
local UINavigationMenu = ClassTypes.UINavigationMenu
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local TaskManager = PlayerManager.TaskManager

require"Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem
require "Modules.ModuleTask.ChildComponent.TaskListFirstItem"
local TaskListFirstItem = ClassTypes.TaskListFirstItem
require "Modules.ModuleTask.ChildComponent.TaskListSecondItem"
local TaskListSecondItem = ClassTypes.TaskListSecondItem


function WeekTaskView:Awake()
    self._preF = -1
    self._preFI = -1
    self._preSI = -1
    self:Init()
end

function WeekTaskView:Init()
    self._onWeekTaskUpdate = function() self:UpdataData() end
    self._onWeekTaskPass = function() self:UpdataData() end
    self._onWeekCurCountUpdate = function() self:UpdataData() end
    
    self._titleText = self:GetChildComponent("Container_Content/Container_Title/Image_Title/Text_Title", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Container_Content/Container_Desc/Text_Desc", "TextMeshWrapper")
    self._targetText = self:GetChildComponent("Container_Content/Container_Target/Container_Steps/Container_Step/Text_Content", "TextMeshWrapper")
    self._tipsText = self:GetChildComponent("Container_Progress/Text_Tips", "TextMeshWrapper")
    self._tipsText.text = LanguageDataHelper.CreateContent(53247, GlobalParamsHelper.GetParamValue(636))

    self._buttonGo = self:FindChildGO("Container_Content/Button_Go")
    self._csBH:AddClick(self._buttonGo, function()self:GoBtnClick() end)
    
    self._completeGo = self:FindChildGO("Container_Content/Button_Complete")
    self._csBH:AddClick(self._completeGo, function()self:CompleteBtnClick() end)

    self._progressComp = UIScaleProgressBar.AddProgressBar(self.gameObject,"Container_Progress/ProgressBar_Progress")
    self._progressComp:SetProgress(0)
    self._progressComp:ShowTweenAnimate(true)
    self._progressComp:SetMutipleTween(false)
    self._progressComp:SetIsResetToZero(true)
    self._progressComp:SetTweenTime(0.2)

    self:InitRewardList()
    self:InitTaskMenu()
end

function WeekTaskView:InitTaskMenu()
    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "Container_Week_Task_List/NavigationMenu")
	self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(TaskListFirstItem)
	self._menu:SetSecondLevelItemType(TaskListSecondItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(5, 5)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)
end

function WeekTaskView:OnMenuFItemClicked(idx)
    if self._preF ~= -1 and self._preF ~= idx then
        local item = self._menu:GetFItem(self._preF)
        item:Reset()
    end
    self._selectedFItem = self._menu:GetFItem(idx)
    self._selectedFItem:ToggleSelected()
    self._preF = idx
end

function WeekTaskView:OnMenuSItemClicked(fIndex, sIndex)
    if self._preFI ~= -1 and self._preSI ~= -1 then
        local item = self._menu:GetSItem(self._preFI, self._preSI)
        item:OnSelected(false)
    end
    self._selectedSItem = self._menu:GetSItem(fIndex,sIndex)
    self._selectedSItem:OnSelected(true)
    self._preFI = fIndex
    self._preSI = sIndex
    self:RefreshTaskData()
end

function WeekTaskView:RefreshTaskData()
    local needAcceptTask = WeekTaskManager:NeedAcceptTask()
    local completeTask = WeekTaskManager:IsCompleteWeekTask()
    local weekTaskData = self._menuData[1][2]:GetTask()
    self._titleText.text = weekTaskData:GetTitleNameAndColor()
    self._descText.text = weekTaskData:GetTaskDesc()
    -- if #self._menuData[1] > 1 and self._menuData[1][2] then
    --     local task = self._menuData[1][2]:GetTask()
    --     self._titleText.text = task:GetTitleNameAndColor()
    -- else
    --     -- self._titleText.text = LanguageDataHelper.CreateContent(925)
    -- end
    
    self:UpdateProgress()

    if needAcceptTask then
        self._completeGo:SetActive(false)
    else
        self._completeGo:SetActive(not completeTask)
    end

    if needAcceptTask then
        self._buttonGo:SetActive(true)
        self._rewardList:SetDataList({})
        local data = GlobalParamsHelper.GetParamValue(528)
        for k,v in pairs(data) do
            local npcName = NPCDataHelper.GetNPCName(k)
            self._targetText.text = LanguageDataHelper.CreateContentWithArgs(TaskConfig.TALK_TO_SOMEONE, {["0"] = npcName})
        end
    else
        if completeTask then
            self._buttonGo:SetActive(false)
            self._targetText.text = LanguageDataHelper.CreateContent(74505)
        else
            self._buttonGo:SetActive(true)
            self._targetText.text = TaskCommonManager:GetTaskCurStepText(weekTaskData)
        end

        local reward = weekTaskData:GetMyRewards()
        local datas = {}
        for i,v in ipairs(reward) do
            local data = {}
            table.insert(data,v:GetItemId())
            table.insert(data,v:GetCount())
            table.insert(datas, data)
        end
        if not table.isEmpty(datas) then
            local data = {}
            table.insert(data,GlobalParamsHelper.GetParamValue(918))
            table.insert(data, weekTaskData:GetMoneyReward())
            table.insert(datas, data)
        end
        self._rewardList:SetDataList(datas)
    end
end

function WeekTaskView:UpdateProgress()
    local totalNum = GlobalParamsHelper.GetParamValue(636)
    local nowNum = GameWorld.Player().week_task_cur_week_count

    self._progressComp:SetProgressText(tostring(nowNum).."/"..tostring(totalNum))
    if nowNum == totalNum then
        self._progressComp:SetProgress(0.99)
    else
        local progress = nowNum/totalNum
        self._progressComp:SetProgress(progress) 
    end
end

function WeekTaskView:InitRewardList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/Container_Rewards/Container_Rewards/ScrollViewList")
	self._rewardList = list
	self._rewardListlView = scrollView 
    self._rewardListlView:SetHorizontalMove(false)
    self._rewardListlView:SetVerticalMove(false)
    self._rewardList:SetItemType(RewardItem)
    self._rewardList:SetPadding(0, 0, 0, 0)
    self._rewardList:SetGap(10, 0)
    self._rewardList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function WeekTaskView:OnEnable()
    self:AddEventListeners()
    self:UpdataData()
end

function WeekTaskView:OnDisable()
    self:RemoveEventListeners()
end

function WeekTaskView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.WEEK_TASK_REFRESH, self._onWeekTaskUpdate)
    EventDispatcher:AddEventListener(GameEvents.ON_WEEK_TASK_PASS_WEEK, self._onWeekTaskPass)
    EventDispatcher:AddEventListener(GameEvents.WEEK_TASK_CUR_COUNT_UPDATE, self._onWeekCurCountUpdate)
end

function WeekTaskView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.WEEK_TASK_REFRESH, self._onWeekTaskUpdate)
    EventDispatcher:RemoveEventListener(GameEvents.ON_WEEK_TASK_PASS_WEEK, self._onWeekTaskPass)
    EventDispatcher:RemoveEventListener(GameEvents.WEEK_TASK_CUR_COUNT_UPDATE, self._onWeekCurCountUpdate)
end

function WeekTaskView:UpdataData()
    -- local weekTaskData = WeekTaskManager:GetWeekTaskData()
    -- local needAcceptTask = WeekTaskManager:NeedAcceptTask()
    local completeTask = WeekTaskManager:IsCompleteWeekTask()
    self._menuData = WeekTaskManager:GetTaskMenuData()
    self._menu:SetDataList(self._menuData)
    
    self._menu:SelectFItem(0)
    self:OnMenuSItemClicked(0, 0)
end

function WeekTaskView:OnDestroy()

end

function WeekTaskView:GoBtnClick()
    WeekTaskManager:RunWeekTask(true)
    GUIManager.ClosePanel(PanelsConfig.Task)
end

function WeekTaskView:CompleteBtnClick()
    local allCompleteNum = GameWorld.Player().week_task_cur_week_count
    local completeNum = GlobalParamsHelper.GetParamValue(637) - allCompleteNum % GlobalParamsHelper.GetParamValue(637)
    local cb = function() WeekTaskManager:FastCommitCircleTask() end
    GUIManager.ShowDailyTips(SettingType.WEEK_TASK_FAST_COMPLETE, cb,nil, LanguageDataHelper.CreateContent(58714, {["0"] = completeNum * GlobalParamsHelper.GetParamValue(836)}))
end 