-- TaskStepView.lua
local TaskStepView = Class.TaskStepView(ClassTypes.BaseComponent)

TaskStepView.interface = GameConfig.ComponentsConfig.Component
TaskStepView.classPath = "Modules.ModuleTask.ChildView.TaskStepView"
local UIProgressBar = ClassTypes.UIProgressBar

local PrintTable = GameWorld.PrintTable
--local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIToggle = ClassTypes.UIToggle

function TaskStepView:Awake()
    self._csBH = self:GetComponent("LuaUIComponent")
    self._textStep = self:GetChildComponent("Text_Content", "TextMeshWrapper")
end

function TaskStepView:SetTargetData(targetInfo, targetDescContent, curTimes)
    local totalTimes = targetInfo:GetTotalTimes()
    if self._textStep then
        if totalTimes == 1 then --一个目标时不显示进度条
            self._textStep.text = targetDescContent
        else
            self._textStep.text = targetDescContent .. " (" .. curTimes .. "/" .. totalTimes .. ") "
        end
    end
end

function TaskStepView:SetVisible(isVisible)
    self.gameObject:SetActive(isVisible)
end

function TaskStepView:OnDestroy()
end
