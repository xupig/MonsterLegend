-- CircleTaskView.lua
local CircleTaskView = Class.CircleTaskView(ClassTypes.BaseComponent)

CircleTaskView.interface = GameConfig.ComponentsConfig.Component
CircleTaskView.classPath = "Modules.ModuleTask.ChildView.CircleTaskView"

local CircleTaskManager = PlayerManager.CircleTaskManager
local TaskCommonManager = PlayerManager.TaskCommonManager
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local TaskConfig = GameConfig.TaskConfig
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GUIManager = GameManager.GUIManager
local UINavigationMenu = ClassTypes.UINavigationMenu
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local TaskManager = PlayerManager.TaskManager

require"Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem
require "Modules.ModuleTask.ChildComponent.TaskListFirstItem"
local TaskListFirstItem = ClassTypes.TaskListFirstItem
require "Modules.ModuleTask.ChildComponent.TaskListSecondItem"
local TaskListSecondItem = ClassTypes.TaskListSecondItem


function CircleTaskView:Awake()
    self._preF = -1
    self._preFI = -1
    self._preSI = -1
    self:Init()
end

function CircleTaskView:Init()
    self._onCircleTaskUpdate = function() self:UpdataData() end
    self._onCircleTaskPass = function() self:UpdataData() end
    self._onCircleCurCountUpdate = function() self:UpdataData() end

    self._titleText = self:GetChildComponent("Container_Content/Container_Title/Image_Title/Text_Title", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Container_Content/Container_Desc/Text_Desc", "TextMeshWrapper")
    self._targetText = self:GetChildComponent("Container_Content/Container_Target/Container_Steps/Container_Step/Text_Content", "TextMeshWrapper")

    self._buttonGo = self:FindChildGO("Container_Content/Button_Go")
    self._csBH:AddClick(self._buttonGo, function()self:GoBtnClick() end)
    
    self._completeGo = self:FindChildGO("Container_Content/Button_Complete")
    self._csBH:AddClick(self._completeGo, function()self:CompleteBtnClick() end)

    self._progressComp = UIScaleProgressBar.AddProgressBar(self.gameObject,"Container_Progress/ProgressBar_Progress")
    self._progressComp:SetProgress(0)
    self._progressComp:ShowTweenAnimate(true)
    self._progressComp:SetMutipleTween(false)
    self._progressComp:SetIsResetToZero(true)
    self._progressComp:SetTweenTime(0.2)

    self:InitRewardList()
    self:InitTaskMenu()
end

function CircleTaskView:InitTaskMenu()
    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "Container_Circle_Task_List/NavigationMenu")
	self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(TaskListFirstItem)
	self._menu:SetSecondLevelItemType(TaskListSecondItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(5, 5)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)
end

function CircleTaskView:OnMenuFItemClicked(idx)
    if self._preF ~= -1 and self._preF ~= idx then
        local item = self._menu:GetFItem(self._preF)
        item:Reset()
    end
    self._selectedFItem = self._menu:GetFItem(idx)
    self._selectedFItem:ToggleSelected()
    self._preF = idx
end

function CircleTaskView:OnMenuSItemClicked(fIndex, sIndex)
    if self._preFI ~= -1 and self._preSI ~= -1 then
        local item = self._menu:GetSItem(self._preFI, self._preSI)
        item:OnSelected(false)
    end
    self._selectedSItem = self._menu:GetSItem(fIndex,sIndex)
    self._selectedSItem:OnSelected(true)
    self._preFI = fIndex
    self._preSI = sIndex
    self:RefreshTaskData()
end

function CircleTaskView:RefreshTaskData()
    local needAcceptTask = CircleTaskManager:NeedAcceptTask()
    local completeTask = CircleTaskManager:IsCompleteCircleTask()
    local circleTaskData = self._menuData[1][2]:GetTask()
    self._titleText.text = circleTaskData:GetTitleNameAndColor()
    self._descText.text = circleTaskData:GetTaskDesc()
    -- if #self._menuData[1] > 1 and self._menuData[1][2] then
    --     local task = self._menuData[1][2]:GetTask()
    --     self._titleText.text = task:GetTitleNameAndColor()
    -- else
    --     -- self._titleText.text = LanguageDataHelper.CreateContent(925)
    -- end
    
    local totalNum = GlobalParamsHelper.GetParamValue(487)
    local nowNum = GameWorld.Player().circle_task_cur_day_count

    self._progressComp:SetProgressText(tostring(nowNum).."/"..tostring(totalNum))
    if nowNum == totalNum then
        self._progressComp:SetProgress(0.99)
    else
        local progress = nowNum/totalNum
        self._progressComp:SetProgress(progress) 
    end

    if needAcceptTask then
        self._completeGo:SetActive(false)
    else
        self._completeGo:SetActive(not completeTask)
    end

    if needAcceptTask then
        self._buttonGo:SetActive(true)
        self._rewardList:SetDataList({})
        local data = GlobalParamsHelper.GetParamValue(528)
        for k,v in pairs(data) do
            local npcName = NPCDataHelper.GetNPCName(k)
            self._targetText.text = LanguageDataHelper.CreateContentWithArgs(TaskConfig.TALK_TO_SOMEONE, {["0"] = npcName})
        end
    else
        if completeTask then
            self._buttonGo:SetActive(false)
            self._targetText.text = LanguageDataHelper.CreateContent(74505)
        else
            self._buttonGo:SetActive(true)
            self._targetText.text = TaskCommonManager:GetTaskCurStepText(circleTaskData)
        end

        local reward = circleTaskData:GetMyRewards()
        local datas = {}
        for i,v in ipairs(reward) do
            local data = {}
            table.insert(data,v:GetItemId())
            table.insert(data,v:GetCount())
            table.insert(datas, data)
        end
        self._rewardList:SetDataList(datas)
    end
end

function CircleTaskView:InitRewardList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/Container_Rewards/Container_Rewards/ScrollViewList")
	self._rewardList = list
	self._rewardListlView = scrollView 
    self._rewardListlView:SetHorizontalMove(false)
    self._rewardListlView:SetVerticalMove(false)
    self._rewardList:SetItemType(RewardItem)
    self._rewardList:SetPadding(0, 0, 0, 0)
    self._rewardList:SetGap(10, 0)
    self._rewardList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function CircleTaskView:OnEnable()
    self:AddEventListeners()
    self:UpdataData()
end

function CircleTaskView:OnDisable()
    self:RemoveEventListeners()
end

function CircleTaskView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.CIRCLE_TASK_REFRESH, self._onCircleTaskUpdate)
    EventDispatcher:AddEventListener(GameEvents.ON_CIRCLE_TASK_PASS_Day, self._onCircleTaskPass)
    EventDispatcher:AddEventListener(GameEvents.CIRCLE_TASK_CUR_COUNT_UPDATE, self._onCircleCurCountUpdate)
end

function CircleTaskView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.CIRCLE_TASK_REFRESH, self._onCircleTaskUpdate)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CIRCLE_TASK_PASS_Day, self._onCircleTaskPass)
    EventDispatcher:RemoveEventListener(GameEvents.CIRCLE_TASK_CUR_COUNT_UPDATE, self._onCircleCurCountUpdate)
end

function CircleTaskView:UpdataData()
    local circleTaskData = CircleTaskManager:GetCircleTaskData()
    local needAcceptTask = CircleTaskManager:NeedAcceptTask()
    local completeTask = CircleTaskManager:IsCompleteCircleTask()
    self._menuData = CircleTaskManager:GetTaskMenuData()
    self._menu:SetDataList(self._menuData)
    
    self._menu:SelectFItem(0)
    self:OnMenuSItemClicked(0, 0)
end

function CircleTaskView:OnDestroy()

end

function CircleTaskView:GoBtnClick()
    -- CircleTaskManager:AcceptTask()
    CircleTaskManager:RunCircleTask(true)
    GUIManager.ClosePanel(PanelsConfig.Task)
end

function CircleTaskView:CompleteBtnClick()
    local allCompleteNum = GameWorld.Player().circle_task_cur_day_count
    local completeNum = GlobalParamsHelper.GetParamValue(487) - allCompleteNum    
    local cb = function() CircleTaskManager:FastCommitAllTask() end
    GUIManager.ShowDailyTips(SettingType.CIRCLE_TASK_FAST_COMPLETE, cb,nil, LanguageDataHelper.CreateContent(58654, {["0"] = completeNum * GlobalParamsHelper.GetParamValue(490)}), true)

    -- local settingData = PlayerManager.PlayerDataManager.playerSettingData
    -- local dailyData = settingData:GetData(SettingType.DAILY_TIPS)

    -- if dailyData == nil or dailyData[SettingType.CIRCLE_TASK_FAST_COMPLETE] == nil 
    --     or (DateTimeUtil.SameDay(dailyData[SettingType.CIRCLE_TASK_FAST_COMPLETE]) == false) then 
    --     local value = {
    --         ["confirmCB"]= function()
    --             CircleTaskManager:FastCommitTask()
    --         end,
    --         -- ["cancleButton"] = false,
    --         ["text"] = LanguageDataHelper.CreateContent(58654, {["0"] = GlobalParamsHelper.GetParamValue(490)}),
    --         ["type"] = SettingType.CIRCLE_TASK_FAST_COMPLETE
    --     }
    --     local msgData ={["id"] = MessageBoxType.ToggleTips,["value"]=value}
    --     GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
    -- else
    --     CircleTaskManager:FastCommitTask()
    -- end
    -- CircleTaskManager:FastCommitTask()
end 