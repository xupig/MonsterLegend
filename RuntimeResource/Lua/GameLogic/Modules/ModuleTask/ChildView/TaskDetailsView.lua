-- TaskDetailsView.lua
local TaskDetailsView = Class.TaskDetailsView(ClassTypes.BaseComponent)

require "Modules.ModuleTask.ChildView.TaskStepView"
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"

TaskDetailsView.interface = GameConfig.ComponentsConfig.Component
TaskDetailsView.classPath = "Modules.ModuleTask.ChildView.TaskDetailsView"
local TaskStepView = ClassTypes.TaskStepView
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local TaskRewardItem = ClassTypes.TaskRewardItem
local TaskManager = PlayerManager.TaskManager
local TaskCommonManager = PlayerManager.TaskCommonManager

local action_config = require("ServerConfig/action_config")
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local TracingPointData = ClassTypes.TracingPointData

function TaskDetailsView:Awake()
    self._titleText = self:GetChildComponent("Container_Title/Image_Title/Text_Title", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Container_Desc/Text_Desc", "TextMeshWrapper")
    self._containerSteps = self:FindChildGO("Container_Target/Container_Steps")
    self._containerStepItem = self:FindChildGO("Container_Target/Container_Steps/Container_Step")
    self._buttonGo = self:FindChildGO("Button_Go")
    self._csBH:AddClick(self._buttonGo, function()self:GoBtnClick() end)

    self._stepItemPool = {}
    self._stepItems = {}
    self:InitList()
end

function TaskDetailsView:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Rewards/Container_Rewards/ScrollViewList")
    self._list = list
    self._scrollView = scrollView
	self._scrollView:SetHorizontalMove(true)
    self._scrollView:SetVerticalMove(false)
    self._list:SetItemType(TaskRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, 100, 1)
    -- local itemClickedCB =
    --     function(idx)
    --         self:OnListItemClicked(idx)
    --     end
    -- self._list:SetItemClickedCB(itemClickedCB)
    -- self._scrollView:SetOnClickCB(function(idx)self:_onSetBackToCurTask() end)
end

function TaskDetailsView:SetTaskInfo(taskInfo)
    if taskInfo then
        local name = taskInfo:GetTitleNameAndColor()
        self._titleText.text = name
        self._descText.text = LanguageDataHelper.CreateContent(taskInfo:GetDetail())
        self._taskId = taskInfo:GetId()    
        
        self:SetTargetsInfo(taskInfo)
        self._list:SetDataList(taskInfo:GetMyRewards())
    
        self:SetTaskStatus(taskInfo)
    end
end

function TaskDetailsView:SetTaskStatus(taskInfo, canAccept)
    -- self._textLimit.gameObject:SetActive(canAccept)
    -- self._buttonGo:SetActive(not canAccept)
    -- if not canAccept then
    --     self._textLimit.text = LanguageDataHelper.CreateContentWithArgs(10516, {["0"] = taskInfo:GetSelfLimit()})
    -- end
end

function TaskDetailsView:SetTargetsInfo(taskInfo)
    self._containerStepItem:SetActive(true)
    self:RemoveAllStep()
    local targetsInfo = taskInfo:GetTargetsInfo()
    local targetsDesc = taskInfo:GetTargetsDesc()
    
    local hasSetData = false
    for i, event in ipairs(targetsInfo) do
        local id = event:GetId()
        local item = self:AddStep(id)
        -- if self._targetViews[id] == nil then
        --     local target = self:CopyUIGameObject("Container_Content/Container_Steps/Container_Step", "Container_Content/Container_Steps")
        --     self._targetViews[id] = UIComponentUtil.AddLuaUIComponent(target, TaskStepView)
        -- end
        if taskInfo:GetState() == public_config.TASK_STATE_COMPLETED or taskInfo:GetState() == public_config.TASK_STATE_ACCEPTABLE then
            item:SetTargetData(event, event:GetTalkToSomeOneContent(), 0)
        else
            item:SetTargetData(event, LanguageDataHelper.CreateContent(targetsDesc[id]), taskInfo:GetTargetsCurTimes()[id] or 0)
        end
        hasSetData = true
    end
    
    if hasSetData == false then
        self._containerSteps:SetActive(false)
    else
        self._containerSteps:SetActive(true)
    end
    
    self._containerStepItem:SetActive(false)-- 将用于复制的模板隐藏
end

-- function TaskDetailsView:OnClickTrackMark(flag)
--     EventDispatcher:TriggerEvent(GameEvents.OnTaskTrackMarkChanged, self._taskId, flag)
-- -- LoggerHelper.Log("Ash: OnClickTrackMark:" .. tostring(flag))
-- end

function TaskDetailsView:OnDestroy()
    self._textLocation = nil
    -- self._toggleTrackMark = nil
    self._targetViews = nil
    self._stepItemPool = nil
    self._stepItems = nil
end

-- 任务步骤对象池
function TaskDetailsView:CreateStepItem()
    local itemPool = self._stepItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Target/Container_Steps/Container_Step", "Container_Target/Container_Steps")
    local item = UIComponentUtil.AddLuaUIComponent(go, TaskStepView)
    return item
end

function TaskDetailsView:ReleaseStepItem(item)
    local itemPool = self._stepItemPool
    table.insert(itemPool, item)
end

function TaskDetailsView:AddStep(id)
    local item = self:CreateStepItem()
    item.gameObject:SetActive(true)
    self._stepItems[id] = item
    return item
end

function TaskDetailsView:RemoveAllStep()
    for k, item in pairs(self._stepItems) do
        item.gameObject:SetActive(false)
        self:ReleaseStepItem(item)
    end
    self._stepItems = {}
end


function TaskDetailsView:GoBtnClick()
    local taskInfo = TaskManager:GetTaskById(self._taskId)
    local eventItemData = TaskCommonManager:GetNextEvent(taskInfo)
    local trackTask = TracingPointData(taskInfo, eventItemData)
    TaskCommonManager:OnManualHandleTask(trackTask)
    GameManager.GUIManager.ClosePanel(GameConfig.PanelsConfig.Task)
end

