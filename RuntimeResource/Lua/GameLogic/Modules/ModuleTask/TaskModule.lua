-- TaskModule.lua
TaskModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function TaskModule.Init()
    GUIManager.AddPanel(PanelsConfig.Task, "Panel_Task", GUILayer.LayerUIPanel, "Modules.ModuleTask.TaskPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return TaskModule
