-- TaskListFirstItem.lua
--一级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local TaskListFirstItem = Class.TaskListFirstItem(UINavigationMenuItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
TaskListFirstItem.interface = GameConfig.ComponentsConfig.PointableComponent
TaskListFirstItem.classPath = "Modules.ModuleTask.ChildComponent.TaskListFirstItem"

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function TaskListFirstItem:Awake()
    UINavigationMenuItem.Awake(self)
    self._isExpanded = false
    self:IninView()
end

function TaskListFirstItem:OnDestroy()

end

function TaskListFirstItem:IninView()
    self._titleText = self:FindChildGO("Text_Title"):GetComponent("TextMeshWrapper")
	self._imgContract = self:FindChildGO("Container_Arrow/Image_Contract")
    self._imgExpand = self:FindChildGO("Container_Arrow/Image_Expand")
    self._selectedImage = self:FindChildGO("Image_Selected")
end

function TaskListFirstItem:OnPointerDown(pointerEventData)

end

function TaskListFirstItem:ToggleSelected()
    if self._isExpanded then
		self._isExpanded = false
		self._imgContract:SetActive(true)
        self._imgExpand:SetActive(false)
        self._selectedImage:SetActive(false)
	else
		self._isExpanded = true
		self._imgContract:SetActive(false)
        self._imgExpand:SetActive(true)
        self._selectedImage:SetActive(true)
	end
end

function TaskListFirstItem:Reset()
    if self._isExpanded then
		self._isExpanded = false
		self._imgContract:SetActive(true)
        self._imgExpand:SetActive(false)
        self._selectedImage:SetActive(false)
	end
end


--override {type}
function TaskListFirstItem:OnRefreshData(data)
    -- self:ToggleSelected()
    self._titleText.text = data.text
end