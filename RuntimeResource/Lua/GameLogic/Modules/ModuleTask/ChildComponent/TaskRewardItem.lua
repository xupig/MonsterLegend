--任务奖励和交付道具ListItem
-- TaskRewardItem.lua

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local TaskRewardItem = Class.TaskRewardItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
TaskRewardItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
TaskRewardItem.classPath = "Modules.ModuleTask.ChildComponent.TaskRewardItem"

function TaskRewardItem:Awake()
    self._base.Awake(self)
    -- self._textCount = self:GetChildComponent("Text_Count", "TextMeshWrapper")
    self._itemManager = ItemManager()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._icon:ActivateTipsById()
end

function TaskRewardItem:OnDestroy()
    self._textCount = nil
    self._icon = nil
    self._base.OnDestroy(self)
end

--override
function TaskRewardItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        -- self._textCount.text = LanguageDataHelper.CreateContent(901) .. data:GetCount()
        self._icon:SetItem(data:GetItemId(), data:GetCount())
    else
        self._icon:SetItem(data:GetItemId())
        -- self._textCount.text = ""
    end
end
