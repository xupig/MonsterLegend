--支线任务上任务的ListItem
-- TaskInfoItem.lua
local TaskInfoItem = Class.TaskInfoItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local action_config = require("ServerConfig/action_config")
local UIListItem = ClassTypes.UIListItem
local UIToggle = ClassTypes.UIToggle
local TaskStepView = ClassTypes.TaskStepView
local public_config = GameWorld.public_config
TaskInfoItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
TaskInfoItem.classPath = "Modules.ModuleTask.ChildComponent.TaskInfoItem"

function TaskInfoItem:Awake()
    self._base.Awake(self)
    self._textHead = self:GetChildComponent("Container_Banner/Text_Head", "TextMeshWrapper")
    self._textDetails = self:GetChildComponent("Container_Content/Text_Details", "TextMeshWrapper")
    self._toggleTrackMark = UIToggle.AddToggle(self.gameObject, "Container_Banner/Toggle_TrackMark")
    self._toggleTrackMark:SetOnValueChangedCB(function(state)self:OnClickTrackMark(state) end)
    self._imageHeadIcon = self:FindChildGO("Container_Banner/Container_HeadIcon/Image_HeadIcon")
    local target = self:FindChildGO("Container_Content/Container_Step")
    self._targetView = UIComponentUtil.AddLuaUIComponent(target, TaskStepView)
end

function TaskInfoItem:OnDestroy()
    self._textHead = nil
    self._textDetails = nil
    self._targetView = nil
    self._toggleTrackMark = nil
    self._base.OnDestroy(self)
end

function TaskInfoItem:OnClickTrackMark(flag)
    EventDispatcher:TriggerEvent(GameEvents.OnTaskTrackMarkChanged, self._data:GetId(), flag)
-- LoggerHelper.Log("Ash: OnClickTrackMark:" .. tostring(flag))
end

--override
function TaskInfoItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        self._textHead.text = LanguageDataHelper.CreateContent(data:GetName())
        self._textDetails.text = LanguageDataHelper.CreateContent(data:GetDesc())
        self._toggleTrackMark:GetToggleWrapper().isOn = data:GetIsTrack()
        self._targetView:SetVisible(true)
        GameWorld.AddIcon(self._imageHeadIcon, data:GetIcon(), nil)
        self:SetTargetsInfo(data:GetTargetsInfo(), data:GetTargetsDesc(), data:GetTargetsCurTimes(), data:GetState())
    else
        self._textHead.text = ""
        self._textDetails.text = ""
        self._targetView:SetVisible(false)
    end
end

function TaskInfoItem:SetTargetsInfo(targetsInfo, targetsDesc, targetsTime, taskState)
    for i, event in ipairs(targetsInfo) do
        local id = event:GetId()
        if taskState == public_config.TASK_STATE_COMPLETED then
            self._targetView:SetTargetData(event, event:GetTalkToSomeOneContent(), 0)
        else
            if event:GetTotalTimes() > targetsTime[id] then
                self._targetView:SetTargetData(event, LanguageDataHelper.CreateContent(targetsDesc[id]), targetsTime[id] or 0)
                return
            end
        end
    end
end
