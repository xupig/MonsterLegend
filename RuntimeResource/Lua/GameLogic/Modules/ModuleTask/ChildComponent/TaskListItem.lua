--任务列表
-- TaskListItem.lua
local TaskListItem = Class.TaskListItem(ClassTypes.UIListItem)
TaskListItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
TaskListItem.classPath = "Modules.ModuleTask.ChildComponent.TaskListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config

function TaskListItem:Awake()
    self._base.Awake(self)
    self._selectGo = self:FindChildGO("Image_Select")
    self._textContent = self:GetChildComponent("Text_Content", "TextMeshWrapper")
    self._newGo = self:FindChildGO("Image_New")
end

function TaskListItem:OnDestroy()
    self._base.OnDestroy(self)
end

--override
function TaskListItem:OnRefreshData(datas)
    local data = datas.data
    local canAccept = datas.canAccept

    if data:GetState() == public_config.TASK_STATE_ACCEPTABLE then
        self._newGo:SetActive(true)
    else
        self._newGo:SetActive(false)
    end

    if data:GetTaskType() == public_config.TASK_TYPE_MAIN then
        self._textContent.text = LanguageDataHelper.CreateContent(134) .. LanguageDataHelper.CreateContent(data:GetName())
    else
        self._textContent.text = LanguageDataHelper.CreateContent(135) .. LanguageDataHelper.CreateContent(data:GetName())
    end
end

function TaskListItem:SetSelect(value)
    self._selectGo:SetActive(value)
end
