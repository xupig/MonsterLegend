--幕间剧的ListItem
-- InteractInfoItem.lua
local InteractInfoItem = Class.InteractInfoItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local action_config = require("ServerConfig/action_config")
local UIListItem = ClassTypes.UIListItem
local TaskStepView = ClassTypes.TaskStepView
InteractInfoItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
InteractInfoItem.classPath = "Modules.ModuleTask.ChildComponent.InteractInfoItem"

function InteractInfoItem:Awake()
    self._base.Awake(self)
    self._textTitle = self:GetChildComponent("Container_Title/Text_Title", "TextMeshWrapper")
    self._textDetail = self:GetChildComponent("Text_Detail", "TextMeshWrapper")
end

function InteractInfoItem:OnDestroy()
    self._textTitle = nil
    self._textDetail = nil
    self._base.OnDestroy(self)
end

--override
function InteractInfoItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        self._textTitle.text = LanguageDataHelper.CreateContent(data:GetTitle())
        self._textDetail.text = LanguageDataHelper.CreateContent(data:GetDetail())
    else
        self._textTitle.text = ""
        self._textDetail.text = ""
    end
end
