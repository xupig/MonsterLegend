-- TaskListSecondItem.lua
--二级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local TaskListSecondItem = Class.TaskListSecondItem(UINavigationMenuItem)

TaskListSecondItem.interface = GameConfig.ComponentsConfig.PointableComponent
TaskListSecondItem.classPath = "Modules.ModuleTask.ChildComponent.TaskListSecondItem"

local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TitleDataHelper = GameDataHelper.TitleDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local TitleManager = PlayerManager.TitleManager
local FontStyleHelper = GameDataHelper.FontStyleHelper

function TaskListSecondItem:Awake()
    UINavigationMenuItem.Awake(self)
    self:IninView()
end

function TaskListSecondItem:OnDestroy()

end

function TaskListSecondItem:IninView()
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._statusText = self:GetChildComponent("Text_Status", "TextMeshWrapper")
    self._selectedImage = self:FindChildGO("Image_Selected")
end

function TaskListSecondItem:OnPointerDown(pointerEventData)

end

--override {titleId}
function TaskListSecondItem:OnRefreshData(curMapTrackTask)
    local data = curMapTrackTask:GetTask()
    self.data = data
    local state = data:GetState()
    local name,color = data:GetTitleNameAndColor() 
    self._nameText.text = name
    self._nameText.color = color
    
    if state == public_config.TASK_STATE_COMPLETED then
        self._statusText.text = LanguageDataHelper.CreateContent(10586)
    elseif state == public_config.TASK_STATE_ACCEPTABLE then
        self._statusText.text = LanguageDataHelper.CreateContent(10585)
    else
        self._statusText.text = LanguageDataHelper.CreateContent(10584)
    end
end

function TaskListSecondItem:OnSelected(state)
    self._selectedImage:SetActive(state)
end