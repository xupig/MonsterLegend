--主界面已跟踪任务上任务的ListItem
-- MarriageTitleInfoListItem.lua
local MarriageTitleInfoListItem = Class.MarriageTitleInfoListItem(ClassTypes.UIListItem)
MarriageTitleInfoListItem.interface = GameConfig.ComponentsConfig.Component
MarriageTitleInfoListItem.classPath = "Modules.ModuleMarriage.ChildComponent.MarriageTitleInfoListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local MarriageRaiseManager = PlayerManager.MarriageRaiseManager
local TitleManager = PlayerManager.TitleManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function MarriageTitleInfoListItem:Awake()
    self._base.Awake(self)
    self._containerTitle = self:FindChildGO("Container_Title")
    self._textContent = self:GetChildComponent("Text_Content", "TextMeshWrapper")
    self._textProgress = self:GetChildComponent("Text_Progress", "TextMeshWrapper")
    self._infos={}
    self._infos[1]=self:GetChildComponent('Container_Condition/Text_Info1',"TextMeshWrapper")
    self._infos[2]=self:GetChildComponent('Container_Condition/Text_Info2',"TextMeshWrapper")
    self._infos[3]=self:GetChildComponent('Container_Condition/Text_Info3',"TextMeshWrapper")
    self._levelCb = function() return self:UpdateringLeve()end
    self._badyCb = function()return self:UpdteBady()end
    self._intimacyCb = function()return self:UpdateIntimacy()end
    self.TitleEvenfuns = {
        [5059]=self._levelCb,
        [5060]=self._badyCb,
        [5061]=self._badyCb,
        [5062]=self._badyCb,
        [5063]=self._badyCb,
        [5064]=self._intimacyCb
    }
    self:InitProgress()
end

function MarriageTitleInfoListItem:UpdateringLeve()
    return MarriageRaiseManager:UpdateringLeve()
end

function MarriageTitleInfoListItem:UpdteBady(badyid)
    return MarriageRaiseManager:UpdteBady(badyid)
end

function MarriageTitleInfoListItem:UpdateIntimacy()
    return MarriageRaiseManager:UpdateIntimacy()  
end

function MarriageTitleInfoListItem:InitProgress()
    self._pbHeat = UIScaleProgressBar.AddProgressBar(self.gameObject, "ProgressBar_Progress")
    self._pbHeat:SetProgress(0)
    self._pbHeat:ShowTweenAnimate(false)
    self._pbHeat:SetMutipleTween(false)
    self._pbHeat:SetIsResetToZero(false)
end

function MarriageTitleInfoListItem:OnDestroy()
    self._containerTitle = nil
    self._textContent = nil
    self._textProgress = nil
    self._base.OnDestroy(self)
end

--override
function MarriageTitleInfoListItem:OnRefreshData(data)
    self._data = data
    local args = GlobalParamsHelper.GetParamValue(875)
    if data ~= nil then
        local titleid = data:GetTitleId()
        local state = TitleManager:GetStateByTitleId(titleid)
        GameWorld.AddIconAnim(self._containerTitle, data:GetIcon())
        GameWorld.SetIconAnimSpeed(self._containerTitle, 1)
        local condition = data:GetCondition()
        for k,v in ipairs(condition) do 
            if v.contentid then
                if self.TitleEvenfuns[v.key] then
                    if args[v.key] then
                        local v1=LanguageDataHelper.GetContent(args[v.key])
                        local v2=v.conditionvalue        
                        self._infos[k].text = LanguageDataHelper.CreateContentWithArgs(v.contentid,{['0']=v1})
                    else
                        local v1=self.TitleEvenfuns[v.key]()
                        local v2=v.conditionvalue   
                        if state~=0 or v1>v2 then
                            self._infos[k].text = LanguageDataHelper.CreateContentWithArgs(v.contentid,{['0']=v2,['1']=v2})
                        else
                            self._infos[k].text = LanguageDataHelper.CreateContentWithArgs(v.contentid,{['0']=v1,['1']=v2})
                        end     
                    end
                end
            end
        end       
        if state==0 then
            local per = MarriageRaiseManager:UpdtePro(condition)
            self._pbHeat:SetProgress(per)
            if per>0 and per<0.01 then
                per=0.01
            end
            self._textProgress.text =  string.format("%.f",per*100)..'%'
        else
            local per = 1
            self._pbHeat:SetProgress(per)
            self._textProgress.text =  string.format("%.f",per*100)..'%'
        end
       
    else
        self._textProgress.text = "0%"
        self._pbHeat:SetProgress(0)
    end
    self:AddListener()
end

function MarriageTitleInfoListItem:UpdtePro()
    local d = self._data:GetCondition()
    local per=1
    local f = false
    for k,v in ipairs(d) do 
        if v.contentid then
            if self.TitleEvenfuns[v.key] then
                f=true
                local v1=self.TitleEvenfuns[v.key]()
                local v2=v.conditionvalue
                self._infos[k].text = LanguageDataHelper.CreateContentWithArgs(v.contentid,{['0']=v1,['1']=v2})
                per=per*(v1/v2)
            end
        end
    end
    if not f then
        return 0
    end
    return per
end

function MarriageTitleInfoListItem:RemoveListener()
    
end

function MarriageTitleInfoListItem:AddListener()
    
end
