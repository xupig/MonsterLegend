-- BodySkillItem.lua
local UIListItem = ClassTypes.UIListItem
local BodySkillItem = Class.BodySkillItem(UIListItem)
BodySkillItem.interface = GameConfig.ComponentsConfig.Component
BodySkillItem.classPath = "Modules.ModuleMarriage.ChildComponent.BodySkillItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local SkillDataHelper = GameDataHelper.SkillDataHelper
local TipsManager = GameManager.TipsManager
function BodySkillItem:Awake()
	self._icongo = self:FindChildGO("Container_Icon")
	self._selected = false
end

function BodySkillItem:OnPointerDown()
	TipsManager:ShowSkillTips(self.data[1], self.data[2],1,LanguageDataHelper.GetContent(self.data[3]))
end

function BodySkillItem:SetSelected(isSelected)
	self._selected = isSelected
end

function BodySkillItem:ToggleSelected()
	self:SetSelected(not self._selected)
end

--override {type}
function BodySkillItem:OnRefreshData(data)
	self.data = data
	if data[4]==true then
		GameWorld.AddIcon(self._icongo,SkillDataHelper.GetSkillIcon(data[1]),0,false,1)
	else
		GameWorld.AddIcon(self._icongo,SkillDataHelper.GetSkillIcon(data[1]),0,false,0)
	end
end