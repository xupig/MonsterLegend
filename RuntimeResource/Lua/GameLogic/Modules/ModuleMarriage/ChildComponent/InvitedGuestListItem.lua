--主界面已跟踪任务上任务的ListItem
-- InvitedGuestListItem.lua
local InvitedGuestListItem = Class.InvitedGuestListItem(ClassTypes.UIListItem)
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
InvitedGuestListItem.interface = GameConfig.ComponentsConfig.Component
InvitedGuestListItem.classPath = "Modules.ModuleMarriage.ChildComponent.InvitedGuestListItem"

function InvitedGuestListItem:Awake()
    self._base.Awake(self)
    self._textName = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._textNameOffLine = self:GetChildComponent("Container_content/Text_Name_OffLine", "TextMeshWrapper")
    self._btnCancel = self:FindChildGO("Container_content/Button_Cancel")
    self._btnCancel:SetActive(false)
    -- self._csBH:AddClick(self._btnCancel, function()self:CancelBtnClick() end)
end

function InvitedGuestListItem:OnDestroy()
    self._textName = nil
    self._btnCancel = nil
    self._base.OnDestroy(self)
end

function InvitedGuestListItem:CancelBtnClick()
end

--override
function InvitedGuestListItem:OnRefreshData(data)
    if data ~= nil then
        self._data = data
        if data[3] == 1 then
            self._textName.gameObject:SetActive(true)
            self._textNameOffLine.gameObject:SetActive(false)
            self._textName.text = data[2]
        else
            self._textName.gameObject:SetActive(false)
            self._textNameOffLine.gameObject:SetActive(true)
            self._textNameOffLine.text = data[2]
        end
   end
end
