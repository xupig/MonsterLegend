--主界面已跟踪任务上任务的ListItem
-- InviteGuestFriendListItem.lua
local InviteGuestFriendListItem = Class.InviteGuestFriendListItem(ClassTypes.UIListItem)
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
InviteGuestFriendListItem.interface = GameConfig.ComponentsConfig.Component
InviteGuestFriendListItem.classPath = "Modules.ModuleMarriage.ChildComponent.InviteGuestFriendListItem"
local MarriageManager = PlayerManager.MarriageManager

function InviteGuestFriendListItem:Awake()
    self._base.Awake(self)
    self._textName = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._textNameOffLine = self:GetChildComponent("Container_content/Text_Name_OffLine", "TextMeshWrapper")
    self._btnInvite = self:FindChildGO("Container_content/Button_Invite")
    self._csBH:AddClick(self._btnInvite, function()self:InviteBtnClick() end)
end

function InviteGuestFriendListItem:OnDestroy()
    self._textName = nil
    self._btnInvite = nil
    self._base.OnDestroy(self)
end

function InviteGuestFriendListItem:InviteBtnClick()
    if self._data ~= nil then
        MarriageManager:MarryInvite(self._data:GetDbid())      
    end
end

--override
function InviteGuestFriendListItem:OnRefreshData(data)
    if data ~= nil then
        self._data = data
        if data:GetIsOnline() == 1 then
            self._textName.gameObject:SetActive(true)
            self._textNameOffLine.gameObject:SetActive(false)
            self._textName.text = data:GetName()
        else
            self._textName.gameObject:SetActive(false)
            self._textNameOffLine.gameObject:SetActive(true)
            self._textNameOffLine.text = data:GetName()
        end
    end
end
