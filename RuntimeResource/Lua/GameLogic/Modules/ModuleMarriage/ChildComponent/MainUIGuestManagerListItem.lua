--主界面已跟踪任务上任务的ListItem
-- MainUIGuestManagerListItem.lua
local MainUIGuestManagerListItem = Class.MainUIGuestManagerListItem(ClassTypes.UIListItem)
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
MainUIGuestManagerListItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
MainUIGuestManagerListItem.classPath = "Modules.ModuleMarriage.ChildComponent.MainUIGuestManagerListItem"
local MarriageManager = PlayerManager.MarriageManager

function MainUIGuestManagerListItem:Awake()
    self._base.Awake(self)
    self._textName = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._btnReject = self:FindChildGO("Container_content/Button_Reject")
    self._btnAgree = self:FindChildGO("Container_content/Button_Agree")
    self._csBH:AddClick(self._btnReject, function()self:RejectBtnClick() end)
    self._csBH:AddClick(self._btnAgree, function()self:AgreeBtnClick() end)

end

function MainUIGuestManagerListItem:RejectBtnClick()
    MarriageManager:RemoveInviteGuest(self._data)
end

function MainUIGuestManagerListItem:AgreeBtnClick()
    MarriageManager:AgreeInviteGuest(self._data)
end

function MainUIGuestManagerListItem:OnDestroy()
    self._textName = nil
    self._btnReject = nil
    self._btnAgree = nil
    self._base.OnDestroy(self)
end

--override
function MainUIGuestManagerListItem:OnRefreshData(data)
    if data ~= nil then
        self._data = data
        if self._textName then
            self._textName.text = data[2]
        end
    end
end
