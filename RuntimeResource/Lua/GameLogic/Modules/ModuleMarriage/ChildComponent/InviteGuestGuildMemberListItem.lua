--主界面已跟踪任务上任务的ListItem
-- InviteGuestGuildMemberListItem.lua
local InviteGuestGuildMemberListItem = Class.InviteGuestGuildMemberListItem(ClassTypes.UIListItem)
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
InviteGuestGuildMemberListItem.interface = GameConfig.ComponentsConfig.Component
InviteGuestGuildMemberListItem.classPath = "Modules.ModuleMarriage.ChildComponent.InviteGuestGuildMemberListItem"
local MarriageManager = PlayerManager.MarriageManager

function InviteGuestGuildMemberListItem:Awake()
    self._base.Awake(self)
    self._textName = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._textNameOffLine = self:GetChildComponent("Container_content/Text_Name_OffLine", "TextMeshWrapper")
    self._btnInvite = self:FindChildGO("Container_content/Button_Invite")
    self._csBH:AddClick(self._btnInvite, function()self:InviteBtnClick() end)
end

function InviteGuestGuildMemberListItem:OnDestroy()
    self._textName = nil
    self._btnInvite = nil
    self._base.OnDestroy(self)
end

function InviteGuestGuildMemberListItem:InviteBtnClick()
    if self._data ~= nil then
        MarriageManager:MarryInvite(self._data[public_config.GUILD_MEMBER_INFO_DBID])
    end
end

--override
function InviteGuestGuildMemberListItem:OnRefreshData(data)
    if data ~= nil then
        self._data = data
        if data[public_config.GUILD_MEMBER_INFO_IS_ONLINE] == 1 then
            self._textName.gameObject:SetActive(true)
            self._textNameOffLine.gameObject:SetActive(false)
            self._textName.text = data[public_config.GUILD_MEMBER_INFO_NAME]
        else
            self._textName.gameObject:SetActive(false)
            self._textNameOffLine.gameObject:SetActive(true)
            self._textNameOffLine.text = data[public_config.GUILD_MEMBER_INFO_NAME]
        end
    end
end
