-- BobyActiveShowItem.lua
local BobyActiveShowItem = Class.BobyActiveShowItem(ClassTypes.BaseComponent)

BobyActiveShowItem.interface = GameConfig.ComponentsConfig.Component
BobyActiveShowItem.classPath = "Modules.ModuleMarriage.ChildComponent.BobyActiveShowItem"

local closeTime = 3
local MarryChildbaseDataHelper = GameDataHelper.MarryChildbaseDataHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

--override
function BobyActiveShowItem:Awake()
    self._titleText = self:GetChildComponent("Button_Achievement/Text_Title",'TextMeshWrapper')
    self._achievementButton = self:FindChildGO("Button_Achievement")
    self._csBH:AddClick(self._achievementButton, function() self:OnAchievementButtonClick() end)

    self._initLocalPosition = self.transform.localPosition
    self._containerPos = self.gameObject:AddComponent(typeof(XGameObjectTweenPosition))
end

function BobyActiveShowItem:OnDestroy()
    self:ClearTimer()
    if  self.gameObject then 
        self.gameObject:SetActive(false)
    end
end

function BobyActiveShowItem:SetAchievementId(achievementId)
    self._achievementId = achievementId
    self._titleText.text = LanguageDataHelper.GetContent(MarryChildbaseDataHelper:GetName(achievementId))
end

function BobyActiveShowItem:OnAchievementButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_BOBYAVTIVE_ITEM_CLICK, self._achievementId)
end

function BobyActiveShowItem:ShowView()
    self._isMove = false
    self.gameObject:SetActive(true)
    self:AddTimer()
    self.transform.localPosition = Vector3.New(0,0,0)
end

function BobyActiveShowItem:Move()
    self._isMove = true
    self._containerPos:SetFromToPos(Vector3.New(0,0,0), Vector3(0, 160, 0), 0.5, 1, nil)
end

function BobyActiveShowItem:IsMove()
    return self._isMove
end

function BobyActiveShowItem:Hide()
    self:ClearTimer()
    self.gameObject:SetActive(false)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_BOBYACTIVE_ITEM, self._achievementId)
end

function BobyActiveShowItem:AddTimer()
    self:ClearTimer()
    self._timer = TimerHeap:AddSecTimer(closeTime, 0, 0, function()
        self:ClearTimer()
        self.gameObject:SetActive(false)
        EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_BOBYACTIVE_ITEM, self._achievementId)
    end)
end

function BobyActiveShowItem:ClearTimer()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    if self.transform then
        self._containerPos.IsTweening = false
        self.transform.localPosition = self._initLocalPosition
    end
end