--主界面已跟踪任务上任务的ListItem
-- InviteGuestListItem.lua
local InviteGuestListItem = Class.InviteGuestListItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local UIToggle = ClassTypes.UIToggle
local public_config = GameWorld.public_config
local FontStyleHelper = GameDataHelper.FontStyleHelper
InviteGuestListItem.interface = GameConfig.ComponentsConfig.Component
InviteGuestListItem.classPath = "Modules.ModuleMarriage.ChildComponent.InviteGuestListItem"

function InviteGuestListItem:Awake()
    self._base.Awake(self)
    self._textName = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
end

function InviteGuestListItem:OnDestroy()
    self._textName = nil
    self._btnInvite = nil
    self._base.OnDestroy(self)
end

function InviteGuestListItem:InviteBtnClick()
end

function InviteGuestListItem:InitBtnInvite()
    self._btnInvite = self:FindChildGO("Button_Invite")
    self._csBH:AddClick(self._btnInvite, function()self:InviteBtnClick() end)
end

--override
function InviteGuestListItem:OnRefreshData(data)
    self._data = data
    -- if data ~= nil then
    --     local bossInfo = self._data -- WorldBossManager:GetBossInfo(data)
    --     if bossInfo.IsGodIslandData then
    --         local monsterType = bossInfo:GetMonsterType()
    --         if monsterType == 1 then
    --             self._textHead.text = bossInfo:GetNameChinese() .. "Lv." .. tostring(bossInfo:GetLevel())
    --         else
    --             self._textHead.text = bossInfo:GetNameChinese() .. ": " .. tostring(bossInfo:GetLeftCount())
    --         end
    --     else
    --         self._textHead.text = tostring(LanguageDataHelper.GetContent(bossInfo:GetBossName())) .. "Lv." .. tostring(bossInfo:GetBossLevel())
    --     end
    --     self:SetItemActive(data:GetIsSelected())
    --     self:UpdateCountDown()
    -- else
    --     self._textHead.text = ""
    --     self._textDead.text = ""
    -- end
    -- self:SetItemActive(false)
end
