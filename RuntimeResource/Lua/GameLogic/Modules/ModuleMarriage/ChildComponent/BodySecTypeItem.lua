-- BodySecTypeItem.lua
--道具合成二级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local BodySecTypeItem = Class.BodySecTypeItem(UINavigationMenuItem)
BodySecTypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
BodySecTypeItem.classPath = "Modules.ModuleMarriage.ChildComponent.BodySecTypeItem"
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MarryChildattriDataHelper = GameDataHelper.MarryChildattriDataHelper
local MarriageRaiseData = PlayerManager.PlayerDataManager.marriageRaiseData
local MarriageRaiseManager = PlayerManager.MarriageRaiseManager

function BodySecTypeItem:Awake()
	UINavigationMenuItem.Awake(self)

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtLevel = self:GetChildComponent("Text_Level",'TextMeshWrapper')
	self._txtDisactive = self:GetChildComponent("Text_DisActive",'TextMeshWrapper')

	self._imgRedPoint = self:FindChildGO("Image_RedPoint")
	self._imgRedPoint:SetActive(false)
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected:SetActive(false)

	--当前选中的成品icon
    self._icon = self:FindChildGO("Container_Icon")
end

--override {type}
function BodySecTypeItem:OnRefreshData(data)
	self._data = data
	self._txtName.text = LanguageDataHelper.GetContent(data.name)
	self:RefreshData()
end

function BodySecTypeItem:RefreshData() 
	if self._data then
		if MarriageRaiseData:IsActiveBady(self._data.id) then
			self._txtDisactive.text=''
			self._txtLevel.text = MarriageRaiseData:GetMarrayBadyInfo()[self._data.id][2]..LanguageDataHelper.GetContent(305)
		else
			self._txtLevel.text = ''
			self._txtDisactive.text = LanguageDataHelper.GetContent(536)
		end
		self:RefreshActiveState()
		self:OnRefreshRed()
	end	
end

function BodySecTypeItem:OnRefreshRed()
	if MarriageRaiseData:IsActiveBady(self._data.id) then
		local info = GameWorld.Player().marry_pet_info[self._data.id]
		local level = info[public_config.MARRY_PET_GRADE]
        local exp = info[public_config.MARRY_PET_EXP]
		local f = MarriageRaiseManager:BobyRedPointUp(self._data.id,level,exp)
		self._imgRedPoint:SetActive(f)
	else
		local f = MarriageRaiseManager:BodyRedPointStateById(self._data.id)
		self._imgRedPoint:SetActive(f)	
	end
end

function BodySecTypeItem:RefreshActiveState() 
	if MarriageRaiseData:IsActiveBady(self._data.id) then
		GameWorld.AddIcon(self._icon,self._data.icon,0,false,1) 
	else
		GameWorld.AddIcon(self._icon,self._data.icon,0,false,0) 
	end
end

function BodySecTypeItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end