-- MarryUpCostItem.lua
local MarryUpCostItem = Class.MarryUpCostItem(ClassTypes.BaseLuaUIComponent)
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
MarryUpCostItem.interface = GameConfig.ComponentsConfig.Component
MarryUpCostItem.classPath = "Modules.ModuleMarriage.ChildComponent.MarryUpCostItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local bagData = PlayerManager.PlayerDataManager.bagData

function MarryUpCostItem:Awake()
    self._base.Awake(self)
    local itemGo = GUIManager.AddItem(self:FindChildGO("Container_ItemIcon"), 1)
    self._iconContainer = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._iconContainer:ActivateTipsById()
    self._TextName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._TextName.text = ''
end

function MarryUpCostItem:OnDestroy()

end

--override
function MarryUpCostItem:OnRefreshData(data)
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(data[1])
    self._iconContainer:SetItem(data[1])
    if ownCount>=data[2] then
        self._iconContainer:SetCountString(BaseUtil.GetColorString(ownCount..'/'..data[2], ColorConfig.J))
    else
        self._iconContainer:SetCountString(BaseUtil.GetColorString(ownCount..'/'..data[2], ColorConfig.H))
    end
    if data[3] then
        self._TextName.text = ItemDataHelper.GetItemNameWithColor(data[1])
    end
end
