-- BodyTypeItem.lua
--一级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local BodyTypeItem = Class.BodyTypeItem(UINavigationMenuItem)
BodyTypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
BodyTypeItem.classPath = "Modules.ModuleMarriage.ChildComponent.BodyTypeItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil

function BodyTypeItem:Awake()
	UINavigationMenuItem.Awake(self)

	self._txtTypeName = self:GetChildComponent("Text_TypeName",'TextMeshWrapper')
	self._selected = false
end

function BodyTypeItem:OnPointerDown(pointerEventData)

end

function BodyTypeItem:SetSelected(isSelected)
	self._selected = isSelected
	if isSelected then
		local str = BaseUtil.GetColorString(self._str, ColorConfig.A)
		self._txtTypeName.text = str
	else
		self._txtTypeName.text = self._str
	end
end

function BodyTypeItem:ToggleSelected()
	self:SetSelected(not self._selected)
end

--override {type}
function BodyTypeItem:OnRefreshData(data)
	self._str = LanguageDataHelper.CreateContent(data)
	self._txtTypeName.text = self._str
end