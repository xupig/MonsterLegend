local MarriageAttrItem = Class.MarriageAttrItem(ClassTypes.BaseLuaUIComponent)

MarriageAttrItem.interface = GameConfig.ComponentsConfig.Component
MarriageAttrItem.classPath = "Modules.ModuleMarriage.ChildComponent.MarriageAttrItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local AttriDataHelper = GameDataHelper.AttriDataHelper

function MarriageAttrItem:Awake()
    self._base.Awake(self)
    self.data = nil
    self:InitView()
end

function MarriageAttrItem:OnDestroy()
    self.data = nil
end

function MarriageAttrItem:InitView()
    self._nameText = self:GetChildComponent("Text_AttrName", "TextMeshWrapper")
    self._valueText = self:GetChildComponent("Text_AttrValue", "TextMeshWrapper")
    self._addValueText = self:GetChildComponent("Text_AddAttrValue", "TextMeshWrapper")
end

--data: {attriId, curValue, delta}
function MarriageAttrItem:OnRefreshData(data)
    self.data = data
    self._nameText.text = AttriDataHelper:GetName(data[1])
    self._valueText.text = AttriDataHelper:ConvertAttriValue(data[1],data[2])
    self._addValueText.text = data[3]
end