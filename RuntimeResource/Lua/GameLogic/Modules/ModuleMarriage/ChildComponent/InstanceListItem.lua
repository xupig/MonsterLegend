-- InstanceListItem.lua
require "UIComponent.Extend.ItemGrid"
local UIListItem = ClassTypes.UIListItem
local InstanceListItem = Class.InstanceListItem(UIListItem)
InstanceListItem.interface = GameConfig.ComponentsConfig.Component
InstanceListItem.classPath = "Modules.ModuleMarriage.ChildComponent.InstanceListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local SkillDataHelper = GameDataHelper.SkillDataHelper
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
function InstanceListItem:Awake()
	local parent = self:FindChildGO("Container_ItemIcon")
	local itemGo = GUIManager.AddItem(parent, 1)
	self._item = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
	self._item:ActivateTipsById()
end

function InstanceListItem:OnDestroy() 
    
end

function InstanceListItem:OnRefreshData(data)
	self._item:SetItem(data[1],data[2])   
end