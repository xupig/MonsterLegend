--主界面已跟踪任务上任务的ListItem
-- BlessingInfoListItem.lua
local BlessingInfoListItem = Class.BlessingInfoListItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local UIToggle = ClassTypes.UIToggle
local public_config = GameWorld.public_config
local FontStyleHelper = GameDataHelper.FontStyleHelper
local DateTimeUtil = GameUtil.DateTimeUtil

BlessingInfoListItem.interface = GameConfig.ComponentsConfig.Component
BlessingInfoListItem.classPath = "Modules.ModuleMarriage.ChildComponent.BlessingInfoListItem"

function BlessingInfoListItem:Awake()
    self._base.Awake(self)
    self._textName = self:GetChildComponent("Container_Content/Text_Name", "TextMeshWrapper")
end

function BlessingInfoListItem:OnDestroy()
    self._textName = nil
    self._base.OnDestroy(self)
end

--override
function BlessingInfoListItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = data[1]
        argsTable["1"] = data[2]
        argsTable["2"] = public_config.MONEY_TYPE_COUPONS == data[4] and data[5] .. ItemDataHelper.GetItemName(data[4]) or ItemDataHelper.GetItemName(data[4])
        -- local time = DateTimeUtil.SomeDay(data[3])
        self._textName.text = DateTimeUtil.GetDateTimeStr(data[3]).. " " .. LanguageDataHelper.CreateContent(59051, argsTable)
    --{name1,name2,os.time(),item_id,item_count}--红包的道具id变成为元宝的道具id
    end
end
