--主界面已跟踪任务上任务的ListItem
-- PartnerListItem.lua
local PartnerListItem = Class.PartnerListItem(ClassTypes.UIListItem)
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
PartnerListItem.interface = GameConfig.ComponentsConfig.Component
PartnerListItem.classPath = "Modules.ModuleMarriage.ChildComponent.PartnerListItem"

function PartnerListItem:Awake()
    self._base.Awake(self)
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
end

function PartnerListItem:OnDestroy()
    self._textName = nil
    self._base.OnDestroy(self)
end

--override
function PartnerListItem:OnRefreshData(data)
    if data ~= nil then
        self._textName.text = data:GetName()
    end
end
