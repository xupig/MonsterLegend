--主界面已跟踪任务上任务的ListItem
-- WeddingDateListItem.lua
local WeddingDateListItem = Class.WeddingDateListItem(ClassTypes.UIListItem)
WeddingDateListItem.interface = GameConfig.ComponentsConfig.Component
WeddingDateListItem.classPath = "Modules.ModuleMarriage.ChildComponent.WeddingDateListItem"
local StringStyleUtil = GameUtil.StringStyleUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config

function WeddingDateListItem:Awake()
    self._base.Awake(self)
    -- self._imageSelected = self:FindChildGO("Image_Selected")
    self._textPassTime = self:GetChildComponent("Container_Content/Container_Pass/Text_Time", "TextMeshWrapper")
    self._textCanBookTime = self:GetChildComponent("Container_Content/Container_CanBook/Text_Time", "TextMeshWrapper")
    self._textPassContent = self:GetChildComponent("Container_Content/Container_Pass/Text_Content", "TextMeshWrapper")
    self._containerPass = self:FindChildGO("Container_Content/Container_Pass")
    self._containerCanBook = self:FindChildGO("Container_Content/Container_CanBook")
end

function WeddingDateListItem:OnDestroy()
    self._imageSelected = nil
    self._textPassTime = nil
    self._textCanBookTime = nil
    self._containerPass = nil
    self._containerCanBook = nil
    self._base.OnDestroy(self)
end

--override
function WeddingDateListItem:OnRefreshData(data)
    if data ~= nil then
        self._data = data
        self._textPassTime.text = data.time
        self._textCanBookTime.text = data.time
        self._containerCanBook:SetActive(data.bookingState == "free")
        self._containerPass:SetActive(data.bookingState ~= "free")
        if data.bookingState == "pass" then
            self._textPassContent.text = LanguageDataHelper.CreateContent(59010)
        else
            self._textPassContent.text = StringStyleUtil.GetColorStringWithTextMesh(LanguageDataHelper.CreateContent(59075), StringStyleUtil.red)
        end
    end
end
