-- MarriagePlayerContainerView.lua
local MarriagePlayerContainerView = Class.MarriagePlayerContainerView(ClassTypes.BaseComponent)
MarriagePlayerContainerView.interface = GameConfig.ComponentsConfig.Component
MarriagePlayerContainerView.classPath = "Modules.ModuleMarriage.ChildView.MarriagePlayerContainerView"
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
-- local TaskManager = PlayerManager.TaskManager
local ActorModelComponent = GameMain.ActorModelComponent
local PlayerModelComponent = GameMain.PlayerModelComponent
local public_config = GameWorld.public_config
local CreateRoleManager = GameManager.CreateRoleManager

function MarriagePlayerContainerView:Awake()
    self._imageMan = self:FindChildGO("Image_Man")
    self._imageWoman = self:FindChildGO("Image_Woman")
    self._imageBG = self:FindChildGO("Image_BG")
    self._modelGo = self:FindChildGO("Container_Model")
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
end

function MarriagePlayerContainerView:OnDestroy()
end

function MarriagePlayerContainerView:ShowSelf()
    if self._playerComponent == nil then
        self._playerComponent = self:AddChildComponent('Container_Model', PlayerModelComponent)
    end
    GameWorld.ShowPlayer():SetCurWing(0)
    self._playerComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 0.96)
    local player = GameWorld.Player()
    self._textName.text = player.name
    if player.gender == public_config.GENDER_FEMALE then
        self._imageMan:SetActive(false)
        self._imageWoman:SetActive(true)
    else
        self._imageMan:SetActive(true)
        self._imageWoman:SetActive(false)
    end
end

function MarriagePlayerContainerView:InitPartner()
    if self._imageModelMan == nil then
        self._imageModelMan = self:FindChildGO("Image_ModelMan")
    end
    if self._imageModelWoman == nil then
        self._imageModelWoman = self:FindChildGO("Image_ModelWoman")
    end
end

function MarriagePlayerContainerView:ShowNoPartner()
    self:InitPartner()
    self._modelGo:SetActive(false)
    self._imageMan:SetActive(false)
    self._imageWoman:SetActive(false)
    self._imageBG:SetActive(false)
    self._textName.gameObject:SetActive(false)
    local player = GameWorld.Player()
    if player.gender == public_config.GENDER_FEMALE then
        self._imageModelMan:SetActive(true)
        self._imageModelWoman:SetActive(false)
    else
        self._imageModelMan:SetActive(false)
        self._imageModelWoman:SetActive(true)
    end
end

function MarriagePlayerContainerView:ShowPartner(vocation, name, facade)
    self:InitPartner()
    self._imageModelMan:SetActive(false)
    self._imageModelWoman:SetActive(false)
    self._modelGo:SetActive(true)
    self._textName.text = name
    if self._playerComponent == nil then
        self._playerComponent = self:AddChildComponent('Container_Model', ActorModelComponent)
    end
    self._playerComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 0.96)
    local strfacade = facade
    local index,_,s = string.find(strfacade,"21:([%d,]+)")
    if index~=nil then
        strfacade= string.gsub(strfacade,s,"0,0,0,0,0,0",1)
    end
    self._playerComponent:LoadAvatarModel(vocation, strfacade, false)
    local gender = CreateRoleManager:GetGenderByVocation(vocation)
    if gender == public_config.GENDER_FEMALE then
        self._imageMan:SetActive(false)
        self._imageWoman:SetActive(true)
    else
        self._imageMan:SetActive(true)
        self._imageWoman:SetActive(false)
    end
end

function MarriagePlayerContainerView:ShowFx()
    if self._playerComponent then
        self._playerComponent:ShowFx()
    end
end

function MarriagePlayerContainerView:HideFx()
    if self._playerComponent then
        self._playerComponent:HideFx()
    end
end

function MarriagePlayerContainerView:Show()
    if self._playerComponent then
        self._playerComponent:Show()
    end
end

function MarriagePlayerContainerView:Hide()
    if self._playerComponent then
        self._playerComponent:Hide()
    end
end
