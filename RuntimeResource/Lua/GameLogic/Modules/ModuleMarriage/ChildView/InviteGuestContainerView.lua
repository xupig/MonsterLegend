-- InviteGuestContainerView.lua
local InviteGuestContainerView = Class.InviteGuestContainerView(ClassTypes.BaseComponent)
InviteGuestContainerView.interface = GameConfig.ComponentsConfig.Component
InviteGuestContainerView.classPath = "Modules.ModuleMarriage.ChildView.InviteGuestContainerView"
require "Modules.ModuleMarriage.ChildComponent.InviteGuestFriendListItem"
local InviteGuestFriendListItem = ClassTypes.InviteGuestFriendListItem
require "Modules.ModuleMarriage.ChildComponent.InviteGuestGuildMemberListItem"
local InviteGuestGuildMemberListItem = ClassTypes.InviteGuestGuildMemberListItem
require "Modules.ModuleMarriage.ChildComponent.InvitedGuestListItem"
local InvitedGuestListItem = ClassTypes.InvitedGuestListItem

-- local SceneConfig = GameConfig.SceneConfig
-- local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIList = ClassTypes.UIList
local GuildManager = PlayerManager.GuildManager
local FriendManager = PlayerManager.FriendManager
local public_config = GameWorld.public_config
local MarriageManager = PlayerManager.MarriageManager

function InviteGuestContainerView:Awake()
    self._textTitle = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    self._textInviteCount = self:GetChildComponent("Text_InviteCount", "TextMeshWrapper")
    
    self._btnBuyCount = self:FindChildGO("Button_BuyCount")
    self._csBH:AddClick(self._btnBuyCount, function()self:BuyCountBtnClick() end)
    self._groupToggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "Container_ForInvite/ToggleGroup_Tab")
    self._groupToggleGroup:SetOnSelectedIndexChangedCB(function(index)self:OnGroupToggleClick(index) end)
    
    self:InitInviteGuestFriendList()
    self:InitInviteGuestGuildMemberList()
    self:InitInvitedList()
    self._scrollViewListFriendList = self:FindChildGO("Container_ForInvite/ScrollViewList_FriendList")
    self._scrollViewListGuildMemberList = self:FindChildGO("Container_ForInvite/ScrollViewList_GuildMemberList")
    
    self._onRefreshGuildSelfMemberInfo = function()self:OnRefreshGuildSelfMemberInfo() end
    self._onRefreshFriendList = function()self:OnRefreshFriendList() end
    self._onMarriageBookInfo = function()self:OnMarriageBookInfo() end
end

function InviteGuestContainerView:OnDestroy()
end

function InviteGuestContainerView:ShowView()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Self_Member_Info, self._onRefreshGuildSelfMemberInfo)
    EventDispatcher:AddEventListener(GameEvents.RefreshFriendList, self._onRefreshFriendList)
    EventDispatcher:AddEventListener(GameEvents.MarriageBookInfo, self._onMarriageBookInfo)
    FriendManager:SendFriendListReq()
    GuildManager:GuildSelfMemberInfoReq()
    self:OnGroupToggleClick(0)
    self:UpdateInviteCount()
end

function InviteGuestContainerView:CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Self_Member_Info, self._onRefreshGuildSelfMemberInfo)
    EventDispatcher:RemoveEventListener(GameEvents.RefreshFriendList, self._onRefreshFriendList)
    EventDispatcher:RemoveEventListener(GameEvents.MarriageBookInfo, self._onMarriageBookInfo)
    self._guildSelfMemberList = nil
    self._friendList = nil
end

function InviteGuestContainerView:UpdateInviteCount()
    self._textInviteCount.text = MarriageManager:GetMyWeddingGuestCount() .. "/" .. MarriageManager:GetMyGuestCount()
end

function InviteGuestContainerView:OnRefreshGuildSelfMemberInfo()
    self._guildSelfMemberList = GuildManager:GetGuildSelfMemberList()
    self:HandleForInviteList()
end

function InviteGuestContainerView:OnRefreshFriendList()
    self._friendList = FriendManager:GetFriendListData()
    self:HandleForInviteList()
end

function InviteGuestContainerView:OnMarriageBookInfo()
    self:UpdateInviteCount()
    self:HandleForInviteList()
end

function InviteGuestContainerView:GuestListContainDbid(list, dbid, isOnline)
    for i, v in ipairs(list) do
        if v[1] == dbid then
            v[3] = isOnline
            return true
        end
    end
    return false
end

function InviteGuestContainerView:HandleForInviteList()
    if self._friendList == nil or self._guildSelfMemberList == nil then
        return
    end
    -- LoggerHelper.Log("Ash HandleForInviteList")
    local friendIds = {}
    local friendList = {}
    local mateName = MarriageManager:GetMateName()
    local selfDbid = GameWorld.Player().dbid
    local guests = MarriageManager:GetMyWeddingGuest()
    local levelThresHold = GlobalParamsHelper.GetParamValue(744)-- 结婚婚礼玩家参与等级
    for i, v in ipairs(self._friendList) do
        if v:GetLevel() >= levelThresHold then
            if v:GetName() ~= mateName then
                if not self:GuestListContainDbid(guests, v:GetDbid(), v:GetIsOnline()) then
                    table.insert(friendList, v)
                end
            end
            friendIds[v:GetDbid()] = v
        end
    end
    -- LoggerHelper.Log("Ash HandleForInviteList friendIds " .. PrintTable:TableToStr(friendIds))
    local guildSelfMemberList = {}
    for i, v in ipairs(self._guildSelfMemberList) do
        local memberDbid = v[public_config.GUILD_MEMBER_INFO_DBID]
        if memberDbid ~= selfDbid and
            v[public_config.GUILD_MEMBER_INFO_LEVEL] >= levelThresHold and
            friendIds[memberDbid] == nil then
            -- LoggerHelper.Log("Ash HandleForInviteList v[public_config.GUILD_MEMBER_INFO_DBID] " .. v[public_config.GUILD_MEMBER_INFO_DBID])
            if not self:GuestListContainDbid(guests, memberDbid, v[public_config.GUILD_MEMBER_INFO_IS_ONLINE]) then
                table.insert(guildSelfMemberList, v)
            end
        end
    end
    self._listInviteGuestFriend:SetDataList(friendList)
    self._listInviteGuestGuildMember:SetDataList(guildSelfMemberList)
    self._listInvited:SetDataList(guests)
end

function InviteGuestContainerView:InitInviteGuestFriendList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_ForInvite/ScrollViewList_FriendList")
    self._listInviteGuestFriend = list
    self._scrollViewInviteGuestFriend = scrollView
    self._listInviteGuestFriend:SetItemType(InviteGuestFriendListItem)
    self._listInviteGuestFriend:SetPadding(0, 0, 0, 0)
    self._listInviteGuestFriend:SetGap(0, 0)
    self._listInviteGuestFriend:SetDirection(UIList.DirectionTopToDown, 1, 1);
end

function InviteGuestContainerView:InitInviteGuestGuildMemberList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_ForInvite/ScrollViewList_GuildMemberList")
    self._listInviteGuestGuildMember = list
    self._scrollViewInviteGuestGuildMember = scrollView
    self._listInviteGuestGuildMember:SetItemType(InviteGuestGuildMemberListItem)
    self._listInviteGuestGuildMember:SetPadding(0, 0, 0, 0)
    self._listInviteGuestGuildMember:SetGap(0, 0)
    self._listInviteGuestGuildMember:SetDirection(UIList.DirectionTopToDown, 1, 1);
end

function InviteGuestContainerView:InitInvitedList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Invited/ScrollViewList_InvitedList")
    self._listInvited = list
    self._scrollViewInvited = scrollView
    self._listInvited:SetItemType(InvitedGuestListItem)
    self._listInvited:SetPadding(0, 0, 0, 0)
    self._listInvited:SetGap(0, 0)
    self._listInvited:SetDirection(UIList.DirectionTopToDown, 1, 1);
end

function InviteGuestContainerView:OnGroupToggleClick(index)
    if index == 0 then
        self._scrollViewListFriendList:SetActive(true)
        self._scrollViewListGuildMemberList:SetActive(false)
    else
        self._scrollViewListFriendList:SetActive(false)
        self._scrollViewListGuildMemberList:SetActive(true)
    end
end

function InviteGuestContainerView:BuyCountBtnClick()
    MarriageManager:BuyInviteCount()
end
