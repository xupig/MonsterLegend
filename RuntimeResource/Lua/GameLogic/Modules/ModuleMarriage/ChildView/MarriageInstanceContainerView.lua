-- MarriageInstanceContainerView.lua
require "Modules.ModuleMarriage.ChildComponent.InstanceListItem"

local MarriageInstanceContainerView = Class.MarriageInstanceContainerView(ClassTypes.BaseComponent)
MarriageInstanceContainerView.interface = GameConfig.ComponentsConfig.Component
MarriageInstanceContainerView.classPath = "Modules.ModuleMarriage.ChildView.MarriageInstanceContainerView"
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local UIList = ClassTypes.UIList
local InstanceListItem = ClassTypes.InstanceListItem
local MarriageRaiseManager = PlayerManager.MarriageRaiseManager
local MarriageManager = PlayerManager.MarriageManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local TeamManager = PlayerManager.TeamManager
local teamData = PlayerManager.PlayerDataManager.teamData

function MarriageInstanceContainerView:Awake()
    self:InitView()
    self:InitCB()
end

function MarriageInstanceContainerView:InitView()
    local txtinfo = self:GetChildComponent("Container_Left/Text_Info",'TextMeshWrapper')
    txtinfo.text = LanguageDataHelper.GetContent(59113)
    self.txttime = self:GetChildComponent("Container_Left/Text_Times",'TextMeshWrapper')
    local txttitle = self:GetChildComponent("Container_Info/Text_InfoTitle",'TextMeshWrapper')
    txttitle.text = LanguageDataHelper.GetContent(59109)

    local txtinf1 = self:GetChildComponent("Container_Info/Text_Info",'TextMeshWrapper')
    txtinf1.text = LanguageDataHelper.GetContent(59110)

    local txtcontent = self:GetChildComponent("Container_Info/Text_InfoContent",'TextMeshWrapper')
    txtcontent.text = LanguageDataHelper.CreateContent(59111)

    local txtreward = self:GetChildComponent("Container_Info/Text_Reward",'TextMeshWrapper')
    txtreward.text = LanguageDataHelper.GetContent(59112)

    local txtchallenge = self:GetChildComponent("Container_Info/Button_Challenge/Text",'TextMeshWrapper')
    txtchallenge.text = LanguageDataHelper.GetContent(59116)

    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'Container_Info/ScrollViewList')
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(InstanceListItem)
    self._list:SetPadding(0,0,0,20)
    self._list:SetGap(0,10)
    self._list:SetDirection(UIList.DirectionLeftToRight,-1,-1)

end
function MarriageInstanceContainerView:InitCB()
    local btnbuy = self:FindChildGO("Container_Left/Button_BuyTimes")
    self._csBH:AddClick(btnbuy,function()self:OnBtnBuy()end)

    local btnaskbuy = self:FindChildGO("Container_Left/Button_AskForBuy")
    self._csBH:AddClick(btnaskbuy,function()self:OnBtnAskBuy()end)

    local btnchallenge = self:FindChildGO("Container_Info/Button_Challenge")
    self._csBH:AddClick(btnchallenge,function()self:OnChallenge()end)

    self._refresh = function()self:RefreshView()end
end



function MarriageInstanceContainerView:OnChallenge()
    self:OnTeamEnter()
end

function MarriageInstanceContainerView:OnBtnBuy()
    if MarriageManager:HaveMate() then
        local d = {}
        local data = {}
        d.id = MessageBoxType.Tip
        d.value =  data
        data.confirmCB=function()MarriageRaiseManager:MarryBuyCountInstanceReq() end
        data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.MessageBox) end
        local arg = LanguageDataHelper.GetArgsTable()
        local v = GlobalParamsHelper.GetParamValue(822)
        arg['0']=v
        data.text = LanguageDataHelper.CreateContent(59118,arg)
        GUIManager.ShowPanel(PanelsConfig.MessageBox, d)
    else
        local showText = LanguageDataHelper.GetContent(59128)
        GUIManager.ShowText(2, showText)
    end
end

function MarriageInstanceContainerView:OnBtnAskBuy()
    if MarriageManager:HaveMate() then
        MarriageRaiseManager:MarryAskSpouseBuyReq()
    else
        local showText = LanguageDataHelper.GetContent(59120)
        GUIManager.ShowText(2, showText)
    end
end

function MarriageInstanceContainerView:OnTeamEnter()
	if GameWorld.Player().team_status > 0 then
		local memberCount  = teamData:GetMyTeamMemberCount()
        if memberCount == 2 then         
            TeamManager:RequestChangeTargetId(10)
			self:RequestTeamEnter()
            GUIManager.ClosePanel(PanelsConfig.Marriage)
            return
        end
    end
    teamData:SetReadyTarget(10)
    local data = {}
    data.targetId = 10
    TeamManager:OpenTeamPanel(data)
    GUIManager.ClosePanel(PanelsConfig.Marriage)
end
function MarriageInstanceContainerView:RequestTeamEnter()
	TeamManager:RequestTeamStart(GlobalParamsHelper.GetParamValue(786))
end
function MarriageInstanceContainerView:OnDestroy()

end

function MarriageInstanceContainerView:ShowView()
    self:AddListener()
    self:RefreshView()
end

function MarriageInstanceContainerView:CloseView()
    self:RemoveListener()
end

function MarriageInstanceContainerView:AddListener()
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MARRY_INSTANCE_MATE_TIME, self._refresh)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MARRY_INSTANCE_EXTRA_TIME, self._refresh)
end

function MarriageInstanceContainerView:RemoveListener()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MARRY_INSTANCE_MATE_TIME, self._refresh)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MARRY_INSTANCE_EXTRA_TIME, self._refresh)
end
function MarriageInstanceContainerView:RefreshView()
    local player=GameWorld.Player()
    local total = GlobalParamsHelper.GetParamValue(794)+player.marry_instance_mate_time+player.marry_instance_extra_time
    self.txttime.text = total-player.marry_instance_challenged_time..'/'..total
    local rewards = GlobalParamsHelper.GetParamValue(818)
    local d={}
    for k, v in pairs(rewards) do
        table.insert(d,{tonumber(k),v})
    end
    self._list:SetDataList(d)
end
