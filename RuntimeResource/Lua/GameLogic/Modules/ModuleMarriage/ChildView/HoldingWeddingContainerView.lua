-- HoldingWeddingContainerView.lua
local HoldingWeddingContainerView = Class.HoldingWeddingContainerView(ClassTypes.BaseComponent)
HoldingWeddingContainerView.interface = GameConfig.ComponentsConfig.Component
HoldingWeddingContainerView.classPath = "Modules.ModuleMarriage.ChildView.HoldingWeddingContainerView"
require "Modules.ModuleMarriage.ChildView.WeddingPlayerContainerView"
local WeddingPlayerContainerView = ClassTypes.WeddingPlayerContainerView

local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local MarriageManager = PlayerManager.MarriageManager
local DateTimeUtil = GameUtil.DateTimeUtil

function HoldingWeddingContainerView:Awake()
    self._partnerContainerView = self:AddChildLuaUIComponent("Container_Partner", WeddingPlayerContainerView)
    self._selfContainerView = self:AddChildLuaUIComponent("Container_Self", WeddingPlayerContainerView)
    
    self._textInfo = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    
    self._btnClose = self:FindChildGO("Button_Close")
    self._btnGuestManager = self:FindChildGO("Button_GuestManager")
    self._imageGuestManagerRedPoint = self:FindChildGO("Button_GuestManager/Image_RedPoint")
    self._btnAskForInvite = self:FindChildGO("Button_AskForInvite")
    self._btnAskForInviteGetWrapper = self:GetChildComponent("Button_AskForInvite", "ButtonWrapper")
    self._btnAttendWedding = self:FindChildGO("Button_AttendWedding")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    self._csBH:AddClick(self._btnGuestManager, function()self:GuestManagerBtnClick() end)
    self._csBH:AddClick(self._btnAskForInvite, function()self:AskForInviteBtnClick() end)
    self._csBH:AddClick(self._btnAttendWedding, function()self:AttendWeddingBtnClick() end)
    self._onMarriageQueryInfo = function()self:OnMarriageQueryInfo() end
    self._onGuestManagerUpdate = function(flag)self:OnGuestManagerUpdate(flag) end
end

function HoldingWeddingContainerView:OnDestroy()
end

function HoldingWeddingContainerView:ShowView(isOnlySelf)
    -- LoggerHelper.Log("Ash HoldingWeddingContainerView ShowView")
    EventDispatcher:AddEventListener(GameEvents.MarriageQueryInfo, self._onMarriageQueryInfo)
    self._isOnlySelf = isOnlySelf
    self._btnAskForInviteGetWrapper.interactable = true
    self:OnMarriageQueryInfo()
    MarriageManager:RegistGuestManagerUpdateFunc2(self._onGuestManagerUpdate)
    self._btnGuestManager:SetActive(false)
    self._btnAskForInvite:SetActive(true)
end


function HoldingWeddingContainerView:CloseView()
    -- LoggerHelper.Log("Ash HoldingWeddingContainerView CloseView")
    EventDispatcher:RemoveEventListener(GameEvents.MarriageQueryInfo, self._onMarriageQueryInfo)
    self:ClearInteractableTimer()
    MarriageManager:RegistGuestManagerUpdateFunc2(nil)
end

function HoldingWeddingContainerView:OnGuestManagerUpdate(flag)
    self._imageGuestManagerRedPoint:SetActive(flag)
end

function HoldingWeddingContainerView:OnMarriageQueryInfo()
    local name1, name2, vocation1, vocation2, dbid1, dbid2, curHour = nil
    if self._isOnlySelf then
        -- LoggerHelper.Log("Ash HoldingWeddingContainerView OnMarriageQueryInfo")
        name1, name2, vocation1, vocation2, dbid1, dbid2, curHour = MarriageManager:GetCurWeddingHourData()
    else
        -- LoggerHelper.Log("Ash HoldingWeddingContainerView GetWeddingHourData")
        name1, name2, vocation1, vocation2, dbid1, dbid2 = MarriageManager:GetWeddingHourData()
    end
    local selfDbid = GameWorld.Player().dbid
    if selfDbid == dbid1 or selfDbid == dbid2 then
        self._btnGuestManager:SetActive(true)
        self._btnAskForInvite:SetActive(false)
    else
        self._btnGuestManager:SetActive(false)
        self._btnAskForInvite:SetActive(true)
    end
    if curHour == nil then
        curHour = MarriageManager:GetMyWeddingHour()
    end
    if name1 ~= nil then
        self._selfContainerView:SetPlayer(vocation1, name1)
        self._partnerContainerView:SetPlayer(vocation2, name2)
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = name1
        argsTable["1"] = name2
        argsTable["2"] = MarriageManager:GetWeddingTime(curHour)
        self._textInfo.text = LanguageDataHelper.CreateContent(59022, argsTable)
    end
end

function HoldingWeddingContainerView:CloseBtnClick()
    self:CloseView()
    self.gameObject:SetActive(false)
    if self._isOnlySelf then
        GUIManager.ClosePanel(PanelsConfig.GetMarry)
    end
end

function HoldingWeddingContainerView:GuestManagerBtnClick()
    GUIManager.ShowPanel(PanelsConfig.MarriageGuestManager)
end

function HoldingWeddingContainerView:AskForInviteBtnClick()
    local levelThresHold = GlobalParamsHelper.GetParamValue(744)-- 结婚婚礼玩家参与等级
    local player = GameWorld.Player()
    if player.level >= levelThresHold then
        self._btnAskForInviteGetWrapper.interactable = false
        self:ClearInteractableTimer()
        self._interactableTimer = TimerHeap:AddSecTimer(10, 0, 0, function()self._btnAskForInviteGetWrapper.interactable = true end)
        MarriageManager:MarryAsk()
    else
        local showText = levelThresHold .. LanguageDataHelper.GetContent(59089)
        GUIManager.ShowText(2, showText)
    end
end

function HoldingWeddingContainerView:ClearInteractableTimer()
    if self._interactableTimer then
        TimerHeap:DelTimer(self._interactableTimer)
        self._interactableTimer = nil
    end
end

function HoldingWeddingContainerView:AttendWeddingBtnClick()
    local levelThresHold = GlobalParamsHelper.GetParamValue(744)-- 结婚婚礼玩家参与等级
    local player = GameWorld.Player()
    if player.level >= levelThresHold then
        MarriageManager:MarryEnter()
    else
        local showText = levelThresHold .. LanguageDataHelper.GetContent(59089)
        GUIManager.ShowText(2, showText)
    end
end
