-- ProposeContainerView.lua
local ProposeContainerView = Class.ProposeContainerView(ClassTypes.BaseComponent)
ProposeContainerView.interface = GameConfig.ComponentsConfig.Component
ProposeContainerView.classPath = "Modules.ModuleMarriage.ChildView.ProposeContainerView"
require "Modules.ModuleMarriage.ChildView.WeddingPlayerContainerView"
local WeddingPlayerContainerView = ClassTypes.WeddingPlayerContainerView
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local TaskRewardItem = ClassTypes.TaskRewardItem
local TaskRewardData = ClassTypes.TaskRewardData

-- local SceneConfig = GameConfig.SceneConfig
-- local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local MarriageManager = PlayerManager.MarriageManager

function ProposeContainerView:Awake()
    self._partnerContainerView = self:AddChildLuaUIComponent("Container_Partner", WeddingPlayerContainerView)
    self._selfContainerView = self:AddChildLuaUIComponent("Container_Self", WeddingPlayerContainerView)
    self._textTitle = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    
    self._gridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Items")
    self._gridLauoutGroup:SetItemType(TaskRewardItem)
    
    self._btnAccept = self:FindChildGO("Button_Accept")
    self._btnReject = self:FindChildGO("Button_Reject")
    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnAccept, function()self:AcceptBtnClick() end)
    self._csBH:AddClick(self._btnReject, function()self:RejectBtnClick() end)
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
end

function ProposeContainerView:OnDestroy()
end

function ProposeContainerView:ShowView(args)
    self._selfContainerView:ShowSelf(false)
    if args then
        self._partnerContainerView:SetPlayer(args[4])
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = args[2]
        self._textTitle.text = LanguageDataHelper.CreateContent(59012, argsTable)
        local rewards = nil
        if args[3] == 1 then
            rewards = GlobalParamsHelper.GetParamValue(737)
        elseif args[3] == 2 then
            rewards = GlobalParamsHelper.GetParamValue(738)
        else
            rewards = GlobalParamsHelper.GetParamValue(739)
        end
        local result = {}
        local group = GameWorld.Player():GetVocationGroup()
        if rewards[group] then
            for i = 1, #rewards[group], 2 do
                table.insert(result, TaskRewardData(rewards[group][i], rewards[group][i + 1]))
            end
        end
           self._gridLauoutGroup:SetDataList(result)
    end
end

function ProposeContainerView:CloseView()
end

function ProposeContainerView:AcceptBtnClick()
    MarriageManager:RespondPropose(true)
    self:CloseBtnClick()
end

function ProposeContainerView:RejectBtnClick()
    MarriageManager:RespondPropose(false)
    self:CloseBtnClick()
end

function ProposeContainerView:CloseBtnClick()
    self.gameObject:SetActive(false)
end
