-- GoddessContainerView.lua
local GoddessContainerView = Class.GoddessContainerView(ClassTypes.BaseComponent)
GoddessContainerView.interface = GameConfig.ComponentsConfig.Component
GoddessContainerView.classPath = "Modules.ModuleMarriage.ChildView.GoddessContainerView"
local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local GUIManager = GameManager.GUIManager
local ActorModelComponent = GameMain.ActorModelComponent

function GoddessContainerView:Awake()
    self._btnGetMarry = self:FindChildGO("Button_GetMarry")
    self._btnWeddingMall = self:FindChildGO("Button_WeddingMall")
    self._btnDivorce = self:FindChildGO("Button_Divorce")
    self._csBH:AddClick(self._btnGetMarry, function()self:GetMarryBtnClick() end)
    self._csBH:AddClick(self._btnWeddingMall, function()self:WeddingMallBtnClick() end)
    self._csBH:AddClick(self._btnDivorce, function()self:DivorceBtnClick() end)
    self._leftModelComponent = self:AddChildComponent('Container_NPC', ActorModelComponent)
    self._leftModelComponent:SetStartSetting(Vector3(0, 0, 100), Vector3(0, 180, 0), 1)
end

function GoddessContainerView:OnDestroy()
end

function GoddessContainerView:ShowView()
    self._iconBg = self:FindChildGO("Image_BG")
    GameWorld.AddIcon(self._iconBg,13511)
    local npcId = GlobalParamsHelper.GetParamValue(714)
    local npcData = NPCDataHelper.GetNPCData(npcId)
    self._leftModelComponent.gameObject:SetActive(true)
    self._leftModelComponent:LoadModel(npcData.model)
end

function GoddessContainerView:CloseView()
    self._iconBg = nil
end

function GoddessContainerView:SetGetMarryBtnClickCB(callback)
    self._onGetMarryBtnClick = callback
end

function GoddessContainerView:SetWeddingMallBtnClickCB(callback)
    self._onWeddingMallBtnClick = callback
end

function GoddessContainerView:SetDivorceBtnClickCB(callback)
    self._onDivorceBtnClick = callback
end

function GoddessContainerView:GetMarryBtnClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_get_marry_goddess_button")
    if self._onGetMarryBtnClick then
        self._onGetMarryBtnClick()
    end
end

function GoddessContainerView:WeddingMallBtnClick()
    if self._onWeddingMallBtnClick then
        self._onWeddingMallBtnClick()
    end
    self._leftModelComponent.gameObject:SetActive(false)
end

function GoddessContainerView:DivorceBtnClick()
    if self._onDivorceBtnClick then
        self._onDivorceBtnClick()
    end
end