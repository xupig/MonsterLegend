-- SendBlessingsContainerView.lua
local SendBlessingsContainerView = Class.SendBlessingsContainerView(ClassTypes.BaseComponent)
SendBlessingsContainerView.interface = GameConfig.ComponentsConfig.Component
SendBlessingsContainerView.classPath = "Modules.ModuleMarriage.ChildView.SendBlessingsContainerView"
require "Modules.ModuleMarriage.ChildView.WeddingPlayerContainerView"
local WeddingPlayerContainerView = ClassTypes.WeddingPlayerContainerView
require "Modules.ModuleMarriage.ChildComponent.BlessingInfoListItem"
local BlessingInfoListItem = ClassTypes.BlessingInfoListItem

local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GUIManager = GameManager.GUIManager
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIList = ClassTypes.UIList
local MarriageManager = PlayerManager.MarriageManager
local public_config = GameWorld.public_config
local bagData = PlayerManager.PlayerDataManager.bagData
local SystemInfoManager = GameManager.SystemInfoManager
local error_code = GameWorld.error_code

function SendBlessingsContainerView:Awake()
    self._partnerContainerView = self:AddChildLuaUIComponent("Container_Partner", WeddingPlayerContainerView)
    self._selfContainerView = self:AddChildLuaUIComponent("Container_Self", WeddingPlayerContainerView)
    self._sendToggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "Container_Send/ToggleGroup_Send")
    self._sendToggleGroup:SetOnSelectedIndexChangedCB(function(index)self:OnSendToggleClick(index) end)
    self._curGiftIndex = 0
    
    local text1D = self:GetChildComponent("Container_Send/ToggleGroup_Send/Toggle_1D/Label", 'TextMeshWrapper')
    local text2D = self:GetChildComponent("Container_Send/ToggleGroup_Send/Toggle_2D/Label", 'TextMeshWrapper')
    local text3D = self:GetChildComponent("Container_Send/ToggleGroup_Send/Toggle_3D/Label", 'TextMeshWrapper')
    local text1R = self:GetChildComponent("Container_Send/ToggleGroup_Send/Toggle_1R/Label", 'TextMeshWrapper')
    local text2R = self:GetChildComponent("Container_Send/ToggleGroup_Send/Toggle_2R/Label", 'TextMeshWrapper')
    local text3R = self:GetChildComponent("Container_Send/ToggleGroup_Send/Toggle_3R/Label", 'TextMeshWrapper')
    local text4R = self:GetChildComponent("Container_Send/ToggleGroup_Send/Toggle_4R/Label", 'TextMeshWrapper')
    local textToggle = {}
    table.insert(textToggle, text1D)
    table.insert(textToggle, text2D)
    table.insert(textToggle, text3D)
    table.insert(textToggle, text1R)
    table.insert(textToggle, text2R)
    table.insert(textToggle, text3R)
    table.insert(textToggle, text4R)
    self._textToggle = textToggle
    
    self._btnPartner = self:FindChildGO("Container_Partner")
    self._containerPartner = self:FindChildGO("Container_Partner/Container_Selected")
    self._csBH:AddClick(self._btnPartner, function()self:PartnerBtnClick() end)
    self._btnSelf = self:FindChildGO("Container_Self")
    self._containerSelf = self:FindChildGO("Container_Self/Container_Selected")
    self._csBH:AddClick(self._btnSelf, function()self:SelfBtnClick() end)
    self._btnSend = self:FindChildGO("Container_Send/Button_Send")
    self._csBH:AddClick(self._btnSend, function()self:SendBtnClick() end)
    self:InitList()
    self._onMarryBlessRecord = function(args)self:OnMarryBlessRecord(args) end
end

function SendBlessingsContainerView:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Invited/ScrollViewList_InvitedList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(BlessingInfoListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1);
end

function SendBlessingsContainerView:OnSendToggleClick(index)
    self._curGiftIndex = index
    -- LoggerHelper.Log("Ash: SendBlessingsContainerView OnSendToggleClick:" .. index)
end

function SendBlessingsContainerView:OnDestroy()
    self._partnerContainerView = nil
    self._selfContainerView = nil
    self._sendToggleGroup = nil
    self._btnPartner = nil
    self._containerPartner = nil
    self._btnSelf = nil
    self._containerSelf = nil
    self._btnSend = nil
    self._list = nil
    self._scrollView = nil
    self._textToggle = nil
    self._iconBg = nil
end

function SendBlessingsContainerView:ShowView()
    self._iconBg = self:FindChildGO("Image_BG")
    GameWorld.AddIcon(self._iconBg,13511)
    local name1, name2, vocation1, vocation2, dbid1, dbid2 = MarriageManager:GetCurWeddingHourData()
    -- LoggerHelper.Log("Ash HoldingWeddingContainerView OnMarriageQueryInfo")
    self._dbid1 = dbid1
    self._dbid2 = dbid2
    self._curDbid = nil
    self._containerPartner:SetActive(false)
    self._containerSelf:SetActive(false)
    if name1 ~= nil then
        self._selfContainerView:SetPlayer(vocation1, name1)
        self._partnerContainerView:SetPlayer(vocation2, name2)
    end
    local giftInfo = GlobalParamsHelper.GetMSortValue(779) or {}-- 结婚送红包和送花配置（道具id：数量）
    for i, v in ipairs(giftInfo) do
        local text = nil
        if v[1] == public_config.MONEY_TYPE_COUPONS then
            text = v[2] .. ItemDataHelper.GetItemName(v[1])
        else
            text = ItemDataHelper.GetItemName(v[1])
        end
        if self._textToggle[i] then
            self._textToggle[i].text = text
        end
    end
    MarriageManager:MarryBlessRecord()
    EventDispatcher:AddEventListener(GameEvents.MarryBlessRecord, self._onMarryBlessRecord)
    self:SelfBtnClick()
end

function SendBlessingsContainerView:CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.MarryBlessRecord, self._onMarryBlessRecord)
    self._curDbid = nil
    self._iconBg = nil
end

function SendBlessingsContainerView:SelfBtnClick()
    self._containerPartner:SetActive(false)
    self._containerSelf:SetActive(true)
    self._curDbid = self._dbid1
end

function SendBlessingsContainerView:PartnerBtnClick()
    self._containerPartner:SetActive(true)
    self._containerSelf:SetActive(false)
    self._curDbid = self._dbid2
end

function SendBlessingsContainerView:SendBtnClick()
    if self._curDbid == nil then
        return
    end
    local giftInfo = GlobalParamsHelper.GetMSortValue(779) or {}-- 结婚送红包和送花配置（道具id：数量）
    local gift = giftInfo[self._curGiftIndex + 1]
    if gift then
        local itemId = gift[1]
        local count = gift[2]
        if public_config.MONEY_TYPE_COUPONS == itemId then
            MarriageManager:MarrySendRed(self._curDbid, count)
        else
            local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(itemId)
            if pos > 0 then
                MarriageManager:MarrySendFlower(self._curDbid, itemId)
            else
                SystemInfoManager:ShowClientTip(error_code.ERR_ITEM_NOT_ENOUGH)
            end
        end
    end
end

function SendBlessingsContainerView:OnMarryBlessRecord(args)
    self._list:SetDataList(args)
end
