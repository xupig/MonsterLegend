-- DivorceContainerView.lua
local DivorceContainerView = Class.DivorceContainerView(ClassTypes.BaseComponent)
DivorceContainerView.interface = GameConfig.ComponentsConfig.Component
DivorceContainerView.classPath = "Modules.ModuleMarriage.ChildView.DivorceContainerView"
require "Modules.ModuleMarriage.ChildView.WeddingPlayerContainerView"
local WeddingPlayerContainerView = ClassTypes.WeddingPlayerContainerView

local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local MarriageManager = PlayerManager.MarriageManager

function DivorceContainerView:Awake()
    self._partnerContainerView = self:AddChildLuaUIComponent("Container_Partner", WeddingPlayerContainerView)
    self._selfContainerView = self:AddChildLuaUIComponent("Container_Self", WeddingPlayerContainerView)
 
    self._btnDivorce = self:FindChildGO("Button_Divorce")
    self._csBH:AddClick(self._btnDivorce, function()self:DivorceBtnClick() end)
end

function DivorceContainerView:OnDestroy()
end

function DivorceContainerView:ShowView()
    self._iconBg = self:FindChildGO("Image_BG")
    GameWorld.AddIcon(self._iconBg,13511)
    self._selfContainerView:ShowSelf(true)
    self._partnerContainerView:SetPlayer(MarriageManager:GetMateVocation(), MarriageManager:GetMateName())
end

function DivorceContainerView:CloseView()
    self._iconBg = nil
end

function DivorceContainerView:DivorceBtnClick()
    GUIManager.ShowMessageBox(1, LanguageDataHelper.GetContent(59074),
    function()
        MarriageManager:Divorce()
        GUIManager.ClosePanel(PanelsConfig.GetMarry)
        GUIManager.ShowPanel(PanelsConfig.GetMarry)
    end)
end
