-- BabyContainerView.lua
require"Modules.ModuleMarriage.ChildComponent.BodyTypeItem"
require"Modules.ModuleMarriage.ChildComponent.BodySecTypeItem"
require "Modules.ModuleMarriage.ChildComponent.MarriageAttrItem"
require "Modules.ModuleMarriage.ChildComponent.MarryUpCostItem"
require "Modules.ModuleMarriage.ChildComponent.BodySkillItem"
require "Modules.ModulePartner.ChildView.PartnerTipsShowView"

local BabyContainerView = Class.BabyContainerView(ClassTypes.BaseComponent)
BabyContainerView.interface = GameConfig.ComponentsConfig.Component
BabyContainerView.classPath = "Modules.ModuleMarriage.ChildView.BabyContainerView"
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local UINavigationMenu = ClassTypes.UINavigationMenu
local BodyTypeItem = ClassTypes.BodyTypeItem
local BodySecTypeItem = ClassTypes.BodySecTypeItem
local MarryChildbaseDataHelper = GameDataHelper.MarryChildbaseDataHelper
local MarriageAttrItem = ClassTypes.MarriageAttrItem
local UIComponentUtil = GameUtil.UIComponentUtil
local MarryChildattriDataHelper = GameDataHelper.MarryChildattriDataHelper
local ActorModelComponent = GameMain.ActorModelComponent
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local RectTransform = UnityEngine.RectTransform
local UIToggle = ClassTypes.UIToggle
local MarryUpCostItem = ClassTypes.MarryUpCostItem
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local XArtNumber = GameMain.XArtNumber
local BodySkillItem = ClassTypes.BodySkillItem
local UIList = ClassTypes.UIList
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local MarriageRaiseData = PlayerManager.PlayerDataManager.marriageRaiseData
local MarriageRaiseManager = PlayerManager.MarriageRaiseManager
local BaseUtil = GameUtil.BaseUtil
local PartnerTipsShowView = ClassTypes.PartnerTipsShowView
local AttriDataHelper = GameDataHelper.AttriDataHelper

local UIParticle = ClassTypes.UIParticle
local TransformManager = PlayerManager.TransformManager
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

function BabyContainerView:Awake()
    self:InitCallBack()
    self:InitProgressBar()
    self:InitView()
    self._isextend=false
    self.preLe=1
    self._seleLevel=1
    -- self._menuData = MarryChildbaseDataHelper:GetTypeDataCfg()
	-- self._menu:SetDataList(self._menuData)
end
function BabyContainerView:InitView()
    self._indexjie = {}
    self._indexjie[1]=4016
    self._indexjie[2]=4017
    self._indexjie[3]=4018
    self._indexjie[4]=4019
    self._indexjie[5]=4020
    self._indexjie[6]=4021
    self._indexjie[7]=4022
    self._indexjie[8]=4023
    self._indexjie[9]=4024
    self._indexjie[10]=4025
    self._toggle = UIToggle.AddToggle(self.gameObject,"Container_Right/Toggle_Hide")
    self._btnOnShow = self:FindChildGO("Container_Right/Toggle_Hide/Background")
    self._checkmarkShow = self:FindChildGO("Container_Right/Toggle_Hide/Checkmark")
    self._toggle:SetOnValueChangedCB(function(stage)self:OnToggleClick(stage)end)
    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "NavigationMenu_Item")
	self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(BodyTypeItem)
	self._menu:SetSecondLevelItemType(BodySecTypeItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(3, 0)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)

    self._levelGo = self:FindChildGO("Container_Right/Container_Title/Container_Level")

    self._ToggleGo = self:FindChildGO("Container_Right/Toggle_Hide")

	self._txtname = self:GetChildComponent("Container_Right/Container_Title/Text_AttrValue",'TextMeshWrapper')

    self._Attrs = {}
    self._AttrGos = {}
    for i=1,4 do
        local itemGo = self:FindChildGO("Container_Right/Container_Attrs/Container_Item"..i)
        local item = UIComponentUtil.AddLuaUIComponent(itemGo, MarriageAttrItem) 
        itemGo:SetActive(false)
        self._AttrGos[i] = itemGo
   		self._Attrs[i] = item
    end
    local itemGo = self:FindChildGO("Container_Right/Container_Items/item")
    self._costitem = UIComponentUtil.AddLuaUIComponent(itemGo, MarryUpCostItem) 
    
    self._roleModeGo = self:FindChildGO("Container_Right/Container_Model")
    self._rolesMode = self:AddChildComponent("Container_Right/Container_Model",ActorModelComponent);

    local btnup = self:FindChildGO("Container_Right/Button_Upgrade")
    self.btnupRed = self:FindChildGO("Container_Right/Button_Upgrade/Image_Tip")
    self.btnupRed:SetActive(false)
    self.txtup = self:GetChildComponent("Container_Right/Button_Upgrade/Text",'TextMeshWrapper')
    self._csBH:AddClick(btnup,function()self:onBtnUp()end)

    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'Container_Right/Container_Skill/ScrollViewList')
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(BodySkillItem)
    self._list:SetPadding(10,0,0,18)
    self._list:SetGap(0,10)
    self._list:SetDirection(UIList.DirectionTopToDown,1,-1)
    local itemClickedCB = 
	function(index)
		self:OnListItemClicked(index)
	end
    self._list:SetItemClickedCB(itemClickedCB)

    self._fpNumber = self:AddChildComponent('Container_Right/Container_FightPower', XArtNumber)

    self._NotActiveGo = self:FindChildGO("Container_Right/Image_NotActive")
    self._IconGo = self:FindChildGO("Container_Right/Image_Icon")
    self._fpNumberGo = self:FindChildGO("Container_Right/Container_FightPower")

    self._btnuptwrap = self:GetChildComponent("Container_Right/Button_Upgrade","ButtonWrapper")

    self.txtstartTips = self:GetChildComponent("Container_Right/Image_NotActive/Text_TipsStart",'TextMeshWrapper')
    self.tipsShowView = self:AddChildLuaUIComponent("Container_Right/Container_TipsShow", PartnerTipsShowView)

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Right/Container_Fx/fx_ui_30034")
    self._fx:SetLifeTime(2)
    self._fx:Stop()

    self._LevelUpGo = self:FindChildGO("Container_Right/Image_LevelUp")
    self._LevelUpGo.transform.localScale = Vector3.zero
    self._tweenScaleLevelUp = self:AddChildComponent('Container_Right/Image_LevelUp', XGameObjectTweenScale)
end

function BabyContainerView:OnListItemClicked(idx)
    self._list:GetItem(idx):OnPointerDown()
end

function BabyContainerView:InitProgressBar()
    self._progressBarGo = self:FindChildGO("Container_Right/ProgressBar_Bless")
    self._progressBarLength = self._progressBarGo:GetComponent(typeof(RectTransform)).sizeDelta.x
    self._barComp = UIScaleProgressBar.AddProgressBar(self.gameObject,"Container_Right/ProgressBar_Bless")
    self._barComp:SetProgress(0)
    self._barComp:ShowTweenAnimate(true)
    self._barComp:SetMutipleTween(false)
    self._barComp:SetIsResetToZero(false)
    self._barComp:SetTweenTime(0.2)
    self._TextAttach = self:GetChildComponent("Container_Right/ProgressBar_Bless/Text_Attach", "TextMeshWrapper")
end

function BabyContainerView:ShowLevelUp()
    self._tweenScaleLevelUp:OnceOutQuad(1, 1, function()  
        self._LevelUpGo.transform.localScale = Vector3.zero
    end)
    
    self._fx:Play()
end

function BabyContainerView:onBtnUp()
    if MarriageRaiseData:IsActiveBady(self._selectedComposeData.id) then
        if self._isQuick==true then
            self._isQuick=false
            self.txtup.text = LanguageDataHelper.GetContent(59094)
            return
        end
        local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._costid)
        if pos~=0 then
            self.txtup.text = LanguageDataHelper.GetContent(58621)
            self._isQuick=true
            self:UpRpc()
        else
            local showText = LanguageDataHelper.GetContent(80219)
            GUIManager.ShowText(2, showText)
        end
    else
        self.txtup.text = LanguageDataHelper.GetContent(59130)
        MarriageRaiseManager:MarryPetActiveReq(self._selectedComposeData.id)
    end
end

function BabyContainerView:UpRpc()
    local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._costid)
    if pos~=0 then
        MarriageRaiseManager:MarryPetUpReq(self._selectedComposeData.id,pos)
    else
        local showText = LanguageDataHelper.GetContent(80219)
        GUIManager.ShowText(2, showText)
    end
end

function BabyContainerView:InitCallBack()
    self._updateOnShowCb =function ()self:UpdateOnShow()end
    self._upresp = function(errorid)self:UpResp(errorid)end
    self._activeresp = function(errorid)self:ActiveResp(errorid)end
end

function BabyContainerView:OnToggleClick(stage)
    TransformManager:RequestTransformShow(public_config.TREASURE_TYPE_PET,public_config.TREASURE_SHOW_ORIGIN_MARRY,self._selectedComposeData.id)
end

function BabyContainerView:OnMenuFItemClicked(idx)
    self._isextend=not self._isextend
	if self._selectedFItem then
		self._selectedFItem:SetSelected(false)
		if self._selectedFIndex and self._selectedFIndex == idx then
			return
		end
	end
	self._selectedFItem = self._menu:GetFItem(idx)
	self._selectedFIndex = idx
    self._selectedFItem:ToggleSelected()
    self._isQuick=false
    self.txtup.text = LanguageDataHelper.GetContent(59094)
end

function BabyContainerView:OnMenuSItemClicked(fIndex,sIndex)
	if self._selectedSItem then
		self._selectedSItem:SetSelected(false)
    end
    self._fIndex = fIndex
    self._sIndex = sIndex
    self:OnMenuSItemUpdate(fIndex,sIndex)
    local BadyId=self._selectedComposeData.id
    if MarriageRaiseData:IsActiveBady(BadyId) then
        self.preLe = MarriageRaiseData:GetMarrayBadyInfo()[BadyId][2]
        self:UpdateOnShow()
        self.txtup.text = LanguageDataHelper.GetContent(59094)
    else
        self.pre=1
        self.txtup.text = LanguageDataHelper.GetContent(59130)
    end
    self._isQuick=false
end

function BabyContainerView:OnMenuSItemUpdate(fIndex,sIndex)
	self._selectedComposeData = self._menu:GetSecondLevelDataByIndex(fIndex,sIndex)
	self._selectedSItem = self._menu:GetSItem(fIndex,sIndex)
    self._selectedSItem:SetSelected(true)
    self:UpdateComposeInfo()
end

function BabyContainerView:InitInfo()
    local id,num = next(GlobalParamsHelper.GetParamValue(781))
    self._costid = id
    self._costnum = num
    if not self._isextend then
        self._menu:SelectFItem(0)
        self._isextend=true
    end
    self:OnMenuSItemClicked(0,0)
end

function BabyContainerView:UpdateComposeInfo()
    self._txtname.text = LanguageDataHelper.GetContent(self._selectedComposeData.name)
    local marryatts={}
    local nextcoupleatts=nil
    local BadyId=self._selectedComposeData.id
    if MarriageRaiseData:IsActiveBady(BadyId) then
        self._NotActiveGo:SetActive(false)
        self._IconGo:SetActive(true)
        self._btnuptwrap.interactable = true
        local level = MarriageRaiseData:GetMarrayBadyInfo()[BadyId][2]
        self._seleLevel = level
        GameWorld.AddIcon(self._IconGo,self._indexjie[level])
        marryatts = MarryChildattriDataHelper:GetCfgAttByidLevel(BadyId,level).attri
        local cfg = MarryChildattriDataHelper:GetCfgAttByidLevel(BadyId,level+1)
        if cfg then
            nextcoupleatts = cfg.attri
        end
        self._toggle:SetIsOn(true)
        self._ToggleGo:SetActive(true)
        local roleAttris = marryatts
        local attriList = {}
        for k,v in pairs(roleAttris) do
            table.insert(attriList, {["attri"]=k, ["value"]=v})
        end
        local x=1   
        for attid,v in pairs(marryatts) do
            attid = tonumber(attid)
            if nextcoupleatts then
                if AttriDataHelper:GetShowType(attid)==1 then
                    self._Attrs[x]:OnRefreshData({attid,v,(nextcoupleatts[attid]-v)})
                else
                    local n = (nextbaseatts[attid]-v)/100
                    local a = string.format("%.2f",n).."%"
                    self._Attrs[x]:OnRefreshData({attid,v,a})
                end
            else
                self._Attrs[x]:OnRefreshData({attid,v,0})
            end
            self._AttrGos[x]:SetActive(true)
            x = x+1
        end
        if nextcoupleatts==nil then
            self._barComp:SetProgress(0)
            self._TextAttach.text = ""
        else
            local ownCount = MarriageRaiseData:GetMarrayBadyInfo()[BadyId][3]
            local exp = MarryChildattriDataHelper:GetCfgAttByidLevel(BadyId,level).exp
            self._TextAttach.text = ownCount..'/'..exp
            self._barComp:SetProgress(ownCount/exp)
        end
        local fightPower = BaseUtil.CalculateFightPower(attriList, GameWorld.Player().vocation)
        self._fpNumber:SetNumber(fightPower)
        self._fpNumberGo:SetActive(true)
        self._progressBarGo:SetActive(true)
        self._costitem:OnRefreshData({self._costid,1,false})
        if not self._isQuick then
            self.txtup.text = LanguageDataHelper.GetContent(59094)        
        end
    else
        self._ToggleGo:SetActive(false)
        self._NotActiveGo:SetActive(true)
        self._IconGo:SetActive(false)
        marryatts = MarryChildattriDataHelper:GetCfgAttByidLevel(BadyId,1).attri
        local roleAttris = marryatts
        local attriList = {}
        for k,v in pairs(roleAttris) do
            table.insert(attriList, {["attri"]=k, ["value"]=0})
        end
        local x=1   
        for attid,v in pairs(marryatts) do
            attid = tonumber(attid)
            if AttriDataHelper:GetShowType(attid)==1 then
                self._Attrs[x]:OnRefreshData({attid,0,v})
            else
                local a = string.format("%.2f",v/100).."%"
                self._Attrs[x]:OnRefreshData({attid,0,a})
            end
            self._AttrGos[x]:SetActive(true)
            x = x+1
        end
        self._progressBarGo:SetActive(false)
        self._fpNumberGo:SetActive(false)
        self.txtup.text = LanguageDataHelper.GetContent(59130)
        local d=MarryChildbaseDataHelper:GetUnlock(BadyId)
        if self._sIndex==0 then
            local a = MarryChildbaseDataHelper:GetTypeDataCfg()[self._fIndex+1][2].unlock
            self._costitem:OnRefreshData({a[2][1],a[2][2],false}) 
        elseif d[2]then
            self._costitem:OnRefreshData({d[2][1],d[2][2],false})
        else
            self._costitem:OnRefreshData({self._costid,1,false})
        end
        local n = LanguageDataHelper.GetContent(MarryChildbaseDataHelper:GetName(BadyId-1))
        local nameid = MarryChildbaseDataHelper:GetUnlockChinese(BadyId)
--      1-某个宝宝达到N阶
--      2-需要指定道具{类型,道具id,数量,宝宝ID,阶数
--      3-婚戒,0,婚戒等级
        if d[1] then
            local info = MarriageRaiseData:GetMarrayBadyInfo()[d[1][1]]
            local level=0
            if info then
                level=info[2]
            end
            if d[1][2]>level then
                self._btnuptwrap.interactable = false
            else
                self._btnuptwrap.interactable = true
            end
            self.txtstartTips.text=LanguageDataHelper.CreateContentWithArgs(nameid,{["0"]=n,["1"]=d[1][2]}) 
        else
            self._btnuptwrap.interactable = true
            self.txtstartTips.text=LanguageDataHelper.GetContent(nameid)
        end
               
    end
    local badyid = self._selectedComposeData.id
    self._rolesMode:SetStartSetting(Vector3(0,0,-350),Vector3(0,180,0),1)        
    self._rolesMode:LoadModel(MarryChildbaseDataHelper:GetMode(badyid))
  
    local result = MarryChildbaseDataHelper:GetPassiveSkill(badyid)
    local items={}
    if result then
        for k,v in pairs(result) do
            local f=MarriageRaiseData:IsSkillActive(v,badyid)
            table.insert(items,{tonumber(k),v,MarryChildbaseDataHelper:GetName(badyid),f})
        end
        table.sort(items,function(a,b)
            if a[2]<b[2] then
                return true
            end
            return false
        end)
        self._list:SetDataList(items)
    end
end

function BabyContainerView:OnDestroy()
end

function BabyContainerView:ShowView()
    self._menuData = MarryChildbaseDataHelper:GetTypeDataCfg()
	self._menu:SetDataList(self._menuData)
    self:AddListener()
    self:InitInfo()
end


function BabyContainerView:UpdateOnShow()
    local showInfo = GameWorld.Player().illusion_show_info[public_config.TREASURE_TYPE_PET]
    if showInfo[public_config.TREASURE_ILLUSION_SHOW_SYSTEM] == public_config.TREASURE_SHOW_ORIGIN_MARRY and
        showInfo[public_config.TREASURE_ILLUSION_SHOW_ID] == self._selectedComposeData.id then
        self._btnOnShow:SetActive(false)
        self._checkmarkShow:SetActive(true)
    else
        self._btnOnShow:SetActive(true)
        self._checkmarkShow:SetActive(false)
    end
end

function BabyContainerView:UpResp(errorid)
    if errorid~=0 then
        if self._isQuick==true then
            self._isQuick=false
        end
    else
        self.tipsShowView:ShowView(self._costnum)
        if self._isQuick==true then
            self:UpRpc()
        end
    end
    self._menu:GetSItem(self._fIndex,self._sIndex):RefreshData()
    self:UpdateInfo()
    if self.preLe<self._seleLevel then
        self:ShowLevelUp()
        self.preLe=self._seleLevel
    end
end

function BabyContainerView:UpdateInfo()
    self:OnMenuSItemUpdate(self._fIndex,self._sIndex)
end



function BabyContainerView:ActiveResp(errorid)
    if errorid==0 then
        self:UpdateInfo()
    end
    self._menu:GetSItem(self._fIndex,self._sIndex):RefreshData()
end

function BabyContainerView:CloseView()
    self:RemoveListener()
    self._isQuick=false
    self.txtup.text = LanguageDataHelper.GetContent(59094)
end

function BabyContainerView:AddListener()
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)
    EventDispatcher:AddEventListener(GameEvents.MarryBadyUpdate,self._upresp)
    EventDispatcher:AddEventListener(GameEvents.MarryBadyActivedate,self._activeresp)
end
function BabyContainerView:RemoveListener()
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)
    EventDispatcher:RemoveEventListener(GameEvents.MarryBadyUpdate,self._upresp)
    EventDispatcher:RemoveEventListener(GameEvents.MarryBadyActivedate,self._activeresp)
end

