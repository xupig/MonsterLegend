-- MarriageInfoContainerView.lua
local MarriageInfoContainerView = Class.MarriageInfoContainerView(ClassTypes.BaseComponent)
MarriageInfoContainerView.interface = GameConfig.ComponentsConfig.Component
MarriageInfoContainerView.classPath = "Modules.ModuleMarriage.ChildView.MarriageInfoContainerView"
require "Modules.ModuleMarriage.ChildView.MarriagePlayerContainerView"
local MarriagePlayerContainerView = ClassTypes.MarriagePlayerContainerView
require "Modules.ModuleMarriage.ChildView.MarriageTitleInfoContainerView"
local MarriageTitleInfoContainerView = ClassTypes.MarriageTitleInfoContainerView
require "UIComponent.Extend.TipsCom"
local TipsCom = ClassTypes.TipsCom

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local MarriageManager = PlayerManager.MarriageManager
local TitleManager = PlayerManager.TitleManager
local FriendData = PlayerManager.PlayerDataManager.friendData
local MarriageRaiseData = PlayerManager.PlayerDataManager.marriageRaiseData
local MarriageRaiseManager = PlayerManager.MarriageRaiseManager
local PlayerActionManager = GameManager.PlayerActionManager

function MarriageInfoContainerView:Awake()
    self._partnerContainerView = self:AddChildLuaUIComponent("Container_Partner", MarriagePlayerContainerView)
    self._selfContainerView = self:AddChildLuaUIComponent("Container_Self", MarriagePlayerContainerView)
    self._titleInfoContainerView = self:AddChildLuaUIComponent("Container_TitleInfo", MarriageTitleInfoContainerView)
    self._textIntimacy = self:GetChildComponent("Container_Bottom/Text_Intimacy", "TextMeshWrapper")
    self._textWeddingDays = self:GetChildComponent("Container_Bottom/Text_WeddingDays", "TextMeshWrapper")
    

    self._parterGo = self:FindChildGO("Container_Partner")
    self._selfGO = self:FindChildGO("Container_Self")

    self._btnViewTitle = self:FindChildGO("Container_Center/Button_ViewTitle")
    self._csBH:AddClick(self._btnViewTitle, function()self:ViewTitleBtnClick() end)
    
    self._btnGetMarry = self:FindChildGO("Container_Bottom/Button_GetMarry")
    self._csBH:AddClick(self._btnGetMarry, function()self:GetMarryBtnClick() end)
    self._onMarriageQueryInfo = function()self:OnMarriageQueryInfo() end
    self._setTitleFx = function()self:SetFxV() end

    self._containerTitle = self:FindChildGO("Container_Center/Container_Title")
    self._textContent = self:GetChildComponent("Container_Center/Text_TitleProgress", "TextMeshWrapper")
    
    self._IntrodueGO = self:FindChildGO("Container_Introdue")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Introdue", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(59165))
    self._IntrodueGO:SetActive(false)

    self._btnGetMarry = self:FindChildGO("Container_Bottom/Button_WeddingPlan")
    self._csBH:AddClick(self._btnGetMarry, function()self:IntroduceBtnClick() end)

end

function MarriageInfoContainerView:IntroduceBtnClick()
    self._IntrodueGO:SetActive(not self._IntrodueGO.activeSelf)
end

function MarriageInfoContainerView:OnDestroy()
end

function MarriageInfoContainerView:ShowView()
    EventDispatcher:AddEventListener(GameEvents.MarriageQueryInfo, self._onMarriageQueryInfo)
    EventDispatcher:AddEventListener(GameEvents.MarriageTitleFx, self._setTitleFx)
    self._titleInfoContainerView.gameObject:SetActive(false)
    self._selfContainerView:ShowSelf()
    local haveMate = MarriageManager:HaveMate()
    if haveMate then
        MarriageManager:QueryInfo()
    end
    self:UpdateMateInfo()
    self:ShowModel()
end

function MarriageInfoContainerView:UpdateMateInfo()
    local haveMate = MarriageManager:HaveMate()
    if haveMate then
        local  x = GameWorld.Player().marry_time
        local item  = FriendData:GetFriendInfoByDbid(MarriageManager:GetMateDbid())
        if item then
            self._textIntimacy.text = item:GetFriendlyVal()..''
        end
        self._textWeddingDays.text = DateTimeUtil.MinusServerDay(x)['day']..LanguageDataHelper.GetContent(147)
    else
        self._textIntimacy.text = '0'
        self._partnerContainerView:ShowNoPartner()
        self._textWeddingDays.text = "0"..LanguageDataHelper.GetContent(147)
    end
end

function MarriageInfoContainerView:CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.MarriageQueryInfo, self._onMarriageQueryInfo)
    EventDispatcher:RemoveEventListener(GameEvents.MarriageTitleFx, self._setTitleFx)
    self:HideModel()
    GameWorld.ShowPlayer():SyncShowPlayerFacade()
end

function MarriageInfoContainerView:OnMarriageQueryInfo()
    local item  = FriendData:GetFriendInfoByDbid(MarriageManager:GetMateDbid())
    if item then
        self._textIntimacy.text = item:GetFriendlyVal()..''
    end
    local title = self:GetNOTitleId()
    local titleitem = MarriageRaiseData:GetMadeTitleItemByData(title[2])
    if title[1]==1 then
        self._textContent.text = LanguageDataHelper.CreateContentWithArgs(59143,{['0']=100})
    else
        local per = MarriageRaiseManager:UpdtePro(titleitem:GetCondition())
        self._textContent.text = LanguageDataHelper.CreateContentWithArgs(59143,{['0']=string.format("%.f",per*100).."%"})
    end
    GameWorld.AddIconAnim(self._containerTitle,titleitem:GetIcon())
    GameWorld.SetIconAnimSpeed(self._containerTitle, 1)
    self._partnerContainerView:ShowPartner(MarriageManager:GetMateVocation(), MarriageManager:GetMateName(), MarriageManager:GetMateFacade())
    self:UpdateMateInfo()
end

function MarriageInfoContainerView:GetNOTitleId()
    local cfg = MarriageRaiseData:GetcfgTitles()
    for k,v in ipairs(cfg) do
        local state = TitleManager:GetStateByTitleId(v.id)
        if state == 0 then
            return {0,v.id}  
        end
    end
    return {1,cfg[#cfg].id}
end

function MarriageInfoContainerView:ViewTitleBtnClick()
    self._selfContainerView:HideFx()
    self._partnerContainerView:HideFx()
    self._titleInfoContainerView.gameObject:SetActive(true)
end

function MarriageInfoContainerView:GetMarryBtnClick()
    MarriageManager:GotoMarriage()
    GUIManager.ClosePanel(PanelsConfig.Marriage)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_marry_tomarry_button")
end

function MarriageInfoContainerView:SetFxV()
    self._selfContainerView:ShowFx()
    self._partnerContainerView:ShowFx()
end

function MarriageInfoContainerView:ShowModel()
    self._partnerContainerView:Show()
    self._selfContainerView:Show()
end

function MarriageInfoContainerView:HideModel()
    self._partnerContainerView:Hide()
    self._selfContainerView:Hide()
end