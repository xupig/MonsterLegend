-- GuestManagerMainUIView.lua
local GuestManagerMainUIView = Class.GuestManagerMainUIView(ClassTypes.BaseComponent)
require "Modules.ModuleMarriage.ChildComponent.MainUIGuestManagerListItem"
local MainUIGuestManagerListItem = ClassTypes.MainUIGuestManagerListItem

GuestManagerMainUIView.interface = GameConfig.ComponentsConfig.Component
GuestManagerMainUIView.classPath = "Modules.ModuleMarriage.ChildView.GuestManagerMainUIView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil

local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MarriageManager = PlayerManager.MarriageManager
local GUIManager = GameManager.GUIManager

function GuestManagerMainUIView:Awake()
    self:InitList()
    self._textInviteCount = self:GetChildComponent("Text_InviteCount", "TextMeshWrapper")
    self._btnBuyCount = self:FindChildGO("Button_BuyCount")
    self._btnClose = self:FindChildGO("Button_Close")
    self._btnRejectAll = self:FindChildGO("Button_RejectAll")
    self._btnAgreeAll = self:FindChildGO("Button_AgreeAll")
    self._csBH:AddClick(self._btnBuyCount, function()self:BuyCountBtnClick() end)
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    self._csBH:AddClick(self._btnRejectAll, function()self:RejectAllBtnClick() end)
    self._csBH:AddClick(self._btnAgreeAll, function()self:AgreeAllBtnClick() end)
    self._onMarriageAskInviteChanged = function()self:OnMarriageAskInviteChanged() end
    self._onMarriageBookInfo = function()self:OnMarriageBookInfo() end
end

function GuestManagerMainUIView:ShowView()
    self:OnMarriageBookInfo()
    self._list:SetDataList(MarriageManager:GetAskInviteList())
    -- LoggerHelper.Log("Ash: GuestManagerMainUIView OnEnable:", true)
    EventDispatcher:AddEventListener(GameEvents.MarriageAskInviteChanged, self._onMarriageAskInviteChanged)
    EventDispatcher:AddEventListener(GameEvents.MarriageBookInfo, self._onMarriageBookInfo)
end

function GuestManagerMainUIView:HideView()
    -- LoggerHelper.Log("Ash: GuestManagerMainUIView OnDisable:", true)
    -- self._list:SetAllItemDirty()
    EventDispatcher:RemoveEventListener(GameEvents.MarriageAskInviteChanged, self._onMarriageAskInviteChanged)
    EventDispatcher:RemoveEventListener(GameEvents.MarriageBookInfo, self._onMarriageBookInfo)
end

function GuestManagerMainUIView:OnDestroy()
end

function GuestManagerMainUIView:OnMarriageAskInviteChanged()
    -- LoggerHelper.Log("Ash: OnReceiveAsk: " .. PrintTable:TableToStr(MarriageManager:GetAskInviteList()))
    self._list:SetDataList(MarriageManager:GetAskInviteList())
end

function GuestManagerMainUIView:OnMarriageBookInfo()
    self._textInviteCount.text = MarriageManager:GetMyWeddingGuestCount() .. "/" .. MarriageManager:GetMyGuestCount()
end

function GuestManagerMainUIView:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/ScrollViewList_InviteList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(MainUIGuestManagerListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1);
end

function GuestManagerMainUIView:BuyCountBtnClick()
    MarriageManager:BuyInviteCount()
end

function GuestManagerMainUIView:CloseBtnClick()
    GUIManager.ClosePanel(PanelsConfig.MarriageGuestManager)
end

function GuestManagerMainUIView:RejectAllBtnClick()
    MarriageManager:RemoveAllInviteGuest()
end

function GuestManagerMainUIView:AgreeAllBtnClick()
    MarriageManager:AgreeAllInviteGuest()
end
