-- ShowLoveContainerView.lua
local ShowLoveContainerView = Class.ShowLoveContainerView(ClassTypes.BaseComponent)
ShowLoveContainerView.interface = GameConfig.ComponentsConfig.Component
ShowLoveContainerView.classPath = "Modules.ModuleMarriage.ChildView.ShowLoveContainerView"
require "Modules.ModuleMarriage.ChildView.WeddingPlayerContainerView"
local WeddingPlayerContainerView = ClassTypes.WeddingPlayerContainerView
require "Modules.ModuleMarriage.ChildView.ShowLoveItemContainerView"
local ShowLoveItemContainerView = ClassTypes.ShowLoveItemContainerView
require "Modules.ModuleMarriage.ChildComponent.PartnerListItem"
local PartnerListItem = ClassTypes.PartnerListItem

-- local SceneConfig = GameConfig.SceneConfig
-- local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local FriendManager = PlayerManager.FriendManager
local MarriageManager = PlayerManager.MarriageManager
local CreateRoleManager = GameManager.CreateRoleManager

function ShowLoveContainerView:Awake()
    self._partnerContainerView = self:AddChildLuaUIComponent("Container_Partner", WeddingPlayerContainerView)
    self._selfContainerView = self:AddChildLuaUIComponent("Container_Self", WeddingPlayerContainerView)
    
    self._btnSelectPartner = self:FindChildGO("Button_SelectPartner")
    self._btnHelp = self:FindChildGO("Button_Help")
    self._btnConfirm = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(self._btnSelectPartner, function()self:SelectPartnerBtnClick() end)
    self._csBH:AddClick(self._btnHelp, function()self:HelpBtnClick() end)
    self._csBH:AddClick(self._btnConfirm, function()self:ConfirmBtnClick() end)
    self._containerContent = self:FindChildGO("Container_Content")
    self._containerContent:SetActive(false)
    self:InitPartnerList()
    self._btnPartnerListMask = self:FindChildGO("Container_PartnerList/Image_Mask")
    self._csBH:AddClick(self._btnPartnerListMask, function()self:PartnerListMaskClick() end)
    self._containerPartnerList = self:FindChildGO("Container_PartnerList")
    self._containerPartnerList:SetActive(false)
    
    self._selection1ContainerView = self:AddChildLuaUIComponent("Container_Selection/Container_1", ShowLoveItemContainerView)
    self._selection2ContainerView = self:AddChildLuaUIComponent("Container_Selection/Container_2", ShowLoveItemContainerView)
    self._selection3ContainerView = self:AddChildLuaUIComponent("Container_Selection/Container_3", ShowLoveItemContainerView)
    local onSelectedCallback = function(id)self:OnSelectedCallback(id) end
    self._selection1ContainerView:SetSelectedCallback(1, onSelectedCallback)
    self._selection2ContainerView:SetSelectedCallback(2, onSelectedCallback)
    self._selection3ContainerView:SetSelectedCallback(3, onSelectedCallback)
    
    self._onRefreshFriendList = function()self:OnRefreshFriendList() end
end

function ShowLoveContainerView:OnDestroy()
end

function ShowLoveContainerView:InitPartnerList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_PartnerList/ScrollViewList")
    self._listPartner = list
    self._scrollViewPartner = scrollView
    self._listPartner:SetItemType(PartnerListItem)
    self._listPartner:SetPadding(0, 0, 0, 0)
    self._listPartner:SetGap(0, 0)
    self._listPartner:SetDirection(UIList.DirectionTopToDown, 1, 1);
    self._listPartner:SetDataList(self._listData)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._listPartner:SetItemClickedCB(itemClickedCB)
end

function ShowLoveContainerView:OnListItemClicked(idx)
    local data = self._listPartner:GetDataByIndex(idx)
    self._partnerContainerView:SetPlayer(data:GetVocation(), data:GetName())
    self._curPartnerData = data
    self:PartnerListMaskClick()
end

function ShowLoveContainerView:ShowView()
    self._iconBg = self:FindChildGO("Image_BG")
    GameWorld.AddIcon(self._iconBg,13511)
    self._selectedId = nil
    self._curPartnerData = nil
    self._selfContainerView:ShowSelf(true)
    self._partnerContainerView:ClearPlayer(LanguageDataHelper.GetContent(59072))
    -- LoggerHelper.Log("Ash ShowLoveItemContainerView ShowView " .. PrintTable:TableToStr(GlobalParamsHelper.GetParamValue(734)))
    -- LoggerHelper.Log("Ash ShowLoveItemContainerView ShowView " .. tostring(GlobalParamsHelper.GetParamValue(734)))
    self._selection1ContainerView:SetPrice(public_config.MONEY_TYPE_COUPONS_BIND, GlobalParamsHelper.GetParamValue(734))-- 结婚档次第1档价格（绑钻）
    self._selection2ContainerView:SetPrice(public_config.MONEY_TYPE_COUPONS, GlobalParamsHelper.GetParamValue(735))-- 结婚档次第2档价格（钻石）
    self._selection3ContainerView:SetPrice(public_config.MONEY_TYPE_COUPONS, GlobalParamsHelper.GetParamValue(736))-- 结婚档次第3档价格（钻石）
    self._selection1ContainerView:ShowRewards(GlobalParamsHelper.GetParamValue(737))-- 结婚档次第1档奖励
    self._selection2ContainerView:ShowRewards(GlobalParamsHelper.GetParamValue(738))-- 结婚档次第2档奖励
    self._selection3ContainerView:ShowRewards(GlobalParamsHelper.GetParamValue(739))-- 结婚档次第3档奖励
    self._selection1ContainerView:SetWeddingTimes(0)
    self._selection2ContainerView:SetWeddingTimes(1)
    self._selection3ContainerView:SetWeddingTimes(1)
    self._selection1ContainerView:SetSelected(false)
    self._selection2ContainerView:SetSelected(false)
    self._selection3ContainerView:SetSelected(false)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleMarriage.ChildView.ShowLoveContainerView")
end

function ShowLoveContainerView:CloseView()
    self._selectedId = nil
    self._curPartnerData = nil
    self._iconBg = nil
end

function ShowLoveContainerView:OnSelectedCallback(id)
    self._selectedId = id
    self._selection1ContainerView:SetSelected(false)
    self._selection2ContainerView:SetSelected(false)
    self._selection3ContainerView:SetSelected(false)
end

function ShowLoveContainerView:SelectPartnerBtnClick()
    EventDispatcher:AddEventListener(GameEvents.RefreshFriendList, self._onRefreshFriendList)
    FriendManager:SendFriendListReq()
    self._containerPartnerList:SetActive(true)
end

function ShowLoveContainerView:OnRefreshFriendList()
    local result = {}
    local levelThresHold = GlobalParamsHelper.GetParamValue(744)-- 结婚婚礼玩家参与等级
    local friendlyValThresHold = GlobalParamsHelper.GetParamValue(733)-- 结婚好友亲密度条件
    local friendList = FriendManager:GetFriendListData()
    local player = GameWorld.Player()
    local playerName = player.name
    local playerGender = CreateRoleManager:GetGenderByVocation(player.vocation)
    for i, v in ipairs(friendList) do
        if v:GetLevel() >= levelThresHold
            and v:GetFriendlyVal() >= friendlyValThresHold
            and v:GetIsOnline() == 1
            and CreateRoleManager:GetGenderByVocation(v:GetVocation()) ~= playerGender
            and (v:GetMateName() == "" or v:GetMateName() == playerName) then
            table.insert(result, v)
        end
    end
    --   LoggerHelper.Log("Ash OnRefreshFriendList " .. PrintTable:TableToStr(result))
    self._listPartner:SetDataList(result)
end

function ShowLoveContainerView:HelpBtnClick()
    self._containerContent:SetActive(true)
end

function ShowLoveContainerView:ConfirmBtnClick()
    if self._curPartnerData == nil then
        local showText = LanguageDataHelper.GetContent(59072)
        GUIManager.ShowText(2, showText)
        return
    end
    if self._selectedId == nil then
        local showText = LanguageDataHelper.GetContent(59001)
        GUIManager.ShowText(2, showText)
        return
    end
    MarriageManager:Propose(self._curPartnerData:GetDbid(), self._selectedId)
end

function ShowLoveContainerView:PartnerListMaskClick()
    EventDispatcher:RemoveEventListener(GameEvents.RefreshFriendList, self._onRefreshFriendList)
    self._containerPartnerList:SetActive(false)
end
