-- MarriageTitleInfoContainerView.lua
local MarriageTitleInfoContainerView = Class.MarriageTitleInfoContainerView(ClassTypes.BaseComponent)
MarriageTitleInfoContainerView.interface = GameConfig.ComponentsConfig.Component
MarriageTitleInfoContainerView.classPath = "Modules.ModuleMarriage.ChildView.MarriageTitleInfoContainerView"
require "Modules.ModuleMarriage.ChildComponent.MarriageTitleInfoListItem"
local MarriageTitleInfoListItem = ClassTypes.MarriageTitleInfoListItem
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local UIList = ClassTypes.UIList
local MarriageRaiseData = PlayerManager.PlayerDataManager.marriageRaiseData

function MarriageTitleInfoContainerView:Awake()
    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    self:InitList()
end

function MarriageTitleInfoContainerView:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._list:SetItemType(MarriageTitleInfoListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1);
end

function MarriageTitleInfoContainerView:OnDestroy()

end

function MarriageTitleInfoContainerView:ShowView()
    local d = MarriageRaiseData:GetMadeTitles()
    self._list:SetDataList(d)
end

function MarriageTitleInfoContainerView:CloseView()
    local l = self._list:GetLength()-1
    for i=0,l do
        self._list:GetItem(i):RemoveListener()
    end
end

function MarriageTitleInfoContainerView:CloseBtnClick()
    self.gameObject:SetActive(false)
    EventDispatcher:TriggerEvent(GameEvents.MarriageTitleFx)
end

function MarriageTitleInfoContainerView:OnEnable()
    self:ShowView()
end

function MarriageTitleInfoContainerView:OnDisable() 
    self:CloseView()
end

