-- WeddingRingContainerView.lua
require "Modules.ModuleMarriage.ChildComponent.MarriageAttrItem"
require "Modules.ModuleMarriage.ChildComponent.MarryUpCostItem"
require "Modules.ModulePartner.ChildView.PartnerTipsShowView"
local WeddingRingContainerView = Class.WeddingRingContainerView(ClassTypes.BaseComponent)
WeddingRingContainerView.interface = GameConfig.ComponentsConfig.Component
WeddingRingContainerView.classPath = "Modules.ModuleMarriage.ChildView.WeddingRingContainerView"
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local MarriageRaiseManager = PlayerManager.MarriageRaiseManager
local MarriageManager = PlayerManager.MarriageManager
local MarriageRaiseData = PlayerManager.PlayerDataManager.marriageRaiseData
local MarriageAttrItem = ClassTypes.MarriageAttrItem
local MarryUpCostItem = ClassTypes.MarryUpCostItem
local XArtNumber = GameMain.XArtNumber
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local RectTransform = UnityEngine.RectTransform
local UIComponentUtil = GameUtil.UIComponentUtil
local MarryRingDataHelper = GameDataHelper.MarryRingDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local AttriDataHelper = GameDataHelper.AttriDataHelper
local UIParticle = ClassTypes.UIParticle
local PartnerTipsShowView = ClassTypes.PartnerTipsShowView
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

function WeddingRingContainerView:Awake()
    self._isQuick = false
    self.preLe=1
    self:InitComCB()
    self:InitView()
end

function WeddingRingContainerView:InitView()
    self._NotActiveGo = self:FindChildGO("Image_NotActive")
    self._StartUpGo = self:FindChildGO("Image_StarUp")
    self._LevelUpGo = self:FindChildGO("Image_LevelUp")
    self._EnhancingGO = self:FindChildGO("Container_Left/Image_Enhancing")
    self._UnEnhanceGO = self:FindChildGO("Container_Left/Image_UnEnhance")
    self._NameGO = self:FindChildGO("Container_Name")
    self._DegreeGO = self:FindChildGO("Container_Degree")
    self._GlodGO = self:FindChildGO("Container_StarLevel/Image_Glod")
    self._ContainerModel = self:FindChildGO("Container_Model")
    self._ContainerLevel = self:FindChildGO("Container_Level")
    self._IconGo = self:FindChildGO("Image_Icon")

    local textAttr = self:GetChildComponent("Container_Left/Text_AttrValue", "TextMeshWrapper")
    textAttr.text = LanguageDataHelper.GetContent(59092)

    self._coupleAttrs = {}
    self._coupleAttrGos = {}
    for i=1,4 do
        local itemGo = self:FindChildGO("Container_CoupleAttrs/Container_Item"..i)
        local item = UIComponentUtil.AddLuaUIComponent(itemGo, MarriageAttrItem) 
        itemGo:SetActive(false)
        self._coupleAttrGos[i]=itemGo
   		self._coupleAttrs[i] = item
    end

    self._Attrs = {}
    self._AttrGos = {}
    for i=1,4 do
        local itemGo = self:FindChildGO("Container_Attrs/Container_Item"..i)
        local item = UIComponentUtil.AddLuaUIComponent(itemGo, MarriageAttrItem) 
        itemGo:SetActive(false)
        self._AttrGos[i] = itemGo
   		self._Attrs[i] = item
    end

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)

    self._TextRing = self:GetChildComponent("Container_Level/Text_Ring", "TextMeshWrapper")
    self._TextRing.text = LanguageDataHelper.GetContent(1534)

    self._TextLevel = self:GetChildComponent("Container_Level/Text_Level", "TextMeshWrapper")

    local TextTitle = self:GetChildComponent("Container_Bless/Text_Title", "TextMeshWrapper")
    TextTitle.text = LanguageDataHelper.GetContent(59091)

    self.TextUp = self:GetChildComponent("Container_Bless/Button_Upgrade/Text", "TextMeshWrapper")
    self.TextUp.text = LanguageDataHelper.GetContent(59094)

    local TextDev = self:GetChildComponent("Container_Bless/Button_Devour/Text", "TextMeshWrapper")
    TextDev.text = LanguageDataHelper.GetContent(59093)

    local TextTips1 = self:GetChildComponent("Container_BG/Text_Tips1", "TextMeshWrapper")
    TextTips1.text = LanguageDataHelper.GetContent(59090)


    self._BlessItems = {}
    self._BlessItemGos = {}
    -- for i=1,1 do
        local itemGo = self:FindChildGO("Container_Bless/Container_Items/item1")
        local item = UIComponentUtil.AddLuaUIComponent(itemGo, MarryUpCostItem) 
        -- itemGo:SetActive(false)
        self._BlessItems[1] = item
        self._BlessItemGos[1] = itemGo
    -- end

    local btnUp = self:FindChildGO("Container_Bless/Button_Upgrade")
    self._csBH:AddClick(btnUp, function()self:OnBtnUp() end)

    local btnDevour = self:FindChildGO("Container_Bless/Button_Devour")
    self._csBH:AddClick(btnDevour, function()self:OnBtnDevour() end)

    self.glodRect = self:FindChildGO("Container_StarLevel/Image_Glod"):GetComponent(typeof(RectTransform))
    GameWorld.AddIcon(self._ContainerModel,GlobalParamsHelper.GetParamValue(809),0,false,1,false,true,1) 
    self._NotActiveGo:SetActive(false)
    self._StartUpGo:SetActive(false)
    self._LevelUpGo:SetActive(true)
    self._LevelUpGo.transform.localScale = Vector3.zero
    self._tweenScaleLevelUp = self:AddChildComponent('Image_LevelUp', XGameObjectTweenScale)

    self.OneUpred=self:FindChildGO("Container_Bless/Button_Upgrade/Image_Tip")
    self.Upred=self:FindChildGO("Container_Bless/Button_Devour/Image_Tip")
    self.OneUpred:SetActive(false)
    self.Upred:SetActive(false)

    local btnget = self:FindChildGO("Image_NotActive/Button_Get")
    local btngettxt = self:GetChildComponent("Image_NotActive/Button_Get/Text",'TextMeshWrapper')
    btngettxt.text = LanguageDataHelper.GetContent(544)
    self._csBH:AddClick(btnget,function()self:btnget()end)

    self._indexjie = {}
    self._indexjie[1]=4016
    self._indexjie[2]=4017
    self._indexjie[3]=4018
    self._indexjie[4]=4019
    self._indexjie[5]=4020
    self._indexjie[6]=4021
    self._indexjie[7]=4022
    self._indexjie[8]=4023
    self._indexjie[9]=4024
    self._indexjie[10]=4025

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30034")
    self._fx:SetLifeTime(2)
    self._fx:Stop()
    self:InitProgressBar()

    self.tipsShowView = self:AddChildLuaUIComponent("Container_TipsShow", PartnerTipsShowView)
end

function WeddingRingContainerView:InitProgressBar()
    self._progressBarLength = self:FindChildGO("Container_Bless/ProgressBar_Bless/Image_Background"):GetComponent(typeof(RectTransform)).sizeDelta.x
    self._barComp = UIScaleProgressBar.AddProgressBar(self.gameObject,"Container_Bless/ProgressBar_Bless")
    self._barComp:SetProgress(0)
    self._barComp:ShowTweenAnimate(true)
    self._barComp:SetMutipleTween(false)
    self._barComp:SetIsResetToZero(false)
    self._barComp:SetTweenTime(0.2)
    self._TextAttach = self:GetChildComponent("Container_Bless/ProgressBar_Bless/Text_Attach", "TextMeshWrapper")
    self._TextAttach.text = ''

end

function WeddingRingContainerView:ShowLevelUp()
    self._tweenScaleLevelUp:OnceOutQuad(1, 1, function()  
        self._LevelUpGo.transform.localScale = Vector3.zero
    end)
    self._fx:Play()
end
function WeddingRingContainerView:InitComCB()
    self._upresp = function(errorid)self:UpResp(errorid)end
end

function WeddingRingContainerView:OnBtnUp()
    if self._isQuick==true then
        self._isQuick=false
        self.TextUp.text = LanguageDataHelper.GetContent(59094)
        return 
    end
    self._isQuick=true
    self.TextUp.text = LanguageDataHelper.GetContent(58621)
    self:UpRpc()
end

function WeddingRingContainerView:UpRpc()
    local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._costid) or 0
    -- if pos~=0 then
        MarriageRaiseManager:MarryRingReq(pos)
    -- else
    --     self.TextUp.text = LanguageDataHelper.GetContent(59094)
    -- end
end
function WeddingRingContainerView:OnBtnDevour()
    self._isQuick=false
    local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._costid) or 0
    -- if pos~=0 then
        MarriageRaiseManager:MarryRingReq(pos)  
    -- end
end

function WeddingRingContainerView:OnDestroy()

end

function WeddingRingContainerView:ShowView()
    self:AddListener()
    local id,num = next(GlobalParamsHelper.GetParamValue(780))
    self._costid = id
    self._costnum = num
    if MarriageRaiseData:IsPutOnRing() then
        local attItem = MarriageRaiseData:GetMarrRingAtt()
        self.preLe = attItem:GetLevel()
    end
    self:RefreshRingAtt()
end

function WeddingRingContainerView:btnget()
     GUIManager.ClosePanel(PanelsConfig.Marriage)
    local data ={["id"]=1}
    GUIManager.ShowPanel(PanelsConfig.Shop,data,nil)
end

function WeddingRingContainerView:RefreshRingAtt()
    local attriList = {}
    if MarriageRaiseData:IsPutOnRing() then
        self._NotActiveGo:SetActive(false)
        local attItem = MarriageRaiseData:GetMarrRingAtt()
        local baseatts = attItem:GetAttriBase()
        local nextbaseatts = MarryRingDataHelper:GetAttriBase(attItem._id+1)
        local x=1
        for attid,v in pairs(baseatts) do
            attid = tonumber(attid)
            if nextbaseatts then
                if AttriDataHelper:GetShowType(attid)==1 then
                    self._Attrs[x]:OnRefreshData({attid,v,(nextbaseatts[attid]-v)})
                else
                    local n = (nextbaseatts[attid]-v)/100
                    local a = string.format("%.2f",n).."%"
                    self._Attrs[x]:OnRefreshData({attid,v,a})
                end
            else
                self._Attrs[x]:OnRefreshData({tonumber(attid),v,0})
            end
            self._AttrGos[x]:SetActive(true)
            x = x+1
        end
        local nextcoupleatts = MarryRingDataHelper:GetAttriMarry(attItem._id+1)
        x=1
        local marryatts = attItem:GetAttriMarry()
        for attid,v in pairs(marryatts) do
            attid = tonumber(attid)
            if nextcoupleatts then
                if AttriDataHelper:GetShowType(attid)==1 then
                    self._coupleAttrs[x]:OnRefreshData({attid,v,(nextcoupleatts[attid]-v)})
                else
                    local n = (nextcoupleatts[attid]-v)/100
                    local a = string.format("%.2f",n).."%"
                    self._coupleAttrs[x]:OnRefreshData({attid,v,a})
                end
            else
                self._coupleAttrs[x]:OnRefreshData({attid,v,0})
            end
            self._coupleAttrGos[x]:SetActive(true)
            x = x+1
        end

        local le = attItem:GetLevel()
        if self.preLe<le then
            self:ShowLevelUp()
            self.preLe=le
        end
    
        self._ContainerLevel:SetActive(true)
        local len = math.max(1,math.ceil(le/10))
        local l=le-(len-1)*10
        self.glodRect.sizeDelta = Vector2(40*l,40)
        self._GlodGO:SetActive(false)
        self._GlodGO:SetActive(true)
        if len>0 then
            GameWorld.AddIcon(self._IconGo,self._indexjie[len])
        end
        self._TextLevel.text = le..LanguageDataHelper.GetContent(69)
        local nextexp = MarryRingDataHelper:GetExp(attItem._id+1)
        if nextexp==nil then
            self._TextAttach.text = ''
            self._barComp:SetProgress(0)
        else
            local curexp = GameWorld.Player().marry_ring_exp
            self._TextAttach.text = curexp..'/'..nextexp
            local p = curexp/nextexp
            self._barComp:SetProgress(p)
        end
        if MarriageManager:HaveMate() then
            self._EnhancingGO:SetActive(true)
            self._UnEnhanceGO:SetActive(false)
        else
            self._EnhancingGO:SetActive(false)
            self._UnEnhanceGO:SetActive(true)
        end
        for k,v in pairs(baseatts) do
            table.insert(attriList, {["attri"]=k, ["value"]=v})
        end
    else
        self._EnhancingGO:SetActive(false)
        self._UnEnhanceGO:SetActive(true)
        local i = MarriageRaiseData:GetIdByLevel(1)
        local marryatts = MarryRingDataHelper:GetAttriMarry(i)
        local x=1
        for attid,v in pairs(marryatts) do
            attid = tonumber(attid)
            if AttriDataHelper:GetShowType(attid)==1 then
                self._coupleAttrs[x]:OnRefreshData({attid,0,v})
            else
                local a = string.format("%.2f",v/100).."%"
                self._coupleAttrs[x]:OnRefreshData({attid,0,a})
            end
            self._coupleAttrGos[x]:SetActive(true)
            x = x+1
        end
        local baseatts = MarryRingDataHelper:GetAttriBase(i)
        local x=1
        for attid,v in pairs(baseatts) do
            attid = tonumber(attid)
            if AttriDataHelper:GetShowType(attid)==1 then
                self._Attrs[x]:OnRefreshData({attid,0,v})
            else
                local a = string.format("%.2f",v/100).."%"
                self._Attrs[x]:OnRefreshData({attid,0,a})
            end
            self._AttrGos[x]:SetActive(true)
            x = x+1
        end
        local nextexp = MarryRingDataHelper:GetExp(i+1)
        self._TextAttach.text = '0/'..nextexp
        self.glodRect.sizeDelta = Vector2(0,40)
        self._barComp:SetProgress(0)
        self._NotActiveGo:SetActive(true)
        self._StartUpGo:SetActive(false)
        self._LevelUpGo:SetActive(false)
        self._ContainerLevel:SetActive(false)
        for k,v in pairs(baseatts) do
            table.insert(attriList, {["attri"]=k, ["value"]=v})
        end
    end 
    self._BlessItems[1]:OnRefreshData({self._costid,1,true})

    
    local fightPower = BaseUtil.CalculateFightPower(attriList, GameWorld.Player().vocation)
    self._fpNumber:SetNumber(fightPower)

end

function WeddingRingContainerView:UpResp(errorid)
    if errorid~=0 then
        if self._isQuick==true then
            self._isQuick=false
        end
        self.TextUp.text = LanguageDataHelper.GetContent(59094)
    else
        self.tipsShowView:ShowView(self._costnum)
        if self._isQuick==true then
            self:UpRpc()
        end
    end
    self:RefreshRingAtt()

end

function WeddingRingContainerView:AddListener()
    EventDispatcher:AddEventListener(GameEvents.MarryRingUpdate,self._upresp)
end

function WeddingRingContainerView:CloseView()
    self._isQuick=false
    self:RemoveListener()
end

function WeddingRingContainerView:RemoveListener()
    EventDispatcher:RemoveEventListener(GameEvents.MarryRingUpdate,self._upresp)
end

