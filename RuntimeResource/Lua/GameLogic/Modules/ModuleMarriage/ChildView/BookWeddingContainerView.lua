-- BookWeddingContainerView.lua
local BookWeddingContainerView = Class.BookWeddingContainerView(ClassTypes.BaseComponent)
BookWeddingContainerView.interface = GameConfig.ComponentsConfig.Component
BookWeddingContainerView.classPath = "Modules.ModuleMarriage.ChildView.BookWeddingContainerView"
require "Modules.ModuleMarriage.ChildComponent.WeddingDateListItem"
local WeddingDateListItem = ClassTypes.WeddingDateListItem
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local TaskRewardItem = ClassTypes.TaskRewardItem
local TaskRewardData = ClassTypes.TaskRewardData

-- local SceneConfig = GameConfig.SceneConfig
-- local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local MarriageManager = PlayerManager.MarriageManager
local DateTimeUtil = GameUtil.DateTimeUtil
local SystemInfoManager = GameManager.SystemInfoManager
local error_code = GameWorld.error_code

function BookWeddingContainerView:Awake()
    self._btnHelp = self:FindChildGO("Container_Left/Button_Help")
    self._csBH:AddClick(self._btnHelp, function()self:HelpBtnClick() end)
    self._textSelf = self:GetChildComponent("Container_Left/Text_Self", "TextMeshWrapper")
    self._textPartner = self:GetChildComponent("Container_Left/Text_Partner", "TextMeshWrapper")
    self._textTitle = self:GetChildComponent("Container_Left/Text_Title", "TextMeshWrapper")
    self:InitRewardsList()
    
    self:InitWeddingDateList()
    self._btnBookNow = self:FindChildGO("Container_Right/Button_BookNow")
    self._csBH:AddClick(self._btnBookNow, function()self:BookNowBtnClick() end)
    
    self._btnContentMask = self:FindChildGO("Container_Content/Image_Mask")
    self._csBH:AddClick(self._btnContentMask, function()self:ContentMaskBtnClick() end)
    self._containerContent = self:FindChildGO("Container_Content")
    self._containerContent:SetActive(false)
    self._onMarriageBookInfo = function()self:OnMarriageBookInfo() end
    self._onMarriageQueryInvite = function()self:OnMarriageQueryInvite() end
end

function BookWeddingContainerView:OnDestroy()
end

function BookWeddingContainerView:ShowView()
    self._textSelf.text = GameWorld.Player().name
    self._textPartner.text = MarriageManager:GetMateName()
    
    local reviews= GlobalParamsHelper.GetParamValue(802)-- 预约婚礼界面奖励显示
    local result = {}
    if reviews then
        for i,v in ipairs(reviews) do
            table.insert(result, TaskRewardData(v, 1))
        end
    end
    self._listRewards:SetDataList(result)
    
    self:OnMarriageBookInfo()
    MarriageManager:BookInfo()
    EventDispatcher:AddEventListener(GameEvents.MarriageBookInfo, self._onMarriageBookInfo)
    EventDispatcher:AddEventListener(GameEvents.MarriageQueryInvite, self._onMarriageQueryInvite)
end

function BookWeddingContainerView:CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.MarriageBookInfo, self._onMarriageBookInfo)
    EventDispatcher:RemoveEventListener(GameEvents.MarriageQueryInvite, self._onMarriageQueryInvite)
end

function BookWeddingContainerView:OnMarriageBookInfo()
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = MarriageManager:GetWeddingCount()
    self._textTitle.text = LanguageDataHelper.CreateContent(59030, argsTable)
    self:ShowWeddingDateList()
end

function BookWeddingContainerView:OnMarriageQueryInvite()
    self:ShowWeddingDateList()
end

function BookWeddingContainerView:ShowWeddingDateList()
    local now_date = DateTimeUtil.Now()
    local cur_hour = now_date.hour
    local min = now_date.min
    local allBookData = MarriageManager:GetAllBookingData()
    -- LoggerHelper.Log("Ash allBookData: " .. PrintTable:TableToStr(allBookData))
    local result = {}
    local lastHour = 23
    local firstCanBook = lastHour
    for i = 1, lastHour do
        local bookingState = "free"
        if allBookData[i] then
            bookingState = "hasBook"
        else
            if cur_hour >= i then
                bookingState = "pass"
            elseif cur_hour + 1 == i then
                if min >= 55 then
                    bookingState = "pass"
                end
            end
        end
        if bookingState == "free" and firstCanBook == lastHour then
            firstCanBook = i
        end
        local data = {time = MarriageManager:GetWeddingTime(i), bookingState = bookingState}
        table.insert(result, data)
    end
    
    local myBookingHour = MarriageManager:GetMyBookingHour()
    if myBookingHour ~= 0 then
        self._selectedHour = myBookingHour
    else
        self._selectedHour = firstCanBook
    end
    self._listWeddingDate:SetDataList(result)
    self._listWeddingDate:SetPositionByNum(self._selectedHour)
    self._listWeddingDate:SetSelectedIndex(self._selectedHour - 1)
    -- LoggerHelper.Log("Ash ShowWeddingDateList: " .. self._selectedHour)
end


function BookWeddingContainerView:InitRewardsList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Left/Container_Rewards/ScrollViewList")
    self._listRewards = list
    self._scrollViewRewards = scrollView
    self._scrollViewRewards:SetHorizontalMove(false)
    self._scrollViewRewards:SetVerticalMove(false)
    self._listRewards:SetItemType(TaskRewardItem)
    self._listRewards:SetPadding(0, 0, 0, 0)
    self._listRewards:SetGap(0, 10)
    self._listRewards:SetDirection(UIList.DirectionLeftToRight, 100, 1)
end

function BookWeddingContainerView:InitWeddingDateList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Right/ScrollViewList_TimeList")
    self._listWeddingDate = list
    self._scrollViewWeddingDate = scrollView
    self._listWeddingDate:SetItemType(WeddingDateListItem)
    self._listWeddingDate:SetPadding(0, 0, 0, 0)
    self._listWeddingDate:SetGap(0, 0)
    self._listWeddingDate:SetDirection(UIList.DirectionTopToDown, 1, 1);
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._listWeddingDate:SetItemClickedCB(itemClickedCB)
end

function BookWeddingContainerView:OnListItemClicked(idx)
    self._selectedHour = idx + 1
-- LoggerHelper.Log("Ash _selectedHour: " .. self._selectedHour)
end

function BookWeddingContainerView:BookNowBtnClick()
    if MarriageManager:GetWeddingCount() == 0 then
        SystemInfoManager:ShowClientTip(error_code.ERR_MARRY_NO_WEDDING_COUNT)
        return
    end
    local data = self._listWeddingDate:GetDataByIndex(self._selectedHour - 1)
    if data.bookingState == "pass" then
        SystemInfoManager:ShowClientTip(error_code.ERR_MARRY_WRONG_BOOK_TIME)
    elseif data.bookingState == "hasBook" then
        SystemInfoManager:ShowClientTip(error_code.ERR_MARRY_HAS_BOOK)
    else
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = MarriageManager:GetWeddingTime(self._selectedHour)
        local content = LanguageDataHelper.CreateContent(59076, argsTable)
        GUIManager.ShowMessageBox(2, content,
            function()MarriageManager:MarryBook(self._selectedHour) end)
    end
end

function BookWeddingContainerView:ContentMaskBtnClick()
    self._containerContent:SetActive(false)
end

function BookWeddingContainerView:HelpBtnClick()
    self._containerContent:SetActive(true)
end
