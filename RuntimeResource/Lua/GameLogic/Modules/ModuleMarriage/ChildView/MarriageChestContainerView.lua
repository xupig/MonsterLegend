-- MarriageChestContainerView.lua
require "Modules.ModuleMarriage.ChildComponent.InstanceListItem"
local MarriageChestContainerView = Class.MarriageChestContainerView(ClassTypes.BaseComponent)
MarriageChestContainerView.interface = GameConfig.ComponentsConfig.Component
MarriageChestContainerView.classPath = "Modules.ModuleMarriage.ChildView.MarriageChestContainerView"
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local MarriageRaiseManager = PlayerManager.MarriageRaiseManager
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local InstanceListItem = ClassTypes.InstanceListItem
local MarriageManager = PlayerManager.MarriageManager
local MarriageRaiseData = PlayerManager.PlayerDataManager.marriageRaiseData
local ActorModelComponent = GameMain.ActorModelComponent
local MessageBoxType = GameConfig.EnumType.MessageBoxType

function MarriageChestContainerView:Awake()
    self:InitComCB()
    self:InitView()
end

function MarriageChestContainerView:InitView()
    self.TextTime = self:GetChildComponent("Container_Left/Text_TimeLeft", "TextMeshWrapper")
    self.TextPrice = self:GetChildComponent("Container_Left/Container_Chest/Text_Price", "TextMeshWrapper")
    self.TextleftDay = self:GetChildComponent("Container_Right/Text_TimeLeft", "TextMeshWrapper")
    self.TextleftDay.text = ''
    self.containerModel = self:FindChildGO("Container_Right/Container_Partner/Container_Model")

    self._gridLauoutGroup1 = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Left/Container_Chest/Container_Items")
    self._gridLauoutGroup1:SetItemType(InstanceListItem)

    self._gridLauoutGroup2 = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Left/Container_DailyGet/Container_Items")
    self._gridLauoutGroup2:SetItemType(InstanceListItem)

    self._btnchestwrap = self:GetChildComponent("Container_Left/Container_Chest/Button_Get","ButtonWrapper")
    self._btndailywrap = self:GetChildComponent("Container_Left/Container_DailyGet/Button_Get","ButtonWrapper")

    self._btnchesttxt = self:GetChildComponent("Container_Left/Container_Chest/Button_Get/Text","TextMeshWrapper")
    self._btndailytxt = self:GetChildComponent("Container_Left/Container_DailyGet/Button_Get/Text","TextMeshWrapper")

    self._btnbuytxt = self:GetChildComponent("Container_Right/Button_Buy/Text","TextMeshWrapper")
    self._btnbuywrap = self:GetChildComponent("Container_Right/Button_Buy","ButtonWrapper")

end

function MarriageChestContainerView:OnBtnHelp()
    -- body
end

function MarriageChestContainerView:OnBtnChestGet()
    MarriageRaiseManager:MarryAskChestBuyReq()
end

function MarriageChestContainerView:OnBtnDailyGet()
    MarriageRaiseManager:MarryBoxRewardReq()
end

function MarriageChestContainerView:OnBtnBuy()
    -- if MarriageManager:HaveMate() then
        local d = {}
        local data = {}
        d.id = MessageBoxType.Tip
        d.value =  data
        data.confirmCB=function()MarriageRaiseManager:MarryBuyBoxReq() end
        data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.MessageBox) end
        local arg = LanguageDataHelper.GetArgsTable()
        local v = GlobalParamsHelper.GetParamValue(790)
        arg['0']=v
        data.text = LanguageDataHelper.CreateContent(59119,arg)
        GUIManager.ShowPanel(PanelsConfig.MessageBox, d)
    -- else
    --     local showText = "结婚后才能购买"
    --     GUIManager.ShowText(2, showText)
    -- end
end

function MarriageChestContainerView:InitComCB()
    local btnhelp = self:FindChildGO("Container_Left/Button_Help")
    self._csBH:AddClick(btnhelp,function()self:OnBtnHelp()end)

    local btnChestGet = self:FindChildGO("Container_Left/Container_Chest/Button_Get")
    self._csBH:AddClick(btnChestGet,function()self:OnBtnChestGet()end)

    local btnDailyGet = self:FindChildGO("Container_Left/Container_DailyGet/Button_Get")
    self._csBH:AddClick(btnDailyGet,function()self:OnBtnDailyGet()end)

    local btnBuy = self:FindChildGO("Container_Right/Button_Buy")
    self._csBH:AddClick(btnBuy,function()self:OnBtnBuy()end)

    self._updataView = function() self:RefreshView()end 

end
function MarriageChestContainerView:OnDestroy()
end

function MarriageChestContainerView:ShowView()
    self:AddListener()
    local id,num = next(GlobalParamsHelper.GetParamValue(791))
    self.TextPrice.text = num..LanguageDataHelper.GetContent(971)
    local rewards = GlobalParamsHelper.GetParamValue(793)
    local result={}
    for k,v in pairs(rewards) do
        table.insert(result,{tonumber(k),v})
    end
    self._gridLauoutGroup2:SetDataList(result)
    local d = {}
    table.insert(d,{id,num})
    self._gridLauoutGroup1:SetDataList(d)
    self:RefreshView()
end

function MarriageChestContainerView:CloseView()
    self:RemoveListener()
end

function MarriageChestContainerView:RemoveListener()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MARRY_BOX_INFO, self._updataView)
end

function MarriageChestContainerView:AddListener()
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MARRY_BOX_INFO, self._updataView)
end

function MarriageChestContainerView:RefreshView()
    if MarriageRaiseData:IsHaveBox() then
        self._btnbuywrap.interactable=false
        self._btnbuytxt.text = LanguageDataHelper.GetContent(564)
        if MarriageRaiseData:IsImmediateGet() then
            self._btnchestwrap.interactable = false
            self._btnchesttxt.text = LanguageDataHelper.CreateContent(563)
        else
            self._btnchestwrap.interactable = true
            self._btnchesttxt.text = LanguageDataHelper.CreateContent(123)

        end
        if MarriageRaiseData:IsGetToday() then
            self._btndailywrap.interactable = false
            self._btndailytxt.text = LanguageDataHelper.CreateContent(563)         
        else
            self._btndailywrap.interactable = true
            self._btndailytxt.text = LanguageDataHelper.CreateContent(123)

        end
        local day = GlobalParamsHelper.GetParamValue(792)
        local arg = LanguageDataHelper.GetArgsTable()
        local v = day-MarriageRaiseData:GetBoxTimes()
        arg['0']=v
        self.TextTime.text = LanguageDataHelper.CreateContent(59125,arg)
    else
        self._btnbuytxt.text = LanguageDataHelper.GetContent(76944)
        self._btnbuywrap.interactable=true
        local arg = LanguageDataHelper.GetArgsTable()
        self._btnchestwrap.interactable = false
        self._btndailywrap.interactable = false
        arg['0']=0
        self.TextTime.text = LanguageDataHelper.CreateContent(59125,arg)
    end     
end


