-- ShowLoveItemContainerView.lua
local ShowLoveItemContainerView = Class.ShowLoveItemContainerView(ClassTypes.BaseComponent)
ShowLoveItemContainerView.interface = GameConfig.ComponentsConfig.Component
ShowLoveItemContainerView.classPath = "Modules.ModuleMarriage.ChildView.ShowLoveItemContainerView"
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local TaskRewardItem = ClassTypes.TaskRewardItem
local TaskRewardData = ClassTypes.TaskRewardData

local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local TitleDataHelper = GameDataHelper.TitleDataHelper

function ShowLoveItemContainerView:Awake()
    self._textPriceValue = self:GetChildComponent("Text_PriceValue", "TextMeshWrapper")
    self._containerSelected = self:FindChildGO("Container_Selected")
    self._containerSelected:SetActive(false)
    self._containerCurrency = self:FindChildGO("Container_Currency")
    self._imgTitleGo = self:FindChildGO("Image_Title")
    self._rewardItems = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Items")
    self._rewardItems:SetItemType(TaskRewardItem)
    self._textTimes = self:GetChildComponent("Text_Times", "TextMeshWrapper")
    
    self._btnRaycast = self:FindChildGO("Button_Raycast")
    self._csBH:AddClick(self._btnRaycast, function()self:RaycastBtnClick() end)
end

function ShowLoveItemContainerView:OnDestroy()
end

function ShowLoveItemContainerView:ShowRewards(rewards)
    local group = GameWorld.Player():GetVocationGroup()
    local result = {}
    if rewards[group] then
        for i = 1, #rewards[group], 2 do
            table.insert(result, TaskRewardData(rewards[group][i], rewards[group][i + 1]))
        end
        local rid = rewards[group][1]
        local titleid = ItemDataHelper.GetItemEffect(rid)[2]
        local icon = TitleDataHelper.GetTitleShowName(titleid)
        GameWorld.AddIconAnim(self._imgTitleGo,icon)
        GameWorld.SetIconAnimSpeed(self._imgTitleGo, 1)
    end
    self._rewardItems:SetDataList(result)
end

function ShowLoveItemContainerView:SetPrice(moneyId, price)
    GameWorld.AddIcon(self._containerCurrency, ItemDataHelper.GetIcon(moneyId))
    self._textPriceValue.text = tostring(price)
end

function ShowLoveItemContainerView:SetWeddingTimes(times)
    self._textTimes.text = times .. LanguageDataHelper.CreateContent(916)
end

function ShowLoveItemContainerView:SetSelected(flag)
    self._containerSelected:SetActive(flag)
end

function ShowLoveItemContainerView:SetSelectedCallback(id, callback)
    self._id = id
    self._selectedCallback = callback
end

function ShowLoveItemContainerView:RaycastBtnClick()
    if self._selectedCallback then
        self._selectedCallback(self._id)
    end
    self:SetSelected(true)
end
