-- AcceptLoveContainerView.lua
local AcceptLoveContainerView = Class.AcceptLoveContainerView(ClassTypes.BaseComponent)
AcceptLoveContainerView.interface = GameConfig.ComponentsConfig.Component
AcceptLoveContainerView.classPath = "Modules.ModuleMarriage.ChildView.AcceptLoveContainerView"
require "Modules.ModuleMarriage.ChildView.WeddingPlayerContainerView"
local WeddingPlayerContainerView = ClassTypes.WeddingPlayerContainerView

-- local SceneConfig = GameConfig.SceneConfig
-- local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
-- local TaskManager = PlayerManager.TaskManager
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local MarriageManager = PlayerManager.MarriageManager
local StringStyleUtil = GameUtil.StringStyleUtil

function AcceptLoveContainerView:Awake()
    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    
    self._partnerContainerView = self:AddChildLuaUIComponent("Container_Partner", WeddingPlayerContainerView)
    self._selfContainerView = self:AddChildLuaUIComponent("Container_Self", WeddingPlayerContainerView)
    
    self._containerBooking = self:FindChildGO("Container_Booking")
    self._btnConfirm = self:FindChildGO("Container_Booking/Button_Confirm")
    self._csBH:AddClick(self._btnConfirm, function()self:ConfirmBtnClick() end)
    self._textLater = self:GetChildComponent("Container_Booking/Text_Later", "TextMeshWrapper")
    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject, "Container_Booking/Text_Later")
    self._linkTextMesh:SetOnClickCB(function(linkIndex, postion)self:OnClickLink(linkIndex, postion) end)
    self._textLater.text =  StringStyleUtil.GetColorStringWithTextMesh('<u><link="CLICK_LINK">' .. LanguageDataHelper.CreateContent(59020) .. '</link></u>',StringStyleUtil.green)
    self._onMarriageQueryInfo = function()self:OnMarriageQueryInfo() end
end

function AcceptLoveContainerView:OnDestroy()
end

function AcceptLoveContainerView:ShowView(args)
    EventDispatcher:AddEventListener(GameEvents.MarriageQueryInfo, self._onMarriageQueryInfo)
    self._selfContainerView:ShowSelf(true)
    self:OnMarriageQueryInfo(args)
end

function AcceptLoveContainerView:CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.MarriageQueryInfo, self._onMarriageQueryInfo)
end

function AcceptLoveContainerView:OnMarriageQueryInfo(args)
    if args then
        self._partnerContainerView:SetPlayer(args[3], args[2])
    else
        self._partnerContainerView:SetPlayer(MarriageManager:GetMateVocation(), MarriageManager:GetMateName())
    end
end

function AcceptLoveContainerView:ShowBooking(flag)
    self._containerBooking:SetActive(flag)
end

function AcceptLoveContainerView:OnClickLink(linkId, postion)
    self:CloseView()
    self.gameObject:SetActive(false)
end

function AcceptLoveContainerView:CloseBtnClick()
    self:CloseView()
    self.gameObject:SetActive(false)
end

function AcceptLoveContainerView:SetBookingCB(callback)
    self._callback = callback
end

function AcceptLoveContainerView:ConfirmBtnClick()
    if self._callback then
        self._callback()
    end
end
