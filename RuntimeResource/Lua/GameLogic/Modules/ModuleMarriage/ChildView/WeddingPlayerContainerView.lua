-- WeddingPlayerContainerView.lua
local WeddingPlayerContainerView = Class.WeddingPlayerContainerView(ClassTypes.BaseComponent)
WeddingPlayerContainerView.interface = GameConfig.ComponentsConfig.Component
WeddingPlayerContainerView.classPath = "Modules.ModuleMarriage.ChildView.WeddingPlayerContainerView"
local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local RoleDataHelper = GameDataHelper.RoleDataHelper

function WeddingPlayerContainerView:Awake()
    self._containerHead = self:FindChildGO("Container_Head")
end

function WeddingPlayerContainerView:OnDestroy()
end

function WeddingPlayerContainerView:ShowSelf(showName)
    local player = GameWorld.Player()
    if showName == true then
        if self._textName == nil then
            self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
        end
        self._textName.text = player.name
    end
    local icon = RoleDataHelper.GetHeadIconByVocation(player.vocation)
    GameWorld.AddIcon(self._containerHead, icon, nil, true)
end

function WeddingPlayerContainerView:SetPlayer(vocation, name)
    if name ~= nil then
        if self._textName == nil then
            self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
        end
        self._textName.text = name
    end
    local icon = RoleDataHelper.GetHeadIconByVocation(vocation)
    self._containerHead:SetActive(true)
    GameWorld.AddIcon(self._containerHead, icon, nil, true)
end

function WeddingPlayerContainerView:ClearPlayer(nameContent)
    self._containerHead:SetActive(false)
    if self._textName ~= nil then
        self._textName.text = nameContent or ""
    end
end
