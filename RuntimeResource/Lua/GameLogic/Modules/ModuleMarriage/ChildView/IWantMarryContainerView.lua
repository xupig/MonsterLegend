-- IWantMarryContainerView.lua
local IWantMarryContainerView = Class.IWantMarryContainerView(ClassTypes.BaseComponent)
IWantMarryContainerView.interface = GameConfig.ComponentsConfig.Component
IWantMarryContainerView.classPath = "Modules.ModuleMarriage.ChildView.IWantMarryContainerView"
local SceneConfig = GameConfig.SceneConfig
local GameSceneManager = GameManager.GameSceneManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager

function IWantMarryContainerView:Awake()
    self._btnShowLove = self:FindChildGO("Button_ShowLove")
    self._btnAcceptLove = self:FindChildGO("Button_AcceptLove")
    self._btnBookWedding = self:FindChildGO("Button_BookWedding")
    self._btnInviteGuest = self:FindChildGO("Button_InviteGuest")
    self._btnHoldingWedding = self:FindChildGO("Button_HoldingWedding")
    self._btnBecomeCouple = self:FindChildGO("Button_BecomeCouple")
    self._csBH:AddClick(self._btnShowLove, function()self:ShowLoveBtnClick() end)
    self._csBH:AddClick(self._btnAcceptLove, function()self:AcceptLoveBtnClick() end)
    self._csBH:AddClick(self._btnBookWedding, function()self:BookWeddingBtnClick() end)
    self._csBH:AddClick(self._btnInviteGuest, function()self:InviteGuestBtnClick() end)
    self._csBH:AddClick(self._btnHoldingWedding, function()self:HoldingWeddingBtnClick() end)
    self._csBH:AddClick(self._btnBecomeCouple, function()self:BecomeCoupleBtnClick() end)
end

function IWantMarryContainerView:OnDestroy()
end

function IWantMarryContainerView:ShowView()
    self._iconBg = self:FindChildGO("Image_BG")
    GameWorld.AddIcon(self._iconBg,13511)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleMarriage.ChildView.IWantMarryContainerView")
end

function IWantMarryContainerView:CloseView()
    self._iconBg = nil
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_show_love_close_button")
end

function IWantMarryContainerView:SetShowLoveBtnClickCB(callback)
    self._onShowLoveBtnClick = callback
end

function IWantMarryContainerView:SetAcceptLoveBtnClickCB(callback)
    self._onAcceptLoveBtnClick = callback
end

function IWantMarryContainerView:SetBookWeddingBtnClickCB(callback)
    self._onBookWeddingBtnClick = callback
end

function IWantMarryContainerView:SetInviteGuestBtnClickCB(callback)
    self._onInviteGuestBtnClick = callback
end

function IWantMarryContainerView:SetHoldingWeddingBtnClickCB(callback)
    self._onHoldingWeddingBtnClick = callback
end

function IWantMarryContainerView:SetBecomeCoupleBtnClickCB(callback)
    self._onBecomeCoupleBtnClick = callback
end

function IWantMarryContainerView:ShowLoveBtnClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_show_love_button")
    if self._onShowLoveBtnClick then
        self._onShowLoveBtnClick()
    end
    
end

function IWantMarryContainerView:AcceptLoveBtnClick()
    if self._onAcceptLoveBtnClick then
        self._onAcceptLoveBtnClick()
    end
end

function IWantMarryContainerView:BookWeddingBtnClick()
    if self._onBookWeddingBtnClick then
        self._onBookWeddingBtnClick()
    end
end

function IWantMarryContainerView:InviteGuestBtnClick()
    if self._onInviteGuestBtnClick then
        self._onInviteGuestBtnClick()
    end
end

function IWantMarryContainerView:HoldingWeddingBtnClick()
    if self._onHoldingWeddingBtnClick then
        self._onHoldingWeddingBtnClick()
    end
end

function IWantMarryContainerView:BecomeCoupleBtnClick()
    if self._onBecomeCoupleBtnClick then
        self._onBecomeCoupleBtnClick()
    end
end
