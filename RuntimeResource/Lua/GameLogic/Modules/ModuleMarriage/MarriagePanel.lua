-- MarriagePanel.lua
local MarriagePanel = Class.MarriagePanel(ClassTypes.BasePanel)

require "Modules.ModuleMarriage.ChildView.MarriageInfoContainerView"
require "Modules.ModuleMarriage.ChildView.WeddingRingContainerView"
require "Modules.ModuleMarriage.ChildView.BabyContainerView"
require "Modules.ModuleMarriage.ChildView.MarriageInstanceContainerView"
require "Modules.ModuleMarriage.ChildView.MarriageChestContainerView"

local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager
local WorldBossManager = PlayerManager.WorldBossManager
local MarriagePanelType = GameConfig.EnumType.MarriagePanelType
local MarriageInfoContainerView = ClassTypes.MarriageInfoContainerView
local WeddingRingContainerView = ClassTypes.WeddingRingContainerView
local BabyContainerView = ClassTypes.BabyContainerView
local MarriageInstanceContainerView = ClassTypes.MarriageInstanceContainerView
local MarriageChestContainerView = ClassTypes.MarriageChestContainerView

function MarriagePanel:Awake()
    self._selectTab = -1
    self._indexToGO = {}
    self._indexToView = {}
    self:InitComps()
end

function MarriagePanel:OnShow(data)
    GameWorld.ShowPlayer():ShowModel(1)
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleMarriage.MarriagePanel")
end

function MarriagePanel:OnClose()
    GameWorld.ShowPlayer():ShowModel(3)
end

function MarriagePanel:OnDestroy()

end

function MarriagePanel:InitComps()  
    self:InitView("Container_Info", MarriageInfoContainerView,MarriagePanelType.Info)
    self:InitView("Container_WeddingRing", WeddingRingContainerView,MarriagePanelType.WeddingRing)
    self:InitView("Container_Baby", BabyContainerView,MarriagePanelType.Baby)
    self:InitView("Container_Instance", MarriageInstanceContainerView,MarriagePanelType.Instance)
    self:InitView("Container_Chest", MarriageChestContainerView,MarriagePanelType.Chest)
end

function MarriagePanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function MarriagePanel:StructingViewInit()
    return true
end

--关闭的时候重新开启二级主菜单
function MarriagePanel:ReopenSubMainPanel()
    return true
end

function MarriagePanel:BaseShowModel()
    if self._selectedIndex==MarriagePanelType.Info then
        self._selectedView:ShowModel()
    end
end

function MarriagePanel:BaseHideModel()
    if self._selectedIndex==MarriagePanelType.Info then
        self._selectedView:HideModel()
    end 
end

-- function MarriagePanel:ScaleToHide()
--     return true
-- end