-- MarriageModule.lua
MarriageModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function MarriageModule.Init()
    GUIManager.AddPanel(PanelsConfig.Marriage, "Panel_Marriage", GUILayer.LayerUIPanel, "Modules.ModuleMarriage.MarriagePanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.GetMarry, "Panel_GetMarry", GUILayer.LayerUIPanel, "Modules.ModuleMarriage.GetMarryPanel", true, false, nil, true, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.MarryBabyActiveShow, "Panel_MarryBabyActiveShow", GUILayer.LayerUIFloat, "Modules.ModuleMarriage.MarryBabyActiveShowPanel", true, false, nil, true, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.MarriageGuestManager, "Panel_MarriageGuestManager", GUILayer.LayerUIPanel, "Modules.ModuleMarriage.MarriageGuestManagerPanel", true, false, nil, true, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return MarriageModule
