-- GetMarryPanel.lua
local GetMarryPanel = Class.GetMarryPanel(ClassTypes.BasePanel)

require "Modules.ModuleMarriage.ChildView.GoddessContainerView"
require "Modules.ModuleMarriage.ChildView.DivorceContainerView"
require "Modules.ModuleMarriage.ChildView.IWantMarryContainerView"
require "Modules.ModuleMarriage.ChildView.ShowLoveContainerView"
require "Modules.ModuleMarriage.ChildView.AcceptLoveContainerView"
require "Modules.ModuleMarriage.ChildView.BookWeddingContainerView"
require "Modules.ModuleMarriage.ChildView.ProposeContainerView"
require "Modules.ModuleMarriage.ChildView.InviteGuestContainerView"
require "Modules.ModuleMarriage.ChildView.SendBlessingsContainerView"
require "Modules.ModuleMarriage.ChildView.HoldingWeddingContainerView"

local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MarriageManager = PlayerManager.MarriageManager
-- local WorldBossManager = PlayerManager.WorldBossManager
-- local BossPanelType = GameConfig.EnumType.BossPanelType
local GoddessContainerView = ClassTypes.GoddessContainerView
local DivorceContainerView = ClassTypes.DivorceContainerView
local IWantMarryContainerView = ClassTypes.IWantMarryContainerView
local ShowLoveContainerView = ClassTypes.ShowLoveContainerView
local AcceptLoveContainerView = ClassTypes.AcceptLoveContainerView
local BookWeddingContainerView = ClassTypes.BookWeddingContainerView
local ProposeContainerView = ClassTypes.ProposeContainerView
local InviteGuestContainerView = ClassTypes.InviteGuestContainerView
local SendBlessingsContainerView = ClassTypes.SendBlessingsContainerView
local HoldingWeddingContainerView = ClassTypes.HoldingWeddingContainerView

function GetMarryPanel:Awake()
    self._viewMap = {}
    self._componentMap = {}
    self._gameObjectMap = {}
    self._callbackMap = {}
    self._contianerWin = self:FindChildGO("Container_Win")
    self._btnClose = self:FindChildGO("Container_Win/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
    
    self:InitView("Container_Goddess", GoddessContainerView, function()
            
            local onGetMarryBtnClick = function()self:GetMarryBtnClick() end
            local onWeddingMallBtnClick = function()self:WeddingMallBtnClick() end
            local onDivorceBtnClick = function()self:DivorceBtnClick() end
            local view = self:GetViewComponent("Container_Goddess")
            view:SetGetMarryBtnClickCB(onGetMarryBtnClick)
            view:SetWeddingMallBtnClickCB(onWeddingMallBtnClick)
            view:SetDivorceBtnClickCB(onDivorceBtnClick)
    end)
    self:InitView("Container_Divorce", DivorceContainerView)
    self:InitView("Container_IWantMarriage", IWantMarryContainerView, function()
        local view = self:GetViewComponent("Container_IWantMarriage")
        local onShowLoveBtnClick = function()self:ShowLoveBtnClick() end
            view:SetShowLoveBtnClickCB(onShowLoveBtnClick)
            local onBookWeddingBtnClick = function()self:BookWeddingBtnClick() end
            view:SetBookWeddingBtnClickCB(onBookWeddingBtnClick)
            local onInviteGuestBtnClick = function()self:InviteGuestBtnClick() end
            view:SetInviteGuestBtnClickCB(onInviteGuestBtnClick)
            local onAcceptLoveBtnClick = function()self:AcceptLoveBtnClick() end
            view:SetAcceptLoveBtnClickCB(onAcceptLoveBtnClick)
            local onHoldingWeddingBtnClick = function()self:HoldingWeddingBtnClick() end
            view:SetHoldingWeddingBtnClickCB(onHoldingWeddingBtnClick)
            local onBecomeCoupleBtnClick = function()self:BecomeCoupleBtnClick() end
            view:SetBecomeCoupleBtnClickCB(onBecomeCoupleBtnClick)
    end)
    self:InitView("Container_ShowLove", ShowLoveContainerView)
    self:InitView("Container_AcceptLove", AcceptLoveContainerView, function ()
        self:GetViewComponent("Container_AcceptLove"):SetBookingCB(function()self:GetViewGameObject("Container_AcceptLove"):SetActive(false); self:BookWeddingBtnClick() end)
    end)
    self:InitView("Container_BookWedding", BookWeddingContainerView)
    self:InitView("Container_Propose", ProposeContainerView)
    self:InitView("Container_InviteGuest", InviteGuestContainerView)
    self:InitView("Container_HoldingWedding", HoldingWeddingContainerView)
    self:InitView("Container_SendBlessings", SendBlessingsContainerView)
    
    self._onProposeReqSuccess = function()self:OnProposeReqSuccess() end
end

function GetMarryPanel:InitView(childPath, view, addComponentCallback)
    self._viewMap[childPath] = view
    if addComponentCallback then
        self._callbackMap[childPath] = addComponentCallback
    end
end

function GetMarryPanel:GetViewComponent(childPath)
    if not self._componentMap[childPath] then
        self._componentMap[childPath] = self:AddChildLuaUIComponent(childPath, self._viewMap[childPath])
        if self._callbackMap[childPath] then
            self._callbackMap[childPath]()
        end
    end
    return self._componentMap[childPath]
end

function GetMarryPanel:GetViewGameObject(childPath)
    if not self._gameObjectMap[childPath] then
        self._gameObjectMap[childPath] = self:FindChildGO(childPath)
    end
    return self._gameObjectMap[childPath]
end

function GetMarryPanel:OnShow(data)
    self._contianerWin:SetActive(true)
    self:GetViewGameObject("Container_Divorce"):SetActive(false)
    self:GetViewGameObject("Container_IWantMarriage"):SetActive(false)
    self:GetViewGameObject("Container_ShowLove"):SetActive(false)
    self:GetViewGameObject("Container_AcceptLove"):SetActive(false)
    self:GetViewGameObject("Container_BookWedding"):SetActive(false)
    self:GetViewGameObject("Container_InviteGuest"):SetActive(false)
    self:GetViewGameObject("Container_SendBlessings"):SetActive(false)
    self:GetViewGameObject("Container_HoldingWedding"):SetActive(false)
    if data ~= nil then
        if data[1] == "ReceivePropose" then
            self:GetMarryBtnClick()
            self:GetViewGameObject("Container_Propose"):SetActive(true)
            self:GetViewComponent("Container_Propose"):ShowView(data[2])
        elseif data[1] == "ReceiveRespondPropose" then
            self:GetMarryBtnClick()
            self:GetViewGameObject("Container_AcceptLove"):SetActive(true)
            self:GetViewComponent("Container_AcceptLove"):ShowView(data[2])
            self:GetViewComponent("Container_AcceptLove"):ShowBooking(true)
        elseif data[1] == "ShowSendBlessings" then
            self:GetViewGameObject("Container_Goddess"):SetActive(false)
            self:GetViewGameObject("Container_Propose"):SetActive(false)
            self:GetViewGameObject("Container_SendBlessings"):SetActive(true)
            self:GetViewComponent("Container_SendBlessings"):ShowView()
        elseif data[1] == "HoldingWedding" then
            self._contianerWin:SetActive(false)
            self:GetViewGameObject("Container_Goddess"):SetActive(false)
            self:GetViewGameObject("Container_Propose"):SetActive(false)
            self:GetViewGameObject("Container_HoldingWedding"):SetActive(true)
            self:GetViewComponent("Container_HoldingWedding"):ShowView(true)
        end
    else
        self:GetViewGameObject("Container_Goddess"):SetActive(true)
        self:GetViewGameObject("Container_Propose"):SetActive(false)
        self:GetViewComponent("Container_Goddess"):ShowView()
        EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleMarriage.GetMarryPanel")
    end
    EventDispatcher:AddEventListener(GameEvents.ProposeReqSuccess, self._onProposeReqSuccess)
    MarriageManager:BookInfo()
    MarriageManager:QueryInfo()
end

function GetMarryPanel:OnClose()
    self:GetViewComponent("Container_Goddess"):CloseView()
    self:GetViewComponent("Container_Divorce"):CloseView()
    self:GetViewComponent("Container_IWantMarriage"):CloseView()
    self:GetViewComponent("Container_ShowLove"):CloseView()
    self:GetViewComponent("Container_AcceptLove"):CloseView()
    self:GetViewComponent("Container_BookWedding"):CloseView()
    self:GetViewComponent("Container_Propose"):CloseView()
    self:GetViewComponent("Container_InviteGuest"):CloseView()
    self:GetViewComponent("Container_SendBlessings"):CloseView()
    self:GetViewComponent("Container_HoldingWedding"):CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.ProposeReqSuccess, self._onProposeReqSuccess)
end

function GetMarryPanel:OnDestroy()

end

function GetMarryPanel:OnProposeReqSuccess()
    self:CloseShowLove()
end

function GetMarryPanel:GetMarryBtnClick()
    self:GetViewGameObject("Container_Goddess"):SetActive(false)
    self:GetViewGameObject("Container_IWantMarriage"):SetActive(true)
    self:GetViewComponent("Container_IWantMarriage"):ShowView()
    if self._curView then
        self._curView:CloseView()
        self._curView.gameObject:SetActive(false)
    end
end

function GetMarryPanel:WeddingMallBtnClick()
    local data = {["id"] = EnumType.ShopPageType.Marriage}
    GUIManager.ShowPanel(PanelsConfig.Shop, data)
    GUIManager.ClosePanel(PanelsConfig.GetMarry)
end

function GetMarryPanel:DivorceBtnClick()
    if MarriageManager:HaveMate() then
        self:GetViewGameObject("Container_Goddess"):SetActive(false)
        self:GetViewGameObject("Container_Divorce"):SetActive(true)
        self:GetViewComponent("Container_Divorce"):ShowView()
    else
        local showText = LanguageDataHelper.GetContent(59073)
        GUIManager.ShowText(2, showText)
    end
end

function GetMarryPanel:ShowLoveBtnClick()
    self:GetViewGameObject("Container_IWantMarriage"):SetActive(false)
    self:GetViewGameObject("Container_ShowLove"):SetActive(true)
    self:GetViewComponent("Container_ShowLove"):ShowView()
    self._curView = self:GetViewComponent("Container_ShowLove")
    self._backToIWantMarriage = true
end

function GetMarryPanel:CloseShowLove()
    self:GetViewGameObject("Container_IWantMarriage"):SetActive(true)
    self:GetViewGameObject("Container_ShowLove"):SetActive(false)
    self:GetViewComponent("Container_ShowLove"):CloseView()
end

function GetMarryPanel:BookWeddingBtnClick()
    if MarriageManager:HaveMate() then
        self:GetViewGameObject("Container_IWantMarriage"):SetActive(false)
        self:GetViewGameObject("Container_BookWedding"):SetActive(true)
        self:GetViewComponent("Container_BookWedding"):ShowView()
        self._backToIWantMarriage = true
        self._curView = self:GetViewComponent("Container_BookWedding")
    else
        local showText = LanguageDataHelper.GetContent(59073)
        GUIManager.ShowText(2, showText)
    end
end

function GetMarryPanel:InviteGuestBtnClick()
    if not MarriageManager:HaveMate() then
        local showText = LanguageDataHelper.GetContent(59073)
        GUIManager.ShowText(2, showText)
        return
    end
    if MarriageManager:GetMyBookingHour() == nil then
        local showText = LanguageDataHelper.GetContent(59077)
        GUIManager.ShowText(2, showText)
        return
    end
    self:GetViewGameObject("Container_IWantMarriage"):SetActive(false)
    self:GetViewGameObject("Container_InviteGuest"):SetActive(true)
    self:GetViewComponent("Container_InviteGuest"):ShowView()
    self._backToIWantMarriage = true
    self._curView = self:GetViewComponent("Container_InviteGuest")
end

function GetMarryPanel:AcceptLoveBtnClick()
    if MarriageManager:HaveMate() then
        self:GetViewGameObject("Container_AcceptLove"):SetActive(true)
        self:GetViewComponent("Container_AcceptLove"):ShowView()
        self:GetViewComponent("Container_AcceptLove"):ShowBooking(true)
    else
        local showText = LanguageDataHelper.GetContent(59073)
        GUIManager.ShowText(2, showText)
    end
end

function GetMarryPanel:HoldingWeddingBtnClick()
    if MarriageManager:HaveMate() then
        self:GetViewGameObject("Container_HoldingWedding"):SetActive(true)
        self:GetViewComponent("Container_HoldingWedding"):ShowView()
    else
        local showText = LanguageDataHelper.GetContent(59073)
        GUIManager.ShowText(2, showText)
    end
end

function GetMarryPanel:BecomeCoupleBtnClick()
    if MarriageManager:HaveMate() then
        self:GetViewGameObject("Container_AcceptLove"):SetActive(true)
        self:GetViewComponent("Container_AcceptLove"):ShowView()
        self:GetViewComponent("Container_AcceptLove"):ShowBooking(false)
    else
        local showText = LanguageDataHelper.GetContent(59073)
        GUIManager.ShowText(2, showText)
    end
end

function GetMarryPanel:CloseBtnClick()
    if self._backToIWantMarriage then
        self._backToIWantMarriage = false
        self:GetMarryBtnClick()
    else
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_get_marry_close_button")
        GUIManager.ClosePanel(PanelsConfig.GetMarry)
    end
end
