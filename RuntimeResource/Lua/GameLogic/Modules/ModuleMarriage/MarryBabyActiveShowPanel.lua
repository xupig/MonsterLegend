-- MarryBabyActiveShowPanel.lua
require "Modules.ModuleMarriage.ChildComponent.BobyActiveShowItem"

local MarryBabyActiveShowPanel = Class.MarryBabyActiveShowPanel(ClassTypes.BasePanel)

local BobyActiveShowItem = ClassTypes.BobyActiveShowItem
local MarryChildbaseDataHelper = GameDataHelper.MarryChildbaseDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local closeTime = 3

function MarryBabyActiveShowPanel:Awake()
	self:InitView()
end

function MarryBabyActiveShowPanel:OnDestroy()
	self._csBH = nil
end

function MarryBabyActiveShowPanel:OnShow(achievementId)
    self:AddEventListeners()
    self._showList = {}
    self._showList[achievementId] = achievementId
    self:SetData(achievementId)
end

function MarryBabyActiveShowPanel:SetData(achievementId)
    self:ShowItem(achievementId)
    GameWorld.PlaySound(14)
end

function MarryBabyActiveShowPanel:OnClose()
    for i,v in ipairs(self._itemContainers) do
        v:ClearTimer()
    end

    for k,v in pairs(self._useViews) do
        v:ClearTimer()
        table.insert(self._itemContainers, v)
    end

    self._useNum = 0
    self._useViews = {}
    self._moveAchievementId = nil
    self._showAchievementId = nil
	self:RemoveEventListeners()
end

function MarryBabyActiveShowPanel:AddEventListeners()  
    EventDispatcher:AddEventListener(GameEvents.ON_BOBYAVTIVE_ITEM_CLICK, self._onItemClick)
    EventDispatcher:AddEventListener(GameEvents.ON_CLOSE_BOBYACTIVE_ITEM, self._closeItem)
end

function MarryBabyActiveShowPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_BOBYAVTIVE_ITEM_CLICK, self._onItemClick)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLOSE_BOBYACTIVE_ITEM, self._closeItem)
end

function MarryBabyActiveShowPanel:InitView()
    self._onItemClick = function(achievementId) self:OnItemClick(achievementId) end
    self._closeItem = function(achievementId) self:CloseItem(achievementId) end

    self._useViews = {}
    self._itemContainers = {}
    self._itemNum = 0
    self._useNum = 0
end

function MarryBabyActiveShowPanel:SetValue(achievementId)
    self._titleText.text = LanguageDataHelper.GetContent(MarryChildbaseDataHelper:GetName(achievementId))
end

function MarryBabyActiveShowPanel:OnItemClick(achievementId)
    GUIManager.ClosePanel(PanelsConfig.MarryBabyActiveShow)

    if GUIManager.reopenSubMainMenu > 0 then
        GUIManager.reopenSubMainMenu = GUIManager.reopenSubMainMenu + 1
    end
    GUIManager.ShowPanel(PanelsConfig.Achievement, achievementId)
end

function MarryBabyActiveShowPanel:CloseItem(achievementId)
    if self._moveAchievementId == achievementId then
        self._moveAchievementId = nil
    end
    
    self._useNum = self._useNum - 1
    if self._useNum == 0 then
        GUIManager.ClosePanel(PanelsConfig.MarryBabyActiveShow)
    end
    table.insert(self._itemContainers, self._useViews[achievementId])
    self._useViews[achievementId] = nil
end

function MarryBabyActiveShowPanel:CopyContainer()
    self._itemNum = self._itemNum + 1
    return self:CopyUIGameObject("Container_MarryBabyActiveShow/Container_Demo", "Container_MarryBabyActiveShow/Container_Appear")
end

function MarryBabyActiveShowPanel:ShowItem(achievementId)
    local view
    if table.isEmpty(self._itemContainers) then
        --需要创建新的container
        local item = self:CopyContainer()
        view = UIComponentUtil.AddLuaUIComponent(item, BobyActiveShowItem)
    else
        view = self._itemContainers[#self._itemContainers]
        self._itemContainers[#self._itemContainers] = nil
    end

    if self._showAchievementId ~= nil then        
        self._useViews[self._showAchievementId]:Move()
        if self._moveAchievementId ~= nil then
            self._useViews[self._moveAchievementId]:Hide()
        end
        self._moveAchievementId = self._showAchievementId     
    end
    view:SetAchievementId(achievementId)
    view:ShowView()
    self._showAchievementId = achievementId
    self._useNum = self._useNum + 1
    self._useViews[achievementId] = view
end