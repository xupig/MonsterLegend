-- MarriageGuestManagerPanel.lua
local MarriageGuestManagerPanel = Class.MarriageGuestManagerPanel(ClassTypes.BasePanel)

require "Modules.ModuleMarriage.ChildView.GuestManagerMainUIView"

local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager
local GuestManagerMainUIView = ClassTypes.GuestManagerMainUIView

function MarriageGuestManagerPanel:Awake()
    self._guestManagerView = self:AddChildLuaUIComponent("Container_GuestManager", GuestManagerMainUIView)
end

function MarriageGuestManagerPanel:OnShow(data)
    self._guestManagerView:ShowView()
end

function MarriageGuestManagerPanel:OnClose()
    self._guestManagerView:HideView()
end

function MarriageGuestManagerPanel:OnDestroy()

end

function MarriageGuestManagerPanel:WinBGType()
    return PanelWinBGType.NoBG
end