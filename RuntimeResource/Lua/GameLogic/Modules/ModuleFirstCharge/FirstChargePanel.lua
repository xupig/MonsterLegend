require "Modules.ModuleFirstCharge.ChildComponent.FirstChargeRewardItem"

local FirstChargePanel = Class.FirstChargePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FirstChargeRewardItem = ClassTypes.FirstChargeRewardItem
local XArtNumber = GameMain.XArtNumber
local EquipModelComponent = GameMain.EquipModelComponent
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PlayerModelComponent = GameMain.PlayerModelComponent
local action_config = require("ServerConfig/action_config")
local UIParticle = ClassTypes.UIParticle
local XImageFlowLight = GameMain.XImageFlowLight

--override
function FirstChargePanel:Awake()
    self._state = 0
    self:InitView()
    self:InitListenerFunc()
end

function FirstChargePanel:InitListenerFunc()
    self._refreshFirstChargeRewardState = function() self:RefreshButtonState() end
end


--override
function FirstChargePanel:OnDestroy()

end

function FirstChargePanel:OnShow(data)
    self._iconBg = self:FindChildGO('Image_BG2')
    GameWorld.AddIcon(self._iconBg,13524)
    GUIManager.ClosePanel(PanelsConfig.FirstChargeTips)
    --领取首充后直接跳转充值界面
    if GameWorld.Player().first_charge_reward_got == 1 then
        GUIManager.ClosePanel(PanelsConfig.FirstCharge)
        GUIManager.ShowPanelByFunctionId(43)
        return
    end
    self:AddEventListeners()
    self:RefreshView()
    self._fx1.gameObject:SetActive(true)
    self._fx2.gameObject:SetActive(true)
    self:ShowModel()
end

function FirstChargePanel:OnClose()
    self._iconBg=nil
    self:RemoveEventListeners()
    GameWorld.ShowPlayer():ShowModel(3)
    GameWorld.ShowPlayer():SyncShowPlayerFacade()
    self._fx1.gameObject:SetActive(false)
    self._fx2.gameObject:SetActive(false)
    self:HideModel()
end

function FirstChargePanel:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.Refresh_First_Charge_Reward_State, self._refreshFirstChargeRewardState)
end

function FirstChargePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_First_Charge_Reward_State, self._refreshFirstChargeRewardState)
end

function FirstChargePanel:InitView()
    self._closeBtn = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeBtn, function() self:OnCloseButton() end)
    self._chargeBtn = self:FindChildGO("Button_Charge")
    self._csBH:AddClick(self._chargeBtn, function() self:OnChargeButton() end)
    self._chargeBtnText = self:GetChildComponent("Button_Charge/Text", "TextMeshWrapper")
    self._chargeBtnWrapper = self._chargeBtn:GetComponent("ButtonWrapper")
    self._chargeBtnWrapper.interactable = false

    self._chargeButtonFlowLight = self:AddChildComponent("Button_Charge", XImageFlowLight)
    self._chargeButtonFlowLight.enabled = false

    self._redPointImage = self:FindChildGO("Button_Charge/Image_RedPoint")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(FirstChargeRewardItem)
    self._list:SetPadding(0, 0, 0, 4)
    self._list:SetGap(0, 20)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)

    self._fashionNumber = self:AddChildComponent('Container_FashionItem/Container_FightPower', XArtNumber)
    self._fashionNumber:SetNumber(0)
    self._fashionItem = self:AddChildLuaUIComponent("Container_FashionItem/Container_Item", FirstChargeRewardItem)
    self._fashionModelComponent = self:AddChildComponent('Container_FashionItem/Container_Model', PlayerModelComponent)

    self._weaponNumber = self:AddChildComponent('Container_WeaponItem/Container_FightPower', XArtNumber)
    self._weaponNumber:SetNumber(0)
    self._weaponItem = self:AddChildLuaUIComponent("Container_WeaponItem/Container_Item", FirstChargeRewardItem)
    self._weaponModelComponent = self:AddChildComponent('Container_WeaponItem/Container_Model', EquipModelComponent)

    self._fx1 = UIParticle.AddParticle(self.gameObject, "Container_FashionItem/Container_Fx/FirstCharge")
    self._fx2 = UIParticle.AddParticle(self.gameObject, "Container_WeaponItem/Container_Fx/FirstCharge")
end

function FirstChargePanel:OnCloseButton()
    GUIManager.ClosePanel(PanelsConfig.FirstCharge)
end

function FirstChargePanel:OnChargeButton()
    if GameWorld.Player().first_charge_reward_got == 0 then
        self:OnCloseButton()
        GUIManager.ShowPanelByFunctionId(43)
    elseif GameWorld.Player().first_charge_reward_got == 2 then
        GameWorld.Player().server.first_charge_action_req(action_config.FIRST_CHARGE_REWARD, "")
    end
end

function FirstChargePanel:RefreshView()
    local cfg = GlobalParamsHelper.GetParamValue(764)
    local listData = {}
    for k,v in pairs(cfg) do
        table.insert(listData, {k,v})
    end
    self._list:SetDataList(listData)
    self:ShowFashionReward()
    self:ShowWeaponReward()
    self:RefreshButtonState()
end

function FirstChargePanel:RefreshButtonState()
    if GameWorld.Player().first_charge_reward_got == 1 then
        --已领取
        self._chargeBtnText.text = LanguageDataHelper.CreateContent(76903)
        self._chargeBtnWrapper.interactable = false
        self._redPointImage:SetActive(false)
        self._chargeButtonFlowLight.enabled = false
    elseif GameWorld.Player().first_charge_reward_got == 0 then
        --未达到
        self._chargeBtnText.text = LanguageDataHelper.CreateContent(76901)
        self._chargeBtnWrapper.interactable = true
        self._redPointImage:SetActive(false)
        self._chargeButtonFlowLight.enabled = true
    elseif GameWorld.Player().first_charge_reward_got == 2 then
        --可领取
        self._chargeBtnText.text = LanguageDataHelper.CreateContent(76614)
        self._chargeBtnWrapper.interactable = true
        self._redPointImage:SetActive(true)
        self._chargeButtonFlowLight.enabled = true
    end
end

function FirstChargePanel:ShowFashionReward()
    local cfg = GlobalParamsHelper.GetParamValue(762)[GameWorld.Player():GetVocationGroup()]
    self._fashionItem:OnRefreshData({cfg[1], 1})

    GameWorld.ShowPlayer():ShowModel(1)
    if cfg[2] ~= 0 then
        GameWorld.ShowPlayer():SetCurCloth(cfg[2])
    end
    if cfg[3] ~= 0 then
        GameWorld.ShowPlayer():SetCurHead(cfg[3])
    end
    if cfg[4] ~= 0 then
        GameWorld.ShowPlayer():SetCurWeapon(cfg[4])
    end
    --去掉翅膀显示
    GameWorld.ShowPlayer():SetCurWing(0)
    self._fashionModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)

    self._fashionNumber:SetNumber(cfg[5])
end

function FirstChargePanel:ShowWeaponReward()
    local cfg = GlobalParamsHelper.GetParamValue(763)[GameWorld.Player():GetVocationGroup()]
    self._weaponItem:OnRefreshData({cfg[1], 1})
    self._weaponModelComponent:SetStartSetting(Vector3(cfg[3], cfg[4], cfg[5]), Vector3(cfg[6], cfg[7], cfg[8]), cfg[9])
    self._weaponModelComponent:LoadEquipModel(cfg[2], "")
    self._weaponNumber:SetNumber(cfg[10])
end

function FirstChargePanel:ShowModel()
    self._fashionModelComponent:Show()
    self._weaponModelComponent:Show()
end

function FirstChargePanel:HideModel()
    self._fashionModelComponent:Hide()
    self._weaponModelComponent:Hide()
end

function FirstChargePanel:WinBGType()
    return PanelWinBGType.NoBG
end