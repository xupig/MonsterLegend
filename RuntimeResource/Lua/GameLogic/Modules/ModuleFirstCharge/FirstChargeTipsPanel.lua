local FirstChargeTipsPanel = Class.FirstChargeTipsPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local XArtNumber = GameMain.XArtNumber
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local action_config = require("ServerConfig/action_config")
local EquipModelComponent = GameMain.EquipModelComponent
local UIParticle = ClassTypes.UIParticle
local TimerHeap = GameWorld.TimerHeap
local UIComponentUtil = GameUtil.UIComponentUtil

local End_Time = 6000

--override
function FirstChargeTipsPanel:Awake()
    self.disableCameraCulling = true
    self._state = 0
    self:InitView()
end

--override
function FirstChargeTipsPanel:OnDestroy()

end

function FirstChargeTipsPanel:OnShow(data)
    self:AddEventListeners()
    self:RefreshView()
    self._timer = TimerHeap:AddTimer(End_Time, 0, 0, function()
		self:OnCloseButton()
    end)
    self:ShowModel()
    self._fx.gameObject:SetActive(true)
end

function FirstChargeTipsPanel:OnClose()
    self:RemoveEventListeners()
    self:HideModel()
    self._fx.gameObject:SetActive(false)
end

function FirstChargeTipsPanel:AddEventListeners()
    
end

function FirstChargeTipsPanel:RemoveEventListeners()

end

function FirstChargeTipsPanel:InitView()
    self._tipsContainer = self:FindChildGO("Container_Tips")

    self._closeBtn = self:FindChildGO("Container_Tips/Button_Close")
    self._csBH:AddClick(self._closeBtn, function() self:OnCloseButton() end)
    self._chargeBtn = self:FindChildGO("Container_Tips/Button_Charge")
    self._csBH:AddClick(self._chargeBtn, function() self:OnChargeButton() end)
    self._bgBtn = self:FindChildGO("Container_Tips/Button_BG")
    self._csBH:AddClick(self._bgBtn, function() self:OnBGButton() end)

    self._weaponModelComponent = self:AddChildComponent('Container_Tips/Container_Model', EquipModelComponent)
    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Tips/Container_Fx/FirstCharge")
    self._fx:SetScale(0.8)
end

function FirstChargeTipsPanel:OnCloseButton()
    GUIManager.ClosePanel(PanelsConfig.FirstChargeTips)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function FirstChargeTipsPanel:OnChargeButton()
    self:OnCloseButton()
    GUIManager.ShowPanel(PanelsConfig.FirstCharge)
end

function FirstChargeTipsPanel:OnBGButton()
    self:OnCloseButton()
    GUIManager.ShowPanel(PanelsConfig.FirstCharge)
end


function FirstChargeTipsPanel:RefreshView()
    self:SetShowPosition()
    self:ShowWeapon()
end

function FirstChargeTipsPanel:SetShowPosition()
    local uiRoot = GUIManager.GetUIRoot()
	local targetGo = UIComponentUtil.FindChild(uiRoot, "LayerUIMain/Panel_MainMenu/Container_Row1/Container_Stable/Button_3")
	if targetGo ~= nil then
        local targetRect = targetGo.transform:GetComponent('RectTransform')
        self._tipsContainer.transform.position = targetRect.position
        self._tipsContainer.transform.localPosition = self._tipsContainer.transform.localPosition + Vector3.New(18,-30,0)
    end
end

function FirstChargeTipsPanel:ShowWeapon()
    local cfg = GlobalParamsHelper.GetParamValue(834)[GameWorld.Player():GetVocationGroup()]
    self._weaponModelComponent:SetStartSetting(Vector3(cfg[2], cfg[3], cfg[4]), Vector3(cfg[5], cfg[6], cfg[7]), cfg[8])
    self._weaponModelComponent:LoadEquipModel(cfg[1], "")
end

function FirstChargeTipsPanel:ShowModel()
    self._weaponModelComponent:Show()
end

function FirstChargeTipsPanel:HideModel()
    self._weaponModelComponent:Hide()
end

function FirstChargeTipsPanel:WinBGType()
    return PanelWinBGType.NoBG
end