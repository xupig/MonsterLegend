local ChargeMirrorPanel = Class.ChargeMirrorPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local action_config = require("ServerConfig/action_config")

--override
function ChargeMirrorPanel:Awake()

end

--override
function ChargeMirrorPanel:OnDestroy()

end

function ChargeMirrorPanel:OnShow(data)
    GUIManager.ClosePanel(PanelsConfig.ChargeMirror)
    GUIManager.ShowPanelByFunctionId(43)
end

function ChargeMirrorPanel:OnClose()

end

function ChargeMirrorPanel:WinBGType()
    return PanelWinBGType.NoBG
end