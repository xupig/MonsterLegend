require "UIComponent.Extend.ItemGrid"

local FirstChargeRewardItem = Class.FirstChargeRewardItem(ClassTypes.UIListItem)

FirstChargeRewardItem.interface = GameConfig.ComponentsConfig.Component
FirstChargeRewardItem.classPath = "Modules.ModuleFirstCharge.ChildComponent.FirstChargeRewardItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local GUIManager = GameManager.GUIManager

function FirstChargeRewardItem:Awake()
    self:InitView()
end

--override
function FirstChargeRewardItem:OnDestroy() 

end

function FirstChargeRewardItem:InitView()
    self._parent = self:FindChildGO("Container_Icon")
    local itemGo = GUIManager.AddItem(self._parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function()
                    TipsManager:ShowItemTipsById(self.itemId)
                end
    self._icon:SetPointerDownCB(self._func)
end

--data {itemId, count}
function FirstChargeRewardItem:OnRefreshData(data)
    self.data = data
    self.itemId = data[1]
    self._icon:SetItem(self.itemId, data[2])
    self._icon:SetBind(true)
end