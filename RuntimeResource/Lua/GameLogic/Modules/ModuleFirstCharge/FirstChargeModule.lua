--FirstChargeModule.lua
FirstChargeModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function FirstChargeModule.Init()
    GUIManager.AddPanel(PanelsConfig.FirstCharge,"Panel_FirstCharge",GUILayer.LayerUIMiddle,"Modules.ModuleFirstCharge.FirstChargePanel",true,false, nil, true, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.FirstChargeTips,"Panel_FirstChargeTips",GUILayer.LayerUIMiddle,"Modules.ModuleFirstCharge.FirstChargeTipsPanel",true,false, nil, true, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.ChargeMirror,"Panel_ChargeMirror",GUILayer.LayerUIMain,"Modules.ModuleFirstCharge.ChargeMirrorPanel",true,false, nil, true, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return FirstChargeModule