-- GuildConquestFlagItem.lua
local GuildConquestFlagItem = Class.GuildConquestFlagItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIListItem = ClassTypes.UIListItem
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local GuildConquestManager = PlayerManager.GuildConquestManager

local IndexToNumber = {"A", "B", "C", "D", "E"}

GuildConquestFlagItem.interface = GameConfig.ComponentsConfig.Component
GuildConquestFlagItem.classPath = "Modules.ModuleGuildConquestSpace.ChildComponent.GuildConquestFlagItem"

function GuildConquestFlagItem:Awake()
    self._base.Awake(self)
    self:InitView()
    self.selected = false
    self._stickBeginDrag = function() self:OnChangeSelectState() end
    self._onPlayerSkillButton = function() self:OnChangeSelectState() end
end

function GuildConquestFlagItem:OnDestroy()
end

function GuildConquestFlagItem:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.Stick_Begin_Drag, self._stickBeginDrag)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandPlaySkill, self._onPlayerSkillButton)
end

function GuildConquestFlagItem:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.Stick_Begin_Drag, self._stickBeginDrag)
    EventDispatcher:RemoveEventListener(GameEvents.PlayerCommandPlaySkill, self._onPlayerSkillButton)
end

function GuildConquestFlagItem:InitView()
    self._indexText = self:GetChildComponent("Container_content/Text_Index", "TextMeshWrapper")
    self._nameText = self:GetChildComponent("Container_content/Text_Name", "TextMeshWrapper")
    self._icon = self:FindChildGO("Container_content/Container_Icon")
    self._goImage = self:FindChildGO("Container_content/Image_Go")
    self._goImage:SetActive(false)
end

function GuildConquestFlagItem:OnChangeSelectState()
    if self.selected then
        self:ShowSelect(false)
    end
end

--override {id,pos}
function GuildConquestFlagItem:OnRefreshData(data)
    self.data = data
    self._indexText.text = IndexToNumber[self:GetIndex() + 1]
end

function GuildConquestFlagItem:ShowSelect(state)
    self.selected = state
    self._goImage:SetActive(state)
    if state and GuildConquestManager:GetCanMoveByPath() then
        GameWorld.Player():Move(self.data[2], -1)
    end
end

function GuildConquestFlagItem:SetFaction(factionId)
    local playerFactionId = GameWorld.Player().faction_id
    local iconId = 0
    if factionId == 0 then
        iconId = 13416 --未抢资源点
        self._nameText.text = LanguageDataHelper.CreateContent(53471)
    elseif factionId == playerFactionId then
        iconId = 13414 --我方资源点
        self._nameText.text = LanguageDataHelper.CreateContent(53470)
    else
        iconId = 13415 --敌方资源点
        self._nameText.text = LanguageDataHelper.CreateContent(53469)
    end
    GameWorld.AddIcon(self._icon, iconId)
end

