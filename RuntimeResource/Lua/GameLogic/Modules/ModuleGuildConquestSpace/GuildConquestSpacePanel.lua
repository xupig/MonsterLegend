require "Modules.ModuleGuildConquestSpace.ChildComponent.GuildConquestFlagItem"

local GuildConquestSpacePanel = Class.GuildConquestSpacePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InstanceManager = PlayerManager.InstanceManager
local UIProgressBar = ClassTypes.UIProgressBar
local StringStyleUtil = GameUtil.StringStyleUtil
local GuildDataHelper = GameDataHelper.GuildDataHelper
local public_config = require("ServerConfig/public_config")
local action_config = require("ServerConfig/action_config")
local GuildConquestManager = PlayerManager.GuildConquestManager
local GuildManager = PlayerManager.GuildManager
local DramaManager = GameManager.DramaManager
local GameSceneManager = GameManager.GameSceneManager
local GuildConquestFlagItem = ClassTypes.GuildConquestFlagItem

local Unit_COUNT = 4
local Grade_Lang = {[1]=53423, [2]=53424, [3]=53425, [4]=53426, [5]=53427}
local Max_Point_Count = 5
local Self_Point_Icon = 13414
local Enemy_Point_Icon = 13415
local Empty_Point_Icon = 13416

local Warnning_Point = 1500
local Start_Count_Down_Sec = 60

local function SortByKey1(a,b)
    return tonumber(a[1]) < tonumber(b[1])
end

--override
function GuildConquestSpacePanel:Awake()
    self.disableCameraCulling = true
    self:InitView()
    self:InitListenerFunc()
    self.firstShowText = false
    self.sceneLoaded = false
    self.canCloseBlock = false
end

function GuildConquestSpacePanel:InitListenerFunc()
    self._setGuildConquestInfo = function(data) self:SetGuildConquestInfo(data) end
    self._setGuildConquestPlayerCount = function(data) self:SetGuildConquestPlayerCount(data) end
    self._setGuildConquestResource = function(data) self:SetGuildConquestResource(data) end
    self._checkGuildConquestCloseBlock = function() self:CheckCloseBlock() end
    self._onGuildConquestSettle = function(data) self:OnGuildConquestSettle(data) end
    self._onDramaPlayEnd = function(name,dramaKey,id) self:OnDramaPlayEnd(name,dramaKey,id) end
    self._onClickFlagFactionChanged = function()self:OnClickFlagFactionChanged() end
    self._onShowPersonalInfo = function()self:ShowPersonalInfo() end
end

--override
function GuildConquestSpacePanel:OnDestroy()

end

function GuildConquestSpacePanel:OnShow(data)
    self._preSelected = -1
    self.firstShowText = false
    self:SetData(data)
    EventDispatcher:TriggerEvent(GameEvents.Show_VIP_Button, false)
    --EventDispatcher:TriggerEvent(GameEvents.Show_Be_Strong_Button, false)

    local mapId = GameSceneManager:GetCurrMapID()
    local clickFlags = GameSceneManager:GetClickFlagInfo(mapId)
    local flagData = {}
    self.idToIndex = {}
    for id, pos in pairs(clickFlags) do
        table.insert(flagData, {id, pos})
    end
    table.sort(flagData, SortByKey1)
    for k,v in ipairs(flagData) do
        self.idToIndex[v[1]] = k
    end
    self._list:RemoveAll()
    self._list:SetDataList(flagData)
    self:ShowPersonalInfo()
    self:OnClickFlagFactionChanged()
end

function GuildConquestSpacePanel:SetData(data)
    self:RefreshView(data)
end

function GuildConquestSpacePanel:OnClose()
    self:Reset()
    EventDispatcher:TriggerEvent(GameEvents.Show_VIP_Button, true)
    --EventDispatcher:TriggerEvent(GameEvents.Show_Be_Strong_Button, true)
end

function GuildConquestSpacePanel:OnEnable()
    self._tipsContainer:SetActive(true)
    self:AddEventListeners()
    self._startTimeShow = false
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateCountDown() end)
    self._personalInfoGO:SetActive(false)
    self:CheckCloseBlock()
end

function GuildConquestSpacePanel:OnDisable()
    self:RemoveEventListeners()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function GuildConquestSpacePanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Set_Guild_Conquest_Info, self._setGuildConquestInfo)
    EventDispatcher:AddEventListener(GameEvents.Set_Guild_Conquest_Player_Count, self._setGuildConquestPlayerCount)
    EventDispatcher:AddEventListener(GameEvents.Set_Guild_Conquest_Resource, self._setGuildConquestResource)
    EventDispatcher:AddEventListener(GameEvents.Check_Guild_Conquest_Close_Block, self._checkGuildConquestCloseBlock)
    EventDispatcher:AddEventListener(GameEvents.On_Guild_Conquest_Settle, self._onGuildConquestSettle)
    EventDispatcher:AddEventListener(GameEvents.DramaPlayEnd, self._onDramaPlayEnd)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Conquest_Click_Flag_Info, self._onClickFlagFactionChanged)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Conquest_Show_Personal_Info, self._onShowPersonalInfo)
end

function GuildConquestSpacePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guild_Conquest_Info, self._setGuildConquestInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guild_Conquest_Player_Count, self._setGuildConquestPlayerCount)
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guild_Conquest_Resource, self._setGuildConquestResource)
    EventDispatcher:RemoveEventListener(GameEvents.Check_Guild_Conquest_Close_Block, self._checkGuildConquestCloseBlock)
    EventDispatcher:RemoveEventListener(GameEvents.On_Guild_Conquest_Settle, self._onGuildConquestSettle)
    EventDispatcher:RemoveEventListener(GameEvents.DramaPlayEnd, self._onDramaPlayEnd)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Conquest_Click_Flag_Info, self._onClickFlagFactionChanged)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Conquest_Show_Personal_Info, self._onShowPersonalInfo)
end

function GuildConquestSpacePanel:InitView()
    self._nameText = self:GetChildComponent("Container_Self/Text_Name", "TextMeshWrapper")
    self._countText = self:GetChildComponent("Container_Self/Text_Count", "TextMeshWrapper")
    self._bgIcon = self:FindChildGO("Container_Self/Container_IconBG")
    self._icon = self:FindChildGO("Container_Self/Container_Icon")

    self._nameEnemyText = self:GetChildComponent("Container_Enemy/Text_Name", "TextMeshWrapper")
    self._countEnemyText = self:GetChildComponent("Container_Enemy/Text_Count", "TextMeshWrapper")
    self._bgIconEnemy = self:FindChildGO("Container_Enemy/Container_IconBG")
    self._iconEnemy = self:FindChildGO("Container_Enemy/Container_Icon")

    self:InitProgress()

    self._instanceStartTimeContainer = self:FindChildGO("Container_InstanceStartTime")
    self._instanceStartTimeContainer:SetActive(false)
    self._startTimeText = self:GetChildComponent("Container_InstanceStartTime/Text_Time", "TextMeshWrapper")

    self._personalInfoGO = self:FindChildGO("Container_KillInfo")
    self._personalInfoText = self:GetChildComponent("Container_KillInfo/Image_BG/Text_Info", "TextMeshWrapper")

    self._tipsContainer = self:FindChildGO("Container_Tips")
    self._closeBtn = self:FindChildGO("Container_Tips/Button_Confirm")
    self._csBH:AddClick(self._closeBtn, function() self:OnTipsCloseButton() end)
    self._tipsContainer:SetActive(false)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_FlagInfo/ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(GuildConquestFlagItem)
    self._list:SetPadding(4, 0, 0, 0)
    self._list:SetGap(5, 5)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 100)
    self._scrollView:SetScrollRectEnable(false)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list:SetItemClickedCB(itemClickedCB)
end

function GuildConquestSpacePanel:OnListItemClicked(idx)
    if self._preSelected ~= -1 then
        self._list:GetItem(self._preSelected):ShowSelect(false)
    end
    self._list:GetItem(idx):ShowSelect(true)
    self._preSelected = idx
    local data = self._list:GetDataByIndex(idx)
end

function GuildConquestSpacePanel:InitProgress()
    self._comp = UIProgressBar.AddProgressBar(self.gameObject, "Container_Self/ProgressBar")
    self._comp:SetProgress(0)
    self._comp:ShowTweenAnimate(true)
    self._comp:SetMutipleTween(true)
    self._comp:SetIsResetToZero(false)
    self._comp:SetTweenTime(0.2)

    self._compEnemy = UIProgressBar.AddProgressBar(self.gameObject, "Container_Enemy/ProgressBar")
    self._compEnemy:SetProgress(0)
    self._compEnemy:ShowTweenAnimate(true)
    self._compEnemy:SetMutipleTween(true)
    self._compEnemy:SetIsResetToZero(false)
    self._compEnemy:SetTweenTime(0.2)

    self:OnSelfResourceChange(0)
    self:OnEnemyResourceChange(0)
end

function GuildConquestSpacePanel:OnTipsCloseButton()
    self._tipsContainer:SetActive(false)
end

function GuildConquestSpacePanel:OnClickFlagFactionChanged()
    local playerFactionId = GameWorld.Player().faction_id
    local clickFlagInfo = GuildConquestManager:GetClickFlagInfo()
    for id, factionId in pairs(clickFlagInfo) do
        local index = self.idToIndex[id]
        self._list:GetItem(index-1):SetFaction(factionId)
    end
end

function GuildConquestSpacePanel:ShowPersonalInfo()
    local data = GuildConquestManager:GetActionData(action_config.GUILD_CONQUEST_NOTIFY_PERSONAL_INFO) or {}
    local killCount = data[1] or 0
    local deathCount = data[2] or 0
    local resourceCount = data[3] or 0
    self._personalInfoText.text = LanguageDataHelper.CreateContentWithArgs(53466, {["0"]=killCount, ["1"]=deathCount, ["2"]=resourceCount})
end

function GuildConquestSpacePanel:OnDramaPlayEnd(name,dramaKey,id)
    --CG播放结束显示结算
end

function GuildConquestSpacePanel:OnGuildConquestSettle(data)
    --播放CG
    --DramaManager:Trigger("LEVEL_UP", "100")
end

function GuildConquestSpacePanel:CheckCloseBlock()
    if self.canCloseBlock and GuildConquestManager:GetSceneLoadFinish() then
        self._tipsContainer:SetActive(false)
        self._personalInfoGO:SetActive(true)
        EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, 280002)
        EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, 280003)
        EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, 280004)
        EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, 280005)
        EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, 280006)
        EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, 280007)
        EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, 330002)
        GuildConquestManager:SetCanMoveByPath(true)
    end
end

function GuildConquestSpacePanel:UpdateCountDown()
    self:ShowInstanceStartTime()
end

function GuildConquestSpacePanel:ShowInstanceStartTime()
    local state = false
    local leftTime = 0
    local serverData = GuildConquestManager:GetActionData(action_config.GUILD_CONQUEST_NOTIFY_ROOM_STAGE)
    if not self.canCloseBlock and serverData ~= nil and serverData[1] ~= public_config.INSTANCE_STAGE_3 then
        self.canCloseBlock = true
        self:CheckCloseBlock()
    end
    if serverData ~= nil and serverData[1] == public_config.INSTANCE_STAGE_3 then
        local startTime = serverData[2]
        leftTime = math.max(startTime + Start_Count_Down_Sec - DateTimeUtil.GetServerTime(), 0)
        if leftTime > 0 then
            state = true
        end
    end
    if self._startTimeShow ~= state then
        self._startTimeShow = state
        self._instanceStartTimeContainer:SetActive(state)
    end
    if self._startTimeShow then
        self._startTimeText.text = LanguageDataHelper.CreateContentWithArgs(53465, {["0"]=leftTime})
    end
end

function GuildConquestSpacePanel:OnSelfResourceChange(cur)
    local max = GlobalParamsHelper.GetParamValue(685)
    self._comp:SetProgressByValue(cur, max)
    local str = LanguageDataHelper.CreateContentWithArgs(53420,{["0"]=cur .. "/" .. max})
    self._comp:SetProgressText(str)
    if not self.firstShowText and cur >= Warnning_Point then
        self.firstShowText = true
        if self.selfConquestInfo ~= nil then
            GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(53464, {["0"]=self.selfConquestInfo[1]}))
        end
    end
end

function GuildConquestSpacePanel:OnEnemyResourceChange(cur)
    local max = GlobalParamsHelper.GetParamValue(685)
    self._compEnemy:SetProgressByValue(cur, max)
    local str = LanguageDataHelper.CreateContentWithArgs(53420,{["0"]=cur .. "/" .. max})
    self._compEnemy:SetProgressText(str)
    if not self.firstShowText and cur >= Warnning_Point then
        self.firstShowText = true
        if self.enemyConquestInfo ~= nil then
            GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(53464, {["0"]=self.enemyConquestInfo[1]}))
        end
    end
end

--{公会名，图腾id，旗帜id ,公会对战排名}
function GuildConquestSpacePanel:ShowSelfInfo(data)
    self.selfConquestInfo = data
    self._nameText.text = data[1]
    local flagId = data[3]
    local totemId = data[2]
    GameWorld.AddIcon(self._bgIcon, flagId)
    GameWorld.AddIcon(self._icon, totemId)
end

--{num,score}
function GuildConquestSpacePanel:ShowSelfResourceInfo(data)
    self:OnSelfResourceChange(data[2])
end

function GuildConquestSpacePanel:ShowSelfPlayerCount(count)
    count = count or 0
    self._countText.text = LanguageDataHelper.CreateContentWithArgs(53421,{["0"]=count})
end

--{公会名，图腾id，旗帜id, 公会对战排名}
function GuildConquestSpacePanel:ShowEnemyInfo(data)
    self.enemyConquestInfo = data
    self._nameEnemyText.text = data[1]
    local flagId = data[3]
    local totemId = data[2]
    GameWorld.AddIcon(self._bgIconEnemy, flagId)
    GameWorld.AddIcon(self._iconEnemy, totemId)
end

--{num,score}
function GuildConquestSpacePanel:ShowEnemyResourceInfo(data)
    self:OnEnemyResourceChange(data[2])
end

function GuildConquestSpacePanel:ShowEnemyPlayerCount(count)
    count = count or 0
    self._countEnemyText.text = LanguageDataHelper.CreateContentWithArgs(53421,{["0"]=count})
end

function GuildConquestSpacePanel:SetGuildConquestInfo(data)
    local selfGuildId = GuildManager:GetGuildId()
    for k,v in pairs(data) do
        if k == selfGuildId then
            self:ShowSelfInfo(v)
        else
            self:ShowEnemyInfo(v)
        end
    end
end

function GuildConquestSpacePanel:SetGuildConquestPlayerCount(data)
    local selfGuildId = GuildManager:GetGuildId()
    for k,v in pairs(data) do
        if k == selfGuildId then
            self:ShowSelfPlayerCount(v)
        else
            self:ShowEnemyPlayerCount(v)
        end
    end
end

function GuildConquestSpacePanel:SetGuildConquestResource(data)
    local selfGuildId = GuildManager:GetGuildId()
    for k,v in pairs(data) do
        if k == selfGuildId then
            self:ShowSelfResourceInfo(v)
        else
            self:ShowEnemyResourceInfo(v)
        end
    end
end

function GuildConquestSpacePanel:RefreshView(data)
    
end

function GuildConquestSpacePanel:Reset()
    self.firstShowText = false
    self.enemyConquestInfo = nil
    self.selfConquestInfo = nil
    self._instanceStartTimeContainer:SetActive(false)
    self._startTimeShow = false
    self.sceneLoaded = false
    self.canCloseBlock = false
end

function GuildConquestSpacePanel:WinBGType()
    return PanelWinBGType.NoBG
end