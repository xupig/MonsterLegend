GuildConquestSpaceModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function GuildConquestSpaceModule.Init()
    GUIManager.AddPanel(PanelsConfig.GuildConquestSpace,"Panel_GuildConquestSpace",GUILayer.LayerUIMiddle,"Modules.ModuleGuildConquestSpace.GuildConquestSpacePanel",true,PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return GuildConquestSpaceModule