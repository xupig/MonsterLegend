local DailyActivityExPanel = Class.DailyActivityExPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager

require "Modules.ModuleDailyActivity.ChildView.DailyView"
local DailyView = ClassTypes.DailyView

require "Modules.ModuleDailyActivity.ChildView.DailyFindView"
local DailyFindView = ClassTypes.DailyFindView

local UIToggleGroup = ClassTypes.UIToggleGroup

function DailyActivityExPanel:Awake()
	self:InitCallBack()
	self:InitComps()
end

function DailyActivityExPanel:InitCallBack()
    self._tabClickCB = function (index)
        self:OnToggleGroupClick(index)
    end
end

--tab点击处理
function DailyActivityExPanel:OnToggleGroupClick(index)
    if self._select and index ~= self._select then
        self._views[self._select]:SetActive(false)
    end
    self._select = index
    self._views[self._select]:SetActive(true)
    self._views[self._select]:RefreshView(self._select)
end

function DailyActivityExPanel:InitComps()
    self._views = {}
	self._dailyView = self:AddChildLuaUIComponent("Container_Daily", DailyView)
	self._dailyFindView = self:AddChildLuaUIComponent("Container_Find", DailyFindView)
    
    self._views[1] = self._dailyView                --每日必做
    self._views[2] = self._dailyView                --限时活动
    self._views[3] = self._dailyFindView            --资源找回
 	
    self:SetTabClickCallBack(self._tabClickCB)
end

function DailyActivityExPanel:CloseBtnClick()
	GUIManager.ClosePanel(PanelsConfig.DailyActivityEx)
end

function DailyActivityExPanel:OnShow(data)
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
end

function DailyActivityExPanel:OnClose()
    self._select = nil
    self._dailyView:SetActive(false)
    self._dailyFindView:SetActive(false)
end

function DailyActivityExPanel:WinBGType()
    return PanelWinBGType.NormalBG
end