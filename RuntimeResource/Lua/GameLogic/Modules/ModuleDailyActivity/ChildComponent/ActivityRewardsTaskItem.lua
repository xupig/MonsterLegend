-- ActivityRewardsTaskItem.lua
local ActivityRewardsTaskItem = Class.ActivityRewardsTaskItem(ClassTypes.UIListItem)

ActivityRewardsTaskItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
ActivityRewardsTaskItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.ActivityRewardsTaskItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper

function ActivityRewardsTaskItem:Awake()
    self._icon = self:FindChildGO("Container_Icon")
end

function ActivityRewardsTaskItem:OnDestroy()
    self._icon = nil
end

--override
function ActivityRewardsTaskItem:OnRefreshData(data)
    GameWorld.AddIcon(self._icon, ItemDataHelper.GetIcon(data), nil)
end
