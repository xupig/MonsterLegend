--幕间剧的ListItem
-- DailyActivityTaskItem.lua
local DailyActivityTaskItem = Class.DailyActivityTaskItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ActivityDataHelper = GameDataHelper.ActivityDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local action_config = require("ServerConfig/action_config")
local UIListItem = ClassTypes.UIListItem
local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local MenuConfig = GameConfig.MenuConfig
local DailyActivityConfig = GameConfig.DailyActivityConfig

DailyActivityTaskItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
DailyActivityTaskItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.DailyActivityTaskItem"

require "Modules.ModuleDailyActivity.ChildComponent.DailyActivityTagsItem"
local DailyActivityTagsItem = ClassTypes.DailyActivityTagsItem

function DailyActivityTaskItem:Awake()
    self._base.Awake(self)
    self._textTitle = self:GetChildComponent("Container_Content/Text_Title", "TextMeshWrapper")
    self._textTimes = self:GetChildComponent("Container_Content/Container_Times/Text_Times", "TextMeshWrapper")
    self._textLimit = self:GetChildComponent("Container_Content/Text_Limit", "TextMeshWrapper")
    self._timesContainer = self:FindChildGO("Container_Content/Container_Times")
    self._imageFinished = self:FindChildGO("Container_Content/Image_Finished")
    self._containerTags = self:FindChildGO("Container_Content/Container_Tags")
    self._containerType = self:FindChildGO("Container_Content/Container_Type")
    self._containerIcon = self:FindChildGO("Container_Content/Container_Icon")
    self._selectImageGo = self:FindChildGO("Container_Content/Image_Select")
    self._btnJoin = self:FindChildGO("Container_Content/Button_Join")
    self._csBH:AddClick(self._btnJoin, function()self:JoinBtnClick() end)
    
    self._textTimes.text = ""
    self._textLimit.text = ""
    self._imageFinished:SetActive(false)

    self:InitTagsList()
end

function DailyActivityTaskItem:InitTagsList()
    local tagsListScrollView, tagsList = UIList.AddScrollViewList(self.gameObject, "Container_Content/Container_Tags/ScrollViewList")
    self._tagsList = tagsList
    self._tagsListScrollView = tagsListScrollView
    self._tagsListScrollView:SetHorizontalMove(false)
    self._tagsListScrollView:SetVerticalMove(false)
    self._tagsList:SetItemType(DailyActivityTagsItem)
    self._tagsList:SetPadding(0, 0, 0, 0)
    self._tagsList:SetGap(0, 0)
    self._tagsList:SetDirection(UIList.DirectionLeftToRight, -1, 1)
    
    self._tagsList:SetDataList({})
end

function DailyActivityTaskItem:OnDestroy()
    self._textTitle = nil
    self._textTimes = nil
    self._textLimit = nil
    self._imageFinished = nil
    self._containerTags = nil
    self._containerType = nil
    self._base.OnDestroy(self)
end

function DailyActivityTaskItem:JoinBtnClick()
    local follow = ActivityDataHelper:GetFollow(self._data)
    if follow ~= nil and follow[MenuConfig.SHOW_PANEL] ~= nil then
        for i, v in ipairs(follow[MenuConfig.SHOW_PANEL]) do
            GUIManager.ShowPanel(v)
        end
    end
    self:GetListObj():OnItemClicked(self)
end

function DailyActivityTaskItem:SetSelect(flag)
    self._selectImageGo:SetActive(flag)
end

--override
function DailyActivityTaskItem:OnRefreshData(data)
    self._data = data

    local isWeekly = ActivityDataHelper:GetType(data) == DailyActivityConfig.WEEKLY_VITALITY
    self._containerType:SetActive(isWeekly)

    if data ~= nil then
        GameWorld.AddIcon(self._containerIcon, ActivityDataHelper:GetIcon(data))
        self._textTitle.text = LanguageDataHelper.CreateContent(ActivityDataHelper:GetName(data))
        local tags = ActivityDataHelper:GetTag(data)
        if tags == nil then
            self._tagsList:SetDataList({})
        else
            self._tagsList:SetDataList(tags)
        end
        if ActivityDataHelper:GetTimes(data) == 0 then
            self._timesContainer:SetActive(false)
        else
            self._timesContainer:SetActive(true)
            local func = ActivityDataHelper:GetFunc(data)
            local leftTimes  
            if func then
                local flag = false
                local funcData
                for i,v in ipairs(func) do
                    if v[1] == DailyActivityConfig.GetLeftTimes then
                        flag = true
                        funcData = v
                        break
                    end
                end
                if flag then
                    local claaName = funcData[2][1]
                    local funcName = funcData[2][2]
                    if funcData and claaName and funcName then
                        local funcClass = PlayerManager[claaName]
                        leftTimes = funcClass[funcName](funcClass)
                        local totalTimes = ActivityDataHelper:GetTimes(data)
                        self._textTimes.text = (totalTimes - leftTimes) .. LanguageDataHelper.CreateContent(900) .. totalTimes           
                        local complete = (leftTimes == 0)
                        self._btnJoin:SetActive(not complete)
                        self._imageFinished:SetActive(complete)
                    end                    
                end
            end
        end
    else
        self._textTitle.text = ""
        self._textTimes.text = ""
    end
end
