-- DailyAppearAttrItem.lua
local DailyAppearAttrItem = Class.DailyAppearAttrItem(ClassTypes.UIListItem)

DailyAppearAttrItem.interface = GameConfig.ComponentsConfig.Component
DailyAppearAttrItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.DailyAppearAttrItem"

local AttriDataHelper = GameDataHelper.AttriDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

--override
function DailyAppearAttrItem:Awake()
    self._nameText = self:GetChildComponent("Text_Attr_Name",'TextMeshWrapper')    
    self._valueText = self:GetChildComponent("Text_Attr_Value",'TextMeshWrapper')
end

--override
function DailyAppearAttrItem:OnDestroy() 
    self._nameText = nil
    self._valueText = nil
end

function DailyAppearAttrItem:OnRefreshData(data)
    local nowLevel = GameWorld.Player().daily_level

    self._nameText.text = AttriDataHelper:GetName(data[1]) .. " :"
    if data[3] > nowLevel then
        self._valueText.text = StringStyleUtil.GetColorString(data[2], StringStyleUtil.green)
    else
        self._valueText.text = data[2]
    end
end