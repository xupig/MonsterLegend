-- RewardTipsItem.lua
local RewardTipsItem = Class.RewardTipsItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RewardTipsItem.interface = GameConfig.ComponentsConfig.Component
RewardTipsItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.RewardTipsItem"

local UIList = ClassTypes.UIList
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function RewardTipsItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function RewardTipsItem:OnDestroy() 

end

--override
function RewardTipsItem:OnRefreshData(data)
    --LoggerHelper.Error("RewardTipsItem:OnRefreshData(data)========================================")
    if data then
        --LoggerHelper.Error("RewardTipsItem:OnRefreshData========================================"..PrintTable:TableToStr(data))
        local itemId = data[1]
        local num = data[2][1]
        local isBind = data[2][2]

        self._itemIcon:ActivateTipsById()
        self._itemIcon:SetItem(itemId, num)
        if isBind == 1 then
            self._itemIcon:SetBind(true)
        else
            self._itemIcon:SetBind(false)
        end

    end
end