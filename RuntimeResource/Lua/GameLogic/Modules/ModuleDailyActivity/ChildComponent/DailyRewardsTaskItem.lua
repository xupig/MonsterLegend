--任务奖励和交付道具ListItem
-- DailyRewardsTaskItem.lua
require "UIComponent.Extend.ItemManager"
require "Modules.ModuleTask.ChildComponent.TaskRewardItem"
local TaskRewardItem = ClassTypes.TaskRewardItem
local ItemManager = ClassTypes.ItemManager
local DailyRewardsTaskItem = Class.DailyRewardsTaskItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DailyTaskDataHelper = GameDataHelper.DailyTaskDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local action_config = require("ServerConfig/action_config")
local UIListItem = ClassTypes.UIListItem
local UIToggle = ClassTypes.UIToggle
local public_config = GameWorld.public_config
local DailyActivityManager = PlayerManager.DailyActivityManager
local UIList = ClassTypes.UIList
local DailyActivityConfig = GameConfig.DailyActivityConfig
require "UIComponent.Extend.ItemManager"

DailyRewardsTaskItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
DailyRewardsTaskItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.DailyRewardsTaskItem"

function DailyRewardsTaskItem:Awake()
    self._base.Awake(self)
    self._textTitle = self:GetChildComponent("Container_Content/Text_Title", "TextMeshWrapper")
    self._textDesc = self:GetChildComponent("Container_Content/Text_Desc", "TextMeshWrapper")
    self._textProgress = self:GetChildComponent("Container_Content/Text_Progress", "TextMeshWrapper")
    self._imageFinished = self:FindChildGO("Container_Content/Image_Finished")
    self._containerIcon = self:FindChildGO("Container_Content/Container_Icon")
    self._containerVitalityIcon = self:FindChildGO("Container_Content/Container_Vitality/Container_Icon")
    self._textVitality = self:GetChildComponent("Container_Content/Container_Vitality/Text_Count", "TextMeshWrapper")
    self._btnGet = self:FindChildGO("Container_Content/Button_Get")
    self._btnGoTo = self:FindChildGO("Container_Content/Button_GoTo")
    self._csBH:AddClick(self._btnGet, function()self:GetBtnClick() end)
    self:InitList()
    self._onUpdateDailyActivityData = function(id)self:UpdateDailyActivityData(id) end
    self._itemManager = ItemManager()
    self._icon = self._itemManager:GetLuaUIComponent(nil, self._containerVitalityIcon)
    self._icon:SetItem(DailyActivityConfig.VITALITY_ITEM_ID)
    EventDispatcher:AddEventListener(GameEvents.OnUpdateDailyActivityData, self._onUpdateDailyActivityData)
end

function DailyRewardsTaskItem:OnDestroy()
    EventDispatcher:RemoveEventListener(GameEvents.OnUpdateDailyActivityData, self._onUpdateDailyActivityData)
    self._textTitle = nil
    self._textDesc = nil
    self._containerIcon = nil
    self._imageFinished = nil
    self._textProgress = nil
    self._btnGet = nil
    self._btnGoTo = nil
    self._scrollView = nil
    self._list = nil
    self._base.OnDestroy(self)
end

function DailyRewardsTaskItem:InitList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/Container_Rewards/ScrollViewList")
    self._list = list
    self._scrollView = scrollView
    self._scrollView:SetHorizontalMove(true)
    self._scrollView:SetVerticalMove(false)
    self._list:SetItemType(TaskRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, 100, 1)
end

function DailyRewardsTaskItem:UpdateDailyActivityData(id)
    if self._data == id then
        self:OnRefreshData(id)
    end
end

function DailyRewardsTaskItem:GetBtnClick()
    -- LoggerHelper.Log("Ash: GetBtnClick:" .. self._data)
    DailyActivityManager:CommitTask(self._data)
end

--override
function DailyRewardsTaskItem:OnRefreshData(data)
    self._data = data
    if data ~= nil then
        GameWorld.AddIcon(self._containerIcon, DailyTaskDataHelper:GetIcon(data), nil)
        self._textTitle.text = LanguageDataHelper.CreateContent(DailyTaskDataHelper:GetName(data))
        self._textDesc.text = LanguageDataHelper.CreateContent(DailyTaskDataHelper:GetDesc(data))
        self._textVitality.text = LanguageDataHelper.CreateContent(901) .. DailyTaskDataHelper:GetActivePoint(data)
        
        local dailyActivity = DailyActivityManager:GetDailyActivityById(data)
        local rewards = dailyActivity:GetReward()
        self._list:SetDataList(rewards)
        if #rewards < 3 then
            self._scrollView:SetScrollRectEnable(false)
        else
            self._scrollView:SetScrollRectEnable(true)
        end
        if dailyActivity:GetState() == public_config.DAILY_TASK_STATE_COMPLETED
            or dailyActivity:GetState() == public_config.DAILY_TASK_STATE_COMMITED then
            if dailyActivity:GetState() == public_config.DAILY_TASK_STATE_COMPLETED then
                self._btnGet:SetActive(true)
                self._btnGoTo:SetActive(false)
                self._imageFinished:SetActive(false)
            else
                self._btnGet:SetActive(false)
                self._btnGoTo:SetActive(false)
                self._imageFinished:SetActive(true)
            end
        else
            self._btnGet:SetActive(false)
            self._btnGoTo:SetActive(true)
            self._imageFinished:SetActive(false)
        end
        self._textProgress.text = dailyActivity:GetProgress()
    
    else
        self._textVitality.text = ""
        self._textProgress.text = ""
        self._textTitle.text = ""
        self._textDesc.text = ""
        self._list:SetDataList({})
    end
end
