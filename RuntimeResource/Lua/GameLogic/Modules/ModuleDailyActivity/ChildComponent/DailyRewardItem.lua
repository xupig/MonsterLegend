-- DailyRewardItem.lua
local DailyRewardItem = Class.DailyRewardItem(ClassTypes.BaseComponent)

DailyRewardItem.interface = GameConfig.ComponentsConfig.PointableComponent
DailyRewardItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.DailyRewardItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

local ActivityPointRewardDataHelper = GameDataHelper.ActivityPointRewardDataHelper
local RectTransform = UnityEngine.RectTransform
local TipsManager = GameManager.TipsManager
local DailyConfig = GameConfig.DailyConfig
local DailyActivityExManager = PlayerManager.DailyActivityExManager
local UIParticle = ClassTypes.UIParticle

--override
function DailyRewardItem:Awake()
    self._canGetImage = self:FindChildGO("Image_Can_Get")
    self._alreadyGetContainer = self:FindChildGO("Container_Already_Get")
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._icon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
    self._pointText = self:GetChildComponent("Image_Bg/Text_Num",'TextMeshWrapper')    
    -- self._icon:ActivateTipsById()
    self._onClick = function(pointerEventData) self:OnPointerUp(pointerEventData) end
    self._icon:SetPointerUpCB(self._onClick)

    self._fx_Container = self:FindChildGO("Container_Fx")
    self._fx_ui_30011 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30011")
    self:StopFx()
end

--override
function DailyRewardItem:OnDestroy() 
    self._canGetImage = nil
    self._alreadyGetContainer = nil
    self._itemContainer = nil
    self._icon = nil
    self._pointText = nil
    self._fx_ui_30011:Stop()
end

function DailyRewardItem:PlayFx()
    self._fx_ui_30011:Play(true, true)
end

function DailyRewardItem:StopFx()
    self._fx_ui_30011:Stop()
end

--override
function DailyRewardItem:OnPointerDown(pointerEventData)
end

--override
function DailyRewardItem:OnPointerUp(pointerEventData)
	-- if self._puFunc ~= nil then
    -- pointerEventData = Global.ChangeScreenClickPosition(pointerEventData)
    local status = self:GetStatus()
    if status == DailyConfig.None then
        TipsManager:ShowItemTipsById(self._itemId)
    elseif status == DailyConfig.CAN_GET then
        DailyActivityExManager:GetReward(self._id)
    elseif status == DailyConfig.ALREADY_GET then
    end
	-- end
end

function DailyRewardItem:SetPointerUpCB(func)
    self._puFunc = func
end

function DailyRewardItem:InitData(id, length)
    self._id = id
    local rewards = ActivityPointRewardDataHelper:GetReward(id)
    local max = ActivityPointRewardDataHelper:GetMaxPoint()
    local point = ActivityPointRewardDataHelper:GetCostPoint(id)
    local rewardsRecord = GameWorld.Player().daily_activity_point_reward
    local dailyPoints = GameWorld.Player().daily_activity_point
    self._itemId = rewards[1][1]

    self:RefreshStatus()

    self._icon:SetItem(rewards[1][1], rewards[1][2], 0, true)
    self._pointText.text = point
    self.gameObject:GetComponent(typeof(RectTransform)).transform.localPosition = Vector3.New(length * point/max, 0, 0)
end

function DailyRewardItem:RefreshStatus()
    local status = self:GetStatus()
    if status == DailyConfig.None then
        self._canGetImage:SetActive(false)
        self._alreadyGetContainer:SetActive(false)
        self:StopFx()
    elseif status == DailyConfig.CAN_GET then
        self._canGetImage:SetActive(true)
        self._alreadyGetContainer:SetActive(false)
        self:PlayFx()
    elseif status == DailyConfig.ALREADY_GET then
        self._alreadyGetContainer:SetActive(true)
        self._canGetImage:SetActive(false)
        self:StopFx()
    end
end

function DailyRewardItem:GetStatus()
    local point = ActivityPointRewardDataHelper:GetCostPoint(self._id)
    local rewardsRecord = GameWorld.Player().daily_activity_point_reward
    local dailyPoints = GameWorld.Player().daily_activity_point
    if rewardsRecord and rewardsRecord[self._id] then
        return DailyConfig.ALREADY_GET
    else
        if dailyPoints >= point then
            return DailyConfig.CAN_GET
        else
            return DailyConfig.None
        end
    end 
    return DailyConfig.None
end

