--幕间剧的ListItem
-- DailyActivityTagsItem.lua
local DailyActivityTagsItem = Class.DailyActivityTagsItem(ClassTypes.UIListItem)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

DailyActivityTagsItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
DailyActivityTagsItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.DailyActivityTagsItem"


function DailyActivityTagsItem:Awake()
    self._icon = self:FindChildGO("Container_Icon")
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
end


function DailyActivityTagsItem:OnDestroy()
    self._icon = nil
    self._textName = nil
end


--override
function DailyActivityTagsItem:OnRefreshData(data)
    local iconId = data[1]
    local nameId = data[2]

    GameWorld.AddIcon(self._icon, iconId)
    self._textName.text = LanguageDataHelper.CreateContent(nameId)
end
