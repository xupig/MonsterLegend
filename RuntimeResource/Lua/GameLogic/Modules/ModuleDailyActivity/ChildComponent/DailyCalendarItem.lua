-- DailyCalendarItem.lua
local DailyCalendarItem = Class.DailyCalendarItem(ClassTypes.UIListItem)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper

DailyCalendarItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
DailyCalendarItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.DailyCalendarItem"


function DailyCalendarItem:Awake()
    self._nameList = {}
    self._selectImageList = {}
    self._oddImageList = {}
    self._evenImageList = {}
    self._timeText = self:GetChildComponent("Container_Open/Text_Time", "TextMeshWrapper")
    for i=1,7 do
        table.insert(self._nameList, self:GetChildComponent("Container_" .. i .. "/Text_Name", "TextMeshWrapper"))
        table.insert(self._selectImageList, self:FindChildGO("Container_" .. i .. "/Image_Select"))
        table.insert(self._oddImageList, self:FindChildGO("Container_" .. i .. "/Image_Bg_Odd"))
        table.insert(self._evenImageList, self:FindChildGO("Container_" .. i .. "/Image_Bg_Even"))
    end
end


function DailyCalendarItem:OnDestroy()
end


--override
function DailyCalendarItem:OnRefreshData(data)
    local index = self:GetIndex()
    local wDay = DateTimeUtil.GetWday()
    for i=1,7 do
        self._oddImageList[i]:SetActive(not ((index + 1) % 2 == 0))
        self._evenImageList[i]:SetActive((index + 1) % 2 == 0)
        self._selectImageList[i]:SetActive(i == wDay)
        if data[i] then
            self._nameList[i].text = LanguageDataHelper.CreateContent(ActivityDailyDataHelper:GetName(data[i]))
        end
    end
    self._timeText.text = data[10]
end
