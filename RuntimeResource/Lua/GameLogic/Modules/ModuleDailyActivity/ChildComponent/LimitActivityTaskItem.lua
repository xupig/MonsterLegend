--幕间剧的ListItem
-- LimitActivityTaskItem.lua
local LimitActivityTaskItem = Class.LimitActivityTaskItem(ClassTypes.UIListItem)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ActivityDataHelper = GameDataHelper.ActivityDataHelper
local OpenTimeDataHelper = GameDataHelper.OpenTimeDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local DateTimeUtil = GameUtil.DateTimeUtil
local action_config = require("ServerConfig/action_config")
local UIListItem = ClassTypes.UIListItem
local GUIManager = GameManager.GUIManager
local MenuConfig = GameConfig.MenuConfig
local DailyActivityConfig = GameConfig.DailyActivityConfig
local DailyActivityManager = PlayerManager.DailyActivityManager

LimitActivityTaskItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
LimitActivityTaskItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.LimitActivityTaskItem"


function LimitActivityTaskItem:Awake()
    self._base.Awake(self)
    self._textTitle = self:GetChildComponent("Container_Content/Text_Title", "TextMeshWrapper")
    self._textTimes = self:GetChildComponent("Container_Content/Text_Times", "TextMeshWrapper")
    self._textLimit = self:GetChildComponent("Container_Content/Text_Limit", "TextMeshWrapper")
    self._textStatus = self:GetChildComponent("Container_Content/Container_Status/Text", "TextMeshWrapper")
    self._containerIcon = self:FindChildGO("Container_Content/Container_Icon")
    self._selectImageGo = self:FindChildGO("Container_Content/Image_Select")
    self._statusContainer = self:FindChildGO("Container_Content/Container_Status")
    self._lockImage = self:FindChildGO("Container_Content/Image_Lock")

    self._btnJoin = self:FindChildGO("Container_Content/Button_Join")
    self._csBH:AddClick(self._btnJoin, function()self:JoinBtnClick() end)
    
    self._textTimes.text = ""
    self._textLimit.text = ""
end

function LimitActivityTaskItem:OnDestroy()
    self._textTitle = nil
    self._textTimes = nil
    self._textLimit = nil
    self._base.OnDestroy(self)
end

function LimitActivityTaskItem:JoinBtnClick()
    local follow = ActivityDataHelper:GetFollow(self._data)
    if follow ~= nil and follow[MenuConfig.SHOW_PANEL] ~= nil then
        for i, v in ipairs(follow[MenuConfig.SHOW_PANEL]) do
            GUIManager.ShowPanel(v)
        end
    end
    self:GetListObj():OnItemClicked(self)
end

function LimitActivityTaskItem:SetSelect(flag)
    self._selectImageGo:SetActive(flag)
end

--override
function LimitActivityTaskItem:OnRefreshData(data)
    local id = data[1]
    local time = data[2]
    self._data = id
    -- LoggerHelper.Log(time)
    
    local timeId = ActivityDataHelper:GetTime(id)
    local openLevel = ActivityDataHelper:GetLevel(id)

    if id ~= nil then
        if GameWorld.Player() and GameWorld.Player().level < openLevel then
            --未达到开启条件
            GameWorld.AddIcon(self._containerIcon, ActivityDataHelper:GetIcon(id), 0, false, 0)
            self._lockImage:SetActive(true)
            self._textLimit.gameObject:SetActive(true)
            self._textLimit.text = openLevel .. "级开启"
            self._statusContainer:SetActive(false)
            self._btnJoin:SetActive(false)
        else            
            GameWorld.AddIcon(self._containerIcon, ActivityDataHelper:GetIcon(id))
            self._lockImage:SetActive(false)
            self._textLimit.gameObject:SetActive(false)
            if time == nil then
                self._textTimes.text = ""
                self._statusContainer:SetActive(false)
                self._btnJoin:SetActive(false)
            else
                self._textTimes.text = time:GetDescription()
                local curTime
                if time:GetDurationTime() < DateTimeUtil.OneHourTime then
                    curtime = DateTimeUtil.Now(time:GetDurationTime())
                else
                    curTime = DateTimeUtil.Now(DateTimeUtil.OneHourTime)
                end
                
                local status = time:GetActivityStatus(curTime)
                if status == DailyActivityConfig.OPEN then
                    self._statusContainer:SetActive(true)
                    self._btnJoin:SetActive(true)
                    self._textStatus.text = "开启中"
                    self._textTimes.gameObject:SetActive(false)
                elseif status == DailyActivityConfig.OPEN_SOON then
                    self._statusContainer:SetActive(true)
                    self._btnJoin:SetActive(false)
                    self._textStatus.text = "即将开启"
                elseif status == DailyActivityConfig.NOT_OPEN then
                    self._statusContainer:SetActive(false)
                    self._btnJoin:SetActive(false)
                end                
            end            
        end
        
        self._textTitle.text = LanguageDataHelper.CreateContent(ActivityDataHelper:GetName(id))        
    else
        self._textTitle.text = ""
        self._textTimes.text = ""
    end
end