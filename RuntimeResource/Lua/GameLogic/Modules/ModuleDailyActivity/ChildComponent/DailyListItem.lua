-- DailyListItem.lua
local DailyListItem = Class.DailyListItem(ClassTypes.UIListItem)

DailyListItem.interface = GameConfig.ComponentsConfig.Component
DailyListItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.DailyListItem"

local AttriDataHelper = GameDataHelper.AttriDataHelper
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CommonTimeListConfig = GameConfig.CommonTimeListConfig
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local public_config = GameWorld.public_config
local XImageFlowLight = GameMain.XImageFlowLight
local DailyActivityConfig = GameConfig.DailyActivityConfig

require "PlayerManager/PlayerData/CommonTimeListData"
local CommonTimeListData = ClassTypes.CommonTimeListData

--override
function DailyListItem:Awake()
    self._frameContainer = self:FindChildGO("Container_Frame")
    self._icon = self:FindChildGO("Container_Icon")
    self._lockImage = self:FindChildGO("Image_Lock")
    self._completeImage = self:FindChildGO("Container_Status_Images/Image_Complete")
    self._startImage = self:FindChildGO("Container_Status_Images/Image_Start")
    self._startSoonImage = self:FindChildGO("Container_Status_Images/Image_Start_Soon")
    
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper')    
    self._timesText = self:GetChildComponent("Text_Times",'TextMeshWrapper')    
    self._pointsText = self:GetChildComponent("Text_Points",'TextMeshWrapper')    
    self._openConditionText = self:GetChildComponent("Text_Open_Condition",'TextMeshWrapper')    
    self._notOpenText = self:GetChildComponent("Text_Not_Open",'TextMeshWrapper')    
    self._notOpenText.gameObject:SetActive(false)

    self._tagsContainer = self:FindChildGO("Container_Tags")
    self._tag1Container = self:FindChildGO("Container_Tags/Container_Tag_1")
    self._tag2Container = self:FindChildGO("Container_Tags/Container_Tag_2")

    self._goButtonFlowLight = self:AddChildComponent("Button_Go", XImageFlowLight)
    self._goButton = self:FindChildGO("Button_Go")
    self._csBH:AddClick(self._goButton, function() self:OnGoButtonClick() end)
end

--override
function DailyListItem:OnDestroy()
end

function DailyListItem:OnRefreshData(id)
    self._id = id
    local timesList = GameWorld.Player().daily_activity_cur_counts
    local times = ActivityDailyDataHelper:GetTimes(id) or 0
    local openLevel = ActivityDailyDataHelper:GetOpenLevel(id)
    local openWorldLevel = ActivityDailyDataHelper:GetOpenWorldLevel(id)
    local nowLevel = GameWorld.Player().level
    local nowWorldLevel = GameWorld.Player().world_level
    local rewardPoints = ActivityDailyDataHelper:GetRewardPoint(id) or 0
    local completeTimes = timesList[id] or 0
    local isOpen = true --是否已经开启了该功能
    local enter = self:CanEnter()
    local rewardTagsData = ActivityDailyDataHelper:GetRewardShowList(id)

    if not table.isEmpty(rewardTagsData) then
        self._tag1Container:SetActive(true)
        GameWorld.AddIcon(self._tag1Container, rewardTagsData[1])
        if rewardTagsData[2] then
            self._tag2Container:SetActive(true)
            GameWorld.AddIcon(self._tag2Container, rewardTagsData[2])
        else
            self._tag2Container:SetActive(false)
        end
    else
        self._tag1Container:SetActive(false)
        self._tag2Container:SetActive(false)
    end

    -- GameWorld.AddIcon(self._icon, ActivityDailyDataHelper:GetIcon(id),0,true)
    self._nameText.text = LanguageDataHelper.CreateContent(ActivityDailyDataHelper:GetName(id))
    self._timesText.text = LanguageDataHelper.CreateContent(52024, completeTimes .. '/' .. times)
    
    self._pointsText.text = LanguageDataHelper.CreateContent(52025, (completeTimes * rewardPoints) .. '/' .. (times * rewardPoints))

    if nowLevel >= openLevel and nowWorldLevel >= openWorldLevel then
        self._lockImage:SetActive(false)
        GameWorld.AddIcon(self._icon, ActivityDailyDataHelper:GetIcon(id),0,true,1)
        GameWorld.AddIcon(self._frameContainer, 4105,0,false,1)
        self._openConditionText.gameObject:SetActive(false)
        if times == 0 or times == "" then
            self._timesText.gameObject:SetActive(false)
            self._pointsText.gameObject:SetActive(false)
            -- self._goButton:SetActive(true)
            self:SetGoButtonActive(true)
            self._completeImage:SetActive(false)
        else
            self._timesText.gameObject:SetActive(true)
            if rewardPoints == 0 then
                self._pointsText.gameObject:SetActive(false)
            else
                self._pointsText.gameObject:SetActive(true)                        
            end
            if completeTimes >= times then
                self._completeImage:SetActive(true)
                -- self._goButton:SetActive(false)
                self:SetGoButtonActive(false)
            else
                self._completeImage:SetActive(false)
                -- self._goButton:SetActive(true)
                self:SetGoButtonActive(true)
            end
        end
    else
        isOpen = false
        if times == 0 or times == "" then
            self._timesText.gameObject:SetActive(false)
            self._pointsText.gameObject:SetActive(false)
        end
        self._lockImage:SetActive(true)
        GameWorld.AddIcon(self._icon, ActivityDailyDataHelper:GetIcon(id),0,true,0)
        GameWorld.AddIcon(self._frameContainer, 4105,0,false,0)
        -- self._goButton:SetActive(false)
        self:SetGoButtonActive(false)
        self._completeImage:SetActive(false)
        self._openConditionText.gameObject:SetActive(true)
        if openWorldLevel > 1 then
            self._openConditionText.text = LanguageDataHelper.CreateContent(53472, tostring(openWorldLevel))
        else
            self._openConditionText.text = LanguageDataHelper.CreateContent(52026, tostring(openLevel))
        end
    end

    if ActivityDailyDataHelper:GetType(id) == 1 then--每日必做
        self._startImage:SetActive(false)
        self._startSoonImage:SetActive(false)
    elseif ActivityDailyDataHelper:GetType(id) == 2 then--限时活动
        self._completeImage:SetActive(false)
        if not isOpen then return end
        local commonTimeData = CommonTimeListData(ActivityDailyDataHelper:GetDayLimit(id), ActivityDailyDataHelper:GetTimeLimit(id))
        local status = commonTimeData:GetOpenStatus()
        if status == CommonTimeListConfig.OPENNING then
            self._startImage:SetActive(true)
            self._startSoonImage:SetActive(false)
            -- self._goButton:SetActive(true)
            if enter then
                self:SetGoButtonActive(true)
            else
                self:SetGoButtonActive(false)
            end
        elseif status == CommonTimeListConfig.WAIT_OPEN then
            self._startImage:SetActive(false)
            local openTime = commonTimeData:GetLatestOpenTime()
            local nowMins = DateTimeUtil.GetTodayMins()
            self._openConditionText.gameObject:SetActive(true)
            if openTime then
                self._startSoonImage:SetActive((openTime - nowMins) < GlobalParamsHelper.GetParamValue(561) /60)
            end
            self._openConditionText.text = commonTimeData:GetDescText(status)
            -- self._goButton:SetActive(false)
            self:SetGoButtonActive(false)
        elseif status == CommonTimeListConfig.OVER_TIME or status == CommonTimeListConfig.NOT_OPEN_TODAY then
            self._startImage:SetActive(false)
            self._startSoonImage:SetActive(false)
            -- self._goButton:SetActive(false)
            self:SetGoButtonActive(false)
            self._openConditionText.gameObject:SetActive(true)
            self._openConditionText.text = commonTimeData:GetDescText(status)
        end
    end


end

function DailyListItem:OnGoButtonClick()
    GameManager.GUIManager.ClosePanel(PanelsConfig.DailyActivityEx)
    local follow = ActivityDailyDataHelper:GetFollow(self._id)
    if self._id == public_config.ACTIVITY_ID_CIRCLE_TASK then
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_daily_goto_circle_task_ui_button")
    end
    -- local funcParams = ActivityDailyDataHelper:GetFunctionParams(self._id)
    -- if funcParams and funcParams[DailyActivityConfig.IS_OPEN_IN_OPENTIME] then
    --     local func = loadstring(funcParams[DailyActivityConfig.IS_OPEN_IN_OPENTIME])
    --     if func() then
    --         EventDispatcher:TriggerEvent(GameEvents.ON_DAILY_CLICK, follow)
    --     end
    -- end
    EventDispatcher:TriggerEvent(GameEvents.ON_DAILY_CLICK, follow)
end

function DailyListItem:SetGoButtonActive(flag)
    self._goButton:SetActive(flag)    
    self._goButtonFlowLight.enabled = flag
end

--在活动时间内，是否能进去
function DailyListItem:CanEnter()
    local funcParams = ActivityDailyDataHelper:GetFunctionParams(self._id)
    if funcParams and funcParams[DailyActivityConfig.IS_OPEN_IN_OPENTIME] then
        local func = loadstring(funcParams[DailyActivityConfig.IS_OPEN_IN_OPENTIME])
        return func()
    end
    return false
end