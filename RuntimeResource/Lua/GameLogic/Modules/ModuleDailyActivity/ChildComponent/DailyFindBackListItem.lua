-- DailyFindBackListItem.lua
local DailyFindBackListItem = Class.DailyFindBackListItem(ClassTypes.UIListItem)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local UIList = ClassTypes.UIList
local DailyConfig = GameConfig.DailyConfig
local XImageFlowLight = GameMain.XImageFlowLight

DailyFindBackListItem.interface = GameConfig.ComponentsConfig.PointerDownComponent
DailyFindBackListItem.classPath = "Modules.ModuleDailyActivity.ChildComponent.DailyFindBackListItem"

require"Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem

function DailyFindBackListItem:Awake()
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper')
    self._descText = self:GetChildComponent("Text_Desc",'TextMeshWrapper')

    self._iconContainer = self:FindChildGO("Container_Price/Container_Icon")
    self._moneyNumText = self:GetChildComponent("Container_Price/Text_Num",'TextMeshWrapper')

    self._alreadyImage = self:FindChildGO("Image_Already_Find")

    self._goButtonFlowLight = self:AddChildComponent("Button_Find", XImageFlowLight)
    self._findButton = self:FindChildGO("Button_Find")
    self._csBH:AddClick(self._findButton, function() self:OnFindButtonClick() end)
    
    self:InitRewardList()
end

function DailyFindBackListItem:InitRewardList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listlView = scrollView 
    -- self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    -- scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(RewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
end

function DailyFindBackListItem:OnDestroy()
    self._nameText = nil
    self._descText = nil
    self._iconContainer = nil
    self._moneyNumText = nil
    self._alreadyImage = nil
    self._findButton = nil
    self._goButtonFlowLight = nil
end

--override
function DailyFindBackListItem:OnRefreshData(data)
    self._data = data
    local type = data:GetType()
    local exTimes = data:GetExTimes()
    local commonTimes = data:GetCommonTimes()
    local rewardData

    self._nameText.text = data:GetName()
    self._descText.text = data:GetDesc()

    

    if type == DailyConfig.REWARD_BACK_TYPE_EX then--钻石找回
        local num, id = data:GetExBaseCost()
        GameWorld.AddIcon(self._iconContainer, id)
        self._moneyNumText.text = num
        rewardData = data:GetExRewardData()
        if exTimes == 0 and commonTimes == 0 then
            self:SetGoButtonActive(false)
            self._alreadyImage:SetActive(true)
        else
            self:SetGoButtonActive(true)
            self._alreadyImage:SetActive(false)
        end
    elseif DailyConfig.REWARD_BACK_TYPE_COMMON then--金币找回
        local num, id = data:GetCommonCost()
        GameWorld.AddIcon(self._iconContainer, id)
        self._moneyNumText.text = num
        rewardData = data:GetCommonRewardData()
        if commonTimes == 0 then
            self:SetGoButtonActive(false)
            self._alreadyImage:SetActive(true)
        else
            self:SetGoButtonActive(true)
            self._alreadyImage:SetActive(false)
        end
    end
    if #rewardData >= 4 then
        self._listlView:SetHorizontalMove(true)
    else
        self._listlView:SetHorizontalMove(false)
    end
    self._list:SetDataList(rewardData)
end

function DailyFindBackListItem:OnFindButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_DAILY_FIND_BACK_CLICK, self._data)
end

function DailyFindBackListItem:SetGoButtonActive(flag)
    self._findButton:SetActive(flag)    
    self._goButtonFlowLight.enabled = flag
end
