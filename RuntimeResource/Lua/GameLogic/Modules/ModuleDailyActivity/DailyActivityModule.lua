DailyActivityModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function DailyActivityModule.Init()
    GUIManager.AddPanel(PanelsConfig.DailyActivity, "Panel_DailyActivity", GUILayer.LayerUIPanel, "Modules.ModuleDailyActivity.DailyActivityPanel", true, false,nil,true)
    GUIManager.AddPanel(PanelsConfig.DailyActivityEx, "Panel_DailyActivityEx", GUILayer.LayerUIPanel, "Modules.ModuleDailyActivity.DailyActivityExPanel", true, false,nil,true)
    GUIManager.AddPanel(PanelsConfig.DailyTips,"Panel_DailyTips",GUILayer.LayerUIPanel,"Modules.ModuleDailyActivity.DailyTipsPanel",true,false,nil,true)
    GUIManager.AddPanel(PanelsConfig.SkyDrop, "Panel_SkyDrop", GUILayer.LayerUIFloat, "Modules.ModuleDailyActivity.SkyDropPanel", true, PanelsConfig.EXTEND_TO_FIT)  
end

return DailyActivityModule