--DailyTipsPanel.lua
local ClassTypes = ClassTypes
local DailyTipsPanel = Class.DailyTipsPanel(ClassTypes.BasePanel)
require  "Modules.ModuleDailyActivity.ChildComponent.RewardTipsItem"

local RewardTipsItem = ClassTypes.RewardTipsItem

local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local public_config = GameWorld.public_config
local UIToggleGroup = ClassTypes.UIToggleGroup
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local DailyActivityExManager = PlayerManager.DailyActivityExManager
local DailyConfig = GameConfig.DailyConfig

local UIList = ClassTypes.UIList

function DailyTipsPanel:Awake()
    self:InitView()
    self:InitScollView()
end

function DailyTipsPanel:InitView()
    self._buttonBGGO = self:FindChildGO("Button_Bg")
    self._csBH:AddClick(self._buttonBGGO,function() self:OnBGButtonClick() end)

    self._actionListTrans = self:FindChildGO("Container_Tips_Right").transform

    self._frameContainer = self:FindChildGO("Container_Tips_Right/Container_Frame")
    self._rightTips = self:FindChildGO("Container_Tips_Right")
    self._txtName = self:GetChildComponent("Container_Tips_Right/Text_Name", 'TextMeshWrapper')
    self._txtRightTime = self:GetChildComponent("Container_Tips_Right/Text_TypeName", 'TextMeshWrapper')
    self._icon = self:FindChildGO("Container_Tips_Right/Container_Icon")

    self._txtTimeActivity = self:GetChildComponent("Container_Tips_Right/Text_Time", 'TextMeshWrapper')
    self._txtLimit = self:GetChildComponent("Container_Tips_Right/Text_Limit", 'TextMeshWrapper')
    self._txtDesc = self:GetChildComponent("Container_Tips_Right/Text_Desc", 'TextMeshWrapper')

    self._txtValueActivity = self:GetChildComponent("Container_Tips_Right/Text_Activity_Value", 'TextMeshWrapper')
    self._txtReward = self:GetChildComponent("Container_Tips_Right/Text_Reward", 'TextMeshWrapper')



end

function DailyTipsPanel:OnBGButtonClick()
    GUIManager.ClosePanel(PanelsConfig.DailyTips)
end
function DailyTipsPanel:OnDestroy()
end

function DailyTipsPanel:OnShow(data)
  if data then
    local id = data._id
    local index = data._index
    local timesList = GameWorld.Player().daily_activity_cur_counts
    local times = ActivityDailyDataHelper:GetTimes(id) or 0
    local completeTimes = timesList[id] or 0
  
    if index%2 == 0 then
        self._rightTips:SetActive(true)
        self:InitRightData(id,times,completeTimes)
        self._actionListTrans.anchoredPosition = Vector2.New(DailyConfig.TipRightPos.x,DailyConfig.TipRightPos.y)
    else
        self._rightTips:SetActive(true)
        self:InitRightData(id,times,completeTimes)
        self._actionListTrans.anchoredPosition = Vector2.New(DailyConfig.TipLeftPos.x,DailyConfig.TipLeftPos.y)
    end
  end
end

function DailyTipsPanel:OnClose()
    
end

function DailyTipsPanel:InitRightData(id,times,completeTimes)
    GameWorld.AddIcon(self._icon, ActivityDailyDataHelper:GetIcon(id),0,true)
    GameWorld.AddIcon(self._frameContainer, 4105,0,false,1)
    self._txtName.text = LanguageDataHelper.CreateContent(ActivityDailyDataHelper:GetName(id))
    self._txtRightTime.text = LanguageDataHelper.CreateContent(52024, completeTimes .. '/' .. times)

    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = ActivityDailyDataHelper:GetOpenLevel(id)
    self._txtLimit.text = LanguageDataHelper.CreateContent(ActivityDailyDataHelper:GetDescLevel(id),argsTable)

    local argsTableLimit = LanguageDataHelper.GetArgsTable()
    argsTableLimit["0"] = LanguageDataHelper.CreateContent(ActivityDailyDataHelper:GetDescTipTime(id))
    self._txtTimeActivity.text = LanguageDataHelper.CreateContent(52038,argsTableLimit)

    local argsTableDesc = LanguageDataHelper.GetArgsTable()
    argsTableDesc["0"] = LanguageDataHelper.CreateContent(ActivityDailyDataHelper:GetDesc(id))
    self._txtDesc.text = LanguageDataHelper.CreateContent(52040,argsTableDesc)

    local argsTableValue = LanguageDataHelper.GetArgsTable()
    argsTableValue["0"] = ActivityDailyDataHelper:GetRewardPoint(id)
    self._txtValueActivity.text = LanguageDataHelper.CreateContent(52041,argsTableValue)

    local RewardList = ActivityDailyDataHelper:GetRewardPreview(id)
    self._list:SetDataList(RewardList)
    --LoggerHelper.Error("ActivityDailyDataHelper:GetRewardPoint(id): " .. ActivityDailyDataHelper:GetRewardPoint(id))
   
end


--初始化标签
function DailyTipsPanel:InitScollView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Tips_Right/ScrollViewList")
    self._list = list
    self._listView = scrollView
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(RewardTipsItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)

end



--tab点击处理
function DailyTipsPanel:OnToggleGroupClick(index)

end

function DailyTipsPanel:ShowDailyActivity()

end

function DailyTipsPanel:ShowDailyRewards()

end

function DailyTipsPanel:SetViewActive(view, isActive)

end


