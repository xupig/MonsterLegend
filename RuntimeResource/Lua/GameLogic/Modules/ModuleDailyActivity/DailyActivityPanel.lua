--DailyActivityPanel.lua
local ClassTypes = ClassTypes
local DailyActivityPanel = Class.DailyActivityPanel(ClassTypes.BasePanel)
require "Modules.ModuleDailyActivity.ChildView.DailyActivityView"
require "Modules.ModuleDailyActivity.ChildView.DailyRewardsView"

local DailyActivityView = ClassTypes.DailyActivityView
local DailyRewardsView = ClassTypes.DailyRewardsView

local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local public_config = GameWorld.public_config
local UIToggleGroup = ClassTypes.UIToggleGroup
local PanelBGType = GameConfig.PanelsConfig.PanelBGType

function DailyActivityPanel:Awake()
    self:InitViews()
    self:InitToggleGroup()
end

function DailyActivityPanel:InitViews()
    self._btnClose = self:FindChildGO("Container_Buttons/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:CloseBtnClick() end)
end

function DailyActivityPanel:OnDestroy()
end

function DailyActivityPanel:OnShow()
    self._toggleGroup:SetSelectIndex(0)
    self:OnToggleGroupClick(0)
end

function DailyActivityPanel:OnClose()
end

--初始化标签
function DailyActivityPanel:InitToggleGroup()
    self._toggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "Container_Tab")
    self._toggleGroup:SetSelectIndex(0)
    self._toggleGroup:SetOnSelectedIndexChangedCB(function(index)self:OnToggleGroupClick(index) end)
end

--tab点击处理
function DailyActivityPanel:OnToggleGroupClick(index)
    self._currentToggleIndex = index
    if index == 0 then
        self:ShowDailyActivity()
        -- self._dailyActivityView:ShowTaskInfo()
    elseif index == 1 then
        self:ShowDailyRewards()
        -- self._dailyRewardsView:ShowTaskInfo()
    -- else
    --     self:ShowReview()
    end
end

function DailyActivityPanel:ShowDailyActivity()
    if self._dailyActivityView == nil then
        self._dailyActivityView = self:AddChildLuaUIComponent("Container_Daily", DailyActivityView)
    end
    self:SetViewActive(self._dailyActivityView, true)
    self:SetViewActive(self._dailyRewardsView, false)
end

function DailyActivityPanel:ShowDailyRewards()
    if self._dailyRewardsView == nil then
        self._dailyRewardsView = self:AddChildLuaUIComponent("Container_Reward", DailyRewardsView)
    end
    self:SetViewActive(self._dailyActivityView, false)
    self:SetViewActive(self._dailyRewardsView, true)
end

function DailyActivityPanel:SetViewActive(view, isActive)
    if view ~= nil then
        view.gameObject:SetActive(isActive)
    end
end
function DailyActivityPanel:CloseBtnClick()
    GUIManager.ClosePanel(PanelsConfig.DailyActivity)
end

-- -- 0 没有  1 暗红底板  2 蓝色底板  3 遮罩底板
function DailyActivityPanel:CommonBGType()
    return PanelBGType.BLUE
end
