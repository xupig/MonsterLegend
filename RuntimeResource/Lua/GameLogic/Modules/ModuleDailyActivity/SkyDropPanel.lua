--SkyDropPanel.lua
local SkyDropPanel = Class.SkyDropPanel(ClassTypes.BasePanel)
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local DateTimeUtil = GameUtil.DateTimeUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function SkyDropPanel:Awake()
	self.disableCameraCulling = true
    self:InitCallback()
    self:InitViews()
end

function SkyDropPanel:InitCallback()
	 self._timerCb = function ()
        self:UpdateTimer()
    end
end

function SkyDropPanel:InitViews()
	self._txtCD = self:GetChildComponent("Text_CD","TextMeshWrapper")
end

function SkyDropPanel:OnShow(data)
	--local endTime = data[1]

	self._timeleft = GlobalParamsHelper.GetParamValue(939)
	self._timerId = TimerHeap:AddSecTimer(0, 1, 0, self._timerCb)
end

function SkyDropPanel:UpdateTimer()
    if self._timeleft <= 0 then
        TimerHeap:DelTimer(self._timerId)
        self._timerId = nil
        GUIManager.ClosePanel(PanelsConfig.SkyDrop)
    end
    self._timeleft = self._timeleft - 1
    self._txtCD.text = DateTimeUtil:FormatFullTime(self._timeleft)
end

function SkyDropPanel:OnClose()
	
end