-- DailyPointRewardView.lua
local DailyPointRewardView = Class.DailyPointRewardView(ClassTypes.BaseComponent)

DailyPointRewardView.interface = GameConfig.ComponentsConfig.PointableComponent
DailyPointRewardView.classPath = "Modules.ModuleDailyActivity.ChildView.DailyPointRewardView"
require "Modules.ModuleDailyActivity.ChildView.PointRewardTipView"
local PointRewardTipView = ClassTypes.PointRewardTipView

local public_config = GameWorld.public_config
local DailyActivityManager = PlayerManager.DailyActivityManager
local UIComponentUtil = GameUtil.UIComponentUtil
local DailyActivityConfig = GameConfig.DailyActivityConfig

function DailyPointRewardView:Awake()
    self._containerIcon = self:FindChildGO("Container_Icon")
    self._containerEnable = self:FindChildGO("Container_Enable")
    self._imageAnchorDisable = self:FindChildGO("Image_Anchor_Disable")
    self._imageEnable = self:FindChildGO("Container_Enable/Image_Anchor_Enable")
    self._imageGet = self:FindChildGO("Container_Enable/Image_Item_Get")
    self._textVitalityValue = self:GetChildComponent("Text_Value", "TextMeshWrapper")
    -- self._onIconClick = function()self:IconClick() end
    -- self._csBH:AddClick(self._containerIcon, self._onIconClick)
    self._containerTip = self:FindChildGO("Container_Tip")
    self._viewTip = UIComponentUtil.AddLuaUIComponent(self._containerTip, PointRewardTipView)
end

function DailyPointRewardView:OnEnable()
end

function DailyPointRewardView:OnDisable()
end

function DailyPointRewardView:OnPointerDown(pointerEventData)
    self._containerTip:SetActive(true)
end

function DailyPointRewardView:OnPointerUp(pointerEventData)
    self._containerTip:SetActive(false)
    self:IconClick()
end

function DailyPointRewardView:OnDestroy()
    self._containerIcon = nil
    self._containerEnable = nil
    self._imageAnchorDisable = nil
    self._imageEnable = nil
    self._imageGet = nil
    self._textVitalityValue = nil
    self._containerTip = nil
    self._viewTip = nil
end

function DailyPointRewardView:IconClick()
    --LoggerHelper.Log("Ash: IconClick: " .. self._id)
    if self._callback ~= nil then
        self._callback(self._id)
    end
end

function DailyPointRewardView:SetData(id, iconId, value, callback)
    self._containerTip:SetActive(true)
    self._id = id
    self._callback = callback
    self._textVitalityValue.text = value
    self._viewTip:SetData(id)
    GameWorld.AddIcon(self._containerIcon, iconId, nil, true)
    self._containerTip:SetActive(false)
end

function DailyPointRewardView:SetState(status)
    -- if enable then
    --     self._containerEnable:SetActive(true)
    --     self._imageAnchorDisable:SetActive(false)
    -- else
    --     self._containerEnable:SetActive(false)
    --     self._imageAnchorDisable:SetActive(true)
    -- end
    if status == DailyActivityConfig.REWARD_NONE then--未达到领取条件
        self._imageAnchorDisable:SetActive(true)
        self._imageEnable:SetActive(false)
        self._imageGet:SetActive(false)
    elseif status == DailyActivityConfig.REWARD_CAN_GET then--可以领取奖励但未领取
        self._imageAnchorDisable:SetActive(false)
        self._imageEnable:SetActive(true)
        self._imageGet:SetActive(false)
    elseif status == DailyActivityConfig.REWARD_GET then--奖励已经领取
        self._imageAnchorDisable:SetActive(false)
        self._imageEnable:SetActive(true)
        self._imageGet:SetActive(true)
    end
end
