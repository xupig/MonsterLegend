-- PointRewardTipView.lua
local PointRewardTipView = Class.PointRewardTipView(ClassTypes.BaseComponent)

PointRewardTipView.interface = GameConfig.ComponentsConfig.Component
PointRewardTipView.classPath = "Modules.ModuleDailyActivity.ChildView.PointRewardTipView"
require "Modules.ModuleDailyActivity.ChildView.RewardItemView"
local RewardItemView = ClassTypes.RewardItemView

local public_config = GameWorld.public_config
local DailyActivityManager = PlayerManager.DailyActivityManager
local UIComponentUtil = GameUtil.UIComponentUtil

function PointRewardTipView:Awake()
    local containerPointReward = self:FindChildGO("Container_Rewards/Container_Reward")
    containerPointReward:SetActive(false)
    self._itemPool = {}
    self._items = {}
end

function PointRewardTipView:OnDestroy()
    self._itemPool = nil
    self._items = nil
end

function PointRewardTipView:SetData(id)
    local list = DailyActivityManager:GetPointRewardsList(id)
    self:RemoveAll()
    for i, v in ipairs(list) do
        self:AddItem(i, v)
    end
end

-- 积分奖励道具对象池
function PointRewardTipView:CreateItem()
    local itemPool = self._itemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Rewards/Container_Reward", "Container_Rewards")
    local item = UIComponentUtil.AddLuaUIComponent(go, RewardItemView)
    return item
end

function PointRewardTipView:ReleaseItem(item)
    local itemPool = self._itemPool
    table.insert(itemPool, item)
end

function PointRewardTipView:AddItem(id, rewardData)
    local item = self:CreateItem()
    item.gameObject:SetActive(true)
    self._items[id] = item
    item:SetData(rewardData)
    return item
end

function PointRewardTipView:RemoveAll()
    for k, item in pairs(self._items) do
        item.gameObject:SetActive(false)
        self:ReleaseItem(item)
    end
    self._items = {}
end
