-- DailyCalendarView.lua
local DailyCalendarView = Class.DailyCalendarView(ClassTypes.BaseComponent)

DailyCalendarView.interface = GameConfig.ComponentsConfig.Component
DailyCalendarView.classPath = "Modules.ModuleDailyActivity.ChildView.DailyCalendarView"

require "Modules.ModuleDailyActivity.ChildComponent.DailyCalendarItem"
local DailyCalendarItem = ClassTypes.DailyCalendarItem

local ActivityDataDataHelper = GameDataHelper.ActivityDataDataHelper
local UIList = ClassTypes.UIList

function DailyCalendarView:Awake()
    self:InitViews()
    self:InitCallBack()
end

function DailyCalendarView:InitViews()
    self._initCalendar = false

    self._calendarCloseButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._calendarCloseButton, function() self:OnCalendarCloseButtonClick() end)

    self._calendarShade = self:FindChildGO("Image_Shade")
    self._csBH:AddClick(self._calendarShade, function() self:OnCalendarCloseButtonClick() end)
    
    self:InitCalendarView()
end

function DailyCalendarView:InitCalendarView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/ScrollViewList")
	self._calendarList = list
	self._calendarListlView = scrollView 
    self._calendarListlView:SetHorizontalMove(false)
    self._calendarListlView:SetVerticalMove(true)
    self._calendarList:SetItemType(DailyCalendarItem)
    self._calendarList:SetPadding(0, 0, 0, 0)
    self._calendarList:SetGap(0, 0)
    self._calendarList:SetDirection(UIList.DirectionTopToDown, 1, -1)
end


function DailyCalendarView:OnEnable()
    self:AddEventListeners()
    self:InitCalendarData()
end

function DailyCalendarView:OnDisable()
    self:RemoveEventListeners()
end

function DailyCalendarView:InitCallBack()

end

function DailyCalendarView:AddEventListeners()  
end

function DailyCalendarView:RemoveEventListeners() 
end

function DailyCalendarView:OnCalendarCloseButtonClick()
    self:SetActive(false)
end

function DailyCalendarView:InitCalendarData()
    if self._initCalendar then return end
    self._calendarListData =  ActivityDataDataHelper:GetCalendarData()  
    self._calendarList:SetDataList(self._calendarListData)
    self._initCalendar = true
end