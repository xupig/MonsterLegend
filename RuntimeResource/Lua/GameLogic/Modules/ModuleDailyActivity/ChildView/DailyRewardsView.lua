-- DailyRewardsView.lua
local DailyRewardsView = Class.DailyRewardsView(ClassTypes.BaseComponent)

DailyRewardsView.interface = GameConfig.ComponentsConfig.Component
DailyRewardsView.classPath = "Modules.ModuleDailyActivity.ChildView.DailyRewardsView"

require "Modules.ModuleDailyActivity.ChildComponent.DailyRewardsTaskItem"
require "Modules.ModuleDailyActivity.ChildView.DailyPointRewardView"
require "Modules.ModuleDailyActivity.ChildView.WeeklyPointRewardView"
local DailyRewardsTaskItem = ClassTypes.DailyRewardsTaskItem
local DailyPointRewardView = ClassTypes.DailyPointRewardView
local WeeklyPointRewardView = ClassTypes.WeeklyPointRewardView

local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local DailyActivityManager = PlayerManager.DailyActivityManager
local UIProgressBar = ClassTypes.UIProgressBar
local PointRewardDataHelper = GameDataHelper.PointRewardDataHelper
local Vector3 = Vector3
local UIComponentUtil = GameUtil.UIComponentUtil
local DailyActivityConfig = GameConfig.DailyActivityConfig

function DailyRewardsView:Awake()
    self._containerPointRewards = self:FindChildGO("Container_Main/Container_PointRewards")
    self._textDailyVitalityValue = self:GetChildComponent("Container_Main/Container_DailyVitality/Text_Value", "TextMeshWrapper")
    self._textWeeklyVitalityValue = self:GetChildComponent("Container_Bottom/Container_WeeklyVitality/Text_Value", "TextMeshWrapper")
    self._pbVitality = UIProgressBar.AddProgressBar(self.gameObject, "Container_Main/Container_PointRewards/ProgressBar_Vitality")
    self:InitList()
    self._dailyPointRewardItemPool = {}
    self._dailyPointRewardItems = {}
    self._weeklyPointRewardItemPool = {}
    self._weeklyPointRewardItems = {}
    local containerDailyPointReward = self:FindChildGO("Container_Main/Container_PointRewards/Container_PointReward")
    containerDailyPointReward:SetActive(false)
    local containerWeeklyPointReward = self:FindChildGO("Container_Bottom/Container_PointRewards/Container_PointReward")
    containerWeeklyPointReward:SetActive(false)
    local PointRewards = self._containerPointRewards:GetComponent(typeof(UnityEngine.RectTransform))
    self._containerPointRewardsWidth = PointRewards.sizeDelta.x
    self._onUpdateDailyActivityData = function(id)self:UpdateDailyActivityData(id) end
    self._onDailyActivityDataStateChanged = function(id)self:DailyActivityDataStateChanged(id) end
    self._onDailyTaskPointIconClick = function(id)self:DailyTaskPointIconClick(id) end
    self._onWeeklyTaskPointIconClick = function(id)self:WeeklyTaskPointIconClick(id) end
    self._onDailyTaskPointRewardResp = function(id)self:UpdateDailyTaskPointReward() end
    self._onWeeklyTaskPointRewardResp = function(id)self:UpdateWeeklyTaskPointReward() end
-- LoggerHelper.Log("Ash: _containerPointRewardsWidth: " .. tostring(self._containerPointRewardsWidth))
end

function DailyRewardsView:OnEnable()
    -- self:LoadListData()
    -- self._onTaskChanged = function()self:LoadListData() end
    self:ShowInfo()
    EventDispatcher:AddEventListener(GameEvents.OnUpdateDailyActivityData, self._onUpdateDailyActivityData)
    EventDispatcher:AddEventListener(GameEvents.OnDailyActivityDataStateChanged, self._onDailyActivityDataStateChanged)
    EventDispatcher:AddEventListener(GameEvents.DailyTaskPointRewardResp, self._onDailyTaskPointRewardResp)
    EventDispatcher:AddEventListener(GameEvents.WeeklyTaskPointRewardResp, self._onWeeklyTaskPointRewardResp)
end

function DailyRewardsView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.OnUpdateDailyActivityData, self._onUpdateDailyActivityData)
    EventDispatcher:RemoveEventListener(GameEvents.OnDailyActivityDataStateChanged, self._onDailyActivityDataStateChanged)
    EventDispatcher:RemoveEventListener(GameEvents.DailyTaskPointRewardResp, self._onDailyTaskPointRewardResp)
    EventDispatcher:RemoveEventListener(GameEvents.WeeklyTaskPointRewardResp, self._onWeeklyTaskPointRewardResp)
end

function DailyRewardsView:OnDestroy()
    self._taskList = nil
    self._textDailyVitalityValue = nil
    self._dailyPointRewardItemPool = nil
    self._dailyPointRewardItems = nil
    self._weeklyPointRewardItemPool = nil
    self._weeklyPointRewardItems = nil
end

function DailyRewardsView:InitList()
    local dailyListScrollView, dailyList = UIList.AddScrollViewList(self.gameObject, "Container_TaskList")
    self._taskList = dailyList
    self._taskListScrollView = dailyListScrollView
    self._taskListScrollView:SetHorizontalMove(false)
    self._taskListScrollView:SetVerticalMove(true)
    self._taskList:SetItemType(DailyRewardsTaskItem)
    self._taskList:SetPadding(0, 0, 0, 0)
    self._taskList:SetGap(10, 0)
    self._taskList:SetDirection(UIList.DirectionTopToDown, 1, 100)
end

function DailyRewardsView:ShowInfo()
    local dtg = DailyActivityManager:GetSortedDailyActivitiesList()
    -- LoggerHelper.Log("Ash: GetSortedDailyActivitiesList: " .. PrintTable:TableToStr(dtg))
    self._taskList:SetDataList(dtg)
    
    self._allDailyPointRewards, self._maxDailyPointRewards, self._allWeeklyPointRewards = DailyActivityManager:GetAllAndMaxPointReward()
    self:UpdateDailyTaskPoint()
    self:UpdateDailyTaskWeekPoint()
    self:SetDailyTaskPointReward()
    self:SetWeeklyTaskPointReward()
end

function DailyRewardsView:SetDailyTaskPointReward()
    local ownedDailyTaskPointReward = DailyActivityManager:GetDailyTaskPointReward()
    -- LoggerHelper.Log("Ash: ownedDailyTaskPointReward: " .. PrintTable:TableToStr(ownedDailyTaskPointReward))
    self:RemoveAllDailyPointReward()
    for i, v in ipairs(self._allDailyPointRewards) do
        local point = PointRewardDataHelper:GetPoint(v)
        local posX = self._containerPointRewardsWidth * point / self._maxDailyPointRewards
        local dtp = DailyActivityManager:GetDailyTaskPoint()
        local owned
        if ownedDailyTaskPointReward[v] ~= nil then--奖励已领取
            owned = DailyActivityConfig.REWARD_GET
        else
            if dtp >= point then--奖励可领取，但未领取
                owned = DailyActivityConfig.REWARD_CAN_GET
            else
                owned = DailyActivityConfig.REWARD_NONE
            end
        end
        -- local owned = ownedDailyTaskPointReward[v] ~= nil
        -- LoggerHelper.Log("Ash: owned: " .. tostring(owned))
        self:AddDailyPointReward(v, posX, PointRewardDataHelper:GetIcon(v), point, owned)
    end
end

function DailyRewardsView:UpdateDailyTaskPointReward()
    -- local ownedDailyTaskPointReward = DailyActivityManager:GetDailyTaskPointReward()
    -- LoggerHelper.Log("Ash: ownedDailyTaskPointReward: " .. PrintTable:TableToStr(ownedDailyTaskPointReward))
    -- self:ResetDailyPointRewardOwned(false)
    -- for k, v in pairs(ownedDailyTaskPointReward) do
    --     self:SetDailyPointRewardOwned(k, true)
    -- end
    self:SetDailyTaskPointReward()
end

function DailyRewardsView:SetWeeklyTaskPointReward()
    local ownedDailyTaskWeekPointReward = DailyActivityManager:GetDailyTaskWeekPointReward()
    -- LoggerHelper.Log("Ash: ownedDailyTaskWeekPointReward: " .. PrintTable:TableToStr(ownedDailyTaskWeekPointReward))
    self:RemoveAllWeeklyPointReward()
    for i, v in ipairs(self._allWeeklyPointRewards) do
        local point = PointRewardDataHelper:GetPoint(v)
        local owned = ownedDailyTaskWeekPointReward[v] ~= nil
        local item = self:AddWeeklyPointReward(v, PointRewardDataHelper:GetIcon(v), point, owned)
        item.transform:SetSiblingIndex(i)
    end
end

function DailyRewardsView:UpdateWeeklyTaskPointReward()
    -- local ownedDailyTaskWeekPointReward = DailyActivityManager:GetDailyTaskWeekPointReward()
    -- LoggerHelper.Log("Ash: ownedDailyTaskWeekPointReward: " .. PrintTable:TableToStr(ownedDailyTaskWeekPointReward))
    -- self:ResetWeeklyPointRewardOwned(false)
    -- for k, v in pairs(ownedDailyTaskWeekPointReward) do
    --     self:SetWeeklyPointRewardOwned(k, true)
    -- end
    self:SetWeeklyTaskPointReward()
end

function DailyRewardsView:UpdateDailyTaskPoint()
    local dtp = DailyActivityManager:GetDailyTaskPoint()
    self._textDailyVitalityValue.text = tostring(dtp)
    self._pbVitality:SetProgress(dtp / self._maxDailyPointRewards)
end

function DailyRewardsView:UpdateDailyTaskWeekPoint()
    local dtwp = DailyActivityManager:GetDailyTaskWeekPoint()
    self._textWeeklyVitalityValue.text = tostring(dtwp)
end

function DailyRewardsView:UpdateDailyActivityData(id)
    self:UpdateDailyTaskPoint()
    self:UpdateDailyTaskWeekPoint()
    self:SetDailyTaskPointReward()
end

function DailyRewardsView:DailyActivityDataStateChanged(id)
    local dtg = DailyActivityManager:GetSortedDailyActivitiesList()
    -- LoggerHelper.Log("Ash: DailyActivityDataStateChanged: " .. PrintTable:TableToStr(dtg))
    self._taskList:SetDataList(dtg)
end

function DailyRewardsView:DailyTaskPointIconClick(id)
    local dtp = DailyActivityManager:GetDailyTaskPoint()
    local point = PointRewardDataHelper:GetPoint(id)
    --LoggerHelper.Log("Ash: DailyTaskPointIconClick: " .. id .. " " .. dtp .. " " .. point)
    if dtp >= point then
        DailyActivityManager:DailyTaskPointRewardReq(id)
    end
end

function DailyRewardsView:WeeklyTaskPointIconClick(id)
    local dtp = DailyActivityManager:GetDailyTaskWeekPoint()
    local point = PointRewardDataHelper:GetPoint(id)
    --LoggerHelper.Log("Ash: DailyTaskPointIconClick: " .. id .. " " .. dtp .. " " .. point)
    if dtp >= point then
        DailyActivityManager:WeeklyTaskPointRewardReq(id)
    end
end

-- 积分日奖励对象池
function DailyRewardsView:CreateDailyPointRewardItem()
    local itemPool = self._dailyPointRewardItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Main/Container_PointRewards/Container_PointReward", "Container_Main/Container_PointRewards")
    local item = UIComponentUtil.AddLuaUIComponent(go, DailyPointRewardView)
    return item
end

function DailyRewardsView:ReleaseDailyPointRewardItem(item)
    local itemPool = self._dailyPointRewardItemPool
    table.insert(itemPool, item)
end

function DailyRewardsView:AddDailyPointReward(id, posX, iconId, value, owned)
    local item = self:CreateDailyPointRewardItem()
    item.gameObject:SetActive(true)
    self._dailyPointRewardItems[id] = item
    item.transform.localPosition = Vector3(posX, 0, 0)
    item:SetData(id, iconId, value, self._onDailyTaskPointIconClick)
    item:SetState(owned)
    return item
end

function DailyRewardsView:ResetDailyPointRewardOwned(owned)
    for k, v in pairs(self._dailyPointRewardItems) do
        v:SetState(owned)
    end
end

function DailyRewardsView:SetDailyPointRewardOwned(id, owned)
    if self._dailyPointRewardItems[id] ~= nil then
        self._dailyPointRewardItems[id]:SetState(owned)
    end
end

function DailyRewardsView:RemoveAllDailyPointReward()
    for k, item in pairs(self._dailyPointRewardItems) do
        item.gameObject:SetActive(false)
        self:ReleaseDailyPointRewardItem(item)
    end
    self._dailyPointRewardItems = {}
end

-- 积分周奖励对象池
function DailyRewardsView:CreateWeeklyPointRewardItem()
    local itemPool = self._weeklyPointRewardItemPool
    if #itemPool > 0 then
        return table.remove(itemPool)
    end
    local go = self:CopyUIGameObject("Container_Bottom/Container_PointRewards/Container_PointReward", "Container_Bottom/Container_PointRewards")
    local item = UIComponentUtil.AddLuaUIComponent(go, WeeklyPointRewardView)
    return item
end

function DailyRewardsView:ReleaseWeeklyPointRewardItem(item)
    local itemPool = self._weeklyPointRewardItemPool
    table.insert(itemPool, item)
end

function DailyRewardsView:AddWeeklyPointReward(id, iconId, value, owned)
    local item = self:CreateWeeklyPointRewardItem()
    item.gameObject:SetActive(true)
    self._weeklyPointRewardItems[id] = item
    item:SetData(id, iconId, value, self._onWeeklyTaskPointIconClick)
    item:SetState(owned)
    return item
end

function DailyRewardsView:ResetWeeklyPointRewardOwned(owned)
    for k, v in pairs(self._weeklyPointRewardItems) do
        v:SetState(owned)
    end
end

function DailyRewardsView:SetWeeklyPointRewardOwned(id, owned)
    if self._weeklyPointRewardItems[id] ~= nil then
        self._weeklyPointRewardItems[id]:SetState(owned)
    end
end

function DailyRewardsView:RemoveAllWeeklyPointReward()
    for k, item in pairs(self._weeklyPointRewardItems) do
        item.gameObject:SetActive(false)
        self:ReleaseWeeklyPointRewardItem(item)
    end
    self._weeklyPointRewardItems = {}
end
