--任务奖励和交付道具ListItem
-- RewardItemView.lua
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
-- RewardItemView.lua
local RewardItemView = Class.RewardItemView(ClassTypes.BaseComponent)

RewardItemView.interface = GameConfig.ComponentsConfig.Component
RewardItemView.classPath = "Modules.ModuleDailyActivity.ChildView.RewardItemView"

local public_config = GameWorld.public_config
local DailyActivityManager = PlayerManager.DailyActivityManager
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function RewardItemView:Awake()
    self._base.Awake(self)
    self._textCount = self:GetChildComponent("Text_Count", "TextMeshWrapper")
    self._itemManager = ItemManager()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
end

function RewardItemView:OnDestroy()
    self._textCount = nil
    self._icon = nil
    self._base.OnDestroy(self)
end

function RewardItemView:SetData(data)
    self._textCount.text = LanguageDataHelper.CreateContent(901) .. data:GetCount()
    self._icon:SetItem(data:GetItemId())
end
