-- DailyAppearView.lua
local DailyAppearView = Class.DailyAppearView(ClassTypes.BaseComponent)

DailyAppearView.interface = GameConfig.ComponentsConfig.Component
DailyAppearView.classPath = "Modules.ModuleDailyActivity.ChildView.DailyAppearView"

require "Modules.ModuleDailyActivity.ChildComponent.DailyAppearAttrItem"
local DailyAppearAttrItem = ClassTypes.DailyAppearAttrItem

local UIList = ClassTypes.UIList
local XArtNumber = GameMain.XArtNumber
local ActorModelComponent = GameMain.ActorModelComponent
local UIProgressBar = ClassTypes.UIProgressBar
local UIToggle = ClassTypes.UIToggle
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local ActivityLevelDataHelper = GameDataHelper.ActivityLevelDataHelper
local DailyActivityExManager = PlayerManager.DailyActivityExManager
local ActivityAppearanceDataHelper = GameDataHelper.ActivityAppearanceDataHelper
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local BaseUtil = GameUtil.BaseUtil
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale
local UIParticle = ClassTypes.UIParticle

function DailyAppearView:Awake()
    self:InitViews()
    self:InitCallBack()
end

function DailyAppearView:InitViews()
    self._activeImage = self:FindChildGO("Container_Content/Image_NotActive")

    self._fpNumber = self:AddChildComponent('Container_Content/Container_Content/Container_FightPower', XArtNumber)

    self._modelComponent = self:AddChildComponent('Container_Content/Container_Model', ActorModelComponent)
    self._modelComponent:SetStartSetting(Vector3(0, 0, -150), Vector3(0, 180, 0), 1)
    self._appearPosition = self:FindChildGO("Container_Content/Container_Model").transform.position

    self._appearLevelText = self:GetChildComponent("Container_Content/Container_Level/Text_Level",'TextMeshWrapper')

    self._nowLevelText = self:GetChildComponent("Container_Level/Container_Now/Image_Bg/Text",'TextMeshWrapper')
    self._nextLevelText = self:GetChildComponent("Container_Level/Container_Next/Image_Bg/Text",'TextMeshWrapper')
    self._levelButtonText = self:GetChildComponent("Container_Content/Button_Upgrade/Text",'TextMeshWrapper')

    -- self._expComp = UIProgressBar.AddProgressBar(self.gameObject,"Container_Content/Container_Level/ProgressBar_Exp")
    self._expComp = UIScaleProgressBar.AddProgressBar(self.gameObject,"Container_Content/Container_Level/ProgressBar_Exp")
    self._expComp:SetProgress(0)
    self._expComp:ShowTweenAnimate(true)
    self._expComp:SetMutipleTween(false)
    self._expComp:SetIsResetToZero(false)
    self._expComp:SetTweenTime(0.2)

    -- self._showToggleGo = self:FindChildGO("Container_Content/Toggle_Hide")
    -- self._showToggle = UIToggle.AddToggle(self.gameObject, "Container_Content/Toggle_Hide")
    -- self._showToggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

    self._levelUpButton = self:FindChildGO("Container_Content/Button_Upgrade")
    self._csBH:AddClick(self._levelUpButton, function() self:OnLevelUpButtonClick() end)

    self.levelUpTip = self:FindChildGO("Container_Content/Image_LevelUp").transform
    self.levelUpTip.localScale = Vector3.zero
    self._animScaleLevelUp = self:AddChildComponent('Container_Content/Image_LevelUp', XGameObjectTweenScale)
    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Content/Container_Fx/fx_ui_30034")
    self._fx:SetLifeTime(2)
    self._fx:Stop()


    -- self._preButton = self:FindChildGO("Container_Content/Button_Pre")
    -- self._csBH:AddClick(self._preButton, function() self:OnAppeaPreButtonClick() end)

    -- self._nextButton = self:FindChildGO("Container_Content/Button_Next")
    -- self._csBH:AddClick(self._nextButton, function() self:OnAppearNextButtonClick() end)


    self._appearCloseButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._appearCloseButton, function() self:OnAppearCloseButtonClick() end)

    self._appearShade = self:FindChildGO("Image_Shade")
    self._csBH:AddClick(self._appearShade, function() self:OnAppearCloseButtonClick() end)

    self:InitAttrGridLayoutGroup()
end

function DailyAppearView:InitAttrGridLayoutGroup()
    -- self._attrGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Content/Container_Content/Container_Attr")
    -- self._attrGridLauoutGroup:SetItemType(DailyAppearAttrItem)

    self._nowAttrGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Level/Container_Now/Container_Attr")
    self._nowAttrGridLauoutGroup:SetItemType(DailyAppearAttrItem)
    self._nextAttrGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Level/Container_Next/Container_Attr")
    self._nextAttrGridLauoutGroup:SetItemType(DailyAppearAttrItem)
end

function DailyAppearView:InitCallBack()
    self._appearLevelUp = function() self:AppearLevelUp() end
    self._appearChange = function() self:AppearChange() end
end

function DailyAppearView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ON_ACTIVITY_APPEAR_LEVEL_UP, self._appearLevelUp)
    EventDispatcher:AddEventListener(GameEvents.ON_ACTIVITY_APPEAR_CHANGE, self._appearChange)   
end

function DailyAppearView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_ACTIVITY_APPEAR_LEVEL_UP, self._appearLevelUp)
    EventDispatcher:RemoveEventListener(GameEvents.ON_ACTIVITY_APPEAR_CHANGE, self._appearChange)   
end

function DailyAppearView:OnEnable()
    self:AddEventListeners()
    self:SetNowSelectModel()
    self:RefreshLevelInfo()
    self:SetAppearAttr()
end

function DailyAppearView:OnDisable()
    self:RemoveEventListeners()
end

function DailyAppearView:RefreshLevelInfo()
    self._maxLevel = ActivityLevelDataHelper:GetMaxLevel()
    -- self._appearLevelText.text = "Lv.".. GameWorld.Player().daily_level
    local exp = GameWorld.Player().daily_point
    local levelUpExp = ActivityLevelDataHelper:GetNextCost(GameWorld.Player().daily_level)
    self._expComp:SetProgressText(tostring(exp).."/"..tostring(levelUpExp))
    if exp > levelUpExp then
        self._expComp:SetProgress(1)
    else
        local progress = exp/levelUpExp
        if progress == 0 then
            progress = 0.001
        end
        self._expComp:SetProgress(progress) 
    end
    local flag = false
    if  (exp < levelUpExp) or self._maxLevel == GameWorld.Player().daily_level then
        flag = true
    end

    local nowLevel = GameWorld.Player().daily_level
    self._nowLevelText.text = LanguageDataHelper.CreateContent(52113, {["0"] = nowLevel})
    if nowLevel == self._maxLevel then
        self._nextLevelText.text = LanguageDataHelper.CreateContent(52118)--已满级
        self._levelButtonText.text = LanguageDataHelper.CreateContent(52118)--已满级
    else
        self._nextLevelText.text = LanguageDataHelper.CreateContent(52114, {["0"] = nowLevel + 1})
        self._levelButtonText.text = LanguageDataHelper.CreateContent(52070)
    end
    self._levelUpButton:GetComponent("ButtonWrapper"):SetInteractable(not flag)
end

function DailyAppearView:SetNowSelectModel()
    -- LoggerHelper.Error("appearId==" .. tostring(GameWorld.Player().daily_level))
    -- self._selectedGrade = GameWorld.Player().daily_show
    -- if self._selectedGrade == 0 then        
    self._selectedGrade = ActivityAppearanceDataHelper:GetAppearanceIdByLevel(GameWorld.Player().daily_level)
    -- end
    self:RefreshModel()
end

function DailyAppearView:RefreshModel()    
    local modelId = ActivityAppearanceDataHelper:GetModel(self._selectedGrade)
    self._modelComponent:LoadModel(modelId)
    -- local modelId = ActivityAppearanceDataHelper:GetModel(self._selectedGrade)
    -- local fullPath = "Assets/Resources/Fx/fx_prefab/skill/particle_buff_richangwaixing.prefab"
    -- local pos = self._appearPosition
    -- local face = Vector3.zero
    -- local uid = GameWorld.PlayFX(fullPath, pos, face, -1)

    -- self._showToggle:SetIsOn(GameWorld.Player().daily_show == self._selectedGrade)
    -- self._preButton:SetActive(not (self._selectedGrade == 1))
    -- self._nextButton:SetActive(not (self._selectedGrade == #ActivityAppearanceDataHelper:GetAllId()))
    -- local isActive = ActivityAppearanceDataHelper:GetLevel(self._selectedGrade) > GameWorld.Player().daily_level
    -- self._activeImage:SetActive(isActive)
    -- self._showToggleGo:SetActive(not isActive)
end


function DailyAppearView:OnAppearCloseButtonClick()
    self:SetActive(false)
end

function DailyAppearView:OnLevelUpButtonClick()
    -- local exp = GameWorld.Player().daily_point
    -- local levelUpExp = ActivityLevelDataHelper:GetNextCost(GameWorld.Player().daily_level)
    -- if levelUpExp <= exp then
    DailyActivityExManager:LevelUp()
    -- end
end

function DailyAppearView:OnAppeaPreButtonClick()
    if self._selectedGrade == 1 then  return end
    self._selectedGrade = self._selectedGrade - 1
    self:RefreshModel() 
end

function DailyAppearView:OnAppearNextButtonClick()
    if self._selectedGrade == #ActivityAppearanceDataHelper:GetAllId() then  return end  
    self._selectedGrade = self._selectedGrade + 1
    self:RefreshModel()
 end
 
--外形升级
function DailyAppearView:AppearLevelUp()
    self:DoTweenForLevelUp()
    self:RefreshLevelInfo()
    self:ChangeAppear()
    self:SetAppearAttr()
end

function DailyAppearView:DoTweenForLevelUp()
    self._animScaleLevelUp.IsTweening = false
    self.levelUpTip.localScale = Vector3.zero
    self._fx:Stop()
    self._animScaleLevelUp:OnceOutQuad(1, 1.2, function()  
        self.levelUpTip.localScale = Vector3.zero
    end)
    self._fx:Play()
end

function DailyAppearView:ChangeAppear()
    local beforLevelId =  DailyActivityExManager:GetCanAppearId()
    local afterlevelId = ActivityAppearanceDataHelper:GetAppearanceIdByLevel(GameWorld.Player().daily_level)
    if afterlevelId > beforLevelId then
        DailyActivityExManager:SetCanAppearId(afterlevelId)
        DailyActivityExManager:SetAppearanceShowId(afterlevelId)
    end
    -- local appearId = ActivityAppearanceDataHelper:GetAppearanceIdByLevel(GameWorld.Player().daily_level)
    -- if appearId ~= GameWorld.Player().daily_show and GameWorld.Player().daily_show ~= 0  then
    --     DailyActivityExManager:SetAppearanceShowId(appearId)
    -- else
    --     self._selectedGrade = appearId
    --     self:RefreshModel()
    -- end
end

function DailyAppearView:AppearChange()
    self._selectedGrade = GameWorld.Player().daily_show
    self:RefreshModel()
end

function DailyAppearView:SetAppearAttr()
    -- LoggerHelper.Error("GameWorld.Player().daily_level ==" .. tostring(GameWorld.Player().daily_level))
    -- local attrs = ActivityLevelDataHelper:GetAtrri(GameWorld.Player().daily_level)
    -- -- LoggerHelper.Error("attrs ==== " .. PrintTable:TableToStr(attrs))
    -- if attrs ~= nil then
    --     local valueDatas = {}
    --     for k,v in ipairs(attrs) do
    --         local valueData = {}
    --         valueData.attri = v[1]
    --         valueData.value = v[2]
    --         table.insert(valueDatas, valueData)
    --     end
    --     -- LoggerHelper.Error("valueDatas ==== " .. PrintTable:TableToStr(valueDatas))

    --     local fightPower = BaseUtil.CalculateFightPower(valueDatas, GameWorld.Player().vocation)
    --     -- LoggerHelper.Error("fightPower ====" .. tostring(fightPower))
    --     self._fpNumber:SetNumber(fightPower)

    --     self._attrGridLauoutGroup:SetDataList(attrs)
    -- end

    local nowLevel = GameWorld.Player().daily_level
    local nowAtrriDatas = ActivityLevelDataHelper:GetAtrri(nowLevel)
    for i,v in ipairs(nowAtrriDatas) do
        v[3] = nowLevel
    end
    self._nowAttrGridLauoutGroup:SetDataList(nowAtrriDatas)
    local nextAtrriDatas = ActivityLevelDataHelper:GetAtrri(nowLevel + 1)
    if nextAtrriDatas == nil then
        nextAtrriDatas = {}
    else
        for i,v in ipairs(nextAtrriDatas) do
            v[3] = nowLevel + 1
        end
    end
    
    self._nextAttrGridLauoutGroup:SetDataList(nextAtrriDatas)
end

function DailyAppearView:OnToggleValueChanged(state)
    if state then
        DailyActivityExManager:SetAppearanceShowId(self._selectedGrade)
    elseif self._selectedGrade == GameWorld.Player().daily_show then
        DailyActivityExManager:SetAppearanceHide()
    end
end