-- DailyView.lua
local DailyView = Class.DailyView(ClassTypes.BaseComponent)

DailyView.interface = GameConfig.ComponentsConfig.Component
DailyView.classPath = "Modules.ModuleDailyActivity.ChildView.DailyView"

local UIComponentUtil = GameUtil.UIComponentUtil
local ShopType = GameConfig.EnumType.ShopType
local GUIManager = GameManager.GUIManager
local XGameObjectComplexIcon = GameMain.XGameObjectComplexIcon

require "Modules.ModuleDailyActivity.ChildView.DailyCalendarView"
local DailyCalendarView = ClassTypes.DailyCalendarView

require "Modules.ModuleDailyActivity.ChildView.DailyAppearView"
local DailyAppearView = ClassTypes.DailyAppearView

require "Modules.ModuleDailyActivity.ChildComponent.DailyRewardItem"
local DailyRewardItem = ClassTypes.DailyRewardItem

require "Modules.ModuleDailyActivity.ChildComponent.DailyAppearAttrItem"
local DailyAppearAttrItem = ClassTypes.DailyAppearAttrItem

require "Modules.ModuleDailyActivity.ChildComponent.DailyListItem"
local DailyListItem = ClassTypes.DailyListItem


local ActivityPointRewardDataHelper = GameDataHelper.ActivityPointRewardDataHelper
local RectTransform = UnityEngine.RectTransform
local UIList = ClassTypes.UIList
local ActivityDataDataHelper = GameDataHelper.ActivityDataDataHelper
local ActivityAppearanceDataHelper = GameDataHelper.ActivityAppearanceDataHelper
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local ActorModelComponent = GameMain.ActorModelComponent
local UIProgressBar = ClassTypes.UIProgressBar
local ActivityLevelDataHelper = GameDataHelper.ActivityLevelDataHelper
local DailyActivityExManager = PlayerManager.DailyActivityExManager
local EquipManager = GameManager.EquipManager
local UIToggle = ClassTypes.UIToggle
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local AttriDataHelper = GameDataHelper.AttriDataHelper
local BaseUtil = GameUtil.BaseUtil
local XArtNumber = GameMain.XArtNumber
local DailyConfig = GameConfig.DailyConfig


function DailyView:Awake()
    self:InitViews()
    self:InitCallBack()
    self._updateAppearRedpoint = function(flag) self:UpdateAppearRedpoint(flag) end
end

function DailyView:InitViews()
    self._dailyCalendarView = self:AddChildLuaUIComponent("Container_Calendar", DailyCalendarView)
    self._dailyAppearView = self:AddChildLuaUIComponent("Container_Appearance", DailyAppearView)

    self._timeText = self:GetChildComponent("Container_Time/Text_Time",'TextMeshWrapper')    
    self._pointText = self:GetChildComponent("Container_Buttom/Container_Progress/Text_Num",'TextMeshWrapper')    
    self._rewardDemoContainer = self:FindChildGO("Container_Buttom/Container_Progress/Container_Rewards/Container_Reward")
    self._progressBarLength = self:FindChildGO("Container_Buttom/Container_Progress/ProgressBar/Image_Background"):GetComponent(typeof(RectTransform)).sizeDelta.x

    self._appearanceContainer = self:FindChildGO("Container_Appearance")

    self._barComp = UIScaleProgressBar.AddProgressBar(self.gameObject,"Container_Buttom/Container_Progress/ProgressBar")
    self._barComp:SetProgress(0)
    self._barComp:ShowTweenAnimate(true)
    self._barComp:SetMutipleTween(false)
    self._barComp:SetIsResetToZero(false)
    self._barComp:SetTweenTime(0.2)

    self._addOfflineTimeButton = self:FindChildGO("Container_Time/Button_Add")
    self._csBH:AddClick(self._addOfflineTimeButton, function() self:OnOfflineTimeClick() end)

    self._calendarButton = self:FindChildGO("Container_Buttom/Button_Week_Calendar")
    self._csBH:AddClick(self._calendarButton, function() self:OnCalendarButtonClick() end)

    self._appearButton = self:FindChildGO("Container_Buttom/Button_Appearance")
    self._csBH:AddClick(self._appearButton, function() self:OnAppearButtonClick() end)

    self._redpointImage = self:FindChildGO("Container_Buttom/Button_Appearance/Image_Red")
    -- self._animContainer = self:FindChildGO("Container_Anim")
    -- self._animContainer:SetActive(true)
    -- self._complexIcon =  self._animContainer:AddComponent(typeof(XGameObjectComplexIcon))
    -- self._complexIcon:AddComplexIcon(1)
    -- self._complexIcon:StartPlay()
    -- self._animContainer = self:FindChildGO("Container_Anim")
    -- GameWorld.AddIconAnim(self._animContainer, 2)
    -- GameWorld.SetIconAnimSpeed(self._animContainer, 5)

    self:InitRewardView()
    self:InitDailyList()
end

function DailyView:InitDailyList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/ScrollViewList")
	self._dailyList = list
	self._dailyListlView = scrollView 
    self._dailyListlView:SetHorizontalMove(false)
    self._dailyListlView:SetVerticalMove(true)
    self._dailyList:SetItemType(DailyListItem)
    self._dailyList:SetPadding(0, 0, 0, 0)
    self._dailyList:SetGap(0, 0)
    self._dailyList:SetDirection(UIList.DirectionLeftToRight, 2, 100)
    local itemClickedCB = 
	function(index)
		self:OnListItemClicked(index)
	end
    self._dailyList:SetItemClickedCB(itemClickedCB)
end

function DailyView:InitRewardView()
    local ids = ActivityPointRewardDataHelper:GetAllId()
    self._rewardViews = {}
    local max = ActivityPointRewardDataHelper:GetMaxPoint()
    for i,v in ipairs(ids) do
        local go = self:CopyRewardContainer()
        table.insert(self._rewardViews, UIComponentUtil.AddLuaUIComponent(go, DailyRewardItem))
        self._rewardViews[i]:InitData(v, self._progressBarLength)
    end
    self._rewardDemoContainer:SetActive(false)
end

function DailyView:CopyRewardContainer()
    return self:CopyUIGameObject("Container_Buttom/Container_Progress/Container_Rewards/Container_Reward", "Container_Buttom/Container_Progress/Container_Rewards")
end

function DailyView:OnEnable()
    self:AddEventListeners()
    DailyActivityExManager:RegisterAppearFunc(self._updateAppearRedpoint)
end

function DailyView:OnDisable()
    DailyActivityExManager:RegisterAppearFunc(nil)
    self:RemoveEventListeners()
end

function DailyView:OnListItemClicked(idx)
    local id = self._dailyList:GetDataByIndex(idx)
    -- self._seledata = data
    
    if id==-1 then return end
    local data = {}
    data._id = id
    data._index = idx
    GUIManager.ShowPanel(PanelsConfig.DailyTips,data)
end

function DailyView:InitCallBack()
    self._getRewardComplete = function(id) self:GetRewardComplete(id) end
    self._offineTimeChangeCB = function(id) self:SetTime() end
    self._onUpdateData= function()self:SetDailyListData(self._num) end
    self._onSetPointData= function()self:SetPointData() end
end

function DailyView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ON_ACTIVITY_GETREWARD_COMPLETE, self._getRewardComplete)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEFT_HANG_UP_TIME, self._offineTimeChangeCB)
    EventDispatcher:AddEventListener(GameEvents.DAILY_CUR_COUNT_UPDATE, self._onUpdateData)--每日必做次数更新
    EventDispatcher:AddEventListener(GameEvents.DAILY_POINT_REWARD_UPDATE, self._onSetPointData)--每日必做奖励变化
    EventDispatcher:AddEventListener(GameEvents.DAILY_POINT_COUNT_UPDATE, self._onSetPointData)--每日活跃度变化
end

function DailyView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_ACTIVITY_GETREWARD_COMPLETE, self._getRewardComplete)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEFT_HANG_UP_TIME, self._offineTimeChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.DAILY_CUR_COUNT_UPDATE, self._onUpdateData)--每日必做次数更新
    EventDispatcher:RemoveEventListener(GameEvents.DAILY_POINT_REWARD_UPDATE, self._onSetPointData)--每日必做奖励变化
    EventDispatcher:RemoveEventListener(GameEvents.DAILY_POINT_COUNT_UPDATE, self._onSetPointData)--每日活跃度变化
end

function DailyView:OnDestroy()
end

function DailyView:RefreshView(num)
    --num:1 每日必做
    --2 限时任务
    self._num = num
    self:SetTime()
    self:SetPointData()
    self:SetDailyListData(num)
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleDailyActivity.DailyActivityExPanel")
end

function DailyView:SetDailyListData(num)
    if num == DailyConfig.Must then
        self._dailyListData = DailyActivityExManager:GetMustDailyListData()
        self._dailyList:SetDataList(self._dailyListData)
    elseif num == DailyConfig.Limit then
        self._dailyListData = DailyActivityExManager:GetLimitDailyListData()
        self._dailyList:SetDataList(self._dailyListData)
    end
end

function DailyView:SetTime()
    self._timeText.text = DateTimeUtil:GetMinFormatHHMM(GameWorld.Player().left_hang_up_time)
end

function DailyView:SetPointData()
    local max = ActivityPointRewardDataHelper:GetMaxPoint()
    local point = GameWorld.Player().daily_activity_point
    self._pointText.text = point
    -- self._barComp:SetProgress(point/max)
    if point > max then
        self._barComp:SetProgress(1)
    else
        local progress = point/max
        self._barComp:SetProgress(progress) 
    end

    for i,v in ipairs(self._rewardViews) do
        v:RefreshStatus()
    end
end

function DailyView:OnCalendarButtonClick()
    self._dailyCalendarView:SetActive(true)
end

function DailyView:OnAppearButtonClick()    
    self._dailyAppearView:SetActive(true)
end

function DailyView:OnOfflineTimeClick()
    EquipManager:OffineTimeGuide()
end

function DailyView:UpdateAppearRedpoint(flag)
    self._redpointImage:SetActive(flag)
end


function DailyView:GetRewardComplete(id)
    self._rewardViews[id]:RefreshStatus()
end