-- WeeklyPointRewardView.lua
local WeeklyPointRewardView = Class.WeeklyPointRewardView(ClassTypes.BaseComponent)

WeeklyPointRewardView.interface = GameConfig.ComponentsConfig.PointableComponent
WeeklyPointRewardView.classPath = "Modules.ModuleDailyActivity.ChildView.WeeklyPointRewardView"
require "Modules.ModuleDailyActivity.ChildView.PointRewardTipView"
local PointRewardTipView = ClassTypes.PointRewardTipView

local public_config = GameWorld.public_config
local DailyActivityManager = PlayerManager.DailyActivityManager
local UIComponentUtil = GameUtil.UIComponentUtil

function WeeklyPointRewardView:Awake()
    self._containerIcon = self:FindChildGO("Container_Icon")
    self._imageItemGet = self:FindChildGO("Image_Item_Get")
    self._textWeeklyVitalityValue = self:GetChildComponent("Container_Value/Text_Value", "TextMeshWrapper")
    -- self._onIconClick = function()self:IconClick() end
    -- self._csBH:AddClick(self._containerIcon, self._onIconClick)
    self._containerTip = self:FindChildGO("Container_Tip")
    self._viewTip = UIComponentUtil.AddLuaUIComponent(self._containerTip, PointRewardTipView)
end

function WeeklyPointRewardView:OnEnable()
end

function WeeklyPointRewardView:OnDisable()
end

function WeeklyPointRewardView:OnPointerDown(pointerEventData)
    self._containerTip:SetActive(true)
end

function WeeklyPointRewardView:OnPointerUp(pointerEventData)
    self._containerTip:SetActive(false)
    self:IconClick()
end

function WeeklyPointRewardView:OnDestroy()
    self._containerIcon = nil
    self._imageItemGet = nil
    self._textWeeklyVitalityValue = nil
end

function WeeklyPointRewardView:IconClick()
    --LoggerHelper.Log("Ash: IconClick: " .. self._id)
    if self._callback ~= nil then
        self._callback(self._id)
    end
end

function WeeklyPointRewardView:SetData(id, iconId, value, callback)
    self._containerTip:SetActive(true)
    self._id = id
    self._callback = callback
    self._textWeeklyVitalityValue.text = value
    self._viewTip:SetData(id)
    GameWorld.AddIcon(self._containerIcon, iconId, nil, true)
    self._containerTip:SetActive(false)
end

function WeeklyPointRewardView:SetState(enable)
    if enable then
        self._imageItemGet:SetActive(true)
    else
        self._imageItemGet:SetActive(false)
    end
end
