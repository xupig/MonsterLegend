-- DailyFindView.lua
local DailyFindView = Class.DailyFindView(ClassTypes.BaseComponent)

DailyFindView.interface = GameConfig.ComponentsConfig.Component
DailyFindView.classPath = "Modules.ModuleDailyActivity.ChildView.DailyFindView"

require "Modules.ModuleDailyActivity.ChildComponent.DailyFindBackListItem"
local DailyFindBackListItem = ClassTypes.DailyFindBackListItem

local UIList = ClassTypes.UIList
local UIToggleGroup = ClassTypes.UIToggleGroup
local DailyActivityExManager = PlayerManager.DailyActivityExManager
local DailyConfig = GameConfig.DailyConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

require "UIComponent.Base.UISlider"
local UISlider = ClassTypes.UISlider

function DailyFindView:Awake()
    self:InitViews()
    self:InitCallBack()
end

function DailyFindView:InitCallBack()
    self._getRewardData = function() self:RefreshRewardData() end
    self._findBackClick = function(data) self:FindBackClick(data) end
    self._getRewardBackComplete = function() self:GetRewardBackComplete() end
    self._updateExFindBackRepoint = function(flag) self:UpdateExFindBackRepoint(flag) end
    self._updateCommonFindBackRepoint = function(flag) self:UpdateCommonFindBackRepoint(flag) end 
end

function DailyFindView:InitViews()
    self._exRedpoint = self:FindChildGO("ToggleGroup/Toggle_Diamond/Image_RedPoint")
    self._commonRedpoint = self:FindChildGO("ToggleGroup/Toggle_Money/Image_RedPoint")

    self._findNone = self:FindChildGO("Container_None")
    
    self._findBackContainer = self:FindChildGO("Container_PopupSmall")    
    self._findSlider = UISlider.AddSlider(self.gameObject, "Container_PopupSmall/Container_Num/Slider")
    self._findSlider:SetOnValueChangedCB(function() self:BuyNumChange() end)

	self._toggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup")
    self._toggleGroup:SetOnSelectedIndexChangedCB(function(index) self:OnMenuTabBarClick(index) end)
    
    self._backTipsText = self:GetChildComponent("Text_Back_Tips",'TextMeshWrapper')
    self._findBackDescText = self:GetChildComponent("Container_PopupSmall/Text_Desc",'TextMeshWrapper')
    self._buyPriceText = self:GetChildComponent("Container_PopupSmall/Container_Buy_Info/Text_Price",'TextMeshWrapper')

    self._exTipsGo = self:FindChildGO("Container_PopupSmall/Container_Buy_Info/Text_Tips")

    self._findBackCloseButton = self:FindChildGO("Container_PopupSmall/Button_Close")
    self._csBH:AddClick(self._findBackCloseButton, function() self:OnFindBackCloseClick() end)

    self._findBackReqButton = self:FindChildGO("Container_PopupSmall/Button_Find")
    self._csBH:AddClick(self._findBackReqButton, function() self:OnFindBackReq() end)

    self._findBackPlusClickButton = self:FindChildGO("Container_PopupSmall/Container_Num/Button_Plus_BG")
    self._csBH:AddClick(self._findBackPlusClickButton, function() self:OnFindBackPlusClick() end)

    self._findBackReduceClickButton = self:FindChildGO("Container_PopupSmall/Container_Num/Button_Reduce_BG")
    self._csBH:AddClick(self._findBackReduceClickButton, function() self:OnFindBackReduceClick() end)
    
    self._findBackContainer:SetActive(false)
    self._findNone:SetActive(false)
    self:InitFindBackListView()
end

function DailyFindView:InitFindBackListView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/ScrollViewList")
	self._findBackList = list
	self._findBackListlView = scrollView 
    self._findBackListlView:SetHorizontalMove(false)
    self._findBackListlView:SetVerticalMove(true)
    self._findBackList:SetItemType(DailyFindBackListItem)
    self._findBackList:SetPadding(0, 0, 0, 0)
    self._findBackList:SetGap(10, 5)
    self._findBackList:SetDirection(UIList.DirectionLeftToRight, 2, 100)
end

function DailyFindView:AddEventListeners()  
    EventDispatcher:AddEventListener(GameEvents.ON_GET_REWARD_BACK_DATA, self._getRewardData)
    EventDispatcher:AddEventListener(GameEvents.ON_DAILY_FIND_BACK_CLICK, self._findBackClick)
    EventDispatcher:AddEventListener(GameEvents.REWARD_BACK_GET_REWARD_COMPLETE, self._getRewardBackComplete)
end

function DailyFindView:RemoveEventListeners() 
    EventDispatcher:RemoveEventListener(GameEvents.ON_GET_REWARD_BACK_DATA, self._getRewardData)
    EventDispatcher:RemoveEventListener(GameEvents.ON_DAILY_FIND_BACK_CLICK, self._findBackClick)
    EventDispatcher:RemoveEventListener(GameEvents.REWARD_BACK_GET_REWARD_COMPLETE, self._getRewardBackComplete)
end

function DailyFindView:OnEnable()
    DailyActivityExManager:RegisterExFindBackFunc(self._updateExFindBackRepoint)
    DailyActivityExManager:RegisterCommonFindBackFunc(self._updateCommonFindBackRepoint)
    self:AddEventListeners()	
end

function DailyFindView:RefreshView()
    -- DailyActivityExManager:GetRewardBackInfo()
    self._toggleGroup:SetSelectIndex(DailyConfig.REWARD_BACK_TYPE_EX)
    self:OnMenuTabBarClick(DailyConfig.REWARD_BACK_TYPE_EX)
    
    self._backTipsText.text = LanguageDataHelper.CreateContent(52082)
end

--查询资源找回数据处理方法
function DailyFindView:RefreshRewardData()
    -- LoggerHelper.Error("DailyFindView:RefreshRewardData()")
    if self._select == DailyConfig.REWARD_BACK_TYPE_EX then--钻石找回
        self._findBackList:SetDataList(DailyActivityExManager:GetExRewardListDatas())
        self._backTipsText.text = LanguageDataHelper.CreateContent(52082)

        local findList = DailyActivityExManager:GetExRewardListDatas() or {}
        self._findNone:SetActive(DailyFindView:IsFindNone(findList))
 

    elseif self._select == DailyConfig.REWARD_BACK_TYPE_COMMON then--金币找回
        self._findBackList:SetDataList(DailyActivityExManager:GetCommonRewardListDatas())
        self._backTipsText.text = LanguageDataHelper.CreateContent(52083)

        local findList = DailyActivityExManager:GetCommonRewardListDatas() or {}
        self._findNone:SetActive(DailyFindView:IsFindNone(findList))
    end
end

function DailyFindView:IsFindNone(data)
    if #data == 0 then
        return true
    else
        return false
    end
end


--资源找回按钮事件
function DailyFindView:FindBackClick(data)
    self._findBackContainer:SetActive(true)
    self._data = data
    local exNum =  self._data:GetExTimes()
    local commonNum = self._data:GetCommonTimes()

    self._findBackDescText.text = self._data:GetName() .. self._data:GetDesc()

    if self._select == DailyConfig.REWARD_BACK_TYPE_EX then--钻石找回
        self._maxNum = exNum + commonNum
        self._findSlider:SetMaxValue(self._maxNum)
        if commonNum == 0 then
            self._findSlider:SetValue(exNum)
            self._buyPriceText.text = self._data:GetBuyTotalText(exNum)
        else
            self._findSlider:SetValue(commonNum)
            self._buyPriceText.text = self._data:GetBuyTotalText(commonNum)
        end
    elseif self._select == DailyConfig.REWARD_BACK_TYPE_COMMON then--金币找回
        self._maxNum = commonNum
        self._findSlider:SetMaxValue(self._maxNum)
        self._findSlider:SetValue(self._maxNum)
        self._buyPriceText.text = self._data:GetBuyTotalText(self._maxNum)
    end

    if self._maxNum == 1 then
        self._findSlider:SetMinValue(0)
        self._findSlider:SetEnabled(false)
    else        
        self._findSlider:SetMinValue(1)
        self._findSlider:SetEnabled(true)
    end
    

    self._exTipsGo:SetActive(self._select == DailyConfig.REWARD_BACK_TYPE_EX)
end

--资源找回窗口关闭点击处理
function DailyFindView:OnFindBackCloseClick()
    self._findBackContainer:SetActive(false)
end

--资源购买次数变化
function DailyFindView:BuyNumChange()
    -- LoggerHelper.Error("DailyFindView:BuyNumChange()" .. tostring(self._findSlider:GetValue()))
    -- LoggerHelper.Error("text ========" ..self._data:GetBuyTotalText(self._findSlider:GetValue()))
    self._buyPriceText.text = self._data:GetBuyTotalText(self._findSlider:GetValue())
end

--资源找回增加次数
function DailyFindView:OnFindBackPlusClick()
    -- LoggerHelper.Error("DailyFindView:OnFindBackPlusClick()")
    local num = self._findSlider:GetValue()
    if num == self._maxNum then return end
    self._findSlider:SetValue(num + 1)
    self._buyPriceText.text = self._data:GetBuyTotalText(num + 1)
end

--资源找回减少次数
function DailyFindView:OnFindBackReduceClick()
    -- LoggerHelper.Error("DailyFindView:OnFindBackReduceClick()")
    local num = self._findSlider:GetValue()
    if num == 1 then return end
    self._findSlider:SetValue(num -1)
    self._buyPriceText.text = self._data:GetBuyTotalText(num -1)
end

--请求资源找回
function DailyFindView:OnFindBackReq()
    -- LoggerHelper.Error("self._findSlider:GetValue() ==" .. tostring(self._findSlider:GetValue()))
    DailyActivityExManager:GetRewardBack(self._data:GetId(), self._data:GetReqType(), self._findSlider:GetValue())
end

--资源返回结果返回
function DailyFindView:GetRewardBackComplete()
    self._findBackContainer:SetActive(false)
end


function DailyFindView:OnDisable()
    self._select = nil
    DailyActivityExManager:RegisterExFindBackFunc(nil)
    DailyActivityExManager:RegisterCommonFindBackFunc(nil)
    self:RemoveEventListeners()
end


function DailyFindView:OnMenuTabBarClick(index)
    self._select = index
    DailyActivityExManager:GetRewardBackInfo()
end

function DailyFindView:UpdateExFindBackRepoint(flag)
    self._exRedpoint:SetActive(flag)
end

function DailyFindView:UpdateCommonFindBackRepoint(flag)
    self._commonRedpoint:SetActive(flag)
end