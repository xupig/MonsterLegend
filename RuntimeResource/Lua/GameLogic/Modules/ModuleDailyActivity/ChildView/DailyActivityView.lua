-- DailyActivityView.lua
local DailyActivityView = Class.DailyActivityView(ClassTypes.BaseComponent)

require "Modules.ModuleDailyActivity.ChildComponent.DailyActivityTaskItem"
require "Modules.ModuleDailyActivity.ChildComponent.LimitActivityTaskItem"
require "Modules.ModuleDailyActivity.ChildComponent.ActivityRewardsTaskItem"

local DailyActivityTaskItem = ClassTypes.DailyActivityTaskItem
local LimitActivityTaskItem = ClassTypes.LimitActivityTaskItem
local ActivityRewardsTaskItem = ClassTypes.ActivityRewardsTaskItem

DailyActivityView.interface = GameConfig.ComponentsConfig.Component
DailyActivityView.classPath = "Modules.ModuleDailyActivity.ChildView.DailyActivityView"
local ActivityDataHelper = GameDataHelper.ActivityDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil

local action_config = require("ServerConfig/action_config")
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local DailyActivityManager = PlayerManager.DailyActivityManager
local ActivityInfoManager = PlayerManager.ActivityInfoManager
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig

local DAILYLIST = "DAILYLIST"
local LIMITLIST = "LIMITLIST"

function DailyActivityView:Awake()
    self._descText = self:GetChildComponent("Container_Desc/Text_DescContent", "TextMeshWrapper")

    self._showActivityButton = self:FindChildGO("Container_Desc/Button_ShowDetials")
    self._csBH:AddClick(self._showActivityButton, function() self:OnShowActivityButtonClick() end)

    self:InitDailyList()
    self:InitLimitList()
    self:InitRewardList()  
    
    self._showActivityButton:SetActive(false)
end

function DailyActivityView:OnEnable()
    if self._onGetAllActivityTime == nil then self._onGetAllActivityTime = function(data) self:OnGetAllActivityTime(data) end end
    EventDispatcher:AddEventListener(GameEvents.OnGetAllActivityTime, self._onGetAllActivityTime)
    self:ShowInfo()    
end

function DailyActivityView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.OnGetAllActivityTime, self._onGetAllActivityTime)
end

function DailyActivityView:InitDailyList()
    local dailyListScrollView, dailyList = UIList.AddScrollViewList(self.gameObject, "Container_DailyList")
    self._dailyList = dailyList
    self._dailyListScrollView = dailyListScrollView
    self._dailyListScrollView:SetHorizontalMove(false)
    self._dailyListScrollView:SetVerticalMove(true)
    self._dailyList:SetItemType(DailyActivityTaskItem)
    self._dailyList:SetPadding(0, 0, 0, 0)
    self._dailyList:SetGap(8, 24)
    self._dailyList:SetDirection(UIList.DirectionLeftToRight, 2, 100)
    
    local itemClickedCB = 
	function(idx)
		self:OnDailyListItemClicked(idx)
	end
	self._dailyList:SetItemClickedCB(itemClickedCB)
end

function DailyActivityView:OnDailyListItemClicked(idx)
    self:HandleListItemClicked(idx, DAILYLIST)
end

function DailyActivityView:InitLimitList()
    local limitListScrollView, limitList = UIList.AddScrollViewList(self.gameObject, "Container_LimitList")
    self._limitList = limitList
    self._limitListScrollView = limitListScrollView
    self._limitListScrollView:SetHorizontalMove(false)
    self._limitListScrollView:SetVerticalMove(true)
    self._limitList:SetItemType(LimitActivityTaskItem)
    self._limitList:SetPadding(0, 0, 0, 0)
    self._limitList:SetGap(8, 5)
    self._limitList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    local itemClickedCB = 
	function(idx)
		self:OnLimitListItemClicked(idx)
	end
	self._limitList:SetItemClickedCB(itemClickedCB)
end

function DailyActivityView:OnLimitListItemClicked(idx)
    self:HandleListItemClicked(idx, LIMITLIST)
end

function DailyActivityView:InitRewardList()
    local rewardScrollView, rewardList = UIList.AddScrollViewList(self.gameObject, "Container_Desc/Container_Rewards/ScrollViewList")
    self._rewardList = rewardList
    self._rewardListScrollView = rewardScrollView
    self._rewardListScrollView:SetHorizontalMove(true)
    self._rewardListScrollView:SetVerticalMove(false)
    self._rewardList:SetItemType(ActivityRewardsTaskItem)
    self._rewardList:SetPadding(0, 0, 0, 0)
    self._rewardList:SetGap(0, 0)
    self._rewardList:SetDirection(UIList.DirectionLeftToRight, -1, 1)

    self._rewardList:SetDataList({})
end

function DailyActivityView:HandleListItemClicked(idx, type)
    if self._selectType ~= nil then
        if self._selectType == DAILYLIST then
            self._dailyList:GetItem(self._selectIndex):SetSelect(false)
        elseif self._selectType == LIMITLIST then
            self._limitList:GetItem(self._selectIndex):SetSelect(false)
        end
    end
    self._selectType = type
    self._selectIndex = idx
    if type == DAILYLIST then
        self._dailyList:GetItem(self._selectIndex):SetSelect(true)
        self._selectId = self._dailyListData[idx+1]
    elseif type == LIMITLIST then
        self._limitList:GetItem(self._selectIndex):SetSelect(true)
        self._selectId = self._limitListData[idx+1][1]
    end
    
    self._descText.text = LanguageDataHelper.CreateContent(ActivityDataHelper:GetDesc(self._selectId))

    self._rewardListData = ActivityDataHelper:GetRewardPreview(self._selectId)
    self._rewardList:SetDataList(self._rewardListData)

    self._showActivityButton:SetActive(true)
end

function DailyActivityView:ShowInfo()
    local activityIds = ActivityDataHelper:GetAllId()
    self._limitListData = {}
    self._dailyListData = {}
    for _, id in ipairs(activityIds) do
        local activityType = ActivityDataHelper:GetType(id)
        if activityType == 3 then -- 限时
            table.insert(self._limitListData, id)
        else -- 日常、周常
            table.insert(self._dailyListData, id)
        end
    end
    -- self._limitList:SetDataList(self._limitListData)
    self._dailyList:SetDataList(self._dailyListData)
    ActivityInfoManager:GetAllActivityTimeReq()
end

function DailyActivityView:OnGetAllActivityTime(args)
    -- LoggerHelper.Log("sam DailyActivityView:OnGetAllActivityTime(args)" .. PrintTable:TableToStr(args))
    local datas = {}
    for i,v in ipairs(self._limitListData) do
        local data = {}
        table.insert(data, v)
        local timeId = args:GetDataById(v)
        table.insert(data, args:GetDataById(v))  
        table.insert(datas, data)     
    end
    self._limitListData = datas
    self._limitList:SetDataList(datas)
end

function DailyActivityView:OnShowActivityButtonClick()
    local item
    if self._selectType == DAILYLIST then
        item = self._dailyList:GetItem(self._selectIndex)
    elseif self._selectType == LIMITLIST then
        item = self._limitList:GetItem(self._selectIndex)
    end
    item:JoinBtnClick()
end

function DailyActivityView:OnDestroy()
    self._dailyList = nil
    self._dailyListScrollView = nil
    self._dailyListData = nil
    self._limitList = nil
    self._limitListScrollView = nil
    self._limitListData = nil
    self._rewardList = nil
    self._rewardListScrollView = nil
    self._rewardListData = nil
    self._selectType = nil 
    self._selectIndex = nil
    self._selectId = nil
    self._onGetAllActivityTime = nil
end
