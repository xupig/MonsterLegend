require "Modules.ModuleDailyCharge.ChildComponent.DailyChargeRewardItem"
require "Modules.ModuleDailyCharge.ChildComponent.DailyChargeRewardTotalItem"
require "Modules.ModuleDailyCharge.ChildComponent.DailyChargeGradeItem"

local DailyChargePanel = Class.DailyChargePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DailyChargeRewardItem = ClassTypes.DailyChargeRewardItem
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DailyChargeRewardTotalItem = ClassTypes.DailyChargeRewardTotalItem
local DailyChargeGradeItem = ClassTypes.DailyChargeGradeItem
local DailyChargeDataHelper = GameDataHelper.DailyChargeDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local DailyChargeManager = PlayerManager.DailyChargeManager
local XImageFlowLight = GameMain.XImageFlowLight

local MARK_OPEN_SERVER_DAYS = 7

--override
function DailyChargePanel:Awake()
    self._preSelected = -1
    self.state = 0
    self:InitView()
    self:InitListenerFunc()
end

function DailyChargePanel:InitListenerFunc()
    self._refreshDailyChargeRewardRecord = function() self:ShowReward() end
    self._refreshDailyChargeCumulativeRecord = function() self:RefreshCumulativeRecord() end
end


--override
function DailyChargePanel:OnDestroy()

end

function DailyChargePanel:OnShow(data)
    self:AddEventListeners()
    self:RefreshView()
end

function DailyChargePanel:OnClose()
    self:RemoveEventListeners()
end

function DailyChargePanel:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.Refresh_Daily_Charge_Reward_Record, self._refreshDailyChargeRewardRecord)

    EventDispatcher:AddEventListener(GameEvents.Refresh_Daily_Charge_Cumulative_Record, self._refreshDailyChargeCumulativeRecord)
end

function DailyChargePanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Daily_Charge_Reward_Record, self._refreshDailyChargeRewardRecord)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Daily_Charge_Cumulative_Record, self._refreshDailyChargeCumulativeRecord)
end

function DailyChargePanel:InitView()
    self._closeBtn = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeBtn, function() self:OnCloseButton() end)
    self._actionBtn = self:FindChildGO("Button_Action")
    self._csBH:AddClick(self._actionBtn, function() self:OnActionButton() end)
    self._actionBtnText = self:GetChildComponent("Button_Action/Text", "TextMeshWrapper")
    self._actionBtnWrapper = self._actionBtn:GetComponent("ButtonWrapper")
    self._actionBtnWrapper.interactable = false

    self._actionButtonFlowLight = self:AddChildComponent("Button_Action", XImageFlowLight)
    self._actionButtonFlowLight.enabled = false

    self._redPointImage = self:FindChildGO("Button_Action/Image_RedPoint")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(DailyChargeRewardItem)
    self._list:SetPadding(16, 0, 0, 12)
    self._list:SetGap(5, 20)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 2)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)

    local scrollViewTotal, listTotal = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Reward")
    self._listTotal = listTotal
	self._scrollViewTotal = scrollViewTotal
    self._listTotal:SetItemType(DailyChargeRewardTotalItem)
    self._listTotal:SetPadding(10, 0, 5, 18)
    self._listTotal:SetGap(5, 0)
    self._listTotal:SetDirection(UIList.DirectionTopToDown, 1, 1)

    local scrollViewGrade, listGrade = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Grade")
    self._listGrade = listGrade
	self._scrollViewGrade = scrollViewGrade
    self._listGrade:SetItemType(DailyChargeGradeItem)
    self._listGrade:SetPadding(0, 0, 0, 6)
    self._listGrade:SetGap(0, 10)
    self._listGrade:SetDirection(UIList.DirectionLeftToRight, 100, 1)
    scrollViewGrade:SetHorizontalMove(true)
    scrollViewGrade:SetVerticalMove(false)
    scrollViewGrade:SetScrollRectEnable(false)

    local itemClickedCB = function(idx) self:OnListItemClicked(idx) end
	self._listGrade:SetItemClickedCB(itemClickedCB)
end

function DailyChargePanel:OnListItemClicked(idx)
    if self._preSelected ~= -1 then
        self._listGrade:GetItem(self._preSelected):ShowSelected(false)
    end
    self._listGrade:GetItem(idx):ShowSelected(true)
    self._preSelected = idx
    local itemData = self._listGrade:GetDataByIndex(idx)

    local openDays = DateTimeUtil.GetServerOpenDays()

    if openDays <= MARK_OPEN_SERVER_DAYS then
        local cfg = DailyChargeDataHelper:GetMorrowDailyChargeDataByArgs(itemData, openDays)
        self._list:SetDataList(cfg.reward)
    else
        local chargeDays = (openDays - MARK_OPEN_SERVER_DAYS - 1) % 3 + 1
        local cfg = DailyChargeDataHelper:GetDailyChargeDataByArgs(itemData, chargeDays, GameWorld.Player().world_level)
        self._list:SetDataList(cfg.reward)
    end
    self:RefreshButtonState(itemData)
end

function DailyChargePanel:OnCloseButton()
    GUIManager.ClosePanel(PanelsConfig.DailyCharge)
end

function DailyChargePanel:OnActionButton()
    if self.state == 0 then
        self:OnCloseButton()
        GUIManager.ShowPanelByFunctionId(43)
    elseif self.state == 2 then
        local itemData = self._listGrade:GetDataByIndex(self._preSelected)
        DailyChargeManager:GetChargeReward(itemData)
    end
end

function DailyChargePanel:RefreshView()
    self:RefreshCumulativeRecord()
    self._listGrade:SetDataList(GlobalParamsHelper.GetParamValue(766))
    self:ShowReward()
end

function DailyChargePanel:ShowReward()
    local gradeData = GlobalParamsHelper.GetParamValue(766)
    local showIndex = #gradeData
    for i=#gradeData,1,-1 do
        if GameWorld.Player().daily_charge_reward_record[gradeData[i]] == 0 then
            showIndex = i
            break
        end
    end
    self:OnListItemClicked(showIndex-1)
end

function DailyChargePanel:RefreshButtonState(cost)
    if GameWorld.Player().daily_charge_reward_record[cost] == 1 then
        --已领取
        self._actionBtnText.text = LanguageDataHelper.CreateContent(76903)
        self._actionBtnWrapper.interactable = false
        self.state = 1
        self._redPointImage:SetActive(false)
        self._actionButtonFlowLight.enabled = false
    elseif GameWorld.Player().daily_charge_reward_record[cost] == nil then
        --未达到
        self._actionBtnText.text = LanguageDataHelper.CreateContent(76901)
        self._actionBtnWrapper.interactable = true
        self.state = 0
        self._redPointImage:SetActive(false)
        self._actionButtonFlowLight.enabled = true
    elseif GameWorld.Player().daily_charge_reward_record[cost] == 0 then
        --可领取
        self.state = 2
        self._actionBtnText.text = LanguageDataHelper.CreateContent(76902)
        self._actionBtnWrapper.interactable = true
        self._redPointImage:SetActive(true)
        self._actionButtonFlowLight.enabled = true
    end
end

function DailyChargePanel:RefreshCumulativeRecord()
    local totalData = DailyChargeDataHelper:GetAllCumulativeDailyChargeData()
    self._listTotal:SetDataList(totalData)
end

function DailyChargePanel:WinBGType()
    return PanelWinBGType.NoBG
end