--DailyChargeModule.lua
DailyChargeModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function DailyChargeModule.Init()
     GUIManager.AddPanel(PanelsConfig.DailyCharge,"Panel_DailyCharge",GUILayer.LayerUIPanel,"Modules.ModuleDailyCharge.DailyChargePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return DailyChargeModule