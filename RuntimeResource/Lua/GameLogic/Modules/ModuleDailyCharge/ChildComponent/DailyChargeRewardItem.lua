require "UIComponent.Extend.ItemGrid"

local DailyChargeRewardItem = Class.DailyChargeRewardItem(ClassTypes.UIListItem)

DailyChargeRewardItem.interface = GameConfig.ComponentsConfig.Component
DailyChargeRewardItem.classPath = "Modules.ModuleDailyCharge.ChildComponent.DailyChargeRewardItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local GUIManager = GameManager.GUIManager

function DailyChargeRewardItem:Awake()
    self:InitView()
end

--override
function DailyChargeRewardItem:OnDestroy() 

end

function DailyChargeRewardItem:InitView()
    self._parent = self:FindChildGO("Container_Icon")
    local itemGo = GUIManager.AddItem(self._parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function()
                    TipsManager:ShowItemTipsById(self.itemId)
                end
    self._icon:SetPointerDownCB(self._func)
end

--data {itemId, count}
function DailyChargeRewardItem:OnRefreshData(data)
    self.data = data
    self.itemId = data[1]
    self._icon:SetItem(self.itemId, data[2])
    self._icon:SetBind(true)
end