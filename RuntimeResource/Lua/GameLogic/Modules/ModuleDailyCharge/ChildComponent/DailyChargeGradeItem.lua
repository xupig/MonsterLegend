local DailyChargeGradeItem = Class.DailyChargeGradeItem(ClassTypes.UIListItem)

DailyChargeGradeItem.interface = GameConfig.ComponentsConfig.Component
DailyChargeGradeItem.classPath = "Modules.ModuleDailyCharge.ChildComponent.DailyChargeGradeItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager

function DailyChargeGradeItem:Awake()
    self:InitView()
end

--override
function DailyChargeGradeItem:OnDestroy() 

end

function DailyChargeGradeItem:InitView()
    self._notSelected = self:FindChildGO("Background")
    self._selected = self:FindChildGO("Checkmark")
    self._selected:SetActive(false)
    self._notSelected:SetActive(true)

    self._notSelectedText = self:GetChildComponent("Background/Label", "TextMeshWrapper")
    self._selectedText = self:GetChildComponent("Checkmark/Label", "TextMeshWrapper")
end

--data {money}
function DailyChargeGradeItem:OnRefreshData(data)
    self.data = data
    self._notSelectedText.text = LanguageDataHelper.CreateContentWithArgs(76895, {["0"]=data})
    self._selectedText.text = LanguageDataHelper.CreateContentWithArgs(76895, {["0"]=data})
end

function DailyChargeGradeItem:ShowSelected(state)
    self._selected:SetActive(state)
    self._notSelected:SetActive(not state)
end