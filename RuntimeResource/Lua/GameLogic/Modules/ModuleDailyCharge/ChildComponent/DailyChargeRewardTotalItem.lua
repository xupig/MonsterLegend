require "UIComponent.Extend.ItemGrid"

local DailyChargeRewardTotalItem = Class.DailyChargeRewardTotalItem(ClassTypes.UIListItem)

DailyChargeRewardTotalItem.interface = GameConfig.ComponentsConfig.Component
DailyChargeRewardTotalItem.classPath = "Modules.ModuleDailyCharge.ChildComponent.DailyChargeRewardTotalItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local GUIManager = GameManager.GUIManager
local DailyChargeDataHelper = GameDataHelper.DailyChargeDataHelper
local DailyChargeManager = PlayerManager.DailyChargeManager

function DailyChargeRewardTotalItem:Awake()
    self:InitView()
end

--override
function DailyChargeRewardTotalItem:OnDestroy() 

end

function DailyChargeRewardTotalItem:InitView()
    self._parent = self:FindChildGO("Container_Icon")
    local itemGo = GUIManager.AddItem(self._parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function()
                    TipsManager:ShowItemTipsById(self.itemId)
                end
    self._icon:SetPointerDownCB(self._func)

    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")

    self._getBtn = self:FindChildGO("Button_Get")
    self._csBH:AddClick(self._getBtn, function() self:OnGetButton() end)
    self._getBtnText = self:GetChildComponent("Button_Get/Text", "TextMeshWrapper")
    self._getBtnWrapper = self._getBtn:GetComponent("ButtonWrapper")
    self._getBtnWrapper.interactable = false
end

--data {id}
function DailyChargeRewardTotalItem:OnRefreshData(data)
    self.data = data

    local chargeDay = 0
    for k,v in pairs(GameWorld.Player().daily_charge_cumulative_record) do
        if v ~= nil then
            chargeDay = chargeDay + 1
        end
    end

    local count
    local cfg = DailyChargeDataHelper:GetCumulativeDailyChargeData(data[1])
    self.day = cfg.day
    self.itemId, count = next(cfg.reward)
    self._icon:SetItem(self.itemId, count)
    self._icon:SetBind(true)
    self._infoText.text = LanguageDataHelper.CreateContentWithArgs(76891, {["0"]= cfg.day, ["1"]= chargeDay, ["2"]= cfg.day, ["3"]= cfg.cost})
    self:RefreshButtonState()
end

function DailyChargeRewardTotalItem:RefreshButtonState()
    if GameWorld.Player().daily_charge_cumulative_record[self.day] == 0 then
        self._getBtnWrapper.interactable = true
        self._getBtnText.text = LanguageDataHelper.CreateContent(123)
    elseif GameWorld.Player().daily_charge_cumulative_record[self.day] == 1 then
        self._getBtnWrapper.interactable = false
        self._getBtnText.text = LanguageDataHelper.CreateContent(76903)
    else
        self._getBtnWrapper.interactable = false
        self._getBtnText.text = LanguageDataHelper.CreateContent(123)
    end
end

function DailyChargeRewardTotalItem:OnGetButton()
    if GameWorld.Player().daily_charge_cumulative_record[self.day] == 0 then
        DailyChargeManager:GetChargeCumulativeReward(self.day)
    end
end