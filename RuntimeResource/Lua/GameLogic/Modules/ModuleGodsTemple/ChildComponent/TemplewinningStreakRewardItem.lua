require "UIComponent.Extend.ItemGrid"
require "Modules.ModuleGodsTemple.ChildComponent.TempleRewardItem"

local TemplewinningStreakRewardItem = Class.TemplewinningStreakRewardItem(ClassTypes.UIListItem)

TemplewinningStreakRewardItem.interface = GameConfig.ComponentsConfig.Component
TemplewinningStreakRewardItem.classPath = "Modules.ModuleGodsTemple.ChildComponent.TemplewinningStreakRewardItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local GUIManager = GameManager.GUIManager
local GuildDataHelper = GameDataHelper.GuildDataHelper
local guildConquestData = PlayerManager.PlayerDataManager.guildConquestData
local TempleRewardItem = ClassTypes.TempleRewardItem
local UIList = ClassTypes.UIList

function TemplewinningStreakRewardItem:Awake()
    self:InitView()
end

--override
function TemplewinningStreakRewardItem:OnDestroy() 

end

function TemplewinningStreakRewardItem:InitView()
    self._actionGO = self:FindChildGO("Button_Action")
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Reward")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(TempleRewardItem)
    self._list:SetPadding(10, 0, 0, 10)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
end

--data {win, id}
function TemplewinningStreakRewardItem:OnRefreshData(data)
    self.data = data
    local winCount = guildConquestData.templeData[public_config.OVERLORD_KEY_GUILD_WIN_NUM]
    self._actionGO:SetActive(false)
    if winCount == nil and winCount == data[1] then
        self._actionGO:SetActive(true)
    end
    self._infoText.text = LanguageDataHelper.CreateContentWithArgs(80410, {["0"]=data[1]})

    local listData = {}
    local cfg = GuildDataHelper:GetDominateGuildRewardData(data[2])
    for k,v in pairs(cfg.reward) do
        table.insert(listData, {k,v})
    end
    self._list:SetDataList(listData)
end