require "UIComponent.Extend.ItemGrid"

local TempleRewardItem = Class.TempleRewardItem(ClassTypes.UIListItem)

TempleRewardItem.interface = GameConfig.ComponentsConfig.Component
TempleRewardItem.classPath = "Modules.ModuleGodsTemple.ChildComponent.TempleRewardItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local TipsManager = GameManager.TipsManager
local GUIManager = GameManager.GUIManager

function TempleRewardItem:Awake()
    self:InitView()
end

--override
function TempleRewardItem:OnDestroy() 

end

function TempleRewardItem:InitView()
    self._parent = self:FindChildGO("Container_Icon")
    local itemGo = GUIManager.AddItem(self._parent, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._func = function()
                    TipsManager:ShowItemTipsById(self.itemId)
                end
    self._icon:SetPointerDownCB(self._func)
end

--data {itemId, count}
function TempleRewardItem:OnRefreshData(data)
    self.data = data
    self.itemId = data[1]
    self._icon:SetItem(self.itemId, data[2])
end