require "Modules.ModuleGodsTemple.ChildView.TempleView"

local GodsTemplePanel = Class.GodsTemplePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TempleView = ClassTypes.TempleView

--override
function GodsTemplePanel:Awake()
    self:InitComps()
end

--override
function GodsTemplePanel:OnDestroy()

end

function GodsTemplePanel:OnShow(data)
    self:AddEventListeners()
    self:RefreshView()
end

function GodsTemplePanel:OnClose()
    self:RemoveEventListeners()
end

function GodsTemplePanel:AddEventListeners()

end

function GodsTemplePanel:RemoveEventListeners()

end

function GodsTemplePanel:InitComps()
	self:InitView("Container_Temple", TempleView, 1)
end

function GodsTemplePanel:StructingViewInit()
    return true
end

function GodsTemplePanel:RefreshView()

end

function GodsTemplePanel:BaseShowModel()
    self._selectedView:ShowModel(true)
end

function GodsTemplePanel:BaseHideModel()
    self._selectedView:ShowModel(false)
end

function GodsTemplePanel:WinBGType()
    return PanelWinBGType.NormalBG
end