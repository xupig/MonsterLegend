require "Modules.ModuleGodsTemple.ChildView.TemplewinningStreakRewardView"
require "Modules.ModuleGodsTemple.ChildView.TemplewinningStreakEndView"
require "Modules.ModuleGodsTemple.ChildComponent.TempleRewardItem"

local TempleView = Class.TempleView(ClassTypes.BaseLuaUIComponent)

TempleView.interface = GameConfig.ComponentsConfig.Component
TempleView.classPath = "Modules.ModuleGodsTemple.ChildView.TempleView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildConquestManager = PlayerManager.GuildConquestManager
local guildConquestData = PlayerManager.PlayerDataManager.guildConquestData
local public_config = require("ServerConfig/public_config")
local GuildDataHelper = GameDataHelper.GuildDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local NobilityDataHelper = GameDataHelper.NobilityDataHelper
local TemplewinningStreakRewardView = ClassTypes.TemplewinningStreakRewardView
local TemplewinningStreakEndView = ClassTypes.TemplewinningStreakEndView
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ActorModelComponent = GameMain.ActorModelComponent
local TempleRewardItem = ClassTypes.TempleRewardItem
local GuildManager = PlayerManager.GuildManager

function TempleView:Awake()
    self:InitView()
    self:InitListenerFunc()
end

function TempleView:InitListenerFunc()
    self._refreshGuildTempleInfo = function() self:RefreshView() end
    self._refreshGuildTempleRewardState = function() self:RefreshRewardButton() end
    self._showModel = function(state) self:ShowModel(state) end
end

function TempleView:OnDestroy()

end

function TempleView:OnEnable()
    -- self:AddEventListeners()
end

function TempleView:OnDisable()
    -- self:RemoveEventListeners()
end

function TempleView:CloseView()
    self:RemoveEventListeners()
end

function TempleView:ShowView(data)
    self:AddEventListeners()
    GuildConquestManager:GuildTempleInfoReq()
end

function TempleView:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Temple_Info, self._refreshGuildTempleInfo)

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Temple_Reward_State, self._refreshGuildTempleRewardState)

    EventDispatcher:AddEventListener(GameEvents.Guild_Temple_Show_Model, self._showModel)
end

function TempleView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Temple_Info, self._refreshGuildTempleInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Temple_Reward_State, self._refreshGuildTempleRewardState)
    EventDispatcher:RemoveEventListener(GameEvents.Guild_Temple_Show_Model, self._showModel)
end

function TempleView:InitView()
    self._watchButton = self:FindChildGO("Button_Watch")
    self._csBH:AddClick(self._watchButton,function() self:OnWatchButtonClick() end)
    self._rewardButton = self:FindChildGO("Button_Reward")
    self._csBH:AddClick(self._rewardButton,function() self:OnRewardButtonClick() end)
    self._rewardBtnWrapper = self._rewardButton:GetComponent("ButtonWrapper")
    self._rewardBtnText = self:GetChildComponent("Button_Reward/Text", "TextMeshWrapper")
    self._redPointImage = self:FindChildGO("Button_Reward/Image_RedPoint")

    self._nobilityIcon = self:FindChildGO("Container_Name/Container_Icon")
    self._presidentNameText = self:GetChildComponent("Container_Name/Text_Name", "TextMeshWrapper")

    --self._noModelImage = self:FindChildGO("Container_President/Image_NoModel")
    self._noModelIconObj = self:FindChildGO("Container_President/Container_NoModelIcon")

    self._modelContainer = self:FindChildGO("Container_President/Container_Model")
    self._modelComponent = self:AddChildComponent('Container_President/Container_Model', ActorModelComponent)
    self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)

    self._guildNameText = self:GetChildComponent("Container_Guild/Text_Name", "TextMeshWrapper")

    self._winCountText = self:GetChildComponent("Container_Win/Text_Info", "TextMeshWrapper")
    self._winRewardButton = self:FindChildGO("Container_Win/Container_WinReward/Button_Reward")
    self._csBH:AddClick(self._winRewardButton,function() self:OnWinRewardButtonClick() end)
    self._winEndButton = self:FindChildGO("Container_Win/Container_WinEnd/Button_End")
    self._csBH:AddClick(self._winEndButton,function() self:OnWinEndButtonClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Right/ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(TempleRewardItem)
    self._list:SetPadding(10, 0, 0, 10)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)

    local scrollViewReward, listReward = UIList.AddScrollViewList(self.gameObject, "Container_Reward/ScrollViewList")
    self._listReward = listReward
	self._scrollViewReward = scrollViewReward
    self._listReward:SetItemType(TempleRewardItem)
    self._listReward:SetPadding(10, 0, 0, 10)
    self._listReward:SetGap(5, 10)
    self._listReward:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    scrollViewReward:SetHorizontalMove(true)
    scrollViewReward:SetVerticalMove(false)

    self._winningStreakRewardGO = self:FindChildGO("Container_WinningStreakReward")
    self._winningStreakRewardView = self:AddChildLuaUIComponent("Container_WinningStreakReward", TemplewinningStreakRewardView)
    self._winningStreakRewardGO:SetActive(false)

    self._winningStreakEndGO = self:FindChildGO("Container_WinningStreakEnd")
    self._winningStreakEndView = self:AddChildLuaUIComponent("Container_WinningStreakEnd", TemplewinningStreakEndView)
    self._winningStreakEndGO:SetActive(false)
end

function TempleView:RefreshView()
    if GuildConquestManager:HasTempleData() then
        self:ShowTempleView()
    else
        self:ShowDefaultView()
    end

    self:RefreshRewardButton()

    local listData = {}
    for k,v in pairs(GlobalParamsHelper.GetParamValue(746)) do
        table.insert(listData, {k,v})
    end
    self._list:SetDataList(listData)

    local listRewardData = {}
    for k,v in pairs(GlobalParamsHelper.GetParamValue(748)) do
        table.insert(listRewardData, {k,v})
    end
    self._listReward:SetDataList(listRewardData)
end

function TempleView:OnWatchButtonClick()
    GUIManager.ClosePanel(PanelsConfig.GodsTemple)
    if GuildManager:IsInGuild() then
        GUIManager.ShowPanelByFunctionId(417)  
    else
        GUIManager.ShowPanel(PanelsConfig.ApplyForGuild)
    end
end

function TempleView:OnRewardButtonClick()
    GuildConquestManager:GuildTempleRewardReq()
end

function TempleView:OnWinRewardButtonClick()
    self._winningStreakRewardGO:SetActive(true)
    self._winningStreakRewardView:ShowView()
end

function TempleView:OnWinEndButtonClick()
    self._winningStreakEndGO:SetActive(true)
    self._winningStreakEndView:ShowView()
end

function TempleView:ShowModel(state)
    if state then
        self._modelComponent:Show()
    else
        self._modelComponent:Hide()
    end
end

function TempleView:RefreshRewardButton()
    if not GuildConquestManager:IsSelfGuild() then
        self._rewardBtnText.text = LanguageDataHelper.CreateContentWithArgs(80404)
        self._rewardBtnWrapper.interactable = false
        self._redPointImage:SetActive(false)
        return
    end
    if guildConquestData.templeRewardState == 1 then
        self._rewardBtnText.text = LanguageDataHelper.CreateContentWithArgs(80418)
        self._rewardBtnWrapper.interactable = false
        self._redPointImage:SetActive(false)
    else
        self._rewardBtnText.text = LanguageDataHelper.CreateContentWithArgs(80404)
        self._rewardBtnWrapper.interactable = true
        self._redPointImage:SetActive(true)
    end
end

function TempleView:ShowDefaultView()
    self._guildNameText.text = LanguageDataHelper.CreateContentWithArgs(80409)
    self._presidentNameText.text = LanguageDataHelper.CreateContentWithArgs(80409)
    self._winCountText.text = LanguageDataHelper.CreateContentWithArgs(80408)

    self._nobilityIcon:SetActive(false)
    --self._noModelImage:SetActive(true)
    self._noModelIconObj:SetActive(true)
    GameWorld.AddIcon(self._noModelIconObj,13551)
    self._modelContainer:SetActive(false)
end

function TempleView:ShowTempleView()
    local templeData = guildConquestData.templeData
    self._guildNameText.text = templeData[public_config.OVERLORD_KEY_GUILD_NAME]
    self._presidentNameText.text = templeData[public_config.OVERLORD_KEY_GUILD_PRESIDENT_NAME]
    self._winCountText.text = LanguageDataHelper.CreateContentWithArgs(80407, {["0"]=templeData[public_config.OVERLORD_KEY_GUILD_WIN_NUM]})

    local nobility = templeData[public_config.OVERLORD_KEY_GUILD_PRESIDENT_NOBILITY] or 0
    --爵位
    if nobility ~= 0 then
        local icon = NobilityDataHelper:GetNobilityShow(nobility)
        self._nobilityIcon:SetActive(true)
        GameWorld.AddIcon(self._nobilityIcon, icon, nil)
    else
        self._iconContainer:SetActive(false)
    end

    --self._noModelImage:SetActive(false)
    --GameWorld.RemoveIcon(self._noModelIconObj)
    self._noModelIconObj:SetActive(false)
    self._modelContainer:SetActive(true)

    local facade = templeData[public_config.OVERLORD_KEY_GUILD_PRESIDENT_FACADE]
    local vocation = templeData[public_config.OVERLORD_KEY_GUILD_PRESIDENT_VOCATION] or 101
    self._modelComponent:LoadAvatarModel(vocation, facade, false)
end