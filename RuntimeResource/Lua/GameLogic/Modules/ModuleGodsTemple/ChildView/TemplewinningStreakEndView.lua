require "Modules.ModuleGodsTemple.ChildComponent.TempleRewardItem"

local TemplewinningStreakEndView = Class.TemplewinningStreakEndView(ClassTypes.BaseLuaUIComponent)

TemplewinningStreakEndView.interface = GameConfig.ComponentsConfig.Component
TemplewinningStreakEndView.classPath = "Modules.ModuleGodsTemple.ChildView.TemplewinningStreakEndView"

local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local GuildDataHelper = GameDataHelper.GuildDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local UIList = ClassTypes.UIList
local TempleRewardItem = ClassTypes.TempleRewardItem
local guildConquestData = PlayerManager.PlayerDataManager.guildConquestData

function TemplewinningStreakEndView:Awake()
    self:InitView()
end
--override
function TemplewinningStreakEndView:OnDestroy() 

end

function TemplewinningStreakEndView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function() self:OnCloseButtonClick() end)

    self._endRewardInfoText = self:GetChildComponent("Container_EndReward/Text_Info", "TextMeshWrapper")
    self._inspireInfoText = self:GetChildComponent("Container_Inspire/Text_Info", "TextMeshWrapper")

    self._curRewardInfoText = self:GetChildComponent("Container_CurReward/Text_Info", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_CurReward/ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(TempleRewardItem)
    self._list:SetPadding(10, 0, 10, 16)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function TemplewinningStreakEndView:ShowView()
    local winCount = guildConquestData.templeData[public_config.OVERLORD_KEY_GUILD_WIN_NUM] or 0

    self._endRewardInfoText.text = LanguageDataHelper.CreateContentWithArgs(80416)

    local listData = {}
    if winCount >= 2 then
        local cfg = GuildDataHelper:GetAllDominateGuildRewardData(GameWorld.Player().world_level)
        local cfgId
        for k,v in pairs(cfg) do
            if v[1] == winCount then
                cfgId = v[2]
                break
            end
        end
        local data = GuildDataHelper:GetDominateGuildRewardData(cfgId)
        for k,v in pairs(data.reward_match) do
            table.insert(listData, {k,v})
        end
        local str = LanguageDataHelper.CreateContentWithArgs(80422, {["0"]=data.attri_num})
        self._inspireInfoText.text = LanguageDataHelper.CreateContentWithArgs(80414, {["0"]=str})
    else
        self._inspireInfoText.text = LanguageDataHelper.CreateContentWithArgs(80414, {["0"]=LanguageDataHelper.CreateContentWithArgs(80423)})
    end
    self._list:SetDataList(listData)

    if #listData == 0 then
        self._curRewardInfoText.text = LanguageDataHelper.CreateContentWithArgs(80423)
    else
        self._curRewardInfoText.text = ""
    end

    EventDispatcher:TriggerEvent(GameEvents.Guild_Temple_Show_Model, false)
end

function TemplewinningStreakEndView:OnCloseButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.Guild_Temple_Show_Model, true)
    self.gameObject:SetActive(false)
end