require "Modules.ModuleGodsTemple.ChildComponent.TemplewinningStreakRewardItem"

local TemplewinningStreakRewardView = Class.TemplewinningStreakRewardView(ClassTypes.BaseLuaUIComponent)

TemplewinningStreakRewardView.interface = GameConfig.ComponentsConfig.Component
TemplewinningStreakRewardView.classPath = "Modules.ModuleGodsTemple.ChildView.TemplewinningStreakRewardView"

local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local GuildDataHelper = GameDataHelper.GuildDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TemplewinningStreakRewardItem = ClassTypes.TemplewinningStreakRewardItem
local guildConquestData = PlayerManager.PlayerDataManager.guildConquestData
local UIList = ClassTypes.UIList

function TemplewinningStreakRewardView:Awake()
    self:InitView()
end
--override
function TemplewinningStreakRewardView:OnDestroy() 

end

function TemplewinningStreakRewardView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function() self:OnCloseButtonClick() end)

    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(TemplewinningStreakRewardItem)
    self._list:SetPadding(10, 0, 0, 16)
    self._list:SetGap(5, 10)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, 1)

end

function TemplewinningStreakRewardView:ShowView()
    local data = GuildDataHelper:GetAllDominateGuildRewardData(GameWorld.Player().world_level)
    self._list:SetDataList(data)

    local winCount = guildConquestData.templeData[public_config.OVERLORD_KEY_GUILD_WIN_NUM] or 0
    self._infoText.text = LanguageDataHelper.CreateContentWithArgs(80411, {["0"]=winCount})

    EventDispatcher:TriggerEvent(GameEvents.Guild_Temple_Show_Model, false)
end

function TemplewinningStreakRewardView:OnCloseButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.Guild_Temple_Show_Model, true)
    self.gameObject:SetActive(false)
end