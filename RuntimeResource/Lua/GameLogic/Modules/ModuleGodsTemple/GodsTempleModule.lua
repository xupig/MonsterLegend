GodsTempleModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function GodsTempleModule.Init()
    GUIManager.AddPanel(PanelsConfig.GodsTemple,"Panel_GodsTemple",GUILayer.LayerUIPanel,"Modules.ModuleGodsTemple.GodsTemplePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return GodsTempleModule