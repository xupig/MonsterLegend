--ZeroDollarBuyModule.lua
ZeroDollarBuyModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function ZeroDollarBuyModule.Init()
    GUIManager.AddPanel(PanelsConfig.ZeroDollarBuy,"Panel_ZeroDollarBuy",GUILayer.LayerUIPanel,"Modules.ModuleZeroDollarBuy.ZeroDollarBuyPanel",true,false, nil, true, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return ZeroDollarBuyModule