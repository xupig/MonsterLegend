require "Modules.ModuleZeroDollarBuy.ChildComponent.ZeroDollarBuyRewardItem"

local ZeroDollarBuyPanel = Class.ZeroDollarBuyPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ZeroDollarBuyRewardItem = ClassTypes.ZeroDollarBuyRewardItem
local XArtNumber = GameMain.XArtNumber
local EquipModelComponent = GameMain.EquipModelComponent
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PlayerModelComponent = GameMain.PlayerModelComponent
local action_config = require("ServerConfig/action_config")
local UIParticle = ClassTypes.UIParticle
local XImageFlowLight = GameMain.XImageFlowLight
local UIToggleGroup = ClassTypes.UIToggleGroup
local ZeroDollarBuyDataHelper = GameDataHelper.ZeroDollarBuyDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ActorModelComponent = GameMain.ActorModelComponent
local ZeroDollarBuyManager = PlayerManager.ZeroDollarBuyManager
local TransformDataHelper = GameDataHelper.TransformDataHelper
local max = math.max
local public_config = GameWorld.public_config

local Max_Toggle_Count = 4

--override
function ZeroDollarBuyPanel:Awake()
    self._state = 0
    self._selectTab = -1
    self.state = 1
    self:InitView()
    self:InitListenerFunc()
end

function ZeroDollarBuyPanel:InitListenerFunc()
    self._refreshZeroPriceInfo = function() self:RefreshButtonState() end
    self._countDownFunc = function() self:CountDown() end
    self._onLevelChangeCB = function()self:RefreshButtonState() end
end


--override
function ZeroDollarBuyPanel:OnDestroy()

end

function ZeroDollarBuyPanel:OnShow(data)
    self:AddEventListeners()
    self._fx1.gameObject:SetActive(true)
    self:ShowModel()
    self.toggle:SetSelectIndex(0)
    self:OnToggleGroupClick(0)

    self._timer = TimerHeap:AddSecTimer(0, 1, 0, self._countDownFunc)
end

function ZeroDollarBuyPanel:OnClose()
    self:RemoveEventListeners()
    GameWorld.ShowPlayer():ShowModel(3)
    GameWorld.ShowPlayer():SyncShowPlayerFacade()
    self._fx1.gameObject:SetActive(false)
    self:HideModel()
    GameWorld.ShowPlayer():ShowModel(3)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer=nil
    end
end

function ZeroDollarBuyPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.REFRESH_ZERO_PRICE_INFO, self._refreshZeroPriceInfo)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
end

function ZeroDollarBuyPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.REFRESH_ZERO_PRICE_INFO, self._refreshZeroPriceInfo)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
end

function ZeroDollarBuyPanel:InitView()
    self._closeBtn = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeBtn, function() self:OnCloseButton() end)
    self._chargeBtn = self:FindChildGO("Button_Action")
    self._csBH:AddClick(self._chargeBtn, function() self:OnChargeButton() end)
    self._chargeBtnText = self:GetChildComponent("Button_Action/Text", "TextMeshWrapper")

    self._redPointImage = self:FindChildGO("Button_Action/Image_RedPoint")
    self._tipsIcon = self:FindChildGO("Container_Icon")
    self._hasGotImage = self:FindChildGO("Image_HasGot")
    self._hasGotImage:SetActive(false)
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")

    self._descText = self:GetChildComponent("Text_Desc", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(ZeroDollarBuyRewardItem)
    self._list:SetPadding(0, 0, 0, 4)
    self._list:SetGap(0, 20)
    self._list:SetDirection(UIList.DirectionLeftToRight, 1, 1)
    scrollView:SetHorizontalMove(true)
    scrollView:SetVerticalMove(false)

    self._showIconGO = self:FindChildGO("Container_ShowItem/Container_Icon")
    self._actorModelGO = self:FindChildGO("Container_ShowItem/Container_Model")
    self._equipModelGO = self:FindChildGO("Container_ShowItem/Container_Equip")
    self._playerModelGO = self:FindChildGO("Container_ShowItem/Container_Player")
    self._actorModelComponent = self:AddChildComponent('Container_ShowItem/Container_Model', ActorModelComponent)
    self._equipModelComponent = self:AddChildComponent('Container_ShowItem/Container_Equip', EquipModelComponent)
    self._playerModelComponent = self:AddChildComponent('Container_ShowItem/Container_Player', PlayerModelComponent)

    self._returnRewardItem = self:AddChildLuaUIComponent("Container_ReturnReward", ZeroDollarBuyRewardItem)

    self._fx1 = UIParticle.AddParticle(self.gameObject, "Container_ShowItem/Container_Fx/FirstCharge")

    self:InitToggleGroup()
end

function ZeroDollarBuyPanel:InitToggleGroup()
    self.toggle = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup")
	self.toggle:SetOnSelectedIndexChangedCB(function(index) self:OnToggleGroupClick(index) end)
    self.toggle:SetSelectIndex(0)
    self:OnToggleGroupClick(0)

    self.toggleIconGO = {}
    for i=1,Max_Toggle_Count do
        self.toggleIconGO[i] = self:FindChildGO("ToggleGroup/Toggle"..i.."/Container_Icon")
        local iconList = ZeroDollarBuyDataHelper:GetIcon(i)
        GameWorld.AddIcon(self.toggleIconGO[i], iconList[1])
    end
end

function ZeroDollarBuyPanel:OnToggleGroupClick(index)
    if self._selectTab == index then
        return
    end
    self._selectTab = index
    self.id = index + 1
    self:RefreshView()
end

function ZeroDollarBuyPanel:OnCloseButton()
    GUIManager.ClosePanel(PanelsConfig.ZeroDollarBuy)
end

function ZeroDollarBuyPanel:OnChargeButton()
    if self.state == 1 then
        --购买
        ZeroDollarBuyManager:ZeroDollarBuyReq(self.id)
    else
        --领取奖励
        ZeroDollarBuyManager:ZeroDollarRebateReq(self.id)
    end
end

function ZeroDollarBuyPanel:RefreshView()
    local item, count = next(ZeroDollarBuyDataHelper:GetReturnReward(self.id))
    self._returnRewardItem:OnRefreshData({item, count})

    if item == public_config.MONEY_TYPE_COUPONS then
        self._descText.text = LanguageDataHelper.CreateContent(80836)
    else 
        self._descText.text = LanguageDataHelper.CreateContent(80837)
    end

    local reward = ZeroDollarBuyDataHelper:GetReward(self.id)
    local cfg = reward[GameWorld.Player():GetVocationGroup()]
    local listData = {}
    for k=1,#cfg-1,2 do
        table.insert(listData, {cfg[k],cfg[k+1]})
    end
    self._list:SetDataList(listData)

    local iconList = ZeroDollarBuyDataHelper:GetIcon(self.id)
    GameWorld.AddIcon(self._tipsIcon, iconList[2])

    self:RefreshModel()
    self:RefreshButtonState()
end

function ZeroDollarBuyPanel:RefreshModel()
    self._showIconGO:SetActive(false)
    self._actorModelGO:SetActive(false)
    self._equipModelGO:SetActive(false)
    self._playerModelGO:SetActive(false)

    local cfg = ZeroDollarBuyDataHelper:GetDisplay(self.id)[GameWorld.Player():GetVocationGroup()]

    local modelInfo
    if cfg[1] == 2 then
        local partnerTransformCfg = TransformDataHelper.GetTreasureTotalCfg(cfg[2])
        local vocationGroup = math.floor(GameWorld.Player().vocation/100)
        local uiModelCfgId = partnerTransformCfg.model_vocation[vocationGroup]
        modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
    end
    if cfg[1] == 1 then
        self._equipModelGO:SetActive(true)
        self._equipModelComponent:SetStartSetting(Vector3(cfg[3], cfg[4], cfg[5]), Vector3(cfg[6], cfg[7], cfg[8]), cfg[9])
        self._equipModelComponent:LoadEquipModel(cfg[2], "")
    elseif cfg[1] == 2 then
        self._actorModelGO:SetActive(true)
        local modelId = modelInfo.model_ui
        local posx = modelInfo.pos[1]
        local posy = modelInfo.pos[2]
        local posz = modelInfo.pos[3]
        local rotationx = modelInfo.rotation[1]
        local rotationy = modelInfo.rotation[2]
        local rotationz = modelInfo.rotation[3]
        local scale = modelInfo.scale
        self._actorModelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
        self._actorModelComponent:LoadModel(modelId)
    elseif cfg[1] == 3 then
        self._showIconGO:SetActive(true)
        -- local iconId = ItemDataHelper.GetIcon(cfg[2])
        GameWorld.AddIcon(self._showIconGO, cfg[2])
    elseif cfg[1] == 4 then
        self._playerModelGO:SetActive(true)
        GameWorld.ShowPlayer():ShowModel(1)
        if cfg[3] ~= 0 then
            GameWorld.ShowPlayer():SetCurHead(cfg[3])
        end
        if cfg[4] ~= 0 then
            GameWorld.ShowPlayer():SetCurCloth(cfg[4])
        end
        if cfg[5] ~= 0 then
            GameWorld.ShowPlayer():SetCurWeapon(cfg[5])
        end
        GameWorld.ShowPlayer():SetCurWing(0)
        self._playerModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, cfg[2], 0), 1)
    elseif cfg[1] == 5 then
        self._actorModelGO:SetActive(true)
        self._actorModelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
        self._actorModelComponent:LoadModel(cfg[2])
    end
end

--state 1:未购买 2：购买还不能领取 3：可领取 4：已领取
function ZeroDollarBuyPanel:RefreshButtonState()
    local player = GameWorld.Player()
    local data = player.zero_price_info[self.id]
    local _, price = next(ZeroDollarBuyDataHelper:GetPrice(self.id))
    self.freeItem = price==0
    self._chargeBtn:SetActive(true)
    self._hasGotImage:SetActive(false)
    self._redPointImage:SetActive(false)
    self.state = 1
    if data ~= nil and data[2] == 1 then
        --已领取
        self.state = 4
        self._chargeBtnText.text = LanguageDataHelper.CreateContent(81656)
        self._chargeBtn:SetActive(false)
        self._hasGotImage:SetActive(true)
    elseif data == nil then
        --未购买
        self.state = 1
        if price == 0 then
            self._chargeBtnText.text = LanguageDataHelper.CreateContent(81655)
            if GameWorld.Player().level >= ZeroDollarBuyDataHelper:GetLevel(self.id) then
                self._redPointImage:SetActive(true)
            end
        else
            self._chargeBtnText.text = LanguageDataHelper.CreateContent(81654)
        end
    elseif self:CanGet(data) then
        --可领取
        self.state = 3
        self._chargeBtnText.text = LanguageDataHelper.CreateContent(81656)
        self._redPointImage:SetActive(true)
    else
        --购买但还不能领取
        self.state = 2
        self._chargeBtnText.text = LanguageDataHelper.CreateContent(81656)
    end
    self:CountDown()
end

function ZeroDollarBuyPanel:CountDown()
    if self.state == 1 then
        if self.freeItem then
            self._infoText.text = LanguageDataHelper.CreateContent(81663)
        else
            local endTime = ZeroDollarBuyManager:GetOpenDaysEndTime()
            local left = max(endTime- DateTimeUtil.GetServerTime(), 0)
            if left<=0 then
                self._infoText.text = ""
            else
                self._infoText.text = LanguageDataHelper.CreateContentWithArgs(81658, {["0"]=DateTimeUtil:FormatFullTime(left)})
            end
        end
    elseif self.state == 2 then
        local returnTime = GameWorld.Player().zero_price_info[self.id][1] + ZeroDollarBuyDataHelper:GetTime(self.id)
        local returnLeftTime = max(returnTime-DateTimeUtil.GetServerTime(), 0)
        if returnLeftTime<=0 then
            self._infoText.text = ""
        else
            self._infoText.text = LanguageDataHelper.CreateContentWithArgs(81657, {["0"]=DateTimeUtil:FormatFullTime(returnLeftTime)})
        end
    else
        self._infoText.text = ""
    end
end

function ZeroDollarBuyPanel:CanGet(data)
    if data == nil then
        return false
    end
    if data[1] + ZeroDollarBuyDataHelper:GetTime(self.id) > DateTimeUtil.GetServerTime() then
        return false
    end
    local level = ZeroDollarBuyDataHelper:GetLevel(self.id)
    if GameWorld.Player().level < level then
        return false
    end
    return true
end

function ZeroDollarBuyPanel:ShowModel()
    self._actorModelComponent:Show()
    self._equipModelComponent:Show()
    self._playerModelComponent:Show()
end

function ZeroDollarBuyPanel:HideModel()
    self._actorModelComponent:Hide()
    self._equipModelComponent:Hide()
    self._playerModelComponent:Hide()
end

function ZeroDollarBuyPanel:WinBGType()
    return PanelWinBGType.NoBG
end