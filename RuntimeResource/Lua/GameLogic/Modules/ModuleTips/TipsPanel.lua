-- TipsPanel.lua

local TipsPanel = Class.TipsPanel(ClassTypes.BasePanel)

require "Modules.ModuleTips.ChildView.SkillTipsView"
require "Modules.ModuleTips.ChildView.DragonSoulTipsView"
require "Modules.ModuleTips.ChildView.StrengthStarGemTipsView"
local SkillTipsView = ClassTypes.SkillTipsView
local DragonSoulTipsView = ClassTypes.DragonSoulTipsView
local StrengthStarGemTipsView = ClassTypes.StrengthStarGemTipsView
local PanelsConfig = GameConfig.PanelsConfig

function TipsPanel:Awake()
	self._csBH = self:GetComponent("LuaUIPanel")
	self._views = {}

	self._btnBg = self:FindChildGO("Button_Bg")
	self._csBH:AddClick(self._btnBg, function() self:OnClickClose() end )
end

function TipsPanel:OnShow(data)
	local index =  data.tipsType
	if self._selectedTipsView then
		self._selectedTipsView:SetActive(false)
	end
	if self._views[index] == nil then
		self:InitViews(index)
	end
	self._selectedTipsView = self._views[index]
	self._selectedTipsView:SetActive(true)
	self._selectedTipsView:UpdateView(data)
end

function TipsPanel:OnClose()
	
end

function TipsPanel:InitViews(index)
	if index == 1 then
		--技能tips
		self._views[1] = self:AddChildLuaUIComponent("Container_SkillTips", SkillTipsView)
	elseif index == 2 then
		--龙魂tips
		self._views[2] = self:AddChildLuaUIComponent("Container_DragonSoul", DragonSoulTipsView)
	elseif index == 3 then
		--强化、星级、宝石tips
		self._views[3] = self:AddChildLuaUIComponent("Container_StrengthStarGem", StrengthStarGemTipsView)
	end
end

function TipsPanel:OnClickClose()
	GameManager.GUIManager.ClosePanel(PanelsConfig.Tips)
end