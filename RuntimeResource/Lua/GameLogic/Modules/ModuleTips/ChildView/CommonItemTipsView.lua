-- CommonItemTipsView.lua
local CommonItemTipsView = Class.CommonItemTipsView(ClassTypes.ItemTipsViewBase)
CommonItemTipsView.interface = GameConfig.ComponentsConfig.Component
CommonItemTipsView.classPath = "Modules.ModuleTips.ChildView.CommonItemTipsView"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local BagManager = PlayerManager.BagManager
local TipsManager = GameManager.TipsManager
local ShopManager = PlayerManager.ShopManager
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local SortOrderedRenderAgent = GameMain.SortOrderedRenderAgent

require "UIComponent.Extend.SimpleCurrencyCom"
local SimpleCurrencyCom = ClassTypes.SimpleCurrencyCom

function CommonItemTipsView:Awake()
    self._csBH = self:GetComponent("LuaUIComponent")
    self:InitBg()
    self:InitBase()
    self:InitButtons()
    self:InitOtherComps()
end

function CommonItemTipsView:InitBg()
    self._tranform = self.transform
    local bg = self:FindChildGO("Image_Bg")
    self._bgRectTransform = bg:GetComponent(typeof(UnityEngine.RectTransform))
    local bgFrame = self:FindChildGO("Image_Frame")
    self._frameRectTransform = bgFrame:GetComponent(typeof(UnityEngine.RectTransform))
end

function CommonItemTipsView:InitBase()
    self._base.InitBase(self)
    
    self._txtUseLv = self:GetChildComponent("Text_Level", 'TextMeshWrapper')
    self._txtVocation = self:GetChildComponent("Text_Vocation", 'TextMeshWrapper')
    self._txtDesc = self:GetChildComponent("Text_Desc", 'TextMeshWrapper')
    
    self._txtAchieveDesc = self:GetChildComponent("Text_AchieveDesc", 'TextMeshWrapper')
    self._txtAchieveDescGO = self:FindChildGO("Text_AchieveDesc")
end

function CommonItemTipsView:InitButtons()
    self._containerBottom = self:FindChildGO("Container_Bottom")
    
    --self._btnTrade 		= self:FindChildGO("Container_Buttons/Button_Trade")
    self._btnUse = self:FindChildGO("Container_Bottom/Button_Use")
    self._btnCombine = self:FindChildGO("Container_Bottom/Button_Combine")
    self._btnSell = self:FindChildGO("Container_Bottom/Button_Sell")
    self._btnSave = self:FindChildGO("Container_Bottom/Button_Save")
    self._btnGet = self:FindChildGO("Container_Bottom/Button_Get")
    self._btnGuildExchange = self:FindChildGO("Container_Bottom/Button_GuildExchange")
    self._btnForSale = self:FindChildGO("Container_Bottom/Button_ForSale")
    self._btnOffSell = self:FindChildGO("Container_Bottom/Button_OffSell")
    self._btnBuy = self:FindChildGO("Container_Bottom/Button_Buy")--交易行购买
    self._btnShopBuy = self:FindChildGO("Container_Bottom/Button_ShopBuy")--商城购买
    self._btnSplit = self:FindChildGO("Container_Bottom/Button_Split")--物品拆分
    self._btnGuideBuy = self:FindChildGO("Container_Bottom/Button_GuideBuy")--引导购买
    self._btnOpenRankBuy = self:FindChildGO("Container_Bottom/Button_OpenRankBuy")--开服限购
    
    self._txtBtnUse = self:GetChildComponent("Container_Bottom/Button_Use/Text", 'TextMeshWrapper')
    --self._csBH:AddClick(self._btnTrade, function() self:OnTradeBtnClick() end)
    self._csBH:AddClick(self._btnUse, function()self:OnUseBtnClick() end)
    self._csBH:AddClick(self._btnCombine, function()self:OnCombineClick() end)
    self._csBH:AddClick(self._btnSell, function()self:OnClickSell() end)
    self._csBH:AddClick(self._btnSave, function()self:OnClickSave() end)
    self._csBH:AddClick(self._btnGet, function()self:OnClickGet() end)
    self._csBH:AddClick(self._btnGuildExchange, function()self:OnClicGuildExchange() end)
    self._csBH:AddClick(self._btnForSale, function()self:OnClickForSale() end)
    self._csBH:AddClick(self._btnOffSell, function()self:OnClickOffSell() end)
    self._csBH:AddClick(self._btnBuy, function()self:OnClickBuy() end)
    self._csBH:AddClick(self._btnShopBuy, function()self:OnClickShopBuy() end)
    self._csBH:AddClick(self._btnSplit, function()self:OnClickForSplit() end)
    self._csBH:AddClick(self._btnGuideBuy, function()self:OnClickGuideBuy() end)
    self._csBH:AddClick(self._btnOpenRankBuy, function()self:OnClickOpenRankBuy() end)

    self._requestUseCb = function ()
        self:RequestUse()
    end
end

function CommonItemTipsView:InitOtherComps()
    -----------公会仓库兑换--------------
    self._containerGuildExchange = self:FindChildGO("Container_Bottom/Container_GuildExchange")
    self._inputFieldMeshGECount = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_Bottom/Container_GuildExchange/InputFieldMesh_Count")
    self._inputFieldMeshGECount:SetOnEndMeshEditCB(function(text)self:OnEndEditGECount(text) end)
    self._inputTextGECount = self:GetChildComponent("Container_Bottom/Container_GuildExchange/InputFieldMesh_Count", 'InputFieldMeshWrapper')
    self._txtGuildPoint = self:GetChildComponent("Container_Bottom/Container_GuildExchange/Container_Point/Text_Num", 'TextMeshWrapper')
    local containerGuildExchangePoint = self:FindChildGO("Container_Bottom/Container_GuildExchange/Container_Point/Container_MoneyIcon")
    GameWorld.AddIcon(containerGuildExchangePoint, 56)
    
    self._btnGEPlus = self:FindChildGO("Container_Bottom/Container_GuildExchange/Button_Plus")
    self._csBH:AddClick(self._btnGEPlus, function()self:OnClicGEPlus() end)
    self._btnGEReduce = self:FindChildGO("Container_Bottom/Container_GuildExchange/Button_Reduce")
    self._csBH:AddClick(self._btnGEReduce, function()self:OnClicGEReduce() end)
    
    self._guildPetItemCfg = GlobalParamsHelper.GetParamValue(592)
    
    -----------商城--------------
    self._containerShopBuy = self:FindChildGO("Container_Bottom/Container_ShopBuy")
    self._inputFieldMeshShopCount = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_Bottom/Container_ShopBuy/InputFieldMesh_Count")
    self._inputFieldMeshShopCount:SetOnEndMeshEditCB(function(text)self:OnEndEditShopCount(text) end)
    self._inputTextShopCount = self:GetChildComponent("Container_Bottom/Container_ShopBuy/InputFieldMesh_Count", 'InputFieldMeshWrapper')
    
    self._shopPriceCom = self:AddChildLuaUIComponent("Container_Bottom/Container_ShopBuy/Container_Diamond", SimpleCurrencyCom)
    self._shopPriceCom:InitComps()
    self._shopPriceCom:SetShowLackMoney(true)
    
    self._btnShopPlus = self:FindChildGO("Container_Bottom/Container_ShopBuy/Button_Plus")
    self._csBH:AddClick(self._btnShopPlus, function()self:OnClickShopPricePlus() end)
    GameWorld.AddHitArea(self._btnShopPlus, 60, 60, 20, 20)

    self._btnShopReduce = self:FindChildGO("Container_Bottom/Container_ShopBuy/Button_Reduce")
    self._csBH:AddClick(self._btnShopReduce, function()self:OnClickShopPriceReduce() end)
    GameWorld.AddHitArea(self._btnShopReduce, 60, 60, 20, 20)

    self._btnShopMax = self:FindChildGO("Container_Bottom/Container_ShopBuy/Button_Max")
    self._csBH:AddClick(self._btnShopMax, function()self:OnClickShopPriceMax() end)
end

--根据功能开放显示按钮
function CommonItemTipsView:FiltButtons()
    local showList = {}
    
    self:AddBtnToShowList(self._buttonArgs.guildExchange, true, showList, self._btnGuildExchange)
    self:AddBtnToShowList(self._buttonArgs.save, true, showList, self._btnSave)
    self:AddBtnToShowList(self._buttonArgs.get, true, showList, self._btnGet)
    self:AddBtnToShowList(self._buttonArgs.use, self:CheckShowUse(), showList, self._btnUse)
    self:AddBtnToShowList(self._buttonArgs.combine, self:CheckShowCombine(), showList, self._btnCombine)
    self:AddBtnToShowList(self._buttonArgs.forSale, self:CheckShowForSale(self._itemCfg), showList, self._btnForSale)
    self:AddBtnToShowList(self._buttonArgs.offSell, true, showList, self._btnOffSell)
    self:AddBtnToShowList(self._buttonArgs.buy, true, showList, self._btnBuy)
    self:AddBtnToShowList(self._buttonArgs.sell, self:CheckCanSell(self._itemCfg), showList, self._btnSell)
    self:AddBtnToShowList(self._buttonArgs.shopBuy, true, showList, self._btnShopBuy)
    self:AddBtnToShowList(self._buttonArgs.guideBuy, true, showList, self._btnGuideBuy)
    self:AddBtnToShowList(self._buttonArgs.split, self:CheckCanSplit(), showList, self._btnSplit)
    self:AddBtnToShowList(self._buttonArgs.openRankBuy, true, showList, self._btnOpenRankBuy)

    for i = 1, #showList do
        local curY = (i - 1) * 52 + 22
        self:SetPosition(showList[i], curY)
    end
    
    --商城购买
	if self._buttonArgs.shopBuy then
		
		--LoggerHelper.Log("self._buttonArgs.shopBuy:" .. PrintTable:TableToStr(self._buttonArgs.shopBuy))
        self._containerShopBuy:SetActive(true)
        self._inputTextShopCount.text = "1"
        self._shopBuyCount = 1
        local shopBuyArgs = self._buttonArgs.shopBuy
        self._shopBuyId = shopBuyArgs[1]
        self._singleShopPrice = shopBuyArgs[2]
        self._shopBuyLimit = shopBuyArgs[3]
        local moneyId = shopBuyArgs[4]
        self._shopPriceCom:SetMoneyId(moneyId)
        if self._shopBuyLimit == -1 then
            self._btnShopMax:SetActive(false)
        else
            self._btnShopMax:SetActive(true)
        end
        self:UpdateShopPrice()
        
    elseif self._buttonArgs.openRankBuy then
        self._containerShopBuy:SetActive(true)
        self._inputTextShopCount.text = "1"
        self._shopBuyCount = 1
        local shopBuyArgs = self._buttonArgs.openRankBuy
        self._openId = shopBuyArgs[1]
        self._singleShopPrice = shopBuyArgs[2]
        self._shopBuyLimit = shopBuyArgs[3]
        local moneyId = shopBuyArgs[4]
        self._shopPriceCom:SetMoneyId(moneyId)
        if self._shopBuyLimit == -1 then
            self._btnShopMax:SetActive(false)
        else
            self._btnShopMax:SetActive(true)
        end
        
	elseif self._buttonArgs.guideBuy then
		--引导购买
        self._containerShopBuy:SetActive(true)
        local shopBuyArgs = self._buttonArgs.guideBuy
        self._itemId = shopBuyArgs[1]
        self._shopBuyLimit = -1
        self._inputTextShopCount.text = "1"
        self._shopBuyCount = 1
        
        local itemCostNum = ItemDataHelper.GetGuideBuyCost(self._itemId)
        --LoggerHelper.Error("itemCostNum"..PrintTable:TableToStr(itemCostNum))
        if itemCostNum then
            for i,item in ipairs(itemCostNum) do
                if item[1] == 3 then
                    self._shopPriceCom:SetMoneyId(public_config.MONEY_TYPE_COUPONS)
                    self._singleShopPrice = item[2]
                    self._shopPriceCom:SetBigMoneyNum(item[2])
                else
                    self._shopPriceCom:SetMoneyId(public_config.MONEY_TYPE_COUPONS_BIND)
                    self._singleShopPrice = item[2]
                    self._shopPriceCom:SetBigMoneyNum(item[2])
                end
            end
        end

    
        if self._shopBuyLimit == -1 then
            self._btnShopMax:SetActive(false)
        else
            self._btnShopMax:SetActive(true)
        end

	else
		self._containerShopBuy:SetActive(false)
    end
    
    --公会兑换
    if self._buttonArgs.guildExchange then
        self._containerGuildExchange:SetActive(true)
        self._inputTextGECount.text = "1"
        self._geCount = 1
        self._singleGuildPoint = self._guildPetItemCfg[self._itemCfg.id]
        self:UpdateGEPoint()
    else
        self._containerGuildExchange:SetActive(false)
    end

end

--是否可以拆分
function CommonItemTipsView:CheckCanSplit()
    if self._itemData and self._itemData:GetCount() > 1 then
        return true
    end
    return false
end


function CommonItemTipsView:CheckShowCombine()
    return self._itemCfg.itemType == public_config.ITEM_ITEMTYPE_GEM
end

function CommonItemTipsView:CheckShowUse()
    local effectId = self._itemCfg.effectId
    if self._itemCfg.itemType == public_config.ITEM_ITEMTYPE_GEM then
        self._txtBtnUse.text = LanguageDataHelper.CreateContent(217)
        return true
    end
    if effectId or self._itemCfg.open_in_bag then
        --使用按钮文本分类处理
        if self._itemCfg.open_in_bag then
            self._txtBtnUse.text = LanguageDataHelper.CreateContent(self._itemCfg.open_in_bag[1][2])
        else
            self._txtBtnUse.text = LanguageDataHelper.CreateContent(516)
        end
        return true
    else
        return false
    end
end

--宝石合成
function CommonItemTipsView:OnCombineClick()
    if BagManager:OpenPanelFunction(36) then
        GUIManager.ClosePanel(PanelsConfig.ItemTips)
        GUIManager.ClosePanel(PanelsConfig.Role)
    end
end

--批量使用物品
function CommonItemTipsView:OnClickForBulkUse()
    local data = {["id"] = MessageBoxType.BulkUse, ["value"] = self._itemData}
    GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
    GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--拆分物品
function CommonItemTipsView:OnClickForSplit()
    if self._itemData and self._itemData:GetCount() > 1 then
        local data = {["id"] = MessageBoxType.PropSplit, ["value"] = self._itemData}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
        GUIManager.ClosePanel(PanelsConfig.ItemTips)
    end
end

------------------------------公会兑换-------------------------------------
function CommonItemTipsView:OnEndEditGECount(text)
    if self._inputTextGECount.text == "0" then
        self._inputTextGECount.text = "1"
        self._geCount = 1
    else
        self._geCount = tonumber(text)
    end
    self:UpdateGEPoint()
end

function CommonItemTipsView:UpdateGEPoint()
    self._txtGuildPoint.text = tostring(self._singleGuildPoint * self._geCount)
end

function CommonItemTipsView:OnClicGEPlus()
    self._geCount = self._geCount + 1
    self._inputTextGECount.text = tostring(self._geCount)
    self:UpdateGEPoint()
end

function CommonItemTipsView:OnClicGEReduce()
    if self._geCount > 1 then
        self._geCount = self._geCount - 1
        self._inputTextGECount.text = tostring(self._geCount)
        self:UpdateGEPoint()
    end
end

------------------------------商城购买---------------------------------------
function CommonItemTipsView:OnEndEditShopCount(text)
    local count = tonumber(self._inputTextShopCount.text)
    if count == 0 then
        self._inputTextShopCount.text = "1"
        self._shopBuyCount = 1
    elseif self._shopBuyLimit > 0 and count > self._shopBuyLimit then
        self._inputTextShopCount.text = tostring(self._shopBuyLimit)
        self._shopBuyCount = self._shopBuyLimit
    else
        self._shopBuyCount = tonumber(text)
    end
    self:UpdateShopPrice()
end

function CommonItemTipsView:UpdateShopPrice()
    self._shopPriceCom:SetMoneyNum(self._singleShopPrice * self._shopBuyCount)
end

function CommonItemTipsView:OnClickShopPricePlus()
    if self._shopBuyCount < self._shopBuyLimit or self._shopBuyLimit == -1 then
        self._shopBuyCount = self._shopBuyCount + 1
        self._inputTextShopCount.text = tostring(self._shopBuyCount)
        self:UpdateShopPrice()
    end
end

function CommonItemTipsView:OnClickShopPriceReduce()
    if self._shopBuyCount > 1 then
        self._shopBuyCount = self._shopBuyCount - 1
        self._inputTextShopCount.text = tostring(self._shopBuyCount)
        self:UpdateShopPrice()
    end
end

function CommonItemTipsView:OnClickShopPriceMax()
    self._shopBuyCount = math.max(self._shopBuyLimit,1)
    self._inputTextShopCount.text = tostring(self._shopBuyCount)
    self:UpdateShopPrice()
end

-----------------------------------------------------------------------------
function CommonItemTipsView:OnUseBtnClick()
    local func = function()  GUIManager.ClosePanel(PanelsConfig.ItemTips) end
	BagManager:OnUseClick(self._itemData,func) 
end

--根据物品server数据更新tip
function CommonItemTipsView:UpdateView(itemData)
    self._itemCfg = itemData.cfgData
    self._itemData = itemData
    self._bagType = itemData.bagType
    self._bind = itemData:BindFlag()
    local itemId = itemData.cfg_id
    self._base.UpdateViewBaseInfo(self, itemId)
    self:SetItemName(itemId)
    
    if self._buttonArgs then
        self._containerBottom:SetActive(true)
        self:FiltButtons()
    else
        self._containerBottom:SetActive(false)
    end
    
    self:UpdateLayout()
    self:UpdateModel()
end

--根据物品ID更新tip
function CommonItemTipsView:UpdateViewById(itemId)
    self._itemCfg = ItemDataHelper.GetItem(itemId)
    self._bagType = 0
    self._base.UpdateViewBaseInfo(self, itemId)
    self:SetItemName(itemId)
    self._txtBind.text = ""
    if self._buttonArgs then
        self._containerBottom:SetActive(true)
        self:FiltButtons()
    else
        self._containerBottom:SetActive(false)
    end
    self:UpdateLayout()
    self:UpdateModel()
end

function CommonItemTipsView:UpdateModel()
    local modelData = self._itemCfg.mode_vocation
    local vocationMatch = ItemDataHelper.CheckVocationMatch(self._itemCfg.id,GameWorld.Player().vocation,true)
    if modelData ~= nil and vocationMatch then
        --幻化道具展示
        if modelData[4] then
            EventDispatcher:TriggerEvent(GameEvents.Item_Tips_Show_Model,2,modelData[4],self._dy)
        elseif modelData[6] or modelData[7] or modelData[8] then
            EventDispatcher:TriggerEvent(GameEvents.Item_Tips_Show_Model,3,modelData,self._dy)
        --人物模型展示
        else

            for k, v in pairs(modelData) do
                if k == 1 then
                    GameWorld.ShowPlayer():SetCurCloth(v)
                elseif k == 2 then
                    GameWorld.ShowPlayer():SetCurWeapon(v)
                elseif k == 3 then
                    GameWorld.ShowPlayer():SetCurHead(v)
                elseif k == 5 then
                    GameWorld.ShowPlayer():SetCurEffect(v)
                    TimerHeap:AddTimer(200, 0, 0, function() SortOrderedRenderAgent.ReorderAll() end)
                end
            end
            EventDispatcher:TriggerEvent(GameEvents.Item_Tips_Show_Model,1,nil,self._dy)
        end
        self._tranform.anchoredPosition = Vector3(160,self._dy * 0.5,0)
    else
        self._tranform.anchoredPosition = Vector3(0,self._dy * 0.5,0)
    end
end

function CommonItemTipsView:UpdateLayout()
    local dy = self._txtDesc.preferredHeight - 90
    
    self:SetPosition(self._txtAchieveDescGO, -dy + 0)
    
    dy = dy + self._txtAchieveDesc.preferredHeight - 10
    
    local exCompHeight = 0
    if self._buttonArgs and (self._buttonArgs.shopBuy or self._buttonArgs.guildExchange or self._buttonArgs.guideBuy or self._buttonArgs.openRankBuy) then
        exCompHeight = 105
    end
    
    dy = dy + exCompHeight
    if dy > 0 then
        self._bgRectTransform.sizeDelta = Vector2(364, 350 + dy)
        self._frameRectTransform.sizeDelta = Vector2(382, 360 + dy)
        self:SetPosition(self._containerBottom, -dy)
        --self:SetPosition(self, dy * 0.5)
    else
        self._bgRectTransform.sizeDelta = Vector2(364, 350)
        self._frameRectTransform.sizeDelta = Vector2(382, 360)
        self:SetPosition(self._containerBottom, 0)
        --self:SetPosition(self, 0)
    end
    self._dy = math.max(0,dy)
end

function CommonItemTipsView:SetItemDesc(itemId)
    --经验丹特殊处理
    if self._itemCfg.effectId and self._itemCfg.effectId[1] == 17 then
        local level = GameWorld.Player().level
        local addExp = StringStyleUtil.GetLongNumberString(math.floor(self._itemCfg.effectId[2] / 10000 * RoleDataHelper.GetCurLevelExp(level)))
        self._txtDesc.text = LanguageDataHelper.CreateContentWithArgs(self._itemCfg.description, {["0"] = addExp})
    else
        self._txtDesc.text = ItemDataHelper.GetDescription(itemId)
    end
    
    if self._itemCfg.achieve_chinese and self._itemCfg.achieve_chinese > 0 then
        self._txtAchieveDesc.text = LanguageDataHelper.CreateContent(self._itemCfg.achieve_chinese)
        self._txtAchieveDescGO:SetActive(true)
    --self._txtAchieveHeight = 30
    else
        self._txtAchieveDesc.text = ""
        self._txtAchieveDescGO:SetActive(false)
    --self._txtAchieveHeight = 0
    end
end
