-- SkillTipsView.lua
local SkillTipsView = Class.SkillTipsView(ClassTypes.BaseComponent)
SkillTipsView.interface = GameConfig.ComponentsConfig.Component
SkillTipsView.classPath = "Modules.ModuleTips.ChildView.SkillTipsView"

local SkillDataHelper = GameDataHelper.SkillDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TransformManager = PlayerManager.TransformManager

function SkillTipsView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtTypeName = self:GetChildComponent("Text_TypeName",'TextMeshWrapper')
	self._txtDesc = self:GetChildComponent("Text_Desc",'TextMeshWrapper')
	self._txtCondition = self:GetChildComponent("Text_Condition",'TextMeshWrapper')
	self._iconContainer = self:FindChildGO("Container_Icon")
end

function SkillTipsView:UpdateView(data)
	local skillId = data.skillId
	local conditionLevel = data.conditionLevel
	local systemType = data.treasureType
	local systemName = data.systemName
	self._txtName.text = SkillDataHelper.GetName(skillId)
	self._txtTypeName.text = LanguageDataHelper.CreateContent(SkillDataHelper.GetSkillType(skillId)+21316)
	self._txtDesc.text = SkillDataHelper.GetDesc(skillId)
	if conditionLevel then
		local tipId
		if systemType > 4 then
			tipId = 514
		else
			tipId = 176
		end
		if systemName == nil then
			systemName = TransformManager:GetSystemCn(systemType)
		end
		self._txtCondition.text = systemName..LanguageDataHelper.CreateContentWithArgs(tipId,{["0"]=conditionLevel})
	else
		self._txtCondition.text = ""
	end
	GameWorld.AddIcon(self._iconContainer,SkillDataHelper.GetSkillIcon(skillId))
end

