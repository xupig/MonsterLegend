-- TipsModelView.lua
local TipsModelView = Class.TipsModelView(ClassTypes.BaseComponent)
TipsModelView.interface = GameConfig.ComponentsConfig.Component
TipsModelView.classPath = "Modules.ModuleTips.ChildView.TipsModelView"
local PlayerModelComponent = GameMain.PlayerModelComponent
local TransformDataHelper = GameDataHelper.TransformDataHelper
local EquipModelComponent = GameMain.EquipModelComponent
local ActorModelComponent = GameMain.ActorModelComponent
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local public_config = GameWorld.public_config
local UIParticle = ClassTypes.UIParticle
local CommonIconType = GameConfig.EnumType.CommonIconType
local GUIManager = GameManager.GUIManager
local TitleDataHelper = GameDataHelper.TitleDataHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local Move_Y = 20

function TipsModelView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self._tranform = self.transform
	local bg = self:FindChildGO("Image_Bg")
    self._bgRectTransform = bg:GetComponent(typeof(UnityEngine.RectTransform))
    local bgFrame = self:FindChildGO("Image_Frame")
    self._frameRectTransform = bgFrame:GetComponent(typeof(UnityEngine.RectTransform))

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Bottom/FirstCharge")
    self._containerBottom = self:FindChildGO("Container_Bottom")
    self._containerBottomTrans = self._containerBottom.transform

	local modelGo = self:FindChildGO("Container_Model")
	self._modelTrans = modelGo.transform
	self._modelComponent = self:AddChildComponent('Container_Model', PlayerModelComponent)

	local treasureModelGo = self:FindChildGO("Container_TreasureModel")
	local treasureModelTrans = treasureModelGo.transform
	self._treaureModelComponent = self:AddChildComponent('Container_TreasureModel', EquipModelComponent)

	local partnerModelGo = self:FindChildGO("Container_ActorModel")
	local partnerModelTrans = partnerModelGo.transform
	self._partnerModelComponent = self:AddChildComponent('Container_ActorModel', ActorModelComponent)

	self._moveTo = {}
	self._tweenPos = {}
	self._photoIconGo = self:FindChildGO("Container_PhotoIcon")
	local photoIconTrans = self._photoIconGo.transform
    self._tweenPos[4] = self:AddChildComponent('Container_PhotoIcon', XGameObjectTweenPosition)
    
	self._bubbleGo = self:FindChildGO("Container_Bubble")
	local bubbleTrans = self._bubbleGo.transform
	self._tweenPos[5] = self:AddChildComponent('Container_Bubble', XGameObjectTweenPosition)

	self._titleIconGo = self:FindChildGO("Container_TitleIcon")
	local titleIconTrans = self._titleIconGo.transform
	self._tweenPos[6] = self:AddChildComponent('Container_TitleIcon', XGameObjectTweenPosition)

    self._containerList = {}
    self._containerList[1] = modelGo
    self._containerList[2] = treasureModelGo
    self._containerList[3] = partnerModelGo
    self._containerList[4] = self._photoIconGo
    self._containerList[5] = self._bubbleGo
    self._containerList[6] = self._titleIconGo

    self._containerTransList = {}
    self._containerTransList[1] = self._modelTrans
    self._containerTransList[2] = treasureModelTrans
    self._containerTransList[3] = partnerModelTrans
    self._containerTransList[4] = photoIconTrans
    self._containerTransList[5] = bubbleTrans
    self._containerTransList[6] = titleIconTrans
    
    self._posXList = {0,-42,-9,178,138,80}
    self._posYList = {0,-30,25,94,103,59}
end

function TipsModelView:SetShowMode(showMode)
	if self._showMode then
		if showMode == self._showMode then
			return
		end
		self._containerList[self._showMode]:SetActive(false)
	end
	for i=1,6 do
		self._containerList[showMode]:SetActive(true)
	end
	if self._tweenPos[self._showMode] then
		self._tweenPos[self._showMode].isTweening = false
	end
	self._showMode = showMode
end

function TipsModelView:ShowOtherModel(modelData)
	local cfgId
	local type
	for k,v in pairs(modelData) do
		type = k
		cfgId = v
	end
	if type == 6 then
		self:SetShowMode(4)
		GameWorld.AddIcon(self._photoIconGo,cfgId)
	elseif type == 7 then
		self:SetShowMode(5)
		GUIManager.AddCommonIcon(self._bubbleGo,CommonIconType.Bubble,cfgId)
	elseif type == 8 then
		self:SetShowMode(6)
		local icon = TitleDataHelper.GetTitleShowName(cfgId)
		GameWorld.AddIconAnim(self._titleIconGo, icon)
        GameWorld.SetIconAnimSpeed(self._titleIconGo, 5)
	end
end

function TipsModelView:ShowTransformModel(transformCfgId)
	local vocationGroup = math.floor(GameWorld.Player().vocation/100)
	local transformCfg = TransformDataHelper.GetTreasureTotalCfg(transformCfgId)
	local modelInfo = TransformDataHelper.GetUIModelViewCfg(transformCfg.model_vocation[vocationGroup])
	local modelId = modelInfo.model_ui
    local posx = modelInfo.pos[1]
    local posy = modelInfo.pos[2]
    local posz = modelInfo.pos[3]
    local rotationx = modelInfo.rotation[1]
    local rotationy = modelInfo.rotation[2]
    local rotationz = modelInfo.rotation[3]
    local scale = modelInfo.scale
	if transformCfg.type < 5 then
		self:SetShowMode(2)
		self._treasureType = transformCfg.type
		self._treaureModelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
    
	    --神兵自转
	    if self._treasureType == public_config.TREASURE_TYPE_WEAPON then
	        self._treaureModelComponent:SetSelfRotate(true)
	    else
	        self._treaureModelComponent:SetSelfRotate(false)
	    end
	    
	    if self._treasureType == public_config.TREASURE_TYPE_TALISMAN then
	        --法宝替换controller
	        local controllerPath = GlobalParamsHelper.GetParamValue(692)
	        self._treaureModelComponent:LoadEquipModel(modelId, controllerPath)
	    else
	        self._treaureModelComponent:LoadEquipModel(modelId, "")
	    end
	else
		self:SetShowMode(3)
		self._partnerModelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
    	self._partnerModelComponent:LoadModel(modelId)
	end
end

function TipsModelView:ShowPlayerModel()
	self:SetShowMode(1)
	GameWorld.ShowPlayer():ShowModel(1)
	self._modelComponent:SetStartSetting(Vector3(0, 0, 300), Vector3(0, 180, 0), 1)
end

function TipsModelView:UpdateLayout(dy)
	if self._showMode ~= 1 then
		if dy > 0 then
	        self._bgRectTransform.sizeDelta = Vector2(502, 350 + dy)
	        self._frameRectTransform.sizeDelta = Vector2(520, 360 + dy)
	    else
	        self._bgRectTransform.sizeDelta = Vector2(502, 350)
	        self._frameRectTransform.sizeDelta = Vector2(520, 360)
	    end
	end
    
    if self._showMode == 1 then
    	self._modelTrans.anchoredPosition = Vector3(50,- 201,0)
    	self._containerBottom:SetActive(false)
    	local fitHeight = math.max(568,350 + dy)
    	self._bgRectTransform.sizeDelta = Vector2(502, fitHeight)
	    self._frameRectTransform.sizeDelta = Vector2(520, fitHeight+10)
    else
    	local posx = self._posXList[self._showMode]
    	local posy = self._posYList[self._showMode] - dy*0.5

    	self._containerTransList[self._showMode].anchoredPosition = Vector3(posx,posy,0)
    	self._containerBottom:SetActive(true)
    	
    	if self._showMode == 4 or self._showMode == 5 or self._showMode == 6 then
	    	local moveTo = self._containerTransList[self._showMode].localPosition
	    	moveTo.y = moveTo.y + Move_Y
	    	self._tweenPos[self._showMode].isTweening = true
	    	self._tweenPos[self._showMode]:SetToPosTween(moveTo, 2, 4, 4, nil)
	    	self._containerBottomTrans.anchoredPosition = Vector3(116,-dy* 0.5-50,0)
	    else
	    	self._containerBottomTrans.anchoredPosition = Vector3(116,-dy* 0.5-80,0)
	    end
    end

    self._tranform.anchoredPosition = Vector3(-300,dy * 0.5,0)
end