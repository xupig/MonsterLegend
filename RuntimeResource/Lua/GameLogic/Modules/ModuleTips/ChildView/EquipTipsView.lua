-- EquipTipsView.lua

local EquipTipsView = Class.EquipTipsView(ClassTypes.ItemTipsViewBase)
EquipTipsView.interface = GameConfig.ComponentsConfig.PointableComponent
EquipTipsView.classPath = "Modules.ModuleTips.ChildView.EquipTipsView"

require "UIComponent.Base.UIScrollView"
require "UIComponent.Extend.AttriCom"
require "Modules.ModuleTips.ChildComponent.EquipTipsGemAttriItem"
require "Modules.ModuleTips.ChildComponent.EquipTipsSuitCom"
require "UIComponent.Extend.SimpleCurrencyCom"

local SimpleCurrencyCom = ClassTypes.SimpleCurrencyCom
local UIScrollView = ClassTypes.UIScrollView
local AttriCom = ClassTypes.AttriCom
local EquipTipsGemAttriItem = ClassTypes.EquipTipsGemAttriItem
local EquipTipsSuitCom = ClassTypes.EquipTipsSuitCom
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local EquipManager = GameManager.EquipManager
local BagManager = PlayerManager.BagManager
local bagData = PlayerManager.PlayerDataManager.bagData
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemConfig = GameConfig.ItemConfig
local EquipDataHelper = GameDataHelper.EquipDataHelper
local BaseUtil = GameUtil.BaseUtil
local StringStyleUtil = GameUtil.StringStyleUtil
local washQualityMap = {1,3,4,5,6}
local GuildManager = PlayerManager.GuildManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MarryRingDataHelper = GameDataHelper.MarryRingDataHelper
local MarriageRaiseManager = PlayerManager.MarriageRaiseManager

function EquipTipsView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self:InitListener()
	self:InitBg()
	self:InitBase()
	self:InitItemPool()
	self:InitScrollContent()
end

function EquipTipsView:InitListener()
	
end

function EquipTipsView:InitBg()
	self._bg = self:FindChildGO("Image_Bg")
	self._bgRectTransform = self._bg:GetComponent(typeof(UnityEngine.RectTransform))
	self._bgFrame = self:FindChildGO("Image_Frame")
	self._frameRectTransform = self._bgFrame:GetComponent(typeof(UnityEngine.RectTransform))
end

function EquipTipsView:InitBase()
	self._base.InitBase(self)

	local rootPath = "ScrollView_Attri/Mask/Content/"

	self._txtUseLv = self:GetChildComponent(rootPath.."Container_BaseInfo/Text_Level",'TextMeshWrapper')
	self._txtVocation = self:GetChildComponent(rootPath.."Container_BaseInfo/Text_Vocation",'TextMeshWrapper')
	
	self._txtScore = self:GetChildComponent(rootPath.."Container_BaseInfo/Text_Score",'TextMeshWrapper')
	self._txtBaseScore = self:GetChildComponent(rootPath.."Container_BaseInfo/Text_BaseScore",'TextMeshWrapper')

	self._imgLoaded = self:FindChildGO("Image_Loaded")
	self._txtDescGO = self:FindChildGO(rootPath.."Text_Desc")
	self._txtDesc = self._txtDescGO:GetComponent('TextMeshWrapper')
	self._txtTimeLeft = self:GetChildComponent("Text_TimeLeft",'TextMeshWrapper')
end

--装备描述不在这里设定
function EquipTipsView:SetItemDesc(itemId)
	self._desc = ItemDataHelper.GetDescription(itemId)
	if self._desc then
		self._txtDescGO:SetActive(true)
		self._txtDesc.text = self._desc
	else
		self._txtDescGO:SetActive(false)
	end
end

--初始化对比部分内容
function EquipTipsView:InitCompareComps()
	local rootPath = "ScrollView_Attri/Mask/Content/"
	self._compareScore = self:FindChildGO(rootPath.."Container_BaseInfo/Container_ScoreUp")
	self._imgArrowUp = self:FindChildGO(rootPath.."Container_BaseInfo/Container_ScoreUp/Image_IconUp")
	self._imgArrowDown = self:FindChildGO(rootPath.."Container_BaseInfo/Container_ScoreUp/Image_IconDown")
	--self._txtCompareScore = self:GetChildComponent(rootPath.."Container_BaseInfo/Container_ScoreUp/Text_ScoreUp",'TextMeshWrapper')
	self._compareScore:SetActive(false)
end

function EquipTipsView:InitButtons()
	self._containerBottom = self:FindChildGO("Container_Bottom")
	
	self._buttonInited = true
	self._btnEquip 		= self:FindChildGO("Container_Bottom/Button_Equip")
	self._btnForSale 	= self:FindChildGO("Container_Bottom/Button_ForSale")
	self._btnOffSell 	= self:FindChildGO("Container_Bottom/Button_OffSell")
	self._btnBuy 		= self:FindChildGO("Container_Bottom/Button_Buy")
	self._btnSell 		= self:FindChildGO("Container_Bottom/Button_Sell")
	self._btnSave 		= self:FindChildGO("Container_Bottom/Button_Save")
	self._btnGet 		= self:FindChildGO("Container_Bottom/Button_Get")
	self._btnGuildExchange = self:FindChildGO("Container_Bottom/Button_GuildExchange")
	self._btnGuildDonate 	= self:FindChildGO("Container_Bottom/Button_GuildDonate")
	self._btnGuildDestroy	= self:FindChildGO("Container_Bottom/Button_GuildDestroy")
	self._btnRenew 			= self:FindChildGO("Container_Bottom/Button_Renew")
	self._btnUseRing 			= self:FindChildGO("Container_Bottom/Button_UseRing")
	self._btnShopBuy 		= self:FindChildGO("Container_Bottom/Button_ShopBuy")	--商城购买

	self._buttonList = {}
	
	self._csBH:AddClick(self._btnEquip, 		function() self:OnClickPutOn() end)
	self._csBH:AddClick(self._btnForSale, 		function() self:OnClickForSale() end)
	self._csBH:AddClick(self._btnOffSell, 		function() self:OnClickOffSell() end)
	self._csBH:AddClick(self._btnBuy, 			function() self:OnClickBuy() end)
	self._csBH:AddClick(self._btnSell, 			function() self:OnClickSell() end)
	self._csBH:AddClick(self._btnSave, 			function() self:OnClickSave() end)
	self._csBH:AddClick(self._btnGet, 			function() self:OnClickGet() end)
	self._csBH:AddClick(self._btnGuildExchange, function() self:OnClicGuildExchange() end)
	self._csBH:AddClick(self._btnGuildDonate, 	function() self:OnClicGuildDonate() end)
	self._csBH:AddClick(self._btnGuildDestroy, 	function() self:OnClicGuildDestroy() end)
	self._csBH:AddClick(self._btnRenew, 		function() self:OnClicRenew() end)
	self._csBH:AddClick(self._btnUseRing, 		function() self:OnClickUseRing() end)
	self._csBH:AddClick(self._btnShopBuy, 		function() self:OnClickShopBuy() end)

	--公会仓库兑换
	self._containerGuildExchange = self:FindChildGO("Container_Bottom/Container_GuildExchange")
	self._txtGuildPoint = self:GetChildComponent("Container_Bottom/Container_GuildExchange/Container_Point/Text_Num", 'TextMeshWrapper')
	local containerGuildExchangePoint = self:FindChildGO("Container_Bottom/Container_GuildExchange/Container_Point/Container_MoneyIcon")
	GameWorld.AddIcon(containerGuildExchangePoint,56)
	-----------商城购买--------------
	self._containerShopBuy = self:FindChildGO("Container_Bottom/Container_ShopBuy")
	self._shopPriceCom = self:AddChildLuaUIComponent("Container_Bottom/Container_ShopBuy/Container_Diamond", SimpleCurrencyCom)
	self._shopPriceCom:InitComps()
	self._shopPriceCom:SetShowLackMoney(true)
end

--装装备
function EquipTipsView:OnClickPutOn()
	--套装替换提示
	if self:CheckLoadedSuit() then
		local str  = LanguageDataHelper.CreateContent(58661)
		GUIManager.ShowMessageBox(2,str,function() self:PutOnEquip() end)
		return
	end
	self:PutOnEquip()
end

--检查与身上装备是否需要提示
function EquipTipsView:CheckLoadedSuit()
	local loadedEquips = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
	local suitData = GameWorld.Player().equip_suit_data or {}
	local equipType = self._itemData:GetType()
	local suitLevel = suitData[equipType] or 0
	local loadedEquip = loadedEquips[equipType]
	if equipType > 10 or suitLevel == 0 or loadedEquip == nil then
		return false
	end

	local loadedSuitId = loadedEquip:GetSuitId()[suitLevel]
	local loadedSuitCost = loadedEquip:GetSuitCost()[loadedSuitId]
	local curSuitCost = self._itemData:GetSuitCost()
	if curSuitCost then
		if curSuitCost[loadedSuitId] then
			for i=1,#curSuitCost[loadedSuitId] do
				if curSuitCost[loadedSuitId][i] ~= loadedSuitCost[i] then
					return true
				end
			end
			return false
		else
			return true
		end
	else
		return true
	end
end

function EquipTipsView:PutOnEquip()
	local vocation = GameWorld.Player().vocation
	if self._itemData:CheckVocationMatch() then
		EquipManager:PutOnEquip(self._itemData.bagPos)
	else
		local usevocation = self._itemCfg.useVocation
		local args = LanguageDataHelper.GetArgsTable()
		args["0"] = self._itemCfg.levelNeed
		args["1"] = ItemDataHelper.GetVocationString(self._itemCfg.id)
		local str = LanguageDataHelper.CreateContentWithArgs(58794,args)
		GUIManager.ShowText(1,str,2)
	end
	
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--守护续费
function EquipTipsView:OnClicRenew()
	local costCfg = GlobalParamsHelper.GetParamValue(542)
	local cost = costCfg[self._itemData.cfg_id]
	local str = LanguageDataHelper.CreateContentWithArgs(58708,{["0"] = cost})
	GUIManager.ShowMessageBox(2,str,function ()
		self:RequsetRenew()
	end)
end

function EquipTipsView:RequsetRenew()
	EquipManager:RequestGuardBuy(self._itemData.bagType,self._itemData.bagPos)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--婚戒使用
function EquipTipsView:OnClickUseRing()
	MarriageRaiseManager:MarryExtraRingReq(self._itemData.bagPos)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--公会捐赠
function EquipTipsView:OnClicGuildDonate()
	GuildManager:GuildWarehouseDonateReq(self._itemData.bagPos)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--公会销毁
function EquipTipsView:OnClicGuildDestroy()
	GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(53177), 
		function() GuildManager:GuildWarehouseDestoryListReq({self._itemData.bagPos}) GUIManager.ClosePanel(PanelsConfig.ItemTips) end, 
		nil)
end

-- function EquipTipsView:RequestDecompose()
-- 	BagManager:RequestBreak(tostring(self._itemData.bagPos))
-- 	GUIManager.ClosePanel(PanelsConfig.ItemTips)
-- end

--筛选可显示的按钮
function EquipTipsView:FiltButtons()
	local showList = {}

	self:AddBtnToShowList(self._buttonArgs.guildExchange,true,showList,self._btnGuildExchange)
	self:AddBtnToShowList(self._buttonArgs.guildDonate,true,showList,self._btnGuildDonate)
	self:AddBtnToShowList(self._buttonArgs.guildDestroy,true,showList,self._btnGuildDestroy)
	self:AddBtnToShowList(self._buttonArgs.save,true,showList,self._btnSave)
	self:AddBtnToShowList(self._buttonArgs.get,true,showList,self._btnGet)
	self:AddBtnToShowList(self._buttonArgs.renew,self:CheckShouldRenew(),showList,self._btnRenew)
	self:AddBtnToShowList(self._buttonArgs.useRing,self:CheckShouldUseRing(),showList,self._btnUseRing)
	self:AddBtnToShowList(self._buttonArgs.equip,self:CheckShowEquip(),showList,self._btnEquip)
	self:AddBtnToShowList(self._buttonArgs.forSale,self:CheckShowForSale(self._itemCfg),showList,self._btnForSale)
	self:AddBtnToShowList(self._buttonArgs.offSell,true,showList,self._btnOffSell)
	self:AddBtnToShowList(self._buttonArgs.buy,true,showList,self._btnBuy)
	self:AddBtnToShowList(self._buttonArgs.sell,self:CheckCanSell(self._itemCfg),showList,self._btnSell)
	self:AddBtnToShowList(self._buttonArgs.shopBuy,true,showList,self._btnShopBuy)

	for i=1,#showList do
		local curY = (i-1)*52 + 22
		self:SetPosition(showList[i],curY)
	end


	--商城购买
	if self._buttonArgs.shopBuy then
		self._containerShopBuy:SetActive(true)
		self._shopBuyCount = 1
		local shopBuyArgs = self._buttonArgs.shopBuy
		self._shopBuyId = shopBuyArgs[1]
		self._singleShopPrice = shopBuyArgs[2]
		--self._shopBuyLimit = shopBuyArgs[3]
		local moneyId = shopBuyArgs[4]
		self._shopPriceCom:SetMoneyId(moneyId)
		self._shopPriceCom:SetMoneyNum(self._singleShopPrice)
	else
		self._containerShopBuy:SetActive(false)
	end

	--公会兑换
	if self._buttonArgs.guildExchange or self._buttonArgs.guildDonate then
		self._containerGuildExchange:SetActive(true)
		self._geCount = 1
		self._txtGuildPoint.text = tostring(self._itemCfg.guildWarehousePoints)
	else
		self._containerGuildExchange:SetActive(false)
	end

end

function EquipTipsView:CheckShouldUseRing()
	self._showUseRing = false
	--婚戒显示有可能显示续费
	if self._itemCfg.type == public_config.PKG_LOADED_EQUIP_INDEX_RING then
		local ring = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetEquipListBySubType(public_config.PKG_LOADED_EQUIP_INDEX_RING)
		if ring[1] then
			self._showUseRing = true
		end
	end
	return self._showUseRing
end

function EquipTipsView:CheckShouldRenew()
	self._showRenew = false
	--守护有可能显示续费
	if self._itemCfg.type == public_config.PKG_LOADED_EQUIP_INDEX_GUARD and self._guardTimeOut then
		self._showRenew = true
	end
	return self._showRenew
end

function EquipTipsView:CheckShowEquip()
	--守护续费/婚戒使用跟装备互斥
	if self._showUseRing or self._showRenew then
		return false
	end
	return true
end

function EquipTipsView:InitScrollContent()
	self._scroll = UIComponentUtil.FindChild(self.gameObject, "ScrollView_Attri")
	self._scrollViewAttri = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
	self._scrollContent = self:FindChildGO("ScrollView_Attri/Mask/Content")
	self._rectTransform = self._scrollContent:GetComponent(typeof(UnityEngine.RectTransform))
	local rootPath = "ScrollView_Attri/Mask/Content/"

	--基础信息
	self._containerBaseInfo = self:FindChildGO(rootPath.."Container_BaseInfo")
	--基础属性部分
	self._titleBaseAttri = self:FindChildGO(rootPath.."Text_TitleBaseAttri")
	self._containerBaseAttriItemGroup = self:FindChildGO(rootPath.."Container_AttriItemGroup")

	--随机属性部分
	self._titleRanAttri = self:FindChildGO(rootPath.."Text_TitleRanAttri")
	self._containerRanAttriItemGroup = self:FindChildGO(rootPath.."Container_RanAttriItemGroup")

	--强化属性部分
	self._titleWash = self:FindChildGO(rootPath.."Text_TitleWash")
	self._containerWashItemGroup = self:FindChildGO(rootPath.."Container_WashItemGroup")

	--宝石部分
	self._titleGem = self:FindChildGO(rootPath.."Text_TitleGem")
	self._containerGemItemGroup = self:FindChildGO(rootPath.."Container_GemItemGroup")

	--宝石精炼
	self._titleGemRefine = self:FindChildGO(rootPath.."Text_TitleGemRefine")
	self._containerGemRefine = self:FindChildGO(rootPath.."Container_GemRefine")
	self._txtRefineRate = self:FindChildGO(rootPath.."Container_GemRefine/Text_Rate")

	--套装部分
	self._containerSuit = self:AddChildLuaUIComponent(rootPath.."Container_Suit",EquipTipsSuitCom)
	self._containerSuit:SetHightFactor(68)
end

function EquipTipsView:ShowRandAttri(shouldShow)
	self._titleRanAttri:SetActive(shouldShow)
	self._containerRanAttriItemGroup:SetActive(shouldShow)
end

function EquipTipsView:ShowWashAttri(shouldShow)
	self._titleWash:SetActive(shouldShow)
	self._containerWashItemGroup:SetActive(shouldShow)
end

function EquipTipsView:ShowGemInfo(shouldShow)
	self._titleGem:SetActive(shouldShow)
	self._containerGemItemGroup:SetActive(shouldShow)
end

function EquipTipsView:ShowGemRefine(shouldShow)
	self._titleGemRefine:SetActive(shouldShow)
	self._containerGemRefine:SetActive(shouldShow)
end

--初始化属性栏对象池
function EquipTipsView:InitItemPool()
	--对象池
	self._baseAttris = {}
	self._randAttris = {}
	self._washAttris = {}
	
	self._baseAttrisBase = self:FindChildGO("ScrollView_Attri/Mask/Content/Container_AttriItemGroup/Container_AttriItem")
	self._randAttrisBase = self:FindChildGO("ScrollView_Attri/Mask/Content/Container_RanAttriItemGroup/Container_RanAttriItem")
	self._washAttrisBase = self:FindChildGO("ScrollView_Attri/Mask/Content/Container_WashItemGroup/Container_WashItem")

	self._baseAttrisBase:SetActive(false)
	self._randAttrisBase:SetActive(false)
	self._washAttrisBase:SetActive(false)

	self._gemAttris = {}
    for i=1,6 do
    	local item = self:AddChildLuaUIComponent("ScrollView_Attri/Mask/Content/Container_GemItemGroup/Container_GemItem"..i, EquipTipsGemAttriItem)
    	self._gemAttris[i] = item
    end
end

function EquipTipsView:CreateAttriItem()
	local go = self:CopyUIGameObject("ScrollView_Attri/Mask/Content/Container_AttriItemGroup/Container_AttriItem", "ScrollView_Attri/Mask/Content/Container_AttriItemGroup")
    local item = UIComponentUtil.AddLuaUIComponent(go, AttriCom)
    return item
end

function EquipTipsView:CreateRanAttriItem()
	local go = self:CopyUIGameObject("ScrollView_Attri/Mask/Content/Container_RanAttriItemGroup/Container_RanAttriItem", "ScrollView_Attri/Mask/Content/Container_RanAttriItemGroup")
   	local item = UIComponentUtil.AddLuaUIComponent(go, AttriCom)
    return item
end

function EquipTipsView:CreateWashItem()
	local go = self:CopyUIGameObject("ScrollView_Attri/Mask/Content/Container_WashItemGroup/Container_WashItem", "ScrollView_Attri/Mask/Content/Container_WashItemGroup")
    local item = UIComponentUtil.AddLuaUIComponent(go, AttriCom)
    return item
end

--更新比较结果
function EquipTipsView:UpdateCompare(scoreCompare)
	self._compareScore:SetActive(true)
	--local colorId
	if scoreCompare > 0 then
		self._imgArrowUp:SetActive(true)
		self._imgArrowDown:SetActive(false)
		--colorId = ItemConfig.ItemAttriValue
	else
		self._imgArrowUp:SetActive(false)
		self._imgArrowDown:SetActive(true)
		--colorId = ItemConfig.ItemForbidden
	end

	--self._txtCompareScore.text = ConvertStyle(string.format("$(%d,%s)",colorId,tostring(scoreCompare)))
end

function EquipTipsView:HideCompare()
	self._compareScore:SetActive(false)
end

function EquipTipsView:SetShowSuitPreview(showSuitPreview)
	self._showSuitPreview = showSuitPreview
end

--别人的道具特殊的参数（Avartar上的）
function EquipTipsView:SetOtherPlayerArgs(otherPlayerArgs)
	self._otherPlayerArgs = otherPlayerArgs
end

function EquipTipsView:GetPlayerVIPLevel()
	if self._otherPlayerArgs then
		return self._otherPlayerArgs.vipLevel or 0
	else
		return GameWorld.Player().vip_level
	end
end

function EquipTipsView:GetEquipStrengthInfo()
	if self._otherPlayerArgs then
		return self._otherPlayerArgs.strengthInfo or {}
	else
		return GameWorld.Player().equip_stren_info
	end
end

function EquipTipsView:GetEquipWashInfo()
	if self._otherPlayerArgs then
		return self._otherPlayerArgs.washInfo or {}
	else
		return GameWorld.Player().equip_wash_info
	end
end

function EquipTipsView:GetEquipGemInfo()
	if self._otherPlayerArgs then
		return self._otherPlayerArgs.gemInfo or {}
	else
		return GameWorld.Player().equip_gem_info
	end
end

function EquipTipsView:GetEquipSuitData()
	if self._otherPlayerArgs then
		return self._otherPlayerArgs.suitData or {}
	else
		return GameWorld.Player().equip_suit_data or {}
	end
end

--更新数据
function EquipTipsView:UpdateView(equipData)
	self._itemData = equipData
	self._bagType = equipData.bagType
	self._equipType = equipData:GetType()
	self._bind = equipData:BindFlag()
	self._itemCfg = equipData.cfgData
	local itemId = equipData.cfg_id
	self._icon:ActivateSuitIcon(self:GetEquipSuitData())
	self._base.UpdateViewBaseInfo(self,itemId)
    local green = ItemConfig.ItemAttriValue
    local strengthLevel = 0
    --身上装备显示强化
    if self:CheckShowAddition() then
        local strengthInfo = self:GetEquipStrengthInfo()[self._equipType]
        if strengthInfo and strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL] then
            strengthLevel = strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL]
            local maxLevel = self._itemCfg.strengthHighLv
			strengthLevel = math.min(strengthLevel,maxLevel)
    	end
    end

    local nameStr = equipData:GetEquipName(self._showSuitPreview,self:GetEquipSuitData())
	if strengthLevel > 0  then
		nameStr = nameStr..StringStyleUtil.GetColorStringWithColorId("  +"..strengthLevel,green)
	end
	self._txtName.text = nameStr

	self._txtScore.text = tostring(self._itemData:GetFightPower(self._otherPlayerArgs))
	self._txtBaseScore.text = tostring(self._itemData:GetBaseFightPower())
	self._imgLoaded:SetActive(self._bagType == public_config.PKG_TYPE_LOADED_EQUIP)
	
	--守护特殊处理
	if self._equipType == public_config.PKG_LOADED_EQUIP_INDEX_GUARD then
		local timeEnd = equipData:GetTimeEnd()
		local timeLeft = timeEnd - DateTimeUtil.GetServerTime()
		if timeLeft <= 0 then
			self._txtTimeLeft.text = LanguageDataHelper.CreateContent(58656)
			self._guardTimeOut = true
		else
			self._guardTimeOut = false
			if timeLeft > DateTimeUtil.OneDayTime then
				local day = math.ceil(timeLeft/DateTimeUtil.OneDayTime)
				self._txtTimeLeft.text = "("..day..LanguageDataHelper.CreateContent(147)..")"
			else
				self._txtTimeLeft.text = DateTimeUtil:FormatFullTime(timeLeft,true,false,true)
			end
		end
	else
		self._txtTimeLeft.text = ""
	end
	
	self:UpdateAttri(equipData,self._itemCfg)

	--按钮处理
	if self._buttonArgs then
		self._containerBottom:SetActive(true)
		self:FiltButtons()
	elseif self._buttonInited then
		self._containerBottom:SetActive(false)
	end
end

function EquipTipsView:UpdateViewById(itemId)
	self._itemCfg = ItemDataHelper.GetItem(itemId)
	self._bagType = 0 
	self._equipType = self._itemCfg.type
	self._base.UpdateViewBaseInfo(self,itemId)
	self:SetItemName(itemId)
	self:SetShowSuitPreview(false)
	self:HideCompare()
	
	self._imgLoaded:SetActive(false)
	self._txtTimeLeft.text = ""
	self:UpdateAttri(nil,self._itemCfg)

	--按钮处理
	if self._buttonArgs then
		self._containerBottom:SetActive(true)
		self:FiltButtons()
	elseif self._buttonInited then
		self._containerBottom:SetActive(false)
	end
end

--是否显示装备加成数值
function EquipTipsView:CheckShowAddition()
	return self._bagType == public_config.PKG_TYPE_LOADED_EQUIP and self._equipType <= public_config.PKG_LOADED_EQUIP_INDEX_MAX
end

--更新属性
function EquipTipsView:UpdateAttri(equipData,cfg)
	local h = 1
	--基础属性
	local itemBaseAttris
	if cfg.type == public_config.PKG_LOADED_EQUIP_INDEX_RING then
		itemBaseAttris = {}
		local ringLevel = GameWorld.Player().marry_ring_level
		local ringMarryAttri = MarryRingDataHelper:GetAttriBase(ringLevel)
		for k,v in pairs(ringMarryAttri) do
			table.insert(itemBaseAttris,{["attri"] = k, ["value"] = v})
		end
	else
		itemBaseAttris = EquipDataHelper:GetBaseAttris(cfg.id)
	end
	
	local itemBaseAttrisLen = #itemBaseAttris
	--LoggerHelper.Error("itemBaseAttrisLen"..itemBaseAttrisLen)
	self._baseAttrisBase:SetActive(true)
	
	
	for i=1,itemBaseAttrisLen do
		if self._baseAttris[i] == nil then
			local item = self:CreateAttriItem()
			self._baseAttris[i] = item
		end
		self._baseAttris[i]:SetActive(true)
		local attriKey = itemBaseAttris[i].attri
		local baseValue = AttriDataHelper:ConvertAttriValue(attriKey,itemBaseAttris[i].value)
		local valueStr = "  +"..tostring(baseValue)
		--LoggerHelper.Error("itemBaseAttris[i].attri"..itemBaseAttris[i].attri)
		--计算强化加成值
		if self:CheckShowAddition() then
			local strengthInfo = self:GetEquipStrengthInfo()[self._equipType]
			if strengthInfo and strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL] then
				local strengthLevel = strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL]
				local strengthCfg = EquipDataHelper.GetStrengthData(strengthLevel)
				local attriInfo = strengthCfg.attris[self._equipType]
				local colorId = ItemConfig.ItemAttriValue
				local atiAdded = AttriDataHelper:ConvertAttriValue(attriInfo[(i -1)*2 + 1],attriInfo[(i -1)*2 + 2]) 
				local addStr = "("..LanguageDataHelper.CreateContent(214).."+"..atiAdded..")"
				valueStr = valueStr.."  "..StringStyleUtil.GetColorStringWithColorId(addStr,colorId)			
			end
		end
		self._baseAttris[i]:UpdateItem(AttriDataHelper:GetName(attriKey),valueStr,true)
		h = h+1
	end
	self._baseAttrisBase:SetActive(false)
	for j=h,#self._baseAttris do
		self._baseAttris[j]:SetActive(false)
	end

	--随机属性
	--已经获得的装备
	local itemRandAttrisLen = 0
	local itemRandAttris 
	--婚戒属性特殊处理
	if cfg.type == public_config.PKG_LOADED_EQUIP_INDEX_RING then
		itemRandAttris = {}
		local marrySeq = GameWorld.Player().marry_seq
		if marrySeq and marrySeq > 0 then
			local ringLevel = GameWorld.Player().marry_ring_level
			local ringMarryAttri = MarryRingDataHelper:GetAttriMarry(ringLevel)
			for k,v in pairs(ringMarryAttri) do
				table.insert(itemRandAttris,{["attri"] = k, ["value"] = v})
			end
		end
	else
		if equipData then
			itemRandAttris = equipData:GetRandAttrisShow()
		--未获得的装备
		else
			itemRandAttris = EquipDataHelper:GetRecommandRandAttris(cfg.id)
		end
	end
		
	itemRandAttrisLen = #itemRandAttris
	if #itemRandAttris > 0 then
		self:ShowRandAttri(true)
		h = 1
		self._randAttrisBase:SetActive(true)
		for i=1,itemRandAttrisLen do
			if self._randAttris[i] == nil then
				local item = self:CreateRanAttriItem()
				self._randAttris[i] = item
			end
			self._randAttris[i]:SetActive(true)

			local attriKey = itemRandAttris[i].attri
			local attriValue = AttriDataHelper:ConvertAttriValue(attriKey,itemRandAttris[i].value)
			local colorId = ItemConfig.QualityTextMap[AttriDataHelper:GetAttriColor(attriKey)]
			--LoggerHelper.Error("attriKey"..attriKey)
			local str
			if equipData then
				str = AttriDataHelper:GetName(attriKey)
			else
				if itemRandAttris[i].showRecommand then
					str = LanguageDataHelper.CreateContent(304).."  "..AttriDataHelper:GetName(attriKey)
				else
					str = AttriDataHelper:GetName(attriKey)
				end
				--LoggerHelper.Error("attriKey"..attriKey)
			end
			str = str.."  +"..attriValue
			str = StringStyleUtil.GetColorStringWithColorId(str,colorId)
			self._randAttris[i]:UpdateItem(str,"",true)
			h = h+1
		end
		
		for j=h,#self._randAttris do
			self._randAttris[j]:SetActive(false)
		end
		self._randAttrisBase:SetActive(false)
		
	else
		self:ShowRandAttri(false)
	end

	--未获得的装备在这里直接计算
	if equipData == nil then
		local baseAttriPower = BaseUtil.CalculateFightPower(itemBaseAttris,GameWorld.Player().vocation)
		local randAttriPower = BaseUtil.CalculateFightPower(itemRandAttris,GameWorld.Player().vocation)
		local fightPower = baseAttriPower + randAttriPower
		self._txtScore.text = tostring(fightPower)
		self._txtBaseScore.text = tostring(fightPower)
	end

	--洗炼属性
	local itemWashAttrisLen = 0
	if self:CheckShowAddition() then
		local washInfo = self:GetEquipWashInfo()[self._equipType]
		if washInfo then
			self:ShowWashAttri(true)
			h = 1
			itemWashAttrisLen = washInfo[public_config.EQUIP_WASH_INFO_SLOT_COUNT] or 0
			self._washAttrisBase:SetActive(true)
			local washCfg = EquipDataHelper.GetWashData(self._equipType)
			for i=1,itemWashAttrisLen do
				if self._washAttris[i] == nil then
					local item = self:CreateWashItem()
					self._washAttris[i] = item
				end
				self._washAttris[i]:SetActive(true)
				local baseIndex = (i-1)*3
				local attriKey = washInfo[baseIndex+1]
				local attriValue = washInfo[baseIndex+2]
				local attriQulity = washQualityMap[washInfo[baseIndex+3]]
				-- LoggerHelper.Error("attriKey"..attriKey)
				-- LoggerHelper.Log(washCfg.attri)
				local minValue = washCfg.attri[attriKey][1]
				local maxValue = washCfg.attri[attriKey][2]
				attriValue = AttriDataHelper:ConvertAttriValue(attriKey,attriValue)
				minValue = AttriDataHelper:ConvertAttriValue(attriKey,minValue)
				maxValue = AttriDataHelper:ConvertAttriValue(attriKey,maxValue)

				local colorId = ItemConfig.QualityTextMap[attriQulity]
				local valueStr = "  +"..attriValue.."  ["..minValue.."-"..maxValue.."]"
				local attriNameStr = StringStyleUtil.GetColorStringWithColorId(AttriDataHelper:GetName(attriKey),colorId)
				valueStr = StringStyleUtil.GetColorStringWithColorId(valueStr,colorId)
				self._washAttris[i]:UpdateItem(attriNameStr,valueStr,true)
				h = h+1
			end
			
			for j=h,#self._washAttris do
				self._washAttris[j]:SetActive(false)
			end
			self._washAttrisBase:SetActive(false)
			
		else
			self:ShowWashAttri(false)
		end
	else
		self:ShowWashAttri(false)
	end

	--宝石属性
	local showGem = false
	local showRefine = false
	if self:CheckShowAddition() then
		self:ShowGemInfo(true)
		showGem = true
		local gemInfo = self:GetEquipGemInfo()[self._equipType]
		local gemSlotCount = gemInfo[public_config.EQUIP_GEM_INFO_HOLE_COUNT]
		for i=1,6 do
			if i == 6 then
				self._gemAttris[i]:UpdateItem(gemInfo[i])
				self._gemAttris[i]:UpdateVipLock(self:GetPlayerVIPLevel())
			else
				if i<= gemSlotCount then
					self._gemAttris[i]:SetActive(true)
					self._gemAttris[i]:SetLock(false)
					self._gemAttris[i]:UpdateItem(gemInfo[i])
				else
					self._gemAttris[i]:SetActive(false)
					self._gemAttris[i]:SetLock(true)
				end
			end
		end

		--宝石精炼
		local refineLevel = gemInfo[public_config.EQUIP_GEM_INFO_REFINE_LEVEL]
		if refineLevel then
			self:ShowGemRefine(true)
			showRefine = true
			local refineCfg = EquipDataHelper.GetGemRefineData(refineLevel)
			self._txtRefineRate.text =  "+"..refineCfg.attri_add.."%"
		else
			self:ShowGemRefine(false)
			showRefine = false
		end
	else
		self:ShowGemInfo(false)
		self:ShowGemRefine(false)
		showGem = false
		showRefine = false
	end

	--套装属性
	local suitData = self:GetEquipSuitData()
	local suitLevel = suitData[cfg.type] or 0
	self._suitComHight = 0
	if self:CheckShowAddition() and suitLevel > 0 then
		self._containerSuit:SetActive(true)
		local allEquipIds
		if self._otherPlayerArgs then
			allEquipIds = self._otherPlayerArgs.allEquipIds
		end
		self._containerSuit:UpdateCom(cfg,suitData,suitLevel,allEquipIds)
		self._suitComHight = self._containerSuit:GetHeight()
	else
		self._containerSuit:SetActive(false)
	end

	local arg = {}
	self:UpdateLayout(itemBaseAttrisLen,itemRandAttrisLen,itemWashAttrisLen,showGem,showRefine)
end

EquipTipsView.ItemHeight = 26
EquipTipsView.MaskHeight = 440
--更新布局
function EquipTipsView:UpdateLayout(itemBaseAttrisLen,itemRandAttrisLen,itemWashAttrisLen,showGem,showRefine)
	local curY = 0
	--基础信息
	self:SetPosition(self._containerBaseInfo,curY)
	--基础属性
	curY = curY - 139
	self:SetPosition(self._titleBaseAttri,curY)
	curY = curY - 1
	self:SetPosition(self._containerBaseAttriItemGroup,curY)
	curY = curY - EquipTipsView.ItemHeight*itemBaseAttrisLen
	
	--随机属性
	if itemRandAttrisLen > 0 then
		curY = curY - EquipTipsView.ItemHeight - 5
		self:SetPosition(self._titleRanAttri,curY)
		curY = curY - 1
		self:SetPosition(self._containerRanAttriItemGroup,curY)
		curY = curY - EquipTipsView.ItemHeight * itemRandAttrisLen
	end

	--洗炼属性
	if itemWashAttrisLen > 0 then
		curY = curY - EquipTipsView.ItemHeight - 5
		self:SetPosition(self._titleWash,curY)
		curY = curY - 1
		self:SetPosition(self._containerWashItemGroup,curY)
		curY = curY - EquipTipsView.ItemHeight * itemWashAttrisLen
	end

	--宝石属性
	if showGem then
		curY = curY - EquipTipsView.ItemHeight - 5
		self:SetPosition(self._titleGem,curY)
		curY = curY - 1
		self:SetPosition(self._containerGemItemGroup,curY)
		local totalHeight = 0
		for i=1,#self._gemAttris do
			local gemItemHeight = self._gemAttris[i]:GetHeight()
			if gemItemHeight > 0 then
				self:SetPosition(self._gemAttris[i],-52-totalHeight)
			end
			totalHeight = totalHeight + gemItemHeight
		end
		curY = curY - totalHeight - 10
	end

	if showRefine then
		curY = curY - EquipTipsView.ItemHeight - 5
		self:SetPosition(self._titleGemRefine,curY)
		curY = curY - EquipTipsView.ItemHeight
		self:SetPosition(self._containerGemRefine,curY)
	end

	--套装属性
	if self._suitComHight > 0 then
		self:SetPosition(self._containerSuit,curY)
		curY = curY - self._suitComHight
	end
	
	--装备描述
	if self._desc then
		curY = curY - 75
		self:SetPosition(self._txtDescGO,curY)
	end
	
	--设定高度
	local height = -curY
	if height < EquipTipsView.MaskHeight then
		local dY = EquipTipsView.MaskHeight - height
		self._bgRectTransform.sizeDelta = Vector2(364,604-dY)
		self._frameRectTransform.sizeDelta = Vector2(382,610-dY)
		if self._containerBottom then
			self:SetPosition(self._containerBottom,dY)
		end
		height = EquipTipsView.MaskHeight
		self._scrollViewAttri:SetVerticalMove(false)
		self._rectTransform.sizeDelta = Vector2(364,height)
		self._rectTransform.anchoredPosition = Vector2(0,0)
	else
		self._bgRectTransform.sizeDelta = Vector2(364,604)
		self._frameRectTransform.sizeDelta = Vector2(382,610)
		if self._containerBottom then
			self:SetPosition(self._containerBottom,0)
		end
		self._scrollViewAttri:SetVerticalMove(true)
		self._rectTransform.sizeDelta = Vector2(364,height)
		self._scrollViewAttri:ResetContentPosition()
	end
end

