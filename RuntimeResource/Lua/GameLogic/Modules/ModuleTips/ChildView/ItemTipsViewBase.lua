-- ItemTipsViewBase.lua

local ItemTipsViewBase = Class.ItemTipsViewBase(ClassTypes.BaseComponent)
local BagManager = PlayerManager.BagManager
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local public_config = GameWorld.public_config
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local GuildManager = PlayerManager.GuildManager
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local ShopManager = PlayerManager.ShopManager
local bagData = PlayerManager.PlayerDataManager.bagData
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

--基础信息UI初始化
function ItemTipsViewBase:InitBase()
	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtTypeName = self:GetChildComponent("Text_TypeName",'TextMeshWrapper')

	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)

	self._containerQualityBg = self:FindChildGO("Container_QualityBg")
end

--override
function ItemTipsViewBase:OnPointerDown(pointerEventData)
end

function ItemTipsViewBase:OnPointerUp(pointerEventData)
end

function ItemTipsViewBase:SetPosition(com,y)
	com.transform.anchoredPosition = Vector3(com.transform.anchoredPosition.x,y,0)
	--com.transform.localPosition = Vector3(com.transform.localPosition.x,y,0)
end

--根据物品ID更新tip
function ItemTipsViewBase:UpdateViewBaseInfo(itemId)
	local cfgData = ItemDataHelper.GetItem(itemId)
	local player = GameWorld.Player()
	local colorId

	GameWorld.AddIcon(self._containerQualityBg,4500 + cfgData.quality)
 
	if ItemDataHelper.CheckLevelMatch(itemId,player.level) then
		colorId = ItemConfig.ItemNormal
	else
		colorId = ItemConfig.ItemForbidden
	end

	local levelNeedStr = StringStyleUtil.GetLevelString(cfgData.levelNeed,nil,true)
	self._txtUseLv.text = StringStyleUtil.GetColorStringWithColorId(levelNeedStr,colorId)

	if ItemDataHelper.CheckVocationMatch(itemId,player.vocation) then
		colorId = ItemConfig.ItemNormal
	else
		colorId = ItemConfig.ItemForbidden
	end
	self._txtVocation.text = StringStyleUtil.GetColorStringWithColorId(ItemDataHelper.GetVocationString(itemId),colorId)

	local cnType = ItemDataHelper.GetTypeNameByType(cfgData.itemType,cfgData.type)
	self._txtTypeName.text = LanguageDataHelper.CreateContent(cnType)
	self:SetItemDesc(itemId)

	--设定绑定状态
	if self:HasBindType() then
		if self._txtBind == nil then
			self._txtBind = self:GetChildComponent("Text_Bind",'TextMeshWrapper')
		end
		if self._bind == 1 then
			self._txtBind.text = LanguageDataHelper.CreateContent(165)
		else
			local bindType = cfgData.bindType
			if bindType == 2 then
				self._txtBind.text = LanguageDataHelper.CreateContent(166)
			elseif bindType == 3 then
				self._txtBind.text = LanguageDataHelper.CreateContent(167)
			else
				self._txtBind.text = ""
			end
		end
	end
	if self._itemData then
		self._icon:SetItemData(self._itemData,true)
	else
		self._icon:SetItem(itemId)
	end
	
end

function ItemTipsViewBase:HasBindType()
	return true
end

--设定道具描述
function ItemTipsViewBase:SetItemDesc(itemId)
	self._txtDesc.text = ItemDataHelper.GetDescription(itemId)
end

--设定道具名称
function ItemTipsViewBase:SetItemName(itemId)
    self._txtName.text = ItemDataHelper.GetItemNameWithColor(itemId)
end


----------------------------------按钮点击处理----------------------------------------------
function ItemTipsViewBase:SetButtonArgs(buttonArgs)
	self._buttonArgs = buttonArgs
end

--按钮是否显示判定
function ItemTipsViewBase:AddBtnToShowList(btnShow,condition,showList,btn)

	if btnShow and condition then
		table.insert(showList,btn)
		btn:SetActive(true)
	else
		btn:SetActive(false)
	end
end

--检查是否能上架
function ItemTipsViewBase:CheckShowForSale(cfgData)
	--功能未开启
	if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_MARKET_SELL) then
		return
	end
	if self._itemData == nil then
		return false
	else
		if (self._itemData:BindFlag() == nil or self._itemData:BindFlag() == 0) 
			and cfgData.auctionType and cfgData.auctionType > 0 then
			return true
		else
			return false
		end
	end
	
end

--是否能出售
function ItemTipsViewBase:CheckCanSell(cfgData)
	local as = cfgData.allowSale
	--守护装备特殊处理,超时才允许出售
	if self._itemData and cfgData.itemType == public_config.ITEM_ITEMTYPE_EQUIP and
		cfgData.type == public_config.PKG_LOADED_EQUIP_INDEX_GUARD and self._itemData then
		local timeEnd = self._itemData:GetTimeEnd()
		local timeLeft = timeEnd - DateTimeUtil.GetServerTime()
		return timeLeft <= 0
	end

	if as and as > 0 then
		return true
	end
	return false
end

--出售道具
function ItemTipsViewBase:OnClickSell()
	if self._itemData.cfgData.allowSale == 2 then
		local str  = LanguageDataHelper.CreateContent(58769) --"存在价值较高的物品，确认要出售吗？"
		GUIManager.ShowMessageBox(2,str,function() self:RequestSell() end)
	else
		self:RequestSell()
	end
end

function ItemTipsViewBase:RequestSell()
	if self._bagType and self._bagType>0 then
		BagManager:RequestSell(self._bagType..","..tostring(self._itemData.bagPos))
	end
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

-- function ItemTipsViewBase:ShowFunctionButtons(shouldShow)
-- 	self._showFunctionButtons = shouldShow
-- end

--存入道具
function ItemTipsViewBase:OnClickSave()
	local emptyPos = bagData:GetPkg(public_config.PKG_TYPE_WAREHOUSE):GetOneEmtpyPos()
    if emptyPos > 0 then
        BagManager:RequestMoveItem(public_config.PKG_TYPE_ITEM,self._itemData.bagPos,public_config.PKG_TYPE_WAREHOUSE,emptyPos)
    end
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--取出道具
function ItemTipsViewBase:OnClickGet()
	local emptyPos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetOneEmtpyPos()
    if emptyPos > 0 then
        BagManager:RequestMoveItem(self._itemData.bagType,self._itemData.bagPos,public_config.PKG_TYPE_ITEM,emptyPos)
    end
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--交易所购买
function ItemTipsViewBase:OnClickBuy()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRADE_CONFIRM_BUY)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--交易所上架物品
function ItemTipsViewBase:OnClickForSale()
	local data = {["id"]=MessageBoxType.TradeSell,["value"]=self._itemData}
    GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--交易所下架物品
function ItemTipsViewBase:OnClickOffSell()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRADE_OFF_SELL_ITEM)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--帮派兑换道具
function ItemTipsViewBase:OnClicGuildExchange()
	GuildManager:GuildWarehouseBuyReq(self._itemData.bagPos, self._geCount)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--商城购买道具
function ItemTipsViewBase:OnClickShopBuy()
	ShopManager:BuyShopItem(self._shopBuyId, self._shopBuyCount, 0)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end


--引导购买道具
function ItemTipsViewBase:OnClickGuideBuy()
	--LoggerHelper.Log("self._singleShopPrice:" ..PrintTable:TableToStr(self._singleShopPrice))
	local totalPrice = self._shopBuyCount*self._singleShopPrice

	local itemCostNum = ItemDataHelper.GetGuideBuyCost(self._itemId)
	--LoggerHelper.Error("itemCostNum"..PrintTable:TableToStr(itemCostNum))
	if itemCostNum then
		for i,item in ipairs(itemCostNum) do
			if item[1] == 3 then
				if GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS]] >= totalPrice then
					self:OnClickGuideBuySure()
					
				else
					self:OnClickGuideBuySure()
					--ShopManager:BuyGuideItem(self._itemId,self._shopBuyCount)
					-- local argsTable = LanguageDataHelper.GetArgsTable()
					-- argsTable["0"] = totalPrice - GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS]]
					-- local str = LanguageDataHelper.CreateContent(58651, argsTable)
					-- GUIManager.ShowMessageBox(2, str,
					-- function()  end,
					-- function()GUIManager.ClosePanel(PanelsConfig.ItemTips) end)
				end
			else
				if GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS_BIND]] >= totalPrice then
					self:OnClickGuideBuySure()
				else
					local argsTable = LanguageDataHelper.GetArgsTable()
					argsTable["0"] = totalPrice - GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS_BIND]]
					local str = LanguageDataHelper.CreateContent(58718, argsTable)
					GUIManager.ShowMessageBox(2, str,
					function() self:OnClickGuideBuySure() end,
					function()GUIManager.ClosePanel(PanelsConfig.ItemTips) end)
				end
			end
		end
	end


end


-- GUIManager.ShowMessageBox(1, "仍有必要资源需要下载，\n是否继续？（取消后退出游戏）",
-- function()self:CheckResourceReady(self._callback) end,
-- function()Application.Quit() end)


function ItemTipsViewBase:OnClickGuideBuySure()
	ShopManager:BuyGuideItem(self._itemId,self._shopBuyCount)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

function ItemTipsViewBase:OnClickOpenRankBuy()
	ServiceActivityManager:BuyOpenItem(self._openId,self._shopBuyCount)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end


function ItemTipsViewBase:OnShow()
	
end

function ItemTipsViewBase:OnClose()
	self._itemData = nil
	self._itemId = nil
end
