-- DragonSoulTipsView.lua
require("Modules.ModuleTips.ChildView.ItemTipsViewBase")
local DragonSoulTipsView = Class.DragonSoulTipsView(ClassTypes.ItemTipsViewBase)
DragonSoulTipsView.interface = GameConfig.ComponentsConfig.PointableComponent
DragonSoulTipsView.classPath = "Modules.ModuleTips.ChildView.DragonSoulTipsView"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemConfig = GameConfig.ItemConfig
local StringStyleUtil = GameUtil.StringStyleUtil
local DragonsoulDataHelper = GameDataHelper.DragonsoulDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DragonSoulManager = PlayerManager.DragonSoulManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local XGameObjectComplexIcon = GameMain.XGameObjectComplexIcon

function DragonSoulTipsView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self:InitBase()
	self:InitButtons()
end

function DragonSoulTipsView:InitBase()
	local containerIcon = self:FindChildGO("Container_Icon")
	self._complexIcon = containerIcon:AddComponent(typeof(XGameObjectComplexIcon))

	self._containerQualityBg = self:FindChildGO("Container_QualityBg")

	self._txtAttgo1 = self:FindChildGO("Text_Attri1")
	self._txtAttgo2 = self:FindChildGO("Text_Attri2")
	self._txtAttgos = {}
	self._txtAttgos[1]=self._txtAttgo1
	self._txtAttgos[2]=self._txtAttgo2

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	--self._txtTypeName = self:GetChildComponent("Text_TypeName",'TextMeshWrapper')

	self._txtSlot = self:GetChildComponent("Text_Slot",'TextMeshWrapper')
	self._txtAttri1 = self:GetChildComponent("Text_Attri1",'TextMeshWrapper')
	self._txtAttri2 = self:GetChildComponent("Text_Attri2",'TextMeshWrapper')
	self._txtAtts = {}
	self._txtAtts[1] = self._txtAttri1
	self._txtAtts[2] = self._txtAttri2
	self._containerDecompose = self:FindChildGO("Container_Decompose")
	--self._containerDecomposeTrans = self._containerDecompose:GetComponent(typeof(UnityEngine.RectTransform))

	local decomposeIcon = self:FindChildGO("Container_Decompose/Container_DecomposeIcon")
	GameWorld.AddIcon(decomposeIcon,ItemDataHelper.GetIcon(public_config.MONTY_TYPE_DRAGON_PICEC))
	self._txtDecomposeNum = self:GetChildComponent("Container_Decompose/Text_DecomposeNum",'TextMeshWrapper')
end

function DragonSoulTipsView:InitButtons()
	self._containerBottom = self:FindChildGO("Container_Bottom")
	
	self._btnPromote 		= self:FindChildGO("Container_Bottom/Button_Promote")
	self._btnCompose 	= self:FindChildGO("Container_Bottom/Button_Compose")
	self._btnUpgrade 	= self:FindChildGO("Container_Bottom/Button_Upgrade")
	self._btnDisassembly 		= self:FindChildGO("Container_Bottom/Button_Disassembly")
	self._btnUnload 		= self:FindChildGO("Container_Bottom/Button_Unload")
	self._btnEquip 		= self:FindChildGO("Container_Bottom/Button_Equip")
	self._btnDecompose 		= self:FindChildGO("Container_Bottom/Button_Decompose")
	self._redUpGo 		= self:FindChildGO("Container_Bottom/Button_Upgrade/Image")

	self._imageIconBg 		= self:FindChildGO("Image_IconBg")
	
	self._buttonList = {}
	
	self._csBH:AddClick(self._btnPromote, 		function() self:OnClickPromote() end)
	self._csBH:AddClick(self._btnCompose, 		function() self:OnClickCompose() end)
	self._csBH:AddClick(self._btnUpgrade, 		function() self:OnClickUpgrade() end)
	self._csBH:AddClick(self._btnDisassembly, 	function() self:OnClickDisassembly() end)
	self._csBH:AddClick(self._btnUnload, 		function() self:OnClickUnload() end)
	self._csBH:AddClick(self._btnEquip, 		function() self:OnClickEquip() end)
	self._csBH:AddClick(self._btnDecompose, 	function() self:OnClickDecompose() end)
end

--晋升
function DragonSoulTipsView:OnClickPromote()
	--DragonSoulManager.UPLevelRpc(self._slot)
	if GUIManager.reopenSubMainMenu > 0 then
        GUIManager.reopenSubMainMenu = GUIManager.reopenSubMainMenu + 1
    end
	local data={}
    data.firstTabIndex=3
	GUIManager.ShowPanel(PanelsConfig.Compose,data)
	GUIManager.ClosePanel(PanelsConfig.Tips)
end

--凝聚
function DragonSoulTipsView:OnClickCompose()
	if GUIManager.reopenSubMainMenu > 0 then
        GUIManager.reopenSubMainMenu = GUIManager.reopenSubMainMenu + 1
    end
	local data={}
    data.firstTabIndex=3
	GUIManager.ShowPanel(PanelsConfig.Compose,data)
	GUIManager.ClosePanel(PanelsConfig.Tips)
end

--升级
function DragonSoulTipsView:OnClickUpgrade()
	GUIManager.ClosePanel(PanelsConfig.Tips)
	EventDispatcher:TriggerEvent(GameEvents.DragonSoulUpView,{self._slot,self._soulData._id,self._soulData._level})
end

--拆解
function DragonSoulTipsView:OnClickDisassembly()
	GUIManager.ClosePanel(PanelsConfig.Tips)
	local t = 1
	if self._isBag then
		t=2
	end
	EventDispatcher:TriggerEvent(GameEvents.DragonSoulDisView,{self._slot,self._soulData._id,self._soulData._level,t})
end

--卸载
function DragonSoulTipsView:OnClickUnload()
	DragonSoulManager.PutOffRpc(self._slot)
	GUIManager.ClosePanel(PanelsConfig.Tips)
end

--佩戴
function DragonSoulTipsView:OnClickEquip()
	-- if not DragonSoulManager:IsHaveType(self._soulData._id) then
		DragonSoulManager.PutOnRpc(self._slot)
		GUIManager.ClosePanel(PanelsConfig.Tips)
	-- end
end

--分解
function DragonSoulTipsView:OnClickDecompose()
	if self._soulData:GetQuality()>3 then
		local d = {}
		local data = {}
		d.id = MessageBoxType.Tip
		d.value =  data
		data.confirmCB=function()DragonSoulManager.PutResolveRpc(self._slot) GUIManager.ClosePanel(PanelsConfig.Tips) end
		data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.Tips) end
		data.text = LanguageDataHelper.GetContent(61014)
		GUIManager.ShowPanel(PanelsConfig.MessageBox, d)
		return
	end
	DragonSoulManager.PutResolveRpc(self._slot)
	GUIManager.ClosePanel(PanelsConfig.Tips)
end

--筛选可显示的按钮
function DragonSoulTipsView:FiltButtons()
	local showList = {}
	self:AddBtnToShowList(self._buttonArgs.equip,self:CheckEquip(),showList,self._btnEquip)
	self:AddBtnToShowList(self._buttonArgs.promote,self:CheckPromote(),showList,self._btnPromote)
	self:AddBtnToShowList(self._buttonArgs.compose,self:CheckCompose(),showList,self._btnCompose)
	self:AddBtnToShowList(self._buttonArgs.upgrade,self:CheckUpgrade(),showList,self._btnUpgrade)
	self:AddBtnToShowList(self._buttonArgs.disassembly,self:CheckDisassembly(),showList,self._btnDisassembly)
	self:AddBtnToShowList(self._buttonArgs.unload,self:CheckUnload(),showList,self._btnUnload)
	self:AddBtnToShowList(self._buttonArgs.decompose,self:CheckDecompose(),showList,self._btnDecompose)

	for i=1,#showList do
		local curY = (i-1)*52 + 22
		self:SetPosition(showList[i],curY)
	end
end
-- 晋升
function DragonSoulTipsView:CheckPromote()
	if self._soulData:IsDoubleAtt() and not self._soulData:IsFulllevel() then
		return true
	end
	return false
end

-- 升级
function DragonSoulTipsView:CheckUpgrade()
	if self._isBag then
		return false
	end
	if (not self._soulData:IsFulllevel()) and self._soulData:GetTypes()[1]~=99 then
		self._redUpGo:SetActive(DragonSoulManager:RefreshUpRedPoint())
		return true
	end
	return false
end

-- 卸下
function DragonSoulTipsView:CheckUnload()
	if self._isBag then
		return false
	end
	return true
end

-- 佩戴
function DragonSoulTipsView:CheckEquip()
	if self._isBag and self._soulData:GetTypes()[1]~=99 then
		return true
	end
	return false
end
-- 合成
function DragonSoulTipsView:CheckCompose()
	if not self._soulData:IsDoubleAtt() and not self._soulData:IsFulllevel() and self._soulData:GetQuality()>3 then
		return true
	end
	return false
end

-- 拆解
function DragonSoulTipsView:CheckDisassembly()
	if self._soulData:IsDoubleAtt() then
		return true
	end
	return false
end

-- 分解
function DragonSoulTipsView:CheckDecompose()
	if self._isBag then
		if not self._soulData:IsDoubleAtt() then
			return true
		end
		return false
	else
		return false
	end
end


--更新数据
function DragonSoulTipsView:UpdateView(data)
	self._buttonArgs = data.buttonArgs
	local soulData = data.dragonSoulItemData
	self._soulData = soulData
	self._isBag = data.isBag
	self._slot = data.slot
	local icon = soulData:GetIcon()

	self._complexIcon:AddComplexIcon(icon)
    self._complexIcon:StartPlay()
	local name = soulData:GetName()
	self._txtName.text = name.."Lv."..soulData:GetLevel()
	local quality = DragonsoulDataHelper:GetQuality(soulData._id)
	GameWorld.AddIcon(self._containerQualityBg,4500 + quality)
	GameWorld.AddIcon(self._imageIconBg,4100 + quality)

	self._txtSlot.text = soulData:GetTypeName()
	-- self._txtAttgo1:SetActive(false)
	-- self._txtAttgo2:SetActive(false)
	self._txtAtts[1].text = ''
	self._txtAtts[2].text = ''
	local attri = soulData:GetAtt()
	local h = 0
	if attri then
		for k,v in pairs(attri) do
			local str = AttriDataHelper:GetName(k)
			str = str..StringStyleUtil.GetColorStringWithColorId("+"..AttriDataHelper:ConvertAttriValue(k,v),ItemConfig.ItemAttriValue)
			h = h+1
			-- self._txtAttgos[h]:SetActive(true)
			self._txtAtts[h].text = str
		end
	end
	if h > 1 then
		self._containerDecompose:SetActive(false)
	else
		self._containerDecompose:SetActive(true)
		if soulData:GetResolve() then
			self._txtDecomposeNum.text = soulData:GetResolve()[public_config.MONTY_TYPE_DRAGON_PICEC]
		end
	end

	--按钮处理
	if self._buttonArgs then
		self._containerBottom:SetActive(true)
		self:FiltButtons()
	else
		self._containerBottom:SetActive(false)
	end
end



