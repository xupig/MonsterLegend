-- StrengthStarGemTipsView.lua
require("Modules.ModuleTips.ChildView.ItemTipsViewBase")
local StrengthStarGemTipsView = Class.StrengthStarGemTipsView(ClassTypes.ItemTipsViewBase)
StrengthStarGemTipsView.interface = GameConfig.ComponentsConfig.PointableComponent
StrengthStarGemTipsView.classPath = "Modules.ModuleTips.ChildView.StrengthStarGemTipsView"

local EquipStrengthTotalDataHelper = GameDataHelper.EquipStrengthTotalDataHelper
local EquipStarTotalDataHelper = GameDataHelper.EquipStarTotalDataHelper
local GemLevelTotalDataHelper = GameDataHelper.GemLevelTotalDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper

local TITLE_NAME = {"累计强化加成","装备星级加成","全身宝石加成"} 
local CONDITION = {"装备累积强化+%d级","全身装备星级达到%d星","全身装备宝石共%d级"} 
--local END_CONDITION = {"强化总等级：%d / %d","装备总星级：%d / %d","装备宝石总等级：%d / %d"} 
local CURRENT_VALUE = {"当前强化+%d级","当前星级共%d星","当前宝石共%d级"} 
local ATTRI_VALUE = "%s：<#55dd22>+%s</color>"
local DATA_HELPER = {EquipStrengthTotalDataHelper,EquipStarTotalDataHelper,GemLevelTotalDataHelper}
local ATTRI_COUNT = 3
local BG_HEIGHT = {280, 375, 240}

function StrengthStarGemTipsView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self:InitBase()
	self:InitButtons()
end

function StrengthStarGemTipsView:InitBase()
	self._containerQualityBg = self:FindChildGO("Container_BG/Container_QualityBg")
	GameWorld.AddIcon(self._containerQualityBg,4503)

	self._textName = self:GetChildComponent("Container_BG/Text_Name",'TextMeshWrapper')
	self._textCurTitle1 = self:GetChildComponent("Container_Text/Text_Title1",'TextMeshWrapper')
	self._textCurTitle2 = self:GetChildComponent("Container_Text/Text_Title2",'TextMeshWrapper')
	self._goCurTitle1 = self:FindChildGO("Container_Text/Text_Title1")
	
	self._textNextTitle1 = self:GetChildComponent("Container_Text/Text_NextTitle1",'TextMeshWrapper')
	self._textNextTitle2 = self:GetChildComponent("Container_Text/Text_NextTitle2",'TextMeshWrapper')
	self._goNextTitle1 = self:FindChildGO("Container_Text/Text_NextTitle1")
	self._goNextTitle2 = self:FindChildGO("Container_Text/Text_NextTitle2")
	self._textNextTitle1.text = "下阶目标"

	self._textCurAttris = {}
	self._textNextAttris = {}
	self._goCurAttris = {}
	self._goNextAttris = {}
	local pathCurAttri = "Container_Text/Text_Attri"
	local pathNextAttri = "Container_Text/Text_NextAttri"
	for i= 1,ATTRI_COUNT do
		self._textCurAttris[i] = self:GetChildComponent(pathCurAttri..i,'TextMeshWrapper')
		self._textNextAttris[i] = self:GetChildComponent(pathNextAttri..i,'TextMeshWrapper')
		self._goCurAttris[i] = self:FindChildGO(pathCurAttri..i)
		self._goNextAttris[i] = self:FindChildGO(pathNextAttri..i)
	end

	self._bgRect = self:GetChildComponent("Container_BG/Image_Bg", "RectTransform")
	self._frameBgRect = self:GetChildComponent("Container_BG/Image_Frame", "RectTransform")
end

function StrengthStarGemTipsView:InitButtons()
end

function StrengthStarGemTipsView:SetBG(type)
	self._bgRect.sizeDelta = Vector3.New(self._bgRect.sizeDelta.x, BG_HEIGHT[type], self._bgRect.sizeDelta.z)
	self._frameBgRect.sizeDelta = Vector3.New(self._frameBgRect.sizeDelta.x, BG_HEIGHT[type]+6, self._frameBgRect.sizeDelta.z)
end

--更新数据
function StrengthStarGemTipsView:UpdateView(data)
	local subType = data.subType
	local curValue = GameWorld.Player().addition_info[subType]
	--curValue = 4
	local dataHelper = DATA_HELPER[subType]
	local id = dataHelper:BinarySerch(curValue)
	-- LoggerHelper.Log("星级数据")
	-- LoggerHelper.Log(GameWorld.Player().addition_info)
	self._textName.text = TITLE_NAME[subType]
	if id == 0 then
		self:SetBG(1)
		self:SetBegin(subType, curValue)
		local nextRange = dataHelper:GetRange(id+1)
		local nextAttriTable = dataHelper:GetAttris(id+1)
		local nextCondition = nextRange[1]
		self:SetNextView(subType, nextCondition, nextAttriTable)
	elseif id == #dataHelper:GetAllId() then
		self:SetBG(3)
		self:SetEnd(subType)
		local curRange = dataHelper:GetRange(id)
		local curAttriTable = dataHelper:GetAttris(id)
		local curCondition = curRange[1]
		self:SetCurView(subType, curCondition, curAttriTable, curValue)
	else
		self:SetBG(2)
		local curRange = dataHelper:GetRange(id)
		local curAttriTable = dataHelper:GetAttris(id)
		local curCondition = curRange[1]
		self:SetCurView(subType, curCondition, curAttriTable, curValue)

		local nextRange = dataHelper:GetRange(id+1)
		local nextAttriTable = dataHelper:GetAttris(id+1)
		local nextCondition = nextRange[1]
		self:SetNextView(subType, nextCondition, nextAttriTable)
	end
end

function StrengthStarGemTipsView:SetBegin(subType, curValue)
	self._goCurTitle1:SetActive(false)
	for index = 1 , ATTRI_COUNT do
		self._goCurAttris[index]:SetActive(false)
	end
	self._textCurTitle2.text = string.format(CURRENT_VALUE[subType], curValue)
end

function StrengthStarGemTipsView:SetEnd(subType)
	self._goNextTitle1:SetActive(false)
	self._goNextTitle2:SetActive(false)
	for index = 1 , ATTRI_COUNT do
		self._goNextAttris[index]:SetActive(false)
	end
end

function StrengthStarGemTipsView:SetCurView(subType, curCondition, curAttriTable, curValue)
	self._goCurTitle1:SetActive(true)
	self._textCurTitle1.text = string.format(CONDITION[subType], curCondition)
	self._textCurTitle2.text = string.format(CURRENT_VALUE[subType], curValue)
	self:SetAttris(self._textCurAttris, self._goCurAttris, curAttriTable)
end

function StrengthStarGemTipsView:SetNextView(subType, nextCondition, nextAttriTable)
	self._goNextTitle1:SetActive(true)
	self._goNextTitle2:SetActive(true)
	self._textNextTitle2.text = string.format(CONDITION[subType], nextCondition)
	self:SetAttris(self._textNextAttris, self._goNextAttris, nextAttriTable)
end

function StrengthStarGemTipsView:SetAttris(textList, goList, attriTable)
	if attriTable ~= nil then
		local i = 1
		for attriKey, attriValue in pairs(attriTable) do
			goList[i]:SetActive(true)
			local name = AttriDataHelper:GetName(attriKey)
			local value = AttriDataHelper:ConvertAttriValue(attriKey,attriValue)
			textList[i].text = string.format(ATTRI_VALUE, name, value)
			i = i + 1
		end

		if i <= ATTRI_COUNT then
			for index = i , ATTRI_COUNT do
				goList[index]:SetActive(false)
			end
		end
	end
end




