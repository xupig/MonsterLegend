-- EquipMythicalAnimalTipsView.lua

local EquipMythicalAnimalTipsView = Class.EquipMythicalAnimalTipsView(ClassTypes.ItemTipsViewBase)
EquipMythicalAnimalTipsView.interface = GameConfig.ComponentsConfig.PointableComponent
EquipMythicalAnimalTipsView.classPath = "Modules.ModuleTips.ChildView.EquipMythicalAnimalTipsView"

require "UIComponent.Base.UIScrollView"
require "UIComponent.Extend.AttriCom"

local UIScrollView = ClassTypes.UIScrollView
local AttriCom = ClassTypes.AttriCom
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local EquipManager = GameManager.EquipManager
local BagManager = PlayerManager.BagManager
local bagData = PlayerManager.PlayerDataManager.bagData
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemConfig = GameConfig.ItemConfig
local EquipDataHelper = GameDataHelper.EquipDataHelper
local BaseUtil = GameUtil.BaseUtil
local StringStyleUtil = GameUtil.StringStyleUtil
local AnimalManager = PlayerManager.AnimalManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MythicalAnimalsLevelDataHelper = GameDataHelper.MythicalAnimalsLevelDataHelper

function EquipMythicalAnimalTipsView:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self:InitListener()
	self:InitBg()
	self:InitBase()
	self:InitItemPool()
	self:InitScrollContent()
end

function EquipMythicalAnimalTipsView:HasBindType()
	return false
end

function EquipMythicalAnimalTipsView:InitListener()
	
end

function EquipMythicalAnimalTipsView:InitBg()
	self._bg = self:FindChildGO("Image_Bg")
	self._bgRectTransform = self._bg:GetComponent(typeof(UnityEngine.RectTransform))
	self._bgFrame = self:FindChildGO("Image_Frame")
	self._frameRectTransform = self._bgFrame:GetComponent(typeof(UnityEngine.RectTransform))
end

function EquipMythicalAnimalTipsView:InitBase()
	self._base.InitBase(self)

	local rootPath = "ScrollView_Attri/Mask/Content/"

	self._txtUseLv = self:GetChildComponent(rootPath.."Container_BaseInfo/Text_Level",'TextMeshWrapper')
	self._txtVocation = self:GetChildComponent(rootPath.."Container_BaseInfo/Text_Vocation",'TextMeshWrapper')
	
	self._txtScore = self:GetChildComponent(rootPath.."Container_BaseInfo/Text_Score",'TextMeshWrapper')
	self._txtBaseScore = self:GetChildComponent(rootPath.."Container_BaseInfo/Text_BaseScore",'TextMeshWrapper')

	self._imgLoaded = self:FindChildGO("Image_Loaded")
	self._txtDescGO = self:FindChildGO(rootPath.."Text_Desc")
	self._txtDesc = self._txtDescGO:GetComponent('TextMeshWrapper')
end

--初始化对比部分内容
function EquipMythicalAnimalTipsView:InitCompareComps()
	local rootPath = "ScrollView_Attri/Mask/Content/"
	self._compareScore = self:FindChildGO(rootPath.."Container_BaseInfo/Container_ScoreUp")
	self._imgArrowUp = self:FindChildGO(rootPath.."Container_BaseInfo/Container_ScoreUp/Image_IconUp")
	self._imgArrowDown = self:FindChildGO(rootPath.."Container_BaseInfo/Container_ScoreUp/Image_IconDown")
	--self._txtCompareScore = self:GetChildComponent(rootPath.."Container_BaseInfo/Container_ScoreUp/Text_ScoreUp",'TextMeshWrapper')
	self._compareScore:SetActive(false)
end

function EquipMythicalAnimalTipsView:InitButtons()
	self._containerBottom = self:FindChildGO("Container_Bottom")
	
	self._buttonInited = true
	self._btnEquip 		= self:FindChildGO("Container_Bottom/Button_Equip")
	self._btnSell 		= self:FindChildGO("Container_Bottom/Button_Sell")
	self._btnUnload 	= self:FindChildGO("Container_Bottom/Button_Unload")
	self._btnStrength 	= self:FindChildGO("Container_Bottom/Button_Strength")

	self._buttonList = {}
	
	self._csBH:AddClick(self._btnEquip, 		function() self:OnClickEquip() end)
	self._csBH:AddClick(self._btnSell, 			function() self:OnClickSell() end)
	self._csBH:AddClick(self._btnUnload, 		function() self:OnClickUnload() end)
	self._csBH:AddClick(self._btnStrength, 		function() self:OnClickStrength() end)
end

--装装备
function EquipMythicalAnimalTipsView:OnClickEquip()
	AnimalManager:LoadEquip(self._itemData:GetAnimalId(), self._bagPos)
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--卸装备
function EquipMythicalAnimalTipsView:OnClickUnload()
	--LoggerHelper.Error("self._itemData:GetAnimalId() ====" .. tostring(self._itemData:GetAnimalId()))
	--LoggerHelper.Error("self._bagPos ====" .. tostring(self._bagPos))
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
	local animalId = self._itemData:GetAnimalId()
	if AnimalManager:IsAwake(animalId) then
		local value = {
            ["confirmCB"] = function() self:UnloadEquip(animalId) end,
            ["cancelCB"] = nil,
            ["text"] = LanguageDataHelper.CreateContent(76410)
        }
        local msgData = {["id"] = MessageBoxType.Tip, ["value"] = value}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
	else
		self:UnloadEquip(animalId)
	end
end

function EquipMythicalAnimalTipsView:UnloadEquip(animalId)
	AnimalManager:UnloadEquip(animalId, self._bagPos)
end

--出售
function EquipMythicalAnimalTipsView:OnClickSell()
	BagManager:RequestSell(self._bagType..","..tostring(self._itemData.bagPos))
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--强化
function EquipMythicalAnimalTipsView:OnClickStrength()
	GUIManager.ClosePanel(PanelsConfig.ItemTips)
	EventDispatcher:TriggerEvent(GameEvents.ON_UPGRADE_EQUIP_CLICK)
end

--筛选可显示的按钮
function EquipMythicalAnimalTipsView:FiltButtons()
	local showList = {}

	self:AddBtnToShowList(self._buttonArgs.equip,true,showList,self._btnEquip)
	self:AddBtnToShowList(self._buttonArgs.sell,true,showList,self._btnSell)
	self:AddBtnToShowList(self._buttonArgs.unload,true,showList,self._btnUnload)
	self:AddBtnToShowList(self._buttonArgs.strength,true,showList,self._btnStrength)

	for i=1,#showList do
		local curY = (i-1)*52 + 22
		self:SetPosition(showList[i],curY)
	end
end

function EquipMythicalAnimalTipsView:InitScrollContent()
	self._scroll = UIComponentUtil.FindChild(self.gameObject, "ScrollView_Attri")
	self._scrollViewAttri = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
	self._scrollContent = self:FindChildGO("ScrollView_Attri/Mask/Content")
	self._rectTransform = self._scrollContent:GetComponent(typeof(UnityEngine.RectTransform))
	local rootPath = "ScrollView_Attri/Mask/Content/"

	--基础信息
	self._containerBaseInfo = self:FindChildGO(rootPath.."Container_BaseInfo")
	--基础属性部分
	self._titleBaseAttri = self:FindChildGO(rootPath.."Text_TitleBaseAttri")
	self._containerBaseAttriItemGroup = self:FindChildGO(rootPath.."Container_AttriItemGroup")

	--随机属性部分
	self._titleRanAttri = self:FindChildGO(rootPath.."Text_TitleRanAttri")
	self._containerRanAttriItemGroup = self:FindChildGO(rootPath.."Container_RanAttriItemGroup")
end

function EquipMythicalAnimalTipsView:ShowRandAttri(shouldShow)
	self._titleRanAttri:SetActive(shouldShow)
	self._containerRanAttriItemGroup:SetActive(shouldShow)
end

--初始化属性栏对象池
function EquipMythicalAnimalTipsView:InitItemPool()
	--对象池
	self._baseAttris = {}
	self._randAttris = {}
	
	self._baseAttrisBase = self:FindChildGO("ScrollView_Attri/Mask/Content/Container_AttriItemGroup/Container_AttriItem")
	self._randAttrisBase = self:FindChildGO("ScrollView_Attri/Mask/Content/Container_RanAttriItemGroup/Container_RanAttriItem")
	
	self._baseAttrisBase:SetActive(false)
	self._randAttrisBase:SetActive(false)
end

function EquipMythicalAnimalTipsView:CreateAttriItem()
	local go = self:CopyUIGameObject("ScrollView_Attri/Mask/Content/Container_AttriItemGroup/Container_AttriItem", "ScrollView_Attri/Mask/Content/Container_AttriItemGroup")
    local item = UIComponentUtil.AddLuaUIComponent(go, AttriCom)
    return item
end

function EquipMythicalAnimalTipsView:CreateRanAttriItem()
	local go = self:CopyUIGameObject("ScrollView_Attri/Mask/Content/Container_RanAttriItemGroup/Container_RanAttriItem", "ScrollView_Attri/Mask/Content/Container_RanAttriItemGroup")
   	local item = UIComponentUtil.AddLuaUIComponent(go, AttriCom)
    return item
end

--更新比较结果
function EquipMythicalAnimalTipsView:UpdateCompare(scoreCompare)
	self._compareScore:SetActive(true)
	--local colorId
	if scoreCompare >= 0 then
		self._imgArrowUp:SetActive(true)
		self._imgArrowDown:SetActive(false)
		--colorId = ItemConfig.ItemAttriValue
	else
		scoreCompare = -scoreCompare
		self._imgArrowUp:SetActive(false)
		self._imgArrowDown:SetActive(true)
		--colorId = ItemConfig.ItemForbidden
	end

	--self._txtCompareScore.text = ConvertStyle(string.format("$(%d,%s)",colorId,tostring(scoreCompare)))
end

function EquipMythicalAnimalTipsView:HideCompare()
	self._compareScore:SetActive(false)
end

--更新数据
function EquipMythicalAnimalTipsView:UpdateView(equipData)
	self._itemData = equipData
	self._bagType = equipData.bagType
	self._bagPos = equipData.bagPos
	self._itemCfg = equipData.cfgData
	local itemId = equipData.cfg_id
	self._base.UpdateViewBaseInfo(self,itemId)
    local green = ItemConfig.ItemAttriValue
    local strengthLevel = equipData:GetMythicalAnimalEquipLevel()

	if strengthLevel > 0  then
		self._txtName.text = equipData:GetItemName()..StringStyleUtil.GetColorStringWithColorId("  +"..strengthLevel,green)
	else
		self._txtName.text = equipData:GetItemName()
	end

	self._txtScore.text = tostring(self._itemData:GetFightPower())
	self._txtBaseScore.text = tostring(self._itemData:GetBaseFightPower())

	--bagType为0的时候为装备上
	self._imgLoaded:SetActive(self._bagType == 0)
	
	self:UpdateAttri(equipData,self._itemCfg)

	--按钮处理
	if self._buttonArgs then
		self._containerBottom:SetActive(true)
		self:FiltButtons()
	elseif self._buttonInited then
		self._containerBottom:SetActive(false)
	end
end

function EquipMythicalAnimalTipsView:UpdateViewById(itemId)
	self._itemCfg = ItemDataHelper.GetItem(itemId)
	self._bagType = 0 
	self._base.UpdateViewBaseInfo(self,itemId)
	self:SetItemName(itemId)
	self._imgLoaded:SetActive(false)
	self:UpdateAttri(nil,self._itemCfg)

	--按钮处理
	if self._buttonArgs then
		self._containerBottom:SetActive(true)
		self:FiltButtons()
	elseif self._buttonInited then
		self._containerBottom:SetActive(false)
	end
end

--是否显示装备加成数值
function EquipMythicalAnimalTipsView:CheckShowAddition()
	return false--self._bagType == public_config.PKG_TYPE_LOADED_EQUIP and self._bagPos <= public_config.PKG_LOADED_EQUIP_INDEX_MAX
end

--更新属性
function EquipMythicalAnimalTipsView:UpdateAttri(equipData,cfg)
	local h = 1
	--基础属性
	local itemBaseAttris = EquipDataHelper:GetBaseAttris(cfg.id)
	local itemBaseAttrisLen = #itemBaseAttris
	--LoggerHelper.Error("itemBaseAttrisLen"..itemBaseAttrisLen)
	self._baseAttrisBase:SetActive(true)
	
	for i=1,itemBaseAttrisLen do
		if self._baseAttris[i] == nil then
			local item = self:CreateAttriItem()
			self._baseAttris[i] = item
		end
		self._baseAttris[i]:SetActive(true)
		local attriKey = itemBaseAttris[i].attri
		local baseValue = AttriDataHelper:ConvertAttriValue(attriKey,itemBaseAttris[i].value)
		local valueStr = "  +"..tostring(baseValue)
		--LoggerHelper.Error("itemBaseAttris[i].attri"..itemBaseAttris[i].attri)
		--计算强化加成值
		-- if self:CheckShowAddition() then
		local strengthLevel = 0
		if equipData then
			strengthLevel = equipData:GetMythicalAnimalEquipLevel()
		end
		if strengthLevel > 0  then
			local attriInfo = MythicalAnimalsLevelDataHelper:GetAttriByTypeAndLevel(equipData:GetType(),strengthLevel)--EquipDataHelper.GetStrengthData(strengthLevel)
			local strengthValue = attriInfo[attriKey] or 0
			local colorId = ItemConfig.ItemAttriValue
			local atiAdded = AttriDataHelper:ConvertAttriValue(attriKey,strengthValue)
			local addStr = "("..LanguageDataHelper.CreateContent(214).."+"..atiAdded..")"
			valueStr = valueStr.."  "..StringStyleUtil.GetColorStringWithColorId(addStr,colorId)			
		end
		--end
		self._baseAttris[i]:UpdateItem(AttriDataHelper:GetName(attriKey),valueStr,true)
		h = h+1
	end
	self._baseAttrisBase:SetActive(false)
	for j=h,#self._baseAttris do
		self._baseAttris[j]:SetActive(false)
	end

	--随机属性
	--已经获得的装备
	local itemRandAttrisLen = 0
	local itemRandAttris 
	if equipData then
		itemRandAttris = equipData:GetRandAttris()
	--未获得的装备
	else
		itemRandAttris = EquipDataHelper:GetRecommandRandAttris(cfg.id)
	end
		
	itemRandAttrisLen = #itemRandAttris
	if #itemRandAttris > 0 then
		self:ShowRandAttri(true)
		h = 1
		self._randAttrisBase:SetActive(true)
		for i=1,itemRandAttrisLen do
			if self._randAttris[i] == nil then
				local item = self:CreateRanAttriItem()
				self._randAttris[i] = item
			end
			self._randAttris[i]:SetActive(true)

			local attriKey = itemRandAttris[i].attri
			local attriValue = AttriDataHelper:ConvertAttriValue(attriKey,itemRandAttris[i].value)
			local colorId = ItemConfig.QualityTextMap[AttriDataHelper:GetAttriColor(attriKey)]
			--LoggerHelper.Error("attriKey"..attriKey)
			local str
			if equipData then
				str = AttriDataHelper:GetName(attriKey)
			else
				if itemRandAttris[i].showRecommand then
					str = LanguageDataHelper.CreateContent(304).."  "..AttriDataHelper:GetName(attriKey)
				else
					str = AttriDataHelper:GetName(attriKey)
				end
				--LoggerHelper.Error("attriKey"..attriKey)
			end
			str = str.."  +"..attriValue
			str = StringStyleUtil.GetColorStringWithColorId(str,colorId)
			self._randAttris[i]:UpdateItem(str,"",true)
			h = h+1
		end
		
		for j=h,#self._randAttris do
			self._randAttris[j]:SetActive(false)
		end
		self._randAttrisBase:SetActive(false)
		
	else
		self:ShowRandAttri(false)
	end

	--未获得的装备在这里直接计算
	if equipData == nil then
		local baseAttriPower = BaseUtil.CalculateFightPower(itemBaseAttris,GameWorld.Player().vocation)
		local randAttriPower = BaseUtil.CalculateFightPower(itemRandAttris,GameWorld.Player().vocation)
		local fightPower = baseAttriPower + randAttriPower
		self._txtScore.text = tostring(fightPower)
		self._txtBaseScore.text = tostring(fightPower)
	end

	self:UpdateLayout(itemBaseAttrisLen,itemRandAttrisLen)
end

EquipMythicalAnimalTipsView.ItemHeight = 26
EquipMythicalAnimalTipsView.MaskHeight = 440

--更新布局
function EquipMythicalAnimalTipsView:UpdateLayout(itemBaseAttrisLen,itemRandAttrisLen)
	local curY = 0
	--基础信息
	self:SetPosition(self._containerBaseInfo,curY)
	--基础属性
	curY = curY - 139
	self:SetPosition(self._titleBaseAttri,curY)
	curY = curY - 1
	self:SetPosition(self._containerBaseAttriItemGroup,curY)
	curY = curY - EquipMythicalAnimalTipsView.ItemHeight*itemBaseAttrisLen
	
	--随机属性
	if itemRandAttrisLen > 0 then
		curY = curY - EquipMythicalAnimalTipsView.ItemHeight - 5
		self:SetPosition(self._titleRanAttri,curY)
		curY = curY - 1
		self:SetPosition(self._containerRanAttriItemGroup,curY)
		curY = curY - EquipMythicalAnimalTipsView.ItemHeight * itemRandAttrisLen
	end
	
	--装备描述
	curY = curY - 75
	self:SetPosition(self._txtDescGO,curY)
	
	--设定高度
	local height = -curY
	if height < EquipMythicalAnimalTipsView.MaskHeight then
		local dY = EquipMythicalAnimalTipsView.MaskHeight - height
		self._bgRectTransform.sizeDelta = Vector2(364,604-dY)
		self._frameRectTransform.sizeDelta = Vector2(382,610-dY)
		if self._containerBottom then
			self:SetPosition(self._containerBottom,dY)
		end
		height = EquipMythicalAnimalTipsView.MaskHeight
	else
		self._bgRectTransform.sizeDelta = Vector2(364,604)
		self._frameRectTransform.sizeDelta = Vector2(382,610)
		if self._containerBottom then
			self:SetPosition(self._containerBottom,0)
		end
	end

	self._rectTransform.sizeDelta = Vector2(364,height)
	self._scrollViewAttri:ResetContentPosition()
end

function EquipMythicalAnimalTipsView:SetItemDesc(itemId)
	if self._itemCfg.achieve_chinese and self._itemCfg.achieve_chinese > 0 then
		self._txtDesc.text = LanguageDataHelper.CreateContent(self._itemCfg.achieve_chinese)
	else
		self._txtDesc.text = ""
	end
end