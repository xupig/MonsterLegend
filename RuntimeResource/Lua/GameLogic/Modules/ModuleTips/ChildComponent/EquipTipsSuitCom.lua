-- EquipTipsSuitCom.lua
--装备tip上套装小组件
local EquipTipsSuitCom = Class.EquipTipsSuitCom(ClassTypes.BaseComponent)
EquipTipsSuitCom.interface = GameConfig.ComponentsConfig.Component
EquipTipsSuitCom.classPath = "Modules.ModuleTips.ChildComponent.EquipTipsSuitCom"

require "Modules.ModuleTips.ChildComponent.EquipTipsSuitAttriItem"
local EquipTipsSuitAttriItem = ClassTypes.EquipTipsSuitAttriItem
local AttriDataHelper = GameDataHelper.AttriDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local bagData = PlayerManager.PlayerDataManager.bagData
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemConfig = GameConfig.ItemConfig
local ConvertStyle = GameMain.LuaFacade.ConvertStyle
local EquipDataHelper = GameDataHelper.EquipDataHelper

local suitTypePos1 = {1,2,3,4,5,6}
local suitTypePos2 = {7,8,9,10}

function EquipTipsSuitCom:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self._height = 0
	self._equipNameTexts = {}

	self._txtSuitName = self:GetChildComponent("Text_TitleSuitName",'TextMeshWrapper')
	
	self._txtEquip = self:GetChildComponent("Container_SuitEquipGroup/Text_Equip",'TextMeshWrapper')

	self._equipNameTexts[1] = self._txtEquip

	self._attriItems = {}
	self._containerSuitAttriGroup = self:FindChildGO("Container_SuitAttriGroup")
	self._baseEquipCountAttriItem = self:FindChildGO("Container_SuitAttriGroup/Container_SuitAttriItem")

	self._titleHight = 80
end

--获取身上装备符合套装的数量
function EquipTipsSuitCom:GetSuitMatchNum(itemCfg,suitData,suitLevel,suitTypePos,allEquipIds)
	local curSuitId = itemCfg.suitId[suitLevel]
	local count = 0
	local matchList = {}
	local loaded = {}
	if allEquipIds then
		loaded = allEquipIds
	else
		local t = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
		for pos,v in pairs(t) do
			loaded[pos] = v:GetItemID()
		end
	end
	
	for i=1,#suitTypePos do
		local pos = suitTypePos[i]
		if loaded[pos] then
			local equipId = loaded[pos]
			local suitId = ItemDataHelper.GetSuitId(equipId)
			local equipSuitLevel = suitData[pos] or 0
			if equipSuitLevel == suitLevel and pos == ItemDataHelper.GetType(equipId) and suitId[suitLevel] and suitId[suitLevel] == curSuitId then
				count = count + 1
				matchList[i] = true
			end
		end
	end

	return count,matchList
end

function EquipTipsSuitCom:SetHightFactor(titleHight)
	self._titleHight = titleHight
end

function EquipTipsSuitCom:UpdateCom(itemCfg,suitData,suitLevel,allEquipIds)
	local equipType = itemCfg.type
	local suitTypePos
	--套装装备总件数
	if equipType <= 6 then
		suitTypePos = suitTypePos1
	else
		suitTypePos = suitTypePos2
	end
	local totalEquipNum = #suitTypePos
	--当前符合套装条件装备数目
	local matchNum,matchList = self:GetSuitMatchNum(itemCfg,suitData,suitLevel,suitTypePos,allEquipIds)
	local curSuitId = itemCfg.suitId[suitLevel]
	local suitCfg = EquipDataHelper.GetSuitData(curSuitId)
	local suitName = LanguageDataHelper.CreateContent(suitCfg.name)
	self._suitEqupNum = matchNum
	self._txtSuitName.text = suitName..LanguageDataHelper.CreateContent(168).."("..matchNum.."/"..totalEquipNum..")"
	self._height = self._titleHight

	--清空
	for i=1,#self._equipNameTexts do
		self._equipNameTexts[i].text = ""
	end

	for i=1,totalEquipNum do
		if self._equipNameTexts[i] == nil then
			local go = self:CopyUIGameObject("Container_SuitEquipGroup/Text_Equip","Container_SuitEquipGroup")
			local txt = go:GetComponent("TextMeshWrapper")
			self._equipNameTexts[i] = txt
		end
		local colorId
		local cnType = ItemDataHelper.GetTypeNameByType(public_config.ITEM_ITEMTYPE_EQUIP,suitTypePos[i])
		local equipName = suitName..LanguageDataHelper.CreateContent(cnType)
		if matchList[i] then
			colorId = ItemConfig.ItemNormal
		else
			colorId = ItemConfig.ItemDisabled
		end
		self._equipNameTexts[i].text = ConvertStyle(string.format("$(%d,%s)",colorId,equipName))
	end

	self._height = self._height + math.ceil(totalEquipNum/2) * 26

	self:SetPosition(self._containerSuitAttriGroup,-(self._height))

	self._baseEquipCountAttriItem:SetActive(true)
	local j = 1
	local itemY = 0
	for k,attriData in pairs(suitCfg.attri) do
		if self._attriItems[j] == nil then
			local item = self:CreateAttriItem()
			self._attriItems[j] = item
		end
		local item = self._attriItems[j]
		item:SetActive(true)
		local equipCount = tonumber(k)
		item:UpdateData(equipCount,matchNum >= equipCount,attriData)
		self:SetPosition(item,itemY)
		itemY = itemY - item:GetHeight()
		self._height = self._height + item:GetHeight()
		j = j+1
	end

	for i=j,#self._attriItems do
		self._attriItems[i]:SetActive(false)
	end
	self._baseEquipCountAttriItem:SetActive(false)
end

function EquipTipsSuitCom:SetPosition(com,y)
	com.transform.anchoredPosition = Vector3(com.transform.anchoredPosition.x,y,0)
end

function EquipTipsSuitCom:CreateAttriItem()
	local go = self:CopyUIGameObject("Container_SuitAttriGroup/Container_SuitAttriItem", "Container_SuitAttriGroup")
    local item = UIComponentUtil.AddLuaUIComponent(go, EquipTipsSuitAttriItem)
    return item
end

function EquipTipsSuitCom:GetHeight()
	return self._height
end

