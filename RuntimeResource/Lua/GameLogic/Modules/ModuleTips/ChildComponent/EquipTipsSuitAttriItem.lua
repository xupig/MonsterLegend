-- EquipTipsSuitAttriItem.lua
--装备tip上套装小组件下X件装备效果
local EquipTipsSuitAttriItem = Class.EquipTipsSuitAttriItem(ClassTypes.BaseComponent)
EquipTipsSuitAttriItem.interface = GameConfig.ComponentsConfig.Component
EquipTipsSuitAttriItem.classPath = "Modules.ModuleTips.ChildComponent.EquipTipsSuitAttriItem"

require "UIComponent.Extend.AttriCom"
local AttriCom = ClassTypes.AttriCom
local AttriDataHelper = GameDataHelper.AttriDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemConfig = GameConfig.ItemConfig
local BaseUtil = GameUtil.BaseUtil
local ColorConfig = GameConfig.ColorConfig

function EquipTipsSuitAttriItem:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self._txtEquipNum = self:GetChildComponent("Text_EquipNum",'TextMeshWrapper')

	self._height = 0
	--暂无技能
	--self._txtSuitSkill = self:GetChildComponent("Text_SuitSkill",'TextMeshWrapper')

	self._attriItems = {}
	self._baseAttriItem = self:FindChildGO("Container_AttriGroup/Container_SuitAttri")
	self._baseAttriItem:SetActive(false)
end

function EquipTipsSuitAttriItem:UpdateData(num,isActivated,attriData)
	self._height = 0
	local colorId
	if isActivated then
		colorId = ColorConfig.J
	else
		colorId = ColorConfig.C
	end
	local nameStr = "("..num..LanguageDataHelper.CreateContent(526)..")"
	self._txtEquipNum.text = BaseUtil.GetColorString(nameStr, colorId)
	if attriData then
		local j = 1
		self._baseAttriItem:SetActive(true)
		for i=1,#attriData,2 do
			if self._attriItems[j] == nil then
				local item = self:CreateAttriItem()
				self._attriItems[j] = item
			end
			self._attriItems[j]:SetActive(true)

			local attriKey = attriData[i]
			local attriNameStr  = BaseUtil.GetColorString(AttriDataHelper:GetName(attriKey), colorId)
			local attriValue = AttriDataHelper:ConvertAttriValue(attriKey,attriData[i+1])
			local attriValueStr = BaseUtil.GetColorString("   +"..attriValue, colorId)
			self._height = self._height + 26
			self._attriItems[j]:UpdateItem(attriNameStr,attriValueStr,true)
			j = j+1
		end
		self._baseAttriItem:SetActive(false)
		for i=j,#self._attriItems do
			self._attriItems[i]:SetActive(false)
		end
	end
end

function EquipTipsSuitAttriItem:CreateAttriItem()
	local go = self:CopyUIGameObject("Container_AttriGroup/Container_SuitAttri", "Container_AttriGroup")
    local item = UIComponentUtil.AddLuaUIComponent(go, AttriCom)
    return item
end

function EquipTipsSuitAttriItem:GetHeight()
	return self._height
end