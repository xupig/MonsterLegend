-- EquipTipsGemAttriItem.lua
--装备tip上宝石属性item
local EquipTipsGemAttriItem = Class.EquipTipsGemAttriItem(ClassTypes.BaseComponent)
EquipTipsGemAttriItem.interface = GameConfig.ComponentsConfig.Component
EquipTipsGemAttriItem.classPath = "Modules.ModuleTips.ChildComponent.EquipTipsGemAttriItem"

local AttriDataHelper = GameDataHelper.AttriDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function EquipTipsGemAttriItem:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")

	self._txtAttris = {}
	self._txtAttris[1] = self:GetChildComponent("Text_Attri1",'TextMeshWrapper')
	self._txtAttris[2] = self:GetChildComponent("Text_Attri2",'TextMeshWrapper')


	self._gemIcon = self:FindChildGO("Container_Icon")

	self._txtNoGem = self:FindChildGO("Text_NoGem")

	self._txtGemName = self:GetChildComponent("Text_GemName",'TextMeshWrapper')
end

function EquipTipsGemAttriItem:UpdateItem(gemId)
	if gemId then
		local gemCfg = ItemDataHelper.GetItem(gemId)
		local gemIcon = gemCfg.icon
		self._txtGemName.text = ItemDataHelper.GetItemNameWithColor(gemId)
		self._txtNoGem:SetActive(false)
		self._gemIcon:SetActive(true)
		GameWorld.AddIcon(self._gemIcon,gemIcon)
		for i=1,2 do
			if gemCfg.gem_attribute[i] then
				local attriKey = gemCfg.gem_attribute[i][1]
				local attriValue = gemCfg.gem_attribute[i][2]
				self._txtAttris[i].text = AttriDataHelper:GetName(attriKey)..":  +"..AttriDataHelper:ConvertAttriValue(attriKey,attriValue)
			else
				self._txtAttris[i].text = ""
			end
		end
		self._isEmpty = false
	else
		self._txtGemName.text = ""
		self._txtNoGem:SetActive(true)
		for i=1,2 do
			self._txtAttris[i].text = ""
		end
		self._gemIcon:SetActive(false)
		self._isEmpty = true
	end
end

function EquipTipsGemAttriItem:SetLock(isLock)
	self._isLock = isLock
end

function EquipTipsGemAttriItem:UpdateVipLock(vipLevel)
	if self._txtV5  == nil  then
		self._txtV5 = self:FindChildGO("Text_V5")
		self._vipSlotLevel = GlobalParamsHelper.GetParamValue(469)
	end
	self._txtV5:SetActive(self._vipSlotLevel > vipLevel)
end

function EquipTipsGemAttriItem:GetHeight()
	if self._isLock then
		return 0
	end

	if self._isEmpty then
		return 30
	else
		return 52
	end
end

-- function EquipTipsGemAttriItem:CreateAttriItem()
-- 	local go = self:CopyUIGameObject("Container_GemAttri/Container_GemAttriItem", "Container_GemAttri")
--     local item = UIComponentUtil.AddLuaUIComponent(go, AttriCom)
--     return item
-- end
