-- TipsModule.lua
TipsModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function TipsModule.Init()
    GUIManager.AddPanel(PanelsConfig.ItemTips, "Panel_ItemTips", GUILayer.LayerUICommon, "Modules.ModuleTips.ItemTipsPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.Tips, "Panel_Tips", GUILayer.LayerUICommon, "Modules.ModuleTips.TipsPanel", true, false)
end

return TipsModule