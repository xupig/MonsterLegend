-- ItemTipsPanel.lua

local ItemTipsPanel = Class.ItemTipsPanel(ClassTypes.BasePanel)

require "Modules.ModuleTips.ChildView.ItemTipsViewBase"
require "Modules.ModuleTips.ChildView.CommonItemTipsView"
require "Modules.ModuleTips.ChildView.EquipTipsView"
require "Modules.ModuleTips.ChildView.EquipMythicalAnimalTipsView"
require "Modules.ModuleTips.ChildView.TipsModelView"

local CommonItemTipsView = ClassTypes.CommonItemTipsView
local EquipTipsView = ClassTypes.EquipTipsView
local EquipMythicalAnimalTipsView = ClassTypes.EquipMythicalAnimalTipsView
local TipsModelView = ClassTypes.TipsModelView
local CommonItemData = ClassTypes.CommonItemData
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config

function ItemTipsPanel:Awake()
	self._csBH = self:GetComponent("LuaUIPanel")
	self:InitViews()

	self._btnBg = self:FindChildGO("Button_Bg")
	self._csBH:AddClick(self._btnBg, function() self:OnClickClose() end )

	--临时的itemdata数据
	self._tempItemsDataCache = {}
	self._itemId = nil --当前显示tips的itemId
	self._itemTipsShowModel = function(type,args,dy) self:ShowModel(type,args,dy) end
end

function ItemTipsPanel:OnShow(data)
	if self._modelView then
		self._modelView:SetActive(false)
	end

	self:AddEventListeners()
	if data then
		if data[1] == 1 then
			self:ShowTips(data[2],data[3],data[4],data[5],data[6])
		else
			self:ShowTipsById(data[2],data[3])
		end
	end
end

function ItemTipsPanel:OnClose()
	self._views[self._selectedViewIndex]:OnClose()
	self:RemoveEventListeners()
end

function ItemTipsPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Item_Tips_Show_Model, self._itemTipsShowModel)
end

function ItemTipsPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Item_Tips_Show_Model, self._itemTipsShowModel)
end

function ItemTipsPanel:InitViews()
	self._equipTipsView = self:AddChildLuaUIComponent("Container_Equip", EquipTipsView)
	self._equipTipsView:InitButtons()
	self._equipTipsView:InitCompareComps()
	
	self._commonItemTipsView = self:AddChildLuaUIComponent("Container_CommonItem", CommonItemTipsView)

	self._equipMythicalAnimalTipsView = self:AddChildLuaUIComponent("Container_EquipMythicalAnimal", EquipMythicalAnimalTipsView)
	self._equipMythicalAnimalTipsView:InitButtons()
	self._equipMythicalAnimalTipsView:InitCompareComps()

	self._viewsActive = {}
	self._views = {}
	self._views[1] = self._equipTipsView
	self._views[2] = self._commonItemTipsView
	self._views[3] = self._equipMythicalAnimalTipsView

	self._useView = {}
	self._useView[public_config.ITEM_ITEMTYPE_EQUIP] = 1
	self._useView[public_config.ITEM_ITEMTYPE_ITEM] = 2
	self._useView[public_config.ITEM_ITEMTYPE_GEM] = 2
	self._useView[public_config.ITEM_ITEMTYPE_FASHION] = 2
	self._useView[public_config.ITEM_ITEMTYPE_CARD_PIECE] = 2
	--self._useView[public_config.ITEM_ITEMTYPE_GOD_MONSTER] = 3

	--装备对比
	self._viewsActive[99] = false
	self._compareEquipTipsView = self:AddChildLuaUIComponent("Container_CompareEquip", EquipTipsView)
	--神兽装备对比
	self._viewsActive[100] = false
	self._compareMythicalAnimalEquipTipsView = self:AddChildLuaUIComponent("Container_CompareEquipMythicalAnimal", EquipMythicalAnimalTipsView)
end

function ItemTipsPanel:SetSelectedView(cfgData)
	if cfgData.itemType == public_config.ITEM_ITEMTYPE_GOD_MONSTER then
		--LoggerHelper.Error("cfgData.type ===" .. cfgData.type .. "type==" .. type(cfgData.type))
		if cfgData.type == 6 then
			self._selectedViewIndex = 2
		else
			self._selectedViewIndex = 3
		end
	else
		self._selectedViewIndex = self._useView[cfgData.itemType]
	end
end

--只使用道具id情况显示tips
function ItemTipsPanel:ShowTipsById(itemId,buttonArgs)
	self._itemId = itemId
	local cfgData = ItemDataHelper.GetItem(itemId)
	self:SetSelectedView(cfgData)
	for i=1,#self._views do
		if i == self._selectedViewIndex then
			self:SetViewActive(i,true,self._views[i])
			self._views[i]:SetButtonArgs(buttonArgs)
			self._views[i]:UpdateViewById(itemId)
			if i ==1 then
				self:SetXPosition(self._equipTipsView,454)
			end
			if i ==3 then
				self:SetXPosition(self._equipMythicalAnimalTipsView,454)
			end
		else
			self:SetViewActive(i,false,self._views[i])
		end
	end
	self:SetViewActive(99,false,self._compareEquipTipsView)
	self:SetViewActive(100,false,self._compareMythicalAnimalEquipTipsView)
end

--一般情况显示tips
--itemData 道具数据
--buttonArgs 显示按钮参数
--showCompare 显示装备对比
--showSuitPreview 显示套装前缀预览
--otherPlayerArgs 别人的道具特殊的参数（Avartar上的）
function ItemTipsPanel:ShowTips(itemData,buttonArgs,showCompare,showSuitPreview,otherPlayerArgs)
	--LoggerHelper.Log(otherPlayerArgs)
	--LoggerHelper.Error("ItemTipsPanel:ShowTips")
	self._itemId = itemData.cfg_id
	local itemType = itemData.cfgData.itemType
	self:SetSelectedView(itemData.cfgData)
	--self._selectedViewIndex = self._useView[itemType] 
	
	for i=1,#self._views do
		if i == self._selectedViewIndex then
			self:SetViewActive(i,true,self._views[i])
			self._views[i]:SetButtonArgs(buttonArgs)
			--装备tips位置特殊处理
			if i==1 then
				local type = itemData.cfgData.type
				local equipdata = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetEquipListBySubType(type)
				local loadedEquipData = equipdata[1]
				self._equipTipsView:SetShowSuitPreview(showSuitPreview)
				self._equipTipsView:SetOtherPlayerArgs(otherPlayerArgs)
				--装备对比显示
				if showCompare and loadedEquipData then
					local scoreLoaded = loadedEquipData:GetBaseFightPower()
					local scoreCur = itemData:GetBaseFightPower()
					local scoreDiff = scoreCur - scoreLoaded
					self._equipTipsView:UpdateCompare(scoreDiff)
					self:SetViewActive(99,true,self._compareEquipTipsView)
					self._compareEquipTipsView:UpdateView(loadedEquipData)
					self:SetXPosition(self._equipTipsView,652)
				else
					self._equipTipsView:HideCompare()
					self:SetViewActive(99,false,self._compareEquipTipsView)
					self:SetXPosition(self._equipTipsView,454)
				end
			else
				self:SetViewActive(99,false,self._compareEquipTipsView)
			end
		
			if i==3 then
				--神兽装备对比显示,
				--
				if showCompare then
					local scoreLoaded = showCompare:GetBaseFightPower()
					local scoreCur = itemData:GetBaseFightPower()
					local scoreDiff = scoreCur - scoreLoaded
					self._equipMythicalAnimalTipsView:UpdateCompare(scoreDiff)
					self:SetViewActive(100,true,self._compareMythicalAnimalEquipTipsView)
					self._compareMythicalAnimalEquipTipsView:UpdateView(showCompare)
					self:SetXPosition(self._equipMythicalAnimalTipsView,652)
				else
					self._equipMythicalAnimalTipsView:HideCompare()
					self:SetViewActive(100,false,self._compareMythicalAnimalEquipTipsView)
					self:SetXPosition(self._equipMythicalAnimalTipsView,454)
				end
			else
				self:SetViewActive(100,false,self._compareMythicalAnimalEquipTipsView)
			end

			self._views[i]:UpdateView(itemData)
			self._views[i]:OnShow()
		else
			self:SetViewActive(i,false,self._views[i])
			self._views[i]:OnClose()
		end
	end
end

function ItemTipsPanel:SetViewActive(index,isActive,view)
	if self._viewsActive[index] ~= isActive then
		view:SetActive(isActive)
		self._viewsActive[index] = isActive
	end
end

function ItemTipsPanel:SetXPosition(com,x)
	com.transform.anchoredPosition = Vector3(x,com.transform.anchoredPosition.y,0)
end

function ItemTipsPanel:OnClickClose()
	GameManager.GUIManager.ClosePanel(PanelsConfig.ItemTips)
	EventDispatcher:TriggerEvent(GameEvents.On_Close_Item_Tips, self._itemId)
	if self._previewItem then
		--带模型预览的tips 关闭恢复
		if self._modelView then
			self._modelView:SetActive(false)
		end
		GameWorld.ShowPlayer():SyncShowPlayerFacade()
		GameWorld.ShowPlayer():ShowModel(3)
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_BAG_RETSET_PLAYER_MODEL)
		self._previewItem = nil
	end
end

function ItemTipsPanel:ShowModel(type,args,dy)
	if self._modelView == nil then
		self._modelView = self:AddChildLuaUIComponent('Container_Model', TipsModelView)
	end
	self._modelView:SetActive(true)
	
	if type == 1 then
		self._previewItem = true
		self._modelView:ShowPlayerModel()
	elseif type == 2 then
		self._modelView:ShowTransformModel(args)
	elseif type == 3 then
		self._modelView:ShowOtherModel(args)
	end
	self._modelView:UpdateLayout(dy)
end

