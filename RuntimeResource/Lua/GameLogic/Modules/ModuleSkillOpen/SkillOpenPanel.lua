local SkillOpenPanel = Class.SkillOpenPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local SkillManager = PlayerManager.SkillManager
local skillData = PlayerManager.PlayerDataManager.skillData
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local SkillDataHelper = GameDataHelper.SkillDataHelper
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

local Min_Show_Slot = 2
local Max_Show_Slot = 9
local Middle_Show_Slot = 5
local Waitting_Time = 5000
local Move_Time = 1

function SkillOpenPanel:Awake()
    self._mode = 1 --1:直接消失 2:飞行后消失
    self.slot = 1
    self._queue = {}
    self._isMoving = false
	self:InitView()
end

--override
function SkillOpenPanel:OnDestroy()

end

--data {[id],[slot]} slot为0不用移动效果 id:技能id
function SkillOpenPanel:OnShow(data)
    self:AddEventListeners()
    self:CheckShow(data)
end

function SkillOpenPanel:SetData(data)
    self:CheckShow(data)
end

function SkillOpenPanel:OnClose()
    self:RemoveEventListeners()
end

function SkillOpenPanel:AddEventListeners()

end

function SkillOpenPanel:RemoveEventListeners()

end

function SkillOpenPanel:InitView()
    self._closeButton = self:FindChildGO("Container_Skill/Button_BG")
    self._csBH:AddClick(self._closeButton,function () self:OnCloseBtnClick() end)

    self._infoGo = self:FindChildGO("Container_Skill/Container_Info")
    self._skillImage = self:FindChildGO("Container_Skill/Image_Skill")
    self._iconContainer = self:FindChildGO("Container_Skill/Image_Skill/Container_Icon")
    self._nameText = self:GetChildComponent("Container_Skill/Container_Info/Text_Name", "TextMeshWrapper")
    self._descText = self:GetChildComponent("Container_Skill/Container_Info/Text_Desc", "TextMeshWrapper")

    self._pos = self._skillImage.transform.position
    self._tweenPosSkill = self:AddChildComponent('Container_Skill/Image_Skill', XGameObjectTweenPosition)
    self._fx = UIParticle.AddParticle(self.gameObject,"Container_Skill/Image_Skill/Container_Fx/fx_ui_30038")
end

function SkillOpenPanel:CheckShow(data)
    if data == nil then
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.SKILL_OPEN_START)
    if data.slot >= Min_Show_Slot and data.slot <= Max_Show_Slot then
        self._mode = 2
    else
        self._mode = 1
    end
    self.slot = data.slot
    self:ShowView(data)
    self:ClearTimer()
    self._timer = TimerHeap:AddTimer(Waitting_Time, Waitting_Time, 0, function() self:OnCloseBtnClick() end)
end

function SkillOpenPanel:OnCloseBtnClick()
    self:ClearTimer()
    if self._mode == 1 then
        self:Reset()
    elseif self._mode == 2 then
        if self._isMoving then
            return
        end
        self._infoGo:SetActive(false)
        self._isMoving = true
        local moveTo = SkillManager:GetSkillButtonPosInfo(self.slot)

        self._tweenPosSkill:OnceOutQuadWordPos(moveTo, Move_Time, function() self:Reset() end)
    end
    
end

function SkillOpenPanel:ClearTimer()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function SkillOpenPanel:Reset()
    if self._mode == 2 then
        EventDispatcher:TriggerEvent(GameEvents.SET_SKILL_ICON, self.slot, self._icon)
    end
    self._skillImage.transform.position = self._pos
    self._isMoving = false
    self._infoGo:SetActive(true)
    GUIManager.ClosePanel(PanelsConfig.SkillOpen)
    SkillManager:CheckShowSkillOpen()
    EventDispatcher:TriggerEvent(GameEvents.SKILL_OPEN_END)
end

function SkillOpenPanel:ShowView(data)
    local skillId = data.id
    local icon = SkillDataHelper.GetSkillIcon(skillId)
    self._icon = icon
    GameWorld.AddIcon(self._iconContainer, icon)
    self._nameText.text = SkillDataHelper.GetName(skillId)
    self._descText.text = SkillDataHelper.GetDesc(skillId)
    if self._mode == 2 then
        if self.slot >= Min_Show_Slot and self.slot <= Middle_Show_Slot then
            EventDispatcher:TriggerEvent(GameEvents.SkillControlPanel_SetSkillPage, 1)
        else
            EventDispatcher:TriggerEvent(GameEvents.SkillControlPanel_SetSkillPage, 2)
        end
        EventDispatcher:TriggerEvent(GameEvents.SET_SKILL_ICON, self.slot, 0)
    end
end

function SkillOpenPanel:WinBGType()
    return PanelWinBGType.NoBG
end
