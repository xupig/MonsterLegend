-- SkillOpenModule.lua
SkillOpenModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function SkillOpenModule.Init()
	GUIManager.AddPanel(PanelsConfig.SkillOpen, "Panel_SkillOpen", GUILayer.LayerUIFloat, "Modules.ModuleSkillOpen.SkillOpenPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return SkillOpenModule