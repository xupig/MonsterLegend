local ClassTypes = ClassTypes
local LuaFacade = GameMain.LuaFacade
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local Application = UnityEngine.Application

function CSCALL_OnLevelWasLoaded(level)
    EventDispatcher:TriggerEvent(GameEvents.UNGINE_ONLEVELWASLOADED, level)
end

function CSCALL_OnApplicationQuit()
    EventDispatcher:TriggerEvent(GameEvents.UNGINE_ONAPPLICATIONQUIT)
end

function CSCALL_OnApplicationPause()
    EventDispatcher:TriggerEvent(GameEvents.UNGINE_ONAPPLICATIONPAUSE)
end

function CSCALL_OnScreenResize(screenScale)
    EventDispatcher:TriggerEvent(GameEvents.UNGINE_ONSCREENRESIZE, screenScale)
end

function CSCALL_OnTouchScreen(x, y, z)
    EventDispatcher:TriggerEvent(GameEvents.ENGINE_ONTOUCHSCREEN, x, y, z)
end

function CSCALL_OnTriggerSceneWaitingArea(sceneName)
    EventDispatcher:TriggerEvent(GameEvents.ON_TRIGGER_SCENE_WAITING_AREA, sceneName)
end

function CSCALL_OnActionAllPackageFinish(isSuccess)
    EventDispatcher:TriggerEvent(GameEvents.OnActionAllPackageFinish, isSuccess)
end

function CSCALL_CREATE_ENTITY(entitytype, csEntity)
    require("Entities." .. entitytype)
    local entity = ClassTypes[entitytype](csEntity)
    return entity
end

function CSCALL_CREATE_LUA_BEHAVIOUR(type)
    require(type)
    local index, len, className = string.find(type, "%.(%w+)$")
    local obj = ClassTypes[className]();
    return obj
end

function CSCALL_PLAYER_SINGING(startValue, endValue, duration)
    if duration > 0 then
        -- GameManager.GUIManager.ShowPanel(PanelsConfig.Singing)
        EventDispatcher:TriggerEvent(GameEvents.CHANGE_SINGING_VALUE, startValue, endValue, duration)
    else
        -- GameManager.GUIManager.ClosePanel(PanelsConfig.Singing)
        end
end

function CSCALL_CREATE_DAMAGE(position, value, attackType, isPlayer)
    DamageManager:CreateDamageNumber(position, value, attackType, isPlayer)
end

function CSCALL_CHANGE_SKILL_CD(pos, cd, totalCD, roundCD)
    EventDispatcher:TriggerEvent(GameEvents.SET_SKILL_CD, pos, cd, totalCD, roundCD)
end

function CSCALL_CHANGE_SKILL_ICON(pos, iconId)
    EventDispatcher:TriggerEvent(GameEvents.SET_SKILL_ICON, pos, iconId)
end

function CSCALL_CREATE_TABLE(a, b)
    local rst = {}
    return rst
end
--触发UI控制isOpen为关闭or打开，uiId为定义好的对应处理逻辑id
function CSCALL_CONTROL_UI(isOpen, uiId)
    --关闭技能摇杆
    if uiId == 1 then
        EventDispatcher:TriggerEvent(GameEvents.SkillControlPanel_ResetState, isOpen)
    end
    --技能按钮控制
    if uiId == 2 then
        EventDispatcher:TriggerEvent(GameEvents.ON_CSCALL_CONTROL_UI, isOpen)
    end
    --打开开始技能预览按钮
    if uiId == 99 then
        EventDispatcher:TriggerEvent(GameEvents.SkillUpgradeView_ShowPlayButton, isOpen)
    end
    --移动摇杆
    if uiId == 100 then
        EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_STICK, not isOpen)
    end
end
--BOSS状态栏控制
function CSCALL_CONTROL_BOSSSTATE(isOpen, eid)
    eid = eid or 0
    if isOpen then
        GameManager.GUIManager.ShowPanel(PanelsConfig.BossState)
        EventDispatcher:TriggerEvent(GameEvents.BossStatePanel_SetEntity, eid)
    else
        GameManager.GUIManager.ClosePanel(PanelsConfig.BossState)
    end
end

--传送吟唱打断
function CSCALL_BREAK_TELEPORT_SING()
    EventDispatcher:TriggerEvent(GameEvents.ON_BREAK_TELEPORT)
end

--采集吟唱打断
function CSCALL_BREAK_COLLECT_SING()
    EventDispatcher:TriggerEvent(GameEvents.ON_BREAK_COLLECT)
end

function CSCALL_ON_VIDEO_END(id)
    GameManager.DramaManager:Trigger(DramaTriggerType.ON_VIDEO_END, tostring(id))
--LoggerHelper.Log("CSCALL_ON_VIDEO_END:" .. id )
end

function CSCALL_ON_GUIDE_TWEEN_POSITION_END(goName)
    EventDispatcher:TriggerEvent(GameEvents.ON_GUIDE_TWEEN_POSITION_END, goName)
end

function CSCALL_ON_DISCONNECT()
    if GameWorld.Player() == nil then return end
    local data = {}
    -- LoggerHelper.Log("CSCALL_ON_DISCONNECT")
    local value = {["confirmCB"] = function()
        LuaFacade.TryReConnect()
        GameWorld.aiCanThink = true
        GUIManager.ClosePanel(PanelsConfig.SystemTips)
    end,
    ["cancelCB"] = function()
        -- GameManager.GameStateManager.ReturnLogin() -- 屏蔽断网返回登录，保证断网后客户端需要重启，后续完善服务器列表刷新规则、
        Application.Quit()
        GameWorld.aiCanThink = true
        GUIManager.ClosePanel(PanelsConfig.SystemTips)
    end,
    ["autoConfirm"] = true,
    ["text"] = "服务器断开连接，请重试。\n(选择“取消”退出游戏)"}
    GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
end

function CSCALL_ON_RECONNECT_REJECTED()
    if GameWorld.isManualReturnLogin then
        GameWorld.isManualReturnLogin = false
        return
    end
    
    local value = {["confirmCB"] = function()
        Application.Quit()
        -- GameManager.GameStateManager.ReturnLogin()
        GUIManager.ClosePanel(PanelsConfig.SystemTips)
    end,
    ["cancelCB"] = function()
        Application.Quit()
        -- GameManager.GameStateManager.ReturnLogin()
        GUIManager.ClosePanel(PanelsConfig.SystemTips)
    end,
    ["text"] = "服务器断开连接，请检查网络后重进游戏。"}
    GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
end

function CSCALL_ON_RECONNECT_SUCCEEDED()
    --重连成功后提示
    if GameWorld.Player() then
        if GameManager.GameSceneManager:InServerIns() then
            GameWorld.Player().server.pickle_aoi_req()
        end
    end
    GameManager.GUIManager.ShowText(1, "断线重连成功！")
end

function CSCALL_ON_DEF_LINK_ERROR()

end

function CSCALL_ON_DEF_MD5_ERROR()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_1)
    local text = "客户端版本过旧，请重启客户端执行更新。"
    if GameWorld.isEditor then
        text = "与服务器版本不匹配，更新试试吧！"
    end

    GUIManager.ShowMessageBox(2, text,
        function()GUIManager.ClosePanel(PanelsConfig.MessageBox) end,
        function()GUIManager.ClosePanel(PanelsConfig.MessageBox) end)
    EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, false)
end

function CSCALL_REFRESH_LAST_CAST_SPELL(spell)
    if GameWorld.Player() ~= nil then
        GameWorld.Player():RefreshLastSpellInfo(spell)
    end
end

function CSCALL_RIDE_HIDE()
    GameWorld.Player():SetRide(0)--效果先行
    PlayerManager.PartnerManager:RideHideReq()
end

function CSCALL_AUTO_FIGHT()
    GameWorld.Player():StartAutoFight()
end

--client_show为1的buffer增减时触发
function CSCALL_ON_BUFFER_CHANGE()
    EventDispatcher:TriggerEvent(GameEvents.ON_BUFFER_CHANGE)
end

function CSCALL_ON_ENEMY_AVATAR_ID_CHANGE(index, eid)
    EventDispatcher:TriggerEvent(GameEvents.ON_ENEMY_AVATAR_ID_CHANGE, index, eid)
end

--state 0:未攻击 1:攻击
function CSCALL_ON_ENEMY_AVATAR_STATE_CHANGE(index, state)
    EventDispatcher:TriggerEvent(GameEvents.ON_ENEMY_AVATAR_STATE_CHANGE, index, state)
end