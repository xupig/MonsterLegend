--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.life_skill_make_pb')

LIFE_SKILL_MAKE = protobuf.Descriptor();
LIFE_SKILL_MAKE___ID_FIELD = protobuf.FieldDescriptor();
LIFE_SKILL_MAKE___SKILL_ID_FIELD = protobuf.FieldDescriptor();
LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD = protobuf.FieldDescriptor();
LIFE_SKILL_MAKE___COST_ITEM_FIELD = protobuf.FieldDescriptor();
LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD = protobuf.FieldDescriptor();
LIFE_SKILL_MAKE___COST_SILVER_FIELD = protobuf.FieldDescriptor();
LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD = protobuf.FieldDescriptor();
LIFE_SKILL_MAKE___ITEM_ID_FIELD = protobuf.FieldDescriptor();

LIFE_SKILL_MAKE___ID_FIELD.name = "__id"
LIFE_SKILL_MAKE___ID_FIELD.full_name = ".life_skill_make.__id"
LIFE_SKILL_MAKE___ID_FIELD.number = 1
LIFE_SKILL_MAKE___ID_FIELD.index = 0
LIFE_SKILL_MAKE___ID_FIELD.label = 2
LIFE_SKILL_MAKE___ID_FIELD.has_default_value = false
LIFE_SKILL_MAKE___ID_FIELD.default_value = 0
LIFE_SKILL_MAKE___ID_FIELD.type = 5
LIFE_SKILL_MAKE___ID_FIELD.cpp_type = 1

LIFE_SKILL_MAKE___SKILL_ID_FIELD.name = "__skill_id"
LIFE_SKILL_MAKE___SKILL_ID_FIELD.full_name = ".life_skill_make.__skill_id"
LIFE_SKILL_MAKE___SKILL_ID_FIELD.number = 2
LIFE_SKILL_MAKE___SKILL_ID_FIELD.index = 1
LIFE_SKILL_MAKE___SKILL_ID_FIELD.label = 2
LIFE_SKILL_MAKE___SKILL_ID_FIELD.has_default_value = false
LIFE_SKILL_MAKE___SKILL_ID_FIELD.default_value = 0
LIFE_SKILL_MAKE___SKILL_ID_FIELD.type = 5
LIFE_SKILL_MAKE___SKILL_ID_FIELD.cpp_type = 1

LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD.name = "__skill_level"
LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD.full_name = ".life_skill_make.__skill_level"
LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD.number = 3
LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD.index = 2
LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD.label = 2
LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD.has_default_value = false
LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD.default_value = 0
LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD.type = 5
LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD.cpp_type = 1

LIFE_SKILL_MAKE___COST_ITEM_FIELD.name = "__cost_item"
LIFE_SKILL_MAKE___COST_ITEM_FIELD.full_name = ".life_skill_make.__cost_item"
LIFE_SKILL_MAKE___COST_ITEM_FIELD.number = 4
LIFE_SKILL_MAKE___COST_ITEM_FIELD.index = 3
LIFE_SKILL_MAKE___COST_ITEM_FIELD.label = 2
LIFE_SKILL_MAKE___COST_ITEM_FIELD.has_default_value = false
LIFE_SKILL_MAKE___COST_ITEM_FIELD.default_value = ""
LIFE_SKILL_MAKE___COST_ITEM_FIELD.type = 9
LIFE_SKILL_MAKE___COST_ITEM_FIELD.cpp_type = 9

LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD.name = "__cost_activity"
LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD.full_name = ".life_skill_make.__cost_activity"
LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD.number = 5
LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD.index = 4
LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD.label = 2
LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD.has_default_value = false
LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD.default_value = 0
LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD.type = 5
LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD.cpp_type = 1

LIFE_SKILL_MAKE___COST_SILVER_FIELD.name = "__cost_silver"
LIFE_SKILL_MAKE___COST_SILVER_FIELD.full_name = ".life_skill_make.__cost_silver"
LIFE_SKILL_MAKE___COST_SILVER_FIELD.number = 6
LIFE_SKILL_MAKE___COST_SILVER_FIELD.index = 5
LIFE_SKILL_MAKE___COST_SILVER_FIELD.label = 2
LIFE_SKILL_MAKE___COST_SILVER_FIELD.has_default_value = false
LIFE_SKILL_MAKE___COST_SILVER_FIELD.default_value = 0
LIFE_SKILL_MAKE___COST_SILVER_FIELD.type = 5
LIFE_SKILL_MAKE___COST_SILVER_FIELD.cpp_type = 1

LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD.name = "__life_exp_once_done"
LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD.full_name = ".life_skill_make.__life_exp_once_done"
LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD.number = 7
LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD.index = 6
LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD.label = 2
LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD.has_default_value = false
LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD.default_value = 0
LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD.type = 5
LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD.cpp_type = 1

LIFE_SKILL_MAKE___ITEM_ID_FIELD.name = "__item_id"
LIFE_SKILL_MAKE___ITEM_ID_FIELD.full_name = ".life_skill_make.__item_id"
LIFE_SKILL_MAKE___ITEM_ID_FIELD.number = 12
LIFE_SKILL_MAKE___ITEM_ID_FIELD.index = 7
LIFE_SKILL_MAKE___ITEM_ID_FIELD.label = 2
LIFE_SKILL_MAKE___ITEM_ID_FIELD.has_default_value = false
LIFE_SKILL_MAKE___ITEM_ID_FIELD.default_value = 0
LIFE_SKILL_MAKE___ITEM_ID_FIELD.type = 5
LIFE_SKILL_MAKE___ITEM_ID_FIELD.cpp_type = 1

LIFE_SKILL_MAKE.name = "life_skill_make"
LIFE_SKILL_MAKE.full_name = ".life_skill_make"
LIFE_SKILL_MAKE.nested_types = {}
LIFE_SKILL_MAKE.enum_types = {}
LIFE_SKILL_MAKE.fields = {LIFE_SKILL_MAKE___ID_FIELD, LIFE_SKILL_MAKE___SKILL_ID_FIELD, LIFE_SKILL_MAKE___SKILL_LEVEL_FIELD, LIFE_SKILL_MAKE___COST_ITEM_FIELD, LIFE_SKILL_MAKE___COST_ACTIVITY_FIELD, LIFE_SKILL_MAKE___COST_SILVER_FIELD, LIFE_SKILL_MAKE___LIFE_EXP_ONCE_DONE_FIELD, LIFE_SKILL_MAKE___ITEM_ID_FIELD}
LIFE_SKILL_MAKE.is_extendable = false
LIFE_SKILL_MAKE.extensions = {}

life_skill_make = protobuf.Message(LIFE_SKILL_MAKE)

