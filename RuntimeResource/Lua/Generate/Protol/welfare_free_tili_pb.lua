--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.welfare_free_tili_pb')

WELFARE_FREE_TILI = protobuf.Descriptor();
WELFARE_FREE_TILI___ID_FIELD = protobuf.FieldDescriptor();
WELFARE_FREE_TILI___TIME_FIELD = protobuf.FieldDescriptor();
WELFARE_FREE_TILI___TILI_FIELD = protobuf.FieldDescriptor();

WELFARE_FREE_TILI___ID_FIELD.name = "__id"
WELFARE_FREE_TILI___ID_FIELD.full_name = ".welfare_free_tili.__id"
WELFARE_FREE_TILI___ID_FIELD.number = 1
WELFARE_FREE_TILI___ID_FIELD.index = 0
WELFARE_FREE_TILI___ID_FIELD.label = 2
WELFARE_FREE_TILI___ID_FIELD.has_default_value = false
WELFARE_FREE_TILI___ID_FIELD.default_value = 0
WELFARE_FREE_TILI___ID_FIELD.type = 5
WELFARE_FREE_TILI___ID_FIELD.cpp_type = 1

WELFARE_FREE_TILI___TIME_FIELD.name = "__time"
WELFARE_FREE_TILI___TIME_FIELD.full_name = ".welfare_free_tili.__time"
WELFARE_FREE_TILI___TIME_FIELD.number = 2
WELFARE_FREE_TILI___TIME_FIELD.index = 1
WELFARE_FREE_TILI___TIME_FIELD.label = 2
WELFARE_FREE_TILI___TIME_FIELD.has_default_value = false
WELFARE_FREE_TILI___TIME_FIELD.default_value = ""
WELFARE_FREE_TILI___TIME_FIELD.type = 9
WELFARE_FREE_TILI___TIME_FIELD.cpp_type = 9

WELFARE_FREE_TILI___TILI_FIELD.name = "__tili"
WELFARE_FREE_TILI___TILI_FIELD.full_name = ".welfare_free_tili.__tili"
WELFARE_FREE_TILI___TILI_FIELD.number = 3
WELFARE_FREE_TILI___TILI_FIELD.index = 2
WELFARE_FREE_TILI___TILI_FIELD.label = 2
WELFARE_FREE_TILI___TILI_FIELD.has_default_value = false
WELFARE_FREE_TILI___TILI_FIELD.default_value = 0
WELFARE_FREE_TILI___TILI_FIELD.type = 5
WELFARE_FREE_TILI___TILI_FIELD.cpp_type = 1

WELFARE_FREE_TILI.name = "welfare_free_tili"
WELFARE_FREE_TILI.full_name = ".welfare_free_tili"
WELFARE_FREE_TILI.nested_types = {}
WELFARE_FREE_TILI.enum_types = {}
WELFARE_FREE_TILI.fields = {WELFARE_FREE_TILI___ID_FIELD, WELFARE_FREE_TILI___TIME_FIELD, WELFARE_FREE_TILI___TILI_FIELD}
WELFARE_FREE_TILI.is_extendable = false
WELFARE_FREE_TILI.extensions = {}

welfare_free_tili = protobuf.Message(WELFARE_FREE_TILI)

