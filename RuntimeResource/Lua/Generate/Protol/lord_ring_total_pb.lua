--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.lord_ring_total_pb')

LORD_RING_TOTAL = protobuf.Descriptor();
LORD_RING_TOTAL___ID_FIELD = protobuf.FieldDescriptor();
LORD_RING_TOTAL___NAME_FIELD = protobuf.FieldDescriptor();
LORD_RING_TOTAL___ICON_FIELD = protobuf.FieldDescriptor();
LORD_RING_TOTAL___DES_FIELD = protobuf.FieldDescriptor();
LORD_RING_TOTAL___REWARD_FIELD = protobuf.FieldDescriptor();
LORD_RING_TOTAL___TYPE_FIELD = protobuf.FieldDescriptor();

LORD_RING_TOTAL___ID_FIELD.name = "__id"
LORD_RING_TOTAL___ID_FIELD.full_name = ".lord_ring_total.__id"
LORD_RING_TOTAL___ID_FIELD.number = 1
LORD_RING_TOTAL___ID_FIELD.index = 0
LORD_RING_TOTAL___ID_FIELD.label = 2
LORD_RING_TOTAL___ID_FIELD.has_default_value = false
LORD_RING_TOTAL___ID_FIELD.default_value = 0
LORD_RING_TOTAL___ID_FIELD.type = 5
LORD_RING_TOTAL___ID_FIELD.cpp_type = 1

LORD_RING_TOTAL___NAME_FIELD.name = "__name"
LORD_RING_TOTAL___NAME_FIELD.full_name = ".lord_ring_total.__name"
LORD_RING_TOTAL___NAME_FIELD.number = 2
LORD_RING_TOTAL___NAME_FIELD.index = 1
LORD_RING_TOTAL___NAME_FIELD.label = 2
LORD_RING_TOTAL___NAME_FIELD.has_default_value = false
LORD_RING_TOTAL___NAME_FIELD.default_value = 0
LORD_RING_TOTAL___NAME_FIELD.type = 5
LORD_RING_TOTAL___NAME_FIELD.cpp_type = 1

LORD_RING_TOTAL___ICON_FIELD.name = "__icon"
LORD_RING_TOTAL___ICON_FIELD.full_name = ".lord_ring_total.__icon"
LORD_RING_TOTAL___ICON_FIELD.number = 3
LORD_RING_TOTAL___ICON_FIELD.index = 2
LORD_RING_TOTAL___ICON_FIELD.label = 2
LORD_RING_TOTAL___ICON_FIELD.has_default_value = false
LORD_RING_TOTAL___ICON_FIELD.default_value = 0
LORD_RING_TOTAL___ICON_FIELD.type = 5
LORD_RING_TOTAL___ICON_FIELD.cpp_type = 1

LORD_RING_TOTAL___DES_FIELD.name = "__des"
LORD_RING_TOTAL___DES_FIELD.full_name = ".lord_ring_total.__des"
LORD_RING_TOTAL___DES_FIELD.number = 4
LORD_RING_TOTAL___DES_FIELD.index = 3
LORD_RING_TOTAL___DES_FIELD.label = 2
LORD_RING_TOTAL___DES_FIELD.has_default_value = false
LORD_RING_TOTAL___DES_FIELD.default_value = 0
LORD_RING_TOTAL___DES_FIELD.type = 5
LORD_RING_TOTAL___DES_FIELD.cpp_type = 1

LORD_RING_TOTAL___REWARD_FIELD.name = "__reward"
LORD_RING_TOTAL___REWARD_FIELD.full_name = ".lord_ring_total.__reward"
LORD_RING_TOTAL___REWARD_FIELD.number = 5
LORD_RING_TOTAL___REWARD_FIELD.index = 4
LORD_RING_TOTAL___REWARD_FIELD.label = 2
LORD_RING_TOTAL___REWARD_FIELD.has_default_value = false
LORD_RING_TOTAL___REWARD_FIELD.default_value = ""
LORD_RING_TOTAL___REWARD_FIELD.type = 9
LORD_RING_TOTAL___REWARD_FIELD.cpp_type = 9

LORD_RING_TOTAL___TYPE_FIELD.name = "__type"
LORD_RING_TOTAL___TYPE_FIELD.full_name = ".lord_ring_total.__type"
LORD_RING_TOTAL___TYPE_FIELD.number = 6
LORD_RING_TOTAL___TYPE_FIELD.index = 5
LORD_RING_TOTAL___TYPE_FIELD.label = 2
LORD_RING_TOTAL___TYPE_FIELD.has_default_value = false
LORD_RING_TOTAL___TYPE_FIELD.default_value = 0
LORD_RING_TOTAL___TYPE_FIELD.type = 5
LORD_RING_TOTAL___TYPE_FIELD.cpp_type = 1

LORD_RING_TOTAL.name = "lord_ring_total"
LORD_RING_TOTAL.full_name = ".lord_ring_total"
LORD_RING_TOTAL.nested_types = {}
LORD_RING_TOTAL.enum_types = {}
LORD_RING_TOTAL.fields = {LORD_RING_TOTAL___ID_FIELD, LORD_RING_TOTAL___NAME_FIELD, LORD_RING_TOTAL___ICON_FIELD, LORD_RING_TOTAL___DES_FIELD, LORD_RING_TOTAL___REWARD_FIELD, LORD_RING_TOTAL___TYPE_FIELD}
LORD_RING_TOTAL.is_extendable = false
LORD_RING_TOTAL.extensions = {}

lord_ring_total = protobuf.Message(LORD_RING_TOTAL)

