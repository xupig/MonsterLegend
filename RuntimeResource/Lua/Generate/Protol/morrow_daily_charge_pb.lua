--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.morrow_daily_charge_pb')

MORROW_DAILY_CHARGE = protobuf.Descriptor();
MORROW_DAILY_CHARGE___ID_FIELD = protobuf.FieldDescriptor();
MORROW_DAILY_CHARGE___COST_FIELD = protobuf.FieldDescriptor();
MORROW_DAILY_CHARGE___DAY_FIELD = protobuf.FieldDescriptor();
MORROW_DAILY_CHARGE___REWARD_FIELD = protobuf.FieldDescriptor();

MORROW_DAILY_CHARGE___ID_FIELD.name = "__id"
MORROW_DAILY_CHARGE___ID_FIELD.full_name = ".morrow_daily_charge.__id"
MORROW_DAILY_CHARGE___ID_FIELD.number = 1
MORROW_DAILY_CHARGE___ID_FIELD.index = 0
MORROW_DAILY_CHARGE___ID_FIELD.label = 2
MORROW_DAILY_CHARGE___ID_FIELD.has_default_value = false
MORROW_DAILY_CHARGE___ID_FIELD.default_value = 0
MORROW_DAILY_CHARGE___ID_FIELD.type = 5
MORROW_DAILY_CHARGE___ID_FIELD.cpp_type = 1

MORROW_DAILY_CHARGE___COST_FIELD.name = "__cost"
MORROW_DAILY_CHARGE___COST_FIELD.full_name = ".morrow_daily_charge.__cost"
MORROW_DAILY_CHARGE___COST_FIELD.number = 2
MORROW_DAILY_CHARGE___COST_FIELD.index = 1
MORROW_DAILY_CHARGE___COST_FIELD.label = 2
MORROW_DAILY_CHARGE___COST_FIELD.has_default_value = false
MORROW_DAILY_CHARGE___COST_FIELD.default_value = 0
MORROW_DAILY_CHARGE___COST_FIELD.type = 5
MORROW_DAILY_CHARGE___COST_FIELD.cpp_type = 1

MORROW_DAILY_CHARGE___DAY_FIELD.name = "__day"
MORROW_DAILY_CHARGE___DAY_FIELD.full_name = ".morrow_daily_charge.__day"
MORROW_DAILY_CHARGE___DAY_FIELD.number = 3
MORROW_DAILY_CHARGE___DAY_FIELD.index = 2
MORROW_DAILY_CHARGE___DAY_FIELD.label = 2
MORROW_DAILY_CHARGE___DAY_FIELD.has_default_value = false
MORROW_DAILY_CHARGE___DAY_FIELD.default_value = 0
MORROW_DAILY_CHARGE___DAY_FIELD.type = 5
MORROW_DAILY_CHARGE___DAY_FIELD.cpp_type = 1

MORROW_DAILY_CHARGE___REWARD_FIELD.name = "__reward"
MORROW_DAILY_CHARGE___REWARD_FIELD.full_name = ".morrow_daily_charge.__reward"
MORROW_DAILY_CHARGE___REWARD_FIELD.number = 4
MORROW_DAILY_CHARGE___REWARD_FIELD.index = 3
MORROW_DAILY_CHARGE___REWARD_FIELD.label = 2
MORROW_DAILY_CHARGE___REWARD_FIELD.has_default_value = false
MORROW_DAILY_CHARGE___REWARD_FIELD.default_value = ""
MORROW_DAILY_CHARGE___REWARD_FIELD.type = 9
MORROW_DAILY_CHARGE___REWARD_FIELD.cpp_type = 9

MORROW_DAILY_CHARGE.name = "morrow_daily_charge"
MORROW_DAILY_CHARGE.full_name = ".morrow_daily_charge"
MORROW_DAILY_CHARGE.nested_types = {}
MORROW_DAILY_CHARGE.enum_types = {}
MORROW_DAILY_CHARGE.fields = {MORROW_DAILY_CHARGE___ID_FIELD, MORROW_DAILY_CHARGE___COST_FIELD, MORROW_DAILY_CHARGE___DAY_FIELD, MORROW_DAILY_CHARGE___REWARD_FIELD}
MORROW_DAILY_CHARGE.is_extendable = false
MORROW_DAILY_CHARGE.extensions = {}

morrow_daily_charge = protobuf.Message(MORROW_DAILY_CHARGE)

