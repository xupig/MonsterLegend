--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.eudemon_star_pb')

EUDEMON_STAR = protobuf.Descriptor();
EUDEMON_STAR___ID_FIELD = protobuf.FieldDescriptor();
EUDEMON_STAR___STAR_FIELD = protobuf.FieldDescriptor();
EUDEMON_STAR___LEVEL_LIMIT_FIELD = protobuf.FieldDescriptor();
EUDEMON_STAR___COST_FIELD = protobuf.FieldDescriptor();
EUDEMON_STAR___RANK_FIELD = protobuf.FieldDescriptor();
EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD = protobuf.FieldDescriptor();
EUDEMON_STAR___COST_ITEM_FIELD = protobuf.FieldDescriptor();

EUDEMON_STAR___ID_FIELD.name = "__id"
EUDEMON_STAR___ID_FIELD.full_name = ".eudemon_star.__id"
EUDEMON_STAR___ID_FIELD.number = 1
EUDEMON_STAR___ID_FIELD.index = 0
EUDEMON_STAR___ID_FIELD.label = 2
EUDEMON_STAR___ID_FIELD.has_default_value = false
EUDEMON_STAR___ID_FIELD.default_value = 0
EUDEMON_STAR___ID_FIELD.type = 5
EUDEMON_STAR___ID_FIELD.cpp_type = 1

EUDEMON_STAR___STAR_FIELD.name = "__star"
EUDEMON_STAR___STAR_FIELD.full_name = ".eudemon_star.__star"
EUDEMON_STAR___STAR_FIELD.number = 2
EUDEMON_STAR___STAR_FIELD.index = 1
EUDEMON_STAR___STAR_FIELD.label = 2
EUDEMON_STAR___STAR_FIELD.has_default_value = false
EUDEMON_STAR___STAR_FIELD.default_value = 0
EUDEMON_STAR___STAR_FIELD.type = 5
EUDEMON_STAR___STAR_FIELD.cpp_type = 1

EUDEMON_STAR___LEVEL_LIMIT_FIELD.name = "__level_limit"
EUDEMON_STAR___LEVEL_LIMIT_FIELD.full_name = ".eudemon_star.__level_limit"
EUDEMON_STAR___LEVEL_LIMIT_FIELD.number = 3
EUDEMON_STAR___LEVEL_LIMIT_FIELD.index = 2
EUDEMON_STAR___LEVEL_LIMIT_FIELD.label = 2
EUDEMON_STAR___LEVEL_LIMIT_FIELD.has_default_value = false
EUDEMON_STAR___LEVEL_LIMIT_FIELD.default_value = 0
EUDEMON_STAR___LEVEL_LIMIT_FIELD.type = 5
EUDEMON_STAR___LEVEL_LIMIT_FIELD.cpp_type = 1

EUDEMON_STAR___COST_FIELD.name = "__cost"
EUDEMON_STAR___COST_FIELD.full_name = ".eudemon_star.__cost"
EUDEMON_STAR___COST_FIELD.number = 4
EUDEMON_STAR___COST_FIELD.index = 3
EUDEMON_STAR___COST_FIELD.label = 2
EUDEMON_STAR___COST_FIELD.has_default_value = false
EUDEMON_STAR___COST_FIELD.default_value = ""
EUDEMON_STAR___COST_FIELD.type = 9
EUDEMON_STAR___COST_FIELD.cpp_type = 9

EUDEMON_STAR___RANK_FIELD.name = "__rank"
EUDEMON_STAR___RANK_FIELD.full_name = ".eudemon_star.__rank"
EUDEMON_STAR___RANK_FIELD.number = 5
EUDEMON_STAR___RANK_FIELD.index = 4
EUDEMON_STAR___RANK_FIELD.label = 2
EUDEMON_STAR___RANK_FIELD.has_default_value = false
EUDEMON_STAR___RANK_FIELD.default_value = 0
EUDEMON_STAR___RANK_FIELD.type = 5
EUDEMON_STAR___RANK_FIELD.cpp_type = 1

EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD.name = "__skill_passive_count"
EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD.full_name = ".eudemon_star.__skill_passive_count"
EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD.number = 6
EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD.index = 5
EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD.label = 2
EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD.has_default_value = false
EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD.default_value = 0
EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD.type = 5
EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD.cpp_type = 1

EUDEMON_STAR___COST_ITEM_FIELD.name = "__cost_item"
EUDEMON_STAR___COST_ITEM_FIELD.full_name = ".eudemon_star.__cost_item"
EUDEMON_STAR___COST_ITEM_FIELD.number = 7
EUDEMON_STAR___COST_ITEM_FIELD.index = 6
EUDEMON_STAR___COST_ITEM_FIELD.label = 2
EUDEMON_STAR___COST_ITEM_FIELD.has_default_value = false
EUDEMON_STAR___COST_ITEM_FIELD.default_value = ""
EUDEMON_STAR___COST_ITEM_FIELD.type = 9
EUDEMON_STAR___COST_ITEM_FIELD.cpp_type = 9

EUDEMON_STAR.name = "eudemon_star"
EUDEMON_STAR.full_name = ".eudemon_star"
EUDEMON_STAR.nested_types = {}
EUDEMON_STAR.enum_types = {}
EUDEMON_STAR.fields = {EUDEMON_STAR___ID_FIELD, EUDEMON_STAR___STAR_FIELD, EUDEMON_STAR___LEVEL_LIMIT_FIELD, EUDEMON_STAR___COST_FIELD, EUDEMON_STAR___RANK_FIELD, EUDEMON_STAR___SKILL_PASSIVE_COUNT_FIELD, EUDEMON_STAR___COST_ITEM_FIELD}
EUDEMON_STAR.is_extendable = false
EUDEMON_STAR.extensions = {}

eudemon_star = protobuf.Message(EUDEMON_STAR)

