--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.offline_pvp_reward_2_pb')

OFFLINE_PVP_REWARD_2 = protobuf.Descriptor();
OFFLINE_PVP_REWARD_2___ID_FIELD = protobuf.FieldDescriptor();
OFFLINE_PVP_REWARD_2___LEVEL_FIELD = protobuf.FieldDescriptor();
OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD = protobuf.FieldDescriptor();
OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD = protobuf.FieldDescriptor();

OFFLINE_PVP_REWARD_2___ID_FIELD.name = "__id"
OFFLINE_PVP_REWARD_2___ID_FIELD.full_name = ".offline_pvp_reward_2.__id"
OFFLINE_PVP_REWARD_2___ID_FIELD.number = 1
OFFLINE_PVP_REWARD_2___ID_FIELD.index = 0
OFFLINE_PVP_REWARD_2___ID_FIELD.label = 2
OFFLINE_PVP_REWARD_2___ID_FIELD.has_default_value = false
OFFLINE_PVP_REWARD_2___ID_FIELD.default_value = 0
OFFLINE_PVP_REWARD_2___ID_FIELD.type = 5
OFFLINE_PVP_REWARD_2___ID_FIELD.cpp_type = 1

OFFLINE_PVP_REWARD_2___LEVEL_FIELD.name = "__level"
OFFLINE_PVP_REWARD_2___LEVEL_FIELD.full_name = ".offline_pvp_reward_2.__level"
OFFLINE_PVP_REWARD_2___LEVEL_FIELD.number = 2
OFFLINE_PVP_REWARD_2___LEVEL_FIELD.index = 1
OFFLINE_PVP_REWARD_2___LEVEL_FIELD.label = 2
OFFLINE_PVP_REWARD_2___LEVEL_FIELD.has_default_value = false
OFFLINE_PVP_REWARD_2___LEVEL_FIELD.default_value = 0
OFFLINE_PVP_REWARD_2___LEVEL_FIELD.type = 5
OFFLINE_PVP_REWARD_2___LEVEL_FIELD.cpp_type = 1

OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD.name = "__reward_win"
OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD.full_name = ".offline_pvp_reward_2.__reward_win"
OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD.number = 3
OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD.index = 2
OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD.label = 2
OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD.has_default_value = false
OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD.default_value = ""
OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD.type = 9
OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD.cpp_type = 9

OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD.name = "__reward_lose"
OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD.full_name = ".offline_pvp_reward_2.__reward_lose"
OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD.number = 4
OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD.index = 3
OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD.label = 2
OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD.has_default_value = false
OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD.default_value = ""
OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD.type = 9
OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD.cpp_type = 9

OFFLINE_PVP_REWARD_2.name = "offline_pvp_reward_2"
OFFLINE_PVP_REWARD_2.full_name = ".offline_pvp_reward_2"
OFFLINE_PVP_REWARD_2.nested_types = {}
OFFLINE_PVP_REWARD_2.enum_types = {}
OFFLINE_PVP_REWARD_2.fields = {OFFLINE_PVP_REWARD_2___ID_FIELD, OFFLINE_PVP_REWARD_2___LEVEL_FIELD, OFFLINE_PVP_REWARD_2___REWARD_WIN_FIELD, OFFLINE_PVP_REWARD_2___REWARD_LOSE_FIELD}
OFFLINE_PVP_REWARD_2.is_extendable = false
OFFLINE_PVP_REWARD_2.extensions = {}

offline_pvp_reward_2 = protobuf.Message(OFFLINE_PVP_REWARD_2)

