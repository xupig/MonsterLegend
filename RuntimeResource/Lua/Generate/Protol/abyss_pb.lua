--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.abyss_pb')

ABYSS = protobuf.Descriptor();
ABYSS___ID_FIELD = protobuf.FieldDescriptor();
ABYSS___BUFF_COUNT_FIELD = protobuf.FieldDescriptor();
ABYSS___RECOMMEND_COMBAT_VALUE_FIELD = protobuf.FieldDescriptor();
ABYSS___DIFFICULTY_PREVIEW_FIELD = protobuf.FieldDescriptor();
ABYSS___REWARD_PREVIEW_FIELD = protobuf.FieldDescriptor();
ABYSS___COST_FIELD = protobuf.FieldDescriptor();

ABYSS___ID_FIELD.name = "__id"
ABYSS___ID_FIELD.full_name = ".abyss.__id"
ABYSS___ID_FIELD.number = 1
ABYSS___ID_FIELD.index = 0
ABYSS___ID_FIELD.label = 2
ABYSS___ID_FIELD.has_default_value = false
ABYSS___ID_FIELD.default_value = 0
ABYSS___ID_FIELD.type = 5
ABYSS___ID_FIELD.cpp_type = 1

ABYSS___BUFF_COUNT_FIELD.name = "__buff_count"
ABYSS___BUFF_COUNT_FIELD.full_name = ".abyss.__buff_count"
ABYSS___BUFF_COUNT_FIELD.number = 2
ABYSS___BUFF_COUNT_FIELD.index = 1
ABYSS___BUFF_COUNT_FIELD.label = 2
ABYSS___BUFF_COUNT_FIELD.has_default_value = false
ABYSS___BUFF_COUNT_FIELD.default_value = 0
ABYSS___BUFF_COUNT_FIELD.type = 5
ABYSS___BUFF_COUNT_FIELD.cpp_type = 1

ABYSS___RECOMMEND_COMBAT_VALUE_FIELD.name = "__recommend_combat_value"
ABYSS___RECOMMEND_COMBAT_VALUE_FIELD.full_name = ".abyss.__recommend_combat_value"
ABYSS___RECOMMEND_COMBAT_VALUE_FIELD.number = 3
ABYSS___RECOMMEND_COMBAT_VALUE_FIELD.index = 2
ABYSS___RECOMMEND_COMBAT_VALUE_FIELD.label = 2
ABYSS___RECOMMEND_COMBAT_VALUE_FIELD.has_default_value = false
ABYSS___RECOMMEND_COMBAT_VALUE_FIELD.default_value = 0
ABYSS___RECOMMEND_COMBAT_VALUE_FIELD.type = 5
ABYSS___RECOMMEND_COMBAT_VALUE_FIELD.cpp_type = 1

ABYSS___DIFFICULTY_PREVIEW_FIELD.name = "__difficulty_preview"
ABYSS___DIFFICULTY_PREVIEW_FIELD.full_name = ".abyss.__difficulty_preview"
ABYSS___DIFFICULTY_PREVIEW_FIELD.number = 4
ABYSS___DIFFICULTY_PREVIEW_FIELD.index = 3
ABYSS___DIFFICULTY_PREVIEW_FIELD.label = 2
ABYSS___DIFFICULTY_PREVIEW_FIELD.has_default_value = false
ABYSS___DIFFICULTY_PREVIEW_FIELD.default_value = ""
ABYSS___DIFFICULTY_PREVIEW_FIELD.type = 9
ABYSS___DIFFICULTY_PREVIEW_FIELD.cpp_type = 9

ABYSS___REWARD_PREVIEW_FIELD.name = "__reward_preview"
ABYSS___REWARD_PREVIEW_FIELD.full_name = ".abyss.__reward_preview"
ABYSS___REWARD_PREVIEW_FIELD.number = 5
ABYSS___REWARD_PREVIEW_FIELD.index = 4
ABYSS___REWARD_PREVIEW_FIELD.label = 2
ABYSS___REWARD_PREVIEW_FIELD.has_default_value = false
ABYSS___REWARD_PREVIEW_FIELD.default_value = ""
ABYSS___REWARD_PREVIEW_FIELD.type = 9
ABYSS___REWARD_PREVIEW_FIELD.cpp_type = 9

ABYSS___COST_FIELD.name = "__cost"
ABYSS___COST_FIELD.full_name = ".abyss.__cost"
ABYSS___COST_FIELD.number = 6
ABYSS___COST_FIELD.index = 5
ABYSS___COST_FIELD.label = 2
ABYSS___COST_FIELD.has_default_value = false
ABYSS___COST_FIELD.default_value = ""
ABYSS___COST_FIELD.type = 9
ABYSS___COST_FIELD.cpp_type = 9

ABYSS.name = "abyss"
ABYSS.full_name = ".abyss"
ABYSS.nested_types = {}
ABYSS.enum_types = {}
ABYSS.fields = {ABYSS___ID_FIELD, ABYSS___BUFF_COUNT_FIELD, ABYSS___RECOMMEND_COMBAT_VALUE_FIELD, ABYSS___DIFFICULTY_PREVIEW_FIELD, ABYSS___REWARD_PREVIEW_FIELD, ABYSS___COST_FIELD}
ABYSS.is_extendable = false
ABYSS.extensions = {}

abyss = protobuf.Message(ABYSS)

