--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.weapon_gods_pb')

WEAPON_GODS = protobuf.Descriptor();
WEAPON_GODS___ID_FIELD = protobuf.FieldDescriptor();
WEAPON_GODS___RANK_FIELD = protobuf.FieldDescriptor();
WEAPON_GODS___WORLD_LEVEL_FIELD = protobuf.FieldDescriptor();
WEAPON_GODS___REWARD_FIELD = protobuf.FieldDescriptor();

WEAPON_GODS___ID_FIELD.name = "__id"
WEAPON_GODS___ID_FIELD.full_name = ".weapon_gods.__id"
WEAPON_GODS___ID_FIELD.number = 1
WEAPON_GODS___ID_FIELD.index = 0
WEAPON_GODS___ID_FIELD.label = 2
WEAPON_GODS___ID_FIELD.has_default_value = false
WEAPON_GODS___ID_FIELD.default_value = 0
WEAPON_GODS___ID_FIELD.type = 5
WEAPON_GODS___ID_FIELD.cpp_type = 1

WEAPON_GODS___RANK_FIELD.name = "__rank"
WEAPON_GODS___RANK_FIELD.full_name = ".weapon_gods.__rank"
WEAPON_GODS___RANK_FIELD.number = 2
WEAPON_GODS___RANK_FIELD.index = 1
WEAPON_GODS___RANK_FIELD.label = 2
WEAPON_GODS___RANK_FIELD.has_default_value = false
WEAPON_GODS___RANK_FIELD.default_value = 0
WEAPON_GODS___RANK_FIELD.type = 5
WEAPON_GODS___RANK_FIELD.cpp_type = 1

WEAPON_GODS___WORLD_LEVEL_FIELD.name = "__world_level"
WEAPON_GODS___WORLD_LEVEL_FIELD.full_name = ".weapon_gods.__world_level"
WEAPON_GODS___WORLD_LEVEL_FIELD.number = 3
WEAPON_GODS___WORLD_LEVEL_FIELD.index = 2
WEAPON_GODS___WORLD_LEVEL_FIELD.label = 2
WEAPON_GODS___WORLD_LEVEL_FIELD.has_default_value = false
WEAPON_GODS___WORLD_LEVEL_FIELD.default_value = ""
WEAPON_GODS___WORLD_LEVEL_FIELD.type = 9
WEAPON_GODS___WORLD_LEVEL_FIELD.cpp_type = 9

WEAPON_GODS___REWARD_FIELD.name = "__reward"
WEAPON_GODS___REWARD_FIELD.full_name = ".weapon_gods.__reward"
WEAPON_GODS___REWARD_FIELD.number = 4
WEAPON_GODS___REWARD_FIELD.index = 3
WEAPON_GODS___REWARD_FIELD.label = 2
WEAPON_GODS___REWARD_FIELD.has_default_value = false
WEAPON_GODS___REWARD_FIELD.default_value = ""
WEAPON_GODS___REWARD_FIELD.type = 9
WEAPON_GODS___REWARD_FIELD.cpp_type = 9

WEAPON_GODS.name = "weapon_gods"
WEAPON_GODS.full_name = ".weapon_gods"
WEAPON_GODS.nested_types = {}
WEAPON_GODS.enum_types = {}
WEAPON_GODS.fields = {WEAPON_GODS___ID_FIELD, WEAPON_GODS___RANK_FIELD, WEAPON_GODS___WORLD_LEVEL_FIELD, WEAPON_GODS___REWARD_FIELD}
WEAPON_GODS.is_extendable = false
WEAPON_GODS.extensions = {}

weapon_gods = protobuf.Message(WEAPON_GODS)

