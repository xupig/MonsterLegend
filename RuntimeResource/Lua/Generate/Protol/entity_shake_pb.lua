--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.entity_shake_pb')

ENTITY_SHAKE = protobuf.Descriptor();
ENTITY_SHAKE___ID_FIELD = protobuf.FieldDescriptor();
ENTITY_SHAKE___X_AMPLITUDE_FIELD = protobuf.FieldDescriptor();
ENTITY_SHAKE___Y_AMPLITUDE_FIELD = protobuf.FieldDescriptor();
ENTITY_SHAKE___Z_AMPLITUDE_FIELD = protobuf.FieldDescriptor();
ENTITY_SHAKE___PERIOD_FIELD = protobuf.FieldDescriptor();
ENTITY_SHAKE___DURATION_FIELD = protobuf.FieldDescriptor();

ENTITY_SHAKE___ID_FIELD.name = "__id"
ENTITY_SHAKE___ID_FIELD.full_name = ".entity_shake.__id"
ENTITY_SHAKE___ID_FIELD.number = 1
ENTITY_SHAKE___ID_FIELD.index = 0
ENTITY_SHAKE___ID_FIELD.label = 2
ENTITY_SHAKE___ID_FIELD.has_default_value = false
ENTITY_SHAKE___ID_FIELD.default_value = 0
ENTITY_SHAKE___ID_FIELD.type = 5
ENTITY_SHAKE___ID_FIELD.cpp_type = 1

ENTITY_SHAKE___X_AMPLITUDE_FIELD.name = "__x_amplitude"
ENTITY_SHAKE___X_AMPLITUDE_FIELD.full_name = ".entity_shake.__x_amplitude"
ENTITY_SHAKE___X_AMPLITUDE_FIELD.number = 2
ENTITY_SHAKE___X_AMPLITUDE_FIELD.index = 1
ENTITY_SHAKE___X_AMPLITUDE_FIELD.label = 2
ENTITY_SHAKE___X_AMPLITUDE_FIELD.has_default_value = false
ENTITY_SHAKE___X_AMPLITUDE_FIELD.default_value = 0.0
ENTITY_SHAKE___X_AMPLITUDE_FIELD.type = 2
ENTITY_SHAKE___X_AMPLITUDE_FIELD.cpp_type = 6

ENTITY_SHAKE___Y_AMPLITUDE_FIELD.name = "__y_amplitude"
ENTITY_SHAKE___Y_AMPLITUDE_FIELD.full_name = ".entity_shake.__y_amplitude"
ENTITY_SHAKE___Y_AMPLITUDE_FIELD.number = 3
ENTITY_SHAKE___Y_AMPLITUDE_FIELD.index = 2
ENTITY_SHAKE___Y_AMPLITUDE_FIELD.label = 2
ENTITY_SHAKE___Y_AMPLITUDE_FIELD.has_default_value = false
ENTITY_SHAKE___Y_AMPLITUDE_FIELD.default_value = 0.0
ENTITY_SHAKE___Y_AMPLITUDE_FIELD.type = 2
ENTITY_SHAKE___Y_AMPLITUDE_FIELD.cpp_type = 6

ENTITY_SHAKE___Z_AMPLITUDE_FIELD.name = "__z_amplitude"
ENTITY_SHAKE___Z_AMPLITUDE_FIELD.full_name = ".entity_shake.__z_amplitude"
ENTITY_SHAKE___Z_AMPLITUDE_FIELD.number = 4
ENTITY_SHAKE___Z_AMPLITUDE_FIELD.index = 3
ENTITY_SHAKE___Z_AMPLITUDE_FIELD.label = 2
ENTITY_SHAKE___Z_AMPLITUDE_FIELD.has_default_value = false
ENTITY_SHAKE___Z_AMPLITUDE_FIELD.default_value = 0.0
ENTITY_SHAKE___Z_AMPLITUDE_FIELD.type = 2
ENTITY_SHAKE___Z_AMPLITUDE_FIELD.cpp_type = 6

ENTITY_SHAKE___PERIOD_FIELD.name = "__period"
ENTITY_SHAKE___PERIOD_FIELD.full_name = ".entity_shake.__period"
ENTITY_SHAKE___PERIOD_FIELD.number = 5
ENTITY_SHAKE___PERIOD_FIELD.index = 4
ENTITY_SHAKE___PERIOD_FIELD.label = 2
ENTITY_SHAKE___PERIOD_FIELD.has_default_value = false
ENTITY_SHAKE___PERIOD_FIELD.default_value = 0.0
ENTITY_SHAKE___PERIOD_FIELD.type = 2
ENTITY_SHAKE___PERIOD_FIELD.cpp_type = 6

ENTITY_SHAKE___DURATION_FIELD.name = "__duration"
ENTITY_SHAKE___DURATION_FIELD.full_name = ".entity_shake.__duration"
ENTITY_SHAKE___DURATION_FIELD.number = 6
ENTITY_SHAKE___DURATION_FIELD.index = 5
ENTITY_SHAKE___DURATION_FIELD.label = 2
ENTITY_SHAKE___DURATION_FIELD.has_default_value = false
ENTITY_SHAKE___DURATION_FIELD.default_value = 0.0
ENTITY_SHAKE___DURATION_FIELD.type = 2
ENTITY_SHAKE___DURATION_FIELD.cpp_type = 6

ENTITY_SHAKE.name = "entity_shake"
ENTITY_SHAKE.full_name = ".entity_shake"
ENTITY_SHAKE.nested_types = {}
ENTITY_SHAKE.enum_types = {}
ENTITY_SHAKE.fields = {ENTITY_SHAKE___ID_FIELD, ENTITY_SHAKE___X_AMPLITUDE_FIELD, ENTITY_SHAKE___Y_AMPLITUDE_FIELD, ENTITY_SHAKE___Z_AMPLITUDE_FIELD, ENTITY_SHAKE___PERIOD_FIELD, ENTITY_SHAKE___DURATION_FIELD}
ENTITY_SHAKE.is_extendable = false
ENTITY_SHAKE.extensions = {}

entity_shake = protobuf.Message(ENTITY_SHAKE)

