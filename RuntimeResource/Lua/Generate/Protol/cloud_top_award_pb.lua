--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.cloud_top_award_pb')

CLOUD_TOP_AWARD = protobuf.Descriptor();
CLOUD_TOP_AWARD___ID_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___LEVEL_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___FLOOR1_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___FLOOR2_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___FLOOR3_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___FLOOR4_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___FLOOR5_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___FLOOR6_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___FLOOR7_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___FLOOR8_FIELD = protobuf.FieldDescriptor();
CLOUD_TOP_AWARD___FLOOR9_FIELD = protobuf.FieldDescriptor();

CLOUD_TOP_AWARD___ID_FIELD.name = "__id"
CLOUD_TOP_AWARD___ID_FIELD.full_name = ".cloud_top_award.__id"
CLOUD_TOP_AWARD___ID_FIELD.number = 1
CLOUD_TOP_AWARD___ID_FIELD.index = 0
CLOUD_TOP_AWARD___ID_FIELD.label = 2
CLOUD_TOP_AWARD___ID_FIELD.has_default_value = false
CLOUD_TOP_AWARD___ID_FIELD.default_value = 0
CLOUD_TOP_AWARD___ID_FIELD.type = 5
CLOUD_TOP_AWARD___ID_FIELD.cpp_type = 1

CLOUD_TOP_AWARD___LEVEL_FIELD.name = "__level"
CLOUD_TOP_AWARD___LEVEL_FIELD.full_name = ".cloud_top_award.__level"
CLOUD_TOP_AWARD___LEVEL_FIELD.number = 2
CLOUD_TOP_AWARD___LEVEL_FIELD.index = 1
CLOUD_TOP_AWARD___LEVEL_FIELD.label = 2
CLOUD_TOP_AWARD___LEVEL_FIELD.has_default_value = false
CLOUD_TOP_AWARD___LEVEL_FIELD.default_value = 0
CLOUD_TOP_AWARD___LEVEL_FIELD.type = 5
CLOUD_TOP_AWARD___LEVEL_FIELD.cpp_type = 1

CLOUD_TOP_AWARD___FLOOR1_FIELD.name = "__floor1"
CLOUD_TOP_AWARD___FLOOR1_FIELD.full_name = ".cloud_top_award.__floor1"
CLOUD_TOP_AWARD___FLOOR1_FIELD.number = 3
CLOUD_TOP_AWARD___FLOOR1_FIELD.index = 2
CLOUD_TOP_AWARD___FLOOR1_FIELD.label = 2
CLOUD_TOP_AWARD___FLOOR1_FIELD.has_default_value = false
CLOUD_TOP_AWARD___FLOOR1_FIELD.default_value = ""
CLOUD_TOP_AWARD___FLOOR1_FIELD.type = 9
CLOUD_TOP_AWARD___FLOOR1_FIELD.cpp_type = 9

CLOUD_TOP_AWARD___FLOOR2_FIELD.name = "__floor2"
CLOUD_TOP_AWARD___FLOOR2_FIELD.full_name = ".cloud_top_award.__floor2"
CLOUD_TOP_AWARD___FLOOR2_FIELD.number = 4
CLOUD_TOP_AWARD___FLOOR2_FIELD.index = 3
CLOUD_TOP_AWARD___FLOOR2_FIELD.label = 2
CLOUD_TOP_AWARD___FLOOR2_FIELD.has_default_value = false
CLOUD_TOP_AWARD___FLOOR2_FIELD.default_value = ""
CLOUD_TOP_AWARD___FLOOR2_FIELD.type = 9
CLOUD_TOP_AWARD___FLOOR2_FIELD.cpp_type = 9

CLOUD_TOP_AWARD___FLOOR3_FIELD.name = "__floor3"
CLOUD_TOP_AWARD___FLOOR3_FIELD.full_name = ".cloud_top_award.__floor3"
CLOUD_TOP_AWARD___FLOOR3_FIELD.number = 5
CLOUD_TOP_AWARD___FLOOR3_FIELD.index = 4
CLOUD_TOP_AWARD___FLOOR3_FIELD.label = 2
CLOUD_TOP_AWARD___FLOOR3_FIELD.has_default_value = false
CLOUD_TOP_AWARD___FLOOR3_FIELD.default_value = ""
CLOUD_TOP_AWARD___FLOOR3_FIELD.type = 9
CLOUD_TOP_AWARD___FLOOR3_FIELD.cpp_type = 9

CLOUD_TOP_AWARD___FLOOR4_FIELD.name = "__floor4"
CLOUD_TOP_AWARD___FLOOR4_FIELD.full_name = ".cloud_top_award.__floor4"
CLOUD_TOP_AWARD___FLOOR4_FIELD.number = 6
CLOUD_TOP_AWARD___FLOOR4_FIELD.index = 5
CLOUD_TOP_AWARD___FLOOR4_FIELD.label = 2
CLOUD_TOP_AWARD___FLOOR4_FIELD.has_default_value = false
CLOUD_TOP_AWARD___FLOOR4_FIELD.default_value = ""
CLOUD_TOP_AWARD___FLOOR4_FIELD.type = 9
CLOUD_TOP_AWARD___FLOOR4_FIELD.cpp_type = 9

CLOUD_TOP_AWARD___FLOOR5_FIELD.name = "__floor5"
CLOUD_TOP_AWARD___FLOOR5_FIELD.full_name = ".cloud_top_award.__floor5"
CLOUD_TOP_AWARD___FLOOR5_FIELD.number = 7
CLOUD_TOP_AWARD___FLOOR5_FIELD.index = 6
CLOUD_TOP_AWARD___FLOOR5_FIELD.label = 2
CLOUD_TOP_AWARD___FLOOR5_FIELD.has_default_value = false
CLOUD_TOP_AWARD___FLOOR5_FIELD.default_value = ""
CLOUD_TOP_AWARD___FLOOR5_FIELD.type = 9
CLOUD_TOP_AWARD___FLOOR5_FIELD.cpp_type = 9

CLOUD_TOP_AWARD___FLOOR6_FIELD.name = "__floor6"
CLOUD_TOP_AWARD___FLOOR6_FIELD.full_name = ".cloud_top_award.__floor6"
CLOUD_TOP_AWARD___FLOOR6_FIELD.number = 8
CLOUD_TOP_AWARD___FLOOR6_FIELD.index = 7
CLOUD_TOP_AWARD___FLOOR6_FIELD.label = 2
CLOUD_TOP_AWARD___FLOOR6_FIELD.has_default_value = false
CLOUD_TOP_AWARD___FLOOR6_FIELD.default_value = ""
CLOUD_TOP_AWARD___FLOOR6_FIELD.type = 9
CLOUD_TOP_AWARD___FLOOR6_FIELD.cpp_type = 9

CLOUD_TOP_AWARD___FLOOR7_FIELD.name = "__floor7"
CLOUD_TOP_AWARD___FLOOR7_FIELD.full_name = ".cloud_top_award.__floor7"
CLOUD_TOP_AWARD___FLOOR7_FIELD.number = 9
CLOUD_TOP_AWARD___FLOOR7_FIELD.index = 8
CLOUD_TOP_AWARD___FLOOR7_FIELD.label = 2
CLOUD_TOP_AWARD___FLOOR7_FIELD.has_default_value = false
CLOUD_TOP_AWARD___FLOOR7_FIELD.default_value = ""
CLOUD_TOP_AWARD___FLOOR7_FIELD.type = 9
CLOUD_TOP_AWARD___FLOOR7_FIELD.cpp_type = 9

CLOUD_TOP_AWARD___FLOOR8_FIELD.name = "__floor8"
CLOUD_TOP_AWARD___FLOOR8_FIELD.full_name = ".cloud_top_award.__floor8"
CLOUD_TOP_AWARD___FLOOR8_FIELD.number = 10
CLOUD_TOP_AWARD___FLOOR8_FIELD.index = 9
CLOUD_TOP_AWARD___FLOOR8_FIELD.label = 2
CLOUD_TOP_AWARD___FLOOR8_FIELD.has_default_value = false
CLOUD_TOP_AWARD___FLOOR8_FIELD.default_value = ""
CLOUD_TOP_AWARD___FLOOR8_FIELD.type = 9
CLOUD_TOP_AWARD___FLOOR8_FIELD.cpp_type = 9

CLOUD_TOP_AWARD___FLOOR9_FIELD.name = "__floor9"
CLOUD_TOP_AWARD___FLOOR9_FIELD.full_name = ".cloud_top_award.__floor9"
CLOUD_TOP_AWARD___FLOOR9_FIELD.number = 11
CLOUD_TOP_AWARD___FLOOR9_FIELD.index = 10
CLOUD_TOP_AWARD___FLOOR9_FIELD.label = 2
CLOUD_TOP_AWARD___FLOOR9_FIELD.has_default_value = false
CLOUD_TOP_AWARD___FLOOR9_FIELD.default_value = ""
CLOUD_TOP_AWARD___FLOOR9_FIELD.type = 9
CLOUD_TOP_AWARD___FLOOR9_FIELD.cpp_type = 9

CLOUD_TOP_AWARD.name = "cloud_top_award"
CLOUD_TOP_AWARD.full_name = ".cloud_top_award"
CLOUD_TOP_AWARD.nested_types = {}
CLOUD_TOP_AWARD.enum_types = {}
CLOUD_TOP_AWARD.fields = {CLOUD_TOP_AWARD___ID_FIELD, CLOUD_TOP_AWARD___LEVEL_FIELD, CLOUD_TOP_AWARD___FLOOR1_FIELD, CLOUD_TOP_AWARD___FLOOR2_FIELD, CLOUD_TOP_AWARD___FLOOR3_FIELD, CLOUD_TOP_AWARD___FLOOR4_FIELD, CLOUD_TOP_AWARD___FLOOR5_FIELD, CLOUD_TOP_AWARD___FLOOR6_FIELD, CLOUD_TOP_AWARD___FLOOR7_FIELD, CLOUD_TOP_AWARD___FLOOR8_FIELD, CLOUD_TOP_AWARD___FLOOR9_FIELD}
CLOUD_TOP_AWARD.is_extendable = false
CLOUD_TOP_AWARD.extensions = {}

cloud_top_award = protobuf.Message(CLOUD_TOP_AWARD)

