--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.whole_people_jianbao_pb')

WHOLE_PEOPLE_JIANBAO = protobuf.Descriptor();
WHOLE_PEOPLE_JIANBAO___ID_FIELD = protobuf.FieldDescriptor();
WHOLE_PEOPLE_JIANBAO___TURN_FIELD = protobuf.FieldDescriptor();
WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD = protobuf.FieldDescriptor();
WHOLE_PEOPLE_JIANBAO___REWARD_FIELD = protobuf.FieldDescriptor();
WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD = protobuf.FieldDescriptor();
WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD = protobuf.FieldDescriptor();
WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD = protobuf.FieldDescriptor();
WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD = protobuf.FieldDescriptor();
WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD = protobuf.FieldDescriptor();

WHOLE_PEOPLE_JIANBAO___ID_FIELD.name = "__id"
WHOLE_PEOPLE_JIANBAO___ID_FIELD.full_name = ".whole_people_jianbao.__id"
WHOLE_PEOPLE_JIANBAO___ID_FIELD.number = 1
WHOLE_PEOPLE_JIANBAO___ID_FIELD.index = 0
WHOLE_PEOPLE_JIANBAO___ID_FIELD.label = 2
WHOLE_PEOPLE_JIANBAO___ID_FIELD.has_default_value = false
WHOLE_PEOPLE_JIANBAO___ID_FIELD.default_value = 0
WHOLE_PEOPLE_JIANBAO___ID_FIELD.type = 5
WHOLE_PEOPLE_JIANBAO___ID_FIELD.cpp_type = 1

WHOLE_PEOPLE_JIANBAO___TURN_FIELD.name = "__turn"
WHOLE_PEOPLE_JIANBAO___TURN_FIELD.full_name = ".whole_people_jianbao.__turn"
WHOLE_PEOPLE_JIANBAO___TURN_FIELD.number = 2
WHOLE_PEOPLE_JIANBAO___TURN_FIELD.index = 1
WHOLE_PEOPLE_JIANBAO___TURN_FIELD.label = 2
WHOLE_PEOPLE_JIANBAO___TURN_FIELD.has_default_value = false
WHOLE_PEOPLE_JIANBAO___TURN_FIELD.default_value = 0
WHOLE_PEOPLE_JIANBAO___TURN_FIELD.type = 5
WHOLE_PEOPLE_JIANBAO___TURN_FIELD.cpp_type = 1

WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD.name = "__world_level"
WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD.full_name = ".whole_people_jianbao.__world_level"
WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD.number = 3
WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD.index = 2
WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD.label = 2
WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD.has_default_value = false
WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD.default_value = ""
WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD.type = 9
WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD.cpp_type = 9

WHOLE_PEOPLE_JIANBAO___REWARD_FIELD.name = "__reward"
WHOLE_PEOPLE_JIANBAO___REWARD_FIELD.full_name = ".whole_people_jianbao.__reward"
WHOLE_PEOPLE_JIANBAO___REWARD_FIELD.number = 4
WHOLE_PEOPLE_JIANBAO___REWARD_FIELD.index = 3
WHOLE_PEOPLE_JIANBAO___REWARD_FIELD.label = 2
WHOLE_PEOPLE_JIANBAO___REWARD_FIELD.has_default_value = false
WHOLE_PEOPLE_JIANBAO___REWARD_FIELD.default_value = ""
WHOLE_PEOPLE_JIANBAO___REWARD_FIELD.type = 9
WHOLE_PEOPLE_JIANBAO___REWARD_FIELD.cpp_type = 9

WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD.name = "__turn_reward"
WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD.full_name = ".whole_people_jianbao.__turn_reward"
WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD.number = 5
WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD.index = 4
WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD.label = 2
WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD.has_default_value = false
WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD.default_value = ""
WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD.type = 9
WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD.cpp_type = 9

WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD.name = "__important"
WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD.full_name = ".whole_people_jianbao.__important"
WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD.number = 6
WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD.index = 5
WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD.label = 2
WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD.has_default_value = false
WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD.default_value = ""
WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD.type = 9
WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD.cpp_type = 9

WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD.name = "__normal"
WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD.full_name = ".whole_people_jianbao.__normal"
WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD.number = 7
WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD.index = 6
WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD.label = 2
WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD.has_default_value = false
WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD.default_value = ""
WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD.type = 9
WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD.cpp_type = 9

WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD.name = "__dislpay_first"
WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD.full_name = ".whole_people_jianbao.__dislpay_first"
WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD.number = 8
WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD.index = 7
WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD.label = 2
WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD.has_default_value = false
WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD.default_value = ""
WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD.type = 9
WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD.cpp_type = 9

WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD.name = "__dislpay_turn"
WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD.full_name = ".whole_people_jianbao.__dislpay_turn"
WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD.number = 9
WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD.index = 8
WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD.label = 2
WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD.has_default_value = false
WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD.default_value = ""
WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD.type = 9
WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD.cpp_type = 9

WHOLE_PEOPLE_JIANBAO.name = "whole_people_jianbao"
WHOLE_PEOPLE_JIANBAO.full_name = ".whole_people_jianbao"
WHOLE_PEOPLE_JIANBAO.nested_types = {}
WHOLE_PEOPLE_JIANBAO.enum_types = {}
WHOLE_PEOPLE_JIANBAO.fields = {WHOLE_PEOPLE_JIANBAO___ID_FIELD, WHOLE_PEOPLE_JIANBAO___TURN_FIELD, WHOLE_PEOPLE_JIANBAO___WORLD_LEVEL_FIELD, WHOLE_PEOPLE_JIANBAO___REWARD_FIELD, WHOLE_PEOPLE_JIANBAO___TURN_REWARD_FIELD, WHOLE_PEOPLE_JIANBAO___IMPORTANT_FIELD, WHOLE_PEOPLE_JIANBAO___NORMAL_FIELD, WHOLE_PEOPLE_JIANBAO___DISLPAY_FIRST_FIELD, WHOLE_PEOPLE_JIANBAO___DISLPAY_TURN_FIELD}
WHOLE_PEOPLE_JIANBAO.is_extendable = false
WHOLE_PEOPLE_JIANBAO.extensions = {}

whole_people_jianbao = protobuf.Message(WHOLE_PEOPLE_JIANBAO)

