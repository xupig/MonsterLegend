--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.double_reward_pb')

DOUBLE_REWARD = protobuf.Descriptor();
DOUBLE_REWARD___ID_FIELD = protobuf.FieldDescriptor();
DOUBLE_REWARD___DAY_REWARD_FIELD = protobuf.FieldDescriptor();
DOUBLE_REWARD___FOLLOW_FIELD = protobuf.FieldDescriptor();

DOUBLE_REWARD___ID_FIELD.name = "__id"
DOUBLE_REWARD___ID_FIELD.full_name = ".double_reward.__id"
DOUBLE_REWARD___ID_FIELD.number = 1
DOUBLE_REWARD___ID_FIELD.index = 0
DOUBLE_REWARD___ID_FIELD.label = 2
DOUBLE_REWARD___ID_FIELD.has_default_value = false
DOUBLE_REWARD___ID_FIELD.default_value = 0
DOUBLE_REWARD___ID_FIELD.type = 5
DOUBLE_REWARD___ID_FIELD.cpp_type = 1

DOUBLE_REWARD___DAY_REWARD_FIELD.name = "__day_reward"
DOUBLE_REWARD___DAY_REWARD_FIELD.full_name = ".double_reward.__day_reward"
DOUBLE_REWARD___DAY_REWARD_FIELD.number = 2
DOUBLE_REWARD___DAY_REWARD_FIELD.index = 1
DOUBLE_REWARD___DAY_REWARD_FIELD.label = 2
DOUBLE_REWARD___DAY_REWARD_FIELD.has_default_value = false
DOUBLE_REWARD___DAY_REWARD_FIELD.default_value = ""
DOUBLE_REWARD___DAY_REWARD_FIELD.type = 9
DOUBLE_REWARD___DAY_REWARD_FIELD.cpp_type = 9

DOUBLE_REWARD___FOLLOW_FIELD.name = "__follow"
DOUBLE_REWARD___FOLLOW_FIELD.full_name = ".double_reward.__follow"
DOUBLE_REWARD___FOLLOW_FIELD.number = 3
DOUBLE_REWARD___FOLLOW_FIELD.index = 2
DOUBLE_REWARD___FOLLOW_FIELD.label = 2
DOUBLE_REWARD___FOLLOW_FIELD.has_default_value = false
DOUBLE_REWARD___FOLLOW_FIELD.default_value = ""
DOUBLE_REWARD___FOLLOW_FIELD.type = 9
DOUBLE_REWARD___FOLLOW_FIELD.cpp_type = 9

DOUBLE_REWARD.name = "double_reward"
DOUBLE_REWARD.full_name = ".double_reward"
DOUBLE_REWARD.nested_types = {}
DOUBLE_REWARD.enum_types = {}
DOUBLE_REWARD.fields = {DOUBLE_REWARD___ID_FIELD, DOUBLE_REWARD___DAY_REWARD_FIELD, DOUBLE_REWARD___FOLLOW_FIELD}
DOUBLE_REWARD.is_extendable = false
DOUBLE_REWARD.extensions = {}

double_reward = protobuf.Message(DOUBLE_REWARD)

