--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.online_pvp_reward_3_pb')

ONLINE_PVP_REWARD_3 = protobuf.Descriptor();
ONLINE_PVP_REWARD_3___ID_FIELD = protobuf.FieldDescriptor();
ONLINE_PVP_REWARD_3___LEVEL_FIELD = protobuf.FieldDescriptor();
ONLINE_PVP_REWARD_3___TIME_FIELD = protobuf.FieldDescriptor();
ONLINE_PVP_REWARD_3___REWARD_FIELD = protobuf.FieldDescriptor();

ONLINE_PVP_REWARD_3___ID_FIELD.name = "__id"
ONLINE_PVP_REWARD_3___ID_FIELD.full_name = ".online_pvp_reward_3.__id"
ONLINE_PVP_REWARD_3___ID_FIELD.number = 1
ONLINE_PVP_REWARD_3___ID_FIELD.index = 0
ONLINE_PVP_REWARD_3___ID_FIELD.label = 2
ONLINE_PVP_REWARD_3___ID_FIELD.has_default_value = false
ONLINE_PVP_REWARD_3___ID_FIELD.default_value = 0
ONLINE_PVP_REWARD_3___ID_FIELD.type = 5
ONLINE_PVP_REWARD_3___ID_FIELD.cpp_type = 1

ONLINE_PVP_REWARD_3___LEVEL_FIELD.name = "__level"
ONLINE_PVP_REWARD_3___LEVEL_FIELD.full_name = ".online_pvp_reward_3.__level"
ONLINE_PVP_REWARD_3___LEVEL_FIELD.number = 2
ONLINE_PVP_REWARD_3___LEVEL_FIELD.index = 1
ONLINE_PVP_REWARD_3___LEVEL_FIELD.label = 2
ONLINE_PVP_REWARD_3___LEVEL_FIELD.has_default_value = false
ONLINE_PVP_REWARD_3___LEVEL_FIELD.default_value = 0
ONLINE_PVP_REWARD_3___LEVEL_FIELD.type = 5
ONLINE_PVP_REWARD_3___LEVEL_FIELD.cpp_type = 1

ONLINE_PVP_REWARD_3___TIME_FIELD.name = "__time"
ONLINE_PVP_REWARD_3___TIME_FIELD.full_name = ".online_pvp_reward_3.__time"
ONLINE_PVP_REWARD_3___TIME_FIELD.number = 3
ONLINE_PVP_REWARD_3___TIME_FIELD.index = 2
ONLINE_PVP_REWARD_3___TIME_FIELD.label = 2
ONLINE_PVP_REWARD_3___TIME_FIELD.has_default_value = false
ONLINE_PVP_REWARD_3___TIME_FIELD.default_value = 0
ONLINE_PVP_REWARD_3___TIME_FIELD.type = 5
ONLINE_PVP_REWARD_3___TIME_FIELD.cpp_type = 1

ONLINE_PVP_REWARD_3___REWARD_FIELD.name = "__reward"
ONLINE_PVP_REWARD_3___REWARD_FIELD.full_name = ".online_pvp_reward_3.__reward"
ONLINE_PVP_REWARD_3___REWARD_FIELD.number = 4
ONLINE_PVP_REWARD_3___REWARD_FIELD.index = 3
ONLINE_PVP_REWARD_3___REWARD_FIELD.label = 2
ONLINE_PVP_REWARD_3___REWARD_FIELD.has_default_value = false
ONLINE_PVP_REWARD_3___REWARD_FIELD.default_value = ""
ONLINE_PVP_REWARD_3___REWARD_FIELD.type = 9
ONLINE_PVP_REWARD_3___REWARD_FIELD.cpp_type = 9

ONLINE_PVP_REWARD_3.name = "online_pvp_reward_3"
ONLINE_PVP_REWARD_3.full_name = ".online_pvp_reward_3"
ONLINE_PVP_REWARD_3.nested_types = {}
ONLINE_PVP_REWARD_3.enum_types = {}
ONLINE_PVP_REWARD_3.fields = {ONLINE_PVP_REWARD_3___ID_FIELD, ONLINE_PVP_REWARD_3___LEVEL_FIELD, ONLINE_PVP_REWARD_3___TIME_FIELD, ONLINE_PVP_REWARD_3___REWARD_FIELD}
ONLINE_PVP_REWARD_3.is_extendable = false
ONLINE_PVP_REWARD_3.extensions = {}

online_pvp_reward_3 = protobuf.Message(ONLINE_PVP_REWARD_3)

