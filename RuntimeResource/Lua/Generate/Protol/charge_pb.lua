--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.charge_pb')

CHARGE = protobuf.Descriptor();
CHARGE___ID_FIELD = protobuf.FieldDescriptor();
CHARGE___ICON_FIELD = protobuf.FieldDescriptor();
CHARGE___MONEY_FIELD = protobuf.FieldDescriptor();
CHARGE___LISTING_ORDER_FIELD = protobuf.FieldDescriptor();
CHARGE___COUPONS_FIELD = protobuf.FieldDescriptor();
CHARGE___FIRST_BIND_COUPONS_FIELD = protobuf.FieldDescriptor();
CHARGE___ENABLE_FIELD = protobuf.FieldDescriptor();
CHARGE___CHINESE_FIELD = protobuf.FieldDescriptor();
CHARGE___LISTING_ID_FIELD = protobuf.FieldDescriptor();

CHARGE___ID_FIELD.name = "__id"
CHARGE___ID_FIELD.full_name = ".charge.__id"
CHARGE___ID_FIELD.number = 1
CHARGE___ID_FIELD.index = 0
CHARGE___ID_FIELD.label = 2
CHARGE___ID_FIELD.has_default_value = false
CHARGE___ID_FIELD.default_value = 0
CHARGE___ID_FIELD.type = 5
CHARGE___ID_FIELD.cpp_type = 1

CHARGE___ICON_FIELD.name = "__icon"
CHARGE___ICON_FIELD.full_name = ".charge.__icon"
CHARGE___ICON_FIELD.number = 2
CHARGE___ICON_FIELD.index = 1
CHARGE___ICON_FIELD.label = 2
CHARGE___ICON_FIELD.has_default_value = false
CHARGE___ICON_FIELD.default_value = 0
CHARGE___ICON_FIELD.type = 5
CHARGE___ICON_FIELD.cpp_type = 1

CHARGE___MONEY_FIELD.name = "__money"
CHARGE___MONEY_FIELD.full_name = ".charge.__money"
CHARGE___MONEY_FIELD.number = 3
CHARGE___MONEY_FIELD.index = 2
CHARGE___MONEY_FIELD.label = 2
CHARGE___MONEY_FIELD.has_default_value = false
CHARGE___MONEY_FIELD.default_value = 0
CHARGE___MONEY_FIELD.type = 5
CHARGE___MONEY_FIELD.cpp_type = 1

CHARGE___LISTING_ORDER_FIELD.name = "__listing_order"
CHARGE___LISTING_ORDER_FIELD.full_name = ".charge.__listing_order"
CHARGE___LISTING_ORDER_FIELD.number = 4
CHARGE___LISTING_ORDER_FIELD.index = 3
CHARGE___LISTING_ORDER_FIELD.label = 2
CHARGE___LISTING_ORDER_FIELD.has_default_value = false
CHARGE___LISTING_ORDER_FIELD.default_value = 0
CHARGE___LISTING_ORDER_FIELD.type = 5
CHARGE___LISTING_ORDER_FIELD.cpp_type = 1

CHARGE___COUPONS_FIELD.name = "__coupons"
CHARGE___COUPONS_FIELD.full_name = ".charge.__coupons"
CHARGE___COUPONS_FIELD.number = 5
CHARGE___COUPONS_FIELD.index = 4
CHARGE___COUPONS_FIELD.label = 2
CHARGE___COUPONS_FIELD.has_default_value = false
CHARGE___COUPONS_FIELD.default_value = 0
CHARGE___COUPONS_FIELD.type = 5
CHARGE___COUPONS_FIELD.cpp_type = 1

CHARGE___FIRST_BIND_COUPONS_FIELD.name = "__first_bind_coupons"
CHARGE___FIRST_BIND_COUPONS_FIELD.full_name = ".charge.__first_bind_coupons"
CHARGE___FIRST_BIND_COUPONS_FIELD.number = 6
CHARGE___FIRST_BIND_COUPONS_FIELD.index = 5
CHARGE___FIRST_BIND_COUPONS_FIELD.label = 2
CHARGE___FIRST_BIND_COUPONS_FIELD.has_default_value = false
CHARGE___FIRST_BIND_COUPONS_FIELD.default_value = 0
CHARGE___FIRST_BIND_COUPONS_FIELD.type = 5
CHARGE___FIRST_BIND_COUPONS_FIELD.cpp_type = 1

CHARGE___ENABLE_FIELD.name = "__enable"
CHARGE___ENABLE_FIELD.full_name = ".charge.__enable"
CHARGE___ENABLE_FIELD.number = 8
CHARGE___ENABLE_FIELD.index = 6
CHARGE___ENABLE_FIELD.label = 2
CHARGE___ENABLE_FIELD.has_default_value = false
CHARGE___ENABLE_FIELD.default_value = 0
CHARGE___ENABLE_FIELD.type = 5
CHARGE___ENABLE_FIELD.cpp_type = 1

CHARGE___CHINESE_FIELD.name = "__chinese"
CHARGE___CHINESE_FIELD.full_name = ".charge.__chinese"
CHARGE___CHINESE_FIELD.number = 9
CHARGE___CHINESE_FIELD.index = 7
CHARGE___CHINESE_FIELD.label = 2
CHARGE___CHINESE_FIELD.has_default_value = false
CHARGE___CHINESE_FIELD.default_value = 0
CHARGE___CHINESE_FIELD.type = 5
CHARGE___CHINESE_FIELD.cpp_type = 1

CHARGE___LISTING_ID_FIELD.name = "__listing_id"
CHARGE___LISTING_ID_FIELD.full_name = ".charge.__listing_id"
CHARGE___LISTING_ID_FIELD.number = 10
CHARGE___LISTING_ID_FIELD.index = 8
CHARGE___LISTING_ID_FIELD.label = 2
CHARGE___LISTING_ID_FIELD.has_default_value = false
CHARGE___LISTING_ID_FIELD.default_value = ""
CHARGE___LISTING_ID_FIELD.type = 9
CHARGE___LISTING_ID_FIELD.cpp_type = 9

CHARGE.name = "charge"
CHARGE.full_name = ".charge"
CHARGE.nested_types = {}
CHARGE.enum_types = {}
CHARGE.fields = {CHARGE___ID_FIELD, CHARGE___ICON_FIELD, CHARGE___MONEY_FIELD, CHARGE___LISTING_ORDER_FIELD, CHARGE___COUPONS_FIELD, CHARGE___FIRST_BIND_COUPONS_FIELD, CHARGE___ENABLE_FIELD, CHARGE___CHINESE_FIELD, CHARGE___LISTING_ID_FIELD}
CHARGE.is_extendable = false
CHARGE.extensions = {}

charge = protobuf.Message(CHARGE)

