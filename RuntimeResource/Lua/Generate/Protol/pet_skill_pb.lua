--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.pet_skill_pb')

PET_SKILL = protobuf.Descriptor();
PET_SKILL___ID_FIELD = protobuf.FieldDescriptor();
PET_SKILL___PET_SKILL_ID_FIELD = protobuf.FieldDescriptor();

PET_SKILL___ID_FIELD.name = "__id"
PET_SKILL___ID_FIELD.full_name = ".pet_skill.__id"
PET_SKILL___ID_FIELD.number = 1
PET_SKILL___ID_FIELD.index = 0
PET_SKILL___ID_FIELD.label = 2
PET_SKILL___ID_FIELD.has_default_value = false
PET_SKILL___ID_FIELD.default_value = 0
PET_SKILL___ID_FIELD.type = 5
PET_SKILL___ID_FIELD.cpp_type = 1

PET_SKILL___PET_SKILL_ID_FIELD.name = "__pet_skill_id"
PET_SKILL___PET_SKILL_ID_FIELD.full_name = ".pet_skill.__pet_skill_id"
PET_SKILL___PET_SKILL_ID_FIELD.number = 3
PET_SKILL___PET_SKILL_ID_FIELD.index = 1
PET_SKILL___PET_SKILL_ID_FIELD.label = 2
PET_SKILL___PET_SKILL_ID_FIELD.has_default_value = false
PET_SKILL___PET_SKILL_ID_FIELD.default_value = 0
PET_SKILL___PET_SKILL_ID_FIELD.type = 5
PET_SKILL___PET_SKILL_ID_FIELD.cpp_type = 1

PET_SKILL.name = "pet_skill"
PET_SKILL.full_name = ".pet_skill"
PET_SKILL.nested_types = {}
PET_SKILL.enum_types = {}
PET_SKILL.fields = {PET_SKILL___ID_FIELD, PET_SKILL___PET_SKILL_ID_FIELD}
PET_SKILL.is_extendable = false
PET_SKILL.extensions = {}

pet_skill = protobuf.Message(PET_SKILL)

