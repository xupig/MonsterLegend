--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.activity_level_pb')

ACTIVITY_LEVEL = protobuf.Descriptor();
ACTIVITY_LEVEL___ID_FIELD = protobuf.FieldDescriptor();
ACTIVITY_LEVEL___NEXT_COST_FIELD = protobuf.FieldDescriptor();
ACTIVITY_LEVEL___ATRRI_FIELD = protobuf.FieldDescriptor();

ACTIVITY_LEVEL___ID_FIELD.name = "__id"
ACTIVITY_LEVEL___ID_FIELD.full_name = ".activity_level.__id"
ACTIVITY_LEVEL___ID_FIELD.number = 1
ACTIVITY_LEVEL___ID_FIELD.index = 0
ACTIVITY_LEVEL___ID_FIELD.label = 2
ACTIVITY_LEVEL___ID_FIELD.has_default_value = false
ACTIVITY_LEVEL___ID_FIELD.default_value = 0
ACTIVITY_LEVEL___ID_FIELD.type = 5
ACTIVITY_LEVEL___ID_FIELD.cpp_type = 1

ACTIVITY_LEVEL___NEXT_COST_FIELD.name = "__next_cost"
ACTIVITY_LEVEL___NEXT_COST_FIELD.full_name = ".activity_level.__next_cost"
ACTIVITY_LEVEL___NEXT_COST_FIELD.number = 2
ACTIVITY_LEVEL___NEXT_COST_FIELD.index = 1
ACTIVITY_LEVEL___NEXT_COST_FIELD.label = 2
ACTIVITY_LEVEL___NEXT_COST_FIELD.has_default_value = false
ACTIVITY_LEVEL___NEXT_COST_FIELD.default_value = 0
ACTIVITY_LEVEL___NEXT_COST_FIELD.type = 5
ACTIVITY_LEVEL___NEXT_COST_FIELD.cpp_type = 1

ACTIVITY_LEVEL___ATRRI_FIELD.name = "__atrri"
ACTIVITY_LEVEL___ATRRI_FIELD.full_name = ".activity_level.__atrri"
ACTIVITY_LEVEL___ATRRI_FIELD.number = 3
ACTIVITY_LEVEL___ATRRI_FIELD.index = 2
ACTIVITY_LEVEL___ATRRI_FIELD.label = 2
ACTIVITY_LEVEL___ATRRI_FIELD.has_default_value = false
ACTIVITY_LEVEL___ATRRI_FIELD.default_value = ""
ACTIVITY_LEVEL___ATRRI_FIELD.type = 9
ACTIVITY_LEVEL___ATRRI_FIELD.cpp_type = 9

ACTIVITY_LEVEL.name = "activity_level"
ACTIVITY_LEVEL.full_name = ".activity_level"
ACTIVITY_LEVEL.nested_types = {}
ACTIVITY_LEVEL.enum_types = {}
ACTIVITY_LEVEL.fields = {ACTIVITY_LEVEL___ID_FIELD, ACTIVITY_LEVEL___NEXT_COST_FIELD, ACTIVITY_LEVEL___ATRRI_FIELD}
ACTIVITY_LEVEL.is_extendable = false
ACTIVITY_LEVEL.extensions = {}

activity_level = protobuf.Message(ACTIVITY_LEVEL)

