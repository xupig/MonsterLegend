--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.ancient_battlefield_rank_award_pb')

ANCIENT_BATTLEFIELD_RANK_AWARD = protobuf.Descriptor();
ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD = protobuf.FieldDescriptor();
ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD = protobuf.FieldDescriptor();
ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD = protobuf.FieldDescriptor();

ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD.name = "__id"
ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD.full_name = ".ancient_battlefield_rank_award.__id"
ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD.number = 1
ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD.index = 0
ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD.label = 2
ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD.has_default_value = false
ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD.default_value = 0
ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD.type = 5
ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD.cpp_type = 1

ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD.name = "__rank"
ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD.full_name = ".ancient_battlefield_rank_award.__rank"
ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD.number = 2
ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD.index = 1
ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD.label = 2
ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD.has_default_value = false
ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD.default_value = ""
ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD.type = 9
ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD.cpp_type = 9

ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD.name = "__award"
ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD.full_name = ".ancient_battlefield_rank_award.__award"
ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD.number = 3
ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD.index = 2
ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD.label = 2
ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD.has_default_value = false
ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD.default_value = ""
ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD.type = 9
ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD.cpp_type = 9

ANCIENT_BATTLEFIELD_RANK_AWARD.name = "ancient_battlefield_rank_award"
ANCIENT_BATTLEFIELD_RANK_AWARD.full_name = ".ancient_battlefield_rank_award"
ANCIENT_BATTLEFIELD_RANK_AWARD.nested_types = {}
ANCIENT_BATTLEFIELD_RANK_AWARD.enum_types = {}
ANCIENT_BATTLEFIELD_RANK_AWARD.fields = {ANCIENT_BATTLEFIELD_RANK_AWARD___ID_FIELD, ANCIENT_BATTLEFIELD_RANK_AWARD___RANK_FIELD, ANCIENT_BATTLEFIELD_RANK_AWARD___AWARD_FIELD}
ANCIENT_BATTLEFIELD_RANK_AWARD.is_extendable = false
ANCIENT_BATTLEFIELD_RANK_AWARD.extensions = {}

ancient_battlefield_rank_award = protobuf.Message(ANCIENT_BATTLEFIELD_RANK_AWARD)

