--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.wing_models_pb')

WING_MODELS = protobuf.Descriptor();
WING_MODELS___ID_FIELD = protobuf.FieldDescriptor();
WING_MODELS___TYPE_FIELD = protobuf.FieldDescriptor();
WING_MODELS___STAR_FIELD = protobuf.FieldDescriptor();
WING_MODELS___WING_MODELS_PROPORTION_FIELD = protobuf.FieldDescriptor();

WING_MODELS___ID_FIELD.name = "__id"
WING_MODELS___ID_FIELD.full_name = ".wing_models.__id"
WING_MODELS___ID_FIELD.number = 1
WING_MODELS___ID_FIELD.index = 0
WING_MODELS___ID_FIELD.label = 2
WING_MODELS___ID_FIELD.has_default_value = false
WING_MODELS___ID_FIELD.default_value = 0
WING_MODELS___ID_FIELD.type = 5
WING_MODELS___ID_FIELD.cpp_type = 1

WING_MODELS___TYPE_FIELD.name = "__type"
WING_MODELS___TYPE_FIELD.full_name = ".wing_models.__type"
WING_MODELS___TYPE_FIELD.number = 2
WING_MODELS___TYPE_FIELD.index = 1
WING_MODELS___TYPE_FIELD.label = 2
WING_MODELS___TYPE_FIELD.has_default_value = false
WING_MODELS___TYPE_FIELD.default_value = 0
WING_MODELS___TYPE_FIELD.type = 5
WING_MODELS___TYPE_FIELD.cpp_type = 1

WING_MODELS___STAR_FIELD.name = "__star"
WING_MODELS___STAR_FIELD.full_name = ".wing_models.__star"
WING_MODELS___STAR_FIELD.number = 3
WING_MODELS___STAR_FIELD.index = 2
WING_MODELS___STAR_FIELD.label = 2
WING_MODELS___STAR_FIELD.has_default_value = false
WING_MODELS___STAR_FIELD.default_value = 0
WING_MODELS___STAR_FIELD.type = 5
WING_MODELS___STAR_FIELD.cpp_type = 1

WING_MODELS___WING_MODELS_PROPORTION_FIELD.name = "__wing_models_proportion"
WING_MODELS___WING_MODELS_PROPORTION_FIELD.full_name = ".wing_models.__wing_models_proportion"
WING_MODELS___WING_MODELS_PROPORTION_FIELD.number = 7
WING_MODELS___WING_MODELS_PROPORTION_FIELD.index = 3
WING_MODELS___WING_MODELS_PROPORTION_FIELD.label = 2
WING_MODELS___WING_MODELS_PROPORTION_FIELD.has_default_value = false
WING_MODELS___WING_MODELS_PROPORTION_FIELD.default_value = ""
WING_MODELS___WING_MODELS_PROPORTION_FIELD.type = 9
WING_MODELS___WING_MODELS_PROPORTION_FIELD.cpp_type = 9

WING_MODELS.name = "wing_models"
WING_MODELS.full_name = ".wing_models"
WING_MODELS.nested_types = {}
WING_MODELS.enum_types = {}
WING_MODELS.fields = {WING_MODELS___ID_FIELD, WING_MODELS___TYPE_FIELD, WING_MODELS___STAR_FIELD, WING_MODELS___WING_MODELS_PROPORTION_FIELD}
WING_MODELS.is_extendable = false
WING_MODELS.extensions = {}

wing_models = protobuf.Message(WING_MODELS)

