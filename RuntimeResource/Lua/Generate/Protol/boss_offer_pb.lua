--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.boss_offer_pb')

BOSS_OFFER = protobuf.Descriptor();
BOSS_OFFER___ID_FIELD = protobuf.FieldDescriptor();
BOSS_OFFER___ICON_FIELD = protobuf.FieldDescriptor();
BOSS_OFFER___TARGETS_FIELD = protobuf.FieldDescriptor();
BOSS_OFFER___REWARD_FIELD = protobuf.FieldDescriptor();
BOSS_OFFER___NAME_FIELD = protobuf.FieldDescriptor();

BOSS_OFFER___ID_FIELD.name = "__id"
BOSS_OFFER___ID_FIELD.full_name = ".boss_offer.__id"
BOSS_OFFER___ID_FIELD.number = 1
BOSS_OFFER___ID_FIELD.index = 0
BOSS_OFFER___ID_FIELD.label = 2
BOSS_OFFER___ID_FIELD.has_default_value = false
BOSS_OFFER___ID_FIELD.default_value = 0
BOSS_OFFER___ID_FIELD.type = 5
BOSS_OFFER___ID_FIELD.cpp_type = 1

BOSS_OFFER___ICON_FIELD.name = "__icon"
BOSS_OFFER___ICON_FIELD.full_name = ".boss_offer.__icon"
BOSS_OFFER___ICON_FIELD.number = 2
BOSS_OFFER___ICON_FIELD.index = 1
BOSS_OFFER___ICON_FIELD.label = 2
BOSS_OFFER___ICON_FIELD.has_default_value = false
BOSS_OFFER___ICON_FIELD.default_value = ""
BOSS_OFFER___ICON_FIELD.type = 9
BOSS_OFFER___ICON_FIELD.cpp_type = 9

BOSS_OFFER___TARGETS_FIELD.name = "__targets"
BOSS_OFFER___TARGETS_FIELD.full_name = ".boss_offer.__targets"
BOSS_OFFER___TARGETS_FIELD.number = 3
BOSS_OFFER___TARGETS_FIELD.index = 2
BOSS_OFFER___TARGETS_FIELD.label = 2
BOSS_OFFER___TARGETS_FIELD.has_default_value = false
BOSS_OFFER___TARGETS_FIELD.default_value = ""
BOSS_OFFER___TARGETS_FIELD.type = 9
BOSS_OFFER___TARGETS_FIELD.cpp_type = 9

BOSS_OFFER___REWARD_FIELD.name = "__reward"
BOSS_OFFER___REWARD_FIELD.full_name = ".boss_offer.__reward"
BOSS_OFFER___REWARD_FIELD.number = 4
BOSS_OFFER___REWARD_FIELD.index = 3
BOSS_OFFER___REWARD_FIELD.label = 2
BOSS_OFFER___REWARD_FIELD.has_default_value = false
BOSS_OFFER___REWARD_FIELD.default_value = ""
BOSS_OFFER___REWARD_FIELD.type = 9
BOSS_OFFER___REWARD_FIELD.cpp_type = 9

BOSS_OFFER___NAME_FIELD.name = "__name"
BOSS_OFFER___NAME_FIELD.full_name = ".boss_offer.__name"
BOSS_OFFER___NAME_FIELD.number = 5
BOSS_OFFER___NAME_FIELD.index = 4
BOSS_OFFER___NAME_FIELD.label = 2
BOSS_OFFER___NAME_FIELD.has_default_value = false
BOSS_OFFER___NAME_FIELD.default_value = 0
BOSS_OFFER___NAME_FIELD.type = 5
BOSS_OFFER___NAME_FIELD.cpp_type = 1

BOSS_OFFER.name = "boss_offer"
BOSS_OFFER.full_name = ".boss_offer"
BOSS_OFFER.nested_types = {}
BOSS_OFFER.enum_types = {}
BOSS_OFFER.fields = {BOSS_OFFER___ID_FIELD, BOSS_OFFER___ICON_FIELD, BOSS_OFFER___TARGETS_FIELD, BOSS_OFFER___REWARD_FIELD, BOSS_OFFER___NAME_FIELD}
BOSS_OFFER.is_extendable = false
BOSS_OFFER.extensions = {}

boss_offer = protobuf.Message(BOSS_OFFER)

