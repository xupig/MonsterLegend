--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.open_time_pb')

OPEN_TIME = protobuf.Descriptor();
OPEN_TIME___ID_FIELD = protobuf.FieldDescriptor();
OPEN_TIME___OPEN_DATA_FIELD = protobuf.FieldDescriptor();
OPEN_TIME___OPEN_DAY_FIELD = protobuf.FieldDescriptor();
OPEN_TIME___OPEN_TIME_FIELD = protobuf.FieldDescriptor();
OPEN_TIME___DURATION_TIME_FIELD = protobuf.FieldDescriptor();

OPEN_TIME___ID_FIELD.name = "__id"
OPEN_TIME___ID_FIELD.full_name = ".open_time.__id"
OPEN_TIME___ID_FIELD.number = 1
OPEN_TIME___ID_FIELD.index = 0
OPEN_TIME___ID_FIELD.label = 2
OPEN_TIME___ID_FIELD.has_default_value = false
OPEN_TIME___ID_FIELD.default_value = 0
OPEN_TIME___ID_FIELD.type = 5
OPEN_TIME___ID_FIELD.cpp_type = 1

OPEN_TIME___OPEN_DATA_FIELD.name = "__open_data"
OPEN_TIME___OPEN_DATA_FIELD.full_name = ".open_time.__open_data"
OPEN_TIME___OPEN_DATA_FIELD.number = 2
OPEN_TIME___OPEN_DATA_FIELD.index = 1
OPEN_TIME___OPEN_DATA_FIELD.label = 2
OPEN_TIME___OPEN_DATA_FIELD.has_default_value = false
OPEN_TIME___OPEN_DATA_FIELD.default_value = ""
OPEN_TIME___OPEN_DATA_FIELD.type = 9
OPEN_TIME___OPEN_DATA_FIELD.cpp_type = 9

OPEN_TIME___OPEN_DAY_FIELD.name = "__open_day"
OPEN_TIME___OPEN_DAY_FIELD.full_name = ".open_time.__open_day"
OPEN_TIME___OPEN_DAY_FIELD.number = 3
OPEN_TIME___OPEN_DAY_FIELD.index = 2
OPEN_TIME___OPEN_DAY_FIELD.label = 2
OPEN_TIME___OPEN_DAY_FIELD.has_default_value = false
OPEN_TIME___OPEN_DAY_FIELD.default_value = ""
OPEN_TIME___OPEN_DAY_FIELD.type = 9
OPEN_TIME___OPEN_DAY_FIELD.cpp_type = 9

OPEN_TIME___OPEN_TIME_FIELD.name = "__open_time"
OPEN_TIME___OPEN_TIME_FIELD.full_name = ".open_time.__open_time"
OPEN_TIME___OPEN_TIME_FIELD.number = 4
OPEN_TIME___OPEN_TIME_FIELD.index = 3
OPEN_TIME___OPEN_TIME_FIELD.label = 2
OPEN_TIME___OPEN_TIME_FIELD.has_default_value = false
OPEN_TIME___OPEN_TIME_FIELD.default_value = ""
OPEN_TIME___OPEN_TIME_FIELD.type = 9
OPEN_TIME___OPEN_TIME_FIELD.cpp_type = 9

OPEN_TIME___DURATION_TIME_FIELD.name = "__duration_time"
OPEN_TIME___DURATION_TIME_FIELD.full_name = ".open_time.__duration_time"
OPEN_TIME___DURATION_TIME_FIELD.number = 5
OPEN_TIME___DURATION_TIME_FIELD.index = 4
OPEN_TIME___DURATION_TIME_FIELD.label = 2
OPEN_TIME___DURATION_TIME_FIELD.has_default_value = false
OPEN_TIME___DURATION_TIME_FIELD.default_value = 0
OPEN_TIME___DURATION_TIME_FIELD.type = 5
OPEN_TIME___DURATION_TIME_FIELD.cpp_type = 1

OPEN_TIME.name = "open_time"
OPEN_TIME.full_name = ".open_time"
OPEN_TIME.nested_types = {}
OPEN_TIME.enum_types = {}
OPEN_TIME.fields = {OPEN_TIME___ID_FIELD, OPEN_TIME___OPEN_DATA_FIELD, OPEN_TIME___OPEN_DAY_FIELD, OPEN_TIME___OPEN_TIME_FIELD, OPEN_TIME___DURATION_TIME_FIELD}
OPEN_TIME.is_extendable = false
OPEN_TIME.extensions = {}

open_time = protobuf.Message(OPEN_TIME)

