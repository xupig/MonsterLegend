--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.fashion_pb')

FASHION = protobuf.Descriptor();
FASHION___ID_FIELD = protobuf.FieldDescriptor();
FASHION___TYPE_FIELD = protobuf.FieldDescriptor();
FASHION___SHOW_TYPE_FIELD = protobuf.FieldDescriptor();
FASHION___MODE_VOCATION_FIELD = protobuf.FieldDescriptor();
FASHION___RESOLVE_FIELD = protobuf.FieldDescriptor();
FASHION___MAX_STAR_FIELD = protobuf.FieldDescriptor();
FASHION___HEAD_VOCATION_FIELD = protobuf.FieldDescriptor();
FASHION___ORDER_FIELD = protobuf.FieldDescriptor();
FASHION___VOCATION_FIELD = protobuf.FieldDescriptor();
FASHION___LEVEL_SHOW_FIELD = protobuf.FieldDescriptor();

FASHION___ID_FIELD.name = "__id"
FASHION___ID_FIELD.full_name = ".fashion.__id"
FASHION___ID_FIELD.number = 1
FASHION___ID_FIELD.index = 0
FASHION___ID_FIELD.label = 2
FASHION___ID_FIELD.has_default_value = false
FASHION___ID_FIELD.default_value = 0
FASHION___ID_FIELD.type = 5
FASHION___ID_FIELD.cpp_type = 1

FASHION___TYPE_FIELD.name = "__type"
FASHION___TYPE_FIELD.full_name = ".fashion.__type"
FASHION___TYPE_FIELD.number = 3
FASHION___TYPE_FIELD.index = 1
FASHION___TYPE_FIELD.label = 2
FASHION___TYPE_FIELD.has_default_value = false
FASHION___TYPE_FIELD.default_value = 0
FASHION___TYPE_FIELD.type = 5
FASHION___TYPE_FIELD.cpp_type = 1

FASHION___SHOW_TYPE_FIELD.name = "__show_type"
FASHION___SHOW_TYPE_FIELD.full_name = ".fashion.__show_type"
FASHION___SHOW_TYPE_FIELD.number = 5
FASHION___SHOW_TYPE_FIELD.index = 2
FASHION___SHOW_TYPE_FIELD.label = 2
FASHION___SHOW_TYPE_FIELD.has_default_value = false
FASHION___SHOW_TYPE_FIELD.default_value = 0
FASHION___SHOW_TYPE_FIELD.type = 5
FASHION___SHOW_TYPE_FIELD.cpp_type = 1

FASHION___MODE_VOCATION_FIELD.name = "__mode_vocation"
FASHION___MODE_VOCATION_FIELD.full_name = ".fashion.__mode_vocation"
FASHION___MODE_VOCATION_FIELD.number = 6
FASHION___MODE_VOCATION_FIELD.index = 3
FASHION___MODE_VOCATION_FIELD.label = 2
FASHION___MODE_VOCATION_FIELD.has_default_value = false
FASHION___MODE_VOCATION_FIELD.default_value = 0
FASHION___MODE_VOCATION_FIELD.type = 5
FASHION___MODE_VOCATION_FIELD.cpp_type = 1

FASHION___RESOLVE_FIELD.name = "__resolve"
FASHION___RESOLVE_FIELD.full_name = ".fashion.__resolve"
FASHION___RESOLVE_FIELD.number = 7
FASHION___RESOLVE_FIELD.index = 4
FASHION___RESOLVE_FIELD.label = 2
FASHION___RESOLVE_FIELD.has_default_value = false
FASHION___RESOLVE_FIELD.default_value = ""
FASHION___RESOLVE_FIELD.type = 9
FASHION___RESOLVE_FIELD.cpp_type = 9

FASHION___MAX_STAR_FIELD.name = "__max_star"
FASHION___MAX_STAR_FIELD.full_name = ".fashion.__max_star"
FASHION___MAX_STAR_FIELD.number = 8
FASHION___MAX_STAR_FIELD.index = 5
FASHION___MAX_STAR_FIELD.label = 2
FASHION___MAX_STAR_FIELD.has_default_value = false
FASHION___MAX_STAR_FIELD.default_value = 0
FASHION___MAX_STAR_FIELD.type = 5
FASHION___MAX_STAR_FIELD.cpp_type = 1

FASHION___HEAD_VOCATION_FIELD.name = "__head_vocation"
FASHION___HEAD_VOCATION_FIELD.full_name = ".fashion.__head_vocation"
FASHION___HEAD_VOCATION_FIELD.number = 9
FASHION___HEAD_VOCATION_FIELD.index = 6
FASHION___HEAD_VOCATION_FIELD.label = 2
FASHION___HEAD_VOCATION_FIELD.has_default_value = false
FASHION___HEAD_VOCATION_FIELD.default_value = 0
FASHION___HEAD_VOCATION_FIELD.type = 5
FASHION___HEAD_VOCATION_FIELD.cpp_type = 1

FASHION___ORDER_FIELD.name = "__order"
FASHION___ORDER_FIELD.full_name = ".fashion.__order"
FASHION___ORDER_FIELD.number = 10
FASHION___ORDER_FIELD.index = 7
FASHION___ORDER_FIELD.label = 2
FASHION___ORDER_FIELD.has_default_value = false
FASHION___ORDER_FIELD.default_value = 0
FASHION___ORDER_FIELD.type = 5
FASHION___ORDER_FIELD.cpp_type = 1

FASHION___VOCATION_FIELD.name = "__vocation"
FASHION___VOCATION_FIELD.full_name = ".fashion.__vocation"
FASHION___VOCATION_FIELD.number = 11
FASHION___VOCATION_FIELD.index = 8
FASHION___VOCATION_FIELD.label = 2
FASHION___VOCATION_FIELD.has_default_value = false
FASHION___VOCATION_FIELD.default_value = ""
FASHION___VOCATION_FIELD.type = 9
FASHION___VOCATION_FIELD.cpp_type = 9

FASHION___LEVEL_SHOW_FIELD.name = "__level_show"
FASHION___LEVEL_SHOW_FIELD.full_name = ".fashion.__level_show"
FASHION___LEVEL_SHOW_FIELD.number = 12
FASHION___LEVEL_SHOW_FIELD.index = 9
FASHION___LEVEL_SHOW_FIELD.label = 2
FASHION___LEVEL_SHOW_FIELD.has_default_value = false
FASHION___LEVEL_SHOW_FIELD.default_value = 0
FASHION___LEVEL_SHOW_FIELD.type = 5
FASHION___LEVEL_SHOW_FIELD.cpp_type = 1

FASHION.name = "fashion"
FASHION.full_name = ".fashion"
FASHION.nested_types = {}
FASHION.enum_types = {}
FASHION.fields = {FASHION___ID_FIELD, FASHION___TYPE_FIELD, FASHION___SHOW_TYPE_FIELD, FASHION___MODE_VOCATION_FIELD, FASHION___RESOLVE_FIELD, FASHION___MAX_STAR_FIELD, FASHION___HEAD_VOCATION_FIELD, FASHION___ORDER_FIELD, FASHION___VOCATION_FIELD, FASHION___LEVEL_SHOW_FIELD}
FASHION.is_extendable = false
FASHION.extensions = {}

fashion = protobuf.Message(FASHION)

