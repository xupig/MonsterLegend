--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.activity_sum_deposit_pb')

ACTIVITY_SUM_DEPOSIT = protobuf.Descriptor();
ACTIVITY_SUM_DEPOSIT___ID_FIELD = protobuf.FieldDescriptor();
ACTIVITY_SUM_DEPOSIT___COST_FIELD = protobuf.FieldDescriptor();
ACTIVITY_SUM_DEPOSIT___REWARD_FIELD = protobuf.FieldDescriptor();

ACTIVITY_SUM_DEPOSIT___ID_FIELD.name = "__id"
ACTIVITY_SUM_DEPOSIT___ID_FIELD.full_name = ".activity_sum_deposit.__id"
ACTIVITY_SUM_DEPOSIT___ID_FIELD.number = 1
ACTIVITY_SUM_DEPOSIT___ID_FIELD.index = 0
ACTIVITY_SUM_DEPOSIT___ID_FIELD.label = 2
ACTIVITY_SUM_DEPOSIT___ID_FIELD.has_default_value = false
ACTIVITY_SUM_DEPOSIT___ID_FIELD.default_value = 0
ACTIVITY_SUM_DEPOSIT___ID_FIELD.type = 5
ACTIVITY_SUM_DEPOSIT___ID_FIELD.cpp_type = 1

ACTIVITY_SUM_DEPOSIT___COST_FIELD.name = "__cost"
ACTIVITY_SUM_DEPOSIT___COST_FIELD.full_name = ".activity_sum_deposit.__cost"
ACTIVITY_SUM_DEPOSIT___COST_FIELD.number = 2
ACTIVITY_SUM_DEPOSIT___COST_FIELD.index = 1
ACTIVITY_SUM_DEPOSIT___COST_FIELD.label = 2
ACTIVITY_SUM_DEPOSIT___COST_FIELD.has_default_value = false
ACTIVITY_SUM_DEPOSIT___COST_FIELD.default_value = 0
ACTIVITY_SUM_DEPOSIT___COST_FIELD.type = 5
ACTIVITY_SUM_DEPOSIT___COST_FIELD.cpp_type = 1

ACTIVITY_SUM_DEPOSIT___REWARD_FIELD.name = "__reward"
ACTIVITY_SUM_DEPOSIT___REWARD_FIELD.full_name = ".activity_sum_deposit.__reward"
ACTIVITY_SUM_DEPOSIT___REWARD_FIELD.number = 3
ACTIVITY_SUM_DEPOSIT___REWARD_FIELD.index = 2
ACTIVITY_SUM_DEPOSIT___REWARD_FIELD.label = 2
ACTIVITY_SUM_DEPOSIT___REWARD_FIELD.has_default_value = false
ACTIVITY_SUM_DEPOSIT___REWARD_FIELD.default_value = ""
ACTIVITY_SUM_DEPOSIT___REWARD_FIELD.type = 9
ACTIVITY_SUM_DEPOSIT___REWARD_FIELD.cpp_type = 9

ACTIVITY_SUM_DEPOSIT.name = "activity_sum_deposit"
ACTIVITY_SUM_DEPOSIT.full_name = ".activity_sum_deposit"
ACTIVITY_SUM_DEPOSIT.nested_types = {}
ACTIVITY_SUM_DEPOSIT.enum_types = {}
ACTIVITY_SUM_DEPOSIT.fields = {ACTIVITY_SUM_DEPOSIT___ID_FIELD, ACTIVITY_SUM_DEPOSIT___COST_FIELD, ACTIVITY_SUM_DEPOSIT___REWARD_FIELD}
ACTIVITY_SUM_DEPOSIT.is_extendable = false
ACTIVITY_SUM_DEPOSIT.extensions = {}

activity_sum_deposit = protobuf.Message(ACTIVITY_SUM_DEPOSIT)

