--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.marry_ring_pb')

MARRY_RING = protobuf.Descriptor();
MARRY_RING___ID_FIELD = protobuf.FieldDescriptor();
MARRY_RING___LEVEL_FIELD = protobuf.FieldDescriptor();
MARRY_RING___EXP_FIELD = protobuf.FieldDescriptor();
MARRY_RING___ATTRI_BASE_FIELD = protobuf.FieldDescriptor();
MARRY_RING___ATTRI_MARRY_FIELD = protobuf.FieldDescriptor();

MARRY_RING___ID_FIELD.name = "__id"
MARRY_RING___ID_FIELD.full_name = ".marry_ring.__id"
MARRY_RING___ID_FIELD.number = 1
MARRY_RING___ID_FIELD.index = 0
MARRY_RING___ID_FIELD.label = 2
MARRY_RING___ID_FIELD.has_default_value = false
MARRY_RING___ID_FIELD.default_value = 0
MARRY_RING___ID_FIELD.type = 5
MARRY_RING___ID_FIELD.cpp_type = 1

MARRY_RING___LEVEL_FIELD.name = "__level"
MARRY_RING___LEVEL_FIELD.full_name = ".marry_ring.__level"
MARRY_RING___LEVEL_FIELD.number = 2
MARRY_RING___LEVEL_FIELD.index = 1
MARRY_RING___LEVEL_FIELD.label = 2
MARRY_RING___LEVEL_FIELD.has_default_value = false
MARRY_RING___LEVEL_FIELD.default_value = 0
MARRY_RING___LEVEL_FIELD.type = 5
MARRY_RING___LEVEL_FIELD.cpp_type = 1

MARRY_RING___EXP_FIELD.name = "__exp"
MARRY_RING___EXP_FIELD.full_name = ".marry_ring.__exp"
MARRY_RING___EXP_FIELD.number = 3
MARRY_RING___EXP_FIELD.index = 2
MARRY_RING___EXP_FIELD.label = 2
MARRY_RING___EXP_FIELD.has_default_value = false
MARRY_RING___EXP_FIELD.default_value = 0
MARRY_RING___EXP_FIELD.type = 5
MARRY_RING___EXP_FIELD.cpp_type = 1

MARRY_RING___ATTRI_BASE_FIELD.name = "__attri_base"
MARRY_RING___ATTRI_BASE_FIELD.full_name = ".marry_ring.__attri_base"
MARRY_RING___ATTRI_BASE_FIELD.number = 4
MARRY_RING___ATTRI_BASE_FIELD.index = 3
MARRY_RING___ATTRI_BASE_FIELD.label = 2
MARRY_RING___ATTRI_BASE_FIELD.has_default_value = false
MARRY_RING___ATTRI_BASE_FIELD.default_value = ""
MARRY_RING___ATTRI_BASE_FIELD.type = 9
MARRY_RING___ATTRI_BASE_FIELD.cpp_type = 9

MARRY_RING___ATTRI_MARRY_FIELD.name = "__attri_marry"
MARRY_RING___ATTRI_MARRY_FIELD.full_name = ".marry_ring.__attri_marry"
MARRY_RING___ATTRI_MARRY_FIELD.number = 5
MARRY_RING___ATTRI_MARRY_FIELD.index = 4
MARRY_RING___ATTRI_MARRY_FIELD.label = 2
MARRY_RING___ATTRI_MARRY_FIELD.has_default_value = false
MARRY_RING___ATTRI_MARRY_FIELD.default_value = ""
MARRY_RING___ATTRI_MARRY_FIELD.type = 9
MARRY_RING___ATTRI_MARRY_FIELD.cpp_type = 9

MARRY_RING.name = "marry_ring"
MARRY_RING.full_name = ".marry_ring"
MARRY_RING.nested_types = {}
MARRY_RING.enum_types = {}
MARRY_RING.fields = {MARRY_RING___ID_FIELD, MARRY_RING___LEVEL_FIELD, MARRY_RING___EXP_FIELD, MARRY_RING___ATTRI_BASE_FIELD, MARRY_RING___ATTRI_MARRY_FIELD}
MARRY_RING.is_extendable = false
MARRY_RING.extensions = {}

marry_ring = protobuf.Message(MARRY_RING)

