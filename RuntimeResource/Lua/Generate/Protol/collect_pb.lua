--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.collect_pb')

COLLECT = protobuf.Descriptor();
COLLECT___ID_FIELD = protobuf.FieldDescriptor();
COLLECT___TYPE_FIELD = protobuf.FieldDescriptor();
COLLECT___MODEL_ID_FIELD = protobuf.FieldDescriptor();
COLLECT___COLLECT_TIME_FIELD = protobuf.FieldDescriptor();
COLLECT___COLLECTING_TIPS_FIELD = protobuf.FieldDescriptor();
COLLECT___COLLECTED_TIPS_FIELD = protobuf.FieldDescriptor();
COLLECT___LEVEL_FIELD = protobuf.FieldDescriptor();
COLLECT___NAME_FIELD = protobuf.FieldDescriptor();
COLLECT___COLLECTED_SCALE_FIELD = protobuf.FieldDescriptor();
COLLECT___SKILL_TIME_FIELD = protobuf.FieldDescriptor();

COLLECT___ID_FIELD.name = "__id"
COLLECT___ID_FIELD.full_name = ".collect.__id"
COLLECT___ID_FIELD.number = 1
COLLECT___ID_FIELD.index = 0
COLLECT___ID_FIELD.label = 2
COLLECT___ID_FIELD.has_default_value = false
COLLECT___ID_FIELD.default_value = 0
COLLECT___ID_FIELD.type = 5
COLLECT___ID_FIELD.cpp_type = 1

COLLECT___TYPE_FIELD.name = "__type"
COLLECT___TYPE_FIELD.full_name = ".collect.__type"
COLLECT___TYPE_FIELD.number = 2
COLLECT___TYPE_FIELD.index = 1
COLLECT___TYPE_FIELD.label = 2
COLLECT___TYPE_FIELD.has_default_value = false
COLLECT___TYPE_FIELD.default_value = 0
COLLECT___TYPE_FIELD.type = 5
COLLECT___TYPE_FIELD.cpp_type = 1

COLLECT___MODEL_ID_FIELD.name = "__model_id"
COLLECT___MODEL_ID_FIELD.full_name = ".collect.__model_id"
COLLECT___MODEL_ID_FIELD.number = 3
COLLECT___MODEL_ID_FIELD.index = 2
COLLECT___MODEL_ID_FIELD.label = 2
COLLECT___MODEL_ID_FIELD.has_default_value = false
COLLECT___MODEL_ID_FIELD.default_value = 0
COLLECT___MODEL_ID_FIELD.type = 5
COLLECT___MODEL_ID_FIELD.cpp_type = 1

COLLECT___COLLECT_TIME_FIELD.name = "__collect_time"
COLLECT___COLLECT_TIME_FIELD.full_name = ".collect.__collect_time"
COLLECT___COLLECT_TIME_FIELD.number = 4
COLLECT___COLLECT_TIME_FIELD.index = 3
COLLECT___COLLECT_TIME_FIELD.label = 2
COLLECT___COLLECT_TIME_FIELD.has_default_value = false
COLLECT___COLLECT_TIME_FIELD.default_value = 0
COLLECT___COLLECT_TIME_FIELD.type = 5
COLLECT___COLLECT_TIME_FIELD.cpp_type = 1

COLLECT___COLLECTING_TIPS_FIELD.name = "__collecting_tips"
COLLECT___COLLECTING_TIPS_FIELD.full_name = ".collect.__collecting_tips"
COLLECT___COLLECTING_TIPS_FIELD.number = 5
COLLECT___COLLECTING_TIPS_FIELD.index = 4
COLLECT___COLLECTING_TIPS_FIELD.label = 2
COLLECT___COLLECTING_TIPS_FIELD.has_default_value = false
COLLECT___COLLECTING_TIPS_FIELD.default_value = 0
COLLECT___COLLECTING_TIPS_FIELD.type = 5
COLLECT___COLLECTING_TIPS_FIELD.cpp_type = 1

COLLECT___COLLECTED_TIPS_FIELD.name = "__collected_tips"
COLLECT___COLLECTED_TIPS_FIELD.full_name = ".collect.__collected_tips"
COLLECT___COLLECTED_TIPS_FIELD.number = 6
COLLECT___COLLECTED_TIPS_FIELD.index = 5
COLLECT___COLLECTED_TIPS_FIELD.label = 2
COLLECT___COLLECTED_TIPS_FIELD.has_default_value = false
COLLECT___COLLECTED_TIPS_FIELD.default_value = 0
COLLECT___COLLECTED_TIPS_FIELD.type = 5
COLLECT___COLLECTED_TIPS_FIELD.cpp_type = 1

COLLECT___LEVEL_FIELD.name = "__level"
COLLECT___LEVEL_FIELD.full_name = ".collect.__level"
COLLECT___LEVEL_FIELD.number = 7
COLLECT___LEVEL_FIELD.index = 6
COLLECT___LEVEL_FIELD.label = 2
COLLECT___LEVEL_FIELD.has_default_value = false
COLLECT___LEVEL_FIELD.default_value = 0
COLLECT___LEVEL_FIELD.type = 5
COLLECT___LEVEL_FIELD.cpp_type = 1

COLLECT___NAME_FIELD.name = "__name"
COLLECT___NAME_FIELD.full_name = ".collect.__name"
COLLECT___NAME_FIELD.number = 8
COLLECT___NAME_FIELD.index = 7
COLLECT___NAME_FIELD.label = 2
COLLECT___NAME_FIELD.has_default_value = false
COLLECT___NAME_FIELD.default_value = 0
COLLECT___NAME_FIELD.type = 5
COLLECT___NAME_FIELD.cpp_type = 1

COLLECT___COLLECTED_SCALE_FIELD.name = "__collected_scale"
COLLECT___COLLECTED_SCALE_FIELD.full_name = ".collect.__collected_scale"
COLLECT___COLLECTED_SCALE_FIELD.number = 9
COLLECT___COLLECTED_SCALE_FIELD.index = 8
COLLECT___COLLECTED_SCALE_FIELD.label = 2
COLLECT___COLLECTED_SCALE_FIELD.has_default_value = false
COLLECT___COLLECTED_SCALE_FIELD.default_value = 0
COLLECT___COLLECTED_SCALE_FIELD.type = 5
COLLECT___COLLECTED_SCALE_FIELD.cpp_type = 1

COLLECT___SKILL_TIME_FIELD.name = "__skill_time"
COLLECT___SKILL_TIME_FIELD.full_name = ".collect.__skill_time"
COLLECT___SKILL_TIME_FIELD.number = 10
COLLECT___SKILL_TIME_FIELD.index = 9
COLLECT___SKILL_TIME_FIELD.label = 2
COLLECT___SKILL_TIME_FIELD.has_default_value = false
COLLECT___SKILL_TIME_FIELD.default_value = ""
COLLECT___SKILL_TIME_FIELD.type = 9
COLLECT___SKILL_TIME_FIELD.cpp_type = 9

COLLECT.name = "collect"
COLLECT.full_name = ".collect"
COLLECT.nested_types = {}
COLLECT.enum_types = {}
COLLECT.fields = {COLLECT___ID_FIELD, COLLECT___TYPE_FIELD, COLLECT___MODEL_ID_FIELD, COLLECT___COLLECT_TIME_FIELD, COLLECT___COLLECTING_TIPS_FIELD, COLLECT___COLLECTED_TIPS_FIELD, COLLECT___LEVEL_FIELD, COLLECT___NAME_FIELD, COLLECT___COLLECTED_SCALE_FIELD, COLLECT___SKILL_TIME_FIELD}
COLLECT.is_extendable = false
COLLECT.extensions = {}

collect = protobuf.Message(COLLECT)

