--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.daily_task_pb')

DAILY_TASK = protobuf.Descriptor();
DAILY_TASK___ID_FIELD = protobuf.FieldDescriptor();
DAILY_TASK___NAME_FIELD = protobuf.FieldDescriptor();
DAILY_TASK___DESC_FIELD = protobuf.FieldDescriptor();
DAILY_TASK___ICON_FIELD = protobuf.FieldDescriptor();
DAILY_TASK___GROUP_FIELD = protobuf.FieldDescriptor();
DAILY_TASK___LEVEL_FIELD = protobuf.FieldDescriptor();
DAILY_TASK___WORLD_LEVEL_FIELD = protobuf.FieldDescriptor();
DAILY_TASK___TARGETS_FIELD = protobuf.FieldDescriptor();
DAILY_TASK___ACTIVE_POINT_FIELD = protobuf.FieldDescriptor();
DAILY_TASK___REWARD_FIELD = protobuf.FieldDescriptor();

DAILY_TASK___ID_FIELD.name = "__id"
DAILY_TASK___ID_FIELD.full_name = ".daily_task.__id"
DAILY_TASK___ID_FIELD.number = 1
DAILY_TASK___ID_FIELD.index = 0
DAILY_TASK___ID_FIELD.label = 2
DAILY_TASK___ID_FIELD.has_default_value = false
DAILY_TASK___ID_FIELD.default_value = 0
DAILY_TASK___ID_FIELD.type = 5
DAILY_TASK___ID_FIELD.cpp_type = 1

DAILY_TASK___NAME_FIELD.name = "__name"
DAILY_TASK___NAME_FIELD.full_name = ".daily_task.__name"
DAILY_TASK___NAME_FIELD.number = 2
DAILY_TASK___NAME_FIELD.index = 1
DAILY_TASK___NAME_FIELD.label = 2
DAILY_TASK___NAME_FIELD.has_default_value = false
DAILY_TASK___NAME_FIELD.default_value = 0
DAILY_TASK___NAME_FIELD.type = 5
DAILY_TASK___NAME_FIELD.cpp_type = 1

DAILY_TASK___DESC_FIELD.name = "__desc"
DAILY_TASK___DESC_FIELD.full_name = ".daily_task.__desc"
DAILY_TASK___DESC_FIELD.number = 3
DAILY_TASK___DESC_FIELD.index = 2
DAILY_TASK___DESC_FIELD.label = 2
DAILY_TASK___DESC_FIELD.has_default_value = false
DAILY_TASK___DESC_FIELD.default_value = 0
DAILY_TASK___DESC_FIELD.type = 5
DAILY_TASK___DESC_FIELD.cpp_type = 1

DAILY_TASK___ICON_FIELD.name = "__icon"
DAILY_TASK___ICON_FIELD.full_name = ".daily_task.__icon"
DAILY_TASK___ICON_FIELD.number = 4
DAILY_TASK___ICON_FIELD.index = 3
DAILY_TASK___ICON_FIELD.label = 2
DAILY_TASK___ICON_FIELD.has_default_value = false
DAILY_TASK___ICON_FIELD.default_value = 0
DAILY_TASK___ICON_FIELD.type = 5
DAILY_TASK___ICON_FIELD.cpp_type = 1

DAILY_TASK___GROUP_FIELD.name = "__group"
DAILY_TASK___GROUP_FIELD.full_name = ".daily_task.__group"
DAILY_TASK___GROUP_FIELD.number = 5
DAILY_TASK___GROUP_FIELD.index = 4
DAILY_TASK___GROUP_FIELD.label = 2
DAILY_TASK___GROUP_FIELD.has_default_value = false
DAILY_TASK___GROUP_FIELD.default_value = 0
DAILY_TASK___GROUP_FIELD.type = 5
DAILY_TASK___GROUP_FIELD.cpp_type = 1

DAILY_TASK___LEVEL_FIELD.name = "__level"
DAILY_TASK___LEVEL_FIELD.full_name = ".daily_task.__level"
DAILY_TASK___LEVEL_FIELD.number = 6
DAILY_TASK___LEVEL_FIELD.index = 5
DAILY_TASK___LEVEL_FIELD.label = 2
DAILY_TASK___LEVEL_FIELD.has_default_value = false
DAILY_TASK___LEVEL_FIELD.default_value = 0
DAILY_TASK___LEVEL_FIELD.type = 5
DAILY_TASK___LEVEL_FIELD.cpp_type = 1

DAILY_TASK___WORLD_LEVEL_FIELD.name = "__world_level"
DAILY_TASK___WORLD_LEVEL_FIELD.full_name = ".daily_task.__world_level"
DAILY_TASK___WORLD_LEVEL_FIELD.number = 7
DAILY_TASK___WORLD_LEVEL_FIELD.index = 6
DAILY_TASK___WORLD_LEVEL_FIELD.label = 2
DAILY_TASK___WORLD_LEVEL_FIELD.has_default_value = false
DAILY_TASK___WORLD_LEVEL_FIELD.default_value = 0
DAILY_TASK___WORLD_LEVEL_FIELD.type = 5
DAILY_TASK___WORLD_LEVEL_FIELD.cpp_type = 1

DAILY_TASK___TARGETS_FIELD.name = "__targets"
DAILY_TASK___TARGETS_FIELD.full_name = ".daily_task.__targets"
DAILY_TASK___TARGETS_FIELD.number = 8
DAILY_TASK___TARGETS_FIELD.index = 7
DAILY_TASK___TARGETS_FIELD.label = 2
DAILY_TASK___TARGETS_FIELD.has_default_value = false
DAILY_TASK___TARGETS_FIELD.default_value = ""
DAILY_TASK___TARGETS_FIELD.type = 9
DAILY_TASK___TARGETS_FIELD.cpp_type = 9

DAILY_TASK___ACTIVE_POINT_FIELD.name = "__active_point"
DAILY_TASK___ACTIVE_POINT_FIELD.full_name = ".daily_task.__active_point"
DAILY_TASK___ACTIVE_POINT_FIELD.number = 9
DAILY_TASK___ACTIVE_POINT_FIELD.index = 8
DAILY_TASK___ACTIVE_POINT_FIELD.label = 2
DAILY_TASK___ACTIVE_POINT_FIELD.has_default_value = false
DAILY_TASK___ACTIVE_POINT_FIELD.default_value = 0
DAILY_TASK___ACTIVE_POINT_FIELD.type = 5
DAILY_TASK___ACTIVE_POINT_FIELD.cpp_type = 1

DAILY_TASK___REWARD_FIELD.name = "__reward"
DAILY_TASK___REWARD_FIELD.full_name = ".daily_task.__reward"
DAILY_TASK___REWARD_FIELD.number = 10
DAILY_TASK___REWARD_FIELD.index = 9
DAILY_TASK___REWARD_FIELD.label = 2
DAILY_TASK___REWARD_FIELD.has_default_value = false
DAILY_TASK___REWARD_FIELD.default_value = ""
DAILY_TASK___REWARD_FIELD.type = 9
DAILY_TASK___REWARD_FIELD.cpp_type = 9

DAILY_TASK.name = "daily_task"
DAILY_TASK.full_name = ".daily_task"
DAILY_TASK.nested_types = {}
DAILY_TASK.enum_types = {}
DAILY_TASK.fields = {DAILY_TASK___ID_FIELD, DAILY_TASK___NAME_FIELD, DAILY_TASK___DESC_FIELD, DAILY_TASK___ICON_FIELD, DAILY_TASK___GROUP_FIELD, DAILY_TASK___LEVEL_FIELD, DAILY_TASK___WORLD_LEVEL_FIELD, DAILY_TASK___TARGETS_FIELD, DAILY_TASK___ACTIVE_POINT_FIELD, DAILY_TASK___REWARD_FIELD}
DAILY_TASK.is_extendable = false
DAILY_TASK.extensions = {}

daily_task = protobuf.Message(DAILY_TASK)

