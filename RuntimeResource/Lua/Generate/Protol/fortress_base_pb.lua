--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.fortress_base_pb')

FORTRESS_BASE = protobuf.Descriptor();
FORTRESS_BASE___ID_FIELD = protobuf.FieldDescriptor();
FORTRESS_BASE___IS_MULTI_FIELD = protobuf.FieldDescriptor();
FORTRESS_BASE___TYPE_FIELD = protobuf.FieldDescriptor();
FORTRESS_BASE___OPEN_FUNC_FIELD = protobuf.FieldDescriptor();
FORTRESS_BASE___BUILDING_FIELD = protobuf.FieldDescriptor();

FORTRESS_BASE___ID_FIELD.name = "__id"
FORTRESS_BASE___ID_FIELD.full_name = ".fortress_base.__id"
FORTRESS_BASE___ID_FIELD.number = 1
FORTRESS_BASE___ID_FIELD.index = 0
FORTRESS_BASE___ID_FIELD.label = 2
FORTRESS_BASE___ID_FIELD.has_default_value = false
FORTRESS_BASE___ID_FIELD.default_value = 0
FORTRESS_BASE___ID_FIELD.type = 5
FORTRESS_BASE___ID_FIELD.cpp_type = 1

FORTRESS_BASE___IS_MULTI_FIELD.name = "__is_multi"
FORTRESS_BASE___IS_MULTI_FIELD.full_name = ".fortress_base.__is_multi"
FORTRESS_BASE___IS_MULTI_FIELD.number = 2
FORTRESS_BASE___IS_MULTI_FIELD.index = 1
FORTRESS_BASE___IS_MULTI_FIELD.label = 2
FORTRESS_BASE___IS_MULTI_FIELD.has_default_value = false
FORTRESS_BASE___IS_MULTI_FIELD.default_value = 0
FORTRESS_BASE___IS_MULTI_FIELD.type = 5
FORTRESS_BASE___IS_MULTI_FIELD.cpp_type = 1

FORTRESS_BASE___TYPE_FIELD.name = "__type"
FORTRESS_BASE___TYPE_FIELD.full_name = ".fortress_base.__type"
FORTRESS_BASE___TYPE_FIELD.number = 3
FORTRESS_BASE___TYPE_FIELD.index = 2
FORTRESS_BASE___TYPE_FIELD.label = 2
FORTRESS_BASE___TYPE_FIELD.has_default_value = false
FORTRESS_BASE___TYPE_FIELD.default_value = 0
FORTRESS_BASE___TYPE_FIELD.type = 5
FORTRESS_BASE___TYPE_FIELD.cpp_type = 1

FORTRESS_BASE___OPEN_FUNC_FIELD.name = "__open_func"
FORTRESS_BASE___OPEN_FUNC_FIELD.full_name = ".fortress_base.__open_func"
FORTRESS_BASE___OPEN_FUNC_FIELD.number = 4
FORTRESS_BASE___OPEN_FUNC_FIELD.index = 3
FORTRESS_BASE___OPEN_FUNC_FIELD.label = 2
FORTRESS_BASE___OPEN_FUNC_FIELD.has_default_value = false
FORTRESS_BASE___OPEN_FUNC_FIELD.default_value = 0
FORTRESS_BASE___OPEN_FUNC_FIELD.type = 5
FORTRESS_BASE___OPEN_FUNC_FIELD.cpp_type = 1

FORTRESS_BASE___BUILDING_FIELD.name = "__building"
FORTRESS_BASE___BUILDING_FIELD.full_name = ".fortress_base.__building"
FORTRESS_BASE___BUILDING_FIELD.number = 5
FORTRESS_BASE___BUILDING_FIELD.index = 4
FORTRESS_BASE___BUILDING_FIELD.label = 2
FORTRESS_BASE___BUILDING_FIELD.has_default_value = false
FORTRESS_BASE___BUILDING_FIELD.default_value = ""
FORTRESS_BASE___BUILDING_FIELD.type = 9
FORTRESS_BASE___BUILDING_FIELD.cpp_type = 9

FORTRESS_BASE.name = "fortress_base"
FORTRESS_BASE.full_name = ".fortress_base"
FORTRESS_BASE.nested_types = {}
FORTRESS_BASE.enum_types = {}
FORTRESS_BASE.fields = {FORTRESS_BASE___ID_FIELD, FORTRESS_BASE___IS_MULTI_FIELD, FORTRESS_BASE___TYPE_FIELD, FORTRESS_BASE___OPEN_FUNC_FIELD, FORTRESS_BASE___BUILDING_FIELD}
FORTRESS_BASE.is_extendable = false
FORTRESS_BASE.extensions = {}

fortress_base = protobuf.Message(FORTRESS_BASE)

