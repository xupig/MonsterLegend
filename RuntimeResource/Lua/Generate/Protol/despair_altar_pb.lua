--Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf.protobuf"
module('Protol.despair_altar_pb')

DESPAIR_ALTAR = protobuf.Descriptor();
DESPAIR_ALTAR___ID_FIELD = protobuf.FieldDescriptor();
DESPAIR_ALTAR___DESC_FIELD = protobuf.FieldDescriptor();
DESPAIR_ALTAR___DUNGEON_DESC_FIELD = protobuf.FieldDescriptor();
DESPAIR_ALTAR___MAP_FIELD = protobuf.FieldDescriptor();
DESPAIR_ALTAR___CLEARANCE_FIELD = protobuf.FieldDescriptor();
DESPAIR_ALTAR___STAR_TIME_FIELD = protobuf.FieldDescriptor();

DESPAIR_ALTAR___ID_FIELD.name = "__id"
DESPAIR_ALTAR___ID_FIELD.full_name = ".despair_altar.__id"
DESPAIR_ALTAR___ID_FIELD.number = 1
DESPAIR_ALTAR___ID_FIELD.index = 0
DESPAIR_ALTAR___ID_FIELD.label = 2
DESPAIR_ALTAR___ID_FIELD.has_default_value = false
DESPAIR_ALTAR___ID_FIELD.default_value = 0
DESPAIR_ALTAR___ID_FIELD.type = 5
DESPAIR_ALTAR___ID_FIELD.cpp_type = 1

DESPAIR_ALTAR___DESC_FIELD.name = "__desc"
DESPAIR_ALTAR___DESC_FIELD.full_name = ".despair_altar.__desc"
DESPAIR_ALTAR___DESC_FIELD.number = 2
DESPAIR_ALTAR___DESC_FIELD.index = 1
DESPAIR_ALTAR___DESC_FIELD.label = 2
DESPAIR_ALTAR___DESC_FIELD.has_default_value = false
DESPAIR_ALTAR___DESC_FIELD.default_value = 0
DESPAIR_ALTAR___DESC_FIELD.type = 5
DESPAIR_ALTAR___DESC_FIELD.cpp_type = 1

DESPAIR_ALTAR___DUNGEON_DESC_FIELD.name = "__dungeon_desc"
DESPAIR_ALTAR___DUNGEON_DESC_FIELD.full_name = ".despair_altar.__dungeon_desc"
DESPAIR_ALTAR___DUNGEON_DESC_FIELD.number = 3
DESPAIR_ALTAR___DUNGEON_DESC_FIELD.index = 2
DESPAIR_ALTAR___DUNGEON_DESC_FIELD.label = 2
DESPAIR_ALTAR___DUNGEON_DESC_FIELD.has_default_value = false
DESPAIR_ALTAR___DUNGEON_DESC_FIELD.default_value = ""
DESPAIR_ALTAR___DUNGEON_DESC_FIELD.type = 9
DESPAIR_ALTAR___DUNGEON_DESC_FIELD.cpp_type = 9

DESPAIR_ALTAR___MAP_FIELD.name = "__map"
DESPAIR_ALTAR___MAP_FIELD.full_name = ".despair_altar.__map"
DESPAIR_ALTAR___MAP_FIELD.number = 5
DESPAIR_ALTAR___MAP_FIELD.index = 3
DESPAIR_ALTAR___MAP_FIELD.label = 2
DESPAIR_ALTAR___MAP_FIELD.has_default_value = false
DESPAIR_ALTAR___MAP_FIELD.default_value = 0
DESPAIR_ALTAR___MAP_FIELD.type = 5
DESPAIR_ALTAR___MAP_FIELD.cpp_type = 1

DESPAIR_ALTAR___CLEARANCE_FIELD.name = "__clearance"
DESPAIR_ALTAR___CLEARANCE_FIELD.full_name = ".despair_altar.__clearance"
DESPAIR_ALTAR___CLEARANCE_FIELD.number = 6
DESPAIR_ALTAR___CLEARANCE_FIELD.index = 4
DESPAIR_ALTAR___CLEARANCE_FIELD.label = 2
DESPAIR_ALTAR___CLEARANCE_FIELD.has_default_value = false
DESPAIR_ALTAR___CLEARANCE_FIELD.default_value = 0
DESPAIR_ALTAR___CLEARANCE_FIELD.type = 5
DESPAIR_ALTAR___CLEARANCE_FIELD.cpp_type = 1

DESPAIR_ALTAR___STAR_TIME_FIELD.name = "__star_time"
DESPAIR_ALTAR___STAR_TIME_FIELD.full_name = ".despair_altar.__star_time"
DESPAIR_ALTAR___STAR_TIME_FIELD.number = 7
DESPAIR_ALTAR___STAR_TIME_FIELD.index = 5
DESPAIR_ALTAR___STAR_TIME_FIELD.label = 2
DESPAIR_ALTAR___STAR_TIME_FIELD.has_default_value = false
DESPAIR_ALTAR___STAR_TIME_FIELD.default_value = ""
DESPAIR_ALTAR___STAR_TIME_FIELD.type = 9
DESPAIR_ALTAR___STAR_TIME_FIELD.cpp_type = 9

DESPAIR_ALTAR.name = "despair_altar"
DESPAIR_ALTAR.full_name = ".despair_altar"
DESPAIR_ALTAR.nested_types = {}
DESPAIR_ALTAR.enum_types = {}
DESPAIR_ALTAR.fields = {DESPAIR_ALTAR___ID_FIELD, DESPAIR_ALTAR___DESC_FIELD, DESPAIR_ALTAR___DUNGEON_DESC_FIELD, DESPAIR_ALTAR___MAP_FIELD, DESPAIR_ALTAR___CLEARANCE_FIELD, DESPAIR_ALTAR___STAR_TIME_FIELD}
DESPAIR_ALTAR.is_extendable = false
DESPAIR_ALTAR.extensions = {}

despair_altar = protobuf.Message(DESPAIR_ALTAR)

