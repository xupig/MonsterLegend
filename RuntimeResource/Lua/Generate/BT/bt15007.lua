bt15007 = bt.bt_root:new()

function bt15007:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt15007})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_move_to_pos:new(6541,765,5069));
		node1:add_child(bt.bt_move_to_pos:new(7233,765,5539));
		node1:add_child(bt.bt_move_to_pos:new(6790,765,5940));
		node1:add_child(bt.bt_move_to_pos:new(6298,765,5485));
	end

	return tmp
end

return bt15007:new()
