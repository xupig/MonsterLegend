local bt1 = bt.bt_sub_root:new()

function bt1:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt1})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_off_play:new());
		node1:add_child(bt.bt_call_method:new("full_hp",""));
	end

	return tmp
end

return bt1:new()
