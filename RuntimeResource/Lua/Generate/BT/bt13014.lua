bt13014 = bt.bt_root:new()

function bt13014:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt13014})
	tmp.__index = tmp

	do
		local node1 = bt.bt_prior_selector:new();
		self:add_child(node1);
		do
			local node2 = bt.bt_sequence:new();
			node1:add_child(node2);
			node2:add_child(bt.bt_has_buff:new(bt.ai_target_type.self,0,1741));
			node2:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,600,0));
			node2:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,0));
		end
		do
			local node6 = bt.bt_sequence:new();
			node1:add_child(node6);
			node6:add_child(bt.bt_move_to_pos:new(2491,2220,2655));
			node6:add_child(bt.bt_add_buff:new(bt.ai_target_type.self,0,1741,-1));
		end
	end

	return tmp
end

return bt13014:new()
