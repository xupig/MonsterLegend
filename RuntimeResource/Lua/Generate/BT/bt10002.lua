bt10002 = bt.bt_root:new()

function bt10002:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt10002})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.path,0,4000,1,1000));
		node1:add_child(bt.bt_idle:new(5000));
	end

	return tmp
end

return bt10002:new()
