bt12005 = bt.bt_root:new()

function bt12005:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12005})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,500,0));
		do
			local node3 = bt.bt_sequence:new();
			node1:add_child(node3);
			node3:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,200));
			node3:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,8,1,1,1,0));
		end
	end

	return tmp
end

return bt12005:new()
