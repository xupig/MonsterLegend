bt12007 = bt.bt_root:new()

function bt12007:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12007})
	tmp.__index = tmp

	self:add_child(bt.bt_idle:new(0));

	return tmp
end

return bt12007:new()
