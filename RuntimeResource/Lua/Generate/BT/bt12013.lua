bt12013 = bt.bt_root:new()

function bt12013:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12013})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,99999,0));
		do
			local node3 = bt.bt_prior_selector:new();
			node1:add_child(node3);
			do
				local node4 = bt.bt_sequence:new();
				node3:add_child(node4);
				node4:add_child(bt.bt_cmp_target_dis:new(bt.ai_target_type.closest,3000,bt.cmp_type.ge,510));
				do
					local node6 = bt.bt_random_selector:new(30,30,30);
					node4:add_child(node6);
					node6:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,9,1,1,1,0));
					node6:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,10,1,1,1,0));
					node6:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,14,1,1,1,0));
				end
			end
			do
				local node10 = bt.bt_sequence:new();
				node3:add_child(node10);
				node10:add_child(bt.bt_has_buff:new(bt.ai_target_type.self,0,1032));
				node10:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
				do
					local node13 = bt.bt_random_selector:new(20,30,20,14,14);
					node10:add_child(node13);
					node13:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,1,1,1,0));
					node13:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,2,1,1,1,0));
					node13:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,4,1,1,1,0));
					node13:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,5,1,1,1,0));
					node13:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,7,1,1,1,0));
				end
			end
			do
				local node19 = bt.bt_sequence:new();
				node3:add_child(node19);
				node19:add_child(bt.bt_cmp_hp:new(bt.ai_target_type.self,0,bt.cmp_type.le,50));
				node19:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,3,0,1,0,0));
			end
			do
				local node22 = bt.bt_sequence:new();
				node3:add_child(node22);
				node22:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
				do
					local node24 = bt.bt_random_selector:new(20,20,20,20,19);
					node22:add_child(node24);
					node24:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,8,1,1,1,0));
					node24:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,11,1,1,1,0));
					node24:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,7,1,1,1,0));
					node24:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,12,1,1,1,0));
					node24:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,6,1,1,1,0));
				end
			end
		end
	end

	return tmp
end

return bt12013:new()
