bt12019 = bt.bt_root:new()

function bt12019:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12019})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,2000,0));
		node1:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,200));
		node1:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,0));
	end

	return tmp
end

return bt12019:new()
