bt3 = bt.bt_sub_root:new()

function bt3:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt3})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_add_buff:new(bt.ai_target_type.self,0,1049,100));
		node1:add_child(bt.bt_off_play:new());
	end

	return tmp
end

return bt3:new()
