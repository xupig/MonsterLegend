bt12008 = bt.bt_root:new()

function bt12008:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12008})
	tmp.__index = tmp

	do
		local node1 = bt.bt_prior_selector:new();
		self:add_child(node1);
		do
			local node2 = bt.bt_sequence:new();
			node1:add_child(node2);
			node2:add_child(bt.bt_cmp_target_dis:new(bt.ai_target_type.aggro,0,bt.cmp_type.gt,800));
			node2:add_child(bt.bt_remove_aggro_list:new());
		end
		do
			local node5 = bt.bt_sequence:new();
			node1:add_child(node5);
			node5:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,800,0));
			node5:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,1,1,1,0));
		end
	end

	return tmp
end

return bt12008:new()
