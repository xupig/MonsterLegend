bt2 = bt.bt_sub_root:new()

function bt2:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt2})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_off_play:new());
	end

	return tmp
end

return bt2:new()
