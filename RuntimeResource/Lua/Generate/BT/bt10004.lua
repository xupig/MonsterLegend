local bt10004 = bt.bt_sub_root:new()

function bt10004:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt10004})
	tmp.__index = tmp

	self:add_child(bt.bt_owner_off_play:new());

	return tmp
end

return bt10004:new()
