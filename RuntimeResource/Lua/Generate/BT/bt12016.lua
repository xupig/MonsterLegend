bt12016 = bt.bt_root:new()

function bt12016:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12016})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,99999,0));
		do
			local node3 = bt.bt_prior_selector:new();
			node1:add_child(node3);
			do
				local node4 = bt.bt_sequence:new();
				node3:add_child(node4);
				node4:add_child(bt.bt_cmp_target_dis:new(bt.ai_target_type.closest,3000,bt.cmp_type.ge,600));
				do
					local node6 = bt.bt_random_selector:new(20,20,20,20);
					node4:add_child(node6);
					do
						local node7 = bt.bt_sequence:new();
						node6:add_child(node7);
						node7:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,9,1,1,1,0));
						node7:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,800));
					end
					do
						local node10 = bt.bt_sequence:new();
						node6:add_child(node10);
						node10:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,10,1,1,1,0));
						node10:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,800));
					end
					node6:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,14,1,1,1,0));
					node6:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,600));
				end
			end
			do
				local node15 = bt.bt_sequence:new();
				node3:add_child(node15);
				node15:add_child(bt.bt_has_buff:new(bt.ai_target_type.self,0,1032));
				node15:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
				do
					local node18 = bt.bt_random_selector:new(15,25,15,10,10,10);
					node15:add_child(node18);
					do
						local node19 = bt.bt_sequence:new();
						node18:add_child(node19);
						node19:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,1,1,1,0));
						node19:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
					end
					node18:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,2,1,1,1,0));
					do
						local node23 = bt.bt_sequence:new();
						node18:add_child(node23);
						node23:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,4,1,1,1,0));
						node23:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
					end
					node18:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,5,1,1,1,0));
					node18:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,19,1,1,1,0));
					node18:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,7,1,1,1,0));
				end
			end
			do
				local node29 = bt.bt_sequence:new();
				node3:add_child(node29);
				node29:add_child(bt.bt_cmp_hp:new(bt.ai_target_type.self,0,bt.cmp_type.le,50));
				node29:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,3,0,1,0,0));
			end
			do
				local node32 = bt.bt_sequence:new();
				node3:add_child(node32);
				node32:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
				do
					local node34 = bt.bt_random_selector:new(10,10,10,10,10,15,10);
					node32:add_child(node34);
					node34:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,8,1,1,1,0));
					node34:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,11,1,1,1,0));
					do
						local node37 = bt.bt_sequence:new();
						node34:add_child(node37);
						node37:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,7,1,1,1,0));
						node37:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
					end
					node34:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,12,1,1,1,0));
					node34:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,6,1,1,1,0));
					do
						local node42 = bt.bt_sequence:new();
						node34:add_child(node42);
						node42:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,16,1,1,1,0));
						node42:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,800));
					end
					node34:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,19,1,1,1,0));
				end
			end
		end
	end

	return tmp
end

return bt12016:new()
