bt14001 = bt.bt_root:new()

function bt14001:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt14001})
	tmp.__index = tmp

	do
		local node1 = bt.bt_random_selector:new(10,10,10,10,10,10,10);
		self:add_child(node1);
		node1:add_child(bt.bt_move_to_pos:new(10364,3012,6921));
		node1:add_child(bt.bt_move_to_pos:new(9617,3012,7559));
		node1:add_child(bt.bt_move_to_pos:new(9697,3012,6814));
		node1:add_child(bt.bt_move_to_pos:new(10491,3012,7401));
		node1:add_child(bt.bt_move_to_pos:new(10010,3012,7791));
		node1:add_child(bt.bt_move_to_pos:new(9531,3012,7211));
		node1:add_child(bt.bt_move_to_pos:new(10053,3012,6798));
	end

	return tmp
end

return bt14001:new()
