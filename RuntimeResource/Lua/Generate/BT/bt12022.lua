bt12022 = bt.bt_root:new()

function bt12022:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12022})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,2000,0));
		do
			local node3 = bt.bt_random_selector:new(20,20,20,20);
			node1:add_child(node3);
			do
				local node4 = bt.bt_sequence:new();
				node3:add_child(node4);
				node4:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,10));
				node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,0));
			end
			do
				local node7 = bt.bt_sequence:new();
				node3:add_child(node7);
				node7:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,2,1,1,1,0));
				node7:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,0));
			end
			do
				local node10 = bt.bt_sequence:new();
				node3:add_child(node10);
				node10:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,3,1,1,1,0));
				node10:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,0));
			end
			do
				local node13 = bt.bt_sequence:new();
				node3:add_child(node13);
				node13:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,4,1,1,1,0));
				node13:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,0));
			end
		end
	end

	return tmp
end

return bt12022:new()
