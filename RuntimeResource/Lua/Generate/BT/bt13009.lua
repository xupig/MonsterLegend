bt13009 = bt.bt_root:new()

function bt13009:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt13009})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.path,0,4000,12,600,0));
		node1:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,100));
		node1:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,1,1,1,0));
	end

	return tmp
end

return bt13009:new()
