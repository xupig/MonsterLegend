bt15004 = bt.bt_root:new()

function bt15004:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt15004})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,1000,0));
		do
			local node3 = bt.bt_sequence:new();
			node1:add_child(node3);
			node3:add_child(bt.bt_cmp_attri:new(bt.ai_target_type.aggro,0,"god_island_tire_flag",bt.cmp_type.eq,0));
			node3:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
		end
		do
			local node6 = bt.bt_random_selector:new(30,30,20);
			node1:add_child(node6);
			node6:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,1,0,0));
			node6:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,2,0,1,0,0));
			do
				local node9 = bt.bt_prior_selector:new();
				node6:add_child(node9);
				do
					local node10 = bt.bt_sequence:new();
					node9:add_child(node10);
					node10:add_child(bt.bt_has_buff:new(bt.ai_target_type.self,0,1842));
					node10:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,1,0,0));
				end
				node9:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,3,1,1,0,0));
			end
		end
	end

	return tmp
end

return bt15004:new()
