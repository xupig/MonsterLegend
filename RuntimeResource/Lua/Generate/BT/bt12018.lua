bt12018 = bt.bt_root:new()

function bt12018:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12018})
	tmp.__index = tmp

	do
		local node1 = bt.bt_prior_selector:new();
		self:add_child(node1);
		do
			local node2 = bt.bt_sequence:new();
			node1:add_child(node2);
			node2:add_child(bt.bt_cmp_hp:new(bt.ai_target_type.self,0,bt.cmp_type.lt,100));
			node2:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,1000,0));
			node2:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,200));
			node2:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,1,0,0,0));
		end
		do
			local node7 = bt.bt_sequence:new();
			node1:add_child(node7);
			node7:add_child(bt.bt_wander:new(300,500,2000,1,3));
			node7:add_child(bt.bt_off_play:new());
		end
	end

	return tmp
end

return bt12018:new()
