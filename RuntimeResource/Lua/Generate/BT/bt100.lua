bt100 = bt.bt_root:new()

function bt100:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt100})
	tmp.__index = tmp

	do
		local node1 = bt.bt_prior_selector:new();
		self:add_child(node1);
		do
			local node2 = bt.bt_sequence:new();
			node1:add_child(node2);
			node2:add_child(bt.bt_cmp_target_dis:new(bt.ai_target_type.master,0,bt.cmp_type.ge,400));
			node2:add_child(bt.bt_move_to_target:new(bt.ai_target_type.master,0,0,300));
		end
		do
			local node5 = bt.bt_sequence:new();
			node1:add_child(node5);
			node5:add_child(bt.bt_player_in_attack:new());
			node5:add_child(bt.bt_move_to_target:new(bt.ai_target_type.master_target,0,0,200));
			node5:add_child(bt.bt_cast:new(bt.ai_target_type.master_target,0,1,1,0,1,0));
		end
	end

	return tmp
end

return bt100:new()
