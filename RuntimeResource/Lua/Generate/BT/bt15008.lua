bt15008 = bt.bt_root:new()

function bt15008:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt15008})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,1000,0));
		node1:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
		do
			local node4 = bt.bt_random_selector:new(30,30,20);
			node1:add_child(node4);
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,1,0,0));
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,2,0,1,0,0));
			do
				local node7 = bt.bt_prior_selector:new();
				node4:add_child(node7);
				do
					local node8 = bt.bt_sequence:new();
					node7:add_child(node8);
					node8:add_child(bt.bt_has_buff:new(bt.ai_target_type.self,0,1842));
					node8:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,1,0,0));
				end
				node7:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,3,1,1,0,0));
			end
		end
	end

	return tmp
end

return bt15008:new()
