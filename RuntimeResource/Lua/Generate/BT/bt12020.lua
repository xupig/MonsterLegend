bt12020 = bt.bt_root:new()

function bt12020:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12020})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,1000,0));
		node1:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,700));
		do
			local node4 = bt.bt_random_selector:new(30,30,30);
			node1:add_child(node4);
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,0));
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,2,0,0,0,0));
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,3,0,0,0,0));
		end
	end

	return tmp
end

return bt12020:new()
