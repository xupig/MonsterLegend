bt12003 = bt.bt_root:new()

function bt12003:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12003})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,1000,0));
		node1:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,1,1,1,0));
	end

	return tmp
end

return bt12003:new()
