bt15006 = bt.bt_root:new()

function bt15006:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt15006})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_move_to_pos:new(14210,2130,12030));
		node1:add_child(bt.bt_move_to_pos:new(16234,2130,12044));
		node1:add_child(bt.bt_move_to_pos:new(16234,2130,10260));
		node1:add_child(bt.bt_move_to_pos:new(18247,2130,10260));
		node1:add_child(bt.bt_move_to_pos:new(18247,2130,12030));
		node1:add_child(bt.bt_move_to_pos:new(19819,2130,11956));
		node1:add_child(bt.bt_move_to_pos:new(19819,2130,9095));
		node1:add_child(bt.bt_move_to_pos:new(14240,2130,8952));
	end

	return tmp
end

return bt15006:new()
