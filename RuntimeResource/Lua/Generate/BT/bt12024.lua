bt12024 = bt.bt_root:new()

function bt12024:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12024})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,500,0));
		node1:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,3,0,0,0,0));
	end

	return tmp
end

return bt12024:new()
