bt101 = bt.bt_root:new()

function bt101:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt101})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_player_in_attack:new());
		node1:add_child(bt.bt_cast:new(bt.ai_target_type.master_target,0,1,0,0,0,0));
	end

	return tmp
end

return bt101:new()
