bt100000 = bt.bt_root:new()

function bt100000:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt100000})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_pet_search_target:new(300,1000));
		do
			local node3 = bt.bt_prior_selector:new();
			node1:add_child(node3);
			do
				local node4 = bt.bt_sequence:new();
				node3:add_child(node4);
				node4:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,300));
				node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0));
			end
			node3:add_child(bt.bt_idle:new(1000));
		end
	end

	return tmp
end

return bt100000:new()
