bt13013 = bt.bt_root:new()

function bt13013:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt13013})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.path,0,4000,16,0,0));
	end

	return tmp
end

return bt13013:new()
