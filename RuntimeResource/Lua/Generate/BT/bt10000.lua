bt10000 = bt.bt_root:new()

function bt10000:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt10000})
	tmp.__index = tmp

	do
		local node1 = bt.bt_prior_selector:new();
		self:add_child(node1);
		do
			local node2 = bt.bt_sequence:new();
			node1:add_child(node2);
			node2:add_child(bt.bt_search_drop:new(1000));
			node2:add_child(bt.bt_move_to_target:new(bt.ai_target_type.drop,1000,0,10));
		end
		node1:add_child(bt.bt_check_task:new());
		do
			local node6 = bt.bt_sequence:new();
			node1:add_child(node6);
			node6:add_child(bt.bt_owner_search_target:new(1000,0));
			do
				local node8 = bt.bt_prior_selector:new();
				node6:add_child(node8);
				do
					local node9 = bt.bt_sequence:new();
					node8:add_child(node9);
					node9:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,600));
					node9:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,8,0,0,0,1));
				end
				do
					local node12 = bt.bt_sequence:new();
					node8:add_child(node12);
					node12:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,7,0,0,0,1));
				end
				do
					local node14 = bt.bt_sequence:new();
					node8:add_child(node14);
					node14:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,6,0,0,0,1));
				end
				do
					local node16 = bt.bt_sequence:new();
					node8:add_child(node16);
					node16:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,800));
					node16:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,5,0,0,0,1));
				end
				do
					local node19 = bt.bt_sequence:new();
					node8:add_child(node19);
					node19:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,300));
					node19:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,4,0,0,0,1));
				end
				do
					local node22 = bt.bt_sequence:new();
					node8:add_child(node22);
					node22:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,400));
					node22:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,3,0,0,0,1));
				end
				do
					local node25 = bt.bt_sequence:new();
					node8:add_child(node25);
					node25:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,500));
					node25:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,2,0,0,0,1));
				end
				do
					local node28 = bt.bt_sequence:new();
					node8:add_child(node28);
					node28:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,300));
					node28:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,1));
				end
			end
		end
	end

	return tmp
end

return bt10000:new()
