bt10003 = bt.bt_root:new()

function bt10003:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt10003})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_move_to_target:new(bt.ai_target_type.master,0,0,200));
		node1:add_child(bt.bt_idle:new(1000));
	end

	return tmp
end

return bt10003:new()
