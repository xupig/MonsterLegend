bt102 = bt.bt_root:new()

function bt102:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt102})
	tmp.__index = tmp

	do
		local node1 = bt.bt_prior_selector:new();
		self:add_child(node1);
		do
			local node2 = bt.bt_sequence:new();
			node1:add_child(node2);
			node2:add_child(bt.bt_cmp_target_dis:new(bt.ai_target_type.master,0,bt.cmp_type.ge,100));
			node2:add_child(bt.bt_move_to_target:new(bt.ai_target_type.master,0,0,100));
		end
		node1:add_child(bt.bt_idle:new(0));
	end

	return tmp
end

return bt102:new()
