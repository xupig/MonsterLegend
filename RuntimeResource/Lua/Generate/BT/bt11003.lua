bt11003 = bt.bt_root:new()

function bt11003:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt11003})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.range,700,4000,0,2000));
		do
			local node3 = bt.bt_sequence:new();
			node1:add_child(node3);
			node3:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,1600));
			node3:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,1,1,1));
		end
	end

	return tmp
end

return bt11003:new()
