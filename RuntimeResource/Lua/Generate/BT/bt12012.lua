bt12012 = bt.bt_root:new()

function bt12012:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12012})
	tmp.__index = tmp

	self:add_child(bt.bt_search_target:new(bt.ai_patrol_type.path,0,4000,2,0,0));

	return tmp
end

return bt12012:new()
