bt11001 = bt.bt_root:new()

function bt11001:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt11001})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,1800));
		do
			local node3 = bt.bt_random_selector:new(20,60);
			node1:add_child(node3);
			node3:add_child(bt.bt_look_on:new(300,1800,2,5,5,10,10,1000,2000,2000,2000,2000));
			do
				local node5 = bt.bt_sequence:new();
				node3:add_child(node5);
				node5:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,200));
				node5:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,1,1,1));
			end
		end
	end

	return tmp
end

return bt11001:new()
