bt12001 = bt.bt_root:new()

function bt12001:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12001})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,1000,0));
		node1:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,300));
		do
			local node4 = bt.bt_random_selector:new(7,20,14,14,14,7,20);
			node1:add_child(node4);
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,1,1,1,0));
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,2,1,1,1,0));
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,3,1,1,1,0));
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,4,1,1,1,0));
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,5,1,1,1,0));
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,6,0,1,1,0));
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,7,1,1,1,0));
		end
	end

	return tmp
end

return bt12001:new()
