bt12017 = bt.bt_root:new()

function bt12017:new()
		 
	local tmp = {}
	setmetatable(tmp, {__index = bt12017})
	tmp.__index = tmp

	do
		local node1 = bt.bt_sequence:new();
		self:add_child(node1);
		node1:add_child(bt.bt_search_target:new(bt.ai_patrol_type.stand,0,4000,0,1000,0));
		node1:add_child(bt.bt_move_to_target:new(bt.ai_target_type.aggro,0,0,300));
		do
			local node4 = bt.bt_random_selector:new(50,30);
			node1:add_child(node4);
			node4:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,0));
			do
				local node6 = bt.bt_prior_selector:new();
				node4:add_child(node6);
				do
					local node7 = bt.bt_sequence:new();
					node6:add_child(node7);
					node7:add_child(bt.bt_has_buff:new(bt.ai_target_type.self,0,1842));
					node7:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,1,0,0,0,0));
				end
				node6:add_child(bt.bt_cast:new(bt.ai_target_type.aggro,0,2,0,0,0,0));
			end
		end
	end

	return tmp
end

return bt12017:new()
