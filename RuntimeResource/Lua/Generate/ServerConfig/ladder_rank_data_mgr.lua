--local engine = require "engine"
local lua_util = require "lua_util"
--local putil = require "public_util"
local global_params = require "global_params"

local pairs = pairs
--local unpack = unpack
local table = table
local ipairs = ipairs
local os = os

local readXml = lua_util._readXml
--local log_d = putil.log_d
local os_time = os.time
local os_date = os.date
local table_sort = table.sort
local table_insert = table.insert

local ladder_rank_data_mgr = {}

function ladder_rank_data_mgr:init_data()
    self.ladder_match_rank = readXml("/data/xml/ladder_match_rank.xml", "id_i")
    self.rank_to_id = {}
    self.rank_list = {}
    for k,v in pairs(self.ladder_match_rank) do
        local rank = v.rank
        table_insert(self.rank_list, rank[2])
        self.rank_to_id[rank[2]] = k
    end
    table_sort(self.rank_list)
    
    self.season_data = {}
end

--获取排名对应的奖励信息
function ladder_rank_data_mgr:get_data_by_rank(rank)
    local id = 0
    for _, v in ipairs(self.rank_list) do
        if rank <= v then
            id = self.rank_to_id[v]
            break
        end
    end
    return self.ladder_match_rank[id]
end

--生成赛季对应结束时间戳信息
function ladder_rank_data_mgr:update_season_data()
    self.season_data = {}
    local date_str = global_params:get_param("ladder_match_star_time", "2017-7-1 05:00:00")
    local start_time = lua_util.parse_time_str(date_str)
    local start_date = os_date('*t', start_time)
    local cur_time = os_time()
    local start_year = start_date.year
    local start_month = start_date.month
    local start_day = start_date.day
    local start_hour = start_date.hour
    local start_min = start_date.min
    local start_sec = start_date.sec
    local season = 0
    local last_end_time = start_time
    while true do
        start_month = start_month + 1
        if start_month == 13 then
            start_year = start_year + 1
            start_month = 1
        end
        season = season + 1
        local end_time = os_time{year=start_year,month=start_month,day=start_day,hour=start_hour,min=start_min,sec=start_sec}
        if end_time >= cur_time then
            self.season_data.season = season
            self.season_data.start_time = last_end_time
            self.season_data.end_time = end_time
            break
        end
        last_end_time = end_time
    end
end
--返回当前时间所处赛季信息[赛季、赛季开始时间戳、赛季结束时间戳]
function ladder_rank_data_mgr:get_season_data()
    local cur_time = os_time()
    --延后600秒刷新赛季信息，保证当前赛季结算
    if lua_util.get_table_real_count(self.season_data) == 0 or cur_time > self.season_data.end_time + 600 then
        self:update_season_data()
    end
    return self.season_data
end

return ladder_rank_data_mgr

