local cfg = {

    --传送点 101-200
    TELEPORT_POINT                    = 102,    --传送点传送进地图
    SPACE_GET_LINE_INFO               = 103,    --获取指定地图的分线信息
    SPACE_GO_TO_LINE                  = 104,    --进入地图的指定分线

    --邮件系统 201-250
    MAIL_LIST_REQ                     = 201,    --邮件列表
    MAIL_READ_REQ                     = 202,    --读邮件
    MAIL_DEL_REQ                      = 203,    --删除邮件
    MAIL_GET_ATTACHMENT_REQ           = 204,    --获取邮件附件
    MAIL_BATCH_DEL_REQ                = 205,    --批量删除邮件
    MAIL_DEL_ALL_REQ                  = 206,    --删除所有邮件
    MAIL_GET_ALL_ATTACHMENT_REQ       = 207,    --获取所有邮件附件
    MAIL_SEND_MAIL_REQ                = 208,    --发送邮件,根据名字
    MAIL_ON_NEW_MAIL_RESP             = 209,    --新邮件提示
    MAIL_BATCH_GET_ATTACHMENT_REQ     = 210,    --批量提取邮件附件
    
    --摆摊系统 251-300
    PEDDLE_GET_WARE_LIST              = 251,    --获取摆摊列表
    PEDDLE_ADD_CONCERN                = 252,    --增加关注
    PEDDLE_SELL                       = 253,    --摆摊系统出售物品
    PEDDLE_BUY                        = 254,    --摆摊系统购买物品
    PEDDLE_CANCEL                     = 255,    --摆摊系统撤销物品

    ----生活技能系统 301-350
    --LIFE_SKILL_UPGRADE                = 301,    --升级生活技能
    --LIFE_SKILL_MAKE                   = 302,    --制造道具
    --LIFE_SKILL_COLLECT                = 303,    --生活技能采集
    --LIFE_SKILL_ADD_EXP                = 304,    --给指定技能加经验
    --LIFE_SKILL_GET_EQUIP_MAKE_REWARD  = 305,    --获取装备制造奖励

    --聊天系统 351-400
    CHAT_REQ                          = 351,    --发送聊天消息
    --CHAT_CHANGE_WORLD_CHANNEL         = 353,    --请求修改世界聊天分组 --名字或者可以改成：chat_change_world_chat_team
    CHAT_EDIT_OFTEN_USE               = 354,    -- 编辑常用语
    
    --好友系统401~450
    FRIEND_RECOMMEND                    = 401,  --推荐好友列表
    FRIEND_SEARDH                       = 402,  --玩家查找(根据名字)
    FRIEND_APPLY_TO                     = 403,  --申请加好友
    FRIEND_APPLY_LIST                   = 404,  --申请加我为好友的列表
    FRIEND_ACCEPT                       = 405,  --同意添加好友
    FRIEND_REFUSE                       = 406,  --拒绝添加好友
    FRIEND_LIST                         = 407,  --好友列表
    FRIEND_DELE                         = 408,  --删除好友
    FRIEND_BLACK                        = 409,  --拉黑名单
    FRIEND_BLACK_LIST                   = 410,  --黑名单列表
    FRIEND_BLACK_DELE                   = 411,  --从黑名单移除
    FRIEND_NEW_CHAT_LIST                = 412,  --获取好友私聊消息
    FRIEND_FOE                          = 413,  --添加仇敌
    FRIEND_FOE_LIST                     = 414,  --仇敌列表
    FRIEND_FOE_DELE                     = 415,  --删除仇敌
    FRIEND_FRIENDLY_RESP                = 416,  --通知好友度变化（单个好友）
    FRIEND_UNREAD_LIST                  = 417,  --获取未读消息总数（全部好友）
    FRIEND_ONLINE                       = 418,  --好友上线通知
    FRIEND_OFFLINE                      = 419,  --好友下线通知
    --FRIEND_FRIENDLY_CHANGE              = 420,  --好友度更新（单个好友）


    -- FRIEND_ACK_ADD_FRIEND               = 402,  --回应申请加好友
    -- FRIEND_FIND_BY_DBID                 = 403,  --根据dbid查找玩家信息
    -- FRIEND_DEL_FRIEND                   = 404,  --删除好友
    -- FRIEND_GET_LIST                     = 405,  --获取好友列表
    -- FRIEND_FIND_BY_NAME                 = 406,  --根据名字来查找玩家信息

    
    --竞技场-6v6多人pvp-激战永恒之柱-据点-跨服 501-550
    ARENA_POINT_MATCH_REQ              = 501,  --申请匹配-跨服
    ARENA_POINT_CANCEL_MATCH_REQ       = 502,  --取消申请匹配-跨服
    ARENA_POINT_QUIT_REQ               = 503,  --退出副本-跨服

    --跨服系统 551-600
    CROSS_SERVER_ENTER       = 551,  --进入某个玩法的跨服服务器
    CROSS_SERVER_BACK        = 552,  --从跨服回来本服
    
    --道具和装备系统 601-700
    --EQUIP_STREN              = 601,    --装备强化
    --EQUIP_WASH               = 602,    --装备洗练
    --EQUIP_WASH_SAVE          = 603,    --确认洗练结果
    --EQUIP_GEM_LOAD           = 604,    --宝石镶嵌
    --EQUIP_GEM_UNLOAD         = 605,    --宝石卸下
    EQUIP_GEM_COMPOUND       = 606,    --普通宝石合成
    EQUIP_GEM_COMPOUND_FAST  = 607,    --普通宝石合成快速
    EQUIP_LGEM_EAT           = 608,    --传奇宝石吞噬
    EQUIP_LGEM_LVLUP         = 609,    --传奇魔石升级
    EQUIP_LGEM_LOCK          = 610,    --传奇魔石加锁
    EQUIP_LGEM_UNLOCK        = 611,    --传奇魔石解锁
    EQUIP_LOAD               = 612,    --穿装备
    EQUIP_UNLOAD             = 613,    --脱装备
    EQUIP_MAKE               = 614,    --制造装备
    ITEM_GIVE_ITEM_TO_NPC    = 615,    --上交道具给NPC
    ITEM_BUY_BAG_COUNT       = 616,    --买背包格子
    ITEM_BUY_WAREHOUSE_COUNT = 617,    --买仓库格子
    ITEM_SORT_REQ            = 618,    --整理道具
    ITEM_MOVE_REQ            = 619,    --移动道具
    ITEM_SELL_REQ            = 620,    --道具出售
    --ITEM_BREAK_REQ         = 621,    --装备分解
    ITEM_USE_REQ             = 622,    --道具使用
    --EQUIP_REPAIR_REQ       = 623,    --道具修复
    --EQUIP_REPAIR_ONE_KEY_REQ = 624,  --一键修理
    --EQUIP_INHER            = 625,    --继承
    --EQUIP_ENCHANT_REQ      = 626,    --附魔
    ITEM_USE_LITTLE_SHOE     = 627,    --小飞鞋
    ITEM_PKG_SPLIT_REQ       = 628,    --背包拆分
    ITEM_GUIDE_BUY           = 629,    --引导购买
    ITEM_DROP_ITEMS          = 630,    --道具使用。--开xxx道具得到xxxs的道具，客户端要弹框等
    ITEM_NOTIFY_GET_MONEY    = 631,    --通知客户端获得钻石和绑钻{item_id,item_count,get_way}
    ITEM_NOTIFY_COST_MONEY   = 632,    --通知客户端消耗钻石和绑钻{item_id,item_count,cost_way,add_item_id,add_count}
    

    --副本系统 651-700
    INSTANCE_ENTER                  = 651,    --进入副本
    INSTANCE_EXIT                   = 652,    --离开副本
    INSTANCE_TRIGGER_EVENT_TO_CELL  = 655,    --客户端触发事件
    INSTANCE_CLIENT_TELEPORT        = 662,    --客户端传送
    --INSTANCE_SERVER_SETTLE        = 680,    --服务器结算
    --INSTANCE_SERVER_SETTLE_FAIL   = 681,    --服务器结算
    CLIENT_MAP_ENTER                = 682,    --客户端直接进入地图,主城或者野外，非副本。
    --IMAGE_MAP_ENTER               = 683,    --不用。当前位置进入镜像副本,主城或者野外。
    --IMAGE_MAP_EXIT                = 684,    --不用。当前位置退出镜像副本回到上一个原副本,主城或者野外。
    --EVENT_ACTIVITY_MAP_ENTER      = 685,    --镜像副本类似。专门给野外事件进入
    --INSTANCE_CLIENT_SETTLE        = 686,    --客户端副本结算
    --INSTANCE_NIGHTMARE            = 687,    --通知客户端噩梦副本id，每周
    --CLIENT_KILL_MONSTER           = 688,    --客户端杀怪物给奖励(index,item_id)
    INSTANCE_NOTIFY_WORLD_BOSS_FAKE = 686,    --支线世界boss 
    INSTANCE_EXP_NOTIFY_ROOM_STAGE  = 687,    --房间阶段
    INSTANCE_EXP_ENTER              = 689,    --进入经验副本
    INSTANCE_EXP_REWARD             = 690,    --经验本杀怪奖励
    INSTANCE_EXP_INFO               = 691,    --经验副本信息
    INSTANCE_EXP_NOTIFY_INFO        = 692,    --把经验副本信息同步到客户端
    INSTANCE_SETTLE                 = 693,    --普通副本结算
    CLIENT_LOADING_COMPLETED        = 694,    --客户端加载完(进度100)
    --NOTIFY_CLIENT_LOADING         = 695,    --如果多人，需要通知进度    
    INSTANCE_NOTIFY_ROOM_STAGE      = 696,    --通知副本stage和开始时间。{1:stage,2:start_time}
    INSTANCE_NOTIFY_KILL_MONSTERS   = 697,    --通知副本当前已经杀的怪物。{monster_id:num}
    INSTANCE_EXP_SETTLE             = 698,    --经验副本结算
    INSTANCE_EXP_INSPIRE            = 699,    --经验副本鼓舞
    INSTANCE_EXP_BUY_ENTER_NUM      = 700,    --购买经验副本次数

    --组队统 701-800
    TEAM_CREATE                  = 701,  --建立组队
    TEAM_JOIN                    = 702,  --加入队伍
    TEAM_LEAVE                   = 703,  --离开队伍
    TEAM_DISMISS                 = 704,  --解散组队
    TEAM_KICK                    = 705,  --剔除成员
    TEAM_INVITE                  = 706,  --邀请成员
    TEAM_ACCEPT                  = 707,  --接受邀请
    TEAM_START                   = 708,  --开始进入副本 询问队友是否同意
    TEAM_AGREE                   = 709,  --队员同意进入副本
    TEAM_GIVE_RIGHT              = 710,  --给队员提权
    TEAM_CHANGE_CAPTAIN          = 711,  --换队长
    TEAM_CALL                    = 712,  --召唤一队友  --队长才能召唤
    TEAM_CALL_ALL                = 713,  --召唤一所有人 --队长才能召唤
    TEAM_CALL_ACCEPT             = 714,  --是否同意召唤
    TEAM_SEND                    = 715,  --传送到一队友
    TEAM_NEAR_PLAYERS            = 716,  --附近玩家
    TEAM_NEAR_TEAMS              = 717,  --附近队伍。page=0不分页
    TEAM_FRIEND_TEAMS            = 718,  --好友队伍
    TEAM_GUILD_TEAMS             = 719,  --公会队伍
    TEAM_CHANGE_TARGET_ID        = 720,  --改变队伍目标
    TEAM_APPLY                   = 721,  --申请组队
    TEAM_ACCEPT_APPLY            = 722,  --队长审批是否同意申请组队
    TEAM_SET_AUTO_ACCEPT_APPLY   = 723, --设置队长是否自动同意申请
    TEAM_ALL_TEAMS               = 724,   --全部队伍。和附近队伍一致。page=0不分页
    TEAM_RECENT_PLAYERS          = 725,  --最近组队玩家
    TEAM_MATCH                   = 726,  --队伍匹配
    TEAM_CANCEL_MATCH            = 727,  --取消匹配
    TEAM_APPLY_BY_DBID           = 728,  --申请入队(dbid)
    TEAM_ALL_MEMBER_AGREE        = 729,  --通知所有成员即将进入副本（队伍或匹配）客户端停自动任务
    
    --
    TEAM_RECEIVE_INVITE          = 740,  --客户端收到邀请
    TEAM_RECEIVE_START           = 741,  --客户端收到进入副本询问(队长询问)
    TEAM_RECEIVE_CALL            = 742,  --客户端收到召唤请求
    TEAM_RECEIVE_APPLY           = 743,  --客户端队长收到玩家申请请求
    TEAM_RECEIVE_AGREE_RESULT    = 744,  --客户端收到进入副本同意结果
    --
    TEAM_REFRESH_ALL             = 770,  --客户端刷新自己队伍的所有数据
    TEAM_REFRESH_ONLINE          = 771,  --客户端刷新某成员online。
    TEAM_REFRESH_ADD             = 772,  --客户端刷新某成员加入
    TEAM_REFRESH_DEL             = 773,  --客户端刷新刷新某成员删除
    TEAM_REFRESH_QUIT            = 774,  --客户端收到退出队伍(reason,主动退出，被踢出等)。TEAM_REFRESH_DEL是其他玩家收到删除，这个是自己收到退出了队伍。--应该命名为receive_quit,因为这个不是刷新数据
    TEAM_REFRESH_CAPTAIN_DBID    = 775,  --客户端刷新队伍队长的改变。
    TEAM_REFRESH_TARGET_ID       = 776,  --客户端刷新队伍目标的改变。
    TEAM_REFRESH_POS             = 777,  --客户端刷新某成员pos。
    TEAM_REFRESH_HP              = 778,  --客户端刷新某成员cur_hp,max_hp。
    TEAM_REFRESH_MAP_ID          = 779,  --客户端刷新某成员map_id,map_line。
    TEAM_REFRESH_LEVEL           = 780,  --客户端刷新某成员level。
    TEAM_REFRESH_FIGHT_FORCE     = 781,  --客户端刷新某成员fight_force。
    TEAM_REFRESH_GUILD_ID        = 782,  --客户端刷新某成员guild_dbid。--guild_name需要去公会系统根据guild_dbid额外去取。
    TEAM_REFRESH_AUTO_ACCEPT_APPLY = 783, --客户端刷新队伍auto_accept_apply
    TEAM_REFRESH_MP              = 784,  --客户端刷新某成员cur_mp,max_mp。
    TEAM_REFRESH_ITEM_COUNT      = 785,  --客户端刷新某成员item_counts。
    TEAM_REFRESH_FACADE          = 786,  --客户端刷新某成员facade
    TEAM_REFRESH_EXP_INSTANCE_ENTER_TIMES = 787,  --客户端刷新某成员exp_instance_enter_times。
    TEAM_REFRESH_EQUIPMENT_INSTANCE_PASS_NUM = 788, --客户端刷新某成员装备副本通关次数 助战次数

    --任务系统 801-900
    TASK_ACCEPT               = 801,  --接任务
    TASK_COMMIT               = 802,  --提交任务
    TASK_ABORT                = 803,  --放弃任务
    TASK_PROCESSING_LIST      = 804,  --获取正在处理的任务列表。任务状态为可接,已接，已完成
    TASK_COMMITED_LIST_BRANCH = 805,  --获取已经提交的支线任务列表。
    TASK_SET_TRACK            = 806,  --设置追踪
    --
    TASK_REFRESH_PROCESSING   = 830,  --任务刷新，完成和进度。如果有,那么是更新，没有,那就是新加的进行中的任务。
    TASK_DEL_PROCESSING       = 831,  --删除进行中的任务。放弃，或者已提交。reason=0为放弃，reason=1为提交
    TASK_ADD_COMMITED_BRANCH  = 832,  --增加已提交的支线任务。
    TASK_REFRESH_PROCESSING_TRACK = 833, --追踪更新,和任务状态等比是独立的一个变量处理


    --吟唱系统 901-1000
    SING_BREAK                 = 901,  --打断指定的吟唱功能

    --技能系统 1001-1100
    -- SKILL_CHANGE_SLOT = 1001, --改变技能槽位
    -- SKILL_CHANGE_AUTO_CAST = 1002, --设置是否自动释放
    -- SPELL_LEARN_REQ    = 1001, --技能学习
    -- SPELL_EQUIP_REQ    = 1002, --技能装备
    -- SPELL_SP_BACK_REQ    = 1003, --技能点返还
    -- SPELL_RUNE_EQUIP_REQ = 1004, --符文装备
    -- SPELL_RUNE_TASK_REFRESH = 1005, --符文刷新状态
    -- SPELL_RUNE_GET_INFO     = 1006,  --获取符文的相关信息
    
    
    --飞行系统 1101-1200
    --FLY_CAST_REQ    = 1101, --飞行请求
    
    --事件活动 1201-1300
    --EVENT_ACTIVITY_LIST_REQ            = 1201, --个人的事件活动，看成多个EVENT_ACTIVITY_REFRESH。上线主动推送
    --EVENT_ACTIVITY_REFRESH             = 1202, --没就加，有就改进度
    --EVENT_ACTIVITY_DEL                 = 1203, --删除
    --EVENT_ACTIVITY_DAILY               = 1204, --随机的日常事件活动字典。上线主动推送
    --EVENT_ACTIVITY_ENTER               = 1205, --进入区域。前提客户端对应的事件活动id的进度等客户端已知。
    --EVENT_ACTIVITY_LEAVE               = 1206, --离开区域
    --EVENT_ACTIVITY_REWARD              = 1207, --完成奖励

    --pvp 1301-1400
    --EVENT_PVP_MATCH_REQ         = 1301, --pvp 匹配请求
    --EVENT_PVP_MATCH_CANCEL_REQ         = 1302, --pvp 取消匹配请求
    --EVENT_PVP_MATCH_BASE_INFO     = 1303, --获取场景基本信息(双方阵营积分，剩余时间)(to cell)
    --EVENT_PVP_FORT_INFO     = 1304, --据点详细信息
    --EVENT_PVP_PLAYER_LIST_REQ   = 1305, --获取玩家列表
    --EVENT_PVP_SETTLE   = 1306, --结算消息
    --EVENT_PVP_TEAM_MATCH_REQ       = 1307, --队伍匹配
    --EVENT_PVP_GET_PLAYER_INFO      = 1308, --获取玩家自身信息
    --EVENT_PVP_GET_BATTLE_REPORT    = 1309, --获取战报
    --EVENT_PVP_GET_PLAYER_RANK      = 1310, --获取玩家排行
    EVENT_MATCH_INFO                 = 1311, --匹配信息
    --EVENT_PVP_MATCH_REWARD         = 1312, --获取匹配的奖励

    --时装系统 1401-1500
    FASHION_ACTIVATE                = 1401,   --激活时装
    FASHION_LOAD                    = 1402,   --穿时装
    FASHION_UNLOAD                  = 1403,   --脱时装
    FASHION_UPGRADE_STAR            = 1404,   --升星
    FASHION_UPGRADE_LEVEL           = 1405,   --升级
    FASHION_BREAK_DOWN              = 1406,   --分解
    --FASHION_SET_SHOW_TYPE           = 1403,   --设定外观显示类型
    --FASHION_COLOR_NORMAL            = 1404,   --普通染色
    --FASHION_COLOR_HIGH              = 1405,   --高级染色

    --守护灵系统 1501-1600
    --EUDEMON_SET_CAPTAIN             = 1501,   --设定守护灵队长
    --EUDEMON_PLAYED                  = 1502,   --守护灵上阵
    --EUDEMON_UN_PLAYED               = 1503,   --守护灵下阵
    --EUDEMON_UPGRADE_LEVEL           = 1504,   --守护灵升级
    --EUDEMON_UPGRADE_STAR            = 1505,   --守护灵升星
    --EUDEMON_CALL                    = 1506,   --守护灵召唤
    --EUDEMON_SKILL_LEARN             = 1507,   --守护灵技能学习
    --EUDEMON_SKILL_LOCK              = 1508,   --守护灵技能锁定
    --EUDEMON_SKILL_UNLOCK            = 1509,   --守护灵技能解除锁定

    --好友切磋 1601-1700
    FIGHT_FRIEND_APPLY_FIGHT  = 1601,   --切磋申请
    FIGHT_FRIEND_CANCEL_FIGHT = 1602,   --撤销申请
    FIGHT_FRIEND_ACK_FIGHT    = 1603,   --回应申请
    FIGHT_FRIEND_SCENE_INFO   = 1604, --获取场景基本信息(场景状态，剩余时间)(to cell)
    FIGHT_FRIEND_SETTLE       = 1605, --结算消息

    --商城系统 1701-1800
    SHOP_BUY_ITEM         = 1701,   --商城购买
    --SHOP_DIAMOND_TO_GOLD    = 1702,   --商城钻石兑换金币
    --SHOP_GET_SELL_LIMIT   = 1703,   --商城抢购物品已卖数量信息
    --SHOP_SILVER_CHANGE      = 1704,   --银元兑换
    --SHOP_SILVER_CHANGE_INFO = 1705,   --银元兑换信息

    --交易行系统 1801-1900，

    --野外宝箱 1901-2000
    WILD_TREASURE_INFO      = 1901,  --宝箱信息
    WILD_TREASURE_PICK      = 1902,  --拾取宝箱
    WILD_TREASURE_DEL       = 1903,  --删除宝箱
    
    --公会 2201-2300
    GUILD_BUILD                         = 2201,    --创建公会
    GUILD_QUICK_JOIN                    = 2202,    --快速加入
    GUILD_GET_LIST                      = 2203,    --获取公会列表
    GUILD_SET_SETTING                   = 2204,    --设置是否检查自动审批 auto_approve
    GUILD_SET_ANNOUNCEMENT              = 2205,    --设置公会公告
    --GUILD_GET_SIM_INFO                = 2206,    --获取公会简单信息
    GUILD_GET_INFO                      = 2207,    --获取公会详细信息
    --GUILD_GET_PERSONAL_INFO           = 2208,    --获取公会的个人信息
    GUILD_GET_DETAIL_INFO               = 2209,    --公会成员获取所属公会的信息
    GUILD_APPLY                         = 2210,    --申请加入公会
    GUILD_GET_APPLY_LIST                = 2211,    --获取申请列表
    GUILD_REPLY_APPLY                   = 2212,    --回复申请
    GUILD_SET_POSITION                  = 2213,    --设置职位
    GUILD_KICK                          = 2214,    --公会踢人
    GUILD_QUIT                          = 2215,    --退出公会
    GUILD_DISMISS                       = 2216,    --解散公会
    GUILD_BROADCAST_SETTING             = 2217,    --设置公会公告
    GUILD_NOTIFY_APPLY                  = 2218,    --有人申请加入公会后，通知管理员
    GUILD_INVITE                        = 2219,    --邀请加公会
    GUILD_REPLY_INVITE                  = 2220,    --回复邀请加公会
    GUILD_DEPOSE                        = 2221,    --罢免
    GUILD_NOTIFY_JOIN_IN                = 2222,    --通知申请玩家已经通过
    GUILD_NOTIFY_KICKED                 = 2223,    --通知已经被踢出来
    --GUILD_SIGN_ON                     = 2224,    --签到
    --GUILD_SIGN_ON_GET_REWARD          = 2225,    --获取签到的宝箱奖励
    --GUILD_SIGN_ON_COUNT               = 2226,    --获取当前公会的签到人数
    --GUILD_SEND_RED_ENVELOPE           = 2227,    --玩家发红包
    --GUILD_SEND_RED_ENVELOPE_NOTIFY    = 2228,    --玩家发红包后通知所有公会成员
    --GUILD_GET_RED_ENVELOPE            = 2229,    --抢红包
    --GUILD_GET_RED_ENVELOPE_LIST       = 2230,    --获取公会现有的红包列表
    --GUILD_GET_SEND_RED_ENVELOPE_LIST  = 2231,    --获取公会已发的红包列表
    --GUILD_HAVE_RED_ENVELOPE_NOTIFY    = 2232,    --玩家有可以放送的红包提醒
    --GUILD_GET_RED_ENVELOPE_THANKS     = 2233,    --收到红包快速答谢
    --GUILD_SIGN_RED_ENVELOPE_RECORD    = 2234,    --签到红包发送记录
    GUILD_SKILL_LEVEL_UP                = 2235,    --公会技能升级
    GUILD_GET_SKILL_INFO                = 2236,    --获取公会技能信息
    --GUILD_SHOP_GET_INFO               = 2237,    --获取公会商店
    --GUILD_SHOP_BUY                    = 2238,    --获取公会商店买东西
    --GUILD_TERRITORY_ENTER             = 2239,    --公会领地
    --GUILD_GET_RED_ENVELOPE_INFO       = 2240,    --获取红包的具体信息
    GUILD_GET_MEMBER_INFO               = 2241,    --获取其他公会的成员信息
    GUILD_GET_SELF_MEMBER_INFO          = 2242,    --获取自己公会公会的成员信息
    GUILD_GET_RANK                      = 2243,    --获取公会排行榜
    GUILD_GET_INFO_BY_NAME              = 2244,    --通过名字搜索
    --GUILD_GET_RAID_INFO               = 2245,    --获取raid开启，通过情况
    --GUILD_GET_RAID_DETAIL_INFO        = 2246,    --获取raid具体信息
    --GUILD_ENTER_RAID                  = 2247,    --进入公会boss
    --GUILD_RAID_BOSS_INFO              = 2248,    --raid副本boss的信息
    --GUILD_RAID_HARM_RANK              = 2249,    --raid伤害排名
    --GUILD_GET_DONATE_INFO             = 2250,    --获取捐赠信息
    --GUILD_DO_DONATE                   = 2251,    --捐赠
    --GUILD_GET_DONATE_RANK             = 2252,    --获取捐赠排行榜
    GUILD_GET_ONLINE_INFO               = 2253,    --获取所有在线成员列表
    GUILD_REPLY_APPLY_ALL               = 2254,    --回复申请,所有
    GUILD_GET_DAILY_REWARD              = 2255,    --公会每日奖励
    GUILD_WAREHOUSE_PKG                 = 2256,    --公会仓库。字典，pos为key，和玩家背包一致.
    GUILD_WAREHOUSE_SELL                = 2257,    --捐赠。如果成功，再刷一遍仓库背包。
    GUILD_WAREHOUSE_BUY                 = 2258,    --兑换。如果成功，再刷一遍仓库背包。
    GUILD_WAREHOUSE_BUY_PET             = 2259,    --兑换宠物丹。
    GUILD_WAREHOUSE_DESTROY             = 2260,    --销毁
    GUILD_WAREHOUSE_RECORD              = 2261,    --公会记录
    GUILD_RED_ENVELOPE_LIST             = 2262,    --获取公会所有的红包列表
    GUILD_SEND_SYS_RED_ENVELOPE         = 2263,    --玩家发系统红包
    GUILD_SEND_MONEY_RED_ENVELOPE       = 2264,    --玩家发money红包
    GUILD_GRAB_RED_ENVELOPE             = 2265,    --玩家抢红包。抢红包那刻才随机金钱。
    GUILD_RED_ENVELOPE_RECORD           = 2266,    --红包记录
    GUILD_NOTIFY_ADD_RED_ENVELOPE       = 2267,    --通知玩家加红包，加或者修改
    GUILD_NOTIFY_UPDATE_RED_ENVELOPE    = 2268,    --通知玩家更新红包，加或者修改
    GUILD_NOTIFY_DEL_RED_ENVELOPE       = 2269,    --通知玩家删除红包 {红包id}
    GUILD_APPROVE_LEVEL_AND_FIGHT       = 2270,    --设置审批最小等级，最小战力
    GUILD_LEVEL_UP                      = 2271,    --公会升级

    GUILD_MONSTER_GET_DATA              = 2272, --获取公会精英怪刷新列表
    GUILD_MONSTER_ENTER_REQ             = 2273, --进入世界boss（公会精英怪）

    GUILD_BEAST_START                   = 2280, --开启公会神兽挑战
    GUILD_BEAST_ENTER                   = 2281, --申请进入公会神兽
    GUILD_BEAST_SUBMIT_SOUL             = 2282, --提交灵魂
    GUILD_BEAST_GET_INFO                = 2283, --获取{次数，兽粮，开始时间}
    GUILD_BEAST_GET_BOSS_HP             = 2284, --获取{boss最大血量,当前血量} 如果没开启返回{1,1}
    GUILD_BEAST_NOTIFY_ROOM_STAGE       = 2285, --击杀boss的时间戳
    GUILD_BEAST_BROADCAST_START         = 2286, --通知公会成员公会神兽开启

    GUILD_GUARD_ENTER                   = 2290, --进入守卫公会
    GUILD_GUARD_NOTIFY_START_TIME       = 2291, --开始时间
    GUILD_GUARD_NOTIFY_GODDESS_INFO     = 2292, --神像eid 
    GUILD_GUARD_NOTIFY_DAMAGE_RANK      = 2293, --伤害排行
    GUILD_GUARD_NOTIFY_KILL_INFO        = 2294, --波数杀怪信息
    GUILD_GUARD_NOTIFY_SETTLE           = 2295, --结算信息
    
    --成就 2701-2800
    ACHIEVEMENT_LIST_REQ            = 2701,
    ACHIEVEMENT_REFRESH             = 2702,
    ACHIEVEMENT_COMMIT              = 2703, 

    --天梯商店 2801-2900
    BUY_LADDER_SHOP_ITEM_REQ        = 2801,
    REFRESH_LADDER_SHOP_REQ         = 2802,
    
    --天梯赛1v1 2901-3000
    LADDER_MATCH_REQ                = 2901,
    LADDER_CANCEL_MATCH_REQ         = 2902,
    LADDER_MATCH_SETTLE             = 2903, --结算
    LADDER_MATCH_SCENE_INFO         = 2904, --场景基本信息(场景状态，剩余时间)
    LADDER_MATCH_LOADING_INFO       = 2905, --每个玩家加载广播

    --排行榜 3001-3100
    --GET_LADDER_RANK_LIST_REQ           = 3001, --请求天梯排行榜信息
    --GET_AION_RANK_LIST_REQ               = 3002, --请求永恒之塔排行榜
    RANK_LIST_GET_REQ                    = 3003, --统一请求排行榜
    RANK_LIST_POINT                      = 3004, --点赞               

    --观战 3101-3200
    WATCH_WAR_LIST_REQ                   = 3101,
    WATCH_WAR_REQ                        = 3102,
    
    --翅膀养成 3201-3300
    WING_LOAD                   = 3201, --佩戴翅膀
    WING_UNLOAD                 = 3202, --卸下翅膀
    WING_STAR_UP                = 3203, --提升(是否自动使用钻石购买幻羽),星级，加经验，暴击经验，自动升星到10级，10级后停止，等升等阶
    WING_STAR_UP_MORE           = 3204, --提升十次(是否自动使用钻石购买幻羽)
    WING_LEVEL_UP               = 3205, --升等阶(是否自动使用钻石购买幻羽),要求当前星级为10，同样加经验，扣经验
    WING_GEM_LOAD               = 3206, --宝珠镶嵌(hole_pos,gem_pos)
    WING_GEM_UNLOAD             = 3207, --宝珠卸下(hole_pos)    
    WING_GEM_COMPOUND           = 3208, --宝珠合成(gem_pos1,gem_pos2)
    WING_RARE_LOAD              = 3209, --幻化稀有翅膀(wing_rare_id)
    WING_RARE_UNLOAD            = 3210, --卸下稀有翅膀
    
    --魂器系统 3301-3400
    EQUIP_SOUL_LOAD                 = 3301,   --穿魂器
    EQUIP_SOUL_UNLOAD               = 3302,   --脱魂器
    EQUIP_SOUL_EAT                  = 3303,   --魂器吞噬
    EQUIP_SOUL_SKILL_RESET          = 3304,   --魂器技能重铸
    

    --永恒之塔 3401 - 3500
    AION_ENTER              = 3401,     --进入副本
    AION_NOTIFY_ROOM_STAGE  = 3402,     --通知副本状态改变
    AION_SETTLE             = 3403,     --结算返回
    
    
    -- 3501 - 3502
    AVATAR_ON_LEVEL_UP      = 3501,     --玩家升级
    AVATAR_UPDATE_ADD_RATE  = 3502,     --更新玩家身上的加成数据

    --世界boss 3601 - 3700
    WORLD_BOSS_GET_DATA = 3601,     --获取具体信息
    WORLD_BOSS_ENTER    = 3602,     --进入申请
    WORLD_BOSS_INTREST  = 3603,     --申请关注/取关
    WORLD_BOSS_NOTIFY_BOSS_WILL_REBORN = 3604, --通知boss就快刷出
    WORLD_BOSS_SPACE_BOSS_INFO = 3605, --场景内boss刷新信息
    WORLD_BOSS_ON_KILL_BOSS = 3606, --玩家手刃boss时加疲劳值
    WORLD_BOSS_GET_KILLER_RECORDS = 3607, --获取某boss杀死记录当天的
    
    --深渊玩法 3701-3800
    ABYSS_ENTER             = 3701,    --进入(层数)
    ABYSS_BUFFS             = 3702,    --随机的buffs {buffid:1} 上线和改变主动推送给客户端.周buffs
    ABYSS_SETTLE            = 3703,    --结算。room_info的key。
    ABYSS_SPACE_INFO        = 3704,    --场景信息(场景状态，开始时间)，过程中更新状态和时间。room_info的key。
    ABYSS_SPACE_INFO_ENTER  = 3705,    --场景信息（层次和随机的buff{buffid:1}），进入第一次给的信息。room_info的key。
    ABYSS_SPACE_INFO_SCORE  = 3706,    --场景信息(分数)，过程中更新。room_info的key。
    ABYSS_TEAM_ENTER        = 3707,    --组队进入(层数)
    
    --声望    3801 -3802
    PRESTIGE_SHOP_BUY      = 3801,  --声望商店买物品

    --活动信息 3810 - 3820
    --GET_ACTIVITY_TIME         = 3810,    --获取活动时间信息
    --GET_ALL_ACTIVITY_TIME     = 3811,    --获取所有活动时间信息

    --攻略讨论 3901-3920
    DISCUSSION_GET_INFO     = 3901,
    DISCUSSION_ADD_INFO     = 3902,
    DISCUSSION_GIVE_SUPPORT = 3903,

    --生活技能任务 3921-3940
    --LIFE_SKILL_TASK_OEPN     = 3921, --打开界面获取信息
    --LIFE_SKILL_TASK_SEEK     = 3922, --申请求助
    --LIFE_SKILL_TASK_SUBMIT   = 3923, --提交道具

    --竞技信息 4001-4010
    --GET_ATHLETICS_TIME          = 4001,     --获取竞技时间信息
    --GET_ALL_ATHLETICS_TIME      = 4002,     --获取所有竞技时间信息

    -- 占星抽奖 4011- 4050
    DIVINE_REQ = 4011,          --抽一次
    DIVINE_MULTI_REQ = 4012,    --抽5次
    DIVINE_GET_REWARD = 4013,   --领取累计奖励  参数：多少次
    DIVINE_GET_INFO = 4014,     --获取占星信息

    --赏金赛 4051 - 4100
    BOUNTY_APPLY = 4051,            --报名
    BOUNTY_BUY_TICKET = 4052,        --购买赏金券
    BOUNTY_MATCH_REQ = 4053,        --单人匹配
    BOUNTY_MATCH_CANCEL_REQ = 4054,  --取消匹配
    BOUNTY_GET_PLAYER_INFO = 4055,  -- 获取玩家赏金赛信息 
    BOUNTY_MATCH_INFO = 4056,       --匹配消息
    BOUNTY_MATCH_BASE_INFO = 4057,  --获取场景基本信息
    BOUNTY_PLAYER_LIST_REQ = 4058,  -- 获取玩家列表
    BOUNTY_MATCH_SETTLE = 4059,     --结算消息
    BOUNTY_PICK_FLAG = 4060,        --拾取旗帜
    BOUNTY_GET_REWARD = 4061,       --领取奖励
    BOUNTY_GET_CELL_PLAYER_INFO = 4062, --获取玩家cell信息

    --随从 悬赏 4101-4200
    DUDE_RECEIVE_TASK = 4101,   --领取任务
    DUDE_START_TASK = 4102,     --开始任务
    DUDE_REFRESH_TASK = 4103,   --刷新任务
    DUDE_GET_TASK_REWARD = 4104, --领取任务完成奖励
    DUDE_TASK_ADD_SPEED = 4105, --加速任务
    DUDE_GET_ASSIST_LIST= 4106, --求助列表  
    DUDE_SET_ASSIST = 4108,    --设置助战
    DUDE_CANCEL_ASSIST = 4109, -- 取消出战
    DUDE_UPGRADE_RANK = 4110,   --升职称
    DUDE_REPEAT_GET_BREAK_DOWN_CHIP_RESP = 4111, -- 重复获得分解成对应数量的碎片
    DUDE_TASK_FINISHED_RESP = 4112,  --任务完成
    DUDE_UPGRADE_BUILD_RESP = 4113,  --建筑升级
    DUDE_GET_PARTY_REWARD = 4114,   --领取营地建筑奖励

    ----坐骑 4201-4300
    --RIDE_ACTIVE         = 4201, --激活(pos)
    --RIDE_UNACTIVE       = 4202, --取消激活
    --RIDE_BUY            = 4203, --购买格子
    --RIDE_DEL            = 4204, --删除坐骑
    --RIDE_RIDE           = 4205, --上马
    --RIDE_UNRIDE         = 4206, --下马
    
    --pk mode 4301-4400
    PK_MODE_SET_MODE         = 4301, --设置pk模式(mode)
    
    --翅膀 法宝 神兵 披风 4401-4500
    TREASURE_CHANGE_HIDE     = 4401, --隐藏状态
    
    --宠物 4501-4600
    PET_STAR_UP              = 4501, --升星(item_pos)
    PET_LEVEL_UP             = 4502, --升级 (item_pos_list)吞噬 
    --PET_SHOW               = 4503, --显示模型(grade)
    --PET_UNSHOW             = 4504, --不显示模型()
    
    --坐骑 4601-4700
    HORSE_STAR_UP              = 4601, --升星(item_pos)
    HORSE_SHOW                 = 4602, --界面显示(grade)
    --HORSE_UNSHOW             = 4603, --界面不显示()
    HORSE_RIDE                 = 4604, --骑模型()
    HORSE_UNRIDE               = 4605, --不骑模型()
    
    --称号系统 4701-4800
    TITLE_TASK_LIST_REQ             = 4701,
    TITLE_TASK_REFRESH              = 4702,
    TITLE_TASK_DEL                  = 4703,
    TITLE_SET_TITLE_ID              = 4704,
    TITLE_UNSET_TITLE_ID            = 4705,
    
    --爵位系统 4801-4810
    NOBILITY_UPGRADE_REQ            = 4801, --升爵位

    --复活机制 4811-4820
    MGR_REVIVE_APPLY_REVIVE         = 4811, --申请复活
    MGR_REVIVE_ON_AVATAR_DIE        = 4812, --当玩家被杀死，告诉前端被谁杀等信息
    MGR_REVIVE_GET_DIE_MESSAGE      = 4813, --获取死亡信息
    
    --装备强化 4851-4900
    EQUIP_STREN                     = 4851, --强化
    EQUIP_STREN_AUTO                 = 4852, --自动强化
    
    --装备洗练 4901-4950
    EQUIP_WASH                      = 4901, --洗练
    EQUIP_WASH_OPEN_SLOT            = 4902, --开启槽位
   
    --装备宝石 4951-5000
    EQUIP_GEM_LOAD                  = 4951, --镶嵌
    EQUIP_GEM_UNLOAD                = 4952, --拆除
    EQUIP_GEM_REFINE                = 4953, --精炼
    
    --技能 5001-5050
    SKILL_CHANGE_SLOT = 5001,      --改变技能槽位
    SKILL_CHANGE_AUTO_CAST = 5002, --设置是否自动释放

    --天赋 5051 6000
    TALENT_LEVEL_UP = 5051, --天赋加点
    TALENT_RESET    = 5052, --重置

    --守护结界塔 6001 - 6050
    HOLY_TOWER_ENTER            = 6001,     --进入副本
    HOLY_TOWER_BUY_ENTER_NUM    = 6002,     --购买次数
    HOLY_TOWER_SWEEP            = 6003,     --扫荡  
    HOLY_TOWER_SPACE_INFO       = 6004,     --副本状态变化
    HOLY_TOWER_SETTLE           = 6005,     --结算信息
    HOLY_TOWER_WAVE_CHANGE      = 6006,     --波数变化

    --符文 7001 - 7050
    RUNE_PUTON                  = 7001,     --穿戴符文
    RUNE_PUTOFF                 = 7002,     --取下符文
    RUNE_REPLACE                = 7003,     --替换已穿戴符文
    RUNE_UPLEVEL                = 7004,     --符文升级
    RUNE_EXPLODE                = 7005,     --符文分解
    RUNE_EXCHANGE               = 7006,     --符文兑换
    RUNE_SINGLE_LUCK            = 7007,     --符文寻宝(单次)
    RUNE_TEN_LUCK               = 7008,     --符文寻宝(十连)
    RUNE_LUCK_LIST              = 7009,     --符文寻宝记录（公共）
    RUNE_SET_REMIND_FLAG        = 7010,     --符文弹窗提醒标记
    RUNE_EXPLODE_LOT            = 7011,     --符文批量分解

    --合成 7101 - 7150
    COMPOSE_ITEM                = 7101,     --道具合成
    COMPOSE_EQUIP               = 7102,     --装备合成

    --挂机 7151 - 7200
    HANG_UP_CHANGE_AUTO_PICK        = 7151, --自动拾取
    HANG_UP_CHANGE_AUTO_SETTING     = 7152, --其他设置自动


    --循环任务 7201-7250
    CIRCLE_TASK_LIST_REQ             = 7201, --全部任务列表，服务器推送
    CIRCLE_TASK_REFRESH              = 7202, --更新任务状态，服务器推送
    CIRCLE_TASK_ACCEPT_REQ           = 7203, --接任务，随机一个任务来接。前提是现在没有已接任务。
    CIRCLE_TASK_COMMIT_REQ           = 7204, --提交任务。领取奖励
    CIRCLE_TASK_QUICK_COMPLETE       = 7205, --快速完成，包括提交
    CIRCLE_TASK_ONE_CLICK            = 7206, --一键完成全部完成剩余的赏金任务

    --boss之家 7251 - 7300
    BOSS_HOME_GET_DATA                = 7251,     --获取具体信息
    BOSS_HOME_ENTER                   = 7252,     --进入申请
    BOSS_HOME_INTREST                 = 7253,     --申请关注/取关
    BOSS_HOME_NOTIFY_BOSS_WILL_REBORN = 7254, --通知boss就快刷出
    BOSS_HOME_SPACE_BOSS_INFO         = 7255, --场景内boss刷新信息
    BOSS_HOME_GET_KILLER_RECORDS      = 7256, --获取某boss杀死记录当天的
    BOSS_HOME_ON_KILL_BOSS            = 7257, --击杀boss 归属者
    
    --个人boss 7301 - 7350
    PERSONAL_BOSS_ENTER             = 7301, --申请进副本
    PERSONAL_BOSS_NOTIFY_ROOM_STAGE = 7302, --通知副本状态
    PERSONAL_BOSS_SETTLE            = 7303, --结算 1：胜利 0:失败  
    
    --离线1v1 7351 - 7400
    OFFLINE_PVP_REFRESH               = 7351,   --刷新 --打开界面访问一次
    OFFLINE_PVP_CHALLENGE             = 7352,   --挑战。如果成功，修改get_info中的rank_index。如果要非常准确，别人挑战了，那么客户端只能打开界面后轮询了。
    OFFLINE_PVP_INSPIRE               = 7353,   --鼓舞。如果成功，修改get_info中的inspire_count
    OFFLINE_PVP_BUY                   = 7354,   --购买挑战次数
    OFFLINE_PVP_SETTLE                = 7355,   --领取结算奖励。如果成功，修改get_info中的settle_flag = 0
    OFFLINE_PVP_GET_INFO              = 7356,   --获取相关信息。--鼓舞次数，排名，结算标记。打开界面访问一次。还有22点后结算，打开界面后客户端定那个时刻，然后主动申请一下。
    OFFLINE_PVP_GET_REWARD            = 7357,

    --金币副本 7401 - 7450
    INSTANCE_GOLD_ENTER         = 7401, --进副本
    INSTANCE_GOLD_BUY_ENTER_NUM = 7402, --购买次数
    INSTANCE_GOLD_SWEEP         = 7403, --扫荡
    INSTANCE_GOLD_SPACE_INFO    = 7404,
    INSTANCE_GOLD_SETTLE        = 7405,
    INSTANCE_GOLD_WAVE_CHANGE   = 7406,
    INSTANCE_GOLD_NOTIFY_GOLD_COUNT = 7407, --通知金币数量     
    
    --护送女神 7501 - 7550
    GUARD_GODDNESS_GUARD          = 7501, --护送
    GUARD_GODDNESS_GUARD_FINISH   = 7502, --护送结算

    --装备副本 7551 - 7600
    INSTANCE_EQUIPMENT_NOTIFY_ROOM_STAGE    = 7551,    --通知房间状态变化
    --INSTANCE_EQUIPMENT_NOTIFY_WAVE          = 7552,    --通知波数变化
    INSTANCE_EQUIPMENT_NOTIFY_KILL_MONSTERS = 7553,    --通知波数、杀怪数量
    INSTANCE_EQUIPMENT_SETTLE               = 7554,    --通知结算
      
   --装备守护 7601 - 7650
   EQUIP_GUARD_BUY                          = 7601,    --续费

    --市场 7801 - 7850
    --SALE_PUBLISH_SELL_REQ       = 7801,     --商品上架（出售）
    --SALE_GET_ALL_SELL_LIST      = 7802,     --查看在售商品（全部）
    --SALE_BUY_REQ                = 7803,     --购买商品
    --SALE_GET_SELF_SELL_LIST     = 7804,     --查看我的出售
    --SALE_CANCEL_SELL_REQ        = 7805,     --取消出售
    --SALE_PUBLISH_BUY_REQ        = 7806,     --发布求购信息
    --SALE_GET_ALL_BUY_LIST       = 7807,     --查看求购信息（全部）
    --SALE_SELL_REQ               = 7808,     --求购交易
    --SALE_GET_SELF_BUY_LIST      = 7809,     --查看我的求购
    --SALE_CANCEL_BUY_REQ         = 7810,     --取消求购

    --活动模块 7851 - 7900
    ACTIVITY_POINT_REWARD_REQ   = 7851,     --领取活跃点奖励
    ACTIVITY_SHOW               = 7852,     --显示幻化
    ACTIVITY_UNSHOW             = 7853,     --不显示
    ACTIVITY_LEVEL_UP           = 7854,     --升级
 
    --VIP   7901 - 7950
    VIP_LEVEL_REWARD_REQ        = 7901,     --VIP等级奖励领取
    VIP_WEEK_REWARD_REQ         = 7902,     --VIP周奖励领取
    VIP_DAILY_REWARD_REQ        = 7903,     --VIP每日

    --蛮荒禁地 7951 - 8000
    FORBIDDEN_GET_DATA                  = 7951, --获取具体信息
    FORBIDDEN_ENTER                     = 7952, --进入申请
    FORBIDDEN_INTREST                   = 7953, --申请关注/取关
    FORBIDDEN_GET_KILLER_RECORDS        = 7954, --获取某boss杀死记录当天的
    FORBIDDEN_NOTIFY_BOSS_WILL_REBORN   = 7955, --通知boss就快刷出
    FORBIDDEN_SPACE_BOSS_INFO           = 7956, --场景内boss刷新信息
    FORBIDDEN_NOTIFY_CHANGE_ANGER       = 7957, --通知怒气值
    FORBIDDEN_NOTIFY_WILL_EXIT          = 7958, --通知怒气值已满，即将被踢出去
    FORBIDDEN_ON_KILL_BOSS              = 7959, --击杀boss 归属者

    --祈福 8001  -  8050
    PRAY_GOLD_REQ                       = 8001, --金币祈福
    PRAY_EXP_REQ                        = 8002, --经验祈福


    --跨服实时1v1 8051 - 8100
    CROSS_VS11_GET_DATA_REQ             = 8051, --获取数据
    CROSS_VS11_BEGIN_MATCH_REQ          = 8052, --参与匹配
    CROSS_VS11_CROSS_BUY_COUNT          = 8053, --购买跨服额外次数
    CROSS_VS11_GET_DAY_STAGE_RWD        = 8054, --领取每日段位奖励
    CROSS_VS11_GET_DAY_COUNT_RWD        = 8055, --领取每日参与次数奖励
    CROSS_VS11_GET_SEASON_GX_RWD        = 8056, --领取赛季功勋达标奖励
    CROSS_VS11_CANCEL_MATCH_REQ         = 8057, --取消匹配
    CROSS_VS11_SETTLE_RESP              = 8058, --结算
    CROSS_VS11_STAGE_RESP               = 8059, --战斗场景状态切换
    CROSS_VS11_GET_CROSS_RANK_REQ       = 8060, --获取跨服排行榜

    --装备进阶 8101- 8150
    EQUIP_ADVANCE_REQ                   = 8101, --装备进阶

    --资源找回 8151 - 8200
    REWARD_BACK_GET_INFO        = 8151, --获取找回信息
    REWARD_BACK_GET_REWARD      = 8152, --找回奖励

    --套装装备 8201 - 8250
    EQUIP_SUIT_UPLEVEL_REQ      = 8201, --套装装备升级

    --图鉴  8251 - 8300
    CARD_UPLEVEL_REQ            = 8251, --图鉴升级(含激活)
    CARD_GROUP_UPLEVEL_REQ      = 8252, --图鉴组合激活(升级)
    CARD_RESOLVE_REQ            = 8253, --图鉴碎片分解获得经验
    
    --化形 8301 - 8400
    ILLUSION_ACTIVATE           = 8301, --化形激活
    ILLUSION_SHOW               = 8302, --使用外形
    ILLUSION_UPGRADE_STAR       = 8303, --升星 翅膀法宝神兵披风
    ILLUSION_UPGRADE_GRADE      = 8304, --升阶 宠物 坐骑
    ILLUSION_LEVEL_UP           = 8305, --升级
    ILLUSION_BREAK              = 8306, --分解


    --采集 8401 - 8450
    COLLECT_PRE_REQ            = 8401, --预采集 转圈读条
    COLLECT_BREAK_REQ          = 8402, --中断采集
    COLLECT_REQ                = 8403, --真采集

    --青云之巅 8451 - 8500
    CROSS_CLOUD_PEAK_ENTER_REQ            = 8451, --申请进入青云之巅
    CROSS_CLOUD_PEAK_NOTIFY_SPACE_INFO    = 8452, --通知场景里信息
    CROSS_CLOUD_PEAK_NOTIFY_PASS_ALL      = 8453, --已经通关全部了 即将被踢出去
    
    --拍卖行 摆摊 8501 - 8550
    AUCTION_SELL_REQ        = 8501,    --卖(pos,count,price,passwd) --passwd用数字，不填或者-1为没有passwd
    AUCTION_ITEM_LIST       = 8502,    --摆摊列表(auction_type)
    AUCTION_ITEM_MY_LIST    = 8503,    --自己的摆摊列表()
    AUCTION_BUY_REQ         = 8504,    --买(auction_item_dbid,passwd)
    AUCTION_OFF_REQ         = 8505,    --下架(auction_item_dbid)
    --AUCTION_RECORD_LIST   = 8506,    --自己的交易记录 --直接使用avatar身上记录auction_logs
    AUCTION_SEARCH          = 8507,    --搜索(name)
    
    --拍卖行 求购 8551 - 8600
    BEG_PUBLISH_REQ         = 8551,  --发布(item_id,count,price) --count为叠加数量最大值，装备只能1，就求购的是一个背包格子的东西。
    BEG_ITEM_MY_LIST        = 8552,  --自己的求购列表()
    BEG_ITEM_LIST           = 8553,  --求购列表(auction_type)
    BEG_ITEM_LIST_ALL       = 8554,  --求购列表所有
    BEG_CANCEL_REQ          = 8555,  --撤销(beg_item_dbid) 
    BEG_SELL_REQ            = 8556,  --出售(beg_item_dbid) 
    BEG_SEARCH              = 8557,  --搜索(name)
    
    --周任务 8601-8650
    WEEK_TASK_LIST_REQ          = 8601, --全部任务列表，服务器推送
    WEEK_TASK_REFRESH           = 8602, --更新任务状态，服务器推送
    WEEK_TASK_ACCEPT_REQ        = 8603, --接任务，随机一个任务来接。前提是现在没有已接任务。
    WEEK_TASK_COMMIT_REQ        = 8604, --提交任务。领取奖励
    WEEK_TASK_QUICK_COMPLETE    = 8605, --快速完成，包括提交
    WEEK_TASK_ONE_CLICK         = 8606, --一键完成一环任务中剩下的任务
    
    --守护系统（灵宠） 8651 - 8700
    GUARDIAN_UPLEVEL            = 8651, --激活（含升级）
    GUARDIAN_SELECT             = 8652, --选择出战守护
    GUARDIAN_EXPLAIN            = 8653, --分解结晶
    
    --公会篝火 8701-8750
    GUILD_BONFIRE_ENTER                 = 8701, --进入场景
    GUILD_BONFIRE_ANSWER                = 8702, --回答(answer)
    GUILD_BONFIRE_GIFT                  = 8703, --领取礼包)
    GUILD_BONFIRE_NOTIFY_STATE          = 8704, --通知活动状态(state,state_time,question_id,question_time,cur_count)
    GUILD_BONFIRE_NOTIFY_EXP_CONTRIBUTE = 8705, --通知统计的累计经验和贡献(exp,contribute)
    GUILD_BONFIRE_NOTIFY_GIFT           = 8706, --通知礼包状态(gift_state)--0,1
    GUILD_BONFIRE_NOTIFY_RANK           = 8707, --通知排行榜。列表，每个元素为排行榜结构
    GUILD_BONFIRE_COMPLETE              = 8708, --结算

    --转职（四转）  8751 - 8800
    VOC_CHANGE_FOUR_ACTIVATE    = 8751, --激活命格
    VOC_CHANGE_FOUR_APPLY_SUCC  = 8752, --转生成功请求
    
    --答题系统 8801-8851
    QUESTION_ENTER                   = 8801, --进入答题系统场景
    QUESTION_ANSWER                  = 8802, --回答(answer)
    QUESTION_NOTIFY_STATE            = 8803, --通知活动状态
    QUESTION_NOTIFY_EXP              = 8804, --通知统计的累计经验
    QUESTION_RANK_LIST               = 8805, --排行榜
    QUESTION_NOTIFY_ANSWER           = 8806, --通知答题对错

    --神兽岛 8901 - 8950
    GOD_ISLAND_GET_DATA                 = 8901, --获取信息
    GOD_ISLAND_ENTER                    = 8902, --进入
    GOD_ISLAND_INTREST                  = 8903, --关注/取关
    GOD_ISLAND_GET_KILLER_RECORDS       = 8904, --击杀记录
    GOD_ISLAND_ON_KILL_BOSS             = 8905, --
    GOD_ISLAND_NOTIFY_BOSS_WILL_REBORN  = 8906,
    GOD_ISLAND_NOTIFY_SPACE_INFO        = 8907, --场景内信息
    
    --公会争霸 8951-9000
    GUILD_CONQUEST_ENTER                = 8951, --进入场景
    GUILD_CONQUEST_LIST                 = 8952, --争霸列表,字典。{1:{公会名字,对战结果,公会dbid},2:nil,..} --public_config有对应的key
    GUILD_CONQUEST_NOTIFY_BASIC_INFO    = 8953, --{guild_dbid:{公会名，图腾id1，旗帜id,公会对战排名},guild_dbid:{公会名，图腾id1，旗帜id,公会对战排名}}
    GUILD_CONQUEST_NOTIFY_PLAYER_NUM    = 8954, --{guild_dbid:num,guild_id:num}
    GUILD_CONQUEST_NOTIFY_RESOURCE      = 8955, --{guild_dbid:{num,score},guild_dbid:{num,score}}
    GUILD_CONQUEST_SETTLE               = 8956, --结算。结构参考public_config.INSTANCE_SETTLE_KEY_MAP_ID
    GUILD_CONQUEST_NOTIFY_ROOM_STAGE    = 8957, --通知副本stage和开始时间。{1:stage,2:start_time}
    GUILD_CONQUEST_NOTIFY_CLICK_FLAG    = 8958, --{orignal_id:faction_id}
    GUILD_CONQUEST_NOTIFY_PERSONAL_INFO = 8959, --{击杀，死亡，夺点}
    --主宰神殿
    GUILD_OVERLORD_GET_INFO             = 8960, --查看主宰神殿信息
    GUILD_OVERLORD_GET_REWARD           = 8961, --领取每日奖励
    GUILD_OVERLORD_GET_REWARD_STATE     = 8962, --奖励状态

    --塔防副本 9001 - 9050
    TOWER_DEFENSE_ENTER             = 9001, -- 进入
    TOWER_DEFENSE_BUY_ENTER_NUM     = 9002, -- 买次数
    TOWER_DEFENSE_SWEEP             = 9003, -- 扫荡
    TOWER_DEFENSE_START             = 9004, -- 直接开始刷怪
    TOWER_DEFENSE_BUILD_TOWER       = 9005, -- 建塔
    TOWER_DEFENSE_CALL_MONSTER      = 9006, -- 召唤怪
    TOWER_DEFENSE_SET_AUTO_CALL     = 9007, -- 是否自动召唤怪
    TOWER_DEFENSE_NOTIFY_ROOM_STAGE = 9008, -- 通知副本阶段
    TOWER_DEFENSE_NOTIFY_WAVE_INFO  = 9009, -- 通知波数
    TOWER_DEFENSE_NOTIFY_TOWER_INFO = 9010, -- 通知建塔信息
    TOWER_DEFENSE_NOTIFY_SETTLE     = 9011, -- 通知结算信息

    --掉落记录9051 - 9100
    DROP_RECORD_GET_LOCAL   = 9051,
    DROP_RECORD_GET_CROSS   = 9052,

    --七天登录奖励 9101 - 9150
    REWARD_SEVENDAY_GET_REWARD   = 9101,   -- 领取七天奖励
    --REWARD_SEVENDAY_CHECK_REWARD = 9102,   -- 查看七天奖励

    --龙魂  9151    -   9200
    DRAGON_SPIRIT_PUTON     = 9151,     --龙魂穿戴
    DRAGON_SPIRIT_PUTOFF    = 9152,     --龙魂卸下
    DRAGON_SPIRIT_UPLEVEL   = 9153,     --龙魂升级
    DRAGON_SPIRIT_COMPOSE   = 9154,     --龙魂合成
    DRAGON_SPIRIT_RESOLVE   = 9155,     --龙魂分解
    DRAGON_SPIRIT_RESTORE   = 9156,     --龙魂拆解
    
    --福利中心 9201 - 9250
    BENEFIT_DAILY_BONUS_REQ       = 9201, --领取签到奖励
    BENEFIT_CUMULATIVE_REWARD_REQ = 9202, --领取累计签到额外奖励
    BENEFIT_LEVEL_UP_REWARD_REQ   = 9203, --领取冲级奖励
    BENEFIT_EXCHANGE_COUPON_REQ   = 9204, --领取兑换券奖励
    BENEFIT_NOTICE_REWARD_REQ     = 9205, --领取更新公告奖励
    BENEFIT_GET_RECEIVED_INFO_REQ = 9206, --获取已领冲级奖励信息

    --结婚 9251 - 9300
    MARRY_PROPOSE                  = 9251, --提亲(dbid,grade)
    MARRY_RECEIVE_PROPOSE          = 9252, --收到提亲 --{dbid,name,grade,vocation}
    MARRY_RESPOND_PROPOSE          = 9253, --回应提亲(ret) --#1为同意
    MARRY_RECEIVE_RESPOND_PROPOSE  = 9254, --收到回应提亲 --{ret,name,vocation}
    MARRY_DIVORCE                  = 9255, --申请离婚()
    MARRY_QUERY_INFO               = 9256, --查询配偶的信息()() --{to_dbid,to_name,to_facade,to_vocation} --需要mate_name ~= ""
    MARRY_BOOK_INFO                = 9257, --查询预约的信息() --{wedding_count,grade,hour,my_book_data} --需要mate_name ~= "" --如果hour为nil就是没预约。--my_book_data结构参考BOOK_KEY_INVITED
                                           --客户端已结婚上线访问  --结婚，预约，邀请，购买次数，后端会刷给两个结婚者。
    --                             = 9258, --
    MARRY_BOOK                     = 9259, --预约婚礼(hour)
    MARRY_INVITE                   = 9260, --邀请宾客(dbid)
    MARRY_BUY_INVITE_COUNT         = 9261, --购买邀请次数()
    MARRY_ASK                      = 9262, --索要请帖()
    MARRY_RECEIVE_ASK              = 9263, --收到索要请帖 --{dbid,name} --然后调用MARRY_INVITE(dbid)
    MARRY_QUERY_INVITE             = 9264, --查询被邀请信息() --{all_book_data,hour} --hour为nil就是没有被邀请,被邀请者包含结婚者。--all_book_data结构{[hour]={BOOK_KEY_DBID1:dbid1,BOOK_KEY_DBID2:dbid2}}
                                           --所有人上线的时候客户端访问 --预约的时候后端广播，离婚又要后端广播
    MARRY_ENTER                    = 9265, --进入婚礼场景()
    MARRY_CEREMONY                 = 9266, --开始仪式()
    MARRY_RECIVE_CEREMONY          = 9267, --收到开始仪式
    MARRY_SEND_RED                 = 9268, --送钱(dbid,9) --送红包要在线，不可以给自己。--成功，给自己推一次MARRy_BLESS_RECORD
    --MARRY_SEND_FLOWER            = 9269, --送花(dbid,pos) --送花,要在线，不可以给自己。先删除花的道具，加祝福记录，后续再看送花相关。成功，给自己推一次MARRy_BLESS_RECORD --废掉，走送花流程，送花流程多一个渠道来处理加祝福记录。
    MARRY_BLESS_RECORD             = 9270, --祝福记录().所有人看到，打开时候请求。全部吧。 --{my_name,to_name,os.time(),item_id,item_count}--红包的道具id变成为元宝的道具id
    MARRY_NOTIFY_ROOM_STAGE        = 9271, --通知副本stage和开始时间。{1:stage,2:start_time}
    MARRY_NOTIFY_BASIC_INFO        = 9272, --{has_ceremony,hot_degree} --在MarryMgr记录处理  --热度，使用烟花，道具效果，判断是否在结婚场景,处理加热度 
    MARRY_NOTIFY_EXP               = 9273, --{exp} --经验  --在MarryMgr记录处理
    MARRY_NOTIFY_FOOD              = 9274, --{food} --美食  --在MarryMgr记录处理
    MARRY_NOTIFY_GIFT              = 9275, --{gift} --礼物 --在MarryMgr记录处理 --后端热度到某个数值重新刷出礼物采集实体。每次把之前的都清空。
    MARRY_TASTE_FOOD               = 9276, --品尝美食()
    MARRY_RECIVE_FIREWORKS         = 9277, --收到别人播放烟花，结婚场景内广播 --{item_id}
    MARRY_RECIVE_SEND_RED          = 9278, --收到别人送的红包 --{from_name,price}
    MARRY_NOTIFY_BOSS_HP           = 9279, --boss血量
    MARRY_NOTIFY_BOSS              = 9280, --刷boss

    --神兽养成系统9301 - 9350
    GOD_MONSTER_EQUIP_LOAD          = 9301, -- 穿装备
    GOD_MONSTER_EQUIP_UNLOAD        = 9302, -- 卸装备
    GOD_MONSTER_EQUIP_UNLOAD_ALL    = 9303, -- 一键卸
    GOD_MONSTER_EQUIP_STREN         = 9304, -- 强化
    GOD_MONSTER_ASSIST              = 9305, -- 助战
    GOD_MONSTER_RECALL              = 9306, -- 召回
    GOD_MONSTER_ADD_ASSIST          = 9307, -- 增加可助战神兽数量

    --等级投资 9351 - 9400
    LV_INVEST_PAY           = 9351, --投资或则追加投资
    LV_INVEST_GET_REWARD    = 9352, --领取奖励

    --月卡 9401 - 9450
    INVESTMENT_PURCHASE_MONTH_CARD_REQ = 9401, --买月卡
    INVESTMENT_MONTH_CARD_DAILY_REFUND_REQ = 9402, --领月卡每日返利

    --vip投资 9451 - 9500
    INVEST_VIP_PAY          = 9451, --投资
    INVEST_VIP_GET_REWARD   = 9452, --领取奖励

    --开服累充 9501 - 9550
    CUMULATIVE_CHARGE_REFUND_REQ       = 9501, -- 领取累充奖励
    CUMULATIVE_CHARGE_AWARD_REMAIN_REQ = 9502, -- 发放未领取累充奖励

    --查看他人信息  9551 - 9600
    PEEP_OTHER_INFO         = 9551, --查看他人信息
    PEEP_SEND_INFO          = 9552, --发送自己信息给他人查看

    --集字有礼 9601 - 9650
    COLLECT_WORD_EXCHANGE   = 9601, --兑换奖励
    COLLECT_WORD_RECORD     = 9602, --全服限制次数

    --每日首充，开服首充 9651 - 9700
    DAILY_CHARGE_REWARD             = 9651, --领取对应额度的每日奖励
    DAILY_CHARGE_CUMULATIVE_REWARD  = 9652, --领取累计天数奖励
    FIRST_CHARGE_REWARD             = 9653, --领取开服首充奖励

    --月卡 9701 - 9750
    MARRY_RANK_PERFECT_LOVER_HONOR_REQ = 9701, --领取完美情人称号
    MARRY_RANK_GET_RANK_LIST_REQ       = 9702, --获取完美情人榜单列表
    MARRY_RANK_GET_RANK_INFO_REQ       = 9703, --获取完美情人进度

    --开服冲榜 9751 -9800
    OPEN_SERVER_RANK_GET_INFO   = 9751, --信息
    OPEN_SERVER_RANK_GET_REWARD = 9752, --领取奖励
    OPEN_SERVER_RANK_GET_RANK_LIST = 9753, --排行
    OPEN_SERVER_RANK_BUY            = 9754, --冲榜限购

    --欢乐云盘 9801 - 9850
    ROULETTE_REWARD         = 9801, --普通奖励
    ROULETTE_PERSONAL_LIST  = 9802, --普通奖励获奖列表
    ROULETTE_BIG_REWARD_LIST= 9803, --幸运大奖获奖列表
    ROULETTE_TOTAL_LUCK_TIMES_REQ = 9804, --幸运大奖全服次数

    --魔戒寻主 9851 - 9900
    DEMON_RING_SUB_TARGET_REWARD_REQ = 9851, --目标奖励
    DEMON_RING_UNLOCK_REQ            = 9852, --解锁戒指
    DEMON_RING_GET_INFO_REQ          = 9853, --获取进度信息
    DEMON_RING_REFRESH               = 9854, --状态更新
    
    --装备寻宝 9901 - 9950
    LOTTERY_EQUIP_ONE               = 9901, --单抽
    LOTTERY_EQUIP_TEN               = 9902, --10抽
    LOTTERY_EQUIP_FIFTY             = 9903, --50抽
    LOTTERY_EQUIP_EXCHANGE          = 9904, --积分兑换
    LOTTERY_EQUIP_TAKE_OUT          = 9905, --取出
    LOTTERY_EQUIP_TAKE_OUT_ALL      = 9906, --一键取出
    LOTTERY_EQUIP_GET_SERVER_RECORD = 9907, --全服记录
    LOTTERY_EQUIP_GET_PERSONAL_RECORD   = 9908, --自己的记录
    LOTTERY_EQUIP_SET_REMIND_FLAG   = 9909, --今日不在提醒

    --婚戒 9951 - 10000
    MARRY_RING_PROMOTE               = 9951, --提升(item_pos)
    MARRY_RING_EXTRA_RING            = 9952, --多余戒指加经验

    --仙娃 10001 - 10050
    MARRY_PET_UNLOCK    = 10001, --激活
    MARRY_PET_GRADE_UP  = 10002, --升阶

    --结婚副本 10051 - 10100
    MARRY_INSTANCE_ENTER                   = 10051, --进入副本 --这个单人进的接口废弃，用组队进的接口
    MARRY_INSTANCE_PURCHASE_EXTRA_TIME_REQ = 10052, --购买额外次数
    MARRY_INSTANCE_SETTLE                  = 10053, --结算
    MARRY_INSTANCE_ADD_MATE_TIME           = 10054, --增加配偶结婚副本进入次数
    MARRY_INSTANCE_BEG_MATE_TIME_REQ       = 10055, --请求配偶购买
    MARRY_INSTANCE_BE_BEGGED_MATE_TIME_REQ = 10056, --配偶请求购买

    --结婚宝匣 10101 - 10150
    MARRY_BOX_PURCHASE_MARRY_BOX_REQ = 10101, --购买宝匣
    MARRY_BOX_DAILY_REFUND_REQ       = 10102, --领取宝匣每日奖励
    MARRY_BOX_REFUND_REQ             = 10103, --领取宝匣直接返利

    --周卡 10151 - 10200
    WEEK_CARD_DAILY_REFUND_REQ = 10151, --领取周卡每日返利

    --送花 10201 - 10250
    SEND_FLOWER_SEND    = 10201, --送花
    SEND_FLOWER_RECEIVE = 10202, --收花
    SEND_FLOWER_KISS    = 10203, --回吻
    SEND_FLOWER_KISS_BACK   = 10204, --收到回吻
    SEND_FLOWER_EFFECT  = 10205, --全服效果（在线玩家）
    SEND_FLOWER_BY_NAME = 10206, --按名字送花

    --系统设置 10251 - 10300
    SETTING_SET_SECURITY_LOCK_REQ          = 10251, --设置安全锁
    SETTING_CLEAR_SECURITY_LOCK_REQ        = 10252, --清除安全锁
    SETTING_UNLOCK_SECURITY_LOCK_REQ       = 10253, --解除安全锁
    SETTING_LOCK_SECURITY_LOCK_REQ         = 10254, --上锁安全锁
    SETTING_MODIFY_SECURITY_LOCK_PWD_REQ   = 10255, --修改安全锁密码

    --转职（三转） 10301 - 10350
    ONE_KEY_FINISH = 10301,

    --翻牌集福 10351 - 10400
    OPEN_CARD_REQ = 10351, --翻牌
    OPEN_CARD_SUPREME_REWARD_REQ = 10352, --领取福字奖励

    --欢乐许愿  10401 - 10450
    WISH_WISH       = 10401, --许愿

    --升级限时销售限时抢购 10451 - 10500
    LEVELED_UP_LIMITED_TIME_SALE_PURCHASE_REQ = 10451, --购买升级限时销售商品

    --连续消费 10501 - 10550
    DAILY_CONSUME_REWARD    = 10501, --获取每天档位奖励
    DAILY_CONSUME_CUMULATIVE_REWARD = 10502, --获取累计天数奖励

    --全民鉴宝 10551 - 10600
    JIANBAO_REWARD      = 10551, --鉴宝

    --周累充 10601 - 10650
    WEEKLY_CUMULATIVE_CHARGE_REFUND_REQ = 10601, --领取周累充返利

    --周活动登录 10651 - 10700
    WEEK_LOGIN_REWARD   = 10651, --领取登录奖励

    --庆典烟花 10701 - 10750
    ACTIVITY_FIREWORK_BUY   = 10701, --购买庆典烟花
    ACTIVITY_FIREWORK_EFFECT    = 10702, --全服烟花特效

    --神兵现世 10751 - 10760
    HOLY_WEAPON_RANK_GET_INFO = 10751, --获取信息

    --战场之魂 10761 - 10800
    BATTLE_SOUL_ENTER                       = 10761, --申请进入
    BATTLE_SOUL_NOTIFY_END_TIME             = 10762, --通知整个玩法结束的时间
    BATTLE_SOUL_NOTIFY_DAMAGE_RANK          = 10763, --通知伤害排行
    BATTLE_SOUL_NOTIFY_NEXT_BOSS_BORN_TIME  = 10764, --通知下只boss刷新时间
    BATTLE_SOUL_NOTIFY_ALL_BOSS_DIE_TIME    = 10765, --所有boss都打完的时间戳,通知即将退出场景
    
    --庆典兑换 10801 - 10850
    EXCHANGE_REWARD = 10801, --兑换

    --hi点 10851 - 10900
    HI_REWARD_REQ       = 10851,

    --在线奖励 10901 - 10950
    ONLINE_REWARD_REQ = 10901,

    --0元购 10951 - 11000
    ZERO_PRICE_BUY  = 10951,
    ZERO_PRICE_REBATE   = 10952,

    --炫装特卖 11001 - 11050
    DAZZLE_SALE_BUY = 11001,

    --星脉 11051 - 11100
    ZODIAC_UPDATE_REQ = 11051,

    --BOSS悬赏 11101 - 11150
    BOSS_REWARD_UNLOCK_REQ = 11101, --解锁下一阶段
    BOSS_REWARD_GET_INFO_REQ = 11102, --获取进度
    BOSS_REWARD_REFRESH = 11103, --任务完成状态更新
    
    --上古战场 11151-11200
    CROSS_ANCIENT_BATTLE_ENTER_REQ          = 11151, --进入
    CROSS_ANCIENT_BATTLE_NOTIFY_ROOM_STAGE  = 11152, --通知副本stage和开始时间。{1:stage,2:start_time}
    CROSS_ANCIENT_BATTLE_NOTIFY_END_TIME    = 11153, --通知副本结束时间{end_time}
    CROSS_ANCIENT_BATTLE_GET_SCORE_RANK     = 11154, --请求积分排行{{faction,name,level,score,fight_force},...}
    CROSS_ANCIENT_BATTLE_NOTIFY_FACTION_INFO = 11155, --阵营信息 {[faction_1]={player_count,boss_monster_id,boss_monster_eid},[faction_2]={}
    CROSS_ANCIENT_BATTLE_SELF_INFO           = 11156, --自己的信息{faction,kill_num,die_num,rank,score}
    CROSS_ANCIENT_BATTLE_NOTIFY_SETTLE       = 11157, --结算信息 {win_faction,killer_name}
    CROSS_ANCIENT_BATTLE_PICK_BUFF           = 11158, --{faction,player_name}阵营拾取buff

    --天降秘宝 11201 - 11250
    SKY_DROP_COUNT_DOWN = 11201, --倒计时

    --开宗立派 11251 - 11300
    FRAUD_GUILD_REWARD_REQ = 11251, --领奖
    FRAUD_GUILD_GET_INFO_REQ = 11252, --获取任务状态
    FRAUD_GUILD_GET_RECEIVED_INFO_REQ = 11253, --获取限制数量已领信息
    FRAUD_GUILD_REFRESH = 11254, --任务完成状态更新

    --开服乱嗨 11301 - 11350
    FREAK_REWARD_REQ = 11301, --领奖
    FREAK_GET_INFO_REQ = 11302, --获取任务状态

    --限时抢购2 11351 - 11400
    LIMITED_SALE_BUY    = 11351, --买

    --vip冲级礼包 11401 - 11450
    VIP_LEVEL_UP_REWARD = 11401, --领取
    VIP_LEVEL_UP_RECORD = 11402, --获取记录

    --升品 11451 - 11500
    EQUIP_QUALITY_UPGRADE = 11451, --升级品质

    --神兽岛单服 11501 - 11550
    LOCAL_GOD_ISLAND_GET_DATA                 = 11501, --单服 获取信息
    LOCAL_GOD_ISLAND_ENTER                    = 11502, --单服 进入
    LOCAL_GOD_ISLAND_INTREST                  = 11503, --单服 关注/取关
    LOCAL_GOD_ISLAND_GET_KILLER_RECORDS       = 11504, --单服 击杀记录
    LOCAL_GOD_ISLAND_ON_KILL_BOSS             = 11505, --
    LOCAL_GOD_ISLAND_NOTIFY_BOSS_WILL_REBORN  = 11506, --单服 boss即将刷新
    LOCAL_GOD_ISLAND_NOTIFY_SPACE_INFO        = 11507, --单服 场景内信息

    --深渊祭坛 11551 - 11600
    ALTAR_SUCCESS_RESULT                      = 11551, --深渊祭坛胜利结算
    ALTAR_FAIL_RESULT                         = 11552, --深渊祭坛失败结算
    ALTAR_NOTIFY_ROOM_STAGE                   = 11553, --通知房间状态变化
    ALTAR_NOTIFY_KILL_MONSTERS                = 11554, --通知波数、杀怪数量
    
    --巅峰寻宝 11601 - 11650
    LOTTERY_PEAK_ONE               = 11601, --单抽
    LOTTERY_PEAK_TEN               = 11602, --10抽
    LOTTERY_PEAK_FIFTY             = 11603, --50抽
    LOTTERY_PEAK_EXCHANGE          = 11604, --积分兑换
    --LOTTERY_PEAK_TAKE_OUT          = 11605, --取出
    LOTTERY_PEAK_TAKE_OUT_ALL      = 11606, --一键取出
    LOTTERY_PEAK_GET_SERVER_RECORD = 11607, --全服记录
    LOTTERY_PEAK_GET_PERSONAL_RECORD   = 11608, --自己的记录
}

return cfg
