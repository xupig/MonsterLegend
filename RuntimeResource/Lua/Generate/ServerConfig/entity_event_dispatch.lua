--该模块用来管理基于某个entity事件的订阅和触发，可以作为entity的子系统使用。--实体自身
local table = table
local pairs = pairs

local table_insert = table.insert

local entity_event_dispatch = {}


--为某个entity构造一个事件管理器对象
function entity_event_dispatch:new()
    local obj = {}
    setmetatable(obj, {__index = self}) --setmetatable(obj, self)
    
    obj.event_handlers = {}  --实体自身的事件handlers
    return obj
end


--cb_func:回调函数，可以使一个闭包，回调方式为：cb_func(evt_id, evt_param, trigger_obj)
--三个参数分别表示: 事件编号, 事件参数, 事件触发者
function entity_event_dispatch:insert_event(event_id, cb_func)
    local cbs = self.event_handlers[event_id]
    if not cbs then
        self.event_handlers[event_id] = {}
        cbs = self.event_handlers[event_id]
    end
    table_insert(cbs, cb_func)
end


--取消订阅某个事件
function entity_event_dispatch:remove_event(event_id, cb_func)
    local cbs = self.event_handlers[event_id]
    if cbs then
        for index, record in pairs(cbs) do
            if record == cb_func then
                cbs[index] = nil
                break
            end
        end
    end
end


--触发某个事件
function entity_event_dispatch:trigger_event(event_id, event_param, trigger)
    local cbs = self.event_handlers[event_id]
    if cbs then
        for _, cb_func in pairs(cbs) do
            cb_func(event_id, event_param, trigger)
        end
    end
end


-----------------
return entity_event_dispatch

