local cfg = {

    --mailbox中key的定义
    MAILBOX_KEY_SERVER_ID    = 1,      --server_id
    MAILBOX_KEY_CLASS_TYPE   = 2,      --entity class_type
    MAILBOX_KEY_ENTITY_ID    = 3,      --entity id

    CHARACTER_KEY_DBID        = 1,
    CHARACTER_KEY_NAME        = 2,
    CHARACTER_KEY_VOCATION    = 3,
    --CHARACTER_KEY_MODEL     = 4,
    CHARACTER_KEY_RACE        = 4,
    CHARACTER_KEY_GENDER      = 5,
    CHARACTER_KEY_FACADE      = 6,
    CHARACTER_KEY_LEVEL       = 7,
    CHARACTER_KEY_FIGHT_FORCE = 8,

    --性别
    GENDER_FEMALE = 0,                 --性别:女
    GENDER_MALE   = 1,                 --性别:男

    --职业
    VOCATION_ALL       = 0,
    --VOCATION_MIN       = 1,         -- 最小职业编号  --不用0,0在配表中可能代表所有职业
    --VOCATION_MAX       = 6,         -- 最大职业编号

    --种族
    RACE_ELYOS        = 1,    --天族
    RACE_ASMODIANS    = 2,    --魔族


    --删除cell的entity的原因
    DESTROY_CELL_ENTITY_LOGOUT       = 0,    --因为退出而销毁
    DESTROY_CELL_ENTITY_TELEPORT     = 1,    --因为传送而销毁
    DESTROY_CELL_ENTITY_CROSS        = 2,    --因为跨服而销毁
    DESTROY_CELL_ENTITY_INVISIBLE    = 3,    --因为跨服而隐身
    DESTROY_CELL_ENTITY_OTHER_AVATAR = 4,    --切换角色而销毁。仅仅销毁不通知account
    DESTROY_CELL_ENTITY_SHUTDOWN     = 5,    --重启服务器


    --地图类型
    MAP_TYPE_KINGDOM          = 1,    --王城
    MAP_TYPE_WILD             = 2,    --野外
    --MAP_TYPE_ARENA_POINT    = 3,    --竞技场-6v6多人pvp-激战永恒之柱-据点-跨服
    --MAP_TYPE_PVP_MATCH      = 4,    --pvp
    MAP_TYPE_FIGHT            = 6,    --战斗副本 普通副本
    --MAP_TYPE_FIGHT_FRIEND   = 8,    --切磋副本
    --MAP_TYPE_COMMON         = 10,   --普通副本
    --MAP_TYPE_HERO           = 11,   --英雄副本
    --MAP_TYPE_NIGHTMARE      = 12,   --噩梦副本（周常）
    --MAP_TYPE_RAID           = 13,   --Raid副本
    --MAP_TYPE_ABYSS          = 14,   --深渊副本
    --MAP_TYPE_IMAGE          = 15,   --镜像副本
    --MAP_TYPE_EVENT_ACTIVITY = 16,   --野外事件-个人护送-个人征服-个人防守
    --MAP_TYPE_LADDER_MATCH   = 17,   --天梯赛1v1
    --MAP_TYPE_GUILD_TERRITORY= 18,   --公会领地
    MAP_TYPE_AION             = 19,   --永恒之塔
    --MAP_TYPE_GUILD_RAID     = 20,   --公会boss
    MAP_TYPE_WORLD_BOSS       = 21,   --世界boss
    --MAP_TYPE_BOUNTY_MATCH   = 22,   --赏金赛 3v3
    MAP_TYPE_EXP              = 23,   --经验副本
    MAP_TYPE_HOLY_TOWER       = 24,   --守护结界塔（守护仙灵）
    MAP_TYPE_BOSS_HOME        = 25,   --boss之家
    MAP_TYPE_PERSONAL_BOSS    = 26,   --个人boss
    MAP_TYPE_GOLD             = 27,   --金币副本
    MAP_TYPE_EQUIPMENT        = 28,   --装备副本(圣灵血阵)
    MAP_TYPE_FORBIDDEN        = 29,   --蛮荒禁地
    MAP_TYPE_CROSS_VS11       = 30,   --跨服实时1v1
    MAP_TYPE_GUILD_BEAST      = 31,   --公会神兽
    MAP_TYPE_CLOUD_PEAK       = 32,   --青云之巅
    MAP_TYPE_GUARD_GUILD      = 33,   --守卫公会
    MAP_TYPE_GUILD_BONFIRE    = 34,   --公会篝火
    MAP_TYPE_GOD_ISLAND       = 35,   --神兽岛(跨服)
    MAP_TYPE_GUILD_CONQUEST   = 36,   --公会争霸
    MAP_TYPE_QUESTION         = 37,   --答题
    MAP_TYPE_TOWER_DEFENSE    = 38,   --塔防副本
    MAP_TYPE_MARRY            = 39,   --婚礼
    MAP_TYPE_MARRY_INSTANCE   = 40,   --仙缘副本
    MAP_TYPE_BATTLE_SOUL      = 41,   --战场之魂
    MAP_TYPE_ANCIENT_BATTLE   = 43,   --上古战场
    MAP_TYPE_LOCAL_GOD_ISLAND = 44,   --神兽岛(单服)
    MAP_TYPE_ALTAR            = 45,   --深渊祭坛

    --场景不允许创建
    MAP_NO_PET          = 1, --不允许创建宠物
    MAP_NO_FABAO        = 2, --不允许创建法宝
    MAP_NO_HORSE        = 3, --不允许坐骑

    --活动类型
    --ACTIVITY_TYPE_BEGIN             = 1,    --
    --ACTIVITY_TYPE_AION              = 1,    --永恒之塔
    --ACTIVITY_TYPE_WORLD_BOSS        = 2,    --世界boss
    --ACTIVITY_TYPE_ABYSS             = 3,    --深渊
    --ACTIVITY_TYPE_END               = 3,    --用于获取所有活动的信息列表用

    -- 竞技类型
    --ATHLETICS_TYPE_BEGIN                = 1,    -- id
    --ATHLETICS_TYPE_LADDER               = 1,    -- 天梯1v1
    --ATHLETICS_TYPE_PVP_MATCH            = 2,    -- pvp
    --ATHLETICS_TYPE_BOUNTY_MATCH       = 3,    -- 赏金赛 3v3
    --ATHLETICS_TYPE_END                  = 3,    -- 获取所有竞技信息列表

    --临时数据的key
    TMP_DATA_KEY_TELEPORT_INFO                = 1,
    TMP_DATA_KEY_DESTROY_TIMER_ID             = 2,
    TMP_DATA_KEY_HEART_BEAT_TIMER_ID          = 3,
    TMP_DATA_KEY_ONE_SECOND_TIMER_ID          = 4,
    TMP_DATA_KEY_PER_MINUTE_TIMER_ID          = 5,
    --TMP_DATA_KEY_GM_HP_BASE                 = 6,
    TMP_DATA_KEY_LAST_MAP_INFO                = 7,
    TMP_DATA_KEY_AION_SWEEP_TIMER_ID          = 8,
    --TMP_DATA_KEY_CLIENT_MAP_ID              = 9,
    TMP_DATA_KEY_DUDE_TASK                    = 10,
    --TMP_DATA_KEY_CLIENT_DROP_ITEMS          = 11,
    TMP_DATA_KEY_EXP_INSTANCE_DROP_ITEMS      = 12,
    TMP_DATA_KEY_EXP_INSTANCE_WAVE            = 13,
    TMP_DATA_KEY_EXP_INSTANCE_KILL_NUM        = 14,
    TMP_DATA_KEY_EXP_INSTANCE_START_TIME      = 15,
    TMP_DATA_KEY_EXP_INSTANCE_GOLD_INSPIRE_NUM    = 16,
    TMP_DATA_KEY_EXP_INSTANCE_COUPON_INSPIRE_NUM  = 17,
    TMP_DATA_KEY_FIRST_CELL                       = 18,
    TMP_DATA_KEY_OFFLINE_PVP                      = 19,

    --副本解锁条件的key
    INSTANCE_UNLOCK_CONDITION_LEVEL      = 1,    --等级要求
    INSTANCE_UNLOCK_CONDITION_RPE_INS    = 2,    --前置副本

    --摆摊系统排序
    --PEDDLE_LIST_SORT_ASC = 1,    --升序
    --PEDDLE_LIST_SORT_DESC = 2,   --降序

    ------道具消耗渠道--------------------------------------------------
    ITEM_COST_WAY_UNKNOWN                     = -1, --未知渠道
    ITEM_COST_WAY_SELL                        = 1,  --出售道具
    --ITEM_COST_PEDDLE_SELL_COST              = 2,  --摆摊系统出售消耗
    --ITEM_COST_PEDDLE_BUY_COST               = 3,  --摆摊系统购买消耗
    --ITEM_COST_PEDDLE_CANCEL                 = 4,  --下架摆摊物品
    --ITEM_COST_PEDDLE_BUY                    = 5,  --购买摆摊物品
    --ITEM_COST_LIFE_SKILL_UPGRADE            = 6,  --升级生活技能
    --ITEM_COST_LIFE_SKILL_MAKE               = 7,  --是用生活技能制造
    --ITEM_COST_LIFE_SKILL_COLLECT            = 8,  --是用生活技能采集
    ITEM_COST_GM                              = 9,  --gm系统
    ITEM_COST_WAY_EQUIP_STREN                 = 10, --装备强化消耗强化石
    --ITEM_COST_WAY_EQUIP_STREN_INHER         = 11, --装备强化继承消耗
    --ITEM_COST_WAY_EQUIP_BREAK               = 12, --装备分解
    ITEM_COST_WAY_EQUIP_WASH                  = 13, --装备洗练
    ITEM_COST_WAY_GEM_LOAD                    = 14, --装备镶嵌宝石
    --ITEM_COST_WAY_GEM_COMPOUND              = 15, --合成宝石
    --ITEM_COST_WAY_GEM_COMPOUND_F            = 16, --快速合成宝石
    --ITEM_COST_WAY_LGEM_LVLUP                = 17, --传奇魔石升级
    --ITEM_COST_WAY_LGEM_EAT                  = 18, --传奇魔石注入能量
    --ITEM_COST_LIFE_SKILL_ADD_EXP            = 19, --吃道具加生活技能经验
    --TIEM_COST_MAKE_EQUIP                    = 20, --制造装备消耗
    ITEM_COST_WAY_EQUIP_LOAD                  = 21, --穿装备
    --ITEM_COST_WAY_SPELL_LEARN               = 22, --技能学习
    ITEM_COST_GIVE_ITEM_TO_NPC                = 23,  --上交道具给NPC
    ITEM_COST_BUY_BAG_COUNT                   = 24,  --解锁背包格子
    ITEM_COST_BUY_WAREHOUSE_COUNT             = 25,  --解锁仓库格子
    --ITEM_COST_WAY_EXPIRE                    = 26,  --时间到期
    ITEM_COST_WAY_ITEM_MOVE                   = 27, --移动道具
    --ITEM_COST_WAY_EUDEMON_UPGRADE_LEVEL     = 28, --守护灵升级
    --ITEM_COST_WAY_EUDEMON_UPGRADE_STAR      = 29, --守护灵升星
    --ITEM_COST_WAY_EUDEMON_CALL              = 30, --守护灵召唤
    --ITEM_COST_WAY_EUDEMON_LEARN_SKILL       = 31, --守护灵技能学习
    --ITEM_COST_WAY_EUDEMON_LOCK_SKILL        = 32, --守护灵技能锁定
    ITEM_COST_WAY_SHOP_BUY                    = 33, --商城购买消耗
    --ITEM_COST_WAY_SHOP_EXCHANGE             = 34, --商城钻石兑换金币
    ITEM_COST_WAY_AUCTION_SELL                = 35,  --交易行出售
    ITEM_COST_WAY_AUCTION_BUY                 = 36,  --交易行购买
    --ITEM_COST_WAY_LADDER_SHOP_BUY           = 37,  --天梯商店购买消耗
    --ITEM_COST_WAY_LADDER_SHOP_REFRESH       = 38,  --天梯商店刷新
    --ITEM_COST_WAY_BLACKMARKET               = 39,  --黑市
    ITEM_COST_WAY_USE                         = 40,  --玩家使用道具
    --ITEM_COST_WAY_EQUIP_REPAIR              = 41,  --修复消耗
    --ITEM_COST_WAY_DURABILITY_LOSE           = 42,  --部分装备耐久度消耗完消失
    ITEM_COST_WAY_GUILD_BUILD                 = 43,  --创建公会消耗
    --ITEM_COST_WAY_GUILD_SIGNON              = 44,  --工会签到
    --ITEM_COST_WAY_WING                      = 45,  --翅膀培养
    ITEM_COST_WAY_GUILD_SKILL                 = 46,  --公会技能
    --ITEM_COST_WAY_WING_COMPOUND             = 47,  --翅膀宝珠合成
    --ITEM_COST_WAY_GUILD_SHOP                = 48,  --公会商店
    --ITEM_COST_WAY_EQUIP_SOUL_EAT            = 49,  --魂器吞噬
    --ITEM_COST_WAY_AION_SWEEP                = 50,  --永恒之塔扫荡消耗
    --ITEM_COST_WAY_WORLD_BOSS                = 51,  --世界boss
    --ITEM_COST_WAY_PRESTIGE_SHOP             = 52,  --声望商店购买
    --ITEM_COST_WAY_EQUIP_SOUL_RESET          = 53,  --魂器重铸
    --ITEM_COST_WAY_LIFE_SKILL_TASK           = 54,  --生活技能任务
    --ITEM_COST_WAY_ABYSS                     = 55,  --深渊玩法
    --ITEM_COST_WAY_FASHION_COLOR             = 56,  --时装染色消耗
    --ITEM_COST_WAY_DIVINE                    = 57,  --占星消耗
    --ITEM_COST_SLIVER_CHANGE                 = 58,  --银元兑换
    --ITEM_COST_WAY_WELFARE_RETROACTIVE         = 59,  --福利系统补签
    --ITEM_COST_WAY_DUDE_UPGRADE_RANK         = 60,  --随从升职称消耗
    --ITEM_COST_WAY_DUDE_TASK_ADD_SPEED       = 61,  --随从任务加速消耗
    --ITEM_COST_WAY_DUDE_TASK_REFRESH_COST    = 62,  --刷新随从任务消耗
    --ITEM_COST_WAY_RIDE                      = 63,  --坐骑
    --ITEM_COST_WAY_EQUIP_ENCHANT             = 64,  --装备附魔
    ITEM_COST_WAY_PET                         = 66,  --宠物
    ITEM_COST_WAY_HORSE                       = 67,  --坐骑
    ITEM_COST_WAY_PK_MODE                     = 68,  --pk模式
    ITEM_COST_WAY_EXP_INSTANCE                = 69,  --经验副本
    ITEM_COST_WAY_NOBILITY                    = 70,  --爵位
    ITEM_COST_WAY_YUAN_DI_REVIVE_YUAN_BAO     = 71, --原地复活用元宝
    ITEM_COST_WAY_YUAN_DI_REVIVE_BIND_YUAN_BAO= 72, --原地复活用绑定元宝
    ITEM_COST_WAY_FASHION_ACTIVATE            = 73, --时装激活
    ITEM_COST_WAY_FASHION_UPGRADE_STAR        = 74, --时装升星
    ITEM_COST_WAY_FASHION_UPGRADE_LEVEL       = 75, --时装升级
    ITEM_COST_WAY_FASHION_BREAK               = 76, --时装道具分解
    ITEM_COST_WAY_TALENT_LEARN                = 77, --天赋加点
    ITEM_COST_WAY_TALENT_RESET                = 78, --天赋重置消耗
    ITEM_COST_WAY_GEM_REFINE                  = 79, --装备宝石精炼
    ITEM_COST_WAY_HOLY_TOWER_BUY_ENTER_NUM    = 80, --守护结界塔购买进入次数
    ITEM_COST_WAY_HOLY_TOWER_SWEEP            = 81, --守护结界塔扫荡
    ITEM_COST_WAY_COMPOSE_ITEM                = 82, --道具合成
    ITEM_COST_WAY_USE_LITTLE_SHOE             = 83, --使用小飞鞋
    ITEM_COST_RUNE_UPLEVEL                    = 84, --符文升级
    ITEM_COST_RUNE_EXCHANGE                   = 85, --符文兑换
    ITEM_COST_WAY_CIRCLE_TASK                 = 86, --循环任务
    ITEM_COST_WAY_BOSS_HOME_ENTER             = 87, --进boss之家
    ITEM_COST_WAY_PERSONAL_BOSS_ENTER         = 88, --进个人boss
    ITEM_COST_WAY_OFFLINE_PVP                 = 89, --离线pvp
    ITEM_COST_WAY_GUARD_GODDESS               = 90, --守护女神
    ITEM_COST_WAY_INSTANCE_EXP_INSPIRE        = 91, --经验副本鼓舞
    ITEM_COST_WAY_EQUIP_GUARD                 = 92, --装备部位守护
    ITEM_COST_PRAY_GOLD                       = 93, --祈福（金币）
    ITEM_COST_PRAY_EXP                        = 94, --祈福（经验）
    ITEM_COST_WAY_FORBIDDEN_ENTER             = 95, --进入蛮荒禁地
    --ITEM_COST_WAY_SALE_SELL                 = 96, --交易行出售
    --ITEM_COST_WAY_SALE_BUY                  = 97, --交易行购买
    ITEM_COST_WAY_INSTANCE_GOLD_BUY_ENTER_NUM = 98, --金币副本购买进入次数
    ITEM_COST_WAY_INSTANCE_GOLD_SWEEP         = 99, --金币副本扫荡
    ITEM_COST_WAY_INSTANCE_EXP_BUY_ENTER_NUM  = 100, --购买经验副本次数
    ITEM_COST_WAY_EQUIP_ADVANCE               = 101, --装备进阶
    ITEM_COST_WAY_CROSS_VS11_BUY_COUNT        = 102, --跨服购买额外1v1次数
    ITEM_COST_WAY_REWARD_BACK_GOLD            = 103, --资源找回消耗金币
    ITEM_COST_WAY_GUILD_WAREHOUSE             = 104, --公会仓库
    ITEM_COST_WAY_EQUIP_SUIT                  = 105, --装备套装升级
    ITEM_COST_WAY_REWARD_BACK_COUPONS         = 106, --资源找回消耗钻石
    ITEM_COST_WAY_SEND_MONEY_RED              = 107, --发公会v6红包
    ITEM_COST_WAY_ILLUSION_ACTIVATE           = 108, --化形激活
    ITEM_COST_WAY_ILLUSION_STAR_UP            = 109, --化形升星消耗
    ITEM_COST_WAY_ILLUSION_BREAK              = 110, --化形分解
    ITEM_COST_RUNE_LCUK                       = 111, --符文寻宝消耗
    ITEM_COST_WAY_GUILD_SET_ANNO              = 112, --公会设置公告消耗
    ITEM_COST_WAY_BEG_PUBLISH                 = 113, --求购发布
    ITEM_COST_WAY_BEG_SELL                    = 114, --求购出售
    ITEM_COST_WAY_ILLUSION_GRADE_UP           = 115, --化形升阶消耗
    ITEM_COST_WAY_GUARDIAN_UPLEVEL            = 116, --守护升级（含激活）
    ITEM_COST_WAY_GUARDIAN_EXPLAIN            = 117, --分解守护结晶
    ITEM_COST_WAY_VOC_CHANGE_FOUR_ACTIVATE    = 118, --转职（四转），激活命格
    ITEM_COST_WAY_TASK_COMPLETED_COST         = 119, --任务完成消耗
    ITEM_COST_WAY_TOWER_DEFENSE_BUY_ENTER     = 120, --塔防副本购买进入次数
    ITEM_COST_WAY_TOWER_DEFENSE_SWEEP         = 121, --塔防副本扫荡
    ITEM_COST_WAY_TOWER_DEFENSE_CALL_MONSTER  = 122, --塔防副本召唤高爆怪
    ITEM_COST_WAY_DRAGON_SPIRIT_COMPOSE       = 123, --龙魂合成
    ITEM_COST_WAY_DRAGON_SPIRIT_UPLEVEL       = 124, --龙魂升级
    ITEM_COST_WAY_LOAD_GOD_MONSTER_EQUIP      = 125, --穿神兽装备
    ITEM_COST_WAY_GOD_MONSTER_EQUIP_STREN     = 126, --神兽装备强化
    ITEM_COST_WAY_GOD_MONSTER_ADD_ASSIST_COUNT = 127,--增加神兽助战数
    ITEM_COST_WAY_MARRY_PROPOSE                = 128, --求婚
    ITEM_COST_WAY_LV_INVEST                   = 129, --等级投资
    ITEM_COST_WAY_INVEST_VIP                  = 130, --VIP投资
    ITEM_COST_WAY_COLLECT_WORD                = 131, --集字有礼
    ITEM_COST_WAY_MARRY_INVITE_COUNT          = 132, --结婚邀请次数
    ITEM_COST_WAY_ROULETTE                    = 133, --欢乐云盘
    ITEM_COST_WAY_MARRY_SEND_RED              = 134, --婚礼送钱
    ITEM_COST_WAY_SEND_FLOWER                 = 135, --婚礼送花
    ITEM_COST_WAY_ITEM_PAID_USE               = 136, --使用道具消耗
    ITEM_COST_WAY_EQUIP_LOTTERY_ONE           = 137, --装备寻宝单抽
    ITEM_COST_WAY_EQUIP_LOTTERY_TEN           = 138, --装备寻宝十连抽
    ITEM_COST_WAY_EQUIP_LOTTERY_FIFTY         = 139, --装备寻宝50抽
    ITEM_COST_WAY_MARRY_PET                   = 140, --仙娃系统（升阶）
    ITEM_COST_WAY_MARRY_INSTANCE_EXTRA        = 141, --购买仙缘副本额外次数
    ITEM_COST_WAY_SEND_FLOWER                 = 142, --送花
    ITEM_COST_WAY_CARD_UP_LEVEL               = 143,--图鉴升级消耗图鉴碎片背包里面的图片碎片
    ITEM_COST_WAY_CHAT_HORN                   = 144, --聊天频道喇叭
    ITEM_COST_WAY_GUIDE_BUY                   = 145, --引导购买消耗
    ITEM_COST_WAY_MARRY_PET_UNLOCK            = 146, --仙娃激活消耗
    ITEM_COST_WAY_VOC_CHANGE_ONE_KEY          = 147, --一键转职
    ITEM_COST_WAY_OPEN_CARD                   = 148, --翻牌集福
    ITEM_COST_WAY_WISH                        = 149, --欢乐许愿
    ITEM_COST_WAY_JIANBAO                     = 150, --全民鉴宝
    ITEM_COST_WAY_ACTIVITY_FIREWORK           = 151, --庆典烟花
    ITEM_COST_WAY_EXCHANGE                    = 152, --庆典兑换
    ITEM_COST_WAY_MARRY_RING_PROMOTE          = 153, --婚戒提升
    ITEM_COST_WAY_MARRY_RING_EXTRA_RING       = 154, --多余婚戒使用
    ITEM_COST_WAY_INVESTMENT                  = 155, --月卡投资
    ITEM_COST_WAY_LEVEL_SALE                  = 156, --升级限时销售
    ITEM_COST_WAY_MARRY_BOX                   = 157, --结婚宝箱
    ITEM_COST_WAY_WEEK_CARD                   = 158, --周卡
    ITEM_COST_WAY_ZERO_PRICE                  = 159, --0元购
    ITEM_COST_WAY_DAZZLE_SALE                 = 160, --炫装特卖
    ITEM_COST_WAY_OPEN_SERVER_BUY             = 161, --开服冲榜限购
    ITEM_COST_WAY_RUN_EXPODE                  = 162, --符文分解
    ITEM_COST_WAY_RUN_EXPODE_LOT              = 163, --符文分解，多
    ITEM_COST_WAY_DRAGON_COMPOSE              = 164, --龙魂，合成消耗
    ITEM_COST_WAY_DRAGON_EXPODE               = 165, --龙魂，分解消耗，单属性
    ITEM_COST_WAY_DRAGON_EXPODE1              = 166, --龙魂，拆解消耗，双属性
    ITEM_COST_WAY_LIMITED_SALE                = 167, --限时抢购2
    ITEM_COST_WAY_EQUIP_QUALITY               = 168, --升品
    ITEM_COST_WAY_CARD_RESOLVE                = 169, --分图鉴碎片
    ITEM_COST_WAY_LOTTERY_PEAK_ONE            = 170, --装备寻宝单抽
    ITEM_COST_WAY_LOTTERY_PEAK_TEN            = 171, --装备寻宝十连抽
    ITEM_COST_WAY_LOTTERY_PEAK_FIFTY          = 172, --装备寻宝50抽

    -------道具获取渠道-------------------------------------------------------
    ITEM_GET_WAY_UNKNOWN                  = -1,  --未知渠道
    ITEM_GET_WAY_ITEM_SELL                = 1,   --出售道具
    ITEM_GET_MAIL_AVATAR                  = 2,   --玩家间发送邮件得到
    ITEM_GET_GM                           = 3,   --gm系统
    --ITEM_GET_WAY_EQUIP_BREAK            = 4,   --装备分解获得道具
    ITEM_GET_WAY_GEM_UNLOAD               = 5,   --拆宝石
    --ITEM_GET_WAY_GEM_COMPOUND           = 6,   --合成宝石
    --ITEM_GET_WAY_GEM_COMPOUND_F         = 7,   --快速合成宝石
    --ITEM_GET_WAY_LGEM_LVLUP             = 8,   --传奇魔石升级
    ITEM_GET_TASK                         = 9,   --任务系统
    ITEM_GET_WAY_EQUIP_UNLOAD             = 10,  --脱装备
    ITEM_GET_INST                         = 11,  --副本系统
    --ITEM_GET_OWN_DROP                   = 12,  --个人掉落
    --ITEM_GET_SPELL_SP_BACK              = 13,  --技能sp返还
    --ITEM_GET_EVENT_ACTIVITY             = 14,  --野外事件
    --ITEM_GET_CALL_EUDEMON               = 15,  --召唤守护灵产出
    --ITEM_GET_WAY_WELFARE_LEVEL_UP_REWARD  = 16,  --福利系统，等级奖励
    --ITEM_GET_WAY_WELFARE_ONLINE_TIME_REWARD=17,  --福利系统，在线时间奖励
    --ITEM_GET_WAY_WELFARE_FREE_TILI_REWARD = 18,  --福利系统，免费体力
    --ITEM_GET_WAY_WELFARE_WEEKDAY_REWARD   = 19,  --福利系统，每日福利
    --ITEM_GET_WAY_WELFARE_7DAY_REWARD      = 20,  --福利系统，7天登陆奖励
    ITEM_GET_WAY_SHOP_BUY                 = 21,  --商城购买
    --ITEM_GET_WAY_SHOP_EXCHANGE          = 22,  --商城钻石兑换金币
    --ITEM_GET_DAILY_TASK                 = 23,  --日常任务系统
    --ITEM_GET_DAILY_TASK_POINT           = 24,  --日常任务系统 --日常任务点
    ITEM_GET_ACHIEVEMENT                  = 25,  --成就系统
    ITEM_GET_ACHIEVEMENT_POINT            = 26,  --成就系统      --成就点
    ITEM_GET_WAY_AUCTION_SELL_FAIL        = 27,  --拍卖行卖出失败，返回道具
    ITEM_GET_WAY_AUCTION_BUY_FAIL         = 28,  --拍卖行购买失败，返回金钱
    --ITEM_GET_WAY_LADDER_SHOP_BUY        = 29,  --天梯商城购买
    --ITEM_GET_WAY_BLACKMARKET            = 30,  --黑市
    --ITEM_GET_WAY_LADDER_RANK            = 31,  --天梯排行版奖励
    --ITEM_GET_WAY_LADDER_WEEK_REWARD     = 32,  --天梯周奖励
    --ITEM_GET_WAY_LADDER_SEASON_REWARD   = 33,  --天梯赛季奖励
    --ITEM_GET_WAY_WILD_TREASURE          = 34,  --野外宝箱
    --ITEM_GET_WAY_GUILD_SIGNON           = 35,  --工会签到
    --ITEM_GET_WAY_GUILD_RED_ENVELOPE     = 36,  --工会红包
    ITEM_GET_WAY_USE_ITEM                 = 37,  --使用道具获得道具包  礼包
    --ITEM_GET_WAY_WING_UNLOAD            = 38,  --翅膀宝石拆卸
    --ITEM_GET_WAY_WING_COMPOUND          = 39,  --翅膀合成
    --ITEM_GET_WAY_GUILD_SHOP             = 40,  --公会商店购买
    --ITEM_GET_WAY_INHER_BACK             = 41,  --继承时返还宝石
    ITEM_GET_WAY_AION                     = 42,  --永恒之塔通关获得
    --ITEM_GET_WAY_GUILD_RAID             = 43,  --公会boss奖励
    ITEM_GET_WAY_AION_DAILY_MAIL          = 44,  --永恒之塔每日邮件
    ITEM_GET_WAY_AVATAR_LEVEL_UP          = 45,  --玩家升级奖励
    ITEM_GET_WAY_WORLD_REWARD             = 46,  --世界boss奖励
    --ITEM_GET_WAY_LIFE_SKILL_TASK        = 47,  --生活技能任务
    --ITEM_GET_WAY_ABYSS                  = 48,  --深渊
    --ITEM_GET_WAY_DIVINE                 = 49,  --占星
    --ITEM_GET_PVP_MATCH_REWARD             = 50,  --pvp
    --ITEM_GET_SILVER_CHANGE              = 51,  --银元兑换
    --ITEM_GET_WAY_WELFARE_MONTH_REWARD     = 52,  --福利系统月签到
    --ITEM_GET_WAY_DUDE_REPEAT_GET_BREAK_DOWN = 53, --随从重复获得分解
    --ITEM_GET_WAY_DUDE_ASSIST             = 54, --随从助战奖励
    --ITEM_GET_WAY_DUDE_TASK_FINISH_REWARD = 55, --完成随从任务奖励
    --ITEM_GET_WAY_EXP_INSTANCE_MONSTER_REWARD=56,--经验副本每只怪杀死的奖励
    ITEM_GET_WAY_FASHION_BREAK            = 57, --时装分解
    ITEM_GET_WAY_TALENT_RESET             = 58, --天赋重置
    ITEM_GET_WAY_HOLY_TOWER               = 59, --守护结界塔
    ITEM_GET_WAY_RUNE_EXCHANGE_TO_ESSENCE = 60, --符文分解获得精华
    ITEM_GET_CIRCLE_TASK                  = 61, --循环任务
    ITEM_GET_WAY_HANG_UP_OFFLINE          = 62, --离线挂机
    ITEM_GET_WAY_INSTANCE_GOLD            = 63, --金币副本通关
    ITEM_GET_OFFLINE_PVP                  = 64, --离线pvp
    ITEM_GET_GUARD_GODDESS                = 65, --守护女神
    ITEM_GET_WAY_AVATAR_CREATE            = 66, --创建角色
    ITEM_GET_INSTANCE_EQUIPMENT           = 67, --装备副本
    ITEM_GET_ACTIVITY                     = 68, --活动系统
    ITEM_GET_VIP                          = 69, --VIP系统（等级奖励、周奖励）
    ITEM_GET_WAY_PRAY_GOLD                = 70, --祈福获得金币
    ITEM_GET_WAY_PRAY_EXP                 = 71, --祈福获得经验
    ITEM_GET_WAY_GUILD                    = 72, --工会
    ITEM_GET_CROSS_VS11_MATCH_REWARD      = 73, --跨服实时1v1单场奖励
    ITEM_GET_CROSS_VS11_SEASON_RANK_RWD   = 74, --跨服实时1v1赛季排名奖励
    ITEM_GET_CROSS_VS11_SEASON_STAGE_RWD  = 75, --跨服实时1v1赛季段位奖励
    ITEM_GET_CROSS_VS11_DAY_STAGE_RWD     = 76, --跨服实时1v1每日段位奖励
    --ITEM_GET_WAY_REWARD_BACK            = 77, --资源找回
    ITEM_GET_CROSS_VS11_DAY_COUNT_RWD     = 78, --跨服实时1v1每日参与次数奖励
    ITEM_GET_WAY_GUILD_WAREHOUSE_SELL_FAIL= 79, --公会仓库捐献失败返回
    ITEM_GET_CROSS_VS11_SEASON_GX_RWD     = 80, --跨服实时1v1赛季功勋奖励
    ITEM_GET_WAY_GUILD_WAREHOUSE_BUY_PET  = 81, --公会仓库兑换宠物道具
    ITEM_GET_WAY_GUILD_WAREHOUSE_BUY      = 82, --公会仓库兑换
    ITEM_GET_WAY_EQUIP_SUIT_BACK          = 83, --套装装备返还
    ITEM_GET_WAY_REWARD_BACK_GOLD         = 84, --金币找回资源
    ITEM_GET_WAY_REWARD_BACK_COUPONS      = 85, --钻石找回资源
    ITEM_GET_WAY_INSTANCE_GOLD_SWEEP      = 86, --金币副本扫荡
    ITEM_GET_WAY_HOLY_TOWER_SWEEP         = 87, --结界塔扫荡
    ITEM_GET_WAY_GUILD_GRAP_RED           = 88, --公会抢红包
    ITEM_GET_WAY_RUNE_LUCK                = 89, --符文寻宝
    ITEM_GET_WAY_AUCTION_BUY_SUCESS       = 90, --交易行购买成功
    ITEM_GET_WAY_AUCTION_TIMEOUT_DEL      = 91, --交易行超时下架返还道具
    ITEM_GET_WAY_AUCTION_BE_BUY           = 92, --交易行被购买。钻石
    ITEM_GET_WAY_AUCTION_OFF_SUCESS       = 93, --交易行下架成功
    ITEM_GET_WAY_CLOUD_PEAK_REWARD        = 94, --青云之巅通关一层的奖励
    ITEM_GET_WAY_BEG_PUBLISH_FAIL         = 95,  --拍卖行发布失败，返回金钱
    ITEM_GET_WAY_BEG_CANCLE_SUCESS        = 96,  --求购，撤销成功
    ITEM_GET_WAY_BEG_SELL_SUCESS          = 97,  --求购，出售成功
    ITEM_GET_WAY_BEG_SELL_FAIL            = 98,  --求购，出售失败
    ITEM_GET_WAY_BEG_BE_SELL              = 99,  --求购，求到了。
    ITEM_GET_CIRCLE_TASK_RING             = 100, --循环任务。环奖励
    ITEM_GET_WEEK_TASK                    = 101, --周任务。
    ITEM_GET_WEEK_TASK_RING               = 102, --周任务。环奖励
    ITEM_GET_GUARDIAN_EXPLAIN             = 103, --分解守护结晶
    ITEM_GET_WAY_GUILD_GUARD              = 104, --公会守卫
    ITEM_GET_WAY_GUILD_BONFIRE_GIFT       = 105, --公会篝火烧猪。
    TEM_GET_WAY_GUILD_BONFIRE_EXP         = 106, --公会篝火飘经验
    TEM_GET_WAY_GUILD_BONFIRE_ANSWER      = 107, --公会篝火答题
    ITEM_GET_WAY_QUESTION_RANK            = 108, --答题排名奖励。
    ITEM_GET_WAY_TOWER_DEFENSE_SWEEP      = 109, --塔防副本扫荡奖励
    ITEM_GET_WAY_GUILD_CONQUEST_REWARD    = 110, --争霸奖励
    ITEM_GET_DROP_ITEM                    = 111, --怪物掉落
    ITEM_GET_WAY_GOD_ISLAND_TIMER_GET     = 112, --神兽岛每隔一段时间加经验
    ITEM_GET_BENEFIT                      = 113, --每日签到奖励
    ITEM_GET_CUMULATIVE_BENEFIT           = 114, --累计签到额外奖励
    ITEM_GET_WAY_REWARD_SEVENDAY          = 115, --7天登录奖励
    ITEM_GET_WAY_DRAGON_SPIRIT_COMPOSE    = 116, --龙魂合成(拆解)返还
    ITEM_GET_WAY_DRAGON_SPIRIT_RESOLVE    = 117, --龙魂分解返还
    ITEM_GET_WAY_DRAGON_SPIRIT_UPLEVEL_BACK = 118, --龙魂升级消耗返还
    ITEM_GET_MONTH_CARD_DAILY_REFUND        = 119, --月卡每日返利
    ITEM_GET_WAY_LV_INVEST                  = 120,--等级投资称号奖励
    ITEM_GET_WAY_LV_INVEST_ADD              = 121,--等级投资追加奖励
    ITEM_GET_WAY_UNLOAD_GOD_MONSTER_EQUIP   = 122, --卸神兽装备
    ITEM_GET_WAY_MARRY_PROPOSE_FAIL         = 123,--求婚 错误的返回
    ITEM_GET_WAY_INVEST_VIP                 = 124,--vip投资奖励
    ITEM_GET_WAY_MARRY_PROPOSE_REJECT       = 125,--求婚拒绝的返回
    ITEM_GET_WAY_MARRY_DONE                 = 126,--求婚成功
    ITEM_GET_CUMULATIVE_CHARGE_REFUND       = 127,--开服累充奖励
    ITEM_GET_CUMULATIVE_CHARGE_AWARD_REMAIN = 128,--开服累充邮件发放活动结束未领取奖励
    ITEM_GET_WAY_OVERLORD_GUILD_MULTI_WIN   = 129, --连胜奖励
    ITEM_GET_WAY_OVERLORD_GUILD_END_OTHERS  = 130, --终结其他公会奖励
    ITEM_GET_WAY_COLLECT_WORD               = 131, --集字有礼活动奖励
    ITEM_GET_NOTICE_REWARD                  = 132, --更新公告奖励
    ITEM_GET_WAY_CHARGE                     = 133, --充值
    ITEM_GET_WAY_GUILD_OVERLORD_DAILY_REWARD = 134, --主宰公会每日奖励
    ITEM_GET_LEVEL_UP_REWARD                = 135, --冲级奖励
    ITEM_GET_WAY_DAILY_CHARGE               = 136, --每日首充奖励
    ITEM_GET_WAY_DAILY_CHARGE_CUMULATIVE    = 137, --每日首充累计天数奖励
    ITEM_GET_WAY_MARRY_BE_INVITED           = 138, --婚礼被邀请
    ITEM_GET_WAY_FIRST_CHARGE_REWARD        = 139, --开服首充奖励
    ITEM_GET_PERFECT_LOVER_REWARD           = 140, --完美情人
    ITEM_GET_OPEN_SERVER_GUILD_CONQUEST     = 141, --开服工会争霸
    ITEM_GET_WAY_OPEN_SERVER_RANK           = 142, --开服冲榜奖励(主动领取的)
    ITEM_GET_WAY_ROULETTE_REWARD            = 143, --欢乐云盘普通奖励
    ITEM_GET_WAY_ROULETTE_LUCK_REWARD       = 144, --欢乐云盘幸运大奖
    ITEM_GET_WAY_MARRY_FOOD                 = 145, --婚礼美食
    ITEM_GET_WAY_MARRY_EXP                  = 146, --婚礼飘经验
    ITEM_GET_WAY_MARRY_BOOK                 = 147, --预约婚礼成功
    ITEM_GET_DEMON_RING_REWARD              = 148, --魔戒寻主
    ITEM_GET_WAY_BOOK                       = 149, --预约婚礼
    ITEM_GET_WAY_MARRY_SEND_RED_FAIL        = 150, --婚礼送钱失败返还
    ITEM_GET_WAY_MARRY_SEND_FLOWER_FAIL     = 151, --婚礼送花失败返还
    ITEM_GET_WAY_MARRY_BE_SEND_RED          = 152, --婚礼被送钱
    ITEM_GET_WAY_EQUIP_LOTTERY_ONE          = 153, --装备寻宝单抽
    ITEM_GET_WAY_EQUIP_LOTTERY_TEN          = 154, --装备寻宝十连抽
    ITEM_GET_WAY_EQUIP_LOTTERY_FIFTY        = 155, --装备寻宝50抽
    ITEM_GET_MARRY_BOX_DAILY_REFUND         = 156, --结婚宝匣每日奖励
    ITEM_GET_MARRY_BOX_REFUND               = 157, --结婚宝匣立即返利
    ITEM_GET_WAY_EQUIP_LOTTERY_SCORE_EXCHANGE = 158, --寻宝积分兑换
    ITEM_GET_WAY_EQUIP_LOTTERY_TAKE_OUT     = 159, --寻宝仓库取出
    ITEM_GET_WEEK_CARD_DAILY_REFUND         = 160, --周卡每日返利
    ITEM_GET_WAY_SEND_FLOWER_FAIL           = 161, --送花失败
    ITEM_GET_WAY_OPEN_SERVER_RANK_REWARD_MAIL = 162, --开服冲榜奖励(活动结束发邮件的)
    ITEM_GET_WAY_GULD_RED_RETURN            = 163, --公会红包，超时，返回剩余的钱
    ITEM_GET_WAY_GUIDE_BUY                  = 164, --引导购买
    ITEM_GET_WAY_CHARGE_FIRST               = 165, --首次充值饭绑钻
    ITEM_GET_COLLECT_ITEM                   = 166, --采集获得
    ITEM_GET_WAY_LV_INVEST_REWARD           = 167, --等级投资奖励
    ITEM_GET_OPEN_CARD_CRYSTAL              = 168, --翻牌集福福字奖励
    ITEM_GET_OPEN_CARD_AWARD_REMAIN         = 169, --翻牌集福没领
    ITEM_GET_OPEN_CARD                      = 170, --翻牌集福翻牌奖励
    ITEM_GET_WAY_WISH                       = 171, --欢乐许愿
    ITEM_GET_MAIL_GM_TOOL                   = 172, --web gm 后台发邮件
    ITEM_GET_WAY_WEB_GIFT_CODE              = 173, --web 礼包码
    ITEM_GET_MARRY_INST                     = 174, --结婚副本掉落
    ITEM_GET_WAY_BEG_TIMEOUT_DEL            = 175, --求购，超时，返还
    ITEM_GET_WAY_DAILY_CONSUME              = 176, --连续消费奖励
    ITEM_GET_WAY_DAILY_CONSUME_CUMULATIVE   = 177, --连续消费累计天数奖励
    ITEM_GET_LEVEL_UP_SALE                  = 178, --购买升级限时销售商品
    ITEM_GET_WAY_JIANBAO                    = 179, --全民鉴宝
    ITEM_GET_WEEKLY_CUMULATIVE_C_REFUND     = 180, --周累充
    ITEM_GET_WAY_WEEK_LOGIN                 = 181, --周活动登录
    ITEM_GET_WAY_ACTIVITY_FIREWORK          = 182, --庆典烟花
    ITEM_GET_WAY_DAILY_CONSUME_AUTO         = 183, --连续消费跨天自动补领
    ITEM_GET_WAY_HOLY_WEAPON_COST_RANK      = 184, --神兵现世消费榜
    ITEM_GET_WAY_EXCHANGE                   = 185, --庆典兑换
    ITEM_GET_WAY_BATTLE_SOUL                = 186, --战场之魂伤害排行奖励
    ITEM_GET_WAY_HI                         = 187, --hi点
    ITEM_GET_WAY_ACTIVITY_FIREWORK_LOW      = 188, --庆典烟花低保奖励
    ITEM_GET_ONLINE_REWARD                  = 189, --在线奖励
    ITEM_GET_WAY_ZERO_PRICE                 = 190, --0元购
    ITEM_GET_WAY_ZERO_PRICE_REBATE          = 191, --0元购返还奖励
    ITEM_GET_WAY_DAZZLE_SALE                = 192, --炫装特卖
    ITEM_GET_WAY_LOTTERY_EQUIP_TAKE_OUT     = 193, --装备寻宝仓库取出
    ITEM_GET_BOSS_REWARD                    = 194, --BOSS悬赏
    ITEM_GET_WAY_OPEN_SERVER_BUY            = 195, --开服冲榜限购
    ITEM_GET_WAY_ADD_RUN                    = 196, --加符文,道具效果
    ITEM_GET_WAY_RUN_EXCHANGE               = 197, --加符文，兑换
    ITEM_GET_WAY_RUN_LUCK                   = 198, --加符文，单抽
    ITEM_GET_WAY_RUN_LUCK10                 = 199, --加符文，10抽
    ITEM_GET_WAY_ADD_DRAGON                 = 200, --加龙魂，道具效果
    ITEM_GET_WAY_ADD_DRAGON_COMPOSE         = 201, --加龙魂，合成
    ITEM_GET_WAY_ADD_DRAGON_RESTORE         = 202, --加龙魂，拆解返回
    ITEM_GET_WAY_ANCIENT_BATTLE_POINT_REWARD= 203, --上古战场积分奖励
    ITEM_GET_WAY_ANCIENT_BATTLE_RANK_REWARD = 204, --上古战场排名奖励
    ITEM_GET_WAY_ANCIENT_BATTLE_WIN_REWARD  = 205, --上古战场胜方奖励
    ITEM_GET_FRAUD_GUILD                    = 206, --开宗立派
    ITEM_GET_WAY_ANCIENT_BATTLE_LOSE_REWARD = 207, --上古战场负方奖励
    ITEM_GET_FREAK                          = 208, --开服乱嗨
    ITEM_GET_WAY_LIMITED_SALE               = 209, --限时抢购2
    ITEM_GET_WAY_VIP_LEVEL_UP               = 210, --vip冲级礼包
    ITEM_GET_WAY_EQUIP_QUALITY_BACK         = 211, --升品降级返还
    ITEM_GET_WAY_LOCAL_GOD_ISLAND_TIMER_GET = 212, --神兽岛(单服)每隔一段时间加经验
    ITEM_GET_WAY_ALTAR                      = 213, --深渊祭坛
    ITEM_GET_WAY_LOTTERY_PEAK_ONE           = 214, --巅峰寻宝单抽
    ITEM_GET_WAY_LOTTERY_PEAK_TEN           = 215, --巅峰寻宝十连抽
    ITEM_GET_WAY_LOTTERY_PEAK_FIFTY         = 216, --巅峰寻宝50抽
    ITEM_GET_WAY_LOTTERY_PEAK_TAKE_OUT      = 217, --巅峰寻宝仓库取出
    ITEM_GET_WAY_LOTTERY_PEAK_SCORE_EXCHANGE = 218, --巅峰寻宝积分兑换
    
-----------------------系统邮件编号-------------------------------------------------
    --发送没有(titel,text,from)内容的系统邮件需要定义一个编号，在系统邮件表格中找对应的内容。
    --(titel,text,from)=mail_id(就由mail_id读取系统邮件表确定这三个参数内容)
    --不用reason渠道来代替这个mail_id，因为两者功能独立。
    --MAIL_ID_PEDDLE_OFF                = 1001, --摆摊，下架发送的邮件
    --MAIL_ID_PEDDLE_BUY                = 1002, --摆摊系统，购买道具发的邮件
    --MAIL_ID_LIFE_SKILL                = 1003, --生活技能制造道具
    --MAIL_ID_LIFE_SKILL_COLLECT        = 1004, --生活技能采集
    --MAIL_ID_EQUIP_MAKE                = 1005, --制造装备
    MAIL_ID_TASK_COMMIT                 = 1006, --任务系统提交。--背包满，发邮件
    MAIL_ID_INST_SETTLE                 = 1007, --副本结算。--背包满，发邮件
    --MAIL_ID_EVENT_ACTIVIY             = 1008, --野外事件
    --MAIL_ID_WELFARE_LEVEL_UP_REWARD     = 1009, --福利系统等级礼包奖励
    --MAIL_ID_WELFARE_ONLINE_TIME_REWARD  = 1010, --在线时间礼包奖励
    --MAIL_ID_WELFARE_WEEKDAY_REWARD      = 1011, --每日奖励
    --MAIL_ID_WELFARE_7DAY_REWARD         = 1012, --7天登陆奖励
    --MAIL_ID_DAILY_TASK_COMMIT         = 1013, --日常任务系统提交
    --MAIL_ID_DAILY_TASK_POINT          = 1014, --日常任务系统  日常任务点
    MAIL_ID_ACHIEVEMENT_COMMIT          = 1015, --成就系统提交。--背包满，发邮件
    --MAIL_ID_ACHIEVEMENT_POINT         = 1016, --成就系统成就点
    --MAIL_ID_AUCTION                   = 1017, --交易行购买成功，发送道具
    --MAIL_ID_BLACKMARKET               = 1018, --黑市竞拍成功，发送道具
    --MAIL_ID_EQUIP_BREAK               = 1019, --分解装备
    --MAIL_ID_LADDER_RANK               = 1020, --天梯排行榜
    --MAIL_ID_LADDER_WEEK_REWARD        = 1021, --天梯周奖励
    --MAIL_ID_LADDER_SEASON_REWARD      = 1022, --天梯赛季奖励
    --MAIL_ID_AUCTION_SELL_SUCCESS      = 1023, --成功出售
    MAIL_ID_AUCTION_TIMEOUT_DEL         = 1024, --交易行商品超时下架。
    --MAIL_ID_BLACKMARKET_RETURN_PRICE  = 1025, --黑市返还交易金
    --MAIL_ID_GUILD_SIGN_ON_REWARD      = 1026, --公会签到奖励
    --MAIL_ID_GUILD_SHOP_BUY            = 1027, --公会商店购买道具
    --MAIL_ID_EQUIP_INHER               = 1028, --继承时宝石退还
    --MAIL_ID_GUILD_RAID_REWARD         = 1029, --公会boss奖励
    --MAIL_ID_WORLD_BOSS_REWARD         = 1030, --世界boss奖励
    --MAIL_ID_WORLD_BOSS_GUILD_REWARD   = 1031, --公会世界boss奖励
    --MAIL_ID_PRESTIGE_SHOP_BUY         = 1032, --声望商店购买道具
    --MAIL_ID_LIFE_SKILL_TASK           = 1033, --生活技能任务
    MAIL_ID_AION_PASS                   = 1033, --永恒之塔通关。--背包满，发邮件
    --MAIL_ID_DIVINE                    = 1034, --占星抽奖
    --MAIL_ID_PVP_MATCH                   = 1035, --pvp
    MAIL_ID_AION                        = 1036, --永恒之塔。每日邮件
    --MAIL_ID_WELFARE_MONTH_REWARD        = 1037, --福利系统月奖励
    --MAIL_ID_DUDE_BREAK_DOWN           = 1038, -- 随从重复获得分解
    --MAIL_ID_DUDE_ASSIST_REWARD        = 1039, --随从助战奖励
    --MAIL_ID_DUDE_TASK_REWARD          = 1040, --完成随从任务奖励
    --MAIL_ID_BOUNTY_MATCH_REWARD       = 1041, --赏金赛奖励
    MAIL_ID_BE_KILL                     = 1042, --在xx场景被xx杀了。--{3}，你在{1}被{2}击杀
    MAIL_ID_BE_KILL_RED                 = 1043, --在xx场景被xx杀了,扣了xx数量的xx。红名情况会被扣。--{3}，你在{1}被{2}击杀，并扣除了{4}{ItemName_{5}}
    MAIL_ID_HOLY_TOWER                  = 1044, --守护结界塔通关奖励。--背包满，发邮件
    MAIL_ID_CIRCLE_TASK                 = 1045, --循环任务。--背包满，发邮件
    MAIL_ID_HANG_UP_OFFLINE             = 1046, --离线挂机奖励 --背包满，发邮件
    MAIL_ID_INSTANCE_GOLD               = 1047, --金币副本通关奖励。--背包满，发邮件
    MAIL_ID_INSTANCE_EQUIPMENT          = 1048, --装备副本通关奖励。--背包满，发邮件
    MAIL_ID_ACTIVITY                    = 1049, --活动系统。--背包满，发邮件
    MAIL_ID_USE_ITEM                    = 1050, --使用道具礼包。--背包满，发邮件
    MAIL_ID_VIP                         = 1051, --VIP系统奖励。--背包满，发邮件
    MAIL_ID_GUILD_DAILY_REWARD          = 1052, --公会每日奖励。--背包满，发邮件
    MAIL_ID_CROSS_VS11_MATCH_REWARD     = 1053, --跨服实时1v1单场奖励。--背包满，发邮件
    MAIL_ID_CROSS_VS11_SEASON_RANK_RWD  = 1054, --跨服实时1v1赛季排名奖励
    MAIL_ID_CROSS_VS11_SEASON_STAGE_RWD = 1055, --跨服实时1v1赛季段位奖励
    MAIL_ID_CROSS_VS11_DAY_COUNT_RWD    = 1056, --跨服实时1v1每日参与次数奖励
    MAIL_ID_CROSS_VS11_SEASON_GX_RWD    = 1057, --跨服实时1v1赛季功勋奖励
    MAIL_ID_GUILD_WAREHOUSE_SELL_FAIL   = 1058, --公会捐献失败，返回道具。--背包满，发邮件
    MAIL_ID_GUILD_WAREHOUSE_BUY         = 1059, --公会仓库购买成功。--背包满，发邮件
    MAIL_ID_AUCTION_SELL_FAIL           = 1060, --拍卖行卖出失败，返回道具。--背包满，发邮件
    MAIL_ID_AUCTION_BUY_SUCESS          = 1061, --拍卖行买入成功。--您在市场中成功购买{1}个{ItemName_{2}} ，请查收。
    MAIL_ID_AUCTION_BE_BUY              = 1062, --拍卖行商品被购买。给邮件钻石附件。您在市场中成功出售{1}个{ItemName_{2}} ，共盈利{3}钻石，请查收。
    MAIL_ID_AUCTION_OFF_SUCESS          = 1063, --交易行下架成功。--背包满，发邮件
    MAIL_ID_CLOUD_PEAK_REWARD           = 1064, --青云之巅奖励。--背包满，发邮件
    MAIL_ID_BEG_SELL_FAIL               = 1065, --求购，出售失败,返回道具。--背包满，发邮件
    MAIL_ID_BEG_BE_SELL                 = 1066, --求购，求到了。您在市场中求购的{1}个【{ItemName_{2}} 】已成功到货，请查收.
    MAIL_ID_WEEK_TASK                   = 1067, --周任务。--背包满，发邮件
    MAIL_ID_WEEK_TASK_RING              = 1068, --循环任务。环奖励。--背包满，发邮件
    MAIL_ID_GUILD_BONFIRE_GIFT          = 1069, --公会篝火烧猪。--背包满，发邮件
    MAIL_ID_QUESTION_RANK               = 1070, --答题排名奖励。--背包满，发邮件
    MAIL_ID_CIRCLE_TASK_RING            = 1071, --循环任务。环奖励。--背包满，发邮件
    MAIL_ID_REWARD_SEVENDAY             = 1072, --7天登录奖励。--背包满，发邮件
    --MAIL_ID_GUILD_CONQUEST_REWARD     = 1073, --争霸奖励。
    MAIL_ID_DROP_ITEM                   = 1074, --怪物掉落。--背包满，发邮件
    MAIL_ID_LV_INVEST                   = 1075, --等级投资奖励。
    MAIL_ID_INVEST_VIP                  = 1076, --VIP投资奖励
    MAIL_ID_TOWER_DEFENSE_SWEEP         = 1077, --塔防扫荡 --背包满，发邮件
    MAIL_ID_MARRY_PROPOSE_REJECT        = 1078,--求婚拒绝的返回金钱
    MAIL_ID_MARRY_DONE                  = 1079,--求婚成功
    MAIL_ID_BENEFIT                     = 1080, --每日签到相关奖励。 --背包满，发邮件
    MAIL_ID_CUMULATIVE_BENEFIT          = 1081, --累计签到额外奖励。 --背包满，发邮件
    MAIL_ID_MARRY_DIVORCE               = 1082, --离婚
    MAIL_ID_CUMULATIVE_CHARGE_REFUND    = 1083, --开服累充奖励
    MAIL_ID_CUMULATIVE_CHARGE_AWARD_REMAIN  = 1084, --开服累充邮件发放活动结束未领取奖励
    MAIL_ID_GUILD_OVERLORD_TITLE_REWARD     = 1085, --主宰神殿特权奖励
    MAIL_ID_GUILD_OVERLORD_WIN_MULTI_REWARD = 1086, --主宰神殿连胜奖励
    MAIL_ID_GUILD_OVERLORD_END_OTHER_REWARD = 1087, --终结奖励
    MAIL_ID_GUILD_OVERLORD_NOTIFY_MEMBER    = 1088, --主宰神殿成员邮件
    MAIL_ID_COLLECT_WORD                = 1089, --集字有礼奖励。 --背包满，发邮件
    MAIL_ID_NOTICE_REWARD               = 1090, --更新公告奖励
    MAIL_ID_FIRST_CHARGE_REWARD         = 1091, --开服首充奖励
    MAIL_ID_GUILD_OVERLORD_MEMBER_DAILY_REWARD = 1092, --主宰公会每日奖励
    MAIL_ID_LEVEL_UP_REWARD             = 1093, --冲级奖励
    MAIL_ID_DAILY_CHARGE                = 1094, --每日首充奖励
    MAIL_ID_DAILY_CHARGE_CUMULATIVE     = 1095, --每日首充累计天数奖励
    MAIL_ID_MARRY_BE_INVITED            = 1096, --婚礼被邀请
    MAIL_ID_PERFECT_LOVER_REWARD        = 1097, --完美情人
    MAIL_ID_OPEN_SERVER_GUILD_CONQUEST  = 1098, --开服工会争霸
    MAIL_ID_OPEN_SERVER_RANK            = 1099, --开服冲榜
    MAIL_ID_ROULETTE_REWARD             = 1100, --欢乐云盘普通奖励
    MAIL_ID_ROULETTE_LUCK_REWARD        = 1101, --欢乐云盘幸运大奖
    MAIL_ID_ROULETTE_OPEN               = 1102, --欢乐云盘新一轮开启
    MAIL_ID_MARRY_FOOD                  = 1103, --婚礼美食。--背包满，发邮件
    MAIL_ID_MARRY_BOOK                  = 1104, --预约婚礼成功
    MAIL_ID_DEMON_RING_REWARD           = 1105, --魔戒寻主
    MAIL_ID_MARRY_SEND_FLOWER_FAIL      = 1106,  --婚礼送花失败返还。--背包满，发邮件
    MAIL_ID_MARRY_BOX_DAILY_REFUND      = 1107, --结婚宝匣每日奖励
    MAIL_ID_MARRY_BOX_REFUND            = 1108, --结婚宝匣立即返利
    MAIL_ID_WEEK_CARD_DAILY_REFUND      = 1109, --周卡每日返利
    MAIL_ID_SEND_FLOWER_FAIL            = 1110, --送花失败
    MAIL_ID_LV_INVEST_ADD               = 1111, --等级投资追加投资补偿
    MAIL_ID_OPEN_SERVER_RANK_END_REWARD = 1112, --开服冲榜活动结束未领取的奖励
    MAIL_ID_GULD_RED_RETURN             = 1113, --公会红包，超时，返回剩余的钱
    MAIL_ID_GUILD_CONQUEST_REWARD_1_1   = 1114, --争霸奖励,第一轮胜
    MAIL_ID_GUILD_CONQUEST_REWARD_1_0   = 1115, --争霸奖励，第一轮失败
    MAIL_ID_GUILD_CONQUEST_REWARD_2_1   = 1116, --争霸奖励，第二轮胜
    MAIL_ID_GUILD_CONQUEST_REWARD_2_0   = 1117, --争霸奖励，第二轮失败
    MAIL_ID_GM                          = 1118, --GM奖励
    MAIL_ID_COLLECT_ITEM                = 1119, --采集获得 --背包满，发邮件
    MAIL_ID_LV_INVEST_REWARD            = 1120, --等级投资奖励，背包满，发邮件
    MAIL_ID_OPEN_CARD_CRYSTAL           = 1121, --翻牌集福福字奖励背包满
    MAIL_ID_OPEN_CARD_AWARD_REMAIN      = 1122, --翻牌集福未领取的
    MAIL_ID_OPEN_CARD                   = 1123, --翻牌集福翻牌奖励背包满
    MAIL_ID_WISH                        = 1124, --欢乐许愿奖励，背包满，发邮件
    MAIL_ID_WEB_GIFT_CODE               = 1125, --根据礼包码领取的邮件
    MAIL_ID_MARRY_INST                  = 1126, --结婚副本掉落背包满
    MAIL_ID_BEG_TIMEOUT_DEL             = 1127, --求购，超时，返还
    MAIL_ID_ROULETTE_NONE               = 1128, --上轮没人中奖
    MAIL_ID_DAILY_CONSUME               = 1129, --连续消费奖励
    MAIL_ID_DAILY_CONSUME_CUMULATIVE    = 1130, --连续消费累计天数奖励
    MAIL_ID_LEVEL_UP_SALE               = 1131, --升级限时销售背包满
    MAIL_ID_JIANBAO                     = 1132, --全民鉴宝。--背包满，发邮件
    MAIL_ID_WEEKLY_CUMULATIVE_C_REFUND  = 1133, --周累充
    MAIL_ID_WEEK_LOGIN                  = 1134, --周活动登录。--背包满，发邮件
    MAIL_ID_ACTIVITY_FIREWORK           = 1135, --庆典烟花。--背包满，发邮件
    MAIL_ID_DAILY_CONSUME_AUTO          = 1136, --连续消费跨天自动补领
    MAIL_ID_HOLY_WEAPON_COST_RANK       = 1137, --神兵现世消费榜
    MAIL_ID_EXCHANGE                    = 1138, --庆典兑换。--背包满，发邮件
    MAIL_ID_DOUBLE_DAY_ONE              = 1139, --双倍第一天
    MAIL_ID_DOUBLE_DAY_TWO              = 1140, --双倍第二天
    MAIL_ID_DOUBLE_DAY_THREE            = 1141, --双倍第三天
    MAIL_ID_BATTLE_SOUL_TOP             = 1142, --战场之魂伤害前十
    MAIL_ID_BATTLE_SOUL_COMMON          = 1143, --战场之魂保底奖
    MAIL_ID_HI                          = 1144, --hi点系统。--背包满，发邮件
    MAIL_ID_ACTIVITY_FIREWORK_LOW       = 1145, --庆典烟花低保奖励。--背包满，发邮件
    MAIL_ID_ONLINE_REWARD               = 1146, --在线奖励背包满
    MAIL_ID_ZERO_PRICE_BUY              = 1147, --0元购，背包满，发邮件
    MAIL_ID_ZERO_PRICE_REBATE           = 1148, --0元购返还奖励
    MAIL_ID_DAZZLE_SALE                 = 1149, --炫装特卖
    MAIL_ID_BOSS_REWARD                 = 1150, --boss悬赏背包满
    MAIL_ID_OPEN_SERVER_BUY             = 1151, --开服冲榜限购 --背包满，发邮件
    MAIL_ID_REWARD_BACK                 = 1152, --找回--背包满，发邮件
    MAIL_ID_GUARD_GODDESS               = 1153, --护送女神--背包满，发邮件
    MAIL_ID_INST_COMMON                 = 1154, --普通副本结算--背包满，发邮件
    MAIL_ID_OFFLINE_PVP                 = 1155, --离线竞技结算--背包满，发邮件
    MAIL_ID_ANCIENT_BATTLE_POINT_REWARD = 1156, --上古战场积分奖励 --背包满，发邮件
    MAIL_ID_ANCIENT_BATTLE_RANK_REWARD  = 1157, --上古战场排名奖励
    MAIL_ID_ANCIENT_BATTLE_WIN_REWARD   = 1158, --上古战场胜方奖励
    MAIL_ID_FRAUD_GUILD                 = 1159, --开宗立派背包满
    MAIL_ID_ANCIENT_BATTLE_LOSE_REWARD  = 1160, --上古战场负方奖励
    MAIL_ID_FREAK                       = 1161, --开服乱嗨背包满
    MAIL_ID_LIMITED_SALE                = 1162, --限时抢购2
    MAIL_ID_SKY_DROP                    = 1163, --天降秘宝，背包满
    MAIL_ID_VIP_LEVEL_UP                = 1164, --vip冲级礼包，背包满
    MAIL_ID_EQUIP_QUALITY_BACK          = 1165, --升品降级返还，背包满
    MAIL_ID_ALTAR                       = 1166, --深渊祭坛。--背包满，发邮件
    
    --货币类型的道具id
    MONEY_TYPE_EXP                = 1,  --经验
    MONEY_TYPE_COUPONS            = 2,  --钻石
    --MONEY_TYPE_XXXXXX           = 3,
    MONEY_TYPE_COUPONS_BIND       = 4,  --绑定钻石 --绑定钻石优先,默认钻石可代替绑定钻石，除非指定不代替，如绑定商城
    MONEY_TYPE_GOLD               = 5,  --金币
    MONEY_TYPE_ACTIVITY           = 6,  --活力值
    MONEY_TYPE_GUILD_CONTRIBUTE   = 7,  --公会贡献
    MONEY_TYPE_PIONEER            = 8,  --先锋点
    MONEY_TYPE_ATHLETICS          = 9,  --竞技点
    MONEY_TYPE_TALENT             = 10, --天赋点
    MONEY_TYPE_FASHION_CLOTHES    = 11, --衣服时装精华
    MONEY_TYPE_RUNE_ESSENCE       = 12, --符文精华
    MONEY_TYPE_FASHION_WEAPON     = 13, --武器时装精华
    MONEY_TYPE_RUNE_PIECE         = 14, --符文碎片
    MONEY_TYPE_AION_DIAMOND       = 21, --永恒结晶
    MONEY_TYPE_GUARDIAN_DIAMOND   = 22, --守护结晶  守护系统（灵宠）
    MONTY_TYPE_DRAGON_PICEC       = 23, --龙魂碎片
    MONEY_TYPE_FASHION_SPECIAL    = 24, --特效时装精华
    MONEY_TYPE_FASHION_PHOTO      = 25, --头像时装精华
    MONEY_TYPE_FASHION_CHAT       = 26, --聊天时装精华
    MONEY_TYPE_CARD_EXP           = 27, --图鉴经验



    --MONEY_TYPE_PRESTIGE_1       = 21, --声望
    --MONEY_TYPE_PRESTIGE_2       = 22,
    --MONEY_TYPE_PRESTIGE_3       = 23,
    --MONEY_TYPE_PRESTIGE_4       = 24,
    --MONEY_TYPE_PRESTIGE_5       = 25,
    --MONEY_TYPE_PRESTIGE_6       = 26,
    --
    MAX_MONEY_ID                  = 50, --小于这个值的item_id是货币,大于等于这个值的是道具或者装备


    --包裹类型
    PKG_TYPE_ITEM               = 1,  --背包
    PKG_TYPE_WAREHOUSE          = 2,  --仓库
    PKG_TYPE_LOADED_EQUIP       = 3,  --已装备的装备
    --PKG_TYPE_SOLD_TMP         = 4,  --已出售道具的临时包裹
    --PKG_TYPE_LOADED_FASHION   = 5,  --已装备的时装
    PKG_TYPE_FASHION            = 6,  --时装背包 --客户端用到
    --PKG_TYPE_EUDEMON          = 7,  --守护灵背包
    --PKG_TYPE_EQUIP_SOUL       = 8,  --已装备魂器
    --PKG_TYPE_DUDE             = 9,  --随从背包
    --PKG_TYPE_RIDE             = 10, --坐骑背包
    PKG_TYPE_RUNE               = 11, --符文背包
    PKG_TYPE_OFFLINE            = 12, --离线背包，纯前端用
    PKG_TYPE_GUILD_WAREHOUSE    = 13, --公会仓库
    PKG_TYPE_GOD_MOSNTER        = 14, --神兽背包
    PKG_TYPE_LOTTERY_EQUIP      = 15, --装备寻宝临时仓库
    PKG_TYPE_CARD_PIECE         = 16, --图鉴碎片背包
    PKG_TYPE_LOTTERY_PEAK       = 17, --巅峰寻宝临时仓库

    --道具类型
    ITEM_ITEMTYPE_EQUIP       = 1,     --装备
    ITEM_ITEMTYPE_ITEM        = 2,     --其他道具
    ITEM_ITEMTYPE_GEM         = 3,     --宝石
    ITEM_ITEMTYPE_FASHION     = 4,     --时装
    ITEM_ITEMTYPE_GOD_MONSTER = 5,     --神兽背包的道具装备
    ITEM_ITEMTYPE_CARD_PIECE  = 6,     --图鉴碎片

    --类型
    ITEM_TYPE_1 = 1,    --
    ITEM_TYPE_2 = 2,

    --子类型
    ITEM_SUBTYPE_1 = 1,
    ITEM_SUBTYPE_2 = 2,

    GEM_TYPE_LEGEND     = 2,        --宝石类型,2表示传奇魔石

    --道具使用效果
    ITEM_EFF_ID_BUFF                  = 1, --加buff
    --ITEM_EFF_ID_HP_STORE            = 2,--加生命储备值
    --ITEM_EFF_ID_MP_STORE            = 3,--加魔法储备值
    ITEM_EFF_ID_DROP_ID               = 4,--获得掉落包里面的道具
    --ITEM_EFF_ID_EUDEMON             = 6, --加守护灵
    --ITEM_EFF_ID_ABYSS               = 8, --深渊进度
    --ITEM_EFF_ID_RIDE                = 9, --加坐骑
    ITEM_EFF_ID_TREASURE_EXP          = 10, --加宝物经验
    ITEM_EFF_ID_TREASURE_ATTRI        = 11, --加宝物属性
    ITEM_EFF_ID_ADD_RATE              = 12, --加加成效果
    ITEM_EFF_ID_ADD_VIPEXP            = 13, --加VIP经验
    ITEM_EFF_ID_ADD_RUNE              = 14, --加符文
    ITEM_EFF_ID_ADD_HANG_UP_TIME      = 15, --加离线挂机时间
    ITEM_EFF_ID_ADD_EXP1              = 16, --加经验。等级，绝对经验。16,240,16176000000000
    ITEM_EFF_ID_ADD_EXP2              = 17, --加经验。万分比。17,10
    ITEM_EFF_ID_CHOOSE_ITEM           = 18, --选择加道具(要选择)。18,道具id,数量,道具id,数量
    ITEM_EFF_ID_ADD_EXP_INST          = 19, --加经验副本次数
    --ITEM_EFF_ID_ADD_CARD_PIECE      = 20, --图鉴碎片
    ITEM_EFF_ID_ADD_CONTRIBUTE        = 21, --加公会贡献
    ITEM_EFF_ID_ADD_DRAGON_SPIRIT     = 22, --加龙魂
    ITEM_EFF_ID_ADD_GOD_MONSTER_ITEM  = 23, --加神兽道具
    ITEM_EFF_ID_ADD_TITLE             = 24, --加称号
    ITEM_EFF_ID_PAID_USE              = 25, --使用有消耗
    ITEM_EFF_ID_HOT_DEGREE            = 26, --加热度 --烟花道具
    ITEM_EFF_ID_MARRY_EXP             = 27, --加婚戒经验
    ITEM_EFF_ID_WORLD_BOSS_PILAO      = 28, --世界BOSS疲劳恢复
    ITEM_EFF_ID_GOD_ISLAND_PILAO      = 29, --神兽岛疲劳恢复
    ITEM_EFF_ID_CHARGE                = 30, --充值
    ITEM_EFF_ID_PK_VALUE              = 31, --减少PK值
    ITEM_EFF_ID_SKY_DROP              = 32, --天降秘宝奖励

    --能染色的时装type
    FASHION_COLOR_TYPE_INDEX_MIN = 1,
    FASHION_COLOR_TYPE_INDEX_MAX = 4,

    --每个道具的数据结构中的key定义
    ITEM_KEY_ID                      = 1,  --道具id
    ITEM_KEY_COUNT                   = 2,  --叠加数量,这个值如果为空,表示数量为1
    ITEM_KEY_BIND_FLAG               = 3,  --是否绑定,1:已绑定，这个值如果为空,表示未绑定
    --ITEM_KEY_EQUIP_GRADE             = 10, --不用。--装备档次。
    --ITEM_KEY_EQUIP_STREN_LEVEL       = 11, --不用。--强化等级。
    --ITEM_KEY_EQUIP_WASH_IDX          = 12, --不用。--该装备已选择的洗练属性位置。
    --ITEM_KEY_EQUIP_WASH_ATTRI_ID     = 13, --不用。--上一次洗练出来的属性id,可以选择替换现有值 。
    --ITEM_KEY_EQUIP_WASH_ATTRI_VALUE  = 14, --不用。--上一次洗练出来的属性值,可以选择替换现有值
    --ITEM_KEY_EQUIP_WASH_ATTRI2_ID    = 15, --不用。--上一次洗练出来的属性id,可以选择替换现有值
    --ITEM_KEY_EQUIP_WASH_ATTRI2_VALUE = 16, --不用。--上一次洗练出来的属性值,可以选择替换现有值
    --ITEM_KEY_EQUIP_WASH_ATTRI3_ID    = 17, --不用。--上一次洗练出来的属性id,可以选择替换现有值
    --ITEM_KEY_EQUIP_WASH_ATTRI3_VALUE = 18, --不用。--上一次洗练出来的属性值,可以选择替换现有值  --先写死3条临时洗练属性
    --ITEM_KEY_INDEX_ATTRI_MIN         = 21, --不用。
    --ITEM_KEY_EQUIP_BASIC_ATTRI_MIN   = 21, --不用。--装备基础属性,预留21-39(40),可存储10条属性。当然也是生成装备时随机。且强化属性对其有加成。
    --ITEM_KEY_EQUIP_BASIC_ATTRI_MAX   = 39, --不用。
    ITEM_KEY_EQUIP_RAND_ATTRI_MIN    = 41, --装备随机属性,预留41-59(60),可存储10条属性。
    ITEM_KEY_EQUIP_RAND_ATTRI_MAX    = 59, --
    --ITEM_KEY_EQUIP_STREN_ATTRI_MIN   = 61, --不用。--装备强化属性,预留61-69(70),可存储5条属性。没看到有赋值。--不用。
    --ITEM_KEY_EQUIP_STREN_ATTRI_MAX   = 69, --不用。
    --ITEM_KEY_INDEX_ATTRI_MAX         = 69, --不用。
    --ITEM_KEY_GEM_MIN                 = 71, --不用。--已镶嵌宝石,预留71-79
    --ITEM_KEY_GEM_MAX                 = 79, --不用。
    --ITEM_KEY_LGEM_LOCK               = 81, --不用。--传奇魔石锁定标记,这个值如果为空表示未加锁。--删除传奇魔石。--不用
    --ITEM_KEY_LGEM_EXP                = 82, --不用。--传奇魔石当前经验。                                                               --删除传奇魔石。--不用
    --ITEM_KEY_SELL_PRICE              = 99, --不用。--出售给临时背包的总价
    --ITEM_KEY_LGEM_EXP_MIN            = 101,--不用。--装备已镶嵌传奇宝石的经验,和71-79对应,偏移+30。 --删除传奇魔石。--不用
    --ITEM_KEY_LGEM_EXP_MAN            = 109,--不用。
    --ITEM_KEY_SKILL_MIN               = 110,--不用。 --装备自带的技能 预留110-119。生成装备时随机。只能是被动技能。装备使用。魂器使用。
    --ITEM_KEY_SKILL_MAX               = 119,--不用。
    --ITEM_KEY_TIME                    = 120, --不用。--时装道具，0客户端显示通过读表确定永久还是装备后计时的限时，大于0就是到期时间os.time()>就删除。表格，获得时计时或者装备后处理这个时间参数。
    --ITEM_KEY_COLOR_R                 = 121, --不用。--时装道具，染色id
    --ITEM_KEY_COLOR_G                 = 122, --不用。
    --ITEM_KEY_COLOR_B                 = 123, --不用。
    --ITEM_KEY_EQUIP_SOUL_LEVEL        = 125, --不用。--魂器等级
    --ITEM_KEY_EQUIP_SOUL_EXP          = 126, --不用。--魂器经验
    --ITEM_KEY_EQUIP_STREN_LUCKY       = 127, --不用。--强化幸运值
    --ITEM_KEY_EQUIP_DURABILITY        = 130, --不用。--耐久度
    --ITEM_KEY_NUM_OF_REPAIR           = 131, --不用。--修复次数
    --ITEM_KEY_EQUIP_ENCHANT_ATTRI_MIN = 141, --不用。--141—149(150)附魔的属性 141:attri_id,142:value...
    --ITEM_KEY_EQUIP_ENCHANT_ATTRI_MAX = 149, --不用。
    ITEM_KEY_END_TIME                  = 150, --部位11守护的过期时间。只有大于当前时间才是非过期时间。nil默认转为0，小于当前时间，也是在无效时间中。
    ITEM_KEY_FASHION_STAR              = 151, --时装星级
    ITEM_KEY_GOD_MONSTER_EQUIP_LEVEL      = 152, --神兽装备强化等级
    ITEM_KEY_GOD_MONSTER_EQUIP_EXP        = 153, --神兽装备熟练度
    ITEM_KEY_GOD_MONSTER_EQUIP_TOTAL_EXP  = 154, --总熟练度(强化过的装备才有这个)
    ITEM_KEY_GUARD_FIRST                  = 155, --部位11守护是否任务给的，不触发穿戴事件

    --怪物类型
    MONSTER_TYPE_BEAST   = 1, --野兽
    MONSTER_TYPE_MACHINE = 2, --机械

    MONSTER_TYPE_VEHICON            = 1, --小怪
    MONSTER_TYPE_ELITE              = 2, --精英怪
    MONSTER_TYPE_BOSS               = 3, --boss
    MONSTER_TYPE_WORLD_BOSS         = 4, --世界boss
    MONSTER_TYPE_GOD_ISLAND_BOSS    = 5,--神兽岛boss

    ITEM_BIND_FLAG_FALSE = 0,       --nil或者0表示该道具未绑定。                                                                                                                --add道具，默认非绑定。remove道具，默认绑定(内部绑定不够用非绑)。
    ITEM_BIND_FLAG_TRUE  = 1,       --1表示该道具已经绑定。--消耗道具时，非绑可代替绑定，绑定却不可代替非绑。--add道具，默认非绑定。remove道具，默认绑定(内部绑定不够用非绑)。
    ITEM_BIND_FLAG_EQUIP = 2,       --2表示装备后绑定

    --equip_pos
    --装备类型，同时也是已穿装备背包的槽位id
    --1	2	3	4	5	6	7	8	9	10
    --武器	胸部	手部	脚部	头部	项链	戒指
    PKG_LOADED_EQUIP_INDEX_MIN     = 1,
    PKG_LOADED_EQUIP_INDEX_MAX     = 10,
    PKG_LOADED_EQUIP_INDEX_GUARD   = 11, --11是守护
    PKG_LOADED_EQUIP_INDEX_RING    = 12, --12是婚戒
    
    EQUIP_AUTO_EAT_SELL_INDEX_MAM        = 8, --自动吞噬售卖的装备最大部位

    --PKG_LOADED_EQUIP_ATK_INDEX_MIN = 1, --武器
    --PKG_LOADED_EQUIP_ATK_INDEX_MAX = 2,
    --PKG_LOADED_EQUIP_DEF_INDEX_MIN = 3, --防具
    --PKG_LOADED_EQUIP_DEF_INDEX_MAX = 7,
    --PKG_LOADED_EQUIP_JEWE_INDEX_MIN = 8, --首饰
    --PKG_LOADED_EQUIP_JEWE_INDEX_MAX = 11,

    --PKG_EQUIP_SOUL_INDEX_MIN = 1,
    --PKG_EQUIP_SOUL_INDEX_MAX = 5,

    --附魔类型
    --EQUIP_ENCHANT_TYPE_ATK = 1, --攻击
    --EQUIP_ENCHANT_TYPE_DEF = 2, --防御
    --EQUIP_ENCHANT_TYPE_SPE = 3, --特殊 首饰类

    --飞行技能的槽位
    FLY_SPELL_SLOT_MIN = 21,
    FLY_SPELL_SLOT_MAX = 30,

    --战斗实体类型
    COMBATANT_ETYPE_AVATAR        = 1,
    COMBATANT_ETYPE_MONSTER       = 2,
    COMBATANT_ETYPE_TRAP          = 3, --陷阱
    COMBATANT_ETYPE_EVENT_REGION  = 4, --事件活动区域
    COMBATANT_ETYPE_FORT          = 5, --据点
    COMBATANT_ETYPE_PET           = 6, --宠物
    COMBATANT_ETYPE_FABAO         = 7, --法宝
    COMBATANT_ETYPE_MONSTE_RDESTROY_AREA    = 8, --怪物销毁区域 塔防副本用的

    --邮件相关
    MAIL_TYPE_AVATAR          = 0,         --玩家邮件
    MAIL_TYPE_SYSTEM          = 1,         --系统邮件
    MAIL_TYPE_AVATAR_AUTO_GET = 2,         --玩家邮件,但上线自动取，自动get

    MAIL_TIMEOUT                  = 1296000,  --单位秒数 15天=60*60*24*15=1296000
    MAIL_TIMEOUT_SYSTEM           = 1728000,  --单位秒数 20天=60*60*24*20=1728000
    MAIL_TIMEOUT_AVATAR_AUTO_GET  = 8640000,  --单位秒数 100天=60*60*24*100=8640000

    MAIL_PAGE_CNT        = 50,     --邮件每页数量
    MAIL_ATTACHMENT_CNT  = 4,      --邮件附件数量

    MAIL_STATE_NONE       = 0, --未读,无附件 0 => 2
    MAIL_STATE_HAVE       = 1, --未读,带附件 1 => 3 OR 4
    MAIL_STATE_READ       = 2, --已读,无附件
    MAIL_STATE_HERE       = 3, --已读,附件未领取
    MAIL_STATE_RECE       = 4, --已读,附件已领取

    --邮件luatable相关key
    MAIL_LIST_KEY_MAILS                       = 1,
    MAIL_LIST_KEY_CUR_PAGE                    = 2,
    MAIL_LIST_KEY_TOTAL_PAGE                  = 3,
    MAIL_LIST_KEY_MAIL_CNT                    = 4,
    MAIL_LIST_KEY_NEW_MAIL_CNT                = 5,
    MAIL_LIST_KEY_UNGET_ATTACHEMENT_MAIL_CNT  = 6, --应该是ATTACHMENT

    MAIL_INFO_KEY_MAIL_DBID         = 1,
    MAIL_INFO_KEY_MAIL_TYPE         = 2,
    MAIL_INFO_KEY_MAIL_ID           = 3,
    MAIL_INFO_KEY_TITLE             = 4,
    MAIL_INFO_KEY_TEXT              = 5,
    MAIL_INFO_KEY_FROM              = 6,
    MAIL_INFO_KEY_TIME              = 7,
    MAIL_INFO_KEY_ATTACHEMENT       = 8, --应该是ATTACHMENT
    MAIL_INFO_KEY_REASON            = 9,
    MAIL_INFO_KEY_STATE             = 10,
    MAIL_INFO_KEY_EXTERN_INFO       = 11,
    MAIL_INFO_KEY_PUBLIC_MAIL_DBID  = 12,


    ----------------------各子系统管理器定义一个编号----------------------
    --用于和对应的mgr模块对应等处理
    MGR_ID_MAIL               = 1, --邮件系统
    MGR_ID_TEAM               = 2, --组队系统
    MGR_ID_TASK               = 3, --任务系统
    MGR_ID_INST               = 4, --副本系统
    MGR_ID_CHAT               = 5, --聊天系统
    --MGR_ID_EVENT_ACTIVITY   = 6, --事件活动
    MGR_ID_SHOP               = 7, --商城系统
    --MGR_ID_ACTIVITY           = 8, --活动系统
    --MGR_ID_DAILY_TASK       = 9, --日常任务系统
    MGR_ID_ACHIEVEMENT        = 10, --成就系统
    MGR_ID_AUCTION            = 11, --拍卖行
    MGR_ID_LADDER_MATCH       = 12, --天梯赛1v1
    MGR_ID_ITEM               = 13, --道具系统
    MGR_ID_EQUIP_SOUL         = 14, --魂器
    MGR_ID_AION               = 15, --永恒之塔
    --MGR_ID_ABYSS            = 16, --深渊
    MGR_ID_SPACE              = 17, --场景管理
    --MGR_ID_RIDE             = 18, --坐骑
    --MGR_ID_BOUNTY_MATCH     = 19, --赏金赛
    --MGR_ID_EUDEMON          = 20, --守护灵
    MGR_ID_PK_MODE            = 21, --pk模式
    MGR_ID_TREASURE           = 22, --宝物系统（翅膀 法宝 神兵 披风）
    MGR_ID_PET                = 23, --宠物系统
    MGR_ID_HORSE              = 24, --坐骑系统
    MGR_ID_TITLE              = 25, --称号系统
    MGR_ID_EQUIP_STREN        = 26, --装备强化
    MGR_ID_EQUIP_WASH         = 27, --装备洗练
    MGR_ID_ACHIEVEMENT        = 28, --
    MGR_ID_EQUIP_GEM          = 29, --
    MGR_ID_VIP                = 30, --VIP系统
    MGR_ID_VOC_CHANGE         = 31, --
    MGR_ID_HOLY_TOWER         = 32, --守护结界塔
    MGR_ID_RUNE               = 33, --符文系统
    MGR_ID_COMPOSE            = 34, --合成系统
    MGR_ID_HANG_UP            = 35, --挂机系统
    MGR_ID_COMBAT_ATTRI       = 36, --属性
    MGR_ID_OFFLINE_PVP        = 37, --离线1v1
    MGR_ID_PERSONAL_BOSS      = 38, --个人boss
    MGR_ID_INSTANCE_GOLD      = 39, --金币副本
    MGR_ID_RANK_LIST          = 40, --
    MGR_ID_INSTANCE_EQUIPMENT = 41, --装备副本
    --MGR_ID_SALE             = 42, --市场
    MGR_ID_ACTIVITY           = 43, --活动模块
    MGR_ID_CIRCLE_TASK        = 44, --赏金系统
    MGR_ID_EXP_INSTANCE       = 45, --经验副本
    MGR_ID_GUILD              = 46, --公会
    MGR_ID_EQUIP_SUIT         = 47, --套装
    MGR_ID_CARD               = 48, --图鉴
    MGR_ID_BEG                = 49, --求购
    MGR_ID_WEEK_TASK          = 50, --周任务
    MGR_ID_GUILD_BONFIRE      = 51, --公会篝火
    MGR_ID_GUILD_GUARD        = 52, --守卫公会
    MGR_ID_QUESTION           = 53, --答题
    MGR_ID_GUILD_CONQUEST     = 54, --公会争霸
    MGR_ID_COLLECT_BASE       = 55, --采集
    MGR_ID_TOWER_DEFENSE      = 56, --塔防副本
    MGR_ID_DROP_RECORD        = 57, --掉落记录
    MGR_ID_ILLUSION_TOTAL     = 58, --化形总
    MGR_ID_ILLUSION_HORSE     = 59, --坐骑化形
    MGR_ID_CROSS_GOD_ISLAND   = 60, --世界神兽岛
    MGR_ID_GOD_MONSTER        = 61, --神兽系统
    MGR_ID_DRAGON_SPIRIT      = 62, --龙魂
    MGR_ID_MARRY              = 63, --结婚
    MGR_ID_PEEP               = 64, --查看他人信息
    MGR_ID_CHARGE             = 65, --充值
    MGR_ID_BENEFIT            = 66, --福利
    MGR_ID_DAILY_CHARGE       = 67, --每日首充
    MGR_ID_OPEN_SERVER_RANK   = 68, --开服冲榜
    MGR_ID_FRIEND             = 69, --好友系统
    MGR_ID_MARRY_RING         = 70, --婚戒
    MGR_ID_MARRY_PET          = 71, --仙娃系统
    MGR_ID_SEND_FLOWER        = 72, --送花系统
    MGR_ID_MARRY_INSTANCE     = 73, --仙缘副本
    MGR_ID_WORLD_BOSS         = 74, --世界boss
    MGR_ID_COLLECT_WORD       = 75, --集字有礼
    MGR_ID_OPERATE_LOG        = 76, --日志收集
    MGR_ID_OPEN_CARD          = 77, --翻牌集福
    MGR_ID_ROULETTE           = 78, --欢乐云盘
    MGR_ID_GM                 = 79, --GM
    MGR_ID_DAILY_CONSUME      = 80, --连续消费
    MGR_ID_CHANNEL_ACTIVITY   = 81, --渠道活动管理
    MGR_ID_JIANBAO            = 82, --全民鉴宝
    MGR_ID_ACTIVITY_FIREWORK  = 83, --庆典烟花
    MGR_ID_HOLY_WEAPON_COST   = 84, --神兵现世
    MGR_ID_WEEK_LOGIN         = 85, --周活动登录
    MGR_ID_HI                 = 86, --hi点活动
    MGR_ID_ACTION             = 87, --action
    MGR_ID_SKY_DROP           = 88, --天降秘宝
    MGR_ID_ANCIENT_BATTLE     = 89, --上古战场
    MGR_ID_FRAUD_GUILD        = 90, --开宗立派
    MGR_ID_ADDITION           = 91, --强化星级宝石加成
    MGR_ID_VIP_LEVEL_UP       = 92, --vip冲级礼包
    MGR_ID_EQUIP_QUALITY      = 93, --升品
    MGR_ID_LOCAL_GOD_ISLAND   = 94, --神兽岛单服
    MGR_ID_ALTAR              = 95, --深渊祭坛

    ----------------------UserMgr数据里面key------------------------
    USERINFO_KEY_MAILBOX          = 1,     --用户信息,base的mailbox str
    USERINFO_KEY_DBID             = 2,     --dbid
    USERINFO_KEY_NAME             = 3,     --姓名
    USERINFO_KEY_GENDER           = 4,     --性别
    USERINFO_KEY_VOCATION         = 5,     --职业
    USERINFO_KEY_LEVEL            = 6,     --等级
    USERINFO_KEY_ONLINE           = 7,     --是否在线 1:在线 0：否
    USERINFO_KEY_PLAT_NAME        = 8,     --平台渠道。client_info.channel_id。plat_name为空为""为全平台渠道。只要sdk登陆的不会为空为""。
    USERINFO_KEY_RACE             = 9,     --种族
    USERINFO_KEY_FIGHT_FORCE      = 10,    --战斗力
    USERINFO_KEY_MAP_ID           = 11,    --地图
    USERINFO_KEY_MAP_LINE         = 12,    --分线
    USERINFO_KEY_GUILD_DBID       = 13,    --公会dbid
    USERINFO_KEY_OFFLINE_TIME     = 14,    --离线时间
    USERINFO_KEY_FACADE           = 15,    --外观
    USERINFO_KEY_TITLE_ID         = 16,    --称号id
    USERINFO_KEY_CROSS_MAILBOX    = 17,    --用户信息,base的cross_mailbox str
    USERINFO_KEY_NOBILITY         = 18,    --爵位
    USERINFO_KEY_GUILD_NAME       = 19,    --公会名
    USERINFO_KEY_AION_LEVEL       = 20,    --永恒之塔等级
    USERINFO_KEY_ONLINE_TIME      = 21,    --用户上线时间
    USERINFO_KEY_VIP_LEVEL        = 22,    --用户VIP等级
    USERINFO_KEY_MATE_DBID        = 23,    --配偶dbid
    USERINFO_KEY_MATE_NAME        = 24,    --配偶名字
    USERINFO_KEY_ILLUSION_SHOW_INFO = 25,  -- 翅膀魂器宠物等的显示信息
    USERINFO_KEY_ACCOUNT_NAME     = 26,  -- 账户名称

    --全局数据的key
    GLOBAL_DATA_KEY_WORLD_LEVEL                 = "1",  --世界等级
    GLOBAL_DATA_KEY_SYS_INFO                    = "2",  --广播玩家系统消息，系统id
    GLOBAL_DATA_KEY_SYS_INFO_TEXT               = "3",  --广播玩家提示消息,文本内容.加个平台渠道参数，web后台可控制参数。--plat_name为空为""为全平台渠道。
    GLOBAL_DATA_KEY_WORLD_CHAT                  = "4",  --广播世界聊天消息。如果世界聊天内部再分组，那么就需要userMgr管理分组，然后同样到这里进行帅选
    GLOBAL_DATA_KEY_SUB_SYS_FUNC_CALL           = "5",  --广播 子系统函数调用
    GLOBAL_DATA_KEY_FUNC_CALL                   = "6",  --广播 函数调用
    GLOBAL_DATA_KEY_CLIENT_FUNC_CALL            = "7",  --广播 客户端函数调用
    GLOBAL_DATA_KEY_SCRIPT_HOT_UPDATE           = "8",  --广播热更新lua脚本
    GLOBAL_DATA_KEY_ON_NEW_PUBLIC_MAIL          = "9",  --广播 on_new_publc_mail_resp
    --GLOBAL_DATA_KEY_XML_HOT_UPDATE            = "10",  --广播热更新重新读取xml文件
    GLOBAL_DATA_KEY_5CLOCK                      = "11",  --广播5clock
    GLOBAL_DATA_KEY_0CLOCK                      = "12",  --广播0clock
    --GLOBAL_DATA_KEY_SET_DAILY_EVENT_ACTIVITY  = "13", --广播设置随机出来的日常事件活动
    --GLOBAL_DATA_KEY_CLEAR_DAILY_EVENT_ACTIVITY= "14", --广播清除随机出来的日常事件活动
    --GLOBAL_DATA_KEY_END_EVENT_ACTIVITY        = "15", --广播清除个人事件活动
    GLOBAL_DATA_KEY_SERVER_OPEN_TIME            = "16", --开服时间
    --GLOBAL_DATA_KEY_ABYSS_BUFFS               = "17", --深渊buffs
    GLOBAL_DATA_KEY_NIGHTMARE_INST              = "18", --噩梦副本
    GLOBAL_DATA_KEY_SERVER_CELL_MBSTR           = "19",  --广播所有的本服cell
    GLOBAL_DATA_KEY_SERVER_BASE_MBSTR           = "20",  --广播所有的本服base
    GLOBAL_DATA_KEY_WORLD_AVATAR_DO_FUNC        = "21",  --所有avatar执行方法
    GLOBAL_DATA_KEY_22CLOCK                     = "22",  --广播22clock
    GLOBAL_DATA_KEY_MARRY_QUERY_INVITE          = "23",  --广播MARRY_QUERY_INVITE
    GLOBAL_DATA_KEY_SEND_MAIL_ONLINE            = "24",  --发邮件给在线玩家
    GLOBAL_DATA_KEY_CHANNEL_ACTIVITY            = '25', --渠道活动开启相关数据
    GLOBAL_DATA_KEY_FORBIDDEN_UDID              = '26', --封禁udid
    GLOBAL_DATA_KEY_FORBIDDEN_AVATAR_DBIDS      = '27', --封avatar_dbid
    GLOBAL_DATA_KEY_FORBIDDEN_AVATAR_NAMES      = '28', --封avatar_name
    GLOBAL_DATA_KEY_FORBIDDEN_ACCOUNTS          = '29', --封account
    GLOBAL_DATA_KEY_FORBIDDEN_IPS               = '30', --封ip
    GLOBAL_DATA_KEY_MAIL_MGR                    = '31', --广播MailMgr
    
    --跨服全局数据的key
    CROSS_BROADCAST_DATA_KEY_WORLD_CHAT     = 1,  --跨服频道聊天
    CROSS_BROADCAST_DATA_KEY_SYS_INFO       = 2, --跨服公告


    --生活技能
    --LIFE_SKILL_COLLECT        = 1,    --收集技能
    --LIFE_SKILL_MINING         = 2,    --挖掘技能
    --LIFE_SKILL_FORGE          = 3,    --锻造技能
    --LIFE_SKILL_TAILOR         = 4,    --缝纫技能
    --LIFE_SKILL_WORK           = 5,    --细工技能
    --LIFE_SKILL_JEWELS         = 6,    --珠宝技能
    --LIFE_SKILL_ALCHEMY        = 7,    --炼金技能

    --资源点类型
    RESOURCE_WOOD           = 1,    --木材
    RESOURCE_PLANT          = 2,    --植物
    RESOURCE_ORE            = 3,    --矿石
    RESOURCE_GEM            = 4,    --宝石

    -----------------聊天频道类型-----------------
    CHAT_CHANNEL_SYSTEM         = 1, --系统频道
    CHAT_CHANNEL_WORLD          = 2, --世界频道
    CHAT_CHANNEL_VICINITY       = 3, --附近的人聊天
    CHAT_CHANNEL_SEEK_TEAM      = 4, --组队(喊话)
    CHAT_CHANNEL_TEAM           = 5, --队伍频道
    CHAT_CHANNEL_GUILD          = 6, --公会频道
    CHAT_CHANNEL_CROSS_WORLD    = 7, --跨服频道
    CHAT_CHANNEL_HORN           = 8, --小喇叭
    CHAT_CHANNEL_PRIVATE        = 9, --私聊频道 好友
    CHAT_CHANNEL_GUILD_ANNO     = 10, --公会公告


    --------------------组队相关------------------
    TEAM_STATUS_NO      = 0,         --无队伍 --玩家身上的状态
    TEAM_STATUS_MEMBER  = 1,         --队员
    TEAM_STATUS_CAPTAIN = 2,         --队长

    TEAM_STATE_NONE     = 0,         --None --队伍的状态

    TEAM_MIN_LEVEL      = 1,         --10 --最小可组队等级 --应该配在全局参数表
    TEAM_MAX_COUNT      = 5,         --队伍上限
    TEAM_PAGE_CNT       = 10,        --组队列表分页数


    --组队luatable相关key
    TEAM_ALL_KEY_TEAM_ID              = 1,
    TEAM_ALL_KEY_CAPTAIN_DBID         = 2,
    TEAM_ALL_KEY_TEAM_MEMBER_INFOS    = 3,
    TEAM_ALL_KEY_TARGET_ID            = 4,
    TEAM_ALL_KEY_AUTO_ACCEPT_APPLY    = 5,

    TEAM_MEMBER_INFO_KEY_DBID         = 1,
    TEAM_MEMBER_INFO_KEY_NAME         = 2,
    TEAM_MEMBER_INFO_KEY_GENDER       = 3,
    TEAM_MEMBER_INFO_KEY_VOCATION     = 4,
    TEAM_MEMBER_INFO_KEY_RACE         = 5,
    TEAM_MEMBER_INFO_KEY_LEVEL        = 6,
    TEAM_MEMBER_INFO_KEY_FIGHT_FORCE  = 7,
    TEAM_MEMBER_INFO_KEY_MAP_ID       = 8,
    TEAM_MEMBER_INFO_KEY_MAP_LINE     = 9,
    TEAM_MEMBER_INFO_KEY_EID          = 10,
    TEAM_MEMBER_INFO_KEY_POS_X        = 11,
    TEAM_MEMBER_INFO_KEY_POS_Y        = 12,
    TEAM_MEMBER_INFO_KEY_POS_Z        = 13,
    TEAM_MEMBER_INFO_KEY_HP           = 14,
    TEAM_MEMBER_INFO_KEY_MAX_HP       = 15,
    TEAM_MEMBER_INFO_KEY_GUILD_ID     = 16, --实际是GUILD_DBID
    TEAM_MEMBER_INFO_KEY_ONLINE       = 17,
    TEAM_MEMBER_INFO_KEY_MP           = 18,
    TEAM_MEMBER_INFO_KEY_MAX_MP       = 19,
    TEAM_MEMBER_INFO_KEY_ITEM_COUNT   = 20,
    TEAM_MEMBER_INFO_KEY_FACADE       = 21,
    TEAM_MEMBER_INFO_KEY_EXP_INSTANCE = 22, --exp_instance剩余次数
    TEAM_MEMBER_INFO_KEY_MATCH_STATE  = 23, --匹配状态 0:未匹配 1:匹配
    TEAM_MEMBER_INFO_KEY_GODDESS_STATE  = 24, --护送状态 0:未护送 

    TEAM_ONLINE_KEY_DBID              = 1,
    TEAM_ONLINE_KEY_ONLINE            = 2,

    TEAM_POS_KEY_DBID              = 1,
    TEAM_POS_KEY_POS_X             = 2,
    TEAM_POS_KEY_POS_Y             = 3,
    TEAM_POS_KEY_POS_Z             = 4,

    TEAM_RECEIVE_INVITE_KEY_TEAM_ID         = 1,
    TEAM_RECEIVE_INVITE_KEY_DBID            = 2,
    TEAM_RECEIVE_INVITE_KEY_NAME            = 3,
    TEAM_RECEIVE_INVITE_KEY_GENDER          = 4,
    TEAM_RECEIVE_INVITE_KEY_VOCATION        = 5,
    TEAM_RECEIVE_INVITE_KEY_RACE            = 6,
    TEAM_RECEIVE_INVITE_KEY_LEVEL           = 7,

    TEAM_RECEIVE_APPLY_KEY_TEAM_ID          = 1,
    TEAM_RECEIVE_APPLY_KEY_APPLYER_DBID     = 2,
    TEAM_RECEIVE_APPLY_KEY_APPLYER_NAME     = 3,
    TEAM_RECEIVE_APPLY_KEY_APPLYER_GENDER   = 4,
    TEAM_RECEIVE_APPLY_KEY_APPLYER_VOCATION = 5,
    TEAM_RECEIVE_APPLY_KEY_APPLYER_RACE     = 6,
    TEAM_RECEIVE_APPLY_KEY_APPLYER_LEVEL    = 7,

    TEAM_TEAMS_KEY_NEAR_TEAMS         = 1,
    TEAM_TEAMS_KEY_PAGE               = 2,
    TEAM_TEAMS_KEY_TOTAL_PAGE         = 3,

    TEAM_TEAM_KEY_TEAM_ID            = 1,
    TEAM_TEAM_KEY_CAPTAIN_DBID       = 2,
    TEAM_TEAM_KEY_TEAM_MEMBER_INFOS  = 3,
    TEAM_TEAM_KEY_TARGET_ID          = 4,

    TEAM_NEAR_PLAYERS_KEY_NEAR_PLAYERS     = 1,
    TEAM_NEAR_PLAYERS_KEY_PAGE             = 2,
    TEAM_NEAR_PLAYERS_KEY_TOTAL_PAGE       = 3,

    TEAM_NEAR_PLAYER_KEY_DBID        = 1,
    TEAM_NEAR_PLAYER_KEY_NAME        = 2,
    TEAM_NEAR_PLAYER_KEY_GENDER      = 3,
    TEAM_NEAR_PLAYER_KEY_VOCATION    = 4,
    TEAM_NEAR_PLAYER_KEY_RACE        = 5,
    TEAM_NEAR_PLAYER_KEY_LEVEL       = 6,
    TEAM_NEAR_PLAYER_KEY_FIGHT_FORCE = 7,
    TEAM_NEAR_PLAYER_KEY_MAP_ID      = 8,


    --------Avatar上的状态定义-------------------------------------
    STATE_CROSSING      = 2,          --处于跨服状态


    --申请场景时，扩展参数所带的key值定义 --on_select_map_req中的参数params的key
    SPACE_APPLY_KEY_DBID                        = 1,  --玩家的dbid
    SPACE_APPLY_KEY_MAPX                        = 2,  --进入位置的x坐标
    SPACE_APPLY_KEY_MAPY                        = 3,  --进入位置的y坐标
    SPACE_APPLY_KEY_MAPZ                        = 4,  --进入位置的z坐标
    SPACE_APPLY_KEY_CROSS_UUID                  = 5,  --玩家在跨服服务器上的唯一编号
    SPACE_APPLY_KEY_LEVEL                       = 6,  --玩家的等级
    SPACE_APPLY_KEY_FIGHT_FORCE                 = 7,  --玩家的战斗力
    SPACE_APPLY_KEY_TEAM_ID                     = 8,  --队伍编号
    SPACE_APPLY_KEY_ENT_TYPE                    = 9,  --1：表示个人；2：表示队伍
    SPACE_APPLY_KEY_FACTION_ID                  = 10, --玩家在该副本中的阵营编号
    SPACE_APPLY_KEY_MEMBERS                     = 11, --除自己外，把这里面都拉到同一个场景
    SPACE_APPLY_OTHER_INFO                      = 12, --进入情景的其他信息
    --SPACE_APPLY_EVENT_ACTIVITY_ID             = 13, --野外事件id
    --SPACE_APPLY_KEY_WATCH_WAR                 = 14, --是否观战
    --SPACE_APPLY_ABYSS_LAYER                   = 15, --深渊层数
    --SPACE_APPLY_ABYSS_UNLOCK_LAYER            = 16, --深渊当前解锁层数
    SPACE_APPLY_MGR_MB_STR                      = 17, --相应的管理器的mb_str
    SPACE_APPLY_HOLY_TOWER_ID                   = 18, --守护结界塔难度id
    SPACE_APPLY_INSTANCE_GOLD_ID                = 19, --金币副本难度id
    SPACE_APPLY_KEY_AVERAGE_LEVEL               = 20, --平均等级
    SPACE_APPLY_KEY_ROOM_ID                     = 21, --room_id
    SPACE_APPLY_KEY_AION_LAYER                  = 22, --永恒之塔层数
    SPACE_APPLY_KEY_PERSONAL_BOSS_ID            = 23, --个人boss id
    SPACE_APPLY_KEY_GUILD_DBID                  = 24, --公会id
    SPACE_APPLY_KEY_GUILD_OVERLORD_INFO         = 25, --主宰公会信息(faction_id,buff_id)

    --SPACE_APPLY_KEY_MEMBERS中的的数据结构
    SPACE_APPLY_MEMBERS_MB_STR                  = 1, --mbstr
    SPACE_APPLY_MEMBERS_FACTION_ID              = 2, --阵营

    --观战记录的副本列表信息
    --SPACE_INFO_KEY_MAP_ID        = 1,
    --SPACE_INFO_KEY_MAP_LINE      = 2,
    --SPACE_INFO_KEY_SPACE_ID      = 3,
    --SPACE_INFO_KEY_BASE_MB_STR   = 4,
    --SPACE_INFO_KEY_CELL_MB_STR   = 5,
    --SPACE_INFO_KEY_PLAYERS       = 6,
    --SPACE_INFO_KEY_WATCH_COUNT   = 7,
    --SPACE_INFO_KEY_BEGIN_TIME    = 8,

    --观战类型
    --WAR_TYPE_LADDER              = 1, --天梯赛，单人pvp
    --WAR_TYPE_PVE                 = 2, --挑战副本，单人pve，组队多人pve
    --WAR_TYPE_MULTI_PVP           = 3, --多人pvp

    --?
    --SPACE_INFO_PLAYER_KEY_DBID        = 1,
    --SPACE_INFO_PLAYER_KEY_NAME        = 2,
    --SPACE_INFO_PLAYER_KEY_GENDER      = 3,
    --SPACE_INFO_PLAYER_KEY_LEVEL       = 4,
    --SPACE_INFO_PLAYER_KEY_RACE        = 5,
    --SPACE_INFO_PLAYER_KEY_VOCATION    = 6,
    --SPACE_INFO_PLAYER_KEY_FACTION_ID  = 7,
    --SPACE_INFO_PLAYER_KEY_LEADER      = 8,

    --SPACE_INFO_PAGE_CNT               = 5,


    --------------------怪物生成类型--------------------
    MONSTER_BUILD_TYPE_0    = 0,   --纯后端
    MONSTER_BUILD_TYPE_1    = 1,   --纯前端

    --任务状态
    TASK_STATE_ACCEPTABLE       = 1,    --任务可接
    TASK_STATE_ACCEPTED         = 2,    --任务已接受
    TASK_STATE_COMPLETED        = 3,    --任务已完成
    TASK_STATE_COMMITED         = 4,    --任务已提交

    TASK_TYPE_MAIN              = 1,    --主线任务
    TASK_TYPE_BRANCH            = 2,    --支线任务(支线)
    TASK_TYPE_VOCATION          = 3,    --支线任务(转职)
    TASK_TYPE_GUIDE             = 4,    --支线任务(指引)

    --任务luatable相关key
    TASK_INFO_KEY_TARGETS       = 1,
    TASK_INFO_KEY_STATE         = 2,
    TASK_INFO_KEY_TRACK         = 3,

    --事件活动状态
    --EVENT_ACTIVITY_STATE_ACCEPTED          = 1,    --进行中，有进度的
    --EVENT_ACTIVITY_STATE_COMPLETED         = 2,    --已经完成，没了进度信息，要读表

    --日常任务状态
    --DAILY_TASK_STATE_ACCEPTED          = 1,    --进行中，有进度的
    --DAILY_TASK_STATE_COMPLETED         = 2,    --已经完成，没了进度信息，要读表
    --DAILY_TASK_STATE_COMMITED          = 3,    --已经领取奖励，没了进度信息，要读表

    --成就状态
    ACHIEVEMENT_STATE_ACCEPTED          = 1,    --进行中，有进度的
    ACHIEVEMENT_STATE_COMPLETED         = 2,    --已经完成，没了进度信息，要读表
    ACHIEVEMENT_STATE_COMMITED          = 3,    --已经领取奖励，没了进度信息，要读表

    --称号任务状态
    TITLE_TASK_STATE_ACCEPTED          = 1,    --进行中，有进度的
    TITLE_TASK_STATE_COMPLETED         = 2,    --已经完成，没了进度信息，要读表
    TITLE_TASK_STATE_COMMITED          = 3,    --已经领取奖励，没了进度信息，要读表

    --循环任务状态 --和任务共用
    --CIRCLE_TASK_STATE_ACCEPTED          = 1,    --进行中，有进度的
    --CIRCLE_TASK_STATE_COMPLETED         = 2,    --已经完成，没了进度信息，要读表
    --CIRCLE_TASK_STATE_COMMITED          = 3,    --已经领取奖励，没了进度信息，要读表

    --魔戒寻主状态
    DEMON_RING_STATE_ACCEPTED  = 1, --进行中
    DEMON_RING_STATE_COMPLETED = 2, --已完成
    DEMON_RING_STATE_RECEIVED  = 3, --已领取

    --boss悬赏状态
    BOSS_REWARD_STATE_ACCEPTED  = 1, --进行中
    BOSS_REWARD_STATE_COMPLETED = 2, --已完成
    BOSS_REWARD_STATE_RECEIVED  = 3, --已领取

    --开宗立派状态
    FRAUD_GUILD_STATE_ACCEPTED  = 1, --进行中
    FRAUD_GUILD_STATE_COMPLETED = 2, --已完成
    FRAUD_GUILD_STATE_RECEIVED  = 3, --已领取

    --开服乱嗨
    FREAK_POINT           = 0, --拥有的嗨点
    FREAK_STATE_ACCEPTED  = 1, --进行中
    FREAK_STATE_COMPLETED = 2, --已完成

    --禁言类型
    NO_SPEAK_FOREVER           = -1,
    SPEAK_FREE                 = 0,  --能够自由发言

    --功能开启id，功能表格定义
    FUNCTION_ID_ATTRIBUTES          = 1,   --属性
    FUNCTION_ID_BAG                 = 2,   --背包
    FUNCTION_ID_NOBILITY            = 3,   --爵位
    FUNCTION_ID_FASHION             = 4,   --时装                                
    FUNCTION_ID_NAME                = 5,   --称号 
    FUNCTION_ID_BLOOD               = 6,   --血脉 
    FUNCTION_ID_INITIATIVE          = 7,   --主动 
    FUNCTION_ID_PASSIVE             = 8,   --被动
    FUNCTION_ID_TALENT              = 9,   --天赋 
    FUNCTION_ID_EQUIP_STREN         = 10,  --强化
    FUNCTION_ID_EQUIP_QUALITY       = 11,  --升品
    FUNCTION_ID_EQUIP_WASH          = 12,  --洗练
    FUNCTION_ID_ADVANCED            = 61,  --进阶 
    FUNCTION_ID_MOSAIC              = 13,  --镶嵌
    FUNCTION_ID_REFINING            = 14,  --精炼
    FUNCTION_ID_Kill_XIAN           = 15,  --诛仙 
    FUNCTION_ID_Kill_SHEN           = 16,  --诛神
    FUNCTION_ID_AWAKE               = 17,  --觉醒 
    FUNCTION_ID_ADD_SPIRIT          = 18,  --附灵
    FUNCTION_ID_WING                = 19,  --翅膀
    FUNCTION_ID_TALISMAN            = 20,  --法宝
    FUNCTION_ID_WEAPON              = 21,  --神兵
    FUNCTION_ID_CLOAK               = 22,  --披风
    FUNCTION_ID_PET                 = 23,  --宠物
    FUNCTION_ID_HORSE               = 24,  --坐骑
    FUNCTION_ID_GUARDIAN            = 25,  --守护系统（灵宠）
    FUNCTION_ID_DAN_SOUL            = 26,  --丹魂
    FUNCTION_ID_FRIEND              = 27,  --好友
    FUNCTION_ID_PLOT                = 30,  --任务-剧情
    FUNCTION_ID_CIRCLE_TASK         = 31,  --赏金任务
    FUNCTION_ID_UNION_TASK          = 32,  --任务-公会任务
    FUNCTION_ID_ACHIEVEMENT         = 33,  --成就系统
    FUNCTION_ID_WAREHOUSE           = 34,  --仓库
    FUNCTION_ID_COMPOSITE_PROP      = 35,  --合成-道具
    FUNCTION_ID_COMPOSITE_GEM       = 36,  --合成-宝石
    FUNCTION_ID_COMPOSITE_SOUL      = 37,  --合成-灵魂凝聚
    FUNCTION_ID_COMPOSITE_EQUIP     = 38,  --合成-角色装备
    FUNCTION_ID_SYMBOL_MOSAIC       = 39,  --符文-镶嵌
    FUNCTION_ID_SYMBOL_BREAK        = 40,  --符文-分解
    FUNCTION_ID_SYMBOL_EXCHANGE     = 41,  --符文-兑换
    FUNCTION_ID_VIP                 = 42,  --vip
    FUNCTION_ID_CHARGE              = 43,  --充值
    FUNCTION_ID_MARRY_INFO          = 50,  --结婚-信息
    FUNCTION_ID_MARRY_RING          = 51,  --结婚-钻戒
    FUNCTION_ID_MARRY_PET           = 52,  --仙娃
    FUNCTION_ID_MARRY_INSTANCE      = 53,  --结婚副本
    FUNCTION_ID_MARRY_BREGUET       = 54,  --结婚-宝匣
    FUNCTION_ID_FASHION_ARMS        = 55,  --角色-时装武器
    FUNCTION_ID_FASHION_EFFECT      = 56,  --角色-时装特效
    FUNCTION_ID_FASHION_PHOTO       = 57,  --角色-时装头像框
    FUNCTION_ID_FASHION_CHAT        = 58,  --角色-时装聊天框
    FUNCTION_ID_DRAGON_SPIRIT       = 60,  --龙魂
    --                                61,  --进阶
    FUNCTION_ID_PICTURE_VIEW        = 70,  --图鉴-图鉴总览
    FUNCTION_ID_PICTURE_COMBINATION = 71,  --图鉴-图鉴组合
    FUNCTION_ID_MYTHICIAL_ANIMAL    = 80,  --二级主界面-神兽
    FUNCTION_ID_VIEW_PLAYER         = 90,  --查看其它玩家
    FUNCTION_ID_TEAM_HALL           = 91,  --队伍-组队大厅
    FUNCTION_ID_TEAM_MINE           = 92,  --队伍-我的队伍
    FUNCTION_ID_SETTING_BASE        = 93,  --设置-基本设置
    FUNCTION_ID_SETTING_HANG        = 94,  --设置-挂机设置
    FUNCTION_ID_SETTING_SAFE        = 95,  --设置-安全锁
    FUNCTION_ID_AION                = 201, --永恒之塔副本
    FUNCTION_ID_EXP_INSTANCE        = 202, --经验副本
    FUNCTION_ID_HOLY_TOWER          = 203, --守护结界塔
    FUNCTION_ID_GOLD_INSTANCE       = 204, --金币副本
    FUNCTION_ID_EQUIPMENT_INSTANCE  = 205, --圣灵血阵
    FUNCTION_ID_TOWER_DEFENSE       = 206, --塔防副本
    FUNCTION_ID_GHOST               = 207, --幽魂秘境
    FUNCTION_ID_HONG_MENG           = 208, --鸿蒙古殿
    FUNCTION_ID_TOWER_KIll_SHEN     = 209, --诛神塔
    FUNCTION_ID_ONE_TURN            = 301, --一转界面
    FUNCTION_ID_TWO_TURN            = 302, --二转界面
    FUNCTION_ID_THREE_TURN          = 303, --三转界面
    FUNCTION_ID_FOUR_TURN           = 304, --四转界面
    FUNCTION_ID_FIRST_CHARGE        = 310, --一级主界面-首充   
    FUNCTION_ID_OPEN_SERVICE        = 320, --一级主界面-开服活动
    FUNCTION_ID_WORLD_SERVER        = 330, --一级主界面-世界服
    FUNCTION_ID_SYMBOL_TREASURE     = 350, --符文寻宝
    FUNCTION_ID_EQUIP_TREASURE      = 351, --装备寻宝   
    FUNCTION_ID_PEAK_TREASURE       = 355, --巅峰寻宝   
    FUNCTION_ID_PRAY                = 370, --一级主界面-祈福
    FUNCTION_ID_RANK                = 390, --一级主界面-排行
    FUNCTION_ID_MASTER_TEMPLE       = 400, --一级主界面-主宰神殿   
    FUNCTION_ID_WORLD_BOSS          = 401, --世界boss
    FUNCTION_ID_PERSON_BOSS         = 402, --个人BOSS
    FUNCTION_ID_HOME_BOSS           = 403, --BOSS之家
    FUNCTION_ID_BARBAROUS           = 404, --蛮荒禁地
    FUNCTION_ID_GUARD_GODDESS       = 405, --护送女神
    FUNCTION_ID_DAILY_ACTIVITY      = 406, --每日必做
    FUNCTION_ID_DAILY_TIME_LIMIT    = 407, --日常-限时活动
    FUNCTION_ID_DAILY_RESOURCE_FIND = 408, --日常-资源找回
    FUNCTION_ID_GUILD               = 409,  --公会-信息-基础信息
    FUNCTION_ID_GUILD_MEMBER        = 410, --公会-信息-成员列表
    FUNCTION_ID_GUILD_LIST          = 411, --公会-信息-公会列表
    FUNCTION_ID_GUILD_RED_PACKET    = 412, --公会-信息-公会红包
    FUNCTION_ID_GUILD_WAREHOUSE     = 413, --公会-仓库
    FUNCTION_ID_GUILD_SKILL         = 414, --公会-技能
    FUNCTION_ID_GUILD_ANIMAL        = 415, --公会-活动-公会神兽
    FUNCTION_ID_GUILD_BONFIRE       = 416, --公会篝火
    FUNCTION_ID_GUILD_FIGHT         = 417, --公会-活动-公会争霸
    FUNCTION_ID_GUILD_GUARD         = 418, --守卫工会
    FUNCTION_ID_MARKET_BUY          = 419, --市场-购买
    FUNCTION_ID_MARKET_SELL         = 420, --市场-出售
    FUNCTION_ID_MARKET_WANT_LIST    = 421, --市场-求购列表
    FUNCTION_ID_MARKET_WANT_BUY     = 422, --市场-我要求购
    FUNCTION_ID_MARKET_TRADE_LIST   = 423, --市场-交易清单
    FUNCTION_ID_CLOUD_PEAK          = 502, --青云之巅
    FUNCTION_ID_OFFLINE_PVP         = 500, --离线1v1
    FUNCTION_ID_CROSS_VS11          = 501, --跨服实时1v1
    FUNCTION_ID_BATTLE_HELL         = 503, --地狱战场
    FUNCTION_ID_BATTLE_GHOST        = 504, --神魔战场
    FUNCTION_ID_PEAK_FIGHT          = 505, --巅峰对决
    FUNCTION_ID_ISLAND_ANIMAL       = 601, --神兽岛
    FUNCTION_ID_CROSS_FALL_RECORD   = 602, --跨服-掉落记录
    FUNCTION_ID_MAIL                = 603, --邮件
    FUNCTION_ID_BOSS_FALL_RECORD    = 604, --BOSS玩法-掉落记录
    FUNCTION_ID_ILLUSION_WING       = 605, --幻化-翅膀
    FUNCTION_ID_ILLUSION_WEAPON_MAGIC = 606, --幻化-法宝
    FUNCTION_ID_ILLUSION_WEAPON_GOD = 607, --幻化-神兵
    FUNCTION_ID_ILLUSION_CLOAK      = 608, --幻化-披风
    FUNCTION_ID_ILLUSION_PET        = 609, --幻化-宠物
    FUNCTION_ID_ILLUSION_VS11       = 610, --幻化-坐骑
    FUNCTION_ID_QUESTION            = 611, --开心答题
    FUNCTION_ID_DAY_FIRST_CHARGE    = 612, --每日首充
    FUNCTION_ID_ROULETTECT          = 613, --欢乐云盘
    FUNCTION_ID_MAIN_CHARGE         = 614, --一级主界面-充值
    FUNCTION_ID_SEVEN_LOGIN         = 615, --七天登录
    FUNCTION_ID_DEMON_RING          = 616, --魔戒寻主 初始之戒
    FUNCTION_ID_DEMON_RING_CHAOS    = 617, --魔戒寻主 混沌之戒
    FUNCTION_ID_DEMON_RING_DESTORY  = 618, --魔戒寻主 毁灭之戒
    FUNCTION_ID_DEMON_RING_GUARD    = 619, --魔戒寻主 守护之戒
    FUNCTION_ID_DEMON_RING_POWER    = 620, --魔戒寻主 神力之戒
    FUNCTION_ID_OPEN_RANK_LEVEL     = 621, --开服冲榜-冲级达人
    FUNCTION_ID_OPEN_RANK_PET       = 622, --开服冲榜-坐骑进阶
    FUNCTION_ID_OPEN_RANK_HORSE     = 623, --开服冲榜-驯宠达人
    FUNCTION_ID_OPEN_RANK_CHARGE    = 624, --开服冲榜-今日充值
    FUNCTION_ID_OPEN_RANK_GEM       = 625, --开服冲榜-宝石镶嵌
    FUNCTION_ID_OPEN_RANK_COMBAT    = 626, --开服冲榜-战力排行
    FUNCTION_ID_COLLECT_WORD        = 627, --开服活动-集字有礼
    FUNCTION_ID_PERFER_LOVER        = 628, --开服活动-完美情人
    FUNCTION_ID_GUILD_FIGHT_OPENSER = 629, --开服活动-工会争霸
    FUNCTION_ID_WELFARE_SIGN        = 630, --福利-每日签到
    FUNCTION_ID_WELFARE_GIFT        = 631, --福利-冲级豪礼
    FUNCTION_ID_MOUTH_INVEST        = 632,  --月卡投资
    FUNCTION_ID_VIP_INVEST          = 633,  --VIP投资
    FUNCTION_ID_LEVEL_INVEST        = 634,  --等级投资
    FUNCTION_ID_OPEN_CUMULATE       = 635,  --开服累充
    FUNCTION_ID_WELFARE_EXCHANGE    = 636, --福利-礼包兑换
    FUNCTION_ID_WELFARE_ANNOUNCE    = 637, --福利-更新公告
    FUNCTION_ID_OPEN_CARD           = 638, --翻牌集福
    FUNCTION_ID_CHANNEL_ACTIVITY_WEEK_LOGIN      = 639, --周活动登录
    FUNCTION_ID_WEEKLY_CUMULATIVE_C              = 640, --周累充   
    FUNCTION_ID_CHANNEL_ACTIVITY_HOLY_WEAPON     = 641, --神兵现世
    FUNCTION_ID_CHANNEL_ACTIVITY_FIREWORK        = 642, --庆典烟花    
    FUNCTION_ID_CHANNEL_ACTIVITY_DOUBLE          = 643, --周双倍
    FUNCTION_ID_CHANNEL_ACTIVITY_HI              = 644, --hi点
    FUNCTION_ID_CHANNEL_ACTIVITY_EXCHANGE        = 645, --庆典兑换
    FUNCTION_ID_CHANNEL_ACTIVITY_BATTLE_SOUL     = 646, --战场之魂
    FUNCTION_ID_DAILYCONSUME        = 647, --连续消费
    FUNCTION_ID_HAPPYWISH           = 648, --
    FUNCTION_ID_CARD_LUCK           = 649, --
    FUNCTION_ID_IDENTIFY            = 650, --
    FUNCTION_ID_ZERO_DOLLAR_BUY     = 651, --
    FUNCTION_ID_WELFARE_ONLINE    = 652, --福利-在线奖励
    FUNCTION_ID_APPLY_FOR_GUILD   = 654,  --公会-申请公会
    FUNCTION_ID_BOSS_REWARD         = 655, --BOSS悬赏
    FUNCTION_ID_FREAK               = 656, --开服乱嗨
    FUNCTION_ID_SECT               = 657, --开宗立派
    FUNCTION_ID_ALTAR               = 658, --深渊祭坛
    FUNCTION_ID_VIPGIFT             = 659,--vip等级礼包
    FUNCTION_STAR                   = 59, --星脉


    --活动id,活动表格
    ACTIVITY_ID_CIRCLE_TASK         = 1,   --赏金任务
    ACTIVITY_ID_GOLD_INSTANCE       = 2,   --众神宝库
    ACTIVITY_ID_WORLD_BOSS          = 3,   --世界BOSS
    ACTIVITY_ID_GUARD_GODDENSS      = 4,   --护送女神
    ACTIVITY_ID_EQUIP_STREN         = 5,   --装备强化
    ACTIVITY_ID_GUAJI               = 6,   --推荐挂机点，客户端
    ACTIVITY_ID_HOLY_TOWER          = 7,   --守护结界塔
    ACTIVITY_ID_PERSONAL_BOSS       = 8,   --个人BOSS
    ACTIVITY_ID_GUARD_GODDENSS2     = 9,   --双倍护送女神
    ACTIVITY_ID_OFFLINE_PVP         = 10,  --离线1v1
    ACTIVITY_ID_EXP_INSTANCE        = 11,  --经验副本
    ACTIVITY_ID_EQUIPMENT_INSTANCE  = 12,  --圣灵血阵(装备副本)
    ACTIVITY_ID_WEEK_TASK           = 13,  --周任务
    ACTIVITY_ID_QUESTION            = 14,  --答题
    ACTIVITY_ID_SKY_DROP            = 15,  --天降密宝
    ACTIVITY_ID_CROSS_VS11          = 16,  --跨服实时1v1
    ACTIVITY_ID_CLOUD_PEAK          = 17,  --青云之巅
    ACTIVITY_ID_ANCIENT_BATTLE      = 18,  --上古战场
    ACTIVITY_ID_GUILD_BONFIRE       = 20,  --公会篝火
    ACTIVITY_ID_GUILD_GUARD         = 21,  --守卫公会
    ACTIVITY_ID_GUILD_CONQUEST      = 22,  --公会争霸
    ACTIVITY_ID_TOWER_DEFENSE       = 24,  --塔防副本
    ACTIVITY_ID_GUILD_BEAST         = 25,  --公会神兽

    --渠道活动id
    CHANNEL_ACTIVITY_ID_OPEN_CARD           = 1, -- 翻牌集福
    CHANNEL_ACTIVITY_ID_DAILY_CUNSUME       = 2, --连续消费
    CHANNEL_ACTIVITY_ID_JIANBAO             = 3, --全民鉴宝
    CHANNEL_ACTIVITY_ID_WEEKLY_CUMULATIVE_C = 4, --周累充
    CHANNEL_ACTIVITY_ID_WEEK_LOGIN          = 5, --周活动登录
    CHANNEL_ACTIVITY_ID_FIREWORK            = 6, --庆典烟花
    CHANNEL_ACTIVITY_ID_HOLY_WEAPON         = 7, --神兵现世
    CHANNEL_ACTIVITY_ID_EXCHANGE            = 8, --庆典兑换
    CHANNEL_ACTIVITY_ID_DOUBLE              = 9, --周双倍
    CHANNEL_ACTIVITY_ID_BATTLE_SOUL         = 10, --战场之魂
    CHANNEL_ACTIVITY_ID_HI                  = 11, --hi点



    -----------------------判断玩家的限制限制条件-----------------------
    LIMIT_CONDITION_MIN            = 1,    --限制条件下限（包含）
    LIMIT_WORLD_LEVEL              = 1,    --世界等级（大于等于）
    LIMIT_LEVEL                    = 2,    --个人等级限制（大于等于）
    LIMIT_VOCATION                 = 3,    --职业限制(多个参数,in关系，且不填或者填0为全部)
    LIMIT_VIP_LEVEL                = 4,    --VIP等级限制（大于等于）
    LIMIT_LEVEL_LT                 = 5,    --个人等级小于满足条件(小于)
    --LIMIT_RACE                   = 6,    --种族，和职业一致
    LIMIT_FIGHT                    = 7,    --战斗力（大于等于）
    LIMIT_COMMITED_TASK            = 8,    --已完成，主线，支线
    LIMIT_HAS_VOCATION_TASK        = 9,    --是否在转职阶段内（前端用）
    LIMIT_FIRST_CHARGE             = 10,   --是否开服首充判定（前端用）
    LIMIT_CONDITION_MAX            = 20,   --限制条件上限（不包含）

    -----------------------事件id定义-----------------------
    EVENT_ID_KILL_MONSTER               = 1,  --杀怪事件
    --EVENT_ID_KILL_MONSTER_TEAM        = 2,  --不用    -杀怪事件-组队共享
    EVENT_ID_PASS_INST                  = 3,  --通关副本-胜利
    EVENT_ID_CLIENT_PICK_FINISH         = 4,  --采集-客户端-假
    --EVENT_ID_CLIENT_PICK_FINISH_TEAM  = 5,  --不用    -采集-客户端-假-组队共享
    EVENT_ID_CLIENT_NPC_DIALOG_FINISH   = 6,  --NPC对话完成-客户端
    --EVENT_ID_CLIENT_XXX_FINISH        = 7,  --不用    -完成xxx-客户端
    --EVENT_ID_CLIENT_XXX_FINISH_TEAM   = 8,  --不用   -完成xxx-客户端-组队共享
    EVENT_ID_GIVE_ITEM_TO_NPC           = 9,  --上交道具给NPC
    EVENT_ID_GET_ITEM                   = 10, --获得道具
    --EVENT_ID_HAS_ITEM                 = 11, --不用     -拥有道具
    EVENT_ID_LEVEL_UP                   = 12, --玩家升级
    EVENT_ID_HAS_LEVEL                  = 13, --拥有等级
    EVENT_ID_WORLD_LEVEL_UP             = 14, --世界等级升级
    --EVENT_ID_HAS_WORLD_LEVEL          = 15, --不用     -拥有世界等级
    EVENT_ID_ENTER_MAP                  = 16, --进入地图
    --EVENT_ID_TASK_COMMITED            = 17, --不用     -任务提交
    --EVENT_ID_ACHIEVEMENT_COMMITED     = 18, --不用     -成就提交
    EVENT_ID_DAY_LOGIN                  = 19, --每天登陆
    --EVENT_ID_CHARGE                   = 20, --不用       -充值
    EVENT_ID_COLLECT                    = 21, --采集-服务端-真
    --EVENT_ID_HAS_MONSTER              = 22, --不用-拥有怪物
    --EVENT_ID_PRIVATE_ESCORT           = 23, --不用-个人护送
    EVENT_ID_CLIENT_GOTO_POS            = 24, --前往目的地
    EVENT_ID_HAS_WING_LEVEL             = 25, --拥有翅膀等级
    EVENT_ID_HAS_WEAPON_LEVEL           = 26, --拥有神兵等级
    EVENT_ID_HAS_TALISMAN_LEVEL         = 27, --拥有法宝等级
    EVENT_ID_HAS_CLOAK_LEVEL            = 28, --拥有披风等级
    EVENT_ID_HAS_HORSE_GRADE            = 29, --拥有坐骑阶级
    EVENT_ID_HAS_PET_GRADE              = 30, --拥有宠物阶级
    EVENT_ID_HAS_PET_LEVEL              = 31, --拥有宠物等级
    EVENT_ID_HAS_AION_LEVEL             = 32, --已有永恒之塔层数
    EVENT_ID_SELF_DEATH                 = 33, --自身死亡,被杀
    EVENT_ID_KILL_OTHER                 = 34, --杀死其他玩家
    EVENT_ID_KILL_RED                   = 35, --杀死红名玩家
    EVENT_ID_HAS_ACHIEVEMENT_POINT      = 36, --拥有成就点
    EVENT_ID_HAS_VIP                    = 37, --拥有VIP等级
    EVENT_ID_LOAD_EQUIP                 = 38, --穿戴装备
    EVENT_ID_EQUIP_WASH                 = 39, --洗练装备
    EVENT_ID_BUY_PKG_ITEM_COUNT         = 40, --解锁背包格子
    EVENT_ID_BUY_PKG_WAREHOUSE_COUNT    = 41, --解锁仓库格子
    EVENT_ID_HAS_ALL_EQUIP_STAR         = 42, --身上装备的总星数
    EVENT_ID_HAS_ALL_EQUIP_COUNT        = 43, --身上装备的数量 --等于配置的品质
    EVENT_ID_HAS_EQUIP_STREN_COUNT      = 44, --身上强化的数量 --大于配置等级
    EVENT_ID_PASS_INST_EXP              = 45, --通关单次获得经验
    EVENT_ID_ACCEPT_CIRCLE_TASK         = 46, --接到赏金任务
    EVENT_ID_GUARD_GODDESS              = 47, --完成护送
    EVENT_ID_HAS_DAILY_ACTIVITY         = 48, --身上有的日常活跃度
    EVENT_ID_HAS_AION_LAYER             = 49, --爬塔通关
    EVENT_ID_COMPLETE_ACTION            = 50, --完成一个actionid(4~6转职完成)
    EVENT_ID_HAS_ITEM_COUNT             = 51, --身上拥有物品
    EVENT_ID_HAS_DEF                    = 52, --身上拥有防御值
    EVENT_ID_PURCHASE_MONTH_CARD        = 53, --参与月卡投资
    EVENT_ID_INVEST_IN_LV               = 54, --参与等级投资
    EVENT_ID_CHARGE                     = 55, --完成首充
    EVENT_ID_FORGE                      = 56, --锻造装备
    EVENT_ID_MARRY_RING_LEVEL           = 57, --同心锁等级
    EVENT_ID_MARRY_PET                  = 58, --拥有仙娃
    EVENT_ID_MARRY_FRIENDLY             = 59, --仙侣亲密度
    EVENT_ID_DAILY_BONUS                = 60, --每日签到
    EVENT_ID_VOC_CHANGE                 = 61, --转职
    EVENT_ID_GUILD_RED_ENVELOPE_MONEY   = 62, --发公会钱红包
    EVENT_ID_DONATE_EQUIPMENT           = 63, --捐献装备
    EVENT_ID_GUILD_BONFIRE              = 64, --公会篝火采集
    EVENT_ID_GUILD_BONFIRE_ANSWER       = 65, --公会篝火答对题
    EVENT_ID_GUILD_RED_ENVELOPE_SYSTEM  = 66, --发公会系统红包
    EVENT_ID_GUILD_CONQUEST_WON         = 67, --争霸获胜
    EVENT_ID_GUILD_CONQUEST_CHAMP       = 68, --争霸第一名
    EVENT_ID_QUESTION                   = 69, --开心答对题
    EVENT_ID_WEEK_TASK                  = 70, --提交周任务
    EVENT_ID_CIRCLE_TASK                = 71, --提交赏金任务
    EVENT_ID_COMPOSE_EQUIPMENT          = 72, --合成装备
    EVENT_ID_LADDER_MATCH_WON           = 73, --天梯赛胜利
    EVENT_ID_LADDER_MATCH_SERIAL_WON    = 74, --天梯赛连胜
    EVENT_ID_ACTIVATE_ILLUSION          = 75, --激活化形
    EVENT_ID_EXTEND_ASSIT_GOD_MONSTER   = 76, --扩展助战神兽
    EVENT_ID_ACTIVATE_GOD_MONSTER       = 77, --激活神兽
    EVENT_ID_AION_KILL_PLAYER           = 78, --永恒之巅击杀玩家
    EVENT_ID_EXP_INSTANCE_INSPIRE       = 79, --经验副本鼓舞
    EVENT_ID_COLLECT_CRYSTAL            = 80, --采集水晶
    EVENT_ID_SHUT_DOWN_OVERLORD_GUILD   = 81, --终结主宰公会
    EVENT_ID_ACTIVATE_CARD              = 82, --激活图鉴
    EVENT_ID_EVOLUTION_LEGENDARY        = 83, --套装升级为传奇套装（按部位只算首次）
    EVENT_ID_EVOLUTION_INVINCIBLE       = 84, --套装升级为战神套装（按部位只算首次）
    EVENT_ID_IN_GUILD                   = 85, --加入一个公会
    EVENT_ID_KILLED_WORLD_BOSS          = 86, --击杀世界boss
    EVENT_ID_GOSSIP                     = 87, --世界发言
    EVENT_ID_PLAYBOY                    = 88, --拥有好友
    EVENT_ID_MOB_WITH                   = 89, --添加好友
    EVENT_ID_ACCEPT_GUILD_TASK          = 90, --公会任务
    EVENT_ID_FIGHT_FORCE                = 91, --战斗力
    EVENT_ID_SLAY_BOT                   = 92, --玩竞技场
    EVENT_ID_GUILD_PROMOTED             = 93, --公会任命他人（暂时只写了个任命副会长）
    EVENT_ID_GUILD_HOMIES               = 94, --公会人数
    EVENT_ID_GUILD_LEVELS               = 95, --公会等级
    EVENT_ID_GUILD_BUILT                = 96, --创建公会
    EVENT_ID_LOTTERY                    = 97, --参与云购
    EVENT_ID_OKURI_MONEY                = 98, --进行充值
    EVENT_ID_GET_MARRIED                = 99, --结婚
    EVENT_ID_EQUIPMENT_PRAY             = 100, --装备寻宝
    EVENT_ID_RUNE_PRAY                  = 101, --符文寻宝
    EVENT_ID_EXP_PRAY                   = 102, --经验祈祷
    EVENT_ID_DOUBLE_GUARD               = 103, --双倍护送
    EVENT_ID_FASHION_WEAPON             = 104, --拥有武器时装
    EVENT_ID_ANCIENT_KILL_PLAYER        = 105, --上古战场击杀玩家


    -----------------------事件参数定义-----------------------
    EVENT_PARAM_MONSTER_ID                = 1,  --怪物id
    EVENT_PARAM_INST_ID                   = 2,  --普通副本id
    EVENT_PARAM_ITEM_ID                   = 3,  --道具id
    EVENT_PARAM_MAP_TYPE                  = 4,  --地图类型 --没有副本类型
    EVENT_PARAM_NUM                       = 5,  --数量
    EVENT_PARAM_NPC_ID                    = 6,  --npc_id
    EVENT_PARAM_MAP_ID                    = 7,  --地图id
    EVENT_PARAM_CLIENT_COLLECT_ID         = 8,  --前端采集点
    EVENT_PARAM_ITEM_TYPE                 = 9,  --道具类型 大类 普通道具，装备
    EVENT_PARAM_ITEM_QUALITY              = 10, --道具品质  装备品质
    --EVENT_PARAM_COLLECT_TYPE            = 11, --不用    --采集类型
    --EVENT_PARAM_COLLECT_LEVEL           = 12, --不用  --采集点等级
    --EVENT_PARAM_POINT_ID                = 13, --不用  --坐标点
    --EVENT_PARAM_CLIENT_COLLECT_ID       = 14, --不用   --前端采集点
    EVENT_PARAM_CLIENT_GOTO_POS_ID        = 15, --目标定位点
    EVENT_PARAM_AVATAR_MONSTER_LEVEL      = 16, --玩家怪物等级差 avatar.level-monster.level --可以为负数
    EVENT_PARAM_EQUIP_STAR                = 17, --装备星级
    EVENT_PARAM_EQUIP_WASH_P_COUNT        = 18, --紫色及以上洗练属性
    EVENT_PARAM_EQUIP_WASH_O_COUNT        = 19, --橙色及以上洗练属性
    EVENT_PARAM_EQUIP_WASH_R_COUNT        = 20, --红色及以上洗练属性
    EVENT_PARAM_EQUIP_TYPE                = 21, --装备部位 装备小类
    EVENT_PARAM_EQUIP_WASH_P_REAL_COUNT   = 22, --紫色洗练属性
    EVENT_PARAM_EQUIP_WASH_O_REAL_COUNT   = 23, --橙色洗练属性
    EVENT_PARAM_EQUIP_WASH_R_REAL_COUNT   = 24, --红色洗练属性
    EVENT_PARAM_MONSTER_TYPE              = 25, --怪物类型
    --EVENT_PARAM_AION_LAYER              = 26, --不用  --爬塔层数
    EVENT_PARAM_EQUIP_STREN_LEVEL         = 27, --强化等级
    EVENT_PARAM_PASS_INST_EXP             = 28, --通关单次累计经验
    EVENT_PARAM_ITEM_WAY                  = 29, --道具来源渠道
    EVENT_PARAM_ITEM_GRADE                = 30, --道具阶级  装备阶级
    EVENT_PARAM_ACTION_ID                 = 31, --action_id
    EVENT_PARAM_IS_LEGENDARY              = 32, --是否为传奇套装
    EVENT_PARAM_MARRY_PET_ID              = 33, --仙娃id
    EVENT_PARAM_CIRCLE_TASK_TYPE          = 34, --赏金任务类型
    EVENT_PARAM_INSTANCE_STAR             = 35, --副本星级
    EVENT_PARAM_ILLUSION_ID               = 36, --化形id
    EVENT_PARAM_GOD_MONSTER_ID            = 37, --神兽id
    EVENT_PARAM_CRYSTAL_TYPE              = 38, --水晶类型
    EVENT_PARAM_SERIAL_WON_TIME           = 39, --连胜次数
    EVENT_PARAM_MARRY_FRIENDLY            = 40, --仙侣亲密度
    EVENT_PARAM_HOMO_QUANTITY             = 41, --好友个数
    EVENT_PARAM_GUILD_HOMIES              = 42, --公会人数
    EVENT_PARAM_GUILD_LEVELS              = 43, --公会等级
    EVENT_PARAM_GUILD_POSITION            = 44, --公会职位
    --
    EVENT_PARAM_MONSTER_EID               = 99, --怪物eid --服务器内部用

    -----------------------陷阱相关参数定义-----------------------
    ENTER_TRAP_MARK     = -1,
    END_TRAP_MARK       = -2,
    TRAP_DEFAULT_VALUE  = -1,

    --陷阱判定阵营类型
    TRAP_ENEMY          = 1,
    TRAP_FRIEND         = 2,
    TRAP_CASTER         = 3,

    --陷阱失效类型
    TRAP_TIME_END       = 0,
    TRAP_ENTER_END      = 1,

    --吟唱id宏定义
    SING_ID_TELEPORT    =    1,    --跳转id

    --吟唱状态定义
    SING_STATE_NONE     =    0,    --没有吟唱
    SING_STATE_TELEPORT =    1,    --传送吟唱


    --好友系统宏
    FRIEND_INFO_KEY_UPDATE_TIME   = 1,    --最后一次互动时间
    FRIEND_INFO_KEY_NEW_CHAT_CNT  = 2,    --未读消息条数
    FRIEND_INFO_KEY_FRIENDLY_VAL  = 3,    --好友亲密度

    --以下字段是UserMgr补齐的,非存盘字段
    FRIEND_INFO_KEY_NAME          = 11,  --角色名
    FRIEND_INFO_KEY_DBID          = 12,  --dbid
    FRIEND_INFO_KEY_GENDER        = 13,  --性别
    FRIEND_INFO_KEY_LEVEL         = 14,  --角色等级
    FRIEND_INFO_KEY_VOCATION      = 15,  --职业
    FRIEND_INFO_KEY_RACE          = 16,	 --种族
    FRIEND_INFO_KEY_FIGHT_FORCE   = 17,	 --战斗力
    FRIEND_INFO_KEY_OFFLINE_TIME  = 18,  --下线时间
    FRIEND_INFO_KEY_ONLINE        = 19,  --玩家是否在线(1：在线，0：离线)
    FRIEND_INFO_KEY_ONLINE_TIME   = 20,  --上线时间
    FRIEND_INFO_KEY_VIP_LEVEL     = 21,  --VIP等级
    FRIEND_INFO_KEY_MATE_NAME     = 22,  --配偶名字

    FRIEND_LIST_TYPE_FRIEND       = 1,    --好友列表
    FRIEND_LIST_TYPE_RECENT       = 2,    --最近联系人
    FRIEND_LIST_TYPE_TEAM         = 3,    --最近组队
    FRIEND_LIST_TYPE_APPLY        = 4,    --好友申请列表
    FRIEND_LIST_TYPE_RECOMMEND    = 5,    --获取推荐好友列表
    FRIEND_LIST_TYPE_CHAT         = 6,    --获取和某人的未读私聊信息
    FRIEND_LIST_TYPE_FRIEND_ONLINE = 7,   --获取好友是否在线信息
    FRIEND_LIST_TYPE_SOMEONE_ONLINE = 8,  --获取某个列表的玩家是否在线信息

    CHAT_RECORD_KEY_TIMESTAMP     = 1,    --消息发送时间
    CHAT_RECORD_KEY_FROM_DBID     = 2,    --发送人dbid
    CHAT_RECORD_KEY_TEXT          = 3,    --消息内容
    CHAT_RECORD_KEY_ITEM_DATA     = 4,    --消息附带装备数据
    CHAT_RECORD_KEY_IS_SYS        = 5,    --系统
    CHAT_RECORD_KEY_PHOTO_FAS_ID  = 6,    --头像框ID
    CHAT_RECORD_KEY_CHAT_FAS_ID   = 7,    --聊天框ID

    --事件活动类型
    --EVENT_ACTIVITY_TYPE_PUBLIC_PROTECT  = 1, --公共防护
    --EVENT_ACTIVITY_TYPE_PRIVATE_PROTECT = 2, --个人防护
    --EVENT_ACTIVITY_TYPE_PUBLIC_CONQUER  = 3, --公共征服
    --EVENT_ACTIVITY_TYPE_PRIVATE_CONQUER = 4, --个人征服
    --EVENT_ACTIVITY_TYPE_PRIVATE_ESCORT  = 6, --个人护送
    --EVENT_ACTIVITY_TYPE_PUBLIC_BOSS     = 7, --公共boss

    --传送members的数据结构

    --pvp 属性
    --房间属性
    PVP_ROOM_KEY_PLAYERS          = 1, --玩家
    PVP_ROOM_KEY_FORTS            = 2, --据点
    PVP_ROOM_KEY_STAGE            = 3, --状态
    PVP_ROOM_KEY_STAGE_CRT_TIME   = 4,
    PVP_ROOM_KEY_FACTION_SCORE    = 5,
    PVP_ROOM_KEY_STAGE_3_TIME_SEC = 6, --pvp比赛时间
    PVP_ROOM_KEY_SETTLE_FLAG      = 7, --是否结算标志
    PVP_ROOM_KEY_SPECIAL_ITEM     = 8, --特殊道具刷新时间
    PVP_ROOM_KEY_SPACE_DROPS      = 9, --特殊道具的eid
    PVP_ROOM_KEY_MGR_MBSTR       = 10, --管理器的mailbox str

    --据点属性
    PVP_FORT_KEY_ID             = 1 ,--id
    PVP_FORT_KEY_FACTION        = 2 ,--阵营
    PVP_FORT_KEY_POWER          = 3 ,--能量
    PVP_FORT_KEY_POS            = 4 ,
    PVP_FORT_KEY_RAID           = 5 ,--半径
    PVP_FORT_KEY_NEARBY_COUNT   = 6 ,--半径内的人数
    PVP_FORT_KEY_NEARBY_FACTION = 7 ,--半径内玩家的阵营

    --玩家属性
    PVP_PLAYER_KEY_DBID     = 1,
    PVP_PLAYER_KEY_VOCATION = 2,
    PVP_PLAYER_KEY_NAME     = 3,
    PVP_PLAYER_KEY_LEVEL    = 4,
    PVP_PLAYER_KEY_KILL     = 5,
    PVP_PLAYER_KEY_DEATH    = 6,
    PVP_PLAYER_KEY_CONTRIBUTION = 7,
    PVP_PLAYER_KEY_MB_STR       = 8,
    PVP_PLAYER_KEY_EID          = 9,
    PVP_PLAYER_KEY_FACTION_ID   = 10,
    PVP_PLAYER_KEY_BORN_POS     = 11, -- 出生点
    PVP_PLAYER_KEY_LOAD_SCENE_COMPLETE = 12,
    PVP_PLAYER_KEY_ROTATION     = 13,

    --状态
    PVP_STAGE_1 = 1 ,--等玩家入场
    PVP_STAGE_2 = 2 ,--等待光门关闭
    PVP_STAGE_3 = 3 ,--光门关闭
    PVP_STAGE_4 = 4 ,--结算
    PVP_STAGE_5 = 5 ,--弹结算信息给玩家
    PVP_STAGE_6 = 6 ,--回收场景

    --外形显示类型
    FACADE_BODY   = 0, --角色外形显示
    FACADE_WEAPON = 1, --武器外形显示

    --时效类型
    RETENTION_INFINITE   = 0, --无限时间
    RETENTION_GET        = 1, --获得后计时
    RETENTION_USE        = 2, --使用后计时

    --守护灵获取类型渠道
    --EUDEMON_GET_WAY_UNKNOWN       = 0,--未知渠道
    --EUDEMON_GET_GM                = 1,--GM系统
    --EUDEMON_GET_AUTO_USE_ITEM     = 2,--道具自动使用获得

    --守护灵消耗渠道
    --EUDEMON_COST_WAY_UNKNOWN       = 0,--未知渠道
    --EUDEMON_COST_GM                = 1,--GM系统
    --EUDEMON_COST_UPGRADE_LEVEL     = 2,--升级消耗
    --EUDEMON_COST_UPGRADE_STAR      = 3,--升星消耗

    --守护灵上阵类型
    --EUDEMON_NOT_PLAYED                = 0,--未上阵
    --EUDEMON_PLAYED_NOT_CAPTAIN        = 1,--上阵但不是队长
    --EUDEMON_PLAYED_AND_CAPTAIN        = 2,--上阵且是队长

    --守护灵召唤类型
    --EUDEMON_CALL_NORMAL                = 1,--普通
    --EUDEMON_CALL_MYSTERY               = 2,--神秘

    --EUDEMON_LEVEL_S                    = 4,--S级

    --每个守护灵的数据结构中的key定义
    --EUDEMON_KEY_ID                     = 1,  --守护灵id
    --EUDEMON_PLAYED_STATE               = 2,  --守护灵上阵类型
    --EUDEMON_PLAYED_POS                 = 3,  --守护灵上阵界面显示槽位
    --EUDEMON_LEVEL                      = 4,  --守护灵等级
    --EUDEMON_STAR                       = 5,  --守护灵星级
    --EUDEMON_LEVEL_EXP                  = 6,  --守护灵当前等级经验
    --EUDEMON_LEARNED_SKILLS             = 7,  --守护灵已学被动技能 {slot:skill_id}
    --EUDEMON_LOCKED_SLOTS               = 8,  --守护灵已锁定被动技能槽位 {slot:1}


    --好友pk切磋属性 --房间属性
    FIGHT_PLAYER_ROOM_KEY_PLAYERS          = 1 , --玩家
    FIGHT_PLAYER_ROOM_KEY_SETTLE_FLAG      = 2 , --是否结算标志
    FIGHT_PLAYER_ROOM_KEY_STAGE            = 3 , --状态
    FIGHT_PLAYER_ROOM_KEY_STAGE_CRT_TIME   = 4 , --状态开始时间
    FIGHT_PLAYER_ROOM_KEY_STAGE_3_TIME_SEC = 5 , --FIGHT_PLAYER比赛时间

    --好友pk切磋属性 --玩家属性
    FIGHT_PLAYER_PLAYER_KEY_DBID          = 1,
    FIGHT_PLAYER_PLAYER_KEY_NAME          = 2,
    FIGHT_PLAYER_PLAYER_KEY_GENDER        = 3,
    FIGHT_PLAYER_PLAYER_KEY_VOCATION      = 4,
    FIGHT_PLAYER_PLAYER_KEY_RACE          = 5,
    FIGHT_PLAYER_PLAYER_KEY_LEVEL         = 6,
    FIGHT_PLAYER_PLAYER_KEY_EID           = 7,
    FIGHT_PLAYER_PLAYER_KEY_FACTION_ID    = 8,
    FIGHT_PLAYER_PLAYER_KEY_CONTRIBUTION  = 9,
    FIGHT_PLAYER_PLAYER_KEY_BORN_POS      = 10, -- 出生点
    FIGHT_PLAYER_PLAYER_KEY_ROTATION      = 11,

    --好友pk切磋属性 --状态
    FIGHT_PLAYER_STAGE_1    = 1 ,--等玩家入场。服务器等待全部进入场景。
    FIGHT_PLAYER_STAGE_2    = 2 ,--等待倒计时
    FIGHT_PLAYER_STAGE_3    = 3 ,--倒计时完
    FIGHT_PLAYER_STAGE_4    = 4 ,--结算
    FIGHT_PLAYER_STAGE_5    = 5 ,--弹结算信息给玩家
    FIGHT_PLAYER_STAGE_6    = 6 ,--回收场景
    FIGHT_PLAYER_STAGE_1_1  = 7 ,--等待玩家客户端loading。



    --------排行榜类型------------
    RANK_TYPE_COMBAT_VALUE      = 1, --战力榜
    RANK_TYPE_LEVEL             = 2, --等级榜
    RANK_TYPE_PET               = 3, --宠物
    RANK_TYPE_HORSE             = 4, --坐骑
    RANK_TYPE_WING              = 5, --翅膀
    RANK_TYPE_TALISMAN          = 6, --法宝
    RANK_TYPE_WEAPON            = 7, --神兵
    RANK_TYPE_CLOAK             = 8, --披风
    RANK_TYPE_AION_TOWER        = 9, --永恒之塔
    RANK_TYPE_ACHIEVEMENT       = 10, --成就点
    RANK_TYPE_OFFLINE_EXP       = 11, --离线效率
    RANK_TYPE_CROSS_1V1         = 12, --cross1v1
    RANK_TYPE_GEM               = 13, --宝石总等级
    RANK_TYPE_DAY_CHARGE        = 14, --每日充值榜(23点)
    --RANK_TYPE_GUILD           = 30, --工会榜


    ---------排行榜key定义-----------------
    RANK_KEY_CROSS_UUID             = 1,  --用cross_uuid记录，这个也可以得到dbid，兼容假如有一个跨服排行榜
    RANK_KEY_LEVEL                  = 2,  --玩家等级
    RANK_KEY_EXP                    = 3,  --玩家经验
    RANK_KEY_COMBAT_VALUE           = 4,  --战斗力
    RANK_KEY_NAME                   = 5,  --玩家名字
    RANK_KEY_VOCATION               = 6,  --玩家职业
    RANK_KEY_FACADE                 = 7,  --外观
    RANK_KEY_NOBILITY               = 8,  --爵位
    RANK_KEY_PET_GRADE              = 9,  --宠物阶级
    RANK_KEY_PET_STAR               = 10, --宠物星级
    RANK_KEY_PET_BLESS              = 11, --宠物祝福值
    RANK_KEY_HORSE_GRADE            = 12, --坐骑阶级
    RANK_KEY_HORSE_STAR             = 13, --坐骑星级
    RANK_KEY_HORSE_BLESS            = 14, --坐骑祝福值
    RANK_KEY_TREASURE_LEVEL         = 15, --(翅膀、法宝、神兵、披风)等级
    RANK_KEY_TREASURE_EXP           = 16, --(翅膀、法宝、神兵、披风)经验
    RANK_KEY_AION_LAYER             = 17, --永恒之塔层数
    RANK_KEY_ACHIEVEMENT            = 18, --成就点
    RANK_KEY_OFFLINE_EXP            = 19, --离线效率(每分钟获得经验值)
    RANK_KEY_GUILD_NAME             = 20, --公会名
    RANK_KEY_GUILD_DBID             = 21, --公会dbid
    RANK_KEY_BONFIRE_SCORE          = 22, --公会篝火积分
    RANK_KEY_UPDATE_TIME            = 23, --数据更新时间
    RANK_KEY_CROSS_1V1_POINT        = 24, --cross 1v1积分
    RANK_KEY_GEM_LEVEL              = 25, --宝石总等级
    RANK_KEY_DAY_CHARGE             = 26, --每日充值
    RANK_KEY_RANK_VALUE             = 27, --名次
    RANK_KEY_VIP_LEVEL              = 28, --vip
    RANK_KEY_SHOW_INFO              = 29, --显示信息
    RANK_KEY_IS_OPEN_SERVER_RANK    = 30, --是否开服冲榜的 1:是

    --交易所系统
    AUCTION_ITEM_DBID           = 1,   --auction_item_dbid
    AUCTION_ITEM_ITEM           = 2,   --具体道具信息,包括数量
    AUCTION_ITEM_PRICE          = 3,   --总价
    AUCTION_ITEM_CREATE_TIME    = 4,   --上架时间
    AUCTION_ITEM_PASSWD         = 5,   --密码 --负1为没有密码

    BEG_ITEM_DBID           = 1,   --beg_item_dbid
    BEG_ITEM_ITEM           = 2,   --道具信息，包括数量
    BEG_ITEM_PRICE          = 3,   --总价
    BEG_ITEM_CREATE_TIME    = 4,   --求购时间
    BEG_ITEM_AVATAR_NAME    = 5,   --求购人的名字

    MAX_AUCTION_LOG                      = 20,

    AUCTION_OPERATION_BUY                = 1, --买
    AUCTION_OPERATION_SELL               = 2, --卖
    AUCTION_OPERATION_BEG                = 3, --买
    AUCTION_OPERATION_BEG_SELL           = 4, --卖

    --工会详细信息
    GUILD_DETAIL_INFO_RANK              = 1,  --排名
    GUILD_DETAIL_INFO_DBID              = 2,  --dbid
    GUILD_DETAIL_INFO_ICON_ID           = 3,  --图标
    GUILD_DETAIL_INFO_NAME              = 4,  --名字
    GUILD_DETAIL_INFO_LEVEL             = 5,  --级别
    GUILD_DETAIL_INFO_COUNT             = 6,  --人数
    GUILD_DETAIL_INFO_MAX_COUNT         = 7,  --最大人数
    GUILD_DETAIL_INFO_PRESIDENT_NAME    = 8,  --公会首领名字
    GUILD_DETAIL_INFO_ANNOUNCEMENT      = 9,  --公告
    GUILD_DETAIL_INFO_NEED_VERIFY       = 10, --是否需要验证
    GUILD_DETAIL_INFO_EXPERIENCE        = 11, --经验 --资金  --用于LEVEL升级
    --GUILD_DETAIL_INFO_PRESTIGE        = 12, --威望
    GUILD_DETAIL_INFO_MEMBERS           = 13, --成员信息
    GUILD_DETAIL_INFO_PAGE              = 14, --当前页
    GUILD_DETAIL_INFO_MAX_PAGE          = 15, --最大的页数
    GUILD_DETAIL_INFO_FLAG_ID           = 16, --图标
    GUILD_DETAIL_INFO_GRADE             = 17, --评级
    GUILD_DETAIL_INFO_SET_ANNO_COUNT    = 18, --设置公告。已设置次数。每周清空，周一0点 。set_announcement
    GUILD_DETAIL_INFO_GUILD_FIGHT_FORCE = 19, --公会战斗力。(所有玩家战斗力加起来)/19.8。用于排序

    --公会成员数据里面的key
    GUILD_MEMBER_INFO_DBID          = 1,    --成员dbid
    GUILD_MEMBER_INFO_NAME          = 2,    --成员名字
    GUILD_MEMBER_INFO_GENDER        = 3,    --成员性别
    GUILD_MEMBER_INFO_VOCATION      = 4,    --成员职业
    GUILD_MEMBER_INFO_RACE          = 5,    --成员种族
    GUILD_MEMBER_INFO_LEVEL         = 6,    --成员等级
    GUILD_MEMBER_INFO_CONTRIBUTE    = 7,    --成员贡献。总贡献。
    GUILD_MEMBER_INFO_OFFLINE_TIME  = 8,    --成员下线时间
    GUILD_MEMBER_INFO_FIGHT_FORCE   = 9,    --战斗力
    GUILD_MEMBER_INFO_IS_ONLINE     = 10,   --是否在线 (没有存盘的属性)
    GUILD_MEMBER_INFO_POSITION      = 11,   --职位 (没有存盘的属性)
    GUILD_MEMBER_INFO_NOBILITY      = 12,   --爵位 nobility_rank
    GUILD_MEMBER_INFO_CREATE_TIME   = 13,   --加入公会时间

    --玩家身上公会信息的字段 --avatar.guild_info
    GUILD_INFO_DBID                     = 1,    --公会的dbid
    GUILD_INFO_NAME                     = 2,    --公会的名字
    GUILD_INFO_POSITION                 = 3,    --公会的职位
    GUILD_INFO_APPLY_LIST               = 4,    --玩家申请公会的信息
    --GUILD_INFO_LAST_SIGN_ON_TIME      = 5,    --上次签到的时间
    --GUILD_INFO_SIGN_ON_REWARD_PROCESS = 6,    --签到宝箱领取记录
    GUILD_INFO_SKILL                    = 7,    --公会技能
    --GUILD_INFO_SHOP_RECORD            = 8,    --商店购买记录
    --GUILD_INFO_SHOP_TIME              = 9,    --公会商店时间
    GUILD_INFO_HAS_DAILY_REWARD         = 10,   --是否领取公会每日奖励 0和1
    GUILD_INFO_DAILY_RED_COUNT          = 11,   --每天发的money红包数量
    GUILD_INFO_WAREHOUSE_POINTS         = 12,   --公会仓库积分  --退出公会后会长职位将自动转让给下级职位玩家，同时公会贡献、仓库积分将被清空，您已发的公会红包也将会失效

    --申请请求详细信息
    GUILD_APPLY_INFO_DBID          = 1,    --dbid
    GUILD_APPLY_INFO_NAME          = 2,    --名字
    GUILD_APPLY_INFO_GENDER        = 3,    --性别
    GUILD_APPLY_INFO_VOCATION      = 4,    --职业
    GUILD_APPLY_INFO_RACE          = 5,    --种族
    GUILD_APPLY_INFO_LEVEL         = 6,    --等级
    GUILD_APPLY_INFO_CONTRIBUTE    = 7,    --贡献
    GUILD_APPLY_INFO_FIGHT_FORCE   = 8,    --战斗力
    GUILD_APPLY_INFO_TIME          = 9,    --申请时间
    GUILD_APPLY_INFO_NOBILITY      = 10,   --nobility

    --公会职位
    GUILD_POSITION_NORMAL           = 0,    --成员
    GUILD_POSITION_ELITE            = 1,    --精英
    GUILD_POSITION_VICE_PRESIDENT   = 2,    --副会长
    GUILD_POSITION_PRESIDENT        = 3,    --公会长

    --公会是否需要审核
    GUILD_NEED_VERIFY_YES           = 0,    --公会需要检查
    GUILD_NEED_VERIFY_NO            = 1,    --公会不需要检查

    --公会回复申请
    GUILD_REPLY_YES                 = 0,    --通过
    GUILD_REPLY_NO                  = 1,    --不通过

    --公会身上的删除标记
    GUILD_DELETE_NORMAL             = 0,    --正常
    GUILD_DELETE_DISMISS            = 1,    --解散

    GUILD_INFO_LIST_COUNT           = 20,  --公会列表每页多少条数据
    GUILD_INFO_RANK_COUNT           = 50,  --排行榜

     --公会红包
     GUILD_RED_ENVELOPE_INFO_ID            = 1,  --唯一id。每个公会独有唯一。红包列表的key，所以红包列表是字典，因为会直接设置某个红包key为nil，如果超时删除。
     GUILD_RED_ENVELOPE_INFO_CFG_ID        = 2,  --红包配置id。索引红包配置表。
     GUILD_RED_ENVELOPE_INFO_AVATAR_DBID   = 3,  --所属的玩家id
     GUILD_RED_ENVELOPE_INFO_AVATAR_NAME   = 4,  --所属玩家的名字 --不存盘，去UserMgr得到。
     GUILD_RED_ENVELOPE_INFO_MONEY_TYPE    = 5,  --货币类型 。--如果CFG_ID是money红包，那么是钻石。如果是系统红包，那么是绑钻。--不存盘，因为可以通过CFG_ID得到。
     GUILD_RED_ENVELOPE_INFO_MONEY_COUNT   = 6,  --货币数量。系统红包，红包配置表得到。money红包，玩家输入。
     GUILD_RED_ENVELOPE_INFO_TIME          = 7,  --生成时间
     GUILD_RED_ENVELOPE_INFO_IS_SEND       = 8,  --是否已经发送。系统包默认未发送，要发送。money红包默认已发送。
     GUILD_RED_ENVELOPE_INFO_PACKET        = 9,  --可领数量。系统红包，红包配置表得到。money红包，玩家输入。
     GUILD_RED_ENVELOPE_INFO_SET           = 10, --玩家领取记录 。{{dbid,name,money},}。列表，因为需要顺序。原始领取数据。--name应该不存盘，去UserMgr得到。
     GUILD_RED_ENVELOPE_INFO_GET_PACKET    = 11, --已经领了的数量。--领取记录中得到存盘。--后端不用到，仅仅记录给前端，因为都要遍历set。或者不要都可以，客户端反正也遍历。---gotten_packet --应该不要这个。或者兼容，后端遍历计算得到。
     GUILD_RED_ENVELOPE_INFO_GOTTEN_MONEY  = 12, --已领取货币数量。上面的money累加得到存盘。--领取记录中得到。抢红包的时候，得到剩余数量，再随机。--后端不用到，仅仅记录给前端，因为都要遍历set。或者不要都可以，客户端反正也遍历。--应该不要这个。或者兼容，后端遍历计算得到。
     GUILD_RED_ENVELOPE_INFO_BLESSING      = 13, --祝福语
     GUILD_RED_ENVELOPE_INFO_AVATAR_VOC    = 14, --所属玩家的职业。--不存盘，去UserMgr得到。

     --公会红包日志
     GUILD_RED_ENVELOPE_RECORD_CFG_ID        = 1,  --红包配置id。索引红包配置表。
     GUILD_RED_ENVELOPE_RECORD_AVATAR_NAME   = 2,  --所属玩家的名字
     GUILD_RED_ENVELOPE_RECORD_TIME          = 3,  --生成时间
     GUILD_RED_ENVELOPE_RECORD_MONEY_COUNT   = 4,  --货币数量

     --公会仓库日志
     GUILD_WAREHOUSE_RECORD_AVATAR_NAME   = 1,  --玩家的名字
     GUILD_WAREHOUSE_RECORD_TIME          = 2,  --生成时间
     GUILD_WAREHOUSE_RECORD_ITEM_ID       = 3,  --道具id
     GUILD_WAREHOUSE_RECORD_TYPE          = 4,  --操作类型。1捐赠 2销毁 3兑换

     GUILD_WAREHOUSE_RECORD_TYPE_SELL     = 1,  --操作类型。1捐赠 2销毁 3兑换
     GUILD_WAREHOUSE_RECORD_TYPE_DESTROY  = 2,  --操作类型。1捐赠 2销毁 3兑换
     GUILD_WAREHOUSE_RECORD_TYPE_BUY      = 3,  --操作类型。1捐赠 2销毁 3兑换

     MAX_GUILD_PKG_WAREHHOUSE_LOG         = 50,
    MAX_RED_ENVELOPE_LOG                 = 50,

    --玩家永恒之塔信息
    AION_NOW_LAYER              = 1,    -- 当前层数(已通过的最高层)
    AION_LAST_MAIL_REWARD_TIME  = 2,    -- 上一次获取邮件奖励的时间

    --扫荡状态
    --AION_SWEEP_STATUS_TRUE   = 1,    --正在扫荡
    --AION_SWEEP_STATUS_FALSE  = 2,

    --AION_MAX_RESET_TIMES        = 1, -- 重置次数上限
    --AION_MIN_LAYER              = 1, -- 最低层数
    --AION_SWEEP_TIME_PER_LAYER   = 60, -- 每层扫荡时间

    --永恒之塔 房间信息
    -- AION_ROOM_KEY_STAGE_CUT_TIME    = 1,    --
    -- AION_ROOM_KEY_LIMIT_TIME        = 2,    -- 这层的通关限制时间
    -- AION_ROOM_KEY_STAGE             = 3,

    -- --永恒之塔 状态
    -- AION_PLAYER_STAGE_1    = 1,    --
    -- AION_PLAYER_STAGE_2    = 2,    -- 开始战斗
    -- AION_PLAYER_STAGE_3    = 3,    -- 超时回收场景
    -- AION_PLAYER_STAGE_4    = 4,    --
    
    -- --结果
    -- AION_RESULT_SUCCESS     = 0,    --成功
    -- AION_RESULT_FAIL        = 1,    --死亡失败
    -- AION_RESULT_TIME_OUT    = 2,    --超时失败
    
    --关卡类型
    --AION_TYPE_COMMON    = 1,    --普通关
    --AION_TYPE_BOSS      = 2,    --BOSS关

    --临时信息
    --AION_SWEEP_START_TIME   = 1,    --扫荡开始时间
    --AION_SWEEP_START_LAYER  = 2,    --开始层数
    --AION_SWEEP_NOW_LAYER    = 3,    --当前扫荡到的层数
    --AION_SWEEP_LAYER_START_TIME = 4,--

    --世界boss信息
    WORLD_BOSS_KEY_LAST_RESET_TIME  = 1, --上次重置日数据的时间
    WORLD_BOSS_KEY_INTEREST_LIST    = 2, --关注列表bosscfgid bosscfgid bosscfgid ...
    WORLD_BOSS_KEY_PILAO            = 3, --疲劳值
    WORLD_BOSS_KEY_KILL_NUM         = 4,  --当日击杀boss数
    WORLD_BOSS_KEY_LAST_KILL_TIME   = 5,    -- 上次击杀boss时间

    --WORLD_BOSS_MAP_LINE           = 1,    --line
    --WORLD_BOSS_BASE_MB_STR        = 2,    --base mb str
    --WORLD_BOSS_CELL_MB_STR        = 3,    --cell mb str
    --WORLD_BOSS_COUNT              = 4,    --在raid的人数
    --WORLD_BOSS_NUMBERS            = 5,    --raid的成员信息
    --WORLD_BOSS_SETTING_MAP        = 6,    --是否在申请地图中

    --WORLD_BOSS_STAGE_1            = 1,
    --WORLD_BOSS_STAGE_2            = 2,
    --WORLD_BOSS_STAGE_3            = 3,
    --WORLD_BOSS_STAGE_4            = 4,

    --世界boss 伤害排行榜信息信息
    --WORLD_BOSS_PLAYER_INFO_DBID          = 1,    --dbid
    --WORLD_BOSS_PLAYER_INFO_NAME          = 2,    --成员名字
    --WORLD_BOSS_PLAYER_INFO_VOCATION      = 3,    --职业
    --WORLD_BOSS_PLAYER_INFO_HARM          = 4,    --伤害

    --世界boss 伤害排行榜信息信息
    --WORLD_BOSS_GUILD_INFO_DBID          = 1,    --dbid
    --WORLD_BOSS_GUILD_INFO_NAME          = 2,    --公会名字
    --WORLD_BOSS_GUILD_INFO_HARM          = 4,    --伤害

    --留言版身上的删除标记
    DICUSSION_DELETE_NORMAL             = 0,    --正常
    DICUSSION_DELETE_DISMISS            = 1,    --删除

    --留言版身上的删除标记
    DICUSSION_INFO_DBID                 = 1,    --
    DICUSSION_INFO_RANK                 = 2,    -- 排序
    DICUSSION_INFO_AVATAR_DBID          = 3,    -- 作者dbid
    DICUSSION_INFO_AVATAR_NAME          = 4,    --作者名字
    DICUSSION_INFO_AVATAR_VOCATION      = 5,    --作者职业
    DICUSSION_INFO_SUPPORT              = 6,    --点赞数
    DICUSSION_INFO_SUPPORT_FLAG         = 7,    --是否点赞
    DICUSSION_INFO_SUPPORT_TEXT         = 8,    --文本内容
    DICUSSION_INFO_TIME                 = 9,    --文本创建时间

    --玩家占卜信息
    DIVINE_WEEK_NUM                     = 1,    -- 本周次数
    DIVINE_DAY_NUM                      = 2,    -- 当天次数
    DIVINE_WEEK_NUM_REWARD              = 3,    -- 每周次数累计奖励已领取list
    DIVINE_STAR_POWER                   = 4,    -- 辰星之力
    DIVINE_ULTIMATE_REWARD_PROBABILITY  = 5,    -- 保底奖励概率

    MATCH_INFO_LIST         = 1,    --匹配队列
    MATCH_INFO_COUNT        = 2,    --匹配人数
    MATCH_INFO_MAP_ID       = 3,    --匹配地图
    MATCH_INFO_TIME         = 4,    --匹配的时间
    MATCH_INFO_PARAMS       = 5,    --匹配排行参数

    RUNE_TASK_STATE_ACCEPTED   = 1,   --符文激活中
    RUNE_TASK_STATE_COMPLETED  = 2,   --符文激活完成

    MONSTER_BUILD_TYPE_SERVER = 0,   --服务端怪
    MONSTER_BUILD_TYPE_ALL_CLIENT = 1, --客户端怪

    SPELL_STATE_FLY  =  28,  --飞行状态
    SPELL_STATE_GLIDE = 29,  --滑翔状态

    --坐骑背包数据结构key
    --RIDE_KEY_ID                     = 1,  --坐骑id

    --pk模式
    PK_MODE_PEACE       = 0,
    PK_MODE_FORCE       = 1,
    PK_MODE_ALL         = 2,

    --刷怪点已经刷出标志
    REGION_NOT_SPAWN = 0,
    REGION_HAS_SPAWN = 1,

    --宝物类型
    TREASURE_TYPE_WING      = 1,    --翅膀
    TREASURE_TYPE_TALISMAN  = 2,    --法宝
    TREASURE_TYPE_WEAPON    = 3,    --神兵
    TREASURE_TYPE_CLOAK     = 4,    --披风
    TREASURE_TYPE_PET       = 5,    --宠物
    TREASURE_TYPE_HORSE     = 6,    --坐骑

    --宝物（翅膀、法宝、神兵、披风）的属性
    TREASURE_KEY_UNLOCK             = 1, --0:未解锁 1：解锁
    TREASURE_KEY_LEVEL              = 2, --等级
    TREASURE_KEY_EXP                = 3, --经验
    --TREASURE_KEY_HIDE               = 4, --0:不隐藏 1:隐藏
    TREASURE_KEY_ITEM_USE_NUM       = 5, --道具使用次数
    TREASURE_KEY_SKILL              = 6, --解锁的技能
    TREASURE_KEY_ATTRI              = 7, --翎羽灵珠等道具额外加的属性
    TREASURE_KEY_ATTRI_TOTAL_PER    = 8, --总属性百分比

    --宠物info的key
    PET_KEY_ATTRI               = 1, --{k:v},属性,道具给的属性,人物
    PET_KEY_ATTRI_TOTAL_PER     = 2, --总属性百分比,道具给的属性,人物
    PET_KEY_USE                 = 3, --{k:v},道具id,count

    --坐骑info的key
    HORSE_KEY_ATTRI               = 1, --{k:v},属性,道具给的属性,人物
    HORSE_KEY_ATTRI_TOTAL_PER     = 2, --总属性百分比,道具给的属性,人物
    HORSE_KEY_USE                 = 3, --{k:v},道具id,count

    -------------经验副本信息的key------------
    EXP_INSTANCE_ENTER_TIMES        = 1,    --已进入的次数
    EXP_INSTANCE_BUY_ENTER_NUM      = 2,    --已购买的进入次数
    --EXP_INSTANCE_LEFT_ENTER_NUM     = 3,    --剩余进入次数
    EXP_INSTANCE_LAST_ENTER_TIME    = 4,    --上一次进入的时间
    EXP_INSTANCE_ITEM_ADD_ENTER_NUM = 5,    --道具增加的进入次数
    EXP_INSTANCE_LAST_EXP           = 6,    --上一次获得的经验
    --EXP_INSTANCE_MAX_EXP           = 4,    --之前获得的最高经验

    --经验副本 房间信息
    EXP_INSTANCE_ROOM_KEY_PREPARE_TIME    = 1,    --准备时间
    EXP_INSTANCE_ROOM_KEY_STAGE           = 2,
    EXP_INSTANCE_ROOM_KEY_START_TIME      = 3,    --开始时间
    EXP_INSTANCE_ROOM_KEY_END_TIME        = 4,    --结束时间
    EXP_INSTANCE_ROOM_KEY_MONSTER_FLAG    = 5,    --是否已经刷怪的标记
    EXP_INSTANCE_ROOM_KEY_LEVEL           = 6,
    EXP_INSTANCE_ROOM_KEY_MONSTER_LIST    = 7,
    EXP_INSTANCE_ROOM_KEY_MONSTER_REVIVE_LIST=8,
    EXP_INSTANCE_ROOM_KEY_KILL_MONSTER_NUM= 9,
    EXP_INSTANCE_ROOM_KEY_WAVE            = 10,
    EXP_INSTANCE_ROOM_KEY_PLAYERS         = 11,


    --刷怪点的怪物节点信息
    POINT_INFO_COORD = 1,
    POINT_INFO_EID = 2,
    POINT_INFO_NEXT_BORN_TIME = 3,
    POINT_INFO_MONSTER_ID = 4,
    POINT_INFO_REBORN_DURATION_SEC = 5,
    POINT_INFO_START_FACE = 6,
    POINT_INFO_REGION_ID = 7,
    POINT_INFO_DYNAMIC_START_FLAG = 20,--下面都是动态的可有可无的信息,上面是基本必备的信息
    POINT_INFO_DYNAMIC_PROPERTY_ID = 21,
    POINT_INFO_DYNAMIC_AWARD_EXP = 22,
    POINT_INFO_DYNAMIC_FACTION_ID = 23,
    POINT_INFO_DYNAMIC_DROP_ID  = 24,

    --时装类型
    FASHION_TYPE_CLOTHES        = 1, --衣服
    FASHION_TYPE_WEAPON         = 2, --武器
    FASHION_TYPE_SPECIAL        = 3, --特效
    FASHION_TYPE_PHOTO          = 4, --头像框
    FASHION_TYPE_CHAT           = 5, --聊天框

    --时装信息的key
    FASHION_INFO_LEVEL       = 1, --等级
    FASHION_INFO_EQUIPPED    = 2, --当前穿戴的时装id


    --各个地图中的死亡方式
    DIE_TYPE_CAN_NOT         = 0, --不可复活
    DIE_TYPE_NORMAL          = 1, --一般流程，随时可以复活（在复活点or原地）
    DIE_TYPE_LIKE_WOLRD_BOSS = 2, --类似世界boss 受到复活疲劳buff限制（在复活点or原地）
    DIE_TYPE_WAIT_N_SEC      = 3, --强制等待N秒限制才能复活（在复活点）

    --装备强化
    EQUIP_STREN_INFO_LEVEL    = 1,
    EQUIP_STREN_INFO_EXP      = 2,

    --装备洗练
    EQUIP_WASH_INFO_ATTRI_MIN     = 1, --[i]=attri_id,[i+1]=attri_value,[i+2]=attri_index
    EQUIP_WASH_INFO_ATTRI_MAX     = 15, --12,--可以存储4条属性 --多加一条，vip6可开第五条
    EQUIP_WASH_INFO_SLOT_COUNT    = 31,--已开启的槽位的数量,按照顺序开.--预留给前面的属性数据，这里从31开始

    --洗练颜色
    EQUIP_WASH_COLOR_W             = 1, --白
    EQUIP_WASH_COLOR_B             = 2, --蓝
    EQUIP_WASH_COLOR_P             = 3, --紫色
    EQUIP_WASH_COLOR_O             = 4, --橙色
    EQUIP_WASH_COLOR_R             = 5, --红色

    --主动技能解锁方式
    SKILL_ACTIVE_UNLOCK_WAY_TASK       = 1, --任务解锁
    SKILL_ACTIVE_UNLOCK_WAY_ITEM       = 2, --获得道具解锁

    --被动技能解锁方式
    SKILL_PASSIVE_UNLOCK_WAY_TASK     = 1,  --任务解锁
    SKILL_PASSIVE_UNLOCK_WAY_ITEM     = 2,  --获得道具解锁

    --天赋类型
    TALENT_TYPE_1 = 1,  --攻击
    TALENT_TYPE_2 = 2,  --防御
    TALENT_TYPE_3 = 3,  --通用
    TALENT_TYPE_4 = 4,  --精通

    --天赋加点条件
    TALENT_CONDITION_NEED_POINT_1   = 1, --攻击类投入指定点数
    TALENT_CONDITION_NEED_POINT_2   = 2, --防御系投入制定点数
    TALENT_CONDITION_NEED_PRE       = 3, --学习前置天赋技能
    TALENT_CONDITION_NEED_POINT_ALL = 4, --全部投入的总点数

    --装备宝石
    EQUIP_GEM_INFO_GEM_MIN        = 1,--[i]=gem_item_id
    EQUIP_GEM_INFO_GEM_MAX        = 5,
    EQUIP_GEM_INFO_GEM_MAX1       = 6, --vip的孔 vip5的时候开启，且有效期才能镶嵌
    EQUIP_GEM_INFO_HOLE_COUNT     = 7, --非vip的孔的数量
    EQUIP_GEM_INFO_REFINE_LEVEL   = 8, --精炼等级
    EQUIP_GEM_INFO_REFINE_EXP     = 9, --精炼经验
    EQUIP_GEM_INFO_GEM_BIND_MIN   = 10,--[i]=bind_flag --和gem_item_id对应 +10的方式处理记录绑定状态
    EQUIP_GEM_INFO_GEM_BIND_MAX   = 15,
    EQUIP_GEM_INFO_GEM_BIND_MAX1  = 16, --vip的孔 vip5的时候开启，且有效期才能镶嵌
    
    --守护结界塔玩家信息
    HOLY_TOWER_KEY_LEFT_ENTER_NUM   = 1,      --剩余进入次数
    HOLY_TOWER_KEY_BUY_NUM          = 2,      --当天购买次数
    HOLY_TOWER_KEY_PASS_INFO        = 3,      --通关信息 {id=star, ... }
    HOLY_TOWER_KEY_ENTER_NUM        = 4,      --已进入次数
    HOLY_TOWER_KEY_LAST_ENTER_TIME  = 5,        --上次进入时间

    --守护结界塔房间信息
    HOLY_TOWER_ROOM_KEY_STAGE                   = 1,    --状态
    HOLY_TOWER_ROOM_KEY_STAGE_START_TIME        = 2,    --开始时间
    --HOLY_TOWER_ROOM_KEY_END_TIME                = 3,    --结束时间
    HOLY_TOWER_ROOM_KEY_WAVE_BIG                = 4,    --大波数
    HOLY_TOWER_ROOM_KEY_WAVE_SMALL              = 5,    --小波数
    HOLY_TOWER_ROOM_KEY_ID                      = 6,    --关卡id
    HOLY_TOWER_ROOM_KEY_MONSTER_REFRESH_FLAG    = 7,    --怪是否刷玩标志
    HOLY_TOWER_ROOM_KEY_DROP_INFO               = 8,    --掉落信息
    ROOM_KEY_MONSTER_NEXT_WAVE_TIME             = 9,

    --符文
    RUNE_ID                 = 1,    --符文表ID  rune.xml->id_i
    RUNE_LEVEL              = 2,    --符文等级

    --符文寻宝记录
    RUNE_LUCK_ID            = 1,    --寻到的符文ID
    RUNE_LUCK_TIME          = 2,    --寻宝的时间
    RUNE_LUCK_AVATAR_NAME   = 3,    --寻宝角色

    RUNE_NOT_PUTON_LIMIT_TYPE   = 90,   --符文类型type超过(>)此值，则不给密穿戴

    --所有副本结算key --通用的
    INSTANCE_SETTLE_KEY_MAP_ID        = 1, --地图id
    INSTANCE_SETTLE_KEY_RESULT        = 2, --1成功，0失败。win = 1 or 0
    INSTANCE_SETTLE_KEY_ITEMS         = 3, --奖励的道具 {k:v}
    INSTANCE_SETTLE_KEY_STAR          = 4, --星级
    INSTANCE_SETTLE_KEY_KILL_NUM      = 5, --击杀怪物数量
    INSTANCE_SETTLE_KEY_KILL_EXP      = 6, --守卫公会用的 杀怪经验
    INSTANCE_SETTLE_KEY_STAR_EXP      = 7, --守卫公会用的 评分经验
    INSTANCE_SETTLE_KEY_CONQUEST_LIST = 8, --公会争霸结算列表。字典。双方所有人。{dbid:{guild_dbid,player_name,玩家爵位，击杀，死亡，夺点,玩家战斗力}}
    INSTANCE_SETTLE_KEY_BONFIRE_RANK  = 9, --工会篝火排行

    --挂机设置key
    HANG_UP_KEY_AUTO_PICK           = 1, --自动拾取
    HANG_UP_KEY_AUTO_SELL           = 2, --自动出售  1：开 0：关
    HANG_UP_KEY_AUTO_EAT            = 3, --自动吞噬  1：开 0：关
    HANG_UP_KEY_AUTO_TEAM           = 4, --自动组队  1：开 0：关
    HANG_UP_KEY_AUTO_BUY_REVIVAL    = 5, --自动买活  1：开 0：关

    --自动拾取类型 avatar.hang_up_setting[HANG_UP_KEY_AUTO_PICK] = {}
    AUTO_PICK_WHITE     = 1,    --白装    --1为勾选  0为不选
    AUTO_PICK_BLUE      = 2,    --蓝装    --1为勾选  0为不选
    AUTO_PICK_PURPLE    = 3,    --紫装    --1为勾选  0为不选
    AUTO_PICK_ORANGE    = 4,    --橙装    --1为勾选  0为不选
    AUTO_PICK_GOLD      = 5,    --金币    --1为勾选  0为不选
    AUTO_PICK_OTHER     = 6,    --其他    --1为勾选  0为不选

    --品质
    EQUIP_QUALITY_WHITE     = 1,    --白
    EQUIP_QUALITY_BLUE      = 3,    --蓝
    EQUIP_QUALITY_PURPLE    = 4,    --紫
    EQUIP_QUALITY_ORANGE    = 5,    --橙
    EQUIP_QUALITY_RED       = 6,    --红
    EQUIP_QUALITY_PINK      = 7,    --粉

    --星级
    EQUIP_STAR_ZERO  = 0,
    EQUIP_STAR_ONE   = 1,
    EQUIP_STAR_TWO   = 2,
    EQUIP_STAR_THREE = 3,

    --普通副本 --通用的roomkey,factionkey,player_key
    INSTANCE_ROOM_KEY_PLAYERS          = 1,
    INSTANCE_ROOM_KEY_STAGE            = 2,
    INSTANCE_ROOM_KEY_STAGE_START_TIME = 3,
    INSTANCE_ROOM_KEY_SETTLE_FLAG      = 4,
    INSTANCE_ROOM_KEY_KILL_MONSTERS    = 5,
    INSTANCE_ROOM_KEY_ID               = 6, --关卡id
    INSTANCE_ROOM_KEY_WAVE             = 7, --波数
    INSTANCE_ROOM_KEY_FACTIONS         = 8, --按照阵营记录数据
    INSTANCE_ROOM_KEY_MGR_MBSTR        = 9, --mgr的base_mbstr
    INSTANCE_ROOM_KEY_ROOM_ID          = 10, --room_id
    INSTANCE_ROOM_KEY_OVERLORD_INFO    = 11, --主宰工会信息
    INSTANCE_ROOM_KEY_PLAYER_COLLECT_COUNT = 12,--玩家采集数量
    INSTANCE_ROOM_KEY_DROP_INFO            = 13,
    INSTANCE_ROOM_KEY_START_TIME       = 14, --spaceloader start的时间
    INSTANCE_ROOM_KEY_SKY_DROP_STAGE   = 15, --天降密宝状态
    INSTANCE_ROOM_KEY_BOSS_MAX_HP      = 16, --boss总血量
    INSTANCE_ROOM_KEY_BOSS_CUR_HP      = 17, --boss当前血量
    INSTANCE_ROOM_KEY_BOSS_MONSTER_ID  = 18, --boss怪物id

    --factionkey
    INSTANCE_FACTION_KEY_PLAYERS        = 1,
    INSTANCE_FACTION_KEY_GUILD_DBID     = 2,
    INSTANCE_FACTION_KEY_GUILD_NAME     = 3,
    INSTANCE_FACTION_KEY_GUILD_ICON     = 4,
    INSTANCE_FACTION_KEY_GUILD_FLAG     = 5,
    INSTANCE_FACTION_KEY_GUILD_RANK     = 6,
    INSTANCE_FACTION_KEY_PLAYERS_NUM    = 7,
    INSTANCE_FACTION_KEY_RESOURCE_NUM   = 8,
    INSTANCE_FACTION_KEY_RESOURCE_SCORE = 9,

    --playerkey --普通副本(个人boss、金币副本 结界塔 装备副本)
    INSTANCE_PLAYER_KEY_DBID            = 1,
    INSTANCE_PLAYER_KEY_EID             = 2,
    INSTANCE_PLAYER_KEY_CLIENT_LOADING  = 3,
    INSTANCE_PLAYER_KEY_KILL            = 4,
    INSTANCE_PLAYER_KEY_DIE             = 5,
    INSTANCE_PLAYER_KEY_TAKE_RES_COUNT  = 6,
    INSTANCE_PLAYER_KEY_FACTION_ID      = 7,
    INSTANCE_PLAYER_KEY_NAME            = 8,
    INSTANCE_PLAYER_KEY_FIGHT_FORCE     = 9,
    INSTANCE_PLAYER_KEY_NOBILITY_RANK   = 10,
    INSTANCE_PLAYER_KEY_UUID            = 11,

    INSTANCE_STAGE_1                   = 1, --等待服务器进入
    INSTANCE_STAGE_2                   = 2, --等待客户端进入
    INSTANCE_STAGE_3                   = 3, --等待光门倒计时 --同步给客户端，说明光门开始倒计时
    INSTANCE_STAGE_4                   = 4, --玩法                          --同步给客户端，说明光门关闭，开始玩法，刷怪
    INSTANCE_STAGE_5                   = 5, --结算
    INSTANCE_STAGE_6                   = 6, --回收场景

    INSTANCE_STAGE_1_WAIT_TIME         = 5,   --等待服务器进入 --超时
    INSTANCE_STAGE_2_WAIT_TIME         = 10,  --等待客户端进入 --超时
    INSTANCE_STAGE_3_WAIT_TIME         = 0,   --等待光门倒计时 --倒计时间  --5 0 如果为0那么就没有光门倒计时，然而这个过渡状态还是有的，即使下一tick就立刻到play的状态
    INSTANCE_STAGE_4_WAIT_TIME         = 600, --玩法 --最大时间  --超时
    INSTANCE_STAGE_5_WAIT_TIME         = 60,  --结算 --界面显示最大时间

    --被动技能target_type
    PASSIVE_SPELL_TARGET_TYPE_SELF   = 1,
    PASSIVE_SPELL_TARGET_TYPE_OWNER  = 2,
    PASSIVE_SPELL_TARGET_TYPE_PET    = 3,
    PASSIVE_SPELL_TARGET_TYPE_FABAO  = 4,

    --个人boss key
    PERSONAL_BOSS_KEY_CAN_ENTER_NUM   = 1, --当日可进入次数
    PERSONAL_BOSS_KEY_ENTER_NUM       = 2, --当日已进入次数
    PERSONAL_BOSS_KEY_LAST_ENTER_TIME = 3, --上次进入的时间


    --玩家金币副本信息key
    INSTANCE_GOLD_KEY_LEFT_ENTER_NUM   = 1,      --剩余进入次数
    INSTANCE_GOLD_KEY_BUY_NUM          = 2,      --当天购买次数
    INSTANCE_GOLD_KEY_PASS_INFO        = 3,      --通关信息 {id=star, ... }
    INSTANCE_GOLD_KEY_ENTER_NUM        = 4,      --已进入次数
    INSTANCE_GOLD_KEY_LAST_ENTER_TIME   = 5,     -- 上次进入时间

    --离线1v1
    OFFLINE_PVP_KEY_RANK              = 1,
    OFFLINE_PVP_KEY_DBID              = 2, --0为机器人，具体显示找配置。--大于0为具体玩家，使用这里的宏得到需要的数据
    OFFLINE_PVP_KEY_INSPIRE_COUNT     = 3, --鼓舞的次数，加5%的战斗力
    OFFLINE_PVP_KEY_COMBAT_VALUE      = 4,
    OFFLINE_PVP_KEY_NAME              = 5,
    OFFLINE_PVP_KEY_FACADE            = 6,
    OFFLINE_PVP_KEY_ORIGNAL_RANK      = 7, --机器人的原始排名，读表。rank = OFFLINE_PVP_KEY_ORIGNAL_RANK or OFFLINE_PVP_KEY_RANK
    OFFLINE_PVP_KEY_VOCATION          = 8,

    OFFLINE_PVP_INFO_KEY_RANK         = 1, --排名
    OFFLINE_PVP_INFO_KEY_INSPIRE      = 2, --鼓舞次数
    OFFLINE_PVP_INFO_KEY_SETTLE       = 3, --0为无奖可领或者已领奖,1为可领奖
    OFFLINE_PVP_INFO_KEY_RANK_22      = 4, --22点那时候的排名

    --护送女神结算key
    GUARD_GODDESS_KEY_ID      = 1, --
    GUARD_GODDESS_IS_DOUBLE   = 2, --0:no,1:yes
    GUARD_GODDESS_KEY_ITEMS   = 3, --道具 {k:v}

    --在线跨天，跨天登陆
    RESET_DAY_REASON_ONLINE       = 1, --在线跨天
    RESET_DAY_REASON_ANOTHER_DAY  = 2, --跨天登陆

    --复活原因
    REVIVE_REASON_1 = 1,
    REVIVE_REASON_2 = 2,
    REVIVE_REASON_3 = 3,
    REVIVE_REASON_4 = 4,
    REVIVE_REASON_5 = 5,
    REVIVE_REASON_6 = 6,
    REVIVE_REASON_7 = 7,
    REVIVE_REASON_8 = 8,

    --装备副本key
    INSTANCE_EQUIPMENT_KEY_DAY_PASS_NUM     = 1, --当日通关入次数
    INSTANCE_EQUIPMENT_KEY_DAY_ASSIST_NUM   = 2, --当日助战次数
    INSTANCE_EQUIPMENT_KEY_LAST_PASS_TIME   = 3, --上一次通关时间
    INSTANCE_EQUIPMENT_KEY_PASS_INFO        = 4, --难度对应星级｛id=star｝

    --复活类型
    REVIVE_TYPE_BORN_POINT   = 1, --复活点复活
    REVIVE_TYPE_CUR_POINT    = 2, --原地复活    --要扣钱

    --蛮荒禁地key
    FORBIDDEN_KEY_INTEREST          = 1, --关注列表
    FORBIDDEN_KEY_ENTER_TIMES       = 2, --当天进入次数
    FORBIDDEN_KEY_CAN_ENTER_TIMES   = 3, --可进入次数

    --图鉴
    --CARD_PIECE_ID                   = 1, --图鉴碎片ID

    --翅膀法宝神兵披风 化形key
    TREASURE_ILLUSION_KEY_ACTIVATE_INFO = 1, --激活的化形{id=star,...}
    TREASURE_ILLUSION_KEY_LEVEL         = 2, --等级
    TREASURE_ILLUSION_KEY_EXP           = 3, --剩余经验

    --宠物化形key
    PET_ILLUSION_KEY_GRADE      = 1,
    PET_ILLUSION_KEY_BLESS      = 2,
    PET_ILLUSION_KEY_SKILLS     = 3,

    --坐骑化形key
    HORSE_ILLUSION_KEY_GRADE      = 1,
    HORSE_ILLUSION_KEY_BLESS      = 2,
    HORSE_ILLUSION_KEY_SKILLS     = 3,

    --翅膀法宝神兵披风宠物坐骑外形来源系统
    TREASURE_SHOW_ORIGIN_COMMON       = 1, --普通
    TREASURE_SHOW_ORIGIN_ILLUSION     = 2, --化形
    TREASURE_SHOW_ORIGIN_MARRY        = 3, --结婚

    TREASURE_SHOW_ORIGIN_MAX          = 2,

    TREASURE_ILLUSION_SHOW_SYSTEM   = 1,
    TREASURE_ILLUSION_SHOW_ID       = 2,

    --公会神兽 room key
    GUILD_BEAST_ROOM_KEY_STAGE                  = 1,
    GUILD_BEAST_ROOM_KEY_STAGE_START_TIME       = 2,   --阶段开始时间
    GUILD_BEAST_ROOM_KEY_SETTLE_FLAG            = 3,
    GUILD_BEAST_ROOM_KEY_BOSS_MONSTER_ID        = 4,
    GUILD_BEAST_ROOM_KEY_PLAYER_REWARD_COUNT    = 5,   --{eid=count,...}
    GUILD_BEAST_ROOM_KEY_MGR_MBSTR              = 6,
    GUILD_BEAST_ROOM_KEY_OPEN_TIME              = 7,   --开启时间
    GUILD_BEAST_ROOM_KEY_GUILD_DBID             = 8,
    GUILD_BEAST_ROOM_KEY_BOSS_MAX_HP            = 9,
    GUILD_BEAST_ROOM_KEY_BOSS_CUR_HP            = 10,

    --跨服实时1v1
    --如果改了key,记得改这里的范围!!!!!!!!!!!!
    CROSS_VS11_KEY_DATA_MIN_DAY_KEY     = 1,
    CROSS_VS11_KEY_DATA_MAX_DAY_KEY     = 7,
    CROSS_VS11_KEY_DATA_MIN_MONTH_KEY   = 11,
    CROSS_VS11_KEY_DATA_MAX_MONTH_KEY   = 15,

    CROSS_VS11_KEY_DATA_COUNT = 1,     --今日参与次数    --1-10是今日数据
    CROSS_VS11_KEY_DATA_POINT = 2,     --今日胜点
    CROSS_VS11_KEY_DATA_EXP   = 3,     --今日获得经验
    CROSS_VS11_KEY_DATA_BUY_CROSS_COUNT = 4,   --今日购买的跨服额外次数
    CROSS_VS11_KEY_DATA_EXPLOIT         = 5,        --今日功勋
    CROSS_VS11_KEY_DATA_DAY_STAGE_RWD   = 6,        --每日段位奖励领取标记
    CROSS_VS11_KEY_DATA_DAY_COUNT_RWD   = 7,        --每日参与次数奖励领取标记
    CROSS_VS11_KEY_DATA_TOTAL_WIN       = 11,       --本赛季总获胜场次  --11-20是赛季数据
    CROSS_VS11_KEY_DATA_TOTAL_COUNT     = 12,       --本赛季总参加场次
    CROSS_VS11_KEY_DATA_TOTAL_POINT     = 13,       --本赛季总胜点
    CROSS_VS11_KEY_DATA_TOTAL_EXPLOIT   = 14,       --本赛季总功勋
    CROSS_VS11_KEY_DATA_TOTAL_GX_RWD    = 15,       --本赛季功勋奖励领取标记
    CROSS_VS11_KEY_DATA_YESTERDAY_SERVER_ID = 94,   --昨日server_id
    --CROSS_VS11_KEY_DATA_SEASON_STAGE_RWD  = 95     --上赛季段位奖励领取标记
    CROSS_VS11_KEY_DATA_SERVER_ID         = 96,     --0表示参加的跨服匹配,非0表示本服匹配
    CROSS_VS11_KEY_DATA_SEASON_BEGIN_TIME = 97,     --赛季开始时间
    CROSS_VS11_KEY_DATA_YESTERDAY_POINT   = 98,     --昨日总胜点,用来领取奖励
    CROSS_VS11_KEY_DATA_STAGE             = 99,     --当前段位,由total_point计算出来的


    --青云之巅key cloud_peak_info
    CROSS_CLOUD_PEAK_KEY_LAST_ENTER_TIME = 1, --上次进入的时间
    CROSS_CLOUD_PEAK_KEY_DATA_SERVER_ID  = 2,  --0表示跨服,非0表示本服

    --青云之巅 room key
    CLOUD_PEAK_ROOM_KEY_PLAYERS         = 1,
    CLOUD_PEAK_ROOM_KEY_MGR_MBSTR       = 2,
    CLOUD_PEAK_ROOM_KEY_START_TIME      = 3, --开始时间
    CLOUD_PEAK_ROOM_KEY_SERVER_ID       = 4,
    CLOUD_PEAK_ROOM_KEY_LAYER           = 5,
    CLOUD_PEAK_ROOM_KEY_DURATION        = 6, --持续时间
    CLOUD_PEAK_ROOM_KEY_NEED_KILL_NUM   = 7, --这层通关需要击杀数


    --篝火地图
    GUILD_BONFIRE_MAP_LINE       = 1,    --line
    GUILD_BONFIRE_BASE_MB_STR    = 2,    --base mb str
    GUILD_BONFIRE_CELL_MB_STR    = 3,    --cell mb str
    GUILD_BONFIRE_LOADING_MAP    = 4,    --是否在申请地图中

    --守卫公会 guild_guard_info
    GUILD_GUARD_LAST_ENTER_TIME = 1, --上次进入的时间
    GUILD_GUARD_TMP_INFO        = 2, --临时数据
    GUILD_GUARD_LAST_EXP        = 3, --上次获得经验
    GUILD_GUARD_FIRST_DATE      = 4, --第一天玩的时间

    BONFIRE_STATE_NONE             = 0,
    BONFIRE_STATE_PREPARE          = 1,
    BONFIRE_STATE_ANSWER           = 2,

    --采集类型
    COLLECT_TYPE_CRYSTAL_SMALL          = 1, --神兽岛小水晶
    COLLECT_TYPE_CRYSTAL_BIG            = 2, --神兽岛大水晶
    COLLECT_TYPE_GUILD_BEAST            = 3, --公会神兽宝箱
    COLLECT_TYPE_MARRY                  = 4, --婚礼
    COLLECT_TYPE_BATTLE_SOUL            = 5, --战场之魂宝箱
    COLLECT_TYPE_SKY_DROP               = 6, --天降秘宝

    QUESTION_STATE_NONE               = 0,
    QUESTION_STATE_PREPARE            = 1,
    QUESTION_STATE_ANSWER             = 2,  --真正回答开始
    QUESTION_STATE_FINISH_ANSWER      = 3,  --回答结束，但活动时间还没结束，需要等待看排行，然后再全部拉出场景

    QUESTION_PREPARE_TIME            = 40, --等待开始 --和等待结束时间一致
    QUESTION_CHOOSE_ANSWER_TIME      = 20, --每道题的时间

    MAX_QUESTION_COUNT               = 20, --20*20=400就是答题时间，活动表配置的时间-400-40就是剩余时间=40

    --神兽岛 key
    GOD_ISLAND_KEY_LAST_RESET_TIME  = 1, 
    GOD_ISLAND_KEY_INTEREST_LIST    = 2, --跨服关注列表
    GOD_ISLAND_KEY_PILAO            = 3,
    GOD_ISLAND_KEY_KILL_NUM         = 4,
    GOD_ISLAND_KEY_LAST_KILL_TIME   = 5,
    GOD_ISLAND_KEY_INTEREST_LIST_LOCAL = 6, --本服关注列表

    --塔防副本 tower_defense_info key
    TOWER_DEFENSE_KEY_ENTER_NUM         = 1,    --进入次数
    TOWER_DEFENSE_KEY_BUY_NUM           = 2,    --购买次数
    TOWER_DEFENSE_KEY_LAST_ENTER_TIME   = 3,    --上次玩该玩法的时间
    TOWER_DEFENSE_KEY_TMP_INFO          = 4,


    --公会争霸列表key
    GUILD_CONQUEST_KEY_NAME       = 1,   --公会名字
    GUILD_CONQUEST_KEY_RESULT     = 2,  --对战结果
    GUILD_CONQUEST_KEY_DBID       = 3,  --公会dbid
    GUILD_CONQUEST_KEY_RESULT_1   = 4,  --第一轮的对战结果
    GUILD_CONQUEST_KEY_RESULT_2   = 5,  --第二轮的对战结果

    --公会争霸对战结果
    GUILD_CONQUEST_RESULT_NONE  = 0,   --未开始
    GUILD_CONQUEST_RESULT_FAIL  = -1,  --失败
    GUILD_CONQUEST_RESULT_WIN   = 1,   --胜利

    --争霸地图
    GUILD_CONQUEST_MAP_LINE       = 1,    --line
    GUILD_CONQUEST_BASE_MB_STR    = 2,    --base mb str
    GUILD_CONQUEST_CELL_MB_STR    = 3,    --cell mb str
    GUILD_CONQUEST_LOADING_MAP    = 4,    --是否在申请地图中

    --神兽系统
    --神兽装备类型，同时也是已穿装备背包的槽位id
    --1		2	  3	    4	 5
    --角	   牙    爪    甲	  眼
    GOD_MONSTER_LOADED_EQUIP_INDEX_MIN     = 1,
    GOD_MONSTER_LOADED_EQUIP_INDEX_MAX     = 5,
    GOD_MONSTER_CRYSTAL                    = 6, --6是兽晶

    --龙魂
    DRAGON_SPIRIT_ID            = 1,
    DRAGON_SPIRIT_LEVEL         = 2,

    --添加好友度方式
    ADD_FRIENDLY_BY_GM              = 1,
    ADD_FRIENDLY_BY_SEND_FLOWER     = 2, --送花
    ADD_FRIENDLY_BY_MARRY_INSTANCE  = 3,
    ADD_FRIENDLY_BY_EQUIPMENT_INSTANCE = 4,
    ADD_FRIENDLY_BY_EXP_INSTANCE    = 5,
    ADD_FRIENDLY_BY_BOSS_HOME       = 6,

    --结婚存盘 --兼容旧的已经存盘的key
    MARRY_LIST_KEY_DBID1         = 2,  --dbid1
    MARRY_LIST_KEY_DBID2         = 4,  --dbid2
    MARRY_LIST_KEY_WEDDING_COUNT = 6,  --婚礼次数
    MARRY_LIST_KEY_GRADE         = 7,  --婚礼档次
    MARRY_LIST_KEY_TIME          = 8,  --结婚时间

    --结婚玩家内存数据
    MARRY_USER_KEY_REC_ID           = 1,
    MARRY_USER_KEY_MAILBOX          = 2, --这个直接就是mb,而不是mbstr。user_info_key中却是mbstr
    MARRY_USER_KEY_DBID             = 3,
    MARRY_USER_KEY_NAME             = 4,
    MARRY_USER_KEY_LEVEL            = 5,
    MARRY_USER_KEY_MATE_DBID        = 6,
    MARRY_USER_KEY_MATE_NAME        = 7,
    MARRY_USER_KEY_MARRY_REQ_STATE  = 8,
    MARRY_USER_KEY_MARRY_REQ_OTHER  = 9,
    MARRY_USER_KEY_MARRY_REQ_TIME   = 10,
    MARRY_USER_KEY_MARRY_REQ_GRADE  = 11,
    MARRY_USER_KEY_VOCATION         = 12,
    MARRY_USER_KEY_SEQ              = 13, --1自己是求婚者。2自己是被求婚者

    --预约存盘
    BOOK_KEY_DBID1            = 1,    --结婚者A
    BOOK_KEY_DBID2            = 2,    --结婚者B
    BOOK_KEY_INVITED          = 3,    --已邀请宾客列表{dbid1=1,dbid2=2}后端还是字典，但是有个下标  --给到客户端为是列表{[下标]={dbid1,name},[下标]={dbid2,name}}
    BOOK_KEY_BUY_INVITE_COUNT = 4,    --已购买的邀请的。预约的时候直接把grade的次数也给到这里了。
    --不存盘，但收集给前端
    BOOK_KEY_NAME1            = 10,    --结婚者A
    BOOK_KEY_NAME2            = 11,    --结婚者B
    BOOK_KEY_VOCATION1        = 12,    --结婚者A
    BOOK_KEY_VOCATION2        = 13,    --结婚者B

    --结婚状态
    --MARRY_STATE_FREE = 0,   --自由
    MARRY_STATE_REQ    = 1,   --已请求
    MARRY_STATE_REQED  = 2,   --已被人求婚中
    --MARRY_STATE_DONE = 3,   --已完成

    --结婚地图
    MARRY_MAP_LINE        = 1,    --line
    MARRY_BASE_MB_STR     = 2,    --base mb str
    MARRY_CELL_MB_STR     = 3,    --cell mb str
    MARRY_LOADING_MAP     = 4,    --是否在申请地图中
    MARRY_HAS_CEREMONY    = 5,    --是否已经拜堂
    MARRY_HOT_DEGREE      = 6,    --热度
    MARRY_MAP_DBID1       = 7,    --dbid1
    MARRY_MAP_DBID2       = 8,    --dbid2
    MARRY_HOT_DEGREE_DICT = 9,    --热度,是否已经刷新了礼物
    MARRY_BLESS_RECORD    = 10,   --祝福记录

    MAX_MARRY_BLESS_LOG           = 50,
    
    --婚礼修改为40分钟，测试
    MARRY_STAGE_4_WAIT_TIME       = 900, --2400 900 秒 --INSTANCE_STAGE_4_WAIT_TIME
     
    --其他玩家查看信息的key
    PEEP_KEY_COMBAT_ATTRI       = 1,  --战斗属性
    PEEP_KEY_EQUIP              = 2,  --已穿装备
    PEEP_KEY_COMBAT_VALUE       = 3,  --战斗力
    PEEP_KEY_NAME               = 4,  --角色名
    PEEP_KEY_LEVEL              = 5,  --等级
    PEEP_KEY_GENDER             = 6,  --性别
    PEEP_KEY_EXP                = 7,  --经验
    PEEP_KEY_VIP                = 8,  --vip
    PEEP_KEY_PK                 = 9,  --PK值

    PEEP_KEY_SUIT               = 10,   --套装装备
    PEEP_KEY_GEM                = 11,   --装备镶嵌
    PEEP_KEY_WASH               = 12,   --装备洗练
    PEEP_KEY_STREN              = 13,   --装备强化
    PEEP_KEY_FACADE             = 14,   --外观
    PEEP_KEY_GUILD              = 15,   --公会
    PEEP_KEY_VOCATION           = 16,   --职业
    PEEP_KEY_MATE_NAME          = 17,   --配偶名字

    --PEEP_KEY_FATE               = 4,  --命格
    --PEEP_KEY_4GODS              = 5,  --四神
    --PEEP_KEY_XL                 = 8,  --境界
    --PEEP_KEY_TITLE              = 9,  --称号

    --主宰神殿get_info key
    OVERLORD_KEY_GUILD_NAME                 = 1, --公会名
    OVERLORD_KEY_GUILD_WIN_NUM              = 2, --连胜次数
    OVERLORD_KEY_GUILD_DBID                 = 3, --公会dbid
    OVERLORD_KEY_GUILD_PRESIDENT_DBID       = 4, --会长dbid
    OVERLORD_KEY_GUILD_PRESIDENT_NAME       = 5, --会长名字
    OVERLORD_KEY_GUILD_PRESIDENT_NOBILITY   = 6, --会长爵位
    OVERLORD_KEY_GUILD_PRESIDENT_FACADE     = 7, --会长外观
    OVERLORD_KEY_GUILD_PRESIDENT_VOCATION   = 8, --会长职业

    --开服冲榜类型
    OPEN_SERVER_RANK_LEVEL            = 1,
    OPEN_SERVER_RANK_HORSE            = 2,
    OPEN_SERVER_RANK_PET              = 3,
    OPEN_SERVER_RANK_CHARGE           = 4,
    OPEN_SERVER_RANK_GEM              = 5,
    OPEN_SERVER_RANK_COMBAT_VALUE     = 6,

    OPEN_SERVER_RANK_REWARD_GRADE_MIN   = 1,
    OPEN_SERVER_RANK_REWARD_GRADE_MAX   = 6,

    --欢乐云盘信息
    ROULETTE_LUCK_AVATAR_NAME   = 1, --玩家名
    ROULETTE_LUCK_AVATAR_DBID   = 2, --玩家dbid
    ROULETTE_LUCK_AVATAR_VOC    = 3, --玩家职业组
    ROULETTE_TIME               = 4, --时间
    ROULETTE_REWARD             = 5, --奖品

    --仙娃信息
    MARRY_PET_NAME      = 1, --名字
    MARRY_PET_GRADE     = 2, --阶级
    MARRY_PET_EXP       = 3, --当前经验

    --送花类型
    SEND_FLOWER_WAY_HIDE    = 1, --匿名送花
    SEND_FLOWER_WAY_NAME    = 2, --实名送花
    SEND_FLOWER_WAY_MARRY   = 3, --结婚送花

    --送花系统需要发公告的花道具id（这里的值是对应的item_id）
    SEND_FLOWER_ITEM_SYS_NN = 121, --99多玫瑰
    SEND_FLOWER_ITEM_SYS_NNN= 120, --999朵玫瑰

    --集字有礼兑换次数限制类型
    COLLECT_WORD_LIMIT_PERSONAL = 1, --个人
    COLLECT_WORD_LIMIT_SERVER   = 2, --全服 

    --掉落道具时间限制类型
    DROP_TIME_LIMIT_OPEN_SEVER  = 1, --开服时间
    DROP_TIME_LIMIT_FIXED       = 2, --固定时间
    DROP_TIME_LIMIT_ACTIVITY    = 3, --渠道活动限制
    DROP_TIME_LIMIT_COUNT       = 4, --数量限制（每天）

    --获得后需要发公告的特殊装备
    SYS_ITEM_ID_CUPID         = 7961, --丘比特
    SYS_ITEM_ID_PANDA         = 7962, --熊猫憨憨
    SYS_ITEM_ID_HALLOWEEN_IMP = 7963, --万圣小鬼
    SYS_ITEM_ID_SKULL_KING    = 7964, --骷髅王


    --收集日志
    AOFEI_LOG_ACTIVATION             = 1,  --激活  --客户端  --打开游戏就调用，此时还没有选择服务器
    AOFEI_LOG_UPDATE                 = 2,  --更新   --客户端 --更新游戏就调用，此时还没有选择服务器
    AOFEI_LOG_CLIENT_BEHAVIOR        = 3,  --客户端操作日志  --客户端  --里面有各种操作,头像操作，兑换码操作，组队操作，商店操作 
    --
    AOFEI_LOG_ACCOUNT_CREATE         = 11, --创账号
    AOFEI_LOG_AVATAR_CREATE          = 12, --创角
    AOFEI_LOG_LOGIN                  = 13, --登陆
    AOFEI_LOG_LOGOUT                 = 14, --登出
    AOFEI_LOG_ONLINE_COUNT           = 15, --在线人数，5分钟发一次，按渠道写
    AOFEI_LOG_CHARGE                 = 16, --充值(每一笔),每个玩家累计充值
    AOFEI_LOG_COST_MONEY             = 17, --消耗所有金钱，包括钻石绑钻(itemid 50以下)(cost_item_id,cost_count,way)
    AOFEI_LOG_COST_MONEY_COUPONS     = 18, --消耗钻石绑钻，特别拿出来(cost_item_id,cost_count,way,cost_coupons_count,cost_coupons_bind_count,add_item_id,add_count) --假设消耗绑钻，钻石可代替绑钻，那么,cost_coupons_count,cost_coupons_bind_count为真是消耗的钻石和绑钻
    AOFEI_LOG_COST_ITEM              = 19, --消耗道具((itemid 50以上)(cost_item_id,cost_count,way)
    AOFEI_LOG_ADD_MONEY              = 20, --获得所有钱(itemid 50以下)(add_item_id,add_count,way)
    AOFEI_LOG_ADD_ITEM               = 21, --获得道具(itemid 50以上)(add_item_id,add_count,way)
    --
    AOFEI_LOG_ENTER_MAP              = 22, --进入地图(map_id,map_type)
    AOFEI_LOG_COMMIT_TASK            = 23, --提交任务(task_id,task_type)
    AOFEI_LOG_DAILY_ACTIVITY         = 24, --参与日常活动(activity_id)
    AOFEI_LOG_CHAT_NEW               = 25, --内部聊天日志(chat_id, text, to_dbid, guild_name)
    AOFEI_LOG_KILL                   = 26, --被击杀 杀boss
    AOFEI_LOG_UP_LEVEL               = 27, --升级(level)
    AOFEI_LOG_AUCTION                = 28, --拍卖行(auction_state, item_id, item_count, price)
    --
    AOFEI_LOG_CHAT                   = 41, --聊天监控，特殊格式。已被占用
    
    AOFEI_LOG_KILL_TYPE_BE_KILLED   = 1, --被击杀
    AOFEI_LOG_KILL_TYPE_KILL        = 2, --击杀

    AOFEI_LOG_AUCTION_STATE_ON_SHELF  = 1, --上架成功
    AOFEI_LOG_AUCTION_STATE_OFF_SHELF = 2, --下架成功
    AOFEI_LOG_AUCTION_STATE_SOLD_OUT  = 3, --出售成功

    --公告条件
    SYS_NEED_GEM_LEVEL              = 6,    --需要宝石等级
    SYS_NEED_EQUIP_STREN_LEVEL      = 10,   --需要装备强化等级
    SYS_NEED_MARRY_PET_GRADE        = 5,    --需要宝宝阶级
    SYS_NEED_EQUIP_WASH_COLOR_P     = 4,    --需要紫色洗练属性条数
    SYS_NEED_PET_GRADE              = 5,    --需要宠物阶级
    SYS_NEED_HORSE_GRADE            = 5,    --需要坐骑阶级

    --安全锁
    SECURITY_LOCK_FLAG = 1,
    SECURITY_LOCK_PWD = 2,
    SECURITY_QUESTION_ID = 3,
    SECURITY_ANSWER = 4,

    --星脉
    ZODIAC_RANK = 1,
    ZODIAC_LEVEL = 2,

    --翻牌集福
    OPEN_CARD_STEP = 1,         --已经翻了多少张牌
    CHAR_OBTAINED_COUNT = 2,    --已经翻到多少福卡


    LV_INVEST_TITLE_ITEM_ID = 2200, --等级投资奖励称号的道具id

    --工会红包条件
    RED_CONDITION_DAILY_CHARGE_1        = 300,
    RED_CONDITION_DAILY_CHARGE_2        = 680,
    RED_CONDITION_LV_INVEST_1           = 680,
    RED_CONDITION_LV_INVEST_2           = 1280,
    RED_CONDITION_LV_INVEST_3           = 1880,

    --工会红包ID
    RED_ID_DAILY_CHARGE_1       = 3,
    RED_ID_DAILY_CHARGE_2       = 9,
    RED_ID_LV_INVEST_1          = 5,
    RED_ID_LV_INVEST_2          = 10,
    RED_ID_LV_INVEST_3          = 11,
    RED_ID_VIP_INVEST           = 12,

    --宝匣系统
    MARRY_BOX_FLAG = 1,
    MARRY_BOX_DAILY_REFUND_STEP = 2,
    MARRY_BOX_DAILY_REFUND_FLAG = 3,
    MARRY_BOX_REFUND_FLAG = 4,

    --升级限时销售
    LEVEL_UP_SALE_STEP = 1,
    LEVEL_UP_SALE_TIME_STAMP = 2,
    LEVEL_UP_SALE_NORMAL = 3,
    LEVEL_UP_SALE_SUPREME = 4,
    LEVEL_UP_SALE_PRESENT_IS_EXPIRED = 5,

    --守护装备item_id
    EQUIP_GUARD_ID_1    = 7961, --丘比特
    EQUIP_GUARD_ID_2    = 7962, --熊猫憨憨
    EQUIP_GUARD_ID_3    = 7963, --万圣小鬼
    EQUIP_GUARD_ID_4    = 7964, --骷髅王

    --全民鉴宝info
    JIANBAO_LOOP_FLAG   = 1, --循环flag
    JIANBAO_LUCK        = 2, --幸运值
    JIANBAO_ROUND       = 3, --当前轮数
    
    --这里返回的错误码从-100开始,以免和cgi返回的错误码混淆
    USER_MGR_WEB_REQ_ERR_DBID    = -101,
    USER_MGR_WEB_REQ_ERR_PARAMS  = -103,
    USER_MGR_WEB_REQ_ERR_BFORDER  = -104, --没找到
    --
    USER_MGR_WEB_REQ_ERR_NO_USE  = -298,
    USER_MGR_WEB_REQ_ERR_UNKNOW  = -299,


    --渠道活动时间的key
    CHANNEL_ACTIVITY_KEY_BEGIN_TIME      = 1,
    CHANNEL_ACTIVITY_KEY_END_TIME        = 2,
    CHANNEL_ACTIVITY_KEY_OPEN_SERVER_DAY = 3,
    CHANNEL_ACTIVITY_KEY_HAS_CALL_END    = 10,
    CHANNEL_ACTIVITY_KEY_HAS_CALL_BEGIN  = 11,
    
    --hi点活动id
    HI_KILL             = 1, --击杀BOSS（包括世界、个人和boss之家）
    HI_RUNE             = 2, --符文寻宝
    HI_EQUIP            = 3, --装备寻宝
    HI_BLESS            = 4, --祈福
    HI_DAILY_TASK       = 5, --赏金任务
    HI_EXP_INST         = 6, --经验副本
    HI_GUARD_TOWER      = 7, --守护结界塔
    HI_EQUIP_INST       = 8, --装备副本
    HI_SOLO             = 9, --天梯赛
    HI_BONEFIRE         = 10, --公会篝火 
    HI_FIREWORK         = 11, --购买庆典烟花
    HI_CLOUD_PEAK       = 12, --参加一次天堂之巅
    HI_DARK_PLACE       = 13, --进入一次黑暗禁地
    HI_DRANGO           = 14, --进入一次龙魂副本
    HI_CHARGE60         = 15, --累计充值60钻石
    HI_CHARGE680        = 16, --累计充值680钻石
    HI_CHARGE1280       = 17, --累计充值1280钻石
    HI_CHARGE3280       = 18, --累计充值3280钻石

    --深渊祭坛
    ALTAR_TIMES         = 1,    --今天参与深渊祭坛的次数
    ALTAR_LAYER         = 2,    --玩家最高评级通过的最高层


    --支线世界boss
    FAKE_WORLD_BOSS_MAP_ID = 50021,

    --天降秘宝info
    SKY_DROP_TIME           = 1,
    SKY_DROP_COLLECT_TIMES  = 2,

    --加成info
    EQUIP_STRENGTH_TOTAL    = 1, --强化
    EQUIP_STAR_TOTAL        = 2, --星级
    GEM_LEVEL_TOTAL         = 3, --宝石

    --特殊道具 ServerSpaceDrop eff_id
    SPECIAL_ITEM_EFF_ID_ANCIENT_BATTLE = 1, --上古战场己方boss加buff

    --上古战场key ancient_battle_info
    CROSS_ANCIENT_BATTLE_KEY_LAST_ENTER_TIME = 1, --上次进入的时间
    CROSS_ANCIENT_BATTLE_KEY_DATA_SERVER_ID  = 2,  --0表示跨服,非0表示本服


    OPERATION_LOG_1     = 1,  --与服务器版本不匹配！
    OPERATION_LOG_2     = 2,  --PlayerAccount创建成功
    OPERATION_LOG_3     = 3,  --请求创建角色
    OPERATION_LOG_4     = 4,  --请求选择角色登入
    OPERATION_LOG_5     = 5,  --请求创建角色成功
    OPERATION_LOG_6     = 6,  --请求创建角色回调返回：
    OPERATION_LOG_7     = 7,  --返回已有角色列表
    OPERATION_LOG_8     = 8,  --请求选择角色成功
    OPERATION_LOG_9     = 9,  --请求选择角色回调返回：
    OPERATION_LOG_10    = 10,  --PlayerAvatar创建成功
    OPERATION_LOG_11    = 11,  --登录游戏成功
    OPERATION_LOG_12    = 12,  --登录游戏失败，错误码
    OPERATION_LOG_13    = 13,  --登录游戏失败2，错误码
    OPERATION_LOG_14    = 14,  --启动游戏
    OPERATION_LOG_15    = 15,  --点击创角按钮
    OPERATION_LOG_16    = 16,  --播放创角展示动作结束
    OPERATION_LOG_17    = 17,  --开始播放创角展示动作
    OPERATION_LOG_18    = 18,  --创角面板点击进入游戏按钮后等待超时
    OPERATION_LOG_19    = 19,  --打开创角面板
    OPERATION_LOG_20    = 20,  --关闭创角面板
    OPERATION_LOG_21    = 21,  --关闭公告面板
    OPERATION_LOG_22    = 22,  --关闭公告面板2
    OPERATION_LOG_23    = 23,  --关闭选服面板
    OPERATION_LOG_24    = 24,  --打开选角面板
    OPERATION_LOG_25    = 25,  --关闭选角面板
    OPERATION_LOG_26    = 26,  --点击进入游戏按钮
    OPERATION_LOG_27    = 27,  --选角面板点击进入游戏按钮后等待超时
    OPERATION_LOG_28    = 28,  --打开登录界面
    OPERATION_LOG_29    = 29,  --点击登录，服务器未开放
    OPERATION_LOG_30    = 30,  --尝试登录SDK，开始检查SDK登录状态
    OPERATION_LOG_31    = 31,  --SDK初始化状态成功
    OPERATION_LOG_32    = 32,  --SDK初始化状态失败
    OPERATION_LOG_33    = 33,  --点击登录按钮
    OPERATION_LOG_34    = 34,  --服务器列表为空
    OPERATION_LOG_35    = 35,  --打开公告面板
    OPERATION_LOG_36    = 36,  --登录游戏成功1
    OPERATION_LOG_37    = 37,  --登录游戏失败3，错误码
    OPERATION_LOG_38    = 38,  --打开选服面板
    OPERATION_LOG_39    = 39,  --SDK登录成功
    OPERATION_LOG_40    = 40,  --SDK登录返回错误码：
    OPERATION_LOG_41    = 41,  --选服后开始登录游戏
}

return cfg