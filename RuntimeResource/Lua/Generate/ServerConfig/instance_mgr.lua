--local engine = require "engine"
local lua_util = require "lua_util"
--local putil = require "public_util"
--local global_params = require "global_params"
--local public_config = require "public_config"

local _readXml = lua_util._readXml
--local log_d = putil.log_d
--local log_w = putil.log_w
--local prob = lua_util.prob
--local choice = lua_util.choice
--local choose_prob = lua_util.choose_prob

--local tonumber = tonumber
local pairs = pairs
--local os = os
--local unpack = unpack
--local table = table
local string = string
--local math = math
--local next = next

--local table_insert = table.insert
--local table_sort = table.sort
local string_format = string.format

--------------------------------------------------------------
local instance_mgr = {}

function instance_mgr:init_data()
    self.exp_monster = _readXml("/data/xml/exp_monster.xml", "id_i")
end

function instance_mgr:get_exp_instance_monster_cfgs(level, wave)
    for _, data in pairs(self.exp_monster) do
        if data.level == level then
            return data[string_format("monster_id_%d", wave)], data[string_format("atrri_%d", wave)], data[string_format("reward_%d", wave)]
        end
    end
end

return instance_mgr

