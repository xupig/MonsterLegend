local lua_util = require "lua_util"

local readXml = lua_util._readXml

local pairs = pairs

local altar_data_mgr = {}

function altar_data_mgr:init_data()

    self.despair_altar = readXml("/data/xml/despair_altar.xml", "id_i")

    self.map_to_altar_layer = {}

    for layer, data in pairs(self.despair_altar) do
        self.map_to_altar_layer[data.map] = layer
    end
end

function altar_data_mgr:get_layer_by_map_id(map_id)
    return self.map_to_altar_layer[map_id]
end

function altar_data_mgr:get_despair_altar_by_layer(layer)
    return self.despair_altar[layer]
end


return altar_data_mgr