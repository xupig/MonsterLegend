local public_util = {}

local table = table
local engine      = require "engine"
local os = os
local math = math
local pcall = pcall
local ipairs = ipairs
local pairs = pairs
local type = type

local string_format = string.format
local log_xxx = engine.log_xxx


--debug日志
function public_util.log_d(s1, s2, ...)
    log_xxx(0, s1, string_format(s2, ...))
end

--info日志
function public_util.log_i(s1, s2, ...)
    log_xxx(1, s1, string_format(s2, ...))
end

--warning log
function public_util.log_w(s1, s2, ...)
    log_xxx(2, s1, string_format(s2, ...))
end

--error log
function public_util.log_e(s1, s2, ...)
    log_xxx(3, s1, string_format(s2, ...))
end

local _log_d = public_util.log_d
local _log_i = public_util.log_i
local _log_w = public_util.log_w
local _log_e = public_util.log_e

function public_util.add_package_path(dir, subdir)
    local old_path = package.path
    local s = string_format("%s;%s/%s/?.lua;%s/%s/?.luac", old_path,
                dir, subdir, dir, subdir)
    package.path = s
    _log_i("add_package_path.begin", old_path)
    _log_i("add_package_path.end", s)
end

function public_util.add_some_package_path(dir, subdirs)
    _log_i("add_some_package_path.begin", package.path)
    local all_path = {}
    table.insert(all_path, package.path)
    for _, subdir in ipairs(subdirs) do
        table.insert(all_path, string_format("%s/%s/?.lua", dir, subdir) )
        table.insert(all_path, string_format("%s/%s/?.luac", dir, subdir) )
    end
    local new_path = table.concat(all_path, ";")
    package.path = new_path
    _log_i("add_some_package_path.end", new_path)
end

--不用这个，lua_util里面有一个pcall
--function public_util.pcall(func, default_ret, ...)
--    local ret, err_msg = pcall(func, ...)
--    if ret then
--        return err_msg
--    else
--        _log_e("public_util.pcall err", "%s", err_msg)
--        return default_ret
--    end
--end

--脚本层启动后丢掉一些随机数
function public_util.init_random()
    local sd = os.time()
    math.randomseed(sd)
    local i = 1
    local test_count = sd % 10 + 10
    for i=1,test_count,1 do
        math.random(1,10)
    end
end

--通用的globalbase注册后回调方法
function public_util.globalbase_reg_cb(base_name, eid)
    local mm_eid = eid
    local function __callback(ret)
        local en = engine.getEntity(mm_eid)
        if en then
            if ret == 1 then
                --_log_d("public_util.globalbase_reg_cb", base_name)
                en:on_registered()
            else
                _log_w("Register-err", base_name)
                en:destroy()
            end
        end
    end
    return __callback
end

--globalbase第一次创建后的存盘回调方法
function public_util.globalbase_saved_cb(mgr_name1)
    local mgr_name = mgr_name1
    local function __callback(entity, dbid, err)
        if dbid > 0 then
            _log_i("create_"..mgr_name.."_success", '')
            entity:registerGlobally(mgr_name, public_util.globalbase_reg_cb(mgr_name, entity:getId()))
        else
            _log_i("create_"..mgr_name.."_failed", err)
        end
    end
    return __callback
end

--打印一个两层的table
local function print_t1(t, flag)
    for k,v in pairs(t) do
        if flag then
            print('\t', k, v)
        else
            print(k, v)
        end
    end
end

function public_util.print_t2(t)
    for k,v in pairs(t) do
        local vt = type(v)
        if vt == 'table' then
            print(k,'{')
            print_t1(v, true)
            print('}')
        else
            print (k,v)
        end
    end
end

function public_util.print_t3(t)
    for k,v in pairs(t) do
        if type(v)=='table' then
            print(k,'{{')
            public_util.print_t2(v)
            print('}}')
        else
            print(k,v)
        end
    end
end

return public_util

