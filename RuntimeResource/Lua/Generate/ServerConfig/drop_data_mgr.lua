--local engine = require "engine"
local lua_util = require "lua_util"
local putil = require "public_util"

--local string = string
local pairs = pairs
--local unpack = unpack
local next = next
local table = table
local math = math

--local math_floor = math.floor
--local math_random = math.floor
local table_insert = table.insert

--local log_i = putil.log_i
--local log_w = putil.log_w
local log_d = putil.log_d

local drop_data_mgr = {}

function drop_data_mgr:init_data()
    self.drop_xml_data = lua_util._readXml("/data/xml/drop.xml", "id_i")
    self.drop_xml_data[0] = nil
    
    local tmp_xml_data = lua_util._readXml("/data/xml/drop_package.xml", "id_i")
    tmp_xml_data[0] = nil
    local drop_package_xml_data = {}
    local drop_package_xml_data_weight = {}
    for _, v in pairs(tmp_xml_data) do
        local drop_package_id = v.drop_package_id
        if drop_package_id and drop_package_id > 0 then
            if not drop_package_xml_data[drop_package_id] then
                drop_package_xml_data[drop_package_id] = {}
                drop_package_xml_data_weight[drop_package_id] = {}
            end
            if v.weight then
                table_insert(drop_package_xml_data[drop_package_id],v)
                table_insert(drop_package_xml_data_weight[drop_package_id],v.weight)
            end
        end
    end
    self.drop_package_xml_data = drop_package_xml_data
    self.drop_package_xml_data_weight = drop_package_xml_data_weight
end

function drop_data_mgr:get_drop_xml_data(drop_id)
    return self.drop_xml_data[drop_id]
end

function drop_data_mgr:get_drop_package_item(package_id)
    --只返回一个道具id和数量，是这个包的道具按照权重抽取一个。
    local package_xml_data = self.drop_package_xml_data[package_id]
    local drop_package_xml_data_weight = self.drop_package_xml_data_weight[package_id]
    if package_xml_data and next(package_xml_data) then
        local index = lua_util.random_one_ex(drop_package_xml_data_weight)
        local v = package_xml_data[index]
        if v.item_id and v.min_number and v.max_number and v.item_id > 0 and v.min_number > 0 and v.max_number > 0 then
            return v.item_id,math.random(v.min_number,  v.max_number)
        end
    end
end

function drop_data_mgr:get_drop_items(drop_id)
    --log_d("drop_data_mgr:get_drop_items", "drop_id=%s",drop_id)

    local xml_data = self.drop_xml_data[drop_id]
    if not xml_data then
        return
    end
    
    if not xml_data.drop_package or not next(xml_data.drop_package) then
        return
    end
    
    local items_dict = {}
    local items_list = {}
    
    for package_id,v in pairs(xml_data.drop_package) do  --drop_package为_n结构
        --log_d("drop_data_mgr:get_drop_items    1 ", "package_id=%s;v=%s",package_id,engine.cPickle(v))
        if v[1] and v[2] and v[1] > 0 and v[2] > 0 then
            local weight = v[1] / 1000000
            local count = v[2]
            --log_d("drop_data_mgr:get_drop_items    2 ", "weight=%s;count=%s",weight,count)
            for i=1,count do
                if lua_util.prob(weight) then
                    local item_id,item_count = self:get_drop_package_item(package_id)
                    --log_d("drop_data_mgr:get_drop_items    3 ", "i=%s;item_id=%s;item_count=%s",i,item_id,item_count)
                    if item_id then
                        local item = {}
                        item[item_id] = item_count
                        table_insert(items_list,item)
                        items_dict[item_id] =  (items_dict[item_id] or 0) + item_count
                    end
                end
            end
        end
    end
    
    --log_d("drop_data_mgr:get_drop_items    6 ", "drop_id=%s;items_list=%s",drop_id,engine.cPickle(items_list))
    return items_dict,items_list
end

--离线挂机用的
function drop_data_mgr:get_hang_up_reward(drop_id,random_count)
    log_d("drop_data_mgr:get_hang_up_reward","drop_id=%d",drop_id)
    local xml_data = self.drop_xml_data[drop_id]
    if not xml_data then
        return
    end
    
    if not xml_data.drop_package or not next(xml_data.drop_package) then
        return
    end
    
    local items_dict = {}
    --for i = 1, random_count do
    for package_id,v in pairs(xml_data.drop_package) do  --drop_package为_n结构
        if v[1] and v[2] and v[1] > 0 and v[2] > 0 then
            local weight = v[1] / 1000000 --100
            local count = v[2] * random_count
            for i=1,count do
                if lua_util.prob(weight) then
                    local item_id,item_count = self:get_drop_package_item(package_id)
                    if item_id then
                        items_dict[item_id] =  (items_dict[item_id] or 0) + item_count
                    end
                end
            end
        end
    end
    --end
    --log_d("drop_data_mgr:get_drop_items    1 ", "drop_id=%s;items_list=%s",drop_id,engine.cPickle(items_list))
    return items_dict
end



return drop_data_mgr

