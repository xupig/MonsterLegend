---------------------------------------------------------
--检查玩家是否满足各种依赖条件

local error_code = require "error_code"
local public_config = require "public_config"
local putil = require "public_util"
local lua_util = require "lua_util"
local global_data = require "global_data"

local log_d = putil.log_d
--local log_i = putil.log_i
--local log_w = putil.log_w


---------------------------------------------------------
local check_depends = {}

function check_depends:init_data()
    self.depends = {
        [public_config.LIMIT_WORLD_LEVEL]           = "check_world_level",
        [public_config.LIMIT_LEVEL]                 = "check_level",
        [public_config.LIMIT_VOCATION]              = "check_vocation",
        [public_config.LIMIT_VIP_LEVEL]             = "check_vip_level",
        [public_config.LIMIT_LEVEL_LT]              = "check_level_lt",
        [public_config.LIMIT_FIGHT]                 = "check_fight",
        [public_config.LIMIT_COMMITED_TASK]         = "check_commited_task",
    }
end

--检查玩家是否满足一系列条件
function check_depends:get_check_err_id(avatar, depends)
    for key, value in pairs(depends) do
        if key >= public_config.LIMIT_CONDITION_MIN and key < public_config.LIMIT_CONDITION_MAX then
            local check_func_name = self.depends[key]
            local check_func = self[check_func_name]
            if check_func then
                --value为table，因为depends配表为_n  --也可能配置成_m，所以兼容一下
                local value_first
                if type(value) == "table" then
                    value_first = value[1] or 0
                else
                    value_first = value
                end
                local err_id, err_params = check_func(avatar, value, value_first)
                if err_id ~= 0 then
                    return err_id, err_params or {}
                end
            else --没有定义这种限制条件
                log_d("check_depends:get_check_err_id    1 no this limit ", "limit_id=%s", key)
                return error_code.ERR_NO_THIS_CONDITION, {key}
            end
        end
    end
    return 0
end

--检查世界等级
function check_depends.check_level(avatar, value , value_first)
    local world_level = global_data:get_world_level()
    if world_level >= value_first then
        return error_code.ERR_SUCCESSFUL
    else
        return error_code.ERR_WORLD_LEVEL_LOW
    end
end

--检查玩家等级是否大于等于
function check_depends.check_level(avatar, value , value_first)
    if avatar.level >= value_first then
        return error_code.ERR_SUCCESSFUL
    else
        return error_code.ERR_AVATAR_LEVEL_LOW
    end
end

--检查玩家的职业
function check_depends.check_vocation(avatar,value,value_first)
    if type(value) == "table" then
        if lua_util.is_in_table_list(value,avatar.vocation) then
            return error_code.ERR_SUCCESSFUL
        else
            return error_code.ERR_AVATAR_VOCATION
        end
    else
        if value_first == avatar.vocation then
            return error_code.ERR_SUCCESSFUL
        else
            return error_code.ERR_AVATAR_VOCATION
        end
    end
end

--检查玩家vip等级
function check_depends.check_vip_level(avatar,value,value_first)
    if avatar.vip_level >= value_first then
        return error_code.ERR_SUCCESSFUL
    else
        return error_code.ERR_AVATAR_VIP
    end
end

--检查玩家等级是否小于
function check_depends.check_level_lt(avatar, value, value_first)
    if avatar.level < value_first then
        return error_code.ERR_SUCCESSFUL
    else
        return error_code.ERR_AVATAR_LEVEL_HIGH
    end
end

--战斗力大于等于
function check_depends.check_fight(avatar, value, value_first)
    if avatar.fight_force >= value_first then
        return error_code.ERR_SUCCESSFUL
    else
        return error_code.ERR_FIGHT_FORCE_LOW
    end
end

--任务是否提交的，主线和支线
function check_depends.check_commited_task(avatar, value, value_first)
    
    --TODO:任务只有base才有。几乎没有cell调用check_depends
    local mgr_task = require "mgr_task"
    if not mgr_task then
        return error_code.ERR_SUCCESSFUL
    end
    
    if mgr_task:is_commited_task(avatar, value_first) then
        return error_code.ERR_SUCCESSFUL
    else
        return error_code.ERR_NO_COMMITED_TASK
    end  
end

-------------------------------
return check_depends

