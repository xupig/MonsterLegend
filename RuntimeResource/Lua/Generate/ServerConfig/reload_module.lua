--------------------------------------------
--实现热更新某个lua脚本

local lua_util = require "lua_util"
local putil = require "public_util"

local log_d = putil.log_d

local type = type
local pairs = pairs


---------------------------------------------------------
--热更对编码的要求
--1、模块中不允许定义local变量。
--   只能是宏或者local函数等无状态的定义
--   因为热更后，将会被代替。
--2、单例mgr中，调用自己的内部函数(:的，不是.的)一定要用self。
--   因为热更后mgr_xxx的在被模块已经被换成新的了，用self才是旧的那个对象。
--   
--所以，最好热更只是用于修改现有函数和现有值，不做太多复杂的处理。
--对于实体，热更无法新增def属性和def函数，只能修改。当然内部非def函数是可以的，所以对于哪些subsys访问的是可以的。
--对于单例mgr，可以新增属性和函数，和修改旧的。
-------------------------------------------------------


---------------------------------------------------------
local reload_module = {}


------------------
--实现热更新某个lua脚本
--@param module_name:脚本文件名
--@param pathname:脚本路径
function reload_module.reload_module(module_name)
    local old_module = package.loaded[module_name]
    if not old_module then
        --该模块之前未加载过 
        return
    end
    log_d("reload_module.reload_module    1 ", "module_name=%s", module_name)
    
    --全局表 --Avatar.lua中的Avatar表就是全局表
    local old_g = _G[module_name] 

    --模块，使用旧的，新的赋值给旧的。--模块中定义的变量，全部被替换，所以不允许模块中定义变量
    package.loaded[module_name] = nil
    local new_module = lua_util.pcall(require, nil, module_name)
    package.loaded[module_name] = old_module
    if type(old_module) == "table" and type(new_module) == "table" then
        --log_d("reload_module.reload_module    2 ", "module_name=%s", module_name)
        reload_module.update_module(old_module, new_module, module_name,1)
    end
    
    --保证全局表的也是old对象.因为底层require过，引用过。
    if old_g then
        --log_d("reload_module.reload_module    3 ", "module_name=%s;old_g=%s;new_g=%s", module_name,tostring(old_g),tostring(_G[module_name]))
        _G[module_name] = old_g
    end
end

------------------------------------
--将新模块返回的表中的非table成员替换掉老表中对应的项，嵌套的表只更新表中的数据
--@param module: 之前的模块
--@param module_new: 最新模块
function reload_module.update_module(module, module_new,module_name, depth)
    for key, new_value in pairs(module_new) do
        local value_type = type(new_value)
        if value_type ~= "table" then
            module[key] = new_value
            --log_d("reload_module.update_module    1 ", "module_name=%s;depth=%s;key=%s", module_name,depth,key)
        else
            local module_value = module[key]
            if module_value then
                if type(module_value) ~= "table" then
                    --log_d("reload_module.update_module    2 ", "module_name=%s;depth=%s;key=%s",module_name,depth,key)
                    module_value = {}
                    module[key] = module_value
                end
                --log_d("reload_module.update_module    3 ", "module_name=%s;depth=%s;key=%s",module_name,depth,key)
                if module_value ~= new_value and key ~= "__index" then  --自身的引用，避免死循环
                    --log_d("reload_module.update_module   4 ", "module_name=%s;depth=%s;key=%s",module_name,depth,key)
                    reload_module.update_module(module_value, new_value, module_name,depth+1)
                end
            else
                --log_d("reload_module.update_module   5 ", "module_name=%s;depth=%s;key=%s",module_name,depth,key)
                module[key] = new_value
            end
        end
    end
end

--------------------------------------------------
return reload_module



