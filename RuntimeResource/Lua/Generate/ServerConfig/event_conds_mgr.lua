-----------------------------------------------------------------------
--该模块负责管理所有的条件复合事件，并且负责判断某个复合事件是否被触发

local lua_util = require "lua_util"
local putil = require "public_util"
local engine = require "engine"

local log_d = putil.log_d
--local log_i = putil.log_i
--local log_w = putil.log_w
--local log_e = putil.log_e

local table = table
local pairs = pairs
local string = string
local tonumber = tonumber
--local tostring = tostring
local ipairs = ipairs
local next = next

local table_insert = table.insert

local event_conds_mgr = {}


--处理各种逻辑判断
local cond_operators = {'=', '}', '{', '@', '#'}  --定义条件表达式中的条件判断操作符

--等于判断
local function equal(value1, value)
    --value1是event_param
    --value是配置的，读成table_list
    return value1 == value[1]
end

--大于等于判断
local function greater(value1, value)
    --value1是event_param
    --value是配置的，读成table_list
    return value1 >= value[1]
end

--小于判断
local function lesser(value1, value)
    --value1是event_param
    --value是配置的，读成table_list
    return value1 < value[1]
end

--区间判断
local function between(value1, value)
    --value1是event_param
    --value是配置的，读成table_list
    return value[1] <= value1 and value1 <= value[2]
end

--枚举判断
local function enum(value1, value)
    --value1是event_param
    --value是配置的，读成table_list
    return lua_util.is_in_table_list(value,value1)
end

--枚举类型与判断函数之间的映射关系
local COND_CHECK_FUNCS = {
    ['='] = equal,
    ['}'] = greater,
    ['{'] = lesser,
    ['@'] = between,
    ['#'] = enum
}


--解析条件事件配置表
function event_conds_mgr:init_data(is_update)
    self.event_conds_data = lua_util._readXml("/data/xml/event_conds.xml", "id_i")
    self.event_conds_map = {}
    self:statis_event_conds_map()
    log_d("event_conds_mgr:init_data    1 task","event_conds_map=%s",engine.cPickle(self.event_conds_map))
end

--记录每个事件可能触发的条件事件，格式为: {event_id : {event_cond_id1, event_cond_id2, ...}, ...}
function event_conds_mgr:statis_event_conds_map()
    local event_conds_data = self.event_conds_data
    local event_conds_map = self.event_conds_map
    for _, event_cond_info in pairs(event_conds_data) do
        event_cond_info.conds_org = event_cond_info.conds
        local conds = event_cond_info.conds
        if conds then
            event_cond_info.conds = self:gen_event_conds(conds)
        end
        
        event_cond_info.conds_or_org = event_cond_info.conds_or
        local conds_or = event_cond_info.conds_or
        if conds_or then
            event_cond_info.conds_or = self:gen_event_conds(conds_or)
        end
        
        local event_id = event_cond_info["event_id"]
        if not event_conds_map[event_id] then
            event_conds_map[event_id] = {event_id}
        else
            table_insert(event_conds_map[event_id], event_id)
        end
    end
end

--根据一个条件字符串生成复合条件表达式
function event_conds_mgr:gen_event_conds(conds_str)
    local conds_split = lua_util.split_str(conds_str, ';')  --多个条件之间用分号隔开，表示与操作
    local conds_list = {}
    for _, cond_info in ipairs(conds_split) do
        local one_cond
        local cond_key, cond_value, sep_index
        for _, cond_op in pairs(cond_operators) do
            sep_index = string.find(cond_info, cond_op)
            if sep_index ~= nil then
                cond_key = string.sub(cond_info, 1, sep_index - 1)
                cond_value = string.sub(cond_info, sep_index + #cond_op)
                cond_value = lua_util.split_str(cond_value, ',', tonumber) --value可以是多个值
                one_cond = {tonumber(cond_key), cond_value, cond_op}
                break
            end
        end
        if one_cond then
            table_insert(conds_list, one_cond)
        else
            log_d("event_conds_mgr:gen_event_conds    1 task","conds_str=%s",conds_str)
        end
    end
    return conds_list
end

function event_conds_mgr:get_event_cond_info(event_cond_id)
    return self.event_conds_data[event_cond_id]
end

--获取某个条件事件对应的事件
function event_conds_mgr:get_event_id(event_cond_id)
    local event_cond_info = self:get_event_cond_info(event_cond_id)
    if event_cond_info then
        return event_cond_info["event_id"]
    end
end

--获取某个事件所能触发的条件事件
function event_conds_mgr:get_event_cond_map(event_id)
    return self.event_conds_map[event_id]
end

--获取某个事件对应的依赖条件，格式为：{{key, value(是个table), operator}, ...}
function event_conds_mgr:get_conds(event_cond_id)
    local event_cond_info = self:get_event_cond_info(event_cond_id)
    if event_cond_info then
         return event_cond_info.conds
    end
end

--获取某个事件对应的依赖条件，格式为：{{key, value(是个table), operator}, ...}
function event_conds_mgr:get_conds_or(event_cond_id)
    local event_cond_info = self:get_event_cond_info(event_cond_id)
    if event_cond_info then
         return event_cond_info.conds_or
    end
end

function event_conds_mgr:get_param(event_cond_id, param_id)
    local event_conds = self:get_conds(event_cond_id)
    if event_conds then
        for _, one_cond in pairs(event_conds) do
            if one_cond[1] == param_id then
                return one_cond[2][1]
            end
        end
    end
    return nil
end

--判断某个事件是否具有某个条件要求
function event_conds_mgr:has_param(event_cond_id, param_id)
    local event_conds = self:get_conds(event_cond_id)
    if event_conds then
        for _, one_cond in pairs(event_conds) do
            if one_cond[1] == param_id then
                return true
            end
        end
    end
    return false
end

--判断传来的条件是否满足cnd_evt_id事件的条件。
--传来的evt_param封装格式为{[事件参数id1]=事件参数值1, [事件参数id2]=事件参数值2, ...}
--cnd_evt_id，atom_evt_id分别对应event_cnds.xml的id_i，atom_id_i。
function event_conds_mgr:match_cond_event(event_cond_id, event_id, event_param)
    local event_cond_info = self:get_event_cond_info(event_cond_id)
    if event_cond_info then
        if event_id ~= event_cond_info["event_id"] then
            return false
        end
        
        local event_conds = event_cond_info.conds or {}
        for _, one_cond in ipairs(event_conds) do
            local matched = self:check_one_cond(event_param, one_cond)
            --log_d("event_conds_mgr:match_cond_event    1 task","event_cond_id=%s;one_cond=%s;event_param=%s;matched=%s",event_cond_id,engine.cPickle(one_cond),engine.cPickle(event_param),tostring(matched))
            if not matched then
                return false
            end
        end
        
        local event_conds_or = event_cond_info.conds_or or {}
        for _, one_cond in ipairs(event_conds_or) do
            local matched = self:check_one_cond(event_param, one_cond)
            --log_d("event_conds_mgr:match_cond_event    2 task","event_cond_id=%s;one_cond=%s;event_param=%s;matched=%s",event_cond_id,engine.cPickle(one_cond),engine.cPickle(event_param),tostring(matched))
            if matched then
                return true
            end
        end
        
        --不支持填两个，只允许填一个。
        --如果都不填，那么返回true
        --如果填evt_cnds，那么返回true，因为上面判断全部符合条件
        --如果天evt_cnds_or，那么返回false，因为上面判断全部不符合条件
        if not next(event_conds) and not next(event_conds_or) then
            return true
        end
        if next(event_conds) then
            return true
        end
        if next(event_conds_or) then
            return false
        end
    end
    return false
end

--判断某个事件是否满足某个条件
function event_conds_mgr:check_one_cond(event_param, one_cond)
    --one_cond下标
    --参考one_cond = {tonumber(cond_key), cond_value, cond_op}
    
    local check_func = COND_CHECK_FUNCS[one_cond[3]]
    if check_func then
        if event_param[one_cond[1]] and one_cond[2] then
            return check_func(event_param[one_cond[1]], one_cond[2])
        else
            --可以允许没有
            --log_d("event_conds_mgr:check_one_cond    1 error param task","event_param=%s;one_cond=%s",engine.cPickle(event_param),engine.cPickle(one_cond))
            return false
        end
    else
        return true
    end
end




------------------------------
return event_conds_mgr




