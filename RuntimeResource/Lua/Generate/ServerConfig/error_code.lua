local error_code = {

    --系统相关 0-1000
    ERR_SUCCESSFUL                = 0,
    ERR_NO_THIS_FUNC_NAME         = 1,
    ERR_AVATAR_LEVEL_LOW          = 2, --玩家等级过低 check_depends
    ERR_AVATAR_LEVEL_HIGH         = 3, --玩家等级过高 check_depends
    ERR_AVATAR_VOCATION           = 4, --角色职业不正确 check_depends
    ERR_WORLD_LEVEL_LOW           = 5, --世界等级过低 check_depends
    ERR_AVATAR_RACE               = 6, --角色种族不正确 check_depends
    ERR_ACCOUNT_WRITE_TO_DB       = 7, --写数据库失败
    ERR_NO_THIS_PLAYER            = 8, --没有该玩家 --玩家不存在
    ERR_FUNC_NOT_PARAM            = 9, --参数有误
    ERR_SPACE_AVATAR_LEVEL_LOW    = 10, --玩家等级过低，不能进副本
    ERR_SPACE_AVATAR_LEVEL_HIGH   = 11, --玩家等级过高，不能进副本
    ERR_FIGHT_FORCE_LOW           = 12, --战斗力低  check_depends
    ERR_NO_COMMITED_TASK          = 13, --没有完成任务 check_depends
    ERR_NO_THIS_CONDITION         = 14, --check_depends配置的条件没有
    ERR_VIP_LEVEL_LOW             = 15, --vip等级不足
    ERR_CREATE_AVATAR_STOPWORD    = 16, --角色名有敏感词
    ERR_TELEPORT_ERR              = 17, --传送出错
    ERR_TELEPORT_CFG_ERR          = 18, --传送配置出错
    ERR_TELEPORT_NOT_KING         = 19, --传送 不是主城野外
    ERR_DB_ERR                    = 20, --数据库操作失败
    ERR_CREATE_AVATAR_NAME_LEN    = 21, --角色名长度不对
    ERR_CREATE_AVATAR_NAME_NO_NUM = 22, --角色名不能是数字
    ERR_CREATE_AVATAR_NAME_ALREADY_USED = 23, --角色名已经被占用
    ERR_WORLD_LEVEL_LOW_PARAM     = 24, --世界等级{}级开启
    ERR_SPACE_PALYER_COUNT_MAX    = 25, --该场景人数达到上限
    ERR_CHOOSE_FORBIDDEN          = 26, --号被封了
    ERR_CHOOSE_AVATAR_NOT_MINE    = 27, --帐号身上没有该角色
    ERR_CHOOSE_LOADING_STATE      = 28, --正在load avatar
    ERR_FUNCTION_NOT_OPEN         = 29, --功能未开启。假设某个功能后端热更入口关闭。
    ERR_CREATE_AVATAR_STATE       = 30, --服务器状态错误，不能创建角色
    ERR_CHAT_NEED_LEVEL           = 31, --该频道{}级开启
     
    --道具系统 1001-1999
    ERR_PKG_ERR_PKG_TYPE           = 1001,
    ERR_PKG_ERR_PKG_FULL           = 1002, --背包格子满
    ERR_ITEM_NOT_ENOUGH            = 1003, --道具不足
    ERR_GOLD_NOT_ENOUGH            = 1004, --金币不足
    ERR_PKG_HAS_NO_ITEMS           = 1005, --背包没有道具
    ERR_EXP_NOT_ENOUGH             = 1006, --经验不足
    ERR_COUPONS_NOT_ENOUGH         = 1007, --点券不足
    ERR_COUPONS_BIND_NOT_ENOUGH    = 1008, --绑定点券不足
    ERR_SILVER_NOT_ENOUGH          = 1009, --银币不足
    ERR_ATHLETICS_NOT_ENOUGH       = 1010, --竞技点不足
    ERR_TALENT_NOT_ENOUGH          = 1011, --天赋点不够
    ERR_ITEM_PKG_NO_ITEM           = 1012, --背包没有该道具
    ERR_PKG_POS_HAS_NO_ITEMS       = 1013, -- 该格子没有道具
    ERR_ITEM_USE_LEVEL_LOW         = 1014, --玩家等级不够，不能使用道具
    ERR_ITEM_SHOE_NOT_ENOUGH       = 1015, --道具不足,小飞鞋
    ERR_ITEM_HANG_UP_TIME_MAX      = 1016, --离线挂机时间满了
    
    ERR_ITEM_ERR_ID                = 2000,    --该道具不存在
    ERR_ITEM_SELL_ERR_ITEM_ID      = 2001,    --出售物品,该格子的道具的id未配置
    ERR_ITEM_SELL_DIFF_ITEM_ID     = 2002,    --出售物品,该格子的道具的id与输入id不匹配
    ERR_ITEM_SELL_CANT_DROP        = 2003,    --该道具不可出售
    ERR_ITEM_SELL_EQUIP            = 2004,    --装备不能出售,只能分解
    ERR_ITEM_NOT_EFFECT            = 2005,    --道具不存在效果
    ERR_ITEM_EFFECT_OVER           = 2006,    --道具效果超出范围
    ERR_ITEM_CANNOT_GUILD_BUY      = 2007,    --该道具不能引导购买

    ERR_EQUIP_ERR_VALUE            = 2011,    --equip_value表没配置
    ERR_EQUIP_ERR_GRADE_NIL        = 2012,    --随机装备档次失败
    ERR_EQUIP_STRENGH_LEVEL        = 2020,    --玩家等级不够,不能解锁装备强化
    ERR_EQUIP_STRENGTH_PKG_TYPE    = 2021,    --只能强化已穿装备或者包裹里的装备
    ERR_EQUIP_STRENGTH_NO_EQUIP    = 2022,    --要强化的装备不存在
    ERR_EQUIP_STRENGTH_ERR_ITEM_ID = 2023,    --错误的道具id
    ERR_EQUIP_STRENGTH_ERR_EQUIP   = 2024,    --这个道具不是装备
    ERR_EQUIP_STRENGTH_MAX_LEVEL   = 2025,    --已到最大强化等级(强化表没有配置)
    ERR_EQUIP_STRENGTH_LESS_STONE  = 2026,    --强化需要的强化石不足
    ERR_EQUIP_STRENGTH_LESS_LUCK_STONE = 2027,--强化需要的幸运石不足
    ERR_EQUIP_STRENGTH_NO_NEED_LUCK= 2028,    --这个强化等级不能添加幸运石
    ERR_EQUIP_STRENGTH_ERR_LUCK_STONE = 2029, --这个强化等级不能使用玩家指定的幸运石
    ERR_EQUIP_STRENGTH_MAX_LEVEL2  = 2030,    --已到最大强化等级(强化表没有配置材料消耗)
    ERR_EQUIP_STRENGTH_HIGH_LEVEL  = 2031,    --已到该装备所限定的最大强化等级
    ERR_EQUIP_INHER_LEVEL          = 2032,    --玩家等级不够,不能装备继承
    ERR_EQUIP_INHER_PKG_TYPE       = 2033,    --被继承装备只能是已穿装备或者包裹里的装备
    ERR_EQUIP_INHER_PKG_TYPE2      = 2034,    --继承强化的装备只能是已穿装备或者包裹里的装备
    ERR_EQUIP_INHER_NO_SRC_EQUIP   = 2035,    --被继承装备不存在
    ERR_EQUIP_INHER_NO_DEST_EQUIP  = 2036,    --继承装备不存在
    ERR_EQUIP_INHER_ITEM_ID        = 2037,    --错误的道具id
    ERR_EQUIP_INHER_DEST_ITEM_ID   = 2038,    --错误的道具id
    ERR_EQUIP_INHER_ERR_EQUIP      = 2039,    --这个道具不是装备
    ERR_EQUIP_INHER_ERR_DEST_EQUIP = 2040,    --这个道具不是装备
    ERR_EQUIP_INHER_ERR_TYPE       = 2041,    --参与继承的两件装备类型不匹配
    ERR_EQUIP_INHER_ERR_SUBTYPE    = 2042,    --参与继承的两件装备子类型不匹配
    ERR_EQUIP_INHER_LOWER_SRC_LEVEL= 2043,    --只能继承比自己强化等级高的装备的强化等级
    ERR_EQUIP_INHER_NO_COST        = 2044,    --继承需要消耗的货币或者道具不足
    ERR_EQUIP_BREAK_NO_EQUIP       = 2045,    --需要分解的装备不存在
    ERR_EQUIP_BREAK_ERR_ID         = 2046,    --需要分解的装备道具id不存在
    ERR_EQUIP_BREAK_NO_BREAK_LIST  = 2047,    --没有配分解获得字段,表示该装备不可分解
    ERR_EQUIP_BREAK_NO_FREE_GRID   = 2048,    --背包空闲格子不够,不能分解
    ERR_EQUIP_WASH_PKG_TYPE        = 2049,    --被洗练装备只能是已穿装备或者包裹里的装备
    ERR_EQUIP_WASH_LEVEL           = 2050,    --玩家等级不够，不能洗练装备
    ERR_EQUIP_WASH_NO_EQUIP        = 2051,    --需要洗练的装备不存在
    ERR_EQUIP_WASH_ITEM_ID         = 2052,    --洗练的装备道具id错误
    ERR_EQUIP_WASH_ERR_EQUIP       = 2053,    --这个道具不是装备，不能洗练
    ERR_EQUIP_WASH_IDX_EXISTS      = 2054,    --已经选择了一条洗练属性，不能改洗其他属性
    ERR_EQUIP_WASH_MAX_COUNT       = 2055,    --已到今日最大洗练次数
    ERR_EQUIP_WASH_LOW_QUALITY     = 2056,    --装备品质不够，不能洗练
    ERR_EQUIP_WASH_COST            = 2057,    --洗练需要消耗的道具不足
    ERR_EQUIP_WASH_ATTRI_IDX       = 2058,    --选择洗练的槽位不合法
    ERR_EQUIP_WASH_ATTRI_IDX2      = 2059,    --该槽位上没有属性，不能洗练
    ERR_EQUIP_WASH_ERR_VALUE       = 2060,    --没有找到对应的洗练配置数据
    ERR_EQUIP_WASH_SAVE_PKG_TYPE   = 2061,    --替换洗练属性的背包类型不对
    ERR_EQUIP_WASH_SAVE_NO_EQUIP   = 2062,    --替换洗练属性的装备不存在
    ERR_EQUIP_WASH_NEW_VALUE       = 2063,    --没有洗练好的属性可以替换
    ERR_EQUIP_WASH_OLD_VALUE       = 2064,    --没有老的属性可以替换
    ERR_GEM_LOAD_PKG_TYPE          = 2065,    --镶嵌宝石,装备所在包裹类型错误
    ERR_GEM_LOAD_NO_EQUIP          = 2066,    --要镶嵌宝石的装备不存在
    ERR_GEM_LOAD_ITEM_ID           = 2067,    --要镶嵌宝石的装备道具id错误
    ERR_GEM_LOAD_ERR_EQUIP         = 2068,    --要镶嵌宝石的不是装备
    ERR_GEM_LOAD_ERR_HOLE_POS      = 2069,    --宝石孔错误
    ERR_GEM_LOAD_HOLE_LOADED       = 2070,    --该孔上已经镶嵌了宝石了
    ERR_GEM_LOAD_NO_GEM            = 2071,    --宝石不存在
    ERR_GEM_LOAD_ERR_GEM           = 2072,    --该道具不是宝石
    ERR_GEM_LOAD_MAX_POS           = 2073,    --想要镶嵌的槽位超过了该装备的最大孔数
    ERR_GEM_LOAD_EQUIP_BAN         = 2074,    --该装备不可镶嵌宝石
    ERR_GEM_LOAD_GEM_BAN           = 2075,    --该宝石不可镶嵌到装备上
    ERR_GEM_LOAD_ERR_HOLE_TYPE     = 2076,    --宝石孔类型不匹配
    ERR_GEM_LOAD_NEED_LEVEL        = 2077,    --玩家等级不够,不能镶嵌这个宝石
    ERR_GEM_LOAD_CANT_LEGEND       = 2078,    --这件装备不能镶嵌传奇魔石
    ERR_GEM_LOAD_MAX_LEGEND        = 2079,    --这件装备已经镶嵌的传奇魔石数目已到上限
    ERR_GEM_UNLOAD_PKG_TYPE        = 2081,    --拆卸宝石,装备所在包裹类型错误
    ERR_GEM_UNLOAD_NO_EQUIP        = 2082,    --拆卸宝石,装备不存在
    ERR_GEM_UNLOAD_ERR_HOLE_POS    = 2083,    --宝石孔错误
    ERR_GEM_UNLOAD_HOLE_NIL        = 2084,    --该槽位上没有宝石,无法拆下
    ERR_GEM_UNLOAD_PKG_FULL        = 2085,    --背包已满,没法放入拆卸下来的宝石
    ERR_GEM_COMPOUND_ERR_SRC_ID    = 2090,    --合成宝石,错误的低级宝石id
    ERR_GEM_COMPOUND_NO_NEXT_ID    = 2091,    --已经合成到最高级了,不能继续合成
    ERR_GEM_COMPOUND_LESS_SRC_GEM  = 2092,    --低级宝石数量不够
    ERR_GEM_COMPOUND_PKG_FULL      = 2093,    --背包空闲格子不够,不能合成宝石
    ERR_GEM_COMPOUND_LESS_STONE    = 2094,    --提高成功率的道具数目不足
    ERR_GEM_COMPOUND_NEED_LEVEL    = 2095,    --玩家等级不够,不能合成这个宝石
    ERR_GEM_COMPOUND_ERR_TYPE      = 2096,    --不能合成传奇魔石
    ERR_GEM_COMPOUND_ERR_SRC_GEM   = 2097,    --指定的合成物品不是宝石
    ERR_GEM_COMPOUNDF_ERR_DEST_ID  = 2100,    --快速合成,目标宝石不存在
    ERR_GEM_COMPOUNDF_ERR_DEST_GEM = 2101,    --快速合成,目标不是宝石
    ERR_GEM_COMPOUNDF_GEM_LEVEL_NIL= 2102,    --快速合成,目标宝石等级未知
    ERR_GEM_COMPOUNDF_SRC_ID       = 2103,    --低级宝石id错误
    ERR_GEM_COMPOUNDF_SRC_ITEMTYPE = 2104,    --参与合成的道具不是宝石
    ERR_GEM_COMPOUNDF_SRC_GEM_TYPE = 2105,    --参与合成的道具是传奇魔石
    ERR_GEM_COMPOUNDF_LESS_LV1     = 2106,    --合成高级宝石需要的1级宝石数不匹配
    ERR_GEM_COMPOUNDF_LESS_STONE   = 2107,    --合成高级宝石需要的合成符不足
    ERR_GEM_COMPOUNDF_PKG_LESS_GEM = 2108,    --背包里拥有的道具不足
    ERR_GEM_COMPOUNDF_ERR_SUBTYPE  = 2109,    --参与快速合成的宝石和目标宝石不是同类型的宝石
    ERR_LGEM_EAT_NO_ITEM           = 2111,    --背包里的传奇魔石不存在
    ERR_LGEM_EAT_ERR_DEST_GEM      = 2112,    --指定的道具不是魔石
    ERR_LGEM_EAT_ERR_TYPE          = 2113,    --只有传奇魔石才能添加能量值
    ERR_LGEM_EAT_NEED_LEVEL        = 2114,    --玩家等级不够,不能为传奇魔石添加能量
    ERR_LGEM_EAT_MAX_LEVEL         = 2115,    --传奇魔石已到最高级，不能添加能量
    ERR_LGEM_EAT_LEVELUP_EXP_NIL   = 2116,    --传奇魔石已到最高级，不能添加能量2
    ERR_LGEM_EAT_MAX_EXP           = 2117,    --传奇魔石能量值已满,请先升级
    ERR_LGEM_EAT_NO_SRC_ITEM       = 2118,    --提供能量值的传奇魔石道具不存在
    ERR_LGEM_EAT_SRC_ID            = 2119,    --提供能量值的传奇魔石道具没有配置数据
    ERR_LGEM_EAT_SRC_NO_ENERGY     = 2120,    --该道具不能为传奇魔石提供能量值
    ERR_LGEM_EAT_EAT_DEST          = 2121,    --参数错误,不能用自己为自己提供能量值
    ERR_LGEM_EAT_SRC_LOCKED        = 2122,    --提供能量值的魔石已经加锁,请先解锁
    ERR_LGEM_LVLUP_NO_ITEM         = 2125,    --要升级的传奇魔石不存在
    ERR_LGEM_LVLUP_ERR_SRC_GEM     = 2126,    --指定要升级的道具不是魔石
    ERR_LGEM_LVLUP_ERR_TYPE        = 2127,    --指定要升级的道具不是传奇魔石
    ERR_LGEM_LVLUP_NEED_LEVEL      = 2128,    --玩家等级不够,不能为传奇魔石升级
    ERR_LGEM_LVLUP_MAX_LEVEL       = 2129,    --该传奇魔石已经升到最高级了
    ERR_LGEM_LVLUP_LEVELUP_EXP_NIL = 2130,    --该传奇魔石已经升到最高级了2
    ERR_LGEM_LVLUP_LESS_EXP        = 2131,    --传奇魔石能量值未满，不能升级
    ERR_LGEM_LVLUP_PKG_FULL        = 2132,    --背包已满,无法升级传奇魔石
    ERR_LGEM_LVLUP_LESS_STONE      = 2133,    --传奇魔石升级需要的幸运符道具不足
    ERR_LGEM_LOCK_NO_ITEM          = 2140,    --要加锁的传奇魔石不存在
    ERR_LGEM_LOCK_ERR_SRC_GEM      = 2141,    --不是传奇魔石不能加锁
    ERR_LGEM_LOCK_ERR_TYPE         = 2142,    --不是传奇魔石不能加锁2
    ERR_LGEM_UNLOCK_NO_ITEM        = 2143,    --要解锁的传奇魔石不存在
    ERR_EQUIP_LOAD_NO_EQUIP        = 2144,    --要穿的装备不存在
    ERR_EQUIP_LOAD_ERR_TYPE        = 2145,    --装备部位字段配置错误
    ERR_EQUIP_LOAD_IDX_LOADED      = 2146,    --这个部位，角色已经有装备了。自动替换。
    ERR_EQUIP_UNLOAD_ERR_IDX       = 2147,    --脱装备槽位错误
    ERR_EQUIP_UNLOAD_PKG_FULL      = 2148,    --装备背包满了,无法除下装备
    ERR_EQUIP_UNLOAD_NO_EQUIP      = 2149,    --准备脱装备的这个槽位上没有装备呢
    ERR_EQUIP_LOAD_NEED_LEVEL      = 2150,    --玩家等级不够
    ERR_EQUIP_LOAD_NEED_VOCATION   = 2151,    --职业不够
    ERR_EQUIP_MAKE_MATERIAL_NOT_ENOUGH=2152,  --材料不足，不能制造
    ERR_EQUIP_REPAIR_PKG_TYPE       = 2153,    --只能修复背包或已穿的装备
    ERR_EQUIP_REPAIR_NO_EQUIP       = 2154,    --要修复的装备不存在
    ERR_EQUIP_REPAIR_ERR_ITEM_ID    = 2155,    --道具ID错误
    ERR_EQUIP_REPAIR_ERR_EQUIP      = 2156,    --道具不是装备
    ERR_EQUIP_REPAIR_NO_NEED        = 2157,    --道具不需要修复
    ERR_EQUIP_REPAIR_NO_COST        = 2158,    --道具不够
    ERR_EQUIP_REPAIR_ERROR_COST_CFG	= 2159,		--
    ERR_EQUIP_REPAIR_ERR_NO_DURABILIRY    = 2160,	--装备无耐久参数
    ERR_EQUIP_STRENGTH_FAIL               = 2161, --强化失败  --概率
    ERR_EQUIP_GEM_COMPOUND_FAIL           = 2162, --合成失败  --概率
    --ERR_SILVER_CHANGE_NOT_INFO          = 2163, --银元兑换 找不到对应配置
    --ERR_SILVER_CHANGE_MONEY_NOT_ENOUGH  = 2164, --银元兑换，金币不足
    ERR_EQUIP_ENCHANT_PKG_ERR             = 2165, --附魔背包类型不对
    ERR_EQUIP_ENCHANT_LEVEL_ERR           = 2166, --装备附魔需要的等级不够
    ERR_EQUIP_ENCHANT_ITEM_TYPE_ERR       = 2167, --附魔卷轴类型与装备类型不一致
    ERR_EQUIP_ENCHANT_COST_NOT_ENOUGH     = 2168, --附魔的消耗不足
    ERR_ENCHANT_POS_NOT_EQUIP_ERR         = 2169, --位置没有装备
    ERR_BUY_BAG_COUNT_MAX                 = 2170, -- 背包解锁格子超出限制
    ERR_BUY_WAREHOUSE_COUNT_MAX           = 2171, -- 仓库解锁格子超出限制
    ERR_BUY_BAG_WAREHOUSE_COST_NOT_ENOUGH = 2172, -- 解锁背包仓库消耗不够
    ERR_EQUIP_LOAD_GUARD_TIME_LIMIT       = 2173, --守护装备,超时了

    --邮件系统 2201-2250
    ERR_MAIL_NO_MAILS                = 2201,      --玩家没有邮件
    ERR_MAIL_NOT_EXIST               = 2202,      --该邮件(参数mail_dbid)不存在
    ERR_MAIL_HAD_READ                = 2203,      --邮件已被阅读
    ERR_MAIL_NO_ATTACHMENT           = 2204,      --该邮件没有附件
    ERR_MAIL_HAD_GET                 = 2205,      --邮件已领取附件
    ERR_MAIL_DEL_NOT_READ            = 2206,      --该邮件未读,不能删除
    ERR_MAIL_DEL_NOT_GET             = 2207,      --该邮件有附件,不能删除
    ERR_MAIL_DAILY_LIMIT              = 2208,      --已达每日邮件上限

    --摆摊系统 2251-2300
    ERR_PEDDLE_BUY_TIME_OUT          = 2251,      --过期的货物不能购买
    ERR_PEDDLE_BUY_SHOW_TIME         = 2252,      --处于公示期的货物不能购买
    ERR_PEDDLE_BUY_COUNT_NOT_ENOUGH  = 2253,      --道具数量不足
    ERR_PEDDLE_BUY_SELF              = 2254,      --不能购买自己摆摊出去的物品
    ERR_PEDDLE_BUY_MONEY_NOT_ENOUGH  = 2255,      --钱不够，不能购买
    ERR_PEDDLE_CANCEL_NOT_SELF       = 2256,      --不是自己的摆摊数据，不能取消
    ERR_PEDDLE_SELL_ITEM_ERR         = 2257,      --道具信息错误
    ERR_PEDDLE_SELL_ONLY_ONE         = 2258,      --公示的道具只能摆摊一个
    ERR_PEDDLE_SELL_PRICE            = 2259,      --道具价格超出规定
    ERR_PEDDLE_SELL_MONEY_NOT_ENOUGH = 2260,      --银币不足，不能摆摊
    ERR_PEDDLE_WARE_NOT_EXIST        = 2261,      --货物不存在

    --生活技能系统 2301-2400
    --ERR_LIFE_SKILL_EXP_NOT_ENOUGH                 = 2301,      --该生活技能经验不足
    --ERR_LIFE_SKILL_WORLD_CONTRIBUTE_NOT_ENOUGH    = 2302,      --世界贡献度不足
    --ERR_LIFE_SKILL_PLAYER_LEVEL                   = 2303,      --角色等级不足
    --ERR_LIFE_SKILL_WORLD_LEVEL                    = 2304,      --世界等级不足
    --ERR_LIFE_SKILL_SILVER_NOT_ENOUGH              = 2305,      --银币不足
    --ERR_LIFE_SKILL_NO_CFG                         = 2306,      --配置文件错误
    --ERR_LIFE_SKILL_LEVEL_NOT_ENOUGH               = 2307,      --生活技能等级不足
    --ERR_LIFE_SKILL_COST_ENOUGH                    = 2308,      --消耗的材料不足
    --ERR_LIFE_SKILL_EXP_ITEM_NOT_RIGHT             = 2309,      --道具不正确，不能加经验
    --ERR_LIFE_SKILL_EXP_ITEM_NOT_ENOUGH            = 2310,      --道具不足，不能加经验
    --ERR_LIFE_SKILL_EXP_ITEM_NOT_EFFECT            = 2311,      --道具没有该效果，不能加经验
    --ERR_LIFE_SKILL_ACTIVITY_NOT_ENOUGH            = 2312,      --生活技能活力值不足
    --ERR_LIFE_SKILL_EQUIP_MAKE_REWARD_ALREADY      = 2313,      --已经领取过该奖励
    --ERR_LIFE_SKILL_EQUIP_MAKE_REWARD_EXP          = 2314,      --经验不足，不能领取

    --GM系统 2401~2500
    ERR_GM_NO_ACCOUNT_PRIVILEGE   = 2401,    --非GM账号
    ERR_GM_NO_EXC_CMD_PRIVILEGE   = 2402,    --没有执行这条GM指令的权限
    ERR_GM_NO_CMD_IN_XML          = 2403,    --xml没有配置这个GM指令
    ERR_GM_NO_CMD_IN_CODE         = 2404,    --代码里没有这个GM指令
    ERR_GM_CMD_FORMAT_ERR         = 2405,    --GM指令格式错误
    ERR_GM_GIVE_PRIVILEGE_SUCCESS = 2406,    --授予GM权限成功
    ERR_GM_EXC_CMD_SUCCESS        = 2407,    --GM指令执行成功

    --聊天系统 2501~2600
    ERR_CHAT_NO_THIS_CHANNEL             = 2501,      --没有这个聊天频道
    ERR_CHAT_MSG_EMPTY                   = 2502,      --聊天消息为空
    ERR_CHAT_MSG_TOO_LONG                = 2503,      --聊天消息太长，超过60
    ERR_CHAT_SPEAK_TO_YORSELF            = 2504,      --自己和自己聊天
    ERR_CHAT_PERSON_NOT_ONLINE           = 2505,      --聊天对象不在线
    ERR_CHAT_CHANNEL_CD                  = 2506,      --发言过于频繁，还需要等待{0}秒
    ERR_CHAT_SHIELD                      = 2507,      --被对方屏蔽了
    ERR_CHAT_SHIELD_TO_YORSELF           = 2508,      --自己不能屏蔽/取消屏蔽自己
    ERR_CHAT_LEVEL_NOT_ENOUGH            = 2509,      --等级不够,等级达到{0}级后才可以在此频道发言
    ERR_CHAT_NOT_INTO_CHANNEL            = 2510,      --加入{0}方可在此频道发言
    ERR_CHAT_BAN_CHAT_FOREVER            = 2511,      --被永久禁言了
    ERR_CHAT_BAN_CHAT                    = 2512,      --被禁言了，剩余时间
    ERR_CHAT_WORLD_CHAT_TEAM_FULL        = 2513,      --世界聊天，该分组已满
    ERR_CHAT_CALL_CD                     = 2514,      --喊话冷却
    ERR_CHAT_EDIT_OFTEN_USE_CONTENT_LEN  = 2515,      --字数超过限制
    ERR_CHAT_EDIT_OFTEN_USE_NO_CONTENT   = 2516,      --输入不能为空
    ERR_CHAT_NOT_CROSS_SPACE             = 2517,      --跨服场景才能在此频道发言   

    --组队系统 2601~2700
    ERR_TEAM_TEAM_NOT_EXIST               = 2601,     --队伍不存在
    ERR_TEAM_MEMBER_NOT_EXIST             = 2602,     --队员不存在
    ERR_TEAM_IN_TEAM                      = 2603,     --你已经在队伍中
    ERR_TEAM_NOT_IN_TEAM                  = 2604,     --你不在队伍中
    ERR_TEAM_LIMIT_LEVEL                  = 2605,     --你不满足最小组队等级
    ERR_TEAM_MAX_MEMBER                   = 2606,     --队伍已满(队伍已达到最大队员数)
    ERR_TEAM_INVITEE_NOT_EXIST            = 2607,     --被邀请者不存在
    ERR_TEAM_INVITER_NOT_EXIST            = 2608,     --邀请者不存在
    ERR_TEAM_APPLYER_NOT_EXIST            = 2609,     --申请者不存在
    ERR_TEAM_INVITER_NOT_IN_TEAM          = 2610,     --邀请者不在队伍中
    ERR_TEAM_INVITEE_IN_TEAM              = 2611,     --被邀请者已经在队伍中
    ERR_TEAM_APPLYER_IN_TEAM              = 2612,     --申请者已经在队伍中
    ERR_TEAM_INVITEE_LIMIT_LEVEL          = 2613,     --被邀请者不满足最小组队等级
    ERR_TEAM_OTHER_NOT_ONLINE             = 2614,     --[xxx]对方不在线
    ERR_TEAM_INVITER_NOT_ONLINE           = 2615,     --邀请者不在线
    ERR_TEAM_INVITEE_NOT_ONLINE           = 2616,     --被邀请者不在线
    ERR_TEAM_APPLYER_NOT_ONLINE           = 2617,     --申请者不在线
    ERR_TEAM_JOIN_STATE_ERR               = 2618,     --队伍在不允许加入的状态。在等待其他玩家同意进入副本。
    ERR_TEAM_START_STATE_ERR              = 2619,     --队伍在不允许开始的状态.在等待其他玩家同意进入副本。
    ERR_TEAM_NOT_CPTAIN                   = 2620,     --你不是队长(换队长)
    ERR_TEAM_NOT_IN_SAME_TEAM             = 2621,     --你不在该队伍
    ERR_TEAM_INVITER_TEAM_CHANGE          = 2622,     --邀请者已经换队伍
    ERR_TEAM_INVITEE_REJECT               = 2623,     --被邀请者拒绝了你的邀请
    ERR_TEAM_NOT_IN_MAP                   = 2624,     --你不在可组队区域
    ERR_TEAM_INVITEE_NOT_IN_MAP           = 2625,     --被邀请者不在可组队区域
    ERR_TEAM_INVITER_NOT_IN_MAP           = 2626,     --邀请者不在可组队区域
    ERR_TEAM_APPLYER_NOT_IN_MAP           = 2627,     --申请者不在可组队区域
    ERR_TEAM_OTHER_NOT_CPTAIN             = 2628,     --对方不是队长
    ERR_TEAM_CALL_REJECT                  = 2629,     --对方拒绝了你的召唤
    ERR_TEAM_NOT_IN_CALL_MAP              = 2630,     --你不在组队可召唤传送区域
    ERR_TEAM_OTHER_NOT_IN_CALL_MAP        = 2631,     --对方不在组队可召唤传送区域
    ERR_TEAM_CAPTAIN_APPLY_REJECT         = 2632,     --队长拒绝了你的申请
    ERR_TEAM_RACE_ERR                     = 2633,     --种族不匹配
    ERR_TEAM_INST_NOT_MATCH               = 2634,     --team agree的副本id和team start的副本id不同
    ERR_TEAM_ITEM_NOT_ENOUGH              = 2635,     --[xxx]道具不足，不能进副本
    ERR_TEAM_EXP_INSTANCE_TIMES_OVER      = 2636,     --[xxx]经验副本次数用完，不能进副本
    ERR_TEAM_EQUIPMENT_INSTANCE_TIMES_OVER      = 2637, --[xxx]装备副本助战次数用完
    ERR_TEAM_EQUIPMENT_INSTANCE_CAN_NOT_ASSIST  = 2638, --[xxx]装备副本队伍只有一个人不能助战
    ERR_INSTANCE_EXP_BUY_NUM_NOT_ENOUGH         = 2639, --经验副本购买次数不足
    ERR_INSTANCE_EXP_BUY_NUM_COST_NOT_ENOUGH    = 2640, --经验副本购买次数消耗不足
    ERR_TEAM_START_LEVEL_ERR                    = 2641, --[xxx]队员等级不足。组队表配置的等级和地图配置的等级。
    ERR_TEAM_OTHER_NOT_IN_TEAM                  = 2642, --对方没有队伍
    ERR_TEAM_MATCH_MAP_ERR                      = 2643, --正在副本中，不能进入其他副本
    ERR_TEAM_STATE_MATCHING                     = 2644, --正在匹配中
    ERR_TEAM_MEMBER_STATE_MATCHING              = 2645, --{xxx}正在匹配中
    ERR_TEAM_MEMBER_STATE_GODDESS               = 2646, --{xxx}正在护送中
    ERR_TEAM_MATCH_MAP_ERR1                     = 2647, --[xxx]正在副本中，不能进入其他副本

    --任务系统 2701~2800
    ERR_TASK_HAS                         = 2701,     --已经接受了这个任务
    ERR_TASK_CFG_NOT_EXIST               = 2702,     --配置错误，没有这个任务配置数据
    ERR_TASK_LAST_NOT_FINISH             = 2703,     --前置任务未完成
    ERR_TASK_NO_HAS                      = 2704,     --玩家没有接受这个任务
    ERR_TASK_NOT_FINISH                  = 2705,     --任务未完成
    ERR_MAIN_TASK_CAN_NOT_ABORT          = 2706,     --主线任务不能放弃。支线也不能放弃。
    ERR_HAS_MAIN_TASK                    = 2707,     --已经接了一个主线任务，在已接和完成状态
    ERR_MAIN_TASK_HAS_TRACK              = 2708,     --已经有一个主线任务在追踪
    ERR_BRANCH_TASK_TRACK_LIMIT          = 2709,     --超过支线任务的追踪数量


    --副本系统 2801~2900
    ERR_INSTANCE_TELEPORT_LEVEL          = 2801,    --等级不足，不能传送
    ERR_INSTANCE_TELEPORT_SINGING        = 2802,    --正在吟唱中，不能传送
    ERR_INSTANCE_CFG_ERR                 = 2803,    --配置错误
    ERR_NO_OWN_DROP                      = 2804,    --掉落物不存在
    ERR_NO_REVIVE_ID                     = 2805,    --没有配复活id。说明这个副本不能复活。
    ERR_NO_DIE                           = 2806,    --还没有死
    ERR_NO_REVIVE_DATA                   = 2807,    --复活id数据。配置错误。
    ERR_CAN_NOT_REVIVE                   = 2808,    --不允许复活点复活
    ERR_REVIVE_TIMES_LIMIT               = 2809,    --超过复活次数
    ERR_CAN_NOT_BE_RESCUE                = 2810,    --不允许被救
    ERR_BE_RESCUE_TIMES_LIMIT            = 2811,    --超过被救次数
    ERR_REVIVE_CD_LIMIT                  = 2812,    --复活，cd还没过
    ERR_INSTANCE_TIME_LIMIT              = 2813,    --副本次数限制
    ERR_EXP_INSTANCE_TIMES_OVER          = 2814,    --经验副本的次数超过上限
    ERR_EXP_INSTANCE_LEVEL               = 2815,    --玩家等级不满足经验副本要求
    ERR_EXP_INSTANCE_ITEM_NOT_ENOUGH     = 2816,    --道具不足，不能进入
    ERR_CLIENT_MAP_ENTER_ERR             = 2817,    --客户端调用的进入场景，只能是主城或者野外。
    ERR_EXP_INSTANCE_GOLD_INSPIRE_NUM_MAX  = 2818,    --金币鼓舞次数到上限
    ERR_EXP_INSTANCE_COUPONS_INSPIRE_NUM_MAX = 2819,  --钻石鼓舞次数到达上限

    --技能系统 2901~3000
    ERR_SKILL_NOT_LEARNED                = 2901,   --技能未学习
    ERR_SKILL_SLOT_CHANGE_ERR            = 2902,   --普攻不能换位置
    -- ERR_SPELL_NOT_EQUIP                  = 2901,     --技能还没有装备
    -- ERR_SPELL_NOT_PARAM                  = 2902,     --技能参数错误
    -- ERR_SPELL_NOT_LEARNED                = 2903,      --技能还没学习
    -- ERR_SPELL_NOT_SLOT                   = 2904,      --技能安装的位置不正确
    -- ERR_SPELL_HAS_EQUIP                  = 2905,      --技能已经装备了
    -- ERR_SPELL_NOT_LEVELUP                = 2906,      --技能不能升级了
    -- ERR_SPELL_NOT_ENOUGH                 = 2907,     --技能点或者道具不足
    -- ERR_SPELL_LEVEL_NOT_ENOUGH           = 2908,    --等级不足，无法学习
    -- ERR_SPELL_BEFORE_NOT_LEARN           = 2909,     --前置技能没有学习
    -- ERR_SPELL_HAS_LEARN                  = 2910,      --技能已经学习了
    -- ERR_SPELL_SP_IS_ZERO                 = 2911,      --技能点为0
    -- ERR_SPELL_RUNE_NOT_EXIST             = 2912,      --符文不存在
    -- ERR_SPELL_RUNE_EQUIP_SLOT            = 2913,       --符文装备位置不对
    -- ERR_SPELL_RUNE_NOT_UNLOCK            = 2914,       --符文还没有解锁
    -- ERR_SPELL_RUNE_HAS_EQUIP             = 2915,       --符文已经装备了
    -- ERR_SPELL_RUNE_NOT_SATISFY           = 2916,       --不满足解锁条件
    -- ERR_SPELL_RUNE_HAS_UNLOCK           = 2917,       --符文已经解锁
    -- ERR_SPELL_RUNE_UNLOCK_TIME           = 2918,       --符文不能解锁，解锁时间还没到
    -- ERR_SPELL_SLOT_NOT_UNLOCK           = 2919,       --槽位还没解锁

    --飞行系统 3001~3100
    --ERR_FLY_SKILL_NOT_EXIST               = 3001,     --飞行技能不存在
    --ERR_FLY_ENERGY_NOT_ENOUGH             = 3002,     --飞行能量不足
    --ERR_FLY_WING_NOT_EQUIP                = 3003,     --翅膀没有装备
    --ERR_FLY_NOT_BEFORE_STATE              = 3004,     --没有前置状态

    --pvp 3101~3200
    --ERR_PVP_REMATCH                     = 3101,   --重复报名
    --ERR_PVP_MATCH_IN_TEAM               = 3102,   --在队伍里面不能单独匹配
    --ERR_PVP_TEAM_MATCH_NO_TEAM          = 3103,   --不是队长无法队伍匹配
    --ERR_PVP_TEAM_MATCH_COUNT            = 3104,   --队伍人数超过了匹配人数
    ERR_PVP_MATCH_TIME_OUT              = 3105,   --匹配超时
    ERR_PVP_MATCH_CANCEL                = 3106,   --取消匹配
    --ERR_PVP_MATCH_NOT_OPEN              = 3107,   --活动没有开启
    ERR_PVP_MATCH_HAS_END               = 3108,   --活动已经结束
    --ERR_PVP_MATCH_NOT_JOIN              = 3109,   --还没参与过比赛
    --ERR_PVP_MATCH_NOT_WIN_TIME          = 3110,   --胜利次数不足
    --ERR_PVP_MATCH_HAS_GOT               = 3111,   --奖励已经领取

    --好友系统3401-3500
    ERR_FRD_FRD_EXISTED                  = 3401,     --你已经加过这个好友了
    ERR_FRD_MAX_COUNT                    = 3402,     --你的好友数目已到最大值
    ERR_FRD_ACK_FRD_NO_APPLY             = 3403,     --对方没有申请过加你好友,或者申请已经过期
    ERR_FRD_ADD_SELF                     = 3404,     --不能加自己为好友
    ERR_FRD_FIND_DBID_ERR                = 3405,     --该id对应的玩家不存在
    ERR_FRD_FIND_NAME_ERR                = 3406,     --该名字对应的玩家不存在

    ERR_FRIEND_ADD_EXIST                = 3411,     --你已经添加过此好友
    ERR_FRIEND_LIST_OVER_MAX            = 3412,     --好友数已达上限
    ERR_FRIEND_APPLY_NOT_EXIST          = 3413,     --对方没有申请过加你好友,或者申请已经过期
    ERR_FRIEND_CANT_ADD_SELF            = 3414,     --不能添加自己为好友
    ERR_FRIEND_DBID_PARAM_ERROR         = 3415,     --dbid参数错误
    ERR_FRIEND_FIND_NAME_NOT_EXIST      = 3416,     --查找的玩家不存在
    ERR_FRIEND_FIND_NOT_ONLINE          = 3417,     --查找的玩家不在线
    ERR_FRIEND_OTHER_LEVEL_LIMIT        = 3418,     --添加好友，对方等级不够
    ERR_FRIEND_IN_FRIEND_BLACK          = 3419,     --我在对方的黑名单内
    ERR_FRIEND_IN_SELF_BLACK            = 3420,     --对方在我的黑名单内
    ERR_FRIEND_APPLY_BE_REFUSE          = 3421,     --添加好友被拒绝
    ERR_FRIEND_APPLY_BE_PASS            = 3422,     --添加好友被通过
    ERR_FRIEND_BLACK_OVER_MAX           = 3423,     --黑名单已达上限
    ERR_FRIEND_ENEMY_OVER_MAX           = 3424,     --仇敌数据已达上限
    ERR_FRIEND_OTHER_OVER_MAX           = 3425,     --对方好友数已达上限
    ERR_FRIEND_APPLY_IS_EXIST           = 3426,     --重复发送好友添加申请

    --时装系统3501-3600
    ERR_FASHION_ERR_ID                   = 3501,    --时装id错误
    ERR_FASHION_UNLOAD_PKG_FULL          = 3502,    --时装背包满了,无法除下时装
    ERR_FASHION_UNLOAD_NO_FASHION        = 3503,    --准备脱时装的这个槽位上没有时装呢
    ERR_FASHION_LOAD_NO_FASHION          = 3504,    --要穿的时装不存在
    ERR_FASHION_LOAD_ERR_TYPE            = 3505,    --时装部位字段配置错误
    ERR_FASHION_LOAD_NEED_LEVEL          = 3506,    --玩家等级不够
    ERR_FASHION_LOAD_NEED_VOCATION       = 3507,    --职业不够
    ERR_FASHION_COLOR_CFG_ERR            = 3508,    --染色配置错误
    ERR_FASHION_COLOR_PART_ERR           = 3509,    --这个时装部位不能染色
    ERR_FASHION_COLOR_COST_NOT_ENOUGH    = 3510,    --染色道具不足
    ERR_FASHION_HAS_EXIST                = 3511,    --该时装已存在，不能重复激活
    ERR_FASHION_HAS_LOAD                 = 3512,    --该时装已穿戴
    ERR_FASHION_NOT_ACTIVATE             = 3513,    --未激活
    ERR_FASHION_CFG_ERR                  = 3514,    --无配置
    ERR_FASHION_STAR_MAX                 = 3515,    --星级已到最大
    ERR_FASHION_BREAK_COND_ERR           = 3516,    --不符合分解条件
    ERR_FASHION_BREAK_COUNT_ERR          = 3517,    --没有选择要分解的时装道具
    ERR_FASHION_BREAK_POS_NO_ITEM        = 3518,    --选中的格子没有道具

    --守护灵系统3601-3700
    --ERR_EUDEMON_ERR_ID                    = 3601,    --守护灵id错误
    --ERR_EUDEMON_NOT_HAS                   = 3602,    --守护灵未拥有
    --ERR_EUDEMON_PLAYED_HAS_EXIST          = 3603,    --请求上阵守护灵已经上阵
    --ERR_EUDEMON_PLAYED_POS_WRONG          = 3604,    --请求上阵守护灵显示槽位超限
    --ERR_EUDEMON_CAPTAIN_HAS_EXIST         = 3605,    --该守护灵已经是队长，重复任命
    --ERR_EUDEMON_CAPTAIN_NOT_PLAYED        = 3606,    --任命为队长的守护灵还未上阵
    --ERR_EUDEMON_UNPLAYED_NOT_PLAYED       = 3607,    --请求下阵的守护灵还未上阵
    --ERR_EUDEMON_UN_CAPTAIN_NOT_CAPTAIN    = 3608,    --请求撤销非队长的守护灵
    --ERR_EUDEMON_UN_PLAYED_POS_WRONG       = 3609,    --请求下阵守护灵显示槽位数据不存在
    --ERR_EUDEMON_UPGRADE_LEVEL_CNT_MORE    = 3610,    --超过一次升级所能消耗的守护灵最大数
    --ERR_EUDEMON_UPGRADE_LEVEL_NOT_HAS     = 3611,    --待升级守护灵未拥有
    --ERR_EUDEMON_UPGRADE_LEVEL_LIMITED     = 3612,    --待升级守护灵已达等级上限
    --ERR_EUDEMON_UPGRADE_LEVEL_HAS_PLAYED  = 3613,    --升级时被吞噬守护灵已上阵
    --ERR_EUDEMON_UPGRADE_LEVEL_NOT_HAS     = 3614,    --升级时被吞噬守护灵未拥有
    --ERR_EUDEMON_UPGRADE_STAR_NOT_HAS      = 3615,    --待升星守护灵未拥有
    --ERR_EUDEMON_UPGRADE_STAR_LIMITED      = 3616,    --待升星守护灵还未达等级上限
    --ERR_EUDEMON_UPGRADE_STAR_NOT_RIGHT    = 3617,    --升星时被吞噬守护灵数量不匹配
    --ERR_EUDEMON_UPGRADE_STAR_WRONG_STAR   = 3618,    --升星时被吞噬守护灵等级不匹配
    --ERR_EUDEMON_UPGRADE_STAR_HAS_PLAYED   = 3619,    --升星时被吞噬守护灵已上阵
    --ERR_EUDEMON_UPGRADE_STAR_NOT_HAS      = 3620,    --升星时被吞噬守护灵未拥有
    --ERR_EUDEMON_UPGRADE_STAR_MAX          = 3621,    --待升星守护灵已达等级上限
    --ERR_EUDEMON_CALL_ERR_ID               = 3622,    --召唤守护灵请求id错误
    --ERR_EUDEMON_PKG_FULL                  = 3623,    --守护灵背包空闲格子不够
    --ERR_EUDEMON_LEARN_SKILL_NOT_HAS       = 3624,    --技能学习的守护灵未拥有
    --ERR_EUDEMON_LEARN_SKILL_CONFIG_WRONG  = 3625,    --技能学习的技能书道具配置有误
    --ERR_EUDEMON_LEARN_SKILL_HAS_EXIST     = 3626,    --技能学习的该技能已学过
    --ERR_EUDEMON_SKILL_ITEM_COUNT_NOT_ENOUGH = 3627,  --技能书不足
    --ERR_EUDEMON_SKILL_SLOT_CNT_FULL       = 3628,    --技能槽位不足
    --ERR_EUDEMON_SKILL_SLOT_INDEX_WRONG    = 3629,    --技能槽位索引错误
    --ERR_EUDEMON_SKILL_LOCK_SLOT_HAS_EXIST = 3630,    --该技能槽位已经是锁定状态
    --ERR_EUDEMON_SKILL_LOCK_SLOT_CNT_FULL  = 3631,    --技能槽位锁定数量已满
    --ERR_EUDEMON_SKILL_UNLOCK_SLOT_NO_LOCK = 3632,    --解锁的技能槽位还未锁定
    --ERR_EUDEMON_UPGRADE_LEVEL_CNT_WRONG   = 3633,    --升级守护灵所需吞噬数据有误

    --切磋系统3701-3800
    ERR_FIGHT_FRD_TARGET_ME              = 3701,    --跟自己切磋
    ERR_FIGHT_FRD_INTERNAL_1             = 3702,     --内部错误1
    ERR_FIGHT_FRD_FRD_NOT_EXISTED        = 3703,     --找不到好友
    ERR_FIGHT_FRD_FRD_BUSY_APPLY         = 3704,     --对方在申请切磋中
    ERR_FIGHT_FRD_FRD_BUSY_BEAPPLY       = 3705,     --对方在被申请切磋中
    ERR_FIGHT_FRD_ONLINE                 = 3706,     --对方不在线
    ERR_FIGHT_FRD_REQ_TIMEOUT            = 3707,     --该请求超时了
    ERR_FIGHT_FRD_REQ_QUEUE_MAX          = 3708,     --被请求队列满了

    --商城系统3801-3900
    ERR_SHOP_ITEM_ID_ERR                 = 3801,    --商城购买物品id错误
    ERR_SHOP_VERSION_NOT_MATCH           = 3802,    --商城购买物品版本不匹配
    ERR_SHOP_WEEK_LIMIT                  = 3803,    --商城购买物品数量达到周限购次数
    ERR_SHOP_ITEM_SALE_OFF               = 3804,    --商城购买物品已下架
    ERR_SHOP_ITEM_PKG_FULL               = 3805,    --商城购买时背包已满
    ERR_SHOP_PRICE_CONFIG_ERR            = 3806,    --商城购买价格未配置
    ERR_SHOP_SELL_LIMIT_COUNT_LIMIT      = 3807,    --商城购买物品抢购达到限制
    ERR_SHOP_EXCHANGE_TYPE_WRONG         = 3808,    --商城钻石兑换金币兑换次数错误
    ERR_SHOP_BUY_COUNT_WRONG             = 3809,    --商城购买物品数量错误
    ERR_SHOP_BUY_VIP_LEVEL_LIMIT         = 3810,    --VIP等级限制未满足
    ERR_SHOP_BUY_LEVEL_LIMIT             = 3811,    --人物等级限制未满足
    ERR_SHOP_DAY_LIMIT                   = 3812,    --商城购买物品数量达到日限购次数

    --交易行系统3901-4000

    --野外宝箱 4001-4100
    ERR_WILD_TREASURE_NOT_EXIST          = 4001,  --野外宝箱不存在
    ERR_WILD_TREASURE_DISTANCE           = 4002,  --野外宝箱拾取距离太远
    ERR_WILD_TREASURE_HAS_PICK           = 4003,  --已经拾取过了

    --公会 4401-4500
    ERR_GUILD_LEVEL_NOT_ENOUGH           = 4401,  --等级不足，不能操作工会功能
    ERR_GUILD_BUILD_MONEY                = 4402,  --金币不足，无法创建
    ERR_GUILD_BUILD_NAME_LEN             = 4403,  --工会名字字数不正确
    ERR_GUILD_BUILD_ALREADY_IN           = 4404,  --工会已经创建
    ERR_GUILD_BUILD_ING                  = 4405,  --工会正在创建
    ERR_GUILD_BUILD_ALREADY_USED_NAME    = 4406,  --工会名字已经被使用
    ERR_GUILD_QUICK_JOIN_ALREADY_IN      = 4407,  --已经在公会里面了，不能快速进入
    ERR_GUILD_QUICK_JOIN_NO              = 4408,  --没有符合要求的公会
    ERR_GUILD_GET_APPLY_LIST_NO_RIGHT    = 4409,  --没有权限，不能获取申请列表
    ERR_GUILD_REPLY_APPLY_NO_RIGHT       = 4410,  --没有权限，不能回复申请
    ERR_GUILD_REPLY_APPLY_NOT_EXTI       = 4411,  --该玩家申请不存在，不能回复申请
    ERR_GUILD_SETTING_NO_RIGHT           = 4412,  --没有权限，不能设置
    ERR_GUILD_APPLY_ALREADY_IN_GUILD     = 4413,  --已经在一个公会里面，不能再申请
    ERR_GUILD_APPLY_GUILD_NOT_EXIT       = 4414,  --公会不存在，不能再申请
    ERR_GUILD_APPLY_ALREADY              = 4415,  --已经申请该公会了，不能重复申请
    ERR_GUILD_APPLY_COUNT_LIMIT          = 4416,  --申请数量已达上限
    ERR_GUILD_APPLY_LEVEL                = 4417,  --等级不符合要求，不能申请
    ERR_GUILD_APPLY_COUNT_MAX            = 4418,  --公会收到的申请数量已经到达上限
    ERR_GUILD_APPLY_MEMBER_COUNT_MAX     = 4419,  --公会人数已达上限，不能申请
    ERR_GUILD_APPLY_DELETING             = 4420,  --该公会正在删除，不能再申请
    ERR_GUILD_REPLY_ALREADY_IN_GUILD     = 4421,  --对方已经在其他公会里面
    ERR_GUILD_SET_POSITION_NO_VICE_PRESIDENT = 4422, --设置会长的时候，对方不是副会长
    ERR_GUILD_SET_POSITION_VICE_PRESIDENT_COUNT_MAX = 4423,--设置副会长的时候，人数已满
    ERR_GUILD_SET_POSITION_ELITE_COUNT_MAX  = 4424,     --设置精英的时候，人数已满
    ERR_GUILD_SET_POSITION_NO_RIGHT         = 4425,     --没有权限设置
    ERR_GUILD_KICK_NO_RIGHT                 = 4426,     --没有权限踢该玩家
    ERR_GUILD_DISMISS_NOT_PRESIDENT         = 4427,     --不是会长，不能解散
    ERR_GUILD_DISMISS_TOO_MANY_MEMBERS      = 4428,     --人数超过20人，不能解散
    ERR_GUILD_INVITE_ALREADY_IN_GUILD       = 4429,     --对方已经在一个公会里面，不能邀请
    ERR_GUILD_INVITE_ALREADY_FULL           = 4430,     --公会人数已满，不能邀请
    ERR_GUILD_INVITE_NOT_ONLINE             = 4431,     --邀请人不在线
    ERR_GUILD_INVITE_NO_RIGHT               = 4432,     --没有权限邀请
    ERR_GUILD_DEPOSE_NO_RIGHT               = 4433,     --没有权限免职
    ERR_GUILD_DEPOSE_NO_POSITION            = 4434,     --没职位可降
    ERR_GUILD_BUILD_DB                      = 4435,     --数据库操作失败
    ERR_GUILD_INVITE_ALREADY_IN_GUILD       = 4436,     --已经在公会了
    ERR_GUILD_INVITE_DELETING               = 4437,     --邀请工会删除了
    ERR_GUILD_INVITE_GUILD_NOT_EXIT         = 4438,     --邀请工会不存在
    ERR_GUILD_INVITE_MEMBER_COUNT_MAX       = 4439,     --邀请公会人数满了
    --ERR_GUILD_SIGN_ON_ALREADY               = 4440,     --已经签到了
    --ERR_GUILD_SIGN_ON_TYPE_NOT_RIGHT        = 4441,     --玩家的签到类型不对
    --ERR_GUILD_SIGN_ON_COST                  = 4442,     --钱不够，不能签到
    --ERR_GUILD_SIGN_ON_GET_REWARD_NOT_ENOUGH = 4443,     --进度不足，不能领取该宝箱
    --ERR_GUILD_SIGN_ON_GET_REWARD_ALREADY    = 4444,     --该宝箱已经领取
    ERR_GUILD_RED_ENVELOPE_NOT_EXIST        = 4445,     --红包不存在
    ERR_GUILD_RED_ENVELOPE_NOT_BELONG       = 4446,     --红包不属于本人
    ERR_GUILD_RED_ENVELOPE_HAVE_SEND        = 4447,     --红包已经发送了
    --ERR_GUILD_RED_ENVELOPE_TYPE             = 4448,     --红包类型不正确
    --ERR_GUILD_RED_ENVELOPE_TIME             = 4449,     --时间不对不能发送
    --ERR_GUILD_RED_ENVELOPE_SIGN_ON          = 4450,     --签到次数不足
    --ERR_GUILD_RED_ENVELOPE_NOT_RIGHT        = 4451,     --权限不足
    ERR_GUILD_RED_ENVELOPE_NOT_LEFT         = 4452,     --红包已经全部领完
    --ERR_GUILD_RED_ENVELOPE_TIME_OUT         = 4453,     --红包已经过期了
    ERR_GUILD_RED_ENVELOPE_GOT              = 4454,     --红包已经领取过了
    ERR_GUILD_SKILL_NOT_JOIN                = 4455,     --还没加入公会无法升级技能
    ERR_GUILD_SKILL_NOT_CONFIG              = 4456,     --没有相应的配置
    ERR_GUILD_SKILL_MONEY_NOT_ENOUGH        = 4457,     --贡献不足无法升级
    ERR_GUILD_SKILL_MONEY_LEVEL_MAX         = 4458,     --技能升到最高
    --ERR_GUILD_SHOP_NOT_EXIST              = 4459,     --商品不存在
    --ERR_GUILD_SHOP_MONEY_NOT_ENOUGH       = 4460,     --商店购买贡献不足
    --ERR_GUILD_SHOP_BUY_LIMIT              = 4461,     --商店购买次数限制
    --ERR_GUILD_RAID_NOT_OPEN               = 4462,     --对应的raid副本还没开启
    --ERR_GUILD_RAID_NOT_ON_TIME            = 4463,     --还没到开放时间
    --ERR_GUILD_RAID_LEVEL                  = 4464,     --前面的关卡还没开启
    --ERR_GUILD_RAID_NOT_CONFIG             = 4465,     --没有这个关卡
    --ERR_GUILD_RAID_IN_RAID                = 4466,     --已经在raid副本里面了
    --ERR_GUILD_RAID_IS_END                 = 4467,     --raid副本已经结束了
    --ERR_GUILD_RAID_HAVE_OPEN              = 4468,     --今天已经开启过了
    ERR_GUILD_BUILD_STOPWORD                = 4469,     --屏蔽字，公会名字
    ERR_GUILD_BUILD_TOKEN                   = 4470,     --不够令牌
    ERR_GUILD_SETTING_STOPWORD              = 4471,     --屏蔽字，公告
    ERR_GUILD_SKILL_AVATAR_LEVEL_LIMIT      = 4472,     --公会技能还没解锁，无法升级
    ERR_GUILD_NOT_JOIN                      = 4473,     --还没加入公会
    ERR_GUILD_HAS_DAILY_REWARD              = 4474,     --已经领取日常奖励
    ERR_GUILD_WAREHOUSE_PKG_FULL            = 4475,     --公会仓库满
    ERR_GUILD_WAREHOUSE_POINT_NOT_ENOUGH    = 4476,     --公会仓库积分不够
    ERR_GUILD_CFG_ERR                       = 4477,     --配置错误
    ERR_GUILD_WAREHOUSE_POS_NO_ITEM         = 4478,     --公会仓库格子没东西
    ERR_GUILD_RED_ENVELOPE_NOT_SEND         = 4479,     --红包还没有发送了
    ERR_GUILD_SEND_RED_ENVELOPE_VIP_LIMIT   = 4480,     --发红包不够vip等级
    ERR_GUILD_SEND_RED_ENVELOPE_COUNT_LIMIT = 4481,     --每天发红包次数达到限制
    ERR_GUILD_EXP_LIMIT                     = 4482,     --公会资金不足
    ERR_GUILD_QUIT_ON_ACTIVITY              = 4483,     --活动期间不能退出公会
    ERR_GUILD_QUIT_ON_ACTIVITY_OTHER        = 4484,     --活动期间不能退出公会，踢人对方
    ERR_GUILD_WAREHOUSE_BIND                = 4485,     --公会仓库，绑定不能捐献
    ERR_GUILD_SEND_RED_ENVELOPE_PACK_COUNT_LIMIT = 4486, --红包money需要大于红包数量
    --
    ERR_GUILD_BEAST_START_POSITION_ERR      = 4490,     --只有会长，副会长能开启
    ERR_GUILD_BEAST_START_TIME_ERR          = 4491,     --开启时间错误
    ERR_GUILD_BEAST_START_NUM_MAX           = 4492,     --本周开启次数最大了
    ERR_GUILD_BEAST_START_SOUL_NOT_ENOUGH   = 4493,     --开启灵魂不足
    ERR_GUILD_BEAST_START_HAS_START         = 4494,     --已经开启了
    ERR_GUILD_BEAST_NOT_START               = 4495,     --没开启不能进去
    ERR_GUILD_BEAST_HAS_NO_SOUL             = 4496,     --没有灵魂不能提交
    ERR_GUILD_GUARD_HAS_END                 = 4497,     --守卫工会活动已经结束
    ERR_GUILD_GUARD_NOT_OPEN                = 4498,     --守卫工会未开启
    ERR_GUILD_NOBODY                        = 4499,     --没人申请批个毛
    ERR_GUILD_REPLAY_APPLY_MEMBER_COUNT_MAX = 4500,     --满人快滚
    ERR_GUILD_OUT_OF_RANGE                  = 4501,     --批量满人快滚

    --成就 4801-4900
    ERR_ACHIEVEMENT_NOT_FINISH              = 4801,     --成就未完成
    ERR_ACHIEVEMENT_CFG_ERR                 = 4802,     --配置错误

    --天梯商店 4901-5000
    ERR_LADDER_SHOP_ITEM_ID_ERR                 = 4901,    --天梯商城购买物品id错误
    ERR_LADDER_SHOP_ITEM_PKG_FULL               = 4902,    --天梯商城购买时背包已满
    ERR_LADDER_SHOP_PRICE_CONFIG_ERR            = 4903,    --天梯商城购买价格未配置
    ERR_LADDER_SHOP_BUY_COUNT_WRONG             = 4904,    --天梯商城购买物品数量配置错误
    ERR_LADDER_SHOP_RANDOM_CONFIG_WRONG         = 4905,    --天梯商城随机库配置错误
    ERR_LADDER_MATCH_NOT_ON_TIME                = 4906,    --天梯赛还没有开始。天梯赛的配置在这里。

    --观战 5001-5100
    ERR_WATCH_WAR_NO_SPACE                      = 5001,    --没有副本
    ERR_WATCH_WAR_COUNT_LIMIT                   = 5002,    --超过观看人数

    --翅膀养成 5101-5200
    ERR_WING_CFG_ERR                              = 5101,    --配置错误
    ERR_WING_COST_NOT_ENOUGH                      = 5102,    --消耗的材料不足
    ERR_WING_STAR_MAX                             = 5103,    --星级满，请升等阶
    ERR_WING_STAR_NOT_MAX                         = 5104,    --星级未满，请升星
    ERR_WING_HAD_LOAD                             = 5105,    --已经佩戴
    ERR_WING_NO_LOAD                              = 5106,    --还没有佩戴
    ERR_WING_HOLE_NOT_OPEN                        = 5107,    --孔还没有够等阶开启
    ERR_WING_HOLE_HAS_GEM                         = 5108,    --孔已经有镶嵌宝珠
    ERR_WING_GEM_NO_GEM                           = 5109,    --宝珠不存在，背包中没有
    ERR_WING_GEM_ERR_GEM                          = 5110,    --宝珠不是翅膀宝珠
    ERR_WING_HOLE_NO_GEM                          = 5111,    --孔还没有镶嵌宝珠
    ERR_WING_GEM_UNLOAD_PKG_FULL                  = 5112,    --背包满，无法拆卸
    ERR_WING_GEM_QUALITY_DIFF                     = 5113,    --宝珠品质不一致
    ERR_WING_GEM_COMPOUND_PKG_FULL                = 5114,    --背包满，无法融合宝珠
    ERR_WING_GEM_RARE_HAS                         = 5115,    --已经拥有该稀有翅膀
    ERR_WING_GEM_RARE_NO                          = 5116,    --还没有拥有该稀有翅膀
    ERR_WING_GEM_RARE_LOAD                        = 5117,    --已经幻化了该翅膀
    ERR_WING_GEM_RARE_NO_LOAD                     = 5118,    --还没有幻化了该翅膀
    ERR_WING_LEVEL_MAX                            = 5119,    --已经满等阶

    --魂器系统5201-5300
    ERR_EQUIP_SOUL_NO_EQUIP                       = 5201, --没有魂器
    ERR_EQUIP_SOUL_NO_DATA                        = 5202, --配置错误，没有配置
    ERR_EQUIP_SOUL_NO_DATA_ATTRI                  = 5203, --配置错误，没有配置属性数据
    ERR_EQUIP_SOUL_MAX_LEVEL                      = 5204, --达到最大等级
    ERR_EQUIP_SOUL_EAT_SELF                       = 5205, --不能吃自己
    ERR_EQUIP_SOUL_NO_CFG_EXP                     = 5206, --配置错误，没有配置exp
    ERR_EQUIP_SOUL_COST_NOT_ENOUGH                = 5207, --重铸，不足够消耗道具
    ERR_EQUIP_SOUL_MAX_LOCK                       = 5208, --重铸，超过锁定最大数量

    --永恒之塔5301-5400
    ERR_AION_CFG_ERROR                  = 5301, --配置错误
    ERR_AION_NO_RESET_TIMES             = 5302, --重置次数不足
    ERR_AION_LAYER_ERR                  = 5303, --该层不能扫荡,未重置
    ERR_AION_SWEEP_COST_NOT_ENOUGH      = 5304, --扫荡需要的金币不足
    ERR_AION_NO_NEED_RESET              = 5305, --不需要重置
    ERR_AION_REWARD_CANNOT_GET          = 5306, --条件不够
    ERR_AION_TARGET_REWARD_ERR          = 5307, --配置没有该奖励
    ERR_AION_SWEEP_STATUS_ERROR         = 5308, --扫荡状态错误
    ERR_AION_TARGET_REWARD_CANNOT_GET   = 5309, --该奖励已经领取过
    ERR_AION_LOYER_MAX                  = 5310, --已到最高层了

    --升级 5401 - 5402
    ERR_ROLE_EXP_CFG_ERROR              = 5401, --经验配置错误
    ERR_ROLE_EXP_NO_REWARD              = 5402, --没有升级奖励

    --世界boss 5501-5600
    ERR_WORLD_BOSS_LEVEL_LIMIT          = 5501,    --等级不足不能进入
    ERR_WORLD_BOSS_PILAO_LIMIT          = 5502,    --疲劳值限制不能使用

    --声望系统 5601 - 5700
    ERR_PRESTIGE_LEVEL_NOT_ENOUGH           = 5601, --声望等级不够
    ERR_PRESTIGE_BUY_MONEY_NOT_ENOUGH       = 5602, --钱不够
    ERR_PRESTIGE_SHOP_NOT_EXIST             = 5603, --商店没有该物品
    ERR_PRESTIGE_SHOP_CFG_ERR               = 5604, --配置错误

    --世界boss 5701-5800
    ERR_DISSCUSSION_NOT_BOSS        = 5701,     --该boss不存在攻略讨论
    ERR_DISSCUSSION_TEXT_LEN        = 5702,     --发言超过遇到长度
    ERR_DISSCUSSION_NOT_EXIST       = 5703,     --评论不存在
    ERR_DISSCUSSION_SUPPORT_SELF    = 5704,     --不能为自己点赞

    --生活技能模块 5801-5900
    --ERR_LIFE_SKILL_TASK_NOT_ACCEPT        = 5801,     --玩家没这个任务
    --ERR_LIFE_SKILL_TASK_HAVE_FINISH       = 5802,     --已经完成任务了
    --ERR_LIFE_SKILL_TASK_NOT_CONFIG        = 5803,     --玩家没这个任务
    --ERR_LIFE_SKILL_TASK_NOT_ITEM          = 5804,     --该任务没这个请求任务
    --ERR_LIFE_SKILL_TASK_HAVE_SEEK         = 5805,     --该任务已经求助过了
    --ERR_LIFE_SKILL_TASK_ITEM_NOT_ENOUGH   = 5806,     --道具不足无法完成
    --ERR_LIFE_SKILL_TASK_NOT_GUILD         = 5807,     --没有公会
    --ERR_LIFE_SKILL_TASK_NOT_DONATE_INFO   = 5808,     --没有对应的捐赠请求
    --ERR_LIFE_SKILL_TASK_DONATE_NOT_ITEM   = 5809,     --没有相应的道具无法捐赠
    --ERR_LIFE_SKILL_TASK_ITEM_NOT_MATCH    = 5810,     --道具不匹配
    --ERR_LIFE_SKILL_TASK_DONATE_SELF       = 5811,     --自己给自己捐赠

    --深渊玩法 5901-6000
    --ERR_ABYSS_COST_NOT_ENOUGH             =5901,      --深渊钥匙不足

    --占星 6001 - 7000
    ERR_DIVINE_DAY_NUM_ERR              = 6001, -- 每天次数超过限制
    ERR_DIVINE_COST_ERR                 = 6002, -- 占星消耗不够
    ERR_DIVINE_REWARD_NUM_ERR           = 6003, -- 累计奖励次数不够
    ERR_DIVINE_REWARD_ALREADY_GET       = 6004, -- 该累计奖励已经领取
    ERR_DIVINE_CFG_ERR                  = 6005, -- 配置错误

    --赏金赛 7001 = 8000
    --ERR_BOUNTY_LEVEL_ERR = 7001,     --等级不够
    --ERR_BOUNTY_HAS_NO_TICKET = 7002, --没门票
    --ERR_BOUNTY_APPLY_REPEAT = 7003,        --重复报名
    --ERR_BOUNTY_NOT_APPLY = 7004,    --没报名
    --ERR_BOUNTY_REMATCH = 7005,      --重复匹配
    --ERR_BOUNTY_MATCH_REWARD_ERR = 7006, --没有可领取的奖励
    --ERR_BOUNTY_MATCH_NOT_GET_REWARD = 7007, --先领取奖励

    --随从系统 8001 - 8100
    --ERR_PKG_DUDE_HAS_FULL               = 8001,  --随从背包已满
    --ERR_DUDE_RANK_ALREADY_MAX           = 8002, --随从职称已到最高
    --ERR_DUDE_ASSIST_AVATAR_ERR          = 8003, --求助的玩家该随从没有设置出战
    --ERR_DUDE_SET_ASSIST_POS_ERR         = 8004, --位置错误 该位置没有随从
    --ERR_DUDE_START_POS_ERR              = 8005,  --开始任务选择的随从位置有误
    --ERR_DUDE_START_ASSIST_AVATAR_ERR    = 8006, --求助的玩家不是你的好友和公会成员
    --ERR_DUDE_START_ASSIST_AVATAR_NOT_ASSIST = 8007, ----求助的玩家没有助战的随从
    --ERR_DUDE_TASK_HAS_NOT_RECEIVED      =  8008, --任务没领取
    --ERR_DUDE_TASK_HAS_RECEIVED          = 8009, --该任务已领取
    --ERR_DUDE_TASK_NOT_IN_PROGRESS       = 8010, --任务不在进行中
    --ERR_DUDE_TASK_HAS_FINISHED          = 8011, --任务已完成
    --ERR_DUDE_TASK_NOT_IN_FREE_LIST      = 8012, --不在列表，不能领取
    --ERR_DUDE_TASK_NOT_FINISH_BEFORE_TASK = 8013, --先完成未完成的任务
    --ERR_DUDE_UPGRADE_RANK_COST_ERR      = 8014, --升职称消耗不足
    --ERR_DUDE_TASK_DUDE_NUM_LIMIT_ERR    = 8015, --出战的数量超过限制
    --ERR_DUDE_TASK_HAS_NOT_FINISHED      = 8016, --该任务还没完成
    --ERR_DUDE_TASK_REWARD_REPEAT_GET     = 8017, --该奖励已领取过了
    --ERR_DUDE_CANCEL_ASSIST_POS_ERR      = 8018, --取消助战位置错误
    --ERR_DUDE_ADD_SPEED_COST_NOT_ENOUGH  = 8019, --加速消耗不够
    --ERR_DUDE_ADD_SPEED_NUM_NOT_NEED     = 8020, --
    --ERR_DUDE_TASK_REFRESH_COST_NOT_ENOUGH = 8021 ,--刷新消耗不足
    --ERR_DUDE_TASK_RECEIVE_NUM_ERR       = 8022, --今日任务可做次数达到限制
    --ERR_DUDE_PARTY_NOT_UNLOCK           = 8023, --该营地未解锁
    --ERR_DUDE_PARTY_REWARD_ALREADY_GET   = 8024, --奖励已领取
    --ERR_DUDE_PARTY_NOT_THIS_REWARD      = 8025, --没有该奖励

    --坐骑系统 8101 - 8200
    --ERR_RIDE_PKG_FULL                   = 8101,  --坐骑背包已满
    --ERR_RIDE_NOT_HAS                    = 8102,  --pos没有坐骑
    --ERR_RIDE_BUY_FULL                   = 8103,  --达到购买格子上限
    --ERR_RIDE_COST_NOT_ENOUGH            = 8104,  --购买格子不够消耗道具
    --ERR_RIDE_NOT_ACTIVE                 = 8105,  --还没有激活坐骑
    --ERR_RIDE_DEL_ACTIVE                 = 8106,  --已激活，无法删除
    --ERR_RIDE_DEL_RIDE                   = 8107,  --已骑，无法删除
    --ERR_RIDE_HAS_ACTIVE                 = 8108,  --已激活，无法再激活

    --翅膀 法宝 神兵 披风 8201 - 8300
    ERR_TREASURE_LEVEL_MAX              = 8201,  --宝物等级到最大了
    ERR_TREASURE_USE_NUM_MAX            = 8202,  --达到使用上限

    --宠物 8301 - 8400
    ERR_PET_NO_ITEM_BLESS               = 8301,  --配置错误，没有配祝福值
    ERR_PET_FULL_STAR_GRADE             = 8302,  --满阶满星了
    ERR_PET_FULL_LEVEl                  = 8303,  --满级了
    ERR_PET_NO_ITEM_EXP                 = 8304,  --配置错误，没有配经验值
    ERR_PET_ITEM_LIMIT                  = 8305,  --使用道具达到上限
    ERR_PET_NO_ITEM_LIMIT               = 8306,  --配置错误，没有配置道具使用上限
    ERR_PET_NOT_EQUIP                   = 8307,  --道具不是装备

    --坐骑 8401 - 8500
    ERR_HORSE_NO_ITEM_BLESS               = 8401,  --配置错误，没有配祝福值
    ERR_HORSE_FULL_STAR_GRADE             = 8402,  --满阶满星了
    ERR_HORSE_ITEM_LIMIT                  = 8403,  --使用道具达到上限
    ERR_HORSE_NO_ITEM_LIMIT               = 8404,  --配置错误，没有配置道具使用上限

    --pk模式 8501 - 8600
    ERR_PK_MODE_MAP_LIMIT                 = 8501,    --场景不允许设置pk模式
    ERR_PK_MODE_LEVEL_LIMIT               = 8502,    --等级限制不允许设置pk模式
    ERR_PK_VALUE_LESS                     = 8503,    --PK值较少

    --称号系统 8601-8700
    ERR_TITLE_NO_TITLE                    = 8601,    --还没拥有该称号

    --爵位系统 8701-8800
    ERR_NOBILITY_RANK_MAX           = 8701, --等级最大了
    ERR_NOBILITY_FIGHT_FORCE_ERR    = 8702, --升爵位所需战斗力不足

    --复活机制 8801-8900
    ERR_MGR_REVIVE_CAN_REVIVE_TIME        = 8801,  --还没到可复活时间
    ERR_MGR_REVIVE_NO_MONEY               = 8802,  --原地复活不够钱
    ERR_MGR_REVIVE_IS_ALIVE               = 8803,  --人在活着啊
    ERR_MGR_REVIVE_NO_RES                = 8804,  --没有占领到资源点

    --装备强化 8901-8950
    ERR_EQUIP_STREN_CFG_ERR                = 8901,  --配置错误
    ERR_EQUIP_STREN_FULL_LEVEL             = 8902,  --等级满
    ERR_EQUIP_STREN_POS_ERR                = 8903,  --装备部位不对

    --装备洗练 8951-9000
    ERR_EQUIP_WASH_CFG_ERR                = 8951,  --配置错误
    ERR_EQUIP_WASH_LEVEL_LIMIT            = 8952,  --等级限制
    ERR_EQUIP_WASH_LOCK_ERR               = 8953,  --不能全锁定
    ERR_EQUIP_WASH_SPECIAL_ITEM_ERR       = 8954,  --特殊道具错误
    ERR_EQUIP_WASH_OPEN_SLOT_FULL         = 8955,  --解锁满
    ERR_EQUIP_WASH_OPEN_SLOT_VIP          = 8956,  --需要vip6

    --装备宝石 9001-9050
    ERR_EQUIP_GEM                         = 9001,  --配置错误
    ERR_EQUIP_GEM_UNLOAD_PKG_FULL         = 9002,  --拆宝石，背包满
    ERR_EQUIP_GEM_HOLE_NOT_OPEN           = 9003,  --孔还没开
    ERR_EQUIP_GEM_HOLE_HAS_GEM            = 9004,  --已经镶嵌有宝石
    ERR_EQUIP_GEM_NO_ITEM                 = 9005,  --道具不存在
    ERR_EQUIP_GEM_NOT_GEM                 = 9006,  --不是宝石
    ERR_EQUIP_GEM_EQUIP_POS_ERR           = 9007,  --镶嵌的部位不对
    ERR_EQUIP_GEM_NO_GEM                  = 9008,  --还没有镶嵌宝石
    ERR_EQUIP_GEM_NO_ITEM_EXP             = 9009,  --配置错误
    ERR_EQUIP_GEM_POS_ERR                 = 9010,  --部位不对
    ERR_EQUIP_GEM_FULL_LEVEL              = 9011,  --满级

    --天赋9101 - 9150
    ERR_TALENT_CFG_ERR                  = 9101, --配置错误
    ERR_TALENT_LEVEL_MAX                = 9102, --天赋等级最大
    ERR_TALENT_RESET_NOT_NEED           = 9103, --不需要重置
    ERR_TALENT_CONDITION_NOT_REACH      = 9104, --天赋学习条件没达到

    --守护结界塔 9151-9200
    ERR_HOLY_TOWER_CFG_ERR              = 9151, --配置错误
    ERR_HOLY_TOWER_ENTER_NUM_NOT_ENOUGH = 9152, --进副本次数不够
    ERR_HOLY_TOWER_BUY_NUM_NOT_ENOUGH   = 9153, --购买次数不足
    ERR_HOLY_TOWER_BUY_NUM_COST_NOT_ENOUGH = 9154,--买次数消耗不够
    ERR_HOLY_TOWER_SWEEP_NOT_PASS       = 9155, --未通关不能扫荡呀
    ERR_HOLY_TOWER_SWEEP_COST_NOT_ENOUGH = 9156, --扫荡消耗不足

    --符文系统 9201-9250
    ERR_RUNE_XML_ERROR                  = 9201,     --配表错误
    ERR_RUNE_PKG_POSI_PARAM_ERR         = 9202,     --背包位参数超出有效范围
    ERR_RUNE_GRID_POSI_PARAM_ERR        = 9203,     --符文位参数超出有效范围
    ERR_RUNE_GRID_IS_LOCK               = 9204,     --符文格子锁定（未解锁）
    ERR_RUNE_LIMIT_PUTON_TYPE           = 9205,     --不允许穿戴的符文类型（如：精华符文）
    ERR_RUNE_NOT_ALOW_REPEAT_TYPE       = 9206,     --不允许穿戴同类型符文（一种类型符文只能穿一个）
    ERR_RUNE_PKG_NOT_EMPTY_SPACE        = 9207,     --背包没有空余格子,标明是符文背包
    ERR_RUNE_PKG_GRID_IS_EMPTY          = 9208,     --该符文背包格子为空
    ERR_RUNE_PKG_GRID_NOT_EMPTY         = 9209,     --该符文背包格子已穿戴
    --ERR_RUNE_GRID_GRID_IS_EMPTY         = 9210,     --该符文格子为空
    --ERR_RUNE_GRID_GRID_NOT_EMPTY        = 9211,     --该符文格子已穿戴
    ERR_RUNE_UPLEVEL_IS_TOP             = 9212,     --符文已升级到最高
    --ERR_RUNE_UPLEVEL_PIECE_LESS         = 9213,     --符文升级所需消耗精华不够
    --ERR_RUNE_EXCHANGE_PIECE_LESS        = 9214,     --符文兑换所需消耗精华不够
    ERR_RUNE_EXCHANGE_IS_LOCK           = 9215,     --该符文不允许兑换（未解锁状态）
    ERR_RUNE_LUCK_DIAMOND_NOT_ENOUGH    = 9216,     --符文寻宝钻石不足
    ERR_RUNE_LUCK_FREE_PKG_NOT_ENOUGH   = 9217,     --符文背包空格不够

    --合成系统 9301-9350
    ERR_COMPOSE_XML_ERROR               = 9301,     --配表错误
    ERR_COMPOSE_ITEM_PKG_FULL           = 9302,     --合成道具时，背包满了
    ERR_COMPOSE_EQUIP_LEVEL_LIMIT       = 9303,     --合成装备等级限制
    ERR_COMPOSE_EQUIP_USE_GRID_IS_EMPTY = 9304,     --合成装备指定消耗的格子为空（未正确选择合成原料）
    ERR_COMPOSE_EQUIP_VOCATION_ERROR    = 9305,     --合成装备职业数据错误
    ERR_COMPOSE_EQUIP_TYPE_ERROR        = 9306,     --合成装备的装备类型限定不符合
    ERR_COMPOSE_EQUIP_LIMIT_ERROR       = 9307,     --合成装备的限定条件不符合
    ERR_COMPOSE_EQUIP_NOT_EQUIP         = 9308,     --合成装备的物品非装备（不判断指定的神源元）
    ERR_COMPOSE_EQUIP_FAIL              = 9309,     --合成装备失败

    --循环任务 9351-9400
    ERR_CIRCLE_TASK_CFG_ERR                          = 9351,     --配置错误
    ERR_CIRCLE_TASK_HAS                              = 9352,     --已经有循环任务
    ERR_CIRCLE_TASK_NOT_HAS                          = 9353,     --没有循环任务
    ERR_CIRCLE_TASK_ALL_COMPLETED                    = 9354,     --循环任务已全部完成
    ERR_CIRCLE_TASK_NOT_HAS_COMPLETED                = 9355,     --没有已经完成的循环任务
    ERR_CIRCLE_TASK_NOT_HAS_ACCEPT                   = 9356,     --没有已接未完成的循环任务

    --boss之家 9401- 9450
    ERR_BOSS_HOME_CFG_ERR            = 9401,     --配置错误
    ERR_BOSS_HOME_ENTER_COST_ERR     = 9402,     --vip等级过低进副本消耗不足

    --个人boss 9451 - 9500
    ERR_PERSONAL_BOSS_CFG_ERR              = 9451, --配置错误
    ERR_PERSONAL_BOSS_PARAM_ERR            = 9452, --参数错误
    ERR_PERSONAL_BOSS_ENTER_COST_ERR       = 9453, --消耗不足
    ERR_PERSONAL_BOSS_ENTER_NUM_NOT_ENOUGH = 9454, --进入次数不足

    --市场 9501-9550
    --ERR_SALE_SELL_NOT_ENOUGH                        = 9401, --交易行道具不足
    --ERR_SALE_CAN_NOT_SELL                           = 9402, --该道具不能出售
    --ERR_SALE_ITEM_BIND                              = 9403, --道具已经绑定不能出售
    --ERR_SALE_GRID_FULL                              = 9405, --格子满了
    --ERR_SALE_LIMIT_ITEM                           = 9406, --未允许上架的物品或道具

    --金币副本 9501 - 9550
    ERR_INSTANCE_GOLD_CFG_ERR                   = 9501, --配置错误
    ERR_INSTANCE_GOLD_ENTER_NUM_NOT_ENOUGH      = 9502, --进副本次数不够
    ERR_INSTANCE_GOLD_BUY_NUM_NOT_ENOUGH        = 9503, --购买次数不足
    ERR_INSTANCE_GOLD_BUY_NUM_COST_NOT_ENOUGH   = 9504, --买次数消耗不够
    ERR_INSTANCE_GOLD_SWEEP_NOT_PASS            = 9505, --未通关不能扫荡呀
    ERR_INSTANCE_GOLD_SWEEP_COST_NOT_ENOUGH     = 9506, --扫荡消耗不足

    --离线1v1 9551 - 9600
    ERR_OFFLINE_PVP_NO_REWARD                   = 9551, --已经领奖或者没有奖可领
    ERR_OFFLINE_PVP_INSPIRE_LIMIT               = 9552, --鼓舞次数达到上限
    ERR_OFFLINE_PVP_BUY_LIMIT                   = 9553, --购买次数达到上限
    ERR_OFFLINE_PVP_COUNT_LIMIT                 = 9554, --次数达到上限
    ERR_OFFLINE_PVP_PARAM_ERR                   = 9555, --参数错误
    ERR_OFFLINE_PVP_RANK_CHANGE                 = 9556, --排行榜有变化，重新刷新数据

    --守护女神 9601 - 9650
    ERR_GUARD_GODDESS_CFG_ERR                    = 9601, --配置错误
    ERR_GUARD_GODDESS_COUNT_LIMIT                = 9602, --次数上限
    ERR_GUARD_GODDESS_HAS                        = 9603, --在守护女神状态
    ERR_GUARD_GODDESS_NO                         = 9604, --不在守护女神状态

    --装备守护部位 9651 - 9700
    ERR_EQUIP_GODDESS_CFG_ERR                    = 9651, --配置错误
    ERR_EQUIP_GUARD_HAS_TIME                     = 9652, --还有时间
    ERR_EQUIP_GUARD_NO_ITEM                      = 9653, --没找到装备
    ERR_EQUIP_GUARD_NO_TYPE                      = 9654, --不是守护位置的装备

    --活动模块 9701 - 9750
    ERR_ACTIVITY_CFG_ERR                         = 9701, --配置错误
    ERR_ACTIVITY_POINT_LIMIT                     = 9702, --活跃点限制
    ERR_ACTIVITY_HAS_POINT_REWARD                = 9703, --已领取活跃奖励
    ERR_ACTIVITY_LEVEL_LIMIT                     = 9704, --等级不足
    ERR_ACTIVITY_FULL_LEVEL                      = 9705, --满级了
    ERR_ACTIVITY_NOT_ENOUGH_POINT                = 9706, --经验不足

    --VIP系统 9751 - 9800
    ERR_NOT_VIP                                  = 9751, --非VIP玩家
    ERR_VIP_OUTTIME                              = 9752, --VIP过期
    ERR_NOT_REAL_VIP                             = 9753, --非正式VIP（VIP体验卡）
    ERR_VIP_LEVEL_REWARD_GETED                   = 9754, --VIP等级奖励已领取
    ERR_VIP_WEEK_REWARD_GETED                    = 9755, --VIP周奖励已领取
    ERR_VIP_LEVEL_REWARD_LIMIT                   = 9756, --VIP等级不足(领取VIP奖励)
    ERR_VIP_DAILY_REWARD_RECLAIM                 = 9757, --已领取最高档奖励

    --蛮荒禁地 9801-9850
    ERR_FORBIDDEN_CFG_ERR                   = 9801,
    ERR_FORBIDDEN_ENTER_TIMES_NOT_ENOUGH    = 9802, --次数不足
    ERR_FORBIDDEN_ENTER_COST_NOT_ENOUGH     = 9803, --进入消耗不足

    --祈福  9851  -  9900
    ERR_PRAY_GOLD_OVER_MAX_TIMES                 = 9851, --金币祈福超出次数
    ERR_PRAY_GOLD_DIAMOND_NOT_ENOUGH             = 9852, --金币祈福钻石不够
    ERR_PRAY_EXP_OVER_MAX_TIMES                  = 9853, --经验祈福超出次数
    ERR_PRAY_EXP_DIAMOND_NOT_ENOUGH              = 9854, --经验祈福钻石不够

    --装备进阶 9901  -- 9950
    ERR_EQUIP_ADVANCE_BIND                       = 9901, --装备绑定不能进阶
    ERR_EQUIP_ADVANCE_CAN_NOT                    = 9902, --未允许进阶的装备
    ERR_EQUIP_ADVANCE_NOT_EXISTS                 = 9903, --进阶后获取装备不存在

    --跨服实时1v1 9951-9999
    ERR_CROSS_VS11_NO_LEFT_COUNT                 = 9951, --已经没有剩余次数进行今日跨服实时1v1了
    ERR_CROSS_VS11_NOT_CROSS_BUY                 = 9952, --只有跨服匹配才能购买额外次数
    ERR_CROSS_VS11_MAX_BUY_COUNT                 = 9953, --已达今日最大可购买次数
    ERR_CROSS_VS11_ERR_BUY_COUNT                 = 9954, --购买次数错误
    ERR_CROSS_VS11_BUY_LESS_COUPONS              = 9955, --购买次数需要的绑钻或者钻石不足
    ERR_CROSS_VS11_DAY_STAGE_RWD_GOT             = 9956, --今日段位奖励已经领取过了
    ERR_CROSS_VS11_NO_DAY_STAGE_RWD              = 9957, --你没有昨日的段位奖励可以领取
    ERR_CROSS_VS11_NO_DAY_COUNT_RWD              = 9958, --你没有参与场次奖励可以领取
    ERR_CROSS_VS11_DAY_COUNT_RWD_GOT             = 9959, --你已经领过这个参与奖励了
    ERR_CROSS_VS11_ERR_DAY_COUNT_ID              = 9960, --没有这个参与场次的奖励
    ERR_CROSS_VS11_SEASON_GX_RWD_GOT             = 9961, --你已经领过这个功勋奖励了
    ERR_CROSS_VS11_ERR_SEASON_GX_ID              = 9962, --没有这个功勋值的奖励
    ERR_CROSS_VS11_LESS_GX                       = 9963, --功勋值不够,不能领取这个奖励
    ERR_CROSS_VS11_NOT_OPEN_TIME                 = 9964, --未到开放时间

    --装备套装  10001  --  10050
    ERR_EQUIP_SUIT_POS_ERR                       = 10001,   --装备部位不对
    ERR_EQUIP_SUIT_LEVEL_ERROR                   = 10002,   --无效操作（套装等级不对）
    ERR_EQUIP_SUIT_ITEM_CFG_ERROR                = 10003,   --无效操作（套装装备升级数据有误）
    ERR_EQUIP_SUIT_CFG_ERROR                     = 10004,   --无效操作（套装表数据有误）
    ERR_EQUIP_SUIT_BACK_GRID_NOT_ENOUGH          = 10005,   --背包格子不够返还消耗

    --资源找回 10051 - 10100
    ERR_REWARD_BACK_CFG_ERR                     = 10051, --配置错误
    ERR_REWARD_BACK_ACTIVITY_NOT_OPEN           = 10052, --找回的activity未开启
    ERR_REWARD_BACK_NOT_ENOUGH_BACK_NUM         = 10053, --没有剩余的可找回次数
    ERR_REWARD_BACK_COST_NOT_ENOUGH             = 10054, --资源找回消耗不足

    --图鉴错误码 10101 - 10150
    ERR_CARD_BAG_NOT_EMPTY_GRID                 = 10101, --图鉴碎片格子已满
    ERR_CARD_CFG_NOT_EXIST                      = 10102, --该图鉴配置不存在(没有此图鉴)
    ERR_CARD_LEVEL_CFG_NOT_EXIST                = 10103, --该图鉴升级配置不存在
    ERR_CARD_LEVEL_IS_MAX                       = 10104, --该图鉴已达最高等级
    ERR_CARD_UPLEVEL_PIECE_NOT_ENOUGH           = 10105, --图鉴激活（升级）时，碎片不够
    ERR_CARD_UPLEVEL_LEVEL_LIMIT                = 10106, --图鉴激活（升级）时，角色等级不够
    ERR_CARD_GROUP_CFG_NOT_EXIST                = 10107, --该图鉴组合配置表不存在
    ERR_CARD_GROUP_LEVEL_IS_MAX                 = 10108, --该图鉴组合已达最高等级
    ERR_CARD_GROUP_UPLEVEL_STAR_LIMIT           = 10109, --该图鉴组合升级条件未达到
    ERR_CARD_UPLEVEL_EXP_NOT_ENOUGH             = 10110, --图鉴激活（升级）时，经验不够
    ERR_CARD_RESOLVE_INVALID_POS                = 10111, --分解图鉴碎片参数错误（位置没东西）
    ERR_CARD_RESOLVE_INVALID_XML_DATA           = 10112, --分解图鉴碎片表数据有误（这个碎片没配经验）

    --化形10151 - 10200
    ERR_ILLUSION_CFG_ERR                    = 10151, --配置错误
    ERR_ILLUSION_HAS_ACTIVATE               = 10152, --该化形已激活
    ERR_ILLUSION_NOT_ACTIVATE               = 10153, --未激活
    ERR_ILLUSION_HAS_SHOW                   = 10154, --该化形已幻化
    ERR_ILLUSION_STAR_MAX                   = 10155, --最大星级了
    ERR_ILLUSION_LEVEL_MAX                  = 10156, --最大等级了
    ERR_ILLUSION_LEVEL_UP_EXP_NOT_ENOUGH    = 10157, --升级经验不够
    ERR_ILLUSION_BREAK_COUNT_ERR            = 10158, --请选择需要分解的材料
    ERR_ILLUSION_BREAK_POS_NO_ITEM          = 10159, --选的格子没有道具
    ERR_ILLUSION_BREAK_ITEM_ERR             = 10160, --不是化形材料
    ERR_ILLUSION_BREAK_COND_ERR             = 10161, --已激活且满星的才能分解
    ERR_ILLUSION_GRADE_MAX                  = 10162, --最大阶级了
    ERR_ILLUSION_ACTIVATE_VOCATION_ERR      = 10163, --激活化形需要的转职要求没达到

    --拍卖行 摆摊 10201 - 10250
    ERR_AUCTION_CFG_ERR                     = 10201, --配置错误
    ERR_AUCTION_ITEM_BIND                   = 10202, --道具绑定不能出售
    ERR_AUCTION_ITEM_NOT_EXIST              = 10203, --交易行道具不存在
    ERR_AUCTION_GRID_FULL                   = 10204, --格子满了
    ERR_AUCTION_CAN_NOT_SELL                = 10205, --该道具不能出售
    --ERR_AUCTION_MONEY_NOT_ENOUGH          = 10206, --交易行金钱不足
    ERR_AUCTION_BUY_SELF_ITEM               = 10207, --不能买自己出售的道具
    ERR_AUCTION_OWN_NOT_MATCH               = 10208, --不是该道具的拥有者
    ERR_AUCTION_PASSWD_VIP_LIMIT            = 10209, --vip4以上才能设置passwd
    ERR_AUCTION_PASSWD_WRONG                = 10210, --passwd不对
    ERR_AUCTION_PRICE_ATLEAST_2             = 10211, --价钱至少为2

    --拍卖行 求购 10251 - 10300
    ERR_BEG_CAN_NOT_PUBLISH                = 10251, --该道具不能发布
    ERR_BEG_COUNT_MAX_STACK                = 10252, --超过道具叠加数
    ERR_BEG_ITEM_NOT_EXIST                 = 10253, --交易行道具不存在
    ERR_BEG_GRID_FULL                      = 10254, --格子满了
    ERR_BEG_OWN_NOT_MATCH                  = 10255, --不是该道具的拥有者
    ERR_BEG_SELL_SELF_ITEM                 = 10256, --不能出售自己发布的道具

    --青云之巅 10301 - 10350
    ERR_CLOUD_PEAK_OPEN_TIME_ERR            = 10301, --未到开启时间
    ERR_CLOUD_PEAK_HAS_PASS_ALL             = 10302, --已经通关全部了

    --周任务 10351-10400
    ERR_WEEK_TASK_CFG_ERR                   = 10351,     --配置错误
    ERR_WEEK_TASK_HAS                       = 10352,     --已经有循环任务
    ERR_WEEK_TASK_NOT_HAS                   = 10353,     --没有循环任务
    ERR_WEEK_TASK_ALL_COMPLETED             = 10354,     --循环任务已全部完成
    ERR_WEEK_TASK_NOT_HAS_COMPLETED         = 10355,     --没有已经完成的循环任务
    ERR_WEEK_TASK_NOT_HAS_ACCEPT            = 10356,     --没有已接未完成的循环任务

    --守护系统（灵宠） 10401-10450
    ERR_GUARDIAN_OUTOF_CFG                  = 10401,    --不存在的灵宠配置（非法操作）
    ERR_GUARDIAN_UPLEVEL_ONLOCK             = 10402,    --激活条件不满足
    ERR_GUARDIAN_UPLEVEL_ISMAX              = 10403,    --当前守护已达最高等级(未配置到达下一阶消耗)
    ERR_GUARDIAN_UPLEVEL_OVERMAX            = 10404,    --当前守护已达最高等级(无下一阶属性配置)
    ERR_GUARDIAN_SELECT_ERROR               = 10405,    --选择了不允许的出战
    ERR_GUARDIAN_EXPLODE_PARAM_ERROR        = 10406,    --分解指定了非法参数
    ERR_GUARDIAN_EXPLAIN_PARAM_EMPTY        = 10407,    --分解结晶参数有误(参数为空)
    ERR_GUARDIAN_EXPLAIN_PARAM_CANOT        = 10408,    --分解结晶参数有误（未激活守护，激活所需材料，不允许分解）
    ERR_GUARDIAN_EXPLAIN_PARAM_OUTCFG       = 10409,    --分解结晶参数有误（指定了分解道具配置以外的item_id）

    --公会篝火 10451-10500
    ERR_GUILD_BONFIRE_NOT_OPEN                        = 10451,     --活动还没有开始
    ERR_GUILD_BONFIRE_NO_GUILD                        = 10452,     --还没有加入工会
    ERR_GUILD_BONFIRE_NOT_ANSWER_STATE                = 10453,     --不在答题时间
    ERR_GUILD_BONFIRE_HAD_GIFT                        = 10454,     --已经领取礼包


    --转职（四转） 10501 - 10550
    ERR_VOC_CHANGE_FOUR_STAGE_CFG_ERROR             = 10501,    --激活的阶段不存在
    ERR_VOC_CHANGE_FOUR_STAGE_VAL_ERROR             = 10502,    --激活阶段的顺序不对（先升1级再升2级）
    ERR_VOC_CHANGE_FOUR_ACTIVATE_COST_NOT_ENOUGHT   = 10503,    --激活所需消耗不足
    ERR_VOC_CHANGE_FOUR_REPEAT_APPLY                = 10504,    --重复转生请求(已转生成功)
    ERR_VOC_CHANGE_FOUR_APPLY_STAGE_REFUSE          = 10505,    --转生请求被拒绝（未达到满阶条件）
    ERR_VOC_CHANGE_FOUR_APPLY_LEVEL_REFUSE          = 10506,    --转生请求被拒绝（角色未达到指定等级）
    ERR_VOC_CHANGE_FOUR_APPLY_EXP_REFUSE            = 10507,    --转生请求被拒绝（转身所需升到371级的经验不足）
    ERR_VOC_CHANGE_FOUR_APPLY_NOTTHREE_REFUSE       = 10508,    --非法请求（当前不为第三转）

    --采集 10551 - 10600
    ERR_COLLECT_TIMES_LIMIT_ERR         = 10551, --采集次数到达上限
    ERR_COLLECT_BY_OTHERS               = 10552, --正在被其他人采集
    ERR_COLLECT_CANNOT_COLLECT          = 10553, --配置错误，或不是可采集物
    ERR_COLLECT_PRE_TIME_NOT_ENOUGH     = 10554, --采集读条时间不够
    ERR_COLLECT_SPACE_MARRY_MAX         = 10555, --结婚场景采集次数到达上限
    ERR_BATTLE_SOUL_COLLECT_MAX         = 10556, --战场之魂宝箱本轮采集次数已满
    ERR_COLLECT_NOTIFY_COUNT            = 10557, --剩余采集 {}/{}

    --答题系统 10601-10650
    ERR_QUESTION_NOT_OPEN                          = 10601,     --活动还没有开始
    ERR_QUESTION_HAD_ANSWER                        = 10602,     --已经回答
    ERR_QUESTION_NOT_ANSWER_STATE                  = 10603,     --不在答题时间
    ERR_QUESTION_ANSWER_TIME_NOT                   = 10604,     --答题时间未到
    ERR_QUESTION_ANSWER_TIME_PASS                  = 10605,     --答题时间已过
    ERR_QUESTION_QUESTION_ID_ERR                   = 10606,     --客户端发送的题目id错误

    --公会争霸 10651-10700
    ERR_GUILD_CONQUEST_NOT_OPEN                    = 10651,     --活动还没有开始
    ERR_GUILD_CONQUEST_NO_RIGHT                    = 10652,     --没有在对战列表中，没有资格
    ERR_GUILD_CONQUEST_NO_FIGHT                    = 10653,     --轮空，没有对手。已经打完。你的工会，本轮已结束。
    ERR_GUILD_CONQUEST_NEED_TIME                   = 10654,     --入会时间不足

    --塔防副本 10701 - 10750
    ERR_TOWER_DEFENSE_ENTER_NUM_NOT_ENOUGH          = 10701,    --进入次数不足
    ERR_TOWER_DEFENSE_BUY_NUM_NOT_ENOUGH            = 10702,    --购买次数不足
    ERR_TOWER_DEFENSE_SWEEP_COST_NOT_ENOUGH         = 10703,    --扫荡消耗不足
    ERR_TOWER_DEFENSE_CALL_MONSTER_COST_NOT_ENOUGH  = 10704,    --召唤黄金怪消耗不足
    ERR_TOWER_DEFENSE_WAVE_CALL_HAS_MAX             = 10705,    --当前波召唤数量达到限制
    ERR_TOWER_DEFENSE_POINT_HAS_TOWER               = 10706,    --该点已经有塔了
    ERR_TOWER_DEFENSE_TOWER_TYPE_NUM_MAX            = 10707,    --该类型塔建造数量超过限制了
    ERR_TOWER_DEFENSE_CFG_ERR                       = 10708,    --配置错误

    --7天登录 10751 - 10800
    ERR_REWARD_SEVENDAY_REWARD_NOT_EXIST                = 10751,    --奖励不存在
    ERR_REWARD_SEVENDAY_NOT_ENOUGH                      = 10752,    --还不能领取
    ERR_REWARD_SEVENDAY_GOT                             = 10753,    --已领取

    --龙魂 10801 - 18051
    ERR_DRAGON_SPIRIT_COMPOSE_CFG_NOT_EXIST         = 10801,    --龙魂合成配置不存在(非法请求)
    ERR_DRAGON_SPIRIT_NOT_FREE_BODY_POSI            = 10802,    --没有可穿戴的空格子
    ERR_DRAGON_SPIRIT_BAG_IS_FULL                   = 10803,    --龙魂背包已满
    ERR_DRAGON_SPIRIT_COMPOSE_LEVEL_LIMIT           = 10804,    --合成龙魂等级限制
    ERR_DRAGON_SPIRIT_COMPOSE_RND_ERROR             = 10805,    --合成失败（概率失败）
    ERR_DRAGON_SPIRIT_COMPOSE_ITEM_BAG_NOT_ENOUGH   = 10806,    --合成时，背包空格不足返还
    ERR_DRAGON_SPIRIT_COMPOSE_COST_NOT_ENOUGH       = 10807,    --合成时，需消耗的龙魂不足
    ERR_DRAGON_SPIRIT_BAG_POSI_IS_EMPTY             = 10808,    --指定的背包格中没有龙魂
    ERR_DRAGON_SPIRIT_BODY_POSI_IS_EMPTY            = 10809,    --指定的龙魂（已穿戴）格中没有龙魂
    ERR_DRAGON_SPIRIT_LEVEL_CFG_NOT_EXIST           = 10810,    --龙魂属性配置不存在(查表)
    ERR_DRAGON_SPIRIT_LEVEL_IS_TOP                  = 10811,    --龙魂已升级到最高级
    ERR_DRAGON_SPIRIT_RESOLVE_ITEM_BAG_NOT_ENOUGH   = 10812,    --分解时，背包空格不足返还
    ERR_DRAGON_SPIRIT_RESOLVE_ITEM_NOT_SINGLE_PROP  = 10813,    --分解的龙魂不为单属性(非单属性不能分解)
    ERR_DRAGON_SPIRIT_RESOLVE_CFG_NOT_WRITE         = 10814,    --要分解的龙魂，没有配置分解返回字段（配表问题）
    ERR_DRAGON_SPIRIT_RESTORE_POSI_IS_EMPTY         = 10815,    --拆解指定的格子为空
    ERR_DRAGON_SPIRIT_RESTORE_ITEM_NOT_DOBULE_PROP  = 10816,    --拆解的龙魂不为双属性(非单属性不能拆解)
    ERR_DRAGON_SPIRIT_RESTORE_COMPOSECFG_NOT_SET    = 10817,    --要拆解的龙魂，没有对应的合成配置
    ERR_DRAGON_SPIRIT_RESTORE_NOT_BAG_TOBACK        = 10818,    --拆解时，龙魂背包不足以返回龙魂
    ERR_DRAGON_SPIRIT_RESTORE_ITEM_BAG_NOT_ENOUGH   = 10819,    --拆解时，背包空格不足返还
    ERR_DRAGON_SPIRIT_PUT_TYPE_REPEAT               = 10820,    --不允许穿戴重复类型的龙魂
    ERR_DRAGON_SPIRIT_ESSENCE_CANOT_PUTON           = 10821,    --龙魂精华不能穿戴

    --结婚 10851 - 10900
    ERR_MARRY_LEVEL_LIMIT                           = 10851, --自己等级不足
    ERR_MARRY_LEVEL_LIMIT_OTHER                     = 10852, --对方等级不足
    ERR_MARRY_NOT_ONLINE                            = 10853, --对方不在线
    ERR_MARRY_NOT_FRIEND                            = 10854, --不是好友
    ERR_MARRY_FRIEND_DEGREE_LIMIT                   = 10855, --亲密度不够
    ERR_MARRY_STATE_REQ                             = 10856, --自己在求婚状态
    ERR_MARRY_STATE_REQED                           = 10857, --自己在被求婚状态
    ERR_MARRY_HAS_MARRY                             = 10858, --自己已结婚。--除非求婚对象为自己的配偶
    ERR_MARRY_STATE_REQ_OTHER                       = 10859, --对方在求婚状态
    ERR_MARRY_STATE_REQED_OTHER                     = 10860, --对方在被求婚状态
    ERR_MARRY_HAS_MARRY_OTHER                       = 10861, --对方已结婚。--除非求婚对象为自己的配偶
    ERR_MARRY_NO_STATE_REQED                        = 10862, --玩家不在被求婚状态
    ERR_MARRY_NOT_AGREE                             = 10863, --对方拒绝
    ERR_MARRY_SINGLE                                = 10864, --还没有配偶
    ERR_MARRY_NO_WEDDING_COUNT                      = 10865, --没有预约次数
    ERR_MARRY_HAS_BOOK                              = 10866, --已经有人预约
    ERR_MARRY_WRONG_BOOK_TIME                       = 10867, --不可预约的时间。
    ERR_MARRY_GUEST_LEVEL_LIMIT                     = 10868, --宾客等级不足
    ERR_MARRY_GUEST_HAS_INVITE                      = 10869, --已经邀请该宾客
    ERR_MARRY_INVITE_COUNT_LIMIT                    = 10870, --邀请宾客人数达到上限，但可购买
    ERR_MARRY_MAX_INVITE_COUNT_LIMIT                = 10871, --邀请宾客人数达到最大上限
    ERR_MARRY_NO_BOOK                               = 10872, --没有预定
    ERR_MARRY_NO_WEDDING                            = 10873, --当前没有婚礼举行
    ERR_MARRY_WRONG_INVITE_TIME                     = 10874, --不可邀请时间。过了这个hour的活动时间了。
    ERR_MARRY_NO_BE_INVITE                          = 10875, --你没有被邀请
    ERR_MARRY_NOT_IN_MAP                            = 10876, --不在结婚场景。无法使用烟花
    ERR_MARRY_NOT_MARRIER                           = 10877, --不是结婚者。无法开始仪式
    ERR_MARRY_HAS_CEREMONY                          = 10878, --已经开始过仪式
    ERR_MARRY_TO_NOT_MARRIER                        = 10879, --对方不是结婚者。发红包
    ERR_MARRY_FOOD_LIMIT                            = 10880, --品尝美食，满。
    ERR_MARRY_ERR_VOCATION                          = 10881, --异性才能结婚

    --等级投资 10901 - 10950
    LV_INVEST_REWARD_NOT_EXIST          = 10901, --奖励不存在
    LV_INVEST_TYPE_NOT_EXIST            = 10902, --投资档位不存在
    LV_INVEST_TYPE_PAYED                = 10903, --档位已投资
    LV_INVEST_LEVEL_NOT_ENOUGH          = 10904, --等级不够
    LV_INVEST_REWARD_GOT                = 10905, --奖励已领取

    --神兽系统 10951 - 11000
    ERR_GOD_MONSTER_ITEM_ERR            = 10951, --配置错误
    ERR_GOD_MONSTER_PKG_FULL            = 10952, --神兽背包格子不够
    ERR_GOD_MONSTER_PKG_POS_NO_ITEM     = 10953, --格子没有装备
    ERR_GOD_MONSTER_ITEM_TYPE_ERR       = 10954, --不是神兽装备不能穿戴
    ERR_GOD_MONSTER_EQUIP_QUALITY_ERR   = 10955, --品质不够
    ERR_GOD_MONSTER_INDEX_NO_EQUIP      = 10956, --该部位没有装备
    ERR_GOD_MONSTER_NO_LOADED_EQUIP     = 10957, --没有已穿戴的装备
    ERR_GOD_MONSTER_NOT_ASSIST          = 10958, --该神兽未助战
    ERR_GOD_MONSTER_EQUIP_STREN_MAX     = 10959, --强化满级了
    --ERR_GOD_MONSTER_EQUIP_STREN_CANNOT_DOUBLE   = 10960, --xxx
    --ERR_GOD_MONSTER_EQUIP_STREN_DOUBLE_COST_ERR = 10961, --双倍强化消耗不足
    ERR_GOD_MONSTER_HAS_ASSIST          = 10962, --已助战
    ERR_GOD_MONSTER_ASSIST_COUNT_MAX    = 10963, --可助战数达到上限了
    ERR_GOD_MONSTER_CANNOT_ASSIST       = 10964, --未觉醒不能助战
    ERR_GOD_MONSTER_ASSIST_ADD_COUNT_MAX    = 10965, --助战神兽扩展数达到上限

    --vip投资 11001 - 11050
    ERR_REWARD_INVEST_VIP_NOT_EXIST     = 11001, --奖励不存在
    ERR_INVEST_VIP_DAYS_NOT_ENOUGH      = 11002, --投资天数不足
    ERR_INVEST_VIP_REWARD_GOT           = 11003, --奖励已领取
    ERR_INVEST_VIP_PAYED                = 11004, --已投资

    --月卡 11051 - 11100
    ERR_INVESTMENT_PURCHASE_MONTH_CARD_FAIL_HAS_ALREADY_HELD = 11051, --已持有月卡
    ERR_INVESTMENT_HOLDING_NO_MONTH_CARD = 11052, --未购买月卡
    ERR_INVESTMENT_RECLAIM_MONTH_CARD_DAILY_REFUND = 11053, --重复领取
    ERR_INVESTMENT_MONTH_CARD_DAILY_REFUND_FAIL_INVALID_KEY = 11054, --参数错误

    --开服累充 11101 - 11150
    ERR_CUMULATIVE_CHARGE_REFUND_FAIL_CLOSED_EVENT = 11101, --活动已结束
    ERR_CUMULATIVE_CHARGE_REFUND_FAIL_INVALID_PARAMETER = 11102, --参数错误
    ERR_CUMULATIVE_CHARGE_REFUND_FAIL_RECLAIM = 11103, --已领取
    ERR_CUMULATIVE_CHARGE_REFUND_FAIL_INSUFFICIENT = 11104, --充值额度不足
    ERR_CUMULATIVE_CHARGE_REFUND_FAIL_VOCATION = 11105, --职业不匹配

    --集字有礼 11151 - 11200
    ERR_COLLECT_WORD_NOT_ENOUGH     = 11151, --活动道具不足
    ERR_COLLECT_WORD_TIMES_LIMIT    = 11152, --领取次数已满

    --神兽岛（疲劳相关）11201 - 11250
    ERR_CORSS_GOD_ISLAND_PILAO_LIMIT  =11201, --疲劳值限制不能使用

    --查看他人信息
    ERR_PEEP_OTHER_INFO_DBID_ERROR      = 13000,    --查看的DBID参数错误
    ERR_PEEP_OTHER_INFO_NOT_ONLINE      = 13001,    --被查看玩家不在线
    ERR_PEEP_OTHER_INFO_AVATR_NAME_ERROR= 13002,    --查看的角色名参数错误

    --主宰神殿 13051 - 13100
    ERR_GUILD_OVERLORD_NO_GUILD         = 13051, --现在还没有主宰公会
    ERR_GUILD_NOT_OVERLORD              = 13052, --你的公会不是主宰公会
    ERR_GUILD_OVERLORD_REWARD_HAS_GET   = 13053, --你今天已经领取过了

    --每日首充，开服首充 13101 - 13150
    ERR_DAILY_CHARGE_NOT_OPEN           = 13101, --每日首充未开启
    ERR_DAILY_CHARGE_NOT_ENOUGH         = 13102, --每日首充额度不足
    ERR_DAILY_CHARGE_REWARD_GOT         = 13103, --对应额度奖励已领取
    ERR_DAILY_CHARGE_CUM_LIMIT          = 13104, --累计天数不足或奖励已领取
    ERR_FIRST_CHARGE_REWARD_GOT         = 13105, --开服首充奖励已领取
    ERR_HAS_NOT_CHARGE                  = 13106, --未充值

    --开服冲榜 13151 - 13200
    ERR_OPEN_SERVER_RANK_END            = 13151, --整个活动结束了
    --ERR_OPEN_SERVER_RANK_TYPE_NOT_START = 13152, --该冲榜未开始
    ERR_OPEN_SERVER_RANK_TYPE_NOT_END   = 13153, --该冲榜未结束
    ERR_OPEN_SERVER_RANK_NO_REWARD      = 13154, --没有可领取的奖励
    ERR_OPEN_SERVER_RANK_HAS_GET_REWARD = 13155, --奖励已领取
    ERR_OPEN_SERVER_RANK_BUY_TIME_ERR   = 13156, --不在活动时间内
    ERR_OPEN_SERVER_RANK_BUY_LIMIT_MAX  = 13157, --限购次数买完了

    --欢乐云盘 13201 - 13250
    ERR_ROULETTE_LEVEL_LIMT             = 13201, --等级不足
    ERR_ROULETTE_CLOSE                  = 13202, --活动关闭
    ERR_ROULETTE_TIMES_LIMT             = 13203, --次数不足

    --魔戒寻主 13251 - 13300
    ERR_DEMON_RING_INVALID_PARAMETER    = 13251, --参数错误
    ERR_DEMON_RING_RECLAIM              = 13252, --已领取
    ERR_DEMON_RING_IS_LOCKED            = 13253, --未解锁
    ERR_DEMON_RING_UNFINISHED           = 13254, --未完成
    ERR_DEMON_RING_IS_NOT_RECEIVED_ALL  = 13255, --未达成解锁条件

    --福利中心 13301 - 13350
    ERR_BENEFIT_HAS_SIGNED_TODAY                        = 13301, --领取签到奖励失败（已签到）
    ERR_BENEFIT_CUMULATIVE_REWARD_FAIL_HAS_RECEIVED_ALL = 13302, --领取累计签到额外奖励失败（所有阶段奖励均已领取）
    ERR_BENEFIT_CUMULATIVE_REWARD_FAIL_INSUFFICIENT     = 13303, --领取累计签到额外奖励失败（未达到累计签到次数）
    ERR_BENEFIT_CUMULATIVE_REWARD_FAIL_INVALID_KEY      = 13304, --领取累计签到额外奖励失败（参数错误）
    ERR_BENEFIT_NOTICE_REWARD_NO_DATA                   = 13305, --领取更新公告奖励失败（配置表中无可使用当前更新数据）
    ERR_BENEFIT_NOTICE_REWARD_FAIL_RECLAIM              = 13306, --领取更新公告奖励失败（已领取）
    ERR_BENEFIT_LEVEL_UP_REWARD_INVALID_PARAMETER       = 13307, --领取冲级奖励参数错误
    ERR_BENEFIT_LEVEL_UP_REWARD_FAIL_INSUFFICIENT       = 13308, --领取冲级奖励失败（等级未达领取条件）
    ERR_BENEFIT_LEVEL_UP_REWARD_FAIL_RECLAIM            = 13309, --领取冲级奖励失败（已领取）
    ERR_BENEFIT_LEVEL_UP_REWARD_FAIL_NO_REMAIN          = 13310, --领取冲级奖励失败（无剩余）

    --装备寻宝 13351-13400
    ERR_LOTTERY_EQUIP_PKG_FULL          = 13351, --寻宝临时仓库满了
    ERR_LOTTERY_EQUIP_SCORE_NOT_ENOUGH  = 13352, --积分不足
    ERR_LOTTERY_EQUIP_TIMES_MAX         = 13353, --次数达到上限

    --婚戒 13401 - 13450
    ERR_MARRY_RING_NO_ITEM_EXP                          = 13401, --配置错误
    ERR_MARRY_RING_FULL_LEVEl                           = 13402, --满级了
    ERR_MARRY_RING_NO_LOAD                              = 13403, --还没穿戴婚戒
    
    --仙娃 13451 - 13500
    ERR_MARRY_PET_UNLOCKED              = 13451, --宝宝已激活
    ERR_MARRY_PET_BEFORE_NOT_UNLOCK     = 13452, --前置宝宝未激活
    ERR_MARRY_PET_BEFORE_GRADE_LIMIT    = 13453, --前置宝宝阶级不足
    ERR_MARRY_PET_NOT_ITEM_EXP          = 13454, --没有物品经验
    ERR_MARRY_PET_FULL_GRADE            = 13455, --宝宝已满阶
    ERR_MARRY_PET_NOT_UNLOCK            = 13456, --宝宝未激活
    ERR_MARRY_RING_LEVEL_LIMIT          = 13457, --婚戒等级不足
    MARRY_PET_UNLOCK_CONDITION_NOT_ENOUGH   = 13458, --激活条件不足

    --结婚副本 13501 - 13550
    ERR_MARRY_INSTANCE_NOT_ENOUGH_CHANCE        = 13501, --无进入副本次数
    ERR_MARRY_INSTANCE_PURCHASED_NO_MORE_CHANCE = 13502, --无每日购买次数
    ERR_MARRY_INSTANCE_UNMARRIED                = 13503, --未婚不允许
    ERR_MARRY_INSTANCE_MEMBER_COUNT             = 13504, --副本人数限制，不是2个
    ERR_MARRY_INSTANCE_TIMES_OVER               = 13505, --[xxx]有队员副本次数不够
    ERR_MARRY_INSTANCE_OFFLINE                  = 13506, --配偶不在线

    --结婚宝匣 13551 - 13600
    ERR_MARRY_BOX_PURCHASE_MARRY_BOX_FAIL_HAS_ALREADY_HELD = 13551, --已购买宝匣
    ERR_MARRY_BOX_HOLDING_NO_MARRY_BOX                     = 13552, --未持有宝匣
    ERR_MARRY_BOX_RECLAIM_MARRY_BOX_DAILY_REFUND           = 13553, --已领取每日
    ERR_MARRY_BOX_RECLAIM_MARRY_BOX_REFUND                 = 13554, --已领取立即
    ERR_MARRY_BOX_PURCHASE_MARRY_BOX_UNMARRIED             = 13555, --未婚不允许购买


    --周卡 13601 - 13650
    ERR_WEEK_CARD_HOLDING_NO_WEEK_CARD           = 13601, --未持有周卡
    ERR_WEEK_CARD_RECLAIM_WEEK_CARD_DAILY_REFUND = 13602, --已领取周卡每日返利

    --送花 13651 - 13700
    ERR_SEND_FLOWER_ITEM_ERROR          = 13651, --道具错误
    ERR_SEND_FLOWER_WAY_ERROR           = 13652, --方式错误
    ERR_SEND_FLOWER_NOT_ONLINE          = 13653, --对方不在线
    ERR_SEND_FLOWER_IN_BLACKS           = 13654, --你在对方黑名单，或对方在你黑名单
    ERR_SEND_FLOWER_NOT_FOUND           = 13655, --找不到或不在线

    --系统设置 13701 - 13750
    ERR_SETTING_MISSING_ARGUMENTS          = 13701, --缺少参数（字段不能不填）
    ERR_SETTING_PWD_IS_NOT_SIX_NUMBERS     = 13702, --安全码必须为六位数
    ERR_SETTING_ANSWER_IS_EMPTY            = 13703, --安全问题答案为空
    ERR_SETTING_ANSWER_IS_INVALID          = 13704, --安全问题答案须小于九个字符
    ERR_SETTING_SECURITY_LOCK_IS_NOT_SET   = 13705, --未设置安全锁
    ERR_SETTING_ANSWER_IS_WRONG            = 13706, --安全问题答案错误
    ERR_SETTING_SECURITY_LOCK_IS_UNLOCKED  = 13707, --安全锁未上锁
    ERR_SETTING_PWD_IS_WRONG               = 13708, --安全锁密码错误
    ERR_SETTING_SECURITY_LOCK_IS_EFFECTIVE = 13709, --安全锁正在锁定

    --充值 13751 - 13800
    ERR_CHARGE_LISTING_ERR              = 13751, --充值listing错误

    --一键三转 13801 - 13850
    ERR_VOC_CHANGE_ONE_KEY_PERMISSION_DENIED = 13801, --条件不符

    --翻牌集福 13851 - 13900
    ERR_OPEN_CARD_NOT_ENOUGH        = 13851, --福字不足
    ERR_OPEN_CARD_RECLAIM           = 13852, --已领取
    ERR_OPEN_CARD_NO_MORE_CHANCE    = 13853, --全部翻完了
    ERR_OPEN_CARD_INVALID           = 13854, --参数错误
    ERR_OPEN_CARD_IS_ALREADY_OPENED = 13855, --这个位置已经翻开

    --欢乐许愿 13901 - 13950
    ERR_WISH_NOT_OPEN           = 13901, --许愿未开启
    ERR_WISH_END                = 13902, --许愿已结束（指当天抽到大奖后）


    --渠道活动 13951 - 14000
    ERR_CHANNEL_ACTIVITY_IS_CLOSED = 14000, -- 活动在渠道中非开启状态

    --连续消费 14001 - 14050
    DAILY_CONSUME_NOT_ENOUGH            = 14001, --连续消费不足
    DAILY_CONSUME_REWARD_GOT            = 14002, --连续消费奖励已领取
    DAILY_CONSUME_CUMULATIVE_NOT_ENOUGH = 14003, --连续消费累计天数不足
    DAILY_CONSUME_CUMULATIVE_REWARD_GOT = 14004, --连续消费累计天数奖励已领取

    --升级限时销售 14051 - 14100
    ERR_LEVEL_UP_SALE_NO_PERMISSION      = 14051, --都没够等级
    ERR_LEVEL_UP_SALE_INVALID_PARAMETER  = 14052, --参数错误
    ERR_LEVEL_UP_SALE_PRESENT_IS_EXPIRED = 14053, --过期不候
    ERR_LEVEL_UP_SALE_FAIL_REPURCHASE    = 14054, --重复购买

    --周累充 14101 - 14150
    ERR_WEEKLY_CUMULATIVE_C_REFUND_INVALID_PARAMETER = 14101, --参数错误
    ERR_WEEKLY_CUMULATIVE_C_REFUND_FAIL_RECLAIM      = 14102, --已领取
    ERR_WEEKLY_CUMULATIVE_C_REFUND_FAIL_VOCATION     = 14103, --表没配职业
    ERR_WEEKLY_CUMULATIVE_C_REFUND_FAIL_INSUFFICIENT = 14104, --没冲够钱

    --周活动登录 14151 - 14200
    ERR_WEEK_LOGIN_NOT_YET      = 14151, --未登录
    ERR_WEEK_LOGIN_REWARD_GOT   = 14152, --已领取

    --庆典兑换 14201 - 14250
    ERR_EXCHANGE_TIMES_LIMIT    = 14201, --次数满

    --战场之魂 14251 - 14300
    ERR_BATTLE_SOUL_NOT_OPEN        = 14251, --未开启
    ERR_BATTLE_SOUL_END             = 14252, --已结束

    --在线奖励 14301 - 14350
    ERR_ONLINE_REWARD_RECLAIM = 14301, --已领取
    ERR_ONLINE_REWARD_INVALID = 14302, --参数错误
    ERR_ONLINE_REWARD_INSUFFICIENT = 14303, --挂机时间不足
    
    --hi点 14351 - 14400
    ERR_HI_CFG_ERR                  = 14351,
    ERR_HI_HAD_REWARD               = 14352,

    --0元购 14401 - 14450
    ERR_ZERO_PRICE_NOT_OPEN     = 14401, --活动关闭
    ERR_ZERO_PRICE_BOUGHT       = 14402, --已购买
    ERR_ZERO_PRICE_NOT_BUY      = 14403, --未购买
    ERR_ZERO_PRICE_REBATE_GOT   = 14404, --已领取
    ERR_ZERO_PRICE_TIME_LIMIT   = 14405, --时间不够

    --炫装特卖 14451 - 14500
    ERR_DAZZLE_SALE_NOT_OPEN    = 14451, --活动关闭

    --星脉 14501 - 14550
    ERR_ZODIAC_INVALID = 14501, --表有问题
    ERR_ZODIAC_INSUFFICIENT = 14502, --不够

    --BOSS悬赏 14551 - 14600
    ERR_BOSS_REWARD_INVALID = 14551, --我表又错了
    ERR_BOSS_REWARD_RECLAIM = 14552, --已领取
    ERR_BOSS_REWARD_UNFINISHED = 14553, --未完成

    --天降秘宝 14601 - 14650
    ERR_SKY_DROP_COLLECT_TIMES_LIMIT_ERR    = 14601, --次数满

    --开宗立派 14651 - 14700
    ERR_FRAUD_GUILD_INVALID = 14651, --我表又错了
    ERR_FRAUD_GUILD_RECLAIM = 14652, --已领取
    ERR_FRAUD_GUILD_UNFINISHED = 14653, --未完成
    ERR_FRAUD_GUILD_REWARD_NO_REMAIN = 14654, --无剩余

    --上古战场 14701 - 14750
    ERR_ANCIENT_BATTLE_NOT_OPEN                 = 14701, --活动未开启
    ERR_ANCIENT_BATTLE_END                      = 14702, --活动已结束
    ERR_ANCIENT_BATTLE_CAN_NOT_ENTER_10_MINUTE  = 14703, --活动开启10分钟后不能进入

    --开服乱嗨 14751 - 14800
    ERR_FREAK_INVALID = 14751, --我表又错了
    ERR_FREAK_RECLAIM = 14752, --已领取
    ERR_FREAK_INSUFFICIENT = 14753, --不够

    --限时抢购2 14801 - 14850
    ERR_LIMITED_SALE_BOUGHT = 14801, --已经买了
    ERR_LIMITED_SALE_TIME_OUT = 14802, --时间过了

    --vip冲级礼包 14851 - 14900
    ERR_VIP_LEVEL_UP_TIMES_LIMIT    = 14851, --次数已达上限
    ERR_VIP_LEVEL_UP_CONDITIONS_LIMIT   = 14852, --条件不足
    ERR_VIP_LEVEL_UP_REWARD_GOT     = 14853, --已领取

    --升品 14951 - 15000
    ERR_EQUIP_QUALITY_MAX_QUALITY       = 14951, --已达最高品质

    --深渊祭坛 15001 - 15050
    ERR_ALTAR_LAYER_NOT_MATCH         = 15001, --匹配地图与祭坛等级不匹配
    ERR_ALTAR_LAYER_MEMBER_LAYER_LOW  = 15002, --队伍成员的层级过低，不能参与
    ERR_ALTAR_LAYER_SELF_LAYER_LOW    = 15003, --自己的层级过低，不能参与
    ERR_ALTAR_START_LAYER_NOT_MATCH   = 15004, --只能从1、4、6层开始

    --巅峰寻宝 15051-15100
    ERR_LOTTERY_PEAK_PKG_FULL          = 15051, --巅峰寻宝临时仓库满了
    ERR_LOTTERY_PEAK_SCORE_NOT_ENOUGH  = 15052, --巅峰寻宝兑换积分不足
    ERR_LOTTERY_PEAK_TIMES_MAX         = 15053, --巅峰寻宝次数达到上限

-------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------
    --服务器系统公告 30000-...  这段定义放到这个文件最后
    --SYS_ID = ERR_ID 因为是同一个表
    --这里不分系统，按照顺序加，不要跳那么多id

    SYS_ID_GM_EXECUTE_SUCCESS        = 30000,     --执行gm成功
    SYS_ID_GM_SU_SUCCESS             = 30001,     --执行gm提权成功
    --队伍相关
    SYS_ID_TEAM_ENTER                = 30010,     --队伍内广播：xx进入队伍
    SYS_ID_TEAM_CAPTAIN              = 30011,     --队伍内广播：xx成为队长
    SYS_ID_TEAM_LEAVE                = 30012,     --队伍内广播：xx离开队伍
    SYS_ID_TEAM_BE_KICK              = 30013,     --个人通知：被剔除队伍了
    SYS_ID_TEAM_BE_KICK_TEAM         = 30014,     --队伍内广播：xx被剔除队伍了
    SYS_ID_TEAM_NOT_AGREE            = 30015,     --队伍内广播：xx队员不同意进入xx副本
    SYS_ID_TEAM_EXIT                 = 30016,     --队伍内广播：xx队员退出，取消进入xx副本
    SYS_ID_TEAM_CLIENT_DEATH         = 30017,     --队伍内广播：xx队员掉线，取消进入xx副本
    --
    SYS_ID_GUILD_BONFIRE_RIGHT       = 30050,    --公会篝火答对题目
    --SYS_ID_MARRY_REQ               = 30051,    --xxx被yyy求婚了
    SYS_ID_MARRY_AGREE               = 30052,    --XXXX与YYYY牵手成功，完成了KKKK结缘让我们一起祝福这对新人幸福到永久吧！
    SYS_ID_MARRY_BEFORE_5_MIN        = 30053,    --XXXX与YYYY举办的KK婚礼还有5分钟就开始啦，让我们一起祝福这对新人幸福到永久吧！
    SYS_ID_MARRY_BEGIN               = 30054,    --XXXX与YYYY举办的YY婚礼正式开始啦，让我们一起祝福这对新人幸福到永久吧！
    SYS_ID_MARRY_CEREMONY            = 30055,    --新人正在开始举行仪式，请各位宾客尽快占好位置观礼
    SYS_ID_MARRY_COLLECT             = 30056,    --XXX在婚宴大厅拾取YY时人品爆发获得极品KKKKK
    SYS_ID_SERVERS_LV_INVEST         = 40002,    --全服公告：玩家YYYY一掷千金，投资了KKKK档的等级投资，高倍返利，一马当先！
    SYS_ID_SERVERS_INVEST_MONTH_CARD = 40003,    --玩家YYYY一掷千金，投资了月卡投资，高倍返利，一马当先！
    SYS_ID_SERVERS_INVEST_VIP        = 40004,    --全服公告：玩家YYYY一掷千金，投资了VIP投资，高倍返利，一马当先！
    SYS_ID_SERVERS_CUMULATIVE_CHARGE = 40005,    --恭喜xxx完成了开服累充目标，领取了***钻石档奖励
    SYS_ID_SERVERS_DAILY_CHARGE      = 40006,    --全服公告：恭喜xxx完成了每日首充680档目标，获得丰厚奖励
    SYS_ID_SERVERS_ROULETTE_REWARD   = 40007,    --全服公告：恭喜XXX在欢乐云购中获得YYY。我要参与
    SYS_ID_SERVERS_ROULETTE_LUCK     = 40008,    --全服公告：本轮欢乐云购已结束。恭喜XXX夺得超值大奖YYY！
    SYS_ID_SERVERS_ROULETTE_LUCK_NONE= 40009,    --全服公告：本轮欢乐云购已结束。很可惜本轮无人中奖
    SYS_ID_SERVERS_SEND_FLOWER_NN    = 40010,    --玩家XXX向YYY赠送99朵玫瑰，全服公告
    SYS_ID_SERVERS_SEND_FLOWER_NNN   = 40011,    --玩家XXX向YYY赠送999多玫瑰，全服公告
    SYS_ID_SERVERS_MARRY_PET_UNLOCK  = 40012,    --玩家XXX激活仙娃YYY，全服公告
    SYS_ID_SERVERS_MARRY_PET_GRADE_UP= 40013,    --玩家XXX将仙娃YYY升至KKK阶，全服公告
    SYS_ID_SERVERS_LOTTERY_EQUIP     = 40014,    --恭喜玩家XXX在装备寻宝获得XXX
    SYS_ID_SERVERS_COLLECT_WORD      = 40015,    --集字有礼最高一档：玩家AAA在集字活动中成功兑换BBB，集毅力与运气于一身！
    SYS_ID_SERVERS_FIRST_CHARGE      = 40016,    --恭喜AAA参加超值首充，获得：BBB、CCC、DDD、EEE（奖励道具名字）等奖励，神器在手，天下我有。
    SYS_ID_SERVERS_EQUIP_STREN       = 40017,    --恭喜AAA（玩家名称）将BBB（装备名称）强化到CCC级（强化等级），战力飙升！
    SYS_ID_SERVERS_GEM               = 40018,    --AAA（玩家名称）成功镂刻BBB级CCC（宝石类型，如：攻击/生命）宝石，一瞬间珑光交映，瑞彩冲天！
    SYS_ID_SERVERS_PET               = 40019,    --AAA成功将宠物至CCC阶（进阶后的阶数）！战力暴增，可喜可贺！
    SYS_ID_SERVERS_EQUIP_WASH        = 40020,    --AAA（玩家名称）将BBB（装备名称），洗炼出CCC条DDD属性！
    SYS_ID_SERVERS_VOC_CHANGE_ONE    = 40021,    --恭喜AAA（玩家名称）历经千锤百炼，完成一转任务，转职成为BBB（职业名字，需区分男女）
    SYS_ID_SERVERS_VOC_CHANGE_TWO    = 40022,    --恭喜AAA（玩家名称）历经千锤百炼，完成二转任务，转职成为BBB（职业名字，需区分男女）
    SYS_ID_SERVERS_VOC_CHANGE_THREE  = 40023,    --恭喜AAA（玩家名称）历经千锤百炼，完成三转任务，转职成为BBB（职业名字，需区分男女）
    SYS_ID_SERVERS_VOC_CHANGE_FOUR   = 40024,    --恭喜AAA（玩家名称）历经千锤百炼，完成四转任务，转职成为BBB（职业名字，需区分男女）

    SYS_ID_SERVERS_NOBILITY_UPGRADE             = 40025, --[传闻]AAA（玩家名字）成功提升至BBB（境界名称，如：元婴）境界！神游八极，纵横十方！
    SYS_ID_SERVERS_TREASURE_LEVEL_UP            = 40026, --[传闻]AAA成功将BBB（外观名称，如：翅膀）至CCC级（升级后的阶数）！实力大增，恭喜恭喜！
    SYS_ID_SERVERS_COMBAT_VALUE_RANK_ONE_ONLINE = 40027, --[传闻]本服战神AAA（玩家名称）已降临人间，神威尽显！    
    SYS_ID_SERVERS_COMBAT_VALUE_RANK_ONE_CHANGE = 40028, --[传闻]AAA成功超越BBB成为新的战神，威震天下！
    SYS_ID_SERVERS_UNLOCK_PASSIVE_SKILL         = 40029, --[传闻]AAA（玩家名称）完成了天书寻主(获得途径)，掌握了神技{BBB}（技能名字），杀怪经验提升30%(描述)
    SYS_ID_SERVERS_ILLUSION_ACTIVATE            = 40030, --[传闻]AAA（玩家名称）成功激活BBB（化形名字），战力突飞猛进！
    SYS_ID_SERVERS_ILLUSION_GRADE_UP            = 40031, --[传闻]AAA（玩家名称）将BBB（坐骑/宠物的化形名字）升级至CCC阶，战力飙升！
    SYS_ID_SERVERS_ILLUSION_STAR_UP             = 40032, --[传闻]AAA（玩家名称）将BBB（时装/翅膀/法宝/神兵的化形名字）升级至CCC星，战力飙升！
    SYS_ID_SERVERS_BOSS_BORN                    = 40033, --[传闻] AAA（BOSS名字）携带大量珍贵的物品出现在BBB（BOSS所在地图），我要击杀
    SYS_ID_SERVERS_GUILD_BEAST_START            = 40034, --[仙盟]仙盟神兽已经召唤，大家一起打宝吧
    SYS_ID_SERVERS_LOTTERY_RUNE                 = 40035, --[传闻]玩家AAA在符文寻宝中获得BBB，惊喜不断！
    SYS_ID_SERVERS_HORSE                = 40036,    --AAA成功将坐骑至CCC阶（进阶后的阶数）！战力暴增，可喜可贺！
    SYS_ID_SERVERS_GUARD_BUY_1      = 40037,    --AAA成功续费丘比特，杀怪经验持续暴涨！
    SYS_ID_SERVERS_GUARD_BUY_2      = 40038,    --AAA成功续费熊猫憨憨，伤害减免持续保障！
    SYS_ID_SERVERS_GUARD_BUY_3      = 40039,    --AAA成功续费万圣小鬼，杀怪经验持续暴涨！
    SYS_ID_SERVERS_GUARD_BUY_4      = 40040,    --AAA成功续费骷髅王，伤害减免持续保障！
    SYS_ID_SERVERS_GOD_ISLAND_MONSTER_BORN  = 40041, -- AAA(地图)的BBB(精英怪)刷新了，快去打他      神兽岛
    SYS_ID_SERVERS_GOD_ISLAND_CRYSTAL_BORN  = 40042, -- AAA(地图)的BBB(水晶) 刷新了,快去采集        神兽岛
    SYS_ID_SERVERS_GOD_ISLAND_BOSS_BORN     = 40043, -- AAA(地图)的BBB(boss) 刷新了，快去打他      神兽岛
    SYS_ID_SERVERS_WORLD_BOSS_BORN          = 40044, --  AAA（BOSS名字）携带大量珍贵的物品出现在BBB（BOSS所在地图），我要击杀  世界boss
    SYS_ID_SERVERS_BOSS_HOME_BORN           = 40045, --  AAA（BOSS名字）携带大量珍贵的物品出现在BBB（BOSS所在地图），我要击杀  boss之家
    SYS_ID_SERVERS_FORBIDDEN_BOSS_BORN      = 40046, --  AAA（BOSS名字）携带大量珍贵的物品出现在BBB（BOSS所在地图），我要击杀  黑暗禁地

    SYS_ID_SERVERS_CROSS_VS11        = 40051,    --玩家AAA在巅峰竞技活动中已经达到B连胜，令人惊叹！
    --
    SYS_ID_SERVERS_SUPREME_GUARD_CUPID         = 40101,    --AAA（玩家名称）获得了BBB（道具名字），杀怪经验提升50%，升级速率飙升！
    SYS_ID_SERVERS_SUPREME_GUARD_PANDA         = 40102,    --AAA（玩家名称）获得了BBB（道具名字），伤害减免提升20%，战斗能力飞跃提升！
    SYS_ID_SERVERS_SUPREME_GUARD_HALLOWEEN_IMP = 40103,    --AAA（玩家名称）成功合成了BBB（道具名字），杀怪经验提升70%，伤害提升10%
    SYS_ID_SERVERS_SUPREME_GUARD_SKULL_KING    = 40104,    --AAA（玩家名称）成功合成了BBB（道具名字），伤害减免提升30%，暴击抵抗10%，暴伤减免10%
    SYS_ID_SERVERS_DROP_SUPREME_ITEM           = 40105,    --AAA（玩家名称）在BBB（地图名）与CCC（BOSS名字）大战三百回合，获得极品DDD（珍稀物品）！
    SYS_ID_SERVERS_OPEN_SUPREME_ITEM           = 40106,    --AAA（玩家名称）开启BBB（箱子道具名），获得极品CCC（珍稀物品）！
    --
    SYS_ID_SERVERS_BUILT_GUILD        = 40151,    --AAA（玩家名称）已组建起仙盟BBB（仙盟名称），现号召广大侠士加入，降妖伏魔，征战天下！
    SYS_ID_SERVERS_UPGRADED_GUILD     = 40152,    --AAA仙盟的兄弟齐心协力，把仙盟成功升至BBB级仙盟，实力更为壮大了！
    SYS_ID_SERVERS_LEFT_GUILD         = 40153,    --主动退出仙盟：[仙盟]AAA（玩家名称）离开了仙盟。
    SYS_ID_SERVERS_JOINED_GUILD       = 40154,    --[仙盟]欢迎新成员AAA（玩家名称）加入仙盟！
    SYS_ID_SERVERS_EXPELLED_GUILD     = 40155,    --开除出仙盟：[仙盟]AAA（玩家名）已被踢出仙盟。
    SYS_ID_SERVERS_GUILD_RED_ENVELOPE = 40156,    --[仙盟]AAA（玩家名）发布了仙盟红包，请各位盟友尽快领取，先到先得
    SYS_ID_SERVERS_GUILD_PROMOTED     = 40157,    --职位提升：[仙盟]AAA（玩家名称）自加入仙盟后，任劳任怨，功绩突出，现提拔为BBB（仙盟成员职位，如：副盟主）。
    SYS_ID_SERVERS_GUILD_DEMOTED      = 40158,    --职位下降
    SYS_ID_SERVERS_GUILD_TRANSFERED   = 40159,    --转让会长
    SYS_ID_SERVERS_GUILD_DONATED      = 40160,    --XXX捐献了XXX获得了XXX积分
    --
    SYS_ID_SERVERS_COMPOSE_EQUIP   = 40201,    --玩家成功合成装备时，传闻公告

    --
    SYS_ID_SERVERS_EQUIP_SUIT     = 40251,    --[传闻]AAA（玩家名称）成功锻造出BBB（锻造后装备名称），激活CCC（2、3、5件套）件DDD(套装名)，实力大增！

    SYS_ID_SERVERS_WISH         = 40252, --恭喜XXXXX在许愿之泉活动中获得YYYY，运气爆棚！

    SYS_ID_SERVERS_OPEN_CARD = 40253, --恭喜NIGGA在翻牌集福活动获得最终大奖SHIT，运气爆棚！

    --
    SYS_ID_SERVERS_HOMICIDE_GUILD_PRESIDENT1 = 40301, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_PRESIDENT2 = 40302, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_PRESIDENT3 = 40303, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_PRESIDENT4 = 40304, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_PRESIDENT5 = 40305, --

    SYS_ID_SERVERS_HOMICIDE_GUILD_VICE_PRESIDENT1 = 40306, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_VICE_PRESIDENT2 = 40307, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_VICE_PRESIDENT3 = 40308, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_VICE_PRESIDENT4 = 40309, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_VICE_PRESIDENT5 = 40310, --

    SYS_ID_SERVERS_HOMICIDE_GUILD_ELITE1 = 40311, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_ELITE2 = 40312, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_ELITE3 = 40313, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_ELITE4 = 40314, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_ELITE5 = 40315, --

    SYS_ID_SERVERS_HOMICIDE_GUILD_NORMAL1 = 40316, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_NORMAL2 = 40317, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_NORMAL3 = 40318, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_NORMAL4 = 40319, --
    SYS_ID_SERVERS_HOMICIDE_GUILD_NORMAL5 = 40320, --

    SYS_ID_SERVERS_HOMICIDE_LONER1 = 40321, --
    SYS_ID_SERVERS_HOMICIDE_LONER2 = 40322, --
    SYS_ID_SERVERS_HOMICIDE_LONER3 = 40323, --
    SYS_ID_SERVERS_HOMICIDE_LONER4 = 40324, --
    SYS_ID_SERVERS_HOMICIDE_LONER5 = 40325, --

    SYS_ID_SERVERS_REVENGE_GUILD_PRESIDENT = 40326, --
    SYS_ID_SERVERS_REVENGE_GUILD_VICE_PRESIDENT = 40327, --
    SYS_ID_SERVERS_REVENGE_GUILD_ELITE = 40328, --
    SYS_ID_SERVERS_REVENGE_GUILD_NORMAL = 40329, --
 
    SYS_ID_SERVERS_ACTIVE_VIP = 40330,  --激活VIP（含重新激活）
    SYS_ID_AUCTION_SELL       = 40331,  --拍卖行卖出{item_id}
    SYS_ID_SERVERS_DAILY_CONSUME    = 40332, --恭喜xxx完成了连续消费{0}档目标，获得丰厚奖励
    SYS_ID_SERVERS_JIANBAO_IMPORTANT    = 40333, --大奖公告：恭喜XXXXX在鉴宝活动中寻得秘密宝藏，YYYY惊现三界！
    SYS_ID_SERVERS_JIANBAO_NORMAL       = 40334, --小奖公告：恭喜XXXXX在鉴宝活动中寻得YYYY，运气爆棚！（其他玩家可以通过大奖公告进入活动界面）
    SYS_ID_SERVERS_WEEKLY_CUMULATIVE_CHARGE = 40335, --恭喜XXX领取了累充豪礼的YYY档奖励，获得ZZZ，ZZZ，ZZZ，ZZZ等奖励，神兵利器，势不可挡！
    SYS_ID_SERVERS_LEVEL_UP_SALE = 40336, --沙雕用了998购买了原价两三万现价998的垃圾
    SYS_ID_SERVERS_ACTIVITY_FIREWORK    = 40337, --恭喜XXX开启庆典烟花，获得了ZZZ，运气过人！
    SYS_ID_AUCTION_ON_SHELF = 40338, --上架了 道具 数量 价格
    SYS_ID_SERVERS_BLESSED = 40339, --守护女神幸运
    SYS_ID_SERVERS_MARRY_FIREWORK = 40340, --金宇 在结婚场景点燃 超级无敌 烟花，螺旋升天，牛咯！
    SYS_ID_SERVERS_GUILD_CONQUEST_HOMICIDE = 40341, --金宇 在公会争霸击杀了 金宇，牛咯！
    SYS_ID_SERVERS_GUILD_CONQUEST_COLLECTED = 40342, --金宇 在公会争霸占领了 SB 水晶，牛咯！
    --
    SYS_ID_SERVERS_VIP1 = 40351, --
    SYS_ID_SERVERS_VIP2 = 40352, --
    SYS_ID_SERVERS_VIP3 = 40353, --
    SYS_ID_SERVERS_VIP4 = 40354, --
    SYS_ID_SERVERS_VIP5 = 40355, --
    SYS_ID_SERVERS_VIP6 = 40356, --
    SYS_ID_SERVERS_VIP7 = 40357, --
    SYS_ID_SERVERS_VIP8 = 40358, --
    SYS_ID_SERVERS_VIP9 = 40359, --
    SYS_ID_SERVERS_VIPFAKE = 40360, --体验卡

    SYS_ID_SERVERS_ZERO_PRICE_3 = 40401, --
    SYS_ID_SERVERS_ZERO_PRICE_4 = 40402, --

    SYS_ID_PERFECT_LOVER             = 5201314,  --恭喜XXX达成完美情人，获得【三生三世完美情人】称号
}

return error_code
