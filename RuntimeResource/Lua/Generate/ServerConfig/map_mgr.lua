local engine = require "engine"
local lua_util = require "lua_util"
local putil = require "public_util"
local public_config = require "public_config"
local global_params = require "global_params"
local error_code = require "error_code"

local string = string
local pairs = pairs
local unpack = unpack
local math = math

local log_i = putil.log_i
local log_w = putil.log_w
local log_d = putil.log_d
local choose_1 = lua_util.choose_1
local math_random = math.random

local map_mgr = {}

function map_mgr:init_data(is_cell)

    self.world_map = lua_util._readXml("/data/xml/world_map.xml", "id_i")
    self.regional_map = lua_util._readXml("/data/xml/regional_map.xml", "id_i")

    self.instance_data = lua_util._readXml("/data/xml/instance.xml", "id_i")
    self.map_data = lua_util._readXml("/data/xml/map.xml", "id_i")
    self.map_inst = {}
    
    for _, data in pairs(self.regional_map) do
        if data.true_location then
            local x, y, z = unpack(data.true_location)
            if x then
                x = x * 100
            end
            if y then
                y = y * 100
            end
            if z then
                z = z * 100
            end
            data.true_location = {x, y, z,}
        end
    end
    
    for inst_id,v in pairs(self.instance_data) do
        for _,map_id in pairs(v.map_ids) do
            self.map_inst[map_id] = inst_id
        end
    end
    log_d("map_mgr:init_data    1 ", "")

    self.space_data = {}
    local G_LUA_ROOTPATH = G_LUA_ROOTPATH
    for map_id, info in pairs(self.map_data) do
        log_d("map_mgr:init_data    2  ", "space_name=%s", info.space_name)
        
        local space_table = lua_util.read_lua_config(string.format("/data/spaces/%s.xml", info.space_name))
        if space_table then
            if space_table.entities then
                local new_entities = {}
                space_table.id2entities = {}

                for k, v in pairs(space_table.entities) do
                    if not new_entities[v.type] then
                        new_entities[v.type] = {}
                    end
                    new_entities[v.type][v.id] = v
                    space_table.id2entities[v.id] = v
                end
                space_table.entities = new_entities
            end

            if is_cell then
                local tmp1 = space_table['global_params']
                if tmp1 then
                    local mapname = tmp1['mapname']
                    if mapname then
                        --通知引擎读取障碍文件和高度y文件,安全区文件
                        log_d("map_mgr:init_data    3  ", "space_name=%s;mapname=%s", info.space_name,mapname)
                        engine.load_bm(map_id, string.format("%s/data/block_maps_server/ss%s.bm", G_LUA_ROOTPATH, mapname))
                        log_d("map_mgr:init_data    4  ", "space_name=%s;mapname=%s", info.space_name,mapname)
                        engine.load_y(map_id, string.format("%s/data/block_maps_server/ss%s.gd", G_LUA_ROOTPATH, mapname))
                        log_d("map_mgr:init_data    5  ", "space_name=%s;mapname=%s", info.space_name,mapname)
                        engine.load_cast_m(map_id, string.format("%s/data/block_maps_server/ss%s.sam", G_LUA_ROOTPATH, mapname))
                        log_d("map_mgr:init_data    6  ", "space_name=%s;mapname=%s", info.space_name,mapname)
                    end
                end
            end

            self.space_data[info.space_name] = space_table
            log_d("map_mgr:init_data    7 ", "map_id=%d;space_name=%s;space_table=%s", map_id, info.space_name, engine.cPickle(space_table))
        else
            log_w("map_mgr:init_data    8 ", "map_id=%d;space_name=%s", map_id, info.space_name)
        end
    end

    self.exp_instance = lua_util._readXml("/data/xml/exp_instance.xml", "id_i")
    self.holy_tower_data = lua_util._readXml("/data/xml/protect_holy_tower.xml", "id_i")
    self.gold_instance_data = lua_util._readXml("/data/xml/gods_treasury.xml", "id_i")
    self.forbidden_data = lua_util._readXml("/data/xml/forbidden_place.xml", "id_i")
    self.aion_data = lua_util._readXml("/data/xml/aion.xml", "id_i")
    self.personal_boss_data = lua_util._readXml("/data/xml/personal_boss.xml", "id_i")
    self.guild_beast_attri_data = lua_util._readXml("/data/xml/guild_monster_reward.xml", "world_level_i")
    self.guild_beast_data = lua_util._readXml("/data/xml/guild_monster.xml", "id_i")
    self.tower_defense_data = lua_util._readXml("/data/xml/tower_defense.xml", "id_i")
    self.equipment_instance_data = lua_util._readXml("/data/xml/blood_dungeon.xml", "sence_id_i")
    self.tower_defense_award = lua_util._readXml("/data/xml/tower_defense_award.xml", "level_i")
    self.marry_boss_data = lua_util._readXml("/data/xml/marry_boss.xml", "id_i")
    
    self.ancient_battlefield_point_award_data = lua_util._readXml("/data/xml/ancient_battlefield_point_award.xml", "id_i")
    self.ancient_battlefield_point_award_data[0] = nil
    
    log_d("map_mgr:init_data    9 end ", "")
end

function map_mgr:get_map_cfg(map_id)
    return self.map_data[map_id]
end

function map_mgr:get_map_type(map_id)
    if self.map_data[map_id] then
        return self.map_data[map_id].type
    end
end

function map_mgr:get_all_map_cfg()
    return self.map_data
end

function map_mgr:get_space_cfg(space_name)
    return self.space_data[space_name]
end

function map_mgr:get_space_by_map_id(map_id)
    local map_data = self.map_data[map_id]
    if map_data then
        return self.space_data[map_data.space_name]
    end
end

function map_mgr:get_instance_cfg(instance_id)
    return self.instance_data[instance_id]
end

function map_mgr:get_instance_cfg_by_map_id(map_id)
    local inst_id = self.map_inst[map_id]
    if inst_id then
        return self:get_instance_cfg(inst_id)
    end
end

function map_mgr:get_instance_id_by_map_id(map_id)
    return self.map_inst[map_id]
end

function map_mgr:get_world_map(module_id)
    return self.world_map[module_id]
end

function map_mgr:get_regional_map(regional_map_id)
    return self.regional_map[regional_map_id]
end

function map_mgr:get_target_point_id(map_id,teleport_point_id)
    local space_data = self:get_space_by_map_id(map_id)
    if not space_data then
        return
    end
    
    local entities = space_data.entities
    if not entities then
        return
    end
    
    local teleport_point_datas = entities["EntityTeleportData"]
    if not teleport_point_datas then
        return
    end
    
    local teleport_point_data = teleport_point_datas[teleport_point_id]
    if not teleport_point_data then
        return
    end
    
    log_d("map_mgr:get_target_point_id", "map_id=%s;teleport_point_id=%s;teleport_point_data=%s",map_id, teleport_point_id, engine.cPickle(teleport_point_data))
                                        
    if teleport_point_data.target_scene == 0 then
        return 1
    else
        local target_point_id = choose_1(teleport_point_data.target_point_ids)
        log_d("map_mgr:get_target_point_id    1 ", "target_point_id=%s", target_point_id)
        return target_point_id
    end        
end

function map_mgr:get_target_point(map_id,target_point_id)
    --log_d("map_mgr:get_target_point", "map_id=%s;target_point_id=%s", map_id,target_point_id)
    
    local target_space_data = self:get_space_by_map_id(map_id)
    --log_d("map_mgr:get_target_point    1 ", "target_space_data=%s", engine.cPickle(target_space_data or {"nil"}))
    
    if target_space_data then
        --log_d("map_mgr:get_target_point    2 ", "entities=%s", engine.cPickle(target_space_data.entities or {"nil"}))
        
        if target_space_data.entities then
            --log_d("map_mgr:get_target_point    3 ", "target_points=%s", engine.cPickle(target_space_data.entities["PositionPoint"] or {"nil"}))
            
            local target_points = target_space_data.entities["PositionPoint"]
            if target_points then
                --log_d("map_mgr:get_target_point    4 ", "target_points=%s", engine.cPickle(target_points[target_point_id] or {"nil"}))
                
                local target_point = target_points[target_point_id]
                if target_point then
                    log_d("map_mgr:get_target_point    5 ", "map_id=%s;target_point_id=%s;target_point=%s", map_id,target_point_id,engine.cPickle(target_point))
                    return target_point  --target_point.pos_x, target_point.pos_y, target_point.pos_z
                end
            end
        end
    end
end

function map_mgr:get_target_points(map_id)
    log_d("map_mgr:get_target_points", "map_id=%s", map_id)
    
    local target_space_data = self:get_space_by_map_id(map_id)
    --log_d("map_mgr:get_target_points    1 ", "target_space_data=%s", engine.cPickle(target_space_data or {"nil"}))
    
    if target_space_data then
        --log_d("map_mgr:get_target_points    2 ", "entities=%s", engine.cPickle(target_space_data.entities or {"nil"}))
        
        if target_space_data.entities then
            --log_d("map_mgr:get_target_points    3 ", "target_points=%s", engine.cPickle(target_space_data.entities["PositionPoint"] or {"nil"}))
            
            local target_points = target_space_data.entities["PositionPoint"]
            --log_d("map_mgr:get_target_points    4 ", "target_points=%s", engine.cPickle(target_points or "nil"))
            return target_points
        end
    end
end


function map_mgr:get_nearest_revive_point(map_id,faction_id,x,y,z)
    --log_d("map_mgr:get_nearest_revive_point", "map_id=%s;faction_id=%s;x=%s;y=%s;z=%s",map_id,faction_id,x,y,z)
    
    local map_cfg = self:get_map_cfg(map_id)
    local target_points = self:get_target_points(map_id)
    local min_dist = -1
    local min_target_point
    --log_d("map_mgr:get_nearest_revive_point    2 ", "map_cfg=%s",engine.cPickle(map_cfg or {"nil"}))
    
    if target_points and map_cfg and map_cfg.revive_points and map_cfg.revive_points[faction_id] then
        local target_point_ids = map_cfg.revive_points[faction_id]
        --log_d("map_mgr:get_nearest_revive_point    3 ", "target_point_ids=%s;target_points=%s",engine.cPickle(target_point_ids),engine.cPickle(target_points))
        
        for _,target_point_id in pairs(target_point_ids) do
            if target_points[target_point_id] then
                local target_point = target_points[target_point_id]
                local dist = lua_util.distance(x, z,  target_point.pos_x, target_point.pos_z)
                if min_dist == -1 then
                    min_dist = dist 
                    min_target_point = target_point
                end
                if dist < min_dist then
                    min_dist = dist
                    min_target_point = target_point
                end
            end
        end
    end

    local space_data = self:get_space_by_map_id(map_id)
    local pos_x = space_data.global_params.enter_pos_x
    local pos_y = space_data.global_params.enter_pos_y
    local pos_z = space_data.global_params.enter_pos_z
        
    log_d("map_mgr:get_nearest_revive_point    4 ", "min_target_point=%s;pos_x=%s;pos_y=%s;pos_z=%s",engine.cPickle(min_target_point or {"nil"}),pos_x,pos_y,pos_z)
    
    if min_target_point then
        pos_x = min_target_point.pos_x
        pos_y = min_target_point.pos_y
        pos_z = min_target_point.pos_z
    end
        
    return pos_x,pos_y,pos_z
end

function map_mgr:get_nearest_point(points,x,y,z)
    local min_dist = -1
    local min_target_point = nil
    for _,v in pairs(points) do
        local tmp_x,tmp_y,tmp_z = unpack(v)
        local dist = lua_util.distance(x, z,  tmp_x, tmp_z)
        if min_dist == -1 then
            min_dist = dist 
            min_target_point = v
        end
        if dist < min_dist then
            min_dist = dist
            min_target_point = v
        end
    end

    return unpack(min_target_point)
end


function map_mgr:get_nearest_born_point(map_id,faction_id)
    --log_d("map_mgr:get_nearest_born_point", "map_id=%s;faction_id=%s;",map_id,faction_id)
    
    local map_cfg = self:get_map_cfg(map_id)
    local target_points = self:get_target_points(map_id)
    local target_point = nil
    --log_d("map_mgr:get_nearest_born_point    2 ", "map_cfg=%s",engine.cPickle(map_cfg or {"nil"}))
    
    if target_points and map_cfg and map_cfg.born_id and map_cfg.born_id[faction_id] then
        local target_point_id = map_cfg.born_id[faction_id]
        target_point = target_points[target_point_id]
        --log_d("map_mgr:get_nearest_born_point    3 ", "target_point_id=%s;target_point=&s",target_point_id,engine.cPickle(target_point or {"nil"}))
    end

    local space_data = self:get_space_by_map_id(map_id)
    local pos_x = space_data.global_params.enter_pos_x
    local pos_y = space_data.global_params.enter_pos_y
    local pos_z = space_data.global_params.enter_pos_z
        
    log_d("map_mgr:get_nearest_born_point    4 ", "target_point=%s;pos_x=%s;pos_y=%s;pos_z=%s",engine.cPickle(target_point or {"nil"}),pos_x,pos_y,pos_z)
    
    if target_point then
        pos_x = target_point.pos_x
        pos_y = target_point.pos_y
        pos_z = target_point.pos_z
    end
    --log_d("map_mgr:get_nearest_born_point    5 ", "pos_x=%s;pos_y=%s;pos_z=%s",pos_x,pos_y,pos_z)
    return pos_x,pos_y,pos_z
end

function map_mgr:get_born_point_rotation(map_id,faction_id)
    --log_d("map_mgr:get_born_point_rotation", "map_id=%s;faction_id=%s;",map_id,faction_id)
    
    local map_cfg = self:get_map_cfg(map_id)
    local target_points = self:get_target_points(map_id)
    local target_point
    --log_d("map_mgr:get_born_point_rotation    2 ", "map_cfg=%s",engine.cPickle(map_cfg or {"nil"}))
    
    if target_points and map_cfg and map_cfg.born_id and map_cfg.born_id[faction_id] then
        local target_point_id = map_cfg.born_id[faction_id]
        target_point = target_points[target_point_id]
        --log_d("map_mgr:get_born_point_rotation    3 ", "target_point_id=%s;target_point=&s",target_point_id,engine.cPickle(target_point or {"nil"}))
    end

    local rotation_x = 0
    local rotation_y = 0
    local rotation_z = 0
    
    if target_point then
        local params = target_point.rotation_pos
        rotation_x,rotation_y,rotation_z = unpack(params)
    end
    log_d("map_mgr:get_born_point_rotation    4 ", "pos_x=%s;pos_y=%s;pos_z=%s",rotation_x,rotation_y,rotation_z)
    return rotation_x,rotation_y,rotation_z
end

function map_mgr:get_default_rotation(map_id)
    --log_d("map_mgr:get_default_rotation 1  ", "map_id=%s;",map_id)
    local space_data = self:get_space_by_map_id(map_id)
    if not space_data then
        return 0 
    end
    local enter_rotation = space_data.global_params.enter_rotation or 0

    --log_d("map_mgr:get_default_rotation 2 ", "enter_rotation=%s;",enter_rotation)
    return enter_rotation
end

function map_mgr:get_exp_cfg(id)
    return self.exp_instance[id]
end

function map_mgr:get_init_map_id()
    return global_params:get_param("init_map_id", 0)
end

function map_mgr:is_mark_last_map(map_id)
    --下线记录这个地图，下线5分钟后再上线，回到这个地图
    --主城或者野外
    --注意和is_not_reset_map区分。
    
    local map_cfg = self:get_map_cfg(map_id)
    if map_cfg and map_cfg.type then
        if map_cfg.type == public_config.MAP_TYPE_KINGDOM or
            map_cfg.type == public_config.MAP_TYPE_WILD then
              return true
        end
    end
    return false
end

function map_mgr:is_not_reset_map(map_id)
    --一些场景人数为0就reset，一些场景即使人数为0也不reset,就是无论如何也不reset
    --主城或者野外,世界boss等这些玩家进进出出的场景就需要这里返回true，在人数为0时不重置它
    
    local map_cfg = self:get_map_cfg(map_id)
    if map_cfg and map_cfg.type then
        if map_cfg.type == public_config.MAP_TYPE_WILD or 
           map_cfg.type == public_config.MAP_TYPE_KINGDOM or 
           map_cfg.type == public_config.MAP_TYPE_WORLD_BOSS or
           map_cfg.type == public_config.MAP_TYPE_BOSS_HOME or 
           map_cfg.type == public_config.MAP_TYPE_FORBIDDEN or
           map_cfg.type == public_config.MAP_TYPE_GUILD_BEAST or 
           map_cfg.type == public_config.MAP_TYPE_CLOUD_PEAK or 
           map_cfg.type == public_config.MAP_TYPE_GUARD_GUILD or
           map_cfg.type == public_config.MAP_TYPE_GOD_ISLAND or
           map_cfg.type == public_config.MAP_TYPE_GUILD_CONQUEST or
           map_cfg.type == public_config.MAP_TYPE_QUESTION or
           map_cfg.type == public_config.MAP_TYPE_GUILD_BONFIRE or
           map_cfg.type == public_config.MAP_TYPE_MARRY or
           map_cfg.type == public_config.MAP_TYPE_BATTLE_SOUL or
           map_cfg.type == public_config.MAP_TYPE_ANCIENT_BATTLE then
             return true
        end
    end
    return false
end

function map_mgr:get_default_map_id(avatar)
    --如果当前是可以进的。用当前的。野外或者主城的。
    --如果有tmp_data的last_map_info。用last_map_info的。
    --否则用全局默认的。

    local is_mark_last_map = self:is_mark_last_map(avatar.map_id)
    local use_xyz = false
    if is_mark_last_map then
        --容错，检查等级要求
        if self:check_map_level(avatar.map_id,avatar.level) == 0 then
            use_xyz = true
            return avatar.map_id,use_xyz,avatar.map_x, avatar.map_y, avatar.map_z
        else
            log_d("map_mgr:get_default_map_id    1 why not level limit", "map_id=%s;level=%s",avatar.map_id,avatar.level)
        end
    end
    
    local last_map_info = avatar.last_map_info 
    if last_map_info and last_map_info[1] and last_map_info[2] and last_map_info[3] and last_map_info[4] and last_map_info[5] then
        --再次判断是否主城野外
        if self:is_mark_last_map(last_map_info[1]) then
            --容错，检查等级要求
            if self:check_map_level(last_map_info[1],avatar.level) == 0 then
                use_xyz = true
                return last_map_info[1],use_xyz,last_map_info[3], last_map_info[4], last_map_info[5]
            else
                log_d("map_mgr:get_default_map_id    2 why not level limit", "last_map_id=%s;level=%s",last_map_info[1],avatar.level)
            end
        else
            log_d("map_mgr:get_default_map_id    3 why not zhucheng yewai", "last_map_id=%s",last_map_info[1])
        end
    end
    
    return self:get_init_map_id(),use_xyz
end

function map_mgr:get_holy_tower_cfg(id)
    return self.holy_tower_data[id]
end

function map_mgr:get_gold_instance_cfg(id)
    return self.gold_instance_data[id]
end

function map_mgr:get_forbidden_cfg(id)
    return self.forbidden_data[id]
end

function map_mgr:get_aion_cfg(id)
    return self.aion_data[id]
end

function map_mgr:get_personal_boss_cfg(id)
    return self.personal_boss_data[id]
end

function map_mgr:get_guild_beast_cfg(id)
    return self.guild_beast_data[id]
end

function map_mgr:get_guild_beast_attri_cfg(world_level)
    return self.guild_beast_attri_data[world_level]
end

function map_mgr:get_marry_boss_cfg(world_level)
    local id = 1
    for k,v in pairs(self.marry_boss_data) do
        if v.world_level[1] and v.world_level[2] then
            if world_level >= v.world_level[1] and world_level <= v.world_level[2] then
                id = k
            end
        end
    end
    return self.marry_boss_data[id]
end

--是否需要处理疲劳层数 (青云决里头世界boss和另外一个玩法需要，其他都不用)
function map_mgr:is_need_handle_tire_count(map_id)
    local map_cfg = self:get_map_cfg(map_id)
    if map_cfg and map_cfg.type then
        if map_cfg.type == public_config.MAP_TYPE_WORLD_BOSS then
             return true
        end
    end
    return false
end 

--获取随机的复活点
function map_mgr:get_random_revive_point(map_id,faction_id)
    log_d("map_mgr:get_random_revive_point", "map_id=%s;faction_id=%s",map_id,faction_id)
    
    local map_cfg = self:get_map_cfg(map_id)
    local target_points = self:get_target_points(map_id)

    local target_point
    --log_d("map_mgr:get_random_revive_point    2 ", "map_cfg=%s",engine.cPickle(map_cfg or {"nil"}))
    
    if target_points and map_cfg and map_cfg.revive_points and map_cfg.revive_points[faction_id] then
        local target_point_ids = map_cfg.revive_points[faction_id]
        --log_d("map_mgr:get_random_revive_point    3 ", "target_point_ids=%s;target_points=%s",engine.cPickle(target_point_ids),engine.cPickle(target_points))

        local index = math_random(1,#target_point_ids)
        local target_point_id = target_point_ids[index]
        target_point = target_points[target_point_id]
    end

    local space_data = self:get_space_by_map_id(map_id)
    local pos_x = space_data.global_params.enter_pos_x
    local pos_y = space_data.global_params.enter_pos_y
    local pos_z = space_data.global_params.enter_pos_z
        
    --log_d("map_mgr:get_random_revive_point    4 ", "target_point=%s;pos_x=%s;pos_y=%s;pos_z=%s",target_point,pos_x,pos_y,pos_z)
    
    if target_point then
        pos_x = target_point.pos_x
        pos_y = target_point.pos_y
        pos_z = target_point.pos_z
    end
    log_d("map_mgr:get_random_revive_point    5 ", "target_point=%s;pos_x=%s;pos_y=%s;pos_z=%s",engine.cPickle(target_point or {}),pos_x,pos_y,pos_z)
 
    return pos_x,pos_y,pos_z
end

function map_mgr:get_tower_build_points(map_id)
    --log_d("map_mgr:get_tower_build_points", "map_id=%s", map_id)
    
    local target_space_data = self:get_space_by_map_id(map_id)
    --log_d("map_mgr:get_tower_build_points    1 ", "target_space_data=%s", engine.cPickle(target_space_data or {"nil"}))
    
    if target_space_data then
        --log_d("map_mgr:get_tower_build_points    2 ", "entities=%s", engine.cPickle(target_space_data.entities or {"nil"}))
        
        if target_space_data.entities then
            --log_d("map_mgr:get_tower_build_points    3 ", "target_points=%s", engine.cPickle(target_space_data.entities["PositionPoint"] or {"nil"}))
            
            local target_points = target_space_data.entities["EntityTowerTrigger"]
            --log_d("map_mgr:get_tower_build_points    4 ", "target_points=%s", engine.cPickle(target_points or "nil"))
            return target_points
        end
    end
end

function map_mgr:get_tower_defense_cfg(id)
    return self.tower_defense_data[id]
end

function map_mgr:get_equipment_instance_data(map_id)
    return self.equipment_instance_data[map_id]
end

function map_mgr:get_tower_defense_award_cfg(level)
    return self.tower_defense_award[level]
end

function map_mgr:get_ancient_battlefield_point_award_all()
    return self.ancient_battlefield_point_award_data
end

function map_mgr:check_player_count_max(map_id,cur_count)
    local map_cfg = self:get_map_cfg(map_id)
    local limit = map_cfg.max_count or 0
    if cur_count >= limit then
        return error_code.ERR_SPACE_PALYER_COUNT_MAX
    end
    return error_code.ERR_SUCCESSFUL
end

function map_mgr:cur_map_can_to_map(cur_map_id,to_map_id)
    local cur_map_cfg = self:get_map_cfg(cur_map_id)
    local to_map_cfg = self:get_map_cfg(to_map_id)
    if not cur_map_cfg or not to_map_cfg or not cur_map_cfg.type or not to_map_cfg.type then
        return false
    end

    --去这些to_map_id，忽略下面的主城野外判断
    if to_map_cfg.type == public_config.MAP_TYPE_AION or
       to_map_cfg.type == public_config.MAP_TYPE_CLOUD_PEAK or
       to_map_cfg.type == public_config.MAP_TYPE_WORLD_BOSS or
       to_map_cfg.type == public_config.MAP_TYPE_BOSS_HOME or
       to_map_cfg.type == public_config.MAP_TYPE_GOD_ISLAND then
        if cur_map_cfg.type == to_map_cfg.type then
            return true
        end
    end
    
    --要去to_map_id，那么cur_map_id必须是主城和野外
    if cur_map_cfg.type == public_config.MAP_TYPE_KINGDOM or
       cur_map_cfg.type == public_config.MAP_TYPE_WILD then
        return true
    end
    
    return false
end

function map_mgr:check_map_level(map_id,level)
    local map_cfg = self:get_map_cfg(map_id)
    
    if map_cfg and map_cfg.limit_level and level < map_cfg.limit_level then
        return error_code.ERR_SPACE_AVATAR_LEVEL_LOW
    end
    
    return 0
end

return map_mgr
