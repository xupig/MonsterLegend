local lua_util = require "lua_util"
--local putil = require "public_util"
--local engine = require "engine"
local check_depends = require "check_depends"
--local public_config = require "public_config"

--local log_i = putil.log_i
--local log_d = putil.log_d

local tonumber = tonumber
local pairs = pairs
local string = string
local tostring = tostring
local os = os
local math = math

local math_random = math.random

local get_table_real_count = lua_util.get_table_real_count
local string_format = string.format


local global_params = {}

local function Convert( str )
    local number = tonumber(str)
    if number then
        return number
    end
    return str
end

function global_params:init_data()
    local data = lua_util._readXml("/data/xml/global_params.xml", "id_i")
    local result = {}
    self:format_data(data,result)
    self.data = result
    --log_d("global_params:init_data", "data=%s", engine.cPickle(self.data))
    
    self.name_random_1 = lua_util._readXml("/data/xml/name_random_1.xml", "id_i")
    self.name_random_2 = lua_util._readXml("/data/xml/name_random_2.xml", "id_i")
    self.name_random_3 = lua_util._readXml("/data/xml/name_random_3.xml", "id_i")
    self.name_random_4 = lua_util._readXml("/data/xml/name_random_4.xml", "id_i")

    --功能开启表
    self.function_open = lua_util._readXml("/data/xml/function_open.xml", "id_i")
    
    --判断职业
    self._role_init_data = lua_util._readXml("/data/xml/role_data.xml", "id_i")
end

function global_params:format_data(data,result)
    for _, value in pairs(data) do
        local key = value['key']
        local prefix = string.sub(key, -2)
        local key2 = string.sub(key, 0, -3)
        if prefix == "_i" then
            result[key2] = tonumber(value['value'])
        elseif prefix == "_f" then
            result[key2] = tonumber(value['value'])
        elseif prefix == "_s" then
            result[key2] = tostring(value['value'])
        elseif prefix == "_l" then
            --列表
            result[key2] = lua_util.split_str(value['value'], ',', Convert)
        elseif prefix == "_k" then
            --key table
            local tmp = lua_util.split_str(value['value'], ',', Convert)
            local tmp2 = {}
            for _, k in pairs(tmp) do
                tmp2[k] = 1
            end
            result[key2] = tmp2
        elseif prefix == "_m" then
            --字典
            local tmp = lua_util.split_str(value['value'], ';')
            local tmp2 = {}
            for _, v in pairs(tmp) do
                local tmp = lua_util.split_str(v, ':')
                local id = tonumber(tmp[1]) or tmp[1]
                local num = tonumber(tmp[2]) or tmp[2]
                tmp2[id] = num
            end
            result[key2] = tmp2
        elseif prefix == "_y" then
            --2014-01-25 0:12:00
            local i = string.find(value['value'],' ',1)
            local str_data = string.sub(value['value'],1,i-1)
            local str_time = string.sub(value['value'],i+1)
            local dd = lua_util.split_str(str_data,'-',tonumber)
            local tt = lua_util.split_str(str_time,':',tonumber)
            result[key2] = os.time{year=dd[1],month=dd[2],day=dd[3],hour=tt[1],min=tt[2],sec=tt[3]}
        elseif prefix == "_d" then
            local tmp = lua_util.split_str(value['value'], '-')
            result[key2] = os.time{year=tmp[1], month=tmp[2], day=tmp[3], hour=tmp[4], min=tmp[5], sec=tmp[6] }
        elseif prefix == "_t" then
            local tmp = lua_util.split_str(value['value'], '-')
            result[key2] = os.time{year=2000, month=1, day=1, hour=tmp[1], min=tmp[2], sec=tmp[3] }
        elseif prefix == "_n" then
            --字典(列表)
            local tmp = lua_util.split_str(value['value'], ';')
            local tmp2 = {}
            for _, v in pairs(tmp) do
                local tmp = lua_util.split_str(v, ':')
                local id = tonumber(tmp[1]) or tmp[1]
                local num = lua_util.split_str(tmp[2], ',', tonumber)
                tmp2[id] = num
            end
            result[key2] = tmp2
        elseif prefix == "_o" then
            --字典(字典)
            local tmp = lua_util.split_str(value['value'], ';')
            local tmp2 = {}
            for _, v in pairs(tmp) do
                local tmp = lua_util.split_str(v, ':')
                local id = tonumber(tmp[1]) or tmp[1]
                local num = lua_util.split_str(tmp[2], ',', tonumber)
                local tmp3 = {}
                for i = 1,#num,2 do
                    tmp3[num[i]] = num[i+1]
                end
                tmp2[id] = tmp3
            end
            result[key2] = tmp2
        else
            result[key] = value['value']
        end
    end
end

function global_params:get_random_name(vocation)
    local sex = lua_util.get_vocation_gender(vocation)
    if sex == 1 then
        --男
        local name1 = self:get_random(self.name_random_1)
        local name2 = self:get_random(self.name_random_2)
        return string_format("%s%s", name1, name2)
    elseif sex == 0 then
        --女
        local name3 = self:get_random(self.name_random_3) 
        local name4 = self:get_random(self.name_random_4) 
        return string_format("%s%s",name3,name4)
    end
end

function global_params:get_random(t)
    local max = get_table_real_count(t)
    if max > 0 then
        local random_id = math_random(1, max)
        return t[random_id]['name'] or ""
    end
    return ""
end

function global_params:get_param(key, default)
    return self.data[key] or default
end

--判断功能开启
function global_params:check_function_condition(avatar,func_id)
    local err_id = 0
    local err_params = {}
      
    local function_open_cfg = self.function_open[func_id]
    if function_open_cfg then
        local condition = function_open_cfg.condition
        if condition then
            err_id, err_params = check_depends:get_check_err_id(avatar, condition)
        end
    end
    
    return err_id,err_params
end


return global_params


