--该模块定义lua脚本通用接口

local math_random = math.random
local math_floor = math.floor
local table = table
local ipairs = ipairs
local pairs = pairs
local bit32 = bit32
local string = string
local os = os
local math = math
local tonumber = tonumber
local type = type
local pcall = pcall
local unpack = unpack
local error = error
local tostring = tostring

local putil = require "public_util"
local engine = require "engine"
local json = require "json"

local log_d = putil.log_d
local log_i = putil.log_i
local log_w = putil.log_w
local log_e = putil.log_e

--数据库中每张表引擎生成的固定字段
local DB_TABLE_FIXED_FIELDS = {"id", "timestamp"}
local db_field_prefix = "sm_"  --数据库字段前缀


-----------------------------------------------------------------------------------
local lua_util = {}

-----------------------bittable---------------
--从t中获取元素的值
--@return 0,1
function lua_util.bittable_get(t, i)
    local segment = math.floor(i / 32)
    local index = math.fmod(i, 32)
    --段不存在，数据肯定不存在
    if not t[segment] then
        return 0
    end
    local b = bit32.btest(t[segment], bit32.lshift(1, index)) 
    return (b and 1) or 0
end

--设置元素的值
--@i 位置
--@v 0, 1
function lua_util.bittable_set(t, i, v)
    if not v then 
        v = 1 
    end
    if v ~= 0 and v ~= 1 then 
        return 
    end
    local segment = math.floor(i / 32)
    local index = math.fmod(i, 32)
    
    if not t[segment] then
        --如果要存的元素为0，直接返回（空即代表0）
        if v == 0 then return end
        t[segment] = 0
    end

    if v == 0 then
        t[segment] = bit32.band(t[segment], bit32.bnot(bit32.lshift(1, index))) 
    else
        t[segment] = bit32.bor(t[segment], bit32.lshift(1, index)) 
    end

    --如果现在这个值为0，则置空
    if t[segment] == 0 then
        t[segment] = nil
    end
end

--获得这个表中所有值为1的项
--这个函数虽然实现蛋疼了些，但是对于主线任务而言其实是可以一次找到的
--这个函数暂时没有被用到，为了接口完整性暂时保留
function lua_util.bittable_getall(t)
    local found = {}
    for k, v in pairs(t) do
        for index = 0, 31 do
            if bit32.btest(v, bit32.lshift(1, index)) then
                table.insert(found, k * 32 + index) 
            end
        end
    end
    return found
end

--获得这个表中第一个为1的项
--这个函数是给主线任务用的，效率高。
function lua_util.bittable_getfirst(t)
    for k, v in pairs(t) do
        for index = 0, 31 do
            if bit32.btest(v, bit32.lshift(1, index)) then
                return k+index
            end
        end
    end

    return nil
end

--判断表是否为空
function lua_util.bittable_is_empty(t)
    for k, v in pairs(t) do
        if v > 0 then
            return false
        end
    end
    return true
end

--这个无论如何也返回一个table，如果str是空字符串，也会返回{''},所以注意这个接口，特别要转换成number的。或者确定str不为空再调用，如下。
--local params = {}
--if str_params ~= "" then
--    params = lua_util.split_str(str_params, ',', tonumber) --split_str的问题，如果字符串为空，那么返回的{""}，而不是{}
--end
--根据分隔符切割字符串
--func = nil(默认返回str) or tonumber or  
function lua_util.split_str(str, delim, func) 
    
    --空字符串，不返回{''}，返回{} --但其他地方用到
    --if str == "" then
    --    return {}
    --end
    
    local i = 0
    local j = 1
    local t = {}
    while i ~= nil do
        i = string.find(str, delim, j)
        if i ~= nil then
            if func then
                table.insert(t, func(string.sub(str, j, i-1)) or string.sub(str, j, i-1))
            else
                table.insert(t, string.sub(str, j, i-1))
            end
            j = i + 1
        else
            if func then
                table.insert(t, func(string.sub(str, j)) or string.sub(str, j) )
            else
                table.insert(t, string.sub(str, j))
            end
        end
    end
    return t
end

--根据分隔符，转换字符串为一个二维数组,比如："a,2;b,3,4" --> {{a,2}, {b,3,4}}
--func = nil(默认返回str) or tonumber or
function lua_util.parse_str_to_twi_array(str, delim_1, delim_2, func)  
    local result = {}
    local first_splits = lua_util.split_str(str, delim_1)
    for _, v in pairs(first_splits) do
        local second_splits = lua_util.split_str(v, delim_2, func) --tonumber
        table.insert(result, second_splits)
    end
    return result
end

--根据分隔符，转换字符串为一个哈希表,比如："a:2;b:3" --> {a=2, b=3}
--func = nil(默认返回str) or tonumber or
function lua_util.parse_str_to_dict(str, delim_1, delim_2, key_func, value_func)
    local result = {}
    local first_splits = lua_util.split_str(str, delim_1)
    for _, v in pairs(first_splits) do
        local second_splits = lua_util.split_str(v, delim_2)
        if #second_splits == 2 then
            local key
            if key_func then
                key = key_func(second_splits[1]) or second_splits[1]
            else
                key = second_splits[1] 
            end
            local value
            if value_func then
                value = value_func(second_splits[2]) or second_splits[2]
            else
                value = second_splits[2]
            end
            result[key] = value
        end
    end
    return result
end

--根据分隔符，转换字符串为一个哈希表,比如："1:2,1;2:3,2" --> {[1]={2,1}, [2]={3,2}}
--func = nil(默认返回str) or tonumber or
function lua_util.parse_str_to_dict_ex(str, delim_1, delim_2, delim_3, key_func, value_func)
    local result = {}
    local tmp = lua_util.split_str(str, delim_1)
    for _, v in pairs(tmp) do
        local tmp = lua_util.split_str(v, delim_2)
        if #tmp == 2 then
            local key
            if key_func then
                key = key_func(tmp[1]) or tmp[1]
            else
                key = tmp[1]
            end
            local values = lua_util.split_str(tmp[2], delim_3, value_func)
            
            result[key] = values
        end
    end
    return result
end

--根据分隔符，转换字符串分割后，每个表再转为为一个哈希表
--比如："1:200,1;2:100,2;5:0,1@1:300,1;2:200,2;5:0,1" 
--> {{1={1={1=200,2=1},2={1=300,2=2},5={1=0,2=1}},2={1={1=200,2=1},2={1=300,2=2},5={1=0,2=1}}}
--func = nil(默认返回str) or tonumber or
function lua_util.parse_str_to_dict_ex2(str, delim_1, delim_2, delim_3, delim_4, key_func, value_func)
    local result = {}
    local tmp = lua_util.split_str(str, delim_1)
    for group, v in ipairs(tmp) do
        local res_tmp = lua_util.parse_str_to_dict_ex(v, delim_2, delim_3, delim_4, key_func, value_func)
        result[group] = res_tmp
    end
    return result
end

--str格式,key为str,values为str列表。"1:200;2:300,400" = {"1"={"200"},"2"={"300","400""}}
function lua_util.parse_str_to_n_str(str)
    local tmp2 = lua_util.parse_str_to_dict_ex(str, ";", ":", ',')
    return tmp2
end

function lua_util.parse_str_to_n_number(str)
    local tmp2 = lua_util.parse_str_to_dict_ex(str, ";", ":", ',',tonumber,tonumber)
    return tmp2
end

function lua_util.parse_str_to_m_str(str)
    local tmp2 = lua_util.parse_str_to_dict(str, ";", ":")
    return tmp2
end

function lua_util.parse_str_to_m_number(str)
    local tmp2 = lua_util.parse_str_to_dict(str, ";", ":",tonumber,tonumber)
    return tmp2
end



--判断一个字符串是否以另外一个字符串开头
function lua_util.is_start_with_str(str, substr)
    local i,j = string.find(str, substr)
    return i == 1
end

------------------------
--从字符串中方向查找某个字符串,不支持正则表达式
--@param str: 某个字符串
--@param sub_str: 某个子串
function lua_util.reverse_find(str, sub_str)
    local str_len = #str
    local sub_str_len = #sub_str
    local string_byte = string.byte
    for i = str_len - sub_str_len + 1, 1, -1 do
        local b_find = true
        for j = i, i + sub_str_len - 1 do
            if string_byte(str, j) ~= string_byte(sub_str, j-i+1) then
                b_find = false
                break
            end
        end
        if b_find then
            return i
        end
    end
end

--从M个数里(等概率)随机出N个不重复的数
function lua_util.choose_n_norepeated( t, n )
    local m = #t
    --集合数目不够抽取数目
    if m <= n then
        return t
    end

    local t2 = {}   --记录已经抽中的结果
    local t3 = {}    --返回结果
    local i = 0     --已抽取个数
    local mrandom = math.random
    while true do
        local r = mrandom(1,m)
        if not t2[r] then
            table.insert(t3, t[r])
            t2[r] = 1
            i = i+1
            if i >= n then
                return t3
            end
        end
    end

    return t2
end

--从{k = prob}表里挑选一个满足概率的k
function lua_util.choose_prob(t, min_prob, max_prob)
    local ram
    if min_prob and max_prob then
        ram = math.random(min_prob, max_prob)
    else
        ram = math.random()
    end
    local prob = 0
    for k, prob1 in pairs(t) do
        prob = prob + prob1
        if ram <= prob then
            return k
        end
    end
end

--从格式"道具id1,数量1,概率1,道具id2,数量2,概率2,..."中随机出一个道具id和数量
function lua_util.choose_random_item(drop)
    if drop then
        local ram = math.random()
        local n = #drop
        local prop = 0
        for i=3, n, 3 do
            local p = drop[i]
            if p then
                prop = prop + p
                if ram < prop then
                    return {drop[i-2], drop[i-1]}
                end
            end
        end
    end
end

--从格式"道具id1:数量1,权重;道具id2:数量2,权重2;..."中随机出一个道具id和数量
function lua_util.choose_random_item1(drop)
    if drop then
        local ram = math.random()
        local prop = 0
        local total = 0
        for k, v in pairs(drop) do
            total = total + v[2]
        end
        for k, v in pairs(drop) do
            local p = v[2]/total
            prop = prop + p
            if ram < prop then
                return {k, v[1]}
            end
        end
    end
end

--从格式"道具id1:权重;道具id2:权重2;..."中随机出一个道具id
function lua_util.choose_random_item2(drop)
    if drop then
        local ram = math.random()
        local prop = 0
        local total = 0
        for k, v in pairs(drop) do
            total = total + v
        end
        for k, v in pairs(drop) do
            local p = v/total
            prop = prop + p
            if ram < prop then
                return k
            end
        end
    end
end

--x=0.3 0.3概率发生
function lua_util.prob(x)
    if(x <= 0) then
        return false
    end
    return (math.random() <= x)
end

--{0.2,0.3,0.5}，总和1,返回按各自概率返回索引20%返回1，30%返回2,50%返回3
function lua_util.choice(x)
    local d = math.random()
    local sum = 0
    for k,v in pairs(x) do
        if d <= v + sum then
            return k
        end
        sum = sum + v
    end
    --容错，针对概率和不为1的情形
    local max_index
    local max_prob = 0
    for k,v in pairs(x) do
        if v > max_prob then
            max_index = k
        end
    end 
    return max_index
end

--从一个列表中随机一个值
function lua_util.choose_1(t)
    local n = #t
    if n == 0 then
        return nil
    end
    return t[math.random(1, n)]
end

--从一个列表中随机一个值,从其关联列表也返回一个值
function lua_util.choose_2(t, t2)
    local n = #t
    if n == 0 then
        return nil
    end
    local idx = math.random(1, n)
    return t[idx],t2[idx]
end

--从不等概率的一组值里面随机选择个
--table格式如：{[值]=概率}；函数返回 值，概率
function lua_util.getrandomseed(a)
    if type(a)=="table" then
        local max=0
        for k,v in pairs(a) do
            max=max+v
        end
        local seed=math.random(1,max)
        local sumvv=0
        for kk,vv in pairs(a) do
            sumvv=sumvv+vv
            if seed<=sumvv then
                return kk,vv
            end
        end
    end
end

--获取一个表中不为nil的项的数目
function lua_util.get_table_real_count(t)
    local i = 0
    for k,v in pairs(t) do
        i = i + 1
    end
    return i
end

--判断table是否为{}或nil
function lua_util.is_table_empty(t)
    if not t then return true end
    return next(t) == nil
end

--获取一个表中第一个可插入的位置(为nil的项的索引值)
function lua_util.get_table_insert_pos(t)
    local maxn = table.maxn(t)
    for i=1, maxn do
        if t[i] == nil then
            return i
        end
    end
    return maxn + 1
end

--在表里第一个为nil的位置插入一个新项,并且返回该位置
function lua_util.insert_table(t, item)
    local pos = lua_util.get_table_insert_pos(t)
    t[pos]=item
    return pos
end

--计算utf编码字符串的长度
local _utf_arr = {0, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc}
local _utf_arr_len = #_utf_arr
----------------------------------------------------
--获取某个utf8字符串某个索引开始对应的编码字节长度
--@param str: 字符串
--@param begin: 开始位置
----------------------------------------------------
function lua_util.get_utf8_char_byte_cnt(str, begin)
    local count = 1
    local begin_code = string.byte(str, begin, begin)
    for i = _utf_arr_len, 1, -1 do
        if begin_code >= _utf_arr[i] then
            count = i
            break
        end
    end
    return count
end

----------------------------------------
--根据utf8的编码规则，获取字符串某个位置开始处单个字符所对应的编码和字节长度
--@param str: 字符串
--@param begin: 开始位置
----------------------------------------
function lua_util.get_utf8_char_encode(str, begin)
    local encode = 0
    local count = lua_util.get_utf8_char_byte_cnt(str, begin)
    local begin_code = string.byte(str, begin, begin)
    local code = bit32.band(begin_code, 255 - _utf_arr[count])
    encode = bit32.lshift(code, ((count - 1) * 6))
    for i = 1, count - 1 do
        local index = begin + i
        code = bit32.band(string.byte(str, index, index), 0x7f)
        encode = encode + bit32.lshift(code,  (count - 1 - i) * 6)
    end
    return encode, count
end

--求utf8编码的字符个数，1个汉字相当于2个英文字符
function lua_util.utfstr_len(str)
    local len = #str
    local begin = 1
    local cnt = 0
    while begin <= len do
        local tmp = string.byte(str, begin, begin)
        for i = _utf_arr_len, 1, -1 do
            if tmp >= _utf_arr[i] then
                begin = begin + i
                break
            end
        end
        if tmp >= _utf_arr[2] then --中文字符
            cnt = cnt + 2
        else --英文字符
            cnt = cnt + 1
        end
    end
    return cnt
end

--保留一定长度的字符串
function lua_util.utf_str_cut(str, len)
    local length = #str
    local begin = 1
    local cnt = 0
    while begin <= length do
        local tmp = string.byte(str, begin, begin)
        for i = _utf_arr_len, 1, -1 do
            if tmp >= _utf_arr[i] then
                begin = begin + i
                break
            end
        end
        cnt = cnt + 1
        if cnt == len then
            return string.sub(str, 1, begin)
        end
    end
    return str
end

-------------------------------------------
--检查某个utf8编码的字符串是否含有char单个字符
--@param str: 某个utf8编码的字符串
--@param char: 某个utf8编码的单个字符
-------------------------------------------
function lua_util.utfstr_check_char(str, char)
    local char_encode = lua_util.get_utf8_char_encode(char, 1)
    local len = #str
    local begin = 1
    while begin < len do
        local encode, count = lua_util.get_utf8_char_encode(str, begin)
        if encode == char_encode then
            return true
        else
            begin = begin + count
        end
    end
    return false
end

--检查字符串是否是含有中文
function lua_util.check_utfstr_contain_chinese(str)

    local len = #str
    for i=1, len do
        local cur_byte = string.byte(str, i)
        if cur_byte >= 127 then
            return true
        end
    end

    return false
end


--获取当前时间到下一个X分钟时刻点的剩余秒数
--比如当前时间为15:09分,到下一个5分钟时刻点的剩余时间为1分
--到下一个15分钟时刻点的剩余时间为6分,
--到下一个30分钟时刻点的剩余时间为21分,
--到下一个60分钟时刻点的剩余时间为51分,
-- -- -- 等等
function lua_util.get_left_secs_until_next_x_mins(min_interval)
    --这个值不能大于60分钟
    if min_interval > 60 then
        min_interval = 60
    end

    local now_secs = os.time() % 3600  --当前时间的秒数
    local sec_int = min_interval * 60
    local left = sec_int - now_secs % sec_int
    return left
end

--获取当前时间到下一个hh:mi:ss的剩余秒数
function lua_util.get_left_secs_until_next_hhmiss(nh, nm, ns, cur_time)
    local now = os.date('*t', cur_time or os.time())

    local s1 = now.hour * 3600 + now.min * 60 + now.sec
    local s2 = nh * 3600 + nm * 60 + ns
    local delta_s = s2 - s1
    if delta_s < 0 then
        --加一天的时间
        delta_s = delta_s + 86400
    end

    return delta_s
end

--获取当前时间到下一个星期:hh:mi:ss的剩余秒数
function lua_util.get_secs_until_next_wdate(nd, nh, nm, ns)
    local now = os.date('*t', os.time())
    local delta_d = nd - now.wday
    if delta_d < 0 then
        delta_d = delta_d + 7
    end
    local s1 = now.hour * 3600 + now.min * 60 + now.sec
    local s2 = nh * 3600 + nm * 60 + ns
    local delta_s = s2 - s1
    if delta_s <= 0 and delta_d == 0 then
        delta_s = delta_s + 86400 * 7
    else
        delta_s = delta_s + 86400 * delta_d
    end
    return delta_s
end

--获取下一个hh:mi:ss的秒数
function lua_util.get_secs_until_next_hhmiss(nh, nm, ns)
    local now = os.time()
    local date_n = os.date('*t')

    local s1 = date_n.hour * 3600 + date_n.min * 60 + date_n.sec
    local s2 = nh * 3600 + nm * 60 + ns
    local delta_s = s2 - s1
    if delta_s < 0 then
        --加一天的时间
        delta_s = delta_s + 86400
    end

    return delta_s + now
end

--返回当前是星期几，星期一返回1，星期日返回7
function lua_util.get_wday(from_time)
    local now = from_time or os.time()
    local now_tm = os.date('*t',now)
    local wday = now_tm.wday
    wday = wday - 1
    if 0 == wday then
        wday = 7
    end
    return wday
end

function lua_util.get_day(from_time)
    local now = from_time or os.time()
    local now_tm = os.date('*t', now)
    return now_tm.day
end

--计算当天时间是年内的第几周
function lua_util.get_weekth_of_year()
    local nowYear = tonumber(os.date("%Y"))
    local theTime = os.time({year=nowYear,month=1,day=1})
    local wDay = os.date("*t", theTime).wday - 1
    --星期天变为一周的第七天
    if wDay <= 0 then 
        wDay = 7 
    end 
    local yDay = os.date("*t").yday
    local theWeek = math.floor( (wDay + yDay - 1 + 6) / 7)
    return theWeek
end

--获取YYYYMMDD格式的日期
function lua_util.get_yyyymmdd(one_time)
    local one_time = one_time or os.time()
    return os.date("%Y%m%d", one_time)
end

--解析时间
function lua_util.parse_time_str(time_str)
    local i = string.find(time_str,' ',1)
    local str_data = string.sub(time_str,1,i-1)
    local str_time = string.sub(time_str,i+1)
    local dd = lua_util.split_str(str_data,'-',tonumber)
    local tt = lua_util.split_str(str_time,':',tonumber)
    local result = os.time{year=dd[1],month=dd[2],day=dd[3],hour=tt[1],min=tt[2],sec=tt[3]}
    return result
end

function lua_util.date_number_to_year_month_day(date_number)
    local year = math.floor(date_number / 10000)
    local month = math.floor( (date_number - year* 10000 ) / 100 )
    local day = date_number - year* 10000 - month * 100
    return year,month,day
end

function lua_util.get_cross_week(week1,week2) 
    --data_number2 = week2 > date_number1 = week1 --如果反转，为负数的跨周数
    local week1_year,week1_month,week1_day = lua_util.date_number_to_year_month_day(week1)
    local week2_year,week2_month,week2_day = lua_util.date_number_to_year_month_day(week2)

    local week1_time = os.time({year=week1_year,month=week1_month,day=week1_day})
    local week2_time = os.time({year=week2_year,month=week2_month,day=week2_day})
    local cross_week = ((week2_time - week1_time) / 86400)/7
    
    return math.floor(cross_week)
end

function lua_util.get_cross_day(date1,date2)
    local year1,month1,day1 = lua_util.date_number_to_year_month_day(date1)
    local year2,month2,day2 = lua_util.date_number_to_year_month_day(date2)

    local time1 = os.time({year=year1,month=month1,day=day1})
    local time2 = os.time({year=year2,month=month2,day=day2})
    local cross_day = ((time2 - time1) / 86400)
    
    return math.floor(cross_day)
end

--获取某个时间所属周的周一所对应的日期的格式化数值，格式为year*10000+month*100+day。例如得到20170717。当前周的礼拜一。结合每天凌晨5点检查得到是跨凌晨5点。
function lua_util.get_monday_date_number(from_time)
    local now = from_time or os.time()
    local now_tm = os.date('*t', now)
    local wday = now_tm.wday    --lua中sunday是1
    if wday == 1 then
        wday = 8
    end

    local time1 = now - (wday - 2)*86400
    local time1_tm = os.date('*t', time1)
    return time1_tm.year*10000 + time1_tm.month*100 + time1_tm.day
end

--获取某个时间所属月的1号所对应的日期的格式化数值，格式为year*10000+month*100+day。例如得到20170701，当月1号。结合每天凌晨5点检查得到是跨凌晨5点。
function lua_util.get_month_1_date_number(from_time)
    local from_time = from_time or os.time()
    local time_fmt_tbl = os.date("*t", from_time)
    return time_fmt_tbl.year * 10000 + time_fmt_tbl.month * 100 + 1
end

--判断两个时间戳是否为同一天
function lua_util.is_same_day(stamp1, stamp2)
    local day1 = tonumber(os.date("%y%m%d", stamp1))
    local day2 = tonumber(os.date("%y%m%d", stamp2))
    return day1 == day2
end

--判断两个时间戳是否为同一月
function lua_util.is_same_month(stamp1, stamp2)
    local month1 = tonumber(os.date("%y%m", stamp1))
    local month2 = tonumber(os.date("%y%m", stamp2))
    return month1 == month2
end

function lua_util.is_same_week(stamp1, stamp2)
    local monday_date_number1 = lua_util.get_monday_date_number(stamp1)
    local monday_date_number2 = lua_util.get_monday_date_number(stamp2)
    return monday_date_number1 == monday_date_number2
end

-------------------------------
--判断某个时间的日期格式化后的数值， 格式为：year*10000 + month*100 + day
-------------------------------
function lua_util.get_date_format_number(one_time)
    local one_time = one_time or os.time()
    local time_fmt_tbl = os.date("*t", one_time)
    local time_number = time_fmt_tbl.year * 10000 + time_fmt_tbl.month * 100 + time_fmt_tbl.day
    return time_number
end

--获取某个时间的时分字段格式化后的数值，格式为:hour*10000+minute*100+second
function lua_util.get_hms_format_number(one_time)
    one_time = one_time or os.time()
    local time_fmt_tbl = os.date("*t", one_time)
    return time_fmt_tbl.hour * 10000 + time_fmt_tbl.min * 100 + time_fmt_tbl.sec
end

------------
--注意：这个函数不对。
--分到时是60进的，假设17,50,0,30，那么18:00就过期了。所以不能是持续时间，而是计算成结束时间。
--参考使用的is_time_in_hm
--------------------------------
--判断某个时间是否在某个时间段内
--@param one_time: 某个时间
--@param begin_time: 开始时间，格式为：hour*100 + minutes
--@param dur_time: 持续时间，单位为秒
--------------------------------
function lua_util.is_time_in_hm_range(one_time, begin_time, dur_time)
    one_time = one_time or os.time()
    local one_time_hms_num = lua_util.get_hms_format_number(one_time)
    
    local begin_time_hms_num = begin_time * 100
    
    local dur_time_hour = math.floor(dur_time / 3600)
    local dur_time_min = math.floor((dur_time - dur_time_hour * 3600) / 60)
    local dur_time_sec = dur_time - dur_time_hour * 3600 - dur_time_min * 60
    
    local dur_time_num = dur_time_hour * 10000 + dur_time_min * 100 + dur_time_sec
    
    return one_time_hms_num >= begin_time_hms_num and one_time_hms_num < begin_time_hms_num + dur_time_num
end

function lua_util.is_time_in_hm(cur_time, begin_hms_num, end_hms_num)
    cur_time = cur_time or os.time()
    local cur_time_hms_num = lua_util.get_hms_format_number(cur_time)
    --log_d("lua_util.is_time_in_hm    1 ", "cur_time_hms_num=%s", cur_time_hms_num)
    
    return cur_time_hms_num >= begin_hms_num and cur_time_hms_num < end_hms_num
end

--上一次时间到现在跨了多少个夜晚
function lua_util.time_pass_day(last_time)
    if lua_util.is_same_day(last_time, os.time()) then
       return 0 
    end
    local now_tm = os.date('*t',os.time())
    local cur_year = now_tm.year
    local cur_month = now_tm.month
    local cur_day = now_tm.day
    local cur_hour = now_tm.hour
    
    local old_tm = os.date('*t', last_time)
    local old_year = old_tm.year
    local old_month = old_tm.month
    local old_day = old_tm.day
    local old_hour = old_tm.hour
    
    local stime1 = os.time({year=old_year,month=old_month,day=old_day, hour = 23, min = 59, sec = 59})
    local stime2 = os.time({year=cur_year,month=cur_month,day=cur_day, hour=0})
    local pass_day = math.floor((stime2 - stime1)/86400) + 1
    return pass_day
end

--获取table中key的值,如果为nil则返回缺省值
function lua_util.get_table_value(t, key, df)
    local v = t[key]
    if v == nil then
        return df
    end

    return v
end

function lua_util.get_key_by_value(tbl, value)
    for k,v in pairs(tbl) do
        if v == value then
            return k
        end
    end
end


--判断数组中是否有某个元素
function lua_util.is_in_table_list(tbl,value)
    for k, v in pairs(tbl) do
        if value == v then
            return true
        end
    end
    return false
end

--判断数组中某个元素所在的索引
function lua_util.get_value_index_in_array(value, tbl)
    for index, v in ipairs(tbl) do
        if value == v then
            return index
        end
    end
    return -1
end

----------------------
--从数组中删除某个元素
--@param value: 某个值
--@param array： 某个数组
--@param is_all: 表示是否删除所有的value，还是只删除最前面的一个
----------------------
function lua_util.del_value_in_array(value, array, is_all)
    local b_find = false
    if not is_all then
        for i = 1, #array do
            if array[i] == value then
                table.remove(array, i)
                b_find = true
                break
            end
        end
    else
        for i = #array, 1, -1 do
            if array[i] == value then
                table.remove(array, i)
                b_find = true
            end
        end
    end
    return b_find
end

--获取某字符串pos上的字符
function lua_util.get_char_at(s,pos)
    return string.sub(s, pos, pos)
end


--将字符串s的pos位置设置成为newchar
function lua_util.set_char_value(s, pos, newchar)
    local s1 = string.sub(s, 1, pos - 1)
    local s2 = string.sub(s, pos + 1, string.len(s))
    s1 = s1..newchar..s2
    return s1
end

--判断四个数字互不相等
function lua_util.fournum_is_diff(a, b, c, d)
    local t = {}
    t[a] = 1
    t[b] = 1
    t[c] = 1
    t[d] = 1
    if lua_util.get_table_real_count(t) == 4 then
        return true
    end
    return false
end

--获取x的整数部分
function lua_util.getIntPart(x)
    --[[if x <= 0 then
       return math.ceil(x);
    end

    if math.ceil(x) == x then
       x = math.ceil(x);
    else
       x = math.ceil(x) - 1;
    end
    return x;]]
    local a = math.modf(x)
    return a
end

--获取个位数数字
function lua_util.get_data1(value)
    return math.mod(value, 10)
end

--获取十位数数字
function lua_util.get_data2(value)
    return lua_util.getIntPart(value / 10)
end

function lua_util.binary_search(array, len, v, func)
    local left = 1
    local right = len
    local middle = 0

    while left < right + 1 do
        middle = (left + right) / 2
        if func(array[middle]) < v then
            left = middle + 1
        elseif func(array[middle]) > v then
            right = middle - 1
        else
            return middle
        end
    end

    return 1
end

--打出调用堆栈
function lua_util.traceback()
    for level = 1, math.huge do
        if not lua_util.print_stack("stack", level, print) then break end
    end
end

--打印出某个函数的层次信息
function lua_util.print_stack(prefix, level, func)
    local t = debug.getinfo(level,'Sln')

    if not t then return false end

    local debug_table = {}

    for k,v in pairs(t) do
        if v and k == 'name' or k == 'currentline' or k =='short_src' then
            debug_table[#debug_table + 1] = string.format('%s=%s',tostring(k),tostring(v))
        end
    end

    func(string.format('%s: {%s}',prefix,table.concat(debug_table,' ')))
    return true
end

--打印table， 会在启动mogosrv的控制台输出
--@tab_num 递归使用，使用时不用管
function lua_util.print_table(tbl , tab_num)
    if not tab_num then tab_num = 0 end
    local tabs = ""
    for i = 1, tab_num do
        tabs = tabs.."    "
    end
    for key, val in pairs(tbl) do
        if type(val) == "table" then
            print(string.format("%s%s = {", tabs, key))
            tab_num = tab_num + 1
            lua_util.print_table(val, tab_num)
            tab_num = tab_num - 1
            print(string.format("%s}", tabs))
        else
            print(string.format("%s%s = %s", tabs, key, val))
        end
    end
end


--转化table为字符串，比较紧凑，但是格式不友好
function lua_util.format_table(tbl, depth)
    depth = depth or 1
    if depth >= 10 then  --避免循环引用导致死循环
        return "format_table_error"
    end
    local out_parts = {}
    for key, val in pairs(tbl or {}) do
        if type(val) == "table" then
            local val_str = lua_util.format_table(val, depth + 1)
            table.insert(out_parts, string.format("%s=%s", key, val_str))
        else
            table.insert(out_parts, string.format("%s=%s", key, val))
        end
    end
    return string.format("{%s}", table.concat(out_parts, ","))
end


--友好格式化一个表，避免负责结构容易看错嵌套关系
--@depth 递归深度，使用时不用管
function lua_util.format_table_ex(tbl , depth)
    local out_parts = {}
    if not depth then depth = 1 end
    if depth >= 10 then  --避免循环引用导致死循环
        return "format_table_error"
    end
    local tabs = ""
    for i = 1, depth do
        tabs = tabs .. "    "
    end
    for key, val in pairs(tbl) do
        if type(val) == "table" then
            table.insert(out_parts, string.format("%s%s = {", tabs, key))
            depth = depth + 1
            local tbl_str = lua_util.format_table_ex(val, depth)
            table.insert(out_parts, tbl_str)
            depth = depth - 1
            table.insert(out_parts, string.format("%s}", tabs))
        else
            table.insert(out_parts, string.format("%s%s = %s", tabs, key, val))
        end
    end
    if depth == 1 then
        return string.format("\n{\n%s\n}", table.concat(out_parts, "\n"))
    else
        return table.concat(out_parts, "\n")
    end
end

--深度拷贝
--@depth 深度
--@src 源
--@dest 目标
--@return 同dest
function lua_util.deep_copy(depth, src, dest)
    if type(src) ~= "table" then return src end
    if depth < 1 then return end
    if not dest then dest = {} end
    for k,v in pairs(src) do
        if type(v) == "table" then
            if depth <= 1 then
                dest[k] = v
            else
                dest[k] = lua_util.deep_copy(depth-1, v)
            end
        else
            dest[k] = v
        end
    end
    return dest
end

--克隆表格
function lua_util.clone_table(src, dest)
    return lua_util.deep_copy(10, src, dest)
end

--deepcopy,只copy1层
function lua_util.deepcopy_1(t)
    return lua_util.deep_copy(1, t)
end

--删除数组的前n个元素
function lua_util.del_multi_ele_from_array(src, n)
    if n <= 0 then return end
    local count = #src
    if count > n then
        for i = 1, count - n do
            src[i] = src[i + n]
        end
        for i = count, count - n + 1, -1 do
            table.remove(src, i)
        end
    else
        for i = count, 1, -1 do
            table.remove(src, i)
        end
    end
end

--str="k:v,k:v"变成table字典
function lua_util.str_to_dict(str)
    local tmp = lua_util.split_str(str, ',')
    local tmp2 = {}
    for _, v in pairs(tmp) do
        local tmp3 = lua_util.split_str(v, ':')
        local _key = tonumber(tmp3[1]) or tmp3[1]
        local _value = tonumber(tmp3[2]) or tmp3[2]
        tmp2[_key] = _value
    end
    return tmp2
end

function lua_util.random_one_ex(weight)
    local sum = 0
    for _, v in pairs(weight) do
        sum = sum + v
    end
    return lua_util.random_one(weight, sum)
end

--dict = {k:v,k:v}字典,v是概率，返回k
--list={1:v1,2:v2}列表,一样
function lua_util.random_one(data, prob_range)
    if prob_range == nil then
        prob_range = 10000
    end
    local ram = math.random() * prob_range
    local prob = 0
    for k,v in pairs(data) do
        prob = prob + v
        if ram <= prob then
            return k
        end
    end
end

--每个都单独判断,多个
function lua_util.random_each(data,prob_range)
    if prob_range == nil then
        prob_range = 10000
    end
    local tmp ={}
    for k,v in pairs(data) do
        local ram = math.random() * prob_range
        local prob = v
        if ram <= prob then
            table.insert(tmp,k)
        end
    end
    return tmp
end

----从M个数里(等概率)随机出N个不重复的数
--function lua_util.choose_n_norepeated( t, n )
--    local m = #t
--    --集合数目不够抽取数目
--    if m <= n then
--        return t
--    end
--
--    local t2 = {}   --记录已经抽中的结果
--    local t3 = {}    --返回结果
--    local i = 0     --已抽取个数
--    local mrandom = math.random
--    while true do
--        local r = mrandom(1,m)
--        if not t2[r] then
--            table.insert(t3, t[r])
--            t2[r] = 1
--            i = i+1
--            if i >= n then
--                return t3
--            end
--        end
--    end
--
--    return t2
--end

--从M个数里(等概率)随机出N个不重复的数，并且返回所选中的项位置
function lua_util.choose_n_norepeated2( t, n )
    local t2 = {}   --记录已经抽中的结果
    local t3 = {}    --返回结果
    local m = #t
    --集合数目不够抽取数目
    if m <= n then

        local t2 = {}
        for i=1, #t do
            t2[i] = 1
            t3[i] = t[i]
        end

        return t3, t2
    end

    local i = 0     --已抽取个数
    local mrandom = math.random
    while true do
        local r = mrandom(1,m)
        if not t2[r] then
            table.insert(t3, t[r])
            t2[r] = 1
            i = i+1
            if i >= n then
                return t3, t2
            end
        end
    end

    return t3, t2
end

----从{k = prob}表里挑选一个满足概率的k
--function lua_util.choose_prob(t, min_prob, max_prob)
--    local ram
--    if min_prob and max_prob then
--        ram = math.random(min_prob, max_prob)
--    else
--        ram = math.random()
--    end
--    local prob = 0
--    for k, prob1 in pairs(t) do
--        prob = prob + prob1
--        if ram <= prob then
--            return k
--        end
--    end
--end

----从格式"道具id1,数量1,概率1,道具id2,数量2,概率2,..."中随机出一个道具id和数量
--function lua_util.choose_random_item(drop)
--    if drop then
--        local ram = math.random()
--        local n = #drop
--        local prop = 0
--        for i=3, n, 3 do
--            local p = drop[i]
--            if p then
--                prop = prop + p
--                if ram < prop then
--                    return {drop[i-2], drop[i-1]}
--                end
--            end
--        end
--    end
--end

----x=0.3 0.3概率发生
--function lua_util.prob(x)
--    if(x <= 0) then
--        return false
--    end
--    return (math.random() <= x)
--end

----{0.2,0.3,0.5}，总和1,返回按各自概率返回索引20%返回1，30%返回2,50%返回3
--function lua_util.choice(x)
--    local d = math.random()
--    local sum = 0
--    for k,v in pairs(x) do
--        if d <= v + sum then
--            return k
--        end
--        sum = sum + v
--    end
--    return nil
--end

----从一个列表中随机一个值
--function lua_util.choose_1(t)
--    local n = #t
--    if n == 0 then
--        return nil
--    end
--    return t[math.random(1, n)]
--end

----从一个列表中随机一个值,从其关联列表也返回一个值
--function lua_util.choose_2(t, t2)
--    local n = #t
--    if n == 0 then
--        return nil
--    end
--    local idx = math.random(1, n)
--    return t[idx],t2[idx]
--end

--从不等概率的一组值里面随机选择个
--table格式如：{[值]=概率}；函数返回 值，概率
function lua_util.getrandomseed(a)
    if type(a)=="table" then
        local max=0
        for k,v in pairs(a) do
            max=max+v
        end
        local seed=math.random(1,max)
        local sumvv=0
        for kk,vv in pairs(a) do
            sumvv=sumvv+vv
            if seed<=sumvv then
                return kk,vv
            end
        end
    end
end

--分页获取数据。若page_index为0则返回原来的所有数据。
--@all_data 所有数据的数组集合。注意不要传字典.不兼容字典是因为字典的遍历是乱序的，影响了原数据的顺序。
--@page_index 第几页，编号从1开始
--@cnt_every_page 一页的数据个数。
function lua_util.get_data_by_page(all_data, page_index, cnt_every_page)
    if page_index <= 0 then 
        return all_data, 1 
    end
    local result = {}
    local first_index = (page_index - 1) * cnt_every_page + 1  --page_index页第一个数据在所有数据中的index
    local last_index = math.min(#all_data, first_index + cnt_every_page - 1)  --page_index页最后一个数据在所有数据中的index
    for i = first_index, last_index do
        table.insert(result, all_data[i])
    end
    return result, (#all_data + cnt_every_page - 1)/cnt_every_page
end


-------------------------------
--@all_data 所有数据的数组集合。注意不要传字典.不兼容字典是因为字典的遍历是乱序的，影响了原数据的顺序。
--@begin_index: 有效数据的开始索引
--@end_index:   有效数据的结束索引
--@page_index 第几页。
--@cnt_every_page 一页的数据个数。
--------------------------------
function lua_util.get_data_by_page_ex(all_data, begin_index, end_index, page_index, cnt_every_page)
    local result = {}
    local table_insert = table.insert
    if page_index <= 0 then  --获取全部数据
        for i = begin_index, end_index do
            table_insert(result, all_data[i])
        end
        return result, 1
    else
        --page_index页第一个数据在所有数据中的index
        local first_index = begin_index + (page_index - 1) * cnt_every_page + 1
        --page_index页最后一个数据在所有数据中的index
        local last_index = math.min(end_index, begin_index + cnt_every_page - 1)
        for i = first_index, last_index do
            table_insert(result, all_data[i])
        end
        return result, (#all_data + cnt_every_page - 1)/cnt_every_page
    end
end


--将字典转化成数组
function lua_util.transform_dict_to_array(dict)
    local result = {}
    for _, data in pairs(dict) do
        table.insert(result, data)
    end
    return result
end


------------------------
--在某个有序队列查找某个元素
--get_score：是个函数，用来对元素进行评分
------------------------
function lua_util.search_in_sorted_array(src_ary, ele, get_score)
    local lo = 1
    local hi = #src_ary
    local floor = math.floor
    local mid
    local score = get_score(ele)
    while lo <= hi do
        mid = floor((lo + hi)/2)
        if score >= get_score(src_ary[mid]) then
            lo = mid + 1
        else
            hi = mid - 1
        end
    end
    for i = lo - 1, 1, -1 do
        if src_ary[i] == ele then
            return i
        end
    end
end

------------------------
--在某个有序队列中插入某个元素
--@param src_ary:  一个数组
--@param new_ele:  某个元素
--get_score：是个函数，用来对元素进行评分
------------------------
function lua_util.insert_in_sorted_array(src_ary, new_ele, get_score)
    local count = #src_ary
    if #src_ary == 0 then
        table.insert(src_ary, new_ele)
        return 1
    else
        local lo = 1
        local hi = count
        local floor = math.floor
        local mid
        local new_score = get_score(new_ele)
        while lo <= hi do
            mid = floor((lo + hi)/2)
            if new_score >= get_score(src_ary[mid]) then
                lo = mid + 1
            else
                hi = mid - 1
            end
        end
        table.insert(src_ary, lo, new_ele)
        return lo
    end
end

------------------------
--在某个有序队列中删除某个元素
--get_score：是个函数，用来对元素进行评分
------------------------
function lua_util.del_in_sorted_array(src_ary, ele, get_score)
    local index = lua_util.search_in_sorted_array(src_ary,ele,get_score)
    if index and index > 0 then
        table.remove(src_ary, index)
    end
end

------------------------
--在某个有序队列中查找元素评分为某个值的开始和结束索引
--@get_score：是个函数，用来对元素进行评分
------------------------
function lua_util.search_score_in_sorted_array(src_ary, score, get_score)
    local lo = 1
    local hi = #src_ary
    local floor = math.floor
    local mid
    while lo <= hi do
        mid = floor((lo + hi)/2)
        local mid_score = get_score(src_ary[mid])
        if score >= mid_score then
            lo = mid + 1
        else
            hi = mid - 1
        end
    end
    local b_index, e_index
    b_index = 0
    e_index = lo - 1
    for i = e_index, 1, -1 do
        if get_score(src_ary[i]) ~= score then
            b_index = i
            break
        end
    end
    return b_index+1, e_index
end


------------------------
--在某个有序队列中插入某个元素
--cmp_func：是个函数，用来对两个元素进行大小判断，-1是小于，0是等于，1是大于
------------------------
function lua_util.insert_in_sorted_array_ex(src_ary, new_ele, cmp_func)
    local count = #src_ary
    if #src_ary == 0 then
        table.insert(src_ary, new_ele)
        return 1
    else
        local lo = 1
        local hi = count
        local mid
        while lo <= hi do
            mid = math.floor((lo + hi)/2)
            if cmp_func(new_ele, src_ary[mid]) >= 0 then
                lo = mid + 1
            else
                hi = mid - 1
            end
        end
        table.insert(src_ary, lo, new_ele)
        return lo
    end
end

------------------------
--在某个有序队列中删除某个元素
--cmp_func：是个函数，用来对两个元素进行大小判断，-1是小于，0是等于，1是大于
------------------------
function lua_util.del_in_sorted_array_ex(src_ary, ele, cmp_func)
    local lo = 1
    local hi = #src_ary
    local floor = math.floor
    local mid
    while lo <= hi do
        mid = floor((lo + hi)/2)
        if cmp_func(ele, src_ary[lo]) >= 0 then
            lo = mid + 1
        else
            hi = mid - 1
        end
    end
    for i = lo - 1, 1, -1 do
        if src_ary[i] == ele then
            table.remove(src_ary, i)
            return i
        end
    end
end


--keys和values都是列表，对应得到字典
function lua_util.parse_keys_values_to_dict(keys,values)
    local tmp = {}
    for k,id in ipairs(keys) do
        local value = values[k] or 0
       
        if id and value then
            tmp[id] = value
        end
    end
    return tmp
end

--keys和values都是列表，对应得到list
function lua_util.parse_keys_values_to_list(keys,values)
    local tmp = {}
    for k,id in ipairs(keys) do
        local value = values[k] or 0
        
        if id and value then
            table.insert(tmp,{id,value})
        end
    end
    return tmp
end

------------------------
--将第二个数组中的某部分元素合并到第一个数组中
------------------------
function lua_util.concat_array(arr1, arr2, begin_i, end_i)
    local begin_i = begin_i or 1
    local end_i = end_i or #arr2
    for i = begin_i, end_i do
        table.insert(arr1, arr2[i])
    end
end

---------------
--将tbl_2合并到tbl_1,
--@param tbl_1:
--@param tbl_2:
--@param b_overwrite: 如果有重复的key，是否合并该key
-------------
function lua_util.combine_table(tbl_1, tbl_2, b_overwrite)
    for key, value in pairs(tbl_2) do
        if not tbl_1[key] then
            tbl_1[key] = value
        elseif b_overwrite then
            tbl_1[key] = value
        end
    end
end

------------------------
--将多个数组中的元素合并为一个新数组
------------------------
function lua_util.combine_array(tbl_1, tbl_2)
    local new_tbl = {}
    local tbl_insert = table.insert
    for _, ele in pairs(tbl_1) do
        tbl_insert(new_tbl, ele)
    end
    for _, ele in pairs(tbl_2) do
        tbl_insert(new_tbl, ele)
    end
    return new_tbl
end

--------------------
--将一个数组清空
-------------------
function lua_util.clear_array(tbl)
    for i = #tbl, 1, -1 do
        table.remove(tbl, i)
    end
end

--求数组的和
function lua_util.sum(array)
    local total = 0
    for i=1,#array do
        total = total + array[i]
    end
    return total
end

--求数组的最大值
function lua_util.array_max(array)
    local max = array[1]
    for i = 2, #array do
        if array[i] > max then
            max = array[i]
        end
    end
    return max
end

--求数组的最小值
function lua_util.array_min(array)
    local min = array[1]
    for i = 2, #array do
        if array[i] < min then
            min = array[i]
        end
    end
    return min
end

-------------------------------
--在某个数组的任意m个元素的组合中，找到满足某种条件的一个组合
--@param arry: 输入数组
--@param m: 组合的个数
--@param callback: 某个回调函数，当该回调函数返回false时，继续查找其他组合，为true时，停止其他组合的查找，并返回该组合
--@param max_cnt: 最大搜索次数,不传时，默认为1000
-------------------------------
function lua_util.list_array_combine(arry, m, callback, max_cnt)
    local arry_cnt = #arry
    if arry_cnt == 0 then
        if callback(arry, arry) then
            return arry
        else
            return nil
        end
    end

    if m <= arry_cnt then
        local function is_finished(visited)
            for j = 1, #visited do
                if visited[j] == 1 then
                    return j == #visited - m + 1
                end
            end
            return false
        end
        max_cnt = max_cnt or 1000
        local one_combine = {}
        local combined_cnt = 0
        local t_insert = table.insert
        for i = 1, m do
            t_insert(one_combine, 1)
        end
        for i = m + 1, arry_cnt do
            t_insert(one_combine, 0)
        end
        if callback(arry, one_combine) then
            return one_combine
        end
        --开始做1,0的交换操作
        while not is_finished(one_combine) do
            for i = 1, arry_cnt - 1 do
                if one_combine[i] == 1 and one_combine[i+1] == 0 then
                    one_combine[i] = 0
                    one_combine[i+1] = 1
                    --将1全部移到最左端
                    local one_count = 0
                    for j = 1, i - 1 do
                        if one_combine[j] == 1 then
                            one_count = one_count + 1
                        end
                    end
                    for j = 1, one_count do
                        one_combine[j] = 1
                    end
                    for j = one_count + 1, i - 1 do
                        one_combine[j] = 0
                    end
                    if callback(arry, one_combine) then
                        return one_combine
                    end
                    break
                end
            end
            combined_cnt = combined_cnt + 1
            if combined_cnt >= max_cnt then
                return
            end
        end
    end
end


--判断一个表是否为数组
function lua_util.table_is_array(tbl)
    return lua_util.get_table_real_count(tbl) == #tbl
end

function lua_util.table_keys(tbl)
    local tmp = {}
    for k,v in pairs(tbl) do
        tmp[k] = 1
    end
    return tmp
end

--获得“日期戳”
function lua_util.get_date(time_stamp, day_count)
    --return os.date("%y%m%d", time_stamp)
    time_stamp = time_stamp or os.time()
    day_count = day_count or 0
    local t = os.date("*t", time_stamp)
    local time_t = {
        year = t.year,
        month = t.month,
        day = t.day + day_count,
        hour = 0,
        min = 0,
        sec = 0,
    }
    return os.time(time_t)
end

--获得两个时间戳之间隔得天数
function lua_util.get_day_count(day_stamp1, day_stamp2)
    local date1 = lua_util.get_date(day_stamp1)
    local date2 = lua_util.get_date(day_stamp2)

    local sec_num = math.abs(date1 - date2)
    return math.floor(sec_num / 86400)
end

--洗牌函数（Fisher-Yates算法），直接修改传入列表，参数n代表当洗牌出前n个有效数即停止洗牌
function lua_util.shuffle(list, n)
    local count = math.min(n or (#list - 1), #list - 1)
    for i = 1, count do
        local r = math.random(i, #list)
        if i ~= r then
            --exchange list[i] and list[r]
            local swap_tmp  = list[i]
            list[i] = list[r]
            list[r] = swap_tmp
        end
    end
end

function lua_util.dbid_str(dbid)
    local s = string.format("%q", dbid)
    return string.sub(s, 2, (#s - 1))
end


local function _format_key_value(key, value)
    local nKeyLen = #key
    if nKeyLen > 2 then
        local prefix = string.sub(key, -2)
        local key2 = string.sub(key, 0, -3)
        if prefix == "_i" then
            return key2, tonumber(value)
        elseif prefix == "_d" then
            return key2, tonumber(value)
        elseif prefix == "_f" then
            return key2, tonumber(value)
        elseif prefix == "_s" then
            return key2, tostring(value)
        elseif prefix == "_l" then
            --列表
            return key2, lua_util.split_str(value, ',', tonumber)
        elseif prefix == "_k" then
            --key table
            local tmp = lua_util.split_str(value, ',', tonumber)
            local tmp2 = {}
            for _, k in pairs(tmp) do
                tmp2[k] = 1
            end
            return key2, tmp2
        --elseif prefix == "_t" then
        --    直接lua执行代码
        --    local f = loadstring('return ' .. value)
        --    return key2, loadstring('return ' .. value)
        elseif prefix == "_t" then
            --00:00:00
            local tmp = lua_util.split_str(value, ':')
            local sec = 0
            for i,v in ipairs(tmp) do
                local t = tonumber(v)
                if t then
                    sec = t + sec * 60
                end
            end
            return key2, sec
        elseif prefix == "_y" then
            --2014-01-25 0:12:00
            local i = string.find(value,' ',1)
            local str_data = string.sub(value,1,i-1)
            local str_time = string.sub(value,i+1)
            local dd = lua_util.split_str(str_data,'-',tonumber)
            local tt = lua_util.split_str(str_time,':',tonumber)
            return key2, os.time{year=dd[1],month=dd[2],day=dd[3],hour=tt[1],min=tt[2],sec=tt[3]}
        elseif prefix == "_m" then
            --字典
            local tmp = lua_util.split_str(value, ';')
            local tmp2 = {}
            for _, v in pairs(tmp) do
                local tmp = lua_util.split_str(v, ':')
                local id = tonumber(tmp[1]) or tmp[1]
                local num = tonumber(tmp[2]) or tmp[2]
                tmp2[id] = num
            end
            return key2, tmp2
        elseif prefix == "_n" then
            --字典(列表)
            local tmp = lua_util.split_str(value, ';')
            local tmp2 = {}
            for _, v in pairs(tmp) do
                local tmp = lua_util.split_str(v, ':')
                local id = tonumber(tmp[1]) or tmp[1]
                local num = lua_util.split_str(tmp[2], ',', tonumber)
                tmp2[id] = num
            end
            return key2, tmp2
        elseif prefix == "_o" then
            --字典(字典)
            local tmp = lua_util.split_str(value['value'], ';')
            local tmp2 = {}
            for _, v in pairs(tmp) do
                local tmp = lua_util.split_str(v, ':')
                local id = tonumber(tmp[1]) or tmp[1]
                local num = lua_util.split_str(tmp[2], ',', tonumber)
                local tmp3 = {}
                for i = 1,#num,2 do
                    tmp3[num[i]] = num[i+1]
                end
                tmp2[id] = tmp3
            end
            return key2, tmp2
        elseif prefix == "_w" then
            local tmp   = {}
            local tmp1  = lua_util.split_str(value, ';')
            for i, v in pairs(tmp1) do
                local tmp2 = lua_util.split_str(v, ',', lua_util.TryToNumber)
                table.insert(tmp, tmp2)
            end
            return key2, tmp
        else
            return key, value
        end
    else
        return key, value
    end
end

function lua_util.format_key_value(key, value)
    return _format_key_value(key, value)
end

--格式化一个table,该table只有一层关系
local function _format_table(t)
    local v2 = {}
    for key, value in pairs(t) do
        local key2, value2 = _format_key_value(key, value)
        v2[key2] = value2
    end

    return v2
end

lua_util.format_table = _format_table

--根据字段名后缀的含义修改从xml读取的数据类型
function lua_util.format_xml_table(t)
    local t2 = {}
    for k,v in pairs(t) do
        local v2 = _format_table(v)
        
        --注意：读表的key要注意，默认转为数字。
        --读表的key不能是字符串，因为如果字符串是数字形式，那么这个表的key就有是数字的和字符串。如果有数字和字符串，那么使用的参数key就要[tonumber(k) or k],这样容易出错。
        --所以要读表的key为字符串，那么用id_i读完再转。
        
        if tonumber(k) ~= nil then
            t2[tonumber(k)] = v2
        else
            t2[k] = v2
        end
    end
    return t2
end

--读取xml文件
local g_lua_rootpath = G_LUA_ROOTPATH
local engine_readXml = engine.readXml
function lua_util._readXml(path, key)
    local fn = g_lua_rootpath .. path
    local tmp = engine_readXml(fn, key)
    if tmp == nil then
        error(string.format("Failed to read '%s', format error or file not exists.", fn))
    end
    return lua_util.format_xml_table( tmp )
end

local engine_readXml2List = engine.readXmlToList
--根据两个关键字来读xml文件,例如科技表根据科技id和科技等级来决定相关的数据
--注意,这里的key和key2不能带后缀
function lua_util._readXmlBy2Key(path, key, key2)
    local fn = g_lua_rootpath .. path
    local tmp = engine_readXml2List(fn)

    local data = {}
    for k, v in pairs(tmp) do
        for k2, v2 in pairs(v) do
            --k,k2都是没用的字段
            local t = _format_table(v2)
            local vv1 = t[key]
            local vv2 = t[key2]

            local tt = data[vv1]
            if tt == nil then
                data[vv1] = { [vv2] = t }
            else
                tt[vv2] = t
            end
        end
    end

    return data
end

--根据三个关键字来读xml文件
--注意,这里的key、hey2和key3不能带后缀
function lua_util._readXmlBy3Key(path, key, key2, key3)
    local fn = g_lua_rootpath .. path
    local tmp = engine_readXml2List(fn)

    local data = {}
    for k, v in pairs(tmp) do
        for k2, v2 in pairs(v) do
            --k,k2都是没用的字段
            local t = _format_table(v2)
            local vv1 = t[key]
            local vv2 = t[key2]
            local vv3 = t[key3]

            local tt = data[vv1]
            if tt == nil then
                data[vv1] = {[vv2] = {[vv3] = t}}
            else
                local tt1 = tt[vv2]
                if tt1 == nil then
                    tt[vv2] = { [vv3] = t }
                else
                    tt1[vv3] = t
                end
            end
        end
    end

    return data
end

function lua_util._readXml2List(fn0)
    local fn = g_lua_rootpath .. fn0
    local tmp = engine_readXml2List(fn)

    if tmp then
        local data = {}
        for k, v in pairs(tmp) do
            for k2, v2 in pairs(v) do
                local t = _format_table(v2)
                table.insert(data, t)
            end
        end
        return data
    end
end

--读取两层结构的xml文件
function lua_util._read2dpXml(path, key)
    local fn = g_lua_rootpath .. path
    local tmp = engine_readXml(fn, key)
    if tmp == nil then
        error(string.format("Failed to read '%s', format error or file not exists.", fn))
    end
    local tmp2 = {}
    for k1, v1 in pairs(tmp) do
        --print(k1, v1)
        local tmp22 = {}
        for k2, v2 in pairs(v1) do
            --print(k2, v2)
            if type(v2) == 'table' then
                tmp22[k2] = _format_table(v2)
            else
                local kk2,vv2 = _format_key_value(k2, v2)
                tmp22[kk2] = vv2
            end
        end
        tmp2[tonumber(k1)] = tmp22
    end

    return tmp2
end

--读取一个表,取其中的一个key生成一个list
function lua_util.readXml2KeyList(fn0, key)
    local fn = g_lua_rootpath .. fn0
    local tmp = engine_readXml2List(fn)

    if tmp then
        local data = {}
        for k, v in pairs(tmp) do
            for k2, v2 in pairs(v) do
                local t = _format_table(v2)
                table.insert(data, t[key])
            end
        end
        return data
    end
end

--读取一个表,取其中的2个key生成2个list
function lua_util.readXml2KeyList2(fn0, key1,key2)
    local fn = g_lua_rootpath .. fn0
    local tmp = engine_readXml2List(fn)

    if tmp then
        local data = {}
        local data2 = {}
        for k, v in pairs(tmp) do
            for k2, v2 in pairs(v) do
                local t = _format_table(v2)
                table.insert(data, t[key1])
                table.insert(data2, t[key2])
            end
        end
        return {data,data2}
    end
end

--读取一个表,取两个key生成一个表{k=v}
function lua_util.readXml2KVDict(fn0, key1, key2)
    local fn = g_lua_rootpath .. fn0
    local tmp = engine_readXml2List(fn)

    if tmp then
        local data = {}
        for k, v in pairs(tmp) do
            for k2, v2 in pairs(v) do
                local t = _format_table(v2)
                data[t[key1]] = t[key2]
            end
        end
        return data
    end
end

--读取lua格式的配置文件
function lua_util.read_lua_config(path)
    local fn = g_lua_rootpath .. path
    local fd = io.open(fn, 'r')
    if fd then
        local config_str = fd:read("*a")
        local func = loadstring(string.format("local result = %s return result", config_str))

        local result = func()

        return lua_util.format_xml_table2(result)
    else
        log_w("lua_util.read_lua_config", "fn=%s", fn)
    end
end

function lua_util.format_key_value2(key, value, layer)

    if 'table' == type(value) then
        local v2 = lua_util.format_table2(value, layer)
        return key, v2
    end

    local nKeyLen = #key
    if nKeyLen > 2 then
        local prefix = string.sub(key, -2)
        local key2 = string.sub(key, 0, -3)
        if prefix == "_i" then
            return key2, tonumber(value)
        elseif prefix == "_d" then
            return key2, tonumber(value)
        elseif prefix == "_f" then
            return key2, tonumber(value)
        elseif prefix == "_s" then
            return key2, tostring(value)
        elseif prefix == "_l" then
            --列表
            return key2, lua_util.split_str(value, ',', tonumber)
        elseif prefix == "_k" then
            --填的数变为key
            local tmp = lua_util.split_str(value, ',', tonumber)
            local tmp2 = {}
            for _, k in pairs(tmp) do
                tmp2[k] = 1
            end
            return key2, tmp2
        elseif prefix == "_t" then
            --00:00:00
            local tmp = lua_util.split_str(value, ':')
            local sec = 0
            for i,v in ipairs(tmp) do
                local t = tonumber(v)
                if t then
                    sec = t + sec * 60
                end
            end
            return key2, sec
        elseif prefix == "_y" then
            --2014-01-25 0:12:00
            local i = string.find(value,' ',1)
            local str_data = string.sub(value,1,i-1)
            local str_time = string.sub(value,i+1)
            local dd = lua_util.split_str(str_data,'-',tonumber)
            local tt = lua_util.split_str(str_time,':',tonumber)
            return key2, os.time{year=dd[1],month=dd[2],day=dd[3],hour=tt[1],min=tt[2],sec=tt[3]}
        elseif prefix == "_m" then
            --字典
            if value == "" then
                return key2, {}
            end
            
            local tmp = lua_util.split_str(value, ';')
            local tmp2 = {}
            for _, v in pairs(tmp) do
                local tmp = lua_util.split_str(v, ':')
                local id = tonumber(tmp[1]) or tmp[1]
                local num = tonumber(tmp[2]) or tmp[2]
                tmp2[id] = num
            end
            return key2, tmp2
        elseif prefix == "_n" then
            --字典(列表)
            if value == "" then
                return key2, {}
            end
            
            local tmp = lua_util.split_str(value, ';')
            local tmp2 = {}
            for _, v in pairs(tmp) do
                local tmp = lua_util.split_str(v, ':')
                local id = tonumber(tmp[1]) or tmp[1]
                local num = lua_util.split_str(tmp[2], ',', tonumber)
                tmp2[id] = num
            end
            return key2, tmp2
        elseif prefix == "_o" then
            --字典(字典)
            if value == "" then
                return key2, {}
            end
            
            local tmp = lua_util.split_str(value['value'], ';')
            local tmp2 = {}
            for _, v in pairs(tmp) do
                local tmp = lua_util.split_str(v, ':')
                local id = tonumber(tmp[1]) or tmp[1]
                local num = lua_util.split_str(tmp[2], ',', tonumber)
                local tmp3 = {}
                for i = 1,#num,2 do
                    tmp3[num[i]] = num[i+1]
                end
                tmp2[id] = tmp3
            end
            return key2, tmp2
        elseif prefix == "_w" then
            local tmp   = {}
            local tmp1  = lua_util.split_str(value, ';')
            for i, v in pairs(tmp1) do
                local tmp2 = lua_util.split_str(v, ',', lua_util.TryToNumber)
                table.insert(tmp, tmp2)
            end
            return key2, tmp
        else
            return key, value
        end
    else
        return key, value
    end
end

--格式化一个table,该table只有一层关系
function lua_util.format_table2(t, layer)
    layer = layer + 1
    local v2 = {}
    for key, value in pairs(t) do

        local key2, value2 = lua_util.format_key_value2(key, value, layer)
        v2[key2] = value2

        --[[debug start
        if 'table' == type(value2) then
            print('         k',layer , key2, '       ', engine.cPickle(value2))
        else
            print('         k',layer , key2, '       ', value2)
        end
        --debug end
        --]]
    end

    return v2
end

--根据字段名后缀的含义修改从xml读取的数据类型
function lua_util.format_xml_table2(t)
    local t2 = lua_util.format_table2(t, 0)
    return t2
end

---一个通用的方法调用,适用于一个返回参数
--输入参数: entity   : rpc所在的实体
--         rpc_name  : rpc方法名,
--         base_mbstr: 返回的entity mb
--         callback  : 返回的回调方法
--         ...       : rpc对应的参数
function lua_util.generic_base_call(entity, rpc_name, base_mbstr, callback, ...)
    local p1 = entity[rpc_name](entity, ...)
    local mb = engine.unpickle_base_mailbox(base_mbstr)
    mb[callback](p1)
end

function lua_util.generic_base_call_client(entity, rpc_name, base_mbstr, action_id, log_fm, ...)
    local err_id,err_params = entity[rpc_name](entity, ...)
    local mb = engine.unpickle_base_mailbox(base_mbstr)
    if err_id then
        err_params = err_params or {}
        mb.client.on_action_resp(action_id, err_id,err_params)
        log_d(rpc_name, string.format("err_id=%d;"..log_fm, err_id, ...))
    end
end

function lua_util.generic_base_call_client_ne0(entity, rpc_name, base_mbstr, action_id, log_fm, ...)
    local err_id, err_params = entity[rpc_name](entity, ...)
    local mb = engine.unpickle_base_mailbox(base_mbstr)
    if err_id and err_id ~= 0 then
        err_params= err_params or {}
        mb.client.on_action_resp(action_id, err_id, err_params)
        log_d(rpc_name, string.format("err_id=%d;"..log_fm, err_id, ...))
    end
end

--base_mbstr改成mb
function lua_util.generic_base_mb_call_client(entity, rpc_name, mb, action_id, log_fm, ...)
    local err_id, err_params = entity[rpc_name](entity, ...)
    if err_id then
        err_params = err_params or {}
        mb.client.on_action_resp(action_id, err_id, err_params)
        log_d(rpc_name, string.format("err_id=%d;"..log_fm, err_id, ...))
    end
end

--base_mbstr改成mb
function lua_util.generic_base_mb_call_client_ne0(entity, rpc_name, mb, action_id, log_fm, ...)
    local err_id, err_params = entity[rpc_name](entity, ...)
    if err_id and err_id ~= 0 then
        err_params = err_params or {}
        mb.client.on_action_resp(action_id, err_id, err_params)
        log_d(rpc_name, string.format("err_id=%d;"..log_fm, err_id, ...))
    end
end


--只有一个返回参数的pcall调用封装
function lua_util.pcall(func, default_ret, ...)
    local ret, err_msg = pcall(func, ...)
    if ret then
        --调用成功,err_msg其实是返回参数的第一个参数
        return err_msg
    else
        --调用出错,记录异常,返回缺省值
        log_e("lua_util.pcall err", "%s", err_msg)
        return default_ret
    end
end

--对字符长包含的mysql特殊字符进行转义
function lua_util.mysql_escape_str(str)
    local sql_escape_char = "[\\\"]"
    local sql_replace_char = {["\\"] = "\\\\", ["\""] = "\\\""}
    local new_str = str:gsub(sql_escape_char, sql_replace_char)
    return new_str
end

---------------------------------------------
--生成sql更新语句
--@param table_name : 表名
--@param dataset: 需要更新的字段的值
--@param where_field: where条件的字段名；
--@param where_value: where条件的字段值；
function lua_util.gen_sql_update_str(tbl_name, dataset, where_field, where_value)
    if not lua_util.is_in_table_list(DB_TABLE_FIXED_FIELDS,where_field) then
        where_field = db_field_prefix .. where_field
    end
    local fields_format = {}
    local fields_value = {}
    local mysql_escape_str = lua_util.mysql_escape_str
    local table_insert = table.insert
    for field, value in pairs(dataset) do
        local v_type = type(value)
        local field_name_tmpl = string.format("%s_%s=", "sm",field)
        if v_type == "table" then
            table_insert(fields_format, field_name_tmpl .. "\"%s\"")
            table_insert(fields_value, mysql_escape_str(engine.cPickle(value)))
        elseif v_type == "string" then
            table_insert(fields_format, field_name_tmpl .. "\"%s\"")
            table_insert(fields_value, mysql_escape_str(value))
        elseif v_type == "number" then
            table_insert(fields_format, field_name_tmpl .. "%s")
            table_insert(fields_value, value)
        else
            log_e("lua_util.gen_sql_insert_str    1 field_value is nil!", "field=%s",field)
        end
    end
    local sql = string.format("UPDATE tbl_%s SET %s ", tbl_name, table.concat(fields_format, ","))
    sql = string.format(sql, unpack(fields_value))
    if where_field then
        if type(where_value) == "number" then
            sql = sql .. string.format(" WHERE %s=%s", where_field, where_value)
        else
            sql = sql .. string.format(" WHERE %s=\"%s\"", where_field, where_value)
        end
    end
    --log_i("lua_util.gen_sql_update_str", "sql=%s", sql)
    return sql
end

--生成一个日期的整数，默认以凌晨5:00为界。例如得到20170721当天。注意get_month_1_date_number，和get_monday_date_number
local _ONEDAY_SECONDS = 24*60*60
function lua_util.get_number_date(from_time, from_clock)
    local now_time=from_time or os.time()
    local _from_clock=from_clock or 5
    local now_time_table=(os.date("*t",now_time))
    if now_time_table==nil then
        return 0
    end
    if now_time_table.hour<_from_clock then
        local now_time_table_2=(os.date("*t",now_time-_ONEDAY_SECONDS))
        return  now_time_table_2.year*10000+now_time_table_2.month*100+now_time_table_2.day
    end
    return  now_time_table.year*10000+now_time_table.month*100+now_time_table.day
end

function lua_util.format_m(value)
    local tmp = lua_util.split_str(value, ';')
    local tmp2 = {}
    for _, v in pairs(tmp) do
        local tmp = lua_util.split_str(v, ':')
        local id = tonumber(tmp[1]) or tmp[1]
        local num = tonumber(tmp[2]) or tmp[2]
        tmp2[id] = num
    end
    return tmp2
end

function lua_util.format_n(value)
    local tmp = lua_util.split_str(value, ';')
    local tmp2 = {}
    for _, v in pairs(tmp) do
        local tmp = lua_util.split_str(v, ':')
        local id = tonumber(tmp[1]) or tmp[1]
        local num = lua_util.split_str(tmp[2], ',', tonumber)
        tmp2[id] = num
    end
    return tmp2
end

--把list形式的k1,k2,k3转换为{k1:1,k2:1,k3:1}，用于排除重复的k
function lua_util.list2dict(list1)
    local dict1 = {}
    for _, k in ipairs(list1) do
        dict1[k] = 1
    end
    return dict1
end

--把list形式的k1,v1,k2,v2转换为{k1:v1,k2:v2}，变成字典。
function lua_util.list2dict2(list1)
    local dict1 = {}
    for i=1,#list1,2 do
        local k = list1[i]
        local v = list1[i+1]
        if k and v then
            dict1[k] = v
        end
    end
    return dict1
end

--dict(list)形式转换成dict(dict)
--{1:1,2,3,4} = {1:{1:2,3:4}}
function lua_util.dict_list_to_dict_dict(dict_list)
    local tmp = {}
    for k,v in pairs(dict_list) do
        local tmp1 = lua_util.list2dict2(v)
        tmp[k] = tmp1
    end
    return tmp
end

function lua_util.distance(x1, z1, x2, z2)
    return math.sqrt((x1 - x2) * (x1 - x2) + (z1 - z2) * (z1 - z2))
end

function lua_util.mgr_get_entity(eid)
    if eid and eid > 0 then
        return engine.getEntity(eid)
    end
end

--获取某个globalbase的entity(如果在同一个baseapp上的话)
function lua_util.get_globalbase_entity(name)
    local mgr = globalBases[name]
    if mgr then
        local en = engine.getEntity(mgr[3])     --public_config.MAILBOX_KEY_ENTITY_ID,3这个值肯定不会变的
        return en
    end
end


--计算utf编码字符串的长度
local _utf_arr = {0, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc}
local _utf_arr_len = #_utf_arr
function lua_util.utfstrlen(str)
    local len = #str
    local left = len
    local cnt = 0
    while left > 0 do
        local tmp = string.byte(str, -left)
        local i = _utf_arr_len
        while _utf_arr[i] do
            if tmp >=_utf_arr[i] then
                left = left-i
                break
            end
            i=i-1
        end
        cnt=cnt+1
    end
    return cnt
end

--计算属性很多用到，抽出来
function lua_util._add_attri(t, k, v2)
    local v1 = t[k]
    if v1 then
        t[k] = v1 + v2
    else
        t[k] = v2
    end
end

function lua_util._union_table(t1, t2)
    if t1 and t2 then
        for k,v in pairs(t2) do
            lua_util._add_attri(t1, k, v)
        end
    end
end

function lua_util._get_value_by_2key(t, k1, k2)
    local t1 = t[k1]
    if t1 then
        return t1[k2]
    end
end

function lua_util._calc_value_sum(t)
    local sum = 0
    if t then
        for k,v in pairs(t) do
            sum = sum + v
        end
    end

    return sum
end

--职业组,100->1,201->2
function lua_util.get_vocation_group(vocation)
    return math.floor(vocation/100)
end

function lua_util.get_vocation_gender(vocation)
    local vocation_group = lua_util.get_vocation_group(vocation)
    local vocation_gender
    --1,3是男 。2,4是女
    if vocation_group == 1 or vocation_group == 3 then
        vocation_gender = 1
    else
        vocation_gender = 0
    end
    return vocation_gender
end

function lua_util.is_diff_vocation_gender(vocation1,vocation2)
    local vocation_gender1 = lua_util.get_vocation_gender(vocation1)
    local vocation_gender2 = lua_util.get_vocation_gender(vocation2)
    if vocation_gender1 ~= vocation_gender2 then
        return true
    end
    return false
end

--职业第几转,100->0,203->3
function lua_util.get_vocation_change(vocation)
    return vocation % 100
end

function lua_util.get_dbid_from_cross_uuid(cross_uuid)
    local tmp = lua_util.split_str(cross_uuid, '_', tonumber)
    if tmp[2] then
        return tmp[2]
    end
end

function lua_util.get_attris_diff(old_attris,new_attris)
    --注意,old_attris会被修改
    
    local diff = {}
    
    --加和改变
    for attri_id,attri_value in pairs(new_attris) do
        if not old_attris[attri_id] then
            --新的属性
            diff[attri_id] = attri_value
        else
            --旧得属性
            if old_attris[attri_id] ~= new_attris[attri_id] then
                diff[attri_id] = attri_value
            end
        end
        old_attris[attri_id] = nil --那么剩下的还有就是要删除的
    end
    --减
    for attri_id,_ in pairs(old_attris) do
        diff[attri_id] = 0
    end
    
    return diff
end


function lua_util.json2table(message)

    local jtable = {}

    local s, e = pcall(function() jtable = json:decode(message)  end)

    if s then
        return jtable
    end

end

function lua_util.table2json(tbl)

    local jstr = ''

    local s, e = pcall(function() jstr = json:encode(tbl)  end)

    if s then
        return jstr
    end

end

function lua_util.get_aofei_log_time()
    local now = os.time()
    local time_table = os.date("*t", now)
    local week = os.date("%W", now)
    local year = time_table.year
    local month = time_table.month
    local day = time_table.day
    local hour = time_table.hour
    local minute = time_table.min

    return {
        log_ymd=string.format("%d%02d%02d", year, month, day),
        log_ym=string.format("%d%02d", year, month),
        year=year,
        month=month,
        day=day,
        hour=hour,
        minute=minute,
        week=week,
    }
end

return lua_util


