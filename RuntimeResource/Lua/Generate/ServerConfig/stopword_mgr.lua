local engine = require "engine"
local lua_util = require "lua_util"
local putil = require "public_util"

local ipairs = ipairs
local io = io

local log_d = putil.log_d

local stopword_mgr = {}
stopword_mgr.__index = stopword_mgr

function stopword_mgr:init_data()
    local addStopWord = engine.add_stopword
    --1保留字符集
--    addStopWord(1, " ~!@#$%^&*()_+-=[]{};'\\:\"|,./<>?　")
    addStopWord(1, "~ !@#$%^&*()_+-=[]{};'\\:\"|,./<>?")
    --2敏感词汇
    local fn = G_LUA_ROOTPATH .. "/data/filter_words/stopword_sub.txt"
    local f = io.open(fn, 'r')

--    if f then
    local s = ''
    for s2 in f:lines() do
        --其实这里只会循环一次
        s = s .. s2
    end

    local words = lua_util.split_str(s, ',')
    for _, _word in ipairs(words) do
        addStopWord(2, _word)
    end

    --3正则表达式
    local fn3 = G_LUA_ROOTPATH .. "/data/filter_words/stopword_re.txt"
    local f3 = io.open(fn3, 'r')
    for s3 in f3:lines() do
        addStopWord(3, s3)
    end
--    end
end

function stopword_mgr:is_stopword(word)
    return engine.is_stopword(word)
end

return stopword_mgr