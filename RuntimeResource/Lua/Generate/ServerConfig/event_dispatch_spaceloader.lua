--该模块用来管理事件的订阅和触发.本进程。

local table = table
local pairs = pairs

local table_insert = table.insert

local event_dispatch_spaceloader = {}
event_dispatch_spaceloader.event_handlers = {}  --保存所有事件订阅者信息 .本进程。


--所有订阅事件的对象的cb_name对应的回调函数，subscriber:callback(evt_id, evt_param, trigger_obj)
--三个参数分别表示: 事件编号, 事件参数, 事件触发者
function event_dispatch_spaceloader:insert_event(event_id, subscriber, cb_name)
    local cbs = self.event_handlers[event_id]
    if not cbs then
        self.event_handlers[event_id] = {}
        cbs = self.event_handlers[event_id]
    end
    table_insert(cbs, {subscriber, cb_name})
end


--取消订阅某个事件
function event_dispatch_spaceloader:remove_event(event_id, subscriber, cb_name)
    local cbs = self.event_handlers[event_id]
    if cbs then
        for index, record in pairs(cbs) do
            if record[1] == subscriber and record[2] == cb_name then
                cbs[index] = nil
                break
            end
        end
    end
end


--触发某个事件
function event_dispatch_spaceloader:trigger_event(event_id, event_param, trigger)
    local cbs = self.event_handlers[event_id]
    if cbs then
        for _, record in pairs(cbs) do
            local func_cb = record[1][record[2]]
            if func_cb then 
                func_cb(record[1], event_id, event_param, trigger)
            end
        end
    end
end

--获取某个事件已监听的回调函数
function event_dispatch_spaceloader:get_event_callbacks(event_id)
    return self.event_handlers[event_id]
end


-----------------
return event_dispatch_spaceloader

