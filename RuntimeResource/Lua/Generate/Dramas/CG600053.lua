local CG600053 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"PreloadResources"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"400"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"ShowPlayerPet"	,"0"
	}
	,{
		"HideOtherPlayer"	,"1"
	}
	,{
		"HideSelfPlayer"	,"1"
	}
	,{
		"ShowPlayerPet"	,"0"
	}
	,{
		"Judge"	,"IsVocGroup:1"	,"Goto:__Mark3"	,"null"
	}
	,{
		"CreateEntity"	,"NPC"	,"10001"	,"149"	,"124.05,40.1,125.5"	,"180"	,"1"
	}
	,{
		"KeepTimeScale"	,"0.3"	,"2000"
	}
	,{
		"RotateCameraForLocal"	,"10001"	,"bone_wing"	,"0,180,0"	,"0"	,"5"	,"0"
	}
	,{
		"PlaySkill"	,"10001"	,"130"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"RotateCameraForLocal"	,"10001"	,"bone_wing"	,"70,0,0"	,"2000"	,"8"	,"2000"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"RotateCameraForLocal"	,"10001"	,"bone_wing"	,"0,215,0"	,"800"	,"5"	,"800"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"ShowIntroduce"	,"4623"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"300"
	}
	,{
		"Sleep"	,"300"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"KeepTimeScale"	,"1"	,"100"
	}
	,{
		"HideIntroduce"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"ShowPlayerPet"	,"1"
	}
	,{
		"HideOtherPlayer"	,"0"
	}
	,{
		"HideSelfPlayer"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__Mark3"
	}
	,{
		"CreateEntity"	,"NPC"	,"10002"	,"148"	,"124.05,40.1,125.5"	,"180"	,"1"
	}
	,{
		"RotateCameraForLocal"	,"10002"	,"bone_wing"	,"0,180,0"	,"2"	,"5"	,"3"
	}
	,{
		"PlaySkill"	,"10002"	,"87"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"Sleep"	,"1400"
	}
	,{
		"KeepTimeScale"	,"0.2"	,"4000"
	}
	,{
		"ShowIntroduce"	,"4623"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"300"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"KeepTimeScale"	,"1"	,"120"
	}
	,{
		"HideIntroduce"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"ShowPlayerPet"	,"1"
	}
	,{
		"HideOtherPlayer"	,"0"
	}
	,{
		"HideSelfPlayer"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"500"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
}
return CG600053
