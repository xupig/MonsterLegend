local CG200046 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"SetShowPlayerModel"	,"2"
	}
	,{
		"MoveCamera"	,"5.36,180,0"	,"0"	,"-1921.9,3.06,-1899.5"	,"0"
	}
	,{
		"CreateEntity"	,"Avatar"	,"1"	,"201"	,"-1923.41,0.15,-1909.89"	,"90"	,"1"
	}
	,{
		"TeleportEntity"	,"ShowPlayer"	,"-1920.6,0.15,-1909.89"
	}
	,{
		"RotateEntity"	,"ShowPlayer"	,"0,270,0"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"72"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"75"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"83"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"1"	,"123"
	}
	,{
		"Sleep"	,"1100"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"75"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"1"	,"127"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"75"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"PlaySkill"	,"1"	,"121"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"82"
	}
	,{
		"Sleep"	,"1200"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"73"
	}
	,{
		"Sleep"	,"1200"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"PlaySkill"	,"1"	,"121"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"82"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"1"	,"122"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"KeepTimeScale"	,"0.3"	,"1500"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"92"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"OnCGTriggerEventType"	,"121"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleAthletics.AthleticsPanel"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"DestroyEntities"
	}
	,{
		"SetShowPlayerModel"	,"3"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
}
return CG200046
