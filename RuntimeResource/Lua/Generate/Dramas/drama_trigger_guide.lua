local drama_trigger_guide = {
	{
		"0"	,"0"	,"0"	,"0"	,"0"
	}
	,{
		"1"	,"ENTER_MAP"	,"drama_guide"	,"__ExAction"
	}
	,{
		"101"	,"FINISH_QUEST"	,"38"	,"drama_guide"	,"__QuickSell"
	}
	,{
		"102"	,"GUIDE_FUNCTION_OPEN"	,"409"	,"drama_guide"	,"__JoinGuild"
	}
	,{
		"103"	,"CG_END"	,"drama_guide__StartTrip"	,"drama_guide"	,"__FirstTask"
	}
	,{
		"104"	,"GUIDE_FUNCTION_OPEN"	,"201"	,"drama_guide"	,"__Aion"
	}
	,{
		"105"	,"ENTER_MAP_FROM_LAST_MAP"	,"40001"	,"drama_guide"	,"__Rune"
	}
	,{
		"106"	,"GUIDE_FUNCTION_OPEN"	,"10"	,"drama_guide"	,"__EquipIntensify"
	}
	,{
		"107"	,"FINISH_QUEST"	,"drama_guide"	,"__EquipSynthesis"
	}
	,{
		"108"	,"GUIDE_FUNCTION_OPEN"	,"401"	,"drama_guide"	,"__WorldBoss"
	}
	,{
		"109"	,"GUIDE_FUNCTION_OPEN"	,"202"	,"drama_guide"	,"__ExpZone"
	}
	,{
		"110"	,"GUIDE_FUNCTION_OPEN"	,"13"	,"drama_guide"	,"__GemInlay"
	}
	,{
		"111"	,"CG_END"	,"CG600052__begin1"	,"drama_guide"	,"__Ride"
	}
	,{
		"112"	,"FINISH_QUEST"	,"35"	,"drama_guide"	,"__Ring"
	}
	,{
		"113"	,"GUIDE_FUNCTION_OPEN"	,"406"	,"drama_guide"	,"__DailyTask"
	}
	,{
		"114"	,"LEVEL_UP"	,"95"	,"drama_guide"	,"__PetEat"
	}
	,{
		"115"	,"ON_OPEN_VIEW"	,"Modules.ModuleTrigger.TriggerPanel"	,"drama_guide"	,"__StartTrip"
	}
	,{
		"116"	,"ON_FUNCTION_OPEN"	,"351"	,"drama_guide"	,"__EquipTreasure"
	}
	,{
		"117"	,"GUIDE_FUNCTION_OPEN"	,"402"	,"drama_guide"	,"__PersonalBoss"
	}
	,{
		"118"	,"GUIDE_FUNCTION_OPEN"	,"203"	,"drama_guide"	,"__GuardTower"
	}
	,{
		"119"	,"GUIDE_FUNCTION_OPEN"	,"204"	,"drama_guide"	,"__GodsTreasury"
	}
	,{
		"120"	,"GUIDE_FUNCTION_OPEN"	,"206"	,"drama_guide"	,"__DragonSoul"
	}
	,{
		"121"	,"ON_CLOSE_VIEW"	,"Modules.ModuleVIP.VIPPopPanel"	,"drama_guide"	,"__FlyShoes"
	}
	,{
		"122"	,"GUIDE_FUNCTION_OPEN"	,"50"	,"drama_guide"	,"__Marry"
	}
	,{
		"123"	,"ON_OPEN_VIEW"	,"Modules.ModuleVIP.VIPPopPanel"	,"drama_guide"	,"__VipExperience"
	}
}
return drama_trigger_guide
