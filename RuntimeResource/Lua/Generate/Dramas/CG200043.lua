local CG200043 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"SetShowPlayerModel"	,"2"
	}
	,{
		"MoveCamera"	,"5.36,180,0"	,"0"	,"-1921.9,3.06,-1899.5"	,"0"
	}
	,{
		"CreateEntity"	,"Avatar"	,"1"	,"101"	,"-1923.1,0.15,-1909.89"	,"90"	,"1"
	}
	,{
		"TeleportEntity"	,"ShowPlayer"	,"-1920.6,0.15,-1909.89"
	}
	,{
		"RotateEntity"	,"ShowPlayer"	,"0,270,0"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"1"	,"72"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"1"	,"83"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"75"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"123"
	}
	,{
		"Sleep"	,"1100"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"127"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"121"
	}
	,{
		"PlaySkill"	,"1"	,"82"
	}
	,{
		"Sleep"	,"1200"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"PlaySkill"	,"1"	,"73"
	}
	,{
		"Sleep"	,"1200"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"121"
	}
	,{
		"PlaySkill"	,"1"	,"82"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"122"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"KeepTimeScale"	,"0.3"	,"1500"
	}
	,{
		"PlayAction"	,"1"	,"92"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"OnCGTriggerEventType"	,"210"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleAthletics.AthleticsPanel"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"DestroyEntities"
	}
	,{
		"SetShowPlayerModel"	,"3"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
}
return CG200043
