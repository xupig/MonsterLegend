local CG200042 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"SetShowPlayerModel"	,"2"
	}
	,{
		"MoveCamera"	,"5.36,180,0"	,"0"	,"-1921.9,3.06,-1899.5"	,"0"
	}
	,{
		"CreateEntity"	,"Avatar"	,"1"	,"201"	,"-1923.71,0.15,-1909.89"	,"90"	,"1"
	}
	,{
		"TeleportEntity"	,"ShowPlayer"	,"-1919.99,0.15,-1909.89"
	}
	,{
		"RotateEntity"	,"ShowPlayer"	,"0,270,0"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"1"	,"110"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"105"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"75"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"110"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"PlaySkill"	,"1"	,"107"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"75"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"107"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"PlaySkill"	,"1"	,"107"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"PlaySkill"	,"1"	,"107"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"105"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"KeepTimeScale"	,"0.4"	,"2500"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"92"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"1800"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"OnCGTriggerEventType"	,"221"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleAthletics.AthleticsPanel"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"DestroyEntities"
	}
	,{
		"SetShowPlayerModel"	,"3"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
}
return CG200042
