local CG60002 = {
	{
		"__begin1"
	}
	,{
		"PreloadResources"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"RotateEntity"	,"Player"	,"0,180,0"
	}
	,{
		"TeleportEntity"	,"Player"	,"65.76,8.75,115"
	}
	,{
		"MoveCamera"	,"0,0,0"	,"0"	,"65.65,10.16,113"	,"0"
	}
	,{
		"Judge"	,"IsVocGroup:1"	,"Goto:__Mark3"	,"null"
	}
	,{
		"PlaySkill"	,"Player"	,"111"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"KeepTimeScale"	,"0.2"	,"1000"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"MoveCamera"	,"0,0,0"	,"0"	,"65.65,10.16,109"	,"300"
	}
	,{
		"ShowIntroduce"	,"73129"
	}
	,{
		"Sleep"	,"300"
	}
	,{
		"KeepTimeScale"	,"0.1"	,"1500"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"HideIntroduce"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"0"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"End"
	}
	,{
		"__Mark3"
	}
	,{
		"PlaySkill"	,"Player"	,"83"
	}
	,{
		"MoveCamera"	,"0,-16,0"	,"1200"	,"66.2,10.1,108"	,"1200"
	}
	,{
		"Sleep"	,"1200"
	}
	,{
		"KeepTimeScale"	,"0.2"	,"1500"
	}
	,{
		"ShowIntroduce"	,"73122"
	}
	,{
		"MoveCamera"	,"6,344,0"	,"1500"	,"66.2,10.1,110"	,"1500"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"KeepTimeScale"	,"0.1"	,"1500"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"HideIntroduce"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"0"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"End"
	}
}
return CG60002
