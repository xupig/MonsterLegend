local CG60003 = {
	{
		"__begin1"
	}
	,{
		"PreloadResources"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"TeleportEntity"	,"Player"	,"65.76,8.75,115"
	}
	,{
		"RotateEntity"	,"Player"	,"0,180,0"
	}
	,{
		"Judge"	,"IsVocGroup:1"	,"Goto:__Mark3"	,"null"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"MoveCamera"	,"0,0,0"	,"0"	,"65.65,10.541,111.71"	,"0"
	}
	,{
		"PlayAction"	,"Player"	,"7"
	}
	,{
		"MoveCamera"	,"-13,0,0"	,"200"	,"65.65,10.541,111.71"	,"0"
	}
	,{
		"PlayVisualFX"	,"Characters/Avatar/102/fx/fx_skill_shandianbaopo_1_L.prefab"	,"65.76,8.75,115"	,"2500"	,"0,0,0"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"MoveCamera"	,"0,0,0"	,"0"	,"65.65,10.16,109"	,"300"
	}
	,{
		"ShowIntroduce"	,"73130"
	}
	,{
		"Sleep"	,"300"
	}
	,{
		"KeepTimeScale"	,"0.1"	,"800"
	}
	,{
		"Sleep"	,"80"
	}
	,{
		"PlayVisualFX"	,"Characters/Avatar/102/fx/fx_skill_shandianbaopo_2_L.prefab"	,"65.76,8.75,112.89"	,"9000"	,"0,0,0"
	}
	,{
		"Sleep"	,"900"
	}
	,{
		"HideIntroduce"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"0"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"DestroyEntities"
	}
	,{
		"KeepTimeScale"	,"1"	,"500"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"End"
	}
	,{
		"__Mark3"
	}
	,{
		"MoveCamera"	,"0,0,0"	,"0"	,"65.65,10.16,113"	,"0"
	}
	,{
		"PlaySkill"	,"Player"	,"81"
	}
	,{
		"KeepTimeScale"	,"0.2"	,"1500"
	}
	,{
		"Sleep"	,"150"
	}
	,{
		"MoveCamera"	,"10,0,0"	,"1200"	,"66.1,13.06,102.69"	,"2000"
	}
	,{
		"KeepTimeScale"	,"0.1"	,"8000"
	}
	,{
		"PlayVisualFX"	,"Characters/Avatar/101/fx/fx_skill_huitianmiedi_ground.prefab"	,"65.76,8.75,115"	,"9000"	,"0,0,0"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"MoveCamera"	,"5,-5,0"	,"1500"	,"66.2,10.1,110"	,"1500"
	}
	,{
		"Sleep"	,"100"
	}
	,{
		"ShowIntroduce"	,"73123"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"HideIntroduce"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"0"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"KeepTimeScale"	,"1"	,"100"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"End"
	}
}
return CG60003
