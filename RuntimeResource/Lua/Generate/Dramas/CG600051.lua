local CG600051 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"PreloadResources"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"400"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"ShowPlayerPet"	,"0"
	}
	,{
		"HideOtherPlayer"	,"1"
	}
	,{
		"HideSelfPlayer"	,"1"
	}
	,{
		"CreateEntity"	,"NPC"	,"10001"	,"166"	,"198.72,52.72,187.4"	,"180"	,"1"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/chain_waiting.prefab"	,"198.72,52.72,187.2"	,"4500"	,"0,180,0"
	}
	,{
		"MoveCamera"	,"21,0,0"	,"0"	,"198.46,58.43,175.54"	,"0"
	}
	,{
		"Sleep"	,"300"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"MoveCamera"	,"10,30,0"	,"2000"	,"196.05,54,183.5"	,"2500"
	}
	,{
		"PlaySound"	,"1020"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/chain_explode.prefab"	,"198.72,52.72,187.2"	,"2000"	,"0,180,0"
	}
	,{
		"PlaySound"	,"1022"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ShowIntroduce"	,"27001"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"400"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"HideIntroduce"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"ShowPlayerPet"	,"1"
	}
	,{
		"HideOtherPlayer"	,"0"
	}
	,{
		"HideSelfPlayer"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"End"
	}
}
return CG600051
