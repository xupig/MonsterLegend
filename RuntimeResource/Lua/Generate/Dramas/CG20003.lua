local CG20003 = {
	{
		"__begin1"
	}
	,{
		"PreloadResources"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"MoveCamera"	,"45,0,0"	,"0"	,"152,76,47"	,"0"
	}
	,{
		"Sleep"	,"100"
	}
	,{
		"MoveCamera"	,"45,73,0"	,"1500"	,"105,76,103"	,"1500"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShakeCamera"	,"1"	,"500"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"MoveCamera"	,"0,136,0"	,"0"	,"125.78,45.9,118.5"	,"0"
	}
	,{
		"Sleep"	,"100"
	}
	,{
		"MoveCamera"	,"0,142.4,2.4"	,"0"	,"144.48,47.1,100"	,"1600"
	}
	,{
		"Sleep"	,"300"
	}
	,{
		"CreateEntity"	,"Dummy"	,"1"	,"6"	,"146,45,98.14"	,"300"	,"1"
	}
	,{
		"Sleep"	,"1600"
	}
	,{
		"ShowBossIntroduce"	,"6"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"HideBossIntroduce"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"End"
	}
}
return CG20003
