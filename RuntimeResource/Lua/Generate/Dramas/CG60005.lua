local CG60005 = {
	{
		"__begin1"
	}
	,{
		"PreloadResources"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"1"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"TeleportEntity"	,"Player"	,"65.76,8.75,115"
	}
	,{
		"RotateEntity"	,"Player"	,"0,180,0"
	}
	,{
		"MoveCamera"	,"10,0,0"	,"0"	,"65.65,11.89,106"	,"0"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"PlaySkill"	,"Player"	,"78"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"PlaySkill"	,"Player"	,"79"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"PlaySkill"	,"Player"	,"73"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"PlaySkill"	,"Player"	,"82"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"PlaySkill"	,"Player"	,"74"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"PlaySkill"	,"Player"	,"83"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"PlaySkill"	,"Player"	,"81"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"PlaySkill"	,"Player"	,"80"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"0"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"End"
	}
}
return CG60005
