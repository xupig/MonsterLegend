local CG60001 = {
	{
		"__begin1"
	}
	,{
		"PreloadResources"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"CreateEntity"	,"NPC"	,"10001"	,"145"	,"64.82,8.75,76.14"	,"0"	,"1"
	}
	,{
		"CreateEntity"	,"NPC"	,"10002"	,"146"	,"66.72,8.75,76.14"	,"0"	,"1"
	}
	,{
		"CreateEntity"	,"NPC"	,"10003"	,"155"	,"65.81,10.17,35.94"	,"0"	,"1"
	}
	,{
		"Sleep"	,"100"
	}
	,{
		"SetAttr"	,"10001"	,"isStatic"	,"0"
	}
	,{
		"SetAttr"	,"10002"	,"isStatic"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"0"	,"0"
	}
	,{
		"PlayMusic"	,"1000"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,85"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,85"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,85"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,85"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,85"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,85"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,70"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,70"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,70"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,70"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,70"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,70"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,55"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,55"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,55"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,55"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,55"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,55"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,40"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,40"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,40"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,40"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,40"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,40"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,26"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,26"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,26"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"50,20,26"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"65,20,26"	,"80000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/ui/fx_ui_30041.prefab"	,"80,20,26"	,"80000"	,"0,0,0"
	}
	,{
		"MoveCamera"	,"40,180,0"	,"0"	,"66.4,20.14,78.94"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"2000"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlayAction"	,"10001"	,"125"
	}
	,{
		"PlayAction"	,"10002"	,"125"
	}
	,{
		"MoveTo"	,"10001"	,"64.82,8.75,68.02"	,"1"	,""
	}
	,{
		"MoveTo"	,"10002"	,"66.72,8.75,68.02"	,"1"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"MoveTo"	,"10001"	,"64.82,8.75,63.8"	,"1"
	}
	,{
		"MoveTo"	,"10002"	,"66.72,8.75,63.8"	,"1"
	}
	,{
		"Sleep"	,"4000"
	}
	,{
		"RotateCamera"	,"10002"	,"0"	,"0,270,0"	,"0"	,"6"	,"0"
	}
	,{
		"MoveTo"	,"10001"	,"64.82,8.75,58.11"	,"1"
	}
	,{
		"MoveTo"	,"10002"	,"66.72,8.75,58.11"	,"1"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"MoveTo"	,"10001"	,"64.82,8.75,49.66"	,"1"
	}
	,{
		"MoveTo"	,"10002"	,"66.72,8.75,49.66"	,"1"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"RotateCamera"	,"10001"	,"0"	,"0,220,0"	,"0"	,"12"	,"0"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"MoveTo"	,"10001"	,"64.82,8.75,46.74"	,"1"
	}
	,{
		"MoveTo"	,"10002"	,"66.72,8.75,46.74"	,"1"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"MoveCamera"	,"30,0,1"	,"0"	,"65.8,12.5,36.7"	,"0"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"MoveTo"	,"10001"	,"64.82,10.17,39.5"	,"1"
	}
	,{
		"MoveTo"	,"10002"	,"66.72,10.17,39.5"	,"1"
	}
	,{
		"Sleep"	,"8000"
	}
	,{
		"PlayAction"	,"10001"	,"127"
	}
	,{
		"PlayAction"	,"10002"	,"127"
	}
	,{
		"RotateCamera"	,"10003"	,"0"	,"25,145,0"	,"0"	,"20"	,"0"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"RotateCamera"	,"10003"	,"0"	,"25,215,0"	,"28000"	,"20"	,"28000"
	}
	,{
		"Sleep"	,"4000"
	}
	,{
		"ShowSubtitles"	,"59147"	,"3000"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ShowSubtitles"	,"59148"	,"3000"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ShowSubtitles"	,"59149"	,"3000"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ShowSubtitles"	,"59150"	,"3000"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"ShowSubtitles"	,"59151"	,"3000"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ShowSubtitles"	,"59156"	,"3000"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ShowSubtitles"	,"59157"	,"3000"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ShowSubtitles"	,"59158"	,"3000"
	}
	,{
		"Sleep"	,"6000"
	}
	,{
		"MoveCamera"	,"0,180,0"	,"0"	,"65.75,11.19,49.74"	,"0"
	}
	,{
		"RotateEntity"	,"10001"	,"0,90,0"
	}
	,{
		"RotateEntity"	,"10002"	,"0,270,0"
	}
	,{
		"ShowSubtitles"	,"59159"	,"6000"
	}
	,{
		"MoveCamera"	,"0,180,0"	,"6000"	,"65.75,11.19,44.05"	,"6000"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_yanhua.prefab"	,"65.81,10.17,33.94"	,"5000"	,"0,0,0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"3000"
	}
	,{
		"PlayAction"	,"10001"	,"126"
	}
	,{
		"MoveTo"	,"10001"	,"65.82,10.17,39.5"	,"1"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"0"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"End"
	}
}
return CG60001
