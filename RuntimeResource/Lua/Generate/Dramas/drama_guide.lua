local drama_guide = {
	{
		"__ExAction"
	}
	,{
		"Judge"	,"PlayerIsLevel:1"	,"null"	,"End"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Dialogue,77100,0,190,0"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_dialogue_click_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Dialogue,77101,0,190,0"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_dialogue_click_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Dialogue,77102,0,190,0"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_dialogue_click_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"NoMask,LayerUIMain,/Panel_PlayerState/Button_Head,77102,51,0,Right,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_player_state_head_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"End"
	}
	,{
		"__QuickSell"
	}
	,{
		"StopAutoTask"
	}
	,{
		"ShowPanel"	,"Strengthen"	,"0"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__QuickSell2"	,"Goto:__QuickSell1"
	}
	,{
		"__QuickSell1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__QuickSell2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__QuickSell3"	,"Goto:__QuickSell4"
	}
	,{
		"__QuickSell3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__QuickSell4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_ChatSmall/Button_Bag,77107,80,50,Right,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77106,540,500,3414,-10000,-10000,0,0,-260,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_chat_small_bag_button"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleRole.ChildView.RoleBagTabView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Role/Container_Bag/Button_OneClickSell,77109,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_bag_one_click_sell_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Role/Container_Bag/Container_Sell/Button_Sell,77110,-10,50,Left,0,0,0,0,1.2,"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_role_bag_sell_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Role/Container_WinBg/Container_win1/Button_CloseMirror,77111,20,40,Left,0,0,0,0,1.2,1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_role_panel_close_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__JoinGuild"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__JoinGuild2"	,"Goto:__JoinGuild1"
	}
	,{
		"__JoinGuild1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__JoinGuild2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__JoinGuild3"	,"Goto:__JoinGuild4"
	}
	,{
		"__JoinGuild3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__JoinGuild4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_PlayerState/Button_Head,77113,90,50,Right,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77112,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_player_state_head_button"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleSubMainUI.SubMainUIPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_SubMainUI/Container_SystemRight/Container_System1/Image_BG,77115,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77114,560,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_sub_main_ui_button_108"	,"10000"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__Marry"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__Marry2"	,"Goto:__Marry1"
	}
	,{
		"__Marry1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__Marry2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__Marry3"	,"Goto:__Marry4"
	}
	,{
		"__Marry3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__Marry4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_PlayerState/Button_Head,77221,90,50,Right,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77222,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_player_state_head_button"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleSubMainUI.SubMainUIPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_SubMainUI/Container_SystemRight/Container_System2/Image_BG,77223,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_sub_main_ui_button_109"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleMarriage.MarriagePanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Marriage/Container_Info/Container_Bottom/Button_GetMarry,77224,-15,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_marry_tomarry_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleMarriage.GetMarryPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_GetMarry/Container_Goddess/Button_GetMarry,77225,-10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_get_marry_goddess_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleMarriage.ChildView.IWantMarryContainerView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_GetMarry/Container_IWantMarriage/Button_ShowLove,77226,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_show_love_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleMarriage.ChildView.ShowLoveContainerView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_GetMarry/Container_Win/Button_CloseMirror,77228,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77227,430,500,3414,-10000,-10000,0,0,-250,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_show_love_close_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__Aion"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__AionMark2"	,"Goto:__AionMark1"
	}
	,{
		"__AionMark1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__AionMark2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__AionMark3"	,"Goto:__AionMark4"
	}
	,{
		"__AionMark3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__AionMark4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_Row3/Container_Stable/Button_2,77119,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_2"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleInstanceHall.InstanceHallPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_InstanceHall/Container_AionTower/Container_Right/Container_LayerInfo/Button_Challenge,77121,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77118,430,500,3414,-10000,-10000,0,0,-250,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_aion_instance_battle_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__Rune"
	}
	,{
		"StopAutoTask"
	}
	,{
		"Judge"	,"TriggerCount:1003,1"	,"null"	,"End"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__RuneMark1"	,"Goto:__RuneMark2"
	}
	,{
		"__RuneMark1"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__RuneMark2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__RuneMark3"	,"Goto:__RuneMark4"
	}
	,{
		"__RuneMark3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__RuneMark4"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__RuneMark5"	,"Goto:__RuneMark6"
	}
	,{
		"__RuneMark5"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__RuneMark6"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_PlayerState/Button_Head,77123,90,50,Right,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77122,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"ExAction"	,"SetTriggerCount"	,"1003"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_player_state_head_button"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleSubMainUI.SubMainUIPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_SubMainUI/Container_SystemLeft/Container_System6/Image_BG,77125,90,50,Right,0,0,0,0,1.2,1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77124,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_sub_main_ui_button_106"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleRune.RunePanel"
	}
	,{
		"Judge"	,"HasButtonExistAndShow:LayerUIPanel/Panel_Rune/Container_Insert/Container_Hole0/Container_Empty"	,"Goto:__RuneMark7"	,"Goto:__RuneMark8"
	}
	,{
		"__RuneMark7"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Rune/Container_Insert/Container_Hole0/Container_Empty,77127,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77126,1000,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_rune_hole_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleRune.ChildView.RuneUpReView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Rune/Container_Insert/Container_UpRe/Container_Replace/ScrollViewList_Re/Mask/Content/item_0,77129,-140,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77128,1000,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_rune_replace_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Rune/Container_Insert/Container_UpRe/Container_Up/Button_Up,77132,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77133,350,500,3414,-10000,-10000,0,0,-260,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_rune_up_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Rune/Container_WinBg/Container_win1/ToggleGroup_First_Mirror/Toggle2,77134,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77135,360,500,3414,-10000,-10000,0,0,-260,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_rune_resolve_view_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleRune.ChildView.RuneResolveView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Rune/Container_Resolve/Button_OneResolve,77137,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77136,350,500,3414,-10000,-10000,0,0,-260,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_rune_resolve_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Rune/Container_WinBg/Container_win1/Button_CloseMirror,77139,20,40,Left,0,0,0,0,1.2,1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77138,350,500,3414,-10000,-10000,0,0,-260,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_rune_close_button"
	}
	,{
		"__RuneMark8"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__FirstTask"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__FirstTask1"	,"Goto:__FirstTask2"
	}
	,{
		"__FirstTask1"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__FirstTask2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_LeftMainUI/Container_TaskAndTeam/Container_Task/ScrollViewList/Mask/Content/item_0,77140,190,50,Right,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_task_ui_button"	,"10000"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"End"
	}
	,{
		"__Ride"
	}
	,{
		"StopAutoTask"
	}
	,{
		"Judge"	,"IsPanelActive:FunctionOpenTips"	,"Goto:__Ride1"	,"Goto:__Ride2"
	}
	,{
		"__Ride1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"__Ride2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__Ride3"	,"Goto:__Ride4"
	}
	,{
		"__Ride3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__Ride4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_Stick/Button_OffRide,77142,80,50,Right,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77141,700,500,3414,-10000,-10000,0,0,-130,-500,-200,1.2,Right"
	}
	,{
		"PlayerDismount"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_on_ride_button"	,"10000"	,"1"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__EquipIntensify"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__EquipIntensify2"	,"Goto:__EquipIntensify1"
	}
	,{
		"__EquipIntensify1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__EquipIntensify2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__EquipIntensify3"	,"Goto:__EquipIntensify4"
	}
	,{
		"__EquipIntensify3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__EquipIntensify4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_PlayerState/Button_Head,77155,90,50,Right,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77156,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_player_state_head_button"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleSubMainUI.SubMainUIPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_SubMainUI/Container_SystemLeft/Container_System3/Image_BG,77157,90,50,Right,0,0,0,0,1.2,1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77158,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_sub_main_ui_button_103"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleEquip.ChildView.EquipStrengthView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Equip/Container_Strength/Button_AutoStrength,77159,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77160,1000,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_equip_auto_strength_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__EquipSynthesis"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__EquipSynthesis1"	,"Goto:__EquipSynthesis2"
	}
	,{
		"__EquipSynthesis1"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__EquipSynthesis2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_PlayerState/Button_Head,77123,50,0,Right,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77122,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_player_state_head_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleSubMainUI.SubMainUIPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_SubMainUI/Container_SystemLeft/Container_System6/Image_BG,77125,50,0,Right,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77124,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_sub_main_ui_button_103"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleRune.RunePanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Equip/Container_Strength/Button_AutoStrength,77127,-50,0,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77126,1000,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"__WorldBoss"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__WorldBoss2"	,"Goto:__WorldBoss1"
	}
	,{
		"__WorldBoss1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__WorldBoss2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__WorldBoss3"	,"Goto:__WorldBoss4"
	}
	,{
		"__WorldBoss3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__WorldBoss4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_RowBig/Button_7,77161,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77162,520,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_7"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleWorldBoss.WorldBossPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_WorldBoss/Container_WorldBoss/Container_BossInfo/Button_GuideTire,77163,-20,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77164,250,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_world_boss_guide_tire_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_WorldBoss/Container_WorldBoss/Container_BossInfo/Button_Fight,77165,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77166,250,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_world_boss_fight_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__ExpZone"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__ExpZone2"	,"Goto:__ExpZone1"
	}
	,{
		"__ExpZone1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__ExpZone2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__ExpZone3"	,"Goto:__ExpZone4"
	}
	,{
		"__ExpZone3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__ExpZone4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_Row3/Container_Stable/Button_2,77167,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_2"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleInstanceHall.InstanceHallPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_InstanceHall/Container_WinBg/Container_win1/ToggleGroup_Sec_Mirror/Toggle2,77168,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_base_panel_sec_tab_click_2"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleInstanceHall.ChildView.ExpInstanceView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_InstanceHall/Container_ExpInstance/Button_SingleEnter,77169,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77170,300,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_exp_instance_single_button_click"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__GemInlay"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__GemInlay2"	,"Goto:__GemInlay1"
	}
	,{
		"__GemInlay1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__GemInlay2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__GemInlay3"	,"Goto:__GemInlay4"
	}
	,{
		"__GemInlay3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__GemInlay4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_PlayerState/Button_Head,77180,90,50,Right,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77181,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_player_state_head_button"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleSubMainUI.SubMainUIPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_SubMainUI/Container_SystemLeft/Container_System3/Image_BG,77182,90,50,Right,0,0,0,0,1.2,1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_sub_main_ui_button_103"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleEquip.ChildView.EquipStrengthView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Equip/Container_WinBg/Container_win1/ToggleGroup_First_Mirror/Toggle2,77183,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_base_panel_first_tab_click_2"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleEquip.ChildView.EquipGemInlayView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Equip/Container_GemInlay/Container_GemItems/Container_GemItem1/Image_Add1,77184,-10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77185,300,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_equip_gem_slot_button"
	}
	,{
		"Judge"	,"HasButtonExistAndShow:LayerUIPanel/Panel_Equip/Container_GemInlay/Container_SelectGem/List_Gem/Mask/Content/item_0/Image_Bg"	,"Goto:__GemInlay17"	,"Goto:__GemInlay18"
	}
	,{
		"__GemInlay17"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Equip/Container_GemInlay/Container_SelectGem/List_Gem/Mask/Content/item_0/Image_Bg,77186,-100,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_equip_gem_inlay_list_button"
	}
	,{
		"__GemInlay18"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__Ring"
	}
	,{
		"StopAutoTask"
	}
	,{
		"Judge"	,"IsPanelActive:FunctionOpenTips"	,"Goto:__Ring1"	,"Goto:__Ring2"
	}
	,{
		"__Ring1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"__Ring2"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__Ring4"	,"Goto:__Ring3"
	}
	,{
		"__Ring3"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__Ring4"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__Ring5"	,"Goto:__Ring6"
	}
	,{
		"__Ring5"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__Ring6"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_Row1/Container_Stable/Button_5,77148,-10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_5"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleMainUI.OpenActivityPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMiddle,/Panel_OpenActivity/Container_List/Container_Act/ScrollViewList/Mask/Content/item_0/Image_Bg,77220,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_service_activity_ring_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleOpenRing.ChildView.LordRingView"
	}
	,{
		"Judge"	,"HasButtonExistAndShow:LayerUIPanel/Panel_OpenRing/Container_LordRings/ScrollViewRing/Mask/Content/item_0/Container_State/Button_Get_Reward"	,"Goto:__Ring7"	,"Goto:__Ring8"
	}
	,{
		"__Ring7"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_OpenRing/Container_LordRings/ScrollViewRing/Mask/Content/item_0/Container_State/Button_Get_Reward,77149,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77150,500,510,3414,-10000,-10000,0,0,-260,-510,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_service_activity_reward_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_OpenRing/Container_WinBg/Container_win5/Button_CloseMirror,77154,20,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_service_activity_close_button"
	}
	,{
		"__Ring8"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__Welfare"
	}
	,{
		"StopAutoTask"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__Welfare1"	,"Goto:__Welfare2"
	}
	,{
		"__Welfare1"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__Welfare2"
	}
	,{
		"Judge"	,"HasButtonExistAndShow:LayerUIMain/Panel_MainMenu/Container_Row2/Container_Move/Button_10"	,"Goto:__Welfare3"	,"Goto:__Welfare6"
	}
	,{
		"__Welfare3"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_Row2/Container_Move/Button_10,77144,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_10"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleWelfare.ChildView.WelfareSignView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Welfare/Container_Total/ScrollViewList_Buttons/Mask/Content/item_1,77145,160,50,Right,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_welfare_gift_ui_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleWelfare.ChildView.WelfareGiftView"
	}
	,{
		"Judge"	,"HasButtonExistAndShow:LayerUIPanel/Panel_Welfare/Container_Gift/Container_List/ScrollViewList/Mask/Content/item_0/Container_State/Button_Get_Reward"	,"Goto:__Welfare5"	,"Goto:__Welfare6"
	}
	,{
		"__Welfare5"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Welfare/Container_Gift/Container_List/GuideBtn,77146,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77147,530,500,3414,-10000,-10000,0,0,-250,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_welfare_gift_reward_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Welfare/Container_Total/Button_CloseMirror,77154,20,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_welfare_close_button"
	}
	,{
		"__Welfare6"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__DailyTask"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__DailyTask2"	,"Goto:__DailyTask1"
	}
	,{
		"__DailyTask1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__DailyTask2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__DailyTask3"	,"Goto:__DailyTask4"
	}
	,{
		"__DailyTask3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__DailyTask4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_Row3/Container_Stable/Button_1,77151,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_1"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleDailyActivity.DailyActivityExPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_DailyActivityEx/Container_Daily/Container_Content/ScrollViewList/Mask/Content/item_0/Button_Go,77152,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77153,1100,500,3414,-10000,-10000,0,0,-140,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_daily_goto_circle_task_ui_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__PetEat"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__PetEat2"	,"Goto:__PetEat1"
	}
	,{
		"__PetEat1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__PetEat2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__PetEat3"	,"Goto:__PetEat4"
	}
	,{
		"__PetEat3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__PetEat4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_PlayerState/Button_Head,77172,90,50,Right,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77171,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_player_state_head_button"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleSubMainUI.SubMainUIPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_SubMainUI/Container_SystemLeft/Container_System5/Image_BG,77174,90,50,Right,0,0,0,0,1.2,1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77173,680,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_sub_main_ui_button_105"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModulePartner.ChildView.PartnerPetView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Partner/Container_Pet/Container_Bless/Button_Devour,77176,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77175,300,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_partner_pet_devour_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModulePartner.ChildView.PartnerPetDevourView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Partner/Container_Pet/Container_Devour/Button_Devour,77178,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77177,1000,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_partner_pet_begin_devour_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Partner/Container_WinBg/Container_win1/Button_CloseMirror,77154,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_partner_panel_close_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__StartTrip"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"NoMask,LayerUIPanel,/Panel_Trigger/Button_Trigger,77179,-20,50,Left,-50,-50,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_fly_trigger_ui_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"End"
	}
	,{
		"__EquipTreasure"
	}
	,{
		"StopAutoTask"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__EquipTreasure1"	,"Goto:__EquipTreasure2"
	}
	,{
		"__EquipTreasure1"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__EquipTreasure2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_Row2/Container_Stable/Button_9,77187,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_9"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModulePrecious.ChildView.EquipFindView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Precious/Container_EquipFind/Container_TypeOne/Button_Get,77189,120,50,Right,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77188,1000,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_equip_find_type_one_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModulePrecious.ChildView.EquipFindReportView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Precious/Container_EquipFind/Container_Report/Button_Confirm,77191,120,50,Right,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77190,1000,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_equip_find_report_ok_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Precious/Container_EquipFind/Button_Bag,77193,100,50,Right,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77192,1000,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_equip_find_bag_button"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModulePrecious.ChildView.EquipFindWarehouseView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_Precious/Container_EquipFind/Container_Bag/Button_Get,77217,100,50,Right,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_equip_find_warehouse_get_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__PersonalBoss"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__PersonalBoss2"	,"Goto:__PersonalBoss1"
	}
	,{
		"__PersonalBoss1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__PersonalBoss2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__PersonalBoss3"	,"Goto:__PersonalBoss4"
	}
	,{
		"__PersonalBoss3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__PersonalBoss4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_RowBig/Button_7,77206,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77207,520,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_7"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleWorldBoss.WorldBossPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_WorldBoss/Container_WinBg/Container_win1/ToggleGroup_First_Mirror/Toggle2,77208,-20,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_base_panel_first_tab_click_2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_WorldBoss/Container_PersonalBoss/Container_BossInfo/Button_GuideTire,77210,-20,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77209,250,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_personal_boss_guide_tire_button"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_WorldBoss/Container_PersonalBoss/Container_BossInfo/Button_Fight,77212,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77211,250,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_personal_boss_fight_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__GuardTower"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__GuardTower2"	,"Goto:__GuardTower1"
	}
	,{
		"__GuardTower1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__GuardTower2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__GuardTower3"	,"Goto:__GuardTower4"
	}
	,{
		"__GuardTower3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__GuardTower4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_Row3/Container_Stable/Button_2,77198,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_2"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleInstanceHall.InstanceHallPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_InstanceHall/Container_WinBg/Container_win1/ToggleGroup_Sec_Mirror/Toggle3,77199,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_base_panel_sec_tab_click_3"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleInstanceHall.ChildView.PetInstanceView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_InstanceHall/Container_PetInstance/Button_Enter,77201,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77200,300,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_pet_instance_battle_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__GodsTreasury"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__GodsTreasury2"	,"Goto:__GodsTreasury1"
	}
	,{
		"__GodsTreasury1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__GodsTreasury2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__GodsTreasury3"	,"Goto:__GodsTreasury4"
	}
	,{
		"__GodsTreasury3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__GodsTreasury4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_Row3/Container_Stable/Button_2,77194,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_2"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleInstanceHall.InstanceHallPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_InstanceHall/Container_WinBg/Container_win1/ToggleGroup_Sec_Mirror/Toggle4,77195,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_base_panel_sec_tab_click_4"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleInstanceHall.ChildView.MoneyInstanceView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_InstanceHall/Container_PetInstance/Button_Enter,77197,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77196,300,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_money_instance_battle_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__DragonSoul"
	}
	,{
		"StopAutoTask"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_function_open_animate_complete"
	}
	,{
		"Judge"	,"IsInConfigMapType:1,2"	,"Goto:__DragonSoul2"	,"Goto:__DragonSoul1"
	}
	,{
		"__DragonSoul1"
	}
	,{
		"WaitingEvent"	,"ENTER_MAP_TYPE"	,"MainOrWild"
	}
	,{
		"__DragonSoul2"
	}
	,{
		"Judge"	,"HasPanelShow"	,"Goto:__DragonSoul3"	,"Goto:__DragonSoul4"
	}
	,{
		"__DragonSoul3"
	}
	,{
		"WaitingEvent"	,"ON_NO_PANEL_SHOW"	,"guide_no_panel_show"
	}
	,{
		"__DragonSoul4"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_MainMenu/Container_Row3/Container_Stable/Button_2,77202,10,50,Left,0,0,0,0,1.2,1"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_main_menu_button_2"	,"10000"	,"1"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleInstanceHall.InstanceHallPanel"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_InstanceHall/Container_WinBg/Container_win1/ToggleGroup_Sec_Mirror/Toggle5,77203,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_base_panel_sec_tab_click_5"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleInstanceHall.ChildView.TowerInstanceView"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIPanel,/Panel_InstanceHall/Container_PetInstance/Button_Enter,77205,-10,50,Left,0,0,0,0,1.2"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77204,300,500,3414,-10000,-10000,0,0,-270,-500,-200,1.2,Left"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_tower_instance_battle_button"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__FlyShoes"
	}
	,{
		"Judge"	,"TriggerCount:1004,1"	,"null"	,"End"
	}
	,{
		"StopAutoTask"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"Mask,LayerUIMain,/Panel_LeftMainUI/Container_TaskAndTeam/Container_Task/ScrollViewList/Mask/Content/item_0/Container_content/Image_HeadIcon,77213,80,50,Right,0,0,0,0,1.2"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"DialogueWithMask,77214,1000,500,3414,-10000,-10000,0,0,-100,-500,-200,1.2,Right"
	}
	,{
		"ExAction"	,"SetTriggerCount"	,"1004"
	}
	,{
		"WaitingEvent"	,"CLICK_BTN"	,"guide_little_shoes_button"	,"10000"	,"1"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"0"
	}
	,{
		"ShowPanel"	,"GuideSkip"	,"0"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"__VipExperience"
	}
	,{
		"ExAction"	,"ShowGuideArrow"	,"NoMask,LayerUIPanel,/Panel_VIPPop/Container_Experience/Button_Experience,77229,160,50,Right,-50,-50,0,0,1.2"
	}
}
return drama_guide
