local CG600054 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"PreloadResources"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"1"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"400"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowPlayerPet"	,"0"
	}
	,{
		"HideOtherPlayer"	,"1"
	}
	,{
		"HideSelfPlayer"	,"1"
	}
	,{
		"Judge"	,"IsVocGroup:1"	,"Goto:__Mark3"	,"null"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"400"
	}
	,{
		"CreateEntity"	,"NPC"	,"10001"	,"1002"	,"42,40,139.44"	,"180"	,"1"
	}
	,{
		"PlaySkill"	,"10001"	,"131"
	}
	,{
		"RotateCameraForLocal"	,"10001"	,"bip_root"	,"5,180,0,0.5,0"	,"0"	,"2"	,"0"
	}
	,{
		"Sleep"	,"1700"
	}
	,{
		"ShowIntroduce"	,"4624"
	}
	,{
		"KeepTimeScale"	,"0.3"	,"3000"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"300"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPlayerPet"	,"1"
	}
	,{
		"HideOtherPlayer"	,"0"
	}
	,{
		"HideSelfPlayer"	,"0"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"HideIntroduce"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"0"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"End"
	}
	,{
		"__Mark3"
	}
	,{
		"CreateEntity"	,"NPC"	,"10002"	,"151"	,"41,40,140"	,"180"	,"1"
	}
	,{
		"CreateEntity"	,"NPC"	,"10003"	,"1001"	,"43.57,40,136.51"	,"320"	,"1"
	}
	,{
		"RotateCameraForLocal"	,"10002"	,"slot_camera"	,"30,120,0"	,"0"	,"15"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"Sleep"	,"300"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_31010.prefab"	,"43.79,40,138.32"	,"6200"	,"0,0,0"
	}
	,{
		"PlaySound"	,"1023"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"RotateCameraForLocal"	,"10002"	,"slot_camera"	,"10,290,0,0.3,1"	,"6000"	,"4"	,"6000"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_31010.prefab"	,"41.83,40,136.91"	,"4000"	,"0,0,0"
	}
	,{
		"PlaySound"	,"1023"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_31010.prefab"	,"39.39,40,137.73"	,"4000"	,"0,0,0"
	}
	,{
		"PlaySound"	,"1023"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_31010.prefab"	,"37.95,40,139.83"	,"4000"	,"0,0,0"
	}
	,{
		"PlaySound"	,"1023"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_31010.prefab"	,"38.85,40,142.18"	,"4000"	,"0,0,0"
	}
	,{
		"PlaySound"	,"1023"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_31010.prefab"	,"40.79,40,143.34"	,"4000"	,"0,0,0"
	}
	,{
		"PlaySound"	,"1023"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_31010.prefab"	,"43.23,40,142.83"	,"4000"	,"0,0,0"
	}
	,{
		"PlaySound"	,"1023"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/sence/fx_scenes_31010.prefab"	,"44.72,40,140.68"	,"4000"	,"0,0,0"
	}
	,{
		"PlaySound"	,"1023"
	}
	,{
		"Sleep"	,"300"
	}
	,{
		"PlayVisualFX"	,"Fx/fx_prefab/fire/Particle_fire_7.prefab"	,"41,41.83,140"	,"3000"	,"0,0,0"
	}
	,{
		"CreateEntity"	,"NPC"	,"10004"	,"152"	,"41,40,140"	,"180"	,"1"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"ShowIntroduce"	,"4624"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"300"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPlayerPet"	,"1"
	}
	,{
		"HideOtherPlayer"	,"0"
	}
	,{
		"HideSelfPlayer"	,"0"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"HideIntroduce"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"0"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"500"
	}
	,{
		"End"
	}
}
return CG600054
