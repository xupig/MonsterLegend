local CG10005 = {
	{
		"__begin1"
	}
	,{
		"StopAI"
	}
	,{
		"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"MoveCamera"	,"4.38,68.47,0.7"	,"0"	,"104,44.4,190.1"	,"0"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"MoveCamera"	,"4.38,68.47,0.7"	,"0"	,"161.6,44.4,188.8"	,"3000"
	}
	,{
		"Sleep"	,"2900"
	}
	,{
		"MoveCamera"	,"32.845,22.245,0"	,"3500"	,"189.8,44.4,159"	,"3500"
	}
	,{
		"Sleep"	,"3400"
	}
	,{
		"MoveCamera"	,"32.845,22.245,0"	,"0"	,"235,86,203.6"	,"5000"
	}
	,{
		"Sleep"	,"4800"
	}
	,{
		"MoveCamera"	,"4.38,32.246,0.7"	,"3000"	,"239.8,42.7,244.2"	,"3000"
	}
	,{
		"Sleep"	,"4000"
	}
	,{
		"RecoverCamera"
	}
	,{
		"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"StartAI"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
}
return CG10005
