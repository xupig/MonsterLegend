drama_trigger = {
	{
		"0"	,"0"	,"0"	,"0"	,"0"
	}
	,{
		"1001"	,"PLAY_CG"	,"1001"	,"CG1001"
	}
	,{
		"1002"	,"PLAY_CG"	,"1002"	,"CG1002"
	}
	,{
		"1100"	,"ENTER_ELEVATOR"	,"1"	,"elevator"	,"__elevator1"
	}
	,{
		"1101"	,"ENTER_ELEVATOR"	,"2"	,"elevator"	,"__elevator2"
	}
	,{
		"500101"	,"PLAY_CG"	,"500101"	,"CG5001"	,"__begin"
	}
	,{
		"1000201"	,"PLAY_CG"	,"1000201"	,"CG10002"	,"__bomb"
	}
	,{
		"1000202"	,"PLAY_CG"	,"1000202"	,"CG10002"	,"__bombfinish"
	}
	,{
		"1000203"	,"PLAY_CG"	,"1000203"	,"CG10002"	,"__bossentrance"
	}
	,{
		"1000204"	,"FINISH_QUEST"	,"1000204"	,"CG10002"	,"__start"
	}
	,{
		"2000001"	,"PLAY_CG"	,"2000001"	,"CG20000"	,"__begin"
	}
	,{
		"2000002"	,"PLAY_CG"	,"2000002"	,"CG20000"	,"__mission"
	}
	,{
		"2000003"	,"PLAY_CG"	,"2000003"	,"CG20000"	,"__bossentrance"
	}
	,{
		"2000005"	,"PLAY_CG"	,"2000005"	,"CG20000"	,"__hideskill1"
	}
	,{
		"2000006"	,"PLAY_CG"	,"2000006"	,"CG20000"	,"__hideskill2"
	}
	,{
		"2000007"	,"PLAY_CG"	,"2000007"	,"CG20000"	,"__hideskill3"
	}
	,{
		"2000008"	,"PLAY_CG"	,"2000008"	,"CG20000"	,"__hideskill4"
	}
	,{
		"2000009"	,"PLAY_CG"	,"2000009"	,"CG20000"	,"__unlockskill1"
	}
	,{
		"2000010"	,"PLAY_CG"	,"2000010"	,"CG20000"	,"__unlockskill3"
	}
	,{
		"2000012"	,"PLAY_CG"	,"2000012"	,"CG20000"	,"__start"
	}
	,{
		"2000013"	,"PLAY_CG"	,"2000013"	,"CG20000"	,"__unlockskill4"
	}
	,{
		"2000014"	,"PLAY_CG"	,"2000014"	,"CG20000"	,"__unlockskill2"
	}
	,{
		"2000015"	,"PLAY_CG"	,"2000015"	,"CG20000"	,"__unlockskill0"
	}
	,{
		"2000"	,"FINISH_QUEST"	,"4"	,"CG2000"	,"__start"
	}
	,{
		"1103"	,"ACCEPT_QUEST"	,"6"	,"CG2001"	,"__backtocity"
	}
	,{
		"1102"	,"ENTER_MAP"	,"1"	,"CG2001"	,"__start"
	}
	,{
		"2010102"	,"PLAY_CG"	,"2010102"	,"CG20101"	,"__bossentrance"
	}
	,{
		"2010201"	,"PLAY_CG"	,"2010201"	,"CG20102"	,"__bossentrance"
	}
	,{
		"2010301"	,"PLAY_CG"	,"2010301"	,"CG20103"	,"__bossentrance"
	}
	,{
		"2020101"	,"PLAY_CG"	,"2020101"	,"CG20201"	,"__bossentrance"
	}
	,{
		"2020301"	,"PLAY_CG"	,"2020301"	,"CG20203"	,"__bossentrance"
	}
	,{
		"2020401"	,"PLAY_CG"	,"2020401"	,"CG20204"	,"__bossentrance"
	}
	,{
		"2020501"	,"PLAY_CG"	,"2020501"	,"CG20205"	,"__bossentrance"
	}
	,{
		"2020601"	,"PLAY_CG"	,"2020601"	,"CG20206"	,"__bossentrance"
	}
	,{
		"2030401"	,"PLAY_CG"	,"2030401"	,"CG20304"	,"__bossentrance"
	}
	,{
		"2030501"	,"PLAY_CG"	,"2030501"	,"CG20305"	,"__bossentrance"
	}
	,{
		"2030601"	,"PLAY_CG"	,"2030601"	,"CG20306"	,"__bossentrance"
	}
	,{
		"2030701"	,"PLAY_CG"	,"2030701"	,"CG20307"	,"__bossentrance"
	}
	,{
		"2030801"	,"PLAY_CG"	,"2030801"	,"CG20308"	,"__bossentrance"
	}
	,{
		"2040201"	,"PLAY_CG"	,"2040201"	,"CG20402"	,"__bossentrance"
	}
	,{
		"2040401"	,"PLAY_CG"	,"2040401"	,"CG20404"	,"__bossentrance"
	}
	,{
		"2040501"	,"PLAY_CG"	,"2040501"	,"CG20405"	,"__bossentrance"
	}
	,{
		"2040601"	,"PLAY_CG"	,"2040601"	,"CG20406"	,"__bossentrance"
	}
	,{
		"2040701"	,"PLAY_CG"	,"2040701"	,"CG20407"	,"__bossentrance"
	}
	,{
		"2040901"	,"PLAY_CG"	,"2040901"	,"CG20409"	,"__bossentrance"
	}
	,{
		"2050101"	,"PLAY_CG"	,"2050101"	,"CG20501"	,"__bossentrance"
	}
	,{
		"2050201"	,"PLAY_CG"	,"2050201"	,"CG20502"	,"__bossentrance"
	}
	,{
		"2050301"	,"PLAY_CG"	,"2050301"	,"CG20503"	,"__bossentrance"
	}
	,{
		"2050302"	,"PLAY_CG"	,"2050302"	,"CG20503"	,"__endinstance"
	}
	,{
		"2050401"	,"PLAY_CG"	,"2050401"	,"CG20504"	,"__bossentrance"
	}
	,{
		"2050501"	,"PLAY_CG"	,"2050501"	,"CG20505"	,"__bossentrance"
	}
	,{
		"2050601"	,"PLAY_CG"	,"2050601"	,"CG20506"	,"__bossentrance"
	}
	,{
		"2050701"	,"PLAY_CG"	,"2050701"	,"CG20507"	,"__bossentrance"
	}
	,{
		"2050801"	,"PLAY_CG"	,"2050801"	,"CG20508"	,"__bossentrance"
	}
	,{
		"2060101"	,"PLAY_CG"	,"2060101"	,"CG20601"	,"__bossentrance"
	}
	,{
		"2060201"	,"PLAY_CG"	,"2060201"	,"CG20602"	,"__bossentrance"
	}
	,{
		"2060301"	,"PLAY_CG"	,"2060301"	,"CG20603"	,"__bossentrance"
	}
	,{
		"2060401"	,"PLAY_CG"	,"2060401"	,"CG20604"	,"__bossentrance"
	}
	,{
		"2060501"	,"PLAY_CG"	,"2060501"	,"CG20605"	,"__bossentrance"
	}
	,{
		"2060601"	,"PLAY_CG"	,"2060601"	,"CG20606"	,"__bossentrance"
	}
	,{
		"2060701"	,"PLAY_CG"	,"2060701"	,"CG20607"	,"__bossentrance"
	}
	,{
		"2060801"	,"PLAY_CG"	,"2060801"	,"CG20608"	,"__bossentrance"
	}
	,{
		"2060901"	,"PLAY_CG"	,"2060901"	,"CG20609"	,"__bossentrance"
	}
	,{
		"2060902"	,"PLAY_CG"	,"2060902"	,"CG20609"	,"__endinstance"
	}
	,{
		"2080501"	,"PLAY_CG"	,"2080501"	,"CG20805"	,"__bossentrance"
	}
	,{
		"2080601"	,"PLAY_CG"	,"2080601"	,"CG20806"	,"__bossentrance"
	}
	,{
		"2080701"	,"PLAY_CG"	,"2080701"	,"CG20807"	,"__bossentrance"
	}
	,{
		"2080801"	,"PLAY_CG"	,"2080801"	,"CG20808"	,"__bossentrance"
	}
	,{
		"2080901"	,"PLAY_CG"	,"2080901"	,"CG20809"	,"__bossentrance"
	}
	,{
		"2090801"	,"PLAY_CG"	,"2090801"	,"CG20908"	,"__bossentrance"
	}
	,{
		"2090901"	,"PLAY_CG"	,"2090901"	,"CG20909"	,"__bossentrance"
	}
	,{
		"2100901"	,"PLAY_CG"	,"2100901"	,"CG21009"	,"__bossentrance"
	}
	,{
		"2120901"	,"PLAY_CG"	,"2120901"	,"CG21209"	,"__bossentrance"
	}
	,{
		"2190501"	,"PLAY_CG"	,"2190501"	,"CG21905"	,"__enterinstance"
	}
	,{
		"2190502"	,"PLAY_CG"	,"2190502"	,"CG21905"	,"__killmonsters"
	}
	,{
		"2190503"	,"PLAY_CG"	,"2190503"	,"CG21905"	,"__firstwavemonsters"
	}
	,{
		"2190504"	,"PLAY_CG"	,"2190504"	,"CG21905"	,"__secondwavemonsters"
	}
	,{
		"2190505"	,"PLAY_CG"	,"2190505"	,"CG21905"	,"__thirdwavemonsters"
	}
	,{
		"2190506"	,"PLAY_CG"	,"2190506"	,"CG21905"	,"__fail"
	}
	,{
		"2190507"	,"PLAY_CG"	,"2190507"	,"CG21905"	,"__successful"
	}
	,{
		"2000401"	,"ENTER_ELEVATOR"	,"2000401"	,"CG20004"	,"__enterelevator"
	}
	,{
		"1000301"	,"ENTER_ELEVATOR"	,"1000301"	,"CG10003"	,"__enterelevator"
	}
	,{
		"1000302"	,"PLAY_CG"	,"1000302"	,"CG10003"	,"__begin"
	}
}
return drama_trigger
