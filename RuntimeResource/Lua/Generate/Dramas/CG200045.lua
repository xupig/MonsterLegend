local CG200045 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"SetShowPlayerModel"	,"2"
	}
	,{
		"MoveCamera"	,"5.36,180,0"	,"0"	,"-1921.9,3.06,-1899.5"	,"0"
	}
	,{
		"CreateEntity"	,"Avatar"	,"1"	,"201"	,"-1923.1,0.15,-1909.89"	,"90"	,"1"
	}
	,{
		"TeleportEntity"	,"ShowPlayer"	,"-1920.6,0.15,-1909.89"
	}
	,{
		"RotateEntity"	,"ShowPlayer"	,"0,270,0"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"1"	,"110"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"83"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"86"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"PlaySkill"	,"1"	,"123"
	}
	,{
		"Sleep"	,"1100"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"75"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"1"	,"121"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"74"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"1"	,"127"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"85"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"86"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"KeepTimeScale"	,"0.3"	,"2000"
	}
	,{
		"PlayAction"	,"1"	,"92"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"OnCGTriggerEventType"	,"120"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleAthletics.AthleticsPanel"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"DestroyEntities"
	}
	,{
		"SetShowPlayerModel"	,"3"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
}
return CG200045
