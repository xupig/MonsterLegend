local CG99991 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"PreloadResources"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"TeleportEntity"	,"Player"	,"0,0,0"
	}
	,{
		"CreateEntity"	,"NPC"	,"10001"	,"16"	,"112.45,15.73,103.93"	,"180"	,"1"
	}
	,{
		"MoveCamera"	,"60,0,0"	,"0"	,"115.94,32.36,83"	,"0"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"MoveCamera"	,"0,0,0"	,"4000"	,"113.04,17.07,97.69"	,"4000"
	}
	,{
		"Sleep"	,"8000"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"HideIntroduce"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"End"
	}
}
return CG99991
