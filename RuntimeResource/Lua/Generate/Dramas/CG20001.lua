local CG20001 = {
	{
		"__begin1"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"901"	,"1"	,"2"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"901"	,"1"	,"0.6"
	}
	,{
		"PreloadResources"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"MoveCamera"	,"0,13,0"	,"0"	,"86.7,1,165.8"	,"0"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"0"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"901"	,"1"	,"2"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"901"	,"1"	,"0.6"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"CreateEntity"	,"NPC"	,"10004"	,"4"	,"50,30,169.57"	,"316.412"	,"1"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10001"	,"4"	,"69.57,27.2,169.57"	,"316.412"	,"1"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10002"	,"1"	,"63.88,27.41,177.01"	,"148.552"	,"1"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"MoveCamera"	,"6.1,327,0"	,"0.8"	,"75.7,1.2,162.2"	,"0.8"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"MoveCamera"	,"6.1,327,0"	,"1.5"	,"73.4,1,166"	,"1.5"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"PlayAction"	,"10002"	,"4"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlayAction"	,"10001"	,"47"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"MoveTo"	,"10002"	,"69.47,27.15,169.15"	,"1"
	}
	,{
		"MoveCamera"	,"6.2,327,359"	,"0"	,"70,1,164.5"	,"0"
	}
	,{
		"Sleep"	,"0.1"
	}
	,{
		"MoveCamera"	,"6.2,327,359"	,"3"	,"68.75,1.2,168"	,"3"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"DestroyEntity"	,"10001"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ChangeCameraFOV"	,"25"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"901"	,"1"	,"2"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"901"	,"1"	,"0.6"
	}
	,{
		"RotateEntity"	,"10002"	,"0,211.75,0"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"MoveCamera"	,"0,252,359"	,"3"	,"59,1,159"	,"3"
	}
	,{
		"Sleep"	,"4000"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10003"	,"4"	,"53,27.5,158"	,"61"	,"1"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"ChangeCameraFOV"	,"60"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"901"	,"1"	,"2"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"901"	,"1"	,"0.6"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"MoveCamera"	,"0,253,359"	,"0"	,"76.6,2,168.5"	,"0"
	}
	,{
		"TeleportEntity"	,"10002"	,"64.04,27.15,177.13"
	}
	,{
		"TeleportEntity"	,"10003"	,"52.87,27,161.34"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"901"	,"1"	,"2"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"901"	,"1"	,"0.6"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"MoveCamera"	,"83.56,116.5,300"	,"0"	,"59,22.5,164"	,"9"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"901"	,"1"	,"2"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"901"	,"1"	,"0.6"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"MoveTo"	,"10002"	,"60,27,171"	,"-1"
	}
	,{
		"MoveTo"	,"10003"	,"58,27,168"	,"-1"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"End"
	}
}
return CG20001
