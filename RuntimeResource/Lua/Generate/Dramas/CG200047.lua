local CG200047 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"SetShowPlayerModel"	,"2"
	}
	,{
		"MoveCamera"	,"5.36,180,0"	,"0"	,"-1921.9,3.06,-1899.5"	,"0"
	}
	,{
		"CreateEntity"	,"Avatar"	,"1"	,"101"	,"-1923.11,0.15,-1909.89"	,"90"	,"1"
	}
	,{
		"TeleportEntity"	,"ShowPlayer"	,"-1920.6,0.15,-1909.89"
	}
	,{
		"RotateEntity"	,"ShowPlayer"	,"0,270,0"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"72"
	}
	,{
		"PlaySkill"	,"1"	,"83"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"79"
	}
	,{
		"Sleep"	,"900"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"86"
	}
	,{
		"PlaySkill"	,"1"	,"85"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"85"
	}
	,{
		"PlaySkill"	,"1"	,"86"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"77"
	}
	,{
		"PlaySkill"	,"1"	,"72"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"79"
	}
	,{
		"PlaySkill"	,"1"	,"73"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"86"
	}
	,{
		"PlaySkill"	,"1"	,"78"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"KeepTimeScale"	,"0.3"	,"2000"
	}
	,{
		"PlayAction"	,"1"	,"92"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"OnCGTriggerEventType"	,"110"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleAthletics.AthleticsPanel"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"DestroyEntities"
	}
	,{
		"SetShowPlayerModel"	,"3"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
}
return CG200047
