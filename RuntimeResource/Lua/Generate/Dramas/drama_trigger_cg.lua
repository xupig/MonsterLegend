local drama_trigger_cg = {
	{
		"0"	,"0"	,"0"	,"0"	,"0"
	}
	,{
		"100001"	,"PLAY_CG"	,"100001"	,"CG1000"	,"__hideskill"
	}
	,{
		"100002"	,"PLAY_CG"	,"100002"	,"CG1000"	,"__unlockskill1011"
	}
	,{
		"100003"	,"PLAY_CG"	,"100003"	,"CG1000"	,"__unlockskill1012"
	}
	,{
		"100004"	,"PLAY_CG"	,"100004"	,"CG1000"	,"__unlockskill1021"
	}
	,{
		"100005"	,"PLAY_CG"	,"100005"	,"CG1000"	,"__unlockskill1022"
	}
	,{
		"100006"	,"PLAY_CG"	,"100006"	,"CG1000"	,"__unlockskill1031"
	}
	,{
		"100007"	,"PLAY_CG"	,"100007"	,"CG1000"	,"__unlockskill1032"
	}
	,{
		"100008"	,"PLAY_CG"	,"100008"	,"CG1000"	,"__unlockskill1041"
	}
	,{
		"100009"	,"PLAY_CG"	,"100009"	,"CG1000"	,"__unlockskill1042"
	}
	,{
		"100101"	,"PLAY_CG"	,"100101"	,"CG1001"	,"__begin1"
	}
	,{
		"100201"	,"PLAY_CG"	,"100201"	,"CG1002"	,"__hideskill1"
	}
	,{
		"100202"	,"PLAY_CG"	,"100202"	,"CG1002"	,"__hideskill2"
	}
	,{
		"100203"	,"PLAY_CG"	,"100203"	,"CG1002"	,"__hideskill3"
	}
	,{
		"100204"	,"PLAY_CG"	,"100204"	,"CG1002"	,"__hideskill4"
	}
	,{
		"100205"	,"PLAY_CG"	,"100205"	,"CG1002"	,"__unlockskill0"
	}
	,{
		"100206"	,"PLAY_CG"	,"100206"	,"CG1002"	,"__dianjirenwu"
	}
	,{
		"100207"	,"PLAY_CG"	,"100207"	,"CG1002"	,"__mode"
	}
	,{
		"100301"	,"PLAY_CG"	,"100301"	,"CG1003"	,"__hideallskill"
	}
	,{
		"100302"	,"PLAY_CG"	,"100302"	,"CG1003"	,"__unlockskill1"
	}
	,{
		"100303"	,"PLAY_CG"	,"100303"	,"CG1003"	,"__unlockskill3"
	}
	,{
		"100304"	,"PLAY_CG"	,"100304"	,"CG1003"	,"__monsters"
	}
	,{
		"100305"	,"PLAY_CG"	,"100305"	,"CG1003"	,"__ending"
	}
	,{
		"100701"	,"PLAY_CG"	,"100701"	,"CG1007"	,"__begin"
	}
	,{
		"100901"	,"PLAY_CG"	,"100901"	,"CG1009"	,"__begin"
	}
	,{
		"103101"	,"PLAY_CG"	,"103101"	,"CG1031"	,"__begin"
	}
	,{
		"103201"	,"PLAY_CG"	,"103201"	,"CG1032"	,"__begin"
	}
	,{
		"105401"	,"PLAY_CG"	,"105401"	,"CG1054"	,"__begin"
	}
	,{
		"106101"	,"PLAY_CG"	,"106101"	,"CG1061"	,"__begin"
	}
	,{
		"106102"	,"PLAY_CG"	,"106102"	,"CG1061"	,"__boom"
	}
	,{
		"106201"	,"PLAY_CG"	,"106201"	,"CG1062"	,"__begin"
	}
	,{
		"199901"	,"PLAY_CG"	,"199901"	,"CG1999"	,"__talent-choose"
	}
	,{
		"200100"	,"ENTER_ELEVATOR"	,"1"	,"elevator"	,"__elevator1"
	}
	,{
		"200101"	,"ENTER_ELEVATOR"	,"2"	,"elevator"	,"__elevator2"
	}
	,{
		"200103"	,"ENTER_ELEVATOR"	,"3"	,"elevator"	,"__elevator3"
	}
	,{
		"200104"	,"ENTER_ELEVATOR"	,"4"	,"elevator"	,"__elevator4"
	}
	,{
		"200102"	,"ENTER_MAP"	,"1"	,"CG2001"	,"__start"
	}
	,{
		"200200"	,"FINISH_QUEST"	,"1000204"	,"CG10002"	,"__start"
	}
	,{
		"300201"	,"PLAY_CG"	,"300201"	,"CG3002"	,"__tele1"
	}
	,{
		"300202"	,"PLAY_CG"	,"300202"	,"CG3002"	,"__tele2"
	}
	,{
		"300203"	,"PLAY_CG"	,"300203"	,"CG3002"	,"__tele3"
	}
	,{
		"300204"	,"PLAY_CG"	,"300204"	,"CG3002"	,"__tele4"
	}
	,{
		"300205"	,"PLAY_CG"	,"300205"	,"CG3002"	,"__tele5"
	}
	,{
		"300206"	,"PLAY_CG"	,"300206"	,"CG3002"	,"__bomb"
	}
	,{
		"300301"	,"PLAY_CG"	,"300301"	,"CG3003"	,"__tele1"
	}
	,{
		"300302"	,"PLAY_CG"	,"300302"	,"CG3003"	,"__tele2"
	}
	,{
		"300303"	,"PLAY_CG"	,"300303"	,"CG3003"	,"__tele3"
	}
	,{
		"300304"	,"PLAY_CG"	,"300304"	,"CG3003"	,"__tele4"
	}
	,{
		"300305"	,"PLAY_CG"	,"300305"	,"CG3003"	,"__tele5"
	}
	,{
		"300401"	,"PLAY_CG"	,"300401"	,"CG3004"	,"__tele1"
	}
	,{
		"300402"	,"PLAY_CG"	,"300402"	,"CG3004"	,"__tele2"
	}
	,{
		"300403"	,"PLAY_CG"	,"300403"	,"CG3004"	,"__tele3"
	}
	,{
		"300404"	,"PLAY_CG"	,"300404"	,"CG3004"	,"__tele4"
	}
	,{
		"300405"	,"PLAY_CG"	,"300405"	,"CG3004"	,"__tele5"
	}
	,{
		"300501"	,"PLAY_CG"	,"300501"	,"CG3005"	,"__tele1"
	}
	,{
		"300502"	,"PLAY_CG"	,"300502"	,"CG3005"	,"__tele2"
	}
	,{
		"300503"	,"PLAY_CG"	,"300503"	,"CG3005"	,"__tele3"
	}
	,{
		"300504"	,"PLAY_CG"	,"300504"	,"CG3005"	,"__tele4"
	}
	,{
		"300505"	,"PLAY_CG"	,"300505"	,"CG3005"	,"__tele5"
	}
	,{
		"300601"	,"PLAY_CG"	,"300601"	,"CG3006"	,"__tele1"
	}
	,{
		"300602"	,"PLAY_CG"	,"300602"	,"CG3006"	,"__tele2"
	}
	,{
		"300603"	,"PLAY_CG"	,"300603"	,"CG3006"	,"__tele3"
	}
	,{
		"300604"	,"PLAY_CG"	,"300604"	,"CG3006"	,"__tele4"
	}
	,{
		"300605"	,"PLAY_CG"	,"300605"	,"CG3006"	,"__tele5"
	}
	,{
		"300701"	,"PLAY_CG"	,"300701"	,"CG3007"	,"__tele1"
	}
	,{
		"300702"	,"PLAY_CG"	,"300702"	,"CG3007"	,"__tele2"
	}
	,{
		"300703"	,"PLAY_CG"	,"300703"	,"CG3007"	,"__tele3"
	}
	,{
		"300704"	,"PLAY_CG"	,"300704"	,"CG3007"	,"__tele4"
	}
	,{
		"300705"	,"PLAY_CG"	,"300705"	,"CG3007"	,"__tele5"
	}
	,{
		"310101"	,"PLAY_CG"	,"310101"	,"CG3101"	,"__win"
	}
	,{
		"310102"	,"PLAY_CG"	,"310102"	,"CG3101"	,"__tankskill1"
	}
	,{
		"310103"	,"PLAY_CG"	,"310103"	,"CG3101"	,"__tankskill2"
	}
	,{
		"310104"	,"PLAY_CG"	,"310104"	,"CG3101"	,"__tankskill3"
	}
	,{
		"310105"	,"PLAY_CG"	,"310105"	,"CG3101"	,"__tankskill4"
	}
	,{
		"310106"	,"PLAY_CG"	,"310106"	,"CG3101"	,"__hideskill"
	}
	,{
		"310107"	,"PLAY_CG"	,"310107"	,"CG3101"	,"__move"
	}
	,{
		"310108"	,"PLAY_CG"	,"310108"	,"CG3101"	,"__tele1"
	}
	,{
		"310201"	,"COMMIT_QUEST"	,"280"	,"CG3102"	,"__start"
	}
	,{
		"2000001"	,"PLAY_CG"	,"2000001"	,"CG20000"	,"__begin"
	}
	,{
		"2000002"	,"PLAY_CG"	,"2000002"	,"CG20000"	,"__mission"
	}
	,{
		"2000003"	,"PLAY_CG"	,"2000003"	,"CG20000"	,"__bossentrance"
	}
	,{
		"2000005"	,"PLAY_CG"	,"2000005"	,"CG20000"	,"__hideskill1"
	}
	,{
		"2000006"	,"PLAY_CG"	,"2000006"	,"CG20000"	,"__hideskill2"
	}
	,{
		"2000007"	,"PLAY_CG"	,"2000007"	,"CG20000"	,"__hideskill3"
	}
	,{
		"2000008"	,"PLAY_CG"	,"2000008"	,"CG20000"	,"__hideskill4"
	}
	,{
		"2000009"	,"PLAY_CG"	,"2000009"	,"CG20000"	,"__unlockskill1"
	}
	,{
		"2000010"	,"PLAY_CG"	,"2000010"	,"CG20000"	,"__unlockskill3"
	}
	,{
		"2000012"	,"PLAY_CG"	,"2000012"	,"CG20000"	,"__start"
	}
	,{
		"2000013"	,"PLAY_CG"	,"2000013"	,"CG20000"	,"__unlockskill4"
	}
	,{
		"2000014"	,"PLAY_CG"	,"2000014"	,"CG20000"	,"__unlockskill2"
	}
	,{
		"2000015"	,"PLAY_CG"	,"2000015"	,"CG20000"	,"__unlockskill0"
	}
	,{
		"2000016"	,"PLAY_CG"	,"2000016"	,"CG20000"	,"__teachskill4"
	}
	,{
		"2000017"	,"PLAY_CG"	,"2000017"	,"CG20000"	,"__interrupt"
	}
	,{
		"2000018"	,"PLAY_CG"	,"2000018"	,"CG20000"	,"__defence"
	}
	,{
		"2000019"	,"PLAY_CG"	,"2000019"	,"CG20000"	,"__bossentrance2"
	}
	,{
		"2000020"	,"PLAY_CG"	,"2000020"	,"CG20000"	,"__talk"
	}
	,{
		"2000021"	,"PLAY_CG"	,"2000021"	,"CG20000"	,"__unlockskill22"
	}
	,{
		"2000022"	,"PLAY_CG"	,"2000022"	,"CG20000"	,"__start2"
	}
	,{
		"2000023"	,"PLAY_CG"	,"2000023"	,"CG20000"	,"__begin2"
	}
	,{
		"1000201"	,"PLAY_CG"	,"1000201"	,"CG10002"	,"__bomb"
	}
	,{
		"1000202"	,"PLAY_CG"	,"1000202"	,"CG10002"	,"__bombfinish"
	}
	,{
		"1000203"	,"PLAY_CG"	,"1000203"	,"CG10002"	,"__bossentrance"
	}
	,{
		"1000204"	,"PLAY_CG"	,"1000204"	,"CG10002"	,"__bombgoal"
	}
	,{
		"1000205"	,"PLAY_CG"	,"1000205"	,"CG10002"	,"__landing"
	}
	,{
		"1000301"	,"ENTER_ELEVATOR"	,"1000301"	,"CG10003"	,"__enterelevator"
	}
	,{
		"1000302"	,"PLAY_CG"	,"1000302"	,"CG10003"	,"__begin"
	}
	,{
		"1000303"	,"PLAY_CG"	,"1000303"	,"CG10003"	,"__bossentrance"
	}
	,{
		"1000304"	,"PLAY_CG"	,"1000304"	,"CG10003"	,"__bossentrance2"
	}
	,{
		"1000305"	,"PLAY_CG"	,"1000305"	,"CG10003"	,"__bossentrance3"
	}
	,{
		"1000306"	,"PLAY_CG"	,"1000306"	,"CG10003"	,"__partguide"
	}
	,{
		"1000307"	,"PLAY_CG"	,"1000307"	,"CG10003"	,"__passwin"
	}
	,{
		"1000308"	,"PLAY_CG"	,"1000308"	,"CG10003"	,"__passwin2"
	}
	,{
		"500101"	,"PLAY_CG"	,"500101"	,"CG5001"	,"__begin"
	}
	,{
		"500301"	,"PLAY_CG"	,"500301"	,"CG5003"	,"__start"
	}
	,{
		"501001"	,"PLAY_CG"	,"501001"	,"CG5010"	,"__win"
	}
	,{
		"2010102"	,"PLAY_CG"	,"2010102"	,"CG20101"	,"__bossentrance"
	}
	,{
		"2010201"	,"PLAY_CG"	,"2010201"	,"CG20102"	,"__bossentrance"
	}
	,{
		"2010202"	,"PLAY_CG"	,"2010202"	,"CG20102"	,"__begin"
	}
	,{
		"2010301"	,"PLAY_CG"	,"2010301"	,"CG20103"	,"__bossentrance"
	}
	,{
		"2020101"	,"PLAY_CG"	,"2020101"	,"CG20201"	,"__bossentrance"
	}
	,{
		"2020301"	,"PLAY_CG"	,"2020301"	,"CG20203"	,"__bossentrance"
	}
	,{
		"2020401"	,"PLAY_CG"	,"2020401"	,"CG20204"	,"__bossentrance"
	}
	,{
		"2020501"	,"PLAY_CG"	,"2020501"	,"CG20205"	,"__bossentrance"
	}
	,{
		"2020601"	,"PLAY_CG"	,"2020601"	,"CG20206"	,"__bossentrance"
	}
	,{
		"2030401"	,"PLAY_CG"	,"2030401"	,"CG20304"	,"__bossentrance"
	}
	,{
		"2030501"	,"PLAY_CG"	,"2030501"	,"CG20305"	,"__bossentrance"
	}
	,{
		"2030601"	,"PLAY_CG"	,"2030601"	,"CG20306"	,"__bossentrance"
	}
	,{
		"2030701"	,"PLAY_CG"	,"2030701"	,"CG20307"	,"__bossentrance"
	}
	,{
		"2030801"	,"PLAY_CG"	,"2030801"	,"CG20308"	,"__bossentrance"
	}
	,{
		"2040201"	,"PLAY_CG"	,"2040201"	,"CG20402"	,"__bossentrance"
	}
	,{
		"2040401"	,"PLAY_CG"	,"2040401"	,"CG20404"	,"__bossentrance"
	}
	,{
		"2040501"	,"PLAY_CG"	,"2040501"	,"CG20405"	,"__bossentrance"
	}
	,{
		"2040601"	,"PLAY_CG"	,"2040601"	,"CG20406"	,"__bossentrance"
	}
	,{
		"2040701"	,"PLAY_CG"	,"2040701"	,"CG20407"	,"__bossentrance"
	}
	,{
		"2040702"	,"PLAY_CG"	,"2040702"	,"CG20407"	,"__begin"
	}
	,{
		"2040901"	,"PLAY_CG"	,"2040901"	,"CG20409"	,"__bossentrance"
	}
	,{
		"2050101"	,"PLAY_CG"	,"2050101"	,"CG20501"	,"__bossentrance"
	}
	,{
		"2050201"	,"PLAY_CG"	,"2050201"	,"CG20502"	,"__bossentrance"
	}
	,{
		"2050301"	,"PLAY_CG"	,"2050301"	,"CG20503"	,"__bossentrance"
	}
	,{
		"2050302"	,"PLAY_CG"	,"2050302"	,"CG20503"	,"__endinstance"
	}
	,{
		"2050401"	,"PLAY_CG"	,"2050401"	,"CG20504"	,"__bossentrance"
	}
	,{
		"2050501"	,"PLAY_CG"	,"2050501"	,"CG20505"	,"__bossentrance"
	}
	,{
		"2050601"	,"PLAY_CG"	,"2050601"	,"CG20506"	,"__bossentrance"
	}
	,{
		"2050701"	,"PLAY_CG"	,"2050701"	,"CG20507"	,"__bossentrance"
	}
	,{
		"2050801"	,"PLAY_CG"	,"2050801"	,"CG20508"	,"__bossentrance"
	}
	,{
		"2060101"	,"PLAY_CG"	,"2060101"	,"CG20601"	,"__bossentrance"
	}
	,{
		"2060201"	,"PLAY_CG"	,"2060201"	,"CG20602"	,"__bossentrance"
	}
	,{
		"2060301"	,"PLAY_CG"	,"2060301"	,"CG20603"	,"__bossentrance"
	}
	,{
		"2060401"	,"PLAY_CG"	,"2060401"	,"CG20604"	,"__bossentrance"
	}
	,{
		"2060402"	,"PLAY_CG"	,"2060402"	,"CG20604"	,"__waterpipe1"
	}
	,{
		"2060403"	,"PLAY_CG"	,"2060403"	,"CG20604"	,"__waterpipe2"
	}
	,{
		"2060501"	,"PLAY_CG"	,"2060501"	,"CG20605"	,"__bossentrance"
	}
	,{
		"2060601"	,"PLAY_CG"	,"2060601"	,"CG20606"	,"__bossentrance"
	}
	,{
		"2060701"	,"PLAY_CG"	,"2060701"	,"CG20607"	,"__bossentrance"
	}
	,{
		"2060801"	,"PLAY_CG"	,"2060801"	,"CG20608"	,"__bossentrance"
	}
	,{
		"2060901"	,"PLAY_CG"	,"2060901"	,"CG20609"	,"__bossentrance"
	}
	,{
		"2080101"	,"PLAY_CG"	,"2080101"	,"CG20801"	,"__bossentrance"
	}
	,{
		"2080201"	,"PLAY_CG"	,"2080201"	,"CG20802"	,"__bossentrance"
	}
	,{
		"2080301"	,"PLAY_CG"	,"2080301"	,"CG20803"	,"__bossentrance"
	}
	,{
		"2080501"	,"PLAY_CG"	,"2080501"	,"CG20805"	,"__bossentrance"
	}
	,{
		"2080601"	,"PLAY_CG"	,"2080601"	,"CG20806"	,"__bossentrance"
	}
	,{
		"2080701"	,"PLAY_CG"	,"2080701"	,"CG20807"	,"__bossentrance"
	}
	,{
		"2080801"	,"PLAY_CG"	,"2080801"	,"CG20808"	,"__bossentrance"
	}
	,{
		"2080901"	,"PLAY_CG"	,"2080901"	,"CG20809"	,"__bossentrance"
	}
	,{
		"2090501"	,"PLAY_CG"	,"2090501"	,"CG20905"	,"__bossentrance"
	}
	,{
		"2090601"	,"PLAY_CG"	,"2090601"	,"CG20906"	,"__bossentrance"
	}
	,{
		"2090701"	,"PLAY_CG"	,"2090701"	,"CG20907"	,"__bossentrance"
	}
	,{
		"2090801"	,"PLAY_CG"	,"2090801"	,"CG20908"	,"__bossentrance"
	}
	,{
		"2090901"	,"PLAY_CG"	,"2090901"	,"CG20909"	,"__bossentrance"
	}
	,{
		"2100901"	,"PLAY_CG"	,"2100901"	,"CG21009"	,"__bossentrance"
	}
	,{
		"2120901"	,"PLAY_CG"	,"2120901"	,"CG21209"	,"__bossentrance"
	}
	,{
		"2190501"	,"PLAY_CG"	,"2190501"	,"CG21905"	,"__enterinstance"
	}
	,{
		"2190502"	,"PLAY_CG"	,"2190502"	,"CG21905"	,"__killmonsters"
	}
	,{
		"2190503"	,"PLAY_CG"	,"2190503"	,"CG21905"	,"__firstwavemonsters"
	}
	,{
		"2190504"	,"PLAY_CG"	,"2190504"	,"CG21905"	,"__secondwavemonsters"
	}
	,{
		"2190505"	,"PLAY_CG"	,"2190505"	,"CG21905"	,"__thirdwavemonsters"
	}
	,{
		"2190506"	,"PLAY_CG"	,"2190506"	,"CG21905"	,"__fail"
	}
	,{
		"2190507"	,"PLAY_CG"	,"2190507"	,"CG21905"	,"__successful"
	}
	,{
		"4000101"	,"PLAY_CG"	,"4000101"	,"CG40001"	,"__bossentrance"
	}
	,{
		"4000201"	,"PLAY_CG"	,"4000201"	,"CG40002"	,"__bossentrance"
	}
	,{
		"4000301"	,"PLAY_CG"	,"4000301"	,"CG40003"	,"__bossentrance"
	}
	,{
		"4000401"	,"PLAY_CG"	,"4000401"	,"CG40004"	,"__bossentrance"
	}
	,{
		"402101"	,"PLAY_CG"	,"402101"	,"CG4021"	,"__bossentrance"
	}
	,{
		"402102"	,"PLAY_CG"	,"402102"	,"CG4021"	,"__angry"
	}
	,{
		"2000401"	,"ENTER_ELEVATOR"	,"2000401"	,"CG20004"	,"__enterelevator"
	}
	,{
		"5000001"	,"ENTER_MAP"	,"1000"	,"CG50001"	,"__begin1"
	}
	,{
		"5000002"	,"ENTER_MAP"	,"37"	,"CG20001"	,"__begin1"
	}
	,{
		"5000003"	,"PLAY_CG"	,"5000003"	,"CG21000"	,"__begin1"
	}
	,{
		"5000004"	,"ENTER_MAP"	,"3000"	,"CG3000"	,"__begin1"
	}
	,{
		"5000005"	,"PLAY_CG"	,"5000005"	,"CG20005"	,"__begin1"
	}
	,{
		"5000006"	,"PLAY_CG"	,"5000006"	,"CG20003"	,"__begin1"
	}
	,{
		"5000009"	,"PLAY_CG"	,"5000009"	,"CG60001"	,"__begin1"
	}
	,{
		"5000010"	,"PLAY_CG"	,"5000010"	,"CG200041"	,"__begin1"
	}
	,{
		"5000011"	,"PLAY_CG"	,"5000011"	,"CG200042"	,"__begin1"
	}
	,{
		"5000012"	,"PLAY_CG"	,"5000012"	,"CG200043"	,"__begin1"
	}
	,{
		"5000013"	,"PLAY_CG"	,"5000013"	,"CG200044"	,"__begin1"
	}
	,{
		"5000014"	,"PLAY_CG"	,"5000014"	,"CG200045"	,"__begin1"
	}
	,{
		"5000015"	,"PLAY_CG"	,"5000015"	,"CG200046"	,"__begin1"
	}
	,{
		"5000016"	,"PLAY_CG"	,"5000016"	,"CG200047"	,"__begin1"
	}
	,{
		"5000017"	,"PLAY_CG"	,"5000017"	,"CG200048"	,"__begin1"
	}
	,{
		"5000018"	,"FINISH_QUEST"	,"30016"	,"CG60002"	,"__begin1"
	}
	,{
		"5000019"	,"PLAY_CG"	,"5000019"	,"CG10001"	,"__begin1"
	}
	,{
		"5000020"	,"FINISH_QUEST"	,"30017"	,"CG60003"	,"__begin1"
	}
	,{
		"5000021"	,"FINISH_QUEST"	,"30018"	,"CG60004"	,"__begin1"
	}
	,{
		"5000022"	,"ENTER_MAP"	,"50022"	,"CG100010"	,"__begin1"
	}
	,{
		"5000023"	,"FINISH_QUEST"	,"8"	,"CG600051"	,"__begin1"
	}
	,{
		"5000024"	,"FINISH_QUEST"	,"14"	,"CG600052"	,"__begin1"
	}
	,{
		"5000025"	,"FINISH_QUEST"	,"27"	,"CG600053"	,"__begin1"
	}
	,{
		"5000026"	,"FINISH_QUEST"	,"33"	,"CG600054"	,"__begin1"
	}
}
return drama_trigger_cg
