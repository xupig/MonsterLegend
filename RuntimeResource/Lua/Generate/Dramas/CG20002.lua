local CG20002 = {
	{
		"__begin1"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"901"	,"1"	,"2"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"901"	,"1"	,"0.6"
	}
	,{
		"PreloadResources"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"MoveCamera"	,"0,13,0"	,"0"	,"178,1,-48"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10001"	,"4"	,"178,1,-45"	,"316.412"	,"1"
	}
	,{
		"CreateEntity"	,"NPC"	,"10004"	,"6"	,"175,1,-45"	,"316.412"	,"1"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"SetAttr"	,"10001"	,"speed"	,"5"
	}
	,{
		"MoveTo"	,"10001"	,"100,27.2,100"	,"1"
	}
	,{
		"Sleep"	,"10000"
	}
	,{
		"Judge"	,"PlayerIsLevel:10"	,"Goto:__Mark2"	,"Goto:__Mark3"
	}
	,{
		"__Mark3"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10002"	,"4"	,"175,1,-40"	,"316.412"	,"1"
	}
	,{
		"__Mark2"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10003"	,"5"	,"178,1,-40"	,"316.412"	,"1"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ShowPanel"	,"SubMainUI"	,"1"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"ShowPanel"	,"SubMainUI"	,"0"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"RotateCamera"	,"10001"	,"0"	,"45,200,0"	,"2"	,"10"	,"3"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"CameraLookAt"	,"Player"	,"slot_camera"	,"2"	,"10"	,"5,10,4"	,"3"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"RecoverCamera"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"ShakeCamera"	,"28"	,"10"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"PlayVisualFX"	,"Fx/Scene/fx_scene_transporting_effect.prefab"	,"178,0,-48"	,"300"	,"0,0,0"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"PlayTalk"	,"10001"	,"1"	,"901"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"ShowSubtitles"	,"901"	,"1300"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"PlaySkill"	,"10001"	,"1010"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"PlayAction"	,"10001"	,"1"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"AddBuff"	,"10001"	,"10009"	,"999999"	,"0"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"RemoveBuff"	,"10001"	,"10009"	,"0"
	}
	,{
		"StartAI"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"StopAI"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"KeepTimeScale"	,"0.1"	,"2"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"ExAction"	,"PlayerFaceToCamera"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"ExAction"	,"SetRadialBlurColorFul"	,"1"	,"35"
	}
	,{
		"Sleep"	,"5000"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"End"
	}
}
return CG20002
