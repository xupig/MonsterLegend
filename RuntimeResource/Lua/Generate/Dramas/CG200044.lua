local CG200044 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"SetShowPlayerModel"	,"2"
	}
	,{
		"MoveCamera"	,"5.36,180,0"	,"0"	,"-1921.9,3.06,-1899.5"	,"0"
	}
	,{
		"CreateEntity"	,"Avatar"	,"1"	,"101"	,"-1923.41,0.15,-1909.89"	,"90"	,"1"
	}
	,{
		"TeleportEntity"	,"ShowPlayer"	,"-1920.6,0.15,-1909.89"
	}
	,{
		"RotateEntity"	,"ShowPlayer"	,"0,270,0"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"110"
	}
	,{
		"PlaySkill"	,"1"	,"83"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"75"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"PlaySkill"	,"1"	,"86"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"123"
	}
	,{
		"Sleep"	,"1100"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"121"
	}
	,{
		"PlaySkill"	,"1"	,"74"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"127"
	}
	,{
		"PlaySkill"	,"1"	,"85"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"PlaySkill"	,"1"	,"86"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"ShowDamage"	,"1"	,"ShowPlayer"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"75"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"KeepTimeScale"	,"0.3"	,"2000"
	}
	,{
		"PlayAction"	,"ShowPlayer"	,"92"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"ShowPlayer"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"OnCGTriggerEventType"	,"211"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleAthletics.AthleticsPanel"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"DestroyEntities"
	}
	,{
		"SetShowPlayerModel"	,"3"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
}
return CG200044
