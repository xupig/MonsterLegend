local CG21000 = {
	{
		"__begin1"
	}
	,{
		"StopAI"
	}
	,{
		"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10003"	,"1301"	,"164.96,32,290.46"	,"90"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10004"	,"1301"	,"164.96,32,288.22"	,"60"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10005"	,"1301"	,"164.26,32,289.37"	,"30"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10006"	,"1301"	,"168.49,32,289.43"	,"120"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10007"	,"1301"	,"167.02,32,289.43"	,"210"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10008"	,"1301"	,"167.21,32,287.76"	,"90"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10009"	,"1301"	,"166.89,32,291.03"	,"30"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10010"	,"1301"	,"165.48,32,289.43"	,"60"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10011"	,"1301"	,"164.46,32,288.53"	,"120"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10012"	,"1301"	,"166.31,32,288.15"	,"210"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10013"	,"1301"	,"167.08,32,290.33"	,"270"	,"0"
	}
	,{
		"PlayAction"	,"10003"	,"92"
	}
	,{
		"PlayAction"	,"10004"	,"92"
	}
	,{
		"PlayAction"	,"10005"	,"92"
	}
	,{
		"PlayAction"	,"10006"	,"92"
	}
	,{
		"PlayAction"	,"10007"	,"92"
	}
	,{
		"PlayAction"	,"10008"	,"92"
	}
	,{
		"PlayAction"	,"10009"	,"92"
	}
	,{
		"PlayAction"	,"10010"	,"92"
	}
	,{
		"PlayAction"	,"10011"	,"92"
	}
	,{
		"PlayAction"	,"10012"	,"92"
	}
	,{
		"PlayAction"	,"10013"	,"92"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"MoveCamera"	,"0,0,0"	,"0"	,"166.29,32.61,286.23"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10001"	,"4"	,"168,31.99,288.9"	,"270"	,"0"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShowSubtitles"	,"10630"	,"7000"
	}
	,{
		"MoveTo"	,"10001"	,"162.08,31.99,288.9"	,"1.2"
	}
	,{
		"Sleep"	,"7000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"98"	,"1"	,"1000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"98"	,"1"	,"2000"
	}
	,{
		"MoveCamera"	,"0,90,0"	,"0"	,"160.3,32.61,288.39"	,"0"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"MoveCamera"	,"0,90,0"	,"0"	,"160.3,33.82,288.39"	,"2000"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"98"	,"1"	,"1000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"98"	,"1"	,"2000"
	}
	,{
		"CreateEntity"	,"Dummy"	,"10002"	,"1301"	,"160,31.99,288.9"	,"90"	,"1"
	}
	,{
		"MoveCamera"	,"0,0,0"	,"0"	,"161.61,33.87,281.7"	,"0"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"PlaySkill"	,"10001"	,"1010"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"PlayAction"	,"10002"	,"77"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"10001"	,"1020"
	}
	,{
		"Sleep"	,"2100"
	}
	,{
		"PlayAction"	,"10002"	,"75"
	}
	,{
		"Sleep"	,"150"
	}
	,{
		"PlayAction"	,"10002"	,"78"
	}
	,{
		"Sleep"	,"820"
	}
	,{
		"PlayAction"	,"10002"	,"77"
	}
	,{
		"Sleep"	,"150"
	}
	,{
		"PlayAction"	,"10002"	,"78"
	}
	,{
		"Sleep"	,"840"
	}
	,{
		"PlayAction"	,"10002"	,"77"
	}
	,{
		"Sleep"	,"1050"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"98"	,"1"	,"1000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"98"	,"1"	,"1000"
	}
	,{
		"MoveCamera"	,"0,90,0"	,"0"	,"154.35,33.87,288.7"	,"0"
	}
	,{
		"PlayAction"	,"10002"	,"92"
	}
	,{
		"PlayVisualFX"	,"Fx/Scene/fx_buff_devil"	,"165.927,33.187,289.52"	,"1000"	,"0,0,0"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"MoveCamera"	,"0,90,0"	,"0"	,"160.34,33.87,288.7"	,"2000"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"SetRadialBlurColorFul"	,"1"	,"35"
	}
	,{
		"KeepTimeScale"	,"0.7"	,"3000"
	}
	,{
		"PlaySkill"	,"10001"	,"1060"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ShowBossIntroduce"	,"4"
	}
	,{
		"Sleep"	,"2400"
	}
	,{
		"HideBossIntroduce"
	}
	,{
		"RecoverCamera"
	}
	,{
		"SetRadialBlurColorFul"	,"0"	,"35"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"StartAI"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
}
return CG21000
