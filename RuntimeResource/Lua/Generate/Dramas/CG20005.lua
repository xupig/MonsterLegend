local CG20005 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"2000"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"2000"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ShakeCamera"	,"1"	,"2000"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"CreateEntity"	,"Dummy"	,"1"	,"23"	,"140.8,47,68"	,"90"	,"1"
	}
	,{
		"CameraLookAt"	,"Player"	,"0"	,"4"	,"150"	,"143.8031,57.3,61.87"	,"4"
	}
	,{
		"Sleep"	,"4000"
	}
	,{
		"PlayAction"	,"Player"	,"75"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"PlaySkill"	,"Player"	,"101"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"MoveTo"	,"Player"	,"151,47.9,70"	,"-1"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"TeleportEntity"	,"Player"	,"143.8031,57.3,61.87"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"RecoverCamera"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
}
return CG20005
