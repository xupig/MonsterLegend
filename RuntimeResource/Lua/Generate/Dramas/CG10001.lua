local CG10001 = {
	{
		"__begin1"
	}
	,{
		"StopAI"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ChangeCameraFOV"	,"60"
	}
	,{
		"MoveCamera"	,"45,286,5"	,"0"	,"-139,60,284"	,"0"
	}
	,{
		"Sleep"	,"100"
	}
	,{
		"MoveCamera"	,"48.5,295,18"	,"1500"	,"-145.5,60,282.1"	,"1500"
	}
	,{
		"CreateEntity"	,"Dummy"	,"1"	,"2504"	,"-154.54,52.91,286"	,"84.6"	,"0"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"MoveCamera"	,"34.5,324.6,16"	,"2000"	,"-148,57.7,282.5"	,"2000"
	}
	,{
		"MoveTo"	,"1"	,"-150,53,284"	,"1.5"
	}
	,{
		"Sleep"	,"3000"
	}
	,{
		"PlayAction"	,"1"	,"3"
	}
	,{
		"PlaySound"	,"20110"
	}
	,{
		"Sleep"	,"4000"
	}
	,{
		"RecoverCamera"	,"12"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"StartAI"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
}
return CG10001
