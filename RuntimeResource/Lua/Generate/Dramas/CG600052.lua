local CG600052 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"PreloadResources"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"1"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"400"
	}
	,{
		"ShowPlayerPet"	,"0"
	}
	,{
		"HideOtherPlayer"	,"1"
	}
	,{
		"HideSelfPlayer"	,"1"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"PlayVisualFX"	,"Characters/NPC/27/fx/fx_2702_get_horse.prefab"	,"0,0,0"	,"300"	,"0,0,0"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"1"
	}
	,{
		"MoveCamera"	,"0,145,0"	,"0"	,"134,26.7,57.4"	,"0"
	}
	,{
		"MoveCamera"	,"0,135,0"	,"6000"	,"134,26.7,57.4"	,"0"
	}
	,{
		"Judge"	,"IsVocGroup:1"	,"Goto:__Mark3"	,"null"
	}
	,{
		"CreateEntity"	,"NPC"	,"10001"	,"149"	,"137.36,25.38,53.45"	,"0"	,"1"
	}
	,{
		"WaitingEvent"	,"CREATE_DRAMA_NPC_CONTROLLER_LOADED"	,"10001"	,"5000"
	}
	,{
		"PlaySkill"	,"10001"	,"132"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"PlaySound"	,"1019"
	}
	,{
		"Sleep"	,"350"
	}
	,{
		"PlaySound"	,"1019"
	}
	,{
		"Sleep"	,"350"
	}
	,{
		"PlaySound"	,"1019"
	}
	,{
		"Sleep"	,"350"
	}
	,{
		"PlaySound"	,"1019"
	}
	,{
		"Sleep"	,"350"
	}
	,{
		"PlaySound"	,"1019"
	}
	,{
		"Sleep"	,"350"
	}
	,{
		"PlaySound"	,"1019"
	}
	,{
		"Sleep"	,"350"
	}
	,{
		"PlaySound"	,"1019"
	}
	,{
		"Sleep"	,"600"
	}
	,{
		"Sleep"	,"300"
	}
	,{
		"KeepTimeScale"	,"0.2"	,"4000"
	}
	,{
		"ShowIntroduce"	,"27060"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"200"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"HideIntroduce"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"0"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"KeepTimeScale"	,"1"	,"100"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"ShowPlayerPet"	,"1"
	}
	,{
		"HideOtherPlayer"	,"0"
	}
	,{
		"HideSelfPlayer"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"200"
	}
	,{
		"End"
	}
	,{
		"__Mark3"
	}
	,{
		"CreateEntity"	,"NPC"	,"10002"	,"148"	,"137.36,25.38,53.45"	,"0"	,"1"
	}
	,{
		"WaitingEvent"	,"CREATE_DRAMA_NPC_CONTROLLER_LOADED"	,"10002"	,"5000"
	}
	,{
		"PlaySkill"	,"10002"	,"89"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"Sleep"	,"300"
	}
	,{
		"Sleep"	,"2700"
	}
	,{
		"KeepTimeScale"	,"0.2"	,"4000"
	}
	,{
		"ShowIntroduce"	,"27060"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"200"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"HideIntroduce"
	}
	,{
		"EnableDramaSkill"	,"Player"	,"0"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"KeepTimeScale"	,"1"	,"100"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"ShowPlayerPet"	,"1"
	}
	,{
		"HideOtherPlayer"	,"0"
	}
	,{
		"HideSelfPlayer"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"200"
	}
	,{
		"End"
	}
}
return CG600052
