local CG100010 = {
	{
		"__begin1"
	}
	,{
		"StopAutoTask"
	}
	,{
		"PreloadResources"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"400"
	}
	,{
		"ShowPlayerPet"	,"0"
	}
	,{
		"HideOtherPlayer"	,"1"
	}
	,{
		"HideSelfPlayer"	,"1"
	}
	,{
		"CreateEntity"	,"NPC"	,"10001"	,"171"	,"153.7,61.88,214.6"	,"180"	,"1"
	}
	,{
		"MoveCamera"	,"340,0,0"	,"0"	,"153.43,63.38,206.29"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"PlaySkill"	,"10001"	,"50303"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"MoveCamera"	,"359,0,0"	,"250"	,"153.43,63.38,204"	,"250"
	}
	,{
		"ShowIntroduce"	,"38006"
	}
	,{
		"ExAction"	,"SetGlobalRim"	,"1"
	}
	,{
		"Sleep"	,"2000"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"0"	,"1"	,"400"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"ShowPanel"	,"CGSkip"	,"0"
	}
	,{
		"HideIntroduce"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ShowNPC"	,"1"
	}
	,{
		"ShowPanel"	,"BlackSide"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"ShowPlayerPet"	,"1"
	}
	,{
		"HideOtherPlayer"	,"0"
	}
	,{
		"HideSelfPlayer"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"0"	,"1"	,"300"
	}
	,{
		"End"
	}
}
return CG100010
