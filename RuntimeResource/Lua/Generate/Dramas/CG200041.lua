local CG200041 = {
	{
		"__begin1"
	}
	,{
		"MoveCamera"	,"5.36,180,0"	,"0"	,"-1921.9,3.06,-1899.5"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"StopAutoTask"
	}
	,{
		"SetShowPlayerModel"	,"2"
	}
	,{
		"CreateEntity"	,"Avatar"	,"1"	,"201"	,"-1923.71,0.15,-1909.89"	,"90"	,"1"
	}
	,{
		"TeleportEntity"	,"ShowPlayer"	,"-1919.99,0.15,-1909.89"
	}
	,{
		"RotateEntity"	,"ShowPlayer"	,"0,270,0"
	}
	,{
		"Sleep"	,"200"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"110"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"1"	,"105"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"PlaySkill"	,"1"	,"110"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"107"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"PlayAction"	,"1"	,"75"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlaySkill"	,"1"	,"107"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"107"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"1"	,"126"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"107"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"PlaySkill"	,"1"	,"105"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"Sleep"	,"1500"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"ShowDamage"	,"ShowPlayer"	,"1"
	}
	,{
		"Sleep"	,"500"
	}
	,{
		"KeepTimeScale"	,"0.4"	,"2500"
	}
	,{
		"PlaySkill"	,"ShowPlayer"	,"126"
	}
	,{
		"Sleep"	,"700"
	}
	,{
		"PlayAction"	,"1"	,"92"
	}
	,{
		"Sleep"	,"400"
	}
	,{
		"ShowDamage"	,"1"	,"1"
	}
	,{
		"Sleep"	,"1800"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
	,{
		"OnCGTriggerEventType"	,"220"
	}
	,{
		"WaitingEvent"	,"ON_OPEN_VIEW"	,"Modules.ModuleAthletics.AthleticsPanel"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"DestroyEntities"
	}
	,{
		"SetShowPlayerModel"	,"3"
	}
	,{
		"BeginAutoTask"
	}
	,{
		"End"
	}
}
return CG200041
