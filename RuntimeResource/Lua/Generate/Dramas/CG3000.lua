local CG3000 = {
	{
		"__begin1"
	}
	,{
		"StopAI"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"0"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"0"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"1"	,"19005"	,"0"	,"0"
	}
	,{
		"CreateEntity"	,"Dummy"	,"1000"	,"20201"	,"87.14,65.76,386.5"	,"22"	,"0"
	}
	,{
		"MoveCamera"	,"5,89.638,0"	,"0"	,"84.19,66.62,386.73"	,"0"
	}
	,{
		"PlayVisualFX"	,"Characters/NPC/202/fx/fx_202_skill_heianxiyin.prefab"	,"10.1,3.2,61.1"	,"300"	,"0,0,0"
	}
	,{
		"Sleep"	,"3500"
	}
	,{
		"ExAction"	,"ShowChangeMask"	,"0"	,"19005"	,"1"	,"1200"
	}
	,{
		"KeepTimeScale"	,"0.45"	,"500"
	}
	,{
		"PlaySkill"	,"1000"	,"20215"
	}
	,{
		"Sleep"	,"1200"
	}
	,{
		"PlaySound"	,"20001"
	}
	,{
		"KeepTimeScale"	,"0.7"	,"6000"
	}
	,{
		"MoveCamera"	,"5,172.59,0"	,"4500"	,"86.99,69.93,393.61"	,"4500"
	}
	,{
		"Sleep"	,"3500"
	}
	,{
		"PlaySkill"	,"1000"	,"20212"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySound"	,"20002"
	}
	,{
		"Sleep"	,"1000"
	}
	,{
		"PlaySound"	,"20002"
	}
	,{
		"KeepTimeScale"	,"0.1"	,"4000"
	}
	,{
		"MoveCamera"	,"5,172.59,0"	,"0"	,"87.3,68.7,393.19"	,"0"
	}
	,{
		"ShowBossIntroduce"	,"20201"
	}
	,{
		"Sleep"	,"800"
	}
	,{
		"HideBossIntroduce"
	}
	,{
		"DestroyEntities"
	}
	,{
		"ExAction"	,"ShowMonstersAndNPCs"	,"1"
	}
	,{
		"RecoverCamera"
	}
	,{
		"ExAction"	,"ShowMainUI"	,"1"
	}
	,{
		"StartAI"
	}
	,{
		"End"
	}
	,{
		"_ON_END"
	}
}
return CG3000
